﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "object-internals.h"

// System.Collections.Generic.Dictionary`2<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.HoloLensExtendedTrackingManager/PoseAgeEntry>
struct Dictionary_2_t3291483785;
// System.Collections.Generic.HashSet`1<System.Int32>
struct HashSet_1_t420792289;
// System.Collections.Generic.Dictionary`2<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.HoloLensExtendedTrackingManager/PoseInfo>
struct Dictionary_2_t447390288;
// System.Collections.Generic.Dictionary`2<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.TrackableBehaviour/Status>
struct Dictionary_2_t3843164752;
// Vuforia.ARController
struct ARController_t1205915989;
// Vuforia.VuforiaAbstractBehaviour
struct VuforiaAbstractBehaviour_t153010168;
// Vuforia.IUnityPlayer
struct IUnityPlayer_t1272597385;
// System.String
struct String_t;
// System.Collections.Generic.Dictionary`2<System.Int32,System.Collections.Generic.List`1<Vuforia.VuMarkAbstractBehaviour>>
struct Dictionary_2_t2703772225;
// System.Collections.Generic.List`1<Vuforia.VuMarkTarget>
struct List_1_t3023981757;
// System.Collections.Generic.List`1<Vuforia.VuMarkAbstractBehaviour>
struct List_1_t1507765061;
// System.Action`1<Vuforia.VuMarkTarget>
struct Action_1_t4154659892;
// System.Action`1<Vuforia.VuMarkAbstractBehaviour>
struct Action_1_t2638443196;
// System.Char[]
struct CharU5BU5D_t45629911;
// System.Void
struct Void_t2217553113;
// System.Single[]
struct SingleU5BU5D_t1390472614;
// System.Collections.Generic.List`1<UnityEngine.Vector3>
struct List_1_t1758679641;
// System.Collections.Generic.List`1<UnityEngine.Color32>
struct List_1_t3435548858;
// System.Collections.Generic.List`1<UnityEngine.Vector2>
struct List_1_t2432482469;
// System.Collections.Generic.List`1<UnityEngine.Vector4>
struct List_1_t1669821552;
// System.Collections.Generic.List`1<System.Int32>
struct List_1_t3093093796;
// Vuforia.DataSetImpl
struct DataSetImpl_t223764247;
// Vuforia.EyewearCalibrationProfileManager
struct EyewearCalibrationProfileManager_t1615252077;
// Vuforia.EyewearUserCalibrator
struct EyewearUserCalibrator_t173744680;
// System.Action
struct Action_t3619184611;
// UnityEngine.GameObject
struct GameObject_t1811656094;
// UnityEngine.Shader
struct Shader_t3778723916;
// UnityEngine.Camera
struct Camera_t3175186167;
// Vuforia.VuforiaARController
struct VuforiaARController_t1328503142;
// Vuforia.HideExcessAreaAbstractBehaviour[]
struct HideExcessAreaAbstractBehaviourU5BU5D_t1918497157;
// System.Collections.Generic.List`1<Vuforia.HideExcessAreaAbstractBehaviour>
struct List_1_t179562595;
// System.Collections.Generic.Dictionary`2<UnityEngine.Camera,Vuforia.VideoBackgroundAbstractBehaviour>
struct Dictionary_2_t4258919043;
// Vuforia.BackgroundPlaneAbstractBehaviour
struct BackgroundPlaneAbstractBehaviour_t4259835756;
// Vuforia.VuMarkTemplateImpl
struct VuMarkTemplateImpl_t1477556351;
// Vuforia.InstanceIdImpl
struct InstanceIdImpl_t2419732903;
// Vuforia.Image
struct Image_t1445313791;
// Vuforia.DigitalEyewearARController
struct DigitalEyewearARController_t1877756253;
// Vuforia.VideoBackgroundManager
struct VideoBackgroundManager_t2068552549;
// Vuforia.IViewerParameters
struct IViewerParameters_t1927742187;
// UnityEngine.Transform
struct Transform_t3316442598;
// Vuforia.Trackable
struct Trackable_t1140696582;
// Vuforia.DigitalEyewearARController/SerializableViewerParameters
struct SerializableViewerParameters_t484734121;
// Vuforia.DistortionRenderingBehaviour
struct DistortionRenderingBehaviour_t2785666256;
// System.Byte[]
struct ByteU5BU5D_t3172826560;
// Vuforia.EyewearDevice
struct EyewearDevice_t3960213283;
// System.Collections.Generic.List`1<Vuforia.ITrackableEventHandler>
struct List_1_t3589236026;
// System.Int32[]
struct Int32U5BU5D_t2324750880;
// UnityEngine.Camera[]
struct CameraU5BU5D_t1519774542;
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t2021098027;
// UnityEngine.RenderTexture[]
struct RenderTextureU5BU5D_t4230059505;
// Vuforia.ObjectTracker
struct ObjectTracker_t4023638979;
// System.Collections.Generic.List`1<Vuforia.ICloudRecoEventHandler>
struct List_1_t1465274546;
// Vuforia.IExcessAreaClipping
struct IExcessAreaClipping_t2974524491;
// UnityEngine.Mesh
struct Mesh_t996500909;
// Vuforia.ReconstructionFromTargetAbstractBehaviour
struct ReconstructionFromTargetAbstractBehaviour_t2676804737;
// UnityEngine.UI.Graphic
struct Graphic_t2603093343;
// Vuforia.ObjectTarget
struct ObjectTarget_t1833683407;
// UnityEngine.Texture2D
struct Texture2D_t415585320;




#ifndef U3CMODULEU3E_T160650888_H
#define U3CMODULEU3E_T160650888_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t160650888 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T160650888_H
#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef DEVICE_T3018687371_H
#define DEVICE_T3018687371_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.Device
struct  Device_t3018687371  : public RuntimeObject
{
public:

public:
};

struct Device_t3018687371_StaticFields
{
public:
	// Vuforia.Device Vuforia.Device::mInstance
	Device_t3018687371 * ___mInstance_0;

public:
	inline static int32_t get_offset_of_mInstance_0() { return static_cast<int32_t>(offsetof(Device_t3018687371_StaticFields, ___mInstance_0)); }
	inline Device_t3018687371 * get_mInstance_0() const { return ___mInstance_0; }
	inline Device_t3018687371 ** get_address_of_mInstance_0() { return &___mInstance_0; }
	inline void set_mInstance_0(Device_t3018687371 * value)
	{
		___mInstance_0 = value;
		Il2CppCodeGenWriteBarrier((&___mInstance_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEVICE_T3018687371_H
#ifndef HOLOLENSEXTENDEDTRACKINGMANAGER_T3745668720_H
#define HOLOLENSEXTENDEDTRACKINGMANAGER_T3745668720_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.HoloLensExtendedTrackingManager
struct  HoloLensExtendedTrackingManager_t3745668720  : public RuntimeObject
{
public:
	// System.Int32 Vuforia.HoloLensExtendedTrackingManager::mNumFramesStablePose
	int32_t ___mNumFramesStablePose_0;
	// System.Single Vuforia.HoloLensExtendedTrackingManager::mMaxPoseRelDistance
	float ___mMaxPoseRelDistance_1;
	// System.Single Vuforia.HoloLensExtendedTrackingManager::mMaxPoseAngleDiff
	float ___mMaxPoseAngleDiff_2;
	// System.Single Vuforia.HoloLensExtendedTrackingManager::mMaxCamPoseAbsDistance
	float ___mMaxCamPoseAbsDistance_3;
	// System.Single Vuforia.HoloLensExtendedTrackingManager::mMaxCamPoseAngleDiff
	float ___mMaxCamPoseAngleDiff_4;
	// System.Int32 Vuforia.HoloLensExtendedTrackingManager::mMinNumFramesPoseOff
	int32_t ___mMinNumFramesPoseOff_5;
	// System.Single Vuforia.HoloLensExtendedTrackingManager::mMinPoseUpdateRelDistance
	float ___mMinPoseUpdateRelDistance_6;
	// System.Single Vuforia.HoloLensExtendedTrackingManager::mMinPoseUpdateAngleDiff
	float ___mMinPoseUpdateAngleDiff_7;
	// System.Single Vuforia.HoloLensExtendedTrackingManager::mTrackableSizeInViewThreshold
	float ___mTrackableSizeInViewThreshold_8;
	// System.Single Vuforia.HoloLensExtendedTrackingManager::mMaxDistanceFromViewCenterForPoseUpdate
	float ___mMaxDistanceFromViewCenterForPoseUpdate_9;
	// System.Boolean Vuforia.HoloLensExtendedTrackingManager::mSetWorldAnchors
	bool ___mSetWorldAnchors_10;
	// System.Collections.Generic.Dictionary`2<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.HoloLensExtendedTrackingManager/PoseAgeEntry> Vuforia.HoloLensExtendedTrackingManager::mTrackingList
	Dictionary_2_t3291483785 * ___mTrackingList_11;
	// System.Collections.Generic.HashSet`1<System.Int32> Vuforia.HoloLensExtendedTrackingManager::mTrackablesExtendedTrackingEnabled
	HashSet_1_t420792289 * ___mTrackablesExtendedTrackingEnabled_12;
	// System.Collections.Generic.Dictionary`2<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.HoloLensExtendedTrackingManager/PoseInfo> Vuforia.HoloLensExtendedTrackingManager::mTrackablesCurrentlyExtendedTracked
	Dictionary_2_t447390288 * ___mTrackablesCurrentlyExtendedTracked_13;
	// System.Collections.Generic.Dictionary`2<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.TrackableBehaviour/Status> Vuforia.HoloLensExtendedTrackingManager::mExtendedTrackablesState
	Dictionary_2_t3843164752 * ___mExtendedTrackablesState_14;

public:
	inline static int32_t get_offset_of_mNumFramesStablePose_0() { return static_cast<int32_t>(offsetof(HoloLensExtendedTrackingManager_t3745668720, ___mNumFramesStablePose_0)); }
	inline int32_t get_mNumFramesStablePose_0() const { return ___mNumFramesStablePose_0; }
	inline int32_t* get_address_of_mNumFramesStablePose_0() { return &___mNumFramesStablePose_0; }
	inline void set_mNumFramesStablePose_0(int32_t value)
	{
		___mNumFramesStablePose_0 = value;
	}

	inline static int32_t get_offset_of_mMaxPoseRelDistance_1() { return static_cast<int32_t>(offsetof(HoloLensExtendedTrackingManager_t3745668720, ___mMaxPoseRelDistance_1)); }
	inline float get_mMaxPoseRelDistance_1() const { return ___mMaxPoseRelDistance_1; }
	inline float* get_address_of_mMaxPoseRelDistance_1() { return &___mMaxPoseRelDistance_1; }
	inline void set_mMaxPoseRelDistance_1(float value)
	{
		___mMaxPoseRelDistance_1 = value;
	}

	inline static int32_t get_offset_of_mMaxPoseAngleDiff_2() { return static_cast<int32_t>(offsetof(HoloLensExtendedTrackingManager_t3745668720, ___mMaxPoseAngleDiff_2)); }
	inline float get_mMaxPoseAngleDiff_2() const { return ___mMaxPoseAngleDiff_2; }
	inline float* get_address_of_mMaxPoseAngleDiff_2() { return &___mMaxPoseAngleDiff_2; }
	inline void set_mMaxPoseAngleDiff_2(float value)
	{
		___mMaxPoseAngleDiff_2 = value;
	}

	inline static int32_t get_offset_of_mMaxCamPoseAbsDistance_3() { return static_cast<int32_t>(offsetof(HoloLensExtendedTrackingManager_t3745668720, ___mMaxCamPoseAbsDistance_3)); }
	inline float get_mMaxCamPoseAbsDistance_3() const { return ___mMaxCamPoseAbsDistance_3; }
	inline float* get_address_of_mMaxCamPoseAbsDistance_3() { return &___mMaxCamPoseAbsDistance_3; }
	inline void set_mMaxCamPoseAbsDistance_3(float value)
	{
		___mMaxCamPoseAbsDistance_3 = value;
	}

	inline static int32_t get_offset_of_mMaxCamPoseAngleDiff_4() { return static_cast<int32_t>(offsetof(HoloLensExtendedTrackingManager_t3745668720, ___mMaxCamPoseAngleDiff_4)); }
	inline float get_mMaxCamPoseAngleDiff_4() const { return ___mMaxCamPoseAngleDiff_4; }
	inline float* get_address_of_mMaxCamPoseAngleDiff_4() { return &___mMaxCamPoseAngleDiff_4; }
	inline void set_mMaxCamPoseAngleDiff_4(float value)
	{
		___mMaxCamPoseAngleDiff_4 = value;
	}

	inline static int32_t get_offset_of_mMinNumFramesPoseOff_5() { return static_cast<int32_t>(offsetof(HoloLensExtendedTrackingManager_t3745668720, ___mMinNumFramesPoseOff_5)); }
	inline int32_t get_mMinNumFramesPoseOff_5() const { return ___mMinNumFramesPoseOff_5; }
	inline int32_t* get_address_of_mMinNumFramesPoseOff_5() { return &___mMinNumFramesPoseOff_5; }
	inline void set_mMinNumFramesPoseOff_5(int32_t value)
	{
		___mMinNumFramesPoseOff_5 = value;
	}

	inline static int32_t get_offset_of_mMinPoseUpdateRelDistance_6() { return static_cast<int32_t>(offsetof(HoloLensExtendedTrackingManager_t3745668720, ___mMinPoseUpdateRelDistance_6)); }
	inline float get_mMinPoseUpdateRelDistance_6() const { return ___mMinPoseUpdateRelDistance_6; }
	inline float* get_address_of_mMinPoseUpdateRelDistance_6() { return &___mMinPoseUpdateRelDistance_6; }
	inline void set_mMinPoseUpdateRelDistance_6(float value)
	{
		___mMinPoseUpdateRelDistance_6 = value;
	}

	inline static int32_t get_offset_of_mMinPoseUpdateAngleDiff_7() { return static_cast<int32_t>(offsetof(HoloLensExtendedTrackingManager_t3745668720, ___mMinPoseUpdateAngleDiff_7)); }
	inline float get_mMinPoseUpdateAngleDiff_7() const { return ___mMinPoseUpdateAngleDiff_7; }
	inline float* get_address_of_mMinPoseUpdateAngleDiff_7() { return &___mMinPoseUpdateAngleDiff_7; }
	inline void set_mMinPoseUpdateAngleDiff_7(float value)
	{
		___mMinPoseUpdateAngleDiff_7 = value;
	}

	inline static int32_t get_offset_of_mTrackableSizeInViewThreshold_8() { return static_cast<int32_t>(offsetof(HoloLensExtendedTrackingManager_t3745668720, ___mTrackableSizeInViewThreshold_8)); }
	inline float get_mTrackableSizeInViewThreshold_8() const { return ___mTrackableSizeInViewThreshold_8; }
	inline float* get_address_of_mTrackableSizeInViewThreshold_8() { return &___mTrackableSizeInViewThreshold_8; }
	inline void set_mTrackableSizeInViewThreshold_8(float value)
	{
		___mTrackableSizeInViewThreshold_8 = value;
	}

	inline static int32_t get_offset_of_mMaxDistanceFromViewCenterForPoseUpdate_9() { return static_cast<int32_t>(offsetof(HoloLensExtendedTrackingManager_t3745668720, ___mMaxDistanceFromViewCenterForPoseUpdate_9)); }
	inline float get_mMaxDistanceFromViewCenterForPoseUpdate_9() const { return ___mMaxDistanceFromViewCenterForPoseUpdate_9; }
	inline float* get_address_of_mMaxDistanceFromViewCenterForPoseUpdate_9() { return &___mMaxDistanceFromViewCenterForPoseUpdate_9; }
	inline void set_mMaxDistanceFromViewCenterForPoseUpdate_9(float value)
	{
		___mMaxDistanceFromViewCenterForPoseUpdate_9 = value;
	}

	inline static int32_t get_offset_of_mSetWorldAnchors_10() { return static_cast<int32_t>(offsetof(HoloLensExtendedTrackingManager_t3745668720, ___mSetWorldAnchors_10)); }
	inline bool get_mSetWorldAnchors_10() const { return ___mSetWorldAnchors_10; }
	inline bool* get_address_of_mSetWorldAnchors_10() { return &___mSetWorldAnchors_10; }
	inline void set_mSetWorldAnchors_10(bool value)
	{
		___mSetWorldAnchors_10 = value;
	}

	inline static int32_t get_offset_of_mTrackingList_11() { return static_cast<int32_t>(offsetof(HoloLensExtendedTrackingManager_t3745668720, ___mTrackingList_11)); }
	inline Dictionary_2_t3291483785 * get_mTrackingList_11() const { return ___mTrackingList_11; }
	inline Dictionary_2_t3291483785 ** get_address_of_mTrackingList_11() { return &___mTrackingList_11; }
	inline void set_mTrackingList_11(Dictionary_2_t3291483785 * value)
	{
		___mTrackingList_11 = value;
		Il2CppCodeGenWriteBarrier((&___mTrackingList_11), value);
	}

	inline static int32_t get_offset_of_mTrackablesExtendedTrackingEnabled_12() { return static_cast<int32_t>(offsetof(HoloLensExtendedTrackingManager_t3745668720, ___mTrackablesExtendedTrackingEnabled_12)); }
	inline HashSet_1_t420792289 * get_mTrackablesExtendedTrackingEnabled_12() const { return ___mTrackablesExtendedTrackingEnabled_12; }
	inline HashSet_1_t420792289 ** get_address_of_mTrackablesExtendedTrackingEnabled_12() { return &___mTrackablesExtendedTrackingEnabled_12; }
	inline void set_mTrackablesExtendedTrackingEnabled_12(HashSet_1_t420792289 * value)
	{
		___mTrackablesExtendedTrackingEnabled_12 = value;
		Il2CppCodeGenWriteBarrier((&___mTrackablesExtendedTrackingEnabled_12), value);
	}

	inline static int32_t get_offset_of_mTrackablesCurrentlyExtendedTracked_13() { return static_cast<int32_t>(offsetof(HoloLensExtendedTrackingManager_t3745668720, ___mTrackablesCurrentlyExtendedTracked_13)); }
	inline Dictionary_2_t447390288 * get_mTrackablesCurrentlyExtendedTracked_13() const { return ___mTrackablesCurrentlyExtendedTracked_13; }
	inline Dictionary_2_t447390288 ** get_address_of_mTrackablesCurrentlyExtendedTracked_13() { return &___mTrackablesCurrentlyExtendedTracked_13; }
	inline void set_mTrackablesCurrentlyExtendedTracked_13(Dictionary_2_t447390288 * value)
	{
		___mTrackablesCurrentlyExtendedTracked_13 = value;
		Il2CppCodeGenWriteBarrier((&___mTrackablesCurrentlyExtendedTracked_13), value);
	}

	inline static int32_t get_offset_of_mExtendedTrackablesState_14() { return static_cast<int32_t>(offsetof(HoloLensExtendedTrackingManager_t3745668720, ___mExtendedTrackablesState_14)); }
	inline Dictionary_2_t3843164752 * get_mExtendedTrackablesState_14() const { return ___mExtendedTrackablesState_14; }
	inline Dictionary_2_t3843164752 ** get_address_of_mExtendedTrackablesState_14() { return &___mExtendedTrackablesState_14; }
	inline void set_mExtendedTrackablesState_14(Dictionary_2_t3843164752 * value)
	{
		___mExtendedTrackablesState_14 = value;
		Il2CppCodeGenWriteBarrier((&___mExtendedTrackablesState_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HOLOLENSEXTENDEDTRACKINGMANAGER_T3745668720_H
#ifndef TRACKER_T1731515739_H
#define TRACKER_T1731515739_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.Tracker
struct  Tracker_t1731515739  : public RuntimeObject
{
public:
	// System.Boolean Vuforia.Tracker::<IsActive>k__BackingField
	bool ___U3CIsActiveU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CIsActiveU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(Tracker_t1731515739, ___U3CIsActiveU3Ek__BackingField_0)); }
	inline bool get_U3CIsActiveU3Ek__BackingField_0() const { return ___U3CIsActiveU3Ek__BackingField_0; }
	inline bool* get_address_of_U3CIsActiveU3Ek__BackingField_0() { return &___U3CIsActiveU3Ek__BackingField_0; }
	inline void set_U3CIsActiveU3Ek__BackingField_0(bool value)
	{
		___U3CIsActiveU3Ek__BackingField_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRACKER_T1731515739_H
#ifndef DELEGATEHELPER_T2291928084_H
#define DELEGATEHELPER_T2291928084_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.DelegateHelper
struct  DelegateHelper_t2291928084  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DELEGATEHELPER_T2291928084_H
#ifndef EYEWEARUSERCALIBRATOR_T173744680_H
#define EYEWEARUSERCALIBRATOR_T173744680_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.EyewearUserCalibrator
struct  EyewearUserCalibrator_t173744680  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EYEWEARUSERCALIBRATOR_T173744680_H
#ifndef EYEWEARCALIBRATIONPROFILEMANAGER_T1615252077_H
#define EYEWEARCALIBRATIONPROFILEMANAGER_T1615252077_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.EyewearCalibrationProfileManager
struct  EyewearCalibrationProfileManager_t1615252077  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EYEWEARCALIBRATIONPROFILEMANAGER_T1615252077_H
#ifndef VUFORIAEXTENDEDTRACKINGMANAGER_T368417945_H
#define VUFORIAEXTENDEDTRACKINGMANAGER_T368417945_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.VuforiaExtendedTrackingManager
struct  VuforiaExtendedTrackingManager_t368417945  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VUFORIAEXTENDEDTRACKINGMANAGER_T368417945_H
#ifndef NULLHOLOLENSAPIABSTRACTION_T4291924892_H
#define NULLHOLOLENSAPIABSTRACTION_T4291924892_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.NullHoloLensApiAbstraction
struct  NullHoloLensApiAbstraction_t4291924892  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLHOLOLENSAPIABSTRACTION_T4291924892_H
#ifndef NULLHIDEEXCESSAREACLIPPING_T4203830031_H
#define NULLHIDEEXCESSAREACLIPPING_T4203830031_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.NullHideExcessAreaClipping
struct  NullHideExcessAreaClipping_t4203830031  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLHIDEEXCESSAREACLIPPING_T4203830031_H
#ifndef MESHUTILS_T4112614896_H
#define MESHUTILS_T4112614896_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.MeshUtils
struct  MeshUtils_t4112614896  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MESHUTILS_T4112614896_H
#ifndef VALUETYPE_T1306072465_H
#define VALUETYPE_T1306072465_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t1306072465  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t1306072465_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t1306072465_marshaled_com
{
};
#endif // VALUETYPE_T1306072465_H
#ifndef VUMARKMANAGER_T2090159093_H
#define VUMARKMANAGER_T2090159093_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.VuMarkManager
struct  VuMarkManager_t2090159093  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VUMARKMANAGER_T2090159093_H
#ifndef UNITYCAMERAEXTENSIONS_T1038291701_H
#define UNITYCAMERAEXTENSIONS_T1038291701_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.UnityCameraExtensions
struct  UnityCameraExtensions_t1038291701  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYCAMERAEXTENSIONS_T1038291701_H
#ifndef NULLUNITYPLAYER_T1636390204_H
#define NULLUNITYPLAYER_T1636390204_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.NullUnityPlayer
struct  NullUnityPlayer_t1636390204  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLUNITYPLAYER_T1636390204_H
#ifndef ATTRIBUTE_T841672618_H
#define ATTRIBUTE_T841672618_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Attribute
struct  Attribute_t841672618  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ATTRIBUTE_T841672618_H
#ifndef U3CU3EC__DISPLAYCLASS11_0_T2399120476_H
#define U3CU3EC__DISPLAYCLASS11_0_T2399120476_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.ARController/<>c__DisplayClass11_0
struct  U3CU3Ec__DisplayClass11_0_t2399120476  : public RuntimeObject
{
public:
	// Vuforia.ARController Vuforia.ARController/<>c__DisplayClass11_0::controller
	ARController_t1205915989 * ___controller_0;

public:
	inline static int32_t get_offset_of_controller_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass11_0_t2399120476, ___controller_0)); }
	inline ARController_t1205915989 * get_controller_0() const { return ___controller_0; }
	inline ARController_t1205915989 ** get_address_of_controller_0() { return &___controller_0; }
	inline void set_controller_0(ARController_t1205915989 * value)
	{
		___controller_0 = value;
		Il2CppCodeGenWriteBarrier((&___controller_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS11_0_T2399120476_H
#ifndef ARCONTROLLER_T1205915989_H
#define ARCONTROLLER_T1205915989_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.ARController
struct  ARController_t1205915989  : public RuntimeObject
{
public:
	// Vuforia.VuforiaAbstractBehaviour Vuforia.ARController::mVuforiaBehaviour
	VuforiaAbstractBehaviour_t153010168 * ___mVuforiaBehaviour_0;

public:
	inline static int32_t get_offset_of_mVuforiaBehaviour_0() { return static_cast<int32_t>(offsetof(ARController_t1205915989, ___mVuforiaBehaviour_0)); }
	inline VuforiaAbstractBehaviour_t153010168 * get_mVuforiaBehaviour_0() const { return ___mVuforiaBehaviour_0; }
	inline VuforiaAbstractBehaviour_t153010168 ** get_address_of_mVuforiaBehaviour_0() { return &___mVuforiaBehaviour_0; }
	inline void set_mVuforiaBehaviour_0(VuforiaAbstractBehaviour_t153010168 * value)
	{
		___mVuforiaBehaviour_0 = value;
		Il2CppCodeGenWriteBarrier((&___mVuforiaBehaviour_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARCONTROLLER_T1205915989_H
#ifndef UNITYPLAYER_T3865992016_H
#define UNITYPLAYER_T3865992016_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.UnityPlayer
struct  UnityPlayer_t3865992016  : public RuntimeObject
{
public:

public:
};

struct UnityPlayer_t3865992016_StaticFields
{
public:
	// Vuforia.IUnityPlayer Vuforia.UnityPlayer::sPlayer
	RuntimeObject* ___sPlayer_0;

public:
	inline static int32_t get_offset_of_sPlayer_0() { return static_cast<int32_t>(offsetof(UnityPlayer_t3865992016_StaticFields, ___sPlayer_0)); }
	inline RuntimeObject* get_sPlayer_0() const { return ___sPlayer_0; }
	inline RuntimeObject** get_address_of_sPlayer_0() { return &___sPlayer_0; }
	inline void set_sPlayer_0(RuntimeObject* value)
	{
		___sPlayer_0 = value;
		Il2CppCodeGenWriteBarrier((&___sPlayer_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYPLAYER_T3865992016_H
#ifndef CAMERADEVICE_T2431414939_H
#define CAMERADEVICE_T2431414939_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.CameraDevice
struct  CameraDevice_t2431414939  : public RuntimeObject
{
public:

public:
};

struct CameraDevice_t2431414939_StaticFields
{
public:
	// Vuforia.CameraDevice Vuforia.CameraDevice::mInstance
	CameraDevice_t2431414939 * ___mInstance_0;

public:
	inline static int32_t get_offset_of_mInstance_0() { return static_cast<int32_t>(offsetof(CameraDevice_t2431414939_StaticFields, ___mInstance_0)); }
	inline CameraDevice_t2431414939 * get_mInstance_0() const { return ___mInstance_0; }
	inline CameraDevice_t2431414939 ** get_address_of_mInstance_0() { return &___mInstance_0; }
	inline void set_mInstance_0(CameraDevice_t2431414939 * value)
	{
		___mInstance_0 = value;
		Il2CppCodeGenWriteBarrier((&___mInstance_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAMERADEVICE_T2431414939_H
#ifndef IOSCAMRECOVERINGHELPER_T853811003_H
#define IOSCAMRECOVERINGHELPER_T853811003_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.IOSCamRecoveringHelper
struct  IOSCamRecoveringHelper_t853811003  : public RuntimeObject
{
public:

public:
};

struct IOSCamRecoveringHelper_t853811003_StaticFields
{
public:
	// System.Boolean Vuforia.IOSCamRecoveringHelper::sHasJustResumed
	bool ___sHasJustResumed_3;
	// System.Boolean Vuforia.IOSCamRecoveringHelper::sCheckFailedFrameAfterResume
	bool ___sCheckFailedFrameAfterResume_4;
	// System.Int32 Vuforia.IOSCamRecoveringHelper::sCheckedFailedFrameCounter
	int32_t ___sCheckedFailedFrameCounter_5;
	// System.Boolean Vuforia.IOSCamRecoveringHelper::sWaitToRecoverCameraRestart
	bool ___sWaitToRecoverCameraRestart_6;
	// System.Int32 Vuforia.IOSCamRecoveringHelper::sWaitedFrameRecoverCounter
	int32_t ___sWaitedFrameRecoverCounter_7;
	// System.Int32 Vuforia.IOSCamRecoveringHelper::sRecoveryAttemptCounter
	int32_t ___sRecoveryAttemptCounter_8;

public:
	inline static int32_t get_offset_of_sHasJustResumed_3() { return static_cast<int32_t>(offsetof(IOSCamRecoveringHelper_t853811003_StaticFields, ___sHasJustResumed_3)); }
	inline bool get_sHasJustResumed_3() const { return ___sHasJustResumed_3; }
	inline bool* get_address_of_sHasJustResumed_3() { return &___sHasJustResumed_3; }
	inline void set_sHasJustResumed_3(bool value)
	{
		___sHasJustResumed_3 = value;
	}

	inline static int32_t get_offset_of_sCheckFailedFrameAfterResume_4() { return static_cast<int32_t>(offsetof(IOSCamRecoveringHelper_t853811003_StaticFields, ___sCheckFailedFrameAfterResume_4)); }
	inline bool get_sCheckFailedFrameAfterResume_4() const { return ___sCheckFailedFrameAfterResume_4; }
	inline bool* get_address_of_sCheckFailedFrameAfterResume_4() { return &___sCheckFailedFrameAfterResume_4; }
	inline void set_sCheckFailedFrameAfterResume_4(bool value)
	{
		___sCheckFailedFrameAfterResume_4 = value;
	}

	inline static int32_t get_offset_of_sCheckedFailedFrameCounter_5() { return static_cast<int32_t>(offsetof(IOSCamRecoveringHelper_t853811003_StaticFields, ___sCheckedFailedFrameCounter_5)); }
	inline int32_t get_sCheckedFailedFrameCounter_5() const { return ___sCheckedFailedFrameCounter_5; }
	inline int32_t* get_address_of_sCheckedFailedFrameCounter_5() { return &___sCheckedFailedFrameCounter_5; }
	inline void set_sCheckedFailedFrameCounter_5(int32_t value)
	{
		___sCheckedFailedFrameCounter_5 = value;
	}

	inline static int32_t get_offset_of_sWaitToRecoverCameraRestart_6() { return static_cast<int32_t>(offsetof(IOSCamRecoveringHelper_t853811003_StaticFields, ___sWaitToRecoverCameraRestart_6)); }
	inline bool get_sWaitToRecoverCameraRestart_6() const { return ___sWaitToRecoverCameraRestart_6; }
	inline bool* get_address_of_sWaitToRecoverCameraRestart_6() { return &___sWaitToRecoverCameraRestart_6; }
	inline void set_sWaitToRecoverCameraRestart_6(bool value)
	{
		___sWaitToRecoverCameraRestart_6 = value;
	}

	inline static int32_t get_offset_of_sWaitedFrameRecoverCounter_7() { return static_cast<int32_t>(offsetof(IOSCamRecoveringHelper_t853811003_StaticFields, ___sWaitedFrameRecoverCounter_7)); }
	inline int32_t get_sWaitedFrameRecoverCounter_7() const { return ___sWaitedFrameRecoverCounter_7; }
	inline int32_t* get_address_of_sWaitedFrameRecoverCounter_7() { return &___sWaitedFrameRecoverCounter_7; }
	inline void set_sWaitedFrameRecoverCounter_7(int32_t value)
	{
		___sWaitedFrameRecoverCounter_7 = value;
	}

	inline static int32_t get_offset_of_sRecoveryAttemptCounter_8() { return static_cast<int32_t>(offsetof(IOSCamRecoveringHelper_t853811003_StaticFields, ___sRecoveryAttemptCounter_8)); }
	inline int32_t get_sRecoveryAttemptCounter_8() const { return ___sRecoveryAttemptCounter_8; }
	inline int32_t* get_address_of_sRecoveryAttemptCounter_8() { return &___sRecoveryAttemptCounter_8; }
	inline void set_sRecoveryAttemptCounter_8(int32_t value)
	{
		___sRecoveryAttemptCounter_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IOSCAMRECOVERINGHELPER_T853811003_H
#ifndef PLAYMODEUNITYPLAYER_T2581155120_H
#define PLAYMODEUNITYPLAYER_T2581155120_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.PlayModeUnityPlayer
struct  PlayModeUnityPlayer_t2581155120  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYMODEUNITYPLAYER_T2581155120_H
#ifndef TRACKABLEIMPL_T3450720819_H
#define TRACKABLEIMPL_T3450720819_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.TrackableImpl
struct  TrackableImpl_t3450720819  : public RuntimeObject
{
public:
	// System.String Vuforia.TrackableImpl::<Name>k__BackingField
	String_t* ___U3CNameU3Ek__BackingField_0;
	// System.Int32 Vuforia.TrackableImpl::<ID>k__BackingField
	int32_t ___U3CIDU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CNameU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(TrackableImpl_t3450720819, ___U3CNameU3Ek__BackingField_0)); }
	inline String_t* get_U3CNameU3Ek__BackingField_0() const { return ___U3CNameU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CNameU3Ek__BackingField_0() { return &___U3CNameU3Ek__BackingField_0; }
	inline void set_U3CNameU3Ek__BackingField_0(String_t* value)
	{
		___U3CNameU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CNameU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CIDU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(TrackableImpl_t3450720819, ___U3CIDU3Ek__BackingField_1)); }
	inline int32_t get_U3CIDU3Ek__BackingField_1() const { return ___U3CIDU3Ek__BackingField_1; }
	inline int32_t* get_address_of_U3CIDU3Ek__BackingField_1() { return &___U3CIDU3Ek__BackingField_1; }
	inline void set_U3CIDU3Ek__BackingField_1(int32_t value)
	{
		___U3CIDU3Ek__BackingField_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRACKABLEIMPL_T3450720819_H
#ifndef BASEVERTEXEFFECT_T442648142_H
#define BASEVERTEXEFFECT_T442648142_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.BaseVertexEffect
struct  BaseVertexEffect_t442648142  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASEVERTEXEFFECT_T442648142_H
#ifndef COLOR_T2507026410_H
#define COLOR_T2507026410_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color
struct  Color_t2507026410 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t2507026410, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t2507026410, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t2507026410, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t2507026410, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR_T2507026410_H
#ifndef EYEWEARCALIBRATIONPROFILEMANAGERIMPL_T1058943192_H
#define EYEWEARCALIBRATIONPROFILEMANAGERIMPL_T1058943192_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.EyewearCalibrationProfileManagerImpl
struct  EyewearCalibrationProfileManagerImpl_t1058943192  : public EyewearCalibrationProfileManager_t1615252077
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EYEWEARCALIBRATIONPROFILEMANAGERIMPL_T1058943192_H
#ifndef FACTORYSETTER_T372426630_H
#define FACTORYSETTER_T372426630_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.FactorySetter
struct  FactorySetter_t372426630  : public Attribute_t841672618
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FACTORYSETTER_T372426630_H
#ifndef VECTOR4_T2814672345_H
#define VECTOR4_T2814672345_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector4
struct  Vector4_t2814672345 
{
public:
	// System.Single UnityEngine.Vector4::x
	float ___x_1;
	// System.Single UnityEngine.Vector4::y
	float ___y_2;
	// System.Single UnityEngine.Vector4::z
	float ___z_3;
	// System.Single UnityEngine.Vector4::w
	float ___w_4;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector4_t2814672345, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector4_t2814672345, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector4_t2814672345, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}

	inline static int32_t get_offset_of_w_4() { return static_cast<int32_t>(offsetof(Vector4_t2814672345, ___w_4)); }
	inline float get_w_4() const { return ___w_4; }
	inline float* get_address_of_w_4() { return &___w_4; }
	inline void set_w_4(float value)
	{
		___w_4 = value;
	}
};

struct Vector4_t2814672345_StaticFields
{
public:
	// UnityEngine.Vector4 UnityEngine.Vector4::zeroVector
	Vector4_t2814672345  ___zeroVector_5;
	// UnityEngine.Vector4 UnityEngine.Vector4::oneVector
	Vector4_t2814672345  ___oneVector_6;
	// UnityEngine.Vector4 UnityEngine.Vector4::positiveInfinityVector
	Vector4_t2814672345  ___positiveInfinityVector_7;
	// UnityEngine.Vector4 UnityEngine.Vector4::negativeInfinityVector
	Vector4_t2814672345  ___negativeInfinityVector_8;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector4_t2814672345_StaticFields, ___zeroVector_5)); }
	inline Vector4_t2814672345  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector4_t2814672345 * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector4_t2814672345  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector4_t2814672345_StaticFields, ___oneVector_6)); }
	inline Vector4_t2814672345  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector4_t2814672345 * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector4_t2814672345  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_7() { return static_cast<int32_t>(offsetof(Vector4_t2814672345_StaticFields, ___positiveInfinityVector_7)); }
	inline Vector4_t2814672345  get_positiveInfinityVector_7() const { return ___positiveInfinityVector_7; }
	inline Vector4_t2814672345 * get_address_of_positiveInfinityVector_7() { return &___positiveInfinityVector_7; }
	inline void set_positiveInfinityVector_7(Vector4_t2814672345  value)
	{
		___positiveInfinityVector_7 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector4_t2814672345_StaticFields, ___negativeInfinityVector_8)); }
	inline Vector4_t2814672345  get_negativeInfinityVector_8() const { return ___negativeInfinityVector_8; }
	inline Vector4_t2814672345 * get_address_of_negativeInfinityVector_8() { return &___negativeInfinityVector_8; }
	inline void set_negativeInfinityVector_8(Vector4_t2814672345  value)
	{
		___negativeInfinityVector_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR4_T2814672345_H
#ifndef VUMARKMANAGERIMPL_T2293130653_H
#define VUMARKMANAGERIMPL_T2293130653_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.VuMarkManagerImpl
struct  VuMarkManagerImpl_t2293130653  : public VuMarkManager_t2090159093
{
public:
	// System.Collections.Generic.Dictionary`2<System.Int32,System.Collections.Generic.List`1<Vuforia.VuMarkAbstractBehaviour>> Vuforia.VuMarkManagerImpl::mBehaviours
	Dictionary_2_t2703772225 * ___mBehaviours_0;
	// System.Collections.Generic.List`1<Vuforia.VuMarkTarget> Vuforia.VuMarkManagerImpl::mActiveVuMarkTargets
	List_1_t3023981757 * ___mActiveVuMarkTargets_1;
	// System.Collections.Generic.List`1<Vuforia.VuMarkAbstractBehaviour> Vuforia.VuMarkManagerImpl::mDestroyedBehaviours
	List_1_t1507765061 * ___mDestroyedBehaviours_2;
	// System.Action`1<Vuforia.VuMarkTarget> Vuforia.VuMarkManagerImpl::mOnVuMarkDetected
	Action_1_t4154659892 * ___mOnVuMarkDetected_3;
	// System.Action`1<Vuforia.VuMarkTarget> Vuforia.VuMarkManagerImpl::mOnVuMarkLost
	Action_1_t4154659892 * ___mOnVuMarkLost_4;
	// System.Action`1<Vuforia.VuMarkAbstractBehaviour> Vuforia.VuMarkManagerImpl::mOnVuMarkBehaviourDetected
	Action_1_t2638443196 * ___mOnVuMarkBehaviourDetected_5;

public:
	inline static int32_t get_offset_of_mBehaviours_0() { return static_cast<int32_t>(offsetof(VuMarkManagerImpl_t2293130653, ___mBehaviours_0)); }
	inline Dictionary_2_t2703772225 * get_mBehaviours_0() const { return ___mBehaviours_0; }
	inline Dictionary_2_t2703772225 ** get_address_of_mBehaviours_0() { return &___mBehaviours_0; }
	inline void set_mBehaviours_0(Dictionary_2_t2703772225 * value)
	{
		___mBehaviours_0 = value;
		Il2CppCodeGenWriteBarrier((&___mBehaviours_0), value);
	}

	inline static int32_t get_offset_of_mActiveVuMarkTargets_1() { return static_cast<int32_t>(offsetof(VuMarkManagerImpl_t2293130653, ___mActiveVuMarkTargets_1)); }
	inline List_1_t3023981757 * get_mActiveVuMarkTargets_1() const { return ___mActiveVuMarkTargets_1; }
	inline List_1_t3023981757 ** get_address_of_mActiveVuMarkTargets_1() { return &___mActiveVuMarkTargets_1; }
	inline void set_mActiveVuMarkTargets_1(List_1_t3023981757 * value)
	{
		___mActiveVuMarkTargets_1 = value;
		Il2CppCodeGenWriteBarrier((&___mActiveVuMarkTargets_1), value);
	}

	inline static int32_t get_offset_of_mDestroyedBehaviours_2() { return static_cast<int32_t>(offsetof(VuMarkManagerImpl_t2293130653, ___mDestroyedBehaviours_2)); }
	inline List_1_t1507765061 * get_mDestroyedBehaviours_2() const { return ___mDestroyedBehaviours_2; }
	inline List_1_t1507765061 ** get_address_of_mDestroyedBehaviours_2() { return &___mDestroyedBehaviours_2; }
	inline void set_mDestroyedBehaviours_2(List_1_t1507765061 * value)
	{
		___mDestroyedBehaviours_2 = value;
		Il2CppCodeGenWriteBarrier((&___mDestroyedBehaviours_2), value);
	}

	inline static int32_t get_offset_of_mOnVuMarkDetected_3() { return static_cast<int32_t>(offsetof(VuMarkManagerImpl_t2293130653, ___mOnVuMarkDetected_3)); }
	inline Action_1_t4154659892 * get_mOnVuMarkDetected_3() const { return ___mOnVuMarkDetected_3; }
	inline Action_1_t4154659892 ** get_address_of_mOnVuMarkDetected_3() { return &___mOnVuMarkDetected_3; }
	inline void set_mOnVuMarkDetected_3(Action_1_t4154659892 * value)
	{
		___mOnVuMarkDetected_3 = value;
		Il2CppCodeGenWriteBarrier((&___mOnVuMarkDetected_3), value);
	}

	inline static int32_t get_offset_of_mOnVuMarkLost_4() { return static_cast<int32_t>(offsetof(VuMarkManagerImpl_t2293130653, ___mOnVuMarkLost_4)); }
	inline Action_1_t4154659892 * get_mOnVuMarkLost_4() const { return ___mOnVuMarkLost_4; }
	inline Action_1_t4154659892 ** get_address_of_mOnVuMarkLost_4() { return &___mOnVuMarkLost_4; }
	inline void set_mOnVuMarkLost_4(Action_1_t4154659892 * value)
	{
		___mOnVuMarkLost_4 = value;
		Il2CppCodeGenWriteBarrier((&___mOnVuMarkLost_4), value);
	}

	inline static int32_t get_offset_of_mOnVuMarkBehaviourDetected_5() { return static_cast<int32_t>(offsetof(VuMarkManagerImpl_t2293130653, ___mOnVuMarkBehaviourDetected_5)); }
	inline Action_1_t2638443196 * get_mOnVuMarkBehaviourDetected_5() const { return ___mOnVuMarkBehaviourDetected_5; }
	inline Action_1_t2638443196 ** get_address_of_mOnVuMarkBehaviourDetected_5() { return &___mOnVuMarkBehaviourDetected_5; }
	inline void set_mOnVuMarkBehaviourDetected_5(Action_1_t2638443196 * value)
	{
		___mOnVuMarkBehaviourDetected_5 = value;
		Il2CppCodeGenWriteBarrier((&___mOnVuMarkBehaviourDetected_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VUMARKMANAGERIMPL_T2293130653_H
#ifndef VECTOR3_T2903530434_H
#define VECTOR3_T2903530434_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_t2903530434 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_1;
	// System.Single UnityEngine.Vector3::y
	float ___y_2;
	// System.Single UnityEngine.Vector3::z
	float ___z_3;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector3_t2903530434, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector3_t2903530434, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector3_t2903530434, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}
};

struct Vector3_t2903530434_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t2903530434  ___zeroVector_4;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t2903530434  ___oneVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t2903530434  ___upVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t2903530434  ___downVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t2903530434  ___leftVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t2903530434  ___rightVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t2903530434  ___forwardVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t2903530434  ___backVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t2903530434  ___positiveInfinityVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t2903530434  ___negativeInfinityVector_13;

public:
	inline static int32_t get_offset_of_zeroVector_4() { return static_cast<int32_t>(offsetof(Vector3_t2903530434_StaticFields, ___zeroVector_4)); }
	inline Vector3_t2903530434  get_zeroVector_4() const { return ___zeroVector_4; }
	inline Vector3_t2903530434 * get_address_of_zeroVector_4() { return &___zeroVector_4; }
	inline void set_zeroVector_4(Vector3_t2903530434  value)
	{
		___zeroVector_4 = value;
	}

	inline static int32_t get_offset_of_oneVector_5() { return static_cast<int32_t>(offsetof(Vector3_t2903530434_StaticFields, ___oneVector_5)); }
	inline Vector3_t2903530434  get_oneVector_5() const { return ___oneVector_5; }
	inline Vector3_t2903530434 * get_address_of_oneVector_5() { return &___oneVector_5; }
	inline void set_oneVector_5(Vector3_t2903530434  value)
	{
		___oneVector_5 = value;
	}

	inline static int32_t get_offset_of_upVector_6() { return static_cast<int32_t>(offsetof(Vector3_t2903530434_StaticFields, ___upVector_6)); }
	inline Vector3_t2903530434  get_upVector_6() const { return ___upVector_6; }
	inline Vector3_t2903530434 * get_address_of_upVector_6() { return &___upVector_6; }
	inline void set_upVector_6(Vector3_t2903530434  value)
	{
		___upVector_6 = value;
	}

	inline static int32_t get_offset_of_downVector_7() { return static_cast<int32_t>(offsetof(Vector3_t2903530434_StaticFields, ___downVector_7)); }
	inline Vector3_t2903530434  get_downVector_7() const { return ___downVector_7; }
	inline Vector3_t2903530434 * get_address_of_downVector_7() { return &___downVector_7; }
	inline void set_downVector_7(Vector3_t2903530434  value)
	{
		___downVector_7 = value;
	}

	inline static int32_t get_offset_of_leftVector_8() { return static_cast<int32_t>(offsetof(Vector3_t2903530434_StaticFields, ___leftVector_8)); }
	inline Vector3_t2903530434  get_leftVector_8() const { return ___leftVector_8; }
	inline Vector3_t2903530434 * get_address_of_leftVector_8() { return &___leftVector_8; }
	inline void set_leftVector_8(Vector3_t2903530434  value)
	{
		___leftVector_8 = value;
	}

	inline static int32_t get_offset_of_rightVector_9() { return static_cast<int32_t>(offsetof(Vector3_t2903530434_StaticFields, ___rightVector_9)); }
	inline Vector3_t2903530434  get_rightVector_9() const { return ___rightVector_9; }
	inline Vector3_t2903530434 * get_address_of_rightVector_9() { return &___rightVector_9; }
	inline void set_rightVector_9(Vector3_t2903530434  value)
	{
		___rightVector_9 = value;
	}

	inline static int32_t get_offset_of_forwardVector_10() { return static_cast<int32_t>(offsetof(Vector3_t2903530434_StaticFields, ___forwardVector_10)); }
	inline Vector3_t2903530434  get_forwardVector_10() const { return ___forwardVector_10; }
	inline Vector3_t2903530434 * get_address_of_forwardVector_10() { return &___forwardVector_10; }
	inline void set_forwardVector_10(Vector3_t2903530434  value)
	{
		___forwardVector_10 = value;
	}

	inline static int32_t get_offset_of_backVector_11() { return static_cast<int32_t>(offsetof(Vector3_t2903530434_StaticFields, ___backVector_11)); }
	inline Vector3_t2903530434  get_backVector_11() const { return ___backVector_11; }
	inline Vector3_t2903530434 * get_address_of_backVector_11() { return &___backVector_11; }
	inline void set_backVector_11(Vector3_t2903530434  value)
	{
		___backVector_11 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_12() { return static_cast<int32_t>(offsetof(Vector3_t2903530434_StaticFields, ___positiveInfinityVector_12)); }
	inline Vector3_t2903530434  get_positiveInfinityVector_12() const { return ___positiveInfinityVector_12; }
	inline Vector3_t2903530434 * get_address_of_positiveInfinityVector_12() { return &___positiveInfinityVector_12; }
	inline void set_positiveInfinityVector_12(Vector3_t2903530434  value)
	{
		___positiveInfinityVector_12 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t2903530434_StaticFields, ___negativeInfinityVector_13)); }
	inline Vector3_t2903530434  get_negativeInfinityVector_13() const { return ___negativeInfinityVector_13; }
	inline Vector3_t2903530434 * get_address_of_negativeInfinityVector_13() { return &___negativeInfinityVector_13; }
	inline void set_negativeInfinityVector_13(Vector3_t2903530434  value)
	{
		___negativeInfinityVector_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_T2903530434_H
#ifndef VECTOR2_T3577333262_H
#define VECTOR2_T3577333262_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2
struct  Vector2_t3577333262 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_t3577333262, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_t3577333262, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_t3577333262_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_t3577333262  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_t3577333262  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_t3577333262  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_t3577333262  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_t3577333262  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_t3577333262  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_t3577333262  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_t3577333262  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_t3577333262_StaticFields, ___zeroVector_2)); }
	inline Vector2_t3577333262  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_t3577333262 * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_t3577333262  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_t3577333262_StaticFields, ___oneVector_3)); }
	inline Vector2_t3577333262  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_t3577333262 * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_t3577333262  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_t3577333262_StaticFields, ___upVector_4)); }
	inline Vector2_t3577333262  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_t3577333262 * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_t3577333262  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_t3577333262_StaticFields, ___downVector_5)); }
	inline Vector2_t3577333262  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_t3577333262 * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_t3577333262  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_t3577333262_StaticFields, ___leftVector_6)); }
	inline Vector2_t3577333262  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_t3577333262 * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_t3577333262  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_t3577333262_StaticFields, ___rightVector_7)); }
	inline Vector2_t3577333262  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_t3577333262 * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_t3577333262  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_t3577333262_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_t3577333262  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_t3577333262 * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_t3577333262  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_t3577333262_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_t3577333262  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_t3577333262 * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_t3577333262  value)
	{
		___negativeInfinityVector_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_T3577333262_H
#ifndef PLAYMODEEYEWEARUSERCALIBRATORIMPL_T2691755708_H
#define PLAYMODEEYEWEARUSERCALIBRATORIMPL_T2691755708_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.PlayModeEyewearUserCalibratorImpl
struct  PlayModeEyewearUserCalibratorImpl_t2691755708  : public EyewearUserCalibrator_t173744680
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYMODEEYEWEARUSERCALIBRATORIMPL_T2691755708_H
#ifndef ENUM_T978401890_H
#define ENUM_T978401890_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t978401890  : public ValueType_t1306072465
{
public:

public:
};

struct Enum_t978401890_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t45629911* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t978401890_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t45629911* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t45629911** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t45629911* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t978401890_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t978401890_marshaled_com
{
};
#endif // ENUM_T978401890_H
#ifndef VEC2I_T895597728_H
#define VEC2I_T895597728_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.VuforiaRenderer/Vec2I
#pragma pack(push, tp, 1)
struct  Vec2I_t895597728 
{
public:
	// System.Int32 Vuforia.VuforiaRenderer/Vec2I::x
	int32_t ___x_0;
	// System.Int32 Vuforia.VuforiaRenderer/Vec2I::y
	int32_t ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vec2I_t895597728, ___x_0)); }
	inline int32_t get_x_0() const { return ___x_0; }
	inline int32_t* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(int32_t value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vec2I_t895597728, ___y_1)); }
	inline int32_t get_y_1() const { return ___y_1; }
	inline int32_t* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(int32_t value)
	{
		___y_1 = value;
	}
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VEC2I_T895597728_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	IntPtr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline IntPtr_t get_Zero_1() const { return ___Zero_1; }
	inline IntPtr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(IntPtr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef MATRIX4X4_T4266809202_H
#define MATRIX4X4_T4266809202_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Matrix4x4
struct  Matrix4x4_t4266809202 
{
public:
	// System.Single UnityEngine.Matrix4x4::m00
	float ___m00_0;
	// System.Single UnityEngine.Matrix4x4::m10
	float ___m10_1;
	// System.Single UnityEngine.Matrix4x4::m20
	float ___m20_2;
	// System.Single UnityEngine.Matrix4x4::m30
	float ___m30_3;
	// System.Single UnityEngine.Matrix4x4::m01
	float ___m01_4;
	// System.Single UnityEngine.Matrix4x4::m11
	float ___m11_5;
	// System.Single UnityEngine.Matrix4x4::m21
	float ___m21_6;
	// System.Single UnityEngine.Matrix4x4::m31
	float ___m31_7;
	// System.Single UnityEngine.Matrix4x4::m02
	float ___m02_8;
	// System.Single UnityEngine.Matrix4x4::m12
	float ___m12_9;
	// System.Single UnityEngine.Matrix4x4::m22
	float ___m22_10;
	// System.Single UnityEngine.Matrix4x4::m32
	float ___m32_11;
	// System.Single UnityEngine.Matrix4x4::m03
	float ___m03_12;
	// System.Single UnityEngine.Matrix4x4::m13
	float ___m13_13;
	// System.Single UnityEngine.Matrix4x4::m23
	float ___m23_14;
	// System.Single UnityEngine.Matrix4x4::m33
	float ___m33_15;

public:
	inline static int32_t get_offset_of_m00_0() { return static_cast<int32_t>(offsetof(Matrix4x4_t4266809202, ___m00_0)); }
	inline float get_m00_0() const { return ___m00_0; }
	inline float* get_address_of_m00_0() { return &___m00_0; }
	inline void set_m00_0(float value)
	{
		___m00_0 = value;
	}

	inline static int32_t get_offset_of_m10_1() { return static_cast<int32_t>(offsetof(Matrix4x4_t4266809202, ___m10_1)); }
	inline float get_m10_1() const { return ___m10_1; }
	inline float* get_address_of_m10_1() { return &___m10_1; }
	inline void set_m10_1(float value)
	{
		___m10_1 = value;
	}

	inline static int32_t get_offset_of_m20_2() { return static_cast<int32_t>(offsetof(Matrix4x4_t4266809202, ___m20_2)); }
	inline float get_m20_2() const { return ___m20_2; }
	inline float* get_address_of_m20_2() { return &___m20_2; }
	inline void set_m20_2(float value)
	{
		___m20_2 = value;
	}

	inline static int32_t get_offset_of_m30_3() { return static_cast<int32_t>(offsetof(Matrix4x4_t4266809202, ___m30_3)); }
	inline float get_m30_3() const { return ___m30_3; }
	inline float* get_address_of_m30_3() { return &___m30_3; }
	inline void set_m30_3(float value)
	{
		___m30_3 = value;
	}

	inline static int32_t get_offset_of_m01_4() { return static_cast<int32_t>(offsetof(Matrix4x4_t4266809202, ___m01_4)); }
	inline float get_m01_4() const { return ___m01_4; }
	inline float* get_address_of_m01_4() { return &___m01_4; }
	inline void set_m01_4(float value)
	{
		___m01_4 = value;
	}

	inline static int32_t get_offset_of_m11_5() { return static_cast<int32_t>(offsetof(Matrix4x4_t4266809202, ___m11_5)); }
	inline float get_m11_5() const { return ___m11_5; }
	inline float* get_address_of_m11_5() { return &___m11_5; }
	inline void set_m11_5(float value)
	{
		___m11_5 = value;
	}

	inline static int32_t get_offset_of_m21_6() { return static_cast<int32_t>(offsetof(Matrix4x4_t4266809202, ___m21_6)); }
	inline float get_m21_6() const { return ___m21_6; }
	inline float* get_address_of_m21_6() { return &___m21_6; }
	inline void set_m21_6(float value)
	{
		___m21_6 = value;
	}

	inline static int32_t get_offset_of_m31_7() { return static_cast<int32_t>(offsetof(Matrix4x4_t4266809202, ___m31_7)); }
	inline float get_m31_7() const { return ___m31_7; }
	inline float* get_address_of_m31_7() { return &___m31_7; }
	inline void set_m31_7(float value)
	{
		___m31_7 = value;
	}

	inline static int32_t get_offset_of_m02_8() { return static_cast<int32_t>(offsetof(Matrix4x4_t4266809202, ___m02_8)); }
	inline float get_m02_8() const { return ___m02_8; }
	inline float* get_address_of_m02_8() { return &___m02_8; }
	inline void set_m02_8(float value)
	{
		___m02_8 = value;
	}

	inline static int32_t get_offset_of_m12_9() { return static_cast<int32_t>(offsetof(Matrix4x4_t4266809202, ___m12_9)); }
	inline float get_m12_9() const { return ___m12_9; }
	inline float* get_address_of_m12_9() { return &___m12_9; }
	inline void set_m12_9(float value)
	{
		___m12_9 = value;
	}

	inline static int32_t get_offset_of_m22_10() { return static_cast<int32_t>(offsetof(Matrix4x4_t4266809202, ___m22_10)); }
	inline float get_m22_10() const { return ___m22_10; }
	inline float* get_address_of_m22_10() { return &___m22_10; }
	inline void set_m22_10(float value)
	{
		___m22_10 = value;
	}

	inline static int32_t get_offset_of_m32_11() { return static_cast<int32_t>(offsetof(Matrix4x4_t4266809202, ___m32_11)); }
	inline float get_m32_11() const { return ___m32_11; }
	inline float* get_address_of_m32_11() { return &___m32_11; }
	inline void set_m32_11(float value)
	{
		___m32_11 = value;
	}

	inline static int32_t get_offset_of_m03_12() { return static_cast<int32_t>(offsetof(Matrix4x4_t4266809202, ___m03_12)); }
	inline float get_m03_12() const { return ___m03_12; }
	inline float* get_address_of_m03_12() { return &___m03_12; }
	inline void set_m03_12(float value)
	{
		___m03_12 = value;
	}

	inline static int32_t get_offset_of_m13_13() { return static_cast<int32_t>(offsetof(Matrix4x4_t4266809202, ___m13_13)); }
	inline float get_m13_13() const { return ___m13_13; }
	inline float* get_address_of_m13_13() { return &___m13_13; }
	inline void set_m13_13(float value)
	{
		___m13_13 = value;
	}

	inline static int32_t get_offset_of_m23_14() { return static_cast<int32_t>(offsetof(Matrix4x4_t4266809202, ___m23_14)); }
	inline float get_m23_14() const { return ___m23_14; }
	inline float* get_address_of_m23_14() { return &___m23_14; }
	inline void set_m23_14(float value)
	{
		___m23_14 = value;
	}

	inline static int32_t get_offset_of_m33_15() { return static_cast<int32_t>(offsetof(Matrix4x4_t4266809202, ___m33_15)); }
	inline float get_m33_15() const { return ___m33_15; }
	inline float* get_address_of_m33_15() { return &___m33_15; }
	inline void set_m33_15(float value)
	{
		___m33_15 = value;
	}
};

struct Matrix4x4_t4266809202_StaticFields
{
public:
	// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::zeroMatrix
	Matrix4x4_t4266809202  ___zeroMatrix_16;
	// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::identityMatrix
	Matrix4x4_t4266809202  ___identityMatrix_17;

public:
	inline static int32_t get_offset_of_zeroMatrix_16() { return static_cast<int32_t>(offsetof(Matrix4x4_t4266809202_StaticFields, ___zeroMatrix_16)); }
	inline Matrix4x4_t4266809202  get_zeroMatrix_16() const { return ___zeroMatrix_16; }
	inline Matrix4x4_t4266809202 * get_address_of_zeroMatrix_16() { return &___zeroMatrix_16; }
	inline void set_zeroMatrix_16(Matrix4x4_t4266809202  value)
	{
		___zeroMatrix_16 = value;
	}

	inline static int32_t get_offset_of_identityMatrix_17() { return static_cast<int32_t>(offsetof(Matrix4x4_t4266809202_StaticFields, ___identityMatrix_17)); }
	inline Matrix4x4_t4266809202  get_identityMatrix_17() const { return ___identityMatrix_17; }
	inline Matrix4x4_t4266809202 * get_address_of_identityMatrix_17() { return &___identityMatrix_17; }
	inline void set_identityMatrix_17(Matrix4x4_t4266809202  value)
	{
		___identityMatrix_17 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATRIX4X4_T4266809202_H
#ifndef U24ARRAYTYPEU3D12_T1462559582_H
#define U24ARRAYTYPEU3D12_T1462559582_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/$ArrayType=12
#pragma pack(push, tp, 1)
struct  U24ArrayTypeU3D12_t1462559582 
{
public:
	union
	{
		struct
		{
		};
		uint8_t U24ArrayTypeU3D12_t1462559582__padding[12];
	};

public:
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U24ARRAYTYPEU3D12_T1462559582_H
#ifndef QUATERNION_T754065749_H
#define QUATERNION_T754065749_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Quaternion
struct  Quaternion_t754065749 
{
public:
	// System.Single UnityEngine.Quaternion::x
	float ___x_0;
	// System.Single UnityEngine.Quaternion::y
	float ___y_1;
	// System.Single UnityEngine.Quaternion::z
	float ___z_2;
	// System.Single UnityEngine.Quaternion::w
	float ___w_3;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Quaternion_t754065749, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Quaternion_t754065749, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(Quaternion_t754065749, ___z_2)); }
	inline float get_z_2() const { return ___z_2; }
	inline float* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(float value)
	{
		___z_2 = value;
	}

	inline static int32_t get_offset_of_w_3() { return static_cast<int32_t>(offsetof(Quaternion_t754065749, ___w_3)); }
	inline float get_w_3() const { return ___w_3; }
	inline float* get_address_of_w_3() { return &___w_3; }
	inline void set_w_3(float value)
	{
		___w_3 = value;
	}
};

struct Quaternion_t754065749_StaticFields
{
public:
	// UnityEngine.Quaternion UnityEngine.Quaternion::identityQuaternion
	Quaternion_t754065749  ___identityQuaternion_4;

public:
	inline static int32_t get_offset_of_identityQuaternion_4() { return static_cast<int32_t>(offsetof(Quaternion_t754065749_StaticFields, ___identityQuaternion_4)); }
	inline Quaternion_t754065749  get_identityQuaternion_4() const { return ___identityQuaternion_4; }
	inline Quaternion_t754065749 * get_address_of_identityQuaternion_4() { return &___identityQuaternion_4; }
	inline void set_identityQuaternion_4(Quaternion_t754065749  value)
	{
		___identityQuaternion_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUATERNION_T754065749_H
#ifndef RECT_T3436776195_H
#define RECT_T3436776195_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rect
struct  Rect_t3436776195 
{
public:
	// System.Single UnityEngine.Rect::m_XMin
	float ___m_XMin_0;
	// System.Single UnityEngine.Rect::m_YMin
	float ___m_YMin_1;
	// System.Single UnityEngine.Rect::m_Width
	float ___m_Width_2;
	// System.Single UnityEngine.Rect::m_Height
	float ___m_Height_3;

public:
	inline static int32_t get_offset_of_m_XMin_0() { return static_cast<int32_t>(offsetof(Rect_t3436776195, ___m_XMin_0)); }
	inline float get_m_XMin_0() const { return ___m_XMin_0; }
	inline float* get_address_of_m_XMin_0() { return &___m_XMin_0; }
	inline void set_m_XMin_0(float value)
	{
		___m_XMin_0 = value;
	}

	inline static int32_t get_offset_of_m_YMin_1() { return static_cast<int32_t>(offsetof(Rect_t3436776195, ___m_YMin_1)); }
	inline float get_m_YMin_1() const { return ___m_YMin_1; }
	inline float* get_address_of_m_YMin_1() { return &___m_YMin_1; }
	inline void set_m_YMin_1(float value)
	{
		___m_YMin_1 = value;
	}

	inline static int32_t get_offset_of_m_Width_2() { return static_cast<int32_t>(offsetof(Rect_t3436776195, ___m_Width_2)); }
	inline float get_m_Width_2() const { return ___m_Width_2; }
	inline float* get_address_of_m_Width_2() { return &___m_Width_2; }
	inline void set_m_Width_2(float value)
	{
		___m_Width_2 = value;
	}

	inline static int32_t get_offset_of_m_Height_3() { return static_cast<int32_t>(offsetof(Rect_t3436776195, ___m_Height_3)); }
	inline float get_m_Height_3() const { return ___m_Height_3; }
	inline float* get_address_of_m_Height_3() { return &___m_Height_3; }
	inline void set_m_Height_3(float value)
	{
		___m_Height_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RECT_T3436776195_H
#ifndef EYEWEARDEVICE_T3960213283_H
#define EYEWEARDEVICE_T3960213283_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.EyewearDevice
struct  EyewearDevice_t3960213283  : public Device_t3018687371
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EYEWEARDEVICE_T3960213283_H
#ifndef EYEWEARCALIBRATIONREADING_T972160870_H
#define EYEWEARCALIBRATIONREADING_T972160870_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.EyewearDevice/EyewearCalibrationReading
struct  EyewearCalibrationReading_t972160870 
{
public:
	// System.Single[] Vuforia.EyewearDevice/EyewearCalibrationReading::pose
	SingleU5BU5D_t1390472614* ___pose_0;
	// System.Single Vuforia.EyewearDevice/EyewearCalibrationReading::scale
	float ___scale_1;
	// System.Single Vuforia.EyewearDevice/EyewearCalibrationReading::centerX
	float ___centerX_2;
	// System.Single Vuforia.EyewearDevice/EyewearCalibrationReading::centerY
	float ___centerY_3;
	// System.Int32 Vuforia.EyewearDevice/EyewearCalibrationReading::unused
	int32_t ___unused_4;

public:
	inline static int32_t get_offset_of_pose_0() { return static_cast<int32_t>(offsetof(EyewearCalibrationReading_t972160870, ___pose_0)); }
	inline SingleU5BU5D_t1390472614* get_pose_0() const { return ___pose_0; }
	inline SingleU5BU5D_t1390472614** get_address_of_pose_0() { return &___pose_0; }
	inline void set_pose_0(SingleU5BU5D_t1390472614* value)
	{
		___pose_0 = value;
		Il2CppCodeGenWriteBarrier((&___pose_0), value);
	}

	inline static int32_t get_offset_of_scale_1() { return static_cast<int32_t>(offsetof(EyewearCalibrationReading_t972160870, ___scale_1)); }
	inline float get_scale_1() const { return ___scale_1; }
	inline float* get_address_of_scale_1() { return &___scale_1; }
	inline void set_scale_1(float value)
	{
		___scale_1 = value;
	}

	inline static int32_t get_offset_of_centerX_2() { return static_cast<int32_t>(offsetof(EyewearCalibrationReading_t972160870, ___centerX_2)); }
	inline float get_centerX_2() const { return ___centerX_2; }
	inline float* get_address_of_centerX_2() { return &___centerX_2; }
	inline void set_centerX_2(float value)
	{
		___centerX_2 = value;
	}

	inline static int32_t get_offset_of_centerY_3() { return static_cast<int32_t>(offsetof(EyewearCalibrationReading_t972160870, ___centerY_3)); }
	inline float get_centerY_3() const { return ___centerY_3; }
	inline float* get_address_of_centerY_3() { return &___centerY_3; }
	inline void set_centerY_3(float value)
	{
		___centerY_3 = value;
	}

	inline static int32_t get_offset_of_unused_4() { return static_cast<int32_t>(offsetof(EyewearCalibrationReading_t972160870, ___unused_4)); }
	inline int32_t get_unused_4() const { return ___unused_4; }
	inline int32_t* get_address_of_unused_4() { return &___unused_4; }
	inline void set_unused_4(int32_t value)
	{
		___unused_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Vuforia.EyewearDevice/EyewearCalibrationReading
#pragma pack(push, tp, 1)
struct EyewearCalibrationReading_t972160870_marshaled_pinvoke
{
	float* ___pose_0;
	float ___scale_1;
	float ___centerX_2;
	float ___centerY_3;
	int32_t ___unused_4;
};
#pragma pack(pop, tp)
// Native definition for COM marshalling of Vuforia.EyewearDevice/EyewearCalibrationReading
#pragma pack(push, tp, 1)
struct EyewearCalibrationReading_t972160870_marshaled_com
{
	float* ___pose_0;
	float ___scale_1;
	float ___centerX_2;
	float ___centerY_3;
	int32_t ___unused_4;
};
#pragma pack(pop, tp)
#endif // EYEWEARCALIBRATIONREADING_T972160870_H
#ifndef DEVICETRACKER_T54803992_H
#define DEVICETRACKER_T54803992_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.DeviceTracker
struct  DeviceTracker_t54803992  : public Tracker_t1731515739
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEVICETRACKER_T54803992_H
#ifndef VIDEOMODEDATA_T2436408626_H
#define VIDEOMODEDATA_T2436408626_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.CameraDevice/VideoModeData
#pragma pack(push, tp, 1)
struct  VideoModeData_t2436408626 
{
public:
	// System.Int32 Vuforia.CameraDevice/VideoModeData::width
	int32_t ___width_0;
	// System.Int32 Vuforia.CameraDevice/VideoModeData::height
	int32_t ___height_1;
	// System.Single Vuforia.CameraDevice/VideoModeData::frameRate
	float ___frameRate_2;
	// System.Int32 Vuforia.CameraDevice/VideoModeData::unused
	int32_t ___unused_3;

public:
	inline static int32_t get_offset_of_width_0() { return static_cast<int32_t>(offsetof(VideoModeData_t2436408626, ___width_0)); }
	inline int32_t get_width_0() const { return ___width_0; }
	inline int32_t* get_address_of_width_0() { return &___width_0; }
	inline void set_width_0(int32_t value)
	{
		___width_0 = value;
	}

	inline static int32_t get_offset_of_height_1() { return static_cast<int32_t>(offsetof(VideoModeData_t2436408626, ___height_1)); }
	inline int32_t get_height_1() const { return ___height_1; }
	inline int32_t* get_address_of_height_1() { return &___height_1; }
	inline void set_height_1(int32_t value)
	{
		___height_1 = value;
	}

	inline static int32_t get_offset_of_frameRate_2() { return static_cast<int32_t>(offsetof(VideoModeData_t2436408626, ___frameRate_2)); }
	inline float get_frameRate_2() const { return ___frameRate_2; }
	inline float* get_address_of_frameRate_2() { return &___frameRate_2; }
	inline void set_frameRate_2(float value)
	{
		___frameRate_2 = value;
	}

	inline static int32_t get_offset_of_unused_3() { return static_cast<int32_t>(offsetof(VideoModeData_t2436408626, ___unused_3)); }
	inline int32_t get_unused_3() const { return ___unused_3; }
	inline int32_t* get_address_of_unused_3() { return &___unused_3; }
	inline void set_unused_3(int32_t value)
	{
		___unused_3 = value;
	}
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VIDEOMODEDATA_T2436408626_H
#ifndef INT64RANGE_T2805734248_H
#define INT64RANGE_T2805734248_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.CameraDevice/Int64Range
struct  Int64Range_t2805734248 
{
public:
	// System.Int64 Vuforia.CameraDevice/Int64Range::fromInt
	int64_t ___fromInt_0;
	// System.Int64 Vuforia.CameraDevice/Int64Range::toInt
	int64_t ___toInt_1;

public:
	inline static int32_t get_offset_of_fromInt_0() { return static_cast<int32_t>(offsetof(Int64Range_t2805734248, ___fromInt_0)); }
	inline int64_t get_fromInt_0() const { return ___fromInt_0; }
	inline int64_t* get_address_of_fromInt_0() { return &___fromInt_0; }
	inline void set_fromInt_0(int64_t value)
	{
		___fromInt_0 = value;
	}

	inline static int32_t get_offset_of_toInt_1() { return static_cast<int32_t>(offsetof(Int64Range_t2805734248, ___toInt_1)); }
	inline int64_t get_toInt_1() const { return ___toInt_1; }
	inline int64_t* get_address_of_toInt_1() { return &___toInt_1; }
	inline void set_toInt_1(int64_t value)
	{
		___toInt_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT64RANGE_T2805734248_H
#ifndef PLAYMODEEYEWEARCALIBRATIONPROFILEMANAGERIMPL_T1010441882_H
#define PLAYMODEEYEWEARCALIBRATIONPROFILEMANAGERIMPL_T1010441882_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.PlayModeEyewearCalibrationProfileManagerImpl
struct  PlayModeEyewearCalibrationProfileManagerImpl_t1010441882  : public EyewearCalibrationProfileManager_t1615252077
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYMODEEYEWEARCALIBRATIONPROFILEMANAGERIMPL_T1010441882_H
#ifndef EYEWEARUSERCALIBRATORIMPL_T3458973152_H
#define EYEWEARUSERCALIBRATORIMPL_T3458973152_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.EyewearUserCalibratorImpl
struct  EyewearUserCalibratorImpl_t3458973152  : public EyewearUserCalibrator_t173744680
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EYEWEARUSERCALIBRATORIMPL_T3458973152_H
#ifndef CAMERADIRECTION_T1337526284_H
#define CAMERADIRECTION_T1337526284_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.CameraDevice/CameraDirection
struct  CameraDirection_t1337526284 
{
public:
	// System.Int32 Vuforia.CameraDevice/CameraDirection::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(CameraDirection_t1337526284, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAMERADIRECTION_T1337526284_H
#ifndef FOCUSMODE_T4230446711_H
#define FOCUSMODE_T4230446711_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.CameraDevice/FocusMode
struct  FocusMode_t4230446711 
{
public:
	// System.Int32 Vuforia.CameraDevice/FocusMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(FocusMode_t4230446711, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FOCUSMODE_T4230446711_H
#ifndef CAMERADEVICEMODE_T1785007815_H
#define CAMERADEVICEMODE_T1785007815_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.CameraDevice/CameraDeviceMode
struct  CameraDeviceMode_t1785007815 
{
public:
	// System.Int32 Vuforia.CameraDevice/CameraDeviceMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(CameraDeviceMode_t1785007815, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAMERADEVICEMODE_T1785007815_H
#ifndef DATATYPE_T3838319351_H
#define DATATYPE_T3838319351_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.CameraDevice/CameraField/DataType
struct  DataType_t3838319351 
{
public:
	// System.Int32 Vuforia.CameraDevice/CameraField/DataType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(DataType_t3838319351, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATATYPE_T3838319351_H
#ifndef VERTEXHELPER_T419071860_H
#define VERTEXHELPER_T419071860_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.VertexHelper
struct  VertexHelper_t419071860  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<UnityEngine.Vector3> UnityEngine.UI.VertexHelper::m_Positions
	List_1_t1758679641 * ___m_Positions_0;
	// System.Collections.Generic.List`1<UnityEngine.Color32> UnityEngine.UI.VertexHelper::m_Colors
	List_1_t3435548858 * ___m_Colors_1;
	// System.Collections.Generic.List`1<UnityEngine.Vector2> UnityEngine.UI.VertexHelper::m_Uv0S
	List_1_t2432482469 * ___m_Uv0S_2;
	// System.Collections.Generic.List`1<UnityEngine.Vector2> UnityEngine.UI.VertexHelper::m_Uv1S
	List_1_t2432482469 * ___m_Uv1S_3;
	// System.Collections.Generic.List`1<UnityEngine.Vector2> UnityEngine.UI.VertexHelper::m_Uv2S
	List_1_t2432482469 * ___m_Uv2S_4;
	// System.Collections.Generic.List`1<UnityEngine.Vector2> UnityEngine.UI.VertexHelper::m_Uv3S
	List_1_t2432482469 * ___m_Uv3S_5;
	// System.Collections.Generic.List`1<UnityEngine.Vector3> UnityEngine.UI.VertexHelper::m_Normals
	List_1_t1758679641 * ___m_Normals_6;
	// System.Collections.Generic.List`1<UnityEngine.Vector4> UnityEngine.UI.VertexHelper::m_Tangents
	List_1_t1669821552 * ___m_Tangents_7;
	// System.Collections.Generic.List`1<System.Int32> UnityEngine.UI.VertexHelper::m_Indices
	List_1_t3093093796 * ___m_Indices_8;

public:
	inline static int32_t get_offset_of_m_Positions_0() { return static_cast<int32_t>(offsetof(VertexHelper_t419071860, ___m_Positions_0)); }
	inline List_1_t1758679641 * get_m_Positions_0() const { return ___m_Positions_0; }
	inline List_1_t1758679641 ** get_address_of_m_Positions_0() { return &___m_Positions_0; }
	inline void set_m_Positions_0(List_1_t1758679641 * value)
	{
		___m_Positions_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Positions_0), value);
	}

	inline static int32_t get_offset_of_m_Colors_1() { return static_cast<int32_t>(offsetof(VertexHelper_t419071860, ___m_Colors_1)); }
	inline List_1_t3435548858 * get_m_Colors_1() const { return ___m_Colors_1; }
	inline List_1_t3435548858 ** get_address_of_m_Colors_1() { return &___m_Colors_1; }
	inline void set_m_Colors_1(List_1_t3435548858 * value)
	{
		___m_Colors_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_Colors_1), value);
	}

	inline static int32_t get_offset_of_m_Uv0S_2() { return static_cast<int32_t>(offsetof(VertexHelper_t419071860, ___m_Uv0S_2)); }
	inline List_1_t2432482469 * get_m_Uv0S_2() const { return ___m_Uv0S_2; }
	inline List_1_t2432482469 ** get_address_of_m_Uv0S_2() { return &___m_Uv0S_2; }
	inline void set_m_Uv0S_2(List_1_t2432482469 * value)
	{
		___m_Uv0S_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_Uv0S_2), value);
	}

	inline static int32_t get_offset_of_m_Uv1S_3() { return static_cast<int32_t>(offsetof(VertexHelper_t419071860, ___m_Uv1S_3)); }
	inline List_1_t2432482469 * get_m_Uv1S_3() const { return ___m_Uv1S_3; }
	inline List_1_t2432482469 ** get_address_of_m_Uv1S_3() { return &___m_Uv1S_3; }
	inline void set_m_Uv1S_3(List_1_t2432482469 * value)
	{
		___m_Uv1S_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_Uv1S_3), value);
	}

	inline static int32_t get_offset_of_m_Uv2S_4() { return static_cast<int32_t>(offsetof(VertexHelper_t419071860, ___m_Uv2S_4)); }
	inline List_1_t2432482469 * get_m_Uv2S_4() const { return ___m_Uv2S_4; }
	inline List_1_t2432482469 ** get_address_of_m_Uv2S_4() { return &___m_Uv2S_4; }
	inline void set_m_Uv2S_4(List_1_t2432482469 * value)
	{
		___m_Uv2S_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_Uv2S_4), value);
	}

	inline static int32_t get_offset_of_m_Uv3S_5() { return static_cast<int32_t>(offsetof(VertexHelper_t419071860, ___m_Uv3S_5)); }
	inline List_1_t2432482469 * get_m_Uv3S_5() const { return ___m_Uv3S_5; }
	inline List_1_t2432482469 ** get_address_of_m_Uv3S_5() { return &___m_Uv3S_5; }
	inline void set_m_Uv3S_5(List_1_t2432482469 * value)
	{
		___m_Uv3S_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_Uv3S_5), value);
	}

	inline static int32_t get_offset_of_m_Normals_6() { return static_cast<int32_t>(offsetof(VertexHelper_t419071860, ___m_Normals_6)); }
	inline List_1_t1758679641 * get_m_Normals_6() const { return ___m_Normals_6; }
	inline List_1_t1758679641 ** get_address_of_m_Normals_6() { return &___m_Normals_6; }
	inline void set_m_Normals_6(List_1_t1758679641 * value)
	{
		___m_Normals_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_Normals_6), value);
	}

	inline static int32_t get_offset_of_m_Tangents_7() { return static_cast<int32_t>(offsetof(VertexHelper_t419071860, ___m_Tangents_7)); }
	inline List_1_t1669821552 * get_m_Tangents_7() const { return ___m_Tangents_7; }
	inline List_1_t1669821552 ** get_address_of_m_Tangents_7() { return &___m_Tangents_7; }
	inline void set_m_Tangents_7(List_1_t1669821552 * value)
	{
		___m_Tangents_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_Tangents_7), value);
	}

	inline static int32_t get_offset_of_m_Indices_8() { return static_cast<int32_t>(offsetof(VertexHelper_t419071860, ___m_Indices_8)); }
	inline List_1_t3093093796 * get_m_Indices_8() const { return ___m_Indices_8; }
	inline List_1_t3093093796 ** get_address_of_m_Indices_8() { return &___m_Indices_8; }
	inline void set_m_Indices_8(List_1_t3093093796 * value)
	{
		___m_Indices_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_Indices_8), value);
	}
};

struct VertexHelper_t419071860_StaticFields
{
public:
	// UnityEngine.Vector4 UnityEngine.UI.VertexHelper::s_DefaultTangent
	Vector4_t2814672345  ___s_DefaultTangent_9;
	// UnityEngine.Vector3 UnityEngine.UI.VertexHelper::s_DefaultNormal
	Vector3_t2903530434  ___s_DefaultNormal_10;

public:
	inline static int32_t get_offset_of_s_DefaultTangent_9() { return static_cast<int32_t>(offsetof(VertexHelper_t419071860_StaticFields, ___s_DefaultTangent_9)); }
	inline Vector4_t2814672345  get_s_DefaultTangent_9() const { return ___s_DefaultTangent_9; }
	inline Vector4_t2814672345 * get_address_of_s_DefaultTangent_9() { return &___s_DefaultTangent_9; }
	inline void set_s_DefaultTangent_9(Vector4_t2814672345  value)
	{
		___s_DefaultTangent_9 = value;
	}

	inline static int32_t get_offset_of_s_DefaultNormal_10() { return static_cast<int32_t>(offsetof(VertexHelper_t419071860_StaticFields, ___s_DefaultNormal_10)); }
	inline Vector3_t2903530434  get_s_DefaultNormal_10() const { return ___s_DefaultNormal_10; }
	inline Vector3_t2903530434 * get_address_of_s_DefaultNormal_10() { return &___s_DefaultNormal_10; }
	inline void set_s_DefaultNormal_10(Vector3_t2903530434  value)
	{
		___s_DefaultNormal_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VERTEXHELPER_T419071860_H
#ifndef CLIPPING_MODE_T306275072_H
#define CLIPPING_MODE_T306275072_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.HideExcessAreaAbstractBehaviour/CLIPPING_MODE
struct  CLIPPING_MODE_t306275072 
{
public:
	// System.Int32 Vuforia.HideExcessAreaAbstractBehaviour/CLIPPING_MODE::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(CLIPPING_MODE_t306275072, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CLIPPING_MODE_T306275072_H
#ifndef OBJECTTARGETIMPL_T1592445137_H
#define OBJECTTARGETIMPL_T1592445137_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.ObjectTargetImpl
struct  ObjectTargetImpl_t1592445137  : public TrackableImpl_t3450720819
{
public:
	// UnityEngine.Vector3 Vuforia.ObjectTargetImpl::mSize
	Vector3_t2903530434  ___mSize_2;
	// Vuforia.DataSetImpl Vuforia.ObjectTargetImpl::mDataSet
	DataSetImpl_t223764247 * ___mDataSet_3;

public:
	inline static int32_t get_offset_of_mSize_2() { return static_cast<int32_t>(offsetof(ObjectTargetImpl_t1592445137, ___mSize_2)); }
	inline Vector3_t2903530434  get_mSize_2() const { return ___mSize_2; }
	inline Vector3_t2903530434 * get_address_of_mSize_2() { return &___mSize_2; }
	inline void set_mSize_2(Vector3_t2903530434  value)
	{
		___mSize_2 = value;
	}

	inline static int32_t get_offset_of_mDataSet_3() { return static_cast<int32_t>(offsetof(ObjectTargetImpl_t1592445137, ___mDataSet_3)); }
	inline DataSetImpl_t223764247 * get_mDataSet_3() const { return ___mDataSet_3; }
	inline DataSetImpl_t223764247 ** get_address_of_mDataSet_3() { return &___mDataSet_3; }
	inline void set_mDataSet_3(DataSetImpl_t223764247 * value)
	{
		___mDataSet_3 = value;
		Il2CppCodeGenWriteBarrier((&___mDataSet_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OBJECTTARGETIMPL_T1592445137_H
#ifndef MODE_T1603927093_H
#define MODE_T1603927093_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.Device/Mode
struct  Mode_t1603927093 
{
public:
	// System.Int32 Vuforia.Device/Mode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Mode_t1603927093, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MODE_T1603927093_H
#ifndef VIEWERBUTTONTYPE_T783864389_H
#define VIEWERBUTTONTYPE_T783864389_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.ViewerButtonType
struct  ViewerButtonType_t783864389 
{
public:
	// System.Int32 Vuforia.ViewerButtonType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ViewerButtonType_t783864389, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VIEWERBUTTONTYPE_T783864389_H
#ifndef VIEWERTRAYALIGNMENT_T2758466794_H
#define VIEWERTRAYALIGNMENT_T2758466794_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.ViewerTrayAlignment
struct  ViewerTrayAlignment_t2758466794 
{
public:
	// System.Int32 Vuforia.ViewerTrayAlignment::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ViewerTrayAlignment_t2758466794, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VIEWERTRAYALIGNMENT_T2758466794_H
#ifndef VIDEOBACKGROUNDREFLECTION_T728651611_H
#define VIDEOBACKGROUNDREFLECTION_T728651611_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.VuforiaRenderer/VideoBackgroundReflection
struct  VideoBackgroundReflection_t728651611 
{
public:
	// System.Int32 Vuforia.VuforiaRenderer/VideoBackgroundReflection::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(VideoBackgroundReflection_t728651611, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VIDEOBACKGROUNDREFLECTION_T728651611_H
#ifndef SCREENORIENTATION_T2818551509_H
#define SCREENORIENTATION_T2818551509_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ScreenOrientation
struct  ScreenOrientation_t2818551509 
{
public:
	// System.Int32 UnityEngine.ScreenOrientation::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ScreenOrientation_t2818551509, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCREENORIENTATION_T2818551509_H
#ifndef INSTANCEIDTYPE_T3450543871_H
#define INSTANCEIDTYPE_T3450543871_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.InstanceIdType
struct  InstanceIdType_t3450543871 
{
public:
	// System.Int32 Vuforia.InstanceIdType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(InstanceIdType_t3450543871, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INSTANCEIDTYPE_T3450543871_H
#ifndef IMAGEHEADERDATA_T1829409304_H
#define IMAGEHEADERDATA_T1829409304_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.VuforiaManagerImpl/ImageHeaderData
#pragma pack(push, tp, 1)
struct  ImageHeaderData_t1829409304 
{
public:
	// System.IntPtr Vuforia.VuforiaManagerImpl/ImageHeaderData::data
	IntPtr_t ___data_0;
	// System.Int32 Vuforia.VuforiaManagerImpl/ImageHeaderData::width
	int32_t ___width_1;
	// System.Int32 Vuforia.VuforiaManagerImpl/ImageHeaderData::height
	int32_t ___height_2;
	// System.Int32 Vuforia.VuforiaManagerImpl/ImageHeaderData::stride
	int32_t ___stride_3;
	// System.Int32 Vuforia.VuforiaManagerImpl/ImageHeaderData::bufferWidth
	int32_t ___bufferWidth_4;
	// System.Int32 Vuforia.VuforiaManagerImpl/ImageHeaderData::bufferHeight
	int32_t ___bufferHeight_5;
	// System.Int32 Vuforia.VuforiaManagerImpl/ImageHeaderData::format
	int32_t ___format_6;
	// System.Int32 Vuforia.VuforiaManagerImpl/ImageHeaderData::reallocate
	int32_t ___reallocate_7;
	// System.Int32 Vuforia.VuforiaManagerImpl/ImageHeaderData::updated
	int32_t ___updated_8;

public:
	inline static int32_t get_offset_of_data_0() { return static_cast<int32_t>(offsetof(ImageHeaderData_t1829409304, ___data_0)); }
	inline IntPtr_t get_data_0() const { return ___data_0; }
	inline IntPtr_t* get_address_of_data_0() { return &___data_0; }
	inline void set_data_0(IntPtr_t value)
	{
		___data_0 = value;
	}

	inline static int32_t get_offset_of_width_1() { return static_cast<int32_t>(offsetof(ImageHeaderData_t1829409304, ___width_1)); }
	inline int32_t get_width_1() const { return ___width_1; }
	inline int32_t* get_address_of_width_1() { return &___width_1; }
	inline void set_width_1(int32_t value)
	{
		___width_1 = value;
	}

	inline static int32_t get_offset_of_height_2() { return static_cast<int32_t>(offsetof(ImageHeaderData_t1829409304, ___height_2)); }
	inline int32_t get_height_2() const { return ___height_2; }
	inline int32_t* get_address_of_height_2() { return &___height_2; }
	inline void set_height_2(int32_t value)
	{
		___height_2 = value;
	}

	inline static int32_t get_offset_of_stride_3() { return static_cast<int32_t>(offsetof(ImageHeaderData_t1829409304, ___stride_3)); }
	inline int32_t get_stride_3() const { return ___stride_3; }
	inline int32_t* get_address_of_stride_3() { return &___stride_3; }
	inline void set_stride_3(int32_t value)
	{
		___stride_3 = value;
	}

	inline static int32_t get_offset_of_bufferWidth_4() { return static_cast<int32_t>(offsetof(ImageHeaderData_t1829409304, ___bufferWidth_4)); }
	inline int32_t get_bufferWidth_4() const { return ___bufferWidth_4; }
	inline int32_t* get_address_of_bufferWidth_4() { return &___bufferWidth_4; }
	inline void set_bufferWidth_4(int32_t value)
	{
		___bufferWidth_4 = value;
	}

	inline static int32_t get_offset_of_bufferHeight_5() { return static_cast<int32_t>(offsetof(ImageHeaderData_t1829409304, ___bufferHeight_5)); }
	inline int32_t get_bufferHeight_5() const { return ___bufferHeight_5; }
	inline int32_t* get_address_of_bufferHeight_5() { return &___bufferHeight_5; }
	inline void set_bufferHeight_5(int32_t value)
	{
		___bufferHeight_5 = value;
	}

	inline static int32_t get_offset_of_format_6() { return static_cast<int32_t>(offsetof(ImageHeaderData_t1829409304, ___format_6)); }
	inline int32_t get_format_6() const { return ___format_6; }
	inline int32_t* get_address_of_format_6() { return &___format_6; }
	inline void set_format_6(int32_t value)
	{
		___format_6 = value;
	}

	inline static int32_t get_offset_of_reallocate_7() { return static_cast<int32_t>(offsetof(ImageHeaderData_t1829409304, ___reallocate_7)); }
	inline int32_t get_reallocate_7() const { return ___reallocate_7; }
	inline int32_t* get_address_of_reallocate_7() { return &___reallocate_7; }
	inline void set_reallocate_7(int32_t value)
	{
		___reallocate_7 = value;
	}

	inline static int32_t get_offset_of_updated_8() { return static_cast<int32_t>(offsetof(ImageHeaderData_t1829409304, ___updated_8)); }
	inline int32_t get_updated_8() const { return ___updated_8; }
	inline int32_t* get_address_of_updated_8() { return &___updated_8; }
	inline void set_updated_8(int32_t value)
	{
		___updated_8 = value;
	}
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IMAGEHEADERDATA_T1829409304_H
#ifndef VIDEOTEXTUREINFO_T773013061_H
#define VIDEOTEXTUREINFO_T773013061_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.VuforiaRenderer/VideoTextureInfo
#pragma pack(push, tp, 1)
struct  VideoTextureInfo_t773013061 
{
public:
	// Vuforia.VuforiaRenderer/Vec2I Vuforia.VuforiaRenderer/VideoTextureInfo::textureSize
	Vec2I_t895597728  ___textureSize_0;
	// Vuforia.VuforiaRenderer/Vec2I Vuforia.VuforiaRenderer/VideoTextureInfo::imageSize
	Vec2I_t895597728  ___imageSize_1;

public:
	inline static int32_t get_offset_of_textureSize_0() { return static_cast<int32_t>(offsetof(VideoTextureInfo_t773013061, ___textureSize_0)); }
	inline Vec2I_t895597728  get_textureSize_0() const { return ___textureSize_0; }
	inline Vec2I_t895597728 * get_address_of_textureSize_0() { return &___textureSize_0; }
	inline void set_textureSize_0(Vec2I_t895597728  value)
	{
		___textureSize_0 = value;
	}

	inline static int32_t get_offset_of_imageSize_1() { return static_cast<int32_t>(offsetof(VideoTextureInfo_t773013061, ___imageSize_1)); }
	inline Vec2I_t895597728  get_imageSize_1() const { return ___imageSize_1; }
	inline Vec2I_t895597728 * get_address_of_imageSize_1() { return &___imageSize_1; }
	inline void set_imageSize_1(Vec2I_t895597728  value)
	{
		___imageSize_1 = value;
	}
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VIDEOTEXTUREINFO_T773013061_H
#ifndef RECONSTRUCTIONIMPL_T1634174475_H
#define RECONSTRUCTIONIMPL_T1634174475_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.ReconstructionImpl
struct  ReconstructionImpl_t1634174475  : public RuntimeObject
{
public:
	// System.IntPtr Vuforia.ReconstructionImpl::mNativeReconstructionPtr
	IntPtr_t ___mNativeReconstructionPtr_0;
	// System.Boolean Vuforia.ReconstructionImpl::mMaximumAreaIsSet
	bool ___mMaximumAreaIsSet_1;
	// UnityEngine.Rect Vuforia.ReconstructionImpl::mMaximumArea
	Rect_t3436776195  ___mMaximumArea_2;
	// System.Single Vuforia.ReconstructionImpl::mNavMeshPadding
	float ___mNavMeshPadding_3;
	// System.Boolean Vuforia.ReconstructionImpl::mNavMeshUpdatesEnabled
	bool ___mNavMeshUpdatesEnabled_4;

public:
	inline static int32_t get_offset_of_mNativeReconstructionPtr_0() { return static_cast<int32_t>(offsetof(ReconstructionImpl_t1634174475, ___mNativeReconstructionPtr_0)); }
	inline IntPtr_t get_mNativeReconstructionPtr_0() const { return ___mNativeReconstructionPtr_0; }
	inline IntPtr_t* get_address_of_mNativeReconstructionPtr_0() { return &___mNativeReconstructionPtr_0; }
	inline void set_mNativeReconstructionPtr_0(IntPtr_t value)
	{
		___mNativeReconstructionPtr_0 = value;
	}

	inline static int32_t get_offset_of_mMaximumAreaIsSet_1() { return static_cast<int32_t>(offsetof(ReconstructionImpl_t1634174475, ___mMaximumAreaIsSet_1)); }
	inline bool get_mMaximumAreaIsSet_1() const { return ___mMaximumAreaIsSet_1; }
	inline bool* get_address_of_mMaximumAreaIsSet_1() { return &___mMaximumAreaIsSet_1; }
	inline void set_mMaximumAreaIsSet_1(bool value)
	{
		___mMaximumAreaIsSet_1 = value;
	}

	inline static int32_t get_offset_of_mMaximumArea_2() { return static_cast<int32_t>(offsetof(ReconstructionImpl_t1634174475, ___mMaximumArea_2)); }
	inline Rect_t3436776195  get_mMaximumArea_2() const { return ___mMaximumArea_2; }
	inline Rect_t3436776195 * get_address_of_mMaximumArea_2() { return &___mMaximumArea_2; }
	inline void set_mMaximumArea_2(Rect_t3436776195  value)
	{
		___mMaximumArea_2 = value;
	}

	inline static int32_t get_offset_of_mNavMeshPadding_3() { return static_cast<int32_t>(offsetof(ReconstructionImpl_t1634174475, ___mNavMeshPadding_3)); }
	inline float get_mNavMeshPadding_3() const { return ___mNavMeshPadding_3; }
	inline float* get_address_of_mNavMeshPadding_3() { return &___mNavMeshPadding_3; }
	inline void set_mNavMeshPadding_3(float value)
	{
		___mNavMeshPadding_3 = value;
	}

	inline static int32_t get_offset_of_mNavMeshUpdatesEnabled_4() { return static_cast<int32_t>(offsetof(ReconstructionImpl_t1634174475, ___mNavMeshUpdatesEnabled_4)); }
	inline bool get_mNavMeshUpdatesEnabled_4() const { return ___mNavMeshUpdatesEnabled_4; }
	inline bool* get_address_of_mNavMeshUpdatesEnabled_4() { return &___mNavMeshUpdatesEnabled_4; }
	inline void set_mNavMeshUpdatesEnabled_4(bool value)
	{
		___mNavMeshUpdatesEnabled_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RECONSTRUCTIONIMPL_T1634174475_H
#ifndef VIEWERPARAMETERSLIST_T2716421017_H
#define VIEWERPARAMETERSLIST_T2716421017_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.ViewerParametersList
struct  ViewerParametersList_t2716421017  : public RuntimeObject
{
public:
	// System.IntPtr Vuforia.ViewerParametersList::mNativeVPL
	IntPtr_t ___mNativeVPL_0;

public:
	inline static int32_t get_offset_of_mNativeVPL_0() { return static_cast<int32_t>(offsetof(ViewerParametersList_t2716421017, ___mNativeVPL_0)); }
	inline IntPtr_t get_mNativeVPL_0() const { return ___mNativeVPL_0; }
	inline IntPtr_t* get_address_of_mNativeVPL_0() { return &___mNativeVPL_0; }
	inline void set_mNativeVPL_0(IntPtr_t value)
	{
		___mNativeVPL_0 = value;
	}
};

struct ViewerParametersList_t2716421017_StaticFields
{
public:
	// Vuforia.ViewerParametersList Vuforia.ViewerParametersList::mListForAuthoringTools
	ViewerParametersList_t2716421017 * ___mListForAuthoringTools_1;

public:
	inline static int32_t get_offset_of_mListForAuthoringTools_1() { return static_cast<int32_t>(offsetof(ViewerParametersList_t2716421017_StaticFields, ___mListForAuthoringTools_1)); }
	inline ViewerParametersList_t2716421017 * get_mListForAuthoringTools_1() const { return ___mListForAuthoringTools_1; }
	inline ViewerParametersList_t2716421017 ** get_address_of_mListForAuthoringTools_1() { return &___mListForAuthoringTools_1; }
	inline void set_mListForAuthoringTools_1(ViewerParametersList_t2716421017 * value)
	{
		___mListForAuthoringTools_1 = value;
		Il2CppCodeGenWriteBarrier((&___mListForAuthoringTools_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VIEWERPARAMETERSLIST_T2716421017_H
#ifndef OBJECT_T1502412432_H
#define OBJECT_T1502412432_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t1502412432  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	IntPtr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t1502412432, ___m_CachedPtr_0)); }
	inline IntPtr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline IntPtr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(IntPtr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t1502412432_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t1502412432_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t1502412432_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t1502412432_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T1502412432_H
#ifndef VIEW_T3963731153_H
#define VIEW_T3963731153_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.View
struct  View_t3963731153 
{
public:
	// System.Int32 Vuforia.View::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(View_t3963731153, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VIEW_T3963731153_H
#ifndef EYEID_T1454423357_H
#define EYEID_T1454423357_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.EyewearDevice/EyeID
struct  EyeID_t1454423357 
{
public:
	// System.Int32 Vuforia.EyewearDevice/EyeID::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(EyeID_t1454423357, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EYEID_T1454423357_H
#ifndef PLAYMODEEYEWEARDEVICE_T1597218540_H
#define PLAYMODEEYEWEARDEVICE_T1597218540_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.PlayModeEyewearDevice
struct  PlayModeEyewearDevice_t1597218540  : public EyewearDevice_t3960213283
{
public:
	// Vuforia.EyewearCalibrationProfileManager Vuforia.PlayModeEyewearDevice::mProfileManager
	EyewearCalibrationProfileManager_t1615252077 * ___mProfileManager_1;
	// Vuforia.EyewearUserCalibrator Vuforia.PlayModeEyewearDevice::mCalibrator
	EyewearUserCalibrator_t173744680 * ___mCalibrator_2;
	// System.Boolean Vuforia.PlayModeEyewearDevice::mDummyPredictiveTracking
	bool ___mDummyPredictiveTracking_3;

public:
	inline static int32_t get_offset_of_mProfileManager_1() { return static_cast<int32_t>(offsetof(PlayModeEyewearDevice_t1597218540, ___mProfileManager_1)); }
	inline EyewearCalibrationProfileManager_t1615252077 * get_mProfileManager_1() const { return ___mProfileManager_1; }
	inline EyewearCalibrationProfileManager_t1615252077 ** get_address_of_mProfileManager_1() { return &___mProfileManager_1; }
	inline void set_mProfileManager_1(EyewearCalibrationProfileManager_t1615252077 * value)
	{
		___mProfileManager_1 = value;
		Il2CppCodeGenWriteBarrier((&___mProfileManager_1), value);
	}

	inline static int32_t get_offset_of_mCalibrator_2() { return static_cast<int32_t>(offsetof(PlayModeEyewearDevice_t1597218540, ___mCalibrator_2)); }
	inline EyewearUserCalibrator_t173744680 * get_mCalibrator_2() const { return ___mCalibrator_2; }
	inline EyewearUserCalibrator_t173744680 ** get_address_of_mCalibrator_2() { return &___mCalibrator_2; }
	inline void set_mCalibrator_2(EyewearUserCalibrator_t173744680 * value)
	{
		___mCalibrator_2 = value;
		Il2CppCodeGenWriteBarrier((&___mCalibrator_2), value);
	}

	inline static int32_t get_offset_of_mDummyPredictiveTracking_3() { return static_cast<int32_t>(offsetof(PlayModeEyewearDevice_t1597218540, ___mDummyPredictiveTracking_3)); }
	inline bool get_mDummyPredictiveTracking_3() const { return ___mDummyPredictiveTracking_3; }
	inline bool* get_address_of_mDummyPredictiveTracking_3() { return &___mDummyPredictiveTracking_3; }
	inline void set_mDummyPredictiveTracking_3(bool value)
	{
		___mDummyPredictiveTracking_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYMODEEYEWEARDEVICE_T1597218540_H
#ifndef DEDICATEDEYEWEARDEVICE_T4104793313_H
#define DEDICATEDEYEWEARDEVICE_T4104793313_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.DedicatedEyewearDevice
struct  DedicatedEyewearDevice_t4104793313  : public EyewearDevice_t3960213283
{
public:
	// Vuforia.EyewearCalibrationProfileManager Vuforia.DedicatedEyewearDevice::mProfileManager
	EyewearCalibrationProfileManager_t1615252077 * ___mProfileManager_1;
	// Vuforia.EyewearUserCalibrator Vuforia.DedicatedEyewearDevice::mCalibrator
	EyewearUserCalibrator_t173744680 * ___mCalibrator_2;

public:
	inline static int32_t get_offset_of_mProfileManager_1() { return static_cast<int32_t>(offsetof(DedicatedEyewearDevice_t4104793313, ___mProfileManager_1)); }
	inline EyewearCalibrationProfileManager_t1615252077 * get_mProfileManager_1() const { return ___mProfileManager_1; }
	inline EyewearCalibrationProfileManager_t1615252077 ** get_address_of_mProfileManager_1() { return &___mProfileManager_1; }
	inline void set_mProfileManager_1(EyewearCalibrationProfileManager_t1615252077 * value)
	{
		___mProfileManager_1 = value;
		Il2CppCodeGenWriteBarrier((&___mProfileManager_1), value);
	}

	inline static int32_t get_offset_of_mCalibrator_2() { return static_cast<int32_t>(offsetof(DedicatedEyewearDevice_t4104793313, ___mCalibrator_2)); }
	inline EyewearUserCalibrator_t173744680 * get_mCalibrator_2() const { return ___mCalibrator_2; }
	inline EyewearUserCalibrator_t173744680 ** get_address_of_mCalibrator_2() { return &___mCalibrator_2; }
	inline void set_mCalibrator_2(EyewearUserCalibrator_t173744680 * value)
	{
		___mCalibrator_2 = value;
		Il2CppCodeGenWriteBarrier((&___mCalibrator_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEDICATEDEYEWEARDEVICE_T4104793313_H
#ifndef CAMERACONFIGURATIONUTILITY_T995036719_H
#define CAMERACONFIGURATIONUTILITY_T995036719_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.CameraConfigurationUtility
struct  CameraConfigurationUtility_t995036719  : public RuntimeObject
{
public:

public:
};

struct CameraConfigurationUtility_t995036719_StaticFields
{
public:
	// UnityEngine.Vector4 Vuforia.CameraConfigurationUtility::MIN_CENTER
	Vector4_t2814672345  ___MIN_CENTER_0;
	// UnityEngine.Vector4 Vuforia.CameraConfigurationUtility::MAX_CENTER
	Vector4_t2814672345  ___MAX_CENTER_1;
	// UnityEngine.Vector4 Vuforia.CameraConfigurationUtility::MAX_BOTTOM
	Vector4_t2814672345  ___MAX_BOTTOM_2;
	// UnityEngine.Vector4 Vuforia.CameraConfigurationUtility::MAX_TOP
	Vector4_t2814672345  ___MAX_TOP_3;
	// UnityEngine.Vector4 Vuforia.CameraConfigurationUtility::MAX_LEFT
	Vector4_t2814672345  ___MAX_LEFT_4;
	// UnityEngine.Vector4 Vuforia.CameraConfigurationUtility::MAX_RIGHT
	Vector4_t2814672345  ___MAX_RIGHT_5;

public:
	inline static int32_t get_offset_of_MIN_CENTER_0() { return static_cast<int32_t>(offsetof(CameraConfigurationUtility_t995036719_StaticFields, ___MIN_CENTER_0)); }
	inline Vector4_t2814672345  get_MIN_CENTER_0() const { return ___MIN_CENTER_0; }
	inline Vector4_t2814672345 * get_address_of_MIN_CENTER_0() { return &___MIN_CENTER_0; }
	inline void set_MIN_CENTER_0(Vector4_t2814672345  value)
	{
		___MIN_CENTER_0 = value;
	}

	inline static int32_t get_offset_of_MAX_CENTER_1() { return static_cast<int32_t>(offsetof(CameraConfigurationUtility_t995036719_StaticFields, ___MAX_CENTER_1)); }
	inline Vector4_t2814672345  get_MAX_CENTER_1() const { return ___MAX_CENTER_1; }
	inline Vector4_t2814672345 * get_address_of_MAX_CENTER_1() { return &___MAX_CENTER_1; }
	inline void set_MAX_CENTER_1(Vector4_t2814672345  value)
	{
		___MAX_CENTER_1 = value;
	}

	inline static int32_t get_offset_of_MAX_BOTTOM_2() { return static_cast<int32_t>(offsetof(CameraConfigurationUtility_t995036719_StaticFields, ___MAX_BOTTOM_2)); }
	inline Vector4_t2814672345  get_MAX_BOTTOM_2() const { return ___MAX_BOTTOM_2; }
	inline Vector4_t2814672345 * get_address_of_MAX_BOTTOM_2() { return &___MAX_BOTTOM_2; }
	inline void set_MAX_BOTTOM_2(Vector4_t2814672345  value)
	{
		___MAX_BOTTOM_2 = value;
	}

	inline static int32_t get_offset_of_MAX_TOP_3() { return static_cast<int32_t>(offsetof(CameraConfigurationUtility_t995036719_StaticFields, ___MAX_TOP_3)); }
	inline Vector4_t2814672345  get_MAX_TOP_3() const { return ___MAX_TOP_3; }
	inline Vector4_t2814672345 * get_address_of_MAX_TOP_3() { return &___MAX_TOP_3; }
	inline void set_MAX_TOP_3(Vector4_t2814672345  value)
	{
		___MAX_TOP_3 = value;
	}

	inline static int32_t get_offset_of_MAX_LEFT_4() { return static_cast<int32_t>(offsetof(CameraConfigurationUtility_t995036719_StaticFields, ___MAX_LEFT_4)); }
	inline Vector4_t2814672345  get_MAX_LEFT_4() const { return ___MAX_LEFT_4; }
	inline Vector4_t2814672345 * get_address_of_MAX_LEFT_4() { return &___MAX_LEFT_4; }
	inline void set_MAX_LEFT_4(Vector4_t2814672345  value)
	{
		___MAX_LEFT_4 = value;
	}

	inline static int32_t get_offset_of_MAX_RIGHT_5() { return static_cast<int32_t>(offsetof(CameraConfigurationUtility_t995036719_StaticFields, ___MAX_RIGHT_5)); }
	inline Vector4_t2814672345  get_MAX_RIGHT_5() const { return ___MAX_RIGHT_5; }
	inline Vector4_t2814672345 * get_address_of_MAX_RIGHT_5() { return &___MAX_RIGHT_5; }
	inline void set_MAX_RIGHT_5(Vector4_t2814672345  value)
	{
		___MAX_RIGHT_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAMERACONFIGURATIONUTILITY_T995036719_H
#ifndef SEETHROUGHCONFIGURATION_T1856493377_H
#define SEETHROUGHCONFIGURATION_T1856493377_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.DigitalEyewearARController/SeeThroughConfiguration
struct  SeeThroughConfiguration_t1856493377 
{
public:
	// System.Int32 Vuforia.DigitalEyewearARController/SeeThroughConfiguration::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(SeeThroughConfiguration_t1856493377, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SEETHROUGHCONFIGURATION_T1856493377_H
#ifndef VIEWERPARAMETERS_T3954347578_H
#define VIEWERPARAMETERS_T3954347578_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.ViewerParameters
struct  ViewerParameters_t3954347578  : public RuntimeObject
{
public:
	// System.IntPtr Vuforia.ViewerParameters::mNativeVP
	IntPtr_t ___mNativeVP_0;

public:
	inline static int32_t get_offset_of_mNativeVP_0() { return static_cast<int32_t>(offsetof(ViewerParameters_t3954347578, ___mNativeVP_0)); }
	inline IntPtr_t get_mNativeVP_0() const { return ___mNativeVP_0; }
	inline IntPtr_t* get_address_of_mNativeVP_0() { return &___mNativeVP_0; }
	inline void set_mNativeVP_0(IntPtr_t value)
	{
		___mNativeVP_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VIEWERPARAMETERS_T3954347578_H
#ifndef POSEINFO_T2257311701_H
#define POSEINFO_T2257311701_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.HoloLensExtendedTrackingManager/PoseInfo
struct  PoseInfo_t2257311701 
{
public:
	// UnityEngine.Vector3 Vuforia.HoloLensExtendedTrackingManager/PoseInfo::Position
	Vector3_t2903530434  ___Position_0;
	// UnityEngine.Quaternion Vuforia.HoloLensExtendedTrackingManager/PoseInfo::Rotation
	Quaternion_t754065749  ___Rotation_1;
	// System.Int32 Vuforia.HoloLensExtendedTrackingManager/PoseInfo::NumFramesPoseWasOff
	int32_t ___NumFramesPoseWasOff_2;

public:
	inline static int32_t get_offset_of_Position_0() { return static_cast<int32_t>(offsetof(PoseInfo_t2257311701, ___Position_0)); }
	inline Vector3_t2903530434  get_Position_0() const { return ___Position_0; }
	inline Vector3_t2903530434 * get_address_of_Position_0() { return &___Position_0; }
	inline void set_Position_0(Vector3_t2903530434  value)
	{
		___Position_0 = value;
	}

	inline static int32_t get_offset_of_Rotation_1() { return static_cast<int32_t>(offsetof(PoseInfo_t2257311701, ___Rotation_1)); }
	inline Quaternion_t754065749  get_Rotation_1() const { return ___Rotation_1; }
	inline Quaternion_t754065749 * get_address_of_Rotation_1() { return &___Rotation_1; }
	inline void set_Rotation_1(Quaternion_t754065749  value)
	{
		___Rotation_1 = value;
	}

	inline static int32_t get_offset_of_NumFramesPoseWasOff_2() { return static_cast<int32_t>(offsetof(PoseInfo_t2257311701, ___NumFramesPoseWasOff_2)); }
	inline int32_t get_NumFramesPoseWasOff_2() const { return ___NumFramesPoseWasOff_2; }
	inline int32_t* get_address_of_NumFramesPoseWasOff_2() { return &___NumFramesPoseWasOff_2; }
	inline void set_NumFramesPoseWasOff_2(int32_t value)
	{
		___NumFramesPoseWasOff_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSEINFO_T2257311701_H
#ifndef MODE_T1800297771_H
#define MODE_T1800297771_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.MixedRealityController/Mode
struct  Mode_t1800297771 
{
public:
	// System.Int32 Vuforia.MixedRealityController/Mode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Mode_t1800297771, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MODE_T1800297771_H
#ifndef MODEL_CORRECTION_MODE_T3621708838_H
#define MODEL_CORRECTION_MODE_T3621708838_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.RotationalDeviceTracker/MODEL_CORRECTION_MODE
struct  MODEL_CORRECTION_MODE_t3621708838 
{
public:
	// System.Int32 Vuforia.RotationalDeviceTracker/MODEL_CORRECTION_MODE::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(MODEL_CORRECTION_MODE_t3621708838, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MODEL_CORRECTION_MODE_T3621708838_H
#ifndef DEVICETRACKINGMANAGER_T2027984062_H
#define DEVICETRACKINGMANAGER_T2027984062_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.DeviceTrackingManager
struct  DeviceTrackingManager_t2027984062  : public RuntimeObject
{
public:
	// UnityEngine.Vector3 Vuforia.DeviceTrackingManager::mDeviceTrackerPositonOffset
	Vector3_t2903530434  ___mDeviceTrackerPositonOffset_0;
	// UnityEngine.Quaternion Vuforia.DeviceTrackingManager::mDeviceTrackerRotationOffset
	Quaternion_t754065749  ___mDeviceTrackerRotationOffset_1;
	// System.Action Vuforia.DeviceTrackingManager::mBeforeDevicePoseUpdated
	Action_t3619184611 * ___mBeforeDevicePoseUpdated_2;
	// System.Action Vuforia.DeviceTrackingManager::mAfterDevicePoseUpdated
	Action_t3619184611 * ___mAfterDevicePoseUpdated_3;

public:
	inline static int32_t get_offset_of_mDeviceTrackerPositonOffset_0() { return static_cast<int32_t>(offsetof(DeviceTrackingManager_t2027984062, ___mDeviceTrackerPositonOffset_0)); }
	inline Vector3_t2903530434  get_mDeviceTrackerPositonOffset_0() const { return ___mDeviceTrackerPositonOffset_0; }
	inline Vector3_t2903530434 * get_address_of_mDeviceTrackerPositonOffset_0() { return &___mDeviceTrackerPositonOffset_0; }
	inline void set_mDeviceTrackerPositonOffset_0(Vector3_t2903530434  value)
	{
		___mDeviceTrackerPositonOffset_0 = value;
	}

	inline static int32_t get_offset_of_mDeviceTrackerRotationOffset_1() { return static_cast<int32_t>(offsetof(DeviceTrackingManager_t2027984062, ___mDeviceTrackerRotationOffset_1)); }
	inline Quaternion_t754065749  get_mDeviceTrackerRotationOffset_1() const { return ___mDeviceTrackerRotationOffset_1; }
	inline Quaternion_t754065749 * get_address_of_mDeviceTrackerRotationOffset_1() { return &___mDeviceTrackerRotationOffset_1; }
	inline void set_mDeviceTrackerRotationOffset_1(Quaternion_t754065749  value)
	{
		___mDeviceTrackerRotationOffset_1 = value;
	}

	inline static int32_t get_offset_of_mBeforeDevicePoseUpdated_2() { return static_cast<int32_t>(offsetof(DeviceTrackingManager_t2027984062, ___mBeforeDevicePoseUpdated_2)); }
	inline Action_t3619184611 * get_mBeforeDevicePoseUpdated_2() const { return ___mBeforeDevicePoseUpdated_2; }
	inline Action_t3619184611 ** get_address_of_mBeforeDevicePoseUpdated_2() { return &___mBeforeDevicePoseUpdated_2; }
	inline void set_mBeforeDevicePoseUpdated_2(Action_t3619184611 * value)
	{
		___mBeforeDevicePoseUpdated_2 = value;
		Il2CppCodeGenWriteBarrier((&___mBeforeDevicePoseUpdated_2), value);
	}

	inline static int32_t get_offset_of_mAfterDevicePoseUpdated_3() { return static_cast<int32_t>(offsetof(DeviceTrackingManager_t2027984062, ___mAfterDevicePoseUpdated_3)); }
	inline Action_t3619184611 * get_mAfterDevicePoseUpdated_3() const { return ___mAfterDevicePoseUpdated_3; }
	inline Action_t3619184611 ** get_address_of_mAfterDevicePoseUpdated_3() { return &___mAfterDevicePoseUpdated_3; }
	inline void set_mAfterDevicePoseUpdated_3(Action_t3619184611 * value)
	{
		___mAfterDevicePoseUpdated_3 = value;
		Il2CppCodeGenWriteBarrier((&___mAfterDevicePoseUpdated_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEVICETRACKINGMANAGER_T2027984062_H
#ifndef STEREOFRAMEWORK_T3060055999_H
#define STEREOFRAMEWORK_T3060055999_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.DigitalEyewearARController/StereoFramework
struct  StereoFramework_t3060055999 
{
public:
	// System.Int32 Vuforia.DigitalEyewearARController/StereoFramework::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(StereoFramework_t3060055999, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STEREOFRAMEWORK_T3060055999_H
#ifndef ROTATIONALDEVICETRACKER_T2131173692_H
#define ROTATIONALDEVICETRACKER_T2131173692_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.RotationalDeviceTracker
struct  RotationalDeviceTracker_t2131173692  : public DeviceTracker_t54803992
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ROTATIONALDEVICETRACKER_T2131173692_H
#ifndef DISTORTIONRENDERINGMODE_T4279873297_H
#define DISTORTIONRENDERINGMODE_T4279873297_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.DistortionRenderingMode
struct  DistortionRenderingMode_t4279873297 
{
public:
	// System.Int32 Vuforia.DistortionRenderingMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(DistortionRenderingMode_t4279873297, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DISTORTIONRENDERINGMODE_T4279873297_H
#ifndef STATUS_T1358118869_H
#define STATUS_T1358118869_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.TrackableBehaviour/Status
struct  Status_t1358118869 
{
public:
	// System.Int32 Vuforia.TrackableBehaviour/Status::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Status_t1358118869, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STATUS_T1358118869_H
#ifndef U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T4060893174_H
#define U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T4060893174_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>
struct  U3CPrivateImplementationDetailsU3E_t4060893174  : public RuntimeObject
{
public:

public:
};

struct U3CPrivateImplementationDetailsU3E_t4060893174_StaticFields
{
public:
	// <PrivateImplementationDetails>/$ArrayType=12 <PrivateImplementationDetails>::$field-7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46
	U24ArrayTypeU3D12_t1462559582  ___U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0;

public:
	inline static int32_t get_offset_of_U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t4060893174_StaticFields, ___U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0)); }
	inline U24ArrayTypeU3D12_t1462559582  get_U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0() const { return ___U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0; }
	inline U24ArrayTypeU3D12_t1462559582 * get_address_of_U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0() { return &___U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0; }
	inline void set_U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0(U24ArrayTypeU3D12_t1462559582  value)
	{
		___U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T4060893174_H
#ifndef LEGACYHIDEEXCESSAREACLIPPING_T2114833877_H
#define LEGACYHIDEEXCESSAREACLIPPING_T2114833877_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.LegacyHideExcessAreaClipping
struct  LegacyHideExcessAreaClipping_t2114833877  : public RuntimeObject
{
public:
	// UnityEngine.GameObject Vuforia.LegacyHideExcessAreaClipping::mGameObject
	GameObject_t1811656094 * ___mGameObject_0;
	// UnityEngine.Shader Vuforia.LegacyHideExcessAreaClipping::mMatteShader
	Shader_t3778723916 * ___mMatteShader_1;
	// UnityEngine.GameObject Vuforia.LegacyHideExcessAreaClipping::mBgPlane
	GameObject_t1811656094 * ___mBgPlane_2;
	// UnityEngine.GameObject Vuforia.LegacyHideExcessAreaClipping::mLeftPlane
	GameObject_t1811656094 * ___mLeftPlane_3;
	// UnityEngine.GameObject Vuforia.LegacyHideExcessAreaClipping::mRightPlane
	GameObject_t1811656094 * ___mRightPlane_4;
	// UnityEngine.GameObject Vuforia.LegacyHideExcessAreaClipping::mTopPlane
	GameObject_t1811656094 * ___mTopPlane_5;
	// UnityEngine.GameObject Vuforia.LegacyHideExcessAreaClipping::mBottomPlane
	GameObject_t1811656094 * ___mBottomPlane_6;
	// UnityEngine.Camera Vuforia.LegacyHideExcessAreaClipping::mCamera
	Camera_t3175186167 * ___mCamera_7;
	// UnityEngine.Vector3 Vuforia.LegacyHideExcessAreaClipping::mBgPlaneLocalPos
	Vector3_t2903530434  ___mBgPlaneLocalPos_8;
	// UnityEngine.Vector3 Vuforia.LegacyHideExcessAreaClipping::mBgPlaneLocalScale
	Vector3_t2903530434  ___mBgPlaneLocalScale_9;
	// System.Single Vuforia.LegacyHideExcessAreaClipping::mCameraNearPlane
	float ___mCameraNearPlane_10;
	// UnityEngine.Rect Vuforia.LegacyHideExcessAreaClipping::mCameraPixelRect
	Rect_t3436776195  ___mCameraPixelRect_11;
	// System.Single Vuforia.LegacyHideExcessAreaClipping::mCameraFieldOFView
	float ___mCameraFieldOFView_12;
	// Vuforia.VuforiaARController Vuforia.LegacyHideExcessAreaClipping::mVuforiaBehaviour
	VuforiaARController_t1328503142 * ___mVuforiaBehaviour_13;
	// Vuforia.HideExcessAreaAbstractBehaviour[] Vuforia.LegacyHideExcessAreaClipping::mHideBehaviours
	HideExcessAreaAbstractBehaviourU5BU5D_t1918497157* ___mHideBehaviours_14;
	// System.Collections.Generic.List`1<Vuforia.HideExcessAreaAbstractBehaviour> Vuforia.LegacyHideExcessAreaClipping::mDeactivatedHideBehaviours
	List_1_t179562595 * ___mDeactivatedHideBehaviours_15;
	// System.Boolean Vuforia.LegacyHideExcessAreaClipping::mPlanesActivated
	bool ___mPlanesActivated_16;
	// UnityEngine.Vector3 Vuforia.LegacyHideExcessAreaClipping::mLeftPlaneCachedScale
	Vector3_t2903530434  ___mLeftPlaneCachedScale_17;
	// UnityEngine.Vector3 Vuforia.LegacyHideExcessAreaClipping::mRightPlaneCachedScale
	Vector3_t2903530434  ___mRightPlaneCachedScale_18;
	// UnityEngine.Vector3 Vuforia.LegacyHideExcessAreaClipping::mBottomPlaneCachedScale
	Vector3_t2903530434  ___mBottomPlaneCachedScale_19;
	// UnityEngine.Vector3 Vuforia.LegacyHideExcessAreaClipping::mTopPlaneCachedScale
	Vector3_t2903530434  ___mTopPlaneCachedScale_20;

public:
	inline static int32_t get_offset_of_mGameObject_0() { return static_cast<int32_t>(offsetof(LegacyHideExcessAreaClipping_t2114833877, ___mGameObject_0)); }
	inline GameObject_t1811656094 * get_mGameObject_0() const { return ___mGameObject_0; }
	inline GameObject_t1811656094 ** get_address_of_mGameObject_0() { return &___mGameObject_0; }
	inline void set_mGameObject_0(GameObject_t1811656094 * value)
	{
		___mGameObject_0 = value;
		Il2CppCodeGenWriteBarrier((&___mGameObject_0), value);
	}

	inline static int32_t get_offset_of_mMatteShader_1() { return static_cast<int32_t>(offsetof(LegacyHideExcessAreaClipping_t2114833877, ___mMatteShader_1)); }
	inline Shader_t3778723916 * get_mMatteShader_1() const { return ___mMatteShader_1; }
	inline Shader_t3778723916 ** get_address_of_mMatteShader_1() { return &___mMatteShader_1; }
	inline void set_mMatteShader_1(Shader_t3778723916 * value)
	{
		___mMatteShader_1 = value;
		Il2CppCodeGenWriteBarrier((&___mMatteShader_1), value);
	}

	inline static int32_t get_offset_of_mBgPlane_2() { return static_cast<int32_t>(offsetof(LegacyHideExcessAreaClipping_t2114833877, ___mBgPlane_2)); }
	inline GameObject_t1811656094 * get_mBgPlane_2() const { return ___mBgPlane_2; }
	inline GameObject_t1811656094 ** get_address_of_mBgPlane_2() { return &___mBgPlane_2; }
	inline void set_mBgPlane_2(GameObject_t1811656094 * value)
	{
		___mBgPlane_2 = value;
		Il2CppCodeGenWriteBarrier((&___mBgPlane_2), value);
	}

	inline static int32_t get_offset_of_mLeftPlane_3() { return static_cast<int32_t>(offsetof(LegacyHideExcessAreaClipping_t2114833877, ___mLeftPlane_3)); }
	inline GameObject_t1811656094 * get_mLeftPlane_3() const { return ___mLeftPlane_3; }
	inline GameObject_t1811656094 ** get_address_of_mLeftPlane_3() { return &___mLeftPlane_3; }
	inline void set_mLeftPlane_3(GameObject_t1811656094 * value)
	{
		___mLeftPlane_3 = value;
		Il2CppCodeGenWriteBarrier((&___mLeftPlane_3), value);
	}

	inline static int32_t get_offset_of_mRightPlane_4() { return static_cast<int32_t>(offsetof(LegacyHideExcessAreaClipping_t2114833877, ___mRightPlane_4)); }
	inline GameObject_t1811656094 * get_mRightPlane_4() const { return ___mRightPlane_4; }
	inline GameObject_t1811656094 ** get_address_of_mRightPlane_4() { return &___mRightPlane_4; }
	inline void set_mRightPlane_4(GameObject_t1811656094 * value)
	{
		___mRightPlane_4 = value;
		Il2CppCodeGenWriteBarrier((&___mRightPlane_4), value);
	}

	inline static int32_t get_offset_of_mTopPlane_5() { return static_cast<int32_t>(offsetof(LegacyHideExcessAreaClipping_t2114833877, ___mTopPlane_5)); }
	inline GameObject_t1811656094 * get_mTopPlane_5() const { return ___mTopPlane_5; }
	inline GameObject_t1811656094 ** get_address_of_mTopPlane_5() { return &___mTopPlane_5; }
	inline void set_mTopPlane_5(GameObject_t1811656094 * value)
	{
		___mTopPlane_5 = value;
		Il2CppCodeGenWriteBarrier((&___mTopPlane_5), value);
	}

	inline static int32_t get_offset_of_mBottomPlane_6() { return static_cast<int32_t>(offsetof(LegacyHideExcessAreaClipping_t2114833877, ___mBottomPlane_6)); }
	inline GameObject_t1811656094 * get_mBottomPlane_6() const { return ___mBottomPlane_6; }
	inline GameObject_t1811656094 ** get_address_of_mBottomPlane_6() { return &___mBottomPlane_6; }
	inline void set_mBottomPlane_6(GameObject_t1811656094 * value)
	{
		___mBottomPlane_6 = value;
		Il2CppCodeGenWriteBarrier((&___mBottomPlane_6), value);
	}

	inline static int32_t get_offset_of_mCamera_7() { return static_cast<int32_t>(offsetof(LegacyHideExcessAreaClipping_t2114833877, ___mCamera_7)); }
	inline Camera_t3175186167 * get_mCamera_7() const { return ___mCamera_7; }
	inline Camera_t3175186167 ** get_address_of_mCamera_7() { return &___mCamera_7; }
	inline void set_mCamera_7(Camera_t3175186167 * value)
	{
		___mCamera_7 = value;
		Il2CppCodeGenWriteBarrier((&___mCamera_7), value);
	}

	inline static int32_t get_offset_of_mBgPlaneLocalPos_8() { return static_cast<int32_t>(offsetof(LegacyHideExcessAreaClipping_t2114833877, ___mBgPlaneLocalPos_8)); }
	inline Vector3_t2903530434  get_mBgPlaneLocalPos_8() const { return ___mBgPlaneLocalPos_8; }
	inline Vector3_t2903530434 * get_address_of_mBgPlaneLocalPos_8() { return &___mBgPlaneLocalPos_8; }
	inline void set_mBgPlaneLocalPos_8(Vector3_t2903530434  value)
	{
		___mBgPlaneLocalPos_8 = value;
	}

	inline static int32_t get_offset_of_mBgPlaneLocalScale_9() { return static_cast<int32_t>(offsetof(LegacyHideExcessAreaClipping_t2114833877, ___mBgPlaneLocalScale_9)); }
	inline Vector3_t2903530434  get_mBgPlaneLocalScale_9() const { return ___mBgPlaneLocalScale_9; }
	inline Vector3_t2903530434 * get_address_of_mBgPlaneLocalScale_9() { return &___mBgPlaneLocalScale_9; }
	inline void set_mBgPlaneLocalScale_9(Vector3_t2903530434  value)
	{
		___mBgPlaneLocalScale_9 = value;
	}

	inline static int32_t get_offset_of_mCameraNearPlane_10() { return static_cast<int32_t>(offsetof(LegacyHideExcessAreaClipping_t2114833877, ___mCameraNearPlane_10)); }
	inline float get_mCameraNearPlane_10() const { return ___mCameraNearPlane_10; }
	inline float* get_address_of_mCameraNearPlane_10() { return &___mCameraNearPlane_10; }
	inline void set_mCameraNearPlane_10(float value)
	{
		___mCameraNearPlane_10 = value;
	}

	inline static int32_t get_offset_of_mCameraPixelRect_11() { return static_cast<int32_t>(offsetof(LegacyHideExcessAreaClipping_t2114833877, ___mCameraPixelRect_11)); }
	inline Rect_t3436776195  get_mCameraPixelRect_11() const { return ___mCameraPixelRect_11; }
	inline Rect_t3436776195 * get_address_of_mCameraPixelRect_11() { return &___mCameraPixelRect_11; }
	inline void set_mCameraPixelRect_11(Rect_t3436776195  value)
	{
		___mCameraPixelRect_11 = value;
	}

	inline static int32_t get_offset_of_mCameraFieldOFView_12() { return static_cast<int32_t>(offsetof(LegacyHideExcessAreaClipping_t2114833877, ___mCameraFieldOFView_12)); }
	inline float get_mCameraFieldOFView_12() const { return ___mCameraFieldOFView_12; }
	inline float* get_address_of_mCameraFieldOFView_12() { return &___mCameraFieldOFView_12; }
	inline void set_mCameraFieldOFView_12(float value)
	{
		___mCameraFieldOFView_12 = value;
	}

	inline static int32_t get_offset_of_mVuforiaBehaviour_13() { return static_cast<int32_t>(offsetof(LegacyHideExcessAreaClipping_t2114833877, ___mVuforiaBehaviour_13)); }
	inline VuforiaARController_t1328503142 * get_mVuforiaBehaviour_13() const { return ___mVuforiaBehaviour_13; }
	inline VuforiaARController_t1328503142 ** get_address_of_mVuforiaBehaviour_13() { return &___mVuforiaBehaviour_13; }
	inline void set_mVuforiaBehaviour_13(VuforiaARController_t1328503142 * value)
	{
		___mVuforiaBehaviour_13 = value;
		Il2CppCodeGenWriteBarrier((&___mVuforiaBehaviour_13), value);
	}

	inline static int32_t get_offset_of_mHideBehaviours_14() { return static_cast<int32_t>(offsetof(LegacyHideExcessAreaClipping_t2114833877, ___mHideBehaviours_14)); }
	inline HideExcessAreaAbstractBehaviourU5BU5D_t1918497157* get_mHideBehaviours_14() const { return ___mHideBehaviours_14; }
	inline HideExcessAreaAbstractBehaviourU5BU5D_t1918497157** get_address_of_mHideBehaviours_14() { return &___mHideBehaviours_14; }
	inline void set_mHideBehaviours_14(HideExcessAreaAbstractBehaviourU5BU5D_t1918497157* value)
	{
		___mHideBehaviours_14 = value;
		Il2CppCodeGenWriteBarrier((&___mHideBehaviours_14), value);
	}

	inline static int32_t get_offset_of_mDeactivatedHideBehaviours_15() { return static_cast<int32_t>(offsetof(LegacyHideExcessAreaClipping_t2114833877, ___mDeactivatedHideBehaviours_15)); }
	inline List_1_t179562595 * get_mDeactivatedHideBehaviours_15() const { return ___mDeactivatedHideBehaviours_15; }
	inline List_1_t179562595 ** get_address_of_mDeactivatedHideBehaviours_15() { return &___mDeactivatedHideBehaviours_15; }
	inline void set_mDeactivatedHideBehaviours_15(List_1_t179562595 * value)
	{
		___mDeactivatedHideBehaviours_15 = value;
		Il2CppCodeGenWriteBarrier((&___mDeactivatedHideBehaviours_15), value);
	}

	inline static int32_t get_offset_of_mPlanesActivated_16() { return static_cast<int32_t>(offsetof(LegacyHideExcessAreaClipping_t2114833877, ___mPlanesActivated_16)); }
	inline bool get_mPlanesActivated_16() const { return ___mPlanesActivated_16; }
	inline bool* get_address_of_mPlanesActivated_16() { return &___mPlanesActivated_16; }
	inline void set_mPlanesActivated_16(bool value)
	{
		___mPlanesActivated_16 = value;
	}

	inline static int32_t get_offset_of_mLeftPlaneCachedScale_17() { return static_cast<int32_t>(offsetof(LegacyHideExcessAreaClipping_t2114833877, ___mLeftPlaneCachedScale_17)); }
	inline Vector3_t2903530434  get_mLeftPlaneCachedScale_17() const { return ___mLeftPlaneCachedScale_17; }
	inline Vector3_t2903530434 * get_address_of_mLeftPlaneCachedScale_17() { return &___mLeftPlaneCachedScale_17; }
	inline void set_mLeftPlaneCachedScale_17(Vector3_t2903530434  value)
	{
		___mLeftPlaneCachedScale_17 = value;
	}

	inline static int32_t get_offset_of_mRightPlaneCachedScale_18() { return static_cast<int32_t>(offsetof(LegacyHideExcessAreaClipping_t2114833877, ___mRightPlaneCachedScale_18)); }
	inline Vector3_t2903530434  get_mRightPlaneCachedScale_18() const { return ___mRightPlaneCachedScale_18; }
	inline Vector3_t2903530434 * get_address_of_mRightPlaneCachedScale_18() { return &___mRightPlaneCachedScale_18; }
	inline void set_mRightPlaneCachedScale_18(Vector3_t2903530434  value)
	{
		___mRightPlaneCachedScale_18 = value;
	}

	inline static int32_t get_offset_of_mBottomPlaneCachedScale_19() { return static_cast<int32_t>(offsetof(LegacyHideExcessAreaClipping_t2114833877, ___mBottomPlaneCachedScale_19)); }
	inline Vector3_t2903530434  get_mBottomPlaneCachedScale_19() const { return ___mBottomPlaneCachedScale_19; }
	inline Vector3_t2903530434 * get_address_of_mBottomPlaneCachedScale_19() { return &___mBottomPlaneCachedScale_19; }
	inline void set_mBottomPlaneCachedScale_19(Vector3_t2903530434  value)
	{
		___mBottomPlaneCachedScale_19 = value;
	}

	inline static int32_t get_offset_of_mTopPlaneCachedScale_20() { return static_cast<int32_t>(offsetof(LegacyHideExcessAreaClipping_t2114833877, ___mTopPlaneCachedScale_20)); }
	inline Vector3_t2903530434  get_mTopPlaneCachedScale_20() const { return ___mTopPlaneCachedScale_20; }
	inline Vector3_t2903530434 * get_address_of_mTopPlaneCachedScale_20() { return &___mTopPlaneCachedScale_20; }
	inline void set_mTopPlaneCachedScale_20(Vector3_t2903530434  value)
	{
		___mTopPlaneCachedScale_20 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LEGACYHIDEEXCESSAREACLIPPING_T2114833877_H
#ifndef STENCILHIDEEXCESSAREACLIPPING_T2110303595_H
#define STENCILHIDEEXCESSAREACLIPPING_T2110303595_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.StencilHideExcessAreaClipping
struct  StencilHideExcessAreaClipping_t2110303595  : public RuntimeObject
{
public:
	// UnityEngine.GameObject Vuforia.StencilHideExcessAreaClipping::mGameObject
	GameObject_t1811656094 * ___mGameObject_0;
	// UnityEngine.Shader Vuforia.StencilHideExcessAreaClipping::mMatteShader
	Shader_t3778723916 * ___mMatteShader_1;
	// UnityEngine.GameObject Vuforia.StencilHideExcessAreaClipping::mClippingPlane
	GameObject_t1811656094 * ___mClippingPlane_2;
	// UnityEngine.Camera Vuforia.StencilHideExcessAreaClipping::mCamera
	Camera_t3175186167 * ___mCamera_3;
	// System.Single Vuforia.StencilHideExcessAreaClipping::mCameraNearPlane
	float ___mCameraNearPlane_4;
	// System.Single Vuforia.StencilHideExcessAreaClipping::mCameraFarPlane
	float ___mCameraFarPlane_5;
	// UnityEngine.Rect Vuforia.StencilHideExcessAreaClipping::mCameraPixelRect
	Rect_t3436776195  ___mCameraPixelRect_6;
	// System.Single Vuforia.StencilHideExcessAreaClipping::mCameraFieldOfView
	float ___mCameraFieldOfView_7;
	// Vuforia.VuforiaARController Vuforia.StencilHideExcessAreaClipping::mVuforiaBehaviour
	VuforiaARController_t1328503142 * ___mVuforiaBehaviour_8;
	// System.Boolean Vuforia.StencilHideExcessAreaClipping::mPlanesActivated
	bool ___mPlanesActivated_9;
	// UnityEngine.GameObject Vuforia.StencilHideExcessAreaClipping::mBgPlane
	GameObject_t1811656094 * ___mBgPlane_10;
	// UnityEngine.Vector3 Vuforia.StencilHideExcessAreaClipping::mBgPlaneLocalPos
	Vector3_t2903530434  ___mBgPlaneLocalPos_11;
	// UnityEngine.Vector3 Vuforia.StencilHideExcessAreaClipping::mBgPlaneLocalScale
	Vector3_t2903530434  ___mBgPlaneLocalScale_12;

public:
	inline static int32_t get_offset_of_mGameObject_0() { return static_cast<int32_t>(offsetof(StencilHideExcessAreaClipping_t2110303595, ___mGameObject_0)); }
	inline GameObject_t1811656094 * get_mGameObject_0() const { return ___mGameObject_0; }
	inline GameObject_t1811656094 ** get_address_of_mGameObject_0() { return &___mGameObject_0; }
	inline void set_mGameObject_0(GameObject_t1811656094 * value)
	{
		___mGameObject_0 = value;
		Il2CppCodeGenWriteBarrier((&___mGameObject_0), value);
	}

	inline static int32_t get_offset_of_mMatteShader_1() { return static_cast<int32_t>(offsetof(StencilHideExcessAreaClipping_t2110303595, ___mMatteShader_1)); }
	inline Shader_t3778723916 * get_mMatteShader_1() const { return ___mMatteShader_1; }
	inline Shader_t3778723916 ** get_address_of_mMatteShader_1() { return &___mMatteShader_1; }
	inline void set_mMatteShader_1(Shader_t3778723916 * value)
	{
		___mMatteShader_1 = value;
		Il2CppCodeGenWriteBarrier((&___mMatteShader_1), value);
	}

	inline static int32_t get_offset_of_mClippingPlane_2() { return static_cast<int32_t>(offsetof(StencilHideExcessAreaClipping_t2110303595, ___mClippingPlane_2)); }
	inline GameObject_t1811656094 * get_mClippingPlane_2() const { return ___mClippingPlane_2; }
	inline GameObject_t1811656094 ** get_address_of_mClippingPlane_2() { return &___mClippingPlane_2; }
	inline void set_mClippingPlane_2(GameObject_t1811656094 * value)
	{
		___mClippingPlane_2 = value;
		Il2CppCodeGenWriteBarrier((&___mClippingPlane_2), value);
	}

	inline static int32_t get_offset_of_mCamera_3() { return static_cast<int32_t>(offsetof(StencilHideExcessAreaClipping_t2110303595, ___mCamera_3)); }
	inline Camera_t3175186167 * get_mCamera_3() const { return ___mCamera_3; }
	inline Camera_t3175186167 ** get_address_of_mCamera_3() { return &___mCamera_3; }
	inline void set_mCamera_3(Camera_t3175186167 * value)
	{
		___mCamera_3 = value;
		Il2CppCodeGenWriteBarrier((&___mCamera_3), value);
	}

	inline static int32_t get_offset_of_mCameraNearPlane_4() { return static_cast<int32_t>(offsetof(StencilHideExcessAreaClipping_t2110303595, ___mCameraNearPlane_4)); }
	inline float get_mCameraNearPlane_4() const { return ___mCameraNearPlane_4; }
	inline float* get_address_of_mCameraNearPlane_4() { return &___mCameraNearPlane_4; }
	inline void set_mCameraNearPlane_4(float value)
	{
		___mCameraNearPlane_4 = value;
	}

	inline static int32_t get_offset_of_mCameraFarPlane_5() { return static_cast<int32_t>(offsetof(StencilHideExcessAreaClipping_t2110303595, ___mCameraFarPlane_5)); }
	inline float get_mCameraFarPlane_5() const { return ___mCameraFarPlane_5; }
	inline float* get_address_of_mCameraFarPlane_5() { return &___mCameraFarPlane_5; }
	inline void set_mCameraFarPlane_5(float value)
	{
		___mCameraFarPlane_5 = value;
	}

	inline static int32_t get_offset_of_mCameraPixelRect_6() { return static_cast<int32_t>(offsetof(StencilHideExcessAreaClipping_t2110303595, ___mCameraPixelRect_6)); }
	inline Rect_t3436776195  get_mCameraPixelRect_6() const { return ___mCameraPixelRect_6; }
	inline Rect_t3436776195 * get_address_of_mCameraPixelRect_6() { return &___mCameraPixelRect_6; }
	inline void set_mCameraPixelRect_6(Rect_t3436776195  value)
	{
		___mCameraPixelRect_6 = value;
	}

	inline static int32_t get_offset_of_mCameraFieldOfView_7() { return static_cast<int32_t>(offsetof(StencilHideExcessAreaClipping_t2110303595, ___mCameraFieldOfView_7)); }
	inline float get_mCameraFieldOfView_7() const { return ___mCameraFieldOfView_7; }
	inline float* get_address_of_mCameraFieldOfView_7() { return &___mCameraFieldOfView_7; }
	inline void set_mCameraFieldOfView_7(float value)
	{
		___mCameraFieldOfView_7 = value;
	}

	inline static int32_t get_offset_of_mVuforiaBehaviour_8() { return static_cast<int32_t>(offsetof(StencilHideExcessAreaClipping_t2110303595, ___mVuforiaBehaviour_8)); }
	inline VuforiaARController_t1328503142 * get_mVuforiaBehaviour_8() const { return ___mVuforiaBehaviour_8; }
	inline VuforiaARController_t1328503142 ** get_address_of_mVuforiaBehaviour_8() { return &___mVuforiaBehaviour_8; }
	inline void set_mVuforiaBehaviour_8(VuforiaARController_t1328503142 * value)
	{
		___mVuforiaBehaviour_8 = value;
		Il2CppCodeGenWriteBarrier((&___mVuforiaBehaviour_8), value);
	}

	inline static int32_t get_offset_of_mPlanesActivated_9() { return static_cast<int32_t>(offsetof(StencilHideExcessAreaClipping_t2110303595, ___mPlanesActivated_9)); }
	inline bool get_mPlanesActivated_9() const { return ___mPlanesActivated_9; }
	inline bool* get_address_of_mPlanesActivated_9() { return &___mPlanesActivated_9; }
	inline void set_mPlanesActivated_9(bool value)
	{
		___mPlanesActivated_9 = value;
	}

	inline static int32_t get_offset_of_mBgPlane_10() { return static_cast<int32_t>(offsetof(StencilHideExcessAreaClipping_t2110303595, ___mBgPlane_10)); }
	inline GameObject_t1811656094 * get_mBgPlane_10() const { return ___mBgPlane_10; }
	inline GameObject_t1811656094 ** get_address_of_mBgPlane_10() { return &___mBgPlane_10; }
	inline void set_mBgPlane_10(GameObject_t1811656094 * value)
	{
		___mBgPlane_10 = value;
		Il2CppCodeGenWriteBarrier((&___mBgPlane_10), value);
	}

	inline static int32_t get_offset_of_mBgPlaneLocalPos_11() { return static_cast<int32_t>(offsetof(StencilHideExcessAreaClipping_t2110303595, ___mBgPlaneLocalPos_11)); }
	inline Vector3_t2903530434  get_mBgPlaneLocalPos_11() const { return ___mBgPlaneLocalPos_11; }
	inline Vector3_t2903530434 * get_address_of_mBgPlaneLocalPos_11() { return &___mBgPlaneLocalPos_11; }
	inline void set_mBgPlaneLocalPos_11(Vector3_t2903530434  value)
	{
		___mBgPlaneLocalPos_11 = value;
	}

	inline static int32_t get_offset_of_mBgPlaneLocalScale_12() { return static_cast<int32_t>(offsetof(StencilHideExcessAreaClipping_t2110303595, ___mBgPlaneLocalScale_12)); }
	inline Vector3_t2903530434  get_mBgPlaneLocalScale_12() const { return ___mBgPlaneLocalScale_12; }
	inline Vector3_t2903530434 * get_address_of_mBgPlaneLocalScale_12() { return &___mBgPlaneLocalScale_12; }
	inline void set_mBgPlaneLocalScale_12(Vector3_t2903530434  value)
	{
		___mBgPlaneLocalScale_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STENCILHIDEEXCESSAREACLIPPING_T2110303595_H
#ifndef EYEWEARTYPE_T4127891436_H
#define EYEWEARTYPE_T4127891436_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.DigitalEyewearARController/EyewearType
struct  EyewearType_t4127891436 
{
public:
	// System.Int32 Vuforia.DigitalEyewearARController/EyewearType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(EyewearType_t4127891436, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EYEWEARTYPE_T4127891436_H
#ifndef COMPONENT_T4087199522_H
#define COMPONENT_T4087199522_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t4087199522  : public Object_t1502412432
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T4087199522_H
#ifndef NULLCAMERACONFIGURATION_T4274136510_H
#define NULLCAMERACONFIGURATION_T4274136510_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.NullCameraConfiguration
struct  NullCameraConfiguration_t4274136510  : public RuntimeObject
{
public:
	// UnityEngine.ScreenOrientation Vuforia.NullCameraConfiguration::mProjectionOrientation
	int32_t ___mProjectionOrientation_0;

public:
	inline static int32_t get_offset_of_mProjectionOrientation_0() { return static_cast<int32_t>(offsetof(NullCameraConfiguration_t4274136510, ___mProjectionOrientation_0)); }
	inline int32_t get_mProjectionOrientation_0() const { return ___mProjectionOrientation_0; }
	inline int32_t* get_address_of_mProjectionOrientation_0() { return &___mProjectionOrientation_0; }
	inline void set_mProjectionOrientation_0(int32_t value)
	{
		___mProjectionOrientation_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLCAMERACONFIGURATION_T4274136510_H
#ifndef BASECAMERACONFIGURATION_T196193726_H
#define BASECAMERACONFIGURATION_T196193726_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.BaseCameraConfiguration
struct  BaseCameraConfiguration_t196193726  : public RuntimeObject
{
public:
	// Vuforia.CameraDevice/CameraDeviceMode Vuforia.BaseCameraConfiguration::mCameraDeviceMode
	int32_t ___mCameraDeviceMode_0;
	// Vuforia.VuforiaRenderer/VideoBackgroundReflection Vuforia.BaseCameraConfiguration::mLastVideoBackGroundMirroredFromSDK
	int32_t ___mLastVideoBackGroundMirroredFromSDK_1;
	// System.Action Vuforia.BaseCameraConfiguration::mOnVideoBackgroundConfigChanged
	Action_t3619184611 * ___mOnVideoBackgroundConfigChanged_2;
	// System.Collections.Generic.Dictionary`2<UnityEngine.Camera,Vuforia.VideoBackgroundAbstractBehaviour> Vuforia.BaseCameraConfiguration::mVideoBackgroundBehaviours
	Dictionary_2_t4258919043 * ___mVideoBackgroundBehaviours_3;
	// UnityEngine.Rect Vuforia.BaseCameraConfiguration::mVideoBackgroundViewportRect
	Rect_t3436776195  ___mVideoBackgroundViewportRect_4;
	// System.Boolean Vuforia.BaseCameraConfiguration::mRenderVideoBackground
	bool ___mRenderVideoBackground_5;
	// UnityEngine.ScreenOrientation Vuforia.BaseCameraConfiguration::mProjectionOrientation
	int32_t ___mProjectionOrientation_6;
	// Vuforia.VuforiaRenderer/VideoBackgroundReflection Vuforia.BaseCameraConfiguration::mInitialReflection
	int32_t ___mInitialReflection_7;
	// Vuforia.BackgroundPlaneAbstractBehaviour Vuforia.BaseCameraConfiguration::mBackgroundPlaneBehaviour
	BackgroundPlaneAbstractBehaviour_t4259835756 * ___mBackgroundPlaneBehaviour_8;
	// System.Boolean Vuforia.BaseCameraConfiguration::mCameraParameterChanged
	bool ___mCameraParameterChanged_9;

public:
	inline static int32_t get_offset_of_mCameraDeviceMode_0() { return static_cast<int32_t>(offsetof(BaseCameraConfiguration_t196193726, ___mCameraDeviceMode_0)); }
	inline int32_t get_mCameraDeviceMode_0() const { return ___mCameraDeviceMode_0; }
	inline int32_t* get_address_of_mCameraDeviceMode_0() { return &___mCameraDeviceMode_0; }
	inline void set_mCameraDeviceMode_0(int32_t value)
	{
		___mCameraDeviceMode_0 = value;
	}

	inline static int32_t get_offset_of_mLastVideoBackGroundMirroredFromSDK_1() { return static_cast<int32_t>(offsetof(BaseCameraConfiguration_t196193726, ___mLastVideoBackGroundMirroredFromSDK_1)); }
	inline int32_t get_mLastVideoBackGroundMirroredFromSDK_1() const { return ___mLastVideoBackGroundMirroredFromSDK_1; }
	inline int32_t* get_address_of_mLastVideoBackGroundMirroredFromSDK_1() { return &___mLastVideoBackGroundMirroredFromSDK_1; }
	inline void set_mLastVideoBackGroundMirroredFromSDK_1(int32_t value)
	{
		___mLastVideoBackGroundMirroredFromSDK_1 = value;
	}

	inline static int32_t get_offset_of_mOnVideoBackgroundConfigChanged_2() { return static_cast<int32_t>(offsetof(BaseCameraConfiguration_t196193726, ___mOnVideoBackgroundConfigChanged_2)); }
	inline Action_t3619184611 * get_mOnVideoBackgroundConfigChanged_2() const { return ___mOnVideoBackgroundConfigChanged_2; }
	inline Action_t3619184611 ** get_address_of_mOnVideoBackgroundConfigChanged_2() { return &___mOnVideoBackgroundConfigChanged_2; }
	inline void set_mOnVideoBackgroundConfigChanged_2(Action_t3619184611 * value)
	{
		___mOnVideoBackgroundConfigChanged_2 = value;
		Il2CppCodeGenWriteBarrier((&___mOnVideoBackgroundConfigChanged_2), value);
	}

	inline static int32_t get_offset_of_mVideoBackgroundBehaviours_3() { return static_cast<int32_t>(offsetof(BaseCameraConfiguration_t196193726, ___mVideoBackgroundBehaviours_3)); }
	inline Dictionary_2_t4258919043 * get_mVideoBackgroundBehaviours_3() const { return ___mVideoBackgroundBehaviours_3; }
	inline Dictionary_2_t4258919043 ** get_address_of_mVideoBackgroundBehaviours_3() { return &___mVideoBackgroundBehaviours_3; }
	inline void set_mVideoBackgroundBehaviours_3(Dictionary_2_t4258919043 * value)
	{
		___mVideoBackgroundBehaviours_3 = value;
		Il2CppCodeGenWriteBarrier((&___mVideoBackgroundBehaviours_3), value);
	}

	inline static int32_t get_offset_of_mVideoBackgroundViewportRect_4() { return static_cast<int32_t>(offsetof(BaseCameraConfiguration_t196193726, ___mVideoBackgroundViewportRect_4)); }
	inline Rect_t3436776195  get_mVideoBackgroundViewportRect_4() const { return ___mVideoBackgroundViewportRect_4; }
	inline Rect_t3436776195 * get_address_of_mVideoBackgroundViewportRect_4() { return &___mVideoBackgroundViewportRect_4; }
	inline void set_mVideoBackgroundViewportRect_4(Rect_t3436776195  value)
	{
		___mVideoBackgroundViewportRect_4 = value;
	}

	inline static int32_t get_offset_of_mRenderVideoBackground_5() { return static_cast<int32_t>(offsetof(BaseCameraConfiguration_t196193726, ___mRenderVideoBackground_5)); }
	inline bool get_mRenderVideoBackground_5() const { return ___mRenderVideoBackground_5; }
	inline bool* get_address_of_mRenderVideoBackground_5() { return &___mRenderVideoBackground_5; }
	inline void set_mRenderVideoBackground_5(bool value)
	{
		___mRenderVideoBackground_5 = value;
	}

	inline static int32_t get_offset_of_mProjectionOrientation_6() { return static_cast<int32_t>(offsetof(BaseCameraConfiguration_t196193726, ___mProjectionOrientation_6)); }
	inline int32_t get_mProjectionOrientation_6() const { return ___mProjectionOrientation_6; }
	inline int32_t* get_address_of_mProjectionOrientation_6() { return &___mProjectionOrientation_6; }
	inline void set_mProjectionOrientation_6(int32_t value)
	{
		___mProjectionOrientation_6 = value;
	}

	inline static int32_t get_offset_of_mInitialReflection_7() { return static_cast<int32_t>(offsetof(BaseCameraConfiguration_t196193726, ___mInitialReflection_7)); }
	inline int32_t get_mInitialReflection_7() const { return ___mInitialReflection_7; }
	inline int32_t* get_address_of_mInitialReflection_7() { return &___mInitialReflection_7; }
	inline void set_mInitialReflection_7(int32_t value)
	{
		___mInitialReflection_7 = value;
	}

	inline static int32_t get_offset_of_mBackgroundPlaneBehaviour_8() { return static_cast<int32_t>(offsetof(BaseCameraConfiguration_t196193726, ___mBackgroundPlaneBehaviour_8)); }
	inline BackgroundPlaneAbstractBehaviour_t4259835756 * get_mBackgroundPlaneBehaviour_8() const { return ___mBackgroundPlaneBehaviour_8; }
	inline BackgroundPlaneAbstractBehaviour_t4259835756 ** get_address_of_mBackgroundPlaneBehaviour_8() { return &___mBackgroundPlaneBehaviour_8; }
	inline void set_mBackgroundPlaneBehaviour_8(BackgroundPlaneAbstractBehaviour_t4259835756 * value)
	{
		___mBackgroundPlaneBehaviour_8 = value;
		Il2CppCodeGenWriteBarrier((&___mBackgroundPlaneBehaviour_8), value);
	}

	inline static int32_t get_offset_of_mCameraParameterChanged_9() { return static_cast<int32_t>(offsetof(BaseCameraConfiguration_t196193726, ___mCameraParameterChanged_9)); }
	inline bool get_mCameraParameterChanged_9() const { return ___mCameraParameterChanged_9; }
	inline bool* get_address_of_mCameraParameterChanged_9() { return &___mCameraParameterChanged_9; }
	inline void set_mCameraParameterChanged_9(bool value)
	{
		___mCameraParameterChanged_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASECAMERACONFIGURATION_T196193726_H
#ifndef SERIALIZABLEVIEWERPARAMETERS_T484734121_H
#define SERIALIZABLEVIEWERPARAMETERS_T484734121_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.DigitalEyewearARController/SerializableViewerParameters
struct  SerializableViewerParameters_t484734121  : public RuntimeObject
{
public:
	// System.Single Vuforia.DigitalEyewearARController/SerializableViewerParameters::Version
	float ___Version_0;
	// System.String Vuforia.DigitalEyewearARController/SerializableViewerParameters::Name
	String_t* ___Name_1;
	// System.String Vuforia.DigitalEyewearARController/SerializableViewerParameters::Manufacturer
	String_t* ___Manufacturer_2;
	// Vuforia.ViewerButtonType Vuforia.DigitalEyewearARController/SerializableViewerParameters::ButtonType
	int32_t ___ButtonType_3;
	// System.Single Vuforia.DigitalEyewearARController/SerializableViewerParameters::ScreenToLensDistance
	float ___ScreenToLensDistance_4;
	// System.Single Vuforia.DigitalEyewearARController/SerializableViewerParameters::InterLensDistance
	float ___InterLensDistance_5;
	// Vuforia.ViewerTrayAlignment Vuforia.DigitalEyewearARController/SerializableViewerParameters::TrayAlignment
	int32_t ___TrayAlignment_6;
	// System.Single Vuforia.DigitalEyewearARController/SerializableViewerParameters::LensCenterToTrayDistance
	float ___LensCenterToTrayDistance_7;
	// UnityEngine.Vector2 Vuforia.DigitalEyewearARController/SerializableViewerParameters::DistortionCoefficients
	Vector2_t3577333262  ___DistortionCoefficients_8;
	// UnityEngine.Vector4 Vuforia.DigitalEyewearARController/SerializableViewerParameters::FieldOfView
	Vector4_t2814672345  ___FieldOfView_9;
	// System.Boolean Vuforia.DigitalEyewearARController/SerializableViewerParameters::ContainsMagnet
	bool ___ContainsMagnet_10;

public:
	inline static int32_t get_offset_of_Version_0() { return static_cast<int32_t>(offsetof(SerializableViewerParameters_t484734121, ___Version_0)); }
	inline float get_Version_0() const { return ___Version_0; }
	inline float* get_address_of_Version_0() { return &___Version_0; }
	inline void set_Version_0(float value)
	{
		___Version_0 = value;
	}

	inline static int32_t get_offset_of_Name_1() { return static_cast<int32_t>(offsetof(SerializableViewerParameters_t484734121, ___Name_1)); }
	inline String_t* get_Name_1() const { return ___Name_1; }
	inline String_t** get_address_of_Name_1() { return &___Name_1; }
	inline void set_Name_1(String_t* value)
	{
		___Name_1 = value;
		Il2CppCodeGenWriteBarrier((&___Name_1), value);
	}

	inline static int32_t get_offset_of_Manufacturer_2() { return static_cast<int32_t>(offsetof(SerializableViewerParameters_t484734121, ___Manufacturer_2)); }
	inline String_t* get_Manufacturer_2() const { return ___Manufacturer_2; }
	inline String_t** get_address_of_Manufacturer_2() { return &___Manufacturer_2; }
	inline void set_Manufacturer_2(String_t* value)
	{
		___Manufacturer_2 = value;
		Il2CppCodeGenWriteBarrier((&___Manufacturer_2), value);
	}

	inline static int32_t get_offset_of_ButtonType_3() { return static_cast<int32_t>(offsetof(SerializableViewerParameters_t484734121, ___ButtonType_3)); }
	inline int32_t get_ButtonType_3() const { return ___ButtonType_3; }
	inline int32_t* get_address_of_ButtonType_3() { return &___ButtonType_3; }
	inline void set_ButtonType_3(int32_t value)
	{
		___ButtonType_3 = value;
	}

	inline static int32_t get_offset_of_ScreenToLensDistance_4() { return static_cast<int32_t>(offsetof(SerializableViewerParameters_t484734121, ___ScreenToLensDistance_4)); }
	inline float get_ScreenToLensDistance_4() const { return ___ScreenToLensDistance_4; }
	inline float* get_address_of_ScreenToLensDistance_4() { return &___ScreenToLensDistance_4; }
	inline void set_ScreenToLensDistance_4(float value)
	{
		___ScreenToLensDistance_4 = value;
	}

	inline static int32_t get_offset_of_InterLensDistance_5() { return static_cast<int32_t>(offsetof(SerializableViewerParameters_t484734121, ___InterLensDistance_5)); }
	inline float get_InterLensDistance_5() const { return ___InterLensDistance_5; }
	inline float* get_address_of_InterLensDistance_5() { return &___InterLensDistance_5; }
	inline void set_InterLensDistance_5(float value)
	{
		___InterLensDistance_5 = value;
	}

	inline static int32_t get_offset_of_TrayAlignment_6() { return static_cast<int32_t>(offsetof(SerializableViewerParameters_t484734121, ___TrayAlignment_6)); }
	inline int32_t get_TrayAlignment_6() const { return ___TrayAlignment_6; }
	inline int32_t* get_address_of_TrayAlignment_6() { return &___TrayAlignment_6; }
	inline void set_TrayAlignment_6(int32_t value)
	{
		___TrayAlignment_6 = value;
	}

	inline static int32_t get_offset_of_LensCenterToTrayDistance_7() { return static_cast<int32_t>(offsetof(SerializableViewerParameters_t484734121, ___LensCenterToTrayDistance_7)); }
	inline float get_LensCenterToTrayDistance_7() const { return ___LensCenterToTrayDistance_7; }
	inline float* get_address_of_LensCenterToTrayDistance_7() { return &___LensCenterToTrayDistance_7; }
	inline void set_LensCenterToTrayDistance_7(float value)
	{
		___LensCenterToTrayDistance_7 = value;
	}

	inline static int32_t get_offset_of_DistortionCoefficients_8() { return static_cast<int32_t>(offsetof(SerializableViewerParameters_t484734121, ___DistortionCoefficients_8)); }
	inline Vector2_t3577333262  get_DistortionCoefficients_8() const { return ___DistortionCoefficients_8; }
	inline Vector2_t3577333262 * get_address_of_DistortionCoefficients_8() { return &___DistortionCoefficients_8; }
	inline void set_DistortionCoefficients_8(Vector2_t3577333262  value)
	{
		___DistortionCoefficients_8 = value;
	}

	inline static int32_t get_offset_of_FieldOfView_9() { return static_cast<int32_t>(offsetof(SerializableViewerParameters_t484734121, ___FieldOfView_9)); }
	inline Vector4_t2814672345  get_FieldOfView_9() const { return ___FieldOfView_9; }
	inline Vector4_t2814672345 * get_address_of_FieldOfView_9() { return &___FieldOfView_9; }
	inline void set_FieldOfView_9(Vector4_t2814672345  value)
	{
		___FieldOfView_9 = value;
	}

	inline static int32_t get_offset_of_ContainsMagnet_10() { return static_cast<int32_t>(offsetof(SerializableViewerParameters_t484734121, ___ContainsMagnet_10)); }
	inline bool get_ContainsMagnet_10() const { return ___ContainsMagnet_10; }
	inline bool* get_address_of_ContainsMagnet_10() { return &___ContainsMagnet_10; }
	inline void set_ContainsMagnet_10(bool value)
	{
		___ContainsMagnet_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERIALIZABLEVIEWERPARAMETERS_T484734121_H
#ifndef CAMERAFIELD_T382214397_H
#define CAMERAFIELD_T382214397_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.CameraDevice/CameraField
struct  CameraField_t382214397 
{
public:
	// Vuforia.CameraDevice/CameraField/DataType Vuforia.CameraDevice/CameraField::Type
	int32_t ___Type_0;
	// System.String Vuforia.CameraDevice/CameraField::Key
	String_t* ___Key_1;

public:
	inline static int32_t get_offset_of_Type_0() { return static_cast<int32_t>(offsetof(CameraField_t382214397, ___Type_0)); }
	inline int32_t get_Type_0() const { return ___Type_0; }
	inline int32_t* get_address_of_Type_0() { return &___Type_0; }
	inline void set_Type_0(int32_t value)
	{
		___Type_0 = value;
	}

	inline static int32_t get_offset_of_Key_1() { return static_cast<int32_t>(offsetof(CameraField_t382214397, ___Key_1)); }
	inline String_t* get_Key_1() const { return ___Key_1; }
	inline String_t** get_address_of_Key_1() { return &___Key_1; }
	inline void set_Key_1(String_t* value)
	{
		___Key_1 = value;
		Il2CppCodeGenWriteBarrier((&___Key_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Vuforia.CameraDevice/CameraField
struct CameraField_t382214397_marshaled_pinvoke
{
	int32_t ___Type_0;
	char* ___Key_1;
};
// Native definition for COM marshalling of Vuforia.CameraDevice/CameraField
struct CameraField_t382214397_marshaled_com
{
	int32_t ___Type_0;
	Il2CppChar* ___Key_1;
};
#endif // CAMERAFIELD_T382214397_H
#ifndef POSEAGEENTRY_T806437902_H
#define POSEAGEENTRY_T806437902_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.HoloLensExtendedTrackingManager/PoseAgeEntry
struct  PoseAgeEntry_t806437902 
{
public:
	// Vuforia.HoloLensExtendedTrackingManager/PoseInfo Vuforia.HoloLensExtendedTrackingManager/PoseAgeEntry::Pose
	PoseInfo_t2257311701  ___Pose_0;
	// Vuforia.HoloLensExtendedTrackingManager/PoseInfo Vuforia.HoloLensExtendedTrackingManager/PoseAgeEntry::CameraPose
	PoseInfo_t2257311701  ___CameraPose_1;
	// System.Int32 Vuforia.HoloLensExtendedTrackingManager/PoseAgeEntry::Age
	int32_t ___Age_2;

public:
	inline static int32_t get_offset_of_Pose_0() { return static_cast<int32_t>(offsetof(PoseAgeEntry_t806437902, ___Pose_0)); }
	inline PoseInfo_t2257311701  get_Pose_0() const { return ___Pose_0; }
	inline PoseInfo_t2257311701 * get_address_of_Pose_0() { return &___Pose_0; }
	inline void set_Pose_0(PoseInfo_t2257311701  value)
	{
		___Pose_0 = value;
	}

	inline static int32_t get_offset_of_CameraPose_1() { return static_cast<int32_t>(offsetof(PoseAgeEntry_t806437902, ___CameraPose_1)); }
	inline PoseInfo_t2257311701  get_CameraPose_1() const { return ___CameraPose_1; }
	inline PoseInfo_t2257311701 * get_address_of_CameraPose_1() { return &___CameraPose_1; }
	inline void set_CameraPose_1(PoseInfo_t2257311701  value)
	{
		___CameraPose_1 = value;
	}

	inline static int32_t get_offset_of_Age_2() { return static_cast<int32_t>(offsetof(PoseAgeEntry_t806437902, ___Age_2)); }
	inline int32_t get_Age_2() const { return ___Age_2; }
	inline int32_t* get_address_of_Age_2() { return &___Age_2; }
	inline void set_Age_2(int32_t value)
	{
		___Age_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSEAGEENTRY_T806437902_H
#ifndef VUMARKTARGETIMPL_T2724072581_H
#define VUMARKTARGETIMPL_T2724072581_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.VuMarkTargetImpl
struct  VuMarkTargetImpl_t2724072581  : public RuntimeObject
{
public:
	// Vuforia.VuMarkTemplateImpl Vuforia.VuMarkTargetImpl::mVuMarkTemplate
	VuMarkTemplateImpl_t1477556351 * ___mVuMarkTemplate_0;
	// Vuforia.InstanceIdImpl Vuforia.VuMarkTargetImpl::mInstanceId
	InstanceIdImpl_t2419732903 * ___mInstanceId_1;
	// System.Int32 Vuforia.VuMarkTargetImpl::mTargetId
	int32_t ___mTargetId_2;
	// Vuforia.Image Vuforia.VuMarkTargetImpl::mInstanceImage
	Image_t1445313791 * ___mInstanceImage_3;
	// Vuforia.VuforiaManagerImpl/ImageHeaderData Vuforia.VuMarkTargetImpl::mInstanceImageHeader
	ImageHeaderData_t1829409304  ___mInstanceImageHeader_4;

public:
	inline static int32_t get_offset_of_mVuMarkTemplate_0() { return static_cast<int32_t>(offsetof(VuMarkTargetImpl_t2724072581, ___mVuMarkTemplate_0)); }
	inline VuMarkTemplateImpl_t1477556351 * get_mVuMarkTemplate_0() const { return ___mVuMarkTemplate_0; }
	inline VuMarkTemplateImpl_t1477556351 ** get_address_of_mVuMarkTemplate_0() { return &___mVuMarkTemplate_0; }
	inline void set_mVuMarkTemplate_0(VuMarkTemplateImpl_t1477556351 * value)
	{
		___mVuMarkTemplate_0 = value;
		Il2CppCodeGenWriteBarrier((&___mVuMarkTemplate_0), value);
	}

	inline static int32_t get_offset_of_mInstanceId_1() { return static_cast<int32_t>(offsetof(VuMarkTargetImpl_t2724072581, ___mInstanceId_1)); }
	inline InstanceIdImpl_t2419732903 * get_mInstanceId_1() const { return ___mInstanceId_1; }
	inline InstanceIdImpl_t2419732903 ** get_address_of_mInstanceId_1() { return &___mInstanceId_1; }
	inline void set_mInstanceId_1(InstanceIdImpl_t2419732903 * value)
	{
		___mInstanceId_1 = value;
		Il2CppCodeGenWriteBarrier((&___mInstanceId_1), value);
	}

	inline static int32_t get_offset_of_mTargetId_2() { return static_cast<int32_t>(offsetof(VuMarkTargetImpl_t2724072581, ___mTargetId_2)); }
	inline int32_t get_mTargetId_2() const { return ___mTargetId_2; }
	inline int32_t* get_address_of_mTargetId_2() { return &___mTargetId_2; }
	inline void set_mTargetId_2(int32_t value)
	{
		___mTargetId_2 = value;
	}

	inline static int32_t get_offset_of_mInstanceImage_3() { return static_cast<int32_t>(offsetof(VuMarkTargetImpl_t2724072581, ___mInstanceImage_3)); }
	inline Image_t1445313791 * get_mInstanceImage_3() const { return ___mInstanceImage_3; }
	inline Image_t1445313791 ** get_address_of_mInstanceImage_3() { return &___mInstanceImage_3; }
	inline void set_mInstanceImage_3(Image_t1445313791 * value)
	{
		___mInstanceImage_3 = value;
		Il2CppCodeGenWriteBarrier((&___mInstanceImage_3), value);
	}

	inline static int32_t get_offset_of_mInstanceImageHeader_4() { return static_cast<int32_t>(offsetof(VuMarkTargetImpl_t2724072581, ___mInstanceImageHeader_4)); }
	inline ImageHeaderData_t1829409304  get_mInstanceImageHeader_4() const { return ___mInstanceImageHeader_4; }
	inline ImageHeaderData_t1829409304 * get_address_of_mInstanceImageHeader_4() { return &___mInstanceImageHeader_4; }
	inline void set_mInstanceImageHeader_4(ImageHeaderData_t1829409304  value)
	{
		___mInstanceImageHeader_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VUMARKTARGETIMPL_T2724072581_H
#ifndef VUMARKTEMPLATEIMPL_T1477556351_H
#define VUMARKTEMPLATEIMPL_T1477556351_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.VuMarkTemplateImpl
struct  VuMarkTemplateImpl_t1477556351  : public ObjectTargetImpl_t1592445137
{
public:
	// System.String Vuforia.VuMarkTemplateImpl::mUserData
	String_t* ___mUserData_4;
	// UnityEngine.Vector2 Vuforia.VuMarkTemplateImpl::mOrigin
	Vector2_t3577333262  ___mOrigin_5;
	// System.Boolean Vuforia.VuMarkTemplateImpl::mTrackingFromRuntimeAppearance
	bool ___mTrackingFromRuntimeAppearance_6;

public:
	inline static int32_t get_offset_of_mUserData_4() { return static_cast<int32_t>(offsetof(VuMarkTemplateImpl_t1477556351, ___mUserData_4)); }
	inline String_t* get_mUserData_4() const { return ___mUserData_4; }
	inline String_t** get_address_of_mUserData_4() { return &___mUserData_4; }
	inline void set_mUserData_4(String_t* value)
	{
		___mUserData_4 = value;
		Il2CppCodeGenWriteBarrier((&___mUserData_4), value);
	}

	inline static int32_t get_offset_of_mOrigin_5() { return static_cast<int32_t>(offsetof(VuMarkTemplateImpl_t1477556351, ___mOrigin_5)); }
	inline Vector2_t3577333262  get_mOrigin_5() const { return ___mOrigin_5; }
	inline Vector2_t3577333262 * get_address_of_mOrigin_5() { return &___mOrigin_5; }
	inline void set_mOrigin_5(Vector2_t3577333262  value)
	{
		___mOrigin_5 = value;
	}

	inline static int32_t get_offset_of_mTrackingFromRuntimeAppearance_6() { return static_cast<int32_t>(offsetof(VuMarkTemplateImpl_t1477556351, ___mTrackingFromRuntimeAppearance_6)); }
	inline bool get_mTrackingFromRuntimeAppearance_6() const { return ___mTrackingFromRuntimeAppearance_6; }
	inline bool* get_address_of_mTrackingFromRuntimeAppearance_6() { return &___mTrackingFromRuntimeAppearance_6; }
	inline void set_mTrackingFromRuntimeAppearance_6(bool value)
	{
		___mTrackingFromRuntimeAppearance_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VUMARKTEMPLATEIMPL_T1477556351_H
#ifndef MIXEDREALITYCONTROLLER_T4091258454_H
#define MIXEDREALITYCONTROLLER_T4091258454_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.MixedRealityController
struct  MixedRealityController_t4091258454  : public RuntimeObject
{
public:
	// Vuforia.VuforiaARController Vuforia.MixedRealityController::mVuforiaBehaviour
	VuforiaARController_t1328503142 * ___mVuforiaBehaviour_1;
	// Vuforia.DigitalEyewearARController Vuforia.MixedRealityController::mDigitalEyewearBehaviour
	DigitalEyewearARController_t1877756253 * ___mDigitalEyewearBehaviour_2;
	// Vuforia.VideoBackgroundManager Vuforia.MixedRealityController::mVideoBackgroundManager
	VideoBackgroundManager_t2068552549 * ___mVideoBackgroundManager_3;
	// System.Boolean Vuforia.MixedRealityController::mViewerHasBeenSetExternally
	bool ___mViewerHasBeenSetExternally_4;
	// Vuforia.IViewerParameters Vuforia.MixedRealityController::mViewerParameters
	RuntimeObject* ___mViewerParameters_5;
	// System.Boolean Vuforia.MixedRealityController::mFrameWorkHasBeenSetExternally
	bool ___mFrameWorkHasBeenSetExternally_6;
	// Vuforia.DigitalEyewearARController/StereoFramework Vuforia.MixedRealityController::mStereoFramework
	int32_t ___mStereoFramework_7;
	// UnityEngine.Transform Vuforia.MixedRealityController::mCentralAnchorPoint
	Transform_t3316442598 * ___mCentralAnchorPoint_8;
	// UnityEngine.Camera Vuforia.MixedRealityController::mLeftCameraOfExternalSDK
	Camera_t3175186167 * ___mLeftCameraOfExternalSDK_9;
	// UnityEngine.Camera Vuforia.MixedRealityController::mRightCameraOfExternalSDK
	Camera_t3175186167 * ___mRightCameraOfExternalSDK_10;
	// System.Boolean Vuforia.MixedRealityController::mObjectTrackerStopped
	bool ___mObjectTrackerStopped_11;
	// System.Boolean Vuforia.MixedRealityController::mAutoStopCameraIfNotRequired
	bool ___mAutoStopCameraIfNotRequired_12;

public:
	inline static int32_t get_offset_of_mVuforiaBehaviour_1() { return static_cast<int32_t>(offsetof(MixedRealityController_t4091258454, ___mVuforiaBehaviour_1)); }
	inline VuforiaARController_t1328503142 * get_mVuforiaBehaviour_1() const { return ___mVuforiaBehaviour_1; }
	inline VuforiaARController_t1328503142 ** get_address_of_mVuforiaBehaviour_1() { return &___mVuforiaBehaviour_1; }
	inline void set_mVuforiaBehaviour_1(VuforiaARController_t1328503142 * value)
	{
		___mVuforiaBehaviour_1 = value;
		Il2CppCodeGenWriteBarrier((&___mVuforiaBehaviour_1), value);
	}

	inline static int32_t get_offset_of_mDigitalEyewearBehaviour_2() { return static_cast<int32_t>(offsetof(MixedRealityController_t4091258454, ___mDigitalEyewearBehaviour_2)); }
	inline DigitalEyewearARController_t1877756253 * get_mDigitalEyewearBehaviour_2() const { return ___mDigitalEyewearBehaviour_2; }
	inline DigitalEyewearARController_t1877756253 ** get_address_of_mDigitalEyewearBehaviour_2() { return &___mDigitalEyewearBehaviour_2; }
	inline void set_mDigitalEyewearBehaviour_2(DigitalEyewearARController_t1877756253 * value)
	{
		___mDigitalEyewearBehaviour_2 = value;
		Il2CppCodeGenWriteBarrier((&___mDigitalEyewearBehaviour_2), value);
	}

	inline static int32_t get_offset_of_mVideoBackgroundManager_3() { return static_cast<int32_t>(offsetof(MixedRealityController_t4091258454, ___mVideoBackgroundManager_3)); }
	inline VideoBackgroundManager_t2068552549 * get_mVideoBackgroundManager_3() const { return ___mVideoBackgroundManager_3; }
	inline VideoBackgroundManager_t2068552549 ** get_address_of_mVideoBackgroundManager_3() { return &___mVideoBackgroundManager_3; }
	inline void set_mVideoBackgroundManager_3(VideoBackgroundManager_t2068552549 * value)
	{
		___mVideoBackgroundManager_3 = value;
		Il2CppCodeGenWriteBarrier((&___mVideoBackgroundManager_3), value);
	}

	inline static int32_t get_offset_of_mViewerHasBeenSetExternally_4() { return static_cast<int32_t>(offsetof(MixedRealityController_t4091258454, ___mViewerHasBeenSetExternally_4)); }
	inline bool get_mViewerHasBeenSetExternally_4() const { return ___mViewerHasBeenSetExternally_4; }
	inline bool* get_address_of_mViewerHasBeenSetExternally_4() { return &___mViewerHasBeenSetExternally_4; }
	inline void set_mViewerHasBeenSetExternally_4(bool value)
	{
		___mViewerHasBeenSetExternally_4 = value;
	}

	inline static int32_t get_offset_of_mViewerParameters_5() { return static_cast<int32_t>(offsetof(MixedRealityController_t4091258454, ___mViewerParameters_5)); }
	inline RuntimeObject* get_mViewerParameters_5() const { return ___mViewerParameters_5; }
	inline RuntimeObject** get_address_of_mViewerParameters_5() { return &___mViewerParameters_5; }
	inline void set_mViewerParameters_5(RuntimeObject* value)
	{
		___mViewerParameters_5 = value;
		Il2CppCodeGenWriteBarrier((&___mViewerParameters_5), value);
	}

	inline static int32_t get_offset_of_mFrameWorkHasBeenSetExternally_6() { return static_cast<int32_t>(offsetof(MixedRealityController_t4091258454, ___mFrameWorkHasBeenSetExternally_6)); }
	inline bool get_mFrameWorkHasBeenSetExternally_6() const { return ___mFrameWorkHasBeenSetExternally_6; }
	inline bool* get_address_of_mFrameWorkHasBeenSetExternally_6() { return &___mFrameWorkHasBeenSetExternally_6; }
	inline void set_mFrameWorkHasBeenSetExternally_6(bool value)
	{
		___mFrameWorkHasBeenSetExternally_6 = value;
	}

	inline static int32_t get_offset_of_mStereoFramework_7() { return static_cast<int32_t>(offsetof(MixedRealityController_t4091258454, ___mStereoFramework_7)); }
	inline int32_t get_mStereoFramework_7() const { return ___mStereoFramework_7; }
	inline int32_t* get_address_of_mStereoFramework_7() { return &___mStereoFramework_7; }
	inline void set_mStereoFramework_7(int32_t value)
	{
		___mStereoFramework_7 = value;
	}

	inline static int32_t get_offset_of_mCentralAnchorPoint_8() { return static_cast<int32_t>(offsetof(MixedRealityController_t4091258454, ___mCentralAnchorPoint_8)); }
	inline Transform_t3316442598 * get_mCentralAnchorPoint_8() const { return ___mCentralAnchorPoint_8; }
	inline Transform_t3316442598 ** get_address_of_mCentralAnchorPoint_8() { return &___mCentralAnchorPoint_8; }
	inline void set_mCentralAnchorPoint_8(Transform_t3316442598 * value)
	{
		___mCentralAnchorPoint_8 = value;
		Il2CppCodeGenWriteBarrier((&___mCentralAnchorPoint_8), value);
	}

	inline static int32_t get_offset_of_mLeftCameraOfExternalSDK_9() { return static_cast<int32_t>(offsetof(MixedRealityController_t4091258454, ___mLeftCameraOfExternalSDK_9)); }
	inline Camera_t3175186167 * get_mLeftCameraOfExternalSDK_9() const { return ___mLeftCameraOfExternalSDK_9; }
	inline Camera_t3175186167 ** get_address_of_mLeftCameraOfExternalSDK_9() { return &___mLeftCameraOfExternalSDK_9; }
	inline void set_mLeftCameraOfExternalSDK_9(Camera_t3175186167 * value)
	{
		___mLeftCameraOfExternalSDK_9 = value;
		Il2CppCodeGenWriteBarrier((&___mLeftCameraOfExternalSDK_9), value);
	}

	inline static int32_t get_offset_of_mRightCameraOfExternalSDK_10() { return static_cast<int32_t>(offsetof(MixedRealityController_t4091258454, ___mRightCameraOfExternalSDK_10)); }
	inline Camera_t3175186167 * get_mRightCameraOfExternalSDK_10() const { return ___mRightCameraOfExternalSDK_10; }
	inline Camera_t3175186167 ** get_address_of_mRightCameraOfExternalSDK_10() { return &___mRightCameraOfExternalSDK_10; }
	inline void set_mRightCameraOfExternalSDK_10(Camera_t3175186167 * value)
	{
		___mRightCameraOfExternalSDK_10 = value;
		Il2CppCodeGenWriteBarrier((&___mRightCameraOfExternalSDK_10), value);
	}

	inline static int32_t get_offset_of_mObjectTrackerStopped_11() { return static_cast<int32_t>(offsetof(MixedRealityController_t4091258454, ___mObjectTrackerStopped_11)); }
	inline bool get_mObjectTrackerStopped_11() const { return ___mObjectTrackerStopped_11; }
	inline bool* get_address_of_mObjectTrackerStopped_11() { return &___mObjectTrackerStopped_11; }
	inline void set_mObjectTrackerStopped_11(bool value)
	{
		___mObjectTrackerStopped_11 = value;
	}

	inline static int32_t get_offset_of_mAutoStopCameraIfNotRequired_12() { return static_cast<int32_t>(offsetof(MixedRealityController_t4091258454, ___mAutoStopCameraIfNotRequired_12)); }
	inline bool get_mAutoStopCameraIfNotRequired_12() const { return ___mAutoStopCameraIfNotRequired_12; }
	inline bool* get_address_of_mAutoStopCameraIfNotRequired_12() { return &___mAutoStopCameraIfNotRequired_12; }
	inline void set_mAutoStopCameraIfNotRequired_12(bool value)
	{
		___mAutoStopCameraIfNotRequired_12 = value;
	}
};

struct MixedRealityController_t4091258454_StaticFields
{
public:
	// Vuforia.MixedRealityController Vuforia.MixedRealityController::mInstance
	MixedRealityController_t4091258454 * ___mInstance_0;

public:
	inline static int32_t get_offset_of_mInstance_0() { return static_cast<int32_t>(offsetof(MixedRealityController_t4091258454_StaticFields, ___mInstance_0)); }
	inline MixedRealityController_t4091258454 * get_mInstance_0() const { return ___mInstance_0; }
	inline MixedRealityController_t4091258454 ** get_address_of_mInstance_0() { return &___mInstance_0; }
	inline void set_mInstance_0(MixedRealityController_t4091258454 * value)
	{
		___mInstance_0 = value;
		Il2CppCodeGenWriteBarrier((&___mInstance_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MIXEDREALITYCONTROLLER_T4091258454_H
#ifndef RECONSTRUCTIONFROMTARGETIMPL_T886280682_H
#define RECONSTRUCTIONFROMTARGETIMPL_T886280682_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.ReconstructionFromTargetImpl
struct  ReconstructionFromTargetImpl_t886280682  : public ReconstructionImpl_t1634174475
{
public:
	// UnityEngine.Vector3 Vuforia.ReconstructionFromTargetImpl::mOccluderMin
	Vector3_t2903530434  ___mOccluderMin_5;
	// UnityEngine.Vector3 Vuforia.ReconstructionFromTargetImpl::mOccluderMax
	Vector3_t2903530434  ___mOccluderMax_6;
	// UnityEngine.Vector3 Vuforia.ReconstructionFromTargetImpl::mOccluderOffset
	Vector3_t2903530434  ___mOccluderOffset_7;
	// UnityEngine.Quaternion Vuforia.ReconstructionFromTargetImpl::mOccluderRotation
	Quaternion_t754065749  ___mOccluderRotation_8;
	// Vuforia.Trackable Vuforia.ReconstructionFromTargetImpl::mInitializationTarget
	RuntimeObject* ___mInitializationTarget_9;
	// System.Boolean Vuforia.ReconstructionFromTargetImpl::mCanAutoSetInitializationTarget
	bool ___mCanAutoSetInitializationTarget_10;

public:
	inline static int32_t get_offset_of_mOccluderMin_5() { return static_cast<int32_t>(offsetof(ReconstructionFromTargetImpl_t886280682, ___mOccluderMin_5)); }
	inline Vector3_t2903530434  get_mOccluderMin_5() const { return ___mOccluderMin_5; }
	inline Vector3_t2903530434 * get_address_of_mOccluderMin_5() { return &___mOccluderMin_5; }
	inline void set_mOccluderMin_5(Vector3_t2903530434  value)
	{
		___mOccluderMin_5 = value;
	}

	inline static int32_t get_offset_of_mOccluderMax_6() { return static_cast<int32_t>(offsetof(ReconstructionFromTargetImpl_t886280682, ___mOccluderMax_6)); }
	inline Vector3_t2903530434  get_mOccluderMax_6() const { return ___mOccluderMax_6; }
	inline Vector3_t2903530434 * get_address_of_mOccluderMax_6() { return &___mOccluderMax_6; }
	inline void set_mOccluderMax_6(Vector3_t2903530434  value)
	{
		___mOccluderMax_6 = value;
	}

	inline static int32_t get_offset_of_mOccluderOffset_7() { return static_cast<int32_t>(offsetof(ReconstructionFromTargetImpl_t886280682, ___mOccluderOffset_7)); }
	inline Vector3_t2903530434  get_mOccluderOffset_7() const { return ___mOccluderOffset_7; }
	inline Vector3_t2903530434 * get_address_of_mOccluderOffset_7() { return &___mOccluderOffset_7; }
	inline void set_mOccluderOffset_7(Vector3_t2903530434  value)
	{
		___mOccluderOffset_7 = value;
	}

	inline static int32_t get_offset_of_mOccluderRotation_8() { return static_cast<int32_t>(offsetof(ReconstructionFromTargetImpl_t886280682, ___mOccluderRotation_8)); }
	inline Quaternion_t754065749  get_mOccluderRotation_8() const { return ___mOccluderRotation_8; }
	inline Quaternion_t754065749 * get_address_of_mOccluderRotation_8() { return &___mOccluderRotation_8; }
	inline void set_mOccluderRotation_8(Quaternion_t754065749  value)
	{
		___mOccluderRotation_8 = value;
	}

	inline static int32_t get_offset_of_mInitializationTarget_9() { return static_cast<int32_t>(offsetof(ReconstructionFromTargetImpl_t886280682, ___mInitializationTarget_9)); }
	inline RuntimeObject* get_mInitializationTarget_9() const { return ___mInitializationTarget_9; }
	inline RuntimeObject** get_address_of_mInitializationTarget_9() { return &___mInitializationTarget_9; }
	inline void set_mInitializationTarget_9(RuntimeObject* value)
	{
		___mInitializationTarget_9 = value;
		Il2CppCodeGenWriteBarrier((&___mInitializationTarget_9), value);
	}

	inline static int32_t get_offset_of_mCanAutoSetInitializationTarget_10() { return static_cast<int32_t>(offsetof(ReconstructionFromTargetImpl_t886280682, ___mCanAutoSetInitializationTarget_10)); }
	inline bool get_mCanAutoSetInitializationTarget_10() const { return ___mCanAutoSetInitializationTarget_10; }
	inline bool* get_address_of_mCanAutoSetInitializationTarget_10() { return &___mCanAutoSetInitializationTarget_10; }
	inline void set_mCanAutoSetInitializationTarget_10(bool value)
	{
		___mCanAutoSetInitializationTarget_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RECONSTRUCTIONFROMTARGETIMPL_T886280682_H
#ifndef DEVICETRACKERARCONTROLLER_T2334311105_H
#define DEVICETRACKERARCONTROLLER_T2334311105_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.DeviceTrackerARController
struct  DeviceTrackerARController_t2334311105  : public ARController_t1205915989
{
public:
	// System.Boolean Vuforia.DeviceTrackerARController::mAutoInitTracker
	bool ___mAutoInitTracker_3;
	// System.Boolean Vuforia.DeviceTrackerARController::mAutoStartTracker
	bool ___mAutoStartTracker_4;
	// System.Boolean Vuforia.DeviceTrackerARController::mPosePrediction
	bool ___mPosePrediction_5;
	// Vuforia.RotationalDeviceTracker/MODEL_CORRECTION_MODE Vuforia.DeviceTrackerARController::mModelCorrectionMode
	int32_t ___mModelCorrectionMode_6;
	// System.Boolean Vuforia.DeviceTrackerARController::mModelTransformEnabled
	bool ___mModelTransformEnabled_7;
	// UnityEngine.Vector3 Vuforia.DeviceTrackerARController::mModelTransform
	Vector3_t2903530434  ___mModelTransform_8;
	// System.Action Vuforia.DeviceTrackerARController::mTrackerStarted
	Action_t3619184611 * ___mTrackerStarted_9;
	// System.Boolean Vuforia.DeviceTrackerARController::mTrackerWasActiveBeforePause
	bool ___mTrackerWasActiveBeforePause_10;
	// System.Boolean Vuforia.DeviceTrackerARController::mTrackerWasActiveBeforeDisabling
	bool ___mTrackerWasActiveBeforeDisabling_11;

public:
	inline static int32_t get_offset_of_mAutoInitTracker_3() { return static_cast<int32_t>(offsetof(DeviceTrackerARController_t2334311105, ___mAutoInitTracker_3)); }
	inline bool get_mAutoInitTracker_3() const { return ___mAutoInitTracker_3; }
	inline bool* get_address_of_mAutoInitTracker_3() { return &___mAutoInitTracker_3; }
	inline void set_mAutoInitTracker_3(bool value)
	{
		___mAutoInitTracker_3 = value;
	}

	inline static int32_t get_offset_of_mAutoStartTracker_4() { return static_cast<int32_t>(offsetof(DeviceTrackerARController_t2334311105, ___mAutoStartTracker_4)); }
	inline bool get_mAutoStartTracker_4() const { return ___mAutoStartTracker_4; }
	inline bool* get_address_of_mAutoStartTracker_4() { return &___mAutoStartTracker_4; }
	inline void set_mAutoStartTracker_4(bool value)
	{
		___mAutoStartTracker_4 = value;
	}

	inline static int32_t get_offset_of_mPosePrediction_5() { return static_cast<int32_t>(offsetof(DeviceTrackerARController_t2334311105, ___mPosePrediction_5)); }
	inline bool get_mPosePrediction_5() const { return ___mPosePrediction_5; }
	inline bool* get_address_of_mPosePrediction_5() { return &___mPosePrediction_5; }
	inline void set_mPosePrediction_5(bool value)
	{
		___mPosePrediction_5 = value;
	}

	inline static int32_t get_offset_of_mModelCorrectionMode_6() { return static_cast<int32_t>(offsetof(DeviceTrackerARController_t2334311105, ___mModelCorrectionMode_6)); }
	inline int32_t get_mModelCorrectionMode_6() const { return ___mModelCorrectionMode_6; }
	inline int32_t* get_address_of_mModelCorrectionMode_6() { return &___mModelCorrectionMode_6; }
	inline void set_mModelCorrectionMode_6(int32_t value)
	{
		___mModelCorrectionMode_6 = value;
	}

	inline static int32_t get_offset_of_mModelTransformEnabled_7() { return static_cast<int32_t>(offsetof(DeviceTrackerARController_t2334311105, ___mModelTransformEnabled_7)); }
	inline bool get_mModelTransformEnabled_7() const { return ___mModelTransformEnabled_7; }
	inline bool* get_address_of_mModelTransformEnabled_7() { return &___mModelTransformEnabled_7; }
	inline void set_mModelTransformEnabled_7(bool value)
	{
		___mModelTransformEnabled_7 = value;
	}

	inline static int32_t get_offset_of_mModelTransform_8() { return static_cast<int32_t>(offsetof(DeviceTrackerARController_t2334311105, ___mModelTransform_8)); }
	inline Vector3_t2903530434  get_mModelTransform_8() const { return ___mModelTransform_8; }
	inline Vector3_t2903530434 * get_address_of_mModelTransform_8() { return &___mModelTransform_8; }
	inline void set_mModelTransform_8(Vector3_t2903530434  value)
	{
		___mModelTransform_8 = value;
	}

	inline static int32_t get_offset_of_mTrackerStarted_9() { return static_cast<int32_t>(offsetof(DeviceTrackerARController_t2334311105, ___mTrackerStarted_9)); }
	inline Action_t3619184611 * get_mTrackerStarted_9() const { return ___mTrackerStarted_9; }
	inline Action_t3619184611 ** get_address_of_mTrackerStarted_9() { return &___mTrackerStarted_9; }
	inline void set_mTrackerStarted_9(Action_t3619184611 * value)
	{
		___mTrackerStarted_9 = value;
		Il2CppCodeGenWriteBarrier((&___mTrackerStarted_9), value);
	}

	inline static int32_t get_offset_of_mTrackerWasActiveBeforePause_10() { return static_cast<int32_t>(offsetof(DeviceTrackerARController_t2334311105, ___mTrackerWasActiveBeforePause_10)); }
	inline bool get_mTrackerWasActiveBeforePause_10() const { return ___mTrackerWasActiveBeforePause_10; }
	inline bool* get_address_of_mTrackerWasActiveBeforePause_10() { return &___mTrackerWasActiveBeforePause_10; }
	inline void set_mTrackerWasActiveBeforePause_10(bool value)
	{
		___mTrackerWasActiveBeforePause_10 = value;
	}

	inline static int32_t get_offset_of_mTrackerWasActiveBeforeDisabling_11() { return static_cast<int32_t>(offsetof(DeviceTrackerARController_t2334311105, ___mTrackerWasActiveBeforeDisabling_11)); }
	inline bool get_mTrackerWasActiveBeforeDisabling_11() const { return ___mTrackerWasActiveBeforeDisabling_11; }
	inline bool* get_address_of_mTrackerWasActiveBeforeDisabling_11() { return &___mTrackerWasActiveBeforeDisabling_11; }
	inline void set_mTrackerWasActiveBeforeDisabling_11(bool value)
	{
		___mTrackerWasActiveBeforeDisabling_11 = value;
	}
};

struct DeviceTrackerARController_t2334311105_StaticFields
{
public:
	// UnityEngine.Vector3 Vuforia.DeviceTrackerARController::DEFAULT_HEAD_PIVOT
	Vector3_t2903530434  ___DEFAULT_HEAD_PIVOT_1;
	// UnityEngine.Vector3 Vuforia.DeviceTrackerARController::DEFAULT_HANDHELD_PIVOT
	Vector3_t2903530434  ___DEFAULT_HANDHELD_PIVOT_2;
	// Vuforia.DeviceTrackerARController Vuforia.DeviceTrackerARController::mInstance
	DeviceTrackerARController_t2334311105 * ___mInstance_12;
	// System.Object Vuforia.DeviceTrackerARController::mPadlock
	RuntimeObject * ___mPadlock_13;

public:
	inline static int32_t get_offset_of_DEFAULT_HEAD_PIVOT_1() { return static_cast<int32_t>(offsetof(DeviceTrackerARController_t2334311105_StaticFields, ___DEFAULT_HEAD_PIVOT_1)); }
	inline Vector3_t2903530434  get_DEFAULT_HEAD_PIVOT_1() const { return ___DEFAULT_HEAD_PIVOT_1; }
	inline Vector3_t2903530434 * get_address_of_DEFAULT_HEAD_PIVOT_1() { return &___DEFAULT_HEAD_PIVOT_1; }
	inline void set_DEFAULT_HEAD_PIVOT_1(Vector3_t2903530434  value)
	{
		___DEFAULT_HEAD_PIVOT_1 = value;
	}

	inline static int32_t get_offset_of_DEFAULT_HANDHELD_PIVOT_2() { return static_cast<int32_t>(offsetof(DeviceTrackerARController_t2334311105_StaticFields, ___DEFAULT_HANDHELD_PIVOT_2)); }
	inline Vector3_t2903530434  get_DEFAULT_HANDHELD_PIVOT_2() const { return ___DEFAULT_HANDHELD_PIVOT_2; }
	inline Vector3_t2903530434 * get_address_of_DEFAULT_HANDHELD_PIVOT_2() { return &___DEFAULT_HANDHELD_PIVOT_2; }
	inline void set_DEFAULT_HANDHELD_PIVOT_2(Vector3_t2903530434  value)
	{
		___DEFAULT_HANDHELD_PIVOT_2 = value;
	}

	inline static int32_t get_offset_of_mInstance_12() { return static_cast<int32_t>(offsetof(DeviceTrackerARController_t2334311105_StaticFields, ___mInstance_12)); }
	inline DeviceTrackerARController_t2334311105 * get_mInstance_12() const { return ___mInstance_12; }
	inline DeviceTrackerARController_t2334311105 ** get_address_of_mInstance_12() { return &___mInstance_12; }
	inline void set_mInstance_12(DeviceTrackerARController_t2334311105 * value)
	{
		___mInstance_12 = value;
		Il2CppCodeGenWriteBarrier((&___mInstance_12), value);
	}

	inline static int32_t get_offset_of_mPadlock_13() { return static_cast<int32_t>(offsetof(DeviceTrackerARController_t2334311105_StaticFields, ___mPadlock_13)); }
	inline RuntimeObject * get_mPadlock_13() const { return ___mPadlock_13; }
	inline RuntimeObject ** get_address_of_mPadlock_13() { return &___mPadlock_13; }
	inline void set_mPadlock_13(RuntimeObject * value)
	{
		___mPadlock_13 = value;
		Il2CppCodeGenWriteBarrier((&___mPadlock_13), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEVICETRACKERARCONTROLLER_T2334311105_H
#ifndef ROTATIONALDEVICETRACKERIMPL_T1875102819_H
#define ROTATIONALDEVICETRACKERIMPL_T1875102819_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.RotationalDeviceTrackerImpl
struct  RotationalDeviceTrackerImpl_t1875102819  : public RotationalDeviceTracker_t2131173692
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ROTATIONALDEVICETRACKERIMPL_T1875102819_H
#ifndef CUSTOMVIEWERPARAMETERS_T701518465_H
#define CUSTOMVIEWERPARAMETERS_T701518465_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.CustomViewerParameters
struct  CustomViewerParameters_t701518465  : public ViewerParameters_t3954347578
{
public:
	// System.Single Vuforia.CustomViewerParameters::mVersion
	float ___mVersion_1;
	// System.String Vuforia.CustomViewerParameters::mName
	String_t* ___mName_2;
	// System.String Vuforia.CustomViewerParameters::mManufacturer
	String_t* ___mManufacturer_3;
	// Vuforia.ViewerButtonType Vuforia.CustomViewerParameters::mButtonType
	int32_t ___mButtonType_4;
	// System.Single Vuforia.CustomViewerParameters::mScreenToLensDistance
	float ___mScreenToLensDistance_5;
	// Vuforia.ViewerTrayAlignment Vuforia.CustomViewerParameters::mTrayAlignment
	int32_t ___mTrayAlignment_6;
	// System.Boolean Vuforia.CustomViewerParameters::mMagnet
	bool ___mMagnet_7;

public:
	inline static int32_t get_offset_of_mVersion_1() { return static_cast<int32_t>(offsetof(CustomViewerParameters_t701518465, ___mVersion_1)); }
	inline float get_mVersion_1() const { return ___mVersion_1; }
	inline float* get_address_of_mVersion_1() { return &___mVersion_1; }
	inline void set_mVersion_1(float value)
	{
		___mVersion_1 = value;
	}

	inline static int32_t get_offset_of_mName_2() { return static_cast<int32_t>(offsetof(CustomViewerParameters_t701518465, ___mName_2)); }
	inline String_t* get_mName_2() const { return ___mName_2; }
	inline String_t** get_address_of_mName_2() { return &___mName_2; }
	inline void set_mName_2(String_t* value)
	{
		___mName_2 = value;
		Il2CppCodeGenWriteBarrier((&___mName_2), value);
	}

	inline static int32_t get_offset_of_mManufacturer_3() { return static_cast<int32_t>(offsetof(CustomViewerParameters_t701518465, ___mManufacturer_3)); }
	inline String_t* get_mManufacturer_3() const { return ___mManufacturer_3; }
	inline String_t** get_address_of_mManufacturer_3() { return &___mManufacturer_3; }
	inline void set_mManufacturer_3(String_t* value)
	{
		___mManufacturer_3 = value;
		Il2CppCodeGenWriteBarrier((&___mManufacturer_3), value);
	}

	inline static int32_t get_offset_of_mButtonType_4() { return static_cast<int32_t>(offsetof(CustomViewerParameters_t701518465, ___mButtonType_4)); }
	inline int32_t get_mButtonType_4() const { return ___mButtonType_4; }
	inline int32_t* get_address_of_mButtonType_4() { return &___mButtonType_4; }
	inline void set_mButtonType_4(int32_t value)
	{
		___mButtonType_4 = value;
	}

	inline static int32_t get_offset_of_mScreenToLensDistance_5() { return static_cast<int32_t>(offsetof(CustomViewerParameters_t701518465, ___mScreenToLensDistance_5)); }
	inline float get_mScreenToLensDistance_5() const { return ___mScreenToLensDistance_5; }
	inline float* get_address_of_mScreenToLensDistance_5() { return &___mScreenToLensDistance_5; }
	inline void set_mScreenToLensDistance_5(float value)
	{
		___mScreenToLensDistance_5 = value;
	}

	inline static int32_t get_offset_of_mTrayAlignment_6() { return static_cast<int32_t>(offsetof(CustomViewerParameters_t701518465, ___mTrayAlignment_6)); }
	inline int32_t get_mTrayAlignment_6() const { return ___mTrayAlignment_6; }
	inline int32_t* get_address_of_mTrayAlignment_6() { return &___mTrayAlignment_6; }
	inline void set_mTrayAlignment_6(int32_t value)
	{
		___mTrayAlignment_6 = value;
	}

	inline static int32_t get_offset_of_mMagnet_7() { return static_cast<int32_t>(offsetof(CustomViewerParameters_t701518465, ___mMagnet_7)); }
	inline bool get_mMagnet_7() const { return ___mMagnet_7; }
	inline bool* get_address_of_mMagnet_7() { return &___mMagnet_7; }
	inline void set_mMagnet_7(bool value)
	{
		___mMagnet_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CUSTOMVIEWERPARAMETERS_T701518465_H
#ifndef DIGITALEYEWEARARCONTROLLER_T1877756253_H
#define DIGITALEYEWEARARCONTROLLER_T1877756253_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.DigitalEyewearARController
struct  DigitalEyewearARController_t1877756253  : public ARController_t1205915989
{
public:
	// System.Single Vuforia.DigitalEyewearARController::mCameraOffset
	float ___mCameraOffset_7;
	// Vuforia.DistortionRenderingMode Vuforia.DigitalEyewearARController::mDistortionRenderingMode
	int32_t ___mDistortionRenderingMode_8;
	// System.Int32 Vuforia.DigitalEyewearARController::mDistortionRenderingLayer
	int32_t ___mDistortionRenderingLayer_9;
	// Vuforia.DigitalEyewearARController/EyewearType Vuforia.DigitalEyewearARController::mEyewearType
	int32_t ___mEyewearType_10;
	// Vuforia.DigitalEyewearARController/StereoFramework Vuforia.DigitalEyewearARController::mStereoFramework
	int32_t ___mStereoFramework_11;
	// Vuforia.DigitalEyewearARController/SeeThroughConfiguration Vuforia.DigitalEyewearARController::mSeeThroughConfiguration
	int32_t ___mSeeThroughConfiguration_12;
	// System.String Vuforia.DigitalEyewearARController::mViewerName
	String_t* ___mViewerName_13;
	// System.String Vuforia.DigitalEyewearARController::mViewerManufacturer
	String_t* ___mViewerManufacturer_14;
	// System.Boolean Vuforia.DigitalEyewearARController::mUseCustomViewer
	bool ___mUseCustomViewer_15;
	// Vuforia.DigitalEyewearARController/SerializableViewerParameters Vuforia.DigitalEyewearARController::mCustomViewer
	SerializableViewerParameters_t484734121 * ___mCustomViewer_16;
	// UnityEngine.Transform Vuforia.DigitalEyewearARController::mCentralAnchorPoint
	Transform_t3316442598 * ___mCentralAnchorPoint_17;
	// UnityEngine.Transform Vuforia.DigitalEyewearARController::mParentAnchorPoint
	Transform_t3316442598 * ___mParentAnchorPoint_18;
	// UnityEngine.Camera Vuforia.DigitalEyewearARController::mPrimaryCamera
	Camera_t3175186167 * ___mPrimaryCamera_19;
	// UnityEngine.Rect Vuforia.DigitalEyewearARController::mPrimaryCameraOriginalRect
	Rect_t3436776195  ___mPrimaryCameraOriginalRect_20;
	// UnityEngine.Camera Vuforia.DigitalEyewearARController::mSecondaryCamera
	Camera_t3175186167 * ___mSecondaryCamera_21;
	// UnityEngine.Rect Vuforia.DigitalEyewearARController::mSecondaryCameraOriginalRect
	Rect_t3436776195  ___mSecondaryCameraOriginalRect_22;
	// System.Boolean Vuforia.DigitalEyewearARController::mSecondaryCameraDisabledLocally
	bool ___mSecondaryCameraDisabledLocally_23;
	// Vuforia.VuforiaARController Vuforia.DigitalEyewearARController::mVuforiaBehaviour
	VuforiaARController_t1328503142 * ___mVuforiaBehaviour_24;
	// Vuforia.DistortionRenderingBehaviour Vuforia.DigitalEyewearARController::mDistortionRenderingBhvr
	DistortionRenderingBehaviour_t2785666256 * ___mDistortionRenderingBhvr_25;
	// System.Boolean Vuforia.DigitalEyewearARController::mSetFocusPlaneAutomatically
	bool ___mSetFocusPlaneAutomatically_26;

public:
	inline static int32_t get_offset_of_mCameraOffset_7() { return static_cast<int32_t>(offsetof(DigitalEyewearARController_t1877756253, ___mCameraOffset_7)); }
	inline float get_mCameraOffset_7() const { return ___mCameraOffset_7; }
	inline float* get_address_of_mCameraOffset_7() { return &___mCameraOffset_7; }
	inline void set_mCameraOffset_7(float value)
	{
		___mCameraOffset_7 = value;
	}

	inline static int32_t get_offset_of_mDistortionRenderingMode_8() { return static_cast<int32_t>(offsetof(DigitalEyewearARController_t1877756253, ___mDistortionRenderingMode_8)); }
	inline int32_t get_mDistortionRenderingMode_8() const { return ___mDistortionRenderingMode_8; }
	inline int32_t* get_address_of_mDistortionRenderingMode_8() { return &___mDistortionRenderingMode_8; }
	inline void set_mDistortionRenderingMode_8(int32_t value)
	{
		___mDistortionRenderingMode_8 = value;
	}

	inline static int32_t get_offset_of_mDistortionRenderingLayer_9() { return static_cast<int32_t>(offsetof(DigitalEyewearARController_t1877756253, ___mDistortionRenderingLayer_9)); }
	inline int32_t get_mDistortionRenderingLayer_9() const { return ___mDistortionRenderingLayer_9; }
	inline int32_t* get_address_of_mDistortionRenderingLayer_9() { return &___mDistortionRenderingLayer_9; }
	inline void set_mDistortionRenderingLayer_9(int32_t value)
	{
		___mDistortionRenderingLayer_9 = value;
	}

	inline static int32_t get_offset_of_mEyewearType_10() { return static_cast<int32_t>(offsetof(DigitalEyewearARController_t1877756253, ___mEyewearType_10)); }
	inline int32_t get_mEyewearType_10() const { return ___mEyewearType_10; }
	inline int32_t* get_address_of_mEyewearType_10() { return &___mEyewearType_10; }
	inline void set_mEyewearType_10(int32_t value)
	{
		___mEyewearType_10 = value;
	}

	inline static int32_t get_offset_of_mStereoFramework_11() { return static_cast<int32_t>(offsetof(DigitalEyewearARController_t1877756253, ___mStereoFramework_11)); }
	inline int32_t get_mStereoFramework_11() const { return ___mStereoFramework_11; }
	inline int32_t* get_address_of_mStereoFramework_11() { return &___mStereoFramework_11; }
	inline void set_mStereoFramework_11(int32_t value)
	{
		___mStereoFramework_11 = value;
	}

	inline static int32_t get_offset_of_mSeeThroughConfiguration_12() { return static_cast<int32_t>(offsetof(DigitalEyewearARController_t1877756253, ___mSeeThroughConfiguration_12)); }
	inline int32_t get_mSeeThroughConfiguration_12() const { return ___mSeeThroughConfiguration_12; }
	inline int32_t* get_address_of_mSeeThroughConfiguration_12() { return &___mSeeThroughConfiguration_12; }
	inline void set_mSeeThroughConfiguration_12(int32_t value)
	{
		___mSeeThroughConfiguration_12 = value;
	}

	inline static int32_t get_offset_of_mViewerName_13() { return static_cast<int32_t>(offsetof(DigitalEyewearARController_t1877756253, ___mViewerName_13)); }
	inline String_t* get_mViewerName_13() const { return ___mViewerName_13; }
	inline String_t** get_address_of_mViewerName_13() { return &___mViewerName_13; }
	inline void set_mViewerName_13(String_t* value)
	{
		___mViewerName_13 = value;
		Il2CppCodeGenWriteBarrier((&___mViewerName_13), value);
	}

	inline static int32_t get_offset_of_mViewerManufacturer_14() { return static_cast<int32_t>(offsetof(DigitalEyewearARController_t1877756253, ___mViewerManufacturer_14)); }
	inline String_t* get_mViewerManufacturer_14() const { return ___mViewerManufacturer_14; }
	inline String_t** get_address_of_mViewerManufacturer_14() { return &___mViewerManufacturer_14; }
	inline void set_mViewerManufacturer_14(String_t* value)
	{
		___mViewerManufacturer_14 = value;
		Il2CppCodeGenWriteBarrier((&___mViewerManufacturer_14), value);
	}

	inline static int32_t get_offset_of_mUseCustomViewer_15() { return static_cast<int32_t>(offsetof(DigitalEyewearARController_t1877756253, ___mUseCustomViewer_15)); }
	inline bool get_mUseCustomViewer_15() const { return ___mUseCustomViewer_15; }
	inline bool* get_address_of_mUseCustomViewer_15() { return &___mUseCustomViewer_15; }
	inline void set_mUseCustomViewer_15(bool value)
	{
		___mUseCustomViewer_15 = value;
	}

	inline static int32_t get_offset_of_mCustomViewer_16() { return static_cast<int32_t>(offsetof(DigitalEyewearARController_t1877756253, ___mCustomViewer_16)); }
	inline SerializableViewerParameters_t484734121 * get_mCustomViewer_16() const { return ___mCustomViewer_16; }
	inline SerializableViewerParameters_t484734121 ** get_address_of_mCustomViewer_16() { return &___mCustomViewer_16; }
	inline void set_mCustomViewer_16(SerializableViewerParameters_t484734121 * value)
	{
		___mCustomViewer_16 = value;
		Il2CppCodeGenWriteBarrier((&___mCustomViewer_16), value);
	}

	inline static int32_t get_offset_of_mCentralAnchorPoint_17() { return static_cast<int32_t>(offsetof(DigitalEyewearARController_t1877756253, ___mCentralAnchorPoint_17)); }
	inline Transform_t3316442598 * get_mCentralAnchorPoint_17() const { return ___mCentralAnchorPoint_17; }
	inline Transform_t3316442598 ** get_address_of_mCentralAnchorPoint_17() { return &___mCentralAnchorPoint_17; }
	inline void set_mCentralAnchorPoint_17(Transform_t3316442598 * value)
	{
		___mCentralAnchorPoint_17 = value;
		Il2CppCodeGenWriteBarrier((&___mCentralAnchorPoint_17), value);
	}

	inline static int32_t get_offset_of_mParentAnchorPoint_18() { return static_cast<int32_t>(offsetof(DigitalEyewearARController_t1877756253, ___mParentAnchorPoint_18)); }
	inline Transform_t3316442598 * get_mParentAnchorPoint_18() const { return ___mParentAnchorPoint_18; }
	inline Transform_t3316442598 ** get_address_of_mParentAnchorPoint_18() { return &___mParentAnchorPoint_18; }
	inline void set_mParentAnchorPoint_18(Transform_t3316442598 * value)
	{
		___mParentAnchorPoint_18 = value;
		Il2CppCodeGenWriteBarrier((&___mParentAnchorPoint_18), value);
	}

	inline static int32_t get_offset_of_mPrimaryCamera_19() { return static_cast<int32_t>(offsetof(DigitalEyewearARController_t1877756253, ___mPrimaryCamera_19)); }
	inline Camera_t3175186167 * get_mPrimaryCamera_19() const { return ___mPrimaryCamera_19; }
	inline Camera_t3175186167 ** get_address_of_mPrimaryCamera_19() { return &___mPrimaryCamera_19; }
	inline void set_mPrimaryCamera_19(Camera_t3175186167 * value)
	{
		___mPrimaryCamera_19 = value;
		Il2CppCodeGenWriteBarrier((&___mPrimaryCamera_19), value);
	}

	inline static int32_t get_offset_of_mPrimaryCameraOriginalRect_20() { return static_cast<int32_t>(offsetof(DigitalEyewearARController_t1877756253, ___mPrimaryCameraOriginalRect_20)); }
	inline Rect_t3436776195  get_mPrimaryCameraOriginalRect_20() const { return ___mPrimaryCameraOriginalRect_20; }
	inline Rect_t3436776195 * get_address_of_mPrimaryCameraOriginalRect_20() { return &___mPrimaryCameraOriginalRect_20; }
	inline void set_mPrimaryCameraOriginalRect_20(Rect_t3436776195  value)
	{
		___mPrimaryCameraOriginalRect_20 = value;
	}

	inline static int32_t get_offset_of_mSecondaryCamera_21() { return static_cast<int32_t>(offsetof(DigitalEyewearARController_t1877756253, ___mSecondaryCamera_21)); }
	inline Camera_t3175186167 * get_mSecondaryCamera_21() const { return ___mSecondaryCamera_21; }
	inline Camera_t3175186167 ** get_address_of_mSecondaryCamera_21() { return &___mSecondaryCamera_21; }
	inline void set_mSecondaryCamera_21(Camera_t3175186167 * value)
	{
		___mSecondaryCamera_21 = value;
		Il2CppCodeGenWriteBarrier((&___mSecondaryCamera_21), value);
	}

	inline static int32_t get_offset_of_mSecondaryCameraOriginalRect_22() { return static_cast<int32_t>(offsetof(DigitalEyewearARController_t1877756253, ___mSecondaryCameraOriginalRect_22)); }
	inline Rect_t3436776195  get_mSecondaryCameraOriginalRect_22() const { return ___mSecondaryCameraOriginalRect_22; }
	inline Rect_t3436776195 * get_address_of_mSecondaryCameraOriginalRect_22() { return &___mSecondaryCameraOriginalRect_22; }
	inline void set_mSecondaryCameraOriginalRect_22(Rect_t3436776195  value)
	{
		___mSecondaryCameraOriginalRect_22 = value;
	}

	inline static int32_t get_offset_of_mSecondaryCameraDisabledLocally_23() { return static_cast<int32_t>(offsetof(DigitalEyewearARController_t1877756253, ___mSecondaryCameraDisabledLocally_23)); }
	inline bool get_mSecondaryCameraDisabledLocally_23() const { return ___mSecondaryCameraDisabledLocally_23; }
	inline bool* get_address_of_mSecondaryCameraDisabledLocally_23() { return &___mSecondaryCameraDisabledLocally_23; }
	inline void set_mSecondaryCameraDisabledLocally_23(bool value)
	{
		___mSecondaryCameraDisabledLocally_23 = value;
	}

	inline static int32_t get_offset_of_mVuforiaBehaviour_24() { return static_cast<int32_t>(offsetof(DigitalEyewearARController_t1877756253, ___mVuforiaBehaviour_24)); }
	inline VuforiaARController_t1328503142 * get_mVuforiaBehaviour_24() const { return ___mVuforiaBehaviour_24; }
	inline VuforiaARController_t1328503142 ** get_address_of_mVuforiaBehaviour_24() { return &___mVuforiaBehaviour_24; }
	inline void set_mVuforiaBehaviour_24(VuforiaARController_t1328503142 * value)
	{
		___mVuforiaBehaviour_24 = value;
		Il2CppCodeGenWriteBarrier((&___mVuforiaBehaviour_24), value);
	}

	inline static int32_t get_offset_of_mDistortionRenderingBhvr_25() { return static_cast<int32_t>(offsetof(DigitalEyewearARController_t1877756253, ___mDistortionRenderingBhvr_25)); }
	inline DistortionRenderingBehaviour_t2785666256 * get_mDistortionRenderingBhvr_25() const { return ___mDistortionRenderingBhvr_25; }
	inline DistortionRenderingBehaviour_t2785666256 ** get_address_of_mDistortionRenderingBhvr_25() { return &___mDistortionRenderingBhvr_25; }
	inline void set_mDistortionRenderingBhvr_25(DistortionRenderingBehaviour_t2785666256 * value)
	{
		___mDistortionRenderingBhvr_25 = value;
		Il2CppCodeGenWriteBarrier((&___mDistortionRenderingBhvr_25), value);
	}

	inline static int32_t get_offset_of_mSetFocusPlaneAutomatically_26() { return static_cast<int32_t>(offsetof(DigitalEyewearARController_t1877756253, ___mSetFocusPlaneAutomatically_26)); }
	inline bool get_mSetFocusPlaneAutomatically_26() const { return ___mSetFocusPlaneAutomatically_26; }
	inline bool* get_address_of_mSetFocusPlaneAutomatically_26() { return &___mSetFocusPlaneAutomatically_26; }
	inline void set_mSetFocusPlaneAutomatically_26(bool value)
	{
		___mSetFocusPlaneAutomatically_26 = value;
	}
};

struct DigitalEyewearARController_t1877756253_StaticFields
{
public:
	// Vuforia.DigitalEyewearARController Vuforia.DigitalEyewearARController::mInstance
	DigitalEyewearARController_t1877756253 * ___mInstance_27;
	// System.Object Vuforia.DigitalEyewearARController::mPadlock
	RuntimeObject * ___mPadlock_28;

public:
	inline static int32_t get_offset_of_mInstance_27() { return static_cast<int32_t>(offsetof(DigitalEyewearARController_t1877756253_StaticFields, ___mInstance_27)); }
	inline DigitalEyewearARController_t1877756253 * get_mInstance_27() const { return ___mInstance_27; }
	inline DigitalEyewearARController_t1877756253 ** get_address_of_mInstance_27() { return &___mInstance_27; }
	inline void set_mInstance_27(DigitalEyewearARController_t1877756253 * value)
	{
		___mInstance_27 = value;
		Il2CppCodeGenWriteBarrier((&___mInstance_27), value);
	}

	inline static int32_t get_offset_of_mPadlock_28() { return static_cast<int32_t>(offsetof(DigitalEyewearARController_t1877756253_StaticFields, ___mPadlock_28)); }
	inline RuntimeObject * get_mPadlock_28() const { return ___mPadlock_28; }
	inline RuntimeObject ** get_address_of_mPadlock_28() { return &___mPadlock_28; }
	inline void set_mPadlock_28(RuntimeObject * value)
	{
		___mPadlock_28 = value;
		Il2CppCodeGenWriteBarrier((&___mPadlock_28), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DIGITALEYEWEARARCONTROLLER_T1877756253_H
#ifndef INSTANCEIDIMPL_T2419732903_H
#define INSTANCEIDIMPL_T2419732903_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.InstanceIdImpl
struct  InstanceIdImpl_t2419732903  : public RuntimeObject
{
public:
	// Vuforia.InstanceIdType Vuforia.InstanceIdImpl::mDataType
	int32_t ___mDataType_0;
	// System.Byte[] Vuforia.InstanceIdImpl::mBuffer
	ByteU5BU5D_t3172826560* ___mBuffer_1;
	// System.UInt64 Vuforia.InstanceIdImpl::mNumericValue
	uint64_t ___mNumericValue_2;
	// System.UInt32 Vuforia.InstanceIdImpl::mDataLength
	uint32_t ___mDataLength_3;
	// System.String Vuforia.InstanceIdImpl::mCachedStringValue
	String_t* ___mCachedStringValue_4;

public:
	inline static int32_t get_offset_of_mDataType_0() { return static_cast<int32_t>(offsetof(InstanceIdImpl_t2419732903, ___mDataType_0)); }
	inline int32_t get_mDataType_0() const { return ___mDataType_0; }
	inline int32_t* get_address_of_mDataType_0() { return &___mDataType_0; }
	inline void set_mDataType_0(int32_t value)
	{
		___mDataType_0 = value;
	}

	inline static int32_t get_offset_of_mBuffer_1() { return static_cast<int32_t>(offsetof(InstanceIdImpl_t2419732903, ___mBuffer_1)); }
	inline ByteU5BU5D_t3172826560* get_mBuffer_1() const { return ___mBuffer_1; }
	inline ByteU5BU5D_t3172826560** get_address_of_mBuffer_1() { return &___mBuffer_1; }
	inline void set_mBuffer_1(ByteU5BU5D_t3172826560* value)
	{
		___mBuffer_1 = value;
		Il2CppCodeGenWriteBarrier((&___mBuffer_1), value);
	}

	inline static int32_t get_offset_of_mNumericValue_2() { return static_cast<int32_t>(offsetof(InstanceIdImpl_t2419732903, ___mNumericValue_2)); }
	inline uint64_t get_mNumericValue_2() const { return ___mNumericValue_2; }
	inline uint64_t* get_address_of_mNumericValue_2() { return &___mNumericValue_2; }
	inline void set_mNumericValue_2(uint64_t value)
	{
		___mNumericValue_2 = value;
	}

	inline static int32_t get_offset_of_mDataLength_3() { return static_cast<int32_t>(offsetof(InstanceIdImpl_t2419732903, ___mDataLength_3)); }
	inline uint32_t get_mDataLength_3() const { return ___mDataLength_3; }
	inline uint32_t* get_address_of_mDataLength_3() { return &___mDataLength_3; }
	inline void set_mDataLength_3(uint32_t value)
	{
		___mDataLength_3 = value;
	}

	inline static int32_t get_offset_of_mCachedStringValue_4() { return static_cast<int32_t>(offsetof(InstanceIdImpl_t2419732903, ___mCachedStringValue_4)); }
	inline String_t* get_mCachedStringValue_4() const { return ___mCachedStringValue_4; }
	inline String_t** get_address_of_mCachedStringValue_4() { return &___mCachedStringValue_4; }
	inline void set_mCachedStringValue_4(String_t* value)
	{
		___mCachedStringValue_4 = value;
		Il2CppCodeGenWriteBarrier((&___mCachedStringValue_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INSTANCEIDIMPL_T2419732903_H
#ifndef ROTATIONALPLAYMODEDEVICETRACKERIMPL_T2014906089_H
#define ROTATIONALPLAYMODEDEVICETRACKERIMPL_T2014906089_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.RotationalPlayModeDeviceTrackerImpl
struct  RotationalPlayModeDeviceTrackerImpl_t2014906089  : public RotationalDeviceTracker_t2131173692
{
public:
	// UnityEngine.Vector3 Vuforia.RotationalPlayModeDeviceTrackerImpl::mRotation
	Vector3_t2903530434  ___mRotation_1;
	// UnityEngine.Vector3 Vuforia.RotationalPlayModeDeviceTrackerImpl::mModelCorrectionTransform
	Vector3_t2903530434  ___mModelCorrectionTransform_2;
	// Vuforia.RotationalDeviceTracker/MODEL_CORRECTION_MODE Vuforia.RotationalPlayModeDeviceTrackerImpl::mModelCorrection
	int32_t ___mModelCorrection_3;

public:
	inline static int32_t get_offset_of_mRotation_1() { return static_cast<int32_t>(offsetof(RotationalPlayModeDeviceTrackerImpl_t2014906089, ___mRotation_1)); }
	inline Vector3_t2903530434  get_mRotation_1() const { return ___mRotation_1; }
	inline Vector3_t2903530434 * get_address_of_mRotation_1() { return &___mRotation_1; }
	inline void set_mRotation_1(Vector3_t2903530434  value)
	{
		___mRotation_1 = value;
	}

	inline static int32_t get_offset_of_mModelCorrectionTransform_2() { return static_cast<int32_t>(offsetof(RotationalPlayModeDeviceTrackerImpl_t2014906089, ___mModelCorrectionTransform_2)); }
	inline Vector3_t2903530434  get_mModelCorrectionTransform_2() const { return ___mModelCorrectionTransform_2; }
	inline Vector3_t2903530434 * get_address_of_mModelCorrectionTransform_2() { return &___mModelCorrectionTransform_2; }
	inline void set_mModelCorrectionTransform_2(Vector3_t2903530434  value)
	{
		___mModelCorrectionTransform_2 = value;
	}

	inline static int32_t get_offset_of_mModelCorrection_3() { return static_cast<int32_t>(offsetof(RotationalPlayModeDeviceTrackerImpl_t2014906089, ___mModelCorrection_3)); }
	inline int32_t get_mModelCorrection_3() const { return ___mModelCorrection_3; }
	inline int32_t* get_address_of_mModelCorrection_3() { return &___mModelCorrection_3; }
	inline void set_mModelCorrection_3(int32_t value)
	{
		___mModelCorrection_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ROTATIONALPLAYMODEDEVICETRACKERIMPL_T2014906089_H
#ifndef BEHAVIOUR_T363748010_H
#define BEHAVIOUR_T363748010_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_t363748010  : public Component_t4087199522
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_T363748010_H
#ifndef DEDICATEDEYEWEARCAMERACONFIGURATION_T1287559789_H
#define DEDICATEDEYEWEARCAMERACONFIGURATION_T1287559789_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.DedicatedEyewearCameraConfiguration
struct  DedicatedEyewearCameraConfiguration_t1287559789  : public BaseCameraConfiguration_t196193726
{
public:
	// UnityEngine.Camera Vuforia.DedicatedEyewearCameraConfiguration::mPrimaryCamera
	Camera_t3175186167 * ___mPrimaryCamera_11;
	// UnityEngine.Camera Vuforia.DedicatedEyewearCameraConfiguration::mSecondaryCamera
	Camera_t3175186167 * ___mSecondaryCamera_12;
	// System.Int32 Vuforia.DedicatedEyewearCameraConfiguration::mScreenWidth
	int32_t ___mScreenWidth_13;
	// System.Int32 Vuforia.DedicatedEyewearCameraConfiguration::mScreenHeight
	int32_t ___mScreenHeight_14;
	// System.Boolean Vuforia.DedicatedEyewearCameraConfiguration::mNeedToCheckStereo
	bool ___mNeedToCheckStereo_15;
	// System.Single Vuforia.DedicatedEyewearCameraConfiguration::mLastAppliedNearClipPlane
	float ___mLastAppliedNearClipPlane_16;
	// System.Single Vuforia.DedicatedEyewearCameraConfiguration::mLastAppliedFarClipPlane
	float ___mLastAppliedFarClipPlane_17;
	// System.Single Vuforia.DedicatedEyewearCameraConfiguration::mLastAppliedVirtualFoV
	float ___mLastAppliedVirtualFoV_18;
	// System.Single Vuforia.DedicatedEyewearCameraConfiguration::mNewNearClipPlane
	float ___mNewNearClipPlane_19;
	// System.Single Vuforia.DedicatedEyewearCameraConfiguration::mNewFarClipPlane
	float ___mNewFarClipPlane_20;
	// System.Single Vuforia.DedicatedEyewearCameraConfiguration::mNewVirtualFoV
	float ___mNewVirtualFoV_21;
	// Vuforia.EyewearDevice Vuforia.DedicatedEyewearCameraConfiguration::mEyewearDevice
	EyewearDevice_t3960213283 * ___mEyewearDevice_22;

public:
	inline static int32_t get_offset_of_mPrimaryCamera_11() { return static_cast<int32_t>(offsetof(DedicatedEyewearCameraConfiguration_t1287559789, ___mPrimaryCamera_11)); }
	inline Camera_t3175186167 * get_mPrimaryCamera_11() const { return ___mPrimaryCamera_11; }
	inline Camera_t3175186167 ** get_address_of_mPrimaryCamera_11() { return &___mPrimaryCamera_11; }
	inline void set_mPrimaryCamera_11(Camera_t3175186167 * value)
	{
		___mPrimaryCamera_11 = value;
		Il2CppCodeGenWriteBarrier((&___mPrimaryCamera_11), value);
	}

	inline static int32_t get_offset_of_mSecondaryCamera_12() { return static_cast<int32_t>(offsetof(DedicatedEyewearCameraConfiguration_t1287559789, ___mSecondaryCamera_12)); }
	inline Camera_t3175186167 * get_mSecondaryCamera_12() const { return ___mSecondaryCamera_12; }
	inline Camera_t3175186167 ** get_address_of_mSecondaryCamera_12() { return &___mSecondaryCamera_12; }
	inline void set_mSecondaryCamera_12(Camera_t3175186167 * value)
	{
		___mSecondaryCamera_12 = value;
		Il2CppCodeGenWriteBarrier((&___mSecondaryCamera_12), value);
	}

	inline static int32_t get_offset_of_mScreenWidth_13() { return static_cast<int32_t>(offsetof(DedicatedEyewearCameraConfiguration_t1287559789, ___mScreenWidth_13)); }
	inline int32_t get_mScreenWidth_13() const { return ___mScreenWidth_13; }
	inline int32_t* get_address_of_mScreenWidth_13() { return &___mScreenWidth_13; }
	inline void set_mScreenWidth_13(int32_t value)
	{
		___mScreenWidth_13 = value;
	}

	inline static int32_t get_offset_of_mScreenHeight_14() { return static_cast<int32_t>(offsetof(DedicatedEyewearCameraConfiguration_t1287559789, ___mScreenHeight_14)); }
	inline int32_t get_mScreenHeight_14() const { return ___mScreenHeight_14; }
	inline int32_t* get_address_of_mScreenHeight_14() { return &___mScreenHeight_14; }
	inline void set_mScreenHeight_14(int32_t value)
	{
		___mScreenHeight_14 = value;
	}

	inline static int32_t get_offset_of_mNeedToCheckStereo_15() { return static_cast<int32_t>(offsetof(DedicatedEyewearCameraConfiguration_t1287559789, ___mNeedToCheckStereo_15)); }
	inline bool get_mNeedToCheckStereo_15() const { return ___mNeedToCheckStereo_15; }
	inline bool* get_address_of_mNeedToCheckStereo_15() { return &___mNeedToCheckStereo_15; }
	inline void set_mNeedToCheckStereo_15(bool value)
	{
		___mNeedToCheckStereo_15 = value;
	}

	inline static int32_t get_offset_of_mLastAppliedNearClipPlane_16() { return static_cast<int32_t>(offsetof(DedicatedEyewearCameraConfiguration_t1287559789, ___mLastAppliedNearClipPlane_16)); }
	inline float get_mLastAppliedNearClipPlane_16() const { return ___mLastAppliedNearClipPlane_16; }
	inline float* get_address_of_mLastAppliedNearClipPlane_16() { return &___mLastAppliedNearClipPlane_16; }
	inline void set_mLastAppliedNearClipPlane_16(float value)
	{
		___mLastAppliedNearClipPlane_16 = value;
	}

	inline static int32_t get_offset_of_mLastAppliedFarClipPlane_17() { return static_cast<int32_t>(offsetof(DedicatedEyewearCameraConfiguration_t1287559789, ___mLastAppliedFarClipPlane_17)); }
	inline float get_mLastAppliedFarClipPlane_17() const { return ___mLastAppliedFarClipPlane_17; }
	inline float* get_address_of_mLastAppliedFarClipPlane_17() { return &___mLastAppliedFarClipPlane_17; }
	inline void set_mLastAppliedFarClipPlane_17(float value)
	{
		___mLastAppliedFarClipPlane_17 = value;
	}

	inline static int32_t get_offset_of_mLastAppliedVirtualFoV_18() { return static_cast<int32_t>(offsetof(DedicatedEyewearCameraConfiguration_t1287559789, ___mLastAppliedVirtualFoV_18)); }
	inline float get_mLastAppliedVirtualFoV_18() const { return ___mLastAppliedVirtualFoV_18; }
	inline float* get_address_of_mLastAppliedVirtualFoV_18() { return &___mLastAppliedVirtualFoV_18; }
	inline void set_mLastAppliedVirtualFoV_18(float value)
	{
		___mLastAppliedVirtualFoV_18 = value;
	}

	inline static int32_t get_offset_of_mNewNearClipPlane_19() { return static_cast<int32_t>(offsetof(DedicatedEyewearCameraConfiguration_t1287559789, ___mNewNearClipPlane_19)); }
	inline float get_mNewNearClipPlane_19() const { return ___mNewNearClipPlane_19; }
	inline float* get_address_of_mNewNearClipPlane_19() { return &___mNewNearClipPlane_19; }
	inline void set_mNewNearClipPlane_19(float value)
	{
		___mNewNearClipPlane_19 = value;
	}

	inline static int32_t get_offset_of_mNewFarClipPlane_20() { return static_cast<int32_t>(offsetof(DedicatedEyewearCameraConfiguration_t1287559789, ___mNewFarClipPlane_20)); }
	inline float get_mNewFarClipPlane_20() const { return ___mNewFarClipPlane_20; }
	inline float* get_address_of_mNewFarClipPlane_20() { return &___mNewFarClipPlane_20; }
	inline void set_mNewFarClipPlane_20(float value)
	{
		___mNewFarClipPlane_20 = value;
	}

	inline static int32_t get_offset_of_mNewVirtualFoV_21() { return static_cast<int32_t>(offsetof(DedicatedEyewearCameraConfiguration_t1287559789, ___mNewVirtualFoV_21)); }
	inline float get_mNewVirtualFoV_21() const { return ___mNewVirtualFoV_21; }
	inline float* get_address_of_mNewVirtualFoV_21() { return &___mNewVirtualFoV_21; }
	inline void set_mNewVirtualFoV_21(float value)
	{
		___mNewVirtualFoV_21 = value;
	}

	inline static int32_t get_offset_of_mEyewearDevice_22() { return static_cast<int32_t>(offsetof(DedicatedEyewearCameraConfiguration_t1287559789, ___mEyewearDevice_22)); }
	inline EyewearDevice_t3960213283 * get_mEyewearDevice_22() const { return ___mEyewearDevice_22; }
	inline EyewearDevice_t3960213283 ** get_address_of_mEyewearDevice_22() { return &___mEyewearDevice_22; }
	inline void set_mEyewearDevice_22(EyewearDevice_t3960213283 * value)
	{
		___mEyewearDevice_22 = value;
		Il2CppCodeGenWriteBarrier((&___mEyewearDevice_22), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEDICATEDEYEWEARCAMERACONFIGURATION_T1287559789_H
#ifndef BASESTEREOVIEWERCAMERACONFIGURATION_T1087628355_H
#define BASESTEREOVIEWERCAMERACONFIGURATION_T1087628355_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.BaseStereoViewerCameraConfiguration
struct  BaseStereoViewerCameraConfiguration_t1087628355  : public BaseCameraConfiguration_t196193726
{
public:
	// UnityEngine.Camera Vuforia.BaseStereoViewerCameraConfiguration::mPrimaryCamera
	Camera_t3175186167 * ___mPrimaryCamera_10;
	// UnityEngine.Camera Vuforia.BaseStereoViewerCameraConfiguration::mSecondaryCamera
	Camera_t3175186167 * ___mSecondaryCamera_11;
	// System.Boolean Vuforia.BaseStereoViewerCameraConfiguration::mSkewFrustum
	bool ___mSkewFrustum_12;
	// System.Int32 Vuforia.BaseStereoViewerCameraConfiguration::mScreenWidth
	int32_t ___mScreenWidth_13;
	// System.Int32 Vuforia.BaseStereoViewerCameraConfiguration::mScreenHeight
	int32_t ___mScreenHeight_14;

public:
	inline static int32_t get_offset_of_mPrimaryCamera_10() { return static_cast<int32_t>(offsetof(BaseStereoViewerCameraConfiguration_t1087628355, ___mPrimaryCamera_10)); }
	inline Camera_t3175186167 * get_mPrimaryCamera_10() const { return ___mPrimaryCamera_10; }
	inline Camera_t3175186167 ** get_address_of_mPrimaryCamera_10() { return &___mPrimaryCamera_10; }
	inline void set_mPrimaryCamera_10(Camera_t3175186167 * value)
	{
		___mPrimaryCamera_10 = value;
		Il2CppCodeGenWriteBarrier((&___mPrimaryCamera_10), value);
	}

	inline static int32_t get_offset_of_mSecondaryCamera_11() { return static_cast<int32_t>(offsetof(BaseStereoViewerCameraConfiguration_t1087628355, ___mSecondaryCamera_11)); }
	inline Camera_t3175186167 * get_mSecondaryCamera_11() const { return ___mSecondaryCamera_11; }
	inline Camera_t3175186167 ** get_address_of_mSecondaryCamera_11() { return &___mSecondaryCamera_11; }
	inline void set_mSecondaryCamera_11(Camera_t3175186167 * value)
	{
		___mSecondaryCamera_11 = value;
		Il2CppCodeGenWriteBarrier((&___mSecondaryCamera_11), value);
	}

	inline static int32_t get_offset_of_mSkewFrustum_12() { return static_cast<int32_t>(offsetof(BaseStereoViewerCameraConfiguration_t1087628355, ___mSkewFrustum_12)); }
	inline bool get_mSkewFrustum_12() const { return ___mSkewFrustum_12; }
	inline bool* get_address_of_mSkewFrustum_12() { return &___mSkewFrustum_12; }
	inline void set_mSkewFrustum_12(bool value)
	{
		___mSkewFrustum_12 = value;
	}

	inline static int32_t get_offset_of_mScreenWidth_13() { return static_cast<int32_t>(offsetof(BaseStereoViewerCameraConfiguration_t1087628355, ___mScreenWidth_13)); }
	inline int32_t get_mScreenWidth_13() const { return ___mScreenWidth_13; }
	inline int32_t* get_address_of_mScreenWidth_13() { return &___mScreenWidth_13; }
	inline void set_mScreenWidth_13(int32_t value)
	{
		___mScreenWidth_13 = value;
	}

	inline static int32_t get_offset_of_mScreenHeight_14() { return static_cast<int32_t>(offsetof(BaseStereoViewerCameraConfiguration_t1087628355, ___mScreenHeight_14)); }
	inline int32_t get_mScreenHeight_14() const { return ___mScreenHeight_14; }
	inline int32_t* get_address_of_mScreenHeight_14() { return &___mScreenHeight_14; }
	inline void set_mScreenHeight_14(int32_t value)
	{
		___mScreenHeight_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASESTEREOVIEWERCAMERACONFIGURATION_T1087628355_H
#ifndef MONOCAMERACONFIGURATION_T715615799_H
#define MONOCAMERACONFIGURATION_T715615799_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.MonoCameraConfiguration
struct  MonoCameraConfiguration_t715615799  : public BaseCameraConfiguration_t196193726
{
public:
	// UnityEngine.Camera Vuforia.MonoCameraConfiguration::mPrimaryCamera
	Camera_t3175186167 * ___mPrimaryCamera_11;
	// System.Int32 Vuforia.MonoCameraConfiguration::mCameraViewPortWidth
	int32_t ___mCameraViewPortWidth_12;
	// System.Int32 Vuforia.MonoCameraConfiguration::mCameraViewPortHeight
	int32_t ___mCameraViewPortHeight_13;
	// System.Single Vuforia.MonoCameraConfiguration::mLastAppliedNearClipPlane
	float ___mLastAppliedNearClipPlane_14;
	// System.Single Vuforia.MonoCameraConfiguration::mLastAppliedFarClipPlane
	float ___mLastAppliedFarClipPlane_15;
	// System.Single Vuforia.MonoCameraConfiguration::mLastAppliedFoV
	float ___mLastAppliedFoV_16;

public:
	inline static int32_t get_offset_of_mPrimaryCamera_11() { return static_cast<int32_t>(offsetof(MonoCameraConfiguration_t715615799, ___mPrimaryCamera_11)); }
	inline Camera_t3175186167 * get_mPrimaryCamera_11() const { return ___mPrimaryCamera_11; }
	inline Camera_t3175186167 ** get_address_of_mPrimaryCamera_11() { return &___mPrimaryCamera_11; }
	inline void set_mPrimaryCamera_11(Camera_t3175186167 * value)
	{
		___mPrimaryCamera_11 = value;
		Il2CppCodeGenWriteBarrier((&___mPrimaryCamera_11), value);
	}

	inline static int32_t get_offset_of_mCameraViewPortWidth_12() { return static_cast<int32_t>(offsetof(MonoCameraConfiguration_t715615799, ___mCameraViewPortWidth_12)); }
	inline int32_t get_mCameraViewPortWidth_12() const { return ___mCameraViewPortWidth_12; }
	inline int32_t* get_address_of_mCameraViewPortWidth_12() { return &___mCameraViewPortWidth_12; }
	inline void set_mCameraViewPortWidth_12(int32_t value)
	{
		___mCameraViewPortWidth_12 = value;
	}

	inline static int32_t get_offset_of_mCameraViewPortHeight_13() { return static_cast<int32_t>(offsetof(MonoCameraConfiguration_t715615799, ___mCameraViewPortHeight_13)); }
	inline int32_t get_mCameraViewPortHeight_13() const { return ___mCameraViewPortHeight_13; }
	inline int32_t* get_address_of_mCameraViewPortHeight_13() { return &___mCameraViewPortHeight_13; }
	inline void set_mCameraViewPortHeight_13(int32_t value)
	{
		___mCameraViewPortHeight_13 = value;
	}

	inline static int32_t get_offset_of_mLastAppliedNearClipPlane_14() { return static_cast<int32_t>(offsetof(MonoCameraConfiguration_t715615799, ___mLastAppliedNearClipPlane_14)); }
	inline float get_mLastAppliedNearClipPlane_14() const { return ___mLastAppliedNearClipPlane_14; }
	inline float* get_address_of_mLastAppliedNearClipPlane_14() { return &___mLastAppliedNearClipPlane_14; }
	inline void set_mLastAppliedNearClipPlane_14(float value)
	{
		___mLastAppliedNearClipPlane_14 = value;
	}

	inline static int32_t get_offset_of_mLastAppliedFarClipPlane_15() { return static_cast<int32_t>(offsetof(MonoCameraConfiguration_t715615799, ___mLastAppliedFarClipPlane_15)); }
	inline float get_mLastAppliedFarClipPlane_15() const { return ___mLastAppliedFarClipPlane_15; }
	inline float* get_address_of_mLastAppliedFarClipPlane_15() { return &___mLastAppliedFarClipPlane_15; }
	inline void set_mLastAppliedFarClipPlane_15(float value)
	{
		___mLastAppliedFarClipPlane_15 = value;
	}

	inline static int32_t get_offset_of_mLastAppliedFoV_16() { return static_cast<int32_t>(offsetof(MonoCameraConfiguration_t715615799, ___mLastAppliedFoV_16)); }
	inline float get_mLastAppliedFoV_16() const { return ___mLastAppliedFoV_16; }
	inline float* get_address_of_mLastAppliedFoV_16() { return &___mLastAppliedFoV_16; }
	inline void set_mLastAppliedFoV_16(float value)
	{
		___mLastAppliedFoV_16 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOCAMERACONFIGURATION_T715615799_H
#ifndef MONOBEHAVIOUR_T345688271_H
#define MONOBEHAVIOUR_T345688271_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t345688271  : public Behaviour_t363748010
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T345688271_H
#ifndef STEREOVIEWERCAMERACONFIGURATION_T4238609684_H
#define STEREOVIEWERCAMERACONFIGURATION_T4238609684_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.StereoViewerCameraConfiguration
struct  StereoViewerCameraConfiguration_t4238609684  : public BaseStereoViewerCameraConfiguration_t1087628355
{
public:
	// System.Single Vuforia.StereoViewerCameraConfiguration::mLastAppliedLeftNearClipPlane
	float ___mLastAppliedLeftNearClipPlane_16;
	// System.Single Vuforia.StereoViewerCameraConfiguration::mLastAppliedLeftFarClipPlane
	float ___mLastAppliedLeftFarClipPlane_17;
	// System.Single Vuforia.StereoViewerCameraConfiguration::mLastAppliedRightNearClipPlane
	float ___mLastAppliedRightNearClipPlane_18;
	// System.Single Vuforia.StereoViewerCameraConfiguration::mLastAppliedRightFarClipPlane
	float ___mLastAppliedRightFarClipPlane_19;
	// System.Single Vuforia.StereoViewerCameraConfiguration::mCameraOffset
	float ___mCameraOffset_20;
	// System.Boolean Vuforia.StereoViewerCameraConfiguration::mIsDistorted
	bool ___mIsDistorted_21;

public:
	inline static int32_t get_offset_of_mLastAppliedLeftNearClipPlane_16() { return static_cast<int32_t>(offsetof(StereoViewerCameraConfiguration_t4238609684, ___mLastAppliedLeftNearClipPlane_16)); }
	inline float get_mLastAppliedLeftNearClipPlane_16() const { return ___mLastAppliedLeftNearClipPlane_16; }
	inline float* get_address_of_mLastAppliedLeftNearClipPlane_16() { return &___mLastAppliedLeftNearClipPlane_16; }
	inline void set_mLastAppliedLeftNearClipPlane_16(float value)
	{
		___mLastAppliedLeftNearClipPlane_16 = value;
	}

	inline static int32_t get_offset_of_mLastAppliedLeftFarClipPlane_17() { return static_cast<int32_t>(offsetof(StereoViewerCameraConfiguration_t4238609684, ___mLastAppliedLeftFarClipPlane_17)); }
	inline float get_mLastAppliedLeftFarClipPlane_17() const { return ___mLastAppliedLeftFarClipPlane_17; }
	inline float* get_address_of_mLastAppliedLeftFarClipPlane_17() { return &___mLastAppliedLeftFarClipPlane_17; }
	inline void set_mLastAppliedLeftFarClipPlane_17(float value)
	{
		___mLastAppliedLeftFarClipPlane_17 = value;
	}

	inline static int32_t get_offset_of_mLastAppliedRightNearClipPlane_18() { return static_cast<int32_t>(offsetof(StereoViewerCameraConfiguration_t4238609684, ___mLastAppliedRightNearClipPlane_18)); }
	inline float get_mLastAppliedRightNearClipPlane_18() const { return ___mLastAppliedRightNearClipPlane_18; }
	inline float* get_address_of_mLastAppliedRightNearClipPlane_18() { return &___mLastAppliedRightNearClipPlane_18; }
	inline void set_mLastAppliedRightNearClipPlane_18(float value)
	{
		___mLastAppliedRightNearClipPlane_18 = value;
	}

	inline static int32_t get_offset_of_mLastAppliedRightFarClipPlane_19() { return static_cast<int32_t>(offsetof(StereoViewerCameraConfiguration_t4238609684, ___mLastAppliedRightFarClipPlane_19)); }
	inline float get_mLastAppliedRightFarClipPlane_19() const { return ___mLastAppliedRightFarClipPlane_19; }
	inline float* get_address_of_mLastAppliedRightFarClipPlane_19() { return &___mLastAppliedRightFarClipPlane_19; }
	inline void set_mLastAppliedRightFarClipPlane_19(float value)
	{
		___mLastAppliedRightFarClipPlane_19 = value;
	}

	inline static int32_t get_offset_of_mCameraOffset_20() { return static_cast<int32_t>(offsetof(StereoViewerCameraConfiguration_t4238609684, ___mCameraOffset_20)); }
	inline float get_mCameraOffset_20() const { return ___mCameraOffset_20; }
	inline float* get_address_of_mCameraOffset_20() { return &___mCameraOffset_20; }
	inline void set_mCameraOffset_20(float value)
	{
		___mCameraOffset_20 = value;
	}

	inline static int32_t get_offset_of_mIsDistorted_21() { return static_cast<int32_t>(offsetof(StereoViewerCameraConfiguration_t4238609684, ___mIsDistorted_21)); }
	inline bool get_mIsDistorted_21() const { return ___mIsDistorted_21; }
	inline bool* get_address_of_mIsDistorted_21() { return &___mIsDistorted_21; }
	inline void set_mIsDistorted_21(bool value)
	{
		___mIsDistorted_21 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STEREOVIEWERCAMERACONFIGURATION_T4238609684_H
#ifndef EXTERNALSTEREOCAMERACONFIGURATION_T3079436327_H
#define EXTERNALSTEREOCAMERACONFIGURATION_T3079436327_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.ExternalStereoCameraConfiguration
struct  ExternalStereoCameraConfiguration_t3079436327  : public BaseStereoViewerCameraConfiguration_t1087628355
{
public:
	// System.Single Vuforia.ExternalStereoCameraConfiguration::mLastAppliedLeftNearClipPlane
	float ___mLastAppliedLeftNearClipPlane_16;
	// System.Single Vuforia.ExternalStereoCameraConfiguration::mLastAppliedLeftFarClipPlane
	float ___mLastAppliedLeftFarClipPlane_17;
	// System.Single Vuforia.ExternalStereoCameraConfiguration::mLastAppliedRightNearClipPlane
	float ___mLastAppliedRightNearClipPlane_18;
	// System.Single Vuforia.ExternalStereoCameraConfiguration::mLastAppliedRightFarClipPlane
	float ___mLastAppliedRightFarClipPlane_19;
	// System.Single Vuforia.ExternalStereoCameraConfiguration::mLastAppliedLeftVerticalVirtualFoV
	float ___mLastAppliedLeftVerticalVirtualFoV_20;
	// System.Single Vuforia.ExternalStereoCameraConfiguration::mLastAppliedLeftHorizontalVirtualFoV
	float ___mLastAppliedLeftHorizontalVirtualFoV_21;
	// System.Single Vuforia.ExternalStereoCameraConfiguration::mLastAppliedRightVerticalVirtualFoV
	float ___mLastAppliedRightVerticalVirtualFoV_22;
	// System.Single Vuforia.ExternalStereoCameraConfiguration::mLastAppliedRightHorizontalVirtualFoV
	float ___mLastAppliedRightHorizontalVirtualFoV_23;
	// UnityEngine.Matrix4x4 Vuforia.ExternalStereoCameraConfiguration::mLastAppliedLeftProjection
	Matrix4x4_t4266809202  ___mLastAppliedLeftProjection_24;
	// UnityEngine.Matrix4x4 Vuforia.ExternalStereoCameraConfiguration::mLastAppliedRightProjection
	Matrix4x4_t4266809202  ___mLastAppliedRightProjection_25;
	// System.Single Vuforia.ExternalStereoCameraConfiguration::mNewLeftNearClipPlane
	float ___mNewLeftNearClipPlane_26;
	// System.Single Vuforia.ExternalStereoCameraConfiguration::mNewLeftFarClipPlane
	float ___mNewLeftFarClipPlane_27;
	// System.Single Vuforia.ExternalStereoCameraConfiguration::mNewRightNearClipPlane
	float ___mNewRightNearClipPlane_28;
	// System.Single Vuforia.ExternalStereoCameraConfiguration::mNewRightFarClipPlane
	float ___mNewRightFarClipPlane_29;
	// System.Single Vuforia.ExternalStereoCameraConfiguration::mNewLeftVerticalVirtualFoV
	float ___mNewLeftVerticalVirtualFoV_30;
	// System.Single Vuforia.ExternalStereoCameraConfiguration::mNewLeftHorizontalVirtualFoV
	float ___mNewLeftHorizontalVirtualFoV_31;
	// System.Single Vuforia.ExternalStereoCameraConfiguration::mNewRightVerticalVirtualFoV
	float ___mNewRightVerticalVirtualFoV_32;
	// System.Single Vuforia.ExternalStereoCameraConfiguration::mNewRightHorizontalVirtualFoV
	float ___mNewRightHorizontalVirtualFoV_33;
	// UnityEngine.Matrix4x4 Vuforia.ExternalStereoCameraConfiguration::mExternallySetLeftMatrix
	Matrix4x4_t4266809202  ___mExternallySetLeftMatrix_34;
	// UnityEngine.Matrix4x4 Vuforia.ExternalStereoCameraConfiguration::mExternallySetRightMatrix
	Matrix4x4_t4266809202  ___mExternallySetRightMatrix_35;

public:
	inline static int32_t get_offset_of_mLastAppliedLeftNearClipPlane_16() { return static_cast<int32_t>(offsetof(ExternalStereoCameraConfiguration_t3079436327, ___mLastAppliedLeftNearClipPlane_16)); }
	inline float get_mLastAppliedLeftNearClipPlane_16() const { return ___mLastAppliedLeftNearClipPlane_16; }
	inline float* get_address_of_mLastAppliedLeftNearClipPlane_16() { return &___mLastAppliedLeftNearClipPlane_16; }
	inline void set_mLastAppliedLeftNearClipPlane_16(float value)
	{
		___mLastAppliedLeftNearClipPlane_16 = value;
	}

	inline static int32_t get_offset_of_mLastAppliedLeftFarClipPlane_17() { return static_cast<int32_t>(offsetof(ExternalStereoCameraConfiguration_t3079436327, ___mLastAppliedLeftFarClipPlane_17)); }
	inline float get_mLastAppliedLeftFarClipPlane_17() const { return ___mLastAppliedLeftFarClipPlane_17; }
	inline float* get_address_of_mLastAppliedLeftFarClipPlane_17() { return &___mLastAppliedLeftFarClipPlane_17; }
	inline void set_mLastAppliedLeftFarClipPlane_17(float value)
	{
		___mLastAppliedLeftFarClipPlane_17 = value;
	}

	inline static int32_t get_offset_of_mLastAppliedRightNearClipPlane_18() { return static_cast<int32_t>(offsetof(ExternalStereoCameraConfiguration_t3079436327, ___mLastAppliedRightNearClipPlane_18)); }
	inline float get_mLastAppliedRightNearClipPlane_18() const { return ___mLastAppliedRightNearClipPlane_18; }
	inline float* get_address_of_mLastAppliedRightNearClipPlane_18() { return &___mLastAppliedRightNearClipPlane_18; }
	inline void set_mLastAppliedRightNearClipPlane_18(float value)
	{
		___mLastAppliedRightNearClipPlane_18 = value;
	}

	inline static int32_t get_offset_of_mLastAppliedRightFarClipPlane_19() { return static_cast<int32_t>(offsetof(ExternalStereoCameraConfiguration_t3079436327, ___mLastAppliedRightFarClipPlane_19)); }
	inline float get_mLastAppliedRightFarClipPlane_19() const { return ___mLastAppliedRightFarClipPlane_19; }
	inline float* get_address_of_mLastAppliedRightFarClipPlane_19() { return &___mLastAppliedRightFarClipPlane_19; }
	inline void set_mLastAppliedRightFarClipPlane_19(float value)
	{
		___mLastAppliedRightFarClipPlane_19 = value;
	}

	inline static int32_t get_offset_of_mLastAppliedLeftVerticalVirtualFoV_20() { return static_cast<int32_t>(offsetof(ExternalStereoCameraConfiguration_t3079436327, ___mLastAppliedLeftVerticalVirtualFoV_20)); }
	inline float get_mLastAppliedLeftVerticalVirtualFoV_20() const { return ___mLastAppliedLeftVerticalVirtualFoV_20; }
	inline float* get_address_of_mLastAppliedLeftVerticalVirtualFoV_20() { return &___mLastAppliedLeftVerticalVirtualFoV_20; }
	inline void set_mLastAppliedLeftVerticalVirtualFoV_20(float value)
	{
		___mLastAppliedLeftVerticalVirtualFoV_20 = value;
	}

	inline static int32_t get_offset_of_mLastAppliedLeftHorizontalVirtualFoV_21() { return static_cast<int32_t>(offsetof(ExternalStereoCameraConfiguration_t3079436327, ___mLastAppliedLeftHorizontalVirtualFoV_21)); }
	inline float get_mLastAppliedLeftHorizontalVirtualFoV_21() const { return ___mLastAppliedLeftHorizontalVirtualFoV_21; }
	inline float* get_address_of_mLastAppliedLeftHorizontalVirtualFoV_21() { return &___mLastAppliedLeftHorizontalVirtualFoV_21; }
	inline void set_mLastAppliedLeftHorizontalVirtualFoV_21(float value)
	{
		___mLastAppliedLeftHorizontalVirtualFoV_21 = value;
	}

	inline static int32_t get_offset_of_mLastAppliedRightVerticalVirtualFoV_22() { return static_cast<int32_t>(offsetof(ExternalStereoCameraConfiguration_t3079436327, ___mLastAppliedRightVerticalVirtualFoV_22)); }
	inline float get_mLastAppliedRightVerticalVirtualFoV_22() const { return ___mLastAppliedRightVerticalVirtualFoV_22; }
	inline float* get_address_of_mLastAppliedRightVerticalVirtualFoV_22() { return &___mLastAppliedRightVerticalVirtualFoV_22; }
	inline void set_mLastAppliedRightVerticalVirtualFoV_22(float value)
	{
		___mLastAppliedRightVerticalVirtualFoV_22 = value;
	}

	inline static int32_t get_offset_of_mLastAppliedRightHorizontalVirtualFoV_23() { return static_cast<int32_t>(offsetof(ExternalStereoCameraConfiguration_t3079436327, ___mLastAppliedRightHorizontalVirtualFoV_23)); }
	inline float get_mLastAppliedRightHorizontalVirtualFoV_23() const { return ___mLastAppliedRightHorizontalVirtualFoV_23; }
	inline float* get_address_of_mLastAppliedRightHorizontalVirtualFoV_23() { return &___mLastAppliedRightHorizontalVirtualFoV_23; }
	inline void set_mLastAppliedRightHorizontalVirtualFoV_23(float value)
	{
		___mLastAppliedRightHorizontalVirtualFoV_23 = value;
	}

	inline static int32_t get_offset_of_mLastAppliedLeftProjection_24() { return static_cast<int32_t>(offsetof(ExternalStereoCameraConfiguration_t3079436327, ___mLastAppliedLeftProjection_24)); }
	inline Matrix4x4_t4266809202  get_mLastAppliedLeftProjection_24() const { return ___mLastAppliedLeftProjection_24; }
	inline Matrix4x4_t4266809202 * get_address_of_mLastAppliedLeftProjection_24() { return &___mLastAppliedLeftProjection_24; }
	inline void set_mLastAppliedLeftProjection_24(Matrix4x4_t4266809202  value)
	{
		___mLastAppliedLeftProjection_24 = value;
	}

	inline static int32_t get_offset_of_mLastAppliedRightProjection_25() { return static_cast<int32_t>(offsetof(ExternalStereoCameraConfiguration_t3079436327, ___mLastAppliedRightProjection_25)); }
	inline Matrix4x4_t4266809202  get_mLastAppliedRightProjection_25() const { return ___mLastAppliedRightProjection_25; }
	inline Matrix4x4_t4266809202 * get_address_of_mLastAppliedRightProjection_25() { return &___mLastAppliedRightProjection_25; }
	inline void set_mLastAppliedRightProjection_25(Matrix4x4_t4266809202  value)
	{
		___mLastAppliedRightProjection_25 = value;
	}

	inline static int32_t get_offset_of_mNewLeftNearClipPlane_26() { return static_cast<int32_t>(offsetof(ExternalStereoCameraConfiguration_t3079436327, ___mNewLeftNearClipPlane_26)); }
	inline float get_mNewLeftNearClipPlane_26() const { return ___mNewLeftNearClipPlane_26; }
	inline float* get_address_of_mNewLeftNearClipPlane_26() { return &___mNewLeftNearClipPlane_26; }
	inline void set_mNewLeftNearClipPlane_26(float value)
	{
		___mNewLeftNearClipPlane_26 = value;
	}

	inline static int32_t get_offset_of_mNewLeftFarClipPlane_27() { return static_cast<int32_t>(offsetof(ExternalStereoCameraConfiguration_t3079436327, ___mNewLeftFarClipPlane_27)); }
	inline float get_mNewLeftFarClipPlane_27() const { return ___mNewLeftFarClipPlane_27; }
	inline float* get_address_of_mNewLeftFarClipPlane_27() { return &___mNewLeftFarClipPlane_27; }
	inline void set_mNewLeftFarClipPlane_27(float value)
	{
		___mNewLeftFarClipPlane_27 = value;
	}

	inline static int32_t get_offset_of_mNewRightNearClipPlane_28() { return static_cast<int32_t>(offsetof(ExternalStereoCameraConfiguration_t3079436327, ___mNewRightNearClipPlane_28)); }
	inline float get_mNewRightNearClipPlane_28() const { return ___mNewRightNearClipPlane_28; }
	inline float* get_address_of_mNewRightNearClipPlane_28() { return &___mNewRightNearClipPlane_28; }
	inline void set_mNewRightNearClipPlane_28(float value)
	{
		___mNewRightNearClipPlane_28 = value;
	}

	inline static int32_t get_offset_of_mNewRightFarClipPlane_29() { return static_cast<int32_t>(offsetof(ExternalStereoCameraConfiguration_t3079436327, ___mNewRightFarClipPlane_29)); }
	inline float get_mNewRightFarClipPlane_29() const { return ___mNewRightFarClipPlane_29; }
	inline float* get_address_of_mNewRightFarClipPlane_29() { return &___mNewRightFarClipPlane_29; }
	inline void set_mNewRightFarClipPlane_29(float value)
	{
		___mNewRightFarClipPlane_29 = value;
	}

	inline static int32_t get_offset_of_mNewLeftVerticalVirtualFoV_30() { return static_cast<int32_t>(offsetof(ExternalStereoCameraConfiguration_t3079436327, ___mNewLeftVerticalVirtualFoV_30)); }
	inline float get_mNewLeftVerticalVirtualFoV_30() const { return ___mNewLeftVerticalVirtualFoV_30; }
	inline float* get_address_of_mNewLeftVerticalVirtualFoV_30() { return &___mNewLeftVerticalVirtualFoV_30; }
	inline void set_mNewLeftVerticalVirtualFoV_30(float value)
	{
		___mNewLeftVerticalVirtualFoV_30 = value;
	}

	inline static int32_t get_offset_of_mNewLeftHorizontalVirtualFoV_31() { return static_cast<int32_t>(offsetof(ExternalStereoCameraConfiguration_t3079436327, ___mNewLeftHorizontalVirtualFoV_31)); }
	inline float get_mNewLeftHorizontalVirtualFoV_31() const { return ___mNewLeftHorizontalVirtualFoV_31; }
	inline float* get_address_of_mNewLeftHorizontalVirtualFoV_31() { return &___mNewLeftHorizontalVirtualFoV_31; }
	inline void set_mNewLeftHorizontalVirtualFoV_31(float value)
	{
		___mNewLeftHorizontalVirtualFoV_31 = value;
	}

	inline static int32_t get_offset_of_mNewRightVerticalVirtualFoV_32() { return static_cast<int32_t>(offsetof(ExternalStereoCameraConfiguration_t3079436327, ___mNewRightVerticalVirtualFoV_32)); }
	inline float get_mNewRightVerticalVirtualFoV_32() const { return ___mNewRightVerticalVirtualFoV_32; }
	inline float* get_address_of_mNewRightVerticalVirtualFoV_32() { return &___mNewRightVerticalVirtualFoV_32; }
	inline void set_mNewRightVerticalVirtualFoV_32(float value)
	{
		___mNewRightVerticalVirtualFoV_32 = value;
	}

	inline static int32_t get_offset_of_mNewRightHorizontalVirtualFoV_33() { return static_cast<int32_t>(offsetof(ExternalStereoCameraConfiguration_t3079436327, ___mNewRightHorizontalVirtualFoV_33)); }
	inline float get_mNewRightHorizontalVirtualFoV_33() const { return ___mNewRightHorizontalVirtualFoV_33; }
	inline float* get_address_of_mNewRightHorizontalVirtualFoV_33() { return &___mNewRightHorizontalVirtualFoV_33; }
	inline void set_mNewRightHorizontalVirtualFoV_33(float value)
	{
		___mNewRightHorizontalVirtualFoV_33 = value;
	}

	inline static int32_t get_offset_of_mExternallySetLeftMatrix_34() { return static_cast<int32_t>(offsetof(ExternalStereoCameraConfiguration_t3079436327, ___mExternallySetLeftMatrix_34)); }
	inline Matrix4x4_t4266809202  get_mExternallySetLeftMatrix_34() const { return ___mExternallySetLeftMatrix_34; }
	inline Matrix4x4_t4266809202 * get_address_of_mExternallySetLeftMatrix_34() { return &___mExternallySetLeftMatrix_34; }
	inline void set_mExternallySetLeftMatrix_34(Matrix4x4_t4266809202  value)
	{
		___mExternallySetLeftMatrix_34 = value;
	}

	inline static int32_t get_offset_of_mExternallySetRightMatrix_35() { return static_cast<int32_t>(offsetof(ExternalStereoCameraConfiguration_t3079436327, ___mExternallySetRightMatrix_35)); }
	inline Matrix4x4_t4266809202  get_mExternallySetRightMatrix_35() const { return ___mExternallySetRightMatrix_35; }
	inline Matrix4x4_t4266809202 * get_address_of_mExternallySetRightMatrix_35() { return &___mExternallySetRightMatrix_35; }
	inline void set_mExternallySetRightMatrix_35(Matrix4x4_t4266809202  value)
	{
		___mExternallySetRightMatrix_35 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXTERNALSTEREOCAMERACONFIGURATION_T3079436327_H
#ifndef TRACKABLEBEHAVIOUR_T2419079356_H
#define TRACKABLEBEHAVIOUR_T2419079356_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.TrackableBehaviour
struct  TrackableBehaviour_t2419079356  : public MonoBehaviour_t345688271
{
public:
	// System.Double Vuforia.TrackableBehaviour::<TimeStamp>k__BackingField
	double ___U3CTimeStampU3Ek__BackingField_2;
	// System.String Vuforia.TrackableBehaviour::mTrackableName
	String_t* ___mTrackableName_3;
	// System.Boolean Vuforia.TrackableBehaviour::mPreserveChildSize
	bool ___mPreserveChildSize_4;
	// System.Boolean Vuforia.TrackableBehaviour::mInitializedInEditor
	bool ___mInitializedInEditor_5;
	// UnityEngine.Vector3 Vuforia.TrackableBehaviour::mPreviousScale
	Vector3_t2903530434  ___mPreviousScale_6;
	// Vuforia.TrackableBehaviour/Status Vuforia.TrackableBehaviour::mStatus
	int32_t ___mStatus_7;
	// Vuforia.Trackable Vuforia.TrackableBehaviour::mTrackable
	RuntimeObject* ___mTrackable_8;
	// System.Collections.Generic.List`1<Vuforia.ITrackableEventHandler> Vuforia.TrackableBehaviour::mTrackableEventHandlers
	List_1_t3589236026 * ___mTrackableEventHandlers_9;

public:
	inline static int32_t get_offset_of_U3CTimeStampU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(TrackableBehaviour_t2419079356, ___U3CTimeStampU3Ek__BackingField_2)); }
	inline double get_U3CTimeStampU3Ek__BackingField_2() const { return ___U3CTimeStampU3Ek__BackingField_2; }
	inline double* get_address_of_U3CTimeStampU3Ek__BackingField_2() { return &___U3CTimeStampU3Ek__BackingField_2; }
	inline void set_U3CTimeStampU3Ek__BackingField_2(double value)
	{
		___U3CTimeStampU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_mTrackableName_3() { return static_cast<int32_t>(offsetof(TrackableBehaviour_t2419079356, ___mTrackableName_3)); }
	inline String_t* get_mTrackableName_3() const { return ___mTrackableName_3; }
	inline String_t** get_address_of_mTrackableName_3() { return &___mTrackableName_3; }
	inline void set_mTrackableName_3(String_t* value)
	{
		___mTrackableName_3 = value;
		Il2CppCodeGenWriteBarrier((&___mTrackableName_3), value);
	}

	inline static int32_t get_offset_of_mPreserveChildSize_4() { return static_cast<int32_t>(offsetof(TrackableBehaviour_t2419079356, ___mPreserveChildSize_4)); }
	inline bool get_mPreserveChildSize_4() const { return ___mPreserveChildSize_4; }
	inline bool* get_address_of_mPreserveChildSize_4() { return &___mPreserveChildSize_4; }
	inline void set_mPreserveChildSize_4(bool value)
	{
		___mPreserveChildSize_4 = value;
	}

	inline static int32_t get_offset_of_mInitializedInEditor_5() { return static_cast<int32_t>(offsetof(TrackableBehaviour_t2419079356, ___mInitializedInEditor_5)); }
	inline bool get_mInitializedInEditor_5() const { return ___mInitializedInEditor_5; }
	inline bool* get_address_of_mInitializedInEditor_5() { return &___mInitializedInEditor_5; }
	inline void set_mInitializedInEditor_5(bool value)
	{
		___mInitializedInEditor_5 = value;
	}

	inline static int32_t get_offset_of_mPreviousScale_6() { return static_cast<int32_t>(offsetof(TrackableBehaviour_t2419079356, ___mPreviousScale_6)); }
	inline Vector3_t2903530434  get_mPreviousScale_6() const { return ___mPreviousScale_6; }
	inline Vector3_t2903530434 * get_address_of_mPreviousScale_6() { return &___mPreviousScale_6; }
	inline void set_mPreviousScale_6(Vector3_t2903530434  value)
	{
		___mPreviousScale_6 = value;
	}

	inline static int32_t get_offset_of_mStatus_7() { return static_cast<int32_t>(offsetof(TrackableBehaviour_t2419079356, ___mStatus_7)); }
	inline int32_t get_mStatus_7() const { return ___mStatus_7; }
	inline int32_t* get_address_of_mStatus_7() { return &___mStatus_7; }
	inline void set_mStatus_7(int32_t value)
	{
		___mStatus_7 = value;
	}

	inline static int32_t get_offset_of_mTrackable_8() { return static_cast<int32_t>(offsetof(TrackableBehaviour_t2419079356, ___mTrackable_8)); }
	inline RuntimeObject* get_mTrackable_8() const { return ___mTrackable_8; }
	inline RuntimeObject** get_address_of_mTrackable_8() { return &___mTrackable_8; }
	inline void set_mTrackable_8(RuntimeObject* value)
	{
		___mTrackable_8 = value;
		Il2CppCodeGenWriteBarrier((&___mTrackable_8), value);
	}

	inline static int32_t get_offset_of_mTrackableEventHandlers_9() { return static_cast<int32_t>(offsetof(TrackableBehaviour_t2419079356, ___mTrackableEventHandlers_9)); }
	inline List_1_t3589236026 * get_mTrackableEventHandlers_9() const { return ___mTrackableEventHandlers_9; }
	inline List_1_t3589236026 ** get_address_of_mTrackableEventHandlers_9() { return &___mTrackableEventHandlers_9; }
	inline void set_mTrackableEventHandlers_9(List_1_t3589236026 * value)
	{
		___mTrackableEventHandlers_9 = value;
		Il2CppCodeGenWriteBarrier((&___mTrackableEventHandlers_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRACKABLEBEHAVIOUR_T2419079356_H
#ifndef DISTORTIONRENDERINGBEHAVIOUR_T2785666256_H
#define DISTORTIONRENDERINGBEHAVIOUR_T2785666256_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.DistortionRenderingBehaviour
struct  DistortionRenderingBehaviour_t2785666256  : public MonoBehaviour_t345688271
{
public:
	// System.Boolean Vuforia.DistortionRenderingBehaviour::mSingleTexture
	bool ___mSingleTexture_2;
	// System.Int32 Vuforia.DistortionRenderingBehaviour::mRenderLayer
	int32_t ___mRenderLayer_3;
	// System.Int32[] Vuforia.DistortionRenderingBehaviour::mOriginalCullingMasks
	Int32U5BU5D_t2324750880* ___mOriginalCullingMasks_4;
	// UnityEngine.Camera[] Vuforia.DistortionRenderingBehaviour::mStereoCameras
	CameraU5BU5D_t1519774542* ___mStereoCameras_5;
	// UnityEngine.GameObject[] Vuforia.DistortionRenderingBehaviour::mMeshes
	GameObjectU5BU5D_t2021098027* ___mMeshes_6;
	// UnityEngine.RenderTexture[] Vuforia.DistortionRenderingBehaviour::mTextures
	RenderTextureU5BU5D_t4230059505* ___mTextures_7;
	// System.Boolean Vuforia.DistortionRenderingBehaviour::mStarted
	bool ___mStarted_8;
	// System.Boolean Vuforia.DistortionRenderingBehaviour::mVideoBackgroundChanged
	bool ___mVideoBackgroundChanged_9;
	// UnityEngine.Rect Vuforia.DistortionRenderingBehaviour::mOriginalLeftViewport
	Rect_t3436776195  ___mOriginalLeftViewport_10;
	// UnityEngine.Rect Vuforia.DistortionRenderingBehaviour::mOriginalRightViewport
	Rect_t3436776195  ___mOriginalRightViewport_11;
	// UnityEngine.Rect Vuforia.DistortionRenderingBehaviour::mDualTextureLeftViewport
	Rect_t3436776195  ___mDualTextureLeftViewport_12;
	// UnityEngine.Rect Vuforia.DistortionRenderingBehaviour::mDualTextureRightViewport
	Rect_t3436776195  ___mDualTextureRightViewport_13;

public:
	inline static int32_t get_offset_of_mSingleTexture_2() { return static_cast<int32_t>(offsetof(DistortionRenderingBehaviour_t2785666256, ___mSingleTexture_2)); }
	inline bool get_mSingleTexture_2() const { return ___mSingleTexture_2; }
	inline bool* get_address_of_mSingleTexture_2() { return &___mSingleTexture_2; }
	inline void set_mSingleTexture_2(bool value)
	{
		___mSingleTexture_2 = value;
	}

	inline static int32_t get_offset_of_mRenderLayer_3() { return static_cast<int32_t>(offsetof(DistortionRenderingBehaviour_t2785666256, ___mRenderLayer_3)); }
	inline int32_t get_mRenderLayer_3() const { return ___mRenderLayer_3; }
	inline int32_t* get_address_of_mRenderLayer_3() { return &___mRenderLayer_3; }
	inline void set_mRenderLayer_3(int32_t value)
	{
		___mRenderLayer_3 = value;
	}

	inline static int32_t get_offset_of_mOriginalCullingMasks_4() { return static_cast<int32_t>(offsetof(DistortionRenderingBehaviour_t2785666256, ___mOriginalCullingMasks_4)); }
	inline Int32U5BU5D_t2324750880* get_mOriginalCullingMasks_4() const { return ___mOriginalCullingMasks_4; }
	inline Int32U5BU5D_t2324750880** get_address_of_mOriginalCullingMasks_4() { return &___mOriginalCullingMasks_4; }
	inline void set_mOriginalCullingMasks_4(Int32U5BU5D_t2324750880* value)
	{
		___mOriginalCullingMasks_4 = value;
		Il2CppCodeGenWriteBarrier((&___mOriginalCullingMasks_4), value);
	}

	inline static int32_t get_offset_of_mStereoCameras_5() { return static_cast<int32_t>(offsetof(DistortionRenderingBehaviour_t2785666256, ___mStereoCameras_5)); }
	inline CameraU5BU5D_t1519774542* get_mStereoCameras_5() const { return ___mStereoCameras_5; }
	inline CameraU5BU5D_t1519774542** get_address_of_mStereoCameras_5() { return &___mStereoCameras_5; }
	inline void set_mStereoCameras_5(CameraU5BU5D_t1519774542* value)
	{
		___mStereoCameras_5 = value;
		Il2CppCodeGenWriteBarrier((&___mStereoCameras_5), value);
	}

	inline static int32_t get_offset_of_mMeshes_6() { return static_cast<int32_t>(offsetof(DistortionRenderingBehaviour_t2785666256, ___mMeshes_6)); }
	inline GameObjectU5BU5D_t2021098027* get_mMeshes_6() const { return ___mMeshes_6; }
	inline GameObjectU5BU5D_t2021098027** get_address_of_mMeshes_6() { return &___mMeshes_6; }
	inline void set_mMeshes_6(GameObjectU5BU5D_t2021098027* value)
	{
		___mMeshes_6 = value;
		Il2CppCodeGenWriteBarrier((&___mMeshes_6), value);
	}

	inline static int32_t get_offset_of_mTextures_7() { return static_cast<int32_t>(offsetof(DistortionRenderingBehaviour_t2785666256, ___mTextures_7)); }
	inline RenderTextureU5BU5D_t4230059505* get_mTextures_7() const { return ___mTextures_7; }
	inline RenderTextureU5BU5D_t4230059505** get_address_of_mTextures_7() { return &___mTextures_7; }
	inline void set_mTextures_7(RenderTextureU5BU5D_t4230059505* value)
	{
		___mTextures_7 = value;
		Il2CppCodeGenWriteBarrier((&___mTextures_7), value);
	}

	inline static int32_t get_offset_of_mStarted_8() { return static_cast<int32_t>(offsetof(DistortionRenderingBehaviour_t2785666256, ___mStarted_8)); }
	inline bool get_mStarted_8() const { return ___mStarted_8; }
	inline bool* get_address_of_mStarted_8() { return &___mStarted_8; }
	inline void set_mStarted_8(bool value)
	{
		___mStarted_8 = value;
	}

	inline static int32_t get_offset_of_mVideoBackgroundChanged_9() { return static_cast<int32_t>(offsetof(DistortionRenderingBehaviour_t2785666256, ___mVideoBackgroundChanged_9)); }
	inline bool get_mVideoBackgroundChanged_9() const { return ___mVideoBackgroundChanged_9; }
	inline bool* get_address_of_mVideoBackgroundChanged_9() { return &___mVideoBackgroundChanged_9; }
	inline void set_mVideoBackgroundChanged_9(bool value)
	{
		___mVideoBackgroundChanged_9 = value;
	}

	inline static int32_t get_offset_of_mOriginalLeftViewport_10() { return static_cast<int32_t>(offsetof(DistortionRenderingBehaviour_t2785666256, ___mOriginalLeftViewport_10)); }
	inline Rect_t3436776195  get_mOriginalLeftViewport_10() const { return ___mOriginalLeftViewport_10; }
	inline Rect_t3436776195 * get_address_of_mOriginalLeftViewport_10() { return &___mOriginalLeftViewport_10; }
	inline void set_mOriginalLeftViewport_10(Rect_t3436776195  value)
	{
		___mOriginalLeftViewport_10 = value;
	}

	inline static int32_t get_offset_of_mOriginalRightViewport_11() { return static_cast<int32_t>(offsetof(DistortionRenderingBehaviour_t2785666256, ___mOriginalRightViewport_11)); }
	inline Rect_t3436776195  get_mOriginalRightViewport_11() const { return ___mOriginalRightViewport_11; }
	inline Rect_t3436776195 * get_address_of_mOriginalRightViewport_11() { return &___mOriginalRightViewport_11; }
	inline void set_mOriginalRightViewport_11(Rect_t3436776195  value)
	{
		___mOriginalRightViewport_11 = value;
	}

	inline static int32_t get_offset_of_mDualTextureLeftViewport_12() { return static_cast<int32_t>(offsetof(DistortionRenderingBehaviour_t2785666256, ___mDualTextureLeftViewport_12)); }
	inline Rect_t3436776195  get_mDualTextureLeftViewport_12() const { return ___mDualTextureLeftViewport_12; }
	inline Rect_t3436776195 * get_address_of_mDualTextureLeftViewport_12() { return &___mDualTextureLeftViewport_12; }
	inline void set_mDualTextureLeftViewport_12(Rect_t3436776195  value)
	{
		___mDualTextureLeftViewport_12 = value;
	}

	inline static int32_t get_offset_of_mDualTextureRightViewport_13() { return static_cast<int32_t>(offsetof(DistortionRenderingBehaviour_t2785666256, ___mDualTextureRightViewport_13)); }
	inline Rect_t3436776195  get_mDualTextureRightViewport_13() const { return ___mDualTextureRightViewport_13; }
	inline Rect_t3436776195 * get_address_of_mDualTextureRightViewport_13() { return &___mDualTextureRightViewport_13; }
	inline void set_mDualTextureRightViewport_13(Rect_t3436776195  value)
	{
		___mDualTextureRightViewport_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DISTORTIONRENDERINGBEHAVIOUR_T2785666256_H
#ifndef UIBEHAVIOUR_T3858904981_H
#define UIBEHAVIOUR_T3858904981_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.UIBehaviour
struct  UIBehaviour_t3858904981  : public MonoBehaviour_t345688271
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIBEHAVIOUR_T3858904981_H
#ifndef CLOUDRECOABSTRACTBEHAVIOUR_T2501456771_H
#define CLOUDRECOABSTRACTBEHAVIOUR_T2501456771_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.CloudRecoAbstractBehaviour
struct  CloudRecoAbstractBehaviour_t2501456771  : public MonoBehaviour_t345688271
{
public:
	// Vuforia.ObjectTracker Vuforia.CloudRecoAbstractBehaviour::mObjectTracker
	ObjectTracker_t4023638979 * ___mObjectTracker_2;
	// System.Boolean Vuforia.CloudRecoAbstractBehaviour::mCurrentlyInitializing
	bool ___mCurrentlyInitializing_3;
	// System.Boolean Vuforia.CloudRecoAbstractBehaviour::mInitSuccess
	bool ___mInitSuccess_4;
	// System.Boolean Vuforia.CloudRecoAbstractBehaviour::mCloudRecoStarted
	bool ___mCloudRecoStarted_5;
	// System.Boolean Vuforia.CloudRecoAbstractBehaviour::mOnInitializedCalled
	bool ___mOnInitializedCalled_6;
	// System.Collections.Generic.List`1<Vuforia.ICloudRecoEventHandler> Vuforia.CloudRecoAbstractBehaviour::mHandlers
	List_1_t1465274546 * ___mHandlers_7;
	// System.Boolean Vuforia.CloudRecoAbstractBehaviour::mTargetFinderStartedBeforeDisable
	bool ___mTargetFinderStartedBeforeDisable_8;
	// System.String Vuforia.CloudRecoAbstractBehaviour::AccessKey
	String_t* ___AccessKey_9;
	// System.String Vuforia.CloudRecoAbstractBehaviour::SecretKey
	String_t* ___SecretKey_10;

public:
	inline static int32_t get_offset_of_mObjectTracker_2() { return static_cast<int32_t>(offsetof(CloudRecoAbstractBehaviour_t2501456771, ___mObjectTracker_2)); }
	inline ObjectTracker_t4023638979 * get_mObjectTracker_2() const { return ___mObjectTracker_2; }
	inline ObjectTracker_t4023638979 ** get_address_of_mObjectTracker_2() { return &___mObjectTracker_2; }
	inline void set_mObjectTracker_2(ObjectTracker_t4023638979 * value)
	{
		___mObjectTracker_2 = value;
		Il2CppCodeGenWriteBarrier((&___mObjectTracker_2), value);
	}

	inline static int32_t get_offset_of_mCurrentlyInitializing_3() { return static_cast<int32_t>(offsetof(CloudRecoAbstractBehaviour_t2501456771, ___mCurrentlyInitializing_3)); }
	inline bool get_mCurrentlyInitializing_3() const { return ___mCurrentlyInitializing_3; }
	inline bool* get_address_of_mCurrentlyInitializing_3() { return &___mCurrentlyInitializing_3; }
	inline void set_mCurrentlyInitializing_3(bool value)
	{
		___mCurrentlyInitializing_3 = value;
	}

	inline static int32_t get_offset_of_mInitSuccess_4() { return static_cast<int32_t>(offsetof(CloudRecoAbstractBehaviour_t2501456771, ___mInitSuccess_4)); }
	inline bool get_mInitSuccess_4() const { return ___mInitSuccess_4; }
	inline bool* get_address_of_mInitSuccess_4() { return &___mInitSuccess_4; }
	inline void set_mInitSuccess_4(bool value)
	{
		___mInitSuccess_4 = value;
	}

	inline static int32_t get_offset_of_mCloudRecoStarted_5() { return static_cast<int32_t>(offsetof(CloudRecoAbstractBehaviour_t2501456771, ___mCloudRecoStarted_5)); }
	inline bool get_mCloudRecoStarted_5() const { return ___mCloudRecoStarted_5; }
	inline bool* get_address_of_mCloudRecoStarted_5() { return &___mCloudRecoStarted_5; }
	inline void set_mCloudRecoStarted_5(bool value)
	{
		___mCloudRecoStarted_5 = value;
	}

	inline static int32_t get_offset_of_mOnInitializedCalled_6() { return static_cast<int32_t>(offsetof(CloudRecoAbstractBehaviour_t2501456771, ___mOnInitializedCalled_6)); }
	inline bool get_mOnInitializedCalled_6() const { return ___mOnInitializedCalled_6; }
	inline bool* get_address_of_mOnInitializedCalled_6() { return &___mOnInitializedCalled_6; }
	inline void set_mOnInitializedCalled_6(bool value)
	{
		___mOnInitializedCalled_6 = value;
	}

	inline static int32_t get_offset_of_mHandlers_7() { return static_cast<int32_t>(offsetof(CloudRecoAbstractBehaviour_t2501456771, ___mHandlers_7)); }
	inline List_1_t1465274546 * get_mHandlers_7() const { return ___mHandlers_7; }
	inline List_1_t1465274546 ** get_address_of_mHandlers_7() { return &___mHandlers_7; }
	inline void set_mHandlers_7(List_1_t1465274546 * value)
	{
		___mHandlers_7 = value;
		Il2CppCodeGenWriteBarrier((&___mHandlers_7), value);
	}

	inline static int32_t get_offset_of_mTargetFinderStartedBeforeDisable_8() { return static_cast<int32_t>(offsetof(CloudRecoAbstractBehaviour_t2501456771, ___mTargetFinderStartedBeforeDisable_8)); }
	inline bool get_mTargetFinderStartedBeforeDisable_8() const { return ___mTargetFinderStartedBeforeDisable_8; }
	inline bool* get_address_of_mTargetFinderStartedBeforeDisable_8() { return &___mTargetFinderStartedBeforeDisable_8; }
	inline void set_mTargetFinderStartedBeforeDisable_8(bool value)
	{
		___mTargetFinderStartedBeforeDisable_8 = value;
	}

	inline static int32_t get_offset_of_AccessKey_9() { return static_cast<int32_t>(offsetof(CloudRecoAbstractBehaviour_t2501456771, ___AccessKey_9)); }
	inline String_t* get_AccessKey_9() const { return ___AccessKey_9; }
	inline String_t** get_address_of_AccessKey_9() { return &___AccessKey_9; }
	inline void set_AccessKey_9(String_t* value)
	{
		___AccessKey_9 = value;
		Il2CppCodeGenWriteBarrier((&___AccessKey_9), value);
	}

	inline static int32_t get_offset_of_SecretKey_10() { return static_cast<int32_t>(offsetof(CloudRecoAbstractBehaviour_t2501456771, ___SecretKey_10)); }
	inline String_t* get_SecretKey_10() const { return ___SecretKey_10; }
	inline String_t** get_address_of_SecretKey_10() { return &___SecretKey_10; }
	inline void set_SecretKey_10(String_t* value)
	{
		___SecretKey_10 = value;
		Il2CppCodeGenWriteBarrier((&___SecretKey_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CLOUDRECOABSTRACTBEHAVIOUR_T2501456771_H
#ifndef HIDEEXCESSAREAABSTRACTBEHAVIOUR_T1324413388_H
#define HIDEEXCESSAREAABSTRACTBEHAVIOUR_T1324413388_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.HideExcessAreaAbstractBehaviour
struct  HideExcessAreaAbstractBehaviour_t1324413388  : public MonoBehaviour_t345688271
{
public:
	// Vuforia.IExcessAreaClipping Vuforia.HideExcessAreaAbstractBehaviour::mClippingImpl
	RuntimeObject* ___mClippingImpl_2;
	// Vuforia.HideExcessAreaAbstractBehaviour/CLIPPING_MODE Vuforia.HideExcessAreaAbstractBehaviour::mClippingMode
	int32_t ___mClippingMode_3;
	// Vuforia.VuforiaARController Vuforia.HideExcessAreaAbstractBehaviour::mVuforiaBehaviour
	VuforiaARController_t1328503142 * ___mVuforiaBehaviour_4;
	// Vuforia.VideoBackgroundManager Vuforia.HideExcessAreaAbstractBehaviour::mVideoBgMgr
	VideoBackgroundManager_t2068552549 * ___mVideoBgMgr_5;
	// UnityEngine.Vector3 Vuforia.HideExcessAreaAbstractBehaviour::mPlaneOffset
	Vector3_t2903530434  ___mPlaneOffset_6;
	// System.Boolean Vuforia.HideExcessAreaAbstractBehaviour::mSceneScaledDown
	bool ___mSceneScaledDown_7;
	// System.Boolean Vuforia.HideExcessAreaAbstractBehaviour::mStarted
	bool ___mStarted_8;

public:
	inline static int32_t get_offset_of_mClippingImpl_2() { return static_cast<int32_t>(offsetof(HideExcessAreaAbstractBehaviour_t1324413388, ___mClippingImpl_2)); }
	inline RuntimeObject* get_mClippingImpl_2() const { return ___mClippingImpl_2; }
	inline RuntimeObject** get_address_of_mClippingImpl_2() { return &___mClippingImpl_2; }
	inline void set_mClippingImpl_2(RuntimeObject* value)
	{
		___mClippingImpl_2 = value;
		Il2CppCodeGenWriteBarrier((&___mClippingImpl_2), value);
	}

	inline static int32_t get_offset_of_mClippingMode_3() { return static_cast<int32_t>(offsetof(HideExcessAreaAbstractBehaviour_t1324413388, ___mClippingMode_3)); }
	inline int32_t get_mClippingMode_3() const { return ___mClippingMode_3; }
	inline int32_t* get_address_of_mClippingMode_3() { return &___mClippingMode_3; }
	inline void set_mClippingMode_3(int32_t value)
	{
		___mClippingMode_3 = value;
	}

	inline static int32_t get_offset_of_mVuforiaBehaviour_4() { return static_cast<int32_t>(offsetof(HideExcessAreaAbstractBehaviour_t1324413388, ___mVuforiaBehaviour_4)); }
	inline VuforiaARController_t1328503142 * get_mVuforiaBehaviour_4() const { return ___mVuforiaBehaviour_4; }
	inline VuforiaARController_t1328503142 ** get_address_of_mVuforiaBehaviour_4() { return &___mVuforiaBehaviour_4; }
	inline void set_mVuforiaBehaviour_4(VuforiaARController_t1328503142 * value)
	{
		___mVuforiaBehaviour_4 = value;
		Il2CppCodeGenWriteBarrier((&___mVuforiaBehaviour_4), value);
	}

	inline static int32_t get_offset_of_mVideoBgMgr_5() { return static_cast<int32_t>(offsetof(HideExcessAreaAbstractBehaviour_t1324413388, ___mVideoBgMgr_5)); }
	inline VideoBackgroundManager_t2068552549 * get_mVideoBgMgr_5() const { return ___mVideoBgMgr_5; }
	inline VideoBackgroundManager_t2068552549 ** get_address_of_mVideoBgMgr_5() { return &___mVideoBgMgr_5; }
	inline void set_mVideoBgMgr_5(VideoBackgroundManager_t2068552549 * value)
	{
		___mVideoBgMgr_5 = value;
		Il2CppCodeGenWriteBarrier((&___mVideoBgMgr_5), value);
	}

	inline static int32_t get_offset_of_mPlaneOffset_6() { return static_cast<int32_t>(offsetof(HideExcessAreaAbstractBehaviour_t1324413388, ___mPlaneOffset_6)); }
	inline Vector3_t2903530434  get_mPlaneOffset_6() const { return ___mPlaneOffset_6; }
	inline Vector3_t2903530434 * get_address_of_mPlaneOffset_6() { return &___mPlaneOffset_6; }
	inline void set_mPlaneOffset_6(Vector3_t2903530434  value)
	{
		___mPlaneOffset_6 = value;
	}

	inline static int32_t get_offset_of_mSceneScaledDown_7() { return static_cast<int32_t>(offsetof(HideExcessAreaAbstractBehaviour_t1324413388, ___mSceneScaledDown_7)); }
	inline bool get_mSceneScaledDown_7() const { return ___mSceneScaledDown_7; }
	inline bool* get_address_of_mSceneScaledDown_7() { return &___mSceneScaledDown_7; }
	inline void set_mSceneScaledDown_7(bool value)
	{
		___mSceneScaledDown_7 = value;
	}

	inline static int32_t get_offset_of_mStarted_8() { return static_cast<int32_t>(offsetof(HideExcessAreaAbstractBehaviour_t1324413388, ___mStarted_8)); }
	inline bool get_mStarted_8() const { return ___mStarted_8; }
	inline bool* get_address_of_mStarted_8() { return &___mStarted_8; }
	inline void set_mStarted_8(bool value)
	{
		___mStarted_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HIDEEXCESSAREAABSTRACTBEHAVIOUR_T1324413388_H
#ifndef BACKGROUNDPLANEABSTRACTBEHAVIOUR_T4259835756_H
#define BACKGROUNDPLANEABSTRACTBEHAVIOUR_T4259835756_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.BackgroundPlaneAbstractBehaviour
struct  BackgroundPlaneAbstractBehaviour_t4259835756  : public MonoBehaviour_t345688271
{
public:
	// Vuforia.VuforiaRenderer/VideoTextureInfo Vuforia.BackgroundPlaneAbstractBehaviour::mTextureInfo
	VideoTextureInfo_t773013061  ___mTextureInfo_2;
	// System.Int32 Vuforia.BackgroundPlaneAbstractBehaviour::mViewWidth
	int32_t ___mViewWidth_3;
	// System.Int32 Vuforia.BackgroundPlaneAbstractBehaviour::mViewHeight
	int32_t ___mViewHeight_4;
	// System.Int32 Vuforia.BackgroundPlaneAbstractBehaviour::mNumFramesToUpdateVideoBg
	int32_t ___mNumFramesToUpdateVideoBg_5;
	// UnityEngine.Camera Vuforia.BackgroundPlaneAbstractBehaviour::mCamera
	Camera_t3175186167 * ___mCamera_6;
	// System.Int32 Vuforia.BackgroundPlaneAbstractBehaviour::defaultNumDivisions
	int32_t ___defaultNumDivisions_8;
	// UnityEngine.Mesh Vuforia.BackgroundPlaneAbstractBehaviour::mMesh
	Mesh_t996500909 * ___mMesh_9;
	// System.Single Vuforia.BackgroundPlaneAbstractBehaviour::mStereoDepth
	float ___mStereoDepth_10;
	// UnityEngine.Vector3 Vuforia.BackgroundPlaneAbstractBehaviour::mBackgroundOffset
	Vector3_t2903530434  ___mBackgroundOffset_11;
	// Vuforia.VuforiaARController Vuforia.BackgroundPlaneAbstractBehaviour::mVuforiaBehaviour
	VuforiaARController_t1328503142 * ___mVuforiaBehaviour_12;
	// System.Action Vuforia.BackgroundPlaneAbstractBehaviour::mBackgroundPlacedCallback
	Action_t3619184611 * ___mBackgroundPlacedCallback_13;
	// System.Int32 Vuforia.BackgroundPlaneAbstractBehaviour::mNumDivisions
	int32_t ___mNumDivisions_14;

public:
	inline static int32_t get_offset_of_mTextureInfo_2() { return static_cast<int32_t>(offsetof(BackgroundPlaneAbstractBehaviour_t4259835756, ___mTextureInfo_2)); }
	inline VideoTextureInfo_t773013061  get_mTextureInfo_2() const { return ___mTextureInfo_2; }
	inline VideoTextureInfo_t773013061 * get_address_of_mTextureInfo_2() { return &___mTextureInfo_2; }
	inline void set_mTextureInfo_2(VideoTextureInfo_t773013061  value)
	{
		___mTextureInfo_2 = value;
	}

	inline static int32_t get_offset_of_mViewWidth_3() { return static_cast<int32_t>(offsetof(BackgroundPlaneAbstractBehaviour_t4259835756, ___mViewWidth_3)); }
	inline int32_t get_mViewWidth_3() const { return ___mViewWidth_3; }
	inline int32_t* get_address_of_mViewWidth_3() { return &___mViewWidth_3; }
	inline void set_mViewWidth_3(int32_t value)
	{
		___mViewWidth_3 = value;
	}

	inline static int32_t get_offset_of_mViewHeight_4() { return static_cast<int32_t>(offsetof(BackgroundPlaneAbstractBehaviour_t4259835756, ___mViewHeight_4)); }
	inline int32_t get_mViewHeight_4() const { return ___mViewHeight_4; }
	inline int32_t* get_address_of_mViewHeight_4() { return &___mViewHeight_4; }
	inline void set_mViewHeight_4(int32_t value)
	{
		___mViewHeight_4 = value;
	}

	inline static int32_t get_offset_of_mNumFramesToUpdateVideoBg_5() { return static_cast<int32_t>(offsetof(BackgroundPlaneAbstractBehaviour_t4259835756, ___mNumFramesToUpdateVideoBg_5)); }
	inline int32_t get_mNumFramesToUpdateVideoBg_5() const { return ___mNumFramesToUpdateVideoBg_5; }
	inline int32_t* get_address_of_mNumFramesToUpdateVideoBg_5() { return &___mNumFramesToUpdateVideoBg_5; }
	inline void set_mNumFramesToUpdateVideoBg_5(int32_t value)
	{
		___mNumFramesToUpdateVideoBg_5 = value;
	}

	inline static int32_t get_offset_of_mCamera_6() { return static_cast<int32_t>(offsetof(BackgroundPlaneAbstractBehaviour_t4259835756, ___mCamera_6)); }
	inline Camera_t3175186167 * get_mCamera_6() const { return ___mCamera_6; }
	inline Camera_t3175186167 ** get_address_of_mCamera_6() { return &___mCamera_6; }
	inline void set_mCamera_6(Camera_t3175186167 * value)
	{
		___mCamera_6 = value;
		Il2CppCodeGenWriteBarrier((&___mCamera_6), value);
	}

	inline static int32_t get_offset_of_defaultNumDivisions_8() { return static_cast<int32_t>(offsetof(BackgroundPlaneAbstractBehaviour_t4259835756, ___defaultNumDivisions_8)); }
	inline int32_t get_defaultNumDivisions_8() const { return ___defaultNumDivisions_8; }
	inline int32_t* get_address_of_defaultNumDivisions_8() { return &___defaultNumDivisions_8; }
	inline void set_defaultNumDivisions_8(int32_t value)
	{
		___defaultNumDivisions_8 = value;
	}

	inline static int32_t get_offset_of_mMesh_9() { return static_cast<int32_t>(offsetof(BackgroundPlaneAbstractBehaviour_t4259835756, ___mMesh_9)); }
	inline Mesh_t996500909 * get_mMesh_9() const { return ___mMesh_9; }
	inline Mesh_t996500909 ** get_address_of_mMesh_9() { return &___mMesh_9; }
	inline void set_mMesh_9(Mesh_t996500909 * value)
	{
		___mMesh_9 = value;
		Il2CppCodeGenWriteBarrier((&___mMesh_9), value);
	}

	inline static int32_t get_offset_of_mStereoDepth_10() { return static_cast<int32_t>(offsetof(BackgroundPlaneAbstractBehaviour_t4259835756, ___mStereoDepth_10)); }
	inline float get_mStereoDepth_10() const { return ___mStereoDepth_10; }
	inline float* get_address_of_mStereoDepth_10() { return &___mStereoDepth_10; }
	inline void set_mStereoDepth_10(float value)
	{
		___mStereoDepth_10 = value;
	}

	inline static int32_t get_offset_of_mBackgroundOffset_11() { return static_cast<int32_t>(offsetof(BackgroundPlaneAbstractBehaviour_t4259835756, ___mBackgroundOffset_11)); }
	inline Vector3_t2903530434  get_mBackgroundOffset_11() const { return ___mBackgroundOffset_11; }
	inline Vector3_t2903530434 * get_address_of_mBackgroundOffset_11() { return &___mBackgroundOffset_11; }
	inline void set_mBackgroundOffset_11(Vector3_t2903530434  value)
	{
		___mBackgroundOffset_11 = value;
	}

	inline static int32_t get_offset_of_mVuforiaBehaviour_12() { return static_cast<int32_t>(offsetof(BackgroundPlaneAbstractBehaviour_t4259835756, ___mVuforiaBehaviour_12)); }
	inline VuforiaARController_t1328503142 * get_mVuforiaBehaviour_12() const { return ___mVuforiaBehaviour_12; }
	inline VuforiaARController_t1328503142 ** get_address_of_mVuforiaBehaviour_12() { return &___mVuforiaBehaviour_12; }
	inline void set_mVuforiaBehaviour_12(VuforiaARController_t1328503142 * value)
	{
		___mVuforiaBehaviour_12 = value;
		Il2CppCodeGenWriteBarrier((&___mVuforiaBehaviour_12), value);
	}

	inline static int32_t get_offset_of_mBackgroundPlacedCallback_13() { return static_cast<int32_t>(offsetof(BackgroundPlaneAbstractBehaviour_t4259835756, ___mBackgroundPlacedCallback_13)); }
	inline Action_t3619184611 * get_mBackgroundPlacedCallback_13() const { return ___mBackgroundPlacedCallback_13; }
	inline Action_t3619184611 ** get_address_of_mBackgroundPlacedCallback_13() { return &___mBackgroundPlacedCallback_13; }
	inline void set_mBackgroundPlacedCallback_13(Action_t3619184611 * value)
	{
		___mBackgroundPlacedCallback_13 = value;
		Il2CppCodeGenWriteBarrier((&___mBackgroundPlacedCallback_13), value);
	}

	inline static int32_t get_offset_of_mNumDivisions_14() { return static_cast<int32_t>(offsetof(BackgroundPlaneAbstractBehaviour_t4259835756, ___mNumDivisions_14)); }
	inline int32_t get_mNumDivisions_14() const { return ___mNumDivisions_14; }
	inline int32_t* get_address_of_mNumDivisions_14() { return &___mNumDivisions_14; }
	inline void set_mNumDivisions_14(int32_t value)
	{
		___mNumDivisions_14 = value;
	}
};

struct BackgroundPlaneAbstractBehaviour_t4259835756_StaticFields
{
public:
	// System.Single Vuforia.BackgroundPlaneAbstractBehaviour::maxDisplacement
	float ___maxDisplacement_7;

public:
	inline static int32_t get_offset_of_maxDisplacement_7() { return static_cast<int32_t>(offsetof(BackgroundPlaneAbstractBehaviour_t4259835756_StaticFields, ___maxDisplacement_7)); }
	inline float get_maxDisplacement_7() const { return ___maxDisplacement_7; }
	inline float* get_address_of_maxDisplacement_7() { return &___maxDisplacement_7; }
	inline void set_maxDisplacement_7(float value)
	{
		___maxDisplacement_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BACKGROUNDPLANEABSTRACTBEHAVIOUR_T4259835756_H
#ifndef DATASETTRACKABLEBEHAVIOUR_T3960979584_H
#define DATASETTRACKABLEBEHAVIOUR_T3960979584_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.DataSetTrackableBehaviour
struct  DataSetTrackableBehaviour_t3960979584  : public TrackableBehaviour_t2419079356
{
public:
	// System.String Vuforia.DataSetTrackableBehaviour::mDataSetPath
	String_t* ___mDataSetPath_10;
	// System.Boolean Vuforia.DataSetTrackableBehaviour::mExtendedTracking
	bool ___mExtendedTracking_11;
	// System.Boolean Vuforia.DataSetTrackableBehaviour::mInitializeSmartTerrain
	bool ___mInitializeSmartTerrain_12;
	// Vuforia.ReconstructionFromTargetAbstractBehaviour Vuforia.DataSetTrackableBehaviour::mReconstructionToInitialize
	ReconstructionFromTargetAbstractBehaviour_t2676804737 * ___mReconstructionToInitialize_13;
	// UnityEngine.Vector3 Vuforia.DataSetTrackableBehaviour::mSmartTerrainOccluderBoundsMin
	Vector3_t2903530434  ___mSmartTerrainOccluderBoundsMin_14;
	// UnityEngine.Vector3 Vuforia.DataSetTrackableBehaviour::mSmartTerrainOccluderBoundsMax
	Vector3_t2903530434  ___mSmartTerrainOccluderBoundsMax_15;
	// System.Boolean Vuforia.DataSetTrackableBehaviour::mIsSmartTerrainOccluderOffset
	bool ___mIsSmartTerrainOccluderOffset_16;
	// UnityEngine.Vector3 Vuforia.DataSetTrackableBehaviour::mSmartTerrainOccluderOffset
	Vector3_t2903530434  ___mSmartTerrainOccluderOffset_17;
	// UnityEngine.Quaternion Vuforia.DataSetTrackableBehaviour::mSmartTerrainOccluderRotation
	Quaternion_t754065749  ___mSmartTerrainOccluderRotation_18;
	// System.Boolean Vuforia.DataSetTrackableBehaviour::mAutoSetOccluderFromTargetSize
	bool ___mAutoSetOccluderFromTargetSize_19;

public:
	inline static int32_t get_offset_of_mDataSetPath_10() { return static_cast<int32_t>(offsetof(DataSetTrackableBehaviour_t3960979584, ___mDataSetPath_10)); }
	inline String_t* get_mDataSetPath_10() const { return ___mDataSetPath_10; }
	inline String_t** get_address_of_mDataSetPath_10() { return &___mDataSetPath_10; }
	inline void set_mDataSetPath_10(String_t* value)
	{
		___mDataSetPath_10 = value;
		Il2CppCodeGenWriteBarrier((&___mDataSetPath_10), value);
	}

	inline static int32_t get_offset_of_mExtendedTracking_11() { return static_cast<int32_t>(offsetof(DataSetTrackableBehaviour_t3960979584, ___mExtendedTracking_11)); }
	inline bool get_mExtendedTracking_11() const { return ___mExtendedTracking_11; }
	inline bool* get_address_of_mExtendedTracking_11() { return &___mExtendedTracking_11; }
	inline void set_mExtendedTracking_11(bool value)
	{
		___mExtendedTracking_11 = value;
	}

	inline static int32_t get_offset_of_mInitializeSmartTerrain_12() { return static_cast<int32_t>(offsetof(DataSetTrackableBehaviour_t3960979584, ___mInitializeSmartTerrain_12)); }
	inline bool get_mInitializeSmartTerrain_12() const { return ___mInitializeSmartTerrain_12; }
	inline bool* get_address_of_mInitializeSmartTerrain_12() { return &___mInitializeSmartTerrain_12; }
	inline void set_mInitializeSmartTerrain_12(bool value)
	{
		___mInitializeSmartTerrain_12 = value;
	}

	inline static int32_t get_offset_of_mReconstructionToInitialize_13() { return static_cast<int32_t>(offsetof(DataSetTrackableBehaviour_t3960979584, ___mReconstructionToInitialize_13)); }
	inline ReconstructionFromTargetAbstractBehaviour_t2676804737 * get_mReconstructionToInitialize_13() const { return ___mReconstructionToInitialize_13; }
	inline ReconstructionFromTargetAbstractBehaviour_t2676804737 ** get_address_of_mReconstructionToInitialize_13() { return &___mReconstructionToInitialize_13; }
	inline void set_mReconstructionToInitialize_13(ReconstructionFromTargetAbstractBehaviour_t2676804737 * value)
	{
		___mReconstructionToInitialize_13 = value;
		Il2CppCodeGenWriteBarrier((&___mReconstructionToInitialize_13), value);
	}

	inline static int32_t get_offset_of_mSmartTerrainOccluderBoundsMin_14() { return static_cast<int32_t>(offsetof(DataSetTrackableBehaviour_t3960979584, ___mSmartTerrainOccluderBoundsMin_14)); }
	inline Vector3_t2903530434  get_mSmartTerrainOccluderBoundsMin_14() const { return ___mSmartTerrainOccluderBoundsMin_14; }
	inline Vector3_t2903530434 * get_address_of_mSmartTerrainOccluderBoundsMin_14() { return &___mSmartTerrainOccluderBoundsMin_14; }
	inline void set_mSmartTerrainOccluderBoundsMin_14(Vector3_t2903530434  value)
	{
		___mSmartTerrainOccluderBoundsMin_14 = value;
	}

	inline static int32_t get_offset_of_mSmartTerrainOccluderBoundsMax_15() { return static_cast<int32_t>(offsetof(DataSetTrackableBehaviour_t3960979584, ___mSmartTerrainOccluderBoundsMax_15)); }
	inline Vector3_t2903530434  get_mSmartTerrainOccluderBoundsMax_15() const { return ___mSmartTerrainOccluderBoundsMax_15; }
	inline Vector3_t2903530434 * get_address_of_mSmartTerrainOccluderBoundsMax_15() { return &___mSmartTerrainOccluderBoundsMax_15; }
	inline void set_mSmartTerrainOccluderBoundsMax_15(Vector3_t2903530434  value)
	{
		___mSmartTerrainOccluderBoundsMax_15 = value;
	}

	inline static int32_t get_offset_of_mIsSmartTerrainOccluderOffset_16() { return static_cast<int32_t>(offsetof(DataSetTrackableBehaviour_t3960979584, ___mIsSmartTerrainOccluderOffset_16)); }
	inline bool get_mIsSmartTerrainOccluderOffset_16() const { return ___mIsSmartTerrainOccluderOffset_16; }
	inline bool* get_address_of_mIsSmartTerrainOccluderOffset_16() { return &___mIsSmartTerrainOccluderOffset_16; }
	inline void set_mIsSmartTerrainOccluderOffset_16(bool value)
	{
		___mIsSmartTerrainOccluderOffset_16 = value;
	}

	inline static int32_t get_offset_of_mSmartTerrainOccluderOffset_17() { return static_cast<int32_t>(offsetof(DataSetTrackableBehaviour_t3960979584, ___mSmartTerrainOccluderOffset_17)); }
	inline Vector3_t2903530434  get_mSmartTerrainOccluderOffset_17() const { return ___mSmartTerrainOccluderOffset_17; }
	inline Vector3_t2903530434 * get_address_of_mSmartTerrainOccluderOffset_17() { return &___mSmartTerrainOccluderOffset_17; }
	inline void set_mSmartTerrainOccluderOffset_17(Vector3_t2903530434  value)
	{
		___mSmartTerrainOccluderOffset_17 = value;
	}

	inline static int32_t get_offset_of_mSmartTerrainOccluderRotation_18() { return static_cast<int32_t>(offsetof(DataSetTrackableBehaviour_t3960979584, ___mSmartTerrainOccluderRotation_18)); }
	inline Quaternion_t754065749  get_mSmartTerrainOccluderRotation_18() const { return ___mSmartTerrainOccluderRotation_18; }
	inline Quaternion_t754065749 * get_address_of_mSmartTerrainOccluderRotation_18() { return &___mSmartTerrainOccluderRotation_18; }
	inline void set_mSmartTerrainOccluderRotation_18(Quaternion_t754065749  value)
	{
		___mSmartTerrainOccluderRotation_18 = value;
	}

	inline static int32_t get_offset_of_mAutoSetOccluderFromTargetSize_19() { return static_cast<int32_t>(offsetof(DataSetTrackableBehaviour_t3960979584, ___mAutoSetOccluderFromTargetSize_19)); }
	inline bool get_mAutoSetOccluderFromTargetSize_19() const { return ___mAutoSetOccluderFromTargetSize_19; }
	inline bool* get_address_of_mAutoSetOccluderFromTargetSize_19() { return &___mAutoSetOccluderFromTargetSize_19; }
	inline void set_mAutoSetOccluderFromTargetSize_19(bool value)
	{
		___mAutoSetOccluderFromTargetSize_19 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATASETTRACKABLEBEHAVIOUR_T3960979584_H
#ifndef BASEMESHEFFECT_T701321154_H
#define BASEMESHEFFECT_T701321154_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.BaseMeshEffect
struct  BaseMeshEffect_t701321154  : public UIBehaviour_t3858904981
{
public:
	// UnityEngine.UI.Graphic UnityEngine.UI.BaseMeshEffect::m_Graphic
	Graphic_t2603093343 * ___m_Graphic_2;

public:
	inline static int32_t get_offset_of_m_Graphic_2() { return static_cast<int32_t>(offsetof(BaseMeshEffect_t701321154, ___m_Graphic_2)); }
	inline Graphic_t2603093343 * get_m_Graphic_2() const { return ___m_Graphic_2; }
	inline Graphic_t2603093343 ** get_address_of_m_Graphic_2() { return &___m_Graphic_2; }
	inline void set_m_Graphic_2(Graphic_t2603093343 * value)
	{
		___m_Graphic_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_Graphic_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASEMESHEFFECT_T701321154_H
#ifndef OBJECTTARGETABSTRACTBEHAVIOUR_T1410714589_H
#define OBJECTTARGETABSTRACTBEHAVIOUR_T1410714589_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.ObjectTargetAbstractBehaviour
struct  ObjectTargetAbstractBehaviour_t1410714589  : public DataSetTrackableBehaviour_t3960979584
{
public:
	// Vuforia.ObjectTarget Vuforia.ObjectTargetAbstractBehaviour::mObjectTarget
	RuntimeObject* ___mObjectTarget_20;
	// System.Single Vuforia.ObjectTargetAbstractBehaviour::mAspectRatioXY
	float ___mAspectRatioXY_21;
	// System.Single Vuforia.ObjectTargetAbstractBehaviour::mAspectRatioXZ
	float ___mAspectRatioXZ_22;
	// System.Boolean Vuforia.ObjectTargetAbstractBehaviour::mShowBoundingBox
	bool ___mShowBoundingBox_23;
	// UnityEngine.Vector3 Vuforia.ObjectTargetAbstractBehaviour::mBBoxMin
	Vector3_t2903530434  ___mBBoxMin_24;
	// UnityEngine.Vector3 Vuforia.ObjectTargetAbstractBehaviour::mBBoxMax
	Vector3_t2903530434  ___mBBoxMax_25;
	// UnityEngine.Texture2D Vuforia.ObjectTargetAbstractBehaviour::mPreviewImage
	Texture2D_t415585320 * ___mPreviewImage_26;
	// System.Single Vuforia.ObjectTargetAbstractBehaviour::mLength
	float ___mLength_27;
	// System.Single Vuforia.ObjectTargetAbstractBehaviour::mWidth
	float ___mWidth_28;
	// System.Single Vuforia.ObjectTargetAbstractBehaviour::mHeight
	float ___mHeight_29;
	// System.Single Vuforia.ObjectTargetAbstractBehaviour::mLastTransformScale
	float ___mLastTransformScale_30;
	// UnityEngine.Vector3 Vuforia.ObjectTargetAbstractBehaviour::mLastSize
	Vector3_t2903530434  ___mLastSize_31;

public:
	inline static int32_t get_offset_of_mObjectTarget_20() { return static_cast<int32_t>(offsetof(ObjectTargetAbstractBehaviour_t1410714589, ___mObjectTarget_20)); }
	inline RuntimeObject* get_mObjectTarget_20() const { return ___mObjectTarget_20; }
	inline RuntimeObject** get_address_of_mObjectTarget_20() { return &___mObjectTarget_20; }
	inline void set_mObjectTarget_20(RuntimeObject* value)
	{
		___mObjectTarget_20 = value;
		Il2CppCodeGenWriteBarrier((&___mObjectTarget_20), value);
	}

	inline static int32_t get_offset_of_mAspectRatioXY_21() { return static_cast<int32_t>(offsetof(ObjectTargetAbstractBehaviour_t1410714589, ___mAspectRatioXY_21)); }
	inline float get_mAspectRatioXY_21() const { return ___mAspectRatioXY_21; }
	inline float* get_address_of_mAspectRatioXY_21() { return &___mAspectRatioXY_21; }
	inline void set_mAspectRatioXY_21(float value)
	{
		___mAspectRatioXY_21 = value;
	}

	inline static int32_t get_offset_of_mAspectRatioXZ_22() { return static_cast<int32_t>(offsetof(ObjectTargetAbstractBehaviour_t1410714589, ___mAspectRatioXZ_22)); }
	inline float get_mAspectRatioXZ_22() const { return ___mAspectRatioXZ_22; }
	inline float* get_address_of_mAspectRatioXZ_22() { return &___mAspectRatioXZ_22; }
	inline void set_mAspectRatioXZ_22(float value)
	{
		___mAspectRatioXZ_22 = value;
	}

	inline static int32_t get_offset_of_mShowBoundingBox_23() { return static_cast<int32_t>(offsetof(ObjectTargetAbstractBehaviour_t1410714589, ___mShowBoundingBox_23)); }
	inline bool get_mShowBoundingBox_23() const { return ___mShowBoundingBox_23; }
	inline bool* get_address_of_mShowBoundingBox_23() { return &___mShowBoundingBox_23; }
	inline void set_mShowBoundingBox_23(bool value)
	{
		___mShowBoundingBox_23 = value;
	}

	inline static int32_t get_offset_of_mBBoxMin_24() { return static_cast<int32_t>(offsetof(ObjectTargetAbstractBehaviour_t1410714589, ___mBBoxMin_24)); }
	inline Vector3_t2903530434  get_mBBoxMin_24() const { return ___mBBoxMin_24; }
	inline Vector3_t2903530434 * get_address_of_mBBoxMin_24() { return &___mBBoxMin_24; }
	inline void set_mBBoxMin_24(Vector3_t2903530434  value)
	{
		___mBBoxMin_24 = value;
	}

	inline static int32_t get_offset_of_mBBoxMax_25() { return static_cast<int32_t>(offsetof(ObjectTargetAbstractBehaviour_t1410714589, ___mBBoxMax_25)); }
	inline Vector3_t2903530434  get_mBBoxMax_25() const { return ___mBBoxMax_25; }
	inline Vector3_t2903530434 * get_address_of_mBBoxMax_25() { return &___mBBoxMax_25; }
	inline void set_mBBoxMax_25(Vector3_t2903530434  value)
	{
		___mBBoxMax_25 = value;
	}

	inline static int32_t get_offset_of_mPreviewImage_26() { return static_cast<int32_t>(offsetof(ObjectTargetAbstractBehaviour_t1410714589, ___mPreviewImage_26)); }
	inline Texture2D_t415585320 * get_mPreviewImage_26() const { return ___mPreviewImage_26; }
	inline Texture2D_t415585320 ** get_address_of_mPreviewImage_26() { return &___mPreviewImage_26; }
	inline void set_mPreviewImage_26(Texture2D_t415585320 * value)
	{
		___mPreviewImage_26 = value;
		Il2CppCodeGenWriteBarrier((&___mPreviewImage_26), value);
	}

	inline static int32_t get_offset_of_mLength_27() { return static_cast<int32_t>(offsetof(ObjectTargetAbstractBehaviour_t1410714589, ___mLength_27)); }
	inline float get_mLength_27() const { return ___mLength_27; }
	inline float* get_address_of_mLength_27() { return &___mLength_27; }
	inline void set_mLength_27(float value)
	{
		___mLength_27 = value;
	}

	inline static int32_t get_offset_of_mWidth_28() { return static_cast<int32_t>(offsetof(ObjectTargetAbstractBehaviour_t1410714589, ___mWidth_28)); }
	inline float get_mWidth_28() const { return ___mWidth_28; }
	inline float* get_address_of_mWidth_28() { return &___mWidth_28; }
	inline void set_mWidth_28(float value)
	{
		___mWidth_28 = value;
	}

	inline static int32_t get_offset_of_mHeight_29() { return static_cast<int32_t>(offsetof(ObjectTargetAbstractBehaviour_t1410714589, ___mHeight_29)); }
	inline float get_mHeight_29() const { return ___mHeight_29; }
	inline float* get_address_of_mHeight_29() { return &___mHeight_29; }
	inline void set_mHeight_29(float value)
	{
		___mHeight_29 = value;
	}

	inline static int32_t get_offset_of_mLastTransformScale_30() { return static_cast<int32_t>(offsetof(ObjectTargetAbstractBehaviour_t1410714589, ___mLastTransformScale_30)); }
	inline float get_mLastTransformScale_30() const { return ___mLastTransformScale_30; }
	inline float* get_address_of_mLastTransformScale_30() { return &___mLastTransformScale_30; }
	inline void set_mLastTransformScale_30(float value)
	{
		___mLastTransformScale_30 = value;
	}

	inline static int32_t get_offset_of_mLastSize_31() { return static_cast<int32_t>(offsetof(ObjectTargetAbstractBehaviour_t1410714589, ___mLastSize_31)); }
	inline Vector3_t2903530434  get_mLastSize_31() const { return ___mLastSize_31; }
	inline Vector3_t2903530434 * get_address_of_mLastSize_31() { return &___mLastSize_31; }
	inline void set_mLastSize_31(Vector3_t2903530434  value)
	{
		___mLastSize_31 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OBJECTTARGETABSTRACTBEHAVIOUR_T1410714589_H
#ifndef SHADOW_T1539250137_H
#define SHADOW_T1539250137_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Shadow
struct  Shadow_t1539250137  : public BaseMeshEffect_t701321154
{
public:
	// UnityEngine.Color UnityEngine.UI.Shadow::m_EffectColor
	Color_t2507026410  ___m_EffectColor_3;
	// UnityEngine.Vector2 UnityEngine.UI.Shadow::m_EffectDistance
	Vector2_t3577333262  ___m_EffectDistance_4;
	// System.Boolean UnityEngine.UI.Shadow::m_UseGraphicAlpha
	bool ___m_UseGraphicAlpha_5;

public:
	inline static int32_t get_offset_of_m_EffectColor_3() { return static_cast<int32_t>(offsetof(Shadow_t1539250137, ___m_EffectColor_3)); }
	inline Color_t2507026410  get_m_EffectColor_3() const { return ___m_EffectColor_3; }
	inline Color_t2507026410 * get_address_of_m_EffectColor_3() { return &___m_EffectColor_3; }
	inline void set_m_EffectColor_3(Color_t2507026410  value)
	{
		___m_EffectColor_3 = value;
	}

	inline static int32_t get_offset_of_m_EffectDistance_4() { return static_cast<int32_t>(offsetof(Shadow_t1539250137, ___m_EffectDistance_4)); }
	inline Vector2_t3577333262  get_m_EffectDistance_4() const { return ___m_EffectDistance_4; }
	inline Vector2_t3577333262 * get_address_of_m_EffectDistance_4() { return &___m_EffectDistance_4; }
	inline void set_m_EffectDistance_4(Vector2_t3577333262  value)
	{
		___m_EffectDistance_4 = value;
	}

	inline static int32_t get_offset_of_m_UseGraphicAlpha_5() { return static_cast<int32_t>(offsetof(Shadow_t1539250137, ___m_UseGraphicAlpha_5)); }
	inline bool get_m_UseGraphicAlpha_5() const { return ___m_UseGraphicAlpha_5; }
	inline bool* get_address_of_m_UseGraphicAlpha_5() { return &___m_UseGraphicAlpha_5; }
	inline void set_m_UseGraphicAlpha_5(bool value)
	{
		___m_UseGraphicAlpha_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SHADOW_T1539250137_H
#ifndef POSITIONASUV1_T3894084739_H
#define POSITIONASUV1_T3894084739_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.PositionAsUV1
struct  PositionAsUV1_t3894084739  : public BaseMeshEffect_t701321154
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSITIONASUV1_T3894084739_H
#ifndef OUTLINE_T3465219637_H
#define OUTLINE_T3465219637_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Outline
struct  Outline_t3465219637  : public Shadow_t1539250137
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OUTLINE_T3465219637_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1800 = { sizeof (VertexHelper_t419071860), -1, sizeof(VertexHelper_t419071860_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1800[11] = 
{
	VertexHelper_t419071860::get_offset_of_m_Positions_0(),
	VertexHelper_t419071860::get_offset_of_m_Colors_1(),
	VertexHelper_t419071860::get_offset_of_m_Uv0S_2(),
	VertexHelper_t419071860::get_offset_of_m_Uv1S_3(),
	VertexHelper_t419071860::get_offset_of_m_Uv2S_4(),
	VertexHelper_t419071860::get_offset_of_m_Uv3S_5(),
	VertexHelper_t419071860::get_offset_of_m_Normals_6(),
	VertexHelper_t419071860::get_offset_of_m_Tangents_7(),
	VertexHelper_t419071860::get_offset_of_m_Indices_8(),
	VertexHelper_t419071860_StaticFields::get_offset_of_s_DefaultTangent_9(),
	VertexHelper_t419071860_StaticFields::get_offset_of_s_DefaultNormal_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1801 = { sizeof (BaseVertexEffect_t442648142), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1802 = { sizeof (BaseMeshEffect_t701321154), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1802[1] = 
{
	BaseMeshEffect_t701321154::get_offset_of_m_Graphic_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1803 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1804 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1805 = { sizeof (Outline_t3465219637), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1806 = { sizeof (PositionAsUV1_t3894084739), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1807 = { sizeof (Shadow_t1539250137), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1807[4] = 
{
	Shadow_t1539250137::get_offset_of_m_EffectColor_3(),
	Shadow_t1539250137::get_offset_of_m_EffectDistance_4(),
	Shadow_t1539250137::get_offset_of_m_UseGraphicAlpha_5(),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1808 = { sizeof (U3CPrivateImplementationDetailsU3E_t4060893174), -1, sizeof(U3CPrivateImplementationDetailsU3E_t4060893174_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1808[1] = 
{
	U3CPrivateImplementationDetailsU3E_t4060893174_StaticFields::get_offset_of_U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1809 = { sizeof (U24ArrayTypeU3D12_t1462559582)+ sizeof (RuntimeObject), sizeof(U24ArrayTypeU3D12_t1462559582 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1810 = { sizeof (U3CModuleU3E_t160650888), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1811 = { sizeof (ARController_t1205915989), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1811[1] = 
{
	ARController_t1205915989::get_offset_of_mVuforiaBehaviour_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1812 = { sizeof (U3CU3Ec__DisplayClass11_0_t2399120476), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1812[1] = 
{
	U3CU3Ec__DisplayClass11_0_t2399120476::get_offset_of_controller_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1813 = { sizeof (DigitalEyewearARController_t1877756253), -1, sizeof(DigitalEyewearARController_t1877756253_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1813[28] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	DigitalEyewearARController_t1877756253::get_offset_of_mCameraOffset_7(),
	DigitalEyewearARController_t1877756253::get_offset_of_mDistortionRenderingMode_8(),
	DigitalEyewearARController_t1877756253::get_offset_of_mDistortionRenderingLayer_9(),
	DigitalEyewearARController_t1877756253::get_offset_of_mEyewearType_10(),
	DigitalEyewearARController_t1877756253::get_offset_of_mStereoFramework_11(),
	DigitalEyewearARController_t1877756253::get_offset_of_mSeeThroughConfiguration_12(),
	DigitalEyewearARController_t1877756253::get_offset_of_mViewerName_13(),
	DigitalEyewearARController_t1877756253::get_offset_of_mViewerManufacturer_14(),
	DigitalEyewearARController_t1877756253::get_offset_of_mUseCustomViewer_15(),
	DigitalEyewearARController_t1877756253::get_offset_of_mCustomViewer_16(),
	DigitalEyewearARController_t1877756253::get_offset_of_mCentralAnchorPoint_17(),
	DigitalEyewearARController_t1877756253::get_offset_of_mParentAnchorPoint_18(),
	DigitalEyewearARController_t1877756253::get_offset_of_mPrimaryCamera_19(),
	DigitalEyewearARController_t1877756253::get_offset_of_mPrimaryCameraOriginalRect_20(),
	DigitalEyewearARController_t1877756253::get_offset_of_mSecondaryCamera_21(),
	DigitalEyewearARController_t1877756253::get_offset_of_mSecondaryCameraOriginalRect_22(),
	DigitalEyewearARController_t1877756253::get_offset_of_mSecondaryCameraDisabledLocally_23(),
	DigitalEyewearARController_t1877756253::get_offset_of_mVuforiaBehaviour_24(),
	DigitalEyewearARController_t1877756253::get_offset_of_mDistortionRenderingBhvr_25(),
	DigitalEyewearARController_t1877756253::get_offset_of_mSetFocusPlaneAutomatically_26(),
	DigitalEyewearARController_t1877756253_StaticFields::get_offset_of_mInstance_27(),
	DigitalEyewearARController_t1877756253_StaticFields::get_offset_of_mPadlock_28(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1814 = { sizeof (EyewearType_t4127891436)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1814[4] = 
{
	EyewearType_t4127891436::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1815 = { sizeof (StereoFramework_t3060055999)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1815[4] = 
{
	StereoFramework_t3060055999::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1816 = { sizeof (SeeThroughConfiguration_t1856493377)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1816[3] = 
{
	SeeThroughConfiguration_t1856493377::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1817 = { sizeof (SerializableViewerParameters_t484734121), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1817[11] = 
{
	SerializableViewerParameters_t484734121::get_offset_of_Version_0(),
	SerializableViewerParameters_t484734121::get_offset_of_Name_1(),
	SerializableViewerParameters_t484734121::get_offset_of_Manufacturer_2(),
	SerializableViewerParameters_t484734121::get_offset_of_ButtonType_3(),
	SerializableViewerParameters_t484734121::get_offset_of_ScreenToLensDistance_4(),
	SerializableViewerParameters_t484734121::get_offset_of_InterLensDistance_5(),
	SerializableViewerParameters_t484734121::get_offset_of_TrayAlignment_6(),
	SerializableViewerParameters_t484734121::get_offset_of_LensCenterToTrayDistance_7(),
	SerializableViewerParameters_t484734121::get_offset_of_DistortionCoefficients_8(),
	SerializableViewerParameters_t484734121::get_offset_of_FieldOfView_9(),
	SerializableViewerParameters_t484734121::get_offset_of_ContainsMagnet_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1818 = { sizeof (EyewearDevice_t3960213283), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1819 = { sizeof (EyeID_t1454423357)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1819[4] = 
{
	EyeID_t1454423357::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1820 = { sizeof (EyewearCalibrationReading_t972160870)+ sizeof (RuntimeObject), sizeof(EyewearCalibrationReading_t972160870_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1820[5] = 
{
	EyewearCalibrationReading_t972160870::get_offset_of_pose_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	EyewearCalibrationReading_t972160870::get_offset_of_scale_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	EyewearCalibrationReading_t972160870::get_offset_of_centerX_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	EyewearCalibrationReading_t972160870::get_offset_of_centerY_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	EyewearCalibrationReading_t972160870::get_offset_of_unused_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1821 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1822 = { sizeof (NullHoloLensApiAbstraction_t4291924892), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1823 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1824 = { sizeof (DeviceTracker_t54803992), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1825 = { sizeof (DeviceTrackerARController_t2334311105), -1, sizeof(DeviceTrackerARController_t2334311105_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1825[13] = 
{
	DeviceTrackerARController_t2334311105_StaticFields::get_offset_of_DEFAULT_HEAD_PIVOT_1(),
	DeviceTrackerARController_t2334311105_StaticFields::get_offset_of_DEFAULT_HANDHELD_PIVOT_2(),
	DeviceTrackerARController_t2334311105::get_offset_of_mAutoInitTracker_3(),
	DeviceTrackerARController_t2334311105::get_offset_of_mAutoStartTracker_4(),
	DeviceTrackerARController_t2334311105::get_offset_of_mPosePrediction_5(),
	DeviceTrackerARController_t2334311105::get_offset_of_mModelCorrectionMode_6(),
	DeviceTrackerARController_t2334311105::get_offset_of_mModelTransformEnabled_7(),
	DeviceTrackerARController_t2334311105::get_offset_of_mModelTransform_8(),
	DeviceTrackerARController_t2334311105::get_offset_of_mTrackerStarted_9(),
	DeviceTrackerARController_t2334311105::get_offset_of_mTrackerWasActiveBeforePause_10(),
	DeviceTrackerARController_t2334311105::get_offset_of_mTrackerWasActiveBeforeDisabling_11(),
	DeviceTrackerARController_t2334311105_StaticFields::get_offset_of_mInstance_12(),
	DeviceTrackerARController_t2334311105_StaticFields::get_offset_of_mPadlock_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1826 = { sizeof (DistortionRenderingMode_t4279873297)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1826[4] = 
{
	DistortionRenderingMode_t4279873297::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1827 = { sizeof (DistortionRenderingBehaviour_t2785666256), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1827[12] = 
{
	DistortionRenderingBehaviour_t2785666256::get_offset_of_mSingleTexture_2(),
	DistortionRenderingBehaviour_t2785666256::get_offset_of_mRenderLayer_3(),
	DistortionRenderingBehaviour_t2785666256::get_offset_of_mOriginalCullingMasks_4(),
	DistortionRenderingBehaviour_t2785666256::get_offset_of_mStereoCameras_5(),
	DistortionRenderingBehaviour_t2785666256::get_offset_of_mMeshes_6(),
	DistortionRenderingBehaviour_t2785666256::get_offset_of_mTextures_7(),
	DistortionRenderingBehaviour_t2785666256::get_offset_of_mStarted_8(),
	DistortionRenderingBehaviour_t2785666256::get_offset_of_mVideoBackgroundChanged_9(),
	DistortionRenderingBehaviour_t2785666256::get_offset_of_mOriginalLeftViewport_10(),
	DistortionRenderingBehaviour_t2785666256::get_offset_of_mOriginalRightViewport_11(),
	DistortionRenderingBehaviour_t2785666256::get_offset_of_mDualTextureLeftViewport_12(),
	DistortionRenderingBehaviour_t2785666256::get_offset_of_mDualTextureRightViewport_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1828 = { sizeof (DelegateHelper_t2291928084), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1829 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1830 = { sizeof (PlayModeEyewearUserCalibratorImpl_t2691755708), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1831 = { sizeof (PlayModeEyewearCalibrationProfileManagerImpl_t1010441882), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1832 = { sizeof (PlayModeEyewearDevice_t1597218540), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1832[3] = 
{
	PlayModeEyewearDevice_t1597218540::get_offset_of_mProfileManager_1(),
	PlayModeEyewearDevice_t1597218540::get_offset_of_mCalibrator_2(),
	PlayModeEyewearDevice_t1597218540::get_offset_of_mDummyPredictiveTracking_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1833 = { sizeof (DedicatedEyewearDevice_t4104793313), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1833[2] = 
{
	DedicatedEyewearDevice_t4104793313::get_offset_of_mProfileManager_1(),
	DedicatedEyewearDevice_t4104793313::get_offset_of_mCalibrator_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1834 = { sizeof (CameraConfigurationUtility_t995036719), -1, sizeof(CameraConfigurationUtility_t995036719_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1834[6] = 
{
	CameraConfigurationUtility_t995036719_StaticFields::get_offset_of_MIN_CENTER_0(),
	CameraConfigurationUtility_t995036719_StaticFields::get_offset_of_MAX_CENTER_1(),
	CameraConfigurationUtility_t995036719_StaticFields::get_offset_of_MAX_BOTTOM_2(),
	CameraConfigurationUtility_t995036719_StaticFields::get_offset_of_MAX_TOP_3(),
	CameraConfigurationUtility_t995036719_StaticFields::get_offset_of_MAX_LEFT_4(),
	CameraConfigurationUtility_t995036719_StaticFields::get_offset_of_MAX_RIGHT_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1835 = { sizeof (BaseCameraConfiguration_t196193726), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1835[10] = 
{
	BaseCameraConfiguration_t196193726::get_offset_of_mCameraDeviceMode_0(),
	BaseCameraConfiguration_t196193726::get_offset_of_mLastVideoBackGroundMirroredFromSDK_1(),
	BaseCameraConfiguration_t196193726::get_offset_of_mOnVideoBackgroundConfigChanged_2(),
	BaseCameraConfiguration_t196193726::get_offset_of_mVideoBackgroundBehaviours_3(),
	BaseCameraConfiguration_t196193726::get_offset_of_mVideoBackgroundViewportRect_4(),
	BaseCameraConfiguration_t196193726::get_offset_of_mRenderVideoBackground_5(),
	BaseCameraConfiguration_t196193726::get_offset_of_mProjectionOrientation_6(),
	BaseCameraConfiguration_t196193726::get_offset_of_mInitialReflection_7(),
	BaseCameraConfiguration_t196193726::get_offset_of_mBackgroundPlaneBehaviour_8(),
	BaseCameraConfiguration_t196193726::get_offset_of_mCameraParameterChanged_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1836 = { sizeof (BaseStereoViewerCameraConfiguration_t1087628355), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1836[5] = 
{
	BaseStereoViewerCameraConfiguration_t1087628355::get_offset_of_mPrimaryCamera_10(),
	BaseStereoViewerCameraConfiguration_t1087628355::get_offset_of_mSecondaryCamera_11(),
	BaseStereoViewerCameraConfiguration_t1087628355::get_offset_of_mSkewFrustum_12(),
	BaseStereoViewerCameraConfiguration_t1087628355::get_offset_of_mScreenWidth_13(),
	BaseStereoViewerCameraConfiguration_t1087628355::get_offset_of_mScreenHeight_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1837 = { sizeof (StereoViewerCameraConfiguration_t4238609684), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1837[7] = 
{
	0,
	StereoViewerCameraConfiguration_t4238609684::get_offset_of_mLastAppliedLeftNearClipPlane_16(),
	StereoViewerCameraConfiguration_t4238609684::get_offset_of_mLastAppliedLeftFarClipPlane_17(),
	StereoViewerCameraConfiguration_t4238609684::get_offset_of_mLastAppliedRightNearClipPlane_18(),
	StereoViewerCameraConfiguration_t4238609684::get_offset_of_mLastAppliedRightFarClipPlane_19(),
	StereoViewerCameraConfiguration_t4238609684::get_offset_of_mCameraOffset_20(),
	StereoViewerCameraConfiguration_t4238609684::get_offset_of_mIsDistorted_21(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1838 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1839 = { sizeof (HoloLensExtendedTrackingManager_t3745668720), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1839[15] = 
{
	HoloLensExtendedTrackingManager_t3745668720::get_offset_of_mNumFramesStablePose_0(),
	HoloLensExtendedTrackingManager_t3745668720::get_offset_of_mMaxPoseRelDistance_1(),
	HoloLensExtendedTrackingManager_t3745668720::get_offset_of_mMaxPoseAngleDiff_2(),
	HoloLensExtendedTrackingManager_t3745668720::get_offset_of_mMaxCamPoseAbsDistance_3(),
	HoloLensExtendedTrackingManager_t3745668720::get_offset_of_mMaxCamPoseAngleDiff_4(),
	HoloLensExtendedTrackingManager_t3745668720::get_offset_of_mMinNumFramesPoseOff_5(),
	HoloLensExtendedTrackingManager_t3745668720::get_offset_of_mMinPoseUpdateRelDistance_6(),
	HoloLensExtendedTrackingManager_t3745668720::get_offset_of_mMinPoseUpdateAngleDiff_7(),
	HoloLensExtendedTrackingManager_t3745668720::get_offset_of_mTrackableSizeInViewThreshold_8(),
	HoloLensExtendedTrackingManager_t3745668720::get_offset_of_mMaxDistanceFromViewCenterForPoseUpdate_9(),
	HoloLensExtendedTrackingManager_t3745668720::get_offset_of_mSetWorldAnchors_10(),
	HoloLensExtendedTrackingManager_t3745668720::get_offset_of_mTrackingList_11(),
	HoloLensExtendedTrackingManager_t3745668720::get_offset_of_mTrackablesExtendedTrackingEnabled_12(),
	HoloLensExtendedTrackingManager_t3745668720::get_offset_of_mTrackablesCurrentlyExtendedTracked_13(),
	HoloLensExtendedTrackingManager_t3745668720::get_offset_of_mExtendedTrackablesState_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1840 = { sizeof (PoseInfo_t2257311701)+ sizeof (RuntimeObject), sizeof(PoseInfo_t2257311701 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1840[3] = 
{
	PoseInfo_t2257311701::get_offset_of_Position_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	PoseInfo_t2257311701::get_offset_of_Rotation_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	PoseInfo_t2257311701::get_offset_of_NumFramesPoseWasOff_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1841 = { sizeof (PoseAgeEntry_t806437902)+ sizeof (RuntimeObject), sizeof(PoseAgeEntry_t806437902 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1841[3] = 
{
	PoseAgeEntry_t806437902::get_offset_of_Pose_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	PoseAgeEntry_t806437902::get_offset_of_CameraPose_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	PoseAgeEntry_t806437902::get_offset_of_Age_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1842 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1843 = { sizeof (VuforiaExtendedTrackingManager_t368417945), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1844 = { sizeof (VuMarkManagerImpl_t2293130653), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1844[6] = 
{
	VuMarkManagerImpl_t2293130653::get_offset_of_mBehaviours_0(),
	VuMarkManagerImpl_t2293130653::get_offset_of_mActiveVuMarkTargets_1(),
	VuMarkManagerImpl_t2293130653::get_offset_of_mDestroyedBehaviours_2(),
	VuMarkManagerImpl_t2293130653::get_offset_of_mOnVuMarkDetected_3(),
	VuMarkManagerImpl_t2293130653::get_offset_of_mOnVuMarkLost_4(),
	VuMarkManagerImpl_t2293130653::get_offset_of_mOnVuMarkBehaviourDetected_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1845 = { sizeof (InstanceIdImpl_t2419732903), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1845[5] = 
{
	InstanceIdImpl_t2419732903::get_offset_of_mDataType_0(),
	InstanceIdImpl_t2419732903::get_offset_of_mBuffer_1(),
	InstanceIdImpl_t2419732903::get_offset_of_mNumericValue_2(),
	InstanceIdImpl_t2419732903::get_offset_of_mDataLength_3(),
	InstanceIdImpl_t2419732903::get_offset_of_mCachedStringValue_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1846 = { sizeof (VuMarkTargetImpl_t2724072581), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1846[5] = 
{
	VuMarkTargetImpl_t2724072581::get_offset_of_mVuMarkTemplate_0(),
	VuMarkTargetImpl_t2724072581::get_offset_of_mInstanceId_1(),
	VuMarkTargetImpl_t2724072581::get_offset_of_mTargetId_2(),
	VuMarkTargetImpl_t2724072581::get_offset_of_mInstanceImage_3(),
	VuMarkTargetImpl_t2724072581::get_offset_of_mInstanceImageHeader_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1847 = { sizeof (VuMarkTemplateImpl_t1477556351), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1847[3] = 
{
	VuMarkTemplateImpl_t1477556351::get_offset_of_mUserData_4(),
	VuMarkTemplateImpl_t1477556351::get_offset_of_mOrigin_5(),
	VuMarkTemplateImpl_t1477556351::get_offset_of_mTrackingFromRuntimeAppearance_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1848 = { sizeof (MixedRealityController_t4091258454), -1, sizeof(MixedRealityController_t4091258454_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1848[13] = 
{
	MixedRealityController_t4091258454_StaticFields::get_offset_of_mInstance_0(),
	MixedRealityController_t4091258454::get_offset_of_mVuforiaBehaviour_1(),
	MixedRealityController_t4091258454::get_offset_of_mDigitalEyewearBehaviour_2(),
	MixedRealityController_t4091258454::get_offset_of_mVideoBackgroundManager_3(),
	MixedRealityController_t4091258454::get_offset_of_mViewerHasBeenSetExternally_4(),
	MixedRealityController_t4091258454::get_offset_of_mViewerParameters_5(),
	MixedRealityController_t4091258454::get_offset_of_mFrameWorkHasBeenSetExternally_6(),
	MixedRealityController_t4091258454::get_offset_of_mStereoFramework_7(),
	MixedRealityController_t4091258454::get_offset_of_mCentralAnchorPoint_8(),
	MixedRealityController_t4091258454::get_offset_of_mLeftCameraOfExternalSDK_9(),
	MixedRealityController_t4091258454::get_offset_of_mRightCameraOfExternalSDK_10(),
	MixedRealityController_t4091258454::get_offset_of_mObjectTrackerStopped_11(),
	MixedRealityController_t4091258454::get_offset_of_mAutoStopCameraIfNotRequired_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1849 = { sizeof (Mode_t1800297771)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1849[7] = 
{
	Mode_t1800297771::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1850 = { sizeof (RotationalDeviceTracker_t2131173692), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1851 = { sizeof (MODEL_CORRECTION_MODE_t3621708838)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1851[4] = 
{
	MODEL_CORRECTION_MODE_t3621708838::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1852 = { sizeof (CustomViewerParameters_t701518465), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1852[7] = 
{
	CustomViewerParameters_t701518465::get_offset_of_mVersion_1(),
	CustomViewerParameters_t701518465::get_offset_of_mName_2(),
	CustomViewerParameters_t701518465::get_offset_of_mManufacturer_3(),
	CustomViewerParameters_t701518465::get_offset_of_mButtonType_4(),
	CustomViewerParameters_t701518465::get_offset_of_mScreenToLensDistance_5(),
	CustomViewerParameters_t701518465::get_offset_of_mTrayAlignment_6(),
	CustomViewerParameters_t701518465::get_offset_of_mMagnet_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1853 = { sizeof (DeviceTrackingManager_t2027984062), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1853[4] = 
{
	DeviceTrackingManager_t2027984062::get_offset_of_mDeviceTrackerPositonOffset_0(),
	DeviceTrackingManager_t2027984062::get_offset_of_mDeviceTrackerRotationOffset_1(),
	DeviceTrackingManager_t2027984062::get_offset_of_mBeforeDevicePoseUpdated_2(),
	DeviceTrackingManager_t2027984062::get_offset_of_mAfterDevicePoseUpdated_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1854 = { sizeof (FactorySetter_t372426630), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1855 = { sizeof (EyewearCalibrationProfileManagerImpl_t1058943192), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1856 = { sizeof (BackgroundPlaneAbstractBehaviour_t4259835756), -1, sizeof(BackgroundPlaneAbstractBehaviour_t4259835756_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1856[13] = 
{
	BackgroundPlaneAbstractBehaviour_t4259835756::get_offset_of_mTextureInfo_2(),
	BackgroundPlaneAbstractBehaviour_t4259835756::get_offset_of_mViewWidth_3(),
	BackgroundPlaneAbstractBehaviour_t4259835756::get_offset_of_mViewHeight_4(),
	BackgroundPlaneAbstractBehaviour_t4259835756::get_offset_of_mNumFramesToUpdateVideoBg_5(),
	BackgroundPlaneAbstractBehaviour_t4259835756::get_offset_of_mCamera_6(),
	BackgroundPlaneAbstractBehaviour_t4259835756_StaticFields::get_offset_of_maxDisplacement_7(),
	BackgroundPlaneAbstractBehaviour_t4259835756::get_offset_of_defaultNumDivisions_8(),
	BackgroundPlaneAbstractBehaviour_t4259835756::get_offset_of_mMesh_9(),
	BackgroundPlaneAbstractBehaviour_t4259835756::get_offset_of_mStereoDepth_10(),
	BackgroundPlaneAbstractBehaviour_t4259835756::get_offset_of_mBackgroundOffset_11(),
	BackgroundPlaneAbstractBehaviour_t4259835756::get_offset_of_mVuforiaBehaviour_12(),
	BackgroundPlaneAbstractBehaviour_t4259835756::get_offset_of_mBackgroundPlacedCallback_13(),
	BackgroundPlaneAbstractBehaviour_t4259835756::get_offset_of_mNumDivisions_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1857 = { sizeof (EyewearUserCalibratorImpl_t3458973152), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1858 = { sizeof (RotationalPlayModeDeviceTrackerImpl_t2014906089), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1858[3] = 
{
	RotationalPlayModeDeviceTrackerImpl_t2014906089::get_offset_of_mRotation_1(),
	RotationalPlayModeDeviceTrackerImpl_t2014906089::get_offset_of_mModelCorrectionTransform_2(),
	RotationalPlayModeDeviceTrackerImpl_t2014906089::get_offset_of_mModelCorrection_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1859 = { sizeof (RotationalDeviceTrackerImpl_t1875102819), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1860 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1861 = { sizeof (IOSCamRecoveringHelper_t853811003), -1, sizeof(IOSCamRecoveringHelper_t853811003_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1861[9] = 
{
	0,
	0,
	0,
	IOSCamRecoveringHelper_t853811003_StaticFields::get_offset_of_sHasJustResumed_3(),
	IOSCamRecoveringHelper_t853811003_StaticFields::get_offset_of_sCheckFailedFrameAfterResume_4(),
	IOSCamRecoveringHelper_t853811003_StaticFields::get_offset_of_sCheckedFailedFrameCounter_5(),
	IOSCamRecoveringHelper_t853811003_StaticFields::get_offset_of_sWaitToRecoverCameraRestart_6(),
	IOSCamRecoveringHelper_t853811003_StaticFields::get_offset_of_sWaitedFrameRecoverCounter_7(),
	IOSCamRecoveringHelper_t853811003_StaticFields::get_offset_of_sRecoveryAttemptCounter_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1862 = { sizeof (MeshUtils_t4112614896), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1863 = { sizeof (ExternalStereoCameraConfiguration_t3079436327), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1863[21] = 
{
	0,
	ExternalStereoCameraConfiguration_t3079436327::get_offset_of_mLastAppliedLeftNearClipPlane_16(),
	ExternalStereoCameraConfiguration_t3079436327::get_offset_of_mLastAppliedLeftFarClipPlane_17(),
	ExternalStereoCameraConfiguration_t3079436327::get_offset_of_mLastAppliedRightNearClipPlane_18(),
	ExternalStereoCameraConfiguration_t3079436327::get_offset_of_mLastAppliedRightFarClipPlane_19(),
	ExternalStereoCameraConfiguration_t3079436327::get_offset_of_mLastAppliedLeftVerticalVirtualFoV_20(),
	ExternalStereoCameraConfiguration_t3079436327::get_offset_of_mLastAppliedLeftHorizontalVirtualFoV_21(),
	ExternalStereoCameraConfiguration_t3079436327::get_offset_of_mLastAppliedRightVerticalVirtualFoV_22(),
	ExternalStereoCameraConfiguration_t3079436327::get_offset_of_mLastAppliedRightHorizontalVirtualFoV_23(),
	ExternalStereoCameraConfiguration_t3079436327::get_offset_of_mLastAppliedLeftProjection_24(),
	ExternalStereoCameraConfiguration_t3079436327::get_offset_of_mLastAppliedRightProjection_25(),
	ExternalStereoCameraConfiguration_t3079436327::get_offset_of_mNewLeftNearClipPlane_26(),
	ExternalStereoCameraConfiguration_t3079436327::get_offset_of_mNewLeftFarClipPlane_27(),
	ExternalStereoCameraConfiguration_t3079436327::get_offset_of_mNewRightNearClipPlane_28(),
	ExternalStereoCameraConfiguration_t3079436327::get_offset_of_mNewRightFarClipPlane_29(),
	ExternalStereoCameraConfiguration_t3079436327::get_offset_of_mNewLeftVerticalVirtualFoV_30(),
	ExternalStereoCameraConfiguration_t3079436327::get_offset_of_mNewLeftHorizontalVirtualFoV_31(),
	ExternalStereoCameraConfiguration_t3079436327::get_offset_of_mNewRightVerticalVirtualFoV_32(),
	ExternalStereoCameraConfiguration_t3079436327::get_offset_of_mNewRightHorizontalVirtualFoV_33(),
	ExternalStereoCameraConfiguration_t3079436327::get_offset_of_mExternallySetLeftMatrix_34(),
	ExternalStereoCameraConfiguration_t3079436327::get_offset_of_mExternallySetRightMatrix_35(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1864 = { sizeof (NullHideExcessAreaClipping_t4203830031), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1865 = { sizeof (StencilHideExcessAreaClipping_t2110303595), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1865[13] = 
{
	StencilHideExcessAreaClipping_t2110303595::get_offset_of_mGameObject_0(),
	StencilHideExcessAreaClipping_t2110303595::get_offset_of_mMatteShader_1(),
	StencilHideExcessAreaClipping_t2110303595::get_offset_of_mClippingPlane_2(),
	StencilHideExcessAreaClipping_t2110303595::get_offset_of_mCamera_3(),
	StencilHideExcessAreaClipping_t2110303595::get_offset_of_mCameraNearPlane_4(),
	StencilHideExcessAreaClipping_t2110303595::get_offset_of_mCameraFarPlane_5(),
	StencilHideExcessAreaClipping_t2110303595::get_offset_of_mCameraPixelRect_6(),
	StencilHideExcessAreaClipping_t2110303595::get_offset_of_mCameraFieldOfView_7(),
	StencilHideExcessAreaClipping_t2110303595::get_offset_of_mVuforiaBehaviour_8(),
	StencilHideExcessAreaClipping_t2110303595::get_offset_of_mPlanesActivated_9(),
	StencilHideExcessAreaClipping_t2110303595::get_offset_of_mBgPlane_10(),
	StencilHideExcessAreaClipping_t2110303595::get_offset_of_mBgPlaneLocalPos_11(),
	StencilHideExcessAreaClipping_t2110303595::get_offset_of_mBgPlaneLocalScale_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1866 = { sizeof (LegacyHideExcessAreaClipping_t2114833877), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1866[21] = 
{
	LegacyHideExcessAreaClipping_t2114833877::get_offset_of_mGameObject_0(),
	LegacyHideExcessAreaClipping_t2114833877::get_offset_of_mMatteShader_1(),
	LegacyHideExcessAreaClipping_t2114833877::get_offset_of_mBgPlane_2(),
	LegacyHideExcessAreaClipping_t2114833877::get_offset_of_mLeftPlane_3(),
	LegacyHideExcessAreaClipping_t2114833877::get_offset_of_mRightPlane_4(),
	LegacyHideExcessAreaClipping_t2114833877::get_offset_of_mTopPlane_5(),
	LegacyHideExcessAreaClipping_t2114833877::get_offset_of_mBottomPlane_6(),
	LegacyHideExcessAreaClipping_t2114833877::get_offset_of_mCamera_7(),
	LegacyHideExcessAreaClipping_t2114833877::get_offset_of_mBgPlaneLocalPos_8(),
	LegacyHideExcessAreaClipping_t2114833877::get_offset_of_mBgPlaneLocalScale_9(),
	LegacyHideExcessAreaClipping_t2114833877::get_offset_of_mCameraNearPlane_10(),
	LegacyHideExcessAreaClipping_t2114833877::get_offset_of_mCameraPixelRect_11(),
	LegacyHideExcessAreaClipping_t2114833877::get_offset_of_mCameraFieldOFView_12(),
	LegacyHideExcessAreaClipping_t2114833877::get_offset_of_mVuforiaBehaviour_13(),
	LegacyHideExcessAreaClipping_t2114833877::get_offset_of_mHideBehaviours_14(),
	LegacyHideExcessAreaClipping_t2114833877::get_offset_of_mDeactivatedHideBehaviours_15(),
	LegacyHideExcessAreaClipping_t2114833877::get_offset_of_mPlanesActivated_16(),
	LegacyHideExcessAreaClipping_t2114833877::get_offset_of_mLeftPlaneCachedScale_17(),
	LegacyHideExcessAreaClipping_t2114833877::get_offset_of_mRightPlaneCachedScale_18(),
	LegacyHideExcessAreaClipping_t2114833877::get_offset_of_mBottomPlaneCachedScale_19(),
	LegacyHideExcessAreaClipping_t2114833877::get_offset_of_mTopPlaneCachedScale_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1867 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1868 = { sizeof (DedicatedEyewearCameraConfiguration_t1287559789), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1868[13] = 
{
	0,
	DedicatedEyewearCameraConfiguration_t1287559789::get_offset_of_mPrimaryCamera_11(),
	DedicatedEyewearCameraConfiguration_t1287559789::get_offset_of_mSecondaryCamera_12(),
	DedicatedEyewearCameraConfiguration_t1287559789::get_offset_of_mScreenWidth_13(),
	DedicatedEyewearCameraConfiguration_t1287559789::get_offset_of_mScreenHeight_14(),
	DedicatedEyewearCameraConfiguration_t1287559789::get_offset_of_mNeedToCheckStereo_15(),
	DedicatedEyewearCameraConfiguration_t1287559789::get_offset_of_mLastAppliedNearClipPlane_16(),
	DedicatedEyewearCameraConfiguration_t1287559789::get_offset_of_mLastAppliedFarClipPlane_17(),
	DedicatedEyewearCameraConfiguration_t1287559789::get_offset_of_mLastAppliedVirtualFoV_18(),
	DedicatedEyewearCameraConfiguration_t1287559789::get_offset_of_mNewNearClipPlane_19(),
	DedicatedEyewearCameraConfiguration_t1287559789::get_offset_of_mNewFarClipPlane_20(),
	DedicatedEyewearCameraConfiguration_t1287559789::get_offset_of_mNewVirtualFoV_21(),
	DedicatedEyewearCameraConfiguration_t1287559789::get_offset_of_mEyewearDevice_22(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1869 = { sizeof (NullCameraConfiguration_t4274136510), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1869[1] = 
{
	NullCameraConfiguration_t4274136510::get_offset_of_mProjectionOrientation_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1870 = { sizeof (MonoCameraConfiguration_t715615799), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1870[7] = 
{
	0,
	MonoCameraConfiguration_t715615799::get_offset_of_mPrimaryCamera_11(),
	MonoCameraConfiguration_t715615799::get_offset_of_mCameraViewPortWidth_12(),
	MonoCameraConfiguration_t715615799::get_offset_of_mCameraViewPortHeight_13(),
	MonoCameraConfiguration_t715615799::get_offset_of_mLastAppliedNearClipPlane_14(),
	MonoCameraConfiguration_t715615799::get_offset_of_mLastAppliedFarClipPlane_15(),
	MonoCameraConfiguration_t715615799::get_offset_of_mLastAppliedFoV_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1871 = { sizeof (UnityCameraExtensions_t1038291701), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1871[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1872 = { sizeof (View_t3963731153)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1872[6] = 
{
	View_t3963731153::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1873 = { sizeof (ViewerParameters_t3954347578), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1873[1] = 
{
	ViewerParameters_t3954347578::get_offset_of_mNativeVP_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1874 = { sizeof (ViewerParametersList_t2716421017), -1, sizeof(ViewerParametersList_t2716421017_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1874[2] = 
{
	ViewerParametersList_t2716421017::get_offset_of_mNativeVPL_0(),
	ViewerParametersList_t2716421017_StaticFields::get_offset_of_mListForAuthoringTools_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1875 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1876 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1877 = { sizeof (ObjectTargetAbstractBehaviour_t1410714589), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1877[12] = 
{
	ObjectTargetAbstractBehaviour_t1410714589::get_offset_of_mObjectTarget_20(),
	ObjectTargetAbstractBehaviour_t1410714589::get_offset_of_mAspectRatioXY_21(),
	ObjectTargetAbstractBehaviour_t1410714589::get_offset_of_mAspectRatioXZ_22(),
	ObjectTargetAbstractBehaviour_t1410714589::get_offset_of_mShowBoundingBox_23(),
	ObjectTargetAbstractBehaviour_t1410714589::get_offset_of_mBBoxMin_24(),
	ObjectTargetAbstractBehaviour_t1410714589::get_offset_of_mBBoxMax_25(),
	ObjectTargetAbstractBehaviour_t1410714589::get_offset_of_mPreviewImage_26(),
	ObjectTargetAbstractBehaviour_t1410714589::get_offset_of_mLength_27(),
	ObjectTargetAbstractBehaviour_t1410714589::get_offset_of_mWidth_28(),
	ObjectTargetAbstractBehaviour_t1410714589::get_offset_of_mHeight_29(),
	ObjectTargetAbstractBehaviour_t1410714589::get_offset_of_mLastTransformScale_30(),
	ObjectTargetAbstractBehaviour_t1410714589::get_offset_of_mLastSize_31(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1878 = { sizeof (CameraDevice_t2431414939), -1, sizeof(CameraDevice_t2431414939_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1878[1] = 
{
	CameraDevice_t2431414939_StaticFields::get_offset_of_mInstance_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1879 = { sizeof (CameraDeviceMode_t1785007815)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1879[4] = 
{
	CameraDeviceMode_t1785007815::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1880 = { sizeof (FocusMode_t4230446711)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1880[6] = 
{
	FocusMode_t4230446711::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1881 = { sizeof (CameraDirection_t1337526284)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1881[4] = 
{
	CameraDirection_t1337526284::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1882 = { sizeof (VideoModeData_t2436408626)+ sizeof (RuntimeObject), sizeof(VideoModeData_t2436408626 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1882[4] = 
{
	VideoModeData_t2436408626::get_offset_of_width_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	VideoModeData_t2436408626::get_offset_of_height_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	VideoModeData_t2436408626::get_offset_of_frameRate_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	VideoModeData_t2436408626::get_offset_of_unused_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1883 = { sizeof (CameraField_t382214397)+ sizeof (RuntimeObject), sizeof(CameraField_t382214397_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1883[2] = 
{
	CameraField_t382214397::get_offset_of_Type_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	CameraField_t382214397::get_offset_of_Key_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1884 = { sizeof (DataType_t3838319351)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1884[7] = 
{
	DataType_t3838319351::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1885 = { sizeof (Int64Range_t2805734248)+ sizeof (RuntimeObject), sizeof(Int64Range_t2805734248 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1885[2] = 
{
	Int64Range_t2805734248::get_offset_of_fromInt_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Int64Range_t2805734248::get_offset_of_toInt_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1886 = { sizeof (CloudRecoAbstractBehaviour_t2501456771), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1886[9] = 
{
	CloudRecoAbstractBehaviour_t2501456771::get_offset_of_mObjectTracker_2(),
	CloudRecoAbstractBehaviour_t2501456771::get_offset_of_mCurrentlyInitializing_3(),
	CloudRecoAbstractBehaviour_t2501456771::get_offset_of_mInitSuccess_4(),
	CloudRecoAbstractBehaviour_t2501456771::get_offset_of_mCloudRecoStarted_5(),
	CloudRecoAbstractBehaviour_t2501456771::get_offset_of_mOnInitializedCalled_6(),
	CloudRecoAbstractBehaviour_t2501456771::get_offset_of_mHandlers_7(),
	CloudRecoAbstractBehaviour_t2501456771::get_offset_of_mTargetFinderStartedBeforeDisable_8(),
	CloudRecoAbstractBehaviour_t2501456771::get_offset_of_AccessKey_9(),
	CloudRecoAbstractBehaviour_t2501456771::get_offset_of_SecretKey_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1887 = { sizeof (ReconstructionImpl_t1634174475), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1887[5] = 
{
	ReconstructionImpl_t1634174475::get_offset_of_mNativeReconstructionPtr_0(),
	ReconstructionImpl_t1634174475::get_offset_of_mMaximumAreaIsSet_1(),
	ReconstructionImpl_t1634174475::get_offset_of_mMaximumArea_2(),
	ReconstructionImpl_t1634174475::get_offset_of_mNavMeshPadding_3(),
	ReconstructionImpl_t1634174475::get_offset_of_mNavMeshUpdatesEnabled_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1888 = { sizeof (HideExcessAreaAbstractBehaviour_t1324413388), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1888[7] = 
{
	HideExcessAreaAbstractBehaviour_t1324413388::get_offset_of_mClippingImpl_2(),
	HideExcessAreaAbstractBehaviour_t1324413388::get_offset_of_mClippingMode_3(),
	HideExcessAreaAbstractBehaviour_t1324413388::get_offset_of_mVuforiaBehaviour_4(),
	HideExcessAreaAbstractBehaviour_t1324413388::get_offset_of_mVideoBgMgr_5(),
	HideExcessAreaAbstractBehaviour_t1324413388::get_offset_of_mPlaneOffset_6(),
	HideExcessAreaAbstractBehaviour_t1324413388::get_offset_of_mSceneScaledDown_7(),
	HideExcessAreaAbstractBehaviour_t1324413388::get_offset_of_mStarted_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1889 = { sizeof (CLIPPING_MODE_t306275072)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1889[4] = 
{
	CLIPPING_MODE_t306275072::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1890 = { sizeof (ObjectTargetImpl_t1592445137), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1890[2] = 
{
	ObjectTargetImpl_t1592445137::get_offset_of_mSize_2(),
	ObjectTargetImpl_t1592445137::get_offset_of_mDataSet_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1891 = { sizeof (UnityPlayer_t3865992016), -1, sizeof(UnityPlayer_t3865992016_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1891[1] = 
{
	UnityPlayer_t3865992016_StaticFields::get_offset_of_sPlayer_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1892 = { sizeof (NullUnityPlayer_t1636390204), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1893 = { sizeof (PlayModeUnityPlayer_t2581155120), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1894 = { sizeof (ReconstructionFromTargetImpl_t886280682), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1894[6] = 
{
	ReconstructionFromTargetImpl_t886280682::get_offset_of_mOccluderMin_5(),
	ReconstructionFromTargetImpl_t886280682::get_offset_of_mOccluderMax_6(),
	ReconstructionFromTargetImpl_t886280682::get_offset_of_mOccluderOffset_7(),
	ReconstructionFromTargetImpl_t886280682::get_offset_of_mOccluderRotation_8(),
	ReconstructionFromTargetImpl_t886280682::get_offset_of_mInitializationTarget_9(),
	ReconstructionFromTargetImpl_t886280682::get_offset_of_mCanAutoSetInitializationTarget_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1895 = { sizeof (Device_t3018687371), -1, sizeof(Device_t3018687371_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1895[1] = 
{
	Device_t3018687371_StaticFields::get_offset_of_mInstance_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1896 = { sizeof (Mode_t1603927093)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1896[3] = 
{
	Mode_t1603927093::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1897 = { sizeof (ViewerButtonType_t783864389)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1897[5] = 
{
	ViewerButtonType_t783864389::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1898 = { sizeof (ViewerTrayAlignment_t2758466794)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1898[4] = 
{
	ViewerTrayAlignment_t2758466794::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1899 = { 0, -1, 0, 0 };
#ifdef __clang__
#pragma clang diagnostic pop
#endif

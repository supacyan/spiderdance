﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "object-internals.h"

// SpiderController
struct SpiderController_t373354123;
// UnityEngine.MonoBehaviour
struct MonoBehaviour_t345688271;
// UnityEngine.Component
struct Component_t4087199522;
// UnityEngine.Rigidbody
struct Rigidbody_t2492269564;
// UnityEngine.Animation
struct Animation_t2605054404;
// System.String
struct String_t;
// UnityEngine.Transform
struct Transform_t3316442598;
// VRIntegrationHelper
struct VRIntegrationHelper_t1870100115;
// UnityEngine.Camera
struct Camera_t3175186167;
// Vuforia.VuforiaARController
struct VuforiaARController_t1328503142;
// System.Action
struct Action_t3619184611;
// Vuforia.DigitalEyewearARController
struct DigitalEyewearARController_t1877756253;
// Vuforia.HideExcessAreaAbstractBehaviour
struct HideExcessAreaAbstractBehaviour_t1324413388;
// Vuforia.BackgroundPlaneBehaviour
struct BackgroundPlaneBehaviour_t311858283;
// Vuforia.BackgroundPlaneAbstractBehaviour
struct BackgroundPlaneAbstractBehaviour_t4259835756;
// UnityEngine.Object
struct Object_t1502412432;
// Vuforia.AndroidUnityPlayer
struct AndroidUnityPlayer_t3571566439;
// Vuforia.VuforiaRenderer
struct VuforiaRenderer_t969713742;
// Vuforia.CloudRecoBehaviour
struct CloudRecoBehaviour_t2989569978;
// Vuforia.CloudRecoAbstractBehaviour
struct CloudRecoAbstractBehaviour_t2501456771;
// Vuforia.ComponentFactoryStarterBehaviour
struct ComponentFactoryStarterBehaviour_t2906678938;
// System.Type
struct Type_t;
// System.Collections.Generic.List`1<System.Reflection.MethodInfo>
struct List_1_t3148643089;
// System.Collections.Generic.IEnumerable`1<System.Reflection.MethodInfo>
struct IEnumerable_1_t2541680021;
// System.Collections.Generic.List`1<System.Object>
struct List_1_t877107497;
// System.Collections.Generic.IEnumerable`1<System.Object>
struct IEnumerable_1_t270144429;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.Delegate
struct Delegate_t537306192;
// Vuforia.VuforiaBehaviourComponentFactory
struct VuforiaBehaviourComponentFactory_t2590131513;
// Vuforia.IBehaviourComponentFactory
struct IBehaviourComponentFactory_t2618036286;
// Vuforia.CylinderTargetBehaviour
struct CylinderTargetBehaviour_t1224832031;
// Vuforia.CylinderTargetAbstractBehaviour
struct CylinderTargetAbstractBehaviour_t2158579059;
// Vuforia.DefaultInitializationErrorHandler
struct DefaultInitializationErrorHandler_t414875900;
// Vuforia.VuforiaRuntime
struct VuforiaRuntime_t2131788666;
// System.Action`1<Vuforia.VuforiaUnity/InitError>
struct Action_1_t3472505684;
// UnityEngine.GUI/WindowFunction
struct WindowFunction_t3577962812;
// Vuforia.DefaultSmartTerrainEventHandler
struct DefaultSmartTerrainEventHandler_t3202897208;
// Vuforia.ReconstructionBehaviour
struct ReconstructionBehaviour_t3936121853;
// System.Action`1<Vuforia.Prop>
struct Action_1_t1560204979;
// System.Action`1<System.Object>
struct Action_1_t2007785632;
// Vuforia.ReconstructionAbstractBehaviour
struct ReconstructionAbstractBehaviour_t954468633;
// System.Action`1<Vuforia.Surface>
struct Action_1_t1020068010;
// Vuforia.Prop
struct Prop_t1574377637;
// Vuforia.PropAbstractBehaviour
struct PropAbstractBehaviour_t2648882166;
// Vuforia.Surface
struct Surface_t1034240668;
// Vuforia.SurfaceAbstractBehaviour
struct SurfaceAbstractBehaviour_t272541176;
// Vuforia.DefaultTrackableEventHandler
struct DefaultTrackableEventHandler_t2331528573;
// Vuforia.TrackableBehaviour
struct TrackableBehaviour_t2419079356;
// Vuforia.ITrackableEventHandler
struct ITrackableEventHandler_t439119523;
// UnityEngine.Renderer[]
struct RendererU5BU5D_t2667410251;
// System.Object[]
struct ObjectU5BU5D_t3270211303;
// UnityEngine.Collider[]
struct ColliderU5BU5D_t3746774603;
// UnityEngine.Renderer
struct Renderer_t1303575294;
// UnityEngine.Collider
struct Collider_t2301021182;
// Vuforia.GLErrorHandler
struct GLErrorHandler_t1216999495;
// Vuforia.HideExcessAreaBehaviour
struct HideExcessAreaBehaviour_t554831333;
// Vuforia.ImageTargetBehaviour
struct ImageTargetBehaviour_t2918114530;
// Vuforia.ImageTargetAbstractBehaviour
struct ImageTargetAbstractBehaviour_t355543434;
// Vuforia.IOSUnityPlayer
struct IOSUnityPlayer_t1005570149;
// Vuforia.VuforiaNativeIosWrapper
struct VuforiaNativeIosWrapper_t49709812;
// Vuforia.IVuforiaWrapper
struct IVuforiaWrapper_t3917309055;
// Vuforia.MaskOutBehaviour
struct MaskOutBehaviour_t62027326;
// Vuforia.MaskOutAbstractBehaviour
struct MaskOutAbstractBehaviour_t2174599097;
// UnityEngine.Material[]
struct MaterialU5BU5D_t1437145498;
// UnityEngine.Material
struct Material_t1079520667;
// Vuforia.MultiTargetBehaviour
struct MultiTargetBehaviour_t687655925;
// Vuforia.MultiTargetAbstractBehaviour
struct MultiTargetAbstractBehaviour_t1527469731;
// Vuforia.ObjectTargetBehaviour
struct ObjectTargetBehaviour_t1761394684;
// Vuforia.ObjectTargetAbstractBehaviour
struct ObjectTargetAbstractBehaviour_t1410714589;
// Vuforia.PropBehaviour
struct PropBehaviour_t3897855688;
// Vuforia.ReconstructionFromTargetBehaviour
struct ReconstructionFromTargetBehaviour_t2819884381;
// Vuforia.ReconstructionFromTargetAbstractBehaviour
struct ReconstructionFromTargetAbstractBehaviour_t2676804737;
// Vuforia.SurfaceBehaviour
struct SurfaceBehaviour_t1975960194;
// Vuforia.TextRecoBehaviour
struct TextRecoBehaviour_t3253755448;
// Vuforia.TextRecoAbstractBehaviour
struct TextRecoAbstractBehaviour_t1751158196;
// Vuforia.TurnOffBehaviour
struct TurnOffBehaviour_t963849236;
// Vuforia.TurnOffAbstractBehaviour
struct TurnOffAbstractBehaviour_t1066217039;
// UnityEngine.MeshRenderer
struct MeshRenderer_t1441648352;
// UnityEngine.MeshFilter
struct MeshFilter_t137687116;
// Vuforia.TurnOffWordBehaviour
struct TurnOffWordBehaviour_t1385869686;
// UnityEngine.GameObject
struct GameObject_t1811656094;
// Vuforia.UserDefinedTargetBuildingBehaviour
struct UserDefinedTargetBuildingBehaviour_t3295846277;
// Vuforia.UserDefinedTargetBuildingAbstractBehaviour
struct UserDefinedTargetBuildingAbstractBehaviour_t2424076393;
// Vuforia.VideoBackgroundBehaviour
struct VideoBackgroundBehaviour_t177581750;
// Vuforia.VideoBackgroundAbstractBehaviour
struct VideoBackgroundAbstractBehaviour_t3867888217;
// Vuforia.VirtualButtonBehaviour
struct VirtualButtonBehaviour_t1019531330;
// Vuforia.VirtualButtonAbstractBehaviour
struct VirtualButtonAbstractBehaviour_t1965820348;
// Vuforia.VuforiaBehaviour
struct VuforiaBehaviour_t834098562;
// Vuforia.VuforiaAbstractBehaviour
struct VuforiaAbstractBehaviour_t153010168;
// Vuforia.WordAbstractBehaviour
struct WordAbstractBehaviour_t2339709601;
// Vuforia.WordBehaviour
struct WordBehaviour_t2625704100;
// Vuforia.VuMarkAbstractBehaviour
struct VuMarkAbstractBehaviour_t2652615854;
// Vuforia.VuMarkBehaviour
struct VuMarkBehaviour_t3122351057;
// Vuforia.VuforiaAbstractConfiguration
struct VuforiaAbstractConfiguration_t974438950;
// Vuforia.VuforiaConfiguration
struct VuforiaConfiguration_t2489601127;
// Vuforia.IUnityPlayer
struct IUnityPlayer_t1272597385;
// Vuforia.VuforiaAbstractConfiguration/GenericVuforiaConfiguration
struct GenericVuforiaConfiguration_t1574456586;
// Vuforia.NullUnityPlayer
struct NullUnityPlayer_t1636390204;
// Vuforia.PlayModeUnityPlayer
struct PlayModeUnityPlayer_t2581155120;
// Vuforia.WSAUnityPlayer
struct WSAUnityPlayer_t1576425251;
// Vuforia.WireframeBehaviour
struct WireframeBehaviour_t1899115476;
// Vuforia.VuforiaManager
struct VuforiaManager_t745032010;
// UnityEngine.Camera[]
struct CameraU5BU5D_t1519774542;
// UnityEngine.Mesh
struct Mesh_t996500909;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t981680887;
// System.Int32[]
struct Int32U5BU5D_t2324750880;
// UnityEngine.Behaviour
struct Behaviour_t363748010;
// Vuforia.WireframeTrackableEventHandler
struct WireframeTrackableEventHandler_t680679714;
// Vuforia.WireframeBehaviour[]
struct WireframeBehaviourU5BU5D_t1741061725;
// System.Reflection.MethodInfo[]
struct MethodInfoU5BU5D_t1774028895;
// System.Char[]
struct CharU5BU5D_t45629911;
// System.Void
struct Void_t2217553113;
// System.DelegateData
struct DelegateData_t3370401926;
// System.Type[]
struct TypeU5BU5D_t3854702846;
// System.Reflection.MemberFilter
struct MemberFilter_t4208935459;
// System.Collections.Generic.List`1<Vuforia.ITrackerEventHandler>
struct List_1_t1947635164;
// System.Collections.Generic.List`1<Vuforia.IVideoBackgroundEventHandler>
struct List_1_t485517045;
// System.Action`1<System.Boolean>
struct Action_1_t2936672411;
// Vuforia.ICameraConfiguration
struct ICameraConfiguration_t3187182883;
// Vuforia.VideoBackgroundManager
struct VideoBackgroundManager_t2068552549;
// System.Collections.Generic.List`1<System.Type>
struct List_1_t3593106846;
// Vuforia.DigitalEyewearARController/SerializableViewerParameters
struct SerializableViewerParameters_t484734121;
// Vuforia.DistortionRenderingBehaviour
struct DistortionRenderingBehaviour_t2785666256;
// System.IAsyncResult
struct IAsyncResult_t1913953129;
// System.AsyncCallback
struct AsyncCallback_t1057242265;
// Vuforia.VuforiaAbstractConfiguration/DigitalEyewearConfiguration
struct DigitalEyewearConfiguration_t3649657388;
// Vuforia.VuforiaAbstractConfiguration/DatabaseLoadConfiguration
struct DatabaseLoadConfiguration_t3827205120;
// Vuforia.VuforiaAbstractConfiguration/VideoBackgroundConfiguration
struct VideoBackgroundConfiguration_t2579534243;
// Vuforia.VuforiaAbstractConfiguration/SmartTerrainTrackerConfiguration
struct SmartTerrainTrackerConfiguration_t657164935;
// Vuforia.VuforiaAbstractConfiguration/DeviceTrackerConfiguration
struct DeviceTrackerConfiguration_t2458088029;
// Vuforia.VuforiaAbstractConfiguration/WebCamConfiguration
struct WebCamConfiguration_t2750821739;
// UnityEngine.Camera/CameraCallback
struct CameraCallback_t832885096;
// System.Action`1<Vuforia.VuforiaAbstractBehaviour>
struct Action_1_t138837510;
// System.Collections.Generic.List`1<Vuforia.ITextRecoEventHandler>
struct List_1_t4098171074;
// Vuforia.Trackable
struct Trackable_t1140696582;
// System.Collections.Generic.List`1<Vuforia.ITrackableEventHandler>
struct List_1_t3589236026;
// System.Collections.Generic.List`1<Vuforia.ISmartTerrainEventHandler>
struct List_1_t3446838688;
// System.Action`1<Vuforia.SmartTerrainInitializationInfo>
struct Action_1_t1488611326;
// Vuforia.Reconstruction
struct Reconstruction_t3574071110;
// System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.Surface>
struct Dictionary_2_t2230247832;
// System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.SurfaceAbstractBehaviour>
struct Dictionary_2_t1468548340;
// System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.Prop>
struct Dictionary_2_t2770384801;
// System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.PropAbstractBehaviour>
struct Dictionary_2_t3844889330;
// Vuforia.ReconstructionFromTarget
struct ReconstructionFromTarget_t1541138710;
// System.Collections.Generic.List`1<Vuforia.IVirtualButtonEventHandler>
struct List_1_t1800411770;
// Vuforia.VirtualButton
struct VirtualButton_t3666937711;
// Vuforia.ObjectTracker
struct ObjectTracker_t4023638979;
// System.Collections.Generic.List`1<Vuforia.IUserDefinedTargetEventHandler>
struct List_1_t4290827367;
// System.Collections.Generic.HashSet`1<UnityEngine.MeshRenderer>
struct HashSet_1_t1919463348;
// Vuforia.IExcessAreaClipping
struct IExcessAreaClipping_t2974524491;
// System.Collections.Generic.List`1<Vuforia.ICloudRecoEventHandler>
struct List_1_t1465274546;
// Vuforia.SmartTerrainTrackable
struct SmartTerrainTrackable_t3286114719;
// UnityEngine.MeshCollider
struct MeshCollider_t4172739389;
// Vuforia.Word
struct Word_t200975816;
// Vuforia.ObjectTarget
struct ObjectTarget_t1833683407;
// UnityEngine.Texture2D
struct Texture2D_t415585320;
// Vuforia.CylinderTarget
struct CylinderTarget_t1924254572;
// Vuforia.VuMarkTemplate
struct VuMarkTemplate_t2238627375;
// Vuforia.VuMarkTarget
struct VuMarkTarget_t4168832550;
// Vuforia.MultiTarget
struct MultiTarget_t114888232;
// UnityEngine.BoxCollider
struct BoxCollider_t1861275623;
// Vuforia.ImageTarget
struct ImageTarget_t193025956;
// System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VirtualButtonAbstractBehaviour>
struct Dictionary_2_t3161827512;

extern const RuntimeMethod* Component_GetComponent_TisRigidbody_t2492269564_m3707284554_RuntimeMethod_var;
extern const RuntimeMethod* Component_GetComponent_TisAnimation_t2605054404_m2540927662_RuntimeMethod_var;
extern const uint32_t SpiderController_Start_m602803498_MetadataUsageId;
extern RuntimeClass* CrossPlatformInputManager_t2677747245_il2cpp_TypeInfo_var;
extern RuntimeClass* Vector3_t2903530434_il2cpp_TypeInfo_var;
extern RuntimeClass* Mathf_t4094287654_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral224118652;
extern Il2CppCodeGenString* _stringLiteral3600647860;
extern Il2CppCodeGenString* _stringLiteral1036232234;
extern Il2CppCodeGenString* _stringLiteral1625232036;
extern const uint32_t SpiderController_Update_m403246748_MetadataUsageId;
extern const RuntimeMethod* Component_GetComponent_TisCamera_t3175186167_m3957679396_RuntimeMethod_var;
extern const uint32_t VRIntegrationHelper_Awake_m2060921871_MetadataUsageId;
extern RuntimeClass* VuforiaARController_t1328503142_il2cpp_TypeInfo_var;
extern RuntimeClass* Action_t3619184611_il2cpp_TypeInfo_var;
extern const RuntimeMethod* VRIntegrationHelper_OnVuforiaStarted_m714206851_RuntimeMethod_var;
extern const uint32_t VRIntegrationHelper_Start_m1782124057_MetadataUsageId;
extern RuntimeClass* DigitalEyewearARController_t1877756253_il2cpp_TypeInfo_var;
extern RuntimeClass* VRIntegrationHelper_t1870100115_il2cpp_TypeInfo_var;
extern const RuntimeMethod* Component_GetComponent_TisHideExcessAreaAbstractBehaviour_t1324413388_m2951051384_RuntimeMethod_var;
extern const uint32_t VRIntegrationHelper_OnVuforiaStarted_m714206851_MetadataUsageId;
extern RuntimeClass* Object_t1502412432_il2cpp_TypeInfo_var;
extern const RuntimeMethod* Component_GetComponentInChildren_TisBackgroundPlaneBehaviour_t311858283_m2328091248_RuntimeMethod_var;
extern const uint32_t VRIntegrationHelper_LateUpdate_m4260068054_MetadataUsageId;
extern RuntimeClass* VuforiaRuntimeUtilities_t1036756025_il2cpp_TypeInfo_var;
extern const uint32_t VRIntegrationHelper_OnPreRender_m857326075_MetadataUsageId;
extern RuntimeClass* VuforiaRenderer_t969713742_il2cpp_TypeInfo_var;
extern const uint32_t AndroidUnityPlayer_InitializeVuforia_m1842234586_MetadataUsageId;
extern RuntimeClass* SurfaceUtilities_t184061569_il2cpp_TypeInfo_var;
extern const uint32_t AndroidUnityPlayer_Update_m829520880_MetadataUsageId;
extern RuntimeClass* VuforiaUnity_t3212967305_il2cpp_TypeInfo_var;
extern const uint32_t AndroidUnityPlayer_OnPause_m3448964043_MetadataUsageId;
extern const uint32_t AndroidUnityPlayer_OnResume_m3272565274_MetadataUsageId;
extern const uint32_t AndroidUnityPlayer_OnDestroy_m2974002145_MetadataUsageId;
extern const uint32_t AndroidUnityPlayer_InitializeSurface_m325648367_MetadataUsageId;
extern const uint32_t AndroidUnityPlayer_CheckOrientation_m2694551379_MetadataUsageId;
extern RuntimeClass* BackgroundPlaneAbstractBehaviour_t4259835756_il2cpp_TypeInfo_var;
extern const uint32_t BackgroundPlaneBehaviour__ctor_m3613974601_MetadataUsageId;
extern const RuntimeType* Action_t3619184611_0_0_0_var;
extern RuntimeClass* Attribute_t841672618_il2cpp_TypeInfo_var;
extern RuntimeClass* FactorySetter_t372426630_il2cpp_TypeInfo_var;
extern RuntimeClass* Type_t_il2cpp_TypeInfo_var;
extern const RuntimeMethod* Enumerable_ToList_TisMethodInfo_t_m2942684005_RuntimeMethod_var;
extern const RuntimeMethod* List_1_AddRange_m2579895835_RuntimeMethod_var;
extern const RuntimeMethod* List_1_GetEnumerator_m3353760919_RuntimeMethod_var;
extern const RuntimeMethod* Enumerator_get_Current_m1308925728_RuntimeMethod_var;
extern const RuntimeMethod* Enumerator_MoveNext_m1576165614_RuntimeMethod_var;
extern const RuntimeMethod* Enumerator_Dispose_m363041641_RuntimeMethod_var;
extern const uint32_t ComponentFactoryStarterBehaviour_Awake_m3129066026_MetadataUsageId;
extern RuntimeClass* Debug_t3525713211_il2cpp_TypeInfo_var;
extern RuntimeClass* VuforiaBehaviourComponentFactory_t2590131513_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2946044379;
extern const uint32_t ComponentFactoryStarterBehaviour_SetBehaviourComponentFactory_m3084697717_MetadataUsageId;
extern RuntimeClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t DefaultInitializationErrorHandler__ctor_m1788101465_MetadataUsageId;
extern RuntimeClass* VuforiaRuntime_t2131788666_il2cpp_TypeInfo_var;
extern RuntimeClass* Action_1_t3472505684_il2cpp_TypeInfo_var;
extern const RuntimeMethod* DefaultInitializationErrorHandler_OnVuforiaInitializationError_m1077570325_RuntimeMethod_var;
extern const RuntimeMethod* Action_1__ctor_m1316558631_RuntimeMethod_var;
extern const uint32_t DefaultInitializationErrorHandler_Awake_m1234883766_MetadataUsageId;
extern RuntimeClass* WindowFunction_t3577962812_il2cpp_TypeInfo_var;
extern RuntimeClass* GUI_t1999344246_il2cpp_TypeInfo_var;
extern const RuntimeMethod* DefaultInitializationErrorHandler_DrawWindowContent_m704677827_RuntimeMethod_var;
extern Il2CppCodeGenString* _stringLiteral3886165719;
extern const uint32_t DefaultInitializationErrorHandler_OnGUI_m2401365285_MetadataUsageId;
extern const uint32_t DefaultInitializationErrorHandler_OnDestroy_m3701173763_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral3882266322;
extern const uint32_t DefaultInitializationErrorHandler_DrawWindowContent_m704677827_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral1755214121;
extern Il2CppCodeGenString* _stringLiteral1508901986;
extern Il2CppCodeGenString* _stringLiteral2113730205;
extern Il2CppCodeGenString* _stringLiteral1527512223;
extern Il2CppCodeGenString* _stringLiteral1250240110;
extern Il2CppCodeGenString* _stringLiteral131575613;
extern Il2CppCodeGenString* _stringLiteral1916831050;
extern Il2CppCodeGenString* _stringLiteral1517763141;
extern Il2CppCodeGenString* _stringLiteral1236998107;
extern Il2CppCodeGenString* _stringLiteral251546322;
extern Il2CppCodeGenString* _stringLiteral4244220307;
extern const uint32_t DefaultInitializationErrorHandler_SetErrorCode_m749491405_MetadataUsageId;
extern RuntimeClass* Action_1_t1560204979_il2cpp_TypeInfo_var;
extern RuntimeClass* Action_1_t1020068010_il2cpp_TypeInfo_var;
extern const RuntimeMethod* Component_GetComponent_TisReconstructionBehaviour_t3936121853_m3652591174_RuntimeMethod_var;
extern const RuntimeMethod* DefaultSmartTerrainEventHandler_OnPropCreated_m2829740123_RuntimeMethod_var;
extern const RuntimeMethod* Action_1__ctor_m220035642_RuntimeMethod_var;
extern const RuntimeMethod* DefaultSmartTerrainEventHandler_OnSurfaceCreated_m2986227878_RuntimeMethod_var;
extern const RuntimeMethod* Action_1__ctor_m59453672_RuntimeMethod_var;
extern const uint32_t DefaultSmartTerrainEventHandler_Start_m4196669513_MetadataUsageId;
extern const uint32_t DefaultSmartTerrainEventHandler_OnDestroy_m3748050755_MetadataUsageId;
extern const uint32_t DefaultSmartTerrainEventHandler_OnPropCreated_m2829740123_MetadataUsageId;
extern const uint32_t DefaultSmartTerrainEventHandler_OnSurfaceCreated_m2986227878_MetadataUsageId;
extern const RuntimeMethod* Component_GetComponent_TisTrackableBehaviour_t2419079356_m3640808561_RuntimeMethod_var;
extern const uint32_t DefaultTrackableEventHandler_Start_m3092614202_MetadataUsageId;
extern const RuntimeMethod* Component_GetComponentsInChildren_TisRenderer_t1303575294_m3094251748_RuntimeMethod_var;
extern const RuntimeMethod* Component_GetComponentsInChildren_TisCollider_t2301021182_m857431636_RuntimeMethod_var;
extern Il2CppCodeGenString* _stringLiteral2756122832;
extern Il2CppCodeGenString* _stringLiteral3913380118;
extern const uint32_t DefaultTrackableEventHandler_OnTrackingFound_m3375166525_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral2566987390;
extern const uint32_t DefaultTrackableEventHandler_OnTrackingLost_m3475317276_MetadataUsageId;
extern RuntimeClass* GLErrorHandler_t1216999495_il2cpp_TypeInfo_var;
extern const uint32_t GLErrorHandler_SetError_m2768535324_MetadataUsageId;
extern const RuntimeMethod* GLErrorHandler_DrawWindowContent_m4286681250_RuntimeMethod_var;
extern Il2CppCodeGenString* _stringLiteral370773887;
extern const uint32_t GLErrorHandler_OnGUI_m3781023102_MetadataUsageId;
extern const uint32_t GLErrorHandler_DrawWindowContent_m4286681250_MetadataUsageId;
extern const uint32_t GLErrorHandler__cctor_m3154199897_MetadataUsageId;
extern RuntimeClass* VuforiaNativeIosWrapper_t49709812_il2cpp_TypeInfo_var;
extern RuntimeClass* VuforiaWrapper_t3746480205_il2cpp_TypeInfo_var;
extern const uint32_t IOSUnityPlayer_LoadNativeLibraries_m2693898471_MetadataUsageId;
extern const uint32_t IOSUnityPlayer_InitializeVuforia_m1462137961_MetadataUsageId;
extern const uint32_t IOSUnityPlayer_Update_m2549520222_MetadataUsageId;
extern const uint32_t IOSUnityPlayer_OnPause_m632510427_MetadataUsageId;
extern const uint32_t IOSUnityPlayer_OnResume_m4077853536_MetadataUsageId;
extern const uint32_t IOSUnityPlayer_OnDestroy_m1412385811_MetadataUsageId;
extern const uint32_t IOSUnityPlayer_InitializeSurface_m1138500967_MetadataUsageId;
extern const uint32_t IOSUnityPlayer_SetUnityScreenOrientation_m3334399535_MetadataUsageId;
extern RuntimeClass* MaterialU5BU5D_t1437145498_il2cpp_TypeInfo_var;
extern const RuntimeMethod* Component_GetComponent_TisRenderer_t1303575294_m1648558159_RuntimeMethod_var;
extern const uint32_t MaskOutBehaviour_Start_m1067981915_MetadataUsageId;
extern const RuntimeMethod* Component_GetComponent_TisMeshRenderer_t1441648352_m3769347907_RuntimeMethod_var;
extern const RuntimeMethod* Component_GetComponent_TisMeshFilter_t137687116_m62619598_RuntimeMethod_var;
extern const uint32_t TurnOffBehaviour_Awake_m2199196731_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral3274049941;
extern const uint32_t TurnOffWordBehaviour_Awake_m2127443610_MetadataUsageId;
extern RuntimeClass* VideoBackgroundAbstractBehaviour_t3867888217_il2cpp_TypeInfo_var;
extern const uint32_t VideoBackgroundBehaviour__ctor_m4180776799_MetadataUsageId;
extern const RuntimeMethod* GameObject_AddComponent_TisComponentFactoryStarterBehaviour_t2906678938_m2160705532_RuntimeMethod_var;
extern const uint32_t VuforiaBehaviour_Awake_m4269404237_MetadataUsageId;
extern RuntimeClass* VuforiaBehaviour_t834098562_il2cpp_TypeInfo_var;
extern const RuntimeMethod* Object_FindObjectOfType_TisVuforiaBehaviour_t834098562_m3072761646_RuntimeMethod_var;
extern const uint32_t VuforiaBehaviour_get_Instance_m621388178_MetadataUsageId;
extern const RuntimeMethod* GameObject_AddComponent_TisMaskOutBehaviour_t62027326_m1393573829_RuntimeMethod_var;
extern const uint32_t VuforiaBehaviourComponentFactory_AddMaskOutBehaviour_m3026848378_MetadataUsageId;
extern const RuntimeMethod* GameObject_AddComponent_TisVirtualButtonBehaviour_t1019531330_m1545170832_RuntimeMethod_var;
extern const uint32_t VuforiaBehaviourComponentFactory_AddVirtualButtonBehaviour_m3283698660_MetadataUsageId;
extern const RuntimeMethod* GameObject_AddComponent_TisTurnOffBehaviour_t963849236_m3647489448_RuntimeMethod_var;
extern const uint32_t VuforiaBehaviourComponentFactory_AddTurnOffBehaviour_m1098537926_MetadataUsageId;
extern const RuntimeMethod* GameObject_AddComponent_TisImageTargetBehaviour_t2918114530_m3204365795_RuntimeMethod_var;
extern const uint32_t VuforiaBehaviourComponentFactory_AddImageTargetBehaviour_m658548697_MetadataUsageId;
extern const RuntimeMethod* GameObject_AddComponent_TisMultiTargetBehaviour_t687655925_m601583604_RuntimeMethod_var;
extern const uint32_t VuforiaBehaviourComponentFactory_AddMultiTargetBehaviour_m3722869120_MetadataUsageId;
extern const RuntimeMethod* GameObject_AddComponent_TisCylinderTargetBehaviour_t1224832031_m4238243230_RuntimeMethod_var;
extern const uint32_t VuforiaBehaviourComponentFactory_AddCylinderTargetBehaviour_m4246728464_MetadataUsageId;
extern const RuntimeMethod* GameObject_AddComponent_TisWordBehaviour_t2625704100_m1670100688_RuntimeMethod_var;
extern const uint32_t VuforiaBehaviourComponentFactory_AddWordBehaviour_m2557318930_MetadataUsageId;
extern const RuntimeMethod* GameObject_AddComponent_TisTextRecoBehaviour_t3253755448_m1170737699_RuntimeMethod_var;
extern const uint32_t VuforiaBehaviourComponentFactory_AddTextRecoBehaviour_m2362397526_MetadataUsageId;
extern const RuntimeMethod* GameObject_AddComponent_TisObjectTargetBehaviour_t1761394684_m3858239762_RuntimeMethod_var;
extern const uint32_t VuforiaBehaviourComponentFactory_AddObjectTargetBehaviour_m4276558463_MetadataUsageId;
extern const RuntimeMethod* GameObject_AddComponent_TisVuMarkBehaviour_t3122351057_m2352772220_RuntimeMethod_var;
extern const uint32_t VuforiaBehaviourComponentFactory_AddVuMarkBehaviour_m1094247509_MetadataUsageId;
extern const RuntimeMethod* ScriptableObject_CreateInstance_TisVuforiaConfiguration_t2489601127_m4156053030_RuntimeMethod_var;
extern const uint32_t VuforiaBehaviourComponentFactory_CreateVuforiaConfiguration_m4138783976_MetadataUsageId;
extern RuntimeClass* VuforiaAbstractConfiguration_t974438950_il2cpp_TypeInfo_var;
extern const uint32_t VuforiaConfiguration__ctor_m1559832131_MetadataUsageId;
extern const uint32_t VuforiaRuntimeInitialization_InitPlatform_m1082759092_MetadataUsageId;
extern const uint32_t VuforiaRuntimeInitialization_InitVuforia_m609753717_MetadataUsageId;
extern RuntimeClass* NullUnityPlayer_t1636390204_il2cpp_TypeInfo_var;
extern RuntimeClass* AndroidUnityPlayer_t3571566439_il2cpp_TypeInfo_var;
extern RuntimeClass* IOSUnityPlayer_t1005570149_il2cpp_TypeInfo_var;
extern RuntimeClass* PlayModeUnityPlayer_t2581155120_il2cpp_TypeInfo_var;
extern RuntimeClass* WSAUnityPlayer_t1576425251_il2cpp_TypeInfo_var;
extern const uint32_t VuforiaRuntimeInitialization_CreateUnityPlayer_m667590709_MetadataUsageId;
extern RuntimeClass* Material_t1079520667_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3620006369;
extern const uint32_t WireframeBehaviour_Start_m3315478633_MetadataUsageId;
extern RuntimeClass* VuforiaManager_t745032010_il2cpp_TypeInfo_var;
extern const RuntimeMethod* GameObject_GetComponentsInChildren_TisCamera_t3175186167_m4240236485_RuntimeMethod_var;
extern Il2CppCodeGenString* _stringLiteral2847251467;
extern const uint32_t WireframeBehaviour_OnRenderObject_m2382856100_MetadataUsageId;
extern RuntimeClass* Matrix4x4_t4266809202_il2cpp_TypeInfo_var;
extern const uint32_t WireframeBehaviour_OnDrawGizmos_m2104809653_MetadataUsageId;
extern const uint32_t WireframeTrackableEventHandler_Start_m3884813205_MetadataUsageId;
extern const RuntimeMethod* Component_GetComponentsInChildren_TisWireframeBehaviour_t1899115476_m606499660_RuntimeMethod_var;
extern const uint32_t WireframeTrackableEventHandler_OnTrackingFound_m2916522252_MetadataUsageId;
extern const uint32_t WireframeTrackableEventHandler_OnTrackingLost_m1252073333_MetadataUsageId;
extern const uint32_t WSAUnityPlayer_Update_m911514441_MetadataUsageId;
extern const uint32_t WSAUnityPlayer_OnPause_m2169356505_MetadataUsageId;
extern const uint32_t WSAUnityPlayer_OnResume_m1070021309_MetadataUsageId;
extern const uint32_t WSAUnityPlayer_OnDestroy_m3442614810_MetadataUsageId;
extern const uint32_t WSAUnityPlayer_InitializeSurface_m3924250633_MetadataUsageId;
extern const uint32_t WSAUnityPlayer_SetUnityScreenOrientation_m2430850923_MetadataUsageId;
extern RuntimeClass* Input_t3439709668_il2cpp_TypeInfo_var;
extern const uint32_t WSAUnityPlayer_GetActualScreenOrientation_m436681916_MetadataUsageId;

struct ObjectU5BU5D_t3270211303;
struct MethodInfoU5BU5D_t1774028895;
struct RendererU5BU5D_t2667410251;
struct ColliderU5BU5D_t3746774603;
struct MaterialU5BU5D_t1437145498;
struct CameraU5BU5D_t1519774542;
struct Vector3U5BU5D_t981680887;
struct Int32U5BU5D_t2324750880;
struct WireframeBehaviourU5BU5D_t1741061725;


#ifndef U3CMODULEU3E_T160650892_H
#define U3CMODULEU3E_T160650892_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t160650892 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T160650892_H
#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
struct Il2CppArrayBounds;
#ifndef RUNTIMEARRAY_H
#define RUNTIMEARRAY_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Array

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEARRAY_H
#ifndef LIST_1_T3148643089_H
#define LIST_1_T3148643089_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1<System.Reflection.MethodInfo>
struct  List_1_t3148643089  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	MethodInfoU5BU5D_t1774028895* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t3148643089, ____items_1)); }
	inline MethodInfoU5BU5D_t1774028895* get__items_1() const { return ____items_1; }
	inline MethodInfoU5BU5D_t1774028895** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(MethodInfoU5BU5D_t1774028895* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((&____items_1), value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t3148643089, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t3148643089, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}
};

struct List_1_t3148643089_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::EmptyArray
	MethodInfoU5BU5D_t1774028895* ___EmptyArray_4;

public:
	inline static int32_t get_offset_of_EmptyArray_4() { return static_cast<int32_t>(offsetof(List_1_t3148643089_StaticFields, ___EmptyArray_4)); }
	inline MethodInfoU5BU5D_t1774028895* get_EmptyArray_4() const { return ___EmptyArray_4; }
	inline MethodInfoU5BU5D_t1774028895** get_address_of_EmptyArray_4() { return &___EmptyArray_4; }
	inline void set_EmptyArray_4(MethodInfoU5BU5D_t1774028895* value)
	{
		___EmptyArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIST_1_T3148643089_H
#ifndef ATTRIBUTE_T841672618_H
#define ATTRIBUTE_T841672618_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Attribute
struct  Attribute_t841672618  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ATTRIBUTE_T841672618_H
#ifndef MEMBERINFO_T_H
#define MEMBERINFO_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.MemberInfo
struct  MemberInfo_t  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MEMBERINFO_T_H
#ifndef VUFORIABEHAVIOURCOMPONENTFACTORY_T2590131513_H
#define VUFORIABEHAVIOURCOMPONENTFACTORY_T2590131513_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.VuforiaBehaviourComponentFactory
struct  VuforiaBehaviourComponentFactory_t2590131513  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VUFORIABEHAVIOURCOMPONENTFACTORY_T2590131513_H
#ifndef VUFORIANATIVEIOSWRAPPER_T49709812_H
#define VUFORIANATIVEIOSWRAPPER_T49709812_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.VuforiaNativeIosWrapper
struct  VuforiaNativeIosWrapper_t49709812  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VUFORIANATIVEIOSWRAPPER_T49709812_H
#ifndef VUFORIARUNTIMEINITIALIZATION_T2078524236_H
#define VUFORIARUNTIMEINITIALIZATION_T2078524236_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.VuforiaRuntimeInitialization
struct  VuforiaRuntimeInitialization_t2078524236  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VUFORIARUNTIMEINITIALIZATION_T2078524236_H
#ifndef VUFORIARENDERER_T969713742_H
#define VUFORIARENDERER_T969713742_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.VuforiaRenderer
struct  VuforiaRenderer_t969713742  : public RuntimeObject
{
public:

public:
};

struct VuforiaRenderer_t969713742_StaticFields
{
public:
	// Vuforia.VuforiaRenderer Vuforia.VuforiaRenderer::sInstance
	VuforiaRenderer_t969713742 * ___sInstance_0;

public:
	inline static int32_t get_offset_of_sInstance_0() { return static_cast<int32_t>(offsetof(VuforiaRenderer_t969713742_StaticFields, ___sInstance_0)); }
	inline VuforiaRenderer_t969713742 * get_sInstance_0() const { return ___sInstance_0; }
	inline VuforiaRenderer_t969713742 ** get_address_of_sInstance_0() { return &___sInstance_0; }
	inline void set_sInstance_0(VuforiaRenderer_t969713742 * value)
	{
		___sInstance_0 = value;
		Il2CppCodeGenWriteBarrier((&___sInstance_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VUFORIARENDERER_T969713742_H
#ifndef NULLUNITYPLAYER_T1636390204_H
#define NULLUNITYPLAYER_T1636390204_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.NullUnityPlayer
struct  NullUnityPlayer_t1636390204  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLUNITYPLAYER_T1636390204_H
#ifndef STRING_T_H
#define STRING_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.String
struct  String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::length
	int32_t ___length_0;
	// System.Char System.String::start_char
	Il2CppChar ___start_char_1;

public:
	inline static int32_t get_offset_of_length_0() { return static_cast<int32_t>(offsetof(String_t, ___length_0)); }
	inline int32_t get_length_0() const { return ___length_0; }
	inline int32_t* get_address_of_length_0() { return &___length_0; }
	inline void set_length_0(int32_t value)
	{
		___length_0 = value;
	}

	inline static int32_t get_offset_of_start_char_1() { return static_cast<int32_t>(offsetof(String_t, ___start_char_1)); }
	inline Il2CppChar get_start_char_1() const { return ___start_char_1; }
	inline Il2CppChar* get_address_of_start_char_1() { return &___start_char_1; }
	inline void set_start_char_1(Il2CppChar value)
	{
		___start_char_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_2;
	// System.Char[] System.String::WhiteChars
	CharU5BU5D_t45629911* ___WhiteChars_3;

public:
	inline static int32_t get_offset_of_Empty_2() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_2)); }
	inline String_t* get_Empty_2() const { return ___Empty_2; }
	inline String_t** get_address_of_Empty_2() { return &___Empty_2; }
	inline void set_Empty_2(String_t* value)
	{
		___Empty_2 = value;
		Il2CppCodeGenWriteBarrier((&___Empty_2), value);
	}

	inline static int32_t get_offset_of_WhiteChars_3() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___WhiteChars_3)); }
	inline CharU5BU5D_t45629911* get_WhiteChars_3() const { return ___WhiteChars_3; }
	inline CharU5BU5D_t45629911** get_address_of_WhiteChars_3() { return &___WhiteChars_3; }
	inline void set_WhiteChars_3(CharU5BU5D_t45629911* value)
	{
		___WhiteChars_3 = value;
		Il2CppCodeGenWriteBarrier((&___WhiteChars_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STRING_T_H
#ifndef VUFORIAMANAGER_T745032010_H
#define VUFORIAMANAGER_T745032010_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.VuforiaManager
struct  VuforiaManager_t745032010  : public RuntimeObject
{
public:

public:
};

struct VuforiaManager_t745032010_StaticFields
{
public:
	// Vuforia.VuforiaManager Vuforia.VuforiaManager::sInstance
	VuforiaManager_t745032010 * ___sInstance_0;

public:
	inline static int32_t get_offset_of_sInstance_0() { return static_cast<int32_t>(offsetof(VuforiaManager_t745032010_StaticFields, ___sInstance_0)); }
	inline VuforiaManager_t745032010 * get_sInstance_0() const { return ___sInstance_0; }
	inline VuforiaManager_t745032010 ** get_address_of_sInstance_0() { return &___sInstance_0; }
	inline void set_sInstance_0(VuforiaManager_t745032010 * value)
	{
		___sInstance_0 = value;
		Il2CppCodeGenWriteBarrier((&___sInstance_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VUFORIAMANAGER_T745032010_H
#ifndef VALUETYPE_T1306072465_H
#define VALUETYPE_T1306072465_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t1306072465  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t1306072465_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t1306072465_marshaled_com
{
};
#endif // VALUETYPE_T1306072465_H
#ifndef ARCONTROLLER_T1205915989_H
#define ARCONTROLLER_T1205915989_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.ARController
struct  ARController_t1205915989  : public RuntimeObject
{
public:
	// Vuforia.VuforiaAbstractBehaviour Vuforia.ARController::mVuforiaBehaviour
	VuforiaAbstractBehaviour_t153010168 * ___mVuforiaBehaviour_0;

public:
	inline static int32_t get_offset_of_mVuforiaBehaviour_0() { return static_cast<int32_t>(offsetof(ARController_t1205915989, ___mVuforiaBehaviour_0)); }
	inline VuforiaAbstractBehaviour_t153010168 * get_mVuforiaBehaviour_0() const { return ___mVuforiaBehaviour_0; }
	inline VuforiaAbstractBehaviour_t153010168 ** get_address_of_mVuforiaBehaviour_0() { return &___mVuforiaBehaviour_0; }
	inline void set_mVuforiaBehaviour_0(VuforiaAbstractBehaviour_t153010168 * value)
	{
		___mVuforiaBehaviour_0 = value;
		Il2CppCodeGenWriteBarrier((&___mVuforiaBehaviour_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARCONTROLLER_T1205915989_H
#ifndef PLAYMODEUNITYPLAYER_T2581155120_H
#define PLAYMODEUNITYPLAYER_T2581155120_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.PlayModeUnityPlayer
struct  PlayModeUnityPlayer_t2581155120  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYMODEUNITYPLAYER_T2581155120_H
#ifndef COLOR_T2507026410_H
#define COLOR_T2507026410_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color
struct  Color_t2507026410 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t2507026410, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t2507026410, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t2507026410, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t2507026410, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR_T2507026410_H
#ifndef FACTORYSETTER_T372426630_H
#define FACTORYSETTER_T372426630_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.FactorySetter
struct  FactorySetter_t372426630  : public Attribute_t841672618
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FACTORYSETTER_T372426630_H
#ifndef ENUM_T978401890_H
#define ENUM_T978401890_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t978401890  : public ValueType_t1306072465
{
public:

public:
};

struct Enum_t978401890_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t45629911* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t978401890_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t45629911* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t45629911** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t45629911* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t978401890_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t978401890_marshaled_com
{
};
#endif // ENUM_T978401890_H
#ifndef METHODBASE_T3686601602_H
#define METHODBASE_T3686601602_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.MethodBase
struct  MethodBase_t3686601602  : public MemberInfo_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // METHODBASE_T3686601602_H
#ifndef VECTOR2_T3577333262_H
#define VECTOR2_T3577333262_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2
struct  Vector2_t3577333262 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_t3577333262, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_t3577333262, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_t3577333262_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_t3577333262  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_t3577333262  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_t3577333262  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_t3577333262  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_t3577333262  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_t3577333262  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_t3577333262  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_t3577333262  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_t3577333262_StaticFields, ___zeroVector_2)); }
	inline Vector2_t3577333262  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_t3577333262 * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_t3577333262  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_t3577333262_StaticFields, ___oneVector_3)); }
	inline Vector2_t3577333262  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_t3577333262 * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_t3577333262  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_t3577333262_StaticFields, ___upVector_4)); }
	inline Vector2_t3577333262  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_t3577333262 * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_t3577333262  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_t3577333262_StaticFields, ___downVector_5)); }
	inline Vector2_t3577333262  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_t3577333262 * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_t3577333262  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_t3577333262_StaticFields, ___leftVector_6)); }
	inline Vector2_t3577333262  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_t3577333262 * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_t3577333262  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_t3577333262_StaticFields, ___rightVector_7)); }
	inline Vector2_t3577333262  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_t3577333262 * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_t3577333262  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_t3577333262_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_t3577333262  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_t3577333262 * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_t3577333262  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_t3577333262_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_t3577333262  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_t3577333262 * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_t3577333262  value)
	{
		___negativeInfinityVector_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_T3577333262_H
#ifndef ENUMERATOR_T2954986095_H
#define ENUMERATOR_T2954986095_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1/Enumerator<System.Object>
struct  Enumerator_t2954986095 
{
public:
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator::l
	List_1_t877107497 * ___l_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::next
	int32_t ___next_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::ver
	int32_t ___ver_2;
	// T System.Collections.Generic.List`1/Enumerator::current
	RuntimeObject * ___current_3;

public:
	inline static int32_t get_offset_of_l_0() { return static_cast<int32_t>(offsetof(Enumerator_t2954986095, ___l_0)); }
	inline List_1_t877107497 * get_l_0() const { return ___l_0; }
	inline List_1_t877107497 ** get_address_of_l_0() { return &___l_0; }
	inline void set_l_0(List_1_t877107497 * value)
	{
		___l_0 = value;
		Il2CppCodeGenWriteBarrier((&___l_0), value);
	}

	inline static int32_t get_offset_of_next_1() { return static_cast<int32_t>(offsetof(Enumerator_t2954986095, ___next_1)); }
	inline int32_t get_next_1() const { return ___next_1; }
	inline int32_t* get_address_of_next_1() { return &___next_1; }
	inline void set_next_1(int32_t value)
	{
		___next_1 = value;
	}

	inline static int32_t get_offset_of_ver_2() { return static_cast<int32_t>(offsetof(Enumerator_t2954986095, ___ver_2)); }
	inline int32_t get_ver_2() const { return ___ver_2; }
	inline int32_t* get_address_of_ver_2() { return &___ver_2; }
	inline void set_ver_2(int32_t value)
	{
		___ver_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_t2954986095, ___current_3)); }
	inline RuntimeObject * get_current_3() const { return ___current_3; }
	inline RuntimeObject ** get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(RuntimeObject * value)
	{
		___current_3 = value;
		Il2CppCodeGenWriteBarrier((&___current_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMERATOR_T2954986095_H
#ifndef ENUMERATOR_T931554391_H
#define ENUMERATOR_T931554391_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1/Enumerator<System.Reflection.MethodInfo>
struct  Enumerator_t931554391 
{
public:
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator::l
	List_1_t3148643089 * ___l_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::next
	int32_t ___next_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::ver
	int32_t ___ver_2;
	// T System.Collections.Generic.List`1/Enumerator::current
	MethodInfo_t * ___current_3;

public:
	inline static int32_t get_offset_of_l_0() { return static_cast<int32_t>(offsetof(Enumerator_t931554391, ___l_0)); }
	inline List_1_t3148643089 * get_l_0() const { return ___l_0; }
	inline List_1_t3148643089 ** get_address_of_l_0() { return &___l_0; }
	inline void set_l_0(List_1_t3148643089 * value)
	{
		___l_0 = value;
		Il2CppCodeGenWriteBarrier((&___l_0), value);
	}

	inline static int32_t get_offset_of_next_1() { return static_cast<int32_t>(offsetof(Enumerator_t931554391, ___next_1)); }
	inline int32_t get_next_1() const { return ___next_1; }
	inline int32_t* get_address_of_next_1() { return &___next_1; }
	inline void set_next_1(int32_t value)
	{
		___next_1 = value;
	}

	inline static int32_t get_offset_of_ver_2() { return static_cast<int32_t>(offsetof(Enumerator_t931554391, ___ver_2)); }
	inline int32_t get_ver_2() const { return ___ver_2; }
	inline int32_t* get_address_of_ver_2() { return &___ver_2; }
	inline void set_ver_2(int32_t value)
	{
		___ver_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_t931554391, ___current_3)); }
	inline MethodInfo_t * get_current_3() const { return ___current_3; }
	inline MethodInfo_t ** get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(MethodInfo_t * value)
	{
		___current_3 = value;
		Il2CppCodeGenWriteBarrier((&___current_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMERATOR_T931554391_H
#ifndef INT32_T4237944589_H
#define INT32_T4237944589_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Int32
struct  Int32_t4237944589 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Int32_t4237944589, ___m_value_2)); }
	inline int32_t get_m_value_2() const { return ___m_value_2; }
	inline int32_t* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(int32_t value)
	{
		___m_value_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT32_T4237944589_H
#ifndef VEC2I_T895597728_H
#define VEC2I_T895597728_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.VuforiaRenderer/Vec2I
#pragma pack(push, tp, 1)
struct  Vec2I_t895597728 
{
public:
	// System.Int32 Vuforia.VuforiaRenderer/Vec2I::x
	int32_t ___x_0;
	// System.Int32 Vuforia.VuforiaRenderer/Vec2I::y
	int32_t ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vec2I_t895597728, ___x_0)); }
	inline int32_t get_x_0() const { return ___x_0; }
	inline int32_t* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(int32_t value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vec2I_t895597728, ___y_1)); }
	inline int32_t get_y_1() const { return ___y_1; }
	inline int32_t* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(int32_t value)
	{
		___y_1 = value;
	}
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VEC2I_T895597728_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	IntPtr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline IntPtr_t get_Zero_1() const { return ___Zero_1; }
	inline IntPtr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(IntPtr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef BOOLEAN_T2950845069_H
#define BOOLEAN_T2950845069_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Boolean
struct  Boolean_t2950845069 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Boolean_t2950845069, ___m_value_2)); }
	inline bool get_m_value_2() const { return ___m_value_2; }
	inline bool* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(bool value)
	{
		___m_value_2 = value;
	}
};

struct Boolean_t2950845069_StaticFields
{
public:
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_0;
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_1;

public:
	inline static int32_t get_offset_of_FalseString_0() { return static_cast<int32_t>(offsetof(Boolean_t2950845069_StaticFields, ___FalseString_0)); }
	inline String_t* get_FalseString_0() const { return ___FalseString_0; }
	inline String_t** get_address_of_FalseString_0() { return &___FalseString_0; }
	inline void set_FalseString_0(String_t* value)
	{
		___FalseString_0 = value;
		Il2CppCodeGenWriteBarrier((&___FalseString_0), value);
	}

	inline static int32_t get_offset_of_TrueString_1() { return static_cast<int32_t>(offsetof(Boolean_t2950845069_StaticFields, ___TrueString_1)); }
	inline String_t* get_TrueString_1() const { return ___TrueString_1; }
	inline String_t** get_address_of_TrueString_1() { return &___TrueString_1; }
	inline void set_TrueString_1(String_t* value)
	{
		___TrueString_1 = value;
		Il2CppCodeGenWriteBarrier((&___TrueString_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOOLEAN_T2950845069_H
#ifndef MATRIX4X4_T4266809202_H
#define MATRIX4X4_T4266809202_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Matrix4x4
struct  Matrix4x4_t4266809202 
{
public:
	// System.Single UnityEngine.Matrix4x4::m00
	float ___m00_0;
	// System.Single UnityEngine.Matrix4x4::m10
	float ___m10_1;
	// System.Single UnityEngine.Matrix4x4::m20
	float ___m20_2;
	// System.Single UnityEngine.Matrix4x4::m30
	float ___m30_3;
	// System.Single UnityEngine.Matrix4x4::m01
	float ___m01_4;
	// System.Single UnityEngine.Matrix4x4::m11
	float ___m11_5;
	// System.Single UnityEngine.Matrix4x4::m21
	float ___m21_6;
	// System.Single UnityEngine.Matrix4x4::m31
	float ___m31_7;
	// System.Single UnityEngine.Matrix4x4::m02
	float ___m02_8;
	// System.Single UnityEngine.Matrix4x4::m12
	float ___m12_9;
	// System.Single UnityEngine.Matrix4x4::m22
	float ___m22_10;
	// System.Single UnityEngine.Matrix4x4::m32
	float ___m32_11;
	// System.Single UnityEngine.Matrix4x4::m03
	float ___m03_12;
	// System.Single UnityEngine.Matrix4x4::m13
	float ___m13_13;
	// System.Single UnityEngine.Matrix4x4::m23
	float ___m23_14;
	// System.Single UnityEngine.Matrix4x4::m33
	float ___m33_15;

public:
	inline static int32_t get_offset_of_m00_0() { return static_cast<int32_t>(offsetof(Matrix4x4_t4266809202, ___m00_0)); }
	inline float get_m00_0() const { return ___m00_0; }
	inline float* get_address_of_m00_0() { return &___m00_0; }
	inline void set_m00_0(float value)
	{
		___m00_0 = value;
	}

	inline static int32_t get_offset_of_m10_1() { return static_cast<int32_t>(offsetof(Matrix4x4_t4266809202, ___m10_1)); }
	inline float get_m10_1() const { return ___m10_1; }
	inline float* get_address_of_m10_1() { return &___m10_1; }
	inline void set_m10_1(float value)
	{
		___m10_1 = value;
	}

	inline static int32_t get_offset_of_m20_2() { return static_cast<int32_t>(offsetof(Matrix4x4_t4266809202, ___m20_2)); }
	inline float get_m20_2() const { return ___m20_2; }
	inline float* get_address_of_m20_2() { return &___m20_2; }
	inline void set_m20_2(float value)
	{
		___m20_2 = value;
	}

	inline static int32_t get_offset_of_m30_3() { return static_cast<int32_t>(offsetof(Matrix4x4_t4266809202, ___m30_3)); }
	inline float get_m30_3() const { return ___m30_3; }
	inline float* get_address_of_m30_3() { return &___m30_3; }
	inline void set_m30_3(float value)
	{
		___m30_3 = value;
	}

	inline static int32_t get_offset_of_m01_4() { return static_cast<int32_t>(offsetof(Matrix4x4_t4266809202, ___m01_4)); }
	inline float get_m01_4() const { return ___m01_4; }
	inline float* get_address_of_m01_4() { return &___m01_4; }
	inline void set_m01_4(float value)
	{
		___m01_4 = value;
	}

	inline static int32_t get_offset_of_m11_5() { return static_cast<int32_t>(offsetof(Matrix4x4_t4266809202, ___m11_5)); }
	inline float get_m11_5() const { return ___m11_5; }
	inline float* get_address_of_m11_5() { return &___m11_5; }
	inline void set_m11_5(float value)
	{
		___m11_5 = value;
	}

	inline static int32_t get_offset_of_m21_6() { return static_cast<int32_t>(offsetof(Matrix4x4_t4266809202, ___m21_6)); }
	inline float get_m21_6() const { return ___m21_6; }
	inline float* get_address_of_m21_6() { return &___m21_6; }
	inline void set_m21_6(float value)
	{
		___m21_6 = value;
	}

	inline static int32_t get_offset_of_m31_7() { return static_cast<int32_t>(offsetof(Matrix4x4_t4266809202, ___m31_7)); }
	inline float get_m31_7() const { return ___m31_7; }
	inline float* get_address_of_m31_7() { return &___m31_7; }
	inline void set_m31_7(float value)
	{
		___m31_7 = value;
	}

	inline static int32_t get_offset_of_m02_8() { return static_cast<int32_t>(offsetof(Matrix4x4_t4266809202, ___m02_8)); }
	inline float get_m02_8() const { return ___m02_8; }
	inline float* get_address_of_m02_8() { return &___m02_8; }
	inline void set_m02_8(float value)
	{
		___m02_8 = value;
	}

	inline static int32_t get_offset_of_m12_9() { return static_cast<int32_t>(offsetof(Matrix4x4_t4266809202, ___m12_9)); }
	inline float get_m12_9() const { return ___m12_9; }
	inline float* get_address_of_m12_9() { return &___m12_9; }
	inline void set_m12_9(float value)
	{
		___m12_9 = value;
	}

	inline static int32_t get_offset_of_m22_10() { return static_cast<int32_t>(offsetof(Matrix4x4_t4266809202, ___m22_10)); }
	inline float get_m22_10() const { return ___m22_10; }
	inline float* get_address_of_m22_10() { return &___m22_10; }
	inline void set_m22_10(float value)
	{
		___m22_10 = value;
	}

	inline static int32_t get_offset_of_m32_11() { return static_cast<int32_t>(offsetof(Matrix4x4_t4266809202, ___m32_11)); }
	inline float get_m32_11() const { return ___m32_11; }
	inline float* get_address_of_m32_11() { return &___m32_11; }
	inline void set_m32_11(float value)
	{
		___m32_11 = value;
	}

	inline static int32_t get_offset_of_m03_12() { return static_cast<int32_t>(offsetof(Matrix4x4_t4266809202, ___m03_12)); }
	inline float get_m03_12() const { return ___m03_12; }
	inline float* get_address_of_m03_12() { return &___m03_12; }
	inline void set_m03_12(float value)
	{
		___m03_12 = value;
	}

	inline static int32_t get_offset_of_m13_13() { return static_cast<int32_t>(offsetof(Matrix4x4_t4266809202, ___m13_13)); }
	inline float get_m13_13() const { return ___m13_13; }
	inline float* get_address_of_m13_13() { return &___m13_13; }
	inline void set_m13_13(float value)
	{
		___m13_13 = value;
	}

	inline static int32_t get_offset_of_m23_14() { return static_cast<int32_t>(offsetof(Matrix4x4_t4266809202, ___m23_14)); }
	inline float get_m23_14() const { return ___m23_14; }
	inline float* get_address_of_m23_14() { return &___m23_14; }
	inline void set_m23_14(float value)
	{
		___m23_14 = value;
	}

	inline static int32_t get_offset_of_m33_15() { return static_cast<int32_t>(offsetof(Matrix4x4_t4266809202, ___m33_15)); }
	inline float get_m33_15() const { return ___m33_15; }
	inline float* get_address_of_m33_15() { return &___m33_15; }
	inline void set_m33_15(float value)
	{
		___m33_15 = value;
	}
};

struct Matrix4x4_t4266809202_StaticFields
{
public:
	// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::zeroMatrix
	Matrix4x4_t4266809202  ___zeroMatrix_16;
	// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::identityMatrix
	Matrix4x4_t4266809202  ___identityMatrix_17;

public:
	inline static int32_t get_offset_of_zeroMatrix_16() { return static_cast<int32_t>(offsetof(Matrix4x4_t4266809202_StaticFields, ___zeroMatrix_16)); }
	inline Matrix4x4_t4266809202  get_zeroMatrix_16() const { return ___zeroMatrix_16; }
	inline Matrix4x4_t4266809202 * get_address_of_zeroMatrix_16() { return &___zeroMatrix_16; }
	inline void set_zeroMatrix_16(Matrix4x4_t4266809202  value)
	{
		___zeroMatrix_16 = value;
	}

	inline static int32_t get_offset_of_identityMatrix_17() { return static_cast<int32_t>(offsetof(Matrix4x4_t4266809202_StaticFields, ___identityMatrix_17)); }
	inline Matrix4x4_t4266809202  get_identityMatrix_17() const { return ___identityMatrix_17; }
	inline Matrix4x4_t4266809202 * get_address_of_identityMatrix_17() { return &___identityMatrix_17; }
	inline void set_identityMatrix_17(Matrix4x4_t4266809202  value)
	{
		___identityMatrix_17 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATRIX4X4_T4266809202_H
#ifndef RECT_T3436776195_H
#define RECT_T3436776195_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rect
struct  Rect_t3436776195 
{
public:
	// System.Single UnityEngine.Rect::m_XMin
	float ___m_XMin_0;
	// System.Single UnityEngine.Rect::m_YMin
	float ___m_YMin_1;
	// System.Single UnityEngine.Rect::m_Width
	float ___m_Width_2;
	// System.Single UnityEngine.Rect::m_Height
	float ___m_Height_3;

public:
	inline static int32_t get_offset_of_m_XMin_0() { return static_cast<int32_t>(offsetof(Rect_t3436776195, ___m_XMin_0)); }
	inline float get_m_XMin_0() const { return ___m_XMin_0; }
	inline float* get_address_of_m_XMin_0() { return &___m_XMin_0; }
	inline void set_m_XMin_0(float value)
	{
		___m_XMin_0 = value;
	}

	inline static int32_t get_offset_of_m_YMin_1() { return static_cast<int32_t>(offsetof(Rect_t3436776195, ___m_YMin_1)); }
	inline float get_m_YMin_1() const { return ___m_YMin_1; }
	inline float* get_address_of_m_YMin_1() { return &___m_YMin_1; }
	inline void set_m_YMin_1(float value)
	{
		___m_YMin_1 = value;
	}

	inline static int32_t get_offset_of_m_Width_2() { return static_cast<int32_t>(offsetof(Rect_t3436776195, ___m_Width_2)); }
	inline float get_m_Width_2() const { return ___m_Width_2; }
	inline float* get_address_of_m_Width_2() { return &___m_Width_2; }
	inline void set_m_Width_2(float value)
	{
		___m_Width_2 = value;
	}

	inline static int32_t get_offset_of_m_Height_3() { return static_cast<int32_t>(offsetof(Rect_t3436776195, ___m_Height_3)); }
	inline float get_m_Height_3() const { return ___m_Height_3; }
	inline float* get_address_of_m_Height_3() { return &___m_Height_3; }
	inline void set_m_Height_3(float value)
	{
		___m_Height_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RECT_T3436776195_H
#ifndef VECTOR3_T2903530434_H
#define VECTOR3_T2903530434_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_t2903530434 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_1;
	// System.Single UnityEngine.Vector3::y
	float ___y_2;
	// System.Single UnityEngine.Vector3::z
	float ___z_3;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector3_t2903530434, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector3_t2903530434, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector3_t2903530434, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}
};

struct Vector3_t2903530434_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t2903530434  ___zeroVector_4;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t2903530434  ___oneVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t2903530434  ___upVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t2903530434  ___downVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t2903530434  ___leftVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t2903530434  ___rightVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t2903530434  ___forwardVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t2903530434  ___backVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t2903530434  ___positiveInfinityVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t2903530434  ___negativeInfinityVector_13;

public:
	inline static int32_t get_offset_of_zeroVector_4() { return static_cast<int32_t>(offsetof(Vector3_t2903530434_StaticFields, ___zeroVector_4)); }
	inline Vector3_t2903530434  get_zeroVector_4() const { return ___zeroVector_4; }
	inline Vector3_t2903530434 * get_address_of_zeroVector_4() { return &___zeroVector_4; }
	inline void set_zeroVector_4(Vector3_t2903530434  value)
	{
		___zeroVector_4 = value;
	}

	inline static int32_t get_offset_of_oneVector_5() { return static_cast<int32_t>(offsetof(Vector3_t2903530434_StaticFields, ___oneVector_5)); }
	inline Vector3_t2903530434  get_oneVector_5() const { return ___oneVector_5; }
	inline Vector3_t2903530434 * get_address_of_oneVector_5() { return &___oneVector_5; }
	inline void set_oneVector_5(Vector3_t2903530434  value)
	{
		___oneVector_5 = value;
	}

	inline static int32_t get_offset_of_upVector_6() { return static_cast<int32_t>(offsetof(Vector3_t2903530434_StaticFields, ___upVector_6)); }
	inline Vector3_t2903530434  get_upVector_6() const { return ___upVector_6; }
	inline Vector3_t2903530434 * get_address_of_upVector_6() { return &___upVector_6; }
	inline void set_upVector_6(Vector3_t2903530434  value)
	{
		___upVector_6 = value;
	}

	inline static int32_t get_offset_of_downVector_7() { return static_cast<int32_t>(offsetof(Vector3_t2903530434_StaticFields, ___downVector_7)); }
	inline Vector3_t2903530434  get_downVector_7() const { return ___downVector_7; }
	inline Vector3_t2903530434 * get_address_of_downVector_7() { return &___downVector_7; }
	inline void set_downVector_7(Vector3_t2903530434  value)
	{
		___downVector_7 = value;
	}

	inline static int32_t get_offset_of_leftVector_8() { return static_cast<int32_t>(offsetof(Vector3_t2903530434_StaticFields, ___leftVector_8)); }
	inline Vector3_t2903530434  get_leftVector_8() const { return ___leftVector_8; }
	inline Vector3_t2903530434 * get_address_of_leftVector_8() { return &___leftVector_8; }
	inline void set_leftVector_8(Vector3_t2903530434  value)
	{
		___leftVector_8 = value;
	}

	inline static int32_t get_offset_of_rightVector_9() { return static_cast<int32_t>(offsetof(Vector3_t2903530434_StaticFields, ___rightVector_9)); }
	inline Vector3_t2903530434  get_rightVector_9() const { return ___rightVector_9; }
	inline Vector3_t2903530434 * get_address_of_rightVector_9() { return &___rightVector_9; }
	inline void set_rightVector_9(Vector3_t2903530434  value)
	{
		___rightVector_9 = value;
	}

	inline static int32_t get_offset_of_forwardVector_10() { return static_cast<int32_t>(offsetof(Vector3_t2903530434_StaticFields, ___forwardVector_10)); }
	inline Vector3_t2903530434  get_forwardVector_10() const { return ___forwardVector_10; }
	inline Vector3_t2903530434 * get_address_of_forwardVector_10() { return &___forwardVector_10; }
	inline void set_forwardVector_10(Vector3_t2903530434  value)
	{
		___forwardVector_10 = value;
	}

	inline static int32_t get_offset_of_backVector_11() { return static_cast<int32_t>(offsetof(Vector3_t2903530434_StaticFields, ___backVector_11)); }
	inline Vector3_t2903530434  get_backVector_11() const { return ___backVector_11; }
	inline Vector3_t2903530434 * get_address_of_backVector_11() { return &___backVector_11; }
	inline void set_backVector_11(Vector3_t2903530434  value)
	{
		___backVector_11 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_12() { return static_cast<int32_t>(offsetof(Vector3_t2903530434_StaticFields, ___positiveInfinityVector_12)); }
	inline Vector3_t2903530434  get_positiveInfinityVector_12() const { return ___positiveInfinityVector_12; }
	inline Vector3_t2903530434 * get_address_of_positiveInfinityVector_12() { return &___positiveInfinityVector_12; }
	inline void set_positiveInfinityVector_12(Vector3_t2903530434  value)
	{
		___positiveInfinityVector_12 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t2903530434_StaticFields, ___negativeInfinityVector_13)); }
	inline Vector3_t2903530434  get_negativeInfinityVector_13() const { return ___negativeInfinityVector_13; }
	inline Vector3_t2903530434 * get_address_of_negativeInfinityVector_13() { return &___negativeInfinityVector_13; }
	inline void set_negativeInfinityVector_13(Vector3_t2903530434  value)
	{
		___negativeInfinityVector_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_T2903530434_H
#ifndef VOID_T2217553113_H
#define VOID_T2217553113_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t2217553113 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T2217553113_H
#ifndef SINGLE_T3045349759_H
#define SINGLE_T3045349759_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Single
struct  Single_t3045349759 
{
public:
	// System.Single System.Single::m_value
	float ___m_value_7;

public:
	inline static int32_t get_offset_of_m_value_7() { return static_cast<int32_t>(offsetof(Single_t3045349759, ___m_value_7)); }
	inline float get_m_value_7() const { return ___m_value_7; }
	inline float* get_address_of_m_value_7() { return &___m_value_7; }
	inline void set_m_value_7(float value)
	{
		___m_value_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLE_T3045349759_H
#ifndef QUATERNION_T754065749_H
#define QUATERNION_T754065749_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Quaternion
struct  Quaternion_t754065749 
{
public:
	// System.Single UnityEngine.Quaternion::x
	float ___x_0;
	// System.Single UnityEngine.Quaternion::y
	float ___y_1;
	// System.Single UnityEngine.Quaternion::z
	float ___z_2;
	// System.Single UnityEngine.Quaternion::w
	float ___w_3;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Quaternion_t754065749, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Quaternion_t754065749, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(Quaternion_t754065749, ___z_2)); }
	inline float get_z_2() const { return ___z_2; }
	inline float* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(float value)
	{
		___z_2 = value;
	}

	inline static int32_t get_offset_of_w_3() { return static_cast<int32_t>(offsetof(Quaternion_t754065749, ___w_3)); }
	inline float get_w_3() const { return ___w_3; }
	inline float* get_address_of_w_3() { return &___w_3; }
	inline void set_w_3(float value)
	{
		___w_3 = value;
	}
};

struct Quaternion_t754065749_StaticFields
{
public:
	// UnityEngine.Quaternion UnityEngine.Quaternion::identityQuaternion
	Quaternion_t754065749  ___identityQuaternion_4;

public:
	inline static int32_t get_offset_of_identityQuaternion_4() { return static_cast<int32_t>(offsetof(Quaternion_t754065749_StaticFields, ___identityQuaternion_4)); }
	inline Quaternion_t754065749  get_identityQuaternion_4() const { return ___identityQuaternion_4; }
	inline Quaternion_t754065749 * get_address_of_identityQuaternion_4() { return &___identityQuaternion_4; }
	inline void set_identityQuaternion_4(Quaternion_t754065749  value)
	{
		___identityQuaternion_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUATERNION_T754065749_H
#ifndef CAMERADIRECTION_T1337526284_H
#define CAMERADIRECTION_T1337526284_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.CameraDevice/CameraDirection
struct  CameraDirection_t1337526284 
{
public:
	// System.Int32 Vuforia.CameraDevice/CameraDirection::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(CameraDirection_t1337526284, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAMERADIRECTION_T1337526284_H
#ifndef WORDFILTERMODE_T2466111678_H
#define WORDFILTERMODE_T2466111678_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.WordFilterMode
struct  WordFilterMode_t2466111678 
{
public:
	// System.Int32 Vuforia.WordFilterMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(WordFilterMode_t2466111678, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WORDFILTERMODE_T2466111678_H
#ifndef IMAGETARGETTYPE_T2769160834_H
#define IMAGETARGETTYPE_T2769160834_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.ImageTargetType
struct  ImageTargetType_t2769160834 
{
public:
	// System.Int32 Vuforia.ImageTargetType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ImageTargetType_t2769160834, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IMAGETARGETTYPE_T2769160834_H
#ifndef DEVICEORIENTATION_T2381498132_H
#define DEVICEORIENTATION_T2381498132_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.DeviceOrientation
struct  DeviceOrientation_t2381498132 
{
public:
	// System.Int32 UnityEngine.DeviceOrientation::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(DeviceOrientation_t2381498132, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEVICEORIENTATION_T2381498132_H
#ifndef VIDEOTEXTUREINFO_T773013061_H
#define VIDEOTEXTUREINFO_T773013061_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.VuforiaRenderer/VideoTextureInfo
#pragma pack(push, tp, 1)
struct  VideoTextureInfo_t773013061 
{
public:
	// Vuforia.VuforiaRenderer/Vec2I Vuforia.VuforiaRenderer/VideoTextureInfo::textureSize
	Vec2I_t895597728  ___textureSize_0;
	// Vuforia.VuforiaRenderer/Vec2I Vuforia.VuforiaRenderer/VideoTextureInfo::imageSize
	Vec2I_t895597728  ___imageSize_1;

public:
	inline static int32_t get_offset_of_textureSize_0() { return static_cast<int32_t>(offsetof(VideoTextureInfo_t773013061, ___textureSize_0)); }
	inline Vec2I_t895597728  get_textureSize_0() const { return ___textureSize_0; }
	inline Vec2I_t895597728 * get_address_of_textureSize_0() { return &___textureSize_0; }
	inline void set_textureSize_0(Vec2I_t895597728  value)
	{
		___textureSize_0 = value;
	}

	inline static int32_t get_offset_of_imageSize_1() { return static_cast<int32_t>(offsetof(VideoTextureInfo_t773013061, ___imageSize_1)); }
	inline Vec2I_t895597728  get_imageSize_1() const { return ___imageSize_1; }
	inline Vec2I_t895597728 * get_address_of_imageSize_1() { return &___imageSize_1; }
	inline void set_imageSize_1(Vec2I_t895597728  value)
	{
		___imageSize_1 = value;
	}
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VIDEOTEXTUREINFO_T773013061_H
#ifndef STEREOFRAMEWORK_T3060055999_H
#define STEREOFRAMEWORK_T3060055999_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.DigitalEyewearARController/StereoFramework
struct  StereoFramework_t3060055999 
{
public:
	// System.Int32 Vuforia.DigitalEyewearARController/StereoFramework::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(StereoFramework_t3060055999, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STEREOFRAMEWORK_T3060055999_H
#ifndef EYEWEARTYPE_T4127891436_H
#define EYEWEARTYPE_T4127891436_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.DigitalEyewearARController/EyewearType
struct  EyewearType_t4127891436 
{
public:
	// System.Int32 Vuforia.DigitalEyewearARController/EyewearType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(EyewearType_t4127891436, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EYEWEARTYPE_T4127891436_H
#ifndef DISTORTIONRENDERINGMODE_T4279873297_H
#define DISTORTIONRENDERINGMODE_T4279873297_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.DistortionRenderingMode
struct  DistortionRenderingMode_t4279873297 
{
public:
	// System.Int32 Vuforia.DistortionRenderingMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(DistortionRenderingMode_t4279873297, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DISTORTIONRENDERINGMODE_T4279873297_H
#ifndef WORLDCENTERMODE_T2621860492_H
#define WORLDCENTERMODE_T2621860492_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.VuforiaARController/WorldCenterMode
struct  WorldCenterMode_t2621860492 
{
public:
	// System.Int32 Vuforia.VuforiaARController/WorldCenterMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(WorldCenterMode_t2621860492, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WORLDCENTERMODE_T2621860492_H
#ifndef STATUS_T1358118869_H
#define STATUS_T1358118869_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.TrackableBehaviour/Status
struct  Status_t1358118869 
{
public:
	// System.Int32 Vuforia.TrackableBehaviour/Status::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Status_t1358118869, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STATUS_T1358118869_H
#ifndef CAMERADEVICEMODE_T1785007815_H
#define CAMERADEVICEMODE_T1785007815_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.CameraDevice/CameraDeviceMode
struct  CameraDeviceMode_t1785007815 
{
public:
	// System.Int32 Vuforia.CameraDevice/CameraDeviceMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(CameraDeviceMode_t1785007815, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAMERADEVICEMODE_T1785007815_H
#ifndef CLIPPING_MODE_T306275072_H
#define CLIPPING_MODE_T306275072_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.HideExcessAreaAbstractBehaviour/CLIPPING_MODE
struct  CLIPPING_MODE_t306275072 
{
public:
	// System.Int32 Vuforia.HideExcessAreaAbstractBehaviour/CLIPPING_MODE::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(CLIPPING_MODE_t306275072, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CLIPPING_MODE_T306275072_H
#ifndef SEETHROUGHCONFIGURATION_T1856493377_H
#define SEETHROUGHCONFIGURATION_T1856493377_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.DigitalEyewearARController/SeeThroughConfiguration
struct  SeeThroughConfiguration_t1856493377 
{
public:
	// System.Int32 Vuforia.DigitalEyewearARController/SeeThroughConfiguration::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(SeeThroughConfiguration_t1856493377, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SEETHROUGHCONFIGURATION_T1856493377_H
#ifndef DELEGATE_T537306192_H
#define DELEGATE_T537306192_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t537306192  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	IntPtr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	IntPtr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	IntPtr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::method_code
	IntPtr_t ___method_code_5;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_6;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_7;
	// System.DelegateData System.Delegate::data
	DelegateData_t3370401926 * ___data_8;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t537306192, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t537306192, ___invoke_impl_1)); }
	inline IntPtr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline IntPtr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(IntPtr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t537306192, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t537306192, ___method_3)); }
	inline IntPtr_t get_method_3() const { return ___method_3; }
	inline IntPtr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(IntPtr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t537306192, ___delegate_trampoline_4)); }
	inline IntPtr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline IntPtr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(IntPtr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_method_code_5() { return static_cast<int32_t>(offsetof(Delegate_t537306192, ___method_code_5)); }
	inline IntPtr_t get_method_code_5() const { return ___method_code_5; }
	inline IntPtr_t* get_address_of_method_code_5() { return &___method_code_5; }
	inline void set_method_code_5(IntPtr_t value)
	{
		___method_code_5 = value;
	}

	inline static int32_t get_offset_of_method_info_6() { return static_cast<int32_t>(offsetof(Delegate_t537306192, ___method_info_6)); }
	inline MethodInfo_t * get_method_info_6() const { return ___method_info_6; }
	inline MethodInfo_t ** get_address_of_method_info_6() { return &___method_info_6; }
	inline void set_method_info_6(MethodInfo_t * value)
	{
		___method_info_6 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_6), value);
	}

	inline static int32_t get_offset_of_original_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t537306192, ___original_method_info_7)); }
	inline MethodInfo_t * get_original_method_info_7() const { return ___original_method_info_7; }
	inline MethodInfo_t ** get_address_of_original_method_info_7() { return &___original_method_info_7; }
	inline void set_original_method_info_7(MethodInfo_t * value)
	{
		___original_method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_7), value);
	}

	inline static int32_t get_offset_of_data_8() { return static_cast<int32_t>(offsetof(Delegate_t537306192, ___data_8)); }
	inline DelegateData_t3370401926 * get_data_8() const { return ___data_8; }
	inline DelegateData_t3370401926 ** get_address_of_data_8() { return &___data_8; }
	inline void set_data_8(DelegateData_t3370401926 * value)
	{
		___data_8 = value;
		Il2CppCodeGenWriteBarrier((&___data_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DELEGATE_T537306192_H
#ifndef RUNTIMEPLATFORM_T978892161_H
#define RUNTIMEPLATFORM_T978892161_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.RuntimePlatform
struct  RuntimePlatform_t978892161 
{
public:
	// System.Int32 UnityEngine.RuntimePlatform::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(RuntimePlatform_t978892161, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEPLATFORM_T978892161_H
#ifndef INSTANCEIDTYPE_T3450543871_H
#define INSTANCEIDTYPE_T3450543871_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.InstanceIdType
struct  InstanceIdType_t3450543871 
{
public:
	// System.Int32 Vuforia.InstanceIdType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(InstanceIdType_t3450543871, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INSTANCEIDTYPE_T3450543871_H
#ifndef INITERROR_T3486678342_H
#define INITERROR_T3486678342_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.VuforiaUnity/InitError
struct  InitError_t3486678342 
{
public:
	// System.Int32 Vuforia.VuforiaUnity/InitError::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(InitError_t3486678342, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INITERROR_T3486678342_H
#ifndef RENDERERAPI_T3250500906_H
#define RENDERERAPI_T3250500906_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.VuforiaRenderer/RendererAPI
struct  RendererAPI_t3250500906 
{
public:
	// System.Int32 Vuforia.VuforiaRenderer/RendererAPI::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(RendererAPI_t3250500906, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RENDERERAPI_T3250500906_H
#ifndef OBJECT_T1502412432_H
#define OBJECT_T1502412432_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t1502412432  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	IntPtr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t1502412432, ___m_CachedPtr_0)); }
	inline IntPtr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline IntPtr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(IntPtr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t1502412432_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t1502412432_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t1502412432_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t1502412432_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T1502412432_H
#ifndef SCREENORIENTATION_T2818551509_H
#define SCREENORIENTATION_T2818551509_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ScreenOrientation
struct  ScreenOrientation_t2818551509 
{
public:
	// System.Int32 UnityEngine.ScreenOrientation::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ScreenOrientation_t2818551509, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCREENORIENTATION_T2818551509_H
#ifndef WORDTEMPLATEMODE_T620524771_H
#define WORDTEMPLATEMODE_T620524771_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.WordTemplateMode
struct  WordTemplateMode_t620524771 
{
public:
	// System.Int32 Vuforia.WordTemplateMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(WordTemplateMode_t620524771, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WORDTEMPLATEMODE_T620524771_H
#ifndef SENSITIVITY_T2611854098_H
#define SENSITIVITY_T2611854098_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.VirtualButton/Sensitivity
struct  Sensitivity_t2611854098 
{
public:
	// System.Int32 Vuforia.VirtualButton/Sensitivity::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Sensitivity_t2611854098, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SENSITIVITY_T2611854098_H
#ifndef BINDINGFLAGS_T1805189573_H
#define BINDINGFLAGS_T1805189573_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.BindingFlags
struct  BindingFlags_t1805189573 
{
public:
	// System.Int32 System.Reflection.BindingFlags::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(BindingFlags_t1805189573, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BINDINGFLAGS_T1805189573_H
#ifndef FRAMEQUALITY_T1148757_H
#define FRAMEQUALITY_T1148757_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.ImageTargetBuilder/FrameQuality
struct  FrameQuality_t1148757 
{
public:
	// System.Int32 Vuforia.ImageTargetBuilder/FrameQuality::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(FrameQuality_t1148757, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FRAMEQUALITY_T1148757_H
#ifndef WORDPREFABCREATIONMODE_T2868429441_H
#define WORDPREFABCREATIONMODE_T2868429441_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.WordPrefabCreationMode
struct  WordPrefabCreationMode_t2868429441 
{
public:
	// System.Int32 Vuforia.WordPrefabCreationMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(WordPrefabCreationMode_t2868429441, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WORDPREFABCREATIONMODE_T2868429441_H
#ifndef RUNTIMETYPEHANDLE_T845633806_H
#define RUNTIMETYPEHANDLE_T845633806_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.RuntimeTypeHandle
struct  RuntimeTypeHandle_t845633806 
{
public:
	// System.IntPtr System.RuntimeTypeHandle::value
	IntPtr_t ___value_0;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(RuntimeTypeHandle_t845633806, ___value_0)); }
	inline IntPtr_t get_value_0() const { return ___value_0; }
	inline IntPtr_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(IntPtr_t value)
	{
		___value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMETYPEHANDLE_T845633806_H
#ifndef METHODINFO_T_H
#define METHODINFO_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.MethodInfo
struct  MethodInfo_t  : public MethodBase_t3686601602
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // METHODINFO_T_H
#ifndef VIDEOBACKGROUNDREFLECTION_T728651611_H
#define VIDEOBACKGROUNDREFLECTION_T728651611_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.VuforiaRenderer/VideoBackgroundReflection
struct  VideoBackgroundReflection_t728651611 
{
public:
	// System.Int32 Vuforia.VuforiaRenderer/VideoBackgroundReflection::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(VideoBackgroundReflection_t728651611, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VIDEOBACKGROUNDREFLECTION_T728651611_H
#ifndef MULTICASTDELEGATE_T4139169907_H
#define MULTICASTDELEGATE_T4139169907_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t4139169907  : public Delegate_t537306192
{
public:
	// System.MulticastDelegate System.MulticastDelegate::prev
	MulticastDelegate_t4139169907 * ___prev_9;
	// System.MulticastDelegate System.MulticastDelegate::kpm_next
	MulticastDelegate_t4139169907 * ___kpm_next_10;

public:
	inline static int32_t get_offset_of_prev_9() { return static_cast<int32_t>(offsetof(MulticastDelegate_t4139169907, ___prev_9)); }
	inline MulticastDelegate_t4139169907 * get_prev_9() const { return ___prev_9; }
	inline MulticastDelegate_t4139169907 ** get_address_of_prev_9() { return &___prev_9; }
	inline void set_prev_9(MulticastDelegate_t4139169907 * value)
	{
		___prev_9 = value;
		Il2CppCodeGenWriteBarrier((&___prev_9), value);
	}

	inline static int32_t get_offset_of_kpm_next_10() { return static_cast<int32_t>(offsetof(MulticastDelegate_t4139169907, ___kpm_next_10)); }
	inline MulticastDelegate_t4139169907 * get_kpm_next_10() const { return ___kpm_next_10; }
	inline MulticastDelegate_t4139169907 ** get_address_of_kpm_next_10() { return &___kpm_next_10; }
	inline void set_kpm_next_10(MulticastDelegate_t4139169907 * value)
	{
		___kpm_next_10 = value;
		Il2CppCodeGenWriteBarrier((&___kpm_next_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MULTICASTDELEGATE_T4139169907_H
#ifndef SCRIPTABLEOBJECT_T3635579074_H
#define SCRIPTABLEOBJECT_T3635579074_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ScriptableObject
struct  ScriptableObject_t3635579074  : public Object_t1502412432
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_t3635579074_marshaled_pinvoke : public Object_t1502412432_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_t3635579074_marshaled_com : public Object_t1502412432_marshaled_com
{
};
#endif // SCRIPTABLEOBJECT_T3635579074_H
#ifndef MATERIAL_T1079520667_H
#define MATERIAL_T1079520667_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Material
struct  Material_t1079520667  : public Object_t1502412432
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATERIAL_T1079520667_H
#ifndef MESH_T996500909_H
#define MESH_T996500909_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Mesh
struct  Mesh_t996500909  : public Object_t1502412432
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MESH_T996500909_H
#ifndef GENERICVUFORIACONFIGURATION_T1574456586_H
#define GENERICVUFORIACONFIGURATION_T1574456586_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.VuforiaAbstractConfiguration/GenericVuforiaConfiguration
struct  GenericVuforiaConfiguration_t1574456586  : public RuntimeObject
{
public:
	// System.String Vuforia.VuforiaAbstractConfiguration/GenericVuforiaConfiguration::vuforiaLicenseKey
	String_t* ___vuforiaLicenseKey_0;
	// System.Boolean Vuforia.VuforiaAbstractConfiguration/GenericVuforiaConfiguration::delayedInitialization
	bool ___delayedInitialization_1;
	// Vuforia.CameraDevice/CameraDeviceMode Vuforia.VuforiaAbstractConfiguration/GenericVuforiaConfiguration::cameraDeviceModeSetting
	int32_t ___cameraDeviceModeSetting_2;
	// System.Int32 Vuforia.VuforiaAbstractConfiguration/GenericVuforiaConfiguration::maxSimultaneousImageTargets
	int32_t ___maxSimultaneousImageTargets_3;
	// System.Int32 Vuforia.VuforiaAbstractConfiguration/GenericVuforiaConfiguration::maxSimultaneousObjectTargets
	int32_t ___maxSimultaneousObjectTargets_4;
	// System.Boolean Vuforia.VuforiaAbstractConfiguration/GenericVuforiaConfiguration::useDelayedLoadingObjectTargets
	bool ___useDelayedLoadingObjectTargets_5;
	// Vuforia.CameraDevice/CameraDirection Vuforia.VuforiaAbstractConfiguration/GenericVuforiaConfiguration::cameraDirection
	int32_t ___cameraDirection_6;
	// Vuforia.VuforiaRenderer/VideoBackgroundReflection Vuforia.VuforiaAbstractConfiguration/GenericVuforiaConfiguration::mirrorVideoBackground
	int32_t ___mirrorVideoBackground_7;

public:
	inline static int32_t get_offset_of_vuforiaLicenseKey_0() { return static_cast<int32_t>(offsetof(GenericVuforiaConfiguration_t1574456586, ___vuforiaLicenseKey_0)); }
	inline String_t* get_vuforiaLicenseKey_0() const { return ___vuforiaLicenseKey_0; }
	inline String_t** get_address_of_vuforiaLicenseKey_0() { return &___vuforiaLicenseKey_0; }
	inline void set_vuforiaLicenseKey_0(String_t* value)
	{
		___vuforiaLicenseKey_0 = value;
		Il2CppCodeGenWriteBarrier((&___vuforiaLicenseKey_0), value);
	}

	inline static int32_t get_offset_of_delayedInitialization_1() { return static_cast<int32_t>(offsetof(GenericVuforiaConfiguration_t1574456586, ___delayedInitialization_1)); }
	inline bool get_delayedInitialization_1() const { return ___delayedInitialization_1; }
	inline bool* get_address_of_delayedInitialization_1() { return &___delayedInitialization_1; }
	inline void set_delayedInitialization_1(bool value)
	{
		___delayedInitialization_1 = value;
	}

	inline static int32_t get_offset_of_cameraDeviceModeSetting_2() { return static_cast<int32_t>(offsetof(GenericVuforiaConfiguration_t1574456586, ___cameraDeviceModeSetting_2)); }
	inline int32_t get_cameraDeviceModeSetting_2() const { return ___cameraDeviceModeSetting_2; }
	inline int32_t* get_address_of_cameraDeviceModeSetting_2() { return &___cameraDeviceModeSetting_2; }
	inline void set_cameraDeviceModeSetting_2(int32_t value)
	{
		___cameraDeviceModeSetting_2 = value;
	}

	inline static int32_t get_offset_of_maxSimultaneousImageTargets_3() { return static_cast<int32_t>(offsetof(GenericVuforiaConfiguration_t1574456586, ___maxSimultaneousImageTargets_3)); }
	inline int32_t get_maxSimultaneousImageTargets_3() const { return ___maxSimultaneousImageTargets_3; }
	inline int32_t* get_address_of_maxSimultaneousImageTargets_3() { return &___maxSimultaneousImageTargets_3; }
	inline void set_maxSimultaneousImageTargets_3(int32_t value)
	{
		___maxSimultaneousImageTargets_3 = value;
	}

	inline static int32_t get_offset_of_maxSimultaneousObjectTargets_4() { return static_cast<int32_t>(offsetof(GenericVuforiaConfiguration_t1574456586, ___maxSimultaneousObjectTargets_4)); }
	inline int32_t get_maxSimultaneousObjectTargets_4() const { return ___maxSimultaneousObjectTargets_4; }
	inline int32_t* get_address_of_maxSimultaneousObjectTargets_4() { return &___maxSimultaneousObjectTargets_4; }
	inline void set_maxSimultaneousObjectTargets_4(int32_t value)
	{
		___maxSimultaneousObjectTargets_4 = value;
	}

	inline static int32_t get_offset_of_useDelayedLoadingObjectTargets_5() { return static_cast<int32_t>(offsetof(GenericVuforiaConfiguration_t1574456586, ___useDelayedLoadingObjectTargets_5)); }
	inline bool get_useDelayedLoadingObjectTargets_5() const { return ___useDelayedLoadingObjectTargets_5; }
	inline bool* get_address_of_useDelayedLoadingObjectTargets_5() { return &___useDelayedLoadingObjectTargets_5; }
	inline void set_useDelayedLoadingObjectTargets_5(bool value)
	{
		___useDelayedLoadingObjectTargets_5 = value;
	}

	inline static int32_t get_offset_of_cameraDirection_6() { return static_cast<int32_t>(offsetof(GenericVuforiaConfiguration_t1574456586, ___cameraDirection_6)); }
	inline int32_t get_cameraDirection_6() const { return ___cameraDirection_6; }
	inline int32_t* get_address_of_cameraDirection_6() { return &___cameraDirection_6; }
	inline void set_cameraDirection_6(int32_t value)
	{
		___cameraDirection_6 = value;
	}

	inline static int32_t get_offset_of_mirrorVideoBackground_7() { return static_cast<int32_t>(offsetof(GenericVuforiaConfiguration_t1574456586, ___mirrorVideoBackground_7)); }
	inline int32_t get_mirrorVideoBackground_7() const { return ___mirrorVideoBackground_7; }
	inline int32_t* get_address_of_mirrorVideoBackground_7() { return &___mirrorVideoBackground_7; }
	inline void set_mirrorVideoBackground_7(int32_t value)
	{
		___mirrorVideoBackground_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GENERICVUFORIACONFIGURATION_T1574456586_H
#ifndef WSAUNITYPLAYER_T1576425251_H
#define WSAUNITYPLAYER_T1576425251_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.WSAUnityPlayer
struct  WSAUnityPlayer_t1576425251  : public RuntimeObject
{
public:
	// UnityEngine.ScreenOrientation Vuforia.WSAUnityPlayer::mScreenOrientation
	int32_t ___mScreenOrientation_0;

public:
	inline static int32_t get_offset_of_mScreenOrientation_0() { return static_cast<int32_t>(offsetof(WSAUnityPlayer_t1576425251, ___mScreenOrientation_0)); }
	inline int32_t get_mScreenOrientation_0() const { return ___mScreenOrientation_0; }
	inline int32_t* get_address_of_mScreenOrientation_0() { return &___mScreenOrientation_0; }
	inline void set_mScreenOrientation_0(int32_t value)
	{
		___mScreenOrientation_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WSAUNITYPLAYER_T1576425251_H
#ifndef GAMEOBJECT_T1811656094_H
#define GAMEOBJECT_T1811656094_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.GameObject
struct  GameObject_t1811656094  : public Object_t1502412432
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAMEOBJECT_T1811656094_H
#ifndef COMPONENT_T4087199522_H
#define COMPONENT_T4087199522_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t4087199522  : public Object_t1502412432
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T4087199522_H
#ifndef IOSUNITYPLAYER_T1005570149_H
#define IOSUNITYPLAYER_T1005570149_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.IOSUnityPlayer
struct  IOSUnityPlayer_t1005570149  : public RuntimeObject
{
public:
	// UnityEngine.ScreenOrientation Vuforia.IOSUnityPlayer::mScreenOrientation
	int32_t ___mScreenOrientation_0;

public:
	inline static int32_t get_offset_of_mScreenOrientation_0() { return static_cast<int32_t>(offsetof(IOSUnityPlayer_t1005570149, ___mScreenOrientation_0)); }
	inline int32_t get_mScreenOrientation_0() const { return ___mScreenOrientation_0; }
	inline int32_t* get_address_of_mScreenOrientation_0() { return &___mScreenOrientation_0; }
	inline void set_mScreenOrientation_0(int32_t value)
	{
		___mScreenOrientation_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IOSUNITYPLAYER_T1005570149_H
#ifndef TYPE_T_H
#define TYPE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Type
struct  Type_t  : public MemberInfo_t
{
public:
	// System.RuntimeTypeHandle System.Type::_impl
	RuntimeTypeHandle_t845633806  ____impl_1;

public:
	inline static int32_t get_offset_of__impl_1() { return static_cast<int32_t>(offsetof(Type_t, ____impl_1)); }
	inline RuntimeTypeHandle_t845633806  get__impl_1() const { return ____impl_1; }
	inline RuntimeTypeHandle_t845633806 * get_address_of__impl_1() { return &____impl_1; }
	inline void set__impl_1(RuntimeTypeHandle_t845633806  value)
	{
		____impl_1 = value;
	}
};

struct Type_t_StaticFields
{
public:
	// System.Char System.Type::Delimiter
	Il2CppChar ___Delimiter_2;
	// System.Type[] System.Type::EmptyTypes
	TypeU5BU5D_t3854702846* ___EmptyTypes_3;
	// System.Reflection.MemberFilter System.Type::FilterAttribute
	MemberFilter_t4208935459 * ___FilterAttribute_4;
	// System.Reflection.MemberFilter System.Type::FilterName
	MemberFilter_t4208935459 * ___FilterName_5;
	// System.Reflection.MemberFilter System.Type::FilterNameIgnoreCase
	MemberFilter_t4208935459 * ___FilterNameIgnoreCase_6;
	// System.Object System.Type::Missing
	RuntimeObject * ___Missing_7;

public:
	inline static int32_t get_offset_of_Delimiter_2() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Delimiter_2)); }
	inline Il2CppChar get_Delimiter_2() const { return ___Delimiter_2; }
	inline Il2CppChar* get_address_of_Delimiter_2() { return &___Delimiter_2; }
	inline void set_Delimiter_2(Il2CppChar value)
	{
		___Delimiter_2 = value;
	}

	inline static int32_t get_offset_of_EmptyTypes_3() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___EmptyTypes_3)); }
	inline TypeU5BU5D_t3854702846* get_EmptyTypes_3() const { return ___EmptyTypes_3; }
	inline TypeU5BU5D_t3854702846** get_address_of_EmptyTypes_3() { return &___EmptyTypes_3; }
	inline void set_EmptyTypes_3(TypeU5BU5D_t3854702846* value)
	{
		___EmptyTypes_3 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyTypes_3), value);
	}

	inline static int32_t get_offset_of_FilterAttribute_4() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterAttribute_4)); }
	inline MemberFilter_t4208935459 * get_FilterAttribute_4() const { return ___FilterAttribute_4; }
	inline MemberFilter_t4208935459 ** get_address_of_FilterAttribute_4() { return &___FilterAttribute_4; }
	inline void set_FilterAttribute_4(MemberFilter_t4208935459 * value)
	{
		___FilterAttribute_4 = value;
		Il2CppCodeGenWriteBarrier((&___FilterAttribute_4), value);
	}

	inline static int32_t get_offset_of_FilterName_5() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterName_5)); }
	inline MemberFilter_t4208935459 * get_FilterName_5() const { return ___FilterName_5; }
	inline MemberFilter_t4208935459 ** get_address_of_FilterName_5() { return &___FilterName_5; }
	inline void set_FilterName_5(MemberFilter_t4208935459 * value)
	{
		___FilterName_5 = value;
		Il2CppCodeGenWriteBarrier((&___FilterName_5), value);
	}

	inline static int32_t get_offset_of_FilterNameIgnoreCase_6() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterNameIgnoreCase_6)); }
	inline MemberFilter_t4208935459 * get_FilterNameIgnoreCase_6() const { return ___FilterNameIgnoreCase_6; }
	inline MemberFilter_t4208935459 ** get_address_of_FilterNameIgnoreCase_6() { return &___FilterNameIgnoreCase_6; }
	inline void set_FilterNameIgnoreCase_6(MemberFilter_t4208935459 * value)
	{
		___FilterNameIgnoreCase_6 = value;
		Il2CppCodeGenWriteBarrier((&___FilterNameIgnoreCase_6), value);
	}

	inline static int32_t get_offset_of_Missing_7() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Missing_7)); }
	inline RuntimeObject * get_Missing_7() const { return ___Missing_7; }
	inline RuntimeObject ** get_address_of_Missing_7() { return &___Missing_7; }
	inline void set_Missing_7(RuntimeObject * value)
	{
		___Missing_7 = value;
		Il2CppCodeGenWriteBarrier((&___Missing_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TYPE_T_H
#ifndef VUFORIAARCONTROLLER_T1328503142_H
#define VUFORIAARCONTROLLER_T1328503142_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.VuforiaARController
struct  VuforiaARController_t1328503142  : public ARController_t1205915989
{
public:
	// Vuforia.CameraDevice/CameraDeviceMode Vuforia.VuforiaARController::CameraDeviceModeSetting
	int32_t ___CameraDeviceModeSetting_1;
	// System.Int32 Vuforia.VuforiaARController::MaxSimultaneousImageTargets
	int32_t ___MaxSimultaneousImageTargets_2;
	// System.Int32 Vuforia.VuforiaARController::MaxSimultaneousObjectTargets
	int32_t ___MaxSimultaneousObjectTargets_3;
	// System.Boolean Vuforia.VuforiaARController::UseDelayedLoadingObjectTargets
	bool ___UseDelayedLoadingObjectTargets_4;
	// Vuforia.CameraDevice/CameraDirection Vuforia.VuforiaARController::CameraDirection
	int32_t ___CameraDirection_5;
	// Vuforia.VuforiaRenderer/VideoBackgroundReflection Vuforia.VuforiaARController::MirrorVideoBackground
	int32_t ___MirrorVideoBackground_6;
	// Vuforia.VuforiaARController/WorldCenterMode Vuforia.VuforiaARController::mWorldCenterMode
	int32_t ___mWorldCenterMode_7;
	// Vuforia.TrackableBehaviour Vuforia.VuforiaARController::mWorldCenter
	TrackableBehaviour_t2419079356 * ___mWorldCenter_8;
	// System.Collections.Generic.List`1<Vuforia.ITrackerEventHandler> Vuforia.VuforiaARController::mTrackerEventHandlers
	List_1_t1947635164 * ___mTrackerEventHandlers_9;
	// System.Collections.Generic.List`1<Vuforia.IVideoBackgroundEventHandler> Vuforia.VuforiaARController::mVideoBgEventHandlers
	List_1_t485517045 * ___mVideoBgEventHandlers_10;
	// System.Action Vuforia.VuforiaARController::mOnVuforiaInitialized
	Action_t3619184611 * ___mOnVuforiaInitialized_11;
	// System.Action Vuforia.VuforiaARController::mOnVuforiaStarted
	Action_t3619184611 * ___mOnVuforiaStarted_12;
	// System.Action Vuforia.VuforiaARController::mOnVuforiaDeinitialized
	Action_t3619184611 * ___mOnVuforiaDeinitialized_13;
	// System.Action Vuforia.VuforiaARController::mOnTrackablesUpdated
	Action_t3619184611 * ___mOnTrackablesUpdated_14;
	// System.Action Vuforia.VuforiaARController::mRenderOnUpdate
	Action_t3619184611 * ___mRenderOnUpdate_15;
	// System.Action`1<System.Boolean> Vuforia.VuforiaARController::mOnPause
	Action_1_t2936672411 * ___mOnPause_16;
	// System.Boolean Vuforia.VuforiaARController::mPaused
	bool ___mPaused_17;
	// System.Action Vuforia.VuforiaARController::mOnBackgroundTextureChanged
	Action_t3619184611 * ___mOnBackgroundTextureChanged_18;
	// System.Boolean Vuforia.VuforiaARController::mStartHasBeenInvoked
	bool ___mStartHasBeenInvoked_19;
	// System.Boolean Vuforia.VuforiaARController::mHasStarted
	bool ___mHasStarted_20;
	// System.Boolean Vuforia.VuforiaARController::mBackgroundTextureHasChanged
	bool ___mBackgroundTextureHasChanged_21;
	// Vuforia.ICameraConfiguration Vuforia.VuforiaARController::mCameraConfiguration
	RuntimeObject* ___mCameraConfiguration_22;
	// Vuforia.DigitalEyewearARController Vuforia.VuforiaARController::mEyewearBehaviour
	DigitalEyewearARController_t1877756253 * ___mEyewearBehaviour_23;
	// Vuforia.VideoBackgroundManager Vuforia.VuforiaARController::mVideoBackgroundMgr
	VideoBackgroundManager_t2068552549 * ___mVideoBackgroundMgr_24;
	// System.Boolean Vuforia.VuforiaARController::mCheckStopCamera
	bool ___mCheckStopCamera_25;
	// UnityEngine.Material Vuforia.VuforiaARController::mClearMaterial
	Material_t1079520667 * ___mClearMaterial_26;
	// System.Boolean Vuforia.VuforiaARController::mMetalRendering
	bool ___mMetalRendering_27;
	// System.Boolean Vuforia.VuforiaARController::mHasStartedOnce
	bool ___mHasStartedOnce_28;
	// System.Boolean Vuforia.VuforiaARController::mWasEnabledBeforePause
	bool ___mWasEnabledBeforePause_29;
	// System.Boolean Vuforia.VuforiaARController::mObjectTrackerWasActiveBeforePause
	bool ___mObjectTrackerWasActiveBeforePause_30;
	// System.Boolean Vuforia.VuforiaARController::mObjectTrackerWasActiveBeforeDisabling
	bool ___mObjectTrackerWasActiveBeforeDisabling_31;
	// System.Int32 Vuforia.VuforiaARController::mLastUpdatedFrame
	int32_t ___mLastUpdatedFrame_32;
	// System.Collections.Generic.List`1<System.Type> Vuforia.VuforiaARController::mTrackersRequestedToDeinit
	List_1_t3593106846 * ___mTrackersRequestedToDeinit_33;
	// System.Boolean Vuforia.VuforiaARController::mMissedToApplyLeftProjectionMatrix
	bool ___mMissedToApplyLeftProjectionMatrix_34;
	// System.Boolean Vuforia.VuforiaARController::mMissedToApplyRightProjectionMatrix
	bool ___mMissedToApplyRightProjectionMatrix_35;
	// UnityEngine.Matrix4x4 Vuforia.VuforiaARController::mLeftProjectMatrixToApply
	Matrix4x4_t4266809202  ___mLeftProjectMatrixToApply_36;
	// UnityEngine.Matrix4x4 Vuforia.VuforiaARController::mRightProjectMatrixToApply
	Matrix4x4_t4266809202  ___mRightProjectMatrixToApply_37;

public:
	inline static int32_t get_offset_of_CameraDeviceModeSetting_1() { return static_cast<int32_t>(offsetof(VuforiaARController_t1328503142, ___CameraDeviceModeSetting_1)); }
	inline int32_t get_CameraDeviceModeSetting_1() const { return ___CameraDeviceModeSetting_1; }
	inline int32_t* get_address_of_CameraDeviceModeSetting_1() { return &___CameraDeviceModeSetting_1; }
	inline void set_CameraDeviceModeSetting_1(int32_t value)
	{
		___CameraDeviceModeSetting_1 = value;
	}

	inline static int32_t get_offset_of_MaxSimultaneousImageTargets_2() { return static_cast<int32_t>(offsetof(VuforiaARController_t1328503142, ___MaxSimultaneousImageTargets_2)); }
	inline int32_t get_MaxSimultaneousImageTargets_2() const { return ___MaxSimultaneousImageTargets_2; }
	inline int32_t* get_address_of_MaxSimultaneousImageTargets_2() { return &___MaxSimultaneousImageTargets_2; }
	inline void set_MaxSimultaneousImageTargets_2(int32_t value)
	{
		___MaxSimultaneousImageTargets_2 = value;
	}

	inline static int32_t get_offset_of_MaxSimultaneousObjectTargets_3() { return static_cast<int32_t>(offsetof(VuforiaARController_t1328503142, ___MaxSimultaneousObjectTargets_3)); }
	inline int32_t get_MaxSimultaneousObjectTargets_3() const { return ___MaxSimultaneousObjectTargets_3; }
	inline int32_t* get_address_of_MaxSimultaneousObjectTargets_3() { return &___MaxSimultaneousObjectTargets_3; }
	inline void set_MaxSimultaneousObjectTargets_3(int32_t value)
	{
		___MaxSimultaneousObjectTargets_3 = value;
	}

	inline static int32_t get_offset_of_UseDelayedLoadingObjectTargets_4() { return static_cast<int32_t>(offsetof(VuforiaARController_t1328503142, ___UseDelayedLoadingObjectTargets_4)); }
	inline bool get_UseDelayedLoadingObjectTargets_4() const { return ___UseDelayedLoadingObjectTargets_4; }
	inline bool* get_address_of_UseDelayedLoadingObjectTargets_4() { return &___UseDelayedLoadingObjectTargets_4; }
	inline void set_UseDelayedLoadingObjectTargets_4(bool value)
	{
		___UseDelayedLoadingObjectTargets_4 = value;
	}

	inline static int32_t get_offset_of_CameraDirection_5() { return static_cast<int32_t>(offsetof(VuforiaARController_t1328503142, ___CameraDirection_5)); }
	inline int32_t get_CameraDirection_5() const { return ___CameraDirection_5; }
	inline int32_t* get_address_of_CameraDirection_5() { return &___CameraDirection_5; }
	inline void set_CameraDirection_5(int32_t value)
	{
		___CameraDirection_5 = value;
	}

	inline static int32_t get_offset_of_MirrorVideoBackground_6() { return static_cast<int32_t>(offsetof(VuforiaARController_t1328503142, ___MirrorVideoBackground_6)); }
	inline int32_t get_MirrorVideoBackground_6() const { return ___MirrorVideoBackground_6; }
	inline int32_t* get_address_of_MirrorVideoBackground_6() { return &___MirrorVideoBackground_6; }
	inline void set_MirrorVideoBackground_6(int32_t value)
	{
		___MirrorVideoBackground_6 = value;
	}

	inline static int32_t get_offset_of_mWorldCenterMode_7() { return static_cast<int32_t>(offsetof(VuforiaARController_t1328503142, ___mWorldCenterMode_7)); }
	inline int32_t get_mWorldCenterMode_7() const { return ___mWorldCenterMode_7; }
	inline int32_t* get_address_of_mWorldCenterMode_7() { return &___mWorldCenterMode_7; }
	inline void set_mWorldCenterMode_7(int32_t value)
	{
		___mWorldCenterMode_7 = value;
	}

	inline static int32_t get_offset_of_mWorldCenter_8() { return static_cast<int32_t>(offsetof(VuforiaARController_t1328503142, ___mWorldCenter_8)); }
	inline TrackableBehaviour_t2419079356 * get_mWorldCenter_8() const { return ___mWorldCenter_8; }
	inline TrackableBehaviour_t2419079356 ** get_address_of_mWorldCenter_8() { return &___mWorldCenter_8; }
	inline void set_mWorldCenter_8(TrackableBehaviour_t2419079356 * value)
	{
		___mWorldCenter_8 = value;
		Il2CppCodeGenWriteBarrier((&___mWorldCenter_8), value);
	}

	inline static int32_t get_offset_of_mTrackerEventHandlers_9() { return static_cast<int32_t>(offsetof(VuforiaARController_t1328503142, ___mTrackerEventHandlers_9)); }
	inline List_1_t1947635164 * get_mTrackerEventHandlers_9() const { return ___mTrackerEventHandlers_9; }
	inline List_1_t1947635164 ** get_address_of_mTrackerEventHandlers_9() { return &___mTrackerEventHandlers_9; }
	inline void set_mTrackerEventHandlers_9(List_1_t1947635164 * value)
	{
		___mTrackerEventHandlers_9 = value;
		Il2CppCodeGenWriteBarrier((&___mTrackerEventHandlers_9), value);
	}

	inline static int32_t get_offset_of_mVideoBgEventHandlers_10() { return static_cast<int32_t>(offsetof(VuforiaARController_t1328503142, ___mVideoBgEventHandlers_10)); }
	inline List_1_t485517045 * get_mVideoBgEventHandlers_10() const { return ___mVideoBgEventHandlers_10; }
	inline List_1_t485517045 ** get_address_of_mVideoBgEventHandlers_10() { return &___mVideoBgEventHandlers_10; }
	inline void set_mVideoBgEventHandlers_10(List_1_t485517045 * value)
	{
		___mVideoBgEventHandlers_10 = value;
		Il2CppCodeGenWriteBarrier((&___mVideoBgEventHandlers_10), value);
	}

	inline static int32_t get_offset_of_mOnVuforiaInitialized_11() { return static_cast<int32_t>(offsetof(VuforiaARController_t1328503142, ___mOnVuforiaInitialized_11)); }
	inline Action_t3619184611 * get_mOnVuforiaInitialized_11() const { return ___mOnVuforiaInitialized_11; }
	inline Action_t3619184611 ** get_address_of_mOnVuforiaInitialized_11() { return &___mOnVuforiaInitialized_11; }
	inline void set_mOnVuforiaInitialized_11(Action_t3619184611 * value)
	{
		___mOnVuforiaInitialized_11 = value;
		Il2CppCodeGenWriteBarrier((&___mOnVuforiaInitialized_11), value);
	}

	inline static int32_t get_offset_of_mOnVuforiaStarted_12() { return static_cast<int32_t>(offsetof(VuforiaARController_t1328503142, ___mOnVuforiaStarted_12)); }
	inline Action_t3619184611 * get_mOnVuforiaStarted_12() const { return ___mOnVuforiaStarted_12; }
	inline Action_t3619184611 ** get_address_of_mOnVuforiaStarted_12() { return &___mOnVuforiaStarted_12; }
	inline void set_mOnVuforiaStarted_12(Action_t3619184611 * value)
	{
		___mOnVuforiaStarted_12 = value;
		Il2CppCodeGenWriteBarrier((&___mOnVuforiaStarted_12), value);
	}

	inline static int32_t get_offset_of_mOnVuforiaDeinitialized_13() { return static_cast<int32_t>(offsetof(VuforiaARController_t1328503142, ___mOnVuforiaDeinitialized_13)); }
	inline Action_t3619184611 * get_mOnVuforiaDeinitialized_13() const { return ___mOnVuforiaDeinitialized_13; }
	inline Action_t3619184611 ** get_address_of_mOnVuforiaDeinitialized_13() { return &___mOnVuforiaDeinitialized_13; }
	inline void set_mOnVuforiaDeinitialized_13(Action_t3619184611 * value)
	{
		___mOnVuforiaDeinitialized_13 = value;
		Il2CppCodeGenWriteBarrier((&___mOnVuforiaDeinitialized_13), value);
	}

	inline static int32_t get_offset_of_mOnTrackablesUpdated_14() { return static_cast<int32_t>(offsetof(VuforiaARController_t1328503142, ___mOnTrackablesUpdated_14)); }
	inline Action_t3619184611 * get_mOnTrackablesUpdated_14() const { return ___mOnTrackablesUpdated_14; }
	inline Action_t3619184611 ** get_address_of_mOnTrackablesUpdated_14() { return &___mOnTrackablesUpdated_14; }
	inline void set_mOnTrackablesUpdated_14(Action_t3619184611 * value)
	{
		___mOnTrackablesUpdated_14 = value;
		Il2CppCodeGenWriteBarrier((&___mOnTrackablesUpdated_14), value);
	}

	inline static int32_t get_offset_of_mRenderOnUpdate_15() { return static_cast<int32_t>(offsetof(VuforiaARController_t1328503142, ___mRenderOnUpdate_15)); }
	inline Action_t3619184611 * get_mRenderOnUpdate_15() const { return ___mRenderOnUpdate_15; }
	inline Action_t3619184611 ** get_address_of_mRenderOnUpdate_15() { return &___mRenderOnUpdate_15; }
	inline void set_mRenderOnUpdate_15(Action_t3619184611 * value)
	{
		___mRenderOnUpdate_15 = value;
		Il2CppCodeGenWriteBarrier((&___mRenderOnUpdate_15), value);
	}

	inline static int32_t get_offset_of_mOnPause_16() { return static_cast<int32_t>(offsetof(VuforiaARController_t1328503142, ___mOnPause_16)); }
	inline Action_1_t2936672411 * get_mOnPause_16() const { return ___mOnPause_16; }
	inline Action_1_t2936672411 ** get_address_of_mOnPause_16() { return &___mOnPause_16; }
	inline void set_mOnPause_16(Action_1_t2936672411 * value)
	{
		___mOnPause_16 = value;
		Il2CppCodeGenWriteBarrier((&___mOnPause_16), value);
	}

	inline static int32_t get_offset_of_mPaused_17() { return static_cast<int32_t>(offsetof(VuforiaARController_t1328503142, ___mPaused_17)); }
	inline bool get_mPaused_17() const { return ___mPaused_17; }
	inline bool* get_address_of_mPaused_17() { return &___mPaused_17; }
	inline void set_mPaused_17(bool value)
	{
		___mPaused_17 = value;
	}

	inline static int32_t get_offset_of_mOnBackgroundTextureChanged_18() { return static_cast<int32_t>(offsetof(VuforiaARController_t1328503142, ___mOnBackgroundTextureChanged_18)); }
	inline Action_t3619184611 * get_mOnBackgroundTextureChanged_18() const { return ___mOnBackgroundTextureChanged_18; }
	inline Action_t3619184611 ** get_address_of_mOnBackgroundTextureChanged_18() { return &___mOnBackgroundTextureChanged_18; }
	inline void set_mOnBackgroundTextureChanged_18(Action_t3619184611 * value)
	{
		___mOnBackgroundTextureChanged_18 = value;
		Il2CppCodeGenWriteBarrier((&___mOnBackgroundTextureChanged_18), value);
	}

	inline static int32_t get_offset_of_mStartHasBeenInvoked_19() { return static_cast<int32_t>(offsetof(VuforiaARController_t1328503142, ___mStartHasBeenInvoked_19)); }
	inline bool get_mStartHasBeenInvoked_19() const { return ___mStartHasBeenInvoked_19; }
	inline bool* get_address_of_mStartHasBeenInvoked_19() { return &___mStartHasBeenInvoked_19; }
	inline void set_mStartHasBeenInvoked_19(bool value)
	{
		___mStartHasBeenInvoked_19 = value;
	}

	inline static int32_t get_offset_of_mHasStarted_20() { return static_cast<int32_t>(offsetof(VuforiaARController_t1328503142, ___mHasStarted_20)); }
	inline bool get_mHasStarted_20() const { return ___mHasStarted_20; }
	inline bool* get_address_of_mHasStarted_20() { return &___mHasStarted_20; }
	inline void set_mHasStarted_20(bool value)
	{
		___mHasStarted_20 = value;
	}

	inline static int32_t get_offset_of_mBackgroundTextureHasChanged_21() { return static_cast<int32_t>(offsetof(VuforiaARController_t1328503142, ___mBackgroundTextureHasChanged_21)); }
	inline bool get_mBackgroundTextureHasChanged_21() const { return ___mBackgroundTextureHasChanged_21; }
	inline bool* get_address_of_mBackgroundTextureHasChanged_21() { return &___mBackgroundTextureHasChanged_21; }
	inline void set_mBackgroundTextureHasChanged_21(bool value)
	{
		___mBackgroundTextureHasChanged_21 = value;
	}

	inline static int32_t get_offset_of_mCameraConfiguration_22() { return static_cast<int32_t>(offsetof(VuforiaARController_t1328503142, ___mCameraConfiguration_22)); }
	inline RuntimeObject* get_mCameraConfiguration_22() const { return ___mCameraConfiguration_22; }
	inline RuntimeObject** get_address_of_mCameraConfiguration_22() { return &___mCameraConfiguration_22; }
	inline void set_mCameraConfiguration_22(RuntimeObject* value)
	{
		___mCameraConfiguration_22 = value;
		Il2CppCodeGenWriteBarrier((&___mCameraConfiguration_22), value);
	}

	inline static int32_t get_offset_of_mEyewearBehaviour_23() { return static_cast<int32_t>(offsetof(VuforiaARController_t1328503142, ___mEyewearBehaviour_23)); }
	inline DigitalEyewearARController_t1877756253 * get_mEyewearBehaviour_23() const { return ___mEyewearBehaviour_23; }
	inline DigitalEyewearARController_t1877756253 ** get_address_of_mEyewearBehaviour_23() { return &___mEyewearBehaviour_23; }
	inline void set_mEyewearBehaviour_23(DigitalEyewearARController_t1877756253 * value)
	{
		___mEyewearBehaviour_23 = value;
		Il2CppCodeGenWriteBarrier((&___mEyewearBehaviour_23), value);
	}

	inline static int32_t get_offset_of_mVideoBackgroundMgr_24() { return static_cast<int32_t>(offsetof(VuforiaARController_t1328503142, ___mVideoBackgroundMgr_24)); }
	inline VideoBackgroundManager_t2068552549 * get_mVideoBackgroundMgr_24() const { return ___mVideoBackgroundMgr_24; }
	inline VideoBackgroundManager_t2068552549 ** get_address_of_mVideoBackgroundMgr_24() { return &___mVideoBackgroundMgr_24; }
	inline void set_mVideoBackgroundMgr_24(VideoBackgroundManager_t2068552549 * value)
	{
		___mVideoBackgroundMgr_24 = value;
		Il2CppCodeGenWriteBarrier((&___mVideoBackgroundMgr_24), value);
	}

	inline static int32_t get_offset_of_mCheckStopCamera_25() { return static_cast<int32_t>(offsetof(VuforiaARController_t1328503142, ___mCheckStopCamera_25)); }
	inline bool get_mCheckStopCamera_25() const { return ___mCheckStopCamera_25; }
	inline bool* get_address_of_mCheckStopCamera_25() { return &___mCheckStopCamera_25; }
	inline void set_mCheckStopCamera_25(bool value)
	{
		___mCheckStopCamera_25 = value;
	}

	inline static int32_t get_offset_of_mClearMaterial_26() { return static_cast<int32_t>(offsetof(VuforiaARController_t1328503142, ___mClearMaterial_26)); }
	inline Material_t1079520667 * get_mClearMaterial_26() const { return ___mClearMaterial_26; }
	inline Material_t1079520667 ** get_address_of_mClearMaterial_26() { return &___mClearMaterial_26; }
	inline void set_mClearMaterial_26(Material_t1079520667 * value)
	{
		___mClearMaterial_26 = value;
		Il2CppCodeGenWriteBarrier((&___mClearMaterial_26), value);
	}

	inline static int32_t get_offset_of_mMetalRendering_27() { return static_cast<int32_t>(offsetof(VuforiaARController_t1328503142, ___mMetalRendering_27)); }
	inline bool get_mMetalRendering_27() const { return ___mMetalRendering_27; }
	inline bool* get_address_of_mMetalRendering_27() { return &___mMetalRendering_27; }
	inline void set_mMetalRendering_27(bool value)
	{
		___mMetalRendering_27 = value;
	}

	inline static int32_t get_offset_of_mHasStartedOnce_28() { return static_cast<int32_t>(offsetof(VuforiaARController_t1328503142, ___mHasStartedOnce_28)); }
	inline bool get_mHasStartedOnce_28() const { return ___mHasStartedOnce_28; }
	inline bool* get_address_of_mHasStartedOnce_28() { return &___mHasStartedOnce_28; }
	inline void set_mHasStartedOnce_28(bool value)
	{
		___mHasStartedOnce_28 = value;
	}

	inline static int32_t get_offset_of_mWasEnabledBeforePause_29() { return static_cast<int32_t>(offsetof(VuforiaARController_t1328503142, ___mWasEnabledBeforePause_29)); }
	inline bool get_mWasEnabledBeforePause_29() const { return ___mWasEnabledBeforePause_29; }
	inline bool* get_address_of_mWasEnabledBeforePause_29() { return &___mWasEnabledBeforePause_29; }
	inline void set_mWasEnabledBeforePause_29(bool value)
	{
		___mWasEnabledBeforePause_29 = value;
	}

	inline static int32_t get_offset_of_mObjectTrackerWasActiveBeforePause_30() { return static_cast<int32_t>(offsetof(VuforiaARController_t1328503142, ___mObjectTrackerWasActiveBeforePause_30)); }
	inline bool get_mObjectTrackerWasActiveBeforePause_30() const { return ___mObjectTrackerWasActiveBeforePause_30; }
	inline bool* get_address_of_mObjectTrackerWasActiveBeforePause_30() { return &___mObjectTrackerWasActiveBeforePause_30; }
	inline void set_mObjectTrackerWasActiveBeforePause_30(bool value)
	{
		___mObjectTrackerWasActiveBeforePause_30 = value;
	}

	inline static int32_t get_offset_of_mObjectTrackerWasActiveBeforeDisabling_31() { return static_cast<int32_t>(offsetof(VuforiaARController_t1328503142, ___mObjectTrackerWasActiveBeforeDisabling_31)); }
	inline bool get_mObjectTrackerWasActiveBeforeDisabling_31() const { return ___mObjectTrackerWasActiveBeforeDisabling_31; }
	inline bool* get_address_of_mObjectTrackerWasActiveBeforeDisabling_31() { return &___mObjectTrackerWasActiveBeforeDisabling_31; }
	inline void set_mObjectTrackerWasActiveBeforeDisabling_31(bool value)
	{
		___mObjectTrackerWasActiveBeforeDisabling_31 = value;
	}

	inline static int32_t get_offset_of_mLastUpdatedFrame_32() { return static_cast<int32_t>(offsetof(VuforiaARController_t1328503142, ___mLastUpdatedFrame_32)); }
	inline int32_t get_mLastUpdatedFrame_32() const { return ___mLastUpdatedFrame_32; }
	inline int32_t* get_address_of_mLastUpdatedFrame_32() { return &___mLastUpdatedFrame_32; }
	inline void set_mLastUpdatedFrame_32(int32_t value)
	{
		___mLastUpdatedFrame_32 = value;
	}

	inline static int32_t get_offset_of_mTrackersRequestedToDeinit_33() { return static_cast<int32_t>(offsetof(VuforiaARController_t1328503142, ___mTrackersRequestedToDeinit_33)); }
	inline List_1_t3593106846 * get_mTrackersRequestedToDeinit_33() const { return ___mTrackersRequestedToDeinit_33; }
	inline List_1_t3593106846 ** get_address_of_mTrackersRequestedToDeinit_33() { return &___mTrackersRequestedToDeinit_33; }
	inline void set_mTrackersRequestedToDeinit_33(List_1_t3593106846 * value)
	{
		___mTrackersRequestedToDeinit_33 = value;
		Il2CppCodeGenWriteBarrier((&___mTrackersRequestedToDeinit_33), value);
	}

	inline static int32_t get_offset_of_mMissedToApplyLeftProjectionMatrix_34() { return static_cast<int32_t>(offsetof(VuforiaARController_t1328503142, ___mMissedToApplyLeftProjectionMatrix_34)); }
	inline bool get_mMissedToApplyLeftProjectionMatrix_34() const { return ___mMissedToApplyLeftProjectionMatrix_34; }
	inline bool* get_address_of_mMissedToApplyLeftProjectionMatrix_34() { return &___mMissedToApplyLeftProjectionMatrix_34; }
	inline void set_mMissedToApplyLeftProjectionMatrix_34(bool value)
	{
		___mMissedToApplyLeftProjectionMatrix_34 = value;
	}

	inline static int32_t get_offset_of_mMissedToApplyRightProjectionMatrix_35() { return static_cast<int32_t>(offsetof(VuforiaARController_t1328503142, ___mMissedToApplyRightProjectionMatrix_35)); }
	inline bool get_mMissedToApplyRightProjectionMatrix_35() const { return ___mMissedToApplyRightProjectionMatrix_35; }
	inline bool* get_address_of_mMissedToApplyRightProjectionMatrix_35() { return &___mMissedToApplyRightProjectionMatrix_35; }
	inline void set_mMissedToApplyRightProjectionMatrix_35(bool value)
	{
		___mMissedToApplyRightProjectionMatrix_35 = value;
	}

	inline static int32_t get_offset_of_mLeftProjectMatrixToApply_36() { return static_cast<int32_t>(offsetof(VuforiaARController_t1328503142, ___mLeftProjectMatrixToApply_36)); }
	inline Matrix4x4_t4266809202  get_mLeftProjectMatrixToApply_36() const { return ___mLeftProjectMatrixToApply_36; }
	inline Matrix4x4_t4266809202 * get_address_of_mLeftProjectMatrixToApply_36() { return &___mLeftProjectMatrixToApply_36; }
	inline void set_mLeftProjectMatrixToApply_36(Matrix4x4_t4266809202  value)
	{
		___mLeftProjectMatrixToApply_36 = value;
	}

	inline static int32_t get_offset_of_mRightProjectMatrixToApply_37() { return static_cast<int32_t>(offsetof(VuforiaARController_t1328503142, ___mRightProjectMatrixToApply_37)); }
	inline Matrix4x4_t4266809202  get_mRightProjectMatrixToApply_37() const { return ___mRightProjectMatrixToApply_37; }
	inline Matrix4x4_t4266809202 * get_address_of_mRightProjectMatrixToApply_37() { return &___mRightProjectMatrixToApply_37; }
	inline void set_mRightProjectMatrixToApply_37(Matrix4x4_t4266809202  value)
	{
		___mRightProjectMatrixToApply_37 = value;
	}
};

struct VuforiaARController_t1328503142_StaticFields
{
public:
	// Vuforia.VuforiaARController Vuforia.VuforiaARController::mInstance
	VuforiaARController_t1328503142 * ___mInstance_38;
	// System.Object Vuforia.VuforiaARController::mPadlock
	RuntimeObject * ___mPadlock_39;

public:
	inline static int32_t get_offset_of_mInstance_38() { return static_cast<int32_t>(offsetof(VuforiaARController_t1328503142_StaticFields, ___mInstance_38)); }
	inline VuforiaARController_t1328503142 * get_mInstance_38() const { return ___mInstance_38; }
	inline VuforiaARController_t1328503142 ** get_address_of_mInstance_38() { return &___mInstance_38; }
	inline void set_mInstance_38(VuforiaARController_t1328503142 * value)
	{
		___mInstance_38 = value;
		Il2CppCodeGenWriteBarrier((&___mInstance_38), value);
	}

	inline static int32_t get_offset_of_mPadlock_39() { return static_cast<int32_t>(offsetof(VuforiaARController_t1328503142_StaticFields, ___mPadlock_39)); }
	inline RuntimeObject * get_mPadlock_39() const { return ___mPadlock_39; }
	inline RuntimeObject ** get_address_of_mPadlock_39() { return &___mPadlock_39; }
	inline void set_mPadlock_39(RuntimeObject * value)
	{
		___mPadlock_39 = value;
		Il2CppCodeGenWriteBarrier((&___mPadlock_39), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VUFORIAARCONTROLLER_T1328503142_H
#ifndef ANDROIDUNITYPLAYER_T3571566439_H
#define ANDROIDUNITYPLAYER_T3571566439_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.AndroidUnityPlayer
struct  AndroidUnityPlayer_t3571566439  : public RuntimeObject
{
public:
	// UnityEngine.ScreenOrientation Vuforia.AndroidUnityPlayer::mScreenOrientation
	int32_t ___mScreenOrientation_2;
	// UnityEngine.ScreenOrientation Vuforia.AndroidUnityPlayer::mJavaScreenOrientation
	int32_t ___mJavaScreenOrientation_3;
	// System.Int32 Vuforia.AndroidUnityPlayer::mFramesSinceLastOrientationReset
	int32_t ___mFramesSinceLastOrientationReset_4;
	// System.Int32 Vuforia.AndroidUnityPlayer::mFramesSinceLastJavaOrientationCheck
	int32_t ___mFramesSinceLastJavaOrientationCheck_5;

public:
	inline static int32_t get_offset_of_mScreenOrientation_2() { return static_cast<int32_t>(offsetof(AndroidUnityPlayer_t3571566439, ___mScreenOrientation_2)); }
	inline int32_t get_mScreenOrientation_2() const { return ___mScreenOrientation_2; }
	inline int32_t* get_address_of_mScreenOrientation_2() { return &___mScreenOrientation_2; }
	inline void set_mScreenOrientation_2(int32_t value)
	{
		___mScreenOrientation_2 = value;
	}

	inline static int32_t get_offset_of_mJavaScreenOrientation_3() { return static_cast<int32_t>(offsetof(AndroidUnityPlayer_t3571566439, ___mJavaScreenOrientation_3)); }
	inline int32_t get_mJavaScreenOrientation_3() const { return ___mJavaScreenOrientation_3; }
	inline int32_t* get_address_of_mJavaScreenOrientation_3() { return &___mJavaScreenOrientation_3; }
	inline void set_mJavaScreenOrientation_3(int32_t value)
	{
		___mJavaScreenOrientation_3 = value;
	}

	inline static int32_t get_offset_of_mFramesSinceLastOrientationReset_4() { return static_cast<int32_t>(offsetof(AndroidUnityPlayer_t3571566439, ___mFramesSinceLastOrientationReset_4)); }
	inline int32_t get_mFramesSinceLastOrientationReset_4() const { return ___mFramesSinceLastOrientationReset_4; }
	inline int32_t* get_address_of_mFramesSinceLastOrientationReset_4() { return &___mFramesSinceLastOrientationReset_4; }
	inline void set_mFramesSinceLastOrientationReset_4(int32_t value)
	{
		___mFramesSinceLastOrientationReset_4 = value;
	}

	inline static int32_t get_offset_of_mFramesSinceLastJavaOrientationCheck_5() { return static_cast<int32_t>(offsetof(AndroidUnityPlayer_t3571566439, ___mFramesSinceLastJavaOrientationCheck_5)); }
	inline int32_t get_mFramesSinceLastJavaOrientationCheck_5() const { return ___mFramesSinceLastJavaOrientationCheck_5; }
	inline int32_t* get_address_of_mFramesSinceLastJavaOrientationCheck_5() { return &___mFramesSinceLastJavaOrientationCheck_5; }
	inline void set_mFramesSinceLastJavaOrientationCheck_5(int32_t value)
	{
		___mFramesSinceLastJavaOrientationCheck_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANDROIDUNITYPLAYER_T3571566439_H
#ifndef VUFORIARUNTIME_T2131788666_H
#define VUFORIARUNTIME_T2131788666_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.VuforiaRuntime
struct  VuforiaRuntime_t2131788666  : public RuntimeObject
{
public:
	// System.Action`1<Vuforia.VuforiaUnity/InitError> Vuforia.VuforiaRuntime::mOnVuforiaInitError
	Action_1_t3472505684 * ___mOnVuforiaInitError_0;
	// System.Boolean Vuforia.VuforiaRuntime::mFailedToInitialize
	bool ___mFailedToInitialize_1;
	// Vuforia.VuforiaUnity/InitError Vuforia.VuforiaRuntime::mInitError
	int32_t ___mInitError_2;
	// System.Boolean Vuforia.VuforiaRuntime::mHasInitialized
	bool ___mHasInitialized_3;

public:
	inline static int32_t get_offset_of_mOnVuforiaInitError_0() { return static_cast<int32_t>(offsetof(VuforiaRuntime_t2131788666, ___mOnVuforiaInitError_0)); }
	inline Action_1_t3472505684 * get_mOnVuforiaInitError_0() const { return ___mOnVuforiaInitError_0; }
	inline Action_1_t3472505684 ** get_address_of_mOnVuforiaInitError_0() { return &___mOnVuforiaInitError_0; }
	inline void set_mOnVuforiaInitError_0(Action_1_t3472505684 * value)
	{
		___mOnVuforiaInitError_0 = value;
		Il2CppCodeGenWriteBarrier((&___mOnVuforiaInitError_0), value);
	}

	inline static int32_t get_offset_of_mFailedToInitialize_1() { return static_cast<int32_t>(offsetof(VuforiaRuntime_t2131788666, ___mFailedToInitialize_1)); }
	inline bool get_mFailedToInitialize_1() const { return ___mFailedToInitialize_1; }
	inline bool* get_address_of_mFailedToInitialize_1() { return &___mFailedToInitialize_1; }
	inline void set_mFailedToInitialize_1(bool value)
	{
		___mFailedToInitialize_1 = value;
	}

	inline static int32_t get_offset_of_mInitError_2() { return static_cast<int32_t>(offsetof(VuforiaRuntime_t2131788666, ___mInitError_2)); }
	inline int32_t get_mInitError_2() const { return ___mInitError_2; }
	inline int32_t* get_address_of_mInitError_2() { return &___mInitError_2; }
	inline void set_mInitError_2(int32_t value)
	{
		___mInitError_2 = value;
	}

	inline static int32_t get_offset_of_mHasInitialized_3() { return static_cast<int32_t>(offsetof(VuforiaRuntime_t2131788666, ___mHasInitialized_3)); }
	inline bool get_mHasInitialized_3() const { return ___mHasInitialized_3; }
	inline bool* get_address_of_mHasInitialized_3() { return &___mHasInitialized_3; }
	inline void set_mHasInitialized_3(bool value)
	{
		___mHasInitialized_3 = value;
	}
};

struct VuforiaRuntime_t2131788666_StaticFields
{
public:
	// Vuforia.VuforiaRuntime Vuforia.VuforiaRuntime::mInstance
	VuforiaRuntime_t2131788666 * ___mInstance_4;
	// System.Object Vuforia.VuforiaRuntime::mPadlock
	RuntimeObject * ___mPadlock_5;

public:
	inline static int32_t get_offset_of_mInstance_4() { return static_cast<int32_t>(offsetof(VuforiaRuntime_t2131788666_StaticFields, ___mInstance_4)); }
	inline VuforiaRuntime_t2131788666 * get_mInstance_4() const { return ___mInstance_4; }
	inline VuforiaRuntime_t2131788666 ** get_address_of_mInstance_4() { return &___mInstance_4; }
	inline void set_mInstance_4(VuforiaRuntime_t2131788666 * value)
	{
		___mInstance_4 = value;
		Il2CppCodeGenWriteBarrier((&___mInstance_4), value);
	}

	inline static int32_t get_offset_of_mPadlock_5() { return static_cast<int32_t>(offsetof(VuforiaRuntime_t2131788666_StaticFields, ___mPadlock_5)); }
	inline RuntimeObject * get_mPadlock_5() const { return ___mPadlock_5; }
	inline RuntimeObject ** get_address_of_mPadlock_5() { return &___mPadlock_5; }
	inline void set_mPadlock_5(RuntimeObject * value)
	{
		___mPadlock_5 = value;
		Il2CppCodeGenWriteBarrier((&___mPadlock_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VUFORIARUNTIME_T2131788666_H
#ifndef DIGITALEYEWEARARCONTROLLER_T1877756253_H
#define DIGITALEYEWEARARCONTROLLER_T1877756253_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.DigitalEyewearARController
struct  DigitalEyewearARController_t1877756253  : public ARController_t1205915989
{
public:
	// System.Single Vuforia.DigitalEyewearARController::mCameraOffset
	float ___mCameraOffset_7;
	// Vuforia.DistortionRenderingMode Vuforia.DigitalEyewearARController::mDistortionRenderingMode
	int32_t ___mDistortionRenderingMode_8;
	// System.Int32 Vuforia.DigitalEyewearARController::mDistortionRenderingLayer
	int32_t ___mDistortionRenderingLayer_9;
	// Vuforia.DigitalEyewearARController/EyewearType Vuforia.DigitalEyewearARController::mEyewearType
	int32_t ___mEyewearType_10;
	// Vuforia.DigitalEyewearARController/StereoFramework Vuforia.DigitalEyewearARController::mStereoFramework
	int32_t ___mStereoFramework_11;
	// Vuforia.DigitalEyewearARController/SeeThroughConfiguration Vuforia.DigitalEyewearARController::mSeeThroughConfiguration
	int32_t ___mSeeThroughConfiguration_12;
	// System.String Vuforia.DigitalEyewearARController::mViewerName
	String_t* ___mViewerName_13;
	// System.String Vuforia.DigitalEyewearARController::mViewerManufacturer
	String_t* ___mViewerManufacturer_14;
	// System.Boolean Vuforia.DigitalEyewearARController::mUseCustomViewer
	bool ___mUseCustomViewer_15;
	// Vuforia.DigitalEyewearARController/SerializableViewerParameters Vuforia.DigitalEyewearARController::mCustomViewer
	SerializableViewerParameters_t484734121 * ___mCustomViewer_16;
	// UnityEngine.Transform Vuforia.DigitalEyewearARController::mCentralAnchorPoint
	Transform_t3316442598 * ___mCentralAnchorPoint_17;
	// UnityEngine.Transform Vuforia.DigitalEyewearARController::mParentAnchorPoint
	Transform_t3316442598 * ___mParentAnchorPoint_18;
	// UnityEngine.Camera Vuforia.DigitalEyewearARController::mPrimaryCamera
	Camera_t3175186167 * ___mPrimaryCamera_19;
	// UnityEngine.Rect Vuforia.DigitalEyewearARController::mPrimaryCameraOriginalRect
	Rect_t3436776195  ___mPrimaryCameraOriginalRect_20;
	// UnityEngine.Camera Vuforia.DigitalEyewearARController::mSecondaryCamera
	Camera_t3175186167 * ___mSecondaryCamera_21;
	// UnityEngine.Rect Vuforia.DigitalEyewearARController::mSecondaryCameraOriginalRect
	Rect_t3436776195  ___mSecondaryCameraOriginalRect_22;
	// System.Boolean Vuforia.DigitalEyewearARController::mSecondaryCameraDisabledLocally
	bool ___mSecondaryCameraDisabledLocally_23;
	// Vuforia.VuforiaARController Vuforia.DigitalEyewearARController::mVuforiaBehaviour
	VuforiaARController_t1328503142 * ___mVuforiaBehaviour_24;
	// Vuforia.DistortionRenderingBehaviour Vuforia.DigitalEyewearARController::mDistortionRenderingBhvr
	DistortionRenderingBehaviour_t2785666256 * ___mDistortionRenderingBhvr_25;
	// System.Boolean Vuforia.DigitalEyewearARController::mSetFocusPlaneAutomatically
	bool ___mSetFocusPlaneAutomatically_26;

public:
	inline static int32_t get_offset_of_mCameraOffset_7() { return static_cast<int32_t>(offsetof(DigitalEyewearARController_t1877756253, ___mCameraOffset_7)); }
	inline float get_mCameraOffset_7() const { return ___mCameraOffset_7; }
	inline float* get_address_of_mCameraOffset_7() { return &___mCameraOffset_7; }
	inline void set_mCameraOffset_7(float value)
	{
		___mCameraOffset_7 = value;
	}

	inline static int32_t get_offset_of_mDistortionRenderingMode_8() { return static_cast<int32_t>(offsetof(DigitalEyewearARController_t1877756253, ___mDistortionRenderingMode_8)); }
	inline int32_t get_mDistortionRenderingMode_8() const { return ___mDistortionRenderingMode_8; }
	inline int32_t* get_address_of_mDistortionRenderingMode_8() { return &___mDistortionRenderingMode_8; }
	inline void set_mDistortionRenderingMode_8(int32_t value)
	{
		___mDistortionRenderingMode_8 = value;
	}

	inline static int32_t get_offset_of_mDistortionRenderingLayer_9() { return static_cast<int32_t>(offsetof(DigitalEyewearARController_t1877756253, ___mDistortionRenderingLayer_9)); }
	inline int32_t get_mDistortionRenderingLayer_9() const { return ___mDistortionRenderingLayer_9; }
	inline int32_t* get_address_of_mDistortionRenderingLayer_9() { return &___mDistortionRenderingLayer_9; }
	inline void set_mDistortionRenderingLayer_9(int32_t value)
	{
		___mDistortionRenderingLayer_9 = value;
	}

	inline static int32_t get_offset_of_mEyewearType_10() { return static_cast<int32_t>(offsetof(DigitalEyewearARController_t1877756253, ___mEyewearType_10)); }
	inline int32_t get_mEyewearType_10() const { return ___mEyewearType_10; }
	inline int32_t* get_address_of_mEyewearType_10() { return &___mEyewearType_10; }
	inline void set_mEyewearType_10(int32_t value)
	{
		___mEyewearType_10 = value;
	}

	inline static int32_t get_offset_of_mStereoFramework_11() { return static_cast<int32_t>(offsetof(DigitalEyewearARController_t1877756253, ___mStereoFramework_11)); }
	inline int32_t get_mStereoFramework_11() const { return ___mStereoFramework_11; }
	inline int32_t* get_address_of_mStereoFramework_11() { return &___mStereoFramework_11; }
	inline void set_mStereoFramework_11(int32_t value)
	{
		___mStereoFramework_11 = value;
	}

	inline static int32_t get_offset_of_mSeeThroughConfiguration_12() { return static_cast<int32_t>(offsetof(DigitalEyewearARController_t1877756253, ___mSeeThroughConfiguration_12)); }
	inline int32_t get_mSeeThroughConfiguration_12() const { return ___mSeeThroughConfiguration_12; }
	inline int32_t* get_address_of_mSeeThroughConfiguration_12() { return &___mSeeThroughConfiguration_12; }
	inline void set_mSeeThroughConfiguration_12(int32_t value)
	{
		___mSeeThroughConfiguration_12 = value;
	}

	inline static int32_t get_offset_of_mViewerName_13() { return static_cast<int32_t>(offsetof(DigitalEyewearARController_t1877756253, ___mViewerName_13)); }
	inline String_t* get_mViewerName_13() const { return ___mViewerName_13; }
	inline String_t** get_address_of_mViewerName_13() { return &___mViewerName_13; }
	inline void set_mViewerName_13(String_t* value)
	{
		___mViewerName_13 = value;
		Il2CppCodeGenWriteBarrier((&___mViewerName_13), value);
	}

	inline static int32_t get_offset_of_mViewerManufacturer_14() { return static_cast<int32_t>(offsetof(DigitalEyewearARController_t1877756253, ___mViewerManufacturer_14)); }
	inline String_t* get_mViewerManufacturer_14() const { return ___mViewerManufacturer_14; }
	inline String_t** get_address_of_mViewerManufacturer_14() { return &___mViewerManufacturer_14; }
	inline void set_mViewerManufacturer_14(String_t* value)
	{
		___mViewerManufacturer_14 = value;
		Il2CppCodeGenWriteBarrier((&___mViewerManufacturer_14), value);
	}

	inline static int32_t get_offset_of_mUseCustomViewer_15() { return static_cast<int32_t>(offsetof(DigitalEyewearARController_t1877756253, ___mUseCustomViewer_15)); }
	inline bool get_mUseCustomViewer_15() const { return ___mUseCustomViewer_15; }
	inline bool* get_address_of_mUseCustomViewer_15() { return &___mUseCustomViewer_15; }
	inline void set_mUseCustomViewer_15(bool value)
	{
		___mUseCustomViewer_15 = value;
	}

	inline static int32_t get_offset_of_mCustomViewer_16() { return static_cast<int32_t>(offsetof(DigitalEyewearARController_t1877756253, ___mCustomViewer_16)); }
	inline SerializableViewerParameters_t484734121 * get_mCustomViewer_16() const { return ___mCustomViewer_16; }
	inline SerializableViewerParameters_t484734121 ** get_address_of_mCustomViewer_16() { return &___mCustomViewer_16; }
	inline void set_mCustomViewer_16(SerializableViewerParameters_t484734121 * value)
	{
		___mCustomViewer_16 = value;
		Il2CppCodeGenWriteBarrier((&___mCustomViewer_16), value);
	}

	inline static int32_t get_offset_of_mCentralAnchorPoint_17() { return static_cast<int32_t>(offsetof(DigitalEyewearARController_t1877756253, ___mCentralAnchorPoint_17)); }
	inline Transform_t3316442598 * get_mCentralAnchorPoint_17() const { return ___mCentralAnchorPoint_17; }
	inline Transform_t3316442598 ** get_address_of_mCentralAnchorPoint_17() { return &___mCentralAnchorPoint_17; }
	inline void set_mCentralAnchorPoint_17(Transform_t3316442598 * value)
	{
		___mCentralAnchorPoint_17 = value;
		Il2CppCodeGenWriteBarrier((&___mCentralAnchorPoint_17), value);
	}

	inline static int32_t get_offset_of_mParentAnchorPoint_18() { return static_cast<int32_t>(offsetof(DigitalEyewearARController_t1877756253, ___mParentAnchorPoint_18)); }
	inline Transform_t3316442598 * get_mParentAnchorPoint_18() const { return ___mParentAnchorPoint_18; }
	inline Transform_t3316442598 ** get_address_of_mParentAnchorPoint_18() { return &___mParentAnchorPoint_18; }
	inline void set_mParentAnchorPoint_18(Transform_t3316442598 * value)
	{
		___mParentAnchorPoint_18 = value;
		Il2CppCodeGenWriteBarrier((&___mParentAnchorPoint_18), value);
	}

	inline static int32_t get_offset_of_mPrimaryCamera_19() { return static_cast<int32_t>(offsetof(DigitalEyewearARController_t1877756253, ___mPrimaryCamera_19)); }
	inline Camera_t3175186167 * get_mPrimaryCamera_19() const { return ___mPrimaryCamera_19; }
	inline Camera_t3175186167 ** get_address_of_mPrimaryCamera_19() { return &___mPrimaryCamera_19; }
	inline void set_mPrimaryCamera_19(Camera_t3175186167 * value)
	{
		___mPrimaryCamera_19 = value;
		Il2CppCodeGenWriteBarrier((&___mPrimaryCamera_19), value);
	}

	inline static int32_t get_offset_of_mPrimaryCameraOriginalRect_20() { return static_cast<int32_t>(offsetof(DigitalEyewearARController_t1877756253, ___mPrimaryCameraOriginalRect_20)); }
	inline Rect_t3436776195  get_mPrimaryCameraOriginalRect_20() const { return ___mPrimaryCameraOriginalRect_20; }
	inline Rect_t3436776195 * get_address_of_mPrimaryCameraOriginalRect_20() { return &___mPrimaryCameraOriginalRect_20; }
	inline void set_mPrimaryCameraOriginalRect_20(Rect_t3436776195  value)
	{
		___mPrimaryCameraOriginalRect_20 = value;
	}

	inline static int32_t get_offset_of_mSecondaryCamera_21() { return static_cast<int32_t>(offsetof(DigitalEyewearARController_t1877756253, ___mSecondaryCamera_21)); }
	inline Camera_t3175186167 * get_mSecondaryCamera_21() const { return ___mSecondaryCamera_21; }
	inline Camera_t3175186167 ** get_address_of_mSecondaryCamera_21() { return &___mSecondaryCamera_21; }
	inline void set_mSecondaryCamera_21(Camera_t3175186167 * value)
	{
		___mSecondaryCamera_21 = value;
		Il2CppCodeGenWriteBarrier((&___mSecondaryCamera_21), value);
	}

	inline static int32_t get_offset_of_mSecondaryCameraOriginalRect_22() { return static_cast<int32_t>(offsetof(DigitalEyewearARController_t1877756253, ___mSecondaryCameraOriginalRect_22)); }
	inline Rect_t3436776195  get_mSecondaryCameraOriginalRect_22() const { return ___mSecondaryCameraOriginalRect_22; }
	inline Rect_t3436776195 * get_address_of_mSecondaryCameraOriginalRect_22() { return &___mSecondaryCameraOriginalRect_22; }
	inline void set_mSecondaryCameraOriginalRect_22(Rect_t3436776195  value)
	{
		___mSecondaryCameraOriginalRect_22 = value;
	}

	inline static int32_t get_offset_of_mSecondaryCameraDisabledLocally_23() { return static_cast<int32_t>(offsetof(DigitalEyewearARController_t1877756253, ___mSecondaryCameraDisabledLocally_23)); }
	inline bool get_mSecondaryCameraDisabledLocally_23() const { return ___mSecondaryCameraDisabledLocally_23; }
	inline bool* get_address_of_mSecondaryCameraDisabledLocally_23() { return &___mSecondaryCameraDisabledLocally_23; }
	inline void set_mSecondaryCameraDisabledLocally_23(bool value)
	{
		___mSecondaryCameraDisabledLocally_23 = value;
	}

	inline static int32_t get_offset_of_mVuforiaBehaviour_24() { return static_cast<int32_t>(offsetof(DigitalEyewearARController_t1877756253, ___mVuforiaBehaviour_24)); }
	inline VuforiaARController_t1328503142 * get_mVuforiaBehaviour_24() const { return ___mVuforiaBehaviour_24; }
	inline VuforiaARController_t1328503142 ** get_address_of_mVuforiaBehaviour_24() { return &___mVuforiaBehaviour_24; }
	inline void set_mVuforiaBehaviour_24(VuforiaARController_t1328503142 * value)
	{
		___mVuforiaBehaviour_24 = value;
		Il2CppCodeGenWriteBarrier((&___mVuforiaBehaviour_24), value);
	}

	inline static int32_t get_offset_of_mDistortionRenderingBhvr_25() { return static_cast<int32_t>(offsetof(DigitalEyewearARController_t1877756253, ___mDistortionRenderingBhvr_25)); }
	inline DistortionRenderingBehaviour_t2785666256 * get_mDistortionRenderingBhvr_25() const { return ___mDistortionRenderingBhvr_25; }
	inline DistortionRenderingBehaviour_t2785666256 ** get_address_of_mDistortionRenderingBhvr_25() { return &___mDistortionRenderingBhvr_25; }
	inline void set_mDistortionRenderingBhvr_25(DistortionRenderingBehaviour_t2785666256 * value)
	{
		___mDistortionRenderingBhvr_25 = value;
		Il2CppCodeGenWriteBarrier((&___mDistortionRenderingBhvr_25), value);
	}

	inline static int32_t get_offset_of_mSetFocusPlaneAutomatically_26() { return static_cast<int32_t>(offsetof(DigitalEyewearARController_t1877756253, ___mSetFocusPlaneAutomatically_26)); }
	inline bool get_mSetFocusPlaneAutomatically_26() const { return ___mSetFocusPlaneAutomatically_26; }
	inline bool* get_address_of_mSetFocusPlaneAutomatically_26() { return &___mSetFocusPlaneAutomatically_26; }
	inline void set_mSetFocusPlaneAutomatically_26(bool value)
	{
		___mSetFocusPlaneAutomatically_26 = value;
	}
};

struct DigitalEyewearARController_t1877756253_StaticFields
{
public:
	// Vuforia.DigitalEyewearARController Vuforia.DigitalEyewearARController::mInstance
	DigitalEyewearARController_t1877756253 * ___mInstance_27;
	// System.Object Vuforia.DigitalEyewearARController::mPadlock
	RuntimeObject * ___mPadlock_28;

public:
	inline static int32_t get_offset_of_mInstance_27() { return static_cast<int32_t>(offsetof(DigitalEyewearARController_t1877756253_StaticFields, ___mInstance_27)); }
	inline DigitalEyewearARController_t1877756253 * get_mInstance_27() const { return ___mInstance_27; }
	inline DigitalEyewearARController_t1877756253 ** get_address_of_mInstance_27() { return &___mInstance_27; }
	inline void set_mInstance_27(DigitalEyewearARController_t1877756253 * value)
	{
		___mInstance_27 = value;
		Il2CppCodeGenWriteBarrier((&___mInstance_27), value);
	}

	inline static int32_t get_offset_of_mPadlock_28() { return static_cast<int32_t>(offsetof(DigitalEyewearARController_t1877756253_StaticFields, ___mPadlock_28)); }
	inline RuntimeObject * get_mPadlock_28() const { return ___mPadlock_28; }
	inline RuntimeObject ** get_address_of_mPadlock_28() { return &___mPadlock_28; }
	inline void set_mPadlock_28(RuntimeObject * value)
	{
		___mPadlock_28 = value;
		Il2CppCodeGenWriteBarrier((&___mPadlock_28), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DIGITALEYEWEARARCONTROLLER_T1877756253_H
#ifndef MESHFILTER_T137687116_H
#define MESHFILTER_T137687116_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MeshFilter
struct  MeshFilter_t137687116  : public Component_t4087199522
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MESHFILTER_T137687116_H
#ifndef TRANSFORM_T3316442598_H
#define TRANSFORM_T3316442598_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Transform
struct  Transform_t3316442598  : public Component_t4087199522
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRANSFORM_T3316442598_H
#ifndef ACTION_T3619184611_H
#define ACTION_T3619184611_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Action
struct  Action_t3619184611  : public MulticastDelegate_t4139169907
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ACTION_T3619184611_H
#ifndef COLLIDER_T2301021182_H
#define COLLIDER_T2301021182_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Collider
struct  Collider_t2301021182  : public Component_t4087199522
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLLIDER_T2301021182_H
#ifndef ACTION_1_T3472505684_H
#define ACTION_1_T3472505684_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Action`1<Vuforia.VuforiaUnity/InitError>
struct  Action_1_t3472505684  : public MulticastDelegate_t4139169907
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ACTION_1_T3472505684_H
#ifndef BEHAVIOUR_T363748010_H
#define BEHAVIOUR_T363748010_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_t363748010  : public Component_t4087199522
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_T363748010_H
#ifndef WINDOWFUNCTION_T3577962812_H
#define WINDOWFUNCTION_T3577962812_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.GUI/WindowFunction
struct  WindowFunction_t3577962812  : public MulticastDelegate_t4139169907
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WINDOWFUNCTION_T3577962812_H
#ifndef ACTION_1_T1560204979_H
#define ACTION_1_T1560204979_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Action`1<Vuforia.Prop>
struct  Action_1_t1560204979  : public MulticastDelegate_t4139169907
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ACTION_1_T1560204979_H
#ifndef ACTION_1_T1020068010_H
#define ACTION_1_T1020068010_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Action`1<Vuforia.Surface>
struct  Action_1_t1020068010  : public MulticastDelegate_t4139169907
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ACTION_1_T1020068010_H
#ifndef VUFORIAABSTRACTCONFIGURATION_T974438950_H
#define VUFORIAABSTRACTCONFIGURATION_T974438950_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.VuforiaAbstractConfiguration
struct  VuforiaAbstractConfiguration_t974438950  : public ScriptableObject_t3635579074
{
public:
	// Vuforia.VuforiaAbstractConfiguration/GenericVuforiaConfiguration Vuforia.VuforiaAbstractConfiguration::vuforia
	GenericVuforiaConfiguration_t1574456586 * ___vuforia_4;
	// Vuforia.VuforiaAbstractConfiguration/DigitalEyewearConfiguration Vuforia.VuforiaAbstractConfiguration::digitalEyewear
	DigitalEyewearConfiguration_t3649657388 * ___digitalEyewear_5;
	// Vuforia.VuforiaAbstractConfiguration/DatabaseLoadConfiguration Vuforia.VuforiaAbstractConfiguration::databaseLoad
	DatabaseLoadConfiguration_t3827205120 * ___databaseLoad_6;
	// Vuforia.VuforiaAbstractConfiguration/VideoBackgroundConfiguration Vuforia.VuforiaAbstractConfiguration::videoBackground
	VideoBackgroundConfiguration_t2579534243 * ___videoBackground_7;
	// Vuforia.VuforiaAbstractConfiguration/SmartTerrainTrackerConfiguration Vuforia.VuforiaAbstractConfiguration::smartTerrainTracker
	SmartTerrainTrackerConfiguration_t657164935 * ___smartTerrainTracker_8;
	// Vuforia.VuforiaAbstractConfiguration/DeviceTrackerConfiguration Vuforia.VuforiaAbstractConfiguration::deviceTracker
	DeviceTrackerConfiguration_t2458088029 * ___deviceTracker_9;
	// Vuforia.VuforiaAbstractConfiguration/WebCamConfiguration Vuforia.VuforiaAbstractConfiguration::webcam
	WebCamConfiguration_t2750821739 * ___webcam_10;

public:
	inline static int32_t get_offset_of_vuforia_4() { return static_cast<int32_t>(offsetof(VuforiaAbstractConfiguration_t974438950, ___vuforia_4)); }
	inline GenericVuforiaConfiguration_t1574456586 * get_vuforia_4() const { return ___vuforia_4; }
	inline GenericVuforiaConfiguration_t1574456586 ** get_address_of_vuforia_4() { return &___vuforia_4; }
	inline void set_vuforia_4(GenericVuforiaConfiguration_t1574456586 * value)
	{
		___vuforia_4 = value;
		Il2CppCodeGenWriteBarrier((&___vuforia_4), value);
	}

	inline static int32_t get_offset_of_digitalEyewear_5() { return static_cast<int32_t>(offsetof(VuforiaAbstractConfiguration_t974438950, ___digitalEyewear_5)); }
	inline DigitalEyewearConfiguration_t3649657388 * get_digitalEyewear_5() const { return ___digitalEyewear_5; }
	inline DigitalEyewearConfiguration_t3649657388 ** get_address_of_digitalEyewear_5() { return &___digitalEyewear_5; }
	inline void set_digitalEyewear_5(DigitalEyewearConfiguration_t3649657388 * value)
	{
		___digitalEyewear_5 = value;
		Il2CppCodeGenWriteBarrier((&___digitalEyewear_5), value);
	}

	inline static int32_t get_offset_of_databaseLoad_6() { return static_cast<int32_t>(offsetof(VuforiaAbstractConfiguration_t974438950, ___databaseLoad_6)); }
	inline DatabaseLoadConfiguration_t3827205120 * get_databaseLoad_6() const { return ___databaseLoad_6; }
	inline DatabaseLoadConfiguration_t3827205120 ** get_address_of_databaseLoad_6() { return &___databaseLoad_6; }
	inline void set_databaseLoad_6(DatabaseLoadConfiguration_t3827205120 * value)
	{
		___databaseLoad_6 = value;
		Il2CppCodeGenWriteBarrier((&___databaseLoad_6), value);
	}

	inline static int32_t get_offset_of_videoBackground_7() { return static_cast<int32_t>(offsetof(VuforiaAbstractConfiguration_t974438950, ___videoBackground_7)); }
	inline VideoBackgroundConfiguration_t2579534243 * get_videoBackground_7() const { return ___videoBackground_7; }
	inline VideoBackgroundConfiguration_t2579534243 ** get_address_of_videoBackground_7() { return &___videoBackground_7; }
	inline void set_videoBackground_7(VideoBackgroundConfiguration_t2579534243 * value)
	{
		___videoBackground_7 = value;
		Il2CppCodeGenWriteBarrier((&___videoBackground_7), value);
	}

	inline static int32_t get_offset_of_smartTerrainTracker_8() { return static_cast<int32_t>(offsetof(VuforiaAbstractConfiguration_t974438950, ___smartTerrainTracker_8)); }
	inline SmartTerrainTrackerConfiguration_t657164935 * get_smartTerrainTracker_8() const { return ___smartTerrainTracker_8; }
	inline SmartTerrainTrackerConfiguration_t657164935 ** get_address_of_smartTerrainTracker_8() { return &___smartTerrainTracker_8; }
	inline void set_smartTerrainTracker_8(SmartTerrainTrackerConfiguration_t657164935 * value)
	{
		___smartTerrainTracker_8 = value;
		Il2CppCodeGenWriteBarrier((&___smartTerrainTracker_8), value);
	}

	inline static int32_t get_offset_of_deviceTracker_9() { return static_cast<int32_t>(offsetof(VuforiaAbstractConfiguration_t974438950, ___deviceTracker_9)); }
	inline DeviceTrackerConfiguration_t2458088029 * get_deviceTracker_9() const { return ___deviceTracker_9; }
	inline DeviceTrackerConfiguration_t2458088029 ** get_address_of_deviceTracker_9() { return &___deviceTracker_9; }
	inline void set_deviceTracker_9(DeviceTrackerConfiguration_t2458088029 * value)
	{
		___deviceTracker_9 = value;
		Il2CppCodeGenWriteBarrier((&___deviceTracker_9), value);
	}

	inline static int32_t get_offset_of_webcam_10() { return static_cast<int32_t>(offsetof(VuforiaAbstractConfiguration_t974438950, ___webcam_10)); }
	inline WebCamConfiguration_t2750821739 * get_webcam_10() const { return ___webcam_10; }
	inline WebCamConfiguration_t2750821739 ** get_address_of_webcam_10() { return &___webcam_10; }
	inline void set_webcam_10(WebCamConfiguration_t2750821739 * value)
	{
		___webcam_10 = value;
		Il2CppCodeGenWriteBarrier((&___webcam_10), value);
	}
};

struct VuforiaAbstractConfiguration_t974438950_StaticFields
{
public:
	// Vuforia.VuforiaAbstractConfiguration Vuforia.VuforiaAbstractConfiguration::mInstance
	VuforiaAbstractConfiguration_t974438950 * ___mInstance_2;
	// System.Object Vuforia.VuforiaAbstractConfiguration::mPadlock
	RuntimeObject * ___mPadlock_3;

public:
	inline static int32_t get_offset_of_mInstance_2() { return static_cast<int32_t>(offsetof(VuforiaAbstractConfiguration_t974438950_StaticFields, ___mInstance_2)); }
	inline VuforiaAbstractConfiguration_t974438950 * get_mInstance_2() const { return ___mInstance_2; }
	inline VuforiaAbstractConfiguration_t974438950 ** get_address_of_mInstance_2() { return &___mInstance_2; }
	inline void set_mInstance_2(VuforiaAbstractConfiguration_t974438950 * value)
	{
		___mInstance_2 = value;
		Il2CppCodeGenWriteBarrier((&___mInstance_2), value);
	}

	inline static int32_t get_offset_of_mPadlock_3() { return static_cast<int32_t>(offsetof(VuforiaAbstractConfiguration_t974438950_StaticFields, ___mPadlock_3)); }
	inline RuntimeObject * get_mPadlock_3() const { return ___mPadlock_3; }
	inline RuntimeObject ** get_address_of_mPadlock_3() { return &___mPadlock_3; }
	inline void set_mPadlock_3(RuntimeObject * value)
	{
		___mPadlock_3 = value;
		Il2CppCodeGenWriteBarrier((&___mPadlock_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VUFORIAABSTRACTCONFIGURATION_T974438950_H
#ifndef RIGIDBODY_T2492269564_H
#define RIGIDBODY_T2492269564_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rigidbody
struct  Rigidbody_t2492269564  : public Component_t4087199522
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RIGIDBODY_T2492269564_H
#ifndef RENDERER_T1303575294_H
#define RENDERER_T1303575294_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Renderer
struct  Renderer_t1303575294  : public Component_t4087199522
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RENDERER_T1303575294_H
#ifndef VUFORIACONFIGURATION_T2489601127_H
#define VUFORIACONFIGURATION_T2489601127_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.VuforiaConfiguration
struct  VuforiaConfiguration_t2489601127  : public VuforiaAbstractConfiguration_t974438950
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VUFORIACONFIGURATION_T2489601127_H
#ifndef MONOBEHAVIOUR_T345688271_H
#define MONOBEHAVIOUR_T345688271_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t345688271  : public Behaviour_t363748010
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T345688271_H
#ifndef CAMERA_T3175186167_H
#define CAMERA_T3175186167_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Camera
struct  Camera_t3175186167  : public Behaviour_t363748010
{
public:

public:
};

struct Camera_t3175186167_StaticFields
{
public:
	// UnityEngine.Camera/CameraCallback UnityEngine.Camera::onPreCull
	CameraCallback_t832885096 * ___onPreCull_2;
	// UnityEngine.Camera/CameraCallback UnityEngine.Camera::onPreRender
	CameraCallback_t832885096 * ___onPreRender_3;
	// UnityEngine.Camera/CameraCallback UnityEngine.Camera::onPostRender
	CameraCallback_t832885096 * ___onPostRender_4;

public:
	inline static int32_t get_offset_of_onPreCull_2() { return static_cast<int32_t>(offsetof(Camera_t3175186167_StaticFields, ___onPreCull_2)); }
	inline CameraCallback_t832885096 * get_onPreCull_2() const { return ___onPreCull_2; }
	inline CameraCallback_t832885096 ** get_address_of_onPreCull_2() { return &___onPreCull_2; }
	inline void set_onPreCull_2(CameraCallback_t832885096 * value)
	{
		___onPreCull_2 = value;
		Il2CppCodeGenWriteBarrier((&___onPreCull_2), value);
	}

	inline static int32_t get_offset_of_onPreRender_3() { return static_cast<int32_t>(offsetof(Camera_t3175186167_StaticFields, ___onPreRender_3)); }
	inline CameraCallback_t832885096 * get_onPreRender_3() const { return ___onPreRender_3; }
	inline CameraCallback_t832885096 ** get_address_of_onPreRender_3() { return &___onPreRender_3; }
	inline void set_onPreRender_3(CameraCallback_t832885096 * value)
	{
		___onPreRender_3 = value;
		Il2CppCodeGenWriteBarrier((&___onPreRender_3), value);
	}

	inline static int32_t get_offset_of_onPostRender_4() { return static_cast<int32_t>(offsetof(Camera_t3175186167_StaticFields, ___onPostRender_4)); }
	inline CameraCallback_t832885096 * get_onPostRender_4() const { return ___onPostRender_4; }
	inline CameraCallback_t832885096 ** get_address_of_onPostRender_4() { return &___onPostRender_4; }
	inline void set_onPostRender_4(CameraCallback_t832885096 * value)
	{
		___onPostRender_4 = value;
		Il2CppCodeGenWriteBarrier((&___onPostRender_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAMERA_T3175186167_H
#ifndef MESHRENDERER_T1441648352_H
#define MESHRENDERER_T1441648352_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MeshRenderer
struct  MeshRenderer_t1441648352  : public Renderer_t1303575294
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MESHRENDERER_T1441648352_H
#ifndef ANIMATION_T2605054404_H
#define ANIMATION_T2605054404_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Animation
struct  Animation_t2605054404  : public Behaviour_t363748010
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANIMATION_T2605054404_H
#ifndef WIREFRAMEBEHAVIOUR_T1899115476_H
#define WIREFRAMEBEHAVIOUR_T1899115476_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.WireframeBehaviour
struct  WireframeBehaviour_t1899115476  : public MonoBehaviour_t345688271
{
public:
	// UnityEngine.Material Vuforia.WireframeBehaviour::lineMaterial
	Material_t1079520667 * ___lineMaterial_2;
	// System.Boolean Vuforia.WireframeBehaviour::ShowLines
	bool ___ShowLines_3;
	// UnityEngine.Color Vuforia.WireframeBehaviour::LineColor
	Color_t2507026410  ___LineColor_4;
	// UnityEngine.Material Vuforia.WireframeBehaviour::mLineMaterial
	Material_t1079520667 * ___mLineMaterial_5;

public:
	inline static int32_t get_offset_of_lineMaterial_2() { return static_cast<int32_t>(offsetof(WireframeBehaviour_t1899115476, ___lineMaterial_2)); }
	inline Material_t1079520667 * get_lineMaterial_2() const { return ___lineMaterial_2; }
	inline Material_t1079520667 ** get_address_of_lineMaterial_2() { return &___lineMaterial_2; }
	inline void set_lineMaterial_2(Material_t1079520667 * value)
	{
		___lineMaterial_2 = value;
		Il2CppCodeGenWriteBarrier((&___lineMaterial_2), value);
	}

	inline static int32_t get_offset_of_ShowLines_3() { return static_cast<int32_t>(offsetof(WireframeBehaviour_t1899115476, ___ShowLines_3)); }
	inline bool get_ShowLines_3() const { return ___ShowLines_3; }
	inline bool* get_address_of_ShowLines_3() { return &___ShowLines_3; }
	inline void set_ShowLines_3(bool value)
	{
		___ShowLines_3 = value;
	}

	inline static int32_t get_offset_of_LineColor_4() { return static_cast<int32_t>(offsetof(WireframeBehaviour_t1899115476, ___LineColor_4)); }
	inline Color_t2507026410  get_LineColor_4() const { return ___LineColor_4; }
	inline Color_t2507026410 * get_address_of_LineColor_4() { return &___LineColor_4; }
	inline void set_LineColor_4(Color_t2507026410  value)
	{
		___LineColor_4 = value;
	}

	inline static int32_t get_offset_of_mLineMaterial_5() { return static_cast<int32_t>(offsetof(WireframeBehaviour_t1899115476, ___mLineMaterial_5)); }
	inline Material_t1079520667 * get_mLineMaterial_5() const { return ___mLineMaterial_5; }
	inline Material_t1079520667 ** get_address_of_mLineMaterial_5() { return &___mLineMaterial_5; }
	inline void set_mLineMaterial_5(Material_t1079520667 * value)
	{
		___mLineMaterial_5 = value;
		Il2CppCodeGenWriteBarrier((&___mLineMaterial_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WIREFRAMEBEHAVIOUR_T1899115476_H
#ifndef WIREFRAMETRACKABLEEVENTHANDLER_T680679714_H
#define WIREFRAMETRACKABLEEVENTHANDLER_T680679714_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.WireframeTrackableEventHandler
struct  WireframeTrackableEventHandler_t680679714  : public MonoBehaviour_t345688271
{
public:
	// Vuforia.TrackableBehaviour Vuforia.WireframeTrackableEventHandler::mTrackableBehaviour
	TrackableBehaviour_t2419079356 * ___mTrackableBehaviour_2;

public:
	inline static int32_t get_offset_of_mTrackableBehaviour_2() { return static_cast<int32_t>(offsetof(WireframeTrackableEventHandler_t680679714, ___mTrackableBehaviour_2)); }
	inline TrackableBehaviour_t2419079356 * get_mTrackableBehaviour_2() const { return ___mTrackableBehaviour_2; }
	inline TrackableBehaviour_t2419079356 ** get_address_of_mTrackableBehaviour_2() { return &___mTrackableBehaviour_2; }
	inline void set_mTrackableBehaviour_2(TrackableBehaviour_t2419079356 * value)
	{
		___mTrackableBehaviour_2 = value;
		Il2CppCodeGenWriteBarrier((&___mTrackableBehaviour_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WIREFRAMETRACKABLEEVENTHANDLER_T680679714_H
#ifndef SPIDERCONTROLLER_T373354123_H
#define SPIDERCONTROLLER_T373354123_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SpiderController
struct  SpiderController_t373354123  : public MonoBehaviour_t345688271
{
public:
	// UnityEngine.Rigidbody SpiderController::rb
	Rigidbody_t2492269564 * ___rb_2;
	// UnityEngine.Animation SpiderController::anim
	Animation_t2605054404 * ___anim_3;

public:
	inline static int32_t get_offset_of_rb_2() { return static_cast<int32_t>(offsetof(SpiderController_t373354123, ___rb_2)); }
	inline Rigidbody_t2492269564 * get_rb_2() const { return ___rb_2; }
	inline Rigidbody_t2492269564 ** get_address_of_rb_2() { return &___rb_2; }
	inline void set_rb_2(Rigidbody_t2492269564 * value)
	{
		___rb_2 = value;
		Il2CppCodeGenWriteBarrier((&___rb_2), value);
	}

	inline static int32_t get_offset_of_anim_3() { return static_cast<int32_t>(offsetof(SpiderController_t373354123, ___anim_3)); }
	inline Animation_t2605054404 * get_anim_3() const { return ___anim_3; }
	inline Animation_t2605054404 ** get_address_of_anim_3() { return &___anim_3; }
	inline void set_anim_3(Animation_t2605054404 * value)
	{
		___anim_3 = value;
		Il2CppCodeGenWriteBarrier((&___anim_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPIDERCONTROLLER_T373354123_H
#ifndef VUFORIAABSTRACTBEHAVIOUR_T153010168_H
#define VUFORIAABSTRACTBEHAVIOUR_T153010168_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.VuforiaAbstractBehaviour
struct  VuforiaAbstractBehaviour_t153010168  : public MonoBehaviour_t345688271
{
public:
	// Vuforia.VuforiaARController/WorldCenterMode Vuforia.VuforiaAbstractBehaviour::mWorldCenterMode
	int32_t ___mWorldCenterMode_2;
	// Vuforia.TrackableBehaviour Vuforia.VuforiaAbstractBehaviour::mWorldCenter
	TrackableBehaviour_t2419079356 * ___mWorldCenter_3;
	// UnityEngine.Transform Vuforia.VuforiaAbstractBehaviour::mCentralAnchorPoint
	Transform_t3316442598 * ___mCentralAnchorPoint_4;
	// UnityEngine.Transform Vuforia.VuforiaAbstractBehaviour::mParentAnchorPoint
	Transform_t3316442598 * ___mParentAnchorPoint_5;
	// UnityEngine.Camera Vuforia.VuforiaAbstractBehaviour::mPrimaryCamera
	Camera_t3175186167 * ___mPrimaryCamera_6;
	// UnityEngine.Camera Vuforia.VuforiaAbstractBehaviour::mSecondaryCamera
	Camera_t3175186167 * ___mSecondaryCamera_7;
	// System.Boolean Vuforia.VuforiaAbstractBehaviour::mWereBindingFieldsExposed
	bool ___mWereBindingFieldsExposed_8;
	// System.Action Vuforia.VuforiaAbstractBehaviour::AwakeEvent
	Action_t3619184611 * ___AwakeEvent_11;
	// System.Action Vuforia.VuforiaAbstractBehaviour::OnEnableEvent
	Action_t3619184611 * ___OnEnableEvent_12;
	// System.Action Vuforia.VuforiaAbstractBehaviour::StartEvent
	Action_t3619184611 * ___StartEvent_13;
	// System.Action Vuforia.VuforiaAbstractBehaviour::UpdateEvent
	Action_t3619184611 * ___UpdateEvent_14;
	// System.Action Vuforia.VuforiaAbstractBehaviour::OnLevelWasLoadedEvent
	Action_t3619184611 * ___OnLevelWasLoadedEvent_15;
	// System.Action`1<System.Boolean> Vuforia.VuforiaAbstractBehaviour::OnApplicationPauseEvent
	Action_1_t2936672411 * ___OnApplicationPauseEvent_16;
	// System.Action Vuforia.VuforiaAbstractBehaviour::OnDisableEvent
	Action_t3619184611 * ___OnDisableEvent_17;
	// System.Action Vuforia.VuforiaAbstractBehaviour::OnDestroyEvent
	Action_t3619184611 * ___OnDestroyEvent_18;

public:
	inline static int32_t get_offset_of_mWorldCenterMode_2() { return static_cast<int32_t>(offsetof(VuforiaAbstractBehaviour_t153010168, ___mWorldCenterMode_2)); }
	inline int32_t get_mWorldCenterMode_2() const { return ___mWorldCenterMode_2; }
	inline int32_t* get_address_of_mWorldCenterMode_2() { return &___mWorldCenterMode_2; }
	inline void set_mWorldCenterMode_2(int32_t value)
	{
		___mWorldCenterMode_2 = value;
	}

	inline static int32_t get_offset_of_mWorldCenter_3() { return static_cast<int32_t>(offsetof(VuforiaAbstractBehaviour_t153010168, ___mWorldCenter_3)); }
	inline TrackableBehaviour_t2419079356 * get_mWorldCenter_3() const { return ___mWorldCenter_3; }
	inline TrackableBehaviour_t2419079356 ** get_address_of_mWorldCenter_3() { return &___mWorldCenter_3; }
	inline void set_mWorldCenter_3(TrackableBehaviour_t2419079356 * value)
	{
		___mWorldCenter_3 = value;
		Il2CppCodeGenWriteBarrier((&___mWorldCenter_3), value);
	}

	inline static int32_t get_offset_of_mCentralAnchorPoint_4() { return static_cast<int32_t>(offsetof(VuforiaAbstractBehaviour_t153010168, ___mCentralAnchorPoint_4)); }
	inline Transform_t3316442598 * get_mCentralAnchorPoint_4() const { return ___mCentralAnchorPoint_4; }
	inline Transform_t3316442598 ** get_address_of_mCentralAnchorPoint_4() { return &___mCentralAnchorPoint_4; }
	inline void set_mCentralAnchorPoint_4(Transform_t3316442598 * value)
	{
		___mCentralAnchorPoint_4 = value;
		Il2CppCodeGenWriteBarrier((&___mCentralAnchorPoint_4), value);
	}

	inline static int32_t get_offset_of_mParentAnchorPoint_5() { return static_cast<int32_t>(offsetof(VuforiaAbstractBehaviour_t153010168, ___mParentAnchorPoint_5)); }
	inline Transform_t3316442598 * get_mParentAnchorPoint_5() const { return ___mParentAnchorPoint_5; }
	inline Transform_t3316442598 ** get_address_of_mParentAnchorPoint_5() { return &___mParentAnchorPoint_5; }
	inline void set_mParentAnchorPoint_5(Transform_t3316442598 * value)
	{
		___mParentAnchorPoint_5 = value;
		Il2CppCodeGenWriteBarrier((&___mParentAnchorPoint_5), value);
	}

	inline static int32_t get_offset_of_mPrimaryCamera_6() { return static_cast<int32_t>(offsetof(VuforiaAbstractBehaviour_t153010168, ___mPrimaryCamera_6)); }
	inline Camera_t3175186167 * get_mPrimaryCamera_6() const { return ___mPrimaryCamera_6; }
	inline Camera_t3175186167 ** get_address_of_mPrimaryCamera_6() { return &___mPrimaryCamera_6; }
	inline void set_mPrimaryCamera_6(Camera_t3175186167 * value)
	{
		___mPrimaryCamera_6 = value;
		Il2CppCodeGenWriteBarrier((&___mPrimaryCamera_6), value);
	}

	inline static int32_t get_offset_of_mSecondaryCamera_7() { return static_cast<int32_t>(offsetof(VuforiaAbstractBehaviour_t153010168, ___mSecondaryCamera_7)); }
	inline Camera_t3175186167 * get_mSecondaryCamera_7() const { return ___mSecondaryCamera_7; }
	inline Camera_t3175186167 ** get_address_of_mSecondaryCamera_7() { return &___mSecondaryCamera_7; }
	inline void set_mSecondaryCamera_7(Camera_t3175186167 * value)
	{
		___mSecondaryCamera_7 = value;
		Il2CppCodeGenWriteBarrier((&___mSecondaryCamera_7), value);
	}

	inline static int32_t get_offset_of_mWereBindingFieldsExposed_8() { return static_cast<int32_t>(offsetof(VuforiaAbstractBehaviour_t153010168, ___mWereBindingFieldsExposed_8)); }
	inline bool get_mWereBindingFieldsExposed_8() const { return ___mWereBindingFieldsExposed_8; }
	inline bool* get_address_of_mWereBindingFieldsExposed_8() { return &___mWereBindingFieldsExposed_8; }
	inline void set_mWereBindingFieldsExposed_8(bool value)
	{
		___mWereBindingFieldsExposed_8 = value;
	}

	inline static int32_t get_offset_of_AwakeEvent_11() { return static_cast<int32_t>(offsetof(VuforiaAbstractBehaviour_t153010168, ___AwakeEvent_11)); }
	inline Action_t3619184611 * get_AwakeEvent_11() const { return ___AwakeEvent_11; }
	inline Action_t3619184611 ** get_address_of_AwakeEvent_11() { return &___AwakeEvent_11; }
	inline void set_AwakeEvent_11(Action_t3619184611 * value)
	{
		___AwakeEvent_11 = value;
		Il2CppCodeGenWriteBarrier((&___AwakeEvent_11), value);
	}

	inline static int32_t get_offset_of_OnEnableEvent_12() { return static_cast<int32_t>(offsetof(VuforiaAbstractBehaviour_t153010168, ___OnEnableEvent_12)); }
	inline Action_t3619184611 * get_OnEnableEvent_12() const { return ___OnEnableEvent_12; }
	inline Action_t3619184611 ** get_address_of_OnEnableEvent_12() { return &___OnEnableEvent_12; }
	inline void set_OnEnableEvent_12(Action_t3619184611 * value)
	{
		___OnEnableEvent_12 = value;
		Il2CppCodeGenWriteBarrier((&___OnEnableEvent_12), value);
	}

	inline static int32_t get_offset_of_StartEvent_13() { return static_cast<int32_t>(offsetof(VuforiaAbstractBehaviour_t153010168, ___StartEvent_13)); }
	inline Action_t3619184611 * get_StartEvent_13() const { return ___StartEvent_13; }
	inline Action_t3619184611 ** get_address_of_StartEvent_13() { return &___StartEvent_13; }
	inline void set_StartEvent_13(Action_t3619184611 * value)
	{
		___StartEvent_13 = value;
		Il2CppCodeGenWriteBarrier((&___StartEvent_13), value);
	}

	inline static int32_t get_offset_of_UpdateEvent_14() { return static_cast<int32_t>(offsetof(VuforiaAbstractBehaviour_t153010168, ___UpdateEvent_14)); }
	inline Action_t3619184611 * get_UpdateEvent_14() const { return ___UpdateEvent_14; }
	inline Action_t3619184611 ** get_address_of_UpdateEvent_14() { return &___UpdateEvent_14; }
	inline void set_UpdateEvent_14(Action_t3619184611 * value)
	{
		___UpdateEvent_14 = value;
		Il2CppCodeGenWriteBarrier((&___UpdateEvent_14), value);
	}

	inline static int32_t get_offset_of_OnLevelWasLoadedEvent_15() { return static_cast<int32_t>(offsetof(VuforiaAbstractBehaviour_t153010168, ___OnLevelWasLoadedEvent_15)); }
	inline Action_t3619184611 * get_OnLevelWasLoadedEvent_15() const { return ___OnLevelWasLoadedEvent_15; }
	inline Action_t3619184611 ** get_address_of_OnLevelWasLoadedEvent_15() { return &___OnLevelWasLoadedEvent_15; }
	inline void set_OnLevelWasLoadedEvent_15(Action_t3619184611 * value)
	{
		___OnLevelWasLoadedEvent_15 = value;
		Il2CppCodeGenWriteBarrier((&___OnLevelWasLoadedEvent_15), value);
	}

	inline static int32_t get_offset_of_OnApplicationPauseEvent_16() { return static_cast<int32_t>(offsetof(VuforiaAbstractBehaviour_t153010168, ___OnApplicationPauseEvent_16)); }
	inline Action_1_t2936672411 * get_OnApplicationPauseEvent_16() const { return ___OnApplicationPauseEvent_16; }
	inline Action_1_t2936672411 ** get_address_of_OnApplicationPauseEvent_16() { return &___OnApplicationPauseEvent_16; }
	inline void set_OnApplicationPauseEvent_16(Action_1_t2936672411 * value)
	{
		___OnApplicationPauseEvent_16 = value;
		Il2CppCodeGenWriteBarrier((&___OnApplicationPauseEvent_16), value);
	}

	inline static int32_t get_offset_of_OnDisableEvent_17() { return static_cast<int32_t>(offsetof(VuforiaAbstractBehaviour_t153010168, ___OnDisableEvent_17)); }
	inline Action_t3619184611 * get_OnDisableEvent_17() const { return ___OnDisableEvent_17; }
	inline Action_t3619184611 ** get_address_of_OnDisableEvent_17() { return &___OnDisableEvent_17; }
	inline void set_OnDisableEvent_17(Action_t3619184611 * value)
	{
		___OnDisableEvent_17 = value;
		Il2CppCodeGenWriteBarrier((&___OnDisableEvent_17), value);
	}

	inline static int32_t get_offset_of_OnDestroyEvent_18() { return static_cast<int32_t>(offsetof(VuforiaAbstractBehaviour_t153010168, ___OnDestroyEvent_18)); }
	inline Action_t3619184611 * get_OnDestroyEvent_18() const { return ___OnDestroyEvent_18; }
	inline Action_t3619184611 ** get_address_of_OnDestroyEvent_18() { return &___OnDestroyEvent_18; }
	inline void set_OnDestroyEvent_18(Action_t3619184611 * value)
	{
		___OnDestroyEvent_18 = value;
		Il2CppCodeGenWriteBarrier((&___OnDestroyEvent_18), value);
	}
};

struct VuforiaAbstractBehaviour_t153010168_StaticFields
{
public:
	// System.Action`1<Vuforia.VuforiaAbstractBehaviour> Vuforia.VuforiaAbstractBehaviour::BehaviourCreated
	Action_1_t138837510 * ___BehaviourCreated_9;
	// System.Action`1<Vuforia.VuforiaAbstractBehaviour> Vuforia.VuforiaAbstractBehaviour::BehaviourDestroyed
	Action_1_t138837510 * ___BehaviourDestroyed_10;

public:
	inline static int32_t get_offset_of_BehaviourCreated_9() { return static_cast<int32_t>(offsetof(VuforiaAbstractBehaviour_t153010168_StaticFields, ___BehaviourCreated_9)); }
	inline Action_1_t138837510 * get_BehaviourCreated_9() const { return ___BehaviourCreated_9; }
	inline Action_1_t138837510 ** get_address_of_BehaviourCreated_9() { return &___BehaviourCreated_9; }
	inline void set_BehaviourCreated_9(Action_1_t138837510 * value)
	{
		___BehaviourCreated_9 = value;
		Il2CppCodeGenWriteBarrier((&___BehaviourCreated_9), value);
	}

	inline static int32_t get_offset_of_BehaviourDestroyed_10() { return static_cast<int32_t>(offsetof(VuforiaAbstractBehaviour_t153010168_StaticFields, ___BehaviourDestroyed_10)); }
	inline Action_1_t138837510 * get_BehaviourDestroyed_10() const { return ___BehaviourDestroyed_10; }
	inline Action_1_t138837510 ** get_address_of_BehaviourDestroyed_10() { return &___BehaviourDestroyed_10; }
	inline void set_BehaviourDestroyed_10(Action_1_t138837510 * value)
	{
		___BehaviourDestroyed_10 = value;
		Il2CppCodeGenWriteBarrier((&___BehaviourDestroyed_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VUFORIAABSTRACTBEHAVIOUR_T153010168_H
#ifndef TEXTRECOABSTRACTBEHAVIOUR_T1751158196_H
#define TEXTRECOABSTRACTBEHAVIOUR_T1751158196_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.TextRecoAbstractBehaviour
struct  TextRecoAbstractBehaviour_t1751158196  : public MonoBehaviour_t345688271
{
public:
	// System.Boolean Vuforia.TextRecoAbstractBehaviour::mHasInitialized
	bool ___mHasInitialized_2;
	// System.Boolean Vuforia.TextRecoAbstractBehaviour::mTrackerWasActiveBeforePause
	bool ___mTrackerWasActiveBeforePause_3;
	// System.Boolean Vuforia.TextRecoAbstractBehaviour::mTrackerWasActiveBeforeDisabling
	bool ___mTrackerWasActiveBeforeDisabling_4;
	// System.String Vuforia.TextRecoAbstractBehaviour::mWordListFile
	String_t* ___mWordListFile_5;
	// System.String Vuforia.TextRecoAbstractBehaviour::mCustomWordListFile
	String_t* ___mCustomWordListFile_6;
	// System.String Vuforia.TextRecoAbstractBehaviour::mAdditionalCustomWords
	String_t* ___mAdditionalCustomWords_7;
	// Vuforia.WordFilterMode Vuforia.TextRecoAbstractBehaviour::mFilterMode
	int32_t ___mFilterMode_8;
	// System.String Vuforia.TextRecoAbstractBehaviour::mFilterListFile
	String_t* ___mFilterListFile_9;
	// System.String Vuforia.TextRecoAbstractBehaviour::mAdditionalFilterWords
	String_t* ___mAdditionalFilterWords_10;
	// Vuforia.WordPrefabCreationMode Vuforia.TextRecoAbstractBehaviour::mWordPrefabCreationMode
	int32_t ___mWordPrefabCreationMode_11;
	// System.Int32 Vuforia.TextRecoAbstractBehaviour::mMaximumWordInstances
	int32_t ___mMaximumWordInstances_12;
	// System.Collections.Generic.List`1<Vuforia.ITextRecoEventHandler> Vuforia.TextRecoAbstractBehaviour::mTextRecoEventHandlers
	List_1_t4098171074 * ___mTextRecoEventHandlers_13;

public:
	inline static int32_t get_offset_of_mHasInitialized_2() { return static_cast<int32_t>(offsetof(TextRecoAbstractBehaviour_t1751158196, ___mHasInitialized_2)); }
	inline bool get_mHasInitialized_2() const { return ___mHasInitialized_2; }
	inline bool* get_address_of_mHasInitialized_2() { return &___mHasInitialized_2; }
	inline void set_mHasInitialized_2(bool value)
	{
		___mHasInitialized_2 = value;
	}

	inline static int32_t get_offset_of_mTrackerWasActiveBeforePause_3() { return static_cast<int32_t>(offsetof(TextRecoAbstractBehaviour_t1751158196, ___mTrackerWasActiveBeforePause_3)); }
	inline bool get_mTrackerWasActiveBeforePause_3() const { return ___mTrackerWasActiveBeforePause_3; }
	inline bool* get_address_of_mTrackerWasActiveBeforePause_3() { return &___mTrackerWasActiveBeforePause_3; }
	inline void set_mTrackerWasActiveBeforePause_3(bool value)
	{
		___mTrackerWasActiveBeforePause_3 = value;
	}

	inline static int32_t get_offset_of_mTrackerWasActiveBeforeDisabling_4() { return static_cast<int32_t>(offsetof(TextRecoAbstractBehaviour_t1751158196, ___mTrackerWasActiveBeforeDisabling_4)); }
	inline bool get_mTrackerWasActiveBeforeDisabling_4() const { return ___mTrackerWasActiveBeforeDisabling_4; }
	inline bool* get_address_of_mTrackerWasActiveBeforeDisabling_4() { return &___mTrackerWasActiveBeforeDisabling_4; }
	inline void set_mTrackerWasActiveBeforeDisabling_4(bool value)
	{
		___mTrackerWasActiveBeforeDisabling_4 = value;
	}

	inline static int32_t get_offset_of_mWordListFile_5() { return static_cast<int32_t>(offsetof(TextRecoAbstractBehaviour_t1751158196, ___mWordListFile_5)); }
	inline String_t* get_mWordListFile_5() const { return ___mWordListFile_5; }
	inline String_t** get_address_of_mWordListFile_5() { return &___mWordListFile_5; }
	inline void set_mWordListFile_5(String_t* value)
	{
		___mWordListFile_5 = value;
		Il2CppCodeGenWriteBarrier((&___mWordListFile_5), value);
	}

	inline static int32_t get_offset_of_mCustomWordListFile_6() { return static_cast<int32_t>(offsetof(TextRecoAbstractBehaviour_t1751158196, ___mCustomWordListFile_6)); }
	inline String_t* get_mCustomWordListFile_6() const { return ___mCustomWordListFile_6; }
	inline String_t** get_address_of_mCustomWordListFile_6() { return &___mCustomWordListFile_6; }
	inline void set_mCustomWordListFile_6(String_t* value)
	{
		___mCustomWordListFile_6 = value;
		Il2CppCodeGenWriteBarrier((&___mCustomWordListFile_6), value);
	}

	inline static int32_t get_offset_of_mAdditionalCustomWords_7() { return static_cast<int32_t>(offsetof(TextRecoAbstractBehaviour_t1751158196, ___mAdditionalCustomWords_7)); }
	inline String_t* get_mAdditionalCustomWords_7() const { return ___mAdditionalCustomWords_7; }
	inline String_t** get_address_of_mAdditionalCustomWords_7() { return &___mAdditionalCustomWords_7; }
	inline void set_mAdditionalCustomWords_7(String_t* value)
	{
		___mAdditionalCustomWords_7 = value;
		Il2CppCodeGenWriteBarrier((&___mAdditionalCustomWords_7), value);
	}

	inline static int32_t get_offset_of_mFilterMode_8() { return static_cast<int32_t>(offsetof(TextRecoAbstractBehaviour_t1751158196, ___mFilterMode_8)); }
	inline int32_t get_mFilterMode_8() const { return ___mFilterMode_8; }
	inline int32_t* get_address_of_mFilterMode_8() { return &___mFilterMode_8; }
	inline void set_mFilterMode_8(int32_t value)
	{
		___mFilterMode_8 = value;
	}

	inline static int32_t get_offset_of_mFilterListFile_9() { return static_cast<int32_t>(offsetof(TextRecoAbstractBehaviour_t1751158196, ___mFilterListFile_9)); }
	inline String_t* get_mFilterListFile_9() const { return ___mFilterListFile_9; }
	inline String_t** get_address_of_mFilterListFile_9() { return &___mFilterListFile_9; }
	inline void set_mFilterListFile_9(String_t* value)
	{
		___mFilterListFile_9 = value;
		Il2CppCodeGenWriteBarrier((&___mFilterListFile_9), value);
	}

	inline static int32_t get_offset_of_mAdditionalFilterWords_10() { return static_cast<int32_t>(offsetof(TextRecoAbstractBehaviour_t1751158196, ___mAdditionalFilterWords_10)); }
	inline String_t* get_mAdditionalFilterWords_10() const { return ___mAdditionalFilterWords_10; }
	inline String_t** get_address_of_mAdditionalFilterWords_10() { return &___mAdditionalFilterWords_10; }
	inline void set_mAdditionalFilterWords_10(String_t* value)
	{
		___mAdditionalFilterWords_10 = value;
		Il2CppCodeGenWriteBarrier((&___mAdditionalFilterWords_10), value);
	}

	inline static int32_t get_offset_of_mWordPrefabCreationMode_11() { return static_cast<int32_t>(offsetof(TextRecoAbstractBehaviour_t1751158196, ___mWordPrefabCreationMode_11)); }
	inline int32_t get_mWordPrefabCreationMode_11() const { return ___mWordPrefabCreationMode_11; }
	inline int32_t* get_address_of_mWordPrefabCreationMode_11() { return &___mWordPrefabCreationMode_11; }
	inline void set_mWordPrefabCreationMode_11(int32_t value)
	{
		___mWordPrefabCreationMode_11 = value;
	}

	inline static int32_t get_offset_of_mMaximumWordInstances_12() { return static_cast<int32_t>(offsetof(TextRecoAbstractBehaviour_t1751158196, ___mMaximumWordInstances_12)); }
	inline int32_t get_mMaximumWordInstances_12() const { return ___mMaximumWordInstances_12; }
	inline int32_t* get_address_of_mMaximumWordInstances_12() { return &___mMaximumWordInstances_12; }
	inline void set_mMaximumWordInstances_12(int32_t value)
	{
		___mMaximumWordInstances_12 = value;
	}

	inline static int32_t get_offset_of_mTextRecoEventHandlers_13() { return static_cast<int32_t>(offsetof(TextRecoAbstractBehaviour_t1751158196, ___mTextRecoEventHandlers_13)); }
	inline List_1_t4098171074 * get_mTextRecoEventHandlers_13() const { return ___mTextRecoEventHandlers_13; }
	inline List_1_t4098171074 ** get_address_of_mTextRecoEventHandlers_13() { return &___mTextRecoEventHandlers_13; }
	inline void set_mTextRecoEventHandlers_13(List_1_t4098171074 * value)
	{
		___mTextRecoEventHandlers_13 = value;
		Il2CppCodeGenWriteBarrier((&___mTextRecoEventHandlers_13), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTRECOABSTRACTBEHAVIOUR_T1751158196_H
#ifndef TURNOFFABSTRACTBEHAVIOUR_T1066217039_H
#define TURNOFFABSTRACTBEHAVIOUR_T1066217039_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.TurnOffAbstractBehaviour
struct  TurnOffAbstractBehaviour_t1066217039  : public MonoBehaviour_t345688271
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TURNOFFABSTRACTBEHAVIOUR_T1066217039_H
#ifndef MASKOUTABSTRACTBEHAVIOUR_T2174599097_H
#define MASKOUTABSTRACTBEHAVIOUR_T2174599097_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.MaskOutAbstractBehaviour
struct  MaskOutAbstractBehaviour_t2174599097  : public MonoBehaviour_t345688271
{
public:
	// UnityEngine.Material Vuforia.MaskOutAbstractBehaviour::maskMaterial
	Material_t1079520667 * ___maskMaterial_2;

public:
	inline static int32_t get_offset_of_maskMaterial_2() { return static_cast<int32_t>(offsetof(MaskOutAbstractBehaviour_t2174599097, ___maskMaterial_2)); }
	inline Material_t1079520667 * get_maskMaterial_2() const { return ___maskMaterial_2; }
	inline Material_t1079520667 ** get_address_of_maskMaterial_2() { return &___maskMaterial_2; }
	inline void set_maskMaterial_2(Material_t1079520667 * value)
	{
		___maskMaterial_2 = value;
		Il2CppCodeGenWriteBarrier((&___maskMaterial_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MASKOUTABSTRACTBEHAVIOUR_T2174599097_H
#ifndef GLERRORHANDLER_T1216999495_H
#define GLERRORHANDLER_T1216999495_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.GLErrorHandler
struct  GLErrorHandler_t1216999495  : public MonoBehaviour_t345688271
{
public:

public:
};

struct GLErrorHandler_t1216999495_StaticFields
{
public:
	// System.String Vuforia.GLErrorHandler::mErrorText
	String_t* ___mErrorText_2;
	// System.Boolean Vuforia.GLErrorHandler::mErrorOccurred
	bool ___mErrorOccurred_3;

public:
	inline static int32_t get_offset_of_mErrorText_2() { return static_cast<int32_t>(offsetof(GLErrorHandler_t1216999495_StaticFields, ___mErrorText_2)); }
	inline String_t* get_mErrorText_2() const { return ___mErrorText_2; }
	inline String_t** get_address_of_mErrorText_2() { return &___mErrorText_2; }
	inline void set_mErrorText_2(String_t* value)
	{
		___mErrorText_2 = value;
		Il2CppCodeGenWriteBarrier((&___mErrorText_2), value);
	}

	inline static int32_t get_offset_of_mErrorOccurred_3() { return static_cast<int32_t>(offsetof(GLErrorHandler_t1216999495_StaticFields, ___mErrorOccurred_3)); }
	inline bool get_mErrorOccurred_3() const { return ___mErrorOccurred_3; }
	inline bool* get_address_of_mErrorOccurred_3() { return &___mErrorOccurred_3; }
	inline void set_mErrorOccurred_3(bool value)
	{
		___mErrorOccurred_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GLERRORHANDLER_T1216999495_H
#ifndef TRACKABLEBEHAVIOUR_T2419079356_H
#define TRACKABLEBEHAVIOUR_T2419079356_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.TrackableBehaviour
struct  TrackableBehaviour_t2419079356  : public MonoBehaviour_t345688271
{
public:
	// System.Double Vuforia.TrackableBehaviour::<TimeStamp>k__BackingField
	double ___U3CTimeStampU3Ek__BackingField_2;
	// System.String Vuforia.TrackableBehaviour::mTrackableName
	String_t* ___mTrackableName_3;
	// System.Boolean Vuforia.TrackableBehaviour::mPreserveChildSize
	bool ___mPreserveChildSize_4;
	// System.Boolean Vuforia.TrackableBehaviour::mInitializedInEditor
	bool ___mInitializedInEditor_5;
	// UnityEngine.Vector3 Vuforia.TrackableBehaviour::mPreviousScale
	Vector3_t2903530434  ___mPreviousScale_6;
	// Vuforia.TrackableBehaviour/Status Vuforia.TrackableBehaviour::mStatus
	int32_t ___mStatus_7;
	// Vuforia.Trackable Vuforia.TrackableBehaviour::mTrackable
	RuntimeObject* ___mTrackable_8;
	// System.Collections.Generic.List`1<Vuforia.ITrackableEventHandler> Vuforia.TrackableBehaviour::mTrackableEventHandlers
	List_1_t3589236026 * ___mTrackableEventHandlers_9;

public:
	inline static int32_t get_offset_of_U3CTimeStampU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(TrackableBehaviour_t2419079356, ___U3CTimeStampU3Ek__BackingField_2)); }
	inline double get_U3CTimeStampU3Ek__BackingField_2() const { return ___U3CTimeStampU3Ek__BackingField_2; }
	inline double* get_address_of_U3CTimeStampU3Ek__BackingField_2() { return &___U3CTimeStampU3Ek__BackingField_2; }
	inline void set_U3CTimeStampU3Ek__BackingField_2(double value)
	{
		___U3CTimeStampU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_mTrackableName_3() { return static_cast<int32_t>(offsetof(TrackableBehaviour_t2419079356, ___mTrackableName_3)); }
	inline String_t* get_mTrackableName_3() const { return ___mTrackableName_3; }
	inline String_t** get_address_of_mTrackableName_3() { return &___mTrackableName_3; }
	inline void set_mTrackableName_3(String_t* value)
	{
		___mTrackableName_3 = value;
		Il2CppCodeGenWriteBarrier((&___mTrackableName_3), value);
	}

	inline static int32_t get_offset_of_mPreserveChildSize_4() { return static_cast<int32_t>(offsetof(TrackableBehaviour_t2419079356, ___mPreserveChildSize_4)); }
	inline bool get_mPreserveChildSize_4() const { return ___mPreserveChildSize_4; }
	inline bool* get_address_of_mPreserveChildSize_4() { return &___mPreserveChildSize_4; }
	inline void set_mPreserveChildSize_4(bool value)
	{
		___mPreserveChildSize_4 = value;
	}

	inline static int32_t get_offset_of_mInitializedInEditor_5() { return static_cast<int32_t>(offsetof(TrackableBehaviour_t2419079356, ___mInitializedInEditor_5)); }
	inline bool get_mInitializedInEditor_5() const { return ___mInitializedInEditor_5; }
	inline bool* get_address_of_mInitializedInEditor_5() { return &___mInitializedInEditor_5; }
	inline void set_mInitializedInEditor_5(bool value)
	{
		___mInitializedInEditor_5 = value;
	}

	inline static int32_t get_offset_of_mPreviousScale_6() { return static_cast<int32_t>(offsetof(TrackableBehaviour_t2419079356, ___mPreviousScale_6)); }
	inline Vector3_t2903530434  get_mPreviousScale_6() const { return ___mPreviousScale_6; }
	inline Vector3_t2903530434 * get_address_of_mPreviousScale_6() { return &___mPreviousScale_6; }
	inline void set_mPreviousScale_6(Vector3_t2903530434  value)
	{
		___mPreviousScale_6 = value;
	}

	inline static int32_t get_offset_of_mStatus_7() { return static_cast<int32_t>(offsetof(TrackableBehaviour_t2419079356, ___mStatus_7)); }
	inline int32_t get_mStatus_7() const { return ___mStatus_7; }
	inline int32_t* get_address_of_mStatus_7() { return &___mStatus_7; }
	inline void set_mStatus_7(int32_t value)
	{
		___mStatus_7 = value;
	}

	inline static int32_t get_offset_of_mTrackable_8() { return static_cast<int32_t>(offsetof(TrackableBehaviour_t2419079356, ___mTrackable_8)); }
	inline RuntimeObject* get_mTrackable_8() const { return ___mTrackable_8; }
	inline RuntimeObject** get_address_of_mTrackable_8() { return &___mTrackable_8; }
	inline void set_mTrackable_8(RuntimeObject* value)
	{
		___mTrackable_8 = value;
		Il2CppCodeGenWriteBarrier((&___mTrackable_8), value);
	}

	inline static int32_t get_offset_of_mTrackableEventHandlers_9() { return static_cast<int32_t>(offsetof(TrackableBehaviour_t2419079356, ___mTrackableEventHandlers_9)); }
	inline List_1_t3589236026 * get_mTrackableEventHandlers_9() const { return ___mTrackableEventHandlers_9; }
	inline List_1_t3589236026 ** get_address_of_mTrackableEventHandlers_9() { return &___mTrackableEventHandlers_9; }
	inline void set_mTrackableEventHandlers_9(List_1_t3589236026 * value)
	{
		___mTrackableEventHandlers_9 = value;
		Il2CppCodeGenWriteBarrier((&___mTrackableEventHandlers_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRACKABLEBEHAVIOUR_T2419079356_H
#ifndef DEFAULTTRACKABLEEVENTHANDLER_T2331528573_H
#define DEFAULTTRACKABLEEVENTHANDLER_T2331528573_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.DefaultTrackableEventHandler
struct  DefaultTrackableEventHandler_t2331528573  : public MonoBehaviour_t345688271
{
public:
	// Vuforia.TrackableBehaviour Vuforia.DefaultTrackableEventHandler::mTrackableBehaviour
	TrackableBehaviour_t2419079356 * ___mTrackableBehaviour_2;

public:
	inline static int32_t get_offset_of_mTrackableBehaviour_2() { return static_cast<int32_t>(offsetof(DefaultTrackableEventHandler_t2331528573, ___mTrackableBehaviour_2)); }
	inline TrackableBehaviour_t2419079356 * get_mTrackableBehaviour_2() const { return ___mTrackableBehaviour_2; }
	inline TrackableBehaviour_t2419079356 ** get_address_of_mTrackableBehaviour_2() { return &___mTrackableBehaviour_2; }
	inline void set_mTrackableBehaviour_2(TrackableBehaviour_t2419079356 * value)
	{
		___mTrackableBehaviour_2 = value;
		Il2CppCodeGenWriteBarrier((&___mTrackableBehaviour_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFAULTTRACKABLEEVENTHANDLER_T2331528573_H
#ifndef RECONSTRUCTIONABSTRACTBEHAVIOUR_T954468633_H
#define RECONSTRUCTIONABSTRACTBEHAVIOUR_T954468633_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.ReconstructionAbstractBehaviour
struct  ReconstructionAbstractBehaviour_t954468633  : public MonoBehaviour_t345688271
{
public:
	// System.Boolean Vuforia.ReconstructionAbstractBehaviour::mHasInitialized
	bool ___mHasInitialized_2;
	// System.Collections.Generic.List`1<Vuforia.ISmartTerrainEventHandler> Vuforia.ReconstructionAbstractBehaviour::mSmartTerrainEventHandlers
	List_1_t3446838688 * ___mSmartTerrainEventHandlers_3;
	// System.Action`1<Vuforia.SmartTerrainInitializationInfo> Vuforia.ReconstructionAbstractBehaviour::mOnInitialized
	Action_1_t1488611326 * ___mOnInitialized_4;
	// System.Action`1<Vuforia.Prop> Vuforia.ReconstructionAbstractBehaviour::mOnPropCreated
	Action_1_t1560204979 * ___mOnPropCreated_5;
	// System.Action`1<Vuforia.Prop> Vuforia.ReconstructionAbstractBehaviour::mOnPropUpdated
	Action_1_t1560204979 * ___mOnPropUpdated_6;
	// System.Action`1<Vuforia.Prop> Vuforia.ReconstructionAbstractBehaviour::mOnPropDeleted
	Action_1_t1560204979 * ___mOnPropDeleted_7;
	// System.Action`1<Vuforia.Surface> Vuforia.ReconstructionAbstractBehaviour::mOnSurfaceCreated
	Action_1_t1020068010 * ___mOnSurfaceCreated_8;
	// System.Action`1<Vuforia.Surface> Vuforia.ReconstructionAbstractBehaviour::mOnSurfaceUpdated
	Action_1_t1020068010 * ___mOnSurfaceUpdated_9;
	// System.Action`1<Vuforia.Surface> Vuforia.ReconstructionAbstractBehaviour::mOnSurfaceDeleted
	Action_1_t1020068010 * ___mOnSurfaceDeleted_10;
	// System.Boolean Vuforia.ReconstructionAbstractBehaviour::mInitializedInEditor
	bool ___mInitializedInEditor_11;
	// System.Boolean Vuforia.ReconstructionAbstractBehaviour::mMaximumExtentEnabled
	bool ___mMaximumExtentEnabled_12;
	// UnityEngine.Rect Vuforia.ReconstructionAbstractBehaviour::mMaximumExtent
	Rect_t3436776195  ___mMaximumExtent_13;
	// System.Boolean Vuforia.ReconstructionAbstractBehaviour::mAutomaticStart
	bool ___mAutomaticStart_14;
	// System.Boolean Vuforia.ReconstructionAbstractBehaviour::mNavMeshUpdates
	bool ___mNavMeshUpdates_15;
	// System.Single Vuforia.ReconstructionAbstractBehaviour::mNavMeshPadding
	float ___mNavMeshPadding_16;
	// Vuforia.Reconstruction Vuforia.ReconstructionAbstractBehaviour::mReconstruction
	RuntimeObject* ___mReconstruction_17;
	// System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.Surface> Vuforia.ReconstructionAbstractBehaviour::mSurfaces
	Dictionary_2_t2230247832 * ___mSurfaces_18;
	// System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.SurfaceAbstractBehaviour> Vuforia.ReconstructionAbstractBehaviour::mActiveSurfaceBehaviours
	Dictionary_2_t1468548340 * ___mActiveSurfaceBehaviours_19;
	// System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.Prop> Vuforia.ReconstructionAbstractBehaviour::mProps
	Dictionary_2_t2770384801 * ___mProps_20;
	// System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.PropAbstractBehaviour> Vuforia.ReconstructionAbstractBehaviour::mActivePropBehaviours
	Dictionary_2_t3844889330 * ___mActivePropBehaviours_21;
	// Vuforia.SurfaceAbstractBehaviour Vuforia.ReconstructionAbstractBehaviour::mPreviouslySetWorldCenterSurfaceTemplate
	SurfaceAbstractBehaviour_t272541176 * ___mPreviouslySetWorldCenterSurfaceTemplate_22;
	// System.Boolean Vuforia.ReconstructionAbstractBehaviour::mIgnoreNextUpdate
	bool ___mIgnoreNextUpdate_23;

public:
	inline static int32_t get_offset_of_mHasInitialized_2() { return static_cast<int32_t>(offsetof(ReconstructionAbstractBehaviour_t954468633, ___mHasInitialized_2)); }
	inline bool get_mHasInitialized_2() const { return ___mHasInitialized_2; }
	inline bool* get_address_of_mHasInitialized_2() { return &___mHasInitialized_2; }
	inline void set_mHasInitialized_2(bool value)
	{
		___mHasInitialized_2 = value;
	}

	inline static int32_t get_offset_of_mSmartTerrainEventHandlers_3() { return static_cast<int32_t>(offsetof(ReconstructionAbstractBehaviour_t954468633, ___mSmartTerrainEventHandlers_3)); }
	inline List_1_t3446838688 * get_mSmartTerrainEventHandlers_3() const { return ___mSmartTerrainEventHandlers_3; }
	inline List_1_t3446838688 ** get_address_of_mSmartTerrainEventHandlers_3() { return &___mSmartTerrainEventHandlers_3; }
	inline void set_mSmartTerrainEventHandlers_3(List_1_t3446838688 * value)
	{
		___mSmartTerrainEventHandlers_3 = value;
		Il2CppCodeGenWriteBarrier((&___mSmartTerrainEventHandlers_3), value);
	}

	inline static int32_t get_offset_of_mOnInitialized_4() { return static_cast<int32_t>(offsetof(ReconstructionAbstractBehaviour_t954468633, ___mOnInitialized_4)); }
	inline Action_1_t1488611326 * get_mOnInitialized_4() const { return ___mOnInitialized_4; }
	inline Action_1_t1488611326 ** get_address_of_mOnInitialized_4() { return &___mOnInitialized_4; }
	inline void set_mOnInitialized_4(Action_1_t1488611326 * value)
	{
		___mOnInitialized_4 = value;
		Il2CppCodeGenWriteBarrier((&___mOnInitialized_4), value);
	}

	inline static int32_t get_offset_of_mOnPropCreated_5() { return static_cast<int32_t>(offsetof(ReconstructionAbstractBehaviour_t954468633, ___mOnPropCreated_5)); }
	inline Action_1_t1560204979 * get_mOnPropCreated_5() const { return ___mOnPropCreated_5; }
	inline Action_1_t1560204979 ** get_address_of_mOnPropCreated_5() { return &___mOnPropCreated_5; }
	inline void set_mOnPropCreated_5(Action_1_t1560204979 * value)
	{
		___mOnPropCreated_5 = value;
		Il2CppCodeGenWriteBarrier((&___mOnPropCreated_5), value);
	}

	inline static int32_t get_offset_of_mOnPropUpdated_6() { return static_cast<int32_t>(offsetof(ReconstructionAbstractBehaviour_t954468633, ___mOnPropUpdated_6)); }
	inline Action_1_t1560204979 * get_mOnPropUpdated_6() const { return ___mOnPropUpdated_6; }
	inline Action_1_t1560204979 ** get_address_of_mOnPropUpdated_6() { return &___mOnPropUpdated_6; }
	inline void set_mOnPropUpdated_6(Action_1_t1560204979 * value)
	{
		___mOnPropUpdated_6 = value;
		Il2CppCodeGenWriteBarrier((&___mOnPropUpdated_6), value);
	}

	inline static int32_t get_offset_of_mOnPropDeleted_7() { return static_cast<int32_t>(offsetof(ReconstructionAbstractBehaviour_t954468633, ___mOnPropDeleted_7)); }
	inline Action_1_t1560204979 * get_mOnPropDeleted_7() const { return ___mOnPropDeleted_7; }
	inline Action_1_t1560204979 ** get_address_of_mOnPropDeleted_7() { return &___mOnPropDeleted_7; }
	inline void set_mOnPropDeleted_7(Action_1_t1560204979 * value)
	{
		___mOnPropDeleted_7 = value;
		Il2CppCodeGenWriteBarrier((&___mOnPropDeleted_7), value);
	}

	inline static int32_t get_offset_of_mOnSurfaceCreated_8() { return static_cast<int32_t>(offsetof(ReconstructionAbstractBehaviour_t954468633, ___mOnSurfaceCreated_8)); }
	inline Action_1_t1020068010 * get_mOnSurfaceCreated_8() const { return ___mOnSurfaceCreated_8; }
	inline Action_1_t1020068010 ** get_address_of_mOnSurfaceCreated_8() { return &___mOnSurfaceCreated_8; }
	inline void set_mOnSurfaceCreated_8(Action_1_t1020068010 * value)
	{
		___mOnSurfaceCreated_8 = value;
		Il2CppCodeGenWriteBarrier((&___mOnSurfaceCreated_8), value);
	}

	inline static int32_t get_offset_of_mOnSurfaceUpdated_9() { return static_cast<int32_t>(offsetof(ReconstructionAbstractBehaviour_t954468633, ___mOnSurfaceUpdated_9)); }
	inline Action_1_t1020068010 * get_mOnSurfaceUpdated_9() const { return ___mOnSurfaceUpdated_9; }
	inline Action_1_t1020068010 ** get_address_of_mOnSurfaceUpdated_9() { return &___mOnSurfaceUpdated_9; }
	inline void set_mOnSurfaceUpdated_9(Action_1_t1020068010 * value)
	{
		___mOnSurfaceUpdated_9 = value;
		Il2CppCodeGenWriteBarrier((&___mOnSurfaceUpdated_9), value);
	}

	inline static int32_t get_offset_of_mOnSurfaceDeleted_10() { return static_cast<int32_t>(offsetof(ReconstructionAbstractBehaviour_t954468633, ___mOnSurfaceDeleted_10)); }
	inline Action_1_t1020068010 * get_mOnSurfaceDeleted_10() const { return ___mOnSurfaceDeleted_10; }
	inline Action_1_t1020068010 ** get_address_of_mOnSurfaceDeleted_10() { return &___mOnSurfaceDeleted_10; }
	inline void set_mOnSurfaceDeleted_10(Action_1_t1020068010 * value)
	{
		___mOnSurfaceDeleted_10 = value;
		Il2CppCodeGenWriteBarrier((&___mOnSurfaceDeleted_10), value);
	}

	inline static int32_t get_offset_of_mInitializedInEditor_11() { return static_cast<int32_t>(offsetof(ReconstructionAbstractBehaviour_t954468633, ___mInitializedInEditor_11)); }
	inline bool get_mInitializedInEditor_11() const { return ___mInitializedInEditor_11; }
	inline bool* get_address_of_mInitializedInEditor_11() { return &___mInitializedInEditor_11; }
	inline void set_mInitializedInEditor_11(bool value)
	{
		___mInitializedInEditor_11 = value;
	}

	inline static int32_t get_offset_of_mMaximumExtentEnabled_12() { return static_cast<int32_t>(offsetof(ReconstructionAbstractBehaviour_t954468633, ___mMaximumExtentEnabled_12)); }
	inline bool get_mMaximumExtentEnabled_12() const { return ___mMaximumExtentEnabled_12; }
	inline bool* get_address_of_mMaximumExtentEnabled_12() { return &___mMaximumExtentEnabled_12; }
	inline void set_mMaximumExtentEnabled_12(bool value)
	{
		___mMaximumExtentEnabled_12 = value;
	}

	inline static int32_t get_offset_of_mMaximumExtent_13() { return static_cast<int32_t>(offsetof(ReconstructionAbstractBehaviour_t954468633, ___mMaximumExtent_13)); }
	inline Rect_t3436776195  get_mMaximumExtent_13() const { return ___mMaximumExtent_13; }
	inline Rect_t3436776195 * get_address_of_mMaximumExtent_13() { return &___mMaximumExtent_13; }
	inline void set_mMaximumExtent_13(Rect_t3436776195  value)
	{
		___mMaximumExtent_13 = value;
	}

	inline static int32_t get_offset_of_mAutomaticStart_14() { return static_cast<int32_t>(offsetof(ReconstructionAbstractBehaviour_t954468633, ___mAutomaticStart_14)); }
	inline bool get_mAutomaticStart_14() const { return ___mAutomaticStart_14; }
	inline bool* get_address_of_mAutomaticStart_14() { return &___mAutomaticStart_14; }
	inline void set_mAutomaticStart_14(bool value)
	{
		___mAutomaticStart_14 = value;
	}

	inline static int32_t get_offset_of_mNavMeshUpdates_15() { return static_cast<int32_t>(offsetof(ReconstructionAbstractBehaviour_t954468633, ___mNavMeshUpdates_15)); }
	inline bool get_mNavMeshUpdates_15() const { return ___mNavMeshUpdates_15; }
	inline bool* get_address_of_mNavMeshUpdates_15() { return &___mNavMeshUpdates_15; }
	inline void set_mNavMeshUpdates_15(bool value)
	{
		___mNavMeshUpdates_15 = value;
	}

	inline static int32_t get_offset_of_mNavMeshPadding_16() { return static_cast<int32_t>(offsetof(ReconstructionAbstractBehaviour_t954468633, ___mNavMeshPadding_16)); }
	inline float get_mNavMeshPadding_16() const { return ___mNavMeshPadding_16; }
	inline float* get_address_of_mNavMeshPadding_16() { return &___mNavMeshPadding_16; }
	inline void set_mNavMeshPadding_16(float value)
	{
		___mNavMeshPadding_16 = value;
	}

	inline static int32_t get_offset_of_mReconstruction_17() { return static_cast<int32_t>(offsetof(ReconstructionAbstractBehaviour_t954468633, ___mReconstruction_17)); }
	inline RuntimeObject* get_mReconstruction_17() const { return ___mReconstruction_17; }
	inline RuntimeObject** get_address_of_mReconstruction_17() { return &___mReconstruction_17; }
	inline void set_mReconstruction_17(RuntimeObject* value)
	{
		___mReconstruction_17 = value;
		Il2CppCodeGenWriteBarrier((&___mReconstruction_17), value);
	}

	inline static int32_t get_offset_of_mSurfaces_18() { return static_cast<int32_t>(offsetof(ReconstructionAbstractBehaviour_t954468633, ___mSurfaces_18)); }
	inline Dictionary_2_t2230247832 * get_mSurfaces_18() const { return ___mSurfaces_18; }
	inline Dictionary_2_t2230247832 ** get_address_of_mSurfaces_18() { return &___mSurfaces_18; }
	inline void set_mSurfaces_18(Dictionary_2_t2230247832 * value)
	{
		___mSurfaces_18 = value;
		Il2CppCodeGenWriteBarrier((&___mSurfaces_18), value);
	}

	inline static int32_t get_offset_of_mActiveSurfaceBehaviours_19() { return static_cast<int32_t>(offsetof(ReconstructionAbstractBehaviour_t954468633, ___mActiveSurfaceBehaviours_19)); }
	inline Dictionary_2_t1468548340 * get_mActiveSurfaceBehaviours_19() const { return ___mActiveSurfaceBehaviours_19; }
	inline Dictionary_2_t1468548340 ** get_address_of_mActiveSurfaceBehaviours_19() { return &___mActiveSurfaceBehaviours_19; }
	inline void set_mActiveSurfaceBehaviours_19(Dictionary_2_t1468548340 * value)
	{
		___mActiveSurfaceBehaviours_19 = value;
		Il2CppCodeGenWriteBarrier((&___mActiveSurfaceBehaviours_19), value);
	}

	inline static int32_t get_offset_of_mProps_20() { return static_cast<int32_t>(offsetof(ReconstructionAbstractBehaviour_t954468633, ___mProps_20)); }
	inline Dictionary_2_t2770384801 * get_mProps_20() const { return ___mProps_20; }
	inline Dictionary_2_t2770384801 ** get_address_of_mProps_20() { return &___mProps_20; }
	inline void set_mProps_20(Dictionary_2_t2770384801 * value)
	{
		___mProps_20 = value;
		Il2CppCodeGenWriteBarrier((&___mProps_20), value);
	}

	inline static int32_t get_offset_of_mActivePropBehaviours_21() { return static_cast<int32_t>(offsetof(ReconstructionAbstractBehaviour_t954468633, ___mActivePropBehaviours_21)); }
	inline Dictionary_2_t3844889330 * get_mActivePropBehaviours_21() const { return ___mActivePropBehaviours_21; }
	inline Dictionary_2_t3844889330 ** get_address_of_mActivePropBehaviours_21() { return &___mActivePropBehaviours_21; }
	inline void set_mActivePropBehaviours_21(Dictionary_2_t3844889330 * value)
	{
		___mActivePropBehaviours_21 = value;
		Il2CppCodeGenWriteBarrier((&___mActivePropBehaviours_21), value);
	}

	inline static int32_t get_offset_of_mPreviouslySetWorldCenterSurfaceTemplate_22() { return static_cast<int32_t>(offsetof(ReconstructionAbstractBehaviour_t954468633, ___mPreviouslySetWorldCenterSurfaceTemplate_22)); }
	inline SurfaceAbstractBehaviour_t272541176 * get_mPreviouslySetWorldCenterSurfaceTemplate_22() const { return ___mPreviouslySetWorldCenterSurfaceTemplate_22; }
	inline SurfaceAbstractBehaviour_t272541176 ** get_address_of_mPreviouslySetWorldCenterSurfaceTemplate_22() { return &___mPreviouslySetWorldCenterSurfaceTemplate_22; }
	inline void set_mPreviouslySetWorldCenterSurfaceTemplate_22(SurfaceAbstractBehaviour_t272541176 * value)
	{
		___mPreviouslySetWorldCenterSurfaceTemplate_22 = value;
		Il2CppCodeGenWriteBarrier((&___mPreviouslySetWorldCenterSurfaceTemplate_22), value);
	}

	inline static int32_t get_offset_of_mIgnoreNextUpdate_23() { return static_cast<int32_t>(offsetof(ReconstructionAbstractBehaviour_t954468633, ___mIgnoreNextUpdate_23)); }
	inline bool get_mIgnoreNextUpdate_23() const { return ___mIgnoreNextUpdate_23; }
	inline bool* get_address_of_mIgnoreNextUpdate_23() { return &___mIgnoreNextUpdate_23; }
	inline void set_mIgnoreNextUpdate_23(bool value)
	{
		___mIgnoreNextUpdate_23 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RECONSTRUCTIONABSTRACTBEHAVIOUR_T954468633_H
#ifndef RECONSTRUCTIONFROMTARGETABSTRACTBEHAVIOUR_T2676804737_H
#define RECONSTRUCTIONFROMTARGETABSTRACTBEHAVIOUR_T2676804737_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.ReconstructionFromTargetAbstractBehaviour
struct  ReconstructionFromTargetAbstractBehaviour_t2676804737  : public MonoBehaviour_t345688271
{
public:
	// Vuforia.ReconstructionFromTarget Vuforia.ReconstructionFromTargetAbstractBehaviour::mReconstructionFromTarget
	RuntimeObject* ___mReconstructionFromTarget_2;
	// Vuforia.ReconstructionAbstractBehaviour Vuforia.ReconstructionFromTargetAbstractBehaviour::mReconstructionBehaviour
	ReconstructionAbstractBehaviour_t954468633 * ___mReconstructionBehaviour_3;

public:
	inline static int32_t get_offset_of_mReconstructionFromTarget_2() { return static_cast<int32_t>(offsetof(ReconstructionFromTargetAbstractBehaviour_t2676804737, ___mReconstructionFromTarget_2)); }
	inline RuntimeObject* get_mReconstructionFromTarget_2() const { return ___mReconstructionFromTarget_2; }
	inline RuntimeObject** get_address_of_mReconstructionFromTarget_2() { return &___mReconstructionFromTarget_2; }
	inline void set_mReconstructionFromTarget_2(RuntimeObject* value)
	{
		___mReconstructionFromTarget_2 = value;
		Il2CppCodeGenWriteBarrier((&___mReconstructionFromTarget_2), value);
	}

	inline static int32_t get_offset_of_mReconstructionBehaviour_3() { return static_cast<int32_t>(offsetof(ReconstructionFromTargetAbstractBehaviour_t2676804737, ___mReconstructionBehaviour_3)); }
	inline ReconstructionAbstractBehaviour_t954468633 * get_mReconstructionBehaviour_3() const { return ___mReconstructionBehaviour_3; }
	inline ReconstructionAbstractBehaviour_t954468633 ** get_address_of_mReconstructionBehaviour_3() { return &___mReconstructionBehaviour_3; }
	inline void set_mReconstructionBehaviour_3(ReconstructionAbstractBehaviour_t954468633 * value)
	{
		___mReconstructionBehaviour_3 = value;
		Il2CppCodeGenWriteBarrier((&___mReconstructionBehaviour_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RECONSTRUCTIONFROMTARGETABSTRACTBEHAVIOUR_T2676804737_H
#ifndef DEFAULTSMARTTERRAINEVENTHANDLER_T3202897208_H
#define DEFAULTSMARTTERRAINEVENTHANDLER_T3202897208_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.DefaultSmartTerrainEventHandler
struct  DefaultSmartTerrainEventHandler_t3202897208  : public MonoBehaviour_t345688271
{
public:
	// Vuforia.ReconstructionBehaviour Vuforia.DefaultSmartTerrainEventHandler::mReconstructionBehaviour
	ReconstructionBehaviour_t3936121853 * ___mReconstructionBehaviour_2;
	// Vuforia.PropBehaviour Vuforia.DefaultSmartTerrainEventHandler::PropTemplate
	PropBehaviour_t3897855688 * ___PropTemplate_3;
	// Vuforia.SurfaceBehaviour Vuforia.DefaultSmartTerrainEventHandler::SurfaceTemplate
	SurfaceBehaviour_t1975960194 * ___SurfaceTemplate_4;

public:
	inline static int32_t get_offset_of_mReconstructionBehaviour_2() { return static_cast<int32_t>(offsetof(DefaultSmartTerrainEventHandler_t3202897208, ___mReconstructionBehaviour_2)); }
	inline ReconstructionBehaviour_t3936121853 * get_mReconstructionBehaviour_2() const { return ___mReconstructionBehaviour_2; }
	inline ReconstructionBehaviour_t3936121853 ** get_address_of_mReconstructionBehaviour_2() { return &___mReconstructionBehaviour_2; }
	inline void set_mReconstructionBehaviour_2(ReconstructionBehaviour_t3936121853 * value)
	{
		___mReconstructionBehaviour_2 = value;
		Il2CppCodeGenWriteBarrier((&___mReconstructionBehaviour_2), value);
	}

	inline static int32_t get_offset_of_PropTemplate_3() { return static_cast<int32_t>(offsetof(DefaultSmartTerrainEventHandler_t3202897208, ___PropTemplate_3)); }
	inline PropBehaviour_t3897855688 * get_PropTemplate_3() const { return ___PropTemplate_3; }
	inline PropBehaviour_t3897855688 ** get_address_of_PropTemplate_3() { return &___PropTemplate_3; }
	inline void set_PropTemplate_3(PropBehaviour_t3897855688 * value)
	{
		___PropTemplate_3 = value;
		Il2CppCodeGenWriteBarrier((&___PropTemplate_3), value);
	}

	inline static int32_t get_offset_of_SurfaceTemplate_4() { return static_cast<int32_t>(offsetof(DefaultSmartTerrainEventHandler_t3202897208, ___SurfaceTemplate_4)); }
	inline SurfaceBehaviour_t1975960194 * get_SurfaceTemplate_4() const { return ___SurfaceTemplate_4; }
	inline SurfaceBehaviour_t1975960194 ** get_address_of_SurfaceTemplate_4() { return &___SurfaceTemplate_4; }
	inline void set_SurfaceTemplate_4(SurfaceBehaviour_t1975960194 * value)
	{
		___SurfaceTemplate_4 = value;
		Il2CppCodeGenWriteBarrier((&___SurfaceTemplate_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFAULTSMARTTERRAINEVENTHANDLER_T3202897208_H
#ifndef VRINTEGRATIONHELPER_T1870100115_H
#define VRINTEGRATIONHELPER_T1870100115_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VRIntegrationHelper
struct  VRIntegrationHelper_t1870100115  : public MonoBehaviour_t345688271
{
public:
	// System.Boolean VRIntegrationHelper::IsLeft
	bool ___IsLeft_12;
	// UnityEngine.Transform VRIntegrationHelper::TrackableParent
	Transform_t3316442598 * ___TrackableParent_13;

public:
	inline static int32_t get_offset_of_IsLeft_12() { return static_cast<int32_t>(offsetof(VRIntegrationHelper_t1870100115, ___IsLeft_12)); }
	inline bool get_IsLeft_12() const { return ___IsLeft_12; }
	inline bool* get_address_of_IsLeft_12() { return &___IsLeft_12; }
	inline void set_IsLeft_12(bool value)
	{
		___IsLeft_12 = value;
	}

	inline static int32_t get_offset_of_TrackableParent_13() { return static_cast<int32_t>(offsetof(VRIntegrationHelper_t1870100115, ___TrackableParent_13)); }
	inline Transform_t3316442598 * get_TrackableParent_13() const { return ___TrackableParent_13; }
	inline Transform_t3316442598 ** get_address_of_TrackableParent_13() { return &___TrackableParent_13; }
	inline void set_TrackableParent_13(Transform_t3316442598 * value)
	{
		___TrackableParent_13 = value;
		Il2CppCodeGenWriteBarrier((&___TrackableParent_13), value);
	}
};

struct VRIntegrationHelper_t1870100115_StaticFields
{
public:
	// UnityEngine.Matrix4x4 VRIntegrationHelper::mLeftCameraMatrixOriginal
	Matrix4x4_t4266809202  ___mLeftCameraMatrixOriginal_2;
	// UnityEngine.Matrix4x4 VRIntegrationHelper::mRightCameraMatrixOriginal
	Matrix4x4_t4266809202  ___mRightCameraMatrixOriginal_3;
	// UnityEngine.Camera VRIntegrationHelper::mLeftCamera
	Camera_t3175186167 * ___mLeftCamera_4;
	// UnityEngine.Camera VRIntegrationHelper::mRightCamera
	Camera_t3175186167 * ___mRightCamera_5;
	// Vuforia.HideExcessAreaAbstractBehaviour VRIntegrationHelper::mLeftExcessAreaBehaviour
	HideExcessAreaAbstractBehaviour_t1324413388 * ___mLeftExcessAreaBehaviour_6;
	// Vuforia.HideExcessAreaAbstractBehaviour VRIntegrationHelper::mRightExcessAreaBehaviour
	HideExcessAreaAbstractBehaviour_t1324413388 * ___mRightExcessAreaBehaviour_7;
	// UnityEngine.Rect VRIntegrationHelper::mLeftCameraPixelRect
	Rect_t3436776195  ___mLeftCameraPixelRect_8;
	// UnityEngine.Rect VRIntegrationHelper::mRightCameraPixelRect
	Rect_t3436776195  ___mRightCameraPixelRect_9;
	// System.Boolean VRIntegrationHelper::mLeftCameraDataAcquired
	bool ___mLeftCameraDataAcquired_10;
	// System.Boolean VRIntegrationHelper::mRightCameraDataAcquired
	bool ___mRightCameraDataAcquired_11;

public:
	inline static int32_t get_offset_of_mLeftCameraMatrixOriginal_2() { return static_cast<int32_t>(offsetof(VRIntegrationHelper_t1870100115_StaticFields, ___mLeftCameraMatrixOriginal_2)); }
	inline Matrix4x4_t4266809202  get_mLeftCameraMatrixOriginal_2() const { return ___mLeftCameraMatrixOriginal_2; }
	inline Matrix4x4_t4266809202 * get_address_of_mLeftCameraMatrixOriginal_2() { return &___mLeftCameraMatrixOriginal_2; }
	inline void set_mLeftCameraMatrixOriginal_2(Matrix4x4_t4266809202  value)
	{
		___mLeftCameraMatrixOriginal_2 = value;
	}

	inline static int32_t get_offset_of_mRightCameraMatrixOriginal_3() { return static_cast<int32_t>(offsetof(VRIntegrationHelper_t1870100115_StaticFields, ___mRightCameraMatrixOriginal_3)); }
	inline Matrix4x4_t4266809202  get_mRightCameraMatrixOriginal_3() const { return ___mRightCameraMatrixOriginal_3; }
	inline Matrix4x4_t4266809202 * get_address_of_mRightCameraMatrixOriginal_3() { return &___mRightCameraMatrixOriginal_3; }
	inline void set_mRightCameraMatrixOriginal_3(Matrix4x4_t4266809202  value)
	{
		___mRightCameraMatrixOriginal_3 = value;
	}

	inline static int32_t get_offset_of_mLeftCamera_4() { return static_cast<int32_t>(offsetof(VRIntegrationHelper_t1870100115_StaticFields, ___mLeftCamera_4)); }
	inline Camera_t3175186167 * get_mLeftCamera_4() const { return ___mLeftCamera_4; }
	inline Camera_t3175186167 ** get_address_of_mLeftCamera_4() { return &___mLeftCamera_4; }
	inline void set_mLeftCamera_4(Camera_t3175186167 * value)
	{
		___mLeftCamera_4 = value;
		Il2CppCodeGenWriteBarrier((&___mLeftCamera_4), value);
	}

	inline static int32_t get_offset_of_mRightCamera_5() { return static_cast<int32_t>(offsetof(VRIntegrationHelper_t1870100115_StaticFields, ___mRightCamera_5)); }
	inline Camera_t3175186167 * get_mRightCamera_5() const { return ___mRightCamera_5; }
	inline Camera_t3175186167 ** get_address_of_mRightCamera_5() { return &___mRightCamera_5; }
	inline void set_mRightCamera_5(Camera_t3175186167 * value)
	{
		___mRightCamera_5 = value;
		Il2CppCodeGenWriteBarrier((&___mRightCamera_5), value);
	}

	inline static int32_t get_offset_of_mLeftExcessAreaBehaviour_6() { return static_cast<int32_t>(offsetof(VRIntegrationHelper_t1870100115_StaticFields, ___mLeftExcessAreaBehaviour_6)); }
	inline HideExcessAreaAbstractBehaviour_t1324413388 * get_mLeftExcessAreaBehaviour_6() const { return ___mLeftExcessAreaBehaviour_6; }
	inline HideExcessAreaAbstractBehaviour_t1324413388 ** get_address_of_mLeftExcessAreaBehaviour_6() { return &___mLeftExcessAreaBehaviour_6; }
	inline void set_mLeftExcessAreaBehaviour_6(HideExcessAreaAbstractBehaviour_t1324413388 * value)
	{
		___mLeftExcessAreaBehaviour_6 = value;
		Il2CppCodeGenWriteBarrier((&___mLeftExcessAreaBehaviour_6), value);
	}

	inline static int32_t get_offset_of_mRightExcessAreaBehaviour_7() { return static_cast<int32_t>(offsetof(VRIntegrationHelper_t1870100115_StaticFields, ___mRightExcessAreaBehaviour_7)); }
	inline HideExcessAreaAbstractBehaviour_t1324413388 * get_mRightExcessAreaBehaviour_7() const { return ___mRightExcessAreaBehaviour_7; }
	inline HideExcessAreaAbstractBehaviour_t1324413388 ** get_address_of_mRightExcessAreaBehaviour_7() { return &___mRightExcessAreaBehaviour_7; }
	inline void set_mRightExcessAreaBehaviour_7(HideExcessAreaAbstractBehaviour_t1324413388 * value)
	{
		___mRightExcessAreaBehaviour_7 = value;
		Il2CppCodeGenWriteBarrier((&___mRightExcessAreaBehaviour_7), value);
	}

	inline static int32_t get_offset_of_mLeftCameraPixelRect_8() { return static_cast<int32_t>(offsetof(VRIntegrationHelper_t1870100115_StaticFields, ___mLeftCameraPixelRect_8)); }
	inline Rect_t3436776195  get_mLeftCameraPixelRect_8() const { return ___mLeftCameraPixelRect_8; }
	inline Rect_t3436776195 * get_address_of_mLeftCameraPixelRect_8() { return &___mLeftCameraPixelRect_8; }
	inline void set_mLeftCameraPixelRect_8(Rect_t3436776195  value)
	{
		___mLeftCameraPixelRect_8 = value;
	}

	inline static int32_t get_offset_of_mRightCameraPixelRect_9() { return static_cast<int32_t>(offsetof(VRIntegrationHelper_t1870100115_StaticFields, ___mRightCameraPixelRect_9)); }
	inline Rect_t3436776195  get_mRightCameraPixelRect_9() const { return ___mRightCameraPixelRect_9; }
	inline Rect_t3436776195 * get_address_of_mRightCameraPixelRect_9() { return &___mRightCameraPixelRect_9; }
	inline void set_mRightCameraPixelRect_9(Rect_t3436776195  value)
	{
		___mRightCameraPixelRect_9 = value;
	}

	inline static int32_t get_offset_of_mLeftCameraDataAcquired_10() { return static_cast<int32_t>(offsetof(VRIntegrationHelper_t1870100115_StaticFields, ___mLeftCameraDataAcquired_10)); }
	inline bool get_mLeftCameraDataAcquired_10() const { return ___mLeftCameraDataAcquired_10; }
	inline bool* get_address_of_mLeftCameraDataAcquired_10() { return &___mLeftCameraDataAcquired_10; }
	inline void set_mLeftCameraDataAcquired_10(bool value)
	{
		___mLeftCameraDataAcquired_10 = value;
	}

	inline static int32_t get_offset_of_mRightCameraDataAcquired_11() { return static_cast<int32_t>(offsetof(VRIntegrationHelper_t1870100115_StaticFields, ___mRightCameraDataAcquired_11)); }
	inline bool get_mRightCameraDataAcquired_11() const { return ___mRightCameraDataAcquired_11; }
	inline bool* get_address_of_mRightCameraDataAcquired_11() { return &___mRightCameraDataAcquired_11; }
	inline void set_mRightCameraDataAcquired_11(bool value)
	{
		___mRightCameraDataAcquired_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VRINTEGRATIONHELPER_T1870100115_H
#ifndef TURNOFFWORDBEHAVIOUR_T1385869686_H
#define TURNOFFWORDBEHAVIOUR_T1385869686_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.TurnOffWordBehaviour
struct  TurnOffWordBehaviour_t1385869686  : public MonoBehaviour_t345688271
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TURNOFFWORDBEHAVIOUR_T1385869686_H
#ifndef VIRTUALBUTTONABSTRACTBEHAVIOUR_T1965820348_H
#define VIRTUALBUTTONABSTRACTBEHAVIOUR_T1965820348_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.VirtualButtonAbstractBehaviour
struct  VirtualButtonAbstractBehaviour_t1965820348  : public MonoBehaviour_t345688271
{
public:
	// System.String Vuforia.VirtualButtonAbstractBehaviour::mName
	String_t* ___mName_3;
	// Vuforia.VirtualButton/Sensitivity Vuforia.VirtualButtonAbstractBehaviour::mSensitivity
	int32_t ___mSensitivity_4;
	// System.Boolean Vuforia.VirtualButtonAbstractBehaviour::mHasUpdatedPose
	bool ___mHasUpdatedPose_5;
	// UnityEngine.Matrix4x4 Vuforia.VirtualButtonAbstractBehaviour::mPrevTransform
	Matrix4x4_t4266809202  ___mPrevTransform_6;
	// UnityEngine.GameObject Vuforia.VirtualButtonAbstractBehaviour::mPrevParent
	GameObject_t1811656094 * ___mPrevParent_7;
	// System.Boolean Vuforia.VirtualButtonAbstractBehaviour::mSensitivityDirty
	bool ___mSensitivityDirty_8;
	// Vuforia.VirtualButton/Sensitivity Vuforia.VirtualButtonAbstractBehaviour::mPreviousSensitivity
	int32_t ___mPreviousSensitivity_9;
	// System.Boolean Vuforia.VirtualButtonAbstractBehaviour::mPreviouslyEnabled
	bool ___mPreviouslyEnabled_10;
	// System.Boolean Vuforia.VirtualButtonAbstractBehaviour::mPressed
	bool ___mPressed_11;
	// System.Collections.Generic.List`1<Vuforia.IVirtualButtonEventHandler> Vuforia.VirtualButtonAbstractBehaviour::mHandlers
	List_1_t1800411770 * ___mHandlers_12;
	// UnityEngine.Vector2 Vuforia.VirtualButtonAbstractBehaviour::mLeftTop
	Vector2_t3577333262  ___mLeftTop_13;
	// UnityEngine.Vector2 Vuforia.VirtualButtonAbstractBehaviour::mRightBottom
	Vector2_t3577333262  ___mRightBottom_14;
	// System.Boolean Vuforia.VirtualButtonAbstractBehaviour::mUnregisterOnDestroy
	bool ___mUnregisterOnDestroy_15;
	// Vuforia.VirtualButton Vuforia.VirtualButtonAbstractBehaviour::mVirtualButton
	VirtualButton_t3666937711 * ___mVirtualButton_16;

public:
	inline static int32_t get_offset_of_mName_3() { return static_cast<int32_t>(offsetof(VirtualButtonAbstractBehaviour_t1965820348, ___mName_3)); }
	inline String_t* get_mName_3() const { return ___mName_3; }
	inline String_t** get_address_of_mName_3() { return &___mName_3; }
	inline void set_mName_3(String_t* value)
	{
		___mName_3 = value;
		Il2CppCodeGenWriteBarrier((&___mName_3), value);
	}

	inline static int32_t get_offset_of_mSensitivity_4() { return static_cast<int32_t>(offsetof(VirtualButtonAbstractBehaviour_t1965820348, ___mSensitivity_4)); }
	inline int32_t get_mSensitivity_4() const { return ___mSensitivity_4; }
	inline int32_t* get_address_of_mSensitivity_4() { return &___mSensitivity_4; }
	inline void set_mSensitivity_4(int32_t value)
	{
		___mSensitivity_4 = value;
	}

	inline static int32_t get_offset_of_mHasUpdatedPose_5() { return static_cast<int32_t>(offsetof(VirtualButtonAbstractBehaviour_t1965820348, ___mHasUpdatedPose_5)); }
	inline bool get_mHasUpdatedPose_5() const { return ___mHasUpdatedPose_5; }
	inline bool* get_address_of_mHasUpdatedPose_5() { return &___mHasUpdatedPose_5; }
	inline void set_mHasUpdatedPose_5(bool value)
	{
		___mHasUpdatedPose_5 = value;
	}

	inline static int32_t get_offset_of_mPrevTransform_6() { return static_cast<int32_t>(offsetof(VirtualButtonAbstractBehaviour_t1965820348, ___mPrevTransform_6)); }
	inline Matrix4x4_t4266809202  get_mPrevTransform_6() const { return ___mPrevTransform_6; }
	inline Matrix4x4_t4266809202 * get_address_of_mPrevTransform_6() { return &___mPrevTransform_6; }
	inline void set_mPrevTransform_6(Matrix4x4_t4266809202  value)
	{
		___mPrevTransform_6 = value;
	}

	inline static int32_t get_offset_of_mPrevParent_7() { return static_cast<int32_t>(offsetof(VirtualButtonAbstractBehaviour_t1965820348, ___mPrevParent_7)); }
	inline GameObject_t1811656094 * get_mPrevParent_7() const { return ___mPrevParent_7; }
	inline GameObject_t1811656094 ** get_address_of_mPrevParent_7() { return &___mPrevParent_7; }
	inline void set_mPrevParent_7(GameObject_t1811656094 * value)
	{
		___mPrevParent_7 = value;
		Il2CppCodeGenWriteBarrier((&___mPrevParent_7), value);
	}

	inline static int32_t get_offset_of_mSensitivityDirty_8() { return static_cast<int32_t>(offsetof(VirtualButtonAbstractBehaviour_t1965820348, ___mSensitivityDirty_8)); }
	inline bool get_mSensitivityDirty_8() const { return ___mSensitivityDirty_8; }
	inline bool* get_address_of_mSensitivityDirty_8() { return &___mSensitivityDirty_8; }
	inline void set_mSensitivityDirty_8(bool value)
	{
		___mSensitivityDirty_8 = value;
	}

	inline static int32_t get_offset_of_mPreviousSensitivity_9() { return static_cast<int32_t>(offsetof(VirtualButtonAbstractBehaviour_t1965820348, ___mPreviousSensitivity_9)); }
	inline int32_t get_mPreviousSensitivity_9() const { return ___mPreviousSensitivity_9; }
	inline int32_t* get_address_of_mPreviousSensitivity_9() { return &___mPreviousSensitivity_9; }
	inline void set_mPreviousSensitivity_9(int32_t value)
	{
		___mPreviousSensitivity_9 = value;
	}

	inline static int32_t get_offset_of_mPreviouslyEnabled_10() { return static_cast<int32_t>(offsetof(VirtualButtonAbstractBehaviour_t1965820348, ___mPreviouslyEnabled_10)); }
	inline bool get_mPreviouslyEnabled_10() const { return ___mPreviouslyEnabled_10; }
	inline bool* get_address_of_mPreviouslyEnabled_10() { return &___mPreviouslyEnabled_10; }
	inline void set_mPreviouslyEnabled_10(bool value)
	{
		___mPreviouslyEnabled_10 = value;
	}

	inline static int32_t get_offset_of_mPressed_11() { return static_cast<int32_t>(offsetof(VirtualButtonAbstractBehaviour_t1965820348, ___mPressed_11)); }
	inline bool get_mPressed_11() const { return ___mPressed_11; }
	inline bool* get_address_of_mPressed_11() { return &___mPressed_11; }
	inline void set_mPressed_11(bool value)
	{
		___mPressed_11 = value;
	}

	inline static int32_t get_offset_of_mHandlers_12() { return static_cast<int32_t>(offsetof(VirtualButtonAbstractBehaviour_t1965820348, ___mHandlers_12)); }
	inline List_1_t1800411770 * get_mHandlers_12() const { return ___mHandlers_12; }
	inline List_1_t1800411770 ** get_address_of_mHandlers_12() { return &___mHandlers_12; }
	inline void set_mHandlers_12(List_1_t1800411770 * value)
	{
		___mHandlers_12 = value;
		Il2CppCodeGenWriteBarrier((&___mHandlers_12), value);
	}

	inline static int32_t get_offset_of_mLeftTop_13() { return static_cast<int32_t>(offsetof(VirtualButtonAbstractBehaviour_t1965820348, ___mLeftTop_13)); }
	inline Vector2_t3577333262  get_mLeftTop_13() const { return ___mLeftTop_13; }
	inline Vector2_t3577333262 * get_address_of_mLeftTop_13() { return &___mLeftTop_13; }
	inline void set_mLeftTop_13(Vector2_t3577333262  value)
	{
		___mLeftTop_13 = value;
	}

	inline static int32_t get_offset_of_mRightBottom_14() { return static_cast<int32_t>(offsetof(VirtualButtonAbstractBehaviour_t1965820348, ___mRightBottom_14)); }
	inline Vector2_t3577333262  get_mRightBottom_14() const { return ___mRightBottom_14; }
	inline Vector2_t3577333262 * get_address_of_mRightBottom_14() { return &___mRightBottom_14; }
	inline void set_mRightBottom_14(Vector2_t3577333262  value)
	{
		___mRightBottom_14 = value;
	}

	inline static int32_t get_offset_of_mUnregisterOnDestroy_15() { return static_cast<int32_t>(offsetof(VirtualButtonAbstractBehaviour_t1965820348, ___mUnregisterOnDestroy_15)); }
	inline bool get_mUnregisterOnDestroy_15() const { return ___mUnregisterOnDestroy_15; }
	inline bool* get_address_of_mUnregisterOnDestroy_15() { return &___mUnregisterOnDestroy_15; }
	inline void set_mUnregisterOnDestroy_15(bool value)
	{
		___mUnregisterOnDestroy_15 = value;
	}

	inline static int32_t get_offset_of_mVirtualButton_16() { return static_cast<int32_t>(offsetof(VirtualButtonAbstractBehaviour_t1965820348, ___mVirtualButton_16)); }
	inline VirtualButton_t3666937711 * get_mVirtualButton_16() const { return ___mVirtualButton_16; }
	inline VirtualButton_t3666937711 ** get_address_of_mVirtualButton_16() { return &___mVirtualButton_16; }
	inline void set_mVirtualButton_16(VirtualButton_t3666937711 * value)
	{
		___mVirtualButton_16 = value;
		Il2CppCodeGenWriteBarrier((&___mVirtualButton_16), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VIRTUALBUTTONABSTRACTBEHAVIOUR_T1965820348_H
#ifndef USERDEFINEDTARGETBUILDINGABSTRACTBEHAVIOUR_T2424076393_H
#define USERDEFINEDTARGETBUILDINGABSTRACTBEHAVIOUR_T2424076393_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.UserDefinedTargetBuildingAbstractBehaviour
struct  UserDefinedTargetBuildingAbstractBehaviour_t2424076393  : public MonoBehaviour_t345688271
{
public:
	// Vuforia.ObjectTracker Vuforia.UserDefinedTargetBuildingAbstractBehaviour::mObjectTracker
	ObjectTracker_t4023638979 * ___mObjectTracker_2;
	// Vuforia.ImageTargetBuilder/FrameQuality Vuforia.UserDefinedTargetBuildingAbstractBehaviour::mLastFrameQuality
	int32_t ___mLastFrameQuality_3;
	// System.Boolean Vuforia.UserDefinedTargetBuildingAbstractBehaviour::mCurrentlyScanning
	bool ___mCurrentlyScanning_4;
	// System.Boolean Vuforia.UserDefinedTargetBuildingAbstractBehaviour::mWasScanningBeforeDisable
	bool ___mWasScanningBeforeDisable_5;
	// System.Boolean Vuforia.UserDefinedTargetBuildingAbstractBehaviour::mCurrentlyBuilding
	bool ___mCurrentlyBuilding_6;
	// System.Boolean Vuforia.UserDefinedTargetBuildingAbstractBehaviour::mWasBuildingBeforeDisable
	bool ___mWasBuildingBeforeDisable_7;
	// System.Boolean Vuforia.UserDefinedTargetBuildingAbstractBehaviour::mOnInitializedCalled
	bool ___mOnInitializedCalled_8;
	// System.Collections.Generic.List`1<Vuforia.IUserDefinedTargetEventHandler> Vuforia.UserDefinedTargetBuildingAbstractBehaviour::mHandlers
	List_1_t4290827367 * ___mHandlers_9;
	// System.Boolean Vuforia.UserDefinedTargetBuildingAbstractBehaviour::StopTrackerWhileScanning
	bool ___StopTrackerWhileScanning_10;
	// System.Boolean Vuforia.UserDefinedTargetBuildingAbstractBehaviour::StartScanningAutomatically
	bool ___StartScanningAutomatically_11;
	// System.Boolean Vuforia.UserDefinedTargetBuildingAbstractBehaviour::StopScanningWhenFinshedBuilding
	bool ___StopScanningWhenFinshedBuilding_12;

public:
	inline static int32_t get_offset_of_mObjectTracker_2() { return static_cast<int32_t>(offsetof(UserDefinedTargetBuildingAbstractBehaviour_t2424076393, ___mObjectTracker_2)); }
	inline ObjectTracker_t4023638979 * get_mObjectTracker_2() const { return ___mObjectTracker_2; }
	inline ObjectTracker_t4023638979 ** get_address_of_mObjectTracker_2() { return &___mObjectTracker_2; }
	inline void set_mObjectTracker_2(ObjectTracker_t4023638979 * value)
	{
		___mObjectTracker_2 = value;
		Il2CppCodeGenWriteBarrier((&___mObjectTracker_2), value);
	}

	inline static int32_t get_offset_of_mLastFrameQuality_3() { return static_cast<int32_t>(offsetof(UserDefinedTargetBuildingAbstractBehaviour_t2424076393, ___mLastFrameQuality_3)); }
	inline int32_t get_mLastFrameQuality_3() const { return ___mLastFrameQuality_3; }
	inline int32_t* get_address_of_mLastFrameQuality_3() { return &___mLastFrameQuality_3; }
	inline void set_mLastFrameQuality_3(int32_t value)
	{
		___mLastFrameQuality_3 = value;
	}

	inline static int32_t get_offset_of_mCurrentlyScanning_4() { return static_cast<int32_t>(offsetof(UserDefinedTargetBuildingAbstractBehaviour_t2424076393, ___mCurrentlyScanning_4)); }
	inline bool get_mCurrentlyScanning_4() const { return ___mCurrentlyScanning_4; }
	inline bool* get_address_of_mCurrentlyScanning_4() { return &___mCurrentlyScanning_4; }
	inline void set_mCurrentlyScanning_4(bool value)
	{
		___mCurrentlyScanning_4 = value;
	}

	inline static int32_t get_offset_of_mWasScanningBeforeDisable_5() { return static_cast<int32_t>(offsetof(UserDefinedTargetBuildingAbstractBehaviour_t2424076393, ___mWasScanningBeforeDisable_5)); }
	inline bool get_mWasScanningBeforeDisable_5() const { return ___mWasScanningBeforeDisable_5; }
	inline bool* get_address_of_mWasScanningBeforeDisable_5() { return &___mWasScanningBeforeDisable_5; }
	inline void set_mWasScanningBeforeDisable_5(bool value)
	{
		___mWasScanningBeforeDisable_5 = value;
	}

	inline static int32_t get_offset_of_mCurrentlyBuilding_6() { return static_cast<int32_t>(offsetof(UserDefinedTargetBuildingAbstractBehaviour_t2424076393, ___mCurrentlyBuilding_6)); }
	inline bool get_mCurrentlyBuilding_6() const { return ___mCurrentlyBuilding_6; }
	inline bool* get_address_of_mCurrentlyBuilding_6() { return &___mCurrentlyBuilding_6; }
	inline void set_mCurrentlyBuilding_6(bool value)
	{
		___mCurrentlyBuilding_6 = value;
	}

	inline static int32_t get_offset_of_mWasBuildingBeforeDisable_7() { return static_cast<int32_t>(offsetof(UserDefinedTargetBuildingAbstractBehaviour_t2424076393, ___mWasBuildingBeforeDisable_7)); }
	inline bool get_mWasBuildingBeforeDisable_7() const { return ___mWasBuildingBeforeDisable_7; }
	inline bool* get_address_of_mWasBuildingBeforeDisable_7() { return &___mWasBuildingBeforeDisable_7; }
	inline void set_mWasBuildingBeforeDisable_7(bool value)
	{
		___mWasBuildingBeforeDisable_7 = value;
	}

	inline static int32_t get_offset_of_mOnInitializedCalled_8() { return static_cast<int32_t>(offsetof(UserDefinedTargetBuildingAbstractBehaviour_t2424076393, ___mOnInitializedCalled_8)); }
	inline bool get_mOnInitializedCalled_8() const { return ___mOnInitializedCalled_8; }
	inline bool* get_address_of_mOnInitializedCalled_8() { return &___mOnInitializedCalled_8; }
	inline void set_mOnInitializedCalled_8(bool value)
	{
		___mOnInitializedCalled_8 = value;
	}

	inline static int32_t get_offset_of_mHandlers_9() { return static_cast<int32_t>(offsetof(UserDefinedTargetBuildingAbstractBehaviour_t2424076393, ___mHandlers_9)); }
	inline List_1_t4290827367 * get_mHandlers_9() const { return ___mHandlers_9; }
	inline List_1_t4290827367 ** get_address_of_mHandlers_9() { return &___mHandlers_9; }
	inline void set_mHandlers_9(List_1_t4290827367 * value)
	{
		___mHandlers_9 = value;
		Il2CppCodeGenWriteBarrier((&___mHandlers_9), value);
	}

	inline static int32_t get_offset_of_StopTrackerWhileScanning_10() { return static_cast<int32_t>(offsetof(UserDefinedTargetBuildingAbstractBehaviour_t2424076393, ___StopTrackerWhileScanning_10)); }
	inline bool get_StopTrackerWhileScanning_10() const { return ___StopTrackerWhileScanning_10; }
	inline bool* get_address_of_StopTrackerWhileScanning_10() { return &___StopTrackerWhileScanning_10; }
	inline void set_StopTrackerWhileScanning_10(bool value)
	{
		___StopTrackerWhileScanning_10 = value;
	}

	inline static int32_t get_offset_of_StartScanningAutomatically_11() { return static_cast<int32_t>(offsetof(UserDefinedTargetBuildingAbstractBehaviour_t2424076393, ___StartScanningAutomatically_11)); }
	inline bool get_StartScanningAutomatically_11() const { return ___StartScanningAutomatically_11; }
	inline bool* get_address_of_StartScanningAutomatically_11() { return &___StartScanningAutomatically_11; }
	inline void set_StartScanningAutomatically_11(bool value)
	{
		___StartScanningAutomatically_11 = value;
	}

	inline static int32_t get_offset_of_StopScanningWhenFinshedBuilding_12() { return static_cast<int32_t>(offsetof(UserDefinedTargetBuildingAbstractBehaviour_t2424076393, ___StopScanningWhenFinshedBuilding_12)); }
	inline bool get_StopScanningWhenFinshedBuilding_12() const { return ___StopScanningWhenFinshedBuilding_12; }
	inline bool* get_address_of_StopScanningWhenFinshedBuilding_12() { return &___StopScanningWhenFinshedBuilding_12; }
	inline void set_StopScanningWhenFinshedBuilding_12(bool value)
	{
		___StopScanningWhenFinshedBuilding_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // USERDEFINEDTARGETBUILDINGABSTRACTBEHAVIOUR_T2424076393_H
#ifndef DEFAULTINITIALIZATIONERRORHANDLER_T414875900_H
#define DEFAULTINITIALIZATIONERRORHANDLER_T414875900_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.DefaultInitializationErrorHandler
struct  DefaultInitializationErrorHandler_t414875900  : public MonoBehaviour_t345688271
{
public:
	// System.String Vuforia.DefaultInitializationErrorHandler::mErrorText
	String_t* ___mErrorText_2;
	// System.Boolean Vuforia.DefaultInitializationErrorHandler::mErrorOccurred
	bool ___mErrorOccurred_3;

public:
	inline static int32_t get_offset_of_mErrorText_2() { return static_cast<int32_t>(offsetof(DefaultInitializationErrorHandler_t414875900, ___mErrorText_2)); }
	inline String_t* get_mErrorText_2() const { return ___mErrorText_2; }
	inline String_t** get_address_of_mErrorText_2() { return &___mErrorText_2; }
	inline void set_mErrorText_2(String_t* value)
	{
		___mErrorText_2 = value;
		Il2CppCodeGenWriteBarrier((&___mErrorText_2), value);
	}

	inline static int32_t get_offset_of_mErrorOccurred_3() { return static_cast<int32_t>(offsetof(DefaultInitializationErrorHandler_t414875900, ___mErrorOccurred_3)); }
	inline bool get_mErrorOccurred_3() const { return ___mErrorOccurred_3; }
	inline bool* get_address_of_mErrorOccurred_3() { return &___mErrorOccurred_3; }
	inline void set_mErrorOccurred_3(bool value)
	{
		___mErrorOccurred_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFAULTINITIALIZATIONERRORHANDLER_T414875900_H
#ifndef VIDEOBACKGROUNDABSTRACTBEHAVIOUR_T3867888217_H
#define VIDEOBACKGROUNDABSTRACTBEHAVIOUR_T3867888217_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.VideoBackgroundAbstractBehaviour
struct  VideoBackgroundAbstractBehaviour_t3867888217  : public MonoBehaviour_t345688271
{
public:
	// System.Int32 Vuforia.VideoBackgroundAbstractBehaviour::mClearBuffers
	int32_t ___mClearBuffers_2;
	// System.Int32 Vuforia.VideoBackgroundAbstractBehaviour::mSkipStateUpdates
	int32_t ___mSkipStateUpdates_3;
	// Vuforia.VuforiaARController Vuforia.VideoBackgroundAbstractBehaviour::mVuforiaARController
	VuforiaARController_t1328503142 * ___mVuforiaARController_4;
	// UnityEngine.Camera Vuforia.VideoBackgroundAbstractBehaviour::mCamera
	Camera_t3175186167 * ___mCamera_5;
	// Vuforia.BackgroundPlaneAbstractBehaviour Vuforia.VideoBackgroundAbstractBehaviour::mBackgroundBehaviour
	BackgroundPlaneAbstractBehaviour_t4259835756 * ___mBackgroundBehaviour_6;
	// System.Single Vuforia.VideoBackgroundAbstractBehaviour::mStereoDepth
	float ___mStereoDepth_7;
	// System.Boolean Vuforia.VideoBackgroundAbstractBehaviour::mResetMatrix
	bool ___mResetMatrix_10;
	// UnityEngine.Vector2 Vuforia.VideoBackgroundAbstractBehaviour::mVuforiaFrustumSkew
	Vector2_t3577333262  ___mVuforiaFrustumSkew_11;
	// UnityEngine.Vector2 Vuforia.VideoBackgroundAbstractBehaviour::mCenterToEyeAxis
	Vector2_t3577333262  ___mCenterToEyeAxis_12;
	// System.Collections.Generic.HashSet`1<UnityEngine.MeshRenderer> Vuforia.VideoBackgroundAbstractBehaviour::mDisabledMeshRenderers
	HashSet_1_t1919463348 * ___mDisabledMeshRenderers_13;

public:
	inline static int32_t get_offset_of_mClearBuffers_2() { return static_cast<int32_t>(offsetof(VideoBackgroundAbstractBehaviour_t3867888217, ___mClearBuffers_2)); }
	inline int32_t get_mClearBuffers_2() const { return ___mClearBuffers_2; }
	inline int32_t* get_address_of_mClearBuffers_2() { return &___mClearBuffers_2; }
	inline void set_mClearBuffers_2(int32_t value)
	{
		___mClearBuffers_2 = value;
	}

	inline static int32_t get_offset_of_mSkipStateUpdates_3() { return static_cast<int32_t>(offsetof(VideoBackgroundAbstractBehaviour_t3867888217, ___mSkipStateUpdates_3)); }
	inline int32_t get_mSkipStateUpdates_3() const { return ___mSkipStateUpdates_3; }
	inline int32_t* get_address_of_mSkipStateUpdates_3() { return &___mSkipStateUpdates_3; }
	inline void set_mSkipStateUpdates_3(int32_t value)
	{
		___mSkipStateUpdates_3 = value;
	}

	inline static int32_t get_offset_of_mVuforiaARController_4() { return static_cast<int32_t>(offsetof(VideoBackgroundAbstractBehaviour_t3867888217, ___mVuforiaARController_4)); }
	inline VuforiaARController_t1328503142 * get_mVuforiaARController_4() const { return ___mVuforiaARController_4; }
	inline VuforiaARController_t1328503142 ** get_address_of_mVuforiaARController_4() { return &___mVuforiaARController_4; }
	inline void set_mVuforiaARController_4(VuforiaARController_t1328503142 * value)
	{
		___mVuforiaARController_4 = value;
		Il2CppCodeGenWriteBarrier((&___mVuforiaARController_4), value);
	}

	inline static int32_t get_offset_of_mCamera_5() { return static_cast<int32_t>(offsetof(VideoBackgroundAbstractBehaviour_t3867888217, ___mCamera_5)); }
	inline Camera_t3175186167 * get_mCamera_5() const { return ___mCamera_5; }
	inline Camera_t3175186167 ** get_address_of_mCamera_5() { return &___mCamera_5; }
	inline void set_mCamera_5(Camera_t3175186167 * value)
	{
		___mCamera_5 = value;
		Il2CppCodeGenWriteBarrier((&___mCamera_5), value);
	}

	inline static int32_t get_offset_of_mBackgroundBehaviour_6() { return static_cast<int32_t>(offsetof(VideoBackgroundAbstractBehaviour_t3867888217, ___mBackgroundBehaviour_6)); }
	inline BackgroundPlaneAbstractBehaviour_t4259835756 * get_mBackgroundBehaviour_6() const { return ___mBackgroundBehaviour_6; }
	inline BackgroundPlaneAbstractBehaviour_t4259835756 ** get_address_of_mBackgroundBehaviour_6() { return &___mBackgroundBehaviour_6; }
	inline void set_mBackgroundBehaviour_6(BackgroundPlaneAbstractBehaviour_t4259835756 * value)
	{
		___mBackgroundBehaviour_6 = value;
		Il2CppCodeGenWriteBarrier((&___mBackgroundBehaviour_6), value);
	}

	inline static int32_t get_offset_of_mStereoDepth_7() { return static_cast<int32_t>(offsetof(VideoBackgroundAbstractBehaviour_t3867888217, ___mStereoDepth_7)); }
	inline float get_mStereoDepth_7() const { return ___mStereoDepth_7; }
	inline float* get_address_of_mStereoDepth_7() { return &___mStereoDepth_7; }
	inline void set_mStereoDepth_7(float value)
	{
		___mStereoDepth_7 = value;
	}

	inline static int32_t get_offset_of_mResetMatrix_10() { return static_cast<int32_t>(offsetof(VideoBackgroundAbstractBehaviour_t3867888217, ___mResetMatrix_10)); }
	inline bool get_mResetMatrix_10() const { return ___mResetMatrix_10; }
	inline bool* get_address_of_mResetMatrix_10() { return &___mResetMatrix_10; }
	inline void set_mResetMatrix_10(bool value)
	{
		___mResetMatrix_10 = value;
	}

	inline static int32_t get_offset_of_mVuforiaFrustumSkew_11() { return static_cast<int32_t>(offsetof(VideoBackgroundAbstractBehaviour_t3867888217, ___mVuforiaFrustumSkew_11)); }
	inline Vector2_t3577333262  get_mVuforiaFrustumSkew_11() const { return ___mVuforiaFrustumSkew_11; }
	inline Vector2_t3577333262 * get_address_of_mVuforiaFrustumSkew_11() { return &___mVuforiaFrustumSkew_11; }
	inline void set_mVuforiaFrustumSkew_11(Vector2_t3577333262  value)
	{
		___mVuforiaFrustumSkew_11 = value;
	}

	inline static int32_t get_offset_of_mCenterToEyeAxis_12() { return static_cast<int32_t>(offsetof(VideoBackgroundAbstractBehaviour_t3867888217, ___mCenterToEyeAxis_12)); }
	inline Vector2_t3577333262  get_mCenterToEyeAxis_12() const { return ___mCenterToEyeAxis_12; }
	inline Vector2_t3577333262 * get_address_of_mCenterToEyeAxis_12() { return &___mCenterToEyeAxis_12; }
	inline void set_mCenterToEyeAxis_12(Vector2_t3577333262  value)
	{
		___mCenterToEyeAxis_12 = value;
	}

	inline static int32_t get_offset_of_mDisabledMeshRenderers_13() { return static_cast<int32_t>(offsetof(VideoBackgroundAbstractBehaviour_t3867888217, ___mDisabledMeshRenderers_13)); }
	inline HashSet_1_t1919463348 * get_mDisabledMeshRenderers_13() const { return ___mDisabledMeshRenderers_13; }
	inline HashSet_1_t1919463348 ** get_address_of_mDisabledMeshRenderers_13() { return &___mDisabledMeshRenderers_13; }
	inline void set_mDisabledMeshRenderers_13(HashSet_1_t1919463348 * value)
	{
		___mDisabledMeshRenderers_13 = value;
		Il2CppCodeGenWriteBarrier((&___mDisabledMeshRenderers_13), value);
	}
};

struct VideoBackgroundAbstractBehaviour_t3867888217_StaticFields
{
public:
	// System.Int32 Vuforia.VideoBackgroundAbstractBehaviour::mFrameCounter
	int32_t ___mFrameCounter_8;
	// System.Int32 Vuforia.VideoBackgroundAbstractBehaviour::mRenderCounter
	int32_t ___mRenderCounter_9;

public:
	inline static int32_t get_offset_of_mFrameCounter_8() { return static_cast<int32_t>(offsetof(VideoBackgroundAbstractBehaviour_t3867888217_StaticFields, ___mFrameCounter_8)); }
	inline int32_t get_mFrameCounter_8() const { return ___mFrameCounter_8; }
	inline int32_t* get_address_of_mFrameCounter_8() { return &___mFrameCounter_8; }
	inline void set_mFrameCounter_8(int32_t value)
	{
		___mFrameCounter_8 = value;
	}

	inline static int32_t get_offset_of_mRenderCounter_9() { return static_cast<int32_t>(offsetof(VideoBackgroundAbstractBehaviour_t3867888217_StaticFields, ___mRenderCounter_9)); }
	inline int32_t get_mRenderCounter_9() const { return ___mRenderCounter_9; }
	inline int32_t* get_address_of_mRenderCounter_9() { return &___mRenderCounter_9; }
	inline void set_mRenderCounter_9(int32_t value)
	{
		___mRenderCounter_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VIDEOBACKGROUNDABSTRACTBEHAVIOUR_T3867888217_H
#ifndef HIDEEXCESSAREAABSTRACTBEHAVIOUR_T1324413388_H
#define HIDEEXCESSAREAABSTRACTBEHAVIOUR_T1324413388_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.HideExcessAreaAbstractBehaviour
struct  HideExcessAreaAbstractBehaviour_t1324413388  : public MonoBehaviour_t345688271
{
public:
	// Vuforia.IExcessAreaClipping Vuforia.HideExcessAreaAbstractBehaviour::mClippingImpl
	RuntimeObject* ___mClippingImpl_2;
	// Vuforia.HideExcessAreaAbstractBehaviour/CLIPPING_MODE Vuforia.HideExcessAreaAbstractBehaviour::mClippingMode
	int32_t ___mClippingMode_3;
	// Vuforia.VuforiaARController Vuforia.HideExcessAreaAbstractBehaviour::mVuforiaBehaviour
	VuforiaARController_t1328503142 * ___mVuforiaBehaviour_4;
	// Vuforia.VideoBackgroundManager Vuforia.HideExcessAreaAbstractBehaviour::mVideoBgMgr
	VideoBackgroundManager_t2068552549 * ___mVideoBgMgr_5;
	// UnityEngine.Vector3 Vuforia.HideExcessAreaAbstractBehaviour::mPlaneOffset
	Vector3_t2903530434  ___mPlaneOffset_6;
	// System.Boolean Vuforia.HideExcessAreaAbstractBehaviour::mSceneScaledDown
	bool ___mSceneScaledDown_7;
	// System.Boolean Vuforia.HideExcessAreaAbstractBehaviour::mStarted
	bool ___mStarted_8;

public:
	inline static int32_t get_offset_of_mClippingImpl_2() { return static_cast<int32_t>(offsetof(HideExcessAreaAbstractBehaviour_t1324413388, ___mClippingImpl_2)); }
	inline RuntimeObject* get_mClippingImpl_2() const { return ___mClippingImpl_2; }
	inline RuntimeObject** get_address_of_mClippingImpl_2() { return &___mClippingImpl_2; }
	inline void set_mClippingImpl_2(RuntimeObject* value)
	{
		___mClippingImpl_2 = value;
		Il2CppCodeGenWriteBarrier((&___mClippingImpl_2), value);
	}

	inline static int32_t get_offset_of_mClippingMode_3() { return static_cast<int32_t>(offsetof(HideExcessAreaAbstractBehaviour_t1324413388, ___mClippingMode_3)); }
	inline int32_t get_mClippingMode_3() const { return ___mClippingMode_3; }
	inline int32_t* get_address_of_mClippingMode_3() { return &___mClippingMode_3; }
	inline void set_mClippingMode_3(int32_t value)
	{
		___mClippingMode_3 = value;
	}

	inline static int32_t get_offset_of_mVuforiaBehaviour_4() { return static_cast<int32_t>(offsetof(HideExcessAreaAbstractBehaviour_t1324413388, ___mVuforiaBehaviour_4)); }
	inline VuforiaARController_t1328503142 * get_mVuforiaBehaviour_4() const { return ___mVuforiaBehaviour_4; }
	inline VuforiaARController_t1328503142 ** get_address_of_mVuforiaBehaviour_4() { return &___mVuforiaBehaviour_4; }
	inline void set_mVuforiaBehaviour_4(VuforiaARController_t1328503142 * value)
	{
		___mVuforiaBehaviour_4 = value;
		Il2CppCodeGenWriteBarrier((&___mVuforiaBehaviour_4), value);
	}

	inline static int32_t get_offset_of_mVideoBgMgr_5() { return static_cast<int32_t>(offsetof(HideExcessAreaAbstractBehaviour_t1324413388, ___mVideoBgMgr_5)); }
	inline VideoBackgroundManager_t2068552549 * get_mVideoBgMgr_5() const { return ___mVideoBgMgr_5; }
	inline VideoBackgroundManager_t2068552549 ** get_address_of_mVideoBgMgr_5() { return &___mVideoBgMgr_5; }
	inline void set_mVideoBgMgr_5(VideoBackgroundManager_t2068552549 * value)
	{
		___mVideoBgMgr_5 = value;
		Il2CppCodeGenWriteBarrier((&___mVideoBgMgr_5), value);
	}

	inline static int32_t get_offset_of_mPlaneOffset_6() { return static_cast<int32_t>(offsetof(HideExcessAreaAbstractBehaviour_t1324413388, ___mPlaneOffset_6)); }
	inline Vector3_t2903530434  get_mPlaneOffset_6() const { return ___mPlaneOffset_6; }
	inline Vector3_t2903530434 * get_address_of_mPlaneOffset_6() { return &___mPlaneOffset_6; }
	inline void set_mPlaneOffset_6(Vector3_t2903530434  value)
	{
		___mPlaneOffset_6 = value;
	}

	inline static int32_t get_offset_of_mSceneScaledDown_7() { return static_cast<int32_t>(offsetof(HideExcessAreaAbstractBehaviour_t1324413388, ___mSceneScaledDown_7)); }
	inline bool get_mSceneScaledDown_7() const { return ___mSceneScaledDown_7; }
	inline bool* get_address_of_mSceneScaledDown_7() { return &___mSceneScaledDown_7; }
	inline void set_mSceneScaledDown_7(bool value)
	{
		___mSceneScaledDown_7 = value;
	}

	inline static int32_t get_offset_of_mStarted_8() { return static_cast<int32_t>(offsetof(HideExcessAreaAbstractBehaviour_t1324413388, ___mStarted_8)); }
	inline bool get_mStarted_8() const { return ___mStarted_8; }
	inline bool* get_address_of_mStarted_8() { return &___mStarted_8; }
	inline void set_mStarted_8(bool value)
	{
		___mStarted_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HIDEEXCESSAREAABSTRACTBEHAVIOUR_T1324413388_H
#ifndef COMPONENTFACTORYSTARTERBEHAVIOUR_T2906678938_H
#define COMPONENTFACTORYSTARTERBEHAVIOUR_T2906678938_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.ComponentFactoryStarterBehaviour
struct  ComponentFactoryStarterBehaviour_t2906678938  : public MonoBehaviour_t345688271
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENTFACTORYSTARTERBEHAVIOUR_T2906678938_H
#ifndef CLOUDRECOABSTRACTBEHAVIOUR_T2501456771_H
#define CLOUDRECOABSTRACTBEHAVIOUR_T2501456771_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.CloudRecoAbstractBehaviour
struct  CloudRecoAbstractBehaviour_t2501456771  : public MonoBehaviour_t345688271
{
public:
	// Vuforia.ObjectTracker Vuforia.CloudRecoAbstractBehaviour::mObjectTracker
	ObjectTracker_t4023638979 * ___mObjectTracker_2;
	// System.Boolean Vuforia.CloudRecoAbstractBehaviour::mCurrentlyInitializing
	bool ___mCurrentlyInitializing_3;
	// System.Boolean Vuforia.CloudRecoAbstractBehaviour::mInitSuccess
	bool ___mInitSuccess_4;
	// System.Boolean Vuforia.CloudRecoAbstractBehaviour::mCloudRecoStarted
	bool ___mCloudRecoStarted_5;
	// System.Boolean Vuforia.CloudRecoAbstractBehaviour::mOnInitializedCalled
	bool ___mOnInitializedCalled_6;
	// System.Collections.Generic.List`1<Vuforia.ICloudRecoEventHandler> Vuforia.CloudRecoAbstractBehaviour::mHandlers
	List_1_t1465274546 * ___mHandlers_7;
	// System.Boolean Vuforia.CloudRecoAbstractBehaviour::mTargetFinderStartedBeforeDisable
	bool ___mTargetFinderStartedBeforeDisable_8;
	// System.String Vuforia.CloudRecoAbstractBehaviour::AccessKey
	String_t* ___AccessKey_9;
	// System.String Vuforia.CloudRecoAbstractBehaviour::SecretKey
	String_t* ___SecretKey_10;

public:
	inline static int32_t get_offset_of_mObjectTracker_2() { return static_cast<int32_t>(offsetof(CloudRecoAbstractBehaviour_t2501456771, ___mObjectTracker_2)); }
	inline ObjectTracker_t4023638979 * get_mObjectTracker_2() const { return ___mObjectTracker_2; }
	inline ObjectTracker_t4023638979 ** get_address_of_mObjectTracker_2() { return &___mObjectTracker_2; }
	inline void set_mObjectTracker_2(ObjectTracker_t4023638979 * value)
	{
		___mObjectTracker_2 = value;
		Il2CppCodeGenWriteBarrier((&___mObjectTracker_2), value);
	}

	inline static int32_t get_offset_of_mCurrentlyInitializing_3() { return static_cast<int32_t>(offsetof(CloudRecoAbstractBehaviour_t2501456771, ___mCurrentlyInitializing_3)); }
	inline bool get_mCurrentlyInitializing_3() const { return ___mCurrentlyInitializing_3; }
	inline bool* get_address_of_mCurrentlyInitializing_3() { return &___mCurrentlyInitializing_3; }
	inline void set_mCurrentlyInitializing_3(bool value)
	{
		___mCurrentlyInitializing_3 = value;
	}

	inline static int32_t get_offset_of_mInitSuccess_4() { return static_cast<int32_t>(offsetof(CloudRecoAbstractBehaviour_t2501456771, ___mInitSuccess_4)); }
	inline bool get_mInitSuccess_4() const { return ___mInitSuccess_4; }
	inline bool* get_address_of_mInitSuccess_4() { return &___mInitSuccess_4; }
	inline void set_mInitSuccess_4(bool value)
	{
		___mInitSuccess_4 = value;
	}

	inline static int32_t get_offset_of_mCloudRecoStarted_5() { return static_cast<int32_t>(offsetof(CloudRecoAbstractBehaviour_t2501456771, ___mCloudRecoStarted_5)); }
	inline bool get_mCloudRecoStarted_5() const { return ___mCloudRecoStarted_5; }
	inline bool* get_address_of_mCloudRecoStarted_5() { return &___mCloudRecoStarted_5; }
	inline void set_mCloudRecoStarted_5(bool value)
	{
		___mCloudRecoStarted_5 = value;
	}

	inline static int32_t get_offset_of_mOnInitializedCalled_6() { return static_cast<int32_t>(offsetof(CloudRecoAbstractBehaviour_t2501456771, ___mOnInitializedCalled_6)); }
	inline bool get_mOnInitializedCalled_6() const { return ___mOnInitializedCalled_6; }
	inline bool* get_address_of_mOnInitializedCalled_6() { return &___mOnInitializedCalled_6; }
	inline void set_mOnInitializedCalled_6(bool value)
	{
		___mOnInitializedCalled_6 = value;
	}

	inline static int32_t get_offset_of_mHandlers_7() { return static_cast<int32_t>(offsetof(CloudRecoAbstractBehaviour_t2501456771, ___mHandlers_7)); }
	inline List_1_t1465274546 * get_mHandlers_7() const { return ___mHandlers_7; }
	inline List_1_t1465274546 ** get_address_of_mHandlers_7() { return &___mHandlers_7; }
	inline void set_mHandlers_7(List_1_t1465274546 * value)
	{
		___mHandlers_7 = value;
		Il2CppCodeGenWriteBarrier((&___mHandlers_7), value);
	}

	inline static int32_t get_offset_of_mTargetFinderStartedBeforeDisable_8() { return static_cast<int32_t>(offsetof(CloudRecoAbstractBehaviour_t2501456771, ___mTargetFinderStartedBeforeDisable_8)); }
	inline bool get_mTargetFinderStartedBeforeDisable_8() const { return ___mTargetFinderStartedBeforeDisable_8; }
	inline bool* get_address_of_mTargetFinderStartedBeforeDisable_8() { return &___mTargetFinderStartedBeforeDisable_8; }
	inline void set_mTargetFinderStartedBeforeDisable_8(bool value)
	{
		___mTargetFinderStartedBeforeDisable_8 = value;
	}

	inline static int32_t get_offset_of_AccessKey_9() { return static_cast<int32_t>(offsetof(CloudRecoAbstractBehaviour_t2501456771, ___AccessKey_9)); }
	inline String_t* get_AccessKey_9() const { return ___AccessKey_9; }
	inline String_t** get_address_of_AccessKey_9() { return &___AccessKey_9; }
	inline void set_AccessKey_9(String_t* value)
	{
		___AccessKey_9 = value;
		Il2CppCodeGenWriteBarrier((&___AccessKey_9), value);
	}

	inline static int32_t get_offset_of_SecretKey_10() { return static_cast<int32_t>(offsetof(CloudRecoAbstractBehaviour_t2501456771, ___SecretKey_10)); }
	inline String_t* get_SecretKey_10() const { return ___SecretKey_10; }
	inline String_t** get_address_of_SecretKey_10() { return &___SecretKey_10; }
	inline void set_SecretKey_10(String_t* value)
	{
		___SecretKey_10 = value;
		Il2CppCodeGenWriteBarrier((&___SecretKey_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CLOUDRECOABSTRACTBEHAVIOUR_T2501456771_H
#ifndef BACKGROUNDPLANEABSTRACTBEHAVIOUR_T4259835756_H
#define BACKGROUNDPLANEABSTRACTBEHAVIOUR_T4259835756_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.BackgroundPlaneAbstractBehaviour
struct  BackgroundPlaneAbstractBehaviour_t4259835756  : public MonoBehaviour_t345688271
{
public:
	// Vuforia.VuforiaRenderer/VideoTextureInfo Vuforia.BackgroundPlaneAbstractBehaviour::mTextureInfo
	VideoTextureInfo_t773013061  ___mTextureInfo_2;
	// System.Int32 Vuforia.BackgroundPlaneAbstractBehaviour::mViewWidth
	int32_t ___mViewWidth_3;
	// System.Int32 Vuforia.BackgroundPlaneAbstractBehaviour::mViewHeight
	int32_t ___mViewHeight_4;
	// System.Int32 Vuforia.BackgroundPlaneAbstractBehaviour::mNumFramesToUpdateVideoBg
	int32_t ___mNumFramesToUpdateVideoBg_5;
	// UnityEngine.Camera Vuforia.BackgroundPlaneAbstractBehaviour::mCamera
	Camera_t3175186167 * ___mCamera_6;
	// System.Int32 Vuforia.BackgroundPlaneAbstractBehaviour::defaultNumDivisions
	int32_t ___defaultNumDivisions_8;
	// UnityEngine.Mesh Vuforia.BackgroundPlaneAbstractBehaviour::mMesh
	Mesh_t996500909 * ___mMesh_9;
	// System.Single Vuforia.BackgroundPlaneAbstractBehaviour::mStereoDepth
	float ___mStereoDepth_10;
	// UnityEngine.Vector3 Vuforia.BackgroundPlaneAbstractBehaviour::mBackgroundOffset
	Vector3_t2903530434  ___mBackgroundOffset_11;
	// Vuforia.VuforiaARController Vuforia.BackgroundPlaneAbstractBehaviour::mVuforiaBehaviour
	VuforiaARController_t1328503142 * ___mVuforiaBehaviour_12;
	// System.Action Vuforia.BackgroundPlaneAbstractBehaviour::mBackgroundPlacedCallback
	Action_t3619184611 * ___mBackgroundPlacedCallback_13;
	// System.Int32 Vuforia.BackgroundPlaneAbstractBehaviour::mNumDivisions
	int32_t ___mNumDivisions_14;

public:
	inline static int32_t get_offset_of_mTextureInfo_2() { return static_cast<int32_t>(offsetof(BackgroundPlaneAbstractBehaviour_t4259835756, ___mTextureInfo_2)); }
	inline VideoTextureInfo_t773013061  get_mTextureInfo_2() const { return ___mTextureInfo_2; }
	inline VideoTextureInfo_t773013061 * get_address_of_mTextureInfo_2() { return &___mTextureInfo_2; }
	inline void set_mTextureInfo_2(VideoTextureInfo_t773013061  value)
	{
		___mTextureInfo_2 = value;
	}

	inline static int32_t get_offset_of_mViewWidth_3() { return static_cast<int32_t>(offsetof(BackgroundPlaneAbstractBehaviour_t4259835756, ___mViewWidth_3)); }
	inline int32_t get_mViewWidth_3() const { return ___mViewWidth_3; }
	inline int32_t* get_address_of_mViewWidth_3() { return &___mViewWidth_3; }
	inline void set_mViewWidth_3(int32_t value)
	{
		___mViewWidth_3 = value;
	}

	inline static int32_t get_offset_of_mViewHeight_4() { return static_cast<int32_t>(offsetof(BackgroundPlaneAbstractBehaviour_t4259835756, ___mViewHeight_4)); }
	inline int32_t get_mViewHeight_4() const { return ___mViewHeight_4; }
	inline int32_t* get_address_of_mViewHeight_4() { return &___mViewHeight_4; }
	inline void set_mViewHeight_4(int32_t value)
	{
		___mViewHeight_4 = value;
	}

	inline static int32_t get_offset_of_mNumFramesToUpdateVideoBg_5() { return static_cast<int32_t>(offsetof(BackgroundPlaneAbstractBehaviour_t4259835756, ___mNumFramesToUpdateVideoBg_5)); }
	inline int32_t get_mNumFramesToUpdateVideoBg_5() const { return ___mNumFramesToUpdateVideoBg_5; }
	inline int32_t* get_address_of_mNumFramesToUpdateVideoBg_5() { return &___mNumFramesToUpdateVideoBg_5; }
	inline void set_mNumFramesToUpdateVideoBg_5(int32_t value)
	{
		___mNumFramesToUpdateVideoBg_5 = value;
	}

	inline static int32_t get_offset_of_mCamera_6() { return static_cast<int32_t>(offsetof(BackgroundPlaneAbstractBehaviour_t4259835756, ___mCamera_6)); }
	inline Camera_t3175186167 * get_mCamera_6() const { return ___mCamera_6; }
	inline Camera_t3175186167 ** get_address_of_mCamera_6() { return &___mCamera_6; }
	inline void set_mCamera_6(Camera_t3175186167 * value)
	{
		___mCamera_6 = value;
		Il2CppCodeGenWriteBarrier((&___mCamera_6), value);
	}

	inline static int32_t get_offset_of_defaultNumDivisions_8() { return static_cast<int32_t>(offsetof(BackgroundPlaneAbstractBehaviour_t4259835756, ___defaultNumDivisions_8)); }
	inline int32_t get_defaultNumDivisions_8() const { return ___defaultNumDivisions_8; }
	inline int32_t* get_address_of_defaultNumDivisions_8() { return &___defaultNumDivisions_8; }
	inline void set_defaultNumDivisions_8(int32_t value)
	{
		___defaultNumDivisions_8 = value;
	}

	inline static int32_t get_offset_of_mMesh_9() { return static_cast<int32_t>(offsetof(BackgroundPlaneAbstractBehaviour_t4259835756, ___mMesh_9)); }
	inline Mesh_t996500909 * get_mMesh_9() const { return ___mMesh_9; }
	inline Mesh_t996500909 ** get_address_of_mMesh_9() { return &___mMesh_9; }
	inline void set_mMesh_9(Mesh_t996500909 * value)
	{
		___mMesh_9 = value;
		Il2CppCodeGenWriteBarrier((&___mMesh_9), value);
	}

	inline static int32_t get_offset_of_mStereoDepth_10() { return static_cast<int32_t>(offsetof(BackgroundPlaneAbstractBehaviour_t4259835756, ___mStereoDepth_10)); }
	inline float get_mStereoDepth_10() const { return ___mStereoDepth_10; }
	inline float* get_address_of_mStereoDepth_10() { return &___mStereoDepth_10; }
	inline void set_mStereoDepth_10(float value)
	{
		___mStereoDepth_10 = value;
	}

	inline static int32_t get_offset_of_mBackgroundOffset_11() { return static_cast<int32_t>(offsetof(BackgroundPlaneAbstractBehaviour_t4259835756, ___mBackgroundOffset_11)); }
	inline Vector3_t2903530434  get_mBackgroundOffset_11() const { return ___mBackgroundOffset_11; }
	inline Vector3_t2903530434 * get_address_of_mBackgroundOffset_11() { return &___mBackgroundOffset_11; }
	inline void set_mBackgroundOffset_11(Vector3_t2903530434  value)
	{
		___mBackgroundOffset_11 = value;
	}

	inline static int32_t get_offset_of_mVuforiaBehaviour_12() { return static_cast<int32_t>(offsetof(BackgroundPlaneAbstractBehaviour_t4259835756, ___mVuforiaBehaviour_12)); }
	inline VuforiaARController_t1328503142 * get_mVuforiaBehaviour_12() const { return ___mVuforiaBehaviour_12; }
	inline VuforiaARController_t1328503142 ** get_address_of_mVuforiaBehaviour_12() { return &___mVuforiaBehaviour_12; }
	inline void set_mVuforiaBehaviour_12(VuforiaARController_t1328503142 * value)
	{
		___mVuforiaBehaviour_12 = value;
		Il2CppCodeGenWriteBarrier((&___mVuforiaBehaviour_12), value);
	}

	inline static int32_t get_offset_of_mBackgroundPlacedCallback_13() { return static_cast<int32_t>(offsetof(BackgroundPlaneAbstractBehaviour_t4259835756, ___mBackgroundPlacedCallback_13)); }
	inline Action_t3619184611 * get_mBackgroundPlacedCallback_13() const { return ___mBackgroundPlacedCallback_13; }
	inline Action_t3619184611 ** get_address_of_mBackgroundPlacedCallback_13() { return &___mBackgroundPlacedCallback_13; }
	inline void set_mBackgroundPlacedCallback_13(Action_t3619184611 * value)
	{
		___mBackgroundPlacedCallback_13 = value;
		Il2CppCodeGenWriteBarrier((&___mBackgroundPlacedCallback_13), value);
	}

	inline static int32_t get_offset_of_mNumDivisions_14() { return static_cast<int32_t>(offsetof(BackgroundPlaneAbstractBehaviour_t4259835756, ___mNumDivisions_14)); }
	inline int32_t get_mNumDivisions_14() const { return ___mNumDivisions_14; }
	inline int32_t* get_address_of_mNumDivisions_14() { return &___mNumDivisions_14; }
	inline void set_mNumDivisions_14(int32_t value)
	{
		___mNumDivisions_14 = value;
	}
};

struct BackgroundPlaneAbstractBehaviour_t4259835756_StaticFields
{
public:
	// System.Single Vuforia.BackgroundPlaneAbstractBehaviour::maxDisplacement
	float ___maxDisplacement_7;

public:
	inline static int32_t get_offset_of_maxDisplacement_7() { return static_cast<int32_t>(offsetof(BackgroundPlaneAbstractBehaviour_t4259835756_StaticFields, ___maxDisplacement_7)); }
	inline float get_maxDisplacement_7() const { return ___maxDisplacement_7; }
	inline float* get_address_of_maxDisplacement_7() { return &___maxDisplacement_7; }
	inline void set_maxDisplacement_7(float value)
	{
		___maxDisplacement_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BACKGROUNDPLANEABSTRACTBEHAVIOUR_T4259835756_H
#ifndef DATASETTRACKABLEBEHAVIOUR_T3960979584_H
#define DATASETTRACKABLEBEHAVIOUR_T3960979584_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.DataSetTrackableBehaviour
struct  DataSetTrackableBehaviour_t3960979584  : public TrackableBehaviour_t2419079356
{
public:
	// System.String Vuforia.DataSetTrackableBehaviour::mDataSetPath
	String_t* ___mDataSetPath_10;
	// System.Boolean Vuforia.DataSetTrackableBehaviour::mExtendedTracking
	bool ___mExtendedTracking_11;
	// System.Boolean Vuforia.DataSetTrackableBehaviour::mInitializeSmartTerrain
	bool ___mInitializeSmartTerrain_12;
	// Vuforia.ReconstructionFromTargetAbstractBehaviour Vuforia.DataSetTrackableBehaviour::mReconstructionToInitialize
	ReconstructionFromTargetAbstractBehaviour_t2676804737 * ___mReconstructionToInitialize_13;
	// UnityEngine.Vector3 Vuforia.DataSetTrackableBehaviour::mSmartTerrainOccluderBoundsMin
	Vector3_t2903530434  ___mSmartTerrainOccluderBoundsMin_14;
	// UnityEngine.Vector3 Vuforia.DataSetTrackableBehaviour::mSmartTerrainOccluderBoundsMax
	Vector3_t2903530434  ___mSmartTerrainOccluderBoundsMax_15;
	// System.Boolean Vuforia.DataSetTrackableBehaviour::mIsSmartTerrainOccluderOffset
	bool ___mIsSmartTerrainOccluderOffset_16;
	// UnityEngine.Vector3 Vuforia.DataSetTrackableBehaviour::mSmartTerrainOccluderOffset
	Vector3_t2903530434  ___mSmartTerrainOccluderOffset_17;
	// UnityEngine.Quaternion Vuforia.DataSetTrackableBehaviour::mSmartTerrainOccluderRotation
	Quaternion_t754065749  ___mSmartTerrainOccluderRotation_18;
	// System.Boolean Vuforia.DataSetTrackableBehaviour::mAutoSetOccluderFromTargetSize
	bool ___mAutoSetOccluderFromTargetSize_19;

public:
	inline static int32_t get_offset_of_mDataSetPath_10() { return static_cast<int32_t>(offsetof(DataSetTrackableBehaviour_t3960979584, ___mDataSetPath_10)); }
	inline String_t* get_mDataSetPath_10() const { return ___mDataSetPath_10; }
	inline String_t** get_address_of_mDataSetPath_10() { return &___mDataSetPath_10; }
	inline void set_mDataSetPath_10(String_t* value)
	{
		___mDataSetPath_10 = value;
		Il2CppCodeGenWriteBarrier((&___mDataSetPath_10), value);
	}

	inline static int32_t get_offset_of_mExtendedTracking_11() { return static_cast<int32_t>(offsetof(DataSetTrackableBehaviour_t3960979584, ___mExtendedTracking_11)); }
	inline bool get_mExtendedTracking_11() const { return ___mExtendedTracking_11; }
	inline bool* get_address_of_mExtendedTracking_11() { return &___mExtendedTracking_11; }
	inline void set_mExtendedTracking_11(bool value)
	{
		___mExtendedTracking_11 = value;
	}

	inline static int32_t get_offset_of_mInitializeSmartTerrain_12() { return static_cast<int32_t>(offsetof(DataSetTrackableBehaviour_t3960979584, ___mInitializeSmartTerrain_12)); }
	inline bool get_mInitializeSmartTerrain_12() const { return ___mInitializeSmartTerrain_12; }
	inline bool* get_address_of_mInitializeSmartTerrain_12() { return &___mInitializeSmartTerrain_12; }
	inline void set_mInitializeSmartTerrain_12(bool value)
	{
		___mInitializeSmartTerrain_12 = value;
	}

	inline static int32_t get_offset_of_mReconstructionToInitialize_13() { return static_cast<int32_t>(offsetof(DataSetTrackableBehaviour_t3960979584, ___mReconstructionToInitialize_13)); }
	inline ReconstructionFromTargetAbstractBehaviour_t2676804737 * get_mReconstructionToInitialize_13() const { return ___mReconstructionToInitialize_13; }
	inline ReconstructionFromTargetAbstractBehaviour_t2676804737 ** get_address_of_mReconstructionToInitialize_13() { return &___mReconstructionToInitialize_13; }
	inline void set_mReconstructionToInitialize_13(ReconstructionFromTargetAbstractBehaviour_t2676804737 * value)
	{
		___mReconstructionToInitialize_13 = value;
		Il2CppCodeGenWriteBarrier((&___mReconstructionToInitialize_13), value);
	}

	inline static int32_t get_offset_of_mSmartTerrainOccluderBoundsMin_14() { return static_cast<int32_t>(offsetof(DataSetTrackableBehaviour_t3960979584, ___mSmartTerrainOccluderBoundsMin_14)); }
	inline Vector3_t2903530434  get_mSmartTerrainOccluderBoundsMin_14() const { return ___mSmartTerrainOccluderBoundsMin_14; }
	inline Vector3_t2903530434 * get_address_of_mSmartTerrainOccluderBoundsMin_14() { return &___mSmartTerrainOccluderBoundsMin_14; }
	inline void set_mSmartTerrainOccluderBoundsMin_14(Vector3_t2903530434  value)
	{
		___mSmartTerrainOccluderBoundsMin_14 = value;
	}

	inline static int32_t get_offset_of_mSmartTerrainOccluderBoundsMax_15() { return static_cast<int32_t>(offsetof(DataSetTrackableBehaviour_t3960979584, ___mSmartTerrainOccluderBoundsMax_15)); }
	inline Vector3_t2903530434  get_mSmartTerrainOccluderBoundsMax_15() const { return ___mSmartTerrainOccluderBoundsMax_15; }
	inline Vector3_t2903530434 * get_address_of_mSmartTerrainOccluderBoundsMax_15() { return &___mSmartTerrainOccluderBoundsMax_15; }
	inline void set_mSmartTerrainOccluderBoundsMax_15(Vector3_t2903530434  value)
	{
		___mSmartTerrainOccluderBoundsMax_15 = value;
	}

	inline static int32_t get_offset_of_mIsSmartTerrainOccluderOffset_16() { return static_cast<int32_t>(offsetof(DataSetTrackableBehaviour_t3960979584, ___mIsSmartTerrainOccluderOffset_16)); }
	inline bool get_mIsSmartTerrainOccluderOffset_16() const { return ___mIsSmartTerrainOccluderOffset_16; }
	inline bool* get_address_of_mIsSmartTerrainOccluderOffset_16() { return &___mIsSmartTerrainOccluderOffset_16; }
	inline void set_mIsSmartTerrainOccluderOffset_16(bool value)
	{
		___mIsSmartTerrainOccluderOffset_16 = value;
	}

	inline static int32_t get_offset_of_mSmartTerrainOccluderOffset_17() { return static_cast<int32_t>(offsetof(DataSetTrackableBehaviour_t3960979584, ___mSmartTerrainOccluderOffset_17)); }
	inline Vector3_t2903530434  get_mSmartTerrainOccluderOffset_17() const { return ___mSmartTerrainOccluderOffset_17; }
	inline Vector3_t2903530434 * get_address_of_mSmartTerrainOccluderOffset_17() { return &___mSmartTerrainOccluderOffset_17; }
	inline void set_mSmartTerrainOccluderOffset_17(Vector3_t2903530434  value)
	{
		___mSmartTerrainOccluderOffset_17 = value;
	}

	inline static int32_t get_offset_of_mSmartTerrainOccluderRotation_18() { return static_cast<int32_t>(offsetof(DataSetTrackableBehaviour_t3960979584, ___mSmartTerrainOccluderRotation_18)); }
	inline Quaternion_t754065749  get_mSmartTerrainOccluderRotation_18() const { return ___mSmartTerrainOccluderRotation_18; }
	inline Quaternion_t754065749 * get_address_of_mSmartTerrainOccluderRotation_18() { return &___mSmartTerrainOccluderRotation_18; }
	inline void set_mSmartTerrainOccluderRotation_18(Quaternion_t754065749  value)
	{
		___mSmartTerrainOccluderRotation_18 = value;
	}

	inline static int32_t get_offset_of_mAutoSetOccluderFromTargetSize_19() { return static_cast<int32_t>(offsetof(DataSetTrackableBehaviour_t3960979584, ___mAutoSetOccluderFromTargetSize_19)); }
	inline bool get_mAutoSetOccluderFromTargetSize_19() const { return ___mAutoSetOccluderFromTargetSize_19; }
	inline bool* get_address_of_mAutoSetOccluderFromTargetSize_19() { return &___mAutoSetOccluderFromTargetSize_19; }
	inline void set_mAutoSetOccluderFromTargetSize_19(bool value)
	{
		___mAutoSetOccluderFromTargetSize_19 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATASETTRACKABLEBEHAVIOUR_T3960979584_H
#ifndef RECONSTRUCTIONBEHAVIOUR_T3936121853_H
#define RECONSTRUCTIONBEHAVIOUR_T3936121853_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.ReconstructionBehaviour
struct  ReconstructionBehaviour_t3936121853  : public ReconstructionAbstractBehaviour_t954468633
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RECONSTRUCTIONBEHAVIOUR_T3936121853_H
#ifndef CLOUDRECOBEHAVIOUR_T2989569978_H
#define CLOUDRECOBEHAVIOUR_T2989569978_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.CloudRecoBehaviour
struct  CloudRecoBehaviour_t2989569978  : public CloudRecoAbstractBehaviour_t2501456771
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CLOUDRECOBEHAVIOUR_T2989569978_H
#ifndef HIDEEXCESSAREABEHAVIOUR_T554831333_H
#define HIDEEXCESSAREABEHAVIOUR_T554831333_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.HideExcessAreaBehaviour
struct  HideExcessAreaBehaviour_t554831333  : public HideExcessAreaAbstractBehaviour_t1324413388
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HIDEEXCESSAREABEHAVIOUR_T554831333_H
#ifndef SMARTTERRAINTRACKABLEBEHAVIOUR_T4179131702_H
#define SMARTTERRAINTRACKABLEBEHAVIOUR_T4179131702_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.SmartTerrainTrackableBehaviour
struct  SmartTerrainTrackableBehaviour_t4179131702  : public TrackableBehaviour_t2419079356
{
public:
	// Vuforia.SmartTerrainTrackable Vuforia.SmartTerrainTrackableBehaviour::mSmartTerrainTrackable
	RuntimeObject* ___mSmartTerrainTrackable_10;
	// System.Boolean Vuforia.SmartTerrainTrackableBehaviour::mDisableAutomaticUpdates
	bool ___mDisableAutomaticUpdates_11;
	// UnityEngine.MeshFilter Vuforia.SmartTerrainTrackableBehaviour::mMeshFilterToUpdate
	MeshFilter_t137687116 * ___mMeshFilterToUpdate_12;
	// UnityEngine.MeshCollider Vuforia.SmartTerrainTrackableBehaviour::mMeshColliderToUpdate
	MeshCollider_t4172739389 * ___mMeshColliderToUpdate_13;

public:
	inline static int32_t get_offset_of_mSmartTerrainTrackable_10() { return static_cast<int32_t>(offsetof(SmartTerrainTrackableBehaviour_t4179131702, ___mSmartTerrainTrackable_10)); }
	inline RuntimeObject* get_mSmartTerrainTrackable_10() const { return ___mSmartTerrainTrackable_10; }
	inline RuntimeObject** get_address_of_mSmartTerrainTrackable_10() { return &___mSmartTerrainTrackable_10; }
	inline void set_mSmartTerrainTrackable_10(RuntimeObject* value)
	{
		___mSmartTerrainTrackable_10 = value;
		Il2CppCodeGenWriteBarrier((&___mSmartTerrainTrackable_10), value);
	}

	inline static int32_t get_offset_of_mDisableAutomaticUpdates_11() { return static_cast<int32_t>(offsetof(SmartTerrainTrackableBehaviour_t4179131702, ___mDisableAutomaticUpdates_11)); }
	inline bool get_mDisableAutomaticUpdates_11() const { return ___mDisableAutomaticUpdates_11; }
	inline bool* get_address_of_mDisableAutomaticUpdates_11() { return &___mDisableAutomaticUpdates_11; }
	inline void set_mDisableAutomaticUpdates_11(bool value)
	{
		___mDisableAutomaticUpdates_11 = value;
	}

	inline static int32_t get_offset_of_mMeshFilterToUpdate_12() { return static_cast<int32_t>(offsetof(SmartTerrainTrackableBehaviour_t4179131702, ___mMeshFilterToUpdate_12)); }
	inline MeshFilter_t137687116 * get_mMeshFilterToUpdate_12() const { return ___mMeshFilterToUpdate_12; }
	inline MeshFilter_t137687116 ** get_address_of_mMeshFilterToUpdate_12() { return &___mMeshFilterToUpdate_12; }
	inline void set_mMeshFilterToUpdate_12(MeshFilter_t137687116 * value)
	{
		___mMeshFilterToUpdate_12 = value;
		Il2CppCodeGenWriteBarrier((&___mMeshFilterToUpdate_12), value);
	}

	inline static int32_t get_offset_of_mMeshColliderToUpdate_13() { return static_cast<int32_t>(offsetof(SmartTerrainTrackableBehaviour_t4179131702, ___mMeshColliderToUpdate_13)); }
	inline MeshCollider_t4172739389 * get_mMeshColliderToUpdate_13() const { return ___mMeshColliderToUpdate_13; }
	inline MeshCollider_t4172739389 ** get_address_of_mMeshColliderToUpdate_13() { return &___mMeshColliderToUpdate_13; }
	inline void set_mMeshColliderToUpdate_13(MeshCollider_t4172739389 * value)
	{
		___mMeshColliderToUpdate_13 = value;
		Il2CppCodeGenWriteBarrier((&___mMeshColliderToUpdate_13), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SMARTTERRAINTRACKABLEBEHAVIOUR_T4179131702_H
#ifndef VUFORIABEHAVIOUR_T834098562_H
#define VUFORIABEHAVIOUR_T834098562_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.VuforiaBehaviour
struct  VuforiaBehaviour_t834098562  : public VuforiaAbstractBehaviour_t153010168
{
public:

public:
};

struct VuforiaBehaviour_t834098562_StaticFields
{
public:
	// Vuforia.VuforiaBehaviour Vuforia.VuforiaBehaviour::mVuforiaBehaviour
	VuforiaBehaviour_t834098562 * ___mVuforiaBehaviour_19;

public:
	inline static int32_t get_offset_of_mVuforiaBehaviour_19() { return static_cast<int32_t>(offsetof(VuforiaBehaviour_t834098562_StaticFields, ___mVuforiaBehaviour_19)); }
	inline VuforiaBehaviour_t834098562 * get_mVuforiaBehaviour_19() const { return ___mVuforiaBehaviour_19; }
	inline VuforiaBehaviour_t834098562 ** get_address_of_mVuforiaBehaviour_19() { return &___mVuforiaBehaviour_19; }
	inline void set_mVuforiaBehaviour_19(VuforiaBehaviour_t834098562 * value)
	{
		___mVuforiaBehaviour_19 = value;
		Il2CppCodeGenWriteBarrier((&___mVuforiaBehaviour_19), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VUFORIABEHAVIOUR_T834098562_H
#ifndef BACKGROUNDPLANEBEHAVIOUR_T311858283_H
#define BACKGROUNDPLANEBEHAVIOUR_T311858283_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.BackgroundPlaneBehaviour
struct  BackgroundPlaneBehaviour_t311858283  : public BackgroundPlaneAbstractBehaviour_t4259835756
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BACKGROUNDPLANEBEHAVIOUR_T311858283_H
#ifndef MASKOUTBEHAVIOUR_T62027326_H
#define MASKOUTBEHAVIOUR_T62027326_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.MaskOutBehaviour
struct  MaskOutBehaviour_t62027326  : public MaskOutAbstractBehaviour_t2174599097
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MASKOUTBEHAVIOUR_T62027326_H
#ifndef VIRTUALBUTTONBEHAVIOUR_T1019531330_H
#define VIRTUALBUTTONBEHAVIOUR_T1019531330_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.VirtualButtonBehaviour
struct  VirtualButtonBehaviour_t1019531330  : public VirtualButtonAbstractBehaviour_t1965820348
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VIRTUALBUTTONBEHAVIOUR_T1019531330_H
#ifndef WORDABSTRACTBEHAVIOUR_T2339709601_H
#define WORDABSTRACTBEHAVIOUR_T2339709601_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.WordAbstractBehaviour
struct  WordAbstractBehaviour_t2339709601  : public TrackableBehaviour_t2419079356
{
public:
	// Vuforia.WordTemplateMode Vuforia.WordAbstractBehaviour::mMode
	int32_t ___mMode_10;
	// System.String Vuforia.WordAbstractBehaviour::mSpecificWord
	String_t* ___mSpecificWord_11;
	// Vuforia.Word Vuforia.WordAbstractBehaviour::mWord
	RuntimeObject* ___mWord_12;

public:
	inline static int32_t get_offset_of_mMode_10() { return static_cast<int32_t>(offsetof(WordAbstractBehaviour_t2339709601, ___mMode_10)); }
	inline int32_t get_mMode_10() const { return ___mMode_10; }
	inline int32_t* get_address_of_mMode_10() { return &___mMode_10; }
	inline void set_mMode_10(int32_t value)
	{
		___mMode_10 = value;
	}

	inline static int32_t get_offset_of_mSpecificWord_11() { return static_cast<int32_t>(offsetof(WordAbstractBehaviour_t2339709601, ___mSpecificWord_11)); }
	inline String_t* get_mSpecificWord_11() const { return ___mSpecificWord_11; }
	inline String_t** get_address_of_mSpecificWord_11() { return &___mSpecificWord_11; }
	inline void set_mSpecificWord_11(String_t* value)
	{
		___mSpecificWord_11 = value;
		Il2CppCodeGenWriteBarrier((&___mSpecificWord_11), value);
	}

	inline static int32_t get_offset_of_mWord_12() { return static_cast<int32_t>(offsetof(WordAbstractBehaviour_t2339709601, ___mWord_12)); }
	inline RuntimeObject* get_mWord_12() const { return ___mWord_12; }
	inline RuntimeObject** get_address_of_mWord_12() { return &___mWord_12; }
	inline void set_mWord_12(RuntimeObject* value)
	{
		___mWord_12 = value;
		Il2CppCodeGenWriteBarrier((&___mWord_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WORDABSTRACTBEHAVIOUR_T2339709601_H
#ifndef VIDEOBACKGROUNDBEHAVIOUR_T177581750_H
#define VIDEOBACKGROUNDBEHAVIOUR_T177581750_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.VideoBackgroundBehaviour
struct  VideoBackgroundBehaviour_t177581750  : public VideoBackgroundAbstractBehaviour_t3867888217
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VIDEOBACKGROUNDBEHAVIOUR_T177581750_H
#ifndef USERDEFINEDTARGETBUILDINGBEHAVIOUR_T3295846277_H
#define USERDEFINEDTARGETBUILDINGBEHAVIOUR_T3295846277_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.UserDefinedTargetBuildingBehaviour
struct  UserDefinedTargetBuildingBehaviour_t3295846277  : public UserDefinedTargetBuildingAbstractBehaviour_t2424076393
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // USERDEFINEDTARGETBUILDINGBEHAVIOUR_T3295846277_H
#ifndef TEXTRECOBEHAVIOUR_T3253755448_H
#define TEXTRECOBEHAVIOUR_T3253755448_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.TextRecoBehaviour
struct  TextRecoBehaviour_t3253755448  : public TextRecoAbstractBehaviour_t1751158196
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTRECOBEHAVIOUR_T3253755448_H
#ifndef RECONSTRUCTIONFROMTARGETBEHAVIOUR_T2819884381_H
#define RECONSTRUCTIONFROMTARGETBEHAVIOUR_T2819884381_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.ReconstructionFromTargetBehaviour
struct  ReconstructionFromTargetBehaviour_t2819884381  : public ReconstructionFromTargetAbstractBehaviour_t2676804737
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RECONSTRUCTIONFROMTARGETBEHAVIOUR_T2819884381_H
#ifndef TURNOFFBEHAVIOUR_T963849236_H
#define TURNOFFBEHAVIOUR_T963849236_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.TurnOffBehaviour
struct  TurnOffBehaviour_t963849236  : public TurnOffAbstractBehaviour_t1066217039
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TURNOFFBEHAVIOUR_T963849236_H
#ifndef OBJECTTARGETABSTRACTBEHAVIOUR_T1410714589_H
#define OBJECTTARGETABSTRACTBEHAVIOUR_T1410714589_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.ObjectTargetAbstractBehaviour
struct  ObjectTargetAbstractBehaviour_t1410714589  : public DataSetTrackableBehaviour_t3960979584
{
public:
	// Vuforia.ObjectTarget Vuforia.ObjectTargetAbstractBehaviour::mObjectTarget
	RuntimeObject* ___mObjectTarget_20;
	// System.Single Vuforia.ObjectTargetAbstractBehaviour::mAspectRatioXY
	float ___mAspectRatioXY_21;
	// System.Single Vuforia.ObjectTargetAbstractBehaviour::mAspectRatioXZ
	float ___mAspectRatioXZ_22;
	// System.Boolean Vuforia.ObjectTargetAbstractBehaviour::mShowBoundingBox
	bool ___mShowBoundingBox_23;
	// UnityEngine.Vector3 Vuforia.ObjectTargetAbstractBehaviour::mBBoxMin
	Vector3_t2903530434  ___mBBoxMin_24;
	// UnityEngine.Vector3 Vuforia.ObjectTargetAbstractBehaviour::mBBoxMax
	Vector3_t2903530434  ___mBBoxMax_25;
	// UnityEngine.Texture2D Vuforia.ObjectTargetAbstractBehaviour::mPreviewImage
	Texture2D_t415585320 * ___mPreviewImage_26;
	// System.Single Vuforia.ObjectTargetAbstractBehaviour::mLength
	float ___mLength_27;
	// System.Single Vuforia.ObjectTargetAbstractBehaviour::mWidth
	float ___mWidth_28;
	// System.Single Vuforia.ObjectTargetAbstractBehaviour::mHeight
	float ___mHeight_29;
	// System.Single Vuforia.ObjectTargetAbstractBehaviour::mLastTransformScale
	float ___mLastTransformScale_30;
	// UnityEngine.Vector3 Vuforia.ObjectTargetAbstractBehaviour::mLastSize
	Vector3_t2903530434  ___mLastSize_31;

public:
	inline static int32_t get_offset_of_mObjectTarget_20() { return static_cast<int32_t>(offsetof(ObjectTargetAbstractBehaviour_t1410714589, ___mObjectTarget_20)); }
	inline RuntimeObject* get_mObjectTarget_20() const { return ___mObjectTarget_20; }
	inline RuntimeObject** get_address_of_mObjectTarget_20() { return &___mObjectTarget_20; }
	inline void set_mObjectTarget_20(RuntimeObject* value)
	{
		___mObjectTarget_20 = value;
		Il2CppCodeGenWriteBarrier((&___mObjectTarget_20), value);
	}

	inline static int32_t get_offset_of_mAspectRatioXY_21() { return static_cast<int32_t>(offsetof(ObjectTargetAbstractBehaviour_t1410714589, ___mAspectRatioXY_21)); }
	inline float get_mAspectRatioXY_21() const { return ___mAspectRatioXY_21; }
	inline float* get_address_of_mAspectRatioXY_21() { return &___mAspectRatioXY_21; }
	inline void set_mAspectRatioXY_21(float value)
	{
		___mAspectRatioXY_21 = value;
	}

	inline static int32_t get_offset_of_mAspectRatioXZ_22() { return static_cast<int32_t>(offsetof(ObjectTargetAbstractBehaviour_t1410714589, ___mAspectRatioXZ_22)); }
	inline float get_mAspectRatioXZ_22() const { return ___mAspectRatioXZ_22; }
	inline float* get_address_of_mAspectRatioXZ_22() { return &___mAspectRatioXZ_22; }
	inline void set_mAspectRatioXZ_22(float value)
	{
		___mAspectRatioXZ_22 = value;
	}

	inline static int32_t get_offset_of_mShowBoundingBox_23() { return static_cast<int32_t>(offsetof(ObjectTargetAbstractBehaviour_t1410714589, ___mShowBoundingBox_23)); }
	inline bool get_mShowBoundingBox_23() const { return ___mShowBoundingBox_23; }
	inline bool* get_address_of_mShowBoundingBox_23() { return &___mShowBoundingBox_23; }
	inline void set_mShowBoundingBox_23(bool value)
	{
		___mShowBoundingBox_23 = value;
	}

	inline static int32_t get_offset_of_mBBoxMin_24() { return static_cast<int32_t>(offsetof(ObjectTargetAbstractBehaviour_t1410714589, ___mBBoxMin_24)); }
	inline Vector3_t2903530434  get_mBBoxMin_24() const { return ___mBBoxMin_24; }
	inline Vector3_t2903530434 * get_address_of_mBBoxMin_24() { return &___mBBoxMin_24; }
	inline void set_mBBoxMin_24(Vector3_t2903530434  value)
	{
		___mBBoxMin_24 = value;
	}

	inline static int32_t get_offset_of_mBBoxMax_25() { return static_cast<int32_t>(offsetof(ObjectTargetAbstractBehaviour_t1410714589, ___mBBoxMax_25)); }
	inline Vector3_t2903530434  get_mBBoxMax_25() const { return ___mBBoxMax_25; }
	inline Vector3_t2903530434 * get_address_of_mBBoxMax_25() { return &___mBBoxMax_25; }
	inline void set_mBBoxMax_25(Vector3_t2903530434  value)
	{
		___mBBoxMax_25 = value;
	}

	inline static int32_t get_offset_of_mPreviewImage_26() { return static_cast<int32_t>(offsetof(ObjectTargetAbstractBehaviour_t1410714589, ___mPreviewImage_26)); }
	inline Texture2D_t415585320 * get_mPreviewImage_26() const { return ___mPreviewImage_26; }
	inline Texture2D_t415585320 ** get_address_of_mPreviewImage_26() { return &___mPreviewImage_26; }
	inline void set_mPreviewImage_26(Texture2D_t415585320 * value)
	{
		___mPreviewImage_26 = value;
		Il2CppCodeGenWriteBarrier((&___mPreviewImage_26), value);
	}

	inline static int32_t get_offset_of_mLength_27() { return static_cast<int32_t>(offsetof(ObjectTargetAbstractBehaviour_t1410714589, ___mLength_27)); }
	inline float get_mLength_27() const { return ___mLength_27; }
	inline float* get_address_of_mLength_27() { return &___mLength_27; }
	inline void set_mLength_27(float value)
	{
		___mLength_27 = value;
	}

	inline static int32_t get_offset_of_mWidth_28() { return static_cast<int32_t>(offsetof(ObjectTargetAbstractBehaviour_t1410714589, ___mWidth_28)); }
	inline float get_mWidth_28() const { return ___mWidth_28; }
	inline float* get_address_of_mWidth_28() { return &___mWidth_28; }
	inline void set_mWidth_28(float value)
	{
		___mWidth_28 = value;
	}

	inline static int32_t get_offset_of_mHeight_29() { return static_cast<int32_t>(offsetof(ObjectTargetAbstractBehaviour_t1410714589, ___mHeight_29)); }
	inline float get_mHeight_29() const { return ___mHeight_29; }
	inline float* get_address_of_mHeight_29() { return &___mHeight_29; }
	inline void set_mHeight_29(float value)
	{
		___mHeight_29 = value;
	}

	inline static int32_t get_offset_of_mLastTransformScale_30() { return static_cast<int32_t>(offsetof(ObjectTargetAbstractBehaviour_t1410714589, ___mLastTransformScale_30)); }
	inline float get_mLastTransformScale_30() const { return ___mLastTransformScale_30; }
	inline float* get_address_of_mLastTransformScale_30() { return &___mLastTransformScale_30; }
	inline void set_mLastTransformScale_30(float value)
	{
		___mLastTransformScale_30 = value;
	}

	inline static int32_t get_offset_of_mLastSize_31() { return static_cast<int32_t>(offsetof(ObjectTargetAbstractBehaviour_t1410714589, ___mLastSize_31)); }
	inline Vector3_t2903530434  get_mLastSize_31() const { return ___mLastSize_31; }
	inline Vector3_t2903530434 * get_address_of_mLastSize_31() { return &___mLastSize_31; }
	inline void set_mLastSize_31(Vector3_t2903530434  value)
	{
		___mLastSize_31 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OBJECTTARGETABSTRACTBEHAVIOUR_T1410714589_H
#ifndef SURFACEABSTRACTBEHAVIOUR_T272541176_H
#define SURFACEABSTRACTBEHAVIOUR_T272541176_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.SurfaceAbstractBehaviour
struct  SurfaceAbstractBehaviour_t272541176  : public SmartTerrainTrackableBehaviour_t4179131702
{
public:
	// Vuforia.Surface Vuforia.SurfaceAbstractBehaviour::mSurface
	RuntimeObject* ___mSurface_14;

public:
	inline static int32_t get_offset_of_mSurface_14() { return static_cast<int32_t>(offsetof(SurfaceAbstractBehaviour_t272541176, ___mSurface_14)); }
	inline RuntimeObject* get_mSurface_14() const { return ___mSurface_14; }
	inline RuntimeObject** get_address_of_mSurface_14() { return &___mSurface_14; }
	inline void set_mSurface_14(RuntimeObject* value)
	{
		___mSurface_14 = value;
		Il2CppCodeGenWriteBarrier((&___mSurface_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SURFACEABSTRACTBEHAVIOUR_T272541176_H
#ifndef CYLINDERTARGETABSTRACTBEHAVIOUR_T2158579059_H
#define CYLINDERTARGETABSTRACTBEHAVIOUR_T2158579059_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.CylinderTargetAbstractBehaviour
struct  CylinderTargetAbstractBehaviour_t2158579059  : public DataSetTrackableBehaviour_t3960979584
{
public:
	// Vuforia.CylinderTarget Vuforia.CylinderTargetAbstractBehaviour::mCylinderTarget
	RuntimeObject* ___mCylinderTarget_20;
	// System.Single Vuforia.CylinderTargetAbstractBehaviour::mTopDiameterRatio
	float ___mTopDiameterRatio_21;
	// System.Single Vuforia.CylinderTargetAbstractBehaviour::mBottomDiameterRatio
	float ___mBottomDiameterRatio_22;
	// System.Single Vuforia.CylinderTargetAbstractBehaviour::mSideLength
	float ___mSideLength_23;
	// System.Single Vuforia.CylinderTargetAbstractBehaviour::mTopDiameter
	float ___mTopDiameter_24;
	// System.Single Vuforia.CylinderTargetAbstractBehaviour::mBottomDiameter
	float ___mBottomDiameter_25;
	// System.Int32 Vuforia.CylinderTargetAbstractBehaviour::mFrameIndex
	int32_t ___mFrameIndex_26;
	// System.Int32 Vuforia.CylinderTargetAbstractBehaviour::mUpdateFrameIndex
	int32_t ___mUpdateFrameIndex_27;
	// System.Single Vuforia.CylinderTargetAbstractBehaviour::mFutureScale
	float ___mFutureScale_28;
	// System.Single Vuforia.CylinderTargetAbstractBehaviour::mLastTransformScale
	float ___mLastTransformScale_29;

public:
	inline static int32_t get_offset_of_mCylinderTarget_20() { return static_cast<int32_t>(offsetof(CylinderTargetAbstractBehaviour_t2158579059, ___mCylinderTarget_20)); }
	inline RuntimeObject* get_mCylinderTarget_20() const { return ___mCylinderTarget_20; }
	inline RuntimeObject** get_address_of_mCylinderTarget_20() { return &___mCylinderTarget_20; }
	inline void set_mCylinderTarget_20(RuntimeObject* value)
	{
		___mCylinderTarget_20 = value;
		Il2CppCodeGenWriteBarrier((&___mCylinderTarget_20), value);
	}

	inline static int32_t get_offset_of_mTopDiameterRatio_21() { return static_cast<int32_t>(offsetof(CylinderTargetAbstractBehaviour_t2158579059, ___mTopDiameterRatio_21)); }
	inline float get_mTopDiameterRatio_21() const { return ___mTopDiameterRatio_21; }
	inline float* get_address_of_mTopDiameterRatio_21() { return &___mTopDiameterRatio_21; }
	inline void set_mTopDiameterRatio_21(float value)
	{
		___mTopDiameterRatio_21 = value;
	}

	inline static int32_t get_offset_of_mBottomDiameterRatio_22() { return static_cast<int32_t>(offsetof(CylinderTargetAbstractBehaviour_t2158579059, ___mBottomDiameterRatio_22)); }
	inline float get_mBottomDiameterRatio_22() const { return ___mBottomDiameterRatio_22; }
	inline float* get_address_of_mBottomDiameterRatio_22() { return &___mBottomDiameterRatio_22; }
	inline void set_mBottomDiameterRatio_22(float value)
	{
		___mBottomDiameterRatio_22 = value;
	}

	inline static int32_t get_offset_of_mSideLength_23() { return static_cast<int32_t>(offsetof(CylinderTargetAbstractBehaviour_t2158579059, ___mSideLength_23)); }
	inline float get_mSideLength_23() const { return ___mSideLength_23; }
	inline float* get_address_of_mSideLength_23() { return &___mSideLength_23; }
	inline void set_mSideLength_23(float value)
	{
		___mSideLength_23 = value;
	}

	inline static int32_t get_offset_of_mTopDiameter_24() { return static_cast<int32_t>(offsetof(CylinderTargetAbstractBehaviour_t2158579059, ___mTopDiameter_24)); }
	inline float get_mTopDiameter_24() const { return ___mTopDiameter_24; }
	inline float* get_address_of_mTopDiameter_24() { return &___mTopDiameter_24; }
	inline void set_mTopDiameter_24(float value)
	{
		___mTopDiameter_24 = value;
	}

	inline static int32_t get_offset_of_mBottomDiameter_25() { return static_cast<int32_t>(offsetof(CylinderTargetAbstractBehaviour_t2158579059, ___mBottomDiameter_25)); }
	inline float get_mBottomDiameter_25() const { return ___mBottomDiameter_25; }
	inline float* get_address_of_mBottomDiameter_25() { return &___mBottomDiameter_25; }
	inline void set_mBottomDiameter_25(float value)
	{
		___mBottomDiameter_25 = value;
	}

	inline static int32_t get_offset_of_mFrameIndex_26() { return static_cast<int32_t>(offsetof(CylinderTargetAbstractBehaviour_t2158579059, ___mFrameIndex_26)); }
	inline int32_t get_mFrameIndex_26() const { return ___mFrameIndex_26; }
	inline int32_t* get_address_of_mFrameIndex_26() { return &___mFrameIndex_26; }
	inline void set_mFrameIndex_26(int32_t value)
	{
		___mFrameIndex_26 = value;
	}

	inline static int32_t get_offset_of_mUpdateFrameIndex_27() { return static_cast<int32_t>(offsetof(CylinderTargetAbstractBehaviour_t2158579059, ___mUpdateFrameIndex_27)); }
	inline int32_t get_mUpdateFrameIndex_27() const { return ___mUpdateFrameIndex_27; }
	inline int32_t* get_address_of_mUpdateFrameIndex_27() { return &___mUpdateFrameIndex_27; }
	inline void set_mUpdateFrameIndex_27(int32_t value)
	{
		___mUpdateFrameIndex_27 = value;
	}

	inline static int32_t get_offset_of_mFutureScale_28() { return static_cast<int32_t>(offsetof(CylinderTargetAbstractBehaviour_t2158579059, ___mFutureScale_28)); }
	inline float get_mFutureScale_28() const { return ___mFutureScale_28; }
	inline float* get_address_of_mFutureScale_28() { return &___mFutureScale_28; }
	inline void set_mFutureScale_28(float value)
	{
		___mFutureScale_28 = value;
	}

	inline static int32_t get_offset_of_mLastTransformScale_29() { return static_cast<int32_t>(offsetof(CylinderTargetAbstractBehaviour_t2158579059, ___mLastTransformScale_29)); }
	inline float get_mLastTransformScale_29() const { return ___mLastTransformScale_29; }
	inline float* get_address_of_mLastTransformScale_29() { return &___mLastTransformScale_29; }
	inline void set_mLastTransformScale_29(float value)
	{
		___mLastTransformScale_29 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CYLINDERTARGETABSTRACTBEHAVIOUR_T2158579059_H
#ifndef VUMARKABSTRACTBEHAVIOUR_T2652615854_H
#define VUMARKABSTRACTBEHAVIOUR_T2652615854_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.VuMarkAbstractBehaviour
struct  VuMarkAbstractBehaviour_t2652615854  : public DataSetTrackableBehaviour_t3960979584
{
public:
	// System.Single Vuforia.VuMarkAbstractBehaviour::mAspectRatio
	float ___mAspectRatio_20;
	// System.Single Vuforia.VuMarkAbstractBehaviour::mWidth
	float ___mWidth_21;
	// System.Single Vuforia.VuMarkAbstractBehaviour::mHeight
	float ___mHeight_22;
	// System.String Vuforia.VuMarkAbstractBehaviour::mPreviewImage
	String_t* ___mPreviewImage_23;
	// Vuforia.InstanceIdType Vuforia.VuMarkAbstractBehaviour::mIdType
	int32_t ___mIdType_24;
	// System.Int32 Vuforia.VuMarkAbstractBehaviour::mIdLength
	int32_t ___mIdLength_25;
	// UnityEngine.Rect Vuforia.VuMarkAbstractBehaviour::mBoundingBox
	Rect_t3436776195  ___mBoundingBox_26;
	// UnityEngine.Vector2 Vuforia.VuMarkAbstractBehaviour::mOrigin
	Vector2_t3577333262  ___mOrigin_27;
	// System.Boolean Vuforia.VuMarkAbstractBehaviour::mTrackingFromRuntimeAppearance
	bool ___mTrackingFromRuntimeAppearance_28;
	// Vuforia.VuMarkTemplate Vuforia.VuMarkAbstractBehaviour::mVuMarkTemplate
	RuntimeObject* ___mVuMarkTemplate_29;
	// Vuforia.VuMarkTarget Vuforia.VuMarkAbstractBehaviour::mVuMarkTarget
	RuntimeObject* ___mVuMarkTarget_30;
	// System.Int32 Vuforia.VuMarkAbstractBehaviour::mVuMarkResultId
	int32_t ___mVuMarkResultId_31;
	// System.Action Vuforia.VuMarkAbstractBehaviour::mOnTargetAssigned
	Action_t3619184611 * ___mOnTargetAssigned_32;
	// System.Action Vuforia.VuMarkAbstractBehaviour::mOnTargetLost
	Action_t3619184611 * ___mOnTargetLost_33;
	// System.Single Vuforia.VuMarkAbstractBehaviour::mLastTransformScale
	float ___mLastTransformScale_34;
	// UnityEngine.Vector2 Vuforia.VuMarkAbstractBehaviour::mLastSize
	Vector2_t3577333262  ___mLastSize_35;

public:
	inline static int32_t get_offset_of_mAspectRatio_20() { return static_cast<int32_t>(offsetof(VuMarkAbstractBehaviour_t2652615854, ___mAspectRatio_20)); }
	inline float get_mAspectRatio_20() const { return ___mAspectRatio_20; }
	inline float* get_address_of_mAspectRatio_20() { return &___mAspectRatio_20; }
	inline void set_mAspectRatio_20(float value)
	{
		___mAspectRatio_20 = value;
	}

	inline static int32_t get_offset_of_mWidth_21() { return static_cast<int32_t>(offsetof(VuMarkAbstractBehaviour_t2652615854, ___mWidth_21)); }
	inline float get_mWidth_21() const { return ___mWidth_21; }
	inline float* get_address_of_mWidth_21() { return &___mWidth_21; }
	inline void set_mWidth_21(float value)
	{
		___mWidth_21 = value;
	}

	inline static int32_t get_offset_of_mHeight_22() { return static_cast<int32_t>(offsetof(VuMarkAbstractBehaviour_t2652615854, ___mHeight_22)); }
	inline float get_mHeight_22() const { return ___mHeight_22; }
	inline float* get_address_of_mHeight_22() { return &___mHeight_22; }
	inline void set_mHeight_22(float value)
	{
		___mHeight_22 = value;
	}

	inline static int32_t get_offset_of_mPreviewImage_23() { return static_cast<int32_t>(offsetof(VuMarkAbstractBehaviour_t2652615854, ___mPreviewImage_23)); }
	inline String_t* get_mPreviewImage_23() const { return ___mPreviewImage_23; }
	inline String_t** get_address_of_mPreviewImage_23() { return &___mPreviewImage_23; }
	inline void set_mPreviewImage_23(String_t* value)
	{
		___mPreviewImage_23 = value;
		Il2CppCodeGenWriteBarrier((&___mPreviewImage_23), value);
	}

	inline static int32_t get_offset_of_mIdType_24() { return static_cast<int32_t>(offsetof(VuMarkAbstractBehaviour_t2652615854, ___mIdType_24)); }
	inline int32_t get_mIdType_24() const { return ___mIdType_24; }
	inline int32_t* get_address_of_mIdType_24() { return &___mIdType_24; }
	inline void set_mIdType_24(int32_t value)
	{
		___mIdType_24 = value;
	}

	inline static int32_t get_offset_of_mIdLength_25() { return static_cast<int32_t>(offsetof(VuMarkAbstractBehaviour_t2652615854, ___mIdLength_25)); }
	inline int32_t get_mIdLength_25() const { return ___mIdLength_25; }
	inline int32_t* get_address_of_mIdLength_25() { return &___mIdLength_25; }
	inline void set_mIdLength_25(int32_t value)
	{
		___mIdLength_25 = value;
	}

	inline static int32_t get_offset_of_mBoundingBox_26() { return static_cast<int32_t>(offsetof(VuMarkAbstractBehaviour_t2652615854, ___mBoundingBox_26)); }
	inline Rect_t3436776195  get_mBoundingBox_26() const { return ___mBoundingBox_26; }
	inline Rect_t3436776195 * get_address_of_mBoundingBox_26() { return &___mBoundingBox_26; }
	inline void set_mBoundingBox_26(Rect_t3436776195  value)
	{
		___mBoundingBox_26 = value;
	}

	inline static int32_t get_offset_of_mOrigin_27() { return static_cast<int32_t>(offsetof(VuMarkAbstractBehaviour_t2652615854, ___mOrigin_27)); }
	inline Vector2_t3577333262  get_mOrigin_27() const { return ___mOrigin_27; }
	inline Vector2_t3577333262 * get_address_of_mOrigin_27() { return &___mOrigin_27; }
	inline void set_mOrigin_27(Vector2_t3577333262  value)
	{
		___mOrigin_27 = value;
	}

	inline static int32_t get_offset_of_mTrackingFromRuntimeAppearance_28() { return static_cast<int32_t>(offsetof(VuMarkAbstractBehaviour_t2652615854, ___mTrackingFromRuntimeAppearance_28)); }
	inline bool get_mTrackingFromRuntimeAppearance_28() const { return ___mTrackingFromRuntimeAppearance_28; }
	inline bool* get_address_of_mTrackingFromRuntimeAppearance_28() { return &___mTrackingFromRuntimeAppearance_28; }
	inline void set_mTrackingFromRuntimeAppearance_28(bool value)
	{
		___mTrackingFromRuntimeAppearance_28 = value;
	}

	inline static int32_t get_offset_of_mVuMarkTemplate_29() { return static_cast<int32_t>(offsetof(VuMarkAbstractBehaviour_t2652615854, ___mVuMarkTemplate_29)); }
	inline RuntimeObject* get_mVuMarkTemplate_29() const { return ___mVuMarkTemplate_29; }
	inline RuntimeObject** get_address_of_mVuMarkTemplate_29() { return &___mVuMarkTemplate_29; }
	inline void set_mVuMarkTemplate_29(RuntimeObject* value)
	{
		___mVuMarkTemplate_29 = value;
		Il2CppCodeGenWriteBarrier((&___mVuMarkTemplate_29), value);
	}

	inline static int32_t get_offset_of_mVuMarkTarget_30() { return static_cast<int32_t>(offsetof(VuMarkAbstractBehaviour_t2652615854, ___mVuMarkTarget_30)); }
	inline RuntimeObject* get_mVuMarkTarget_30() const { return ___mVuMarkTarget_30; }
	inline RuntimeObject** get_address_of_mVuMarkTarget_30() { return &___mVuMarkTarget_30; }
	inline void set_mVuMarkTarget_30(RuntimeObject* value)
	{
		___mVuMarkTarget_30 = value;
		Il2CppCodeGenWriteBarrier((&___mVuMarkTarget_30), value);
	}

	inline static int32_t get_offset_of_mVuMarkResultId_31() { return static_cast<int32_t>(offsetof(VuMarkAbstractBehaviour_t2652615854, ___mVuMarkResultId_31)); }
	inline int32_t get_mVuMarkResultId_31() const { return ___mVuMarkResultId_31; }
	inline int32_t* get_address_of_mVuMarkResultId_31() { return &___mVuMarkResultId_31; }
	inline void set_mVuMarkResultId_31(int32_t value)
	{
		___mVuMarkResultId_31 = value;
	}

	inline static int32_t get_offset_of_mOnTargetAssigned_32() { return static_cast<int32_t>(offsetof(VuMarkAbstractBehaviour_t2652615854, ___mOnTargetAssigned_32)); }
	inline Action_t3619184611 * get_mOnTargetAssigned_32() const { return ___mOnTargetAssigned_32; }
	inline Action_t3619184611 ** get_address_of_mOnTargetAssigned_32() { return &___mOnTargetAssigned_32; }
	inline void set_mOnTargetAssigned_32(Action_t3619184611 * value)
	{
		___mOnTargetAssigned_32 = value;
		Il2CppCodeGenWriteBarrier((&___mOnTargetAssigned_32), value);
	}

	inline static int32_t get_offset_of_mOnTargetLost_33() { return static_cast<int32_t>(offsetof(VuMarkAbstractBehaviour_t2652615854, ___mOnTargetLost_33)); }
	inline Action_t3619184611 * get_mOnTargetLost_33() const { return ___mOnTargetLost_33; }
	inline Action_t3619184611 ** get_address_of_mOnTargetLost_33() { return &___mOnTargetLost_33; }
	inline void set_mOnTargetLost_33(Action_t3619184611 * value)
	{
		___mOnTargetLost_33 = value;
		Il2CppCodeGenWriteBarrier((&___mOnTargetLost_33), value);
	}

	inline static int32_t get_offset_of_mLastTransformScale_34() { return static_cast<int32_t>(offsetof(VuMarkAbstractBehaviour_t2652615854, ___mLastTransformScale_34)); }
	inline float get_mLastTransformScale_34() const { return ___mLastTransformScale_34; }
	inline float* get_address_of_mLastTransformScale_34() { return &___mLastTransformScale_34; }
	inline void set_mLastTransformScale_34(float value)
	{
		___mLastTransformScale_34 = value;
	}

	inline static int32_t get_offset_of_mLastSize_35() { return static_cast<int32_t>(offsetof(VuMarkAbstractBehaviour_t2652615854, ___mLastSize_35)); }
	inline Vector2_t3577333262  get_mLastSize_35() const { return ___mLastSize_35; }
	inline Vector2_t3577333262 * get_address_of_mLastSize_35() { return &___mLastSize_35; }
	inline void set_mLastSize_35(Vector2_t3577333262  value)
	{
		___mLastSize_35 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VUMARKABSTRACTBEHAVIOUR_T2652615854_H
#ifndef WORDBEHAVIOUR_T2625704100_H
#define WORDBEHAVIOUR_T2625704100_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.WordBehaviour
struct  WordBehaviour_t2625704100  : public WordAbstractBehaviour_t2339709601
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WORDBEHAVIOUR_T2625704100_H
#ifndef MULTITARGETABSTRACTBEHAVIOUR_T1527469731_H
#define MULTITARGETABSTRACTBEHAVIOUR_T1527469731_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.MultiTargetAbstractBehaviour
struct  MultiTargetAbstractBehaviour_t1527469731  : public DataSetTrackableBehaviour_t3960979584
{
public:
	// Vuforia.MultiTarget Vuforia.MultiTargetAbstractBehaviour::mMultiTarget
	RuntimeObject* ___mMultiTarget_20;

public:
	inline static int32_t get_offset_of_mMultiTarget_20() { return static_cast<int32_t>(offsetof(MultiTargetAbstractBehaviour_t1527469731, ___mMultiTarget_20)); }
	inline RuntimeObject* get_mMultiTarget_20() const { return ___mMultiTarget_20; }
	inline RuntimeObject** get_address_of_mMultiTarget_20() { return &___mMultiTarget_20; }
	inline void set_mMultiTarget_20(RuntimeObject* value)
	{
		___mMultiTarget_20 = value;
		Il2CppCodeGenWriteBarrier((&___mMultiTarget_20), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MULTITARGETABSTRACTBEHAVIOUR_T1527469731_H
#ifndef PROPABSTRACTBEHAVIOUR_T2648882166_H
#define PROPABSTRACTBEHAVIOUR_T2648882166_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.PropAbstractBehaviour
struct  PropAbstractBehaviour_t2648882166  : public SmartTerrainTrackableBehaviour_t4179131702
{
public:
	// Vuforia.Prop Vuforia.PropAbstractBehaviour::mProp
	RuntimeObject* ___mProp_14;
	// UnityEngine.BoxCollider Vuforia.PropAbstractBehaviour::mBoxColliderToUpdate
	BoxCollider_t1861275623 * ___mBoxColliderToUpdate_15;

public:
	inline static int32_t get_offset_of_mProp_14() { return static_cast<int32_t>(offsetof(PropAbstractBehaviour_t2648882166, ___mProp_14)); }
	inline RuntimeObject* get_mProp_14() const { return ___mProp_14; }
	inline RuntimeObject** get_address_of_mProp_14() { return &___mProp_14; }
	inline void set_mProp_14(RuntimeObject* value)
	{
		___mProp_14 = value;
		Il2CppCodeGenWriteBarrier((&___mProp_14), value);
	}

	inline static int32_t get_offset_of_mBoxColliderToUpdate_15() { return static_cast<int32_t>(offsetof(PropAbstractBehaviour_t2648882166, ___mBoxColliderToUpdate_15)); }
	inline BoxCollider_t1861275623 * get_mBoxColliderToUpdate_15() const { return ___mBoxColliderToUpdate_15; }
	inline BoxCollider_t1861275623 ** get_address_of_mBoxColliderToUpdate_15() { return &___mBoxColliderToUpdate_15; }
	inline void set_mBoxColliderToUpdate_15(BoxCollider_t1861275623 * value)
	{
		___mBoxColliderToUpdate_15 = value;
		Il2CppCodeGenWriteBarrier((&___mBoxColliderToUpdate_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROPABSTRACTBEHAVIOUR_T2648882166_H
#ifndef IMAGETARGETABSTRACTBEHAVIOUR_T355543434_H
#define IMAGETARGETABSTRACTBEHAVIOUR_T355543434_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.ImageTargetAbstractBehaviour
struct  ImageTargetAbstractBehaviour_t355543434  : public DataSetTrackableBehaviour_t3960979584
{
public:
	// System.Single Vuforia.ImageTargetAbstractBehaviour::mAspectRatio
	float ___mAspectRatio_20;
	// Vuforia.ImageTargetType Vuforia.ImageTargetAbstractBehaviour::mImageTargetType
	int32_t ___mImageTargetType_21;
	// System.Single Vuforia.ImageTargetAbstractBehaviour::mWidth
	float ___mWidth_22;
	// System.Single Vuforia.ImageTargetAbstractBehaviour::mHeight
	float ___mHeight_23;
	// Vuforia.ImageTarget Vuforia.ImageTargetAbstractBehaviour::mImageTarget
	RuntimeObject* ___mImageTarget_24;
	// System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VirtualButtonAbstractBehaviour> Vuforia.ImageTargetAbstractBehaviour::mVirtualButtonBehaviours
	Dictionary_2_t3161827512 * ___mVirtualButtonBehaviours_25;
	// System.Single Vuforia.ImageTargetAbstractBehaviour::mLastTransformScale
	float ___mLastTransformScale_26;
	// UnityEngine.Vector2 Vuforia.ImageTargetAbstractBehaviour::mLastSize
	Vector2_t3577333262  ___mLastSize_27;

public:
	inline static int32_t get_offset_of_mAspectRatio_20() { return static_cast<int32_t>(offsetof(ImageTargetAbstractBehaviour_t355543434, ___mAspectRatio_20)); }
	inline float get_mAspectRatio_20() const { return ___mAspectRatio_20; }
	inline float* get_address_of_mAspectRatio_20() { return &___mAspectRatio_20; }
	inline void set_mAspectRatio_20(float value)
	{
		___mAspectRatio_20 = value;
	}

	inline static int32_t get_offset_of_mImageTargetType_21() { return static_cast<int32_t>(offsetof(ImageTargetAbstractBehaviour_t355543434, ___mImageTargetType_21)); }
	inline int32_t get_mImageTargetType_21() const { return ___mImageTargetType_21; }
	inline int32_t* get_address_of_mImageTargetType_21() { return &___mImageTargetType_21; }
	inline void set_mImageTargetType_21(int32_t value)
	{
		___mImageTargetType_21 = value;
	}

	inline static int32_t get_offset_of_mWidth_22() { return static_cast<int32_t>(offsetof(ImageTargetAbstractBehaviour_t355543434, ___mWidth_22)); }
	inline float get_mWidth_22() const { return ___mWidth_22; }
	inline float* get_address_of_mWidth_22() { return &___mWidth_22; }
	inline void set_mWidth_22(float value)
	{
		___mWidth_22 = value;
	}

	inline static int32_t get_offset_of_mHeight_23() { return static_cast<int32_t>(offsetof(ImageTargetAbstractBehaviour_t355543434, ___mHeight_23)); }
	inline float get_mHeight_23() const { return ___mHeight_23; }
	inline float* get_address_of_mHeight_23() { return &___mHeight_23; }
	inline void set_mHeight_23(float value)
	{
		___mHeight_23 = value;
	}

	inline static int32_t get_offset_of_mImageTarget_24() { return static_cast<int32_t>(offsetof(ImageTargetAbstractBehaviour_t355543434, ___mImageTarget_24)); }
	inline RuntimeObject* get_mImageTarget_24() const { return ___mImageTarget_24; }
	inline RuntimeObject** get_address_of_mImageTarget_24() { return &___mImageTarget_24; }
	inline void set_mImageTarget_24(RuntimeObject* value)
	{
		___mImageTarget_24 = value;
		Il2CppCodeGenWriteBarrier((&___mImageTarget_24), value);
	}

	inline static int32_t get_offset_of_mVirtualButtonBehaviours_25() { return static_cast<int32_t>(offsetof(ImageTargetAbstractBehaviour_t355543434, ___mVirtualButtonBehaviours_25)); }
	inline Dictionary_2_t3161827512 * get_mVirtualButtonBehaviours_25() const { return ___mVirtualButtonBehaviours_25; }
	inline Dictionary_2_t3161827512 ** get_address_of_mVirtualButtonBehaviours_25() { return &___mVirtualButtonBehaviours_25; }
	inline void set_mVirtualButtonBehaviours_25(Dictionary_2_t3161827512 * value)
	{
		___mVirtualButtonBehaviours_25 = value;
		Il2CppCodeGenWriteBarrier((&___mVirtualButtonBehaviours_25), value);
	}

	inline static int32_t get_offset_of_mLastTransformScale_26() { return static_cast<int32_t>(offsetof(ImageTargetAbstractBehaviour_t355543434, ___mLastTransformScale_26)); }
	inline float get_mLastTransformScale_26() const { return ___mLastTransformScale_26; }
	inline float* get_address_of_mLastTransformScale_26() { return &___mLastTransformScale_26; }
	inline void set_mLastTransformScale_26(float value)
	{
		___mLastTransformScale_26 = value;
	}

	inline static int32_t get_offset_of_mLastSize_27() { return static_cast<int32_t>(offsetof(ImageTargetAbstractBehaviour_t355543434, ___mLastSize_27)); }
	inline Vector2_t3577333262  get_mLastSize_27() const { return ___mLastSize_27; }
	inline Vector2_t3577333262 * get_address_of_mLastSize_27() { return &___mLastSize_27; }
	inline void set_mLastSize_27(Vector2_t3577333262  value)
	{
		___mLastSize_27 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IMAGETARGETABSTRACTBEHAVIOUR_T355543434_H
#ifndef PROPBEHAVIOUR_T3897855688_H
#define PROPBEHAVIOUR_T3897855688_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.PropBehaviour
struct  PropBehaviour_t3897855688  : public PropAbstractBehaviour_t2648882166
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROPBEHAVIOUR_T3897855688_H
#ifndef IMAGETARGETBEHAVIOUR_T2918114530_H
#define IMAGETARGETBEHAVIOUR_T2918114530_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.ImageTargetBehaviour
struct  ImageTargetBehaviour_t2918114530  : public ImageTargetAbstractBehaviour_t355543434
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IMAGETARGETBEHAVIOUR_T2918114530_H
#ifndef SURFACEBEHAVIOUR_T1975960194_H
#define SURFACEBEHAVIOUR_T1975960194_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.SurfaceBehaviour
struct  SurfaceBehaviour_t1975960194  : public SurfaceAbstractBehaviour_t272541176
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SURFACEBEHAVIOUR_T1975960194_H
#ifndef OBJECTTARGETBEHAVIOUR_T1761394684_H
#define OBJECTTARGETBEHAVIOUR_T1761394684_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.ObjectTargetBehaviour
struct  ObjectTargetBehaviour_t1761394684  : public ObjectTargetAbstractBehaviour_t1410714589
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OBJECTTARGETBEHAVIOUR_T1761394684_H
#ifndef CYLINDERTARGETBEHAVIOUR_T1224832031_H
#define CYLINDERTARGETBEHAVIOUR_T1224832031_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.CylinderTargetBehaviour
struct  CylinderTargetBehaviour_t1224832031  : public CylinderTargetAbstractBehaviour_t2158579059
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CYLINDERTARGETBEHAVIOUR_T1224832031_H
#ifndef VUMARKBEHAVIOUR_T3122351057_H
#define VUMARKBEHAVIOUR_T3122351057_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.VuMarkBehaviour
struct  VuMarkBehaviour_t3122351057  : public VuMarkAbstractBehaviour_t2652615854
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VUMARKBEHAVIOUR_T3122351057_H
#ifndef MULTITARGETBEHAVIOUR_T687655925_H
#define MULTITARGETBEHAVIOUR_T687655925_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.MultiTargetBehaviour
struct  MultiTargetBehaviour_t687655925  : public MultiTargetAbstractBehaviour_t1527469731
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MULTITARGETBEHAVIOUR_T687655925_H
// System.Object[]
struct ObjectU5BU5D_t3270211303  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) RuntimeObject * m_Items[1];

public:
	inline RuntimeObject * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline RuntimeObject ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, RuntimeObject * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline RuntimeObject * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline RuntimeObject ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, RuntimeObject * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Reflection.MethodInfo[]
struct MethodInfoU5BU5D_t1774028895  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) MethodInfo_t * m_Items[1];

public:
	inline MethodInfo_t * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline MethodInfo_t ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, MethodInfo_t * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline MethodInfo_t * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline MethodInfo_t ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, MethodInfo_t * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UnityEngine.Renderer[]
struct RendererU5BU5D_t2667410251  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Renderer_t1303575294 * m_Items[1];

public:
	inline Renderer_t1303575294 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Renderer_t1303575294 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Renderer_t1303575294 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline Renderer_t1303575294 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Renderer_t1303575294 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Renderer_t1303575294 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UnityEngine.Collider[]
struct ColliderU5BU5D_t3746774603  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Collider_t2301021182 * m_Items[1];

public:
	inline Collider_t2301021182 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Collider_t2301021182 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Collider_t2301021182 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline Collider_t2301021182 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Collider_t2301021182 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Collider_t2301021182 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UnityEngine.Material[]
struct MaterialU5BU5D_t1437145498  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Material_t1079520667 * m_Items[1];

public:
	inline Material_t1079520667 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Material_t1079520667 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Material_t1079520667 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline Material_t1079520667 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Material_t1079520667 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Material_t1079520667 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UnityEngine.Camera[]
struct CameraU5BU5D_t1519774542  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Camera_t3175186167 * m_Items[1];

public:
	inline Camera_t3175186167 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Camera_t3175186167 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Camera_t3175186167 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline Camera_t3175186167 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Camera_t3175186167 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Camera_t3175186167 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t981680887  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Vector3_t2903530434  m_Items[1];

public:
	inline Vector3_t2903530434  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Vector3_t2903530434 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Vector3_t2903530434  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline Vector3_t2903530434  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Vector3_t2903530434 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Vector3_t2903530434  value)
	{
		m_Items[index] = value;
	}
};
// System.Int32[]
struct Int32U5BU5D_t2324750880  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) int32_t m_Items[1];

public:
	inline int32_t GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline int32_t* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, int32_t value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline int32_t GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline int32_t* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, int32_t value)
	{
		m_Items[index] = value;
	}
};
// Vuforia.WireframeBehaviour[]
struct WireframeBehaviourU5BU5D_t1741061725  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) WireframeBehaviour_t1899115476 * m_Items[1];

public:
	inline WireframeBehaviour_t1899115476 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline WireframeBehaviour_t1899115476 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, WireframeBehaviour_t1899115476 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline WireframeBehaviour_t1899115476 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline WireframeBehaviour_t1899115476 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, WireframeBehaviour_t1899115476 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};


// !!0 UnityEngine.Component::GetComponent<System.Object>()
extern "C"  RuntimeObject * Component_GetComponent_TisRuntimeObject_m2412552191_gshared (Component_t4087199522 * __this, const RuntimeMethod* method);
// !!0 UnityEngine.Component::GetComponentInChildren<System.Object>()
extern "C"  RuntimeObject * Component_GetComponentInChildren_TisRuntimeObject_m2445373276_gshared (Component_t4087199522 * __this, const RuntimeMethod* method);
// System.Collections.Generic.List`1<!!0> System.Linq.Enumerable::ToList<System.Object>(System.Collections.Generic.IEnumerable`1<!!0>)
extern "C"  List_1_t877107497 * Enumerable_ToList_TisRuntimeObject_m2484750451_gshared (RuntimeObject * __this /* static, unused */, RuntimeObject* p0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Object>::AddRange(System.Collections.Generic.IEnumerable`1<!0>)
extern "C"  void List_1_AddRange_m980742883_gshared (List_1_t877107497 * __this, RuntimeObject* p0, const RuntimeMethod* method);
// System.Collections.Generic.List`1/Enumerator<!0> System.Collections.Generic.List`1<System.Object>::GetEnumerator()
extern "C"  Enumerator_t2954986095  List_1_GetEnumerator_m1639556462_gshared (List_1_t877107497 * __this, const RuntimeMethod* method);
// !0 System.Collections.Generic.List`1/Enumerator<System.Object>::get_Current()
extern "C"  RuntimeObject * Enumerator_get_Current_m1155707170_gshared (Enumerator_t2954986095 * __this, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Object>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m1031192525_gshared (Enumerator_t2954986095 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1/Enumerator<System.Object>::Dispose()
extern "C"  void Enumerator_Dispose_m1161557187_gshared (Enumerator_t2954986095 * __this, const RuntimeMethod* method);
// System.Void System.Action`1<Vuforia.VuforiaUnity/InitError>::.ctor(System.Object,System.IntPtr)
extern "C"  void Action_1__ctor_m1316558631_gshared (Action_1_t3472505684 * __this, RuntimeObject * p0, IntPtr_t p1, const RuntimeMethod* method);
// System.Void System.Action`1<System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void Action_1__ctor_m1218043724_gshared (Action_1_t2007785632 * __this, RuntimeObject * p0, IntPtr_t p1, const RuntimeMethod* method);
// !!0[] UnityEngine.Component::GetComponentsInChildren<System.Object>(System.Boolean)
extern "C"  ObjectU5BU5D_t3270211303* Component_GetComponentsInChildren_TisRuntimeObject_m2093175410_gshared (Component_t4087199522 * __this, bool p0, const RuntimeMethod* method);
// !!0 UnityEngine.GameObject::AddComponent<System.Object>()
extern "C"  RuntimeObject * GameObject_AddComponent_TisRuntimeObject_m2523233498_gshared (GameObject_t1811656094 * __this, const RuntimeMethod* method);
// !!0 UnityEngine.Object::FindObjectOfType<System.Object>()
extern "C"  RuntimeObject * Object_FindObjectOfType_TisRuntimeObject_m3950945820_gshared (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// !!0 UnityEngine.ScriptableObject::CreateInstance<System.Object>()
extern "C"  RuntimeObject * ScriptableObject_CreateInstance_TisRuntimeObject_m58643206_gshared (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// !!0[] UnityEngine.GameObject::GetComponentsInChildren<System.Object>()
extern "C"  ObjectU5BU5D_t3270211303* GameObject_GetComponentsInChildren_TisRuntimeObject_m4155885061_gshared (GameObject_t1811656094 * __this, const RuntimeMethod* method);

// System.Void UnityEngine.MonoBehaviour::.ctor()
extern "C"  void MonoBehaviour__ctor_m620219618 (MonoBehaviour_t345688271 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.Component::GetComponent<UnityEngine.Rigidbody>()
#define Component_GetComponent_TisRigidbody_t2492269564_m3707284554(__this, method) ((  Rigidbody_t2492269564 * (*) (Component_t4087199522 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m2412552191_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<UnityEngine.Animation>()
#define Component_GetComponent_TisAnimation_t2605054404_m2540927662(__this, method) ((  Animation_t2605054404 * (*) (Component_t4087199522 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m2412552191_gshared)(__this, method)
// System.Single UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager::GetAxis(System.String)
extern "C"  float CrossPlatformInputManager_GetAxis_m3946870782 (RuntimeObject * __this /* static, unused */, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Vector3::.ctor(System.Single,System.Single,System.Single)
extern "C"  void Vector3__ctor_m4053220054 (Vector3_t2903530434 * __this, float p0, float p1, float p2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Vector3::op_Multiply(UnityEngine.Vector3,System.Single)
extern "C"  Vector3_t2903530434  Vector3_op_Multiply_m2063944565 (RuntimeObject * __this /* static, unused */, Vector3_t2903530434  p0, float p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Rigidbody::set_velocity(UnityEngine.Vector3)
extern "C"  void Rigidbody_set_velocity_m3845336739 (Rigidbody_t2492269564 * __this, Vector3_t2903530434  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform UnityEngine.Component::get_transform()
extern "C"  Transform_t3316442598 * Component_get_transform_m2038396632 (Component_t4087199522 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Transform::get_eulerAngles()
extern "C"  Vector3_t2903530434  Transform_get_eulerAngles_m1943903321 (Transform_t3316442598 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Transform::set_eulerAngles(UnityEngine.Vector3)
extern "C"  void Transform_set_eulerAngles_m1572874495 (Transform_t3316442598 * __this, Vector3_t2903530434  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Animation::Play(System.String)
extern "C"  bool Animation_Play_m3576938708 (Animation_t2605054404 * __this, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.Component::GetComponent<UnityEngine.Camera>()
#define Component_GetComponent_TisCamera_t3175186167_m3957679396(__this, method) ((  Camera_t3175186167 * (*) (Component_t4087199522 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m2412552191_gshared)(__this, method)
// System.Void UnityEngine.Camera::set_fieldOfView(System.Single)
extern "C"  void Camera_set_fieldOfView_m337251087 (Camera_t3175186167 * __this, float p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// Vuforia.VuforiaARController Vuforia.VuforiaARController::get_Instance()
extern "C"  VuforiaARController_t1328503142 * VuforiaARController_get_Instance_m738587825 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Action::.ctor(System.Object,System.IntPtr)
extern "C"  void Action__ctor_m2777219059 (Action_t3619184611 * __this, RuntimeObject * p0, IntPtr_t p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.VuforiaARController::RegisterVuforiaStartedCallback(System.Action)
extern "C"  void VuforiaARController_RegisterVuforiaStartedCallback_m2236106145 (VuforiaARController_t1328503142 * __this, Action_t3619184611 * p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// Vuforia.DigitalEyewearARController Vuforia.DigitalEyewearARController::get_Instance()
extern "C"  DigitalEyewearARController_t1877756253 * DigitalEyewearARController_get_Instance_m4227562287 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Camera Vuforia.DigitalEyewearARController::get_PrimaryCamera()
extern "C"  Camera_t3175186167 * DigitalEyewearARController_get_PrimaryCamera_m2496874119 (DigitalEyewearARController_t1877756253 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Camera Vuforia.DigitalEyewearARController::get_SecondaryCamera()
extern "C"  Camera_t3175186167 * DigitalEyewearARController_get_SecondaryCamera_m1746377300 (DigitalEyewearARController_t1877756253 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.Component::GetComponent<Vuforia.HideExcessAreaAbstractBehaviour>()
#define Component_GetComponent_TisHideExcessAreaAbstractBehaviour_t1324413388_m2951051384(__this, method) ((  HideExcessAreaAbstractBehaviour_t1324413388 * (*) (Component_t4087199522 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m2412552191_gshared)(__this, method)
// UnityEngine.Transform Vuforia.DigitalEyewearARController::get_CentralAnchorPoint()
extern "C"  Transform_t3316442598 * DigitalEyewearARController_get_CentralAnchorPoint_m208802276 (DigitalEyewearARController_t1877756253 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Quaternion UnityEngine.Transform::get_localRotation()
extern "C"  Quaternion_t754065749  Transform_get_localRotation_m1836132480 (Transform_t3316442598 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Transform::set_localRotation(UnityEngine.Quaternion)
extern "C"  void Transform_set_localRotation_m4199788559 (Transform_t3316442598 * __this, Quaternion_t754065749  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Transform::get_localPosition()
extern "C"  Vector3_t2903530434  Transform_get_localPosition_m1266101501 (Transform_t3316442598 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Transform::set_localPosition(UnityEngine.Vector3)
extern "C"  void Transform_set_localPosition_m1667085576 (Transform_t3316442598 * __this, Vector3_t2903530434  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Rect UnityEngine.Camera::get_pixelRect()
extern "C"  Rect_t3436776195  Camera_get_pixelRect_m1533752293 (Camera_t3175186167 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Transform::get_right()
extern "C"  Vector3_t2903530434  Transform_get_right_m1220575382 (Transform_t3316442598 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Vector3::get_normalized()
extern "C"  Vector3_t2903530434  Vector3_get_normalized_m2996377041 (Vector3_t2903530434 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Camera::get_stereoSeparation()
extern "C"  float Camera_get_stereoSeparation_m3749102506 (Camera_t3175186167 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Transform::get_position()
extern "C"  Vector3_t2903530434  Transform_get_position_m1291471697 (Transform_t3316442598 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Vector3::op_Addition(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  Vector3_t2903530434  Vector3_op_Addition_m630862342 (RuntimeObject * __this /* static, unused */, Vector3_t2903530434  p0, Vector3_t2903530434  p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Transform::set_position(UnityEngine.Vector3)
extern "C"  void Transform_set_position_m1572818413 (Transform_t3316442598 * __this, Vector3_t2903530434  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Camera::set_pixelRect(UnityEngine.Rect)
extern "C"  void Camera_set_pixelRect_m2963872222 (Camera_t3175186167 * __this, Rect_t3436776195  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.Component::GetComponentInChildren<Vuforia.BackgroundPlaneBehaviour>()
#define Component_GetComponentInChildren_TisBackgroundPlaneBehaviour_t311858283_m2328091248(__this, method) ((  BackgroundPlaneBehaviour_t311858283 * (*) (Component_t4087199522 *, const RuntimeMethod*))Component_GetComponentInChildren_TisRuntimeObject_m2445373276_gshared)(__this, method)
// UnityEngine.Vector3 UnityEngine.Vector3::op_Subtraction(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  Vector3_t2903530434  Vector3_op_Subtraction_m648404402 (RuntimeObject * __this /* static, unused */, Vector3_t2903530434  p0, Vector3_t2903530434  p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.BackgroundPlaneAbstractBehaviour::set_BackgroundOffset(UnityEngine.Vector3)
extern "C"  void BackgroundPlaneAbstractBehaviour_set_BackgroundOffset_m1959300025 (BackgroundPlaneAbstractBehaviour_t4259835756 * __this, Vector3_t2903530434  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.HideExcessAreaAbstractBehaviour::set_PlaneOffset(UnityEngine.Vector3)
extern "C"  void HideExcessAreaAbstractBehaviour_set_PlaneOffset_m956571739 (HideExcessAreaAbstractBehaviour_t1324413388 * __this, Vector3_t2903530434  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Object::op_Inequality(UnityEngine.Object,UnityEngine.Object)
extern "C"  bool Object_op_Inequality_m1918954159 (RuntimeObject * __this /* static, unused */, Object_t1502412432 * p0, Object_t1502412432 * p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Vector3::get_zero()
extern "C"  Vector3_t2903530434  Vector3_get_zero_m2238342052 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.VuforiaARController::UpdateState(System.Boolean,System.Boolean)
extern "C"  void VuforiaARController_UpdateState_m3945451095 (VuforiaARController_t1328503142 * __this, bool p0, bool p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 Vuforia.BackgroundPlaneAbstractBehaviour::get_BackgroundOffset()
extern "C"  Vector3_t2903530434  BackgroundPlaneAbstractBehaviour_get_BackgroundOffset_m3877967905 (BackgroundPlaneAbstractBehaviour_t4259835756 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.VuforiaARController::ApplyCorrectedProjectionMatrix(UnityEngine.Matrix4x4,System.Boolean)
extern "C"  void VuforiaARController_ApplyCorrectedProjectionMatrix_m4248414074 (VuforiaARController_t1328503142 * __this, Matrix4x4_t4266809202  p0, bool p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Matrix4x4 UnityEngine.Camera::get_projectionMatrix()
extern "C"  Matrix4x4_t4266809202  Camera_get_projectionMatrix_m1935965422 (Camera_t3175186167 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Camera::SetStereoProjectionMatrices(UnityEngine.Matrix4x4,UnityEngine.Matrix4x4)
extern "C"  void Camera_SetStereoProjectionMatrices_m3898755345 (Camera_t3175186167 * __this, Matrix4x4_t4266809202  p0, Matrix4x4_t4266809202  p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.VuforiaRuntimeUtilities::MatrixIsNaN(UnityEngine.Matrix4x4)
extern "C"  bool VuforiaRuntimeUtilities_MatrixIsNaN_m1840818379 (RuntimeObject * __this /* static, unused */, Matrix4x4_t4266809202  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Object::.ctor()
extern "C"  void Object__ctor_m2906060009 (RuntimeObject * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.AndroidUnityPlayer::LoadNativeLibrariesFromJava()
extern "C"  void AndroidUnityPlayer_LoadNativeLibrariesFromJava_m798436972 (AndroidUnityPlayer_t3571566439 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.AndroidUnityPlayer::InitAndroidPlatform()
extern "C"  void AndroidUnityPlayer_InitAndroidPlatform_m3919873674 (AndroidUnityPlayer_t3571566439 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// Vuforia.VuforiaRenderer Vuforia.VuforiaRenderer::get_Instance()
extern "C"  VuforiaRenderer_t969713742 * VuforiaRenderer_get_Instance_m1182630307 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.AndroidUnityPlayer::InitVuforia(System.Int32,System.String)
extern "C"  int32_t AndroidUnityPlayer_InitVuforia_m1967216672 (AndroidUnityPlayer_t3571566439 * __this, int32_t ___rendererAPI0, String_t* ___licenseKey1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.AndroidUnityPlayer::InitializeSurface()
extern "C"  void AndroidUnityPlayer_InitializeSurface_m325648367 (AndroidUnityPlayer_t3571566439 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.SurfaceUtilities::HasSurfaceBeenRecreated()
extern "C"  bool SurfaceUtilities_HasSurfaceBeenRecreated_m346027931 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.ScreenOrientation UnityEngine.Screen::get_orientation()
extern "C"  int32_t Screen_get_orientation_m251094311 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.AndroidUnityPlayer::ResetUnityScreenOrientation()
extern "C"  void AndroidUnityPlayer_ResetUnityScreenOrientation_m4124764997 (AndroidUnityPlayer_t3571566439 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.AndroidUnityPlayer::CheckOrientation()
extern "C"  void AndroidUnityPlayer_CheckOrientation_m2694551379 (AndroidUnityPlayer_t3571566439 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.VuforiaUnity::OnPause()
extern "C"  void VuforiaUnity_OnPause_m4153953119 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.VuforiaUnity::OnResume()
extern "C"  void VuforiaUnity_OnResume_m2287209481 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.VuforiaUnity::Deinit()
extern "C"  void VuforiaUnity_Deinit_m2222663190 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.SurfaceUtilities::OnSurfaceCreated()
extern "C"  void SurfaceUtilities_OnSurfaceCreated_m1867899004 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.SurfaceUtilities::SetSurfaceOrientation(UnityEngine.ScreenOrientation)
extern "C"  void SurfaceUtilities_SetSurfaceOrientation_m897924353 (RuntimeObject * __this /* static, unused */, int32_t p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.BackgroundPlaneAbstractBehaviour::.ctor()
extern "C"  void BackgroundPlaneAbstractBehaviour__ctor_m2739397862 (BackgroundPlaneAbstractBehaviour_t4259835756 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.CloudRecoAbstractBehaviour::.ctor()
extern "C"  void CloudRecoAbstractBehaviour__ctor_m3633727160 (CloudRecoAbstractBehaviour_t2501456771 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Type System.Object::GetType()
extern "C"  Type_t * Object_GetType_m3774351292 (RuntimeObject * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<!!0> System.Linq.Enumerable::ToList<System.Reflection.MethodInfo>(System.Collections.Generic.IEnumerable`1<!!0>)
#define Enumerable_ToList_TisMethodInfo_t_m2942684005(__this /* static, unused */, p0, method) ((  List_1_t3148643089 * (*) (RuntimeObject * /* static, unused */, RuntimeObject*, const RuntimeMethod*))Enumerable_ToList_TisRuntimeObject_m2484750451_gshared)(__this /* static, unused */, p0, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.MethodInfo>::AddRange(System.Collections.Generic.IEnumerable`1<!0>)
#define List_1_AddRange_m2579895835(__this, p0, method) ((  void (*) (List_1_t3148643089 *, RuntimeObject*, const RuntimeMethod*))List_1_AddRange_m980742883_gshared)(__this, p0, method)
// System.Collections.Generic.List`1/Enumerator<!0> System.Collections.Generic.List`1<System.Reflection.MethodInfo>::GetEnumerator()
#define List_1_GetEnumerator_m3353760919(__this, method) ((  Enumerator_t931554391  (*) (List_1_t3148643089 *, const RuntimeMethod*))List_1_GetEnumerator_m1639556462_gshared)(__this, method)
// !0 System.Collections.Generic.List`1/Enumerator<System.Reflection.MethodInfo>::get_Current()
#define Enumerator_get_Current_m1308925728(__this, method) ((  MethodInfo_t * (*) (Enumerator_t931554391 *, const RuntimeMethod*))Enumerator_get_Current_m1155707170_gshared)(__this, method)
// System.Type System.Type::GetTypeFromHandle(System.RuntimeTypeHandle)
extern "C"  Type_t * Type_GetTypeFromHandle_m269683661 (RuntimeObject * __this /* static, unused */, RuntimeTypeHandle_t845633806  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Delegate System.Delegate::CreateDelegate(System.Type,System.Object,System.Reflection.MethodInfo)
extern "C"  Delegate_t537306192 * Delegate_CreateDelegate_m1805921115 (RuntimeObject * __this /* static, unused */, Type_t * p0, RuntimeObject * p1, MethodInfo_t * p2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Action::Invoke()
extern "C"  void Action_Invoke_m4106243596 (Action_t3619184611 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Reflection.MethodInfo>::MoveNext()
#define Enumerator_MoveNext_m1576165614(__this, method) ((  bool (*) (Enumerator_t931554391 *, const RuntimeMethod*))Enumerator_MoveNext_m1031192525_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<System.Reflection.MethodInfo>::Dispose()
#define Enumerator_Dispose_m363041641(__this, method) ((  void (*) (Enumerator_t931554391 *, const RuntimeMethod*))Enumerator_Dispose_m1161557187_gshared)(__this, method)
// System.Void UnityEngine.Debug::Log(System.Object)
extern "C"  void Debug_Log_m1673024736 (RuntimeObject * __this /* static, unused */, RuntimeObject * p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.VuforiaBehaviourComponentFactory::.ctor()
extern "C"  void VuforiaBehaviourComponentFactory__ctor_m2242124886 (VuforiaBehaviourComponentFactory_t2590131513 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.BehaviourComponentFactory::set_Instance(Vuforia.IBehaviourComponentFactory)
extern "C"  void BehaviourComponentFactory_set_Instance_m2152858985 (RuntimeObject * __this /* static, unused */, RuntimeObject* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.CylinderTargetAbstractBehaviour::.ctor()
extern "C"  void CylinderTargetAbstractBehaviour__ctor_m2234434592 (CylinderTargetAbstractBehaviour_t2158579059 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// Vuforia.VuforiaRuntime Vuforia.VuforiaRuntime::get_Instance()
extern "C"  VuforiaRuntime_t2131788666 * VuforiaRuntime_get_Instance_m3676190030 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Action`1<Vuforia.VuforiaUnity/InitError>::.ctor(System.Object,System.IntPtr)
#define Action_1__ctor_m1316558631(__this, p0, p1, method) ((  void (*) (Action_1_t3472505684 *, RuntimeObject *, IntPtr_t, const RuntimeMethod*))Action_1__ctor_m1316558631_gshared)(__this, p0, p1, method)
// System.Void Vuforia.VuforiaRuntime::RegisterVuforiaInitErrorCallback(System.Action`1<Vuforia.VuforiaUnity/InitError>)
extern "C"  void VuforiaRuntime_RegisterVuforiaInitErrorCallback_m788535050 (VuforiaRuntime_t2131788666 * __this, Action_1_t3472505684 * p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Screen::get_width()
extern "C"  int32_t Screen_get_width_m2947011129 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Screen::get_height()
extern "C"  int32_t Screen_get_height_m471732428 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Rect::.ctor(System.Single,System.Single,System.Single,System.Single)
extern "C"  void Rect__ctor_m2728905368 (Rect_t3436776195 * __this, float p0, float p1, float p2, float p3, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUI/WindowFunction::.ctor(System.Object,System.IntPtr)
extern "C"  void WindowFunction__ctor_m3870900743 (WindowFunction_t3577962812 * __this, RuntimeObject * p0, IntPtr_t p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Rect UnityEngine.GUI::Window(System.Int32,UnityEngine.Rect,UnityEngine.GUI/WindowFunction,System.String)
extern "C"  Rect_t3436776195  GUI_Window_m2935776987 (RuntimeObject * __this /* static, unused */, int32_t p0, Rect_t3436776195  p1, WindowFunction_t3577962812 * p2, String_t* p3, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.VuforiaRuntime::UnregisterVuforiaInitErrorCallback(System.Action`1<Vuforia.VuforiaUnity/InitError>)
extern "C"  void VuforiaRuntime_UnregisterVuforiaInitErrorCallback_m810573170 (VuforiaRuntime_t2131788666 * __this, Action_1_t3472505684 * p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUI::Label(UnityEngine.Rect,System.String)
extern "C"  void GUI_Label_m2909214338 (RuntimeObject * __this /* static, unused */, Rect_t3436776195  p0, String_t* p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.GUI::Button(UnityEngine.Rect,System.String)
extern "C"  bool GUI_Button_m1337722186 (RuntimeObject * __this /* static, unused */, Rect_t3436776195  p0, String_t* p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Application::Quit()
extern "C"  void Application_Quit_m1111605119 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String System.String::Concat(System.String,System.String)
extern "C"  String_t* String_Concat_m3054569703 (RuntimeObject * __this /* static, unused */, String_t* p0, String_t* p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Debug::LogError(System.Object)
extern "C"  void Debug_LogError_m1848790000 (RuntimeObject * __this /* static, unused */, RuntimeObject * p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.DefaultInitializationErrorHandler::SetErrorCode(Vuforia.VuforiaUnity/InitError)
extern "C"  void DefaultInitializationErrorHandler_SetErrorCode_m749491405 (DefaultInitializationErrorHandler_t414875900 * __this, int32_t ___errorCode0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.DefaultInitializationErrorHandler::SetErrorOccurred(System.Boolean)
extern "C"  void DefaultInitializationErrorHandler_SetErrorOccurred_m3423758312 (DefaultInitializationErrorHandler_t414875900 * __this, bool ___errorOccurred0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.Component::GetComponent<Vuforia.ReconstructionBehaviour>()
#define Component_GetComponent_TisReconstructionBehaviour_t3936121853_m3652591174(__this, method) ((  ReconstructionBehaviour_t3936121853 * (*) (Component_t4087199522 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m2412552191_gshared)(__this, method)
// System.Boolean UnityEngine.Object::op_Implicit(UnityEngine.Object)
extern "C"  bool Object_op_Implicit_m2314733868 (RuntimeObject * __this /* static, unused */, Object_t1502412432 * p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Action`1<Vuforia.Prop>::.ctor(System.Object,System.IntPtr)
#define Action_1__ctor_m220035642(__this, p0, p1, method) ((  void (*) (Action_1_t1560204979 *, RuntimeObject *, IntPtr_t, const RuntimeMethod*))Action_1__ctor_m1218043724_gshared)(__this, p0, p1, method)
// System.Void Vuforia.ReconstructionAbstractBehaviour::RegisterPropCreatedCallback(System.Action`1<Vuforia.Prop>)
extern "C"  void ReconstructionAbstractBehaviour_RegisterPropCreatedCallback_m2985286061 (ReconstructionAbstractBehaviour_t954468633 * __this, Action_1_t1560204979 * p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Action`1<Vuforia.Surface>::.ctor(System.Object,System.IntPtr)
#define Action_1__ctor_m59453672(__this, p0, p1, method) ((  void (*) (Action_1_t1020068010 *, RuntimeObject *, IntPtr_t, const RuntimeMethod*))Action_1__ctor_m1218043724_gshared)(__this, p0, p1, method)
// System.Void Vuforia.ReconstructionAbstractBehaviour::RegisterSurfaceCreatedCallback(System.Action`1<Vuforia.Surface>)
extern "C"  void ReconstructionAbstractBehaviour_RegisterSurfaceCreatedCallback_m479439615 (ReconstructionAbstractBehaviour_t954468633 * __this, Action_1_t1020068010 * p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.ReconstructionAbstractBehaviour::UnregisterPropCreatedCallback(System.Action`1<Vuforia.Prop>)
extern "C"  void ReconstructionAbstractBehaviour_UnregisterPropCreatedCallback_m1396476554 (ReconstructionAbstractBehaviour_t954468633 * __this, Action_1_t1560204979 * p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.ReconstructionAbstractBehaviour::UnregisterSurfaceCreatedCallback(System.Action`1<Vuforia.Surface>)
extern "C"  void ReconstructionAbstractBehaviour_UnregisterSurfaceCreatedCallback_m3341108077 (ReconstructionAbstractBehaviour_t954468633 * __this, Action_1_t1020068010 * p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// Vuforia.PropAbstractBehaviour Vuforia.ReconstructionAbstractBehaviour::AssociateProp(Vuforia.PropAbstractBehaviour,Vuforia.Prop)
extern "C"  PropAbstractBehaviour_t2648882166 * ReconstructionAbstractBehaviour_AssociateProp_m1567680850 (ReconstructionAbstractBehaviour_t954468633 * __this, PropAbstractBehaviour_t2648882166 * p0, RuntimeObject* p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// Vuforia.SurfaceAbstractBehaviour Vuforia.ReconstructionAbstractBehaviour::AssociateSurface(Vuforia.SurfaceAbstractBehaviour,Vuforia.Surface)
extern "C"  SurfaceAbstractBehaviour_t272541176 * ReconstructionAbstractBehaviour_AssociateSurface_m3177208316 (ReconstructionAbstractBehaviour_t954468633 * __this, SurfaceAbstractBehaviour_t272541176 * p0, RuntimeObject* p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.Component::GetComponent<Vuforia.TrackableBehaviour>()
#define Component_GetComponent_TisTrackableBehaviour_t2419079356_m3640808561(__this, method) ((  TrackableBehaviour_t2419079356 * (*) (Component_t4087199522 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m2412552191_gshared)(__this, method)
// System.Void Vuforia.TrackableBehaviour::RegisterTrackableEventHandler(Vuforia.ITrackableEventHandler)
extern "C"  void TrackableBehaviour_RegisterTrackableEventHandler_m537253126 (TrackableBehaviour_t2419079356 * __this, RuntimeObject* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.DefaultTrackableEventHandler::OnTrackingFound()
extern "C"  void DefaultTrackableEventHandler_OnTrackingFound_m3375166525 (DefaultTrackableEventHandler_t2331528573 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.DefaultTrackableEventHandler::OnTrackingLost()
extern "C"  void DefaultTrackableEventHandler_OnTrackingLost_m3475317276 (DefaultTrackableEventHandler_t2331528573 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !!0[] UnityEngine.Component::GetComponentsInChildren<UnityEngine.Renderer>(System.Boolean)
#define Component_GetComponentsInChildren_TisRenderer_t1303575294_m3094251748(__this, p0, method) ((  RendererU5BU5D_t2667410251* (*) (Component_t4087199522 *, bool, const RuntimeMethod*))Component_GetComponentsInChildren_TisRuntimeObject_m2093175410_gshared)(__this, p0, method)
// !!0[] UnityEngine.Component::GetComponentsInChildren<UnityEngine.Collider>(System.Boolean)
#define Component_GetComponentsInChildren_TisCollider_t2301021182_m857431636(__this, p0, method) ((  ColliderU5BU5D_t3746774603* (*) (Component_t4087199522 *, bool, const RuntimeMethod*))Component_GetComponentsInChildren_TisRuntimeObject_m2093175410_gshared)(__this, p0, method)
// System.Void UnityEngine.Renderer::set_enabled(System.Boolean)
extern "C"  void Renderer_set_enabled_m3164806497 (Renderer_t1303575294 * __this, bool p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Collider::set_enabled(System.Boolean)
extern "C"  void Collider_set_enabled_m1322675966 (Collider_t2301021182 * __this, bool p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String Vuforia.TrackableBehaviour::get_TrackableName()
extern "C"  String_t* TrackableBehaviour_get_TrackableName_m3217816807 (TrackableBehaviour_t2419079356 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String System.String::Concat(System.String,System.String,System.String)
extern "C"  String_t* String_Concat_m3656894380 (RuntimeObject * __this /* static, unused */, String_t* p0, String_t* p1, String_t* p2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.HideExcessAreaAbstractBehaviour::.ctor()
extern "C"  void HideExcessAreaAbstractBehaviour__ctor_m21881450 (HideExcessAreaAbstractBehaviour_t1324413388 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.ImageTargetAbstractBehaviour::.ctor()
extern "C"  void ImageTargetAbstractBehaviour__ctor_m2879815750 (ImageTargetAbstractBehaviour_t355543434 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.VuforiaNativeIosWrapper::.ctor()
extern "C"  void VuforiaNativeIosWrapper__ctor_m1849016803 (VuforiaNativeIosWrapper_t49709812 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.VuforiaWrapper::SetImplementation(Vuforia.IVuforiaWrapper)
extern "C"  void VuforiaWrapper_SetImplementation_m2562871935 (RuntimeObject * __this /* static, unused */, RuntimeObject* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.IOSUnityPlayer::setPlatFormNative()
extern "C"  void IOSUnityPlayer_setPlatFormNative_m3887041728 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.IOSUnityPlayer::initQCARiOS(System.Int32,System.Int32,System.String)
extern "C"  int32_t IOSUnityPlayer_initQCARiOS_m3056939392 (RuntimeObject * __this /* static, unused */, int32_t ___rendererAPI0, int32_t ___screenOrientation1, String_t* ___licenseKey2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.IOSUnityPlayer::InitializeSurface()
extern "C"  void IOSUnityPlayer_InitializeSurface_m1138500967 (IOSUnityPlayer_t1005570149 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.IOSUnityPlayer::SetUnityScreenOrientation()
extern "C"  void IOSUnityPlayer_SetUnityScreenOrientation_m3334399535 (IOSUnityPlayer_t1005570149 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.IOSUnityPlayer::setSurfaceOrientationiOS(System.Int32)
extern "C"  void IOSUnityPlayer_setSurfaceOrientationiOS_m2874037796 (RuntimeObject * __this /* static, unused */, int32_t ___screenOrientation0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.MaskOutAbstractBehaviour::.ctor()
extern "C"  void MaskOutAbstractBehaviour__ctor_m3408028974 (MaskOutAbstractBehaviour_t2174599097 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.VuforiaRuntimeUtilities::IsVuforiaEnabled()
extern "C"  bool VuforiaRuntimeUtilities_IsVuforiaEnabled_m3634160515 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.Component::GetComponent<UnityEngine.Renderer>()
#define Component_GetComponent_TisRenderer_t1303575294_m1648558159(__this, method) ((  Renderer_t1303575294 * (*) (Component_t4087199522 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m2412552191_gshared)(__this, method)
// UnityEngine.Material[] UnityEngine.Renderer::get_materials()
extern "C"  MaterialU5BU5D_t1437145498* Renderer_get_materials_m4017502688 (Renderer_t1303575294 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Renderer::set_sharedMaterial(UnityEngine.Material)
extern "C"  void Renderer_set_sharedMaterial_m1952739559 (Renderer_t1303575294 * __this, Material_t1079520667 * p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Renderer::set_sharedMaterials(UnityEngine.Material[])
extern "C"  void Renderer_set_sharedMaterials_m632974399 (Renderer_t1303575294 * __this, MaterialU5BU5D_t1437145498* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.MultiTargetAbstractBehaviour::.ctor()
extern "C"  void MultiTargetAbstractBehaviour__ctor_m2403500192 (MultiTargetAbstractBehaviour_t1527469731 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.ObjectTargetAbstractBehaviour::.ctor()
extern "C"  void ObjectTargetAbstractBehaviour__ctor_m3001407456 (ObjectTargetAbstractBehaviour_t1410714589 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.PropAbstractBehaviour::.ctor()
extern "C"  void PropAbstractBehaviour__ctor_m3916936582 (PropAbstractBehaviour_t2648882166 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.ReconstructionAbstractBehaviour::.ctor()
extern "C"  void ReconstructionAbstractBehaviour__ctor_m1687108356 (ReconstructionAbstractBehaviour_t954468633 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.ReconstructionFromTargetAbstractBehaviour::.ctor()
extern "C"  void ReconstructionFromTargetAbstractBehaviour__ctor_m1639870364 (ReconstructionFromTargetAbstractBehaviour_t2676804737 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.SurfaceAbstractBehaviour::.ctor()
extern "C"  void SurfaceAbstractBehaviour__ctor_m4081210538 (SurfaceAbstractBehaviour_t272541176 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.TextRecoAbstractBehaviour::.ctor()
extern "C"  void TextRecoAbstractBehaviour__ctor_m1589097412 (TextRecoAbstractBehaviour_t1751158196 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.TurnOffAbstractBehaviour::.ctor()
extern "C"  void TurnOffAbstractBehaviour__ctor_m3552357493 (TurnOffAbstractBehaviour_t1066217039 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.Component::GetComponent<UnityEngine.MeshRenderer>()
#define Component_GetComponent_TisMeshRenderer_t1441648352_m3769347907(__this, method) ((  MeshRenderer_t1441648352 * (*) (Component_t4087199522 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m2412552191_gshared)(__this, method)
// System.Void UnityEngine.Object::Destroy(UnityEngine.Object)
extern "C"  void Object_Destroy_m1956460533 (RuntimeObject * __this /* static, unused */, Object_t1502412432 * p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.Component::GetComponent<UnityEngine.MeshFilter>()
#define Component_GetComponent_TisMeshFilter_t137687116_m62619598(__this, method) ((  MeshFilter_t137687116 * (*) (Component_t4087199522 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m2412552191_gshared)(__this, method)
// UnityEngine.Transform UnityEngine.Transform::Find(System.String)
extern "C"  Transform_t3316442598 * Transform_Find_m1259991727 (Transform_t3316442598 * __this, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject UnityEngine.Component::get_gameObject()
extern "C"  GameObject_t1811656094 * Component_get_gameObject_m2283027183 (Component_t4087199522 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.UserDefinedTargetBuildingAbstractBehaviour::.ctor()
extern "C"  void UserDefinedTargetBuildingAbstractBehaviour__ctor_m2409308138 (UserDefinedTargetBuildingAbstractBehaviour_t2424076393 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.VideoBackgroundAbstractBehaviour::.ctor()
extern "C"  void VideoBackgroundAbstractBehaviour__ctor_m274348067 (VideoBackgroundAbstractBehaviour_t3867888217 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.VirtualButtonAbstractBehaviour::.ctor()
extern "C"  void VirtualButtonAbstractBehaviour__ctor_m917555744 (VirtualButtonAbstractBehaviour_t1965820348 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.VuforiaAbstractBehaviour::.ctor()
extern "C"  void VuforiaAbstractBehaviour__ctor_m322960130 (VuforiaAbstractBehaviour_t153010168 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.VuforiaBehaviour::AddOSSpecificExternalDatasetSearchDirs()
extern "C"  void VuforiaBehaviour_AddOSSpecificExternalDatasetSearchDirs_m3856602952 (VuforiaBehaviour_t834098562 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.GameObject::AddComponent<Vuforia.ComponentFactoryStarterBehaviour>()
#define GameObject_AddComponent_TisComponentFactoryStarterBehaviour_t2906678938_m2160705532(__this, method) ((  ComponentFactoryStarterBehaviour_t2906678938 * (*) (GameObject_t1811656094 *, const RuntimeMethod*))GameObject_AddComponent_TisRuntimeObject_m2523233498_gshared)(__this, method)
// System.Void Vuforia.VuforiaAbstractBehaviour::Awake()
extern "C"  void VuforiaAbstractBehaviour_Awake_m4140787687 (VuforiaAbstractBehaviour_t153010168 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Object::op_Equality(UnityEngine.Object,UnityEngine.Object)
extern "C"  bool Object_op_Equality_m2342070775 (RuntimeObject * __this /* static, unused */, Object_t1502412432 * p0, Object_t1502412432 * p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.Object::FindObjectOfType<Vuforia.VuforiaBehaviour>()
#define Object_FindObjectOfType_TisVuforiaBehaviour_t834098562_m3072761646(__this /* static, unused */, method) ((  VuforiaBehaviour_t834098562 * (*) (RuntimeObject * /* static, unused */, const RuntimeMethod*))Object_FindObjectOfType_TisRuntimeObject_m3950945820_gshared)(__this /* static, unused */, method)
// !!0 UnityEngine.GameObject::AddComponent<Vuforia.MaskOutBehaviour>()
#define GameObject_AddComponent_TisMaskOutBehaviour_t62027326_m1393573829(__this, method) ((  MaskOutBehaviour_t62027326 * (*) (GameObject_t1811656094 *, const RuntimeMethod*))GameObject_AddComponent_TisRuntimeObject_m2523233498_gshared)(__this, method)
// !!0 UnityEngine.GameObject::AddComponent<Vuforia.VirtualButtonBehaviour>()
#define GameObject_AddComponent_TisVirtualButtonBehaviour_t1019531330_m1545170832(__this, method) ((  VirtualButtonBehaviour_t1019531330 * (*) (GameObject_t1811656094 *, const RuntimeMethod*))GameObject_AddComponent_TisRuntimeObject_m2523233498_gshared)(__this, method)
// !!0 UnityEngine.GameObject::AddComponent<Vuforia.TurnOffBehaviour>()
#define GameObject_AddComponent_TisTurnOffBehaviour_t963849236_m3647489448(__this, method) ((  TurnOffBehaviour_t963849236 * (*) (GameObject_t1811656094 *, const RuntimeMethod*))GameObject_AddComponent_TisRuntimeObject_m2523233498_gshared)(__this, method)
// !!0 UnityEngine.GameObject::AddComponent<Vuforia.ImageTargetBehaviour>()
#define GameObject_AddComponent_TisImageTargetBehaviour_t2918114530_m3204365795(__this, method) ((  ImageTargetBehaviour_t2918114530 * (*) (GameObject_t1811656094 *, const RuntimeMethod*))GameObject_AddComponent_TisRuntimeObject_m2523233498_gshared)(__this, method)
// !!0 UnityEngine.GameObject::AddComponent<Vuforia.MultiTargetBehaviour>()
#define GameObject_AddComponent_TisMultiTargetBehaviour_t687655925_m601583604(__this, method) ((  MultiTargetBehaviour_t687655925 * (*) (GameObject_t1811656094 *, const RuntimeMethod*))GameObject_AddComponent_TisRuntimeObject_m2523233498_gshared)(__this, method)
// !!0 UnityEngine.GameObject::AddComponent<Vuforia.CylinderTargetBehaviour>()
#define GameObject_AddComponent_TisCylinderTargetBehaviour_t1224832031_m4238243230(__this, method) ((  CylinderTargetBehaviour_t1224832031 * (*) (GameObject_t1811656094 *, const RuntimeMethod*))GameObject_AddComponent_TisRuntimeObject_m2523233498_gshared)(__this, method)
// !!0 UnityEngine.GameObject::AddComponent<Vuforia.WordBehaviour>()
#define GameObject_AddComponent_TisWordBehaviour_t2625704100_m1670100688(__this, method) ((  WordBehaviour_t2625704100 * (*) (GameObject_t1811656094 *, const RuntimeMethod*))GameObject_AddComponent_TisRuntimeObject_m2523233498_gshared)(__this, method)
// !!0 UnityEngine.GameObject::AddComponent<Vuforia.TextRecoBehaviour>()
#define GameObject_AddComponent_TisTextRecoBehaviour_t3253755448_m1170737699(__this, method) ((  TextRecoBehaviour_t3253755448 * (*) (GameObject_t1811656094 *, const RuntimeMethod*))GameObject_AddComponent_TisRuntimeObject_m2523233498_gshared)(__this, method)
// !!0 UnityEngine.GameObject::AddComponent<Vuforia.ObjectTargetBehaviour>()
#define GameObject_AddComponent_TisObjectTargetBehaviour_t1761394684_m3858239762(__this, method) ((  ObjectTargetBehaviour_t1761394684 * (*) (GameObject_t1811656094 *, const RuntimeMethod*))GameObject_AddComponent_TisRuntimeObject_m2523233498_gshared)(__this, method)
// !!0 UnityEngine.GameObject::AddComponent<Vuforia.VuMarkBehaviour>()
#define GameObject_AddComponent_TisVuMarkBehaviour_t3122351057_m2352772220(__this, method) ((  VuMarkBehaviour_t3122351057 * (*) (GameObject_t1811656094 *, const RuntimeMethod*))GameObject_AddComponent_TisRuntimeObject_m2523233498_gshared)(__this, method)
// !!0 UnityEngine.ScriptableObject::CreateInstance<Vuforia.VuforiaConfiguration>()
#define ScriptableObject_CreateInstance_TisVuforiaConfiguration_t2489601127_m4156053030(__this /* static, unused */, method) ((  VuforiaConfiguration_t2489601127 * (*) (RuntimeObject * /* static, unused */, const RuntimeMethod*))ScriptableObject_CreateInstance_TisRuntimeObject_m58643206_gshared)(__this /* static, unused */, method)
// System.Void Vuforia.VuforiaAbstractConfiguration::.ctor()
extern "C"  void VuforiaAbstractConfiguration__ctor_m2046194188 (VuforiaAbstractConfiguration_t974438950 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.VuforiaUnity::SetStandardInitializationParameters()
extern "C"  void VuforiaUnity_SetStandardInitializationParameters_m2442996971 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// Vuforia.IUnityPlayer Vuforia.VuforiaRuntimeInitialization::CreateUnityPlayer()
extern "C"  RuntimeObject* VuforiaRuntimeInitialization_CreateUnityPlayer_m667590709 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.VuforiaRuntime::InitPlatform(Vuforia.IUnityPlayer)
extern "C"  void VuforiaRuntime_InitPlatform_m1211554959 (VuforiaRuntime_t2131788666 * __this, RuntimeObject* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// Vuforia.VuforiaAbstractConfiguration Vuforia.VuforiaAbstractConfiguration::get_Instance()
extern "C"  VuforiaAbstractConfiguration_t974438950 * VuforiaAbstractConfiguration_get_Instance_m2225166302 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// Vuforia.VuforiaAbstractConfiguration/GenericVuforiaConfiguration Vuforia.VuforiaAbstractConfiguration::get_Vuforia()
extern "C"  GenericVuforiaConfiguration_t1574456586 * VuforiaAbstractConfiguration_get_Vuforia_m3071337362 (VuforiaAbstractConfiguration_t974438950 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.VuforiaAbstractConfiguration/GenericVuforiaConfiguration::get_DelayedInitialization()
extern "C"  bool GenericVuforiaConfiguration_get_DelayedInitialization_m2669455364 (GenericVuforiaConfiguration_t1574456586 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.VuforiaRuntime::InitVuforia()
extern "C"  void VuforiaRuntime_InitVuforia_m2414741092 (VuforiaRuntime_t2131788666 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.NullUnityPlayer::.ctor()
extern "C"  void NullUnityPlayer__ctor_m1922586283 (NullUnityPlayer_t1636390204 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.RuntimePlatform UnityEngine.Application::get_platform()
extern "C"  int32_t Application_get_platform_m262417742 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.AndroidUnityPlayer::.ctor()
extern "C"  void AndroidUnityPlayer__ctor_m3800978023 (AndroidUnityPlayer_t3571566439 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.IOSUnityPlayer::.ctor()
extern "C"  void IOSUnityPlayer__ctor_m3437806645 (IOSUnityPlayer_t1005570149 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.VuforiaRuntimeUtilities::IsPlayMode()
extern "C"  bool VuforiaRuntimeUtilities_IsPlayMode_m181100841 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.PlayModeUnityPlayer::.ctor()
extern "C"  void PlayModeUnityPlayer__ctor_m2846066907 (PlayModeUnityPlayer_t2581155120 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.VuforiaRuntimeUtilities::IsWSARuntime()
extern "C"  bool VuforiaRuntimeUtilities_IsWSARuntime_m948106021 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.WSAUnityPlayer::.ctor()
extern "C"  void WSAUnityPlayer__ctor_m1316825009 (WSAUnityPlayer_t1576425251 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.VuMarkAbstractBehaviour::.ctor()
extern "C"  void VuMarkAbstractBehaviour__ctor_m1676613122 (VuMarkAbstractBehaviour_t2652615854 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color UnityEngine.Color::get_green()
extern "C"  Color_t2507026410  Color_get_green_m3178998959 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Material::.ctor(UnityEngine.Material)
extern "C"  void Material__ctor_m2014895328 (Material_t1079520667 * __this, Material_t1079520667 * p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Debug::LogWarning(System.Object)
extern "C"  void Debug_LogWarning_m1906964694 (RuntimeObject * __this /* static, unused */, RuntimeObject * p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// Vuforia.VuforiaManager Vuforia.VuforiaManager::get_Instance()
extern "C"  VuforiaManager_t745032010 * VuforiaManager_get_Instance_m2308098604 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !!0[] UnityEngine.GameObject::GetComponentsInChildren<UnityEngine.Camera>()
#define GameObject_GetComponentsInChildren_TisCamera_t3175186167_m4240236485(__this, method) ((  CameraU5BU5D_t1519774542* (*) (GameObject_t1811656094 *, const RuntimeMethod*))GameObject_GetComponentsInChildren_TisRuntimeObject_m4155885061_gshared)(__this, method)
// UnityEngine.Camera UnityEngine.Camera::get_current()
extern "C"  Camera_t3175186167 * Camera_get_current_m3517990316 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Mesh UnityEngine.MeshFilter::get_sharedMesh()
extern "C"  Mesh_t996500909 * MeshFilter_get_sharedMesh_m306256552 (MeshFilter_t137687116 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3[] UnityEngine.Mesh::get_vertices()
extern "C"  Vector3U5BU5D_t981680887* Mesh_get_vertices_m1276761327 (Mesh_t996500909 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32[] UnityEngine.Mesh::get_triangles()
extern "C"  Int32U5BU5D_t2324750880* Mesh_get_triangles_m2785445945 (Mesh_t996500909 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GL::PushMatrix()
extern "C"  void GL_PushMatrix_m3818946253 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Matrix4x4 UnityEngine.Transform::get_localToWorldMatrix()
extern "C"  Matrix4x4_t4266809202  Transform_get_localToWorldMatrix_m2977818424 (Transform_t3316442598 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GL::MultMatrix(UnityEngine.Matrix4x4)
extern "C"  void GL_MultMatrix_m1086021952 (RuntimeObject * __this /* static, unused */, Matrix4x4_t4266809202  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Material::SetPass(System.Int32)
extern "C"  bool Material_SetPass_m924837141 (Material_t1079520667 * __this, int32_t p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Material::SetColor(System.String,UnityEngine.Color)
extern "C"  void Material_SetColor_m4127411957 (Material_t1079520667 * __this, String_t* p0, Color_t2507026410  p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GL::Begin(System.Int32)
extern "C"  void GL_Begin_m2439050522 (RuntimeObject * __this /* static, unused */, int32_t p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GL::Vertex(UnityEngine.Vector3)
extern "C"  void GL_Vertex_m3910287719 (RuntimeObject * __this /* static, unused */, Vector3_t2903530434  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GL::End()
extern "C"  void GL_End_m1567383974 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GL::PopMatrix()
extern "C"  void GL_PopMatrix_m2336262285 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Behaviour::get_enabled()
extern "C"  bool Behaviour_get_enabled_m2220542426 (Behaviour_t363748010 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform UnityEngine.GameObject::get_transform()
extern "C"  Transform_t3316442598 * GameObject_get_transform_m166152100 (GameObject_t1811656094 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Quaternion UnityEngine.Transform::get_rotation()
extern "C"  Quaternion_t754065749  Transform_get_rotation_m2679542584 (Transform_t3316442598 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Transform::get_lossyScale()
extern "C"  Vector3_t2903530434  Transform_get_lossyScale_m3456855661 (Transform_t3316442598 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::TRS(UnityEngine.Vector3,UnityEngine.Quaternion,UnityEngine.Vector3)
extern "C"  Matrix4x4_t4266809202  Matrix4x4_TRS_m4277632264 (RuntimeObject * __this /* static, unused */, Vector3_t2903530434  p0, Quaternion_t754065749  p1, Vector3_t2903530434  p2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Gizmos::set_matrix(UnityEngine.Matrix4x4)
extern "C"  void Gizmos_set_matrix_m2928965184 (RuntimeObject * __this /* static, unused */, Matrix4x4_t4266809202  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Gizmos::set_color(UnityEngine.Color)
extern "C"  void Gizmos_set_color_m1207670475 (RuntimeObject * __this /* static, unused */, Color_t2507026410  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Gizmos::DrawLine(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  void Gizmos_DrawLine_m1716497926 (RuntimeObject * __this /* static, unused */, Vector3_t2903530434  p0, Vector3_t2903530434  p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.WireframeTrackableEventHandler::OnTrackingFound()
extern "C"  void WireframeTrackableEventHandler_OnTrackingFound_m2916522252 (WireframeTrackableEventHandler_t680679714 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.WireframeTrackableEventHandler::OnTrackingLost()
extern "C"  void WireframeTrackableEventHandler_OnTrackingLost_m1252073333 (WireframeTrackableEventHandler_t680679714 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !!0[] UnityEngine.Component::GetComponentsInChildren<Vuforia.WireframeBehaviour>(System.Boolean)
#define Component_GetComponentsInChildren_TisWireframeBehaviour_t1899115476_m606499660(__this, p0, method) ((  WireframeBehaviourU5BU5D_t1741061725* (*) (Component_t4087199522 *, bool, const RuntimeMethod*))Component_GetComponentsInChildren_TisRuntimeObject_m2093175410_gshared)(__this, p0, method)
// System.Void UnityEngine.Behaviour::set_enabled(System.Boolean)
extern "C"  void Behaviour_set_enabled_m1710894011 (Behaviour_t363748010 * __this, bool p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.WordAbstractBehaviour::.ctor()
extern "C"  void WordAbstractBehaviour__ctor_m147923421 (WordAbstractBehaviour_t2339709601 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.WSAUnityPlayer::setPlatFormNative()
extern "C"  void WSAUnityPlayer_setPlatFormNative_m3380749227 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.WSAUnityPlayer::initVuforiaWSA(System.String)
extern "C"  int32_t WSAUnityPlayer_initVuforiaWSA_m1079303392 (RuntimeObject * __this /* static, unused */, String_t* ___licenseKey0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.WSAUnityPlayer::InitializeSurface()
extern "C"  void WSAUnityPlayer_InitializeSurface_m3924250633 (WSAUnityPlayer_t1576425251 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.ScreenOrientation Vuforia.WSAUnityPlayer::GetActualScreenOrientation()
extern "C"  int32_t WSAUnityPlayer_GetActualScreenOrientation_m436681916 (WSAUnityPlayer_t1576425251 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.WSAUnityPlayer::SetUnityScreenOrientation()
extern "C"  void WSAUnityPlayer_SetUnityScreenOrientation_m2430850923 (WSAUnityPlayer_t1576425251 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.WSAUnityPlayer::setSurfaceOrientationWSA(System.Int32)
extern "C"  void WSAUnityPlayer_setSurfaceOrientationWSA_m586800739 (RuntimeObject * __this /* static, unused */, int32_t ___screenOrientation0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.DeviceOrientation UnityEngine.Input::get_deviceOrientation()
extern "C"  int32_t Input_get_deviceOrientation_m549772300 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void SpiderController::.ctor()
extern "C"  void SpiderController__ctor_m308829737 (SpiderController_t373354123 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m620219618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SpiderController::Start()
extern "C"  void SpiderController_Start_m602803498 (SpiderController_t373354123 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SpiderController_Start_m602803498_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Rigidbody_t2492269564 * L_0 = Component_GetComponent_TisRigidbody_t2492269564_m3707284554(__this, /*hidden argument*/Component_GetComponent_TisRigidbody_t2492269564_m3707284554_RuntimeMethod_var);
		__this->set_rb_2(L_0);
		Animation_t2605054404 * L_1 = Component_GetComponent_TisAnimation_t2605054404_m2540927662(__this, /*hidden argument*/Component_GetComponent_TisAnimation_t2605054404_m2540927662_RuntimeMethod_var);
		__this->set_anim_3(L_1);
		return;
	}
}
// System.Void SpiderController::Update()
extern "C"  void SpiderController_Update_m403246748 (SpiderController_t373354123 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SpiderController_Update_m403246748_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	Vector3_t2903530434  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Vector3_t2903530434  V_3;
	memset(&V_3, 0, sizeof(V_3));
	Vector3_t2903530434  V_4;
	memset(&V_4, 0, sizeof(V_4));
	{
		IL2CPP_RUNTIME_CLASS_INIT(CrossPlatformInputManager_t2677747245_il2cpp_TypeInfo_var);
		float L_0 = CrossPlatformInputManager_GetAxis_m3946870782(NULL /*static, unused*/, _stringLiteral224118652, /*hidden argument*/NULL);
		V_0 = L_0;
		float L_1 = CrossPlatformInputManager_GetAxis_m3946870782(NULL /*static, unused*/, _stringLiteral3600647860, /*hidden argument*/NULL);
		V_1 = L_1;
		float L_2 = V_0;
		float L_3 = V_1;
		Vector3__ctor_m4053220054((&V_2), L_2, (0.0f), L_3, /*hidden argument*/NULL);
		Rigidbody_t2492269564 * L_4 = __this->get_rb_2();
		Vector3_t2903530434  L_5 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t2903530434_il2cpp_TypeInfo_var);
		Vector3_t2903530434  L_6 = Vector3_op_Multiply_m2063944565(NULL /*static, unused*/, L_5, (3.0f), /*hidden argument*/NULL);
		NullCheck(L_4);
		Rigidbody_set_velocity_m3845336739(L_4, L_6, /*hidden argument*/NULL);
		float L_7 = V_0;
		if ((((float)L_7) == ((float)(0.0f))))
		{
			goto IL_0094;
		}
	}
	{
		float L_8 = V_1;
		if ((((float)L_8) == ((float)(0.0f))))
		{
			goto IL_0094;
		}
	}
	{
		Transform_t3316442598 * L_9 = Component_get_transform_m2038396632(__this, /*hidden argument*/NULL);
		Transform_t3316442598 * L_10 = Component_get_transform_m2038396632(__this, /*hidden argument*/NULL);
		NullCheck(L_10);
		Vector3_t2903530434  L_11 = Transform_get_eulerAngles_m1943903321(L_10, /*hidden argument*/NULL);
		V_3 = L_11;
		float L_12 = (&V_3)->get_x_1();
		float L_13 = V_0;
		float L_14 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t4094287654_il2cpp_TypeInfo_var);
		float L_15 = atan2f(L_13, L_14);
		Transform_t3316442598 * L_16 = Component_get_transform_m2038396632(__this, /*hidden argument*/NULL);
		NullCheck(L_16);
		Vector3_t2903530434  L_17 = Transform_get_eulerAngles_m1943903321(L_16, /*hidden argument*/NULL);
		V_4 = L_17;
		float L_18 = (&V_4)->get_z_3();
		Vector3_t2903530434  L_19;
		memset(&L_19, 0, sizeof(L_19));
		Vector3__ctor_m4053220054((&L_19), L_12, ((float)((float)L_15*(float)(57.29578f))), L_18, /*hidden argument*/NULL);
		NullCheck(L_9);
		Transform_set_eulerAngles_m1572874495(L_9, L_19, /*hidden argument*/NULL);
	}

IL_0094:
	{
		float L_20 = V_0;
		if ((!(((float)L_20) == ((float)(0.0f)))))
		{
			goto IL_00aa;
		}
	}
	{
		float L_21 = V_1;
		if ((((float)L_21) == ((float)(0.0f))))
		{
			goto IL_00c0;
		}
	}

IL_00aa:
	{
		Animation_t2605054404 * L_22 = __this->get_anim_3();
		NullCheck(L_22);
		Animation_Play_m3576938708(L_22, _stringLiteral1036232234, /*hidden argument*/NULL);
		goto IL_00d1;
	}

IL_00c0:
	{
		Animation_t2605054404 * L_23 = __this->get_anim_3();
		NullCheck(L_23);
		Animation_Play_m3576938708(L_23, _stringLiteral1625232036, /*hidden argument*/NULL);
	}

IL_00d1:
	{
		return;
	}
}
// System.Void VRIntegrationHelper::.ctor()
extern "C"  void VRIntegrationHelper__ctor_m1226847501 (VRIntegrationHelper_t1870100115 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m620219618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VRIntegrationHelper::Awake()
extern "C"  void VRIntegrationHelper_Awake_m2060921871 (VRIntegrationHelper_t1870100115 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VRIntegrationHelper_Awake_m2060921871_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Camera_t3175186167 * L_0 = Component_GetComponent_TisCamera_t3175186167_m3957679396(__this, /*hidden argument*/Component_GetComponent_TisCamera_t3175186167_m3957679396_RuntimeMethod_var);
		NullCheck(L_0);
		Camera_set_fieldOfView_m337251087(L_0, (90.0f), /*hidden argument*/NULL);
		return;
	}
}
// System.Void VRIntegrationHelper::Start()
extern "C"  void VRIntegrationHelper_Start_m1782124057 (VRIntegrationHelper_t1870100115 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VRIntegrationHelper_Start_m1782124057_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(VuforiaARController_t1328503142_il2cpp_TypeInfo_var);
		VuforiaARController_t1328503142 * L_0 = VuforiaARController_get_Instance_m738587825(NULL /*static, unused*/, /*hidden argument*/NULL);
		IntPtr_t L_1;
		L_1.set_m_value_0((void*)(void*)VRIntegrationHelper_OnVuforiaStarted_m714206851_RuntimeMethod_var);
		Action_t3619184611 * L_2 = (Action_t3619184611 *)il2cpp_codegen_object_new(Action_t3619184611_il2cpp_TypeInfo_var);
		Action__ctor_m2777219059(L_2, __this, L_1, /*hidden argument*/NULL);
		NullCheck(L_0);
		VuforiaARController_RegisterVuforiaStartedCallback_m2236106145(L_0, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VRIntegrationHelper::OnVuforiaStarted()
extern "C"  void VRIntegrationHelper_OnVuforiaStarted_m714206851 (VRIntegrationHelper_t1870100115 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VRIntegrationHelper_OnVuforiaStarted_m714206851_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(DigitalEyewearARController_t1877756253_il2cpp_TypeInfo_var);
		DigitalEyewearARController_t1877756253 * L_0 = DigitalEyewearARController_get_Instance_m4227562287(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		Camera_t3175186167 * L_1 = DigitalEyewearARController_get_PrimaryCamera_m2496874119(L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(VRIntegrationHelper_t1870100115_il2cpp_TypeInfo_var);
		((VRIntegrationHelper_t1870100115_StaticFields*)il2cpp_codegen_static_fields_for(VRIntegrationHelper_t1870100115_il2cpp_TypeInfo_var))->set_mLeftCamera_4(L_1);
		DigitalEyewearARController_t1877756253 * L_2 = DigitalEyewearARController_get_Instance_m4227562287(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_2);
		Camera_t3175186167 * L_3 = DigitalEyewearARController_get_SecondaryCamera_m1746377300(L_2, /*hidden argument*/NULL);
		((VRIntegrationHelper_t1870100115_StaticFields*)il2cpp_codegen_static_fields_for(VRIntegrationHelper_t1870100115_il2cpp_TypeInfo_var))->set_mRightCamera_5(L_3);
		Camera_t3175186167 * L_4 = ((VRIntegrationHelper_t1870100115_StaticFields*)il2cpp_codegen_static_fields_for(VRIntegrationHelper_t1870100115_il2cpp_TypeInfo_var))->get_mLeftCamera_4();
		NullCheck(L_4);
		HideExcessAreaAbstractBehaviour_t1324413388 * L_5 = Component_GetComponent_TisHideExcessAreaAbstractBehaviour_t1324413388_m2951051384(L_4, /*hidden argument*/Component_GetComponent_TisHideExcessAreaAbstractBehaviour_t1324413388_m2951051384_RuntimeMethod_var);
		((VRIntegrationHelper_t1870100115_StaticFields*)il2cpp_codegen_static_fields_for(VRIntegrationHelper_t1870100115_il2cpp_TypeInfo_var))->set_mLeftExcessAreaBehaviour_6(L_5);
		Camera_t3175186167 * L_6 = ((VRIntegrationHelper_t1870100115_StaticFields*)il2cpp_codegen_static_fields_for(VRIntegrationHelper_t1870100115_il2cpp_TypeInfo_var))->get_mRightCamera_5();
		NullCheck(L_6);
		HideExcessAreaAbstractBehaviour_t1324413388 * L_7 = Component_GetComponent_TisHideExcessAreaAbstractBehaviour_t1324413388_m2951051384(L_6, /*hidden argument*/Component_GetComponent_TisHideExcessAreaAbstractBehaviour_t1324413388_m2951051384_RuntimeMethod_var);
		((VRIntegrationHelper_t1870100115_StaticFields*)il2cpp_codegen_static_fields_for(VRIntegrationHelper_t1870100115_il2cpp_TypeInfo_var))->set_mRightExcessAreaBehaviour_7(L_7);
		return;
	}
}
// System.Void VRIntegrationHelper::LateUpdate()
extern "C"  void VRIntegrationHelper_LateUpdate_m4260068054 (VRIntegrationHelper_t1870100115 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VRIntegrationHelper_LateUpdate_m4260068054_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t2903530434  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Rect_t3436776195  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Vector3_t2903530434  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Vector3_t2903530434  V_3;
	memset(&V_3, 0, sizeof(V_3));
	Vector3_t2903530434  V_4;
	memset(&V_4, 0, sizeof(V_4));
	Rect_t3436776195  V_5;
	memset(&V_5, 0, sizeof(V_5));
	Vector3_t2903530434  V_6;
	memset(&V_6, 0, sizeof(V_6));
	Vector3_t2903530434  V_7;
	memset(&V_7, 0, sizeof(V_7));
	BackgroundPlaneBehaviour_t311858283 * V_8 = NULL;
	{
		bool L_0 = __this->get_IsLeft_12();
		if (!L_0)
		{
			goto IL_02dd;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(VRIntegrationHelper_t1870100115_il2cpp_TypeInfo_var);
		bool L_1 = ((VRIntegrationHelper_t1870100115_StaticFields*)il2cpp_codegen_static_fields_for(VRIntegrationHelper_t1870100115_il2cpp_TypeInfo_var))->get_mLeftCameraDataAcquired_10();
		if (!L_1)
		{
			goto IL_02dd;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(VRIntegrationHelper_t1870100115_il2cpp_TypeInfo_var);
		bool L_2 = ((VRIntegrationHelper_t1870100115_StaticFields*)il2cpp_codegen_static_fields_for(VRIntegrationHelper_t1870100115_il2cpp_TypeInfo_var))->get_mRightCameraDataAcquired_11();
		if (!L_2)
		{
			goto IL_02dd;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(DigitalEyewearARController_t1877756253_il2cpp_TypeInfo_var);
		DigitalEyewearARController_t1877756253 * L_3 = DigitalEyewearARController_get_Instance_m4227562287(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_3);
		Transform_t3316442598 * L_4 = DigitalEyewearARController_get_CentralAnchorPoint_m208802276(L_3, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(VRIntegrationHelper_t1870100115_il2cpp_TypeInfo_var);
		Camera_t3175186167 * L_5 = ((VRIntegrationHelper_t1870100115_StaticFields*)il2cpp_codegen_static_fields_for(VRIntegrationHelper_t1870100115_il2cpp_TypeInfo_var))->get_mLeftCamera_4();
		NullCheck(L_5);
		Transform_t3316442598 * L_6 = Component_get_transform_m2038396632(L_5, /*hidden argument*/NULL);
		NullCheck(L_6);
		Quaternion_t754065749  L_7 = Transform_get_localRotation_m1836132480(L_6, /*hidden argument*/NULL);
		NullCheck(L_4);
		Transform_set_localRotation_m4199788559(L_4, L_7, /*hidden argument*/NULL);
		DigitalEyewearARController_t1877756253 * L_8 = DigitalEyewearARController_get_Instance_m4227562287(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_8);
		Transform_t3316442598 * L_9 = DigitalEyewearARController_get_CentralAnchorPoint_m208802276(L_8, /*hidden argument*/NULL);
		Camera_t3175186167 * L_10 = ((VRIntegrationHelper_t1870100115_StaticFields*)il2cpp_codegen_static_fields_for(VRIntegrationHelper_t1870100115_il2cpp_TypeInfo_var))->get_mLeftCamera_4();
		NullCheck(L_10);
		Transform_t3316442598 * L_11 = Component_get_transform_m2038396632(L_10, /*hidden argument*/NULL);
		NullCheck(L_11);
		Vector3_t2903530434  L_12 = Transform_get_localPosition_m1266101501(L_11, /*hidden argument*/NULL);
		NullCheck(L_9);
		Transform_set_localPosition_m1667085576(L_9, L_12, /*hidden argument*/NULL);
		Camera_t3175186167 * L_13 = ((VRIntegrationHelper_t1870100115_StaticFields*)il2cpp_codegen_static_fields_for(VRIntegrationHelper_t1870100115_il2cpp_TypeInfo_var))->get_mLeftCamera_4();
		NullCheck(L_13);
		Transform_t3316442598 * L_14 = Component_get_transform_m2038396632(L_13, /*hidden argument*/NULL);
		NullCheck(L_14);
		Vector3_t2903530434  L_15 = Transform_get_localPosition_m1266101501(L_14, /*hidden argument*/NULL);
		V_0 = L_15;
		Camera_t3175186167 * L_16 = ((VRIntegrationHelper_t1870100115_StaticFields*)il2cpp_codegen_static_fields_for(VRIntegrationHelper_t1870100115_il2cpp_TypeInfo_var))->get_mLeftCamera_4();
		NullCheck(L_16);
		Rect_t3436776195  L_17 = Camera_get_pixelRect_m1533752293(L_16, /*hidden argument*/NULL);
		V_1 = L_17;
		Camera_t3175186167 * L_18 = ((VRIntegrationHelper_t1870100115_StaticFields*)il2cpp_codegen_static_fields_for(VRIntegrationHelper_t1870100115_il2cpp_TypeInfo_var))->get_mLeftCamera_4();
		NullCheck(L_18);
		Transform_t3316442598 * L_19 = Component_get_transform_m2038396632(L_18, /*hidden argument*/NULL);
		NullCheck(L_19);
		Vector3_t2903530434  L_20 = Transform_get_right_m1220575382(L_19, /*hidden argument*/NULL);
		V_3 = L_20;
		Vector3_t2903530434  L_21 = Vector3_get_normalized_m2996377041((&V_3), /*hidden argument*/NULL);
		Camera_t3175186167 * L_22 = ((VRIntegrationHelper_t1870100115_StaticFields*)il2cpp_codegen_static_fields_for(VRIntegrationHelper_t1870100115_il2cpp_TypeInfo_var))->get_mLeftCamera_4();
		NullCheck(L_22);
		float L_23 = Camera_get_stereoSeparation_m3749102506(L_22, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t2903530434_il2cpp_TypeInfo_var);
		Vector3_t2903530434  L_24 = Vector3_op_Multiply_m2063944565(NULL /*static, unused*/, L_21, L_23, /*hidden argument*/NULL);
		Vector3_t2903530434  L_25 = Vector3_op_Multiply_m2063944565(NULL /*static, unused*/, L_24, (-0.5f), /*hidden argument*/NULL);
		V_2 = L_25;
		Camera_t3175186167 * L_26 = ((VRIntegrationHelper_t1870100115_StaticFields*)il2cpp_codegen_static_fields_for(VRIntegrationHelper_t1870100115_il2cpp_TypeInfo_var))->get_mLeftCamera_4();
		NullCheck(L_26);
		Transform_t3316442598 * L_27 = Component_get_transform_m2038396632(L_26, /*hidden argument*/NULL);
		Camera_t3175186167 * L_28 = ((VRIntegrationHelper_t1870100115_StaticFields*)il2cpp_codegen_static_fields_for(VRIntegrationHelper_t1870100115_il2cpp_TypeInfo_var))->get_mLeftCamera_4();
		NullCheck(L_28);
		Transform_t3316442598 * L_29 = Component_get_transform_m2038396632(L_28, /*hidden argument*/NULL);
		NullCheck(L_29);
		Vector3_t2903530434  L_30 = Transform_get_position_m1291471697(L_29, /*hidden argument*/NULL);
		Vector3_t2903530434  L_31 = V_2;
		Vector3_t2903530434  L_32 = Vector3_op_Addition_m630862342(NULL /*static, unused*/, L_30, L_31, /*hidden argument*/NULL);
		NullCheck(L_27);
		Transform_set_position_m1572818413(L_27, L_32, /*hidden argument*/NULL);
		Camera_t3175186167 * L_33 = ((VRIntegrationHelper_t1870100115_StaticFields*)il2cpp_codegen_static_fields_for(VRIntegrationHelper_t1870100115_il2cpp_TypeInfo_var))->get_mLeftCamera_4();
		Rect_t3436776195  L_34 = ((VRIntegrationHelper_t1870100115_StaticFields*)il2cpp_codegen_static_fields_for(VRIntegrationHelper_t1870100115_il2cpp_TypeInfo_var))->get_mLeftCameraPixelRect_8();
		NullCheck(L_33);
		Camera_set_pixelRect_m2963872222(L_33, L_34, /*hidden argument*/NULL);
		Camera_t3175186167 * L_35 = ((VRIntegrationHelper_t1870100115_StaticFields*)il2cpp_codegen_static_fields_for(VRIntegrationHelper_t1870100115_il2cpp_TypeInfo_var))->get_mRightCamera_5();
		NullCheck(L_35);
		Transform_t3316442598 * L_36 = Component_get_transform_m2038396632(L_35, /*hidden argument*/NULL);
		NullCheck(L_36);
		Vector3_t2903530434  L_37 = Transform_get_localPosition_m1266101501(L_36, /*hidden argument*/NULL);
		V_4 = L_37;
		Camera_t3175186167 * L_38 = ((VRIntegrationHelper_t1870100115_StaticFields*)il2cpp_codegen_static_fields_for(VRIntegrationHelper_t1870100115_il2cpp_TypeInfo_var))->get_mRightCamera_5();
		NullCheck(L_38);
		Rect_t3436776195  L_39 = Camera_get_pixelRect_m1533752293(L_38, /*hidden argument*/NULL);
		V_5 = L_39;
		Camera_t3175186167 * L_40 = ((VRIntegrationHelper_t1870100115_StaticFields*)il2cpp_codegen_static_fields_for(VRIntegrationHelper_t1870100115_il2cpp_TypeInfo_var))->get_mRightCamera_5();
		NullCheck(L_40);
		Transform_t3316442598 * L_41 = Component_get_transform_m2038396632(L_40, /*hidden argument*/NULL);
		NullCheck(L_41);
		Vector3_t2903530434  L_42 = Transform_get_right_m1220575382(L_41, /*hidden argument*/NULL);
		V_7 = L_42;
		Vector3_t2903530434  L_43 = Vector3_get_normalized_m2996377041((&V_7), /*hidden argument*/NULL);
		Camera_t3175186167 * L_44 = ((VRIntegrationHelper_t1870100115_StaticFields*)il2cpp_codegen_static_fields_for(VRIntegrationHelper_t1870100115_il2cpp_TypeInfo_var))->get_mRightCamera_5();
		NullCheck(L_44);
		float L_45 = Camera_get_stereoSeparation_m3749102506(L_44, /*hidden argument*/NULL);
		Vector3_t2903530434  L_46 = Vector3_op_Multiply_m2063944565(NULL /*static, unused*/, L_43, L_45, /*hidden argument*/NULL);
		Vector3_t2903530434  L_47 = Vector3_op_Multiply_m2063944565(NULL /*static, unused*/, L_46, (0.5f), /*hidden argument*/NULL);
		V_6 = L_47;
		Camera_t3175186167 * L_48 = ((VRIntegrationHelper_t1870100115_StaticFields*)il2cpp_codegen_static_fields_for(VRIntegrationHelper_t1870100115_il2cpp_TypeInfo_var))->get_mRightCamera_5();
		NullCheck(L_48);
		Transform_t3316442598 * L_49 = Component_get_transform_m2038396632(L_48, /*hidden argument*/NULL);
		Camera_t3175186167 * L_50 = ((VRIntegrationHelper_t1870100115_StaticFields*)il2cpp_codegen_static_fields_for(VRIntegrationHelper_t1870100115_il2cpp_TypeInfo_var))->get_mRightCamera_5();
		NullCheck(L_50);
		Transform_t3316442598 * L_51 = Component_get_transform_m2038396632(L_50, /*hidden argument*/NULL);
		NullCheck(L_51);
		Vector3_t2903530434  L_52 = Transform_get_position_m1291471697(L_51, /*hidden argument*/NULL);
		Vector3_t2903530434  L_53 = V_6;
		Vector3_t2903530434  L_54 = Vector3_op_Addition_m630862342(NULL /*static, unused*/, L_52, L_53, /*hidden argument*/NULL);
		NullCheck(L_49);
		Transform_set_position_m1572818413(L_49, L_54, /*hidden argument*/NULL);
		Camera_t3175186167 * L_55 = ((VRIntegrationHelper_t1870100115_StaticFields*)il2cpp_codegen_static_fields_for(VRIntegrationHelper_t1870100115_il2cpp_TypeInfo_var))->get_mRightCamera_5();
		Rect_t3436776195  L_56 = ((VRIntegrationHelper_t1870100115_StaticFields*)il2cpp_codegen_static_fields_for(VRIntegrationHelper_t1870100115_il2cpp_TypeInfo_var))->get_mRightCameraPixelRect_9();
		NullCheck(L_55);
		Camera_set_pixelRect_m2963872222(L_55, L_56, /*hidden argument*/NULL);
		Camera_t3175186167 * L_57 = ((VRIntegrationHelper_t1870100115_StaticFields*)il2cpp_codegen_static_fields_for(VRIntegrationHelper_t1870100115_il2cpp_TypeInfo_var))->get_mLeftCamera_4();
		NullCheck(L_57);
		BackgroundPlaneBehaviour_t311858283 * L_58 = Component_GetComponentInChildren_TisBackgroundPlaneBehaviour_t311858283_m2328091248(L_57, /*hidden argument*/Component_GetComponentInChildren_TisBackgroundPlaneBehaviour_t311858283_m2328091248_RuntimeMethod_var);
		V_8 = L_58;
		BackgroundPlaneBehaviour_t311858283 * L_59 = V_8;
		Camera_t3175186167 * L_60 = ((VRIntegrationHelper_t1870100115_StaticFields*)il2cpp_codegen_static_fields_for(VRIntegrationHelper_t1870100115_il2cpp_TypeInfo_var))->get_mLeftCamera_4();
		NullCheck(L_60);
		Transform_t3316442598 * L_61 = Component_get_transform_m2038396632(L_60, /*hidden argument*/NULL);
		NullCheck(L_61);
		Vector3_t2903530434  L_62 = Transform_get_position_m1291471697(L_61, /*hidden argument*/NULL);
		DigitalEyewearARController_t1877756253 * L_63 = DigitalEyewearARController_get_Instance_m4227562287(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_63);
		Transform_t3316442598 * L_64 = DigitalEyewearARController_get_CentralAnchorPoint_m208802276(L_63, /*hidden argument*/NULL);
		NullCheck(L_64);
		Vector3_t2903530434  L_65 = Transform_get_position_m1291471697(L_64, /*hidden argument*/NULL);
		Vector3_t2903530434  L_66 = Vector3_op_Subtraction_m648404402(NULL /*static, unused*/, L_62, L_65, /*hidden argument*/NULL);
		NullCheck(L_59);
		BackgroundPlaneAbstractBehaviour_set_BackgroundOffset_m1959300025(L_59, L_66, /*hidden argument*/NULL);
		HideExcessAreaAbstractBehaviour_t1324413388 * L_67 = ((VRIntegrationHelper_t1870100115_StaticFields*)il2cpp_codegen_static_fields_for(VRIntegrationHelper_t1870100115_il2cpp_TypeInfo_var))->get_mLeftExcessAreaBehaviour_6();
		Camera_t3175186167 * L_68 = ((VRIntegrationHelper_t1870100115_StaticFields*)il2cpp_codegen_static_fields_for(VRIntegrationHelper_t1870100115_il2cpp_TypeInfo_var))->get_mLeftCamera_4();
		NullCheck(L_68);
		Transform_t3316442598 * L_69 = Component_get_transform_m2038396632(L_68, /*hidden argument*/NULL);
		NullCheck(L_69);
		Vector3_t2903530434  L_70 = Transform_get_position_m1291471697(L_69, /*hidden argument*/NULL);
		DigitalEyewearARController_t1877756253 * L_71 = DigitalEyewearARController_get_Instance_m4227562287(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_71);
		Transform_t3316442598 * L_72 = DigitalEyewearARController_get_CentralAnchorPoint_m208802276(L_71, /*hidden argument*/NULL);
		NullCheck(L_72);
		Vector3_t2903530434  L_73 = Transform_get_position_m1291471697(L_72, /*hidden argument*/NULL);
		Vector3_t2903530434  L_74 = Vector3_op_Subtraction_m648404402(NULL /*static, unused*/, L_70, L_73, /*hidden argument*/NULL);
		NullCheck(L_67);
		HideExcessAreaAbstractBehaviour_set_PlaneOffset_m956571739(L_67, L_74, /*hidden argument*/NULL);
		HideExcessAreaAbstractBehaviour_t1324413388 * L_75 = ((VRIntegrationHelper_t1870100115_StaticFields*)il2cpp_codegen_static_fields_for(VRIntegrationHelper_t1870100115_il2cpp_TypeInfo_var))->get_mRightExcessAreaBehaviour_7();
		Camera_t3175186167 * L_76 = ((VRIntegrationHelper_t1870100115_StaticFields*)il2cpp_codegen_static_fields_for(VRIntegrationHelper_t1870100115_il2cpp_TypeInfo_var))->get_mRightCamera_5();
		NullCheck(L_76);
		Transform_t3316442598 * L_77 = Component_get_transform_m2038396632(L_76, /*hidden argument*/NULL);
		NullCheck(L_77);
		Vector3_t2903530434  L_78 = Transform_get_position_m1291471697(L_77, /*hidden argument*/NULL);
		DigitalEyewearARController_t1877756253 * L_79 = DigitalEyewearARController_get_Instance_m4227562287(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_79);
		Transform_t3316442598 * L_80 = DigitalEyewearARController_get_CentralAnchorPoint_m208802276(L_79, /*hidden argument*/NULL);
		NullCheck(L_80);
		Vector3_t2903530434  L_81 = Transform_get_position_m1291471697(L_80, /*hidden argument*/NULL);
		Vector3_t2903530434  L_82 = Vector3_op_Subtraction_m648404402(NULL /*static, unused*/, L_78, L_81, /*hidden argument*/NULL);
		NullCheck(L_75);
		HideExcessAreaAbstractBehaviour_set_PlaneOffset_m956571739(L_75, L_82, /*hidden argument*/NULL);
		Transform_t3316442598 * L_83 = __this->get_TrackableParent_13();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1502412432_il2cpp_TypeInfo_var);
		bool L_84 = Object_op_Inequality_m1918954159(NULL /*static, unused*/, L_83, (Object_t1502412432 *)NULL, /*hidden argument*/NULL);
		if (!L_84)
		{
			goto IL_020f;
		}
	}
	{
		Transform_t3316442598 * L_85 = __this->get_TrackableParent_13();
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t2903530434_il2cpp_TypeInfo_var);
		Vector3_t2903530434  L_86 = Vector3_get_zero_m2238342052(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_85);
		Transform_set_localPosition_m1667085576(L_85, L_86, /*hidden argument*/NULL);
	}

IL_020f:
	{
		IL2CPP_RUNTIME_CLASS_INIT(VuforiaARController_t1328503142_il2cpp_TypeInfo_var);
		VuforiaARController_t1328503142 * L_87 = VuforiaARController_get_Instance_m738587825(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_87);
		VuforiaARController_UpdateState_m3945451095(L_87, (bool)0, (bool)1, /*hidden argument*/NULL);
		Transform_t3316442598 * L_88 = __this->get_TrackableParent_13();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1502412432_il2cpp_TypeInfo_var);
		bool L_89 = Object_op_Inequality_m1918954159(NULL /*static, unused*/, L_88, (Object_t1502412432 *)NULL, /*hidden argument*/NULL);
		if (!L_89)
		{
			goto IL_0249;
		}
	}
	{
		Transform_t3316442598 * L_90 = __this->get_TrackableParent_13();
		Transform_t3316442598 * L_91 = L_90;
		NullCheck(L_91);
		Vector3_t2903530434  L_92 = Transform_get_position_m1291471697(L_91, /*hidden argument*/NULL);
		BackgroundPlaneBehaviour_t311858283 * L_93 = V_8;
		NullCheck(L_93);
		Vector3_t2903530434  L_94 = BackgroundPlaneAbstractBehaviour_get_BackgroundOffset_m3877967905(L_93, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t2903530434_il2cpp_TypeInfo_var);
		Vector3_t2903530434  L_95 = Vector3_op_Addition_m630862342(NULL /*static, unused*/, L_92, L_94, /*hidden argument*/NULL);
		NullCheck(L_91);
		Transform_set_position_m1572818413(L_91, L_95, /*hidden argument*/NULL);
	}

IL_0249:
	{
		IL2CPP_RUNTIME_CLASS_INIT(VuforiaARController_t1328503142_il2cpp_TypeInfo_var);
		VuforiaARController_t1328503142 * L_96 = VuforiaARController_get_Instance_m738587825(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(VRIntegrationHelper_t1870100115_il2cpp_TypeInfo_var);
		Matrix4x4_t4266809202  L_97 = ((VRIntegrationHelper_t1870100115_StaticFields*)il2cpp_codegen_static_fields_for(VRIntegrationHelper_t1870100115_il2cpp_TypeInfo_var))->get_mLeftCameraMatrixOriginal_2();
		NullCheck(L_96);
		VuforiaARController_ApplyCorrectedProjectionMatrix_m4248414074(L_96, L_97, (bool)1, /*hidden argument*/NULL);
		VuforiaARController_t1328503142 * L_98 = VuforiaARController_get_Instance_m738587825(NULL /*static, unused*/, /*hidden argument*/NULL);
		Matrix4x4_t4266809202  L_99 = ((VRIntegrationHelper_t1870100115_StaticFields*)il2cpp_codegen_static_fields_for(VRIntegrationHelper_t1870100115_il2cpp_TypeInfo_var))->get_mRightCameraMatrixOriginal_3();
		NullCheck(L_98);
		VuforiaARController_ApplyCorrectedProjectionMatrix_m4248414074(L_98, L_99, (bool)0, /*hidden argument*/NULL);
		Camera_t3175186167 * L_100 = ((VRIntegrationHelper_t1870100115_StaticFields*)il2cpp_codegen_static_fields_for(VRIntegrationHelper_t1870100115_il2cpp_TypeInfo_var))->get_mLeftCamera_4();
		Camera_t3175186167 * L_101 = ((VRIntegrationHelper_t1870100115_StaticFields*)il2cpp_codegen_static_fields_for(VRIntegrationHelper_t1870100115_il2cpp_TypeInfo_var))->get_mLeftCamera_4();
		NullCheck(L_101);
		Matrix4x4_t4266809202  L_102 = Camera_get_projectionMatrix_m1935965422(L_101, /*hidden argument*/NULL);
		Camera_t3175186167 * L_103 = ((VRIntegrationHelper_t1870100115_StaticFields*)il2cpp_codegen_static_fields_for(VRIntegrationHelper_t1870100115_il2cpp_TypeInfo_var))->get_mRightCamera_5();
		NullCheck(L_103);
		Matrix4x4_t4266809202  L_104 = Camera_get_projectionMatrix_m1935965422(L_103, /*hidden argument*/NULL);
		NullCheck(L_100);
		Camera_SetStereoProjectionMatrices_m3898755345(L_100, L_102, L_104, /*hidden argument*/NULL);
		Camera_t3175186167 * L_105 = ((VRIntegrationHelper_t1870100115_StaticFields*)il2cpp_codegen_static_fields_for(VRIntegrationHelper_t1870100115_il2cpp_TypeInfo_var))->get_mRightCamera_5();
		Camera_t3175186167 * L_106 = ((VRIntegrationHelper_t1870100115_StaticFields*)il2cpp_codegen_static_fields_for(VRIntegrationHelper_t1870100115_il2cpp_TypeInfo_var))->get_mLeftCamera_4();
		NullCheck(L_106);
		Matrix4x4_t4266809202  L_107 = Camera_get_projectionMatrix_m1935965422(L_106, /*hidden argument*/NULL);
		Camera_t3175186167 * L_108 = ((VRIntegrationHelper_t1870100115_StaticFields*)il2cpp_codegen_static_fields_for(VRIntegrationHelper_t1870100115_il2cpp_TypeInfo_var))->get_mRightCamera_5();
		NullCheck(L_108);
		Matrix4x4_t4266809202  L_109 = Camera_get_projectionMatrix_m1935965422(L_108, /*hidden argument*/NULL);
		NullCheck(L_105);
		Camera_SetStereoProjectionMatrices_m3898755345(L_105, L_107, L_109, /*hidden argument*/NULL);
		Camera_t3175186167 * L_110 = ((VRIntegrationHelper_t1870100115_StaticFields*)il2cpp_codegen_static_fields_for(VRIntegrationHelper_t1870100115_il2cpp_TypeInfo_var))->get_mLeftCamera_4();
		NullCheck(L_110);
		Transform_t3316442598 * L_111 = Component_get_transform_m2038396632(L_110, /*hidden argument*/NULL);
		Vector3_t2903530434  L_112 = V_0;
		NullCheck(L_111);
		Transform_set_localPosition_m1667085576(L_111, L_112, /*hidden argument*/NULL);
		Camera_t3175186167 * L_113 = ((VRIntegrationHelper_t1870100115_StaticFields*)il2cpp_codegen_static_fields_for(VRIntegrationHelper_t1870100115_il2cpp_TypeInfo_var))->get_mLeftCamera_4();
		Rect_t3436776195  L_114 = V_1;
		NullCheck(L_113);
		Camera_set_pixelRect_m2963872222(L_113, L_114, /*hidden argument*/NULL);
		Camera_t3175186167 * L_115 = ((VRIntegrationHelper_t1870100115_StaticFields*)il2cpp_codegen_static_fields_for(VRIntegrationHelper_t1870100115_il2cpp_TypeInfo_var))->get_mRightCamera_5();
		NullCheck(L_115);
		Transform_t3316442598 * L_116 = Component_get_transform_m2038396632(L_115, /*hidden argument*/NULL);
		Vector3_t2903530434  L_117 = V_4;
		NullCheck(L_116);
		Transform_set_localPosition_m1667085576(L_116, L_117, /*hidden argument*/NULL);
		Camera_t3175186167 * L_118 = ((VRIntegrationHelper_t1870100115_StaticFields*)il2cpp_codegen_static_fields_for(VRIntegrationHelper_t1870100115_il2cpp_TypeInfo_var))->get_mRightCamera_5();
		Rect_t3436776195  L_119 = V_5;
		NullCheck(L_118);
		Camera_set_pixelRect_m2963872222(L_118, L_119, /*hidden argument*/NULL);
	}

IL_02dd:
	{
		return;
	}
}
// System.Void VRIntegrationHelper::OnPreRender()
extern "C"  void VRIntegrationHelper_OnPreRender_m857326075 (VRIntegrationHelper_t1870100115 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VRIntegrationHelper_OnPreRender_m857326075_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = __this->get_IsLeft_12();
		if (!L_0)
		{
			goto IL_0052;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(VRIntegrationHelper_t1870100115_il2cpp_TypeInfo_var);
		bool L_1 = ((VRIntegrationHelper_t1870100115_StaticFields*)il2cpp_codegen_static_fields_for(VRIntegrationHelper_t1870100115_il2cpp_TypeInfo_var))->get_mLeftCameraDataAcquired_10();
		if (L_1)
		{
			goto IL_0052;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(VRIntegrationHelper_t1870100115_il2cpp_TypeInfo_var);
		Camera_t3175186167 * L_2 = ((VRIntegrationHelper_t1870100115_StaticFields*)il2cpp_codegen_static_fields_for(VRIntegrationHelper_t1870100115_il2cpp_TypeInfo_var))->get_mLeftCamera_4();
		NullCheck(L_2);
		Matrix4x4_t4266809202  L_3 = Camera_get_projectionMatrix_m1935965422(L_2, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(VuforiaRuntimeUtilities_t1036756025_il2cpp_TypeInfo_var);
		bool L_4 = VuforiaRuntimeUtilities_MatrixIsNaN_m1840818379(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		if (L_4)
		{
			goto IL_004d;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(VRIntegrationHelper_t1870100115_il2cpp_TypeInfo_var);
		Camera_t3175186167 * L_5 = ((VRIntegrationHelper_t1870100115_StaticFields*)il2cpp_codegen_static_fields_for(VRIntegrationHelper_t1870100115_il2cpp_TypeInfo_var))->get_mLeftCamera_4();
		NullCheck(L_5);
		Matrix4x4_t4266809202  L_6 = Camera_get_projectionMatrix_m1935965422(L_5, /*hidden argument*/NULL);
		((VRIntegrationHelper_t1870100115_StaticFields*)il2cpp_codegen_static_fields_for(VRIntegrationHelper_t1870100115_il2cpp_TypeInfo_var))->set_mLeftCameraMatrixOriginal_2(L_6);
		Camera_t3175186167 * L_7 = ((VRIntegrationHelper_t1870100115_StaticFields*)il2cpp_codegen_static_fields_for(VRIntegrationHelper_t1870100115_il2cpp_TypeInfo_var))->get_mLeftCamera_4();
		NullCheck(L_7);
		Rect_t3436776195  L_8 = Camera_get_pixelRect_m1533752293(L_7, /*hidden argument*/NULL);
		((VRIntegrationHelper_t1870100115_StaticFields*)il2cpp_codegen_static_fields_for(VRIntegrationHelper_t1870100115_il2cpp_TypeInfo_var))->set_mLeftCameraPixelRect_8(L_8);
		((VRIntegrationHelper_t1870100115_StaticFields*)il2cpp_codegen_static_fields_for(VRIntegrationHelper_t1870100115_il2cpp_TypeInfo_var))->set_mLeftCameraDataAcquired_10((bool)1);
	}

IL_004d:
	{
		goto IL_0094;
	}

IL_0052:
	{
		IL2CPP_RUNTIME_CLASS_INIT(VRIntegrationHelper_t1870100115_il2cpp_TypeInfo_var);
		bool L_9 = ((VRIntegrationHelper_t1870100115_StaticFields*)il2cpp_codegen_static_fields_for(VRIntegrationHelper_t1870100115_il2cpp_TypeInfo_var))->get_mRightCameraDataAcquired_11();
		if (L_9)
		{
			goto IL_0094;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(VRIntegrationHelper_t1870100115_il2cpp_TypeInfo_var);
		Camera_t3175186167 * L_10 = ((VRIntegrationHelper_t1870100115_StaticFields*)il2cpp_codegen_static_fields_for(VRIntegrationHelper_t1870100115_il2cpp_TypeInfo_var))->get_mRightCamera_5();
		NullCheck(L_10);
		Matrix4x4_t4266809202  L_11 = Camera_get_projectionMatrix_m1935965422(L_10, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(VuforiaRuntimeUtilities_t1036756025_il2cpp_TypeInfo_var);
		bool L_12 = VuforiaRuntimeUtilities_MatrixIsNaN_m1840818379(NULL /*static, unused*/, L_11, /*hidden argument*/NULL);
		if (L_12)
		{
			goto IL_0094;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(VRIntegrationHelper_t1870100115_il2cpp_TypeInfo_var);
		Camera_t3175186167 * L_13 = ((VRIntegrationHelper_t1870100115_StaticFields*)il2cpp_codegen_static_fields_for(VRIntegrationHelper_t1870100115_il2cpp_TypeInfo_var))->get_mRightCamera_5();
		NullCheck(L_13);
		Matrix4x4_t4266809202  L_14 = Camera_get_projectionMatrix_m1935965422(L_13, /*hidden argument*/NULL);
		((VRIntegrationHelper_t1870100115_StaticFields*)il2cpp_codegen_static_fields_for(VRIntegrationHelper_t1870100115_il2cpp_TypeInfo_var))->set_mRightCameraMatrixOriginal_3(L_14);
		Camera_t3175186167 * L_15 = ((VRIntegrationHelper_t1870100115_StaticFields*)il2cpp_codegen_static_fields_for(VRIntegrationHelper_t1870100115_il2cpp_TypeInfo_var))->get_mRightCamera_5();
		NullCheck(L_15);
		Rect_t3436776195  L_16 = Camera_get_pixelRect_m1533752293(L_15, /*hidden argument*/NULL);
		((VRIntegrationHelper_t1870100115_StaticFields*)il2cpp_codegen_static_fields_for(VRIntegrationHelper_t1870100115_il2cpp_TypeInfo_var))->set_mRightCameraPixelRect_9(L_16);
		((VRIntegrationHelper_t1870100115_StaticFields*)il2cpp_codegen_static_fields_for(VRIntegrationHelper_t1870100115_il2cpp_TypeInfo_var))->set_mRightCameraDataAcquired_11((bool)1);
	}

IL_0094:
	{
		return;
	}
}
// System.Void VRIntegrationHelper::.cctor()
extern "C"  void VRIntegrationHelper__cctor_m161908865 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Void Vuforia.AndroidUnityPlayer::.ctor()
extern "C"  void AndroidUnityPlayer__ctor_m3800978023 (AndroidUnityPlayer_t3571566439 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m2906060009(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.AndroidUnityPlayer::LoadNativeLibraries()
extern "C"  void AndroidUnityPlayer_LoadNativeLibraries_m4086938459 (AndroidUnityPlayer_t3571566439 * __this, const RuntimeMethod* method)
{
	{
		AndroidUnityPlayer_LoadNativeLibrariesFromJava_m798436972(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.AndroidUnityPlayer::InitializePlatform()
extern "C"  void AndroidUnityPlayer_InitializePlatform_m3638562233 (AndroidUnityPlayer_t3571566439 * __this, const RuntimeMethod* method)
{
	{
		AndroidUnityPlayer_InitAndroidPlatform_m3919873674(__this, /*hidden argument*/NULL);
		return;
	}
}
// Vuforia.VuforiaUnity/InitError Vuforia.AndroidUnityPlayer::InitializeVuforia(System.String)
extern "C"  int32_t AndroidUnityPlayer_InitializeVuforia_m1842234586 (AndroidUnityPlayer_t3571566439 * __this, String_t* ___licenseKey0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AndroidUnityPlayer_InitializeVuforia_m1842234586_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(VuforiaRenderer_t969713742_il2cpp_TypeInfo_var);
		VuforiaRenderer_t969713742 * L_0 = VuforiaRenderer_get_Instance_m1182630307(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		int32_t L_1 = VirtFuncInvoker0< int32_t >::Invoke(15 /* Vuforia.VuforiaRenderer/RendererAPI Vuforia.VuforiaRenderer::GetRendererAPI() */, L_0);
		V_0 = L_1;
		int32_t L_2 = V_0;
		String_t* L_3 = ___licenseKey0;
		int32_t L_4 = AndroidUnityPlayer_InitVuforia_m1967216672(__this, L_2, L_3, /*hidden argument*/NULL);
		V_1 = L_4;
		int32_t L_5 = V_1;
		if ((((int32_t)L_5) < ((int32_t)0)))
		{
			goto IL_0021;
		}
	}
	{
		AndroidUnityPlayer_InitializeSurface_m325648367(__this, /*hidden argument*/NULL);
	}

IL_0021:
	{
		int32_t L_6 = V_1;
		return (int32_t)(L_6);
	}
}
// System.Void Vuforia.AndroidUnityPlayer::StartScene()
extern "C"  void AndroidUnityPlayer_StartScene_m1168403463 (AndroidUnityPlayer_t3571566439 * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Void Vuforia.AndroidUnityPlayer::Update()
extern "C"  void AndroidUnityPlayer_Update_m829520880 (AndroidUnityPlayer_t3571566439 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AndroidUnityPlayer_Update_m829520880_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SurfaceUtilities_t184061569_il2cpp_TypeInfo_var);
		bool L_0 = SurfaceUtilities_HasSurfaceBeenRecreated_m346027931(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0015;
		}
	}
	{
		AndroidUnityPlayer_InitializeSurface_m325648367(__this, /*hidden argument*/NULL);
		goto IL_0031;
	}

IL_0015:
	{
		int32_t L_1 = Screen_get_orientation_m251094311(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_2 = __this->get_mScreenOrientation_2();
		if ((((int32_t)L_1) == ((int32_t)L_2)))
		{
			goto IL_002b;
		}
	}
	{
		AndroidUnityPlayer_ResetUnityScreenOrientation_m4124764997(__this, /*hidden argument*/NULL);
	}

IL_002b:
	{
		AndroidUnityPlayer_CheckOrientation_m2694551379(__this, /*hidden argument*/NULL);
	}

IL_0031:
	{
		int32_t L_3 = __this->get_mFramesSinceLastOrientationReset_4();
		__this->set_mFramesSinceLastOrientationReset_4(((int32_t)((int32_t)L_3+(int32_t)1)));
		return;
	}
}
// System.Void Vuforia.AndroidUnityPlayer::OnPause()
extern "C"  void AndroidUnityPlayer_OnPause_m3448964043 (AndroidUnityPlayer_t3571566439 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AndroidUnityPlayer_OnPause_m3448964043_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(VuforiaUnity_t3212967305_il2cpp_TypeInfo_var);
		VuforiaUnity_OnPause_m4153953119(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.AndroidUnityPlayer::OnResume()
extern "C"  void AndroidUnityPlayer_OnResume_m3272565274 (AndroidUnityPlayer_t3571566439 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AndroidUnityPlayer_OnResume_m3272565274_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(VuforiaUnity_t3212967305_il2cpp_TypeInfo_var);
		VuforiaUnity_OnResume_m2287209481(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.AndroidUnityPlayer::OnDestroy()
extern "C"  void AndroidUnityPlayer_OnDestroy_m2974002145 (AndroidUnityPlayer_t3571566439 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AndroidUnityPlayer_OnDestroy_m2974002145_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(VuforiaUnity_t3212967305_il2cpp_TypeInfo_var);
		VuforiaUnity_Deinit_m2222663190(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.AndroidUnityPlayer::Dispose()
extern "C"  void AndroidUnityPlayer_Dispose_m3660720261 (AndroidUnityPlayer_t3571566439 * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Void Vuforia.AndroidUnityPlayer::LoadNativeLibrariesFromJava()
extern "C"  void AndroidUnityPlayer_LoadNativeLibrariesFromJava_m798436972 (AndroidUnityPlayer_t3571566439 * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Void Vuforia.AndroidUnityPlayer::InitAndroidPlatform()
extern "C"  void AndroidUnityPlayer_InitAndroidPlatform_m3919873674 (AndroidUnityPlayer_t3571566439 * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Int32 Vuforia.AndroidUnityPlayer::InitVuforia(System.Int32,System.String)
extern "C"  int32_t AndroidUnityPlayer_InitVuforia_m1967216672 (AndroidUnityPlayer_t3571566439 * __this, int32_t ___rendererAPI0, String_t* ___licenseKey1, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		V_0 = (-1);
		int32_t L_0 = V_0;
		return L_0;
	}
}
// System.Void Vuforia.AndroidUnityPlayer::InitializeSurface()
extern "C"  void AndroidUnityPlayer_InitializeSurface_m325648367 (AndroidUnityPlayer_t3571566439 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AndroidUnityPlayer_InitializeSurface_m325648367_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SurfaceUtilities_t184061569_il2cpp_TypeInfo_var);
		SurfaceUtilities_OnSurfaceCreated_m1867899004(NULL /*static, unused*/, /*hidden argument*/NULL);
		AndroidUnityPlayer_ResetUnityScreenOrientation_m4124764997(__this, /*hidden argument*/NULL);
		AndroidUnityPlayer_CheckOrientation_m2694551379(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.AndroidUnityPlayer::ResetUnityScreenOrientation()
extern "C"  void AndroidUnityPlayer_ResetUnityScreenOrientation_m4124764997 (AndroidUnityPlayer_t3571566439 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = Screen_get_orientation_m251094311(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_mScreenOrientation_2(L_0);
		__this->set_mFramesSinceLastOrientationReset_4(0);
		return;
	}
}
// System.Void Vuforia.AndroidUnityPlayer::CheckOrientation()
extern "C"  void AndroidUnityPlayer_CheckOrientation_m2694551379 (AndroidUnityPlayer_t3571566439 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AndroidUnityPlayer_CheckOrientation_m2694551379_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	{
		int32_t L_0 = __this->get_mFramesSinceLastOrientationReset_4();
		V_0 = (bool)((((int32_t)L_0) < ((int32_t)((int32_t)25)))? 1 : 0);
		bool L_1 = V_0;
		if (L_1)
		{
			goto IL_001c;
		}
	}
	{
		int32_t L_2 = __this->get_mFramesSinceLastJavaOrientationCheck_5();
		V_0 = (bool)((((int32_t)L_2) > ((int32_t)((int32_t)60)))? 1 : 0);
	}

IL_001c:
	{
		bool L_3 = V_0;
		if (!L_3)
		{
			goto IL_0055;
		}
	}
	{
		int32_t L_4 = __this->get_mScreenOrientation_2();
		V_1 = L_4;
		int32_t L_5 = V_1;
		V_2 = L_5;
		int32_t L_6 = V_2;
		int32_t L_7 = __this->get_mJavaScreenOrientation_3();
		if ((((int32_t)L_6) == ((int32_t)L_7)))
		{
			goto IL_0049;
		}
	}
	{
		int32_t L_8 = V_2;
		__this->set_mJavaScreenOrientation_3(L_8);
		int32_t L_9 = __this->get_mJavaScreenOrientation_3();
		IL2CPP_RUNTIME_CLASS_INIT(SurfaceUtilities_t184061569_il2cpp_TypeInfo_var);
		SurfaceUtilities_SetSurfaceOrientation_m897924353(NULL /*static, unused*/, L_9, /*hidden argument*/NULL);
	}

IL_0049:
	{
		__this->set_mFramesSinceLastJavaOrientationCheck_5(0);
		goto IL_0063;
	}

IL_0055:
	{
		int32_t L_10 = __this->get_mFramesSinceLastJavaOrientationCheck_5();
		__this->set_mFramesSinceLastJavaOrientationCheck_5(((int32_t)((int32_t)L_10+(int32_t)1)));
	}

IL_0063:
	{
		return;
	}
}
// System.Void Vuforia.BackgroundPlaneBehaviour::.ctor()
extern "C"  void BackgroundPlaneBehaviour__ctor_m3613974601 (BackgroundPlaneBehaviour_t311858283 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (BackgroundPlaneBehaviour__ctor_m3613974601_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(BackgroundPlaneAbstractBehaviour_t4259835756_il2cpp_TypeInfo_var);
		BackgroundPlaneAbstractBehaviour__ctor_m2739397862(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.CloudRecoBehaviour::.ctor()
extern "C"  void CloudRecoBehaviour__ctor_m2952167636 (CloudRecoBehaviour_t2989569978 * __this, const RuntimeMethod* method)
{
	{
		CloudRecoAbstractBehaviour__ctor_m3633727160(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.ComponentFactoryStarterBehaviour::.ctor()
extern "C"  void ComponentFactoryStarterBehaviour__ctor_m3067491312 (ComponentFactoryStarterBehaviour_t2906678938 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m620219618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.ComponentFactoryStarterBehaviour::Awake()
extern "C"  void ComponentFactoryStarterBehaviour_Awake_m3129066026 (ComponentFactoryStarterBehaviour_t2906678938 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ComponentFactoryStarterBehaviour_Awake_m3129066026_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	List_1_t3148643089 * V_0 = NULL;
	MethodInfo_t * V_1 = NULL;
	Enumerator_t931554391  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Attribute_t841672618 * V_3 = NULL;
	ObjectU5BU5D_t3270211303* V_4 = NULL;
	int32_t V_5 = 0;
	Action_t3619184611 * V_6 = NULL;
	Exception_t1975590229 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1975590229 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Type_t * L_0 = Object_GetType_m3774351292(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		MethodInfoU5BU5D_t1774028895* L_1 = VirtFuncInvoker1< MethodInfoU5BU5D_t1774028895*, int32_t >::Invoke(53 /* System.Reflection.MethodInfo[] System.Type::GetMethods(System.Reflection.BindingFlags) */, L_0, ((int32_t)38));
		List_1_t3148643089 * L_2 = Enumerable_ToList_TisMethodInfo_t_m2942684005(NULL /*static, unused*/, (RuntimeObject*)(RuntimeObject*)L_1, /*hidden argument*/Enumerable_ToList_TisMethodInfo_t_m2942684005_RuntimeMethod_var);
		V_0 = L_2;
		List_1_t3148643089 * L_3 = V_0;
		Type_t * L_4 = Object_GetType_m3774351292(__this, /*hidden argument*/NULL);
		NullCheck(L_4);
		MethodInfoU5BU5D_t1774028895* L_5 = VirtFuncInvoker1< MethodInfoU5BU5D_t1774028895*, int32_t >::Invoke(53 /* System.Reflection.MethodInfo[] System.Type::GetMethods(System.Reflection.BindingFlags) */, L_4, ((int32_t)22));
		NullCheck(L_3);
		List_1_AddRange_m2579895835(L_3, (RuntimeObject*)(RuntimeObject*)L_5, /*hidden argument*/List_1_AddRange_m2579895835_RuntimeMethod_var);
		List_1_t3148643089 * L_6 = V_0;
		NullCheck(L_6);
		Enumerator_t931554391  L_7 = List_1_GetEnumerator_m3353760919(L_6, /*hidden argument*/List_1_GetEnumerator_m3353760919_RuntimeMethod_var);
		V_2 = L_7;
	}

IL_002d:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0098;
		}

IL_0032:
		{
			MethodInfo_t * L_8 = Enumerator_get_Current_m1308925728((&V_2), /*hidden argument*/Enumerator_get_Current_m1308925728_RuntimeMethod_var);
			V_1 = L_8;
			MethodInfo_t * L_9 = V_1;
			NullCheck(L_9);
			ObjectU5BU5D_t3270211303* L_10 = VirtFuncInvoker1< ObjectU5BU5D_t3270211303*, bool >::Invoke(12 /* System.Object[] System.Reflection.MemberInfo::GetCustomAttributes(System.Boolean) */, L_9, (bool)1);
			V_4 = L_10;
			V_5 = 0;
			goto IL_008d;
		}

IL_004b:
		{
			ObjectU5BU5D_t3270211303* L_11 = V_4;
			int32_t L_12 = V_5;
			NullCheck(L_11);
			int32_t L_13 = L_12;
			RuntimeObject * L_14 = (L_11)->GetAt(static_cast<il2cpp_array_size_t>(L_13));
			V_3 = ((Attribute_t841672618 *)CastclassClass((RuntimeObject*)L_14, Attribute_t841672618_il2cpp_TypeInfo_var));
			Attribute_t841672618 * L_15 = V_3;
			if (!((FactorySetter_t372426630 *)IsInstClass((RuntimeObject*)L_15, FactorySetter_t372426630_il2cpp_TypeInfo_var)))
			{
				goto IL_0087;
			}
		}

IL_0061:
		{
			IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
			Type_t * L_16 = Type_GetTypeFromHandle_m269683661(NULL /*static, unused*/, LoadTypeToken(Action_t3619184611_0_0_0_var), /*hidden argument*/NULL);
			MethodInfo_t * L_17 = V_1;
			Delegate_t537306192 * L_18 = Delegate_CreateDelegate_m1805921115(NULL /*static, unused*/, L_16, __this, L_17, /*hidden argument*/NULL);
			V_6 = ((Action_t3619184611 *)IsInstSealed((RuntimeObject*)L_18, Action_t3619184611_il2cpp_TypeInfo_var));
			Action_t3619184611 * L_19 = V_6;
			if (!L_19)
			{
				goto IL_0087;
			}
		}

IL_0080:
		{
			Action_t3619184611 * L_20 = V_6;
			NullCheck(L_20);
			Action_Invoke_m4106243596(L_20, /*hidden argument*/NULL);
		}

IL_0087:
		{
			int32_t L_21 = V_5;
			V_5 = ((int32_t)((int32_t)L_21+(int32_t)1));
		}

IL_008d:
		{
			int32_t L_22 = V_5;
			ObjectU5BU5D_t3270211303* L_23 = V_4;
			NullCheck(L_23);
			if ((((int32_t)L_22) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_23)->max_length)))))))
			{
				goto IL_004b;
			}
		}

IL_0098:
		{
			bool L_24 = Enumerator_MoveNext_m1576165614((&V_2), /*hidden argument*/Enumerator_MoveNext_m1576165614_RuntimeMethod_var);
			if (L_24)
			{
				goto IL_0032;
			}
		}

IL_00a4:
		{
			IL2CPP_LEAVE(0xB7, FINALLY_00a9);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1975590229 *)e.ex;
		goto FINALLY_00a9;
	}

FINALLY_00a9:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_m363041641((&V_2), /*hidden argument*/Enumerator_Dispose_m363041641_RuntimeMethod_var);
		IL2CPP_END_FINALLY(169)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(169)
	{
		IL2CPP_JUMP_TBL(0xB7, IL_00b7)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1975590229 *)
	}

IL_00b7:
	{
		return;
	}
}
// System.Void Vuforia.ComponentFactoryStarterBehaviour::SetBehaviourComponentFactory()
extern "C"  void ComponentFactoryStarterBehaviour_SetBehaviourComponentFactory_m3084697717 (ComponentFactoryStarterBehaviour_t2906678938 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ComponentFactoryStarterBehaviour_SetBehaviourComponentFactory_m3084697717_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3525713211_il2cpp_TypeInfo_var);
		Debug_Log_m1673024736(NULL /*static, unused*/, _stringLiteral2946044379, /*hidden argument*/NULL);
		VuforiaBehaviourComponentFactory_t2590131513 * L_0 = (VuforiaBehaviourComponentFactory_t2590131513 *)il2cpp_codegen_object_new(VuforiaBehaviourComponentFactory_t2590131513_il2cpp_TypeInfo_var);
		VuforiaBehaviourComponentFactory__ctor_m2242124886(L_0, /*hidden argument*/NULL);
		BehaviourComponentFactory_set_Instance_m2152858985(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.CylinderTargetBehaviour::.ctor()
extern "C"  void CylinderTargetBehaviour__ctor_m2933608647 (CylinderTargetBehaviour_t1224832031 * __this, const RuntimeMethod* method)
{
	{
		CylinderTargetAbstractBehaviour__ctor_m2234434592(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.DefaultInitializationErrorHandler::.ctor()
extern "C"  void DefaultInitializationErrorHandler__ctor_m1788101465 (DefaultInitializationErrorHandler_t414875900 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DefaultInitializationErrorHandler__ctor_m1788101465_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)il2cpp_codegen_static_fields_for(String_t_il2cpp_TypeInfo_var))->get_Empty_2();
		__this->set_mErrorText_2(L_0);
		MonoBehaviour__ctor_m620219618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.DefaultInitializationErrorHandler::Awake()
extern "C"  void DefaultInitializationErrorHandler_Awake_m1234883766 (DefaultInitializationErrorHandler_t414875900 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DefaultInitializationErrorHandler_Awake_m1234883766_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(VuforiaRuntime_t2131788666_il2cpp_TypeInfo_var);
		VuforiaRuntime_t2131788666 * L_0 = VuforiaRuntime_get_Instance_m3676190030(NULL /*static, unused*/, /*hidden argument*/NULL);
		IntPtr_t L_1;
		L_1.set_m_value_0((void*)(void*)DefaultInitializationErrorHandler_OnVuforiaInitializationError_m1077570325_RuntimeMethod_var);
		Action_1_t3472505684 * L_2 = (Action_1_t3472505684 *)il2cpp_codegen_object_new(Action_1_t3472505684_il2cpp_TypeInfo_var);
		Action_1__ctor_m1316558631(L_2, __this, L_1, /*hidden argument*/Action_1__ctor_m1316558631_RuntimeMethod_var);
		NullCheck(L_0);
		VuforiaRuntime_RegisterVuforiaInitErrorCallback_m788535050(L_0, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.DefaultInitializationErrorHandler::OnGUI()
extern "C"  void DefaultInitializationErrorHandler_OnGUI_m2401365285 (DefaultInitializationErrorHandler_t414875900 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DefaultInitializationErrorHandler_OnGUI_m2401365285_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = __this->get_mErrorOccurred_3();
		if (!L_0)
		{
			goto IL_003e;
		}
	}
	{
		int32_t L_1 = Screen_get_width_m2947011129(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_2 = Screen_get_height_m471732428(NULL /*static, unused*/, /*hidden argument*/NULL);
		Rect_t3436776195  L_3;
		memset(&L_3, 0, sizeof(L_3));
		Rect__ctor_m2728905368((&L_3), (0.0f), (0.0f), (((float)((float)L_1))), (((float)((float)L_2))), /*hidden argument*/NULL);
		IntPtr_t L_4;
		L_4.set_m_value_0((void*)(void*)DefaultInitializationErrorHandler_DrawWindowContent_m704677827_RuntimeMethod_var);
		WindowFunction_t3577962812 * L_5 = (WindowFunction_t3577962812 *)il2cpp_codegen_object_new(WindowFunction_t3577962812_il2cpp_TypeInfo_var);
		WindowFunction__ctor_m3870900743(L_5, __this, L_4, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t1999344246_il2cpp_TypeInfo_var);
		GUI_Window_m2935776987(NULL /*static, unused*/, 0, L_3, L_5, _stringLiteral3886165719, /*hidden argument*/NULL);
	}

IL_003e:
	{
		return;
	}
}
// System.Void Vuforia.DefaultInitializationErrorHandler::OnDestroy()
extern "C"  void DefaultInitializationErrorHandler_OnDestroy_m3701173763 (DefaultInitializationErrorHandler_t414875900 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DefaultInitializationErrorHandler_OnDestroy_m3701173763_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(VuforiaRuntime_t2131788666_il2cpp_TypeInfo_var);
		VuforiaRuntime_t2131788666 * L_0 = VuforiaRuntime_get_Instance_m3676190030(NULL /*static, unused*/, /*hidden argument*/NULL);
		IntPtr_t L_1;
		L_1.set_m_value_0((void*)(void*)DefaultInitializationErrorHandler_OnVuforiaInitializationError_m1077570325_RuntimeMethod_var);
		Action_1_t3472505684 * L_2 = (Action_1_t3472505684 *)il2cpp_codegen_object_new(Action_1_t3472505684_il2cpp_TypeInfo_var);
		Action_1__ctor_m1316558631(L_2, __this, L_1, /*hidden argument*/Action_1__ctor_m1316558631_RuntimeMethod_var);
		NullCheck(L_0);
		VuforiaRuntime_UnregisterVuforiaInitErrorCallback_m810573170(L_0, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.DefaultInitializationErrorHandler::DrawWindowContent(System.Int32)
extern "C"  void DefaultInitializationErrorHandler_DrawWindowContent_m704677827 (DefaultInitializationErrorHandler_t414875900 * __this, int32_t ___id0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DefaultInitializationErrorHandler_DrawWindowContent_m704677827_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = Screen_get_width_m2947011129(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_1 = Screen_get_height_m471732428(NULL /*static, unused*/, /*hidden argument*/NULL);
		Rect_t3436776195  L_2;
		memset(&L_2, 0, sizeof(L_2));
		Rect__ctor_m2728905368((&L_2), (10.0f), (25.0f), (((float)((float)((int32_t)((int32_t)L_0-(int32_t)((int32_t)20)))))), (((float)((float)((int32_t)((int32_t)L_1-(int32_t)((int32_t)95)))))), /*hidden argument*/NULL);
		String_t* L_3 = __this->get_mErrorText_2();
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t1999344246_il2cpp_TypeInfo_var);
		GUI_Label_m2909214338(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		int32_t L_4 = Screen_get_width_m2947011129(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_5 = Screen_get_height_m471732428(NULL /*static, unused*/, /*hidden argument*/NULL);
		Rect_t3436776195  L_6;
		memset(&L_6, 0, sizeof(L_6));
		Rect__ctor_m2728905368((&L_6), (((float)((float)((int32_t)((int32_t)((int32_t)((int32_t)L_4/(int32_t)2))-(int32_t)((int32_t)75)))))), (((float)((float)((int32_t)((int32_t)L_5-(int32_t)((int32_t)60)))))), (150.0f), (50.0f), /*hidden argument*/NULL);
		bool L_7 = GUI_Button_m1337722186(NULL /*static, unused*/, L_6, _stringLiteral3882266322, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0063;
		}
	}
	{
		Application_Quit_m1111605119(NULL /*static, unused*/, /*hidden argument*/NULL);
	}

IL_0063:
	{
		return;
	}
}
// System.Void Vuforia.DefaultInitializationErrorHandler::SetErrorCode(Vuforia.VuforiaUnity/InitError)
extern "C"  void DefaultInitializationErrorHandler_SetErrorCode_m749491405 (DefaultInitializationErrorHandler_t414875900 * __this, int32_t ___errorCode0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DefaultInitializationErrorHandler_SetErrorCode_m749491405_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = __this->get_mErrorText_2();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_1 = String_Concat_m3054569703(NULL /*static, unused*/, _stringLiteral1755214121, L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3525713211_il2cpp_TypeInfo_var);
		Debug_LogError_m1848790000(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		int32_t L_2 = ___errorCode0;
		switch (((int32_t)((int32_t)L_2+(int32_t)((int32_t)10))))
		{
			case 0:
			{
				goto IL_004b;
			}
			case 1:
			{
				goto IL_00ab;
			}
			case 2:
			{
				goto IL_009b;
			}
			case 3:
			{
				goto IL_007b;
			}
			case 4:
			{
				goto IL_008b;
			}
			case 5:
			{
				goto IL_006b;
			}
			case 6:
			{
				goto IL_005b;
			}
			case 7:
			{
				goto IL_00bb;
			}
			case 8:
			{
				goto IL_00cb;
			}
			case 9:
			{
				goto IL_00db;
			}
		}
	}
	{
		goto IL_00eb;
	}

IL_004b:
	{
		__this->set_mErrorText_2(_stringLiteral1508901986);
		goto IL_00eb;
	}

IL_005b:
	{
		__this->set_mErrorText_2(_stringLiteral2113730205);
		goto IL_00eb;
	}

IL_006b:
	{
		__this->set_mErrorText_2(_stringLiteral1527512223);
		goto IL_00eb;
	}

IL_007b:
	{
		__this->set_mErrorText_2(_stringLiteral1250240110);
		goto IL_00eb;
	}

IL_008b:
	{
		__this->set_mErrorText_2(_stringLiteral131575613);
		goto IL_00eb;
	}

IL_009b:
	{
		__this->set_mErrorText_2(_stringLiteral1916831050);
		goto IL_00eb;
	}

IL_00ab:
	{
		__this->set_mErrorText_2(_stringLiteral1517763141);
		goto IL_00eb;
	}

IL_00bb:
	{
		__this->set_mErrorText_2(_stringLiteral1236998107);
		goto IL_00eb;
	}

IL_00cb:
	{
		__this->set_mErrorText_2(_stringLiteral251546322);
		goto IL_00eb;
	}

IL_00db:
	{
		__this->set_mErrorText_2(_stringLiteral4244220307);
		goto IL_00eb;
	}

IL_00eb:
	{
		return;
	}
}
// System.Void Vuforia.DefaultInitializationErrorHandler::SetErrorOccurred(System.Boolean)
extern "C"  void DefaultInitializationErrorHandler_SetErrorOccurred_m3423758312 (DefaultInitializationErrorHandler_t414875900 * __this, bool ___errorOccurred0, const RuntimeMethod* method)
{
	{
		bool L_0 = ___errorOccurred0;
		__this->set_mErrorOccurred_3(L_0);
		return;
	}
}
// System.Void Vuforia.DefaultInitializationErrorHandler::OnVuforiaInitializationError(Vuforia.VuforiaUnity/InitError)
extern "C"  void DefaultInitializationErrorHandler_OnVuforiaInitializationError_m1077570325 (DefaultInitializationErrorHandler_t414875900 * __this, int32_t ___initError0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___initError0;
		if (!L_0)
		{
			goto IL_0014;
		}
	}
	{
		int32_t L_1 = ___initError0;
		DefaultInitializationErrorHandler_SetErrorCode_m749491405(__this, L_1, /*hidden argument*/NULL);
		DefaultInitializationErrorHandler_SetErrorOccurred_m3423758312(__this, (bool)1, /*hidden argument*/NULL);
	}

IL_0014:
	{
		return;
	}
}
// System.Void Vuforia.DefaultSmartTerrainEventHandler::.ctor()
extern "C"  void DefaultSmartTerrainEventHandler__ctor_m2119808224 (DefaultSmartTerrainEventHandler_t3202897208 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m620219618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.DefaultSmartTerrainEventHandler::Start()
extern "C"  void DefaultSmartTerrainEventHandler_Start_m4196669513 (DefaultSmartTerrainEventHandler_t3202897208 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DefaultSmartTerrainEventHandler_Start_m4196669513_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		ReconstructionBehaviour_t3936121853 * L_0 = Component_GetComponent_TisReconstructionBehaviour_t3936121853_m3652591174(__this, /*hidden argument*/Component_GetComponent_TisReconstructionBehaviour_t3936121853_m3652591174_RuntimeMethod_var);
		__this->set_mReconstructionBehaviour_2(L_0);
		ReconstructionBehaviour_t3936121853 * L_1 = __this->get_mReconstructionBehaviour_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1502412432_il2cpp_TypeInfo_var);
		bool L_2 = Object_op_Implicit_m2314733868(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_004a;
		}
	}
	{
		ReconstructionBehaviour_t3936121853 * L_3 = __this->get_mReconstructionBehaviour_2();
		IntPtr_t L_4;
		L_4.set_m_value_0((void*)(void*)DefaultSmartTerrainEventHandler_OnPropCreated_m2829740123_RuntimeMethod_var);
		Action_1_t1560204979 * L_5 = (Action_1_t1560204979 *)il2cpp_codegen_object_new(Action_1_t1560204979_il2cpp_TypeInfo_var);
		Action_1__ctor_m220035642(L_5, __this, L_4, /*hidden argument*/Action_1__ctor_m220035642_RuntimeMethod_var);
		NullCheck(L_3);
		ReconstructionAbstractBehaviour_RegisterPropCreatedCallback_m2985286061(L_3, L_5, /*hidden argument*/NULL);
		ReconstructionBehaviour_t3936121853 * L_6 = __this->get_mReconstructionBehaviour_2();
		IntPtr_t L_7;
		L_7.set_m_value_0((void*)(void*)DefaultSmartTerrainEventHandler_OnSurfaceCreated_m2986227878_RuntimeMethod_var);
		Action_1_t1020068010 * L_8 = (Action_1_t1020068010 *)il2cpp_codegen_object_new(Action_1_t1020068010_il2cpp_TypeInfo_var);
		Action_1__ctor_m59453672(L_8, __this, L_7, /*hidden argument*/Action_1__ctor_m59453672_RuntimeMethod_var);
		NullCheck(L_6);
		ReconstructionAbstractBehaviour_RegisterSurfaceCreatedCallback_m479439615(L_6, L_8, /*hidden argument*/NULL);
	}

IL_004a:
	{
		return;
	}
}
// System.Void Vuforia.DefaultSmartTerrainEventHandler::OnDestroy()
extern "C"  void DefaultSmartTerrainEventHandler_OnDestroy_m3748050755 (DefaultSmartTerrainEventHandler_t3202897208 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DefaultSmartTerrainEventHandler_OnDestroy_m3748050755_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		ReconstructionBehaviour_t3936121853 * L_0 = __this->get_mReconstructionBehaviour_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1502412432_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Implicit_m2314733868(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_003e;
		}
	}
	{
		ReconstructionBehaviour_t3936121853 * L_2 = __this->get_mReconstructionBehaviour_2();
		IntPtr_t L_3;
		L_3.set_m_value_0((void*)(void*)DefaultSmartTerrainEventHandler_OnPropCreated_m2829740123_RuntimeMethod_var);
		Action_1_t1560204979 * L_4 = (Action_1_t1560204979 *)il2cpp_codegen_object_new(Action_1_t1560204979_il2cpp_TypeInfo_var);
		Action_1__ctor_m220035642(L_4, __this, L_3, /*hidden argument*/Action_1__ctor_m220035642_RuntimeMethod_var);
		NullCheck(L_2);
		ReconstructionAbstractBehaviour_UnregisterPropCreatedCallback_m1396476554(L_2, L_4, /*hidden argument*/NULL);
		ReconstructionBehaviour_t3936121853 * L_5 = __this->get_mReconstructionBehaviour_2();
		IntPtr_t L_6;
		L_6.set_m_value_0((void*)(void*)DefaultSmartTerrainEventHandler_OnSurfaceCreated_m2986227878_RuntimeMethod_var);
		Action_1_t1020068010 * L_7 = (Action_1_t1020068010 *)il2cpp_codegen_object_new(Action_1_t1020068010_il2cpp_TypeInfo_var);
		Action_1__ctor_m59453672(L_7, __this, L_6, /*hidden argument*/Action_1__ctor_m59453672_RuntimeMethod_var);
		NullCheck(L_5);
		ReconstructionAbstractBehaviour_UnregisterSurfaceCreatedCallback_m3341108077(L_5, L_7, /*hidden argument*/NULL);
	}

IL_003e:
	{
		return;
	}
}
// System.Void Vuforia.DefaultSmartTerrainEventHandler::OnPropCreated(Vuforia.Prop)
extern "C"  void DefaultSmartTerrainEventHandler_OnPropCreated_m2829740123 (DefaultSmartTerrainEventHandler_t3202897208 * __this, RuntimeObject* ___prop0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DefaultSmartTerrainEventHandler_OnPropCreated_m2829740123_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		ReconstructionBehaviour_t3936121853 * L_0 = __this->get_mReconstructionBehaviour_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1502412432_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Implicit_m2314733868(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0023;
		}
	}
	{
		ReconstructionBehaviour_t3936121853 * L_2 = __this->get_mReconstructionBehaviour_2();
		PropBehaviour_t3897855688 * L_3 = __this->get_PropTemplate_3();
		RuntimeObject* L_4 = ___prop0;
		NullCheck(L_2);
		ReconstructionAbstractBehaviour_AssociateProp_m1567680850(L_2, L_3, L_4, /*hidden argument*/NULL);
	}

IL_0023:
	{
		return;
	}
}
// System.Void Vuforia.DefaultSmartTerrainEventHandler::OnSurfaceCreated(Vuforia.Surface)
extern "C"  void DefaultSmartTerrainEventHandler_OnSurfaceCreated_m2986227878 (DefaultSmartTerrainEventHandler_t3202897208 * __this, RuntimeObject* ___surface0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DefaultSmartTerrainEventHandler_OnSurfaceCreated_m2986227878_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		ReconstructionBehaviour_t3936121853 * L_0 = __this->get_mReconstructionBehaviour_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1502412432_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Implicit_m2314733868(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0023;
		}
	}
	{
		ReconstructionBehaviour_t3936121853 * L_2 = __this->get_mReconstructionBehaviour_2();
		SurfaceBehaviour_t1975960194 * L_3 = __this->get_SurfaceTemplate_4();
		RuntimeObject* L_4 = ___surface0;
		NullCheck(L_2);
		ReconstructionAbstractBehaviour_AssociateSurface_m3177208316(L_2, L_3, L_4, /*hidden argument*/NULL);
	}

IL_0023:
	{
		return;
	}
}
// System.Void Vuforia.DefaultTrackableEventHandler::.ctor()
extern "C"  void DefaultTrackableEventHandler__ctor_m3838945731 (DefaultTrackableEventHandler_t2331528573 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m620219618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.DefaultTrackableEventHandler::Start()
extern "C"  void DefaultTrackableEventHandler_Start_m3092614202 (DefaultTrackableEventHandler_t2331528573 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DefaultTrackableEventHandler_Start_m3092614202_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		TrackableBehaviour_t2419079356 * L_0 = Component_GetComponent_TisTrackableBehaviour_t2419079356_m3640808561(__this, /*hidden argument*/Component_GetComponent_TisTrackableBehaviour_t2419079356_m3640808561_RuntimeMethod_var);
		__this->set_mTrackableBehaviour_2(L_0);
		TrackableBehaviour_t2419079356 * L_1 = __this->get_mTrackableBehaviour_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1502412432_il2cpp_TypeInfo_var);
		bool L_2 = Object_op_Implicit_m2314733868(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0028;
		}
	}
	{
		TrackableBehaviour_t2419079356 * L_3 = __this->get_mTrackableBehaviour_2();
		NullCheck(L_3);
		TrackableBehaviour_RegisterTrackableEventHandler_m537253126(L_3, __this, /*hidden argument*/NULL);
	}

IL_0028:
	{
		return;
	}
}
// System.Void Vuforia.DefaultTrackableEventHandler::OnTrackableStateChanged(Vuforia.TrackableBehaviour/Status,Vuforia.TrackableBehaviour/Status)
extern "C"  void DefaultTrackableEventHandler_OnTrackableStateChanged_m1274575882 (DefaultTrackableEventHandler_t2331528573 * __this, int32_t ___previousStatus0, int32_t ___newStatus1, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___newStatus1;
		if ((((int32_t)L_0) == ((int32_t)2)))
		{
			goto IL_0015;
		}
	}
	{
		int32_t L_1 = ___newStatus1;
		if ((((int32_t)L_1) == ((int32_t)3)))
		{
			goto IL_0015;
		}
	}
	{
		int32_t L_2 = ___newStatus1;
		if ((!(((uint32_t)L_2) == ((uint32_t)4))))
		{
			goto IL_0020;
		}
	}

IL_0015:
	{
		DefaultTrackableEventHandler_OnTrackingFound_m3375166525(__this, /*hidden argument*/NULL);
		goto IL_0026;
	}

IL_0020:
	{
		DefaultTrackableEventHandler_OnTrackingLost_m3475317276(__this, /*hidden argument*/NULL);
	}

IL_0026:
	{
		return;
	}
}
// System.Void Vuforia.DefaultTrackableEventHandler::OnTrackingFound()
extern "C"  void DefaultTrackableEventHandler_OnTrackingFound_m3375166525 (DefaultTrackableEventHandler_t2331528573 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DefaultTrackableEventHandler_OnTrackingFound_m3375166525_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RendererU5BU5D_t2667410251* V_0 = NULL;
	ColliderU5BU5D_t3746774603* V_1 = NULL;
	Renderer_t1303575294 * V_2 = NULL;
	RendererU5BU5D_t2667410251* V_3 = NULL;
	int32_t V_4 = 0;
	Collider_t2301021182 * V_5 = NULL;
	ColliderU5BU5D_t3746774603* V_6 = NULL;
	int32_t V_7 = 0;
	{
		RendererU5BU5D_t2667410251* L_0 = Component_GetComponentsInChildren_TisRenderer_t1303575294_m3094251748(__this, (bool)1, /*hidden argument*/Component_GetComponentsInChildren_TisRenderer_t1303575294_m3094251748_RuntimeMethod_var);
		V_0 = L_0;
		ColliderU5BU5D_t3746774603* L_1 = Component_GetComponentsInChildren_TisCollider_t2301021182_m857431636(__this, (bool)1, /*hidden argument*/Component_GetComponentsInChildren_TisCollider_t2301021182_m857431636_RuntimeMethod_var);
		V_1 = L_1;
		RendererU5BU5D_t2667410251* L_2 = V_0;
		V_3 = L_2;
		V_4 = 0;
		goto IL_002c;
	}

IL_001a:
	{
		RendererU5BU5D_t2667410251* L_3 = V_3;
		int32_t L_4 = V_4;
		NullCheck(L_3);
		int32_t L_5 = L_4;
		Renderer_t1303575294 * L_6 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5));
		V_2 = L_6;
		Renderer_t1303575294 * L_7 = V_2;
		NullCheck(L_7);
		Renderer_set_enabled_m3164806497(L_7, (bool)1, /*hidden argument*/NULL);
		int32_t L_8 = V_4;
		V_4 = ((int32_t)((int32_t)L_8+(int32_t)1));
	}

IL_002c:
	{
		int32_t L_9 = V_4;
		RendererU5BU5D_t2667410251* L_10 = V_3;
		NullCheck(L_10);
		if ((((int32_t)L_9) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_10)->max_length)))))))
		{
			goto IL_001a;
		}
	}
	{
		ColliderU5BU5D_t3746774603* L_11 = V_1;
		V_6 = L_11;
		V_7 = 0;
		goto IL_0056;
	}

IL_0041:
	{
		ColliderU5BU5D_t3746774603* L_12 = V_6;
		int32_t L_13 = V_7;
		NullCheck(L_12);
		int32_t L_14 = L_13;
		Collider_t2301021182 * L_15 = (L_12)->GetAt(static_cast<il2cpp_array_size_t>(L_14));
		V_5 = L_15;
		Collider_t2301021182 * L_16 = V_5;
		NullCheck(L_16);
		Collider_set_enabled_m1322675966(L_16, (bool)1, /*hidden argument*/NULL);
		int32_t L_17 = V_7;
		V_7 = ((int32_t)((int32_t)L_17+(int32_t)1));
	}

IL_0056:
	{
		int32_t L_18 = V_7;
		ColliderU5BU5D_t3746774603* L_19 = V_6;
		NullCheck(L_19);
		if ((((int32_t)L_18) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_19)->max_length)))))))
		{
			goto IL_0041;
		}
	}
	{
		TrackableBehaviour_t2419079356 * L_20 = __this->get_mTrackableBehaviour_2();
		NullCheck(L_20);
		String_t* L_21 = TrackableBehaviour_get_TrackableName_m3217816807(L_20, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_22 = String_Concat_m3656894380(NULL /*static, unused*/, _stringLiteral2756122832, L_21, _stringLiteral3913380118, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3525713211_il2cpp_TypeInfo_var);
		Debug_Log_m1673024736(NULL /*static, unused*/, L_22, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.DefaultTrackableEventHandler::OnTrackingLost()
extern "C"  void DefaultTrackableEventHandler_OnTrackingLost_m3475317276 (DefaultTrackableEventHandler_t2331528573 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DefaultTrackableEventHandler_OnTrackingLost_m3475317276_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RendererU5BU5D_t2667410251* V_0 = NULL;
	ColliderU5BU5D_t3746774603* V_1 = NULL;
	Renderer_t1303575294 * V_2 = NULL;
	RendererU5BU5D_t2667410251* V_3 = NULL;
	int32_t V_4 = 0;
	Collider_t2301021182 * V_5 = NULL;
	ColliderU5BU5D_t3746774603* V_6 = NULL;
	int32_t V_7 = 0;
	{
		RendererU5BU5D_t2667410251* L_0 = Component_GetComponentsInChildren_TisRenderer_t1303575294_m3094251748(__this, (bool)1, /*hidden argument*/Component_GetComponentsInChildren_TisRenderer_t1303575294_m3094251748_RuntimeMethod_var);
		V_0 = L_0;
		ColliderU5BU5D_t3746774603* L_1 = Component_GetComponentsInChildren_TisCollider_t2301021182_m857431636(__this, (bool)1, /*hidden argument*/Component_GetComponentsInChildren_TisCollider_t2301021182_m857431636_RuntimeMethod_var);
		V_1 = L_1;
		RendererU5BU5D_t2667410251* L_2 = V_0;
		V_3 = L_2;
		V_4 = 0;
		goto IL_002c;
	}

IL_001a:
	{
		RendererU5BU5D_t2667410251* L_3 = V_3;
		int32_t L_4 = V_4;
		NullCheck(L_3);
		int32_t L_5 = L_4;
		Renderer_t1303575294 * L_6 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5));
		V_2 = L_6;
		Renderer_t1303575294 * L_7 = V_2;
		NullCheck(L_7);
		Renderer_set_enabled_m3164806497(L_7, (bool)0, /*hidden argument*/NULL);
		int32_t L_8 = V_4;
		V_4 = ((int32_t)((int32_t)L_8+(int32_t)1));
	}

IL_002c:
	{
		int32_t L_9 = V_4;
		RendererU5BU5D_t2667410251* L_10 = V_3;
		NullCheck(L_10);
		if ((((int32_t)L_9) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_10)->max_length)))))))
		{
			goto IL_001a;
		}
	}
	{
		ColliderU5BU5D_t3746774603* L_11 = V_1;
		V_6 = L_11;
		V_7 = 0;
		goto IL_0056;
	}

IL_0041:
	{
		ColliderU5BU5D_t3746774603* L_12 = V_6;
		int32_t L_13 = V_7;
		NullCheck(L_12);
		int32_t L_14 = L_13;
		Collider_t2301021182 * L_15 = (L_12)->GetAt(static_cast<il2cpp_array_size_t>(L_14));
		V_5 = L_15;
		Collider_t2301021182 * L_16 = V_5;
		NullCheck(L_16);
		Collider_set_enabled_m1322675966(L_16, (bool)0, /*hidden argument*/NULL);
		int32_t L_17 = V_7;
		V_7 = ((int32_t)((int32_t)L_17+(int32_t)1));
	}

IL_0056:
	{
		int32_t L_18 = V_7;
		ColliderU5BU5D_t3746774603* L_19 = V_6;
		NullCheck(L_19);
		if ((((int32_t)L_18) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_19)->max_length)))))))
		{
			goto IL_0041;
		}
	}
	{
		TrackableBehaviour_t2419079356 * L_20 = __this->get_mTrackableBehaviour_2();
		NullCheck(L_20);
		String_t* L_21 = TrackableBehaviour_get_TrackableName_m3217816807(L_20, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_22 = String_Concat_m3656894380(NULL /*static, unused*/, _stringLiteral2756122832, L_21, _stringLiteral2566987390, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3525713211_il2cpp_TypeInfo_var);
		Debug_Log_m1673024736(NULL /*static, unused*/, L_22, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.GLErrorHandler::.ctor()
extern "C"  void GLErrorHandler__ctor_m1829363221 (GLErrorHandler_t1216999495 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m620219618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.GLErrorHandler::SetError(System.String)
extern "C"  void GLErrorHandler_SetError_m2768535324 (RuntimeObject * __this /* static, unused */, String_t* ___errorText0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GLErrorHandler_SetError_m2768535324_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___errorText0;
		IL2CPP_RUNTIME_CLASS_INIT(GLErrorHandler_t1216999495_il2cpp_TypeInfo_var);
		((GLErrorHandler_t1216999495_StaticFields*)il2cpp_codegen_static_fields_for(GLErrorHandler_t1216999495_il2cpp_TypeInfo_var))->set_mErrorText_2(L_0);
		((GLErrorHandler_t1216999495_StaticFields*)il2cpp_codegen_static_fields_for(GLErrorHandler_t1216999495_il2cpp_TypeInfo_var))->set_mErrorOccurred_3((bool)1);
		return;
	}
}
// System.Void Vuforia.GLErrorHandler::OnGUI()
extern "C"  void GLErrorHandler_OnGUI_m3781023102 (GLErrorHandler_t1216999495 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GLErrorHandler_OnGUI_m3781023102_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GLErrorHandler_t1216999495_il2cpp_TypeInfo_var);
		bool L_0 = ((GLErrorHandler_t1216999495_StaticFields*)il2cpp_codegen_static_fields_for(GLErrorHandler_t1216999495_il2cpp_TypeInfo_var))->get_mErrorOccurred_3();
		if (!L_0)
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_1 = Screen_get_width_m2947011129(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_2 = Screen_get_height_m471732428(NULL /*static, unused*/, /*hidden argument*/NULL);
		Rect_t3436776195  L_3;
		memset(&L_3, 0, sizeof(L_3));
		Rect__ctor_m2728905368((&L_3), (0.0f), (0.0f), (((float)((float)L_1))), (((float)((float)L_2))), /*hidden argument*/NULL);
		IntPtr_t L_4;
		L_4.set_m_value_0((void*)(void*)GLErrorHandler_DrawWindowContent_m4286681250_RuntimeMethod_var);
		WindowFunction_t3577962812 * L_5 = (WindowFunction_t3577962812 *)il2cpp_codegen_object_new(WindowFunction_t3577962812_il2cpp_TypeInfo_var);
		WindowFunction__ctor_m3870900743(L_5, __this, L_4, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t1999344246_il2cpp_TypeInfo_var);
		GUI_Window_m2935776987(NULL /*static, unused*/, 0, L_3, L_5, _stringLiteral370773887, /*hidden argument*/NULL);
	}

IL_003d:
	{
		return;
	}
}
// System.Void Vuforia.GLErrorHandler::DrawWindowContent(System.Int32)
extern "C"  void GLErrorHandler_DrawWindowContent_m4286681250 (GLErrorHandler_t1216999495 * __this, int32_t ___id0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GLErrorHandler_DrawWindowContent_m4286681250_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = Screen_get_width_m2947011129(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_1 = Screen_get_height_m471732428(NULL /*static, unused*/, /*hidden argument*/NULL);
		Rect_t3436776195  L_2;
		memset(&L_2, 0, sizeof(L_2));
		Rect__ctor_m2728905368((&L_2), (10.0f), (25.0f), (((float)((float)((int32_t)((int32_t)L_0-(int32_t)((int32_t)20)))))), (((float)((float)((int32_t)((int32_t)L_1-(int32_t)((int32_t)95)))))), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GLErrorHandler_t1216999495_il2cpp_TypeInfo_var);
		String_t* L_3 = ((GLErrorHandler_t1216999495_StaticFields*)il2cpp_codegen_static_fields_for(GLErrorHandler_t1216999495_il2cpp_TypeInfo_var))->get_mErrorText_2();
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t1999344246_il2cpp_TypeInfo_var);
		GUI_Label_m2909214338(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		int32_t L_4 = Screen_get_width_m2947011129(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_5 = Screen_get_height_m471732428(NULL /*static, unused*/, /*hidden argument*/NULL);
		Rect_t3436776195  L_6;
		memset(&L_6, 0, sizeof(L_6));
		Rect__ctor_m2728905368((&L_6), (((float)((float)((int32_t)((int32_t)((int32_t)((int32_t)L_4/(int32_t)2))-(int32_t)((int32_t)75)))))), (((float)((float)((int32_t)((int32_t)L_5-(int32_t)((int32_t)60)))))), (150.0f), (50.0f), /*hidden argument*/NULL);
		bool L_7 = GUI_Button_m1337722186(NULL /*static, unused*/, L_6, _stringLiteral3882266322, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0062;
		}
	}
	{
		Application_Quit_m1111605119(NULL /*static, unused*/, /*hidden argument*/NULL);
	}

IL_0062:
	{
		return;
	}
}
// System.Void Vuforia.GLErrorHandler::.cctor()
extern "C"  void GLErrorHandler__cctor_m3154199897 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GLErrorHandler__cctor_m3154199897_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)il2cpp_codegen_static_fields_for(String_t_il2cpp_TypeInfo_var))->get_Empty_2();
		((GLErrorHandler_t1216999495_StaticFields*)il2cpp_codegen_static_fields_for(GLErrorHandler_t1216999495_il2cpp_TypeInfo_var))->set_mErrorText_2(L_0);
		return;
	}
}
// System.Void Vuforia.HideExcessAreaBehaviour::.ctor()
extern "C"  void HideExcessAreaBehaviour__ctor_m2505028767 (HideExcessAreaBehaviour_t554831333 * __this, const RuntimeMethod* method)
{
	{
		HideExcessAreaAbstractBehaviour__ctor_m21881450(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.ImageTargetBehaviour::.ctor()
extern "C"  void ImageTargetBehaviour__ctor_m2037391769 (ImageTargetBehaviour_t2918114530 * __this, const RuntimeMethod* method)
{
	{
		ImageTargetAbstractBehaviour__ctor_m2879815750(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.IOSUnityPlayer::.ctor()
extern "C"  void IOSUnityPlayer__ctor_m3437806645 (IOSUnityPlayer_t1005570149 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m2906060009(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.IOSUnityPlayer::LoadNativeLibraries()
extern "C"  void IOSUnityPlayer_LoadNativeLibraries_m2693898471 (IOSUnityPlayer_t1005570149 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IOSUnityPlayer_LoadNativeLibraries_m2693898471_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		VuforiaNativeIosWrapper_t49709812 * L_0 = (VuforiaNativeIosWrapper_t49709812 *)il2cpp_codegen_object_new(VuforiaNativeIosWrapper_t49709812_il2cpp_TypeInfo_var);
		VuforiaNativeIosWrapper__ctor_m1849016803(L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(VuforiaWrapper_t3746480205_il2cpp_TypeInfo_var);
		VuforiaWrapper_SetImplementation_m2562871935(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.IOSUnityPlayer::InitializePlatform()
extern "C"  void IOSUnityPlayer_InitializePlatform_m3734093454 (IOSUnityPlayer_t1005570149 * __this, const RuntimeMethod* method)
{
	{
		IOSUnityPlayer_setPlatFormNative_m3887041728(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// Vuforia.VuforiaUnity/InitError Vuforia.IOSUnityPlayer::InitializeVuforia(System.String)
extern "C"  int32_t IOSUnityPlayer_InitializeVuforia_m1462137961 (IOSUnityPlayer_t1005570149 * __this, String_t* ___licenseKey0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IOSUnityPlayer_InitializeVuforia_m1462137961_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(VuforiaRenderer_t969713742_il2cpp_TypeInfo_var);
		VuforiaRenderer_t969713742 * L_0 = VuforiaRenderer_get_Instance_m1182630307(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		int32_t L_1 = VirtFuncInvoker0< int32_t >::Invoke(15 /* Vuforia.VuforiaRenderer/RendererAPI Vuforia.VuforiaRenderer::GetRendererAPI() */, L_0);
		V_0 = L_1;
		int32_t L_2 = V_0;
		int32_t L_3 = Screen_get_orientation_m251094311(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_4 = ___licenseKey0;
		int32_t L_5 = IOSUnityPlayer_initQCARiOS_m3056939392(NULL /*static, unused*/, L_2, L_3, L_4, /*hidden argument*/NULL);
		V_1 = L_5;
		int32_t L_6 = V_1;
		if ((((int32_t)L_6) < ((int32_t)0)))
		{
			goto IL_0025;
		}
	}
	{
		IOSUnityPlayer_InitializeSurface_m1138500967(__this, /*hidden argument*/NULL);
	}

IL_0025:
	{
		int32_t L_7 = V_1;
		return (int32_t)(L_7);
	}
}
// System.Void Vuforia.IOSUnityPlayer::StartScene()
extern "C"  void IOSUnityPlayer_StartScene_m393261486 (IOSUnityPlayer_t1005570149 * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Void Vuforia.IOSUnityPlayer::Update()
extern "C"  void IOSUnityPlayer_Update_m2549520222 (IOSUnityPlayer_t1005570149 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IOSUnityPlayer_Update_m2549520222_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SurfaceUtilities_t184061569_il2cpp_TypeInfo_var);
		bool L_0 = SurfaceUtilities_HasSurfaceBeenRecreated_m346027931(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0015;
		}
	}
	{
		IOSUnityPlayer_InitializeSurface_m1138500967(__this, /*hidden argument*/NULL);
		goto IL_002b;
	}

IL_0015:
	{
		int32_t L_1 = Screen_get_orientation_m251094311(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_2 = __this->get_mScreenOrientation_0();
		if ((((int32_t)L_1) == ((int32_t)L_2)))
		{
			goto IL_002b;
		}
	}
	{
		IOSUnityPlayer_SetUnityScreenOrientation_m3334399535(__this, /*hidden argument*/NULL);
	}

IL_002b:
	{
		return;
	}
}
// System.Void Vuforia.IOSUnityPlayer::Dispose()
extern "C"  void IOSUnityPlayer_Dispose_m3543371289 (IOSUnityPlayer_t1005570149 * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Void Vuforia.IOSUnityPlayer::OnPause()
extern "C"  void IOSUnityPlayer_OnPause_m632510427 (IOSUnityPlayer_t1005570149 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IOSUnityPlayer_OnPause_m632510427_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(VuforiaUnity_t3212967305_il2cpp_TypeInfo_var);
		VuforiaUnity_OnPause_m4153953119(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.IOSUnityPlayer::OnResume()
extern "C"  void IOSUnityPlayer_OnResume_m4077853536 (IOSUnityPlayer_t1005570149 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IOSUnityPlayer_OnResume_m4077853536_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(VuforiaUnity_t3212967305_il2cpp_TypeInfo_var);
		VuforiaUnity_OnResume_m2287209481(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.IOSUnityPlayer::OnDestroy()
extern "C"  void IOSUnityPlayer_OnDestroy_m1412385811 (IOSUnityPlayer_t1005570149 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IOSUnityPlayer_OnDestroy_m1412385811_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(VuforiaUnity_t3212967305_il2cpp_TypeInfo_var);
		VuforiaUnity_Deinit_m2222663190(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.IOSUnityPlayer::InitializeSurface()
extern "C"  void IOSUnityPlayer_InitializeSurface_m1138500967 (IOSUnityPlayer_t1005570149 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IOSUnityPlayer_InitializeSurface_m1138500967_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SurfaceUtilities_t184061569_il2cpp_TypeInfo_var);
		SurfaceUtilities_OnSurfaceCreated_m1867899004(NULL /*static, unused*/, /*hidden argument*/NULL);
		IOSUnityPlayer_SetUnityScreenOrientation_m3334399535(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.IOSUnityPlayer::SetUnityScreenOrientation()
extern "C"  void IOSUnityPlayer_SetUnityScreenOrientation_m3334399535 (IOSUnityPlayer_t1005570149 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IOSUnityPlayer_SetUnityScreenOrientation_m3334399535_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = Screen_get_orientation_m251094311(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_mScreenOrientation_0(L_0);
		int32_t L_1 = __this->get_mScreenOrientation_0();
		IL2CPP_RUNTIME_CLASS_INIT(SurfaceUtilities_t184061569_il2cpp_TypeInfo_var);
		SurfaceUtilities_SetSurfaceOrientation_m897924353(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		int32_t L_2 = __this->get_mScreenOrientation_0();
		IOSUnityPlayer_setSurfaceOrientationiOS_m2874037796(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		return;
	}
}
extern "C" void DEFAULT_CALL setPlatFormNative();
// System.Void Vuforia.IOSUnityPlayer::setPlatFormNative()
extern "C"  void IOSUnityPlayer_setPlatFormNative_m3887041728 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) ();

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(setPlatFormNative)();

}
extern "C" int32_t DEFAULT_CALL initQCARiOS(int32_t, int32_t, char*);
// System.Int32 Vuforia.IOSUnityPlayer::initQCARiOS(System.Int32,System.Int32,System.String)
extern "C"  int32_t IOSUnityPlayer_initQCARiOS_m3056939392 (RuntimeObject * __this /* static, unused */, int32_t ___rendererAPI0, int32_t ___screenOrientation1, String_t* ___licenseKey2, const RuntimeMethod* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (int32_t, int32_t, char*);

	// Marshaling of parameter '___licenseKey2' to native representation
	char* ____licenseKey2_marshaled = NULL;
	____licenseKey2_marshaled = il2cpp_codegen_marshal_string(___licenseKey2);

	// Native function invocation
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(initQCARiOS)(___rendererAPI0, ___screenOrientation1, ____licenseKey2_marshaled);

	// Marshaling cleanup of parameter '___licenseKey2' native representation
	il2cpp_codegen_marshal_free(____licenseKey2_marshaled);
	____licenseKey2_marshaled = NULL;

	return returnValue;
}
extern "C" void DEFAULT_CALL setSurfaceOrientationiOS(int32_t);
// System.Void Vuforia.IOSUnityPlayer::setSurfaceOrientationiOS(System.Int32)
extern "C"  void IOSUnityPlayer_setSurfaceOrientationiOS_m2874037796 (RuntimeObject * __this /* static, unused */, int32_t ___screenOrientation0, const RuntimeMethod* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (int32_t);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(setSurfaceOrientationiOS)(___screenOrientation0);

}
// System.Void Vuforia.MaskOutBehaviour::.ctor()
extern "C"  void MaskOutBehaviour__ctor_m4132362832 (MaskOutBehaviour_t62027326 * __this, const RuntimeMethod* method)
{
	{
		MaskOutAbstractBehaviour__ctor_m3408028974(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.MaskOutBehaviour::Start()
extern "C"  void MaskOutBehaviour_Start_m1067981915 (MaskOutBehaviour_t62027326 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MaskOutBehaviour_Start_m1067981915_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Renderer_t1303575294 * V_0 = NULL;
	int32_t V_1 = 0;
	MaterialU5BU5D_t1437145498* V_2 = NULL;
	int32_t V_3 = 0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(VuforiaRuntimeUtilities_t1036756025_il2cpp_TypeInfo_var);
		bool L_0 = VuforiaRuntimeUtilities_IsVuforiaEnabled_m3634160515(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_005b;
		}
	}
	{
		Renderer_t1303575294 * L_1 = Component_GetComponent_TisRenderer_t1303575294_m1648558159(__this, /*hidden argument*/Component_GetComponent_TisRenderer_t1303575294_m1648558159_RuntimeMethod_var);
		V_0 = L_1;
		Renderer_t1303575294 * L_2 = V_0;
		NullCheck(L_2);
		MaterialU5BU5D_t1437145498* L_3 = Renderer_get_materials_m4017502688(L_2, /*hidden argument*/NULL);
		NullCheck(L_3);
		V_1 = (((int32_t)((int32_t)(((RuntimeArray *)L_3)->max_length))));
		int32_t L_4 = V_1;
		if ((!(((uint32_t)L_4) == ((uint32_t)1))))
		{
			goto IL_0032;
		}
	}
	{
		Renderer_t1303575294 * L_5 = V_0;
		Material_t1079520667 * L_6 = ((MaskOutAbstractBehaviour_t2174599097 *)__this)->get_maskMaterial_2();
		NullCheck(L_5);
		Renderer_set_sharedMaterial_m1952739559(L_5, L_6, /*hidden argument*/NULL);
		goto IL_005b;
	}

IL_0032:
	{
		int32_t L_7 = V_1;
		V_2 = ((MaterialU5BU5D_t1437145498*)SZArrayNew(MaterialU5BU5D_t1437145498_il2cpp_TypeInfo_var, (uint32_t)L_7));
		V_3 = 0;
		goto IL_004d;
	}

IL_0040:
	{
		MaterialU5BU5D_t1437145498* L_8 = V_2;
		int32_t L_9 = V_3;
		Material_t1079520667 * L_10 = ((MaskOutAbstractBehaviour_t2174599097 *)__this)->get_maskMaterial_2();
		NullCheck(L_8);
		ArrayElementTypeCheck (L_8, L_10);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(L_9), (Material_t1079520667 *)L_10);
		int32_t L_11 = V_3;
		V_3 = ((int32_t)((int32_t)L_11+(int32_t)1));
	}

IL_004d:
	{
		int32_t L_12 = V_3;
		int32_t L_13 = V_1;
		if ((((int32_t)L_12) < ((int32_t)L_13)))
		{
			goto IL_0040;
		}
	}
	{
		Renderer_t1303575294 * L_14 = V_0;
		MaterialU5BU5D_t1437145498* L_15 = V_2;
		NullCheck(L_14);
		Renderer_set_sharedMaterials_m632974399(L_14, L_15, /*hidden argument*/NULL);
	}

IL_005b:
	{
		return;
	}
}
// System.Void Vuforia.MultiTargetBehaviour::.ctor()
extern "C"  void MultiTargetBehaviour__ctor_m3961868375 (MultiTargetBehaviour_t687655925 * __this, const RuntimeMethod* method)
{
	{
		MultiTargetAbstractBehaviour__ctor_m2403500192(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.ObjectTargetBehaviour::.ctor()
extern "C"  void ObjectTargetBehaviour__ctor_m920458337 (ObjectTargetBehaviour_t1761394684 * __this, const RuntimeMethod* method)
{
	{
		ObjectTargetAbstractBehaviour__ctor_m3001407456(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.PropBehaviour::.ctor()
extern "C"  void PropBehaviour__ctor_m3434346977 (PropBehaviour_t3897855688 * __this, const RuntimeMethod* method)
{
	{
		PropAbstractBehaviour__ctor_m3916936582(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.ReconstructionBehaviour::.ctor()
extern "C"  void ReconstructionBehaviour__ctor_m4144445438 (ReconstructionBehaviour_t3936121853 * __this, const RuntimeMethod* method)
{
	{
		ReconstructionAbstractBehaviour__ctor_m1687108356(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.ReconstructionFromTargetBehaviour::.ctor()
extern "C"  void ReconstructionFromTargetBehaviour__ctor_m1508461065 (ReconstructionFromTargetBehaviour_t2819884381 * __this, const RuntimeMethod* method)
{
	{
		ReconstructionFromTargetAbstractBehaviour__ctor_m1639870364(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.SurfaceBehaviour::.ctor()
extern "C"  void SurfaceBehaviour__ctor_m2864166666 (SurfaceBehaviour_t1975960194 * __this, const RuntimeMethod* method)
{
	{
		SurfaceAbstractBehaviour__ctor_m4081210538(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.TextRecoBehaviour::.ctor()
extern "C"  void TextRecoBehaviour__ctor_m151706155 (TextRecoBehaviour_t3253755448 * __this, const RuntimeMethod* method)
{
	{
		TextRecoAbstractBehaviour__ctor_m1589097412(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.TurnOffBehaviour::.ctor()
extern "C"  void TurnOffBehaviour__ctor_m2409972887 (TurnOffBehaviour_t963849236 * __this, const RuntimeMethod* method)
{
	{
		TurnOffAbstractBehaviour__ctor_m3552357493(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.TurnOffBehaviour::Awake()
extern "C"  void TurnOffBehaviour_Awake_m2199196731 (TurnOffBehaviour_t963849236 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TurnOffBehaviour_Awake_m2199196731_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	MeshRenderer_t1441648352 * V_0 = NULL;
	MeshFilter_t137687116 * V_1 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(VuforiaRuntimeUtilities_t1036756025_il2cpp_TypeInfo_var);
		bool L_0 = VuforiaRuntimeUtilities_IsVuforiaEnabled_m3634160515(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0024;
		}
	}
	{
		MeshRenderer_t1441648352 * L_1 = Component_GetComponent_TisMeshRenderer_t1441648352_m3769347907(__this, /*hidden argument*/Component_GetComponent_TisMeshRenderer_t1441648352_m3769347907_RuntimeMethod_var);
		V_0 = L_1;
		MeshRenderer_t1441648352 * L_2 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1502412432_il2cpp_TypeInfo_var);
		Object_Destroy_m1956460533(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		MeshFilter_t137687116 * L_3 = Component_GetComponent_TisMeshFilter_t137687116_m62619598(__this, /*hidden argument*/Component_GetComponent_TisMeshFilter_t137687116_m62619598_RuntimeMethod_var);
		V_1 = L_3;
		MeshFilter_t137687116 * L_4 = V_1;
		Object_Destroy_m1956460533(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
	}

IL_0024:
	{
		return;
	}
}
// System.Void Vuforia.TurnOffWordBehaviour::.ctor()
extern "C"  void TurnOffWordBehaviour__ctor_m1693480373 (TurnOffWordBehaviour_t1385869686 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m620219618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.TurnOffWordBehaviour::Awake()
extern "C"  void TurnOffWordBehaviour_Awake_m2127443610 (TurnOffWordBehaviour_t1385869686 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TurnOffWordBehaviour_Awake_m2127443610_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	MeshRenderer_t1441648352 * V_0 = NULL;
	Transform_t3316442598 * V_1 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(VuforiaRuntimeUtilities_t1036756025_il2cpp_TypeInfo_var);
		bool L_0 = VuforiaRuntimeUtilities_IsVuforiaEnabled_m3634160515(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_003f;
		}
	}
	{
		MeshRenderer_t1441648352 * L_1 = Component_GetComponent_TisMeshRenderer_t1441648352_m3769347907(__this, /*hidden argument*/Component_GetComponent_TisMeshRenderer_t1441648352_m3769347907_RuntimeMethod_var);
		V_0 = L_1;
		MeshRenderer_t1441648352 * L_2 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1502412432_il2cpp_TypeInfo_var);
		Object_Destroy_m1956460533(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		Transform_t3316442598 * L_3 = Component_get_transform_m2038396632(__this, /*hidden argument*/NULL);
		NullCheck(L_3);
		Transform_t3316442598 * L_4 = Transform_Find_m1259991727(L_3, _stringLiteral3274049941, /*hidden argument*/NULL);
		V_1 = L_4;
		Transform_t3316442598 * L_5 = V_1;
		bool L_6 = Object_op_Inequality_m1918954159(NULL /*static, unused*/, L_5, (Object_t1502412432 *)NULL, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_003f;
		}
	}
	{
		Transform_t3316442598 * L_7 = V_1;
		NullCheck(L_7);
		GameObject_t1811656094 * L_8 = Component_get_gameObject_m2283027183(L_7, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1502412432_il2cpp_TypeInfo_var);
		Object_Destroy_m1956460533(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
	}

IL_003f:
	{
		return;
	}
}
// System.Void Vuforia.UserDefinedTargetBuildingBehaviour::.ctor()
extern "C"  void UserDefinedTargetBuildingBehaviour__ctor_m2197610956 (UserDefinedTargetBuildingBehaviour_t3295846277 * __this, const RuntimeMethod* method)
{
	{
		UserDefinedTargetBuildingAbstractBehaviour__ctor_m2409308138(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.VideoBackgroundBehaviour::.ctor()
extern "C"  void VideoBackgroundBehaviour__ctor_m4180776799 (VideoBackgroundBehaviour_t177581750 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VideoBackgroundBehaviour__ctor_m4180776799_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(VideoBackgroundAbstractBehaviour_t3867888217_il2cpp_TypeInfo_var);
		VideoBackgroundAbstractBehaviour__ctor_m274348067(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.VirtualButtonBehaviour::.ctor()
extern "C"  void VirtualButtonBehaviour__ctor_m1475241316 (VirtualButtonBehaviour_t1019531330 * __this, const RuntimeMethod* method)
{
	{
		VirtualButtonAbstractBehaviour__ctor_m917555744(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.VuforiaBehaviour::.ctor()
extern "C"  void VuforiaBehaviour__ctor_m4103849126 (VuforiaBehaviour_t834098562 * __this, const RuntimeMethod* method)
{
	{
		VuforiaAbstractBehaviour__ctor_m322960130(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.VuforiaBehaviour::Awake()
extern "C"  void VuforiaBehaviour_Awake_m4269404237 (VuforiaBehaviour_t834098562 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VuforiaBehaviour_Awake_m4269404237_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		VuforiaBehaviour_AddOSSpecificExternalDatasetSearchDirs_m3856602952(__this, /*hidden argument*/NULL);
		GameObject_t1811656094 * L_0 = Component_get_gameObject_m2283027183(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		GameObject_AddComponent_TisComponentFactoryStarterBehaviour_t2906678938_m2160705532(L_0, /*hidden argument*/GameObject_AddComponent_TisComponentFactoryStarterBehaviour_t2906678938_m2160705532_RuntimeMethod_var);
		VuforiaAbstractBehaviour_Awake_m4140787687(__this, /*hidden argument*/NULL);
		return;
	}
}
// Vuforia.VuforiaBehaviour Vuforia.VuforiaBehaviour::get_Instance()
extern "C"  VuforiaBehaviour_t834098562 * VuforiaBehaviour_get_Instance_m621388178 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VuforiaBehaviour_get_Instance_m621388178_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(VuforiaBehaviour_t834098562_il2cpp_TypeInfo_var);
		VuforiaBehaviour_t834098562 * L_0 = ((VuforiaBehaviour_t834098562_StaticFields*)il2cpp_codegen_static_fields_for(VuforiaBehaviour_t834098562_il2cpp_TypeInfo_var))->get_mVuforiaBehaviour_19();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1502412432_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_m2342070775(NULL /*static, unused*/, L_0, (Object_t1502412432 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001a;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1502412432_il2cpp_TypeInfo_var);
		VuforiaBehaviour_t834098562 * L_2 = Object_FindObjectOfType_TisVuforiaBehaviour_t834098562_m3072761646(NULL /*static, unused*/, /*hidden argument*/Object_FindObjectOfType_TisVuforiaBehaviour_t834098562_m3072761646_RuntimeMethod_var);
		IL2CPP_RUNTIME_CLASS_INIT(VuforiaBehaviour_t834098562_il2cpp_TypeInfo_var);
		((VuforiaBehaviour_t834098562_StaticFields*)il2cpp_codegen_static_fields_for(VuforiaBehaviour_t834098562_il2cpp_TypeInfo_var))->set_mVuforiaBehaviour_19(L_2);
	}

IL_001a:
	{
		IL2CPP_RUNTIME_CLASS_INIT(VuforiaBehaviour_t834098562_il2cpp_TypeInfo_var);
		VuforiaBehaviour_t834098562 * L_3 = ((VuforiaBehaviour_t834098562_StaticFields*)il2cpp_codegen_static_fields_for(VuforiaBehaviour_t834098562_il2cpp_TypeInfo_var))->get_mVuforiaBehaviour_19();
		return L_3;
	}
}
// System.Void Vuforia.VuforiaBehaviour::AddOSSpecificExternalDatasetSearchDirs()
extern "C"  void VuforiaBehaviour_AddOSSpecificExternalDatasetSearchDirs_m3856602952 (VuforiaBehaviour_t834098562 * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Void Vuforia.VuforiaBehaviour::.cctor()
extern "C"  void VuforiaBehaviour__cctor_m207459036 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Void Vuforia.VuforiaBehaviourComponentFactory::.ctor()
extern "C"  void VuforiaBehaviourComponentFactory__ctor_m2242124886 (VuforiaBehaviourComponentFactory_t2590131513 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m2906060009(__this, /*hidden argument*/NULL);
		return;
	}
}
// Vuforia.MaskOutAbstractBehaviour Vuforia.VuforiaBehaviourComponentFactory::AddMaskOutBehaviour(UnityEngine.GameObject)
extern "C"  MaskOutAbstractBehaviour_t2174599097 * VuforiaBehaviourComponentFactory_AddMaskOutBehaviour_m3026848378 (VuforiaBehaviourComponentFactory_t2590131513 * __this, GameObject_t1811656094 * ___gameObject0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VuforiaBehaviourComponentFactory_AddMaskOutBehaviour_m3026848378_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1811656094 * L_0 = ___gameObject0;
		NullCheck(L_0);
		MaskOutBehaviour_t62027326 * L_1 = GameObject_AddComponent_TisMaskOutBehaviour_t62027326_m1393573829(L_0, /*hidden argument*/GameObject_AddComponent_TisMaskOutBehaviour_t62027326_m1393573829_RuntimeMethod_var);
		return L_1;
	}
}
// Vuforia.VirtualButtonAbstractBehaviour Vuforia.VuforiaBehaviourComponentFactory::AddVirtualButtonBehaviour(UnityEngine.GameObject)
extern "C"  VirtualButtonAbstractBehaviour_t1965820348 * VuforiaBehaviourComponentFactory_AddVirtualButtonBehaviour_m3283698660 (VuforiaBehaviourComponentFactory_t2590131513 * __this, GameObject_t1811656094 * ___gameObject0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VuforiaBehaviourComponentFactory_AddVirtualButtonBehaviour_m3283698660_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1811656094 * L_0 = ___gameObject0;
		NullCheck(L_0);
		VirtualButtonBehaviour_t1019531330 * L_1 = GameObject_AddComponent_TisVirtualButtonBehaviour_t1019531330_m1545170832(L_0, /*hidden argument*/GameObject_AddComponent_TisVirtualButtonBehaviour_t1019531330_m1545170832_RuntimeMethod_var);
		return L_1;
	}
}
// Vuforia.TurnOffAbstractBehaviour Vuforia.VuforiaBehaviourComponentFactory::AddTurnOffBehaviour(UnityEngine.GameObject)
extern "C"  TurnOffAbstractBehaviour_t1066217039 * VuforiaBehaviourComponentFactory_AddTurnOffBehaviour_m1098537926 (VuforiaBehaviourComponentFactory_t2590131513 * __this, GameObject_t1811656094 * ___gameObject0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VuforiaBehaviourComponentFactory_AddTurnOffBehaviour_m1098537926_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1811656094 * L_0 = ___gameObject0;
		NullCheck(L_0);
		TurnOffBehaviour_t963849236 * L_1 = GameObject_AddComponent_TisTurnOffBehaviour_t963849236_m3647489448(L_0, /*hidden argument*/GameObject_AddComponent_TisTurnOffBehaviour_t963849236_m3647489448_RuntimeMethod_var);
		return L_1;
	}
}
// Vuforia.ImageTargetAbstractBehaviour Vuforia.VuforiaBehaviourComponentFactory::AddImageTargetBehaviour(UnityEngine.GameObject)
extern "C"  ImageTargetAbstractBehaviour_t355543434 * VuforiaBehaviourComponentFactory_AddImageTargetBehaviour_m658548697 (VuforiaBehaviourComponentFactory_t2590131513 * __this, GameObject_t1811656094 * ___gameObject0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VuforiaBehaviourComponentFactory_AddImageTargetBehaviour_m658548697_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1811656094 * L_0 = ___gameObject0;
		NullCheck(L_0);
		ImageTargetBehaviour_t2918114530 * L_1 = GameObject_AddComponent_TisImageTargetBehaviour_t2918114530_m3204365795(L_0, /*hidden argument*/GameObject_AddComponent_TisImageTargetBehaviour_t2918114530_m3204365795_RuntimeMethod_var);
		return L_1;
	}
}
// Vuforia.MultiTargetAbstractBehaviour Vuforia.VuforiaBehaviourComponentFactory::AddMultiTargetBehaviour(UnityEngine.GameObject)
extern "C"  MultiTargetAbstractBehaviour_t1527469731 * VuforiaBehaviourComponentFactory_AddMultiTargetBehaviour_m3722869120 (VuforiaBehaviourComponentFactory_t2590131513 * __this, GameObject_t1811656094 * ___gameObject0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VuforiaBehaviourComponentFactory_AddMultiTargetBehaviour_m3722869120_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1811656094 * L_0 = ___gameObject0;
		NullCheck(L_0);
		MultiTargetBehaviour_t687655925 * L_1 = GameObject_AddComponent_TisMultiTargetBehaviour_t687655925_m601583604(L_0, /*hidden argument*/GameObject_AddComponent_TisMultiTargetBehaviour_t687655925_m601583604_RuntimeMethod_var);
		return L_1;
	}
}
// Vuforia.CylinderTargetAbstractBehaviour Vuforia.VuforiaBehaviourComponentFactory::AddCylinderTargetBehaviour(UnityEngine.GameObject)
extern "C"  CylinderTargetAbstractBehaviour_t2158579059 * VuforiaBehaviourComponentFactory_AddCylinderTargetBehaviour_m4246728464 (VuforiaBehaviourComponentFactory_t2590131513 * __this, GameObject_t1811656094 * ___gameObject0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VuforiaBehaviourComponentFactory_AddCylinderTargetBehaviour_m4246728464_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1811656094 * L_0 = ___gameObject0;
		NullCheck(L_0);
		CylinderTargetBehaviour_t1224832031 * L_1 = GameObject_AddComponent_TisCylinderTargetBehaviour_t1224832031_m4238243230(L_0, /*hidden argument*/GameObject_AddComponent_TisCylinderTargetBehaviour_t1224832031_m4238243230_RuntimeMethod_var);
		return L_1;
	}
}
// Vuforia.WordAbstractBehaviour Vuforia.VuforiaBehaviourComponentFactory::AddWordBehaviour(UnityEngine.GameObject)
extern "C"  WordAbstractBehaviour_t2339709601 * VuforiaBehaviourComponentFactory_AddWordBehaviour_m2557318930 (VuforiaBehaviourComponentFactory_t2590131513 * __this, GameObject_t1811656094 * ___gameObject0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VuforiaBehaviourComponentFactory_AddWordBehaviour_m2557318930_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1811656094 * L_0 = ___gameObject0;
		NullCheck(L_0);
		WordBehaviour_t2625704100 * L_1 = GameObject_AddComponent_TisWordBehaviour_t2625704100_m1670100688(L_0, /*hidden argument*/GameObject_AddComponent_TisWordBehaviour_t2625704100_m1670100688_RuntimeMethod_var);
		return L_1;
	}
}
// Vuforia.TextRecoAbstractBehaviour Vuforia.VuforiaBehaviourComponentFactory::AddTextRecoBehaviour(UnityEngine.GameObject)
extern "C"  TextRecoAbstractBehaviour_t1751158196 * VuforiaBehaviourComponentFactory_AddTextRecoBehaviour_m2362397526 (VuforiaBehaviourComponentFactory_t2590131513 * __this, GameObject_t1811656094 * ___gameObject0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VuforiaBehaviourComponentFactory_AddTextRecoBehaviour_m2362397526_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1811656094 * L_0 = ___gameObject0;
		NullCheck(L_0);
		TextRecoBehaviour_t3253755448 * L_1 = GameObject_AddComponent_TisTextRecoBehaviour_t3253755448_m1170737699(L_0, /*hidden argument*/GameObject_AddComponent_TisTextRecoBehaviour_t3253755448_m1170737699_RuntimeMethod_var);
		return L_1;
	}
}
// Vuforia.ObjectTargetAbstractBehaviour Vuforia.VuforiaBehaviourComponentFactory::AddObjectTargetBehaviour(UnityEngine.GameObject)
extern "C"  ObjectTargetAbstractBehaviour_t1410714589 * VuforiaBehaviourComponentFactory_AddObjectTargetBehaviour_m4276558463 (VuforiaBehaviourComponentFactory_t2590131513 * __this, GameObject_t1811656094 * ___gameObject0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VuforiaBehaviourComponentFactory_AddObjectTargetBehaviour_m4276558463_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1811656094 * L_0 = ___gameObject0;
		NullCheck(L_0);
		ObjectTargetBehaviour_t1761394684 * L_1 = GameObject_AddComponent_TisObjectTargetBehaviour_t1761394684_m3858239762(L_0, /*hidden argument*/GameObject_AddComponent_TisObjectTargetBehaviour_t1761394684_m3858239762_RuntimeMethod_var);
		return L_1;
	}
}
// Vuforia.VuMarkAbstractBehaviour Vuforia.VuforiaBehaviourComponentFactory::AddVuMarkBehaviour(UnityEngine.GameObject)
extern "C"  VuMarkAbstractBehaviour_t2652615854 * VuforiaBehaviourComponentFactory_AddVuMarkBehaviour_m1094247509 (VuforiaBehaviourComponentFactory_t2590131513 * __this, GameObject_t1811656094 * ___gameObject0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VuforiaBehaviourComponentFactory_AddVuMarkBehaviour_m1094247509_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1811656094 * L_0 = ___gameObject0;
		NullCheck(L_0);
		VuMarkBehaviour_t3122351057 * L_1 = GameObject_AddComponent_TisVuMarkBehaviour_t3122351057_m2352772220(L_0, /*hidden argument*/GameObject_AddComponent_TisVuMarkBehaviour_t3122351057_m2352772220_RuntimeMethod_var);
		return L_1;
	}
}
// Vuforia.VuforiaAbstractConfiguration Vuforia.VuforiaBehaviourComponentFactory::CreateVuforiaConfiguration()
extern "C"  VuforiaAbstractConfiguration_t974438950 * VuforiaBehaviourComponentFactory_CreateVuforiaConfiguration_m4138783976 (VuforiaBehaviourComponentFactory_t2590131513 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VuforiaBehaviourComponentFactory_CreateVuforiaConfiguration_m4138783976_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		VuforiaConfiguration_t2489601127 * L_0 = ScriptableObject_CreateInstance_TisVuforiaConfiguration_t2489601127_m4156053030(NULL /*static, unused*/, /*hidden argument*/ScriptableObject_CreateInstance_TisVuforiaConfiguration_t2489601127_m4156053030_RuntimeMethod_var);
		return L_0;
	}
}
// System.Void Vuforia.VuforiaConfiguration::.ctor()
extern "C"  void VuforiaConfiguration__ctor_m1559832131 (VuforiaConfiguration_t2489601127 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VuforiaConfiguration__ctor_m1559832131_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(VuforiaAbstractConfiguration_t974438950_il2cpp_TypeInfo_var);
		VuforiaAbstractConfiguration__ctor_m2046194188(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.VuforiaRuntimeInitialization::InitPlatform()
extern "C"  void VuforiaRuntimeInitialization_InitPlatform_m1082759092 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VuforiaRuntimeInitialization_InitPlatform_m1082759092_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(VuforiaUnity_t3212967305_il2cpp_TypeInfo_var);
		VuforiaUnity_SetStandardInitializationParameters_m2442996971(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(VuforiaRuntime_t2131788666_il2cpp_TypeInfo_var);
		VuforiaRuntime_t2131788666 * L_0 = VuforiaRuntime_get_Instance_m3676190030(NULL /*static, unused*/, /*hidden argument*/NULL);
		RuntimeObject* L_1 = VuforiaRuntimeInitialization_CreateUnityPlayer_m667590709(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		VuforiaRuntime_InitPlatform_m1211554959(L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.VuforiaRuntimeInitialization::InitVuforia()
extern "C"  void VuforiaRuntimeInitialization_InitVuforia_m609753717 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VuforiaRuntimeInitialization_InitVuforia_m609753717_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(VuforiaAbstractConfiguration_t974438950_il2cpp_TypeInfo_var);
		VuforiaAbstractConfiguration_t974438950 * L_0 = VuforiaAbstractConfiguration_get_Instance_m2225166302(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		GenericVuforiaConfiguration_t1574456586 * L_1 = VuforiaAbstractConfiguration_get_Vuforia_m3071337362(L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		bool L_2 = GenericVuforiaConfiguration_get_DelayedInitialization_m2669455364(L_1, /*hidden argument*/NULL);
		if (L_2)
		{
			goto IL_001e;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(VuforiaRuntime_t2131788666_il2cpp_TypeInfo_var);
		VuforiaRuntime_t2131788666 * L_3 = VuforiaRuntime_get_Instance_m3676190030(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_3);
		VuforiaRuntime_InitVuforia_m2414741092(L_3, /*hidden argument*/NULL);
	}

IL_001e:
	{
		return;
	}
}
// Vuforia.IUnityPlayer Vuforia.VuforiaRuntimeInitialization::CreateUnityPlayer()
extern "C"  RuntimeObject* VuforiaRuntimeInitialization_CreateUnityPlayer_m667590709 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VuforiaRuntimeInitialization_CreateUnityPlayer_m667590709_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RuntimeObject* V_0 = NULL;
	{
		NullUnityPlayer_t1636390204 * L_0 = (NullUnityPlayer_t1636390204 *)il2cpp_codegen_object_new(NullUnityPlayer_t1636390204_il2cpp_TypeInfo_var);
		NullUnityPlayer__ctor_m1922586283(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		int32_t L_1 = Application_get_platform_m262417742(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_1) == ((uint32_t)((int32_t)11)))))
		{
			goto IL_001d;
		}
	}
	{
		AndroidUnityPlayer_t3571566439 * L_2 = (AndroidUnityPlayer_t3571566439 *)il2cpp_codegen_object_new(AndroidUnityPlayer_t3571566439_il2cpp_TypeInfo_var);
		AndroidUnityPlayer__ctor_m3800978023(L_2, /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_0058;
	}

IL_001d:
	{
		int32_t L_3 = Application_get_platform_m262417742(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_3) == ((uint32_t)8))))
		{
			goto IL_0033;
		}
	}
	{
		IOSUnityPlayer_t1005570149 * L_4 = (IOSUnityPlayer_t1005570149 *)il2cpp_codegen_object_new(IOSUnityPlayer_t1005570149_il2cpp_TypeInfo_var);
		IOSUnityPlayer__ctor_m3437806645(L_4, /*hidden argument*/NULL);
		V_0 = L_4;
		goto IL_0058;
	}

IL_0033:
	{
		IL2CPP_RUNTIME_CLASS_INIT(VuforiaRuntimeUtilities_t1036756025_il2cpp_TypeInfo_var);
		bool L_5 = VuforiaRuntimeUtilities_IsPlayMode_m181100841(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0048;
		}
	}
	{
		PlayModeUnityPlayer_t2581155120 * L_6 = (PlayModeUnityPlayer_t2581155120 *)il2cpp_codegen_object_new(PlayModeUnityPlayer_t2581155120_il2cpp_TypeInfo_var);
		PlayModeUnityPlayer__ctor_m2846066907(L_6, /*hidden argument*/NULL);
		V_0 = L_6;
		goto IL_0058;
	}

IL_0048:
	{
		IL2CPP_RUNTIME_CLASS_INIT(VuforiaRuntimeUtilities_t1036756025_il2cpp_TypeInfo_var);
		bool L_7 = VuforiaRuntimeUtilities_IsWSARuntime_m948106021(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0058;
		}
	}
	{
		WSAUnityPlayer_t1576425251 * L_8 = (WSAUnityPlayer_t1576425251 *)il2cpp_codegen_object_new(WSAUnityPlayer_t1576425251_il2cpp_TypeInfo_var);
		WSAUnityPlayer__ctor_m1316825009(L_8, /*hidden argument*/NULL);
		V_0 = L_8;
	}

IL_0058:
	{
		RuntimeObject* L_9 = V_0;
		return L_9;
	}
}
// System.Void Vuforia.VuMarkBehaviour::.ctor()
extern "C"  void VuMarkBehaviour__ctor_m1730604351 (VuMarkBehaviour_t3122351057 * __this, const RuntimeMethod* method)
{
	{
		VuMarkAbstractBehaviour__ctor_m1676613122(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.WireframeBehaviour::.ctor()
extern "C"  void WireframeBehaviour__ctor_m2712434658 (WireframeBehaviour_t1899115476 * __this, const RuntimeMethod* method)
{
	{
		__this->set_ShowLines_3((bool)1);
		Color_t2507026410  L_0 = Color_get_green_m3178998959(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_LineColor_4(L_0);
		MonoBehaviour__ctor_m620219618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.WireframeBehaviour::Start()
extern "C"  void WireframeBehaviour_Start_m3315478633 (WireframeBehaviour_t1899115476 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WireframeBehaviour_Start_m3315478633_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Material_t1079520667 * L_0 = __this->get_lineMaterial_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1502412432_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m1918954159(NULL /*static, unused*/, L_0, (Object_t1502412432 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0027;
		}
	}
	{
		Material_t1079520667 * L_2 = __this->get_lineMaterial_2();
		Material_t1079520667 * L_3 = (Material_t1079520667 *)il2cpp_codegen_object_new(Material_t1079520667_il2cpp_TypeInfo_var);
		Material__ctor_m2014895328(L_3, L_2, /*hidden argument*/NULL);
		__this->set_mLineMaterial_5(L_3);
		goto IL_0031;
	}

IL_0027:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3525713211_il2cpp_TypeInfo_var);
		Debug_LogWarning_m1906964694(NULL /*static, unused*/, _stringLiteral3620006369, /*hidden argument*/NULL);
	}

IL_0031:
	{
		return;
	}
}
// System.Void Vuforia.WireframeBehaviour::OnRenderObject()
extern "C"  void WireframeBehaviour_OnRenderObject_m2382856100 (WireframeBehaviour_t1899115476 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WireframeBehaviour_OnRenderObject_m2382856100_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	GameObject_t1811656094 * V_0 = NULL;
	CameraU5BU5D_t1519774542* V_1 = NULL;
	bool V_2 = false;
	Camera_t3175186167 * V_3 = NULL;
	CameraU5BU5D_t1519774542* V_4 = NULL;
	int32_t V_5 = 0;
	MeshFilter_t137687116 * V_6 = NULL;
	Mesh_t996500909 * V_7 = NULL;
	Vector3U5BU5D_t981680887* V_8 = NULL;
	Int32U5BU5D_t2324750880* V_9 = NULL;
	int32_t V_10 = 0;
	Vector3_t2903530434  V_11;
	memset(&V_11, 0, sizeof(V_11));
	Vector3_t2903530434  V_12;
	memset(&V_12, 0, sizeof(V_12));
	Vector3_t2903530434  V_13;
	memset(&V_13, 0, sizeof(V_13));
	{
		IL2CPP_RUNTIME_CLASS_INIT(VuforiaManager_t745032010_il2cpp_TypeInfo_var);
		VuforiaManager_t745032010 * L_0 = VuforiaManager_get_Instance_m2308098604(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		Transform_t3316442598 * L_1 = VirtFuncInvoker0< Transform_t3316442598 * >::Invoke(10 /* UnityEngine.Transform Vuforia.VuforiaManager::get_ARCameraTransform() */, L_0);
		NullCheck(L_1);
		GameObject_t1811656094 * L_2 = Component_get_gameObject_m2283027183(L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		GameObject_t1811656094 * L_3 = V_0;
		NullCheck(L_3);
		CameraU5BU5D_t1519774542* L_4 = GameObject_GetComponentsInChildren_TisCamera_t3175186167_m4240236485(L_3, /*hidden argument*/GameObject_GetComponentsInChildren_TisCamera_t3175186167_m4240236485_RuntimeMethod_var);
		V_1 = L_4;
		V_2 = (bool)0;
		CameraU5BU5D_t1519774542* L_5 = V_1;
		V_4 = L_5;
		V_5 = 0;
		goto IL_0042;
	}

IL_0024:
	{
		CameraU5BU5D_t1519774542* L_6 = V_4;
		int32_t L_7 = V_5;
		NullCheck(L_6);
		int32_t L_8 = L_7;
		Camera_t3175186167 * L_9 = (L_6)->GetAt(static_cast<il2cpp_array_size_t>(L_8));
		V_3 = L_9;
		Camera_t3175186167 * L_10 = Camera_get_current_m3517990316(NULL /*static, unused*/, /*hidden argument*/NULL);
		Camera_t3175186167 * L_11 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1502412432_il2cpp_TypeInfo_var);
		bool L_12 = Object_op_Equality_m2342070775(NULL /*static, unused*/, L_10, L_11, /*hidden argument*/NULL);
		if (!L_12)
		{
			goto IL_003c;
		}
	}
	{
		V_2 = (bool)1;
	}

IL_003c:
	{
		int32_t L_13 = V_5;
		V_5 = ((int32_t)((int32_t)L_13+(int32_t)1));
	}

IL_0042:
	{
		int32_t L_14 = V_5;
		CameraU5BU5D_t1519774542* L_15 = V_4;
		NullCheck(L_15);
		if ((((int32_t)L_14) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_15)->max_length)))))))
		{
			goto IL_0024;
		}
	}
	{
		bool L_16 = V_2;
		if (L_16)
		{
			goto IL_0054;
		}
	}
	{
		return;
	}

IL_0054:
	{
		bool L_17 = __this->get_ShowLines_3();
		if (L_17)
		{
			goto IL_0060;
		}
	}
	{
		return;
	}

IL_0060:
	{
		MeshFilter_t137687116 * L_18 = Component_GetComponent_TisMeshFilter_t137687116_m62619598(__this, /*hidden argument*/Component_GetComponent_TisMeshFilter_t137687116_m62619598_RuntimeMethod_var);
		V_6 = L_18;
		MeshFilter_t137687116 * L_19 = V_6;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1502412432_il2cpp_TypeInfo_var);
		bool L_20 = Object_op_Implicit_m2314733868(NULL /*static, unused*/, L_19, /*hidden argument*/NULL);
		if (L_20)
		{
			goto IL_0075;
		}
	}
	{
		return;
	}

IL_0075:
	{
		Material_t1079520667 * L_21 = __this->get_mLineMaterial_5();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1502412432_il2cpp_TypeInfo_var);
		bool L_22 = Object_op_Equality_m2342070775(NULL /*static, unused*/, L_21, (Object_t1502412432 *)NULL, /*hidden argument*/NULL);
		if (!L_22)
		{
			goto IL_0091;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3525713211_il2cpp_TypeInfo_var);
		Debug_LogWarning_m1906964694(NULL /*static, unused*/, _stringLiteral3620006369, /*hidden argument*/NULL);
		return;
	}

IL_0091:
	{
		MeshFilter_t137687116 * L_23 = V_6;
		NullCheck(L_23);
		Mesh_t996500909 * L_24 = MeshFilter_get_sharedMesh_m306256552(L_23, /*hidden argument*/NULL);
		V_7 = L_24;
		Mesh_t996500909 * L_25 = V_7;
		NullCheck(L_25);
		Vector3U5BU5D_t981680887* L_26 = Mesh_get_vertices_m1276761327(L_25, /*hidden argument*/NULL);
		V_8 = L_26;
		Mesh_t996500909 * L_27 = V_7;
		NullCheck(L_27);
		Int32U5BU5D_t2324750880* L_28 = Mesh_get_triangles_m2785445945(L_27, /*hidden argument*/NULL);
		V_9 = L_28;
		GL_PushMatrix_m3818946253(NULL /*static, unused*/, /*hidden argument*/NULL);
		Transform_t3316442598 * L_29 = Component_get_transform_m2038396632(__this, /*hidden argument*/NULL);
		NullCheck(L_29);
		Matrix4x4_t4266809202  L_30 = Transform_get_localToWorldMatrix_m2977818424(L_29, /*hidden argument*/NULL);
		GL_MultMatrix_m1086021952(NULL /*static, unused*/, L_30, /*hidden argument*/NULL);
		Material_t1079520667 * L_31 = __this->get_mLineMaterial_5();
		NullCheck(L_31);
		Material_SetPass_m924837141(L_31, 0, /*hidden argument*/NULL);
		Material_t1079520667 * L_32 = __this->get_mLineMaterial_5();
		Color_t2507026410  L_33 = __this->get_LineColor_4();
		NullCheck(L_32);
		Material_SetColor_m4127411957(L_32, _stringLiteral2847251467, L_33, /*hidden argument*/NULL);
		GL_Begin_m2439050522(NULL /*static, unused*/, 1, /*hidden argument*/NULL);
		V_10 = 0;
		goto IL_015f;
	}

IL_00f2:
	{
		Vector3U5BU5D_t981680887* L_34 = V_8;
		Int32U5BU5D_t2324750880* L_35 = V_9;
		int32_t L_36 = V_10;
		NullCheck(L_35);
		int32_t L_37 = L_36;
		int32_t L_38 = (L_35)->GetAt(static_cast<il2cpp_array_size_t>(L_37));
		NullCheck(L_34);
		V_11 = (*(Vector3_t2903530434 *)((L_34)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_38))));
		Vector3U5BU5D_t981680887* L_39 = V_8;
		Int32U5BU5D_t2324750880* L_40 = V_9;
		int32_t L_41 = V_10;
		NullCheck(L_40);
		int32_t L_42 = ((int32_t)((int32_t)L_41+(int32_t)1));
		int32_t L_43 = (L_40)->GetAt(static_cast<il2cpp_array_size_t>(L_42));
		NullCheck(L_39);
		V_12 = (*(Vector3_t2903530434 *)((L_39)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_43))));
		Vector3U5BU5D_t981680887* L_44 = V_8;
		Int32U5BU5D_t2324750880* L_45 = V_9;
		int32_t L_46 = V_10;
		NullCheck(L_45);
		int32_t L_47 = ((int32_t)((int32_t)L_46+(int32_t)2));
		int32_t L_48 = (L_45)->GetAt(static_cast<il2cpp_array_size_t>(L_47));
		NullCheck(L_44);
		V_13 = (*(Vector3_t2903530434 *)((L_44)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_48))));
		Vector3_t2903530434  L_49 = V_11;
		GL_Vertex_m3910287719(NULL /*static, unused*/, L_49, /*hidden argument*/NULL);
		Vector3_t2903530434  L_50 = V_12;
		GL_Vertex_m3910287719(NULL /*static, unused*/, L_50, /*hidden argument*/NULL);
		Vector3_t2903530434  L_51 = V_12;
		GL_Vertex_m3910287719(NULL /*static, unused*/, L_51, /*hidden argument*/NULL);
		Vector3_t2903530434  L_52 = V_13;
		GL_Vertex_m3910287719(NULL /*static, unused*/, L_52, /*hidden argument*/NULL);
		Vector3_t2903530434  L_53 = V_13;
		GL_Vertex_m3910287719(NULL /*static, unused*/, L_53, /*hidden argument*/NULL);
		Vector3_t2903530434  L_54 = V_11;
		GL_Vertex_m3910287719(NULL /*static, unused*/, L_54, /*hidden argument*/NULL);
		int32_t L_55 = V_10;
		V_10 = ((int32_t)((int32_t)L_55+(int32_t)3));
	}

IL_015f:
	{
		int32_t L_56 = V_10;
		Int32U5BU5D_t2324750880* L_57 = V_9;
		NullCheck(L_57);
		if ((((int32_t)L_56) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_57)->max_length)))))))
		{
			goto IL_00f2;
		}
	}
	{
		GL_End_m1567383974(NULL /*static, unused*/, /*hidden argument*/NULL);
		GL_PopMatrix_m2336262285(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.WireframeBehaviour::OnDrawGizmos()
extern "C"  void WireframeBehaviour_OnDrawGizmos_m2104809653 (WireframeBehaviour_t1899115476 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WireframeBehaviour_OnDrawGizmos_m2104809653_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	MeshFilter_t137687116 * V_0 = NULL;
	Mesh_t996500909 * V_1 = NULL;
	Vector3U5BU5D_t981680887* V_2 = NULL;
	Int32U5BU5D_t2324750880* V_3 = NULL;
	int32_t V_4 = 0;
	Vector3_t2903530434  V_5;
	memset(&V_5, 0, sizeof(V_5));
	Vector3_t2903530434  V_6;
	memset(&V_6, 0, sizeof(V_6));
	Vector3_t2903530434  V_7;
	memset(&V_7, 0, sizeof(V_7));
	{
		bool L_0 = __this->get_ShowLines_3();
		if (!L_0)
		{
			goto IL_00f9;
		}
	}
	{
		bool L_1 = Behaviour_get_enabled_m2220542426(__this, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_00f9;
		}
	}
	{
		MeshFilter_t137687116 * L_2 = Component_GetComponent_TisMeshFilter_t137687116_m62619598(__this, /*hidden argument*/Component_GetComponent_TisMeshFilter_t137687116_m62619598_RuntimeMethod_var);
		V_0 = L_2;
		MeshFilter_t137687116 * L_3 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1502412432_il2cpp_TypeInfo_var);
		bool L_4 = Object_op_Implicit_m2314733868(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		if (L_4)
		{
			goto IL_0029;
		}
	}
	{
		return;
	}

IL_0029:
	{
		GameObject_t1811656094 * L_5 = Component_get_gameObject_m2283027183(__this, /*hidden argument*/NULL);
		NullCheck(L_5);
		Transform_t3316442598 * L_6 = GameObject_get_transform_m166152100(L_5, /*hidden argument*/NULL);
		NullCheck(L_6);
		Vector3_t2903530434  L_7 = Transform_get_position_m1291471697(L_6, /*hidden argument*/NULL);
		GameObject_t1811656094 * L_8 = Component_get_gameObject_m2283027183(__this, /*hidden argument*/NULL);
		NullCheck(L_8);
		Transform_t3316442598 * L_9 = GameObject_get_transform_m166152100(L_8, /*hidden argument*/NULL);
		NullCheck(L_9);
		Quaternion_t754065749  L_10 = Transform_get_rotation_m2679542584(L_9, /*hidden argument*/NULL);
		GameObject_t1811656094 * L_11 = Component_get_gameObject_m2283027183(__this, /*hidden argument*/NULL);
		NullCheck(L_11);
		Transform_t3316442598 * L_12 = GameObject_get_transform_m166152100(L_11, /*hidden argument*/NULL);
		NullCheck(L_12);
		Vector3_t2903530434  L_13 = Transform_get_lossyScale_m3456855661(L_12, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Matrix4x4_t4266809202_il2cpp_TypeInfo_var);
		Matrix4x4_t4266809202  L_14 = Matrix4x4_TRS_m4277632264(NULL /*static, unused*/, L_7, L_10, L_13, /*hidden argument*/NULL);
		Gizmos_set_matrix_m2928965184(NULL /*static, unused*/, L_14, /*hidden argument*/NULL);
		Color_t2507026410  L_15 = __this->get_LineColor_4();
		Gizmos_set_color_m1207670475(NULL /*static, unused*/, L_15, /*hidden argument*/NULL);
		MeshFilter_t137687116 * L_16 = V_0;
		NullCheck(L_16);
		Mesh_t996500909 * L_17 = MeshFilter_get_sharedMesh_m306256552(L_16, /*hidden argument*/NULL);
		V_1 = L_17;
		Mesh_t996500909 * L_18 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1502412432_il2cpp_TypeInfo_var);
		bool L_19 = Object_op_Inequality_m1918954159(NULL /*static, unused*/, L_18, (Object_t1502412432 *)NULL, /*hidden argument*/NULL);
		if (!L_19)
		{
			goto IL_00f9;
		}
	}
	{
		Mesh_t996500909 * L_20 = V_1;
		NullCheck(L_20);
		Vector3U5BU5D_t981680887* L_21 = Mesh_get_vertices_m1276761327(L_20, /*hidden argument*/NULL);
		V_2 = L_21;
		Mesh_t996500909 * L_22 = V_1;
		NullCheck(L_22);
		Int32U5BU5D_t2324750880* L_23 = Mesh_get_triangles_m2785445945(L_22, /*hidden argument*/NULL);
		V_3 = L_23;
		V_4 = 0;
		goto IL_00ef;
	}

IL_0097:
	{
		Vector3U5BU5D_t981680887* L_24 = V_2;
		Int32U5BU5D_t2324750880* L_25 = V_3;
		int32_t L_26 = V_4;
		NullCheck(L_25);
		int32_t L_27 = L_26;
		int32_t L_28 = (L_25)->GetAt(static_cast<il2cpp_array_size_t>(L_27));
		NullCheck(L_24);
		V_5 = (*(Vector3_t2903530434 *)((L_24)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_28))));
		Vector3U5BU5D_t981680887* L_29 = V_2;
		Int32U5BU5D_t2324750880* L_30 = V_3;
		int32_t L_31 = V_4;
		NullCheck(L_30);
		int32_t L_32 = ((int32_t)((int32_t)L_31+(int32_t)1));
		int32_t L_33 = (L_30)->GetAt(static_cast<il2cpp_array_size_t>(L_32));
		NullCheck(L_29);
		V_6 = (*(Vector3_t2903530434 *)((L_29)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_33))));
		Vector3U5BU5D_t981680887* L_34 = V_2;
		Int32U5BU5D_t2324750880* L_35 = V_3;
		int32_t L_36 = V_4;
		NullCheck(L_35);
		int32_t L_37 = ((int32_t)((int32_t)L_36+(int32_t)2));
		int32_t L_38 = (L_35)->GetAt(static_cast<il2cpp_array_size_t>(L_37));
		NullCheck(L_34);
		V_7 = (*(Vector3_t2903530434 *)((L_34)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_38))));
		Vector3_t2903530434  L_39 = V_5;
		Vector3_t2903530434  L_40 = V_6;
		Gizmos_DrawLine_m1716497926(NULL /*static, unused*/, L_39, L_40, /*hidden argument*/NULL);
		Vector3_t2903530434  L_41 = V_6;
		Vector3_t2903530434  L_42 = V_7;
		Gizmos_DrawLine_m1716497926(NULL /*static, unused*/, L_41, L_42, /*hidden argument*/NULL);
		Vector3_t2903530434  L_43 = V_7;
		Vector3_t2903530434  L_44 = V_5;
		Gizmos_DrawLine_m1716497926(NULL /*static, unused*/, L_43, L_44, /*hidden argument*/NULL);
		int32_t L_45 = V_4;
		V_4 = ((int32_t)((int32_t)L_45+(int32_t)3));
	}

IL_00ef:
	{
		int32_t L_46 = V_4;
		Int32U5BU5D_t2324750880* L_47 = V_3;
		NullCheck(L_47);
		if ((((int32_t)L_46) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_47)->max_length)))))))
		{
			goto IL_0097;
		}
	}

IL_00f9:
	{
		return;
	}
}
// System.Void Vuforia.WireframeTrackableEventHandler::.ctor()
extern "C"  void WireframeTrackableEventHandler__ctor_m1304353620 (WireframeTrackableEventHandler_t680679714 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m620219618(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.WireframeTrackableEventHandler::Start()
extern "C"  void WireframeTrackableEventHandler_Start_m3884813205 (WireframeTrackableEventHandler_t680679714 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WireframeTrackableEventHandler_Start_m3884813205_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		TrackableBehaviour_t2419079356 * L_0 = Component_GetComponent_TisTrackableBehaviour_t2419079356_m3640808561(__this, /*hidden argument*/Component_GetComponent_TisTrackableBehaviour_t2419079356_m3640808561_RuntimeMethod_var);
		__this->set_mTrackableBehaviour_2(L_0);
		TrackableBehaviour_t2419079356 * L_1 = __this->get_mTrackableBehaviour_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1502412432_il2cpp_TypeInfo_var);
		bool L_2 = Object_op_Implicit_m2314733868(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0028;
		}
	}
	{
		TrackableBehaviour_t2419079356 * L_3 = __this->get_mTrackableBehaviour_2();
		NullCheck(L_3);
		TrackableBehaviour_RegisterTrackableEventHandler_m537253126(L_3, __this, /*hidden argument*/NULL);
	}

IL_0028:
	{
		return;
	}
}
// System.Void Vuforia.WireframeTrackableEventHandler::OnTrackableStateChanged(Vuforia.TrackableBehaviour/Status,Vuforia.TrackableBehaviour/Status)
extern "C"  void WireframeTrackableEventHandler_OnTrackableStateChanged_m1946778660 (WireframeTrackableEventHandler_t680679714 * __this, int32_t ___previousStatus0, int32_t ___newStatus1, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___newStatus1;
		if ((((int32_t)L_0) == ((int32_t)2)))
		{
			goto IL_000e;
		}
	}
	{
		int32_t L_1 = ___newStatus1;
		if ((!(((uint32_t)L_1) == ((uint32_t)3))))
		{
			goto IL_0019;
		}
	}

IL_000e:
	{
		WireframeTrackableEventHandler_OnTrackingFound_m2916522252(__this, /*hidden argument*/NULL);
		goto IL_001f;
	}

IL_0019:
	{
		WireframeTrackableEventHandler_OnTrackingLost_m1252073333(__this, /*hidden argument*/NULL);
	}

IL_001f:
	{
		return;
	}
}
// System.Void Vuforia.WireframeTrackableEventHandler::OnTrackingFound()
extern "C"  void WireframeTrackableEventHandler_OnTrackingFound_m2916522252 (WireframeTrackableEventHandler_t680679714 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WireframeTrackableEventHandler_OnTrackingFound_m2916522252_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RendererU5BU5D_t2667410251* V_0 = NULL;
	ColliderU5BU5D_t3746774603* V_1 = NULL;
	WireframeBehaviourU5BU5D_t1741061725* V_2 = NULL;
	Renderer_t1303575294 * V_3 = NULL;
	RendererU5BU5D_t2667410251* V_4 = NULL;
	int32_t V_5 = 0;
	Collider_t2301021182 * V_6 = NULL;
	ColliderU5BU5D_t3746774603* V_7 = NULL;
	int32_t V_8 = 0;
	WireframeBehaviour_t1899115476 * V_9 = NULL;
	WireframeBehaviourU5BU5D_t1741061725* V_10 = NULL;
	int32_t V_11 = 0;
	{
		RendererU5BU5D_t2667410251* L_0 = Component_GetComponentsInChildren_TisRenderer_t1303575294_m3094251748(__this, (bool)1, /*hidden argument*/Component_GetComponentsInChildren_TisRenderer_t1303575294_m3094251748_RuntimeMethod_var);
		V_0 = L_0;
		ColliderU5BU5D_t3746774603* L_1 = Component_GetComponentsInChildren_TisCollider_t2301021182_m857431636(__this, (bool)1, /*hidden argument*/Component_GetComponentsInChildren_TisCollider_t2301021182_m857431636_RuntimeMethod_var);
		V_1 = L_1;
		WireframeBehaviourU5BU5D_t1741061725* L_2 = Component_GetComponentsInChildren_TisWireframeBehaviour_t1899115476_m606499660(__this, (bool)1, /*hidden argument*/Component_GetComponentsInChildren_TisWireframeBehaviour_t1899115476_m606499660_RuntimeMethod_var);
		V_2 = L_2;
		RendererU5BU5D_t2667410251* L_3 = V_0;
		V_4 = L_3;
		V_5 = 0;
		goto IL_0036;
	}

IL_0023:
	{
		RendererU5BU5D_t2667410251* L_4 = V_4;
		int32_t L_5 = V_5;
		NullCheck(L_4);
		int32_t L_6 = L_5;
		Renderer_t1303575294 * L_7 = (L_4)->GetAt(static_cast<il2cpp_array_size_t>(L_6));
		V_3 = L_7;
		Renderer_t1303575294 * L_8 = V_3;
		NullCheck(L_8);
		Renderer_set_enabled_m3164806497(L_8, (bool)1, /*hidden argument*/NULL);
		int32_t L_9 = V_5;
		V_5 = ((int32_t)((int32_t)L_9+(int32_t)1));
	}

IL_0036:
	{
		int32_t L_10 = V_5;
		RendererU5BU5D_t2667410251* L_11 = V_4;
		NullCheck(L_11);
		if ((((int32_t)L_10) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_11)->max_length)))))))
		{
			goto IL_0023;
		}
	}
	{
		ColliderU5BU5D_t3746774603* L_12 = V_1;
		V_7 = L_12;
		V_8 = 0;
		goto IL_0061;
	}

IL_004c:
	{
		ColliderU5BU5D_t3746774603* L_13 = V_7;
		int32_t L_14 = V_8;
		NullCheck(L_13);
		int32_t L_15 = L_14;
		Collider_t2301021182 * L_16 = (L_13)->GetAt(static_cast<il2cpp_array_size_t>(L_15));
		V_6 = L_16;
		Collider_t2301021182 * L_17 = V_6;
		NullCheck(L_17);
		Collider_set_enabled_m1322675966(L_17, (bool)1, /*hidden argument*/NULL);
		int32_t L_18 = V_8;
		V_8 = ((int32_t)((int32_t)L_18+(int32_t)1));
	}

IL_0061:
	{
		int32_t L_19 = V_8;
		ColliderU5BU5D_t3746774603* L_20 = V_7;
		NullCheck(L_20);
		if ((((int32_t)L_19) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_20)->max_length)))))))
		{
			goto IL_004c;
		}
	}
	{
		WireframeBehaviourU5BU5D_t1741061725* L_21 = V_2;
		V_10 = L_21;
		V_11 = 0;
		goto IL_008c;
	}

IL_0077:
	{
		WireframeBehaviourU5BU5D_t1741061725* L_22 = V_10;
		int32_t L_23 = V_11;
		NullCheck(L_22);
		int32_t L_24 = L_23;
		WireframeBehaviour_t1899115476 * L_25 = (L_22)->GetAt(static_cast<il2cpp_array_size_t>(L_24));
		V_9 = L_25;
		WireframeBehaviour_t1899115476 * L_26 = V_9;
		NullCheck(L_26);
		Behaviour_set_enabled_m1710894011(L_26, (bool)1, /*hidden argument*/NULL);
		int32_t L_27 = V_11;
		V_11 = ((int32_t)((int32_t)L_27+(int32_t)1));
	}

IL_008c:
	{
		int32_t L_28 = V_11;
		WireframeBehaviourU5BU5D_t1741061725* L_29 = V_10;
		NullCheck(L_29);
		if ((((int32_t)L_28) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_29)->max_length)))))))
		{
			goto IL_0077;
		}
	}
	{
		TrackableBehaviour_t2419079356 * L_30 = __this->get_mTrackableBehaviour_2();
		NullCheck(L_30);
		String_t* L_31 = TrackableBehaviour_get_TrackableName_m3217816807(L_30, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_32 = String_Concat_m3656894380(NULL /*static, unused*/, _stringLiteral2756122832, L_31, _stringLiteral3913380118, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3525713211_il2cpp_TypeInfo_var);
		Debug_Log_m1673024736(NULL /*static, unused*/, L_32, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.WireframeTrackableEventHandler::OnTrackingLost()
extern "C"  void WireframeTrackableEventHandler_OnTrackingLost_m1252073333 (WireframeTrackableEventHandler_t680679714 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WireframeTrackableEventHandler_OnTrackingLost_m1252073333_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RendererU5BU5D_t2667410251* V_0 = NULL;
	ColliderU5BU5D_t3746774603* V_1 = NULL;
	WireframeBehaviourU5BU5D_t1741061725* V_2 = NULL;
	Renderer_t1303575294 * V_3 = NULL;
	RendererU5BU5D_t2667410251* V_4 = NULL;
	int32_t V_5 = 0;
	Collider_t2301021182 * V_6 = NULL;
	ColliderU5BU5D_t3746774603* V_7 = NULL;
	int32_t V_8 = 0;
	WireframeBehaviour_t1899115476 * V_9 = NULL;
	WireframeBehaviourU5BU5D_t1741061725* V_10 = NULL;
	int32_t V_11 = 0;
	{
		RendererU5BU5D_t2667410251* L_0 = Component_GetComponentsInChildren_TisRenderer_t1303575294_m3094251748(__this, (bool)1, /*hidden argument*/Component_GetComponentsInChildren_TisRenderer_t1303575294_m3094251748_RuntimeMethod_var);
		V_0 = L_0;
		ColliderU5BU5D_t3746774603* L_1 = Component_GetComponentsInChildren_TisCollider_t2301021182_m857431636(__this, (bool)1, /*hidden argument*/Component_GetComponentsInChildren_TisCollider_t2301021182_m857431636_RuntimeMethod_var);
		V_1 = L_1;
		WireframeBehaviourU5BU5D_t1741061725* L_2 = Component_GetComponentsInChildren_TisWireframeBehaviour_t1899115476_m606499660(__this, (bool)1, /*hidden argument*/Component_GetComponentsInChildren_TisWireframeBehaviour_t1899115476_m606499660_RuntimeMethod_var);
		V_2 = L_2;
		RendererU5BU5D_t2667410251* L_3 = V_0;
		V_4 = L_3;
		V_5 = 0;
		goto IL_0036;
	}

IL_0023:
	{
		RendererU5BU5D_t2667410251* L_4 = V_4;
		int32_t L_5 = V_5;
		NullCheck(L_4);
		int32_t L_6 = L_5;
		Renderer_t1303575294 * L_7 = (L_4)->GetAt(static_cast<il2cpp_array_size_t>(L_6));
		V_3 = L_7;
		Renderer_t1303575294 * L_8 = V_3;
		NullCheck(L_8);
		Renderer_set_enabled_m3164806497(L_8, (bool)0, /*hidden argument*/NULL);
		int32_t L_9 = V_5;
		V_5 = ((int32_t)((int32_t)L_9+(int32_t)1));
	}

IL_0036:
	{
		int32_t L_10 = V_5;
		RendererU5BU5D_t2667410251* L_11 = V_4;
		NullCheck(L_11);
		if ((((int32_t)L_10) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_11)->max_length)))))))
		{
			goto IL_0023;
		}
	}
	{
		ColliderU5BU5D_t3746774603* L_12 = V_1;
		V_7 = L_12;
		V_8 = 0;
		goto IL_0061;
	}

IL_004c:
	{
		ColliderU5BU5D_t3746774603* L_13 = V_7;
		int32_t L_14 = V_8;
		NullCheck(L_13);
		int32_t L_15 = L_14;
		Collider_t2301021182 * L_16 = (L_13)->GetAt(static_cast<il2cpp_array_size_t>(L_15));
		V_6 = L_16;
		Collider_t2301021182 * L_17 = V_6;
		NullCheck(L_17);
		Collider_set_enabled_m1322675966(L_17, (bool)0, /*hidden argument*/NULL);
		int32_t L_18 = V_8;
		V_8 = ((int32_t)((int32_t)L_18+(int32_t)1));
	}

IL_0061:
	{
		int32_t L_19 = V_8;
		ColliderU5BU5D_t3746774603* L_20 = V_7;
		NullCheck(L_20);
		if ((((int32_t)L_19) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_20)->max_length)))))))
		{
			goto IL_004c;
		}
	}
	{
		WireframeBehaviourU5BU5D_t1741061725* L_21 = V_2;
		V_10 = L_21;
		V_11 = 0;
		goto IL_008c;
	}

IL_0077:
	{
		WireframeBehaviourU5BU5D_t1741061725* L_22 = V_10;
		int32_t L_23 = V_11;
		NullCheck(L_22);
		int32_t L_24 = L_23;
		WireframeBehaviour_t1899115476 * L_25 = (L_22)->GetAt(static_cast<il2cpp_array_size_t>(L_24));
		V_9 = L_25;
		WireframeBehaviour_t1899115476 * L_26 = V_9;
		NullCheck(L_26);
		Behaviour_set_enabled_m1710894011(L_26, (bool)0, /*hidden argument*/NULL);
		int32_t L_27 = V_11;
		V_11 = ((int32_t)((int32_t)L_27+(int32_t)1));
	}

IL_008c:
	{
		int32_t L_28 = V_11;
		WireframeBehaviourU5BU5D_t1741061725* L_29 = V_10;
		NullCheck(L_29);
		if ((((int32_t)L_28) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_29)->max_length)))))))
		{
			goto IL_0077;
		}
	}
	{
		TrackableBehaviour_t2419079356 * L_30 = __this->get_mTrackableBehaviour_2();
		NullCheck(L_30);
		String_t* L_31 = TrackableBehaviour_get_TrackableName_m3217816807(L_30, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_32 = String_Concat_m3656894380(NULL /*static, unused*/, _stringLiteral2756122832, L_31, _stringLiteral2566987390, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3525713211_il2cpp_TypeInfo_var);
		Debug_Log_m1673024736(NULL /*static, unused*/, L_32, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.WordBehaviour::.ctor()
extern "C"  void WordBehaviour__ctor_m3358840847 (WordBehaviour_t2625704100 * __this, const RuntimeMethod* method)
{
	{
		WordAbstractBehaviour__ctor_m147923421(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.WSAUnityPlayer::.ctor()
extern "C"  void WSAUnityPlayer__ctor_m1316825009 (WSAUnityPlayer_t1576425251 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m2906060009(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.WSAUnityPlayer::LoadNativeLibraries()
extern "C"  void WSAUnityPlayer_LoadNativeLibraries_m411342808 (WSAUnityPlayer_t1576425251 * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Void Vuforia.WSAUnityPlayer::InitializePlatform()
extern "C"  void WSAUnityPlayer_InitializePlatform_m289968300 (WSAUnityPlayer_t1576425251 * __this, const RuntimeMethod* method)
{
	{
		WSAUnityPlayer_setPlatFormNative_m3380749227(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// Vuforia.VuforiaUnity/InitError Vuforia.WSAUnityPlayer::InitializeVuforia(System.String)
extern "C"  int32_t WSAUnityPlayer_InitializeVuforia_m3433808023 (WSAUnityPlayer_t1576425251 * __this, String_t* ___licenseKey0, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		String_t* L_0 = ___licenseKey0;
		int32_t L_1 = WSAUnityPlayer_initVuforiaWSA_m1079303392(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) < ((int32_t)0)))
		{
			goto IL_0014;
		}
	}
	{
		WSAUnityPlayer_InitializeSurface_m3924250633(__this, /*hidden argument*/NULL);
	}

IL_0014:
	{
		int32_t L_3 = V_0;
		return (int32_t)(L_3);
	}
}
// System.Void Vuforia.WSAUnityPlayer::StartScene()
extern "C"  void WSAUnityPlayer_StartScene_m214985046 (WSAUnityPlayer_t1576425251 * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Void Vuforia.WSAUnityPlayer::Update()
extern "C"  void WSAUnityPlayer_Update_m911514441 (WSAUnityPlayer_t1576425251 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WSAUnityPlayer_Update_m911514441_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(SurfaceUtilities_t184061569_il2cpp_TypeInfo_var);
		bool L_0 = SurfaceUtilities_HasSurfaceBeenRecreated_m346027931(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0015;
		}
	}
	{
		WSAUnityPlayer_InitializeSurface_m3924250633(__this, /*hidden argument*/NULL);
		goto IL_002e;
	}

IL_0015:
	{
		int32_t L_1 = WSAUnityPlayer_GetActualScreenOrientation_m436681916(__this, /*hidden argument*/NULL);
		V_0 = L_1;
		int32_t L_2 = V_0;
		int32_t L_3 = __this->get_mScreenOrientation_0();
		if ((((int32_t)L_2) == ((int32_t)L_3)))
		{
			goto IL_002e;
		}
	}
	{
		WSAUnityPlayer_SetUnityScreenOrientation_m2430850923(__this, /*hidden argument*/NULL);
	}

IL_002e:
	{
		return;
	}
}
// System.Void Vuforia.WSAUnityPlayer::Dispose()
extern "C"  void WSAUnityPlayer_Dispose_m1128661295 (WSAUnityPlayer_t1576425251 * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Void Vuforia.WSAUnityPlayer::OnPause()
extern "C"  void WSAUnityPlayer_OnPause_m2169356505 (WSAUnityPlayer_t1576425251 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WSAUnityPlayer_OnPause_m2169356505_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(VuforiaUnity_t3212967305_il2cpp_TypeInfo_var);
		VuforiaUnity_OnPause_m4153953119(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.WSAUnityPlayer::OnResume()
extern "C"  void WSAUnityPlayer_OnResume_m1070021309 (WSAUnityPlayer_t1576425251 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WSAUnityPlayer_OnResume_m1070021309_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(VuforiaUnity_t3212967305_il2cpp_TypeInfo_var);
		VuforiaUnity_OnResume_m2287209481(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.WSAUnityPlayer::OnDestroy()
extern "C"  void WSAUnityPlayer_OnDestroy_m3442614810 (WSAUnityPlayer_t1576425251 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WSAUnityPlayer_OnDestroy_m3442614810_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(VuforiaUnity_t3212967305_il2cpp_TypeInfo_var);
		VuforiaUnity_Deinit_m2222663190(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.WSAUnityPlayer::InitializeSurface()
extern "C"  void WSAUnityPlayer_InitializeSurface_m3924250633 (WSAUnityPlayer_t1576425251 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WSAUnityPlayer_InitializeSurface_m3924250633_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SurfaceUtilities_t184061569_il2cpp_TypeInfo_var);
		SurfaceUtilities_OnSurfaceCreated_m1867899004(NULL /*static, unused*/, /*hidden argument*/NULL);
		WSAUnityPlayer_SetUnityScreenOrientation_m2430850923(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.WSAUnityPlayer::SetUnityScreenOrientation()
extern "C"  void WSAUnityPlayer_SetUnityScreenOrientation_m2430850923 (WSAUnityPlayer_t1576425251 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WSAUnityPlayer_SetUnityScreenOrientation_m2430850923_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = WSAUnityPlayer_GetActualScreenOrientation_m436681916(__this, /*hidden argument*/NULL);
		__this->set_mScreenOrientation_0(L_0);
		int32_t L_1 = __this->get_mScreenOrientation_0();
		IL2CPP_RUNTIME_CLASS_INIT(SurfaceUtilities_t184061569_il2cpp_TypeInfo_var);
		SurfaceUtilities_SetSurfaceOrientation_m897924353(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		int32_t L_2 = __this->get_mScreenOrientation_0();
		WSAUnityPlayer_setSurfaceOrientationWSA_m586800739(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.ScreenOrientation Vuforia.WSAUnityPlayer::GetActualScreenOrientation()
extern "C"  int32_t WSAUnityPlayer_GetActualScreenOrientation_m436681916 (WSAUnityPlayer_t1576425251 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WSAUnityPlayer_GetActualScreenOrientation_m436681916_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		int32_t L_0 = Screen_get_orientation_m251094311(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		int32_t L_1 = V_0;
		if ((!(((uint32_t)L_1) == ((uint32_t)5))))
		{
			goto IL_0053;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t3439709668_il2cpp_TypeInfo_var);
		int32_t L_2 = Input_get_deviceOrientation_m549772300(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_1 = L_2;
		int32_t L_3 = V_1;
		switch (((int32_t)((int32_t)L_3-(int32_t)1)))
		{
			case 0:
			{
				goto IL_003e;
			}
			case 1:
			{
				goto IL_0045;
			}
			case 2:
			{
				goto IL_0030;
			}
			case 3:
			{
				goto IL_0037;
			}
		}
	}
	{
		goto IL_004c;
	}

IL_0030:
	{
		V_0 = 3;
		goto IL_0053;
	}

IL_0037:
	{
		V_0 = 4;
		goto IL_0053;
	}

IL_003e:
	{
		V_0 = 1;
		goto IL_0053;
	}

IL_0045:
	{
		V_0 = 2;
		goto IL_0053;
	}

IL_004c:
	{
		V_0 = 3;
		goto IL_0053;
	}

IL_0053:
	{
		int32_t L_4 = V_0;
		return L_4;
	}
}
// System.Void Vuforia.WSAUnityPlayer::setPlatFormNative()
extern "C"  void WSAUnityPlayer_setPlatFormNative_m3380749227 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) ();
	static PInvokeFunc il2cppPInvokeFunc;
	if (il2cppPInvokeFunc == NULL)
	{
		int parameterSize = 0;
		il2cppPInvokeFunc = il2cpp_codegen_resolve_pinvoke<PInvokeFunc>(IL2CPP_NATIVE_STRING("VuforiaWrapper"), "setPlatFormNative", IL2CPP_CALL_DEFAULT, CHARSET_UNICODE, parameterSize, false);

		if (il2cppPInvokeFunc == NULL)
		{
			IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'setPlatFormNative'"));
		}
	}

	// Native function invocation
	il2cppPInvokeFunc();

}
// System.Int32 Vuforia.WSAUnityPlayer::initVuforiaWSA(System.String)
extern "C"  int32_t WSAUnityPlayer_initVuforiaWSA_m1079303392 (RuntimeObject * __this /* static, unused */, String_t* ___licenseKey0, const RuntimeMethod* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (char*);
	static PInvokeFunc il2cppPInvokeFunc;
	if (il2cppPInvokeFunc == NULL)
	{
		int parameterSize = sizeof(char*);
		il2cppPInvokeFunc = il2cpp_codegen_resolve_pinvoke<PInvokeFunc>(IL2CPP_NATIVE_STRING("VuforiaWrapper"), "initVuforiaWSA", IL2CPP_CALL_DEFAULT, CHARSET_UNICODE, parameterSize, false);

		if (il2cppPInvokeFunc == NULL)
		{
			IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'initVuforiaWSA'"));
		}
	}

	// Marshaling of parameter '___licenseKey0' to native representation
	char* ____licenseKey0_marshaled = NULL;
	____licenseKey0_marshaled = il2cpp_codegen_marshal_string(___licenseKey0);

	// Native function invocation
	int32_t returnValue = il2cppPInvokeFunc(____licenseKey0_marshaled);

	// Marshaling cleanup of parameter '___licenseKey0' native representation
	il2cpp_codegen_marshal_free(____licenseKey0_marshaled);
	____licenseKey0_marshaled = NULL;

	return returnValue;
}
// System.Void Vuforia.WSAUnityPlayer::setSurfaceOrientationWSA(System.Int32)
extern "C"  void WSAUnityPlayer_setSurfaceOrientationWSA_m586800739 (RuntimeObject * __this /* static, unused */, int32_t ___screenOrientation0, const RuntimeMethod* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (int32_t);
	static PInvokeFunc il2cppPInvokeFunc;
	if (il2cppPInvokeFunc == NULL)
	{
		int parameterSize = sizeof(int32_t);
		il2cppPInvokeFunc = il2cpp_codegen_resolve_pinvoke<PInvokeFunc>(IL2CPP_NATIVE_STRING("VuforiaWrapper"), "setSurfaceOrientationWSA", IL2CPP_CALL_DEFAULT, CHARSET_UNICODE, parameterSize, false);

		if (il2cppPInvokeFunc == NULL)
		{
			IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'setSurfaceOrientationWSA'"));
		}
	}

	// Native function invocation
	il2cppPInvokeFunc(___screenOrientation0);

}
#ifdef __clang__
#pragma clang diagnostic pop
#endif

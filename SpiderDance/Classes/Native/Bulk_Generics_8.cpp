﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "object-internals.h"

// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t2298632947;
// System.Collections.Generic.LinkedList`1<System.Object>
struct LinkedList_1_t4198215526;
// System.ObjectDisposedException
struct ObjectDisposedException_t2553224159;
// System.InvalidOperationException
struct InvalidOperationException_t2321794232;
// System.Collections.Generic.LinkedList`1<Vuforia.VuforiaManager/TrackableIdPair>
struct LinkedList_1_t3507684622;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t1606602250;
// System.ArgumentException
struct ArgumentException_t4028401650;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t1486926975;
// System.Collections.IEnumerator
struct IEnumerator_t2306197993;
// System.Collections.Generic.LinkedListNode`1<System.Object>
struct LinkedListNode_1_t1694065801;
// System.ArgumentNullException
struct ArgumentNullException_t2943536221;
// System.Object[]
struct ObjectU5BU5D_t3270211303;
// System.ArgumentOutOfRangeException
struct ArgumentOutOfRangeException_t2174922486;
// System.Type
struct Type_t;
// System.Collections.Generic.IEnumerator`1<Vuforia.VuforiaManager/TrackableIdPair>
struct IEnumerator_1_t796396071;
// System.Collections.Generic.LinkedListNode`1<Vuforia.VuforiaManager/TrackableIdPair>
struct LinkedListNode_1_t1003534897;
// Vuforia.VuforiaManager/TrackableIdPair[]
struct TrackableIdPairU5BU5D_t3613789599;
// System.Collections.Generic.List`1<System.Int32>
struct List_1_t3093093796;
// System.Collections.Generic.List`1<System.Object>
struct List_1_t877107497;
// System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>
struct List_1_t3619054930;
// System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>
struct List_1_t506086561;
// System.Collections.Generic.List`1<UnityEngine.AnimatorClipInfo>
struct List_1_t2171014601;
// System.Collections.Generic.List`1<UnityEngine.Color32>
struct List_1_t3435548858;
// System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>
struct List_1_t2366338965;
// System.Collections.Generic.List`1<UnityEngine.UICharInfo>
struct List_1_t1336301424;
// System.Collections.Generic.List`1<UnityEngine.UILineInfo>
struct List_1_t4148627898;
// System.Collections.Generic.List`1<UnityEngine.UIVertex>
struct List_1_t1641616647;
// System.Collections.Generic.List`1<UnityEngine.Vector2>
struct List_1_t2432482469;
// System.Collections.Generic.List`1<UnityEngine.Vector3>
struct List_1_t1758679641;
// System.Collections.Generic.List`1<UnityEngine.Vector4>
struct List_1_t1669821552;
// System.Collections.Hashtable
struct Hashtable_t1690131612;
// System.Collections.ArrayList
struct ArrayList_t693123750;
// System.Runtime.Serialization.IFormatterConverter
struct IFormatterConverter_t2266836369;
// System.Int32[]
struct Int32U5BU5D_t2324750880;
// System.Reflection.CustomAttributeNamedArgument[]
struct CustomAttributeNamedArgumentU5BU5D_t2347765754;
// System.Reflection.CustomAttributeTypedArgument[]
struct CustomAttributeTypedArgumentU5BU5D_t3682997903;
// UnityEngine.AnimatorClipInfo[]
struct AnimatorClipInfoU5BU5D_t148995015;
// UnityEngine.Color32[]
struct Color32U5BU5D_t2266382834;
// UnityEngine.EventSystems.RaycastResult[]
struct RaycastResultU5BU5D_t2863863115;
// UnityEngine.UICharInfo[]
struct UICharInfoU5BU5D_t3065457572;
// UnityEngine.UILineInfo[]
struct UILineInfoU5BU5D_t38175986;
// UnityEngine.UIVertex[]
struct UIVertexU5BU5D_t227469777;
// UnityEngine.Vector2[]
struct Vector2U5BU5D_t3916253691;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t981680887;
// UnityEngine.Vector4[]
struct Vector4U5BU5D_t2124741284;
// System.IntPtr[]
struct IntPtrU5BU5D_t439965300;
// System.Collections.IDictionary
struct IDictionary_t3485892299;
// System.Char[]
struct CharU5BU5D_t45629911;
// System.Void
struct Void_t2217553113;
// UnityEngine.GameObject
struct GameObject_t1811656094;
// UnityEngine.EventSystems.BaseRaycaster
struct BaseRaycaster_t318802990;
// System.Reflection.MemberInfo
struct MemberInfo_t;
// System.Type[]
struct TypeU5BU5D_t3854702846;
// System.Reflection.MemberFilter
struct MemberFilter_t4208935459;

extern RuntimeClass* StringU5BU5D_t2298632947_il2cpp_TypeInfo_var;
extern RuntimeClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3984347688;
extern Il2CppCodeGenString* _stringLiteral1084017900;
extern Il2CppCodeGenString* _stringLiteral1238787106;
extern const uint32_t KeyValuePair_2_ToString_m3144124502_MetadataUsageId;
extern const uint32_t KeyValuePair_2_ToString_m3380882685_MetadataUsageId;
extern const uint32_t KeyValuePair_2_ToString_m3666435602_MetadataUsageId;
extern const uint32_t KeyValuePair_2_ToString_m1274882003_MetadataUsageId;
extern const uint32_t KeyValuePair_2_ToString_m3427190393_MetadataUsageId;
extern const uint32_t KeyValuePair_2_ToString_m729476552_MetadataUsageId;
extern const uint32_t KeyValuePair_2_ToString_m3779009088_MetadataUsageId;
extern const uint32_t KeyValuePair_2_ToString_m1879012905_MetadataUsageId;
extern const uint32_t KeyValuePair_2_ToString_m4250712101_MetadataUsageId;
extern const uint32_t KeyValuePair_2_ToString_m2759420978_MetadataUsageId;
extern const uint32_t KeyValuePair_2_ToString_m3066038920_MetadataUsageId;
extern const uint32_t KeyValuePair_2_ToString_m2795500392_MetadataUsageId;
extern const uint32_t KeyValuePair_2_ToString_m1074719178_MetadataUsageId;
extern RuntimeClass* ObjectDisposedException_t2553224159_il2cpp_TypeInfo_var;
extern RuntimeClass* InvalidOperationException_t2321794232_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4105241565;
extern const uint32_t Enumerator_System_Collections_IEnumerator_Reset_m2687040866_MetadataUsageId;
extern const uint32_t Enumerator_get_Current_m3886014715_MetadataUsageId;
extern const uint32_t Enumerator_MoveNext_m893837046_MetadataUsageId;
extern const uint32_t Enumerator_Dispose_m3834463819_MetadataUsageId;
extern const uint32_t Enumerator_System_Collections_IEnumerator_Reset_m986577418_MetadataUsageId;
extern const uint32_t Enumerator_get_Current_m1428289307_MetadataUsageId;
extern const uint32_t Enumerator_MoveNext_m562864630_MetadataUsageId;
extern const uint32_t Enumerator_Dispose_m3863799224_MetadataUsageId;
extern RuntimeClass* RuntimeObject_il2cpp_TypeInfo_var;
extern const uint32_t LinkedList_1__ctor_m2557729568_MetadataUsageId;
extern const uint32_t LinkedList_1__ctor_m631841948_MetadataUsageId;
extern RuntimeClass* ArgumentException_t4028401650_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral961150872;
extern const uint32_t LinkedList_1_System_Collections_ICollection_CopyTo_m3341707420_MetadataUsageId;
extern RuntimeClass* ArgumentNullException_t2943536221_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3726057294;
extern const uint32_t LinkedList_1_VerifyReferencedNode_m1279677459_MetadataUsageId;
extern RuntimeClass* ArgumentOutOfRangeException_t2174922486_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3160537771;
extern Il2CppCodeGenString* _stringLiteral2607546579;
extern Il2CppCodeGenString* _stringLiteral1546211127;
extern const uint32_t LinkedList_1_CopyTo_m3325928928_MetadataUsageId;
extern RuntimeClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral481801365;
extern Il2CppCodeGenString* _stringLiteral1332662376;
extern const uint32_t LinkedList_1_GetObjectData_m2710009630_MetadataUsageId;
extern const uint32_t LinkedList_1_OnDeserialization_m1809459994_MetadataUsageId;
extern const uint32_t LinkedList_1__ctor_m1855644674_MetadataUsageId;
extern const uint32_t LinkedList_1__ctor_m3871494995_MetadataUsageId;
extern const uint32_t LinkedList_1_System_Collections_ICollection_CopyTo_m2990393639_MetadataUsageId;
extern const uint32_t LinkedList_1_VerifyReferencedNode_m2144741273_MetadataUsageId;
extern const uint32_t LinkedList_1_CopyTo_m2231156345_MetadataUsageId;
extern const uint32_t LinkedList_1_GetObjectData_m1696023509_MetadataUsageId;
extern const uint32_t LinkedList_1_OnDeserialization_m2897007678_MetadataUsageId;
extern const uint32_t Enumerator_System_Collections_IEnumerator_get_Current_m642494324_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral556965375;
extern const uint32_t Enumerator_VerifyState_m2157842137_MetadataUsageId;
extern const uint32_t Enumerator_System_Collections_IEnumerator_get_Current_m2660490385_MetadataUsageId;
extern const uint32_t Enumerator_VerifyState_m2949725876_MetadataUsageId;
extern const uint32_t Enumerator_System_Collections_IEnumerator_get_Current_m2488257730_MetadataUsageId;
extern const uint32_t Enumerator_VerifyState_m1611858526_MetadataUsageId;
extern const uint32_t Enumerator_System_Collections_IEnumerator_get_Current_m3245126976_MetadataUsageId;
extern const uint32_t Enumerator_VerifyState_m145152094_MetadataUsageId;
extern const uint32_t Enumerator_System_Collections_IEnumerator_get_Current_m4227277968_MetadataUsageId;
extern const uint32_t Enumerator_VerifyState_m1238207820_MetadataUsageId;
extern const uint32_t Enumerator_System_Collections_IEnumerator_get_Current_m3560914353_MetadataUsageId;
extern const uint32_t Enumerator_VerifyState_m1773101339_MetadataUsageId;
extern const uint32_t Enumerator_System_Collections_IEnumerator_get_Current_m2533393695_MetadataUsageId;
extern const uint32_t Enumerator_VerifyState_m1061122233_MetadataUsageId;
extern const uint32_t Enumerator_System_Collections_IEnumerator_get_Current_m3312567892_MetadataUsageId;
extern const uint32_t Enumerator_VerifyState_m4097623086_MetadataUsageId;
extern const uint32_t Enumerator_System_Collections_IEnumerator_get_Current_m1832049754_MetadataUsageId;
extern const uint32_t Enumerator_VerifyState_m3089285984_MetadataUsageId;
extern const uint32_t Enumerator_System_Collections_IEnumerator_get_Current_m1198406995_MetadataUsageId;
extern const uint32_t Enumerator_VerifyState_m2357532199_MetadataUsageId;
extern const uint32_t Enumerator_System_Collections_IEnumerator_get_Current_m741422485_MetadataUsageId;
extern const uint32_t Enumerator_VerifyState_m3589629877_MetadataUsageId;
extern const uint32_t Enumerator_System_Collections_IEnumerator_get_Current_m1440485986_MetadataUsageId;
extern const uint32_t Enumerator_VerifyState_m2353471952_MetadataUsageId;
extern const uint32_t Enumerator_System_Collections_IEnumerator_get_Current_m2740647926_MetadataUsageId;
extern const uint32_t Enumerator_VerifyState_m4257143980_MetadataUsageId;

struct StringU5BU5D_t2298632947;
struct ObjectU5BU5D_t3270211303;
struct TrackableIdPairU5BU5D_t3613789599;
struct Int32U5BU5D_t2324750880;
struct CustomAttributeNamedArgumentU5BU5D_t2347765754;
struct CustomAttributeTypedArgumentU5BU5D_t3682997903;
struct AnimatorClipInfoU5BU5D_t148995015;
struct Color32U5BU5D_t2266382834;
struct RaycastResultU5BU5D_t2863863115;
struct UICharInfoU5BU5D_t3065457572;
struct UILineInfoU5BU5D_t38175986;
struct UIVertexU5BU5D_t227469777;
struct Vector2U5BU5D_t3916253691;
struct Vector3U5BU5D_t981680887;
struct Vector4U5BU5D_t2124741284;


#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
struct Il2CppArrayBounds;
#ifndef RUNTIMEARRAY_H
#define RUNTIMEARRAY_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Array

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEARRAY_H
#ifndef LINKEDLIST_1_T3507684622_H
#define LINKEDLIST_1_T3507684622_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.LinkedList`1<Vuforia.VuforiaManager/TrackableIdPair>
struct  LinkedList_1_t3507684622  : public RuntimeObject
{
public:
	// System.UInt32 System.Collections.Generic.LinkedList`1::count
	uint32_t ___count_0;
	// System.UInt32 System.Collections.Generic.LinkedList`1::version
	uint32_t ___version_1;
	// System.Object System.Collections.Generic.LinkedList`1::syncRoot
	RuntimeObject * ___syncRoot_2;
	// System.Collections.Generic.LinkedListNode`1<T> System.Collections.Generic.LinkedList`1::first
	LinkedListNode_1_t1003534897 * ___first_3;
	// System.Runtime.Serialization.SerializationInfo System.Collections.Generic.LinkedList`1::si
	SerializationInfo_t1606602250 * ___si_4;

public:
	inline static int32_t get_offset_of_count_0() { return static_cast<int32_t>(offsetof(LinkedList_1_t3507684622, ___count_0)); }
	inline uint32_t get_count_0() const { return ___count_0; }
	inline uint32_t* get_address_of_count_0() { return &___count_0; }
	inline void set_count_0(uint32_t value)
	{
		___count_0 = value;
	}

	inline static int32_t get_offset_of_version_1() { return static_cast<int32_t>(offsetof(LinkedList_1_t3507684622, ___version_1)); }
	inline uint32_t get_version_1() const { return ___version_1; }
	inline uint32_t* get_address_of_version_1() { return &___version_1; }
	inline void set_version_1(uint32_t value)
	{
		___version_1 = value;
	}

	inline static int32_t get_offset_of_syncRoot_2() { return static_cast<int32_t>(offsetof(LinkedList_1_t3507684622, ___syncRoot_2)); }
	inline RuntimeObject * get_syncRoot_2() const { return ___syncRoot_2; }
	inline RuntimeObject ** get_address_of_syncRoot_2() { return &___syncRoot_2; }
	inline void set_syncRoot_2(RuntimeObject * value)
	{
		___syncRoot_2 = value;
		Il2CppCodeGenWriteBarrier((&___syncRoot_2), value);
	}

	inline static int32_t get_offset_of_first_3() { return static_cast<int32_t>(offsetof(LinkedList_1_t3507684622, ___first_3)); }
	inline LinkedListNode_1_t1003534897 * get_first_3() const { return ___first_3; }
	inline LinkedListNode_1_t1003534897 ** get_address_of_first_3() { return &___first_3; }
	inline void set_first_3(LinkedListNode_1_t1003534897 * value)
	{
		___first_3 = value;
		Il2CppCodeGenWriteBarrier((&___first_3), value);
	}

	inline static int32_t get_offset_of_si_4() { return static_cast<int32_t>(offsetof(LinkedList_1_t3507684622, ___si_4)); }
	inline SerializationInfo_t1606602250 * get_si_4() const { return ___si_4; }
	inline SerializationInfo_t1606602250 ** get_address_of_si_4() { return &___si_4; }
	inline void set_si_4(SerializationInfo_t1606602250 * value)
	{
		___si_4 = value;
		Il2CppCodeGenWriteBarrier((&___si_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LINKEDLIST_1_T3507684622_H
#ifndef SERIALIZATIONINFO_T1606602250_H
#define SERIALIZATIONINFO_T1606602250_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.Serialization.SerializationInfo
struct  SerializationInfo_t1606602250  : public RuntimeObject
{
public:
	// System.Collections.Hashtable System.Runtime.Serialization.SerializationInfo::serialized
	Hashtable_t1690131612 * ___serialized_0;
	// System.Collections.ArrayList System.Runtime.Serialization.SerializationInfo::values
	ArrayList_t693123750 * ___values_1;
	// System.String System.Runtime.Serialization.SerializationInfo::assemblyName
	String_t* ___assemblyName_2;
	// System.String System.Runtime.Serialization.SerializationInfo::fullTypeName
	String_t* ___fullTypeName_3;
	// System.Runtime.Serialization.IFormatterConverter System.Runtime.Serialization.SerializationInfo::converter
	RuntimeObject* ___converter_4;

public:
	inline static int32_t get_offset_of_serialized_0() { return static_cast<int32_t>(offsetof(SerializationInfo_t1606602250, ___serialized_0)); }
	inline Hashtable_t1690131612 * get_serialized_0() const { return ___serialized_0; }
	inline Hashtable_t1690131612 ** get_address_of_serialized_0() { return &___serialized_0; }
	inline void set_serialized_0(Hashtable_t1690131612 * value)
	{
		___serialized_0 = value;
		Il2CppCodeGenWriteBarrier((&___serialized_0), value);
	}

	inline static int32_t get_offset_of_values_1() { return static_cast<int32_t>(offsetof(SerializationInfo_t1606602250, ___values_1)); }
	inline ArrayList_t693123750 * get_values_1() const { return ___values_1; }
	inline ArrayList_t693123750 ** get_address_of_values_1() { return &___values_1; }
	inline void set_values_1(ArrayList_t693123750 * value)
	{
		___values_1 = value;
		Il2CppCodeGenWriteBarrier((&___values_1), value);
	}

	inline static int32_t get_offset_of_assemblyName_2() { return static_cast<int32_t>(offsetof(SerializationInfo_t1606602250, ___assemblyName_2)); }
	inline String_t* get_assemblyName_2() const { return ___assemblyName_2; }
	inline String_t** get_address_of_assemblyName_2() { return &___assemblyName_2; }
	inline void set_assemblyName_2(String_t* value)
	{
		___assemblyName_2 = value;
		Il2CppCodeGenWriteBarrier((&___assemblyName_2), value);
	}

	inline static int32_t get_offset_of_fullTypeName_3() { return static_cast<int32_t>(offsetof(SerializationInfo_t1606602250, ___fullTypeName_3)); }
	inline String_t* get_fullTypeName_3() const { return ___fullTypeName_3; }
	inline String_t** get_address_of_fullTypeName_3() { return &___fullTypeName_3; }
	inline void set_fullTypeName_3(String_t* value)
	{
		___fullTypeName_3 = value;
		Il2CppCodeGenWriteBarrier((&___fullTypeName_3), value);
	}

	inline static int32_t get_offset_of_converter_4() { return static_cast<int32_t>(offsetof(SerializationInfo_t1606602250, ___converter_4)); }
	inline RuntimeObject* get_converter_4() const { return ___converter_4; }
	inline RuntimeObject** get_address_of_converter_4() { return &___converter_4; }
	inline void set_converter_4(RuntimeObject* value)
	{
		___converter_4 = value;
		Il2CppCodeGenWriteBarrier((&___converter_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERIALIZATIONINFO_T1606602250_H
#ifndef LIST_1_T3093093796_H
#define LIST_1_T3093093796_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1<System.Int32>
struct  List_1_t3093093796  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	Int32U5BU5D_t2324750880* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t3093093796, ____items_1)); }
	inline Int32U5BU5D_t2324750880* get__items_1() const { return ____items_1; }
	inline Int32U5BU5D_t2324750880** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(Int32U5BU5D_t2324750880* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((&____items_1), value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t3093093796, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t3093093796, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}
};

struct List_1_t3093093796_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::EmptyArray
	Int32U5BU5D_t2324750880* ___EmptyArray_4;

public:
	inline static int32_t get_offset_of_EmptyArray_4() { return static_cast<int32_t>(offsetof(List_1_t3093093796_StaticFields, ___EmptyArray_4)); }
	inline Int32U5BU5D_t2324750880* get_EmptyArray_4() const { return ___EmptyArray_4; }
	inline Int32U5BU5D_t2324750880** get_address_of_EmptyArray_4() { return &___EmptyArray_4; }
	inline void set_EmptyArray_4(Int32U5BU5D_t2324750880* value)
	{
		___EmptyArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIST_1_T3093093796_H
#ifndef LIST_1_T877107497_H
#define LIST_1_T877107497_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1<System.Object>
struct  List_1_t877107497  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	ObjectU5BU5D_t3270211303* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t877107497, ____items_1)); }
	inline ObjectU5BU5D_t3270211303* get__items_1() const { return ____items_1; }
	inline ObjectU5BU5D_t3270211303** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(ObjectU5BU5D_t3270211303* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((&____items_1), value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t877107497, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t877107497, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}
};

struct List_1_t877107497_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::EmptyArray
	ObjectU5BU5D_t3270211303* ___EmptyArray_4;

public:
	inline static int32_t get_offset_of_EmptyArray_4() { return static_cast<int32_t>(offsetof(List_1_t877107497_StaticFields, ___EmptyArray_4)); }
	inline ObjectU5BU5D_t3270211303* get_EmptyArray_4() const { return ___EmptyArray_4; }
	inline ObjectU5BU5D_t3270211303** get_address_of_EmptyArray_4() { return &___EmptyArray_4; }
	inline void set_EmptyArray_4(ObjectU5BU5D_t3270211303* value)
	{
		___EmptyArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIST_1_T877107497_H
#ifndef LIST_1_T3619054930_H
#define LIST_1_T3619054930_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>
struct  List_1_t3619054930  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	CustomAttributeNamedArgumentU5BU5D_t2347765754* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t3619054930, ____items_1)); }
	inline CustomAttributeNamedArgumentU5BU5D_t2347765754* get__items_1() const { return ____items_1; }
	inline CustomAttributeNamedArgumentU5BU5D_t2347765754** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(CustomAttributeNamedArgumentU5BU5D_t2347765754* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((&____items_1), value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t3619054930, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t3619054930, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}
};

struct List_1_t3619054930_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::EmptyArray
	CustomAttributeNamedArgumentU5BU5D_t2347765754* ___EmptyArray_4;

public:
	inline static int32_t get_offset_of_EmptyArray_4() { return static_cast<int32_t>(offsetof(List_1_t3619054930_StaticFields, ___EmptyArray_4)); }
	inline CustomAttributeNamedArgumentU5BU5D_t2347765754* get_EmptyArray_4() const { return ___EmptyArray_4; }
	inline CustomAttributeNamedArgumentU5BU5D_t2347765754** get_address_of_EmptyArray_4() { return &___EmptyArray_4; }
	inline void set_EmptyArray_4(CustomAttributeNamedArgumentU5BU5D_t2347765754* value)
	{
		___EmptyArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIST_1_T3619054930_H
#ifndef LIST_1_T506086561_H
#define LIST_1_T506086561_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>
struct  List_1_t506086561  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	CustomAttributeTypedArgumentU5BU5D_t3682997903* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t506086561, ____items_1)); }
	inline CustomAttributeTypedArgumentU5BU5D_t3682997903* get__items_1() const { return ____items_1; }
	inline CustomAttributeTypedArgumentU5BU5D_t3682997903** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(CustomAttributeTypedArgumentU5BU5D_t3682997903* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((&____items_1), value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t506086561, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t506086561, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}
};

struct List_1_t506086561_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::EmptyArray
	CustomAttributeTypedArgumentU5BU5D_t3682997903* ___EmptyArray_4;

public:
	inline static int32_t get_offset_of_EmptyArray_4() { return static_cast<int32_t>(offsetof(List_1_t506086561_StaticFields, ___EmptyArray_4)); }
	inline CustomAttributeTypedArgumentU5BU5D_t3682997903* get_EmptyArray_4() const { return ___EmptyArray_4; }
	inline CustomAttributeTypedArgumentU5BU5D_t3682997903** get_address_of_EmptyArray_4() { return &___EmptyArray_4; }
	inline void set_EmptyArray_4(CustomAttributeTypedArgumentU5BU5D_t3682997903* value)
	{
		___EmptyArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIST_1_T506086561_H
#ifndef LIST_1_T2171014601_H
#define LIST_1_T2171014601_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1<UnityEngine.AnimatorClipInfo>
struct  List_1_t2171014601  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	AnimatorClipInfoU5BU5D_t148995015* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t2171014601, ____items_1)); }
	inline AnimatorClipInfoU5BU5D_t148995015* get__items_1() const { return ____items_1; }
	inline AnimatorClipInfoU5BU5D_t148995015** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(AnimatorClipInfoU5BU5D_t148995015* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((&____items_1), value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t2171014601, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t2171014601, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}
};

struct List_1_t2171014601_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::EmptyArray
	AnimatorClipInfoU5BU5D_t148995015* ___EmptyArray_4;

public:
	inline static int32_t get_offset_of_EmptyArray_4() { return static_cast<int32_t>(offsetof(List_1_t2171014601_StaticFields, ___EmptyArray_4)); }
	inline AnimatorClipInfoU5BU5D_t148995015* get_EmptyArray_4() const { return ___EmptyArray_4; }
	inline AnimatorClipInfoU5BU5D_t148995015** get_address_of_EmptyArray_4() { return &___EmptyArray_4; }
	inline void set_EmptyArray_4(AnimatorClipInfoU5BU5D_t148995015* value)
	{
		___EmptyArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIST_1_T2171014601_H
#ifndef LIST_1_T3435548858_H
#define LIST_1_T3435548858_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1<UnityEngine.Color32>
struct  List_1_t3435548858  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	Color32U5BU5D_t2266382834* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t3435548858, ____items_1)); }
	inline Color32U5BU5D_t2266382834* get__items_1() const { return ____items_1; }
	inline Color32U5BU5D_t2266382834** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(Color32U5BU5D_t2266382834* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((&____items_1), value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t3435548858, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t3435548858, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}
};

struct List_1_t3435548858_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::EmptyArray
	Color32U5BU5D_t2266382834* ___EmptyArray_4;

public:
	inline static int32_t get_offset_of_EmptyArray_4() { return static_cast<int32_t>(offsetof(List_1_t3435548858_StaticFields, ___EmptyArray_4)); }
	inline Color32U5BU5D_t2266382834* get_EmptyArray_4() const { return ___EmptyArray_4; }
	inline Color32U5BU5D_t2266382834** get_address_of_EmptyArray_4() { return &___EmptyArray_4; }
	inline void set_EmptyArray_4(Color32U5BU5D_t2266382834* value)
	{
		___EmptyArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIST_1_T3435548858_H
#ifndef LIST_1_T2366338965_H
#define LIST_1_T2366338965_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>
struct  List_1_t2366338965  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	RaycastResultU5BU5D_t2863863115* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t2366338965, ____items_1)); }
	inline RaycastResultU5BU5D_t2863863115* get__items_1() const { return ____items_1; }
	inline RaycastResultU5BU5D_t2863863115** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(RaycastResultU5BU5D_t2863863115* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((&____items_1), value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t2366338965, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t2366338965, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}
};

struct List_1_t2366338965_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::EmptyArray
	RaycastResultU5BU5D_t2863863115* ___EmptyArray_4;

public:
	inline static int32_t get_offset_of_EmptyArray_4() { return static_cast<int32_t>(offsetof(List_1_t2366338965_StaticFields, ___EmptyArray_4)); }
	inline RaycastResultU5BU5D_t2863863115* get_EmptyArray_4() const { return ___EmptyArray_4; }
	inline RaycastResultU5BU5D_t2863863115** get_address_of_EmptyArray_4() { return &___EmptyArray_4; }
	inline void set_EmptyArray_4(RaycastResultU5BU5D_t2863863115* value)
	{
		___EmptyArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIST_1_T2366338965_H
#ifndef LIST_1_T1336301424_H
#define LIST_1_T1336301424_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1<UnityEngine.UICharInfo>
struct  List_1_t1336301424  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	UICharInfoU5BU5D_t3065457572* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t1336301424, ____items_1)); }
	inline UICharInfoU5BU5D_t3065457572* get__items_1() const { return ____items_1; }
	inline UICharInfoU5BU5D_t3065457572** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(UICharInfoU5BU5D_t3065457572* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((&____items_1), value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t1336301424, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t1336301424, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}
};

struct List_1_t1336301424_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::EmptyArray
	UICharInfoU5BU5D_t3065457572* ___EmptyArray_4;

public:
	inline static int32_t get_offset_of_EmptyArray_4() { return static_cast<int32_t>(offsetof(List_1_t1336301424_StaticFields, ___EmptyArray_4)); }
	inline UICharInfoU5BU5D_t3065457572* get_EmptyArray_4() const { return ___EmptyArray_4; }
	inline UICharInfoU5BU5D_t3065457572** get_address_of_EmptyArray_4() { return &___EmptyArray_4; }
	inline void set_EmptyArray_4(UICharInfoU5BU5D_t3065457572* value)
	{
		___EmptyArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIST_1_T1336301424_H
#ifndef LIST_1_T4148627898_H
#define LIST_1_T4148627898_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1<UnityEngine.UILineInfo>
struct  List_1_t4148627898  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	UILineInfoU5BU5D_t38175986* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t4148627898, ____items_1)); }
	inline UILineInfoU5BU5D_t38175986* get__items_1() const { return ____items_1; }
	inline UILineInfoU5BU5D_t38175986** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(UILineInfoU5BU5D_t38175986* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((&____items_1), value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t4148627898, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t4148627898, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}
};

struct List_1_t4148627898_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::EmptyArray
	UILineInfoU5BU5D_t38175986* ___EmptyArray_4;

public:
	inline static int32_t get_offset_of_EmptyArray_4() { return static_cast<int32_t>(offsetof(List_1_t4148627898_StaticFields, ___EmptyArray_4)); }
	inline UILineInfoU5BU5D_t38175986* get_EmptyArray_4() const { return ___EmptyArray_4; }
	inline UILineInfoU5BU5D_t38175986** get_address_of_EmptyArray_4() { return &___EmptyArray_4; }
	inline void set_EmptyArray_4(UILineInfoU5BU5D_t38175986* value)
	{
		___EmptyArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIST_1_T4148627898_H
#ifndef LIST_1_T1641616647_H
#define LIST_1_T1641616647_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1<UnityEngine.UIVertex>
struct  List_1_t1641616647  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	UIVertexU5BU5D_t227469777* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t1641616647, ____items_1)); }
	inline UIVertexU5BU5D_t227469777* get__items_1() const { return ____items_1; }
	inline UIVertexU5BU5D_t227469777** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(UIVertexU5BU5D_t227469777* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((&____items_1), value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t1641616647, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t1641616647, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}
};

struct List_1_t1641616647_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::EmptyArray
	UIVertexU5BU5D_t227469777* ___EmptyArray_4;

public:
	inline static int32_t get_offset_of_EmptyArray_4() { return static_cast<int32_t>(offsetof(List_1_t1641616647_StaticFields, ___EmptyArray_4)); }
	inline UIVertexU5BU5D_t227469777* get_EmptyArray_4() const { return ___EmptyArray_4; }
	inline UIVertexU5BU5D_t227469777** get_address_of_EmptyArray_4() { return &___EmptyArray_4; }
	inline void set_EmptyArray_4(UIVertexU5BU5D_t227469777* value)
	{
		___EmptyArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIST_1_T1641616647_H
#ifndef LIST_1_T2432482469_H
#define LIST_1_T2432482469_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1<UnityEngine.Vector2>
struct  List_1_t2432482469  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	Vector2U5BU5D_t3916253691* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t2432482469, ____items_1)); }
	inline Vector2U5BU5D_t3916253691* get__items_1() const { return ____items_1; }
	inline Vector2U5BU5D_t3916253691** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(Vector2U5BU5D_t3916253691* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((&____items_1), value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t2432482469, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t2432482469, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}
};

struct List_1_t2432482469_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::EmptyArray
	Vector2U5BU5D_t3916253691* ___EmptyArray_4;

public:
	inline static int32_t get_offset_of_EmptyArray_4() { return static_cast<int32_t>(offsetof(List_1_t2432482469_StaticFields, ___EmptyArray_4)); }
	inline Vector2U5BU5D_t3916253691* get_EmptyArray_4() const { return ___EmptyArray_4; }
	inline Vector2U5BU5D_t3916253691** get_address_of_EmptyArray_4() { return &___EmptyArray_4; }
	inline void set_EmptyArray_4(Vector2U5BU5D_t3916253691* value)
	{
		___EmptyArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIST_1_T2432482469_H
#ifndef LIST_1_T1758679641_H
#define LIST_1_T1758679641_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1<UnityEngine.Vector3>
struct  List_1_t1758679641  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	Vector3U5BU5D_t981680887* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t1758679641, ____items_1)); }
	inline Vector3U5BU5D_t981680887* get__items_1() const { return ____items_1; }
	inline Vector3U5BU5D_t981680887** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(Vector3U5BU5D_t981680887* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((&____items_1), value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t1758679641, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t1758679641, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}
};

struct List_1_t1758679641_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::EmptyArray
	Vector3U5BU5D_t981680887* ___EmptyArray_4;

public:
	inline static int32_t get_offset_of_EmptyArray_4() { return static_cast<int32_t>(offsetof(List_1_t1758679641_StaticFields, ___EmptyArray_4)); }
	inline Vector3U5BU5D_t981680887* get_EmptyArray_4() const { return ___EmptyArray_4; }
	inline Vector3U5BU5D_t981680887** get_address_of_EmptyArray_4() { return &___EmptyArray_4; }
	inline void set_EmptyArray_4(Vector3U5BU5D_t981680887* value)
	{
		___EmptyArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIST_1_T1758679641_H
#ifndef LIST_1_T1669821552_H
#define LIST_1_T1669821552_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1<UnityEngine.Vector4>
struct  List_1_t1669821552  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	Vector4U5BU5D_t2124741284* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t1669821552, ____items_1)); }
	inline Vector4U5BU5D_t2124741284* get__items_1() const { return ____items_1; }
	inline Vector4U5BU5D_t2124741284** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(Vector4U5BU5D_t2124741284* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((&____items_1), value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t1669821552, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t1669821552, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}
};

struct List_1_t1669821552_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::EmptyArray
	Vector4U5BU5D_t2124741284* ___EmptyArray_4;

public:
	inline static int32_t get_offset_of_EmptyArray_4() { return static_cast<int32_t>(offsetof(List_1_t1669821552_StaticFields, ___EmptyArray_4)); }
	inline Vector4U5BU5D_t2124741284* get_EmptyArray_4() const { return ___EmptyArray_4; }
	inline Vector4U5BU5D_t2124741284** get_address_of_EmptyArray_4() { return &___EmptyArray_4; }
	inline void set_EmptyArray_4(Vector4U5BU5D_t2124741284* value)
	{
		___EmptyArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIST_1_T1669821552_H
#ifndef VALUETYPE_T1306072465_H
#define VALUETYPE_T1306072465_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t1306072465  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t1306072465_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t1306072465_marshaled_com
{
};
#endif // VALUETYPE_T1306072465_H
#ifndef MEMBERINFO_T_H
#define MEMBERINFO_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.MemberInfo
struct  MemberInfo_t  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MEMBERINFO_T_H
#ifndef LINKEDLISTNODE_1_T1694065801_H
#define LINKEDLISTNODE_1_T1694065801_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.LinkedListNode`1<System.Object>
struct  LinkedListNode_1_t1694065801  : public RuntimeObject
{
public:
	// T System.Collections.Generic.LinkedListNode`1::item
	RuntimeObject * ___item_0;
	// System.Collections.Generic.LinkedList`1<T> System.Collections.Generic.LinkedListNode`1::container
	LinkedList_1_t4198215526 * ___container_1;
	// System.Collections.Generic.LinkedListNode`1<T> System.Collections.Generic.LinkedListNode`1::forward
	LinkedListNode_1_t1694065801 * ___forward_2;
	// System.Collections.Generic.LinkedListNode`1<T> System.Collections.Generic.LinkedListNode`1::back
	LinkedListNode_1_t1694065801 * ___back_3;

public:
	inline static int32_t get_offset_of_item_0() { return static_cast<int32_t>(offsetof(LinkedListNode_1_t1694065801, ___item_0)); }
	inline RuntimeObject * get_item_0() const { return ___item_0; }
	inline RuntimeObject ** get_address_of_item_0() { return &___item_0; }
	inline void set_item_0(RuntimeObject * value)
	{
		___item_0 = value;
		Il2CppCodeGenWriteBarrier((&___item_0), value);
	}

	inline static int32_t get_offset_of_container_1() { return static_cast<int32_t>(offsetof(LinkedListNode_1_t1694065801, ___container_1)); }
	inline LinkedList_1_t4198215526 * get_container_1() const { return ___container_1; }
	inline LinkedList_1_t4198215526 ** get_address_of_container_1() { return &___container_1; }
	inline void set_container_1(LinkedList_1_t4198215526 * value)
	{
		___container_1 = value;
		Il2CppCodeGenWriteBarrier((&___container_1), value);
	}

	inline static int32_t get_offset_of_forward_2() { return static_cast<int32_t>(offsetof(LinkedListNode_1_t1694065801, ___forward_2)); }
	inline LinkedListNode_1_t1694065801 * get_forward_2() const { return ___forward_2; }
	inline LinkedListNode_1_t1694065801 ** get_address_of_forward_2() { return &___forward_2; }
	inline void set_forward_2(LinkedListNode_1_t1694065801 * value)
	{
		___forward_2 = value;
		Il2CppCodeGenWriteBarrier((&___forward_2), value);
	}

	inline static int32_t get_offset_of_back_3() { return static_cast<int32_t>(offsetof(LinkedListNode_1_t1694065801, ___back_3)); }
	inline LinkedListNode_1_t1694065801 * get_back_3() const { return ___back_3; }
	inline LinkedListNode_1_t1694065801 ** get_address_of_back_3() { return &___back_3; }
	inline void set_back_3(LinkedListNode_1_t1694065801 * value)
	{
		___back_3 = value;
		Il2CppCodeGenWriteBarrier((&___back_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LINKEDLISTNODE_1_T1694065801_H
#ifndef LINKEDLIST_1_T4198215526_H
#define LINKEDLIST_1_T4198215526_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.LinkedList`1<System.Object>
struct  LinkedList_1_t4198215526  : public RuntimeObject
{
public:
	// System.UInt32 System.Collections.Generic.LinkedList`1::count
	uint32_t ___count_0;
	// System.UInt32 System.Collections.Generic.LinkedList`1::version
	uint32_t ___version_1;
	// System.Object System.Collections.Generic.LinkedList`1::syncRoot
	RuntimeObject * ___syncRoot_2;
	// System.Collections.Generic.LinkedListNode`1<T> System.Collections.Generic.LinkedList`1::first
	LinkedListNode_1_t1694065801 * ___first_3;
	// System.Runtime.Serialization.SerializationInfo System.Collections.Generic.LinkedList`1::si
	SerializationInfo_t1606602250 * ___si_4;

public:
	inline static int32_t get_offset_of_count_0() { return static_cast<int32_t>(offsetof(LinkedList_1_t4198215526, ___count_0)); }
	inline uint32_t get_count_0() const { return ___count_0; }
	inline uint32_t* get_address_of_count_0() { return &___count_0; }
	inline void set_count_0(uint32_t value)
	{
		___count_0 = value;
	}

	inline static int32_t get_offset_of_version_1() { return static_cast<int32_t>(offsetof(LinkedList_1_t4198215526, ___version_1)); }
	inline uint32_t get_version_1() const { return ___version_1; }
	inline uint32_t* get_address_of_version_1() { return &___version_1; }
	inline void set_version_1(uint32_t value)
	{
		___version_1 = value;
	}

	inline static int32_t get_offset_of_syncRoot_2() { return static_cast<int32_t>(offsetof(LinkedList_1_t4198215526, ___syncRoot_2)); }
	inline RuntimeObject * get_syncRoot_2() const { return ___syncRoot_2; }
	inline RuntimeObject ** get_address_of_syncRoot_2() { return &___syncRoot_2; }
	inline void set_syncRoot_2(RuntimeObject * value)
	{
		___syncRoot_2 = value;
		Il2CppCodeGenWriteBarrier((&___syncRoot_2), value);
	}

	inline static int32_t get_offset_of_first_3() { return static_cast<int32_t>(offsetof(LinkedList_1_t4198215526, ___first_3)); }
	inline LinkedListNode_1_t1694065801 * get_first_3() const { return ___first_3; }
	inline LinkedListNode_1_t1694065801 ** get_address_of_first_3() { return &___first_3; }
	inline void set_first_3(LinkedListNode_1_t1694065801 * value)
	{
		___first_3 = value;
		Il2CppCodeGenWriteBarrier((&___first_3), value);
	}

	inline static int32_t get_offset_of_si_4() { return static_cast<int32_t>(offsetof(LinkedList_1_t4198215526, ___si_4)); }
	inline SerializationInfo_t1606602250 * get_si_4() const { return ___si_4; }
	inline SerializationInfo_t1606602250 ** get_address_of_si_4() { return &___si_4; }
	inline void set_si_4(SerializationInfo_t1606602250 * value)
	{
		___si_4 = value;
		Il2CppCodeGenWriteBarrier((&___si_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LINKEDLIST_1_T4198215526_H
#ifndef EXCEPTION_T1975590229_H
#define EXCEPTION_T1975590229_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Exception
struct  Exception_t1975590229  : public RuntimeObject
{
public:
	// System.IntPtr[] System.Exception::trace_ips
	IntPtrU5BU5D_t439965300* ___trace_ips_0;
	// System.Exception System.Exception::inner_exception
	Exception_t1975590229 * ___inner_exception_1;
	// System.String System.Exception::message
	String_t* ___message_2;
	// System.String System.Exception::help_link
	String_t* ___help_link_3;
	// System.String System.Exception::class_name
	String_t* ___class_name_4;
	// System.String System.Exception::stack_trace
	String_t* ___stack_trace_5;
	// System.String System.Exception::_remoteStackTraceString
	String_t* ____remoteStackTraceString_6;
	// System.Int32 System.Exception::remote_stack_index
	int32_t ___remote_stack_index_7;
	// System.Int32 System.Exception::hresult
	int32_t ___hresult_8;
	// System.String System.Exception::source
	String_t* ___source_9;
	// System.Collections.IDictionary System.Exception::_data
	RuntimeObject* ____data_10;

public:
	inline static int32_t get_offset_of_trace_ips_0() { return static_cast<int32_t>(offsetof(Exception_t1975590229, ___trace_ips_0)); }
	inline IntPtrU5BU5D_t439965300* get_trace_ips_0() const { return ___trace_ips_0; }
	inline IntPtrU5BU5D_t439965300** get_address_of_trace_ips_0() { return &___trace_ips_0; }
	inline void set_trace_ips_0(IntPtrU5BU5D_t439965300* value)
	{
		___trace_ips_0 = value;
		Il2CppCodeGenWriteBarrier((&___trace_ips_0), value);
	}

	inline static int32_t get_offset_of_inner_exception_1() { return static_cast<int32_t>(offsetof(Exception_t1975590229, ___inner_exception_1)); }
	inline Exception_t1975590229 * get_inner_exception_1() const { return ___inner_exception_1; }
	inline Exception_t1975590229 ** get_address_of_inner_exception_1() { return &___inner_exception_1; }
	inline void set_inner_exception_1(Exception_t1975590229 * value)
	{
		___inner_exception_1 = value;
		Il2CppCodeGenWriteBarrier((&___inner_exception_1), value);
	}

	inline static int32_t get_offset_of_message_2() { return static_cast<int32_t>(offsetof(Exception_t1975590229, ___message_2)); }
	inline String_t* get_message_2() const { return ___message_2; }
	inline String_t** get_address_of_message_2() { return &___message_2; }
	inline void set_message_2(String_t* value)
	{
		___message_2 = value;
		Il2CppCodeGenWriteBarrier((&___message_2), value);
	}

	inline static int32_t get_offset_of_help_link_3() { return static_cast<int32_t>(offsetof(Exception_t1975590229, ___help_link_3)); }
	inline String_t* get_help_link_3() const { return ___help_link_3; }
	inline String_t** get_address_of_help_link_3() { return &___help_link_3; }
	inline void set_help_link_3(String_t* value)
	{
		___help_link_3 = value;
		Il2CppCodeGenWriteBarrier((&___help_link_3), value);
	}

	inline static int32_t get_offset_of_class_name_4() { return static_cast<int32_t>(offsetof(Exception_t1975590229, ___class_name_4)); }
	inline String_t* get_class_name_4() const { return ___class_name_4; }
	inline String_t** get_address_of_class_name_4() { return &___class_name_4; }
	inline void set_class_name_4(String_t* value)
	{
		___class_name_4 = value;
		Il2CppCodeGenWriteBarrier((&___class_name_4), value);
	}

	inline static int32_t get_offset_of_stack_trace_5() { return static_cast<int32_t>(offsetof(Exception_t1975590229, ___stack_trace_5)); }
	inline String_t* get_stack_trace_5() const { return ___stack_trace_5; }
	inline String_t** get_address_of_stack_trace_5() { return &___stack_trace_5; }
	inline void set_stack_trace_5(String_t* value)
	{
		___stack_trace_5 = value;
		Il2CppCodeGenWriteBarrier((&___stack_trace_5), value);
	}

	inline static int32_t get_offset_of__remoteStackTraceString_6() { return static_cast<int32_t>(offsetof(Exception_t1975590229, ____remoteStackTraceString_6)); }
	inline String_t* get__remoteStackTraceString_6() const { return ____remoteStackTraceString_6; }
	inline String_t** get_address_of__remoteStackTraceString_6() { return &____remoteStackTraceString_6; }
	inline void set__remoteStackTraceString_6(String_t* value)
	{
		____remoteStackTraceString_6 = value;
		Il2CppCodeGenWriteBarrier((&____remoteStackTraceString_6), value);
	}

	inline static int32_t get_offset_of_remote_stack_index_7() { return static_cast<int32_t>(offsetof(Exception_t1975590229, ___remote_stack_index_7)); }
	inline int32_t get_remote_stack_index_7() const { return ___remote_stack_index_7; }
	inline int32_t* get_address_of_remote_stack_index_7() { return &___remote_stack_index_7; }
	inline void set_remote_stack_index_7(int32_t value)
	{
		___remote_stack_index_7 = value;
	}

	inline static int32_t get_offset_of_hresult_8() { return static_cast<int32_t>(offsetof(Exception_t1975590229, ___hresult_8)); }
	inline int32_t get_hresult_8() const { return ___hresult_8; }
	inline int32_t* get_address_of_hresult_8() { return &___hresult_8; }
	inline void set_hresult_8(int32_t value)
	{
		___hresult_8 = value;
	}

	inline static int32_t get_offset_of_source_9() { return static_cast<int32_t>(offsetof(Exception_t1975590229, ___source_9)); }
	inline String_t* get_source_9() const { return ___source_9; }
	inline String_t** get_address_of_source_9() { return &___source_9; }
	inline void set_source_9(String_t* value)
	{
		___source_9 = value;
		Il2CppCodeGenWriteBarrier((&___source_9), value);
	}

	inline static int32_t get_offset_of__data_10() { return static_cast<int32_t>(offsetof(Exception_t1975590229, ____data_10)); }
	inline RuntimeObject* get__data_10() const { return ____data_10; }
	inline RuntimeObject** get_address_of__data_10() { return &____data_10; }
	inline void set__data_10(RuntimeObject* value)
	{
		____data_10 = value;
		Il2CppCodeGenWriteBarrier((&____data_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXCEPTION_T1975590229_H
#ifndef STRING_T_H
#define STRING_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.String
struct  String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::length
	int32_t ___length_0;
	// System.Char System.String::start_char
	Il2CppChar ___start_char_1;

public:
	inline static int32_t get_offset_of_length_0() { return static_cast<int32_t>(offsetof(String_t, ___length_0)); }
	inline int32_t get_length_0() const { return ___length_0; }
	inline int32_t* get_address_of_length_0() { return &___length_0; }
	inline void set_length_0(int32_t value)
	{
		___length_0 = value;
	}

	inline static int32_t get_offset_of_start_char_1() { return static_cast<int32_t>(offsetof(String_t, ___start_char_1)); }
	inline Il2CppChar get_start_char_1() const { return ___start_char_1; }
	inline Il2CppChar* get_address_of_start_char_1() { return &___start_char_1; }
	inline void set_start_char_1(Il2CppChar value)
	{
		___start_char_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_2;
	// System.Char[] System.String::WhiteChars
	CharU5BU5D_t45629911* ___WhiteChars_3;

public:
	inline static int32_t get_offset_of_Empty_2() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_2)); }
	inline String_t* get_Empty_2() const { return ___Empty_2; }
	inline String_t** get_address_of_Empty_2() { return &___Empty_2; }
	inline void set_Empty_2(String_t* value)
	{
		___Empty_2 = value;
		Il2CppCodeGenWriteBarrier((&___Empty_2), value);
	}

	inline static int32_t get_offset_of_WhiteChars_3() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___WhiteChars_3)); }
	inline CharU5BU5D_t45629911* get_WhiteChars_3() const { return ___WhiteChars_3; }
	inline CharU5BU5D_t45629911** get_address_of_WhiteChars_3() { return &___WhiteChars_3; }
	inline void set_WhiteChars_3(CharU5BU5D_t45629911* value)
	{
		___WhiteChars_3 = value;
		Il2CppCodeGenWriteBarrier((&___WhiteChars_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STRING_T_H
#ifndef KEYVALUEPAIR_2_T115616549_H
#define KEYVALUEPAIR_2_T115616549_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>
struct  KeyValuePair_2_t115616549 
{
public:
	// TKey System.Collections.Generic.KeyValuePair`2::key
	RuntimeObject * ___key_0;
	// TValue System.Collections.Generic.KeyValuePair`2::value
	bool ___value_1;

public:
	inline static int32_t get_offset_of_key_0() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t115616549, ___key_0)); }
	inline RuntimeObject * get_key_0() const { return ___key_0; }
	inline RuntimeObject ** get_address_of_key_0() { return &___key_0; }
	inline void set_key_0(RuntimeObject * value)
	{
		___key_0 = value;
		Il2CppCodeGenWriteBarrier((&___key_0), value);
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t115616549, ___value_1)); }
	inline bool get_value_1() const { return ___value_1; }
	inline bool* get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(bool value)
	{
		___value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYVALUEPAIR_2_T115616549_H
#ifndef KEYVALUEPAIR_2_T3481697066_H
#define KEYVALUEPAIR_2_T3481697066_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>
struct  KeyValuePair_2_t3481697066 
{
public:
	// TKey System.Collections.Generic.KeyValuePair`2::key
	RuntimeObject * ___key_0;
	// TValue System.Collections.Generic.KeyValuePair`2::value
	RuntimeObject * ___value_1;

public:
	inline static int32_t get_offset_of_key_0() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t3481697066, ___key_0)); }
	inline RuntimeObject * get_key_0() const { return ___key_0; }
	inline RuntimeObject ** get_address_of_key_0() { return &___key_0; }
	inline void set_key_0(RuntimeObject * value)
	{
		___key_0 = value;
		Il2CppCodeGenWriteBarrier((&___key_0), value);
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t3481697066, ___value_1)); }
	inline RuntimeObject * get_value_1() const { return ___value_1; }
	inline RuntimeObject ** get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(RuntimeObject * value)
	{
		___value_1 = value;
		Il2CppCodeGenWriteBarrier((&___value_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYVALUEPAIR_2_T3481697066_H
#ifndef KEYVALUEPAIR_2_T1402716069_H
#define KEYVALUEPAIR_2_T1402716069_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>
struct  KeyValuePair_2_t1402716069 
{
public:
	// TKey System.Collections.Generic.KeyValuePair`2::key
	RuntimeObject * ___key_0;
	// TValue System.Collections.Generic.KeyValuePair`2::value
	int32_t ___value_1;

public:
	inline static int32_t get_offset_of_key_0() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t1402716069, ___key_0)); }
	inline RuntimeObject * get_key_0() const { return ___key_0; }
	inline RuntimeObject ** get_address_of_key_0() { return &___key_0; }
	inline void set_key_0(RuntimeObject * value)
	{
		___key_0 = value;
		Il2CppCodeGenWriteBarrier((&___key_0), value);
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t1402716069, ___value_1)); }
	inline int32_t get_value_1() const { return ___value_1; }
	inline int32_t* get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(int32_t value)
	{
		___value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYVALUEPAIR_2_T1402716069_H
#ifndef BOOLEAN_T2950845069_H
#define BOOLEAN_T2950845069_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Boolean
struct  Boolean_t2950845069 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Boolean_t2950845069, ___m_value_2)); }
	inline bool get_m_value_2() const { return ___m_value_2; }
	inline bool* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(bool value)
	{
		___m_value_2 = value;
	}
};

struct Boolean_t2950845069_StaticFields
{
public:
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_0;
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_1;

public:
	inline static int32_t get_offset_of_FalseString_0() { return static_cast<int32_t>(offsetof(Boolean_t2950845069_StaticFields, ___FalseString_0)); }
	inline String_t* get_FalseString_0() const { return ___FalseString_0; }
	inline String_t** get_address_of_FalseString_0() { return &___FalseString_0; }
	inline void set_FalseString_0(String_t* value)
	{
		___FalseString_0 = value;
		Il2CppCodeGenWriteBarrier((&___FalseString_0), value);
	}

	inline static int32_t get_offset_of_TrueString_1() { return static_cast<int32_t>(offsetof(Boolean_t2950845069_StaticFields, ___TrueString_1)); }
	inline String_t* get_TrueString_1() const { return ___TrueString_1; }
	inline String_t** get_address_of_TrueString_1() { return &___TrueString_1; }
	inline void set_TrueString_1(String_t* value)
	{
		___TrueString_1 = value;
		Il2CppCodeGenWriteBarrier((&___TrueString_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOOLEAN_T2950845069_H
#ifndef CUSTOMATTRIBUTETYPEDARGUMENT_T1650937354_H
#define CUSTOMATTRIBUTETYPEDARGUMENT_T1650937354_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.CustomAttributeTypedArgument
struct  CustomAttributeTypedArgument_t1650937354 
{
public:
	// System.Type System.Reflection.CustomAttributeTypedArgument::argumentType
	Type_t * ___argumentType_0;
	// System.Object System.Reflection.CustomAttributeTypedArgument::value
	RuntimeObject * ___value_1;

public:
	inline static int32_t get_offset_of_argumentType_0() { return static_cast<int32_t>(offsetof(CustomAttributeTypedArgument_t1650937354, ___argumentType_0)); }
	inline Type_t * get_argumentType_0() const { return ___argumentType_0; }
	inline Type_t ** get_address_of_argumentType_0() { return &___argumentType_0; }
	inline void set_argumentType_0(Type_t * value)
	{
		___argumentType_0 = value;
		Il2CppCodeGenWriteBarrier((&___argumentType_0), value);
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(CustomAttributeTypedArgument_t1650937354, ___value_1)); }
	inline RuntimeObject * get_value_1() const { return ___value_1; }
	inline RuntimeObject ** get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(RuntimeObject * value)
	{
		___value_1 = value;
		Il2CppCodeGenWriteBarrier((&___value_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Reflection.CustomAttributeTypedArgument
struct CustomAttributeTypedArgument_t1650937354_marshaled_pinvoke
{
	Type_t * ___argumentType_0;
	Il2CppIUnknown* ___value_1;
};
// Native definition for COM marshalling of System.Reflection.CustomAttributeTypedArgument
struct CustomAttributeTypedArgument_t1650937354_marshaled_com
{
	Type_t * ___argumentType_0;
	Il2CppIUnknown* ___value_1;
};
#endif // CUSTOMATTRIBUTETYPEDARGUMENT_T1650937354_H
#ifndef ANIMATORCLIPINFO_T3315865394_H
#define ANIMATORCLIPINFO_T3315865394_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.AnimatorClipInfo
struct  AnimatorClipInfo_t3315865394 
{
public:
	// System.Int32 UnityEngine.AnimatorClipInfo::m_ClipInstanceID
	int32_t ___m_ClipInstanceID_0;
	// System.Single UnityEngine.AnimatorClipInfo::m_Weight
	float ___m_Weight_1;

public:
	inline static int32_t get_offset_of_m_ClipInstanceID_0() { return static_cast<int32_t>(offsetof(AnimatorClipInfo_t3315865394, ___m_ClipInstanceID_0)); }
	inline int32_t get_m_ClipInstanceID_0() const { return ___m_ClipInstanceID_0; }
	inline int32_t* get_address_of_m_ClipInstanceID_0() { return &___m_ClipInstanceID_0; }
	inline void set_m_ClipInstanceID_0(int32_t value)
	{
		___m_ClipInstanceID_0 = value;
	}

	inline static int32_t get_offset_of_m_Weight_1() { return static_cast<int32_t>(offsetof(AnimatorClipInfo_t3315865394, ___m_Weight_1)); }
	inline float get_m_Weight_1() const { return ___m_Weight_1; }
	inline float* get_address_of_m_Weight_1() { return &___m_Weight_1; }
	inline void set_m_Weight_1(float value)
	{
		___m_Weight_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANIMATORCLIPINFO_T3315865394_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	IntPtr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline IntPtr_t get_Zero_1() const { return ___Zero_1; }
	inline IntPtr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(IntPtr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef COLOR32_T285432355_H
#define COLOR32_T285432355_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color32
struct ALIGN_TYPE(4) Color32_t285432355 
{
public:
	// System.Byte UnityEngine.Color32::r
	uint8_t ___r_0;
	// System.Byte UnityEngine.Color32::g
	uint8_t ___g_1;
	// System.Byte UnityEngine.Color32::b
	uint8_t ___b_2;
	// System.Byte UnityEngine.Color32::a
	uint8_t ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color32_t285432355, ___r_0)); }
	inline uint8_t get_r_0() const { return ___r_0; }
	inline uint8_t* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(uint8_t value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color32_t285432355, ___g_1)); }
	inline uint8_t get_g_1() const { return ___g_1; }
	inline uint8_t* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(uint8_t value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color32_t285432355, ___b_2)); }
	inline uint8_t get_b_2() const { return ___b_2; }
	inline uint8_t* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(uint8_t value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color32_t285432355, ___a_3)); }
	inline uint8_t get_a_3() const { return ___a_3; }
	inline uint8_t* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(uint8_t value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR32_T285432355_H
#ifndef VIRTUALBUTTONDATA_T4213555505_H
#define VIRTUALBUTTONDATA_T4213555505_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.VuforiaManagerImpl/VirtualButtonData
#pragma pack(push, tp, 1)
struct  VirtualButtonData_t4213555505 
{
public:
	// System.Int32 Vuforia.VuforiaManagerImpl/VirtualButtonData::id
	int32_t ___id_0;
	// System.Int32 Vuforia.VuforiaManagerImpl/VirtualButtonData::isPressed
	int32_t ___isPressed_1;

public:
	inline static int32_t get_offset_of_id_0() { return static_cast<int32_t>(offsetof(VirtualButtonData_t4213555505, ___id_0)); }
	inline int32_t get_id_0() const { return ___id_0; }
	inline int32_t* get_address_of_id_0() { return &___id_0; }
	inline void set_id_0(int32_t value)
	{
		___id_0 = value;
	}

	inline static int32_t get_offset_of_isPressed_1() { return static_cast<int32_t>(offsetof(VirtualButtonData_t4213555505, ___isPressed_1)); }
	inline int32_t get_isPressed_1() const { return ___isPressed_1; }
	inline int32_t* get_address_of_isPressed_1() { return &___isPressed_1; }
	inline void set_isPressed_1(int32_t value)
	{
		___isPressed_1 = value;
	}
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VIRTUALBUTTONDATA_T4213555505_H
#ifndef UILINEINFO_T998511395_H
#define UILINEINFO_T998511395_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UILineInfo
struct  UILineInfo_t998511395 
{
public:
	// System.Int32 UnityEngine.UILineInfo::startCharIdx
	int32_t ___startCharIdx_0;
	// System.Int32 UnityEngine.UILineInfo::height
	int32_t ___height_1;
	// System.Single UnityEngine.UILineInfo::topY
	float ___topY_2;
	// System.Single UnityEngine.UILineInfo::leading
	float ___leading_3;

public:
	inline static int32_t get_offset_of_startCharIdx_0() { return static_cast<int32_t>(offsetof(UILineInfo_t998511395, ___startCharIdx_0)); }
	inline int32_t get_startCharIdx_0() const { return ___startCharIdx_0; }
	inline int32_t* get_address_of_startCharIdx_0() { return &___startCharIdx_0; }
	inline void set_startCharIdx_0(int32_t value)
	{
		___startCharIdx_0 = value;
	}

	inline static int32_t get_offset_of_height_1() { return static_cast<int32_t>(offsetof(UILineInfo_t998511395, ___height_1)); }
	inline int32_t get_height_1() const { return ___height_1; }
	inline int32_t* get_address_of_height_1() { return &___height_1; }
	inline void set_height_1(int32_t value)
	{
		___height_1 = value;
	}

	inline static int32_t get_offset_of_topY_2() { return static_cast<int32_t>(offsetof(UILineInfo_t998511395, ___topY_2)); }
	inline float get_topY_2() const { return ___topY_2; }
	inline float* get_address_of_topY_2() { return &___topY_2; }
	inline void set_topY_2(float value)
	{
		___topY_2 = value;
	}

	inline static int32_t get_offset_of_leading_3() { return static_cast<int32_t>(offsetof(UILineInfo_t998511395, ___leading_3)); }
	inline float get_leading_3() const { return ___leading_3; }
	inline float* get_address_of_leading_3() { return &___leading_3; }
	inline void set_leading_3(float value)
	{
		___leading_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UILINEINFO_T998511395_H
#ifndef VECTOR2_T3577333262_H
#define VECTOR2_T3577333262_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2
struct  Vector2_t3577333262 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_t3577333262, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_t3577333262, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_t3577333262_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_t3577333262  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_t3577333262  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_t3577333262  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_t3577333262  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_t3577333262  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_t3577333262  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_t3577333262  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_t3577333262  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_t3577333262_StaticFields, ___zeroVector_2)); }
	inline Vector2_t3577333262  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_t3577333262 * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_t3577333262  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_t3577333262_StaticFields, ___oneVector_3)); }
	inline Vector2_t3577333262  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_t3577333262 * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_t3577333262  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_t3577333262_StaticFields, ___upVector_4)); }
	inline Vector2_t3577333262  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_t3577333262 * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_t3577333262  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_t3577333262_StaticFields, ___downVector_5)); }
	inline Vector2_t3577333262  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_t3577333262 * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_t3577333262  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_t3577333262_StaticFields, ___leftVector_6)); }
	inline Vector2_t3577333262  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_t3577333262 * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_t3577333262  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_t3577333262_StaticFields, ___rightVector_7)); }
	inline Vector2_t3577333262  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_t3577333262 * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_t3577333262  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_t3577333262_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_t3577333262  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_t3577333262 * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_t3577333262  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_t3577333262_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_t3577333262  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_t3577333262 * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_t3577333262  value)
	{
		___negativeInfinityVector_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_T3577333262_H
#ifndef VECTOR3_T2903530434_H
#define VECTOR3_T2903530434_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_t2903530434 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_1;
	// System.Single UnityEngine.Vector3::y
	float ___y_2;
	// System.Single UnityEngine.Vector3::z
	float ___z_3;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector3_t2903530434, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector3_t2903530434, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector3_t2903530434, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}
};

struct Vector3_t2903530434_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t2903530434  ___zeroVector_4;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t2903530434  ___oneVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t2903530434  ___upVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t2903530434  ___downVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t2903530434  ___leftVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t2903530434  ___rightVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t2903530434  ___forwardVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t2903530434  ___backVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t2903530434  ___positiveInfinityVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t2903530434  ___negativeInfinityVector_13;

public:
	inline static int32_t get_offset_of_zeroVector_4() { return static_cast<int32_t>(offsetof(Vector3_t2903530434_StaticFields, ___zeroVector_4)); }
	inline Vector3_t2903530434  get_zeroVector_4() const { return ___zeroVector_4; }
	inline Vector3_t2903530434 * get_address_of_zeroVector_4() { return &___zeroVector_4; }
	inline void set_zeroVector_4(Vector3_t2903530434  value)
	{
		___zeroVector_4 = value;
	}

	inline static int32_t get_offset_of_oneVector_5() { return static_cast<int32_t>(offsetof(Vector3_t2903530434_StaticFields, ___oneVector_5)); }
	inline Vector3_t2903530434  get_oneVector_5() const { return ___oneVector_5; }
	inline Vector3_t2903530434 * get_address_of_oneVector_5() { return &___oneVector_5; }
	inline void set_oneVector_5(Vector3_t2903530434  value)
	{
		___oneVector_5 = value;
	}

	inline static int32_t get_offset_of_upVector_6() { return static_cast<int32_t>(offsetof(Vector3_t2903530434_StaticFields, ___upVector_6)); }
	inline Vector3_t2903530434  get_upVector_6() const { return ___upVector_6; }
	inline Vector3_t2903530434 * get_address_of_upVector_6() { return &___upVector_6; }
	inline void set_upVector_6(Vector3_t2903530434  value)
	{
		___upVector_6 = value;
	}

	inline static int32_t get_offset_of_downVector_7() { return static_cast<int32_t>(offsetof(Vector3_t2903530434_StaticFields, ___downVector_7)); }
	inline Vector3_t2903530434  get_downVector_7() const { return ___downVector_7; }
	inline Vector3_t2903530434 * get_address_of_downVector_7() { return &___downVector_7; }
	inline void set_downVector_7(Vector3_t2903530434  value)
	{
		___downVector_7 = value;
	}

	inline static int32_t get_offset_of_leftVector_8() { return static_cast<int32_t>(offsetof(Vector3_t2903530434_StaticFields, ___leftVector_8)); }
	inline Vector3_t2903530434  get_leftVector_8() const { return ___leftVector_8; }
	inline Vector3_t2903530434 * get_address_of_leftVector_8() { return &___leftVector_8; }
	inline void set_leftVector_8(Vector3_t2903530434  value)
	{
		___leftVector_8 = value;
	}

	inline static int32_t get_offset_of_rightVector_9() { return static_cast<int32_t>(offsetof(Vector3_t2903530434_StaticFields, ___rightVector_9)); }
	inline Vector3_t2903530434  get_rightVector_9() const { return ___rightVector_9; }
	inline Vector3_t2903530434 * get_address_of_rightVector_9() { return &___rightVector_9; }
	inline void set_rightVector_9(Vector3_t2903530434  value)
	{
		___rightVector_9 = value;
	}

	inline static int32_t get_offset_of_forwardVector_10() { return static_cast<int32_t>(offsetof(Vector3_t2903530434_StaticFields, ___forwardVector_10)); }
	inline Vector3_t2903530434  get_forwardVector_10() const { return ___forwardVector_10; }
	inline Vector3_t2903530434 * get_address_of_forwardVector_10() { return &___forwardVector_10; }
	inline void set_forwardVector_10(Vector3_t2903530434  value)
	{
		___forwardVector_10 = value;
	}

	inline static int32_t get_offset_of_backVector_11() { return static_cast<int32_t>(offsetof(Vector3_t2903530434_StaticFields, ___backVector_11)); }
	inline Vector3_t2903530434  get_backVector_11() const { return ___backVector_11; }
	inline Vector3_t2903530434 * get_address_of_backVector_11() { return &___backVector_11; }
	inline void set_backVector_11(Vector3_t2903530434  value)
	{
		___backVector_11 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_12() { return static_cast<int32_t>(offsetof(Vector3_t2903530434_StaticFields, ___positiveInfinityVector_12)); }
	inline Vector3_t2903530434  get_positiveInfinityVector_12() const { return ___positiveInfinityVector_12; }
	inline Vector3_t2903530434 * get_address_of_positiveInfinityVector_12() { return &___positiveInfinityVector_12; }
	inline void set_positiveInfinityVector_12(Vector3_t2903530434  value)
	{
		___positiveInfinityVector_12 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t2903530434_StaticFields, ___negativeInfinityVector_13)); }
	inline Vector3_t2903530434  get_negativeInfinityVector_13() const { return ___negativeInfinityVector_13; }
	inline Vector3_t2903530434 * get_address_of_negativeInfinityVector_13() { return &___negativeInfinityVector_13; }
	inline void set_negativeInfinityVector_13(Vector3_t2903530434  value)
	{
		___negativeInfinityVector_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_T2903530434_H
#ifndef INT32_T4237944589_H
#define INT32_T4237944589_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Int32
struct  Int32_t4237944589 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Int32_t4237944589, ___m_value_2)); }
	inline int32_t get_m_value_2() const { return ___m_value_2; }
	inline int32_t* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(int32_t value)
	{
		___m_value_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT32_T4237944589_H
#ifndef VECTOR4_T2814672345_H
#define VECTOR4_T2814672345_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector4
struct  Vector4_t2814672345 
{
public:
	// System.Single UnityEngine.Vector4::x
	float ___x_1;
	// System.Single UnityEngine.Vector4::y
	float ___y_2;
	// System.Single UnityEngine.Vector4::z
	float ___z_3;
	// System.Single UnityEngine.Vector4::w
	float ___w_4;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector4_t2814672345, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector4_t2814672345, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector4_t2814672345, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}

	inline static int32_t get_offset_of_w_4() { return static_cast<int32_t>(offsetof(Vector4_t2814672345, ___w_4)); }
	inline float get_w_4() const { return ___w_4; }
	inline float* get_address_of_w_4() { return &___w_4; }
	inline void set_w_4(float value)
	{
		___w_4 = value;
	}
};

struct Vector4_t2814672345_StaticFields
{
public:
	// UnityEngine.Vector4 UnityEngine.Vector4::zeroVector
	Vector4_t2814672345  ___zeroVector_5;
	// UnityEngine.Vector4 UnityEngine.Vector4::oneVector
	Vector4_t2814672345  ___oneVector_6;
	// UnityEngine.Vector4 UnityEngine.Vector4::positiveInfinityVector
	Vector4_t2814672345  ___positiveInfinityVector_7;
	// UnityEngine.Vector4 UnityEngine.Vector4::negativeInfinityVector
	Vector4_t2814672345  ___negativeInfinityVector_8;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector4_t2814672345_StaticFields, ___zeroVector_5)); }
	inline Vector4_t2814672345  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector4_t2814672345 * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector4_t2814672345  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector4_t2814672345_StaticFields, ___oneVector_6)); }
	inline Vector4_t2814672345  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector4_t2814672345 * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector4_t2814672345  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_7() { return static_cast<int32_t>(offsetof(Vector4_t2814672345_StaticFields, ___positiveInfinityVector_7)); }
	inline Vector4_t2814672345  get_positiveInfinityVector_7() const { return ___positiveInfinityVector_7; }
	inline Vector4_t2814672345 * get_address_of_positiveInfinityVector_7() { return &___positiveInfinityVector_7; }
	inline void set_positiveInfinityVector_7(Vector4_t2814672345  value)
	{
		___positiveInfinityVector_7 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector4_t2814672345_StaticFields, ___negativeInfinityVector_8)); }
	inline Vector4_t2814672345  get_negativeInfinityVector_8() const { return ___negativeInfinityVector_8; }
	inline Vector4_t2814672345 * get_address_of_negativeInfinityVector_8() { return &___negativeInfinityVector_8; }
	inline void set_negativeInfinityVector_8(Vector4_t2814672345  value)
	{
		___negativeInfinityVector_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR4_T2814672345_H
#ifndef VOID_T2217553113_H
#define VOID_T2217553113_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t2217553113 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T2217553113_H
#ifndef ENUM_T978401890_H
#define ENUM_T978401890_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t978401890  : public ValueType_t1306072465
{
public:

public:
};

struct Enum_t978401890_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t45629911* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t978401890_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t45629911* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t45629911** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t45629911* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t978401890_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t978401890_marshaled_com
{
};
#endif // ENUM_T978401890_H
#ifndef VEC2I_T895597728_H
#define VEC2I_T895597728_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.VuforiaRenderer/Vec2I
#pragma pack(push, tp, 1)
struct  Vec2I_t895597728 
{
public:
	// System.Int32 Vuforia.VuforiaRenderer/Vec2I::x
	int32_t ___x_0;
	// System.Int32 Vuforia.VuforiaRenderer/Vec2I::y
	int32_t ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vec2I_t895597728, ___x_0)); }
	inline int32_t get_x_0() const { return ___x_0; }
	inline int32_t* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(int32_t value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vec2I_t895597728, ___y_1)); }
	inline int32_t get_y_1() const { return ___y_1; }
	inline int32_t* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(int32_t value)
	{
		___y_1 = value;
	}
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VEC2I_T895597728_H
#ifndef QUATERNION_T754065749_H
#define QUATERNION_T754065749_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Quaternion
struct  Quaternion_t754065749 
{
public:
	// System.Single UnityEngine.Quaternion::x
	float ___x_0;
	// System.Single UnityEngine.Quaternion::y
	float ___y_1;
	// System.Single UnityEngine.Quaternion::z
	float ___z_2;
	// System.Single UnityEngine.Quaternion::w
	float ___w_3;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Quaternion_t754065749, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Quaternion_t754065749, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(Quaternion_t754065749, ___z_2)); }
	inline float get_z_2() const { return ___z_2; }
	inline float* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(float value)
	{
		___z_2 = value;
	}

	inline static int32_t get_offset_of_w_3() { return static_cast<int32_t>(offsetof(Quaternion_t754065749, ___w_3)); }
	inline float get_w_3() const { return ___w_3; }
	inline float* get_address_of_w_3() { return &___w_3; }
	inline void set_w_3(float value)
	{
		___w_3 = value;
	}
};

struct Quaternion_t754065749_StaticFields
{
public:
	// UnityEngine.Quaternion UnityEngine.Quaternion::identityQuaternion
	Quaternion_t754065749  ___identityQuaternion_4;

public:
	inline static int32_t get_offset_of_identityQuaternion_4() { return static_cast<int32_t>(offsetof(Quaternion_t754065749_StaticFields, ___identityQuaternion_4)); }
	inline Quaternion_t754065749  get_identityQuaternion_4() const { return ___identityQuaternion_4; }
	inline Quaternion_t754065749 * get_address_of_identityQuaternion_4() { return &___identityQuaternion_4; }
	inline void set_identityQuaternion_4(Quaternion_t754065749  value)
	{
		___identityQuaternion_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUATERNION_T754065749_H
#ifndef SYSTEMEXCEPTION_T870616472_H
#define SYSTEMEXCEPTION_T870616472_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.SystemException
struct  SystemException_t870616472  : public Exception_t1975590229
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SYSTEMEXCEPTION_T870616472_H
#ifndef KEYVALUEPAIR_2_T2536236643_H
#define KEYVALUEPAIR_2_T2536236643_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>
struct  KeyValuePair_2_t2536236643 
{
public:
	// TKey System.Collections.Generic.KeyValuePair`2::key
	int32_t ___key_0;
	// TValue System.Collections.Generic.KeyValuePair`2::value
	RuntimeObject * ___value_1;

public:
	inline static int32_t get_offset_of_key_0() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t2536236643, ___key_0)); }
	inline int32_t get_key_0() const { return ___key_0; }
	inline int32_t* get_address_of_key_0() { return &___key_0; }
	inline void set_key_0(int32_t value)
	{
		___key_0 = value;
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t2536236643, ___value_1)); }
	inline RuntimeObject * get_value_1() const { return ___value_1; }
	inline RuntimeObject ** get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(RuntimeObject * value)
	{
		___value_1 = value;
		Il2CppCodeGenWriteBarrier((&___value_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYVALUEPAIR_2_T2536236643_H
#ifndef ENUMERATOR_T2954986095_H
#define ENUMERATOR_T2954986095_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1/Enumerator<System.Object>
struct  Enumerator_t2954986095 
{
public:
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator::l
	List_1_t877107497 * ___l_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::next
	int32_t ___next_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::ver
	int32_t ___ver_2;
	// T System.Collections.Generic.List`1/Enumerator::current
	RuntimeObject * ___current_3;

public:
	inline static int32_t get_offset_of_l_0() { return static_cast<int32_t>(offsetof(Enumerator_t2954986095, ___l_0)); }
	inline List_1_t877107497 * get_l_0() const { return ___l_0; }
	inline List_1_t877107497 ** get_address_of_l_0() { return &___l_0; }
	inline void set_l_0(List_1_t877107497 * value)
	{
		___l_0 = value;
		Il2CppCodeGenWriteBarrier((&___l_0), value);
	}

	inline static int32_t get_offset_of_next_1() { return static_cast<int32_t>(offsetof(Enumerator_t2954986095, ___next_1)); }
	inline int32_t get_next_1() const { return ___next_1; }
	inline int32_t* get_address_of_next_1() { return &___next_1; }
	inline void set_next_1(int32_t value)
	{
		___next_1 = value;
	}

	inline static int32_t get_offset_of_ver_2() { return static_cast<int32_t>(offsetof(Enumerator_t2954986095, ___ver_2)); }
	inline int32_t get_ver_2() const { return ___ver_2; }
	inline int32_t* get_address_of_ver_2() { return &___ver_2; }
	inline void set_ver_2(int32_t value)
	{
		___ver_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_t2954986095, ___current_3)); }
	inline RuntimeObject * get_current_3() const { return ___current_3; }
	inline RuntimeObject ** get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(RuntimeObject * value)
	{
		___current_3 = value;
		Il2CppCodeGenWriteBarrier((&___current_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMERATOR_T2954986095_H
#ifndef KEYVALUEPAIR_2_T574427458_H
#define KEYVALUEPAIR_2_T574427458_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt16>
struct  KeyValuePair_2_t574427458 
{
public:
	// TKey System.Collections.Generic.KeyValuePair`2::key
	RuntimeObject * ___key_0;
	// TValue System.Collections.Generic.KeyValuePair`2::value
	uint16_t ___value_1;

public:
	inline static int32_t get_offset_of_key_0() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t574427458, ___key_0)); }
	inline RuntimeObject * get_key_0() const { return ___key_0; }
	inline RuntimeObject ** get_address_of_key_0() { return &___key_0; }
	inline void set_key_0(RuntimeObject * value)
	{
		___key_0 = value;
		Il2CppCodeGenWriteBarrier((&___key_0), value);
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t574427458, ___value_1)); }
	inline uint16_t get_value_1() const { return ___value_1; }
	inline uint16_t* get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(uint16_t value)
	{
		___value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYVALUEPAIR_2_T574427458_H
#ifndef ENUMERATOR_T876005098_H
#define ENUMERATOR_T876005098_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1/Enumerator<System.Int32>
struct  Enumerator_t876005098 
{
public:
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator::l
	List_1_t3093093796 * ___l_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::next
	int32_t ___next_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::ver
	int32_t ___ver_2;
	// T System.Collections.Generic.List`1/Enumerator::current
	int32_t ___current_3;

public:
	inline static int32_t get_offset_of_l_0() { return static_cast<int32_t>(offsetof(Enumerator_t876005098, ___l_0)); }
	inline List_1_t3093093796 * get_l_0() const { return ___l_0; }
	inline List_1_t3093093796 ** get_address_of_l_0() { return &___l_0; }
	inline void set_l_0(List_1_t3093093796 * value)
	{
		___l_0 = value;
		Il2CppCodeGenWriteBarrier((&___l_0), value);
	}

	inline static int32_t get_offset_of_next_1() { return static_cast<int32_t>(offsetof(Enumerator_t876005098, ___next_1)); }
	inline int32_t get_next_1() const { return ___next_1; }
	inline int32_t* get_address_of_next_1() { return &___next_1; }
	inline void set_next_1(int32_t value)
	{
		___next_1 = value;
	}

	inline static int32_t get_offset_of_ver_2() { return static_cast<int32_t>(offsetof(Enumerator_t876005098, ___ver_2)); }
	inline int32_t get_ver_2() const { return ___ver_2; }
	inline int32_t* get_address_of_ver_2() { return &___ver_2; }
	inline void set_ver_2(int32_t value)
	{
		___ver_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_t876005098, ___current_3)); }
	inline int32_t get_current_3() const { return ___current_3; }
	inline int32_t* get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(int32_t value)
	{
		___current_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMERATOR_T876005098_H
#ifndef UINT32_T1209397712_H
#define UINT32_T1209397712_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.UInt32
struct  UInt32_t1209397712 
{
public:
	// System.UInt32 System.UInt32::m_value
	uint32_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(UInt32_t1209397712, ___m_value_0)); }
	inline uint32_t get_m_value_0() const { return ___m_value_0; }
	inline uint32_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(uint32_t value)
	{
		___m_value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UINT32_T1209397712_H
#ifndef TRACKABLEIDPAIR_T1331427386_H
#define TRACKABLEIDPAIR_T1331427386_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.VuforiaManager/TrackableIdPair
struct  TrackableIdPair_t1331427386 
{
public:
	// System.Int32 Vuforia.VuforiaManager/TrackableIdPair::TrackableId
	int32_t ___TrackableId_0;
	// System.Int32 Vuforia.VuforiaManager/TrackableIdPair::ResultId
	int32_t ___ResultId_1;

public:
	inline static int32_t get_offset_of_TrackableId_0() { return static_cast<int32_t>(offsetof(TrackableIdPair_t1331427386, ___TrackableId_0)); }
	inline int32_t get_TrackableId_0() const { return ___TrackableId_0; }
	inline int32_t* get_address_of_TrackableId_0() { return &___TrackableId_0; }
	inline void set_TrackableId_0(int32_t value)
	{
		___TrackableId_0 = value;
	}

	inline static int32_t get_offset_of_ResultId_1() { return static_cast<int32_t>(offsetof(TrackableIdPair_t1331427386, ___ResultId_1)); }
	inline int32_t get_ResultId_1() const { return ___ResultId_1; }
	inline int32_t* get_address_of_ResultId_1() { return &___ResultId_1; }
	inline void set_ResultId_1(int32_t value)
	{
		___ResultId_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRACKABLEIDPAIR_T1331427386_H
#ifndef ENUMERATOR_T1292916605_H
#define ENUMERATOR_T1292916605_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.LinkedList`1/Enumerator<Vuforia.VuforiaManager/TrackableIdPair>
struct  Enumerator_t1292916605 
{
public:
	// System.Collections.Generic.LinkedList`1<T> System.Collections.Generic.LinkedList`1/Enumerator::list
	LinkedList_1_t3507684622 * ___list_0;
	// System.Collections.Generic.LinkedListNode`1<T> System.Collections.Generic.LinkedList`1/Enumerator::current
	LinkedListNode_1_t1003534897 * ___current_1;
	// System.Int32 System.Collections.Generic.LinkedList`1/Enumerator::index
	int32_t ___index_2;
	// System.UInt32 System.Collections.Generic.LinkedList`1/Enumerator::version
	uint32_t ___version_3;

public:
	inline static int32_t get_offset_of_list_0() { return static_cast<int32_t>(offsetof(Enumerator_t1292916605, ___list_0)); }
	inline LinkedList_1_t3507684622 * get_list_0() const { return ___list_0; }
	inline LinkedList_1_t3507684622 ** get_address_of_list_0() { return &___list_0; }
	inline void set_list_0(LinkedList_1_t3507684622 * value)
	{
		___list_0 = value;
		Il2CppCodeGenWriteBarrier((&___list_0), value);
	}

	inline static int32_t get_offset_of_current_1() { return static_cast<int32_t>(offsetof(Enumerator_t1292916605, ___current_1)); }
	inline LinkedListNode_1_t1003534897 * get_current_1() const { return ___current_1; }
	inline LinkedListNode_1_t1003534897 ** get_address_of_current_1() { return &___current_1; }
	inline void set_current_1(LinkedListNode_1_t1003534897 * value)
	{
		___current_1 = value;
		Il2CppCodeGenWriteBarrier((&___current_1), value);
	}

	inline static int32_t get_offset_of_index_2() { return static_cast<int32_t>(offsetof(Enumerator_t1292916605, ___index_2)); }
	inline int32_t get_index_2() const { return ___index_2; }
	inline int32_t* get_address_of_index_2() { return &___index_2; }
	inline void set_index_2(int32_t value)
	{
		___index_2 = value;
	}

	inline static int32_t get_offset_of_version_3() { return static_cast<int32_t>(offsetof(Enumerator_t1292916605, ___version_3)); }
	inline uint32_t get_version_3() const { return ___version_3; }
	inline uint32_t* get_address_of_version_3() { return &___version_3; }
	inline void set_version_3(uint32_t value)
	{
		___version_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMERATOR_T1292916605_H
#ifndef UINT16_T3409655978_H
#define UINT16_T3409655978_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.UInt16
struct  UInt16_t3409655978 
{
public:
	// System.UInt16 System.UInt16::m_value
	uint16_t ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(UInt16_t3409655978, ___m_value_2)); }
	inline uint16_t get_m_value_2() const { return ___m_value_2; }
	inline uint16_t* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(uint16_t value)
	{
		___m_value_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UINT16_T3409655978_H
#ifndef ENUMERATOR_T1983447509_H
#define ENUMERATOR_T1983447509_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.LinkedList`1/Enumerator<System.Object>
struct  Enumerator_t1983447509 
{
public:
	// System.Collections.Generic.LinkedList`1<T> System.Collections.Generic.LinkedList`1/Enumerator::list
	LinkedList_1_t4198215526 * ___list_0;
	// System.Collections.Generic.LinkedListNode`1<T> System.Collections.Generic.LinkedList`1/Enumerator::current
	LinkedListNode_1_t1694065801 * ___current_1;
	// System.Int32 System.Collections.Generic.LinkedList`1/Enumerator::index
	int32_t ___index_2;
	// System.UInt32 System.Collections.Generic.LinkedList`1/Enumerator::version
	uint32_t ___version_3;

public:
	inline static int32_t get_offset_of_list_0() { return static_cast<int32_t>(offsetof(Enumerator_t1983447509, ___list_0)); }
	inline LinkedList_1_t4198215526 * get_list_0() const { return ___list_0; }
	inline LinkedList_1_t4198215526 ** get_address_of_list_0() { return &___list_0; }
	inline void set_list_0(LinkedList_1_t4198215526 * value)
	{
		___list_0 = value;
		Il2CppCodeGenWriteBarrier((&___list_0), value);
	}

	inline static int32_t get_offset_of_current_1() { return static_cast<int32_t>(offsetof(Enumerator_t1983447509, ___current_1)); }
	inline LinkedListNode_1_t1694065801 * get_current_1() const { return ___current_1; }
	inline LinkedListNode_1_t1694065801 ** get_address_of_current_1() { return &___current_1; }
	inline void set_current_1(LinkedListNode_1_t1694065801 * value)
	{
		___current_1 = value;
		Il2CppCodeGenWriteBarrier((&___current_1), value);
	}

	inline static int32_t get_offset_of_index_2() { return static_cast<int32_t>(offsetof(Enumerator_t1983447509, ___index_2)); }
	inline int32_t get_index_2() const { return ___index_2; }
	inline int32_t* get_address_of_index_2() { return &___index_2; }
	inline void set_index_2(int32_t value)
	{
		___index_2 = value;
	}

	inline static int32_t get_offset_of_version_3() { return static_cast<int32_t>(offsetof(Enumerator_t1983447509, ___version_3)); }
	inline uint32_t get_version_3() const { return ___version_3; }
	inline uint32_t* get_address_of_version_3() { return &___version_3; }
	inline void set_version_3(uint32_t value)
	{
		___version_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMERATOR_T1983447509_H
#ifndef ENUMERATOR_T3836558239_H
#define ENUMERATOR_T3836558239_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1/Enumerator<UnityEngine.Vector3>
struct  Enumerator_t3836558239 
{
public:
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator::l
	List_1_t1758679641 * ___l_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::next
	int32_t ___next_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::ver
	int32_t ___ver_2;
	// T System.Collections.Generic.List`1/Enumerator::current
	Vector3_t2903530434  ___current_3;

public:
	inline static int32_t get_offset_of_l_0() { return static_cast<int32_t>(offsetof(Enumerator_t3836558239, ___l_0)); }
	inline List_1_t1758679641 * get_l_0() const { return ___l_0; }
	inline List_1_t1758679641 ** get_address_of_l_0() { return &___l_0; }
	inline void set_l_0(List_1_t1758679641 * value)
	{
		___l_0 = value;
		Il2CppCodeGenWriteBarrier((&___l_0), value);
	}

	inline static int32_t get_offset_of_next_1() { return static_cast<int32_t>(offsetof(Enumerator_t3836558239, ___next_1)); }
	inline int32_t get_next_1() const { return ___next_1; }
	inline int32_t* get_address_of_next_1() { return &___next_1; }
	inline void set_next_1(int32_t value)
	{
		___next_1 = value;
	}

	inline static int32_t get_offset_of_ver_2() { return static_cast<int32_t>(offsetof(Enumerator_t3836558239, ___ver_2)); }
	inline int32_t get_ver_2() const { return ___ver_2; }
	inline int32_t* get_address_of_ver_2() { return &___ver_2; }
	inline void set_ver_2(int32_t value)
	{
		___ver_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_t3836558239, ___current_3)); }
	inline Vector3_t2903530434  get_current_3() const { return ___current_3; }
	inline Vector3_t2903530434 * get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(Vector3_t2903530434  value)
	{
		___current_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMERATOR_T3836558239_H
#ifndef INVALIDOPERATIONEXCEPTION_T2321794232_H
#define INVALIDOPERATIONEXCEPTION_T2321794232_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.InvalidOperationException
struct  InvalidOperationException_t2321794232  : public SystemException_t870616472
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INVALIDOPERATIONEXCEPTION_T2321794232_H
#ifndef PROFILEDATA_T740879429_H
#define PROFILEDATA_T740879429_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.WebCamProfile/ProfileData
struct  ProfileData_t740879429 
{
public:
	// Vuforia.VuforiaRenderer/Vec2I Vuforia.WebCamProfile/ProfileData::RequestedTextureSize
	Vec2I_t895597728  ___RequestedTextureSize_0;
	// Vuforia.VuforiaRenderer/Vec2I Vuforia.WebCamProfile/ProfileData::ResampledTextureSize
	Vec2I_t895597728  ___ResampledTextureSize_1;
	// System.Int32 Vuforia.WebCamProfile/ProfileData::RequestedFPS
	int32_t ___RequestedFPS_2;

public:
	inline static int32_t get_offset_of_RequestedTextureSize_0() { return static_cast<int32_t>(offsetof(ProfileData_t740879429, ___RequestedTextureSize_0)); }
	inline Vec2I_t895597728  get_RequestedTextureSize_0() const { return ___RequestedTextureSize_0; }
	inline Vec2I_t895597728 * get_address_of_RequestedTextureSize_0() { return &___RequestedTextureSize_0; }
	inline void set_RequestedTextureSize_0(Vec2I_t895597728  value)
	{
		___RequestedTextureSize_0 = value;
	}

	inline static int32_t get_offset_of_ResampledTextureSize_1() { return static_cast<int32_t>(offsetof(ProfileData_t740879429, ___ResampledTextureSize_1)); }
	inline Vec2I_t895597728  get_ResampledTextureSize_1() const { return ___ResampledTextureSize_1; }
	inline Vec2I_t895597728 * get_address_of_ResampledTextureSize_1() { return &___ResampledTextureSize_1; }
	inline void set_ResampledTextureSize_1(Vec2I_t895597728  value)
	{
		___ResampledTextureSize_1 = value;
	}

	inline static int32_t get_offset_of_RequestedFPS_2() { return static_cast<int32_t>(offsetof(ProfileData_t740879429, ___RequestedFPS_2)); }
	inline int32_t get_RequestedFPS_2() const { return ___RequestedFPS_2; }
	inline int32_t* get_address_of_RequestedFPS_2() { return &___RequestedFPS_2; }
	inline void set_RequestedFPS_2(int32_t value)
	{
		___RequestedFPS_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROFILEDATA_T740879429_H
#ifndef ENUMERATOR_T215393771_H
#define ENUMERATOR_T215393771_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1/Enumerator<UnityEngine.Vector2>
struct  Enumerator_t215393771 
{
public:
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator::l
	List_1_t2432482469 * ___l_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::next
	int32_t ___next_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::ver
	int32_t ___ver_2;
	// T System.Collections.Generic.List`1/Enumerator::current
	Vector2_t3577333262  ___current_3;

public:
	inline static int32_t get_offset_of_l_0() { return static_cast<int32_t>(offsetof(Enumerator_t215393771, ___l_0)); }
	inline List_1_t2432482469 * get_l_0() const { return ___l_0; }
	inline List_1_t2432482469 ** get_address_of_l_0() { return &___l_0; }
	inline void set_l_0(List_1_t2432482469 * value)
	{
		___l_0 = value;
		Il2CppCodeGenWriteBarrier((&___l_0), value);
	}

	inline static int32_t get_offset_of_next_1() { return static_cast<int32_t>(offsetof(Enumerator_t215393771, ___next_1)); }
	inline int32_t get_next_1() const { return ___next_1; }
	inline int32_t* get_address_of_next_1() { return &___next_1; }
	inline void set_next_1(int32_t value)
	{
		___next_1 = value;
	}

	inline static int32_t get_offset_of_ver_2() { return static_cast<int32_t>(offsetof(Enumerator_t215393771, ___ver_2)); }
	inline int32_t get_ver_2() const { return ___ver_2; }
	inline int32_t* get_address_of_ver_2() { return &___ver_2; }
	inline void set_ver_2(int32_t value)
	{
		___ver_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_t215393771, ___current_3)); }
	inline Vector2_t3577333262  get_current_3() const { return ___current_3; }
	inline Vector2_t3577333262 * get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(Vector2_t3577333262  value)
	{
		___current_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMERATOR_T215393771_H
#ifndef UIVERTEX_T2786467440_H
#define UIVERTEX_T2786467440_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UIVertex
struct  UIVertex_t2786467440 
{
public:
	// UnityEngine.Vector3 UnityEngine.UIVertex::position
	Vector3_t2903530434  ___position_0;
	// UnityEngine.Vector3 UnityEngine.UIVertex::normal
	Vector3_t2903530434  ___normal_1;
	// UnityEngine.Color32 UnityEngine.UIVertex::color
	Color32_t285432355  ___color_2;
	// UnityEngine.Vector2 UnityEngine.UIVertex::uv0
	Vector2_t3577333262  ___uv0_3;
	// UnityEngine.Vector2 UnityEngine.UIVertex::uv1
	Vector2_t3577333262  ___uv1_4;
	// UnityEngine.Vector2 UnityEngine.UIVertex::uv2
	Vector2_t3577333262  ___uv2_5;
	// UnityEngine.Vector2 UnityEngine.UIVertex::uv3
	Vector2_t3577333262  ___uv3_6;
	// UnityEngine.Vector4 UnityEngine.UIVertex::tangent
	Vector4_t2814672345  ___tangent_7;

public:
	inline static int32_t get_offset_of_position_0() { return static_cast<int32_t>(offsetof(UIVertex_t2786467440, ___position_0)); }
	inline Vector3_t2903530434  get_position_0() const { return ___position_0; }
	inline Vector3_t2903530434 * get_address_of_position_0() { return &___position_0; }
	inline void set_position_0(Vector3_t2903530434  value)
	{
		___position_0 = value;
	}

	inline static int32_t get_offset_of_normal_1() { return static_cast<int32_t>(offsetof(UIVertex_t2786467440, ___normal_1)); }
	inline Vector3_t2903530434  get_normal_1() const { return ___normal_1; }
	inline Vector3_t2903530434 * get_address_of_normal_1() { return &___normal_1; }
	inline void set_normal_1(Vector3_t2903530434  value)
	{
		___normal_1 = value;
	}

	inline static int32_t get_offset_of_color_2() { return static_cast<int32_t>(offsetof(UIVertex_t2786467440, ___color_2)); }
	inline Color32_t285432355  get_color_2() const { return ___color_2; }
	inline Color32_t285432355 * get_address_of_color_2() { return &___color_2; }
	inline void set_color_2(Color32_t285432355  value)
	{
		___color_2 = value;
	}

	inline static int32_t get_offset_of_uv0_3() { return static_cast<int32_t>(offsetof(UIVertex_t2786467440, ___uv0_3)); }
	inline Vector2_t3577333262  get_uv0_3() const { return ___uv0_3; }
	inline Vector2_t3577333262 * get_address_of_uv0_3() { return &___uv0_3; }
	inline void set_uv0_3(Vector2_t3577333262  value)
	{
		___uv0_3 = value;
	}

	inline static int32_t get_offset_of_uv1_4() { return static_cast<int32_t>(offsetof(UIVertex_t2786467440, ___uv1_4)); }
	inline Vector2_t3577333262  get_uv1_4() const { return ___uv1_4; }
	inline Vector2_t3577333262 * get_address_of_uv1_4() { return &___uv1_4; }
	inline void set_uv1_4(Vector2_t3577333262  value)
	{
		___uv1_4 = value;
	}

	inline static int32_t get_offset_of_uv2_5() { return static_cast<int32_t>(offsetof(UIVertex_t2786467440, ___uv2_5)); }
	inline Vector2_t3577333262  get_uv2_5() const { return ___uv2_5; }
	inline Vector2_t3577333262 * get_address_of_uv2_5() { return &___uv2_5; }
	inline void set_uv2_5(Vector2_t3577333262  value)
	{
		___uv2_5 = value;
	}

	inline static int32_t get_offset_of_uv3_6() { return static_cast<int32_t>(offsetof(UIVertex_t2786467440, ___uv3_6)); }
	inline Vector2_t3577333262  get_uv3_6() const { return ___uv3_6; }
	inline Vector2_t3577333262 * get_address_of_uv3_6() { return &___uv3_6; }
	inline void set_uv3_6(Vector2_t3577333262  value)
	{
		___uv3_6 = value;
	}

	inline static int32_t get_offset_of_tangent_7() { return static_cast<int32_t>(offsetof(UIVertex_t2786467440, ___tangent_7)); }
	inline Vector4_t2814672345  get_tangent_7() const { return ___tangent_7; }
	inline Vector4_t2814672345 * get_address_of_tangent_7() { return &___tangent_7; }
	inline void set_tangent_7(Vector4_t2814672345  value)
	{
		___tangent_7 = value;
	}
};

struct UIVertex_t2786467440_StaticFields
{
public:
	// UnityEngine.Color32 UnityEngine.UIVertex::s_DefaultColor
	Color32_t285432355  ___s_DefaultColor_8;
	// UnityEngine.Vector4 UnityEngine.UIVertex::s_DefaultTangent
	Vector4_t2814672345  ___s_DefaultTangent_9;
	// UnityEngine.UIVertex UnityEngine.UIVertex::simpleVert
	UIVertex_t2786467440  ___simpleVert_10;

public:
	inline static int32_t get_offset_of_s_DefaultColor_8() { return static_cast<int32_t>(offsetof(UIVertex_t2786467440_StaticFields, ___s_DefaultColor_8)); }
	inline Color32_t285432355  get_s_DefaultColor_8() const { return ___s_DefaultColor_8; }
	inline Color32_t285432355 * get_address_of_s_DefaultColor_8() { return &___s_DefaultColor_8; }
	inline void set_s_DefaultColor_8(Color32_t285432355  value)
	{
		___s_DefaultColor_8 = value;
	}

	inline static int32_t get_offset_of_s_DefaultTangent_9() { return static_cast<int32_t>(offsetof(UIVertex_t2786467440_StaticFields, ___s_DefaultTangent_9)); }
	inline Vector4_t2814672345  get_s_DefaultTangent_9() const { return ___s_DefaultTangent_9; }
	inline Vector4_t2814672345 * get_address_of_s_DefaultTangent_9() { return &___s_DefaultTangent_9; }
	inline void set_s_DefaultTangent_9(Vector4_t2814672345  value)
	{
		___s_DefaultTangent_9 = value;
	}

	inline static int32_t get_offset_of_simpleVert_10() { return static_cast<int32_t>(offsetof(UIVertex_t2786467440_StaticFields, ___simpleVert_10)); }
	inline UIVertex_t2786467440  get_simpleVert_10() const { return ___simpleVert_10; }
	inline UIVertex_t2786467440 * get_address_of_simpleVert_10() { return &___simpleVert_10; }
	inline void set_simpleVert_10(UIVertex_t2786467440  value)
	{
		___simpleVert_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIVERTEX_T2786467440_H
#ifndef STATUS_T1358118869_H
#define STATUS_T1358118869_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.TrackableBehaviour/Status
struct  Status_t1358118869 
{
public:
	// System.Int32 Vuforia.TrackableBehaviour/Status::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Status_t1358118869, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STATUS_T1358118869_H
#ifndef BINDINGFLAGS_T1805189573_H
#define BINDINGFLAGS_T1805189573_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.BindingFlags
struct  BindingFlags_t1805189573 
{
public:
	// System.Int32 System.Reflection.BindingFlags::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(BindingFlags_t1805189573, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BINDINGFLAGS_T1805189573_H
#ifndef ENUMERATOR_T1931539200_H
#define ENUMERATOR_T1931539200_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1/Enumerator<UnityEngine.UILineInfo>
struct  Enumerator_t1931539200 
{
public:
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator::l
	List_1_t4148627898 * ___l_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::next
	int32_t ___next_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::ver
	int32_t ___ver_2;
	// T System.Collections.Generic.List`1/Enumerator::current
	UILineInfo_t998511395  ___current_3;

public:
	inline static int32_t get_offset_of_l_0() { return static_cast<int32_t>(offsetof(Enumerator_t1931539200, ___l_0)); }
	inline List_1_t4148627898 * get_l_0() const { return ___l_0; }
	inline List_1_t4148627898 ** get_address_of_l_0() { return &___l_0; }
	inline void set_l_0(List_1_t4148627898 * value)
	{
		___l_0 = value;
		Il2CppCodeGenWriteBarrier((&___l_0), value);
	}

	inline static int32_t get_offset_of_next_1() { return static_cast<int32_t>(offsetof(Enumerator_t1931539200, ___next_1)); }
	inline int32_t get_next_1() const { return ___next_1; }
	inline int32_t* get_address_of_next_1() { return &___next_1; }
	inline void set_next_1(int32_t value)
	{
		___next_1 = value;
	}

	inline static int32_t get_offset_of_ver_2() { return static_cast<int32_t>(offsetof(Enumerator_t1931539200, ___ver_2)); }
	inline int32_t get_ver_2() const { return ___ver_2; }
	inline int32_t* get_address_of_ver_2() { return &___ver_2; }
	inline void set_ver_2(int32_t value)
	{
		___ver_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_t1931539200, ___current_3)); }
	inline UILineInfo_t998511395  get_current_3() const { return ___current_3; }
	inline UILineInfo_t998511395 * get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(UILineInfo_t998511395  value)
	{
		___current_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMERATOR_T1931539200_H
#ifndef UICHARINFO_T2481152217_H
#define UICHARINFO_T2481152217_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UICharInfo
struct  UICharInfo_t2481152217 
{
public:
	// UnityEngine.Vector2 UnityEngine.UICharInfo::cursorPos
	Vector2_t3577333262  ___cursorPos_0;
	// System.Single UnityEngine.UICharInfo::charWidth
	float ___charWidth_1;

public:
	inline static int32_t get_offset_of_cursorPos_0() { return static_cast<int32_t>(offsetof(UICharInfo_t2481152217, ___cursorPos_0)); }
	inline Vector2_t3577333262  get_cursorPos_0() const { return ___cursorPos_0; }
	inline Vector2_t3577333262 * get_address_of_cursorPos_0() { return &___cursorPos_0; }
	inline void set_cursorPos_0(Vector2_t3577333262  value)
	{
		___cursorPos_0 = value;
	}

	inline static int32_t get_offset_of_charWidth_1() { return static_cast<int32_t>(offsetof(UICharInfo_t2481152217, ___charWidth_1)); }
	inline float get_charWidth_1() const { return ___charWidth_1; }
	inline float* get_address_of_charWidth_1() { return &___charWidth_1; }
	inline void set_charWidth_1(float value)
	{
		___charWidth_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UICHARINFO_T2481152217_H
#ifndef RAYCASTRESULT_T3511189758_H
#define RAYCASTRESULT_T3511189758_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.RaycastResult
struct  RaycastResult_t3511189758 
{
public:
	// UnityEngine.GameObject UnityEngine.EventSystems.RaycastResult::m_GameObject
	GameObject_t1811656094 * ___m_GameObject_0;
	// UnityEngine.EventSystems.BaseRaycaster UnityEngine.EventSystems.RaycastResult::module
	BaseRaycaster_t318802990 * ___module_1;
	// System.Single UnityEngine.EventSystems.RaycastResult::distance
	float ___distance_2;
	// System.Single UnityEngine.EventSystems.RaycastResult::index
	float ___index_3;
	// System.Int32 UnityEngine.EventSystems.RaycastResult::depth
	int32_t ___depth_4;
	// System.Int32 UnityEngine.EventSystems.RaycastResult::sortingLayer
	int32_t ___sortingLayer_5;
	// System.Int32 UnityEngine.EventSystems.RaycastResult::sortingOrder
	int32_t ___sortingOrder_6;
	// UnityEngine.Vector3 UnityEngine.EventSystems.RaycastResult::worldPosition
	Vector3_t2903530434  ___worldPosition_7;
	// UnityEngine.Vector3 UnityEngine.EventSystems.RaycastResult::worldNormal
	Vector3_t2903530434  ___worldNormal_8;
	// UnityEngine.Vector2 UnityEngine.EventSystems.RaycastResult::screenPosition
	Vector2_t3577333262  ___screenPosition_9;

public:
	inline static int32_t get_offset_of_m_GameObject_0() { return static_cast<int32_t>(offsetof(RaycastResult_t3511189758, ___m_GameObject_0)); }
	inline GameObject_t1811656094 * get_m_GameObject_0() const { return ___m_GameObject_0; }
	inline GameObject_t1811656094 ** get_address_of_m_GameObject_0() { return &___m_GameObject_0; }
	inline void set_m_GameObject_0(GameObject_t1811656094 * value)
	{
		___m_GameObject_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_GameObject_0), value);
	}

	inline static int32_t get_offset_of_module_1() { return static_cast<int32_t>(offsetof(RaycastResult_t3511189758, ___module_1)); }
	inline BaseRaycaster_t318802990 * get_module_1() const { return ___module_1; }
	inline BaseRaycaster_t318802990 ** get_address_of_module_1() { return &___module_1; }
	inline void set_module_1(BaseRaycaster_t318802990 * value)
	{
		___module_1 = value;
		Il2CppCodeGenWriteBarrier((&___module_1), value);
	}

	inline static int32_t get_offset_of_distance_2() { return static_cast<int32_t>(offsetof(RaycastResult_t3511189758, ___distance_2)); }
	inline float get_distance_2() const { return ___distance_2; }
	inline float* get_address_of_distance_2() { return &___distance_2; }
	inline void set_distance_2(float value)
	{
		___distance_2 = value;
	}

	inline static int32_t get_offset_of_index_3() { return static_cast<int32_t>(offsetof(RaycastResult_t3511189758, ___index_3)); }
	inline float get_index_3() const { return ___index_3; }
	inline float* get_address_of_index_3() { return &___index_3; }
	inline void set_index_3(float value)
	{
		___index_3 = value;
	}

	inline static int32_t get_offset_of_depth_4() { return static_cast<int32_t>(offsetof(RaycastResult_t3511189758, ___depth_4)); }
	inline int32_t get_depth_4() const { return ___depth_4; }
	inline int32_t* get_address_of_depth_4() { return &___depth_4; }
	inline void set_depth_4(int32_t value)
	{
		___depth_4 = value;
	}

	inline static int32_t get_offset_of_sortingLayer_5() { return static_cast<int32_t>(offsetof(RaycastResult_t3511189758, ___sortingLayer_5)); }
	inline int32_t get_sortingLayer_5() const { return ___sortingLayer_5; }
	inline int32_t* get_address_of_sortingLayer_5() { return &___sortingLayer_5; }
	inline void set_sortingLayer_5(int32_t value)
	{
		___sortingLayer_5 = value;
	}

	inline static int32_t get_offset_of_sortingOrder_6() { return static_cast<int32_t>(offsetof(RaycastResult_t3511189758, ___sortingOrder_6)); }
	inline int32_t get_sortingOrder_6() const { return ___sortingOrder_6; }
	inline int32_t* get_address_of_sortingOrder_6() { return &___sortingOrder_6; }
	inline void set_sortingOrder_6(int32_t value)
	{
		___sortingOrder_6 = value;
	}

	inline static int32_t get_offset_of_worldPosition_7() { return static_cast<int32_t>(offsetof(RaycastResult_t3511189758, ___worldPosition_7)); }
	inline Vector3_t2903530434  get_worldPosition_7() const { return ___worldPosition_7; }
	inline Vector3_t2903530434 * get_address_of_worldPosition_7() { return &___worldPosition_7; }
	inline void set_worldPosition_7(Vector3_t2903530434  value)
	{
		___worldPosition_7 = value;
	}

	inline static int32_t get_offset_of_worldNormal_8() { return static_cast<int32_t>(offsetof(RaycastResult_t3511189758, ___worldNormal_8)); }
	inline Vector3_t2903530434  get_worldNormal_8() const { return ___worldNormal_8; }
	inline Vector3_t2903530434 * get_address_of_worldNormal_8() { return &___worldNormal_8; }
	inline void set_worldNormal_8(Vector3_t2903530434  value)
	{
		___worldNormal_8 = value;
	}

	inline static int32_t get_offset_of_screenPosition_9() { return static_cast<int32_t>(offsetof(RaycastResult_t3511189758, ___screenPosition_9)); }
	inline Vector2_t3577333262  get_screenPosition_9() const { return ___screenPosition_9; }
	inline Vector2_t3577333262 * get_address_of_screenPosition_9() { return &___screenPosition_9; }
	inline void set_screenPosition_9(Vector2_t3577333262  value)
	{
		___screenPosition_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.EventSystems.RaycastResult
struct RaycastResult_t3511189758_marshaled_pinvoke
{
	GameObject_t1811656094 * ___m_GameObject_0;
	BaseRaycaster_t318802990 * ___module_1;
	float ___distance_2;
	float ___index_3;
	int32_t ___depth_4;
	int32_t ___sortingLayer_5;
	int32_t ___sortingOrder_6;
	Vector3_t2903530434  ___worldPosition_7;
	Vector3_t2903530434  ___worldNormal_8;
	Vector2_t3577333262  ___screenPosition_9;
};
// Native definition for COM marshalling of UnityEngine.EventSystems.RaycastResult
struct RaycastResult_t3511189758_marshaled_com
{
	GameObject_t1811656094 * ___m_GameObject_0;
	BaseRaycaster_t318802990 * ___module_1;
	float ___distance_2;
	float ___index_3;
	int32_t ___depth_4;
	int32_t ___sortingLayer_5;
	int32_t ___sortingOrder_6;
	Vector3_t2903530434  ___worldPosition_7;
	Vector3_t2903530434  ___worldNormal_8;
	Vector2_t3577333262  ___screenPosition_9;
};
#endif // RAYCASTRESULT_T3511189758_H
#ifndef KEYVALUEPAIR_2_T651451063_H
#define KEYVALUEPAIR_2_T651451063_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.KeyValuePair`2<System.IntPtr,System.Object>
struct  KeyValuePair_2_t651451063 
{
public:
	// TKey System.Collections.Generic.KeyValuePair`2::key
	IntPtr_t ___key_0;
	// TValue System.Collections.Generic.KeyValuePair`2::value
	RuntimeObject * ___value_1;

public:
	inline static int32_t get_offset_of_key_0() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t651451063, ___key_0)); }
	inline IntPtr_t get_key_0() const { return ___key_0; }
	inline IntPtr_t* get_address_of_key_0() { return &___key_0; }
	inline void set_key_0(IntPtr_t value)
	{
		___key_0 = value;
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t651451063, ___value_1)); }
	inline RuntimeObject * get_value_1() const { return ___value_1; }
	inline RuntimeObject ** get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(RuntimeObject * value)
	{
		___value_1 = value;
		Il2CppCodeGenWriteBarrier((&___value_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYVALUEPAIR_2_T651451063_H
#ifndef POSEINFO_T2257311701_H
#define POSEINFO_T2257311701_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.HoloLensExtendedTrackingManager/PoseInfo
struct  PoseInfo_t2257311701 
{
public:
	// UnityEngine.Vector3 Vuforia.HoloLensExtendedTrackingManager/PoseInfo::Position
	Vector3_t2903530434  ___Position_0;
	// UnityEngine.Quaternion Vuforia.HoloLensExtendedTrackingManager/PoseInfo::Rotation
	Quaternion_t754065749  ___Rotation_1;
	// System.Int32 Vuforia.HoloLensExtendedTrackingManager/PoseInfo::NumFramesPoseWasOff
	int32_t ___NumFramesPoseWasOff_2;

public:
	inline static int32_t get_offset_of_Position_0() { return static_cast<int32_t>(offsetof(PoseInfo_t2257311701, ___Position_0)); }
	inline Vector3_t2903530434  get_Position_0() const { return ___Position_0; }
	inline Vector3_t2903530434 * get_address_of_Position_0() { return &___Position_0; }
	inline void set_Position_0(Vector3_t2903530434  value)
	{
		___Position_0 = value;
	}

	inline static int32_t get_offset_of_Rotation_1() { return static_cast<int32_t>(offsetof(PoseInfo_t2257311701, ___Rotation_1)); }
	inline Quaternion_t754065749  get_Rotation_1() const { return ___Rotation_1; }
	inline Quaternion_t754065749 * get_address_of_Rotation_1() { return &___Rotation_1; }
	inline void set_Rotation_1(Quaternion_t754065749  value)
	{
		___Rotation_1 = value;
	}

	inline static int32_t get_offset_of_NumFramesPoseWasOff_2() { return static_cast<int32_t>(offsetof(PoseInfo_t2257311701, ___NumFramesPoseWasOff_2)); }
	inline int32_t get_NumFramesPoseWasOff_2() const { return ___NumFramesPoseWasOff_2; }
	inline int32_t* get_address_of_NumFramesPoseWasOff_2() { return &___NumFramesPoseWasOff_2; }
	inline void set_NumFramesPoseWasOff_2(int32_t value)
	{
		___NumFramesPoseWasOff_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSEINFO_T2257311701_H
#ifndef LINKEDLISTNODE_1_T1003534897_H
#define LINKEDLISTNODE_1_T1003534897_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.LinkedListNode`1<Vuforia.VuforiaManager/TrackableIdPair>
struct  LinkedListNode_1_t1003534897  : public RuntimeObject
{
public:
	// T System.Collections.Generic.LinkedListNode`1::item
	TrackableIdPair_t1331427386  ___item_0;
	// System.Collections.Generic.LinkedList`1<T> System.Collections.Generic.LinkedListNode`1::container
	LinkedList_1_t3507684622 * ___container_1;
	// System.Collections.Generic.LinkedListNode`1<T> System.Collections.Generic.LinkedListNode`1::forward
	LinkedListNode_1_t1003534897 * ___forward_2;
	// System.Collections.Generic.LinkedListNode`1<T> System.Collections.Generic.LinkedListNode`1::back
	LinkedListNode_1_t1003534897 * ___back_3;

public:
	inline static int32_t get_offset_of_item_0() { return static_cast<int32_t>(offsetof(LinkedListNode_1_t1003534897, ___item_0)); }
	inline TrackableIdPair_t1331427386  get_item_0() const { return ___item_0; }
	inline TrackableIdPair_t1331427386 * get_address_of_item_0() { return &___item_0; }
	inline void set_item_0(TrackableIdPair_t1331427386  value)
	{
		___item_0 = value;
	}

	inline static int32_t get_offset_of_container_1() { return static_cast<int32_t>(offsetof(LinkedListNode_1_t1003534897, ___container_1)); }
	inline LinkedList_1_t3507684622 * get_container_1() const { return ___container_1; }
	inline LinkedList_1_t3507684622 ** get_address_of_container_1() { return &___container_1; }
	inline void set_container_1(LinkedList_1_t3507684622 * value)
	{
		___container_1 = value;
		Il2CppCodeGenWriteBarrier((&___container_1), value);
	}

	inline static int32_t get_offset_of_forward_2() { return static_cast<int32_t>(offsetof(LinkedListNode_1_t1003534897, ___forward_2)); }
	inline LinkedListNode_1_t1003534897 * get_forward_2() const { return ___forward_2; }
	inline LinkedListNode_1_t1003534897 ** get_address_of_forward_2() { return &___forward_2; }
	inline void set_forward_2(LinkedListNode_1_t1003534897 * value)
	{
		___forward_2 = value;
		Il2CppCodeGenWriteBarrier((&___forward_2), value);
	}

	inline static int32_t get_offset_of_back_3() { return static_cast<int32_t>(offsetof(LinkedListNode_1_t1003534897, ___back_3)); }
	inline LinkedListNode_1_t1003534897 * get_back_3() const { return ___back_3; }
	inline LinkedListNode_1_t1003534897 ** get_address_of_back_3() { return &___back_3; }
	inline void set_back_3(LinkedListNode_1_t1003534897 * value)
	{
		___back_3 = value;
		Il2CppCodeGenWriteBarrier((&___back_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LINKEDLISTNODE_1_T1003534897_H
#ifndef ENUMERATOR_T1218460160_H
#define ENUMERATOR_T1218460160_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1/Enumerator<UnityEngine.Color32>
struct  Enumerator_t1218460160 
{
public:
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator::l
	List_1_t3435548858 * ___l_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::next
	int32_t ___next_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::ver
	int32_t ___ver_2;
	// T System.Collections.Generic.List`1/Enumerator::current
	Color32_t285432355  ___current_3;

public:
	inline static int32_t get_offset_of_l_0() { return static_cast<int32_t>(offsetof(Enumerator_t1218460160, ___l_0)); }
	inline List_1_t3435548858 * get_l_0() const { return ___l_0; }
	inline List_1_t3435548858 ** get_address_of_l_0() { return &___l_0; }
	inline void set_l_0(List_1_t3435548858 * value)
	{
		___l_0 = value;
		Il2CppCodeGenWriteBarrier((&___l_0), value);
	}

	inline static int32_t get_offset_of_next_1() { return static_cast<int32_t>(offsetof(Enumerator_t1218460160, ___next_1)); }
	inline int32_t get_next_1() const { return ___next_1; }
	inline int32_t* get_address_of_next_1() { return &___next_1; }
	inline void set_next_1(int32_t value)
	{
		___next_1 = value;
	}

	inline static int32_t get_offset_of_ver_2() { return static_cast<int32_t>(offsetof(Enumerator_t1218460160, ___ver_2)); }
	inline int32_t get_ver_2() const { return ___ver_2; }
	inline int32_t* get_address_of_ver_2() { return &___ver_2; }
	inline void set_ver_2(int32_t value)
	{
		___ver_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_t1218460160, ___current_3)); }
	inline Color32_t285432355  get_current_3() const { return ___current_3; }
	inline Color32_t285432355 * get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(Color32_t285432355  value)
	{
		___current_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMERATOR_T1218460160_H
#ifndef PIXEL_FORMAT_T1585627136_H
#define PIXEL_FORMAT_T1585627136_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.Image/PIXEL_FORMAT
struct  PIXEL_FORMAT_t1585627136 
{
public:
	// System.Int32 Vuforia.Image/PIXEL_FORMAT::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(PIXEL_FORMAT_t1585627136, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PIXEL_FORMAT_T1585627136_H
#ifndef ENUMERATOR_T4248893199_H
#define ENUMERATOR_T4248893199_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1/Enumerator<UnityEngine.AnimatorClipInfo>
struct  Enumerator_t4248893199 
{
public:
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator::l
	List_1_t2171014601 * ___l_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::next
	int32_t ___next_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::ver
	int32_t ___ver_2;
	// T System.Collections.Generic.List`1/Enumerator::current
	AnimatorClipInfo_t3315865394  ___current_3;

public:
	inline static int32_t get_offset_of_l_0() { return static_cast<int32_t>(offsetof(Enumerator_t4248893199, ___l_0)); }
	inline List_1_t2171014601 * get_l_0() const { return ___l_0; }
	inline List_1_t2171014601 ** get_address_of_l_0() { return &___l_0; }
	inline void set_l_0(List_1_t2171014601 * value)
	{
		___l_0 = value;
		Il2CppCodeGenWriteBarrier((&___l_0), value);
	}

	inline static int32_t get_offset_of_next_1() { return static_cast<int32_t>(offsetof(Enumerator_t4248893199, ___next_1)); }
	inline int32_t get_next_1() const { return ___next_1; }
	inline int32_t* get_address_of_next_1() { return &___next_1; }
	inline void set_next_1(int32_t value)
	{
		___next_1 = value;
	}

	inline static int32_t get_offset_of_ver_2() { return static_cast<int32_t>(offsetof(Enumerator_t4248893199, ___ver_2)); }
	inline int32_t get_ver_2() const { return ___ver_2; }
	inline int32_t* get_address_of_ver_2() { return &___ver_2; }
	inline void set_ver_2(int32_t value)
	{
		___ver_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_t4248893199, ___current_3)); }
	inline AnimatorClipInfo_t3315865394  get_current_3() const { return ___current_3; }
	inline AnimatorClipInfo_t3315865394 * get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(AnimatorClipInfo_t3315865394  value)
	{
		___current_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMERATOR_T4248893199_H
#ifndef ARGUMENTEXCEPTION_T4028401650_H
#define ARGUMENTEXCEPTION_T4028401650_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ArgumentException
struct  ArgumentException_t4028401650  : public SystemException_t870616472
{
public:
	// System.String System.ArgumentException::param_name
	String_t* ___param_name_12;

public:
	inline static int32_t get_offset_of_param_name_12() { return static_cast<int32_t>(offsetof(ArgumentException_t4028401650, ___param_name_12)); }
	inline String_t* get_param_name_12() const { return ___param_name_12; }
	inline String_t** get_address_of_param_name_12() { return &___param_name_12; }
	inline void set_param_name_12(String_t* value)
	{
		___param_name_12 = value;
		Il2CppCodeGenWriteBarrier((&___param_name_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARGUMENTEXCEPTION_T4028401650_H
#ifndef ENUMERATOR_T2583965159_H
#define ENUMERATOR_T2583965159_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1/Enumerator<System.Reflection.CustomAttributeTypedArgument>
struct  Enumerator_t2583965159 
{
public:
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator::l
	List_1_t506086561 * ___l_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::next
	int32_t ___next_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::ver
	int32_t ___ver_2;
	// T System.Collections.Generic.List`1/Enumerator::current
	CustomAttributeTypedArgument_t1650937354  ___current_3;

public:
	inline static int32_t get_offset_of_l_0() { return static_cast<int32_t>(offsetof(Enumerator_t2583965159, ___l_0)); }
	inline List_1_t506086561 * get_l_0() const { return ___l_0; }
	inline List_1_t506086561 ** get_address_of_l_0() { return &___l_0; }
	inline void set_l_0(List_1_t506086561 * value)
	{
		___l_0 = value;
		Il2CppCodeGenWriteBarrier((&___l_0), value);
	}

	inline static int32_t get_offset_of_next_1() { return static_cast<int32_t>(offsetof(Enumerator_t2583965159, ___next_1)); }
	inline int32_t get_next_1() const { return ___next_1; }
	inline int32_t* get_address_of_next_1() { return &___next_1; }
	inline void set_next_1(int32_t value)
	{
		___next_1 = value;
	}

	inline static int32_t get_offset_of_ver_2() { return static_cast<int32_t>(offsetof(Enumerator_t2583965159, ___ver_2)); }
	inline int32_t get_ver_2() const { return ___ver_2; }
	inline int32_t* get_address_of_ver_2() { return &___ver_2; }
	inline void set_ver_2(int32_t value)
	{
		___ver_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_t2583965159, ___current_3)); }
	inline CustomAttributeTypedArgument_t1650937354  get_current_3() const { return ___current_3; }
	inline CustomAttributeTypedArgument_t1650937354 * get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(CustomAttributeTypedArgument_t1650937354  value)
	{
		___current_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMERATOR_T2583965159_H
#ifndef CUSTOMATTRIBUTENAMEDARGUMENT_T468938427_H
#define CUSTOMATTRIBUTENAMEDARGUMENT_T468938427_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.CustomAttributeNamedArgument
struct  CustomAttributeNamedArgument_t468938427 
{
public:
	// System.Reflection.CustomAttributeTypedArgument System.Reflection.CustomAttributeNamedArgument::typedArgument
	CustomAttributeTypedArgument_t1650937354  ___typedArgument_0;
	// System.Reflection.MemberInfo System.Reflection.CustomAttributeNamedArgument::memberInfo
	MemberInfo_t * ___memberInfo_1;

public:
	inline static int32_t get_offset_of_typedArgument_0() { return static_cast<int32_t>(offsetof(CustomAttributeNamedArgument_t468938427, ___typedArgument_0)); }
	inline CustomAttributeTypedArgument_t1650937354  get_typedArgument_0() const { return ___typedArgument_0; }
	inline CustomAttributeTypedArgument_t1650937354 * get_address_of_typedArgument_0() { return &___typedArgument_0; }
	inline void set_typedArgument_0(CustomAttributeTypedArgument_t1650937354  value)
	{
		___typedArgument_0 = value;
	}

	inline static int32_t get_offset_of_memberInfo_1() { return static_cast<int32_t>(offsetof(CustomAttributeNamedArgument_t468938427, ___memberInfo_1)); }
	inline MemberInfo_t * get_memberInfo_1() const { return ___memberInfo_1; }
	inline MemberInfo_t ** get_address_of_memberInfo_1() { return &___memberInfo_1; }
	inline void set_memberInfo_1(MemberInfo_t * value)
	{
		___memberInfo_1 = value;
		Il2CppCodeGenWriteBarrier((&___memberInfo_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Reflection.CustomAttributeNamedArgument
struct CustomAttributeNamedArgument_t468938427_marshaled_pinvoke
{
	CustomAttributeTypedArgument_t1650937354_marshaled_pinvoke ___typedArgument_0;
	MemberInfo_t * ___memberInfo_1;
};
// Native definition for COM marshalling of System.Reflection.CustomAttributeNamedArgument
struct CustomAttributeNamedArgument_t468938427_marshaled_com
{
	CustomAttributeTypedArgument_t1650937354_marshaled_com ___typedArgument_0;
	MemberInfo_t * ___memberInfo_1;
};
#endif // CUSTOMATTRIBUTENAMEDARGUMENT_T468938427_H
#ifndef STREAMINGCONTEXTSTATES_T3429845421_H
#define STREAMINGCONTEXTSTATES_T3429845421_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.Serialization.StreamingContextStates
struct  StreamingContextStates_t3429845421 
{
public:
	// System.Int32 System.Runtime.Serialization.StreamingContextStates::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(StreamingContextStates_t3429845421, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STREAMINGCONTEXTSTATES_T3429845421_H
#ifndef RUNTIMETYPEHANDLE_T845633806_H
#define RUNTIMETYPEHANDLE_T845633806_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.RuntimeTypeHandle
struct  RuntimeTypeHandle_t845633806 
{
public:
	// System.IntPtr System.RuntimeTypeHandle::value
	IntPtr_t ___value_0;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(RuntimeTypeHandle_t845633806, ___value_0)); }
	inline IntPtr_t get_value_0() const { return ___value_0; }
	inline IntPtr_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(IntPtr_t value)
	{
		___value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMETYPEHANDLE_T845633806_H
#ifndef ENUMERATOR_T3747700150_H
#define ENUMERATOR_T3747700150_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1/Enumerator<UnityEngine.Vector4>
struct  Enumerator_t3747700150 
{
public:
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator::l
	List_1_t1669821552 * ___l_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::next
	int32_t ___next_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::ver
	int32_t ___ver_2;
	// T System.Collections.Generic.List`1/Enumerator::current
	Vector4_t2814672345  ___current_3;

public:
	inline static int32_t get_offset_of_l_0() { return static_cast<int32_t>(offsetof(Enumerator_t3747700150, ___l_0)); }
	inline List_1_t1669821552 * get_l_0() const { return ___l_0; }
	inline List_1_t1669821552 ** get_address_of_l_0() { return &___l_0; }
	inline void set_l_0(List_1_t1669821552 * value)
	{
		___l_0 = value;
		Il2CppCodeGenWriteBarrier((&___l_0), value);
	}

	inline static int32_t get_offset_of_next_1() { return static_cast<int32_t>(offsetof(Enumerator_t3747700150, ___next_1)); }
	inline int32_t get_next_1() const { return ___next_1; }
	inline int32_t* get_address_of_next_1() { return &___next_1; }
	inline void set_next_1(int32_t value)
	{
		___next_1 = value;
	}

	inline static int32_t get_offset_of_ver_2() { return static_cast<int32_t>(offsetof(Enumerator_t3747700150, ___ver_2)); }
	inline int32_t get_ver_2() const { return ___ver_2; }
	inline int32_t* get_address_of_ver_2() { return &___ver_2; }
	inline void set_ver_2(int32_t value)
	{
		___ver_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_t3747700150, ___current_3)); }
	inline Vector4_t2814672345  get_current_3() const { return ___current_3; }
	inline Vector4_t2814672345 * get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(Vector4_t2814672345  value)
	{
		___current_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMERATOR_T3747700150_H
#ifndef KEYVALUEPAIR_2_T432866562_H
#define KEYVALUEPAIR_2_T432866562_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>
struct  KeyValuePair_2_t432866562 
{
public:
	// TKey System.Collections.Generic.KeyValuePair`2::key
	int32_t ___key_0;
	// TValue System.Collections.Generic.KeyValuePair`2::value
	VirtualButtonData_t4213555505  ___value_1;

public:
	inline static int32_t get_offset_of_key_0() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t432866562, ___key_0)); }
	inline int32_t get_key_0() const { return ___key_0; }
	inline int32_t* get_address_of_key_0() { return &___key_0; }
	inline void set_key_0(int32_t value)
	{
		___key_0 = value;
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t432866562, ___value_1)); }
	inline VirtualButtonData_t4213555505  get_value_1() const { return ___value_1; }
	inline VirtualButtonData_t4213555505 * get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(VirtualButtonData_t4213555505  value)
	{
		___value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYVALUEPAIR_2_T432866562_H
#ifndef KEYVALUEPAIR_2_T3161435941_H
#define KEYVALUEPAIR_2_T3161435941_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.KeyValuePair`2<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.TrackableBehaviour/Status>
struct  KeyValuePair_2_t3161435941 
{
public:
	// TKey System.Collections.Generic.KeyValuePair`2::key
	TrackableIdPair_t1331427386  ___key_0;
	// TValue System.Collections.Generic.KeyValuePair`2::value
	int32_t ___value_1;

public:
	inline static int32_t get_offset_of_key_0() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t3161435941, ___key_0)); }
	inline TrackableIdPair_t1331427386  get_key_0() const { return ___key_0; }
	inline TrackableIdPair_t1331427386 * get_address_of_key_0() { return &___key_0; }
	inline void set_key_0(TrackableIdPair_t1331427386  value)
	{
		___key_0 = value;
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t3161435941, ___value_1)); }
	inline int32_t get_value_1() const { return ___value_1; }
	inline int32_t* get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(int32_t value)
	{
		___value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYVALUEPAIR_2_T3161435941_H
#ifndef KEYVALUEPAIR_2_T4060628773_H
#define KEYVALUEPAIR_2_T4060628773_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.KeyValuePair`2<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.HoloLensExtendedTrackingManager/PoseInfo>
struct  KeyValuePair_2_t4060628773 
{
public:
	// TKey System.Collections.Generic.KeyValuePair`2::key
	TrackableIdPair_t1331427386  ___key_0;
	// TValue System.Collections.Generic.KeyValuePair`2::value
	PoseInfo_t2257311701  ___value_1;

public:
	inline static int32_t get_offset_of_key_0() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t4060628773, ___key_0)); }
	inline TrackableIdPair_t1331427386  get_key_0() const { return ___key_0; }
	inline TrackableIdPair_t1331427386 * get_address_of_key_0() { return &___key_0; }
	inline void set_key_0(TrackableIdPair_t1331427386  value)
	{
		___key_0 = value;
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t4060628773, ___value_1)); }
	inline PoseInfo_t2257311701  get_value_1() const { return ___value_1; }
	inline PoseInfo_t2257311701 * get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(PoseInfo_t2257311701  value)
	{
		___value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYVALUEPAIR_2_T4060628773_H
#ifndef KEYVALUEPAIR_2_T603276356_H
#define KEYVALUEPAIR_2_T603276356_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.KeyValuePair`2<Vuforia.Image/PIXEL_FORMAT,System.Object>
struct  KeyValuePair_2_t603276356 
{
public:
	// TKey System.Collections.Generic.KeyValuePair`2::key
	int32_t ___key_0;
	// TValue System.Collections.Generic.KeyValuePair`2::value
	RuntimeObject * ___value_1;

public:
	inline static int32_t get_offset_of_key_0() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t603276356, ___key_0)); }
	inline int32_t get_key_0() const { return ___key_0; }
	inline int32_t* get_address_of_key_0() { return &___key_0; }
	inline void set_key_0(int32_t value)
	{
		___key_0 = value;
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t603276356, ___value_1)); }
	inline RuntimeObject * get_value_1() const { return ___value_1; }
	inline RuntimeObject ** get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(RuntimeObject * value)
	{
		___value_1 = value;
		Il2CppCodeGenWriteBarrier((&___value_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYVALUEPAIR_2_T603276356_H
#ifndef POSEAGEENTRY_T806437902_H
#define POSEAGEENTRY_T806437902_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.HoloLensExtendedTrackingManager/PoseAgeEntry
struct  PoseAgeEntry_t806437902 
{
public:
	// Vuforia.HoloLensExtendedTrackingManager/PoseInfo Vuforia.HoloLensExtendedTrackingManager/PoseAgeEntry::Pose
	PoseInfo_t2257311701  ___Pose_0;
	// Vuforia.HoloLensExtendedTrackingManager/PoseInfo Vuforia.HoloLensExtendedTrackingManager/PoseAgeEntry::CameraPose
	PoseInfo_t2257311701  ___CameraPose_1;
	// System.Int32 Vuforia.HoloLensExtendedTrackingManager/PoseAgeEntry::Age
	int32_t ___Age_2;

public:
	inline static int32_t get_offset_of_Pose_0() { return static_cast<int32_t>(offsetof(PoseAgeEntry_t806437902, ___Pose_0)); }
	inline PoseInfo_t2257311701  get_Pose_0() const { return ___Pose_0; }
	inline PoseInfo_t2257311701 * get_address_of_Pose_0() { return &___Pose_0; }
	inline void set_Pose_0(PoseInfo_t2257311701  value)
	{
		___Pose_0 = value;
	}

	inline static int32_t get_offset_of_CameraPose_1() { return static_cast<int32_t>(offsetof(PoseAgeEntry_t806437902, ___CameraPose_1)); }
	inline PoseInfo_t2257311701  get_CameraPose_1() const { return ___CameraPose_1; }
	inline PoseInfo_t2257311701 * get_address_of_CameraPose_1() { return &___CameraPose_1; }
	inline void set_CameraPose_1(PoseInfo_t2257311701  value)
	{
		___CameraPose_1 = value;
	}

	inline static int32_t get_offset_of_Age_2() { return static_cast<int32_t>(offsetof(PoseAgeEntry_t806437902, ___Age_2)); }
	inline int32_t get_Age_2() const { return ___Age_2; }
	inline int32_t* get_address_of_Age_2() { return &___Age_2; }
	inline void set_Age_2(int32_t value)
	{
		___Age_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSEAGEENTRY_T806437902_H
#ifndef TYPE_T_H
#define TYPE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Type
struct  Type_t  : public MemberInfo_t
{
public:
	// System.RuntimeTypeHandle System.Type::_impl
	RuntimeTypeHandle_t845633806  ____impl_1;

public:
	inline static int32_t get_offset_of__impl_1() { return static_cast<int32_t>(offsetof(Type_t, ____impl_1)); }
	inline RuntimeTypeHandle_t845633806  get__impl_1() const { return ____impl_1; }
	inline RuntimeTypeHandle_t845633806 * get_address_of__impl_1() { return &____impl_1; }
	inline void set__impl_1(RuntimeTypeHandle_t845633806  value)
	{
		____impl_1 = value;
	}
};

struct Type_t_StaticFields
{
public:
	// System.Char System.Type::Delimiter
	Il2CppChar ___Delimiter_2;
	// System.Type[] System.Type::EmptyTypes
	TypeU5BU5D_t3854702846* ___EmptyTypes_3;
	// System.Reflection.MemberFilter System.Type::FilterAttribute
	MemberFilter_t4208935459 * ___FilterAttribute_4;
	// System.Reflection.MemberFilter System.Type::FilterName
	MemberFilter_t4208935459 * ___FilterName_5;
	// System.Reflection.MemberFilter System.Type::FilterNameIgnoreCase
	MemberFilter_t4208935459 * ___FilterNameIgnoreCase_6;
	// System.Object System.Type::Missing
	RuntimeObject * ___Missing_7;

public:
	inline static int32_t get_offset_of_Delimiter_2() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Delimiter_2)); }
	inline Il2CppChar get_Delimiter_2() const { return ___Delimiter_2; }
	inline Il2CppChar* get_address_of_Delimiter_2() { return &___Delimiter_2; }
	inline void set_Delimiter_2(Il2CppChar value)
	{
		___Delimiter_2 = value;
	}

	inline static int32_t get_offset_of_EmptyTypes_3() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___EmptyTypes_3)); }
	inline TypeU5BU5D_t3854702846* get_EmptyTypes_3() const { return ___EmptyTypes_3; }
	inline TypeU5BU5D_t3854702846** get_address_of_EmptyTypes_3() { return &___EmptyTypes_3; }
	inline void set_EmptyTypes_3(TypeU5BU5D_t3854702846* value)
	{
		___EmptyTypes_3 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyTypes_3), value);
	}

	inline static int32_t get_offset_of_FilterAttribute_4() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterAttribute_4)); }
	inline MemberFilter_t4208935459 * get_FilterAttribute_4() const { return ___FilterAttribute_4; }
	inline MemberFilter_t4208935459 ** get_address_of_FilterAttribute_4() { return &___FilterAttribute_4; }
	inline void set_FilterAttribute_4(MemberFilter_t4208935459 * value)
	{
		___FilterAttribute_4 = value;
		Il2CppCodeGenWriteBarrier((&___FilterAttribute_4), value);
	}

	inline static int32_t get_offset_of_FilterName_5() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterName_5)); }
	inline MemberFilter_t4208935459 * get_FilterName_5() const { return ___FilterName_5; }
	inline MemberFilter_t4208935459 ** get_address_of_FilterName_5() { return &___FilterName_5; }
	inline void set_FilterName_5(MemberFilter_t4208935459 * value)
	{
		___FilterName_5 = value;
		Il2CppCodeGenWriteBarrier((&___FilterName_5), value);
	}

	inline static int32_t get_offset_of_FilterNameIgnoreCase_6() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterNameIgnoreCase_6)); }
	inline MemberFilter_t4208935459 * get_FilterNameIgnoreCase_6() const { return ___FilterNameIgnoreCase_6; }
	inline MemberFilter_t4208935459 ** get_address_of_FilterNameIgnoreCase_6() { return &___FilterNameIgnoreCase_6; }
	inline void set_FilterNameIgnoreCase_6(MemberFilter_t4208935459 * value)
	{
		___FilterNameIgnoreCase_6 = value;
		Il2CppCodeGenWriteBarrier((&___FilterNameIgnoreCase_6), value);
	}

	inline static int32_t get_offset_of_Missing_7() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Missing_7)); }
	inline RuntimeObject * get_Missing_7() const { return ___Missing_7; }
	inline RuntimeObject ** get_address_of_Missing_7() { return &___Missing_7; }
	inline void set_Missing_7(RuntimeObject * value)
	{
		___Missing_7 = value;
		Il2CppCodeGenWriteBarrier((&___Missing_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TYPE_T_H
#ifndef KEYVALUEPAIR_2_T1872397222_H
#define KEYVALUEPAIR_2_T1872397222_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.TrackableBehaviour/Status>
struct  KeyValuePair_2_t1872397222 
{
public:
	// TKey System.Collections.Generic.KeyValuePair`2::key
	int32_t ___key_0;
	// TValue System.Collections.Generic.KeyValuePair`2::value
	int32_t ___value_1;

public:
	inline static int32_t get_offset_of_key_0() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t1872397222, ___key_0)); }
	inline int32_t get_key_0() const { return ___key_0; }
	inline int32_t* get_address_of_key_0() { return &___key_0; }
	inline void set_key_0(int32_t value)
	{
		___key_0 = value;
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t1872397222, ___value_1)); }
	inline int32_t get_value_1() const { return ___value_1; }
	inline int32_t* get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(int32_t value)
	{
		___value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYVALUEPAIR_2_T1872397222_H
#ifndef ENUMERATOR_T3719495245_H
#define ENUMERATOR_T3719495245_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1/Enumerator<UnityEngine.UIVertex>
struct  Enumerator_t3719495245 
{
public:
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator::l
	List_1_t1641616647 * ___l_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::next
	int32_t ___next_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::ver
	int32_t ___ver_2;
	// T System.Collections.Generic.List`1/Enumerator::current
	UIVertex_t2786467440  ___current_3;

public:
	inline static int32_t get_offset_of_l_0() { return static_cast<int32_t>(offsetof(Enumerator_t3719495245, ___l_0)); }
	inline List_1_t1641616647 * get_l_0() const { return ___l_0; }
	inline List_1_t1641616647 ** get_address_of_l_0() { return &___l_0; }
	inline void set_l_0(List_1_t1641616647 * value)
	{
		___l_0 = value;
		Il2CppCodeGenWriteBarrier((&___l_0), value);
	}

	inline static int32_t get_offset_of_next_1() { return static_cast<int32_t>(offsetof(Enumerator_t3719495245, ___next_1)); }
	inline int32_t get_next_1() const { return ___next_1; }
	inline int32_t* get_address_of_next_1() { return &___next_1; }
	inline void set_next_1(int32_t value)
	{
		___next_1 = value;
	}

	inline static int32_t get_offset_of_ver_2() { return static_cast<int32_t>(offsetof(Enumerator_t3719495245, ___ver_2)); }
	inline int32_t get_ver_2() const { return ___ver_2; }
	inline int32_t* get_address_of_ver_2() { return &___ver_2; }
	inline void set_ver_2(int32_t value)
	{
		___ver_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_t3719495245, ___current_3)); }
	inline UIVertex_t2786467440  get_current_3() const { return ___current_3; }
	inline UIVertex_t2786467440 * get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(UIVertex_t2786467440  value)
	{
		___current_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMERATOR_T3719495245_H
#ifndef KEYVALUEPAIR_2_T2200618205_H
#define KEYVALUEPAIR_2_T2200618205_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.KeyValuePair`2<System.Object,Vuforia.WebCamProfile/ProfileData>
struct  KeyValuePair_2_t2200618205 
{
public:
	// TKey System.Collections.Generic.KeyValuePair`2::key
	RuntimeObject * ___key_0;
	// TValue System.Collections.Generic.KeyValuePair`2::value
	ProfileData_t740879429  ___value_1;

public:
	inline static int32_t get_offset_of_key_0() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t2200618205, ___key_0)); }
	inline RuntimeObject * get_key_0() const { return ___key_0; }
	inline RuntimeObject ** get_address_of_key_0() { return &___key_0; }
	inline void set_key_0(RuntimeObject * value)
	{
		___key_0 = value;
		Il2CppCodeGenWriteBarrier((&___key_0), value);
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t2200618205, ___value_1)); }
	inline ProfileData_t740879429  get_value_1() const { return ___value_1; }
	inline ProfileData_t740879429 * get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(ProfileData_t740879429  value)
	{
		___value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYVALUEPAIR_2_T2200618205_H
#ifndef ENUMERATOR_T3414180022_H
#define ENUMERATOR_T3414180022_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1/Enumerator<UnityEngine.UICharInfo>
struct  Enumerator_t3414180022 
{
public:
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator::l
	List_1_t1336301424 * ___l_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::next
	int32_t ___next_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::ver
	int32_t ___ver_2;
	// T System.Collections.Generic.List`1/Enumerator::current
	UICharInfo_t2481152217  ___current_3;

public:
	inline static int32_t get_offset_of_l_0() { return static_cast<int32_t>(offsetof(Enumerator_t3414180022, ___l_0)); }
	inline List_1_t1336301424 * get_l_0() const { return ___l_0; }
	inline List_1_t1336301424 ** get_address_of_l_0() { return &___l_0; }
	inline void set_l_0(List_1_t1336301424 * value)
	{
		___l_0 = value;
		Il2CppCodeGenWriteBarrier((&___l_0), value);
	}

	inline static int32_t get_offset_of_next_1() { return static_cast<int32_t>(offsetof(Enumerator_t3414180022, ___next_1)); }
	inline int32_t get_next_1() const { return ___next_1; }
	inline int32_t* get_address_of_next_1() { return &___next_1; }
	inline void set_next_1(int32_t value)
	{
		___next_1 = value;
	}

	inline static int32_t get_offset_of_ver_2() { return static_cast<int32_t>(offsetof(Enumerator_t3414180022, ___ver_2)); }
	inline int32_t get_ver_2() const { return ___ver_2; }
	inline int32_t* get_address_of_ver_2() { return &___ver_2; }
	inline void set_ver_2(int32_t value)
	{
		___ver_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_t3414180022, ___current_3)); }
	inline UICharInfo_t2481152217  get_current_3() const { return ___current_3; }
	inline UICharInfo_t2481152217 * get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(UICharInfo_t2481152217  value)
	{
		___current_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMERATOR_T3414180022_H
#ifndef ENUMERATOR_T149250267_H
#define ENUMERATOR_T149250267_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1/Enumerator<UnityEngine.EventSystems.RaycastResult>
struct  Enumerator_t149250267 
{
public:
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator::l
	List_1_t2366338965 * ___l_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::next
	int32_t ___next_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::ver
	int32_t ___ver_2;
	// T System.Collections.Generic.List`1/Enumerator::current
	RaycastResult_t3511189758  ___current_3;

public:
	inline static int32_t get_offset_of_l_0() { return static_cast<int32_t>(offsetof(Enumerator_t149250267, ___l_0)); }
	inline List_1_t2366338965 * get_l_0() const { return ___l_0; }
	inline List_1_t2366338965 ** get_address_of_l_0() { return &___l_0; }
	inline void set_l_0(List_1_t2366338965 * value)
	{
		___l_0 = value;
		Il2CppCodeGenWriteBarrier((&___l_0), value);
	}

	inline static int32_t get_offset_of_next_1() { return static_cast<int32_t>(offsetof(Enumerator_t149250267, ___next_1)); }
	inline int32_t get_next_1() const { return ___next_1; }
	inline int32_t* get_address_of_next_1() { return &___next_1; }
	inline void set_next_1(int32_t value)
	{
		___next_1 = value;
	}

	inline static int32_t get_offset_of_ver_2() { return static_cast<int32_t>(offsetof(Enumerator_t149250267, ___ver_2)); }
	inline int32_t get_ver_2() const { return ___ver_2; }
	inline int32_t* get_address_of_ver_2() { return &___ver_2; }
	inline void set_ver_2(int32_t value)
	{
		___ver_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_t149250267, ___current_3)); }
	inline RaycastResult_t3511189758  get_current_3() const { return ___current_3; }
	inline RaycastResult_t3511189758 * get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(RaycastResult_t3511189758  value)
	{
		___current_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMERATOR_T149250267_H
#ifndef STREAMINGCONTEXT_T4278693462_H
#define STREAMINGCONTEXT_T4278693462_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.Serialization.StreamingContext
struct  StreamingContext_t4278693462 
{
public:
	// System.Runtime.Serialization.StreamingContextStates System.Runtime.Serialization.StreamingContext::state
	int32_t ___state_0;
	// System.Object System.Runtime.Serialization.StreamingContext::additional
	RuntimeObject * ___additional_1;

public:
	inline static int32_t get_offset_of_state_0() { return static_cast<int32_t>(offsetof(StreamingContext_t4278693462, ___state_0)); }
	inline int32_t get_state_0() const { return ___state_0; }
	inline int32_t* get_address_of_state_0() { return &___state_0; }
	inline void set_state_0(int32_t value)
	{
		___state_0 = value;
	}

	inline static int32_t get_offset_of_additional_1() { return static_cast<int32_t>(offsetof(StreamingContext_t4278693462, ___additional_1)); }
	inline RuntimeObject * get_additional_1() const { return ___additional_1; }
	inline RuntimeObject ** get_address_of_additional_1() { return &___additional_1; }
	inline void set_additional_1(RuntimeObject * value)
	{
		___additional_1 = value;
		Il2CppCodeGenWriteBarrier((&___additional_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Runtime.Serialization.StreamingContext
struct StreamingContext_t4278693462_marshaled_pinvoke
{
	int32_t ___state_0;
	Il2CppIUnknown* ___additional_1;
};
// Native definition for COM marshalling of System.Runtime.Serialization.StreamingContext
struct StreamingContext_t4278693462_marshaled_com
{
	int32_t ___state_0;
	Il2CppIUnknown* ___additional_1;
};
#endif // STREAMINGCONTEXT_T4278693462_H
#ifndef ARGUMENTNULLEXCEPTION_T2943536221_H
#define ARGUMENTNULLEXCEPTION_T2943536221_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ArgumentNullException
struct  ArgumentNullException_t2943536221  : public ArgumentException_t4028401650
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARGUMENTNULLEXCEPTION_T2943536221_H
#ifndef ARGUMENTOUTOFRANGEEXCEPTION_T2174922486_H
#define ARGUMENTOUTOFRANGEEXCEPTION_T2174922486_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ArgumentOutOfRangeException
struct  ArgumentOutOfRangeException_t2174922486  : public ArgumentException_t4028401650
{
public:
	// System.Object System.ArgumentOutOfRangeException::actual_value
	RuntimeObject * ___actual_value_13;

public:
	inline static int32_t get_offset_of_actual_value_13() { return static_cast<int32_t>(offsetof(ArgumentOutOfRangeException_t2174922486, ___actual_value_13)); }
	inline RuntimeObject * get_actual_value_13() const { return ___actual_value_13; }
	inline RuntimeObject ** get_address_of_actual_value_13() { return &___actual_value_13; }
	inline void set_actual_value_13(RuntimeObject * value)
	{
		___actual_value_13 = value;
		Il2CppCodeGenWriteBarrier((&___actual_value_13), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARGUMENTOUTOFRANGEEXCEPTION_T2174922486_H
#ifndef ENUMERATOR_T1401966232_H
#define ENUMERATOR_T1401966232_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1/Enumerator<System.Reflection.CustomAttributeNamedArgument>
struct  Enumerator_t1401966232 
{
public:
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator::l
	List_1_t3619054930 * ___l_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::next
	int32_t ___next_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::ver
	int32_t ___ver_2;
	// T System.Collections.Generic.List`1/Enumerator::current
	CustomAttributeNamedArgument_t468938427  ___current_3;

public:
	inline static int32_t get_offset_of_l_0() { return static_cast<int32_t>(offsetof(Enumerator_t1401966232, ___l_0)); }
	inline List_1_t3619054930 * get_l_0() const { return ___l_0; }
	inline List_1_t3619054930 ** get_address_of_l_0() { return &___l_0; }
	inline void set_l_0(List_1_t3619054930 * value)
	{
		___l_0 = value;
		Il2CppCodeGenWriteBarrier((&___l_0), value);
	}

	inline static int32_t get_offset_of_next_1() { return static_cast<int32_t>(offsetof(Enumerator_t1401966232, ___next_1)); }
	inline int32_t get_next_1() const { return ___next_1; }
	inline int32_t* get_address_of_next_1() { return &___next_1; }
	inline void set_next_1(int32_t value)
	{
		___next_1 = value;
	}

	inline static int32_t get_offset_of_ver_2() { return static_cast<int32_t>(offsetof(Enumerator_t1401966232, ___ver_2)); }
	inline int32_t get_ver_2() const { return ___ver_2; }
	inline int32_t* get_address_of_ver_2() { return &___ver_2; }
	inline void set_ver_2(int32_t value)
	{
		___ver_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_t1401966232, ___current_3)); }
	inline CustomAttributeNamedArgument_t468938427  get_current_3() const { return ___current_3; }
	inline CustomAttributeNamedArgument_t468938427 * get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(CustomAttributeNamedArgument_t468938427  value)
	{
		___current_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMERATOR_T1401966232_H
#ifndef OBJECTDISPOSEDEXCEPTION_T2553224159_H
#define OBJECTDISPOSEDEXCEPTION_T2553224159_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ObjectDisposedException
struct  ObjectDisposedException_t2553224159  : public InvalidOperationException_t2321794232
{
public:
	// System.String System.ObjectDisposedException::obj_name
	String_t* ___obj_name_12;
	// System.String System.ObjectDisposedException::msg
	String_t* ___msg_13;

public:
	inline static int32_t get_offset_of_obj_name_12() { return static_cast<int32_t>(offsetof(ObjectDisposedException_t2553224159, ___obj_name_12)); }
	inline String_t* get_obj_name_12() const { return ___obj_name_12; }
	inline String_t** get_address_of_obj_name_12() { return &___obj_name_12; }
	inline void set_obj_name_12(String_t* value)
	{
		___obj_name_12 = value;
		Il2CppCodeGenWriteBarrier((&___obj_name_12), value);
	}

	inline static int32_t get_offset_of_msg_13() { return static_cast<int32_t>(offsetof(ObjectDisposedException_t2553224159, ___msg_13)); }
	inline String_t* get_msg_13() const { return ___msg_13; }
	inline String_t** get_address_of_msg_13() { return &___msg_13; }
	inline void set_msg_13(String_t* value)
	{
		___msg_13 = value;
		Il2CppCodeGenWriteBarrier((&___msg_13), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OBJECTDISPOSEDEXCEPTION_T2553224159_H
#ifndef KEYVALUEPAIR_2_T2609754974_H
#define KEYVALUEPAIR_2_T2609754974_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.KeyValuePair`2<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.HoloLensExtendedTrackingManager/PoseAgeEntry>
struct  KeyValuePair_2_t2609754974 
{
public:
	// TKey System.Collections.Generic.KeyValuePair`2::key
	TrackableIdPair_t1331427386  ___key_0;
	// TValue System.Collections.Generic.KeyValuePair`2::value
	PoseAgeEntry_t806437902  ___value_1;

public:
	inline static int32_t get_offset_of_key_0() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t2609754974, ___key_0)); }
	inline TrackableIdPair_t1331427386  get_key_0() const { return ___key_0; }
	inline TrackableIdPair_t1331427386 * get_address_of_key_0() { return &___key_0; }
	inline void set_key_0(TrackableIdPair_t1331427386  value)
	{
		___key_0 = value;
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t2609754974, ___value_1)); }
	inline PoseAgeEntry_t806437902  get_value_1() const { return ___value_1; }
	inline PoseAgeEntry_t806437902 * get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(PoseAgeEntry_t806437902  value)
	{
		___value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYVALUEPAIR_2_T2609754974_H
// System.String[]
struct StringU5BU5D_t2298632947  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) String_t* m_Items[1];

public:
	inline String_t* GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline String_t** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, String_t* value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline String_t* GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline String_t** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, String_t* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Object[]
struct ObjectU5BU5D_t3270211303  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) RuntimeObject * m_Items[1];

public:
	inline RuntimeObject * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline RuntimeObject ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, RuntimeObject * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline RuntimeObject * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline RuntimeObject ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, RuntimeObject * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// Vuforia.VuforiaManager/TrackableIdPair[]
struct TrackableIdPairU5BU5D_t3613789599  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) TrackableIdPair_t1331427386  m_Items[1];

public:
	inline TrackableIdPair_t1331427386  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline TrackableIdPair_t1331427386 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, TrackableIdPair_t1331427386  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline TrackableIdPair_t1331427386  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline TrackableIdPair_t1331427386 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, TrackableIdPair_t1331427386  value)
	{
		m_Items[index] = value;
	}
};
// System.Int32[]
struct Int32U5BU5D_t2324750880  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) int32_t m_Items[1];

public:
	inline int32_t GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline int32_t* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, int32_t value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline int32_t GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline int32_t* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, int32_t value)
	{
		m_Items[index] = value;
	}
};
// System.Reflection.CustomAttributeNamedArgument[]
struct CustomAttributeNamedArgumentU5BU5D_t2347765754  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) CustomAttributeNamedArgument_t468938427  m_Items[1];

public:
	inline CustomAttributeNamedArgument_t468938427  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline CustomAttributeNamedArgument_t468938427 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, CustomAttributeNamedArgument_t468938427  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline CustomAttributeNamedArgument_t468938427  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline CustomAttributeNamedArgument_t468938427 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, CustomAttributeNamedArgument_t468938427  value)
	{
		m_Items[index] = value;
	}
};
// System.Reflection.CustomAttributeTypedArgument[]
struct CustomAttributeTypedArgumentU5BU5D_t3682997903  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) CustomAttributeTypedArgument_t1650937354  m_Items[1];

public:
	inline CustomAttributeTypedArgument_t1650937354  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline CustomAttributeTypedArgument_t1650937354 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, CustomAttributeTypedArgument_t1650937354  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline CustomAttributeTypedArgument_t1650937354  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline CustomAttributeTypedArgument_t1650937354 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, CustomAttributeTypedArgument_t1650937354  value)
	{
		m_Items[index] = value;
	}
};
// UnityEngine.AnimatorClipInfo[]
struct AnimatorClipInfoU5BU5D_t148995015  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) AnimatorClipInfo_t3315865394  m_Items[1];

public:
	inline AnimatorClipInfo_t3315865394  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline AnimatorClipInfo_t3315865394 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, AnimatorClipInfo_t3315865394  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline AnimatorClipInfo_t3315865394  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline AnimatorClipInfo_t3315865394 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, AnimatorClipInfo_t3315865394  value)
	{
		m_Items[index] = value;
	}
};
// UnityEngine.Color32[]
struct Color32U5BU5D_t2266382834  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Color32_t285432355  m_Items[1];

public:
	inline Color32_t285432355  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Color32_t285432355 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Color32_t285432355  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline Color32_t285432355  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Color32_t285432355 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Color32_t285432355  value)
	{
		m_Items[index] = value;
	}
};
// UnityEngine.EventSystems.RaycastResult[]
struct RaycastResultU5BU5D_t2863863115  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) RaycastResult_t3511189758  m_Items[1];

public:
	inline RaycastResult_t3511189758  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline RaycastResult_t3511189758 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, RaycastResult_t3511189758  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline RaycastResult_t3511189758  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline RaycastResult_t3511189758 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, RaycastResult_t3511189758  value)
	{
		m_Items[index] = value;
	}
};
// UnityEngine.UICharInfo[]
struct UICharInfoU5BU5D_t3065457572  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) UICharInfo_t2481152217  m_Items[1];

public:
	inline UICharInfo_t2481152217  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline UICharInfo_t2481152217 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, UICharInfo_t2481152217  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline UICharInfo_t2481152217  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline UICharInfo_t2481152217 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, UICharInfo_t2481152217  value)
	{
		m_Items[index] = value;
	}
};
// UnityEngine.UILineInfo[]
struct UILineInfoU5BU5D_t38175986  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) UILineInfo_t998511395  m_Items[1];

public:
	inline UILineInfo_t998511395  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline UILineInfo_t998511395 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, UILineInfo_t998511395  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline UILineInfo_t998511395  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline UILineInfo_t998511395 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, UILineInfo_t998511395  value)
	{
		m_Items[index] = value;
	}
};
// UnityEngine.UIVertex[]
struct UIVertexU5BU5D_t227469777  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) UIVertex_t2786467440  m_Items[1];

public:
	inline UIVertex_t2786467440  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline UIVertex_t2786467440 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, UIVertex_t2786467440  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline UIVertex_t2786467440  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline UIVertex_t2786467440 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, UIVertex_t2786467440  value)
	{
		m_Items[index] = value;
	}
};
// UnityEngine.Vector2[]
struct Vector2U5BU5D_t3916253691  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Vector2_t3577333262  m_Items[1];

public:
	inline Vector2_t3577333262  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Vector2_t3577333262 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Vector2_t3577333262  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline Vector2_t3577333262  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Vector2_t3577333262 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Vector2_t3577333262  value)
	{
		m_Items[index] = value;
	}
};
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t981680887  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Vector3_t2903530434  m_Items[1];

public:
	inline Vector3_t2903530434  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Vector3_t2903530434 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Vector3_t2903530434  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline Vector3_t2903530434  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Vector3_t2903530434 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Vector3_t2903530434  value)
	{
		m_Items[index] = value;
	}
};
// UnityEngine.Vector4[]
struct Vector4U5BU5D_t2124741284  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Vector4_t2814672345  m_Items[1];

public:
	inline Vector4_t2814672345  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Vector4_t2814672345 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Vector4_t2814672345  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline Vector4_t2814672345  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Vector4_t2814672345 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Vector4_t2814672345  value)
	{
		m_Items[index] = value;
	}
};


// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>::set_Key(TKey)
extern "C"  void KeyValuePair_2_set_Key_m3512996173_gshared (KeyValuePair_2_t2536236643 * __this, int32_t p0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>::set_Value(TValue)
extern "C"  void KeyValuePair_2_set_Value_m3337772920_gshared (KeyValuePair_2_t2536236643 * __this, RuntimeObject * p0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m3779811036_gshared (KeyValuePair_2_t2536236643 * __this, int32_t ___key0, RuntimeObject * ___value1, const RuntimeMethod* method);
// TKey System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>::get_Key()
extern "C"  int32_t KeyValuePair_2_get_Key_m15997923_gshared (KeyValuePair_2_t2536236643 * __this, const RuntimeMethod* method);
// TValue System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>::get_Value()
extern "C"  RuntimeObject * KeyValuePair_2_get_Value_m699117648_gshared (KeyValuePair_2_t2536236643 * __this, const RuntimeMethod* method);
// System.String System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>::ToString()
extern "C"  String_t* KeyValuePair_2_ToString_m3144124502_gshared (KeyValuePair_2_t2536236643 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.TrackableBehaviour/Status>::set_Key(TKey)
extern "C"  void KeyValuePair_2_set_Key_m34282199_gshared (KeyValuePair_2_t1872397222 * __this, int32_t p0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.TrackableBehaviour/Status>::set_Value(TValue)
extern "C"  void KeyValuePair_2_set_Value_m1414687358_gshared (KeyValuePair_2_t1872397222 * __this, int32_t p0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.TrackableBehaviour/Status>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m3381747994_gshared (KeyValuePair_2_t1872397222 * __this, int32_t ___key0, int32_t ___value1, const RuntimeMethod* method);
// TKey System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.TrackableBehaviour/Status>::get_Key()
extern "C"  int32_t KeyValuePair_2_get_Key_m2266079803_gshared (KeyValuePair_2_t1872397222 * __this, const RuntimeMethod* method);
// TValue System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.TrackableBehaviour/Status>::get_Value()
extern "C"  int32_t KeyValuePair_2_get_Value_m2289943185_gshared (KeyValuePair_2_t1872397222 * __this, const RuntimeMethod* method);
// System.String System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.TrackableBehaviour/Status>::ToString()
extern "C"  String_t* KeyValuePair_2_ToString_m3380882685_gshared (KeyValuePair_2_t1872397222 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::set_Key(TKey)
extern "C"  void KeyValuePair_2_set_Key_m979245165_gshared (KeyValuePair_2_t432866562 * __this, int32_t p0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::set_Value(TValue)
extern "C"  void KeyValuePair_2_set_Value_m3827392229_gshared (KeyValuePair_2_t432866562 * __this, VirtualButtonData_t4213555505  p0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m113285641_gshared (KeyValuePair_2_t432866562 * __this, int32_t ___key0, VirtualButtonData_t4213555505  ___value1, const RuntimeMethod* method);
// TKey System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::get_Key()
extern "C"  int32_t KeyValuePair_2_get_Key_m1292857486_gshared (KeyValuePair_2_t432866562 * __this, const RuntimeMethod* method);
// TValue System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::get_Value()
extern "C"  VirtualButtonData_t4213555505  KeyValuePair_2_get_Value_m3345055990_gshared (KeyValuePair_2_t432866562 * __this, const RuntimeMethod* method);
// System.String System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::ToString()
extern "C"  String_t* KeyValuePair_2_ToString_m3666435602_gshared (KeyValuePair_2_t432866562 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.KeyValuePair`2<System.IntPtr,System.Object>::set_Key(TKey)
extern "C"  void KeyValuePair_2_set_Key_m2055790825_gshared (KeyValuePair_2_t651451063 * __this, IntPtr_t p0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.KeyValuePair`2<System.IntPtr,System.Object>::set_Value(TValue)
extern "C"  void KeyValuePair_2_set_Value_m1654871410_gshared (KeyValuePair_2_t651451063 * __this, RuntimeObject * p0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.KeyValuePair`2<System.IntPtr,System.Object>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m2085269269_gshared (KeyValuePair_2_t651451063 * __this, IntPtr_t ___key0, RuntimeObject * ___value1, const RuntimeMethod* method);
// TKey System.Collections.Generic.KeyValuePair`2<System.IntPtr,System.Object>::get_Key()
extern "C"  IntPtr_t KeyValuePair_2_get_Key_m2522224863_gshared (KeyValuePair_2_t651451063 * __this, const RuntimeMethod* method);
// TValue System.Collections.Generic.KeyValuePair`2<System.IntPtr,System.Object>::get_Value()
extern "C"  RuntimeObject * KeyValuePair_2_get_Value_m3322101809_gshared (KeyValuePair_2_t651451063 * __this, const RuntimeMethod* method);
// System.String System.Collections.Generic.KeyValuePair`2<System.IntPtr,System.Object>::ToString()
extern "C"  String_t* KeyValuePair_2_ToString_m1274882003_gshared (KeyValuePair_2_t651451063 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>::set_Key(TKey)
extern "C"  void KeyValuePair_2_set_Key_m717171400_gshared (KeyValuePair_2_t115616549 * __this, RuntimeObject * p0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>::set_Value(TValue)
extern "C"  void KeyValuePair_2_set_Value_m781244180_gshared (KeyValuePair_2_t115616549 * __this, bool p0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m2919652111_gshared (KeyValuePair_2_t115616549 * __this, RuntimeObject * ___key0, bool ___value1, const RuntimeMethod* method);
// TKey System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>::get_Key()
extern "C"  RuntimeObject * KeyValuePair_2_get_Key_m3969882595_gshared (KeyValuePair_2_t115616549 * __this, const RuntimeMethod* method);
// TValue System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>::get_Value()
extern "C"  bool KeyValuePair_2_get_Value_m4103818512_gshared (KeyValuePair_2_t115616549 * __this, const RuntimeMethod* method);
// System.String System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>::ToString()
extern "C"  String_t* KeyValuePair_2_ToString_m3427190393_gshared (KeyValuePair_2_t115616549 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>::set_Key(TKey)
extern "C"  void KeyValuePair_2_set_Key_m3774274141_gshared (KeyValuePair_2_t1402716069 * __this, RuntimeObject * p0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>::set_Value(TValue)
extern "C"  void KeyValuePair_2_set_Value_m541298045_gshared (KeyValuePair_2_t1402716069 * __this, int32_t p0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m388328491_gshared (KeyValuePair_2_t1402716069 * __this, RuntimeObject * ___key0, int32_t ___value1, const RuntimeMethod* method);
// TKey System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>::get_Key()
extern "C"  RuntimeObject * KeyValuePair_2_get_Key_m1087122466_gshared (KeyValuePair_2_t1402716069 * __this, const RuntimeMethod* method);
// TValue System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>::get_Value()
extern "C"  int32_t KeyValuePair_2_get_Value_m2458189361_gshared (KeyValuePair_2_t1402716069 * __this, const RuntimeMethod* method);
// System.String System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>::ToString()
extern "C"  String_t* KeyValuePair_2_ToString_m729476552_gshared (KeyValuePair_2_t1402716069 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>::set_Key(TKey)
extern "C"  void KeyValuePair_2_set_Key_m3122105053_gshared (KeyValuePair_2_t3481697066 * __this, RuntimeObject * p0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>::set_Value(TValue)
extern "C"  void KeyValuePair_2_set_Value_m2394843442_gshared (KeyValuePair_2_t3481697066 * __this, RuntimeObject * p0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m973662155_gshared (KeyValuePair_2_t3481697066 * __this, RuntimeObject * ___key0, RuntimeObject * ___value1, const RuntimeMethod* method);
// TKey System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>::get_Key()
extern "C"  RuntimeObject * KeyValuePair_2_get_Key_m4217124106_gshared (KeyValuePair_2_t3481697066 * __this, const RuntimeMethod* method);
// TValue System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>::get_Value()
extern "C"  RuntimeObject * KeyValuePair_2_get_Value_m1734077897_gshared (KeyValuePair_2_t3481697066 * __this, const RuntimeMethod* method);
// System.String System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>::ToString()
extern "C"  String_t* KeyValuePair_2_ToString_m3779009088_gshared (KeyValuePair_2_t3481697066 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt16>::set_Key(TKey)
extern "C"  void KeyValuePair_2_set_Key_m1444094661_gshared (KeyValuePair_2_t574427458 * __this, RuntimeObject * p0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt16>::set_Value(TValue)
extern "C"  void KeyValuePair_2_set_Value_m655597244_gshared (KeyValuePair_2_t574427458 * __this, uint16_t p0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt16>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m1511411907_gshared (KeyValuePair_2_t574427458 * __this, RuntimeObject * ___key0, uint16_t ___value1, const RuntimeMethod* method);
// TKey System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt16>::get_Key()
extern "C"  RuntimeObject * KeyValuePair_2_get_Key_m2791227378_gshared (KeyValuePair_2_t574427458 * __this, const RuntimeMethod* method);
// TValue System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt16>::get_Value()
extern "C"  uint16_t KeyValuePair_2_get_Value_m2138563648_gshared (KeyValuePair_2_t574427458 * __this, const RuntimeMethod* method);
// System.String System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt16>::ToString()
extern "C"  String_t* KeyValuePair_2_ToString_m1879012905_gshared (KeyValuePair_2_t574427458 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,Vuforia.WebCamProfile/ProfileData>::set_Key(TKey)
extern "C"  void KeyValuePair_2_set_Key_m2963078532_gshared (KeyValuePair_2_t2200618205 * __this, RuntimeObject * p0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,Vuforia.WebCamProfile/ProfileData>::set_Value(TValue)
extern "C"  void KeyValuePair_2_set_Value_m3496892223_gshared (KeyValuePair_2_t2200618205 * __this, ProfileData_t740879429  p0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,Vuforia.WebCamProfile/ProfileData>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m3327461017_gshared (KeyValuePair_2_t2200618205 * __this, RuntimeObject * ___key0, ProfileData_t740879429  ___value1, const RuntimeMethod* method);
// TKey System.Collections.Generic.KeyValuePair`2<System.Object,Vuforia.WebCamProfile/ProfileData>::get_Key()
extern "C"  RuntimeObject * KeyValuePair_2_get_Key_m956489385_gshared (KeyValuePair_2_t2200618205 * __this, const RuntimeMethod* method);
// TValue System.Collections.Generic.KeyValuePair`2<System.Object,Vuforia.WebCamProfile/ProfileData>::get_Value()
extern "C"  ProfileData_t740879429  KeyValuePair_2_get_Value_m1073695936_gshared (KeyValuePair_2_t2200618205 * __this, const RuntimeMethod* method);
// System.String System.Collections.Generic.KeyValuePair`2<System.Object,Vuforia.WebCamProfile/ProfileData>::ToString()
extern "C"  String_t* KeyValuePair_2_ToString_m4250712101_gshared (KeyValuePair_2_t2200618205 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.KeyValuePair`2<Vuforia.Image/PIXEL_FORMAT,System.Object>::set_Key(TKey)
extern "C"  void KeyValuePair_2_set_Key_m1462435752_gshared (KeyValuePair_2_t603276356 * __this, int32_t p0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.KeyValuePair`2<Vuforia.Image/PIXEL_FORMAT,System.Object>::set_Value(TValue)
extern "C"  void KeyValuePair_2_set_Value_m3866220215_gshared (KeyValuePair_2_t603276356 * __this, RuntimeObject * p0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.KeyValuePair`2<Vuforia.Image/PIXEL_FORMAT,System.Object>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m2367532738_gshared (KeyValuePair_2_t603276356 * __this, int32_t ___key0, RuntimeObject * ___value1, const RuntimeMethod* method);
// TKey System.Collections.Generic.KeyValuePair`2<Vuforia.Image/PIXEL_FORMAT,System.Object>::get_Key()
extern "C"  int32_t KeyValuePair_2_get_Key_m3814775675_gshared (KeyValuePair_2_t603276356 * __this, const RuntimeMethod* method);
// TValue System.Collections.Generic.KeyValuePair`2<Vuforia.Image/PIXEL_FORMAT,System.Object>::get_Value()
extern "C"  RuntimeObject * KeyValuePair_2_get_Value_m1253090796_gshared (KeyValuePair_2_t603276356 * __this, const RuntimeMethod* method);
// System.String System.Collections.Generic.KeyValuePair`2<Vuforia.Image/PIXEL_FORMAT,System.Object>::ToString()
extern "C"  String_t* KeyValuePair_2_ToString_m2759420978_gshared (KeyValuePair_2_t603276356 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.KeyValuePair`2<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.HoloLensExtendedTrackingManager/PoseAgeEntry>::set_Key(TKey)
extern "C"  void KeyValuePair_2_set_Key_m3192279472_gshared (KeyValuePair_2_t2609754974 * __this, TrackableIdPair_t1331427386  p0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.KeyValuePair`2<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.HoloLensExtendedTrackingManager/PoseAgeEntry>::set_Value(TValue)
extern "C"  void KeyValuePair_2_set_Value_m3595070817_gshared (KeyValuePair_2_t2609754974 * __this, PoseAgeEntry_t806437902  p0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.KeyValuePair`2<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.HoloLensExtendedTrackingManager/PoseAgeEntry>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m820146208_gshared (KeyValuePair_2_t2609754974 * __this, TrackableIdPair_t1331427386  ___key0, PoseAgeEntry_t806437902  ___value1, const RuntimeMethod* method);
// TKey System.Collections.Generic.KeyValuePair`2<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.HoloLensExtendedTrackingManager/PoseAgeEntry>::get_Key()
extern "C"  TrackableIdPair_t1331427386  KeyValuePair_2_get_Key_m3017190976_gshared (KeyValuePair_2_t2609754974 * __this, const RuntimeMethod* method);
// TValue System.Collections.Generic.KeyValuePair`2<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.HoloLensExtendedTrackingManager/PoseAgeEntry>::get_Value()
extern "C"  PoseAgeEntry_t806437902  KeyValuePair_2_get_Value_m3794373347_gshared (KeyValuePair_2_t2609754974 * __this, const RuntimeMethod* method);
// System.String System.Collections.Generic.KeyValuePair`2<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.HoloLensExtendedTrackingManager/PoseAgeEntry>::ToString()
extern "C"  String_t* KeyValuePair_2_ToString_m3066038920_gshared (KeyValuePair_2_t2609754974 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.KeyValuePair`2<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.HoloLensExtendedTrackingManager/PoseInfo>::set_Key(TKey)
extern "C"  void KeyValuePair_2_set_Key_m3132306086_gshared (KeyValuePair_2_t4060628773 * __this, TrackableIdPair_t1331427386  p0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.KeyValuePair`2<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.HoloLensExtendedTrackingManager/PoseInfo>::set_Value(TValue)
extern "C"  void KeyValuePair_2_set_Value_m3890858544_gshared (KeyValuePair_2_t4060628773 * __this, PoseInfo_t2257311701  p0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.KeyValuePair`2<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.HoloLensExtendedTrackingManager/PoseInfo>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m3588891793_gshared (KeyValuePair_2_t4060628773 * __this, TrackableIdPair_t1331427386  ___key0, PoseInfo_t2257311701  ___value1, const RuntimeMethod* method);
// TKey System.Collections.Generic.KeyValuePair`2<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.HoloLensExtendedTrackingManager/PoseInfo>::get_Key()
extern "C"  TrackableIdPair_t1331427386  KeyValuePair_2_get_Key_m4245299421_gshared (KeyValuePair_2_t4060628773 * __this, const RuntimeMethod* method);
// TValue System.Collections.Generic.KeyValuePair`2<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.HoloLensExtendedTrackingManager/PoseInfo>::get_Value()
extern "C"  PoseInfo_t2257311701  KeyValuePair_2_get_Value_m3742287316_gshared (KeyValuePair_2_t4060628773 * __this, const RuntimeMethod* method);
// System.String System.Collections.Generic.KeyValuePair`2<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.HoloLensExtendedTrackingManager/PoseInfo>::ToString()
extern "C"  String_t* KeyValuePair_2_ToString_m2795500392_gshared (KeyValuePair_2_t4060628773 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.KeyValuePair`2<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.TrackableBehaviour/Status>::set_Key(TKey)
extern "C"  void KeyValuePair_2_set_Key_m2719103795_gshared (KeyValuePair_2_t3161435941 * __this, TrackableIdPair_t1331427386  p0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.KeyValuePair`2<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.TrackableBehaviour/Status>::set_Value(TValue)
extern "C"  void KeyValuePair_2_set_Value_m1544936261_gshared (KeyValuePair_2_t3161435941 * __this, int32_t p0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.KeyValuePair`2<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.TrackableBehaviour/Status>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m3910118697_gshared (KeyValuePair_2_t3161435941 * __this, TrackableIdPair_t1331427386  ___key0, int32_t ___value1, const RuntimeMethod* method);
// TKey System.Collections.Generic.KeyValuePair`2<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.TrackableBehaviour/Status>::get_Key()
extern "C"  TrackableIdPair_t1331427386  KeyValuePair_2_get_Key_m1540520745_gshared (KeyValuePair_2_t3161435941 * __this, const RuntimeMethod* method);
// TValue System.Collections.Generic.KeyValuePair`2<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.TrackableBehaviour/Status>::get_Value()
extern "C"  int32_t KeyValuePair_2_get_Value_m3827826732_gshared (KeyValuePair_2_t3161435941 * __this, const RuntimeMethod* method);
// System.String System.Collections.Generic.KeyValuePair`2<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.TrackableBehaviour/Status>::ToString()
extern "C"  String_t* KeyValuePair_2_ToString_m1074719178_gshared (KeyValuePair_2_t3161435941 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.LinkedList`1/Enumerator<System.Object>::.ctor(System.Collections.Generic.LinkedList`1<T>)
extern "C"  void Enumerator__ctor_m3993907956_gshared (Enumerator_t1983447509 * __this, LinkedList_1_t4198215526 * ___parent0, const RuntimeMethod* method);
// T System.Collections.Generic.LinkedList`1/Enumerator<System.Object>::get_Current()
extern "C"  RuntimeObject * Enumerator_get_Current_m3886014715_gshared (Enumerator_t1983447509 * __this, const RuntimeMethod* method);
// System.Object System.Collections.Generic.LinkedList`1/Enumerator<System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  RuntimeObject * Enumerator_System_Collections_IEnumerator_get_Current_m99794827_gshared (Enumerator_t1983447509 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.LinkedList`1/Enumerator<System.Object>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m2687040866_gshared (Enumerator_t1983447509 * __this, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.LinkedList`1/Enumerator<System.Object>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m893837046_gshared (Enumerator_t1983447509 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.LinkedList`1/Enumerator<System.Object>::Dispose()
extern "C"  void Enumerator_Dispose_m3834463819_gshared (Enumerator_t1983447509 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.LinkedList`1/Enumerator<Vuforia.VuforiaManager/TrackableIdPair>::.ctor(System.Collections.Generic.LinkedList`1<T>)
extern "C"  void Enumerator__ctor_m2514524852_gshared (Enumerator_t1292916605 * __this, LinkedList_1_t3507684622 * ___parent0, const RuntimeMethod* method);
// T System.Collections.Generic.LinkedList`1/Enumerator<Vuforia.VuforiaManager/TrackableIdPair>::get_Current()
extern "C"  TrackableIdPair_t1331427386  Enumerator_get_Current_m1428289307_gshared (Enumerator_t1292916605 * __this, const RuntimeMethod* method);
// System.Object System.Collections.Generic.LinkedList`1/Enumerator<Vuforia.VuforiaManager/TrackableIdPair>::System.Collections.IEnumerator.get_Current()
extern "C"  RuntimeObject * Enumerator_System_Collections_IEnumerator_get_Current_m1186992793_gshared (Enumerator_t1292916605 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.LinkedList`1/Enumerator<Vuforia.VuforiaManager/TrackableIdPair>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m986577418_gshared (Enumerator_t1292916605 * __this, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.LinkedList`1/Enumerator<Vuforia.VuforiaManager/TrackableIdPair>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m562864630_gshared (Enumerator_t1292916605 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.LinkedList`1/Enumerator<Vuforia.VuforiaManager/TrackableIdPair>::Dispose()
extern "C"  void Enumerator_Dispose_m3863799224_gshared (Enumerator_t1292916605 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1/Enumerator<System.Int32>::.ctor(System.Collections.Generic.List`1<T>)
extern "C"  void Enumerator__ctor_m2268946854_gshared (Enumerator_t876005098 * __this, List_1_t3093093796 * ___l0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1/Enumerator<System.Int32>::VerifyState()
extern "C"  void Enumerator_VerifyState_m2157842137_gshared (Enumerator_t876005098 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1/Enumerator<System.Int32>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m748325870_gshared (Enumerator_t876005098 * __this, const RuntimeMethod* method);
// System.Object System.Collections.Generic.List`1/Enumerator<System.Int32>::System.Collections.IEnumerator.get_Current()
extern "C"  RuntimeObject * Enumerator_System_Collections_IEnumerator_get_Current_m642494324_gshared (Enumerator_t876005098 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1/Enumerator<System.Int32>::Dispose()
extern "C"  void Enumerator_Dispose_m1755411338_gshared (Enumerator_t876005098 * __this, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Int32>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m2261539925_gshared (Enumerator_t876005098 * __this, const RuntimeMethod* method);
// T System.Collections.Generic.List`1/Enumerator<System.Int32>::get_Current()
extern "C"  int32_t Enumerator_get_Current_m15219049_gshared (Enumerator_t876005098 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1/Enumerator<System.Object>::.ctor(System.Collections.Generic.List`1<T>)
extern "C"  void Enumerator__ctor_m3388526419_gshared (Enumerator_t2954986095 * __this, List_1_t877107497 * ___l0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1/Enumerator<System.Object>::VerifyState()
extern "C"  void Enumerator_VerifyState_m2949725876_gshared (Enumerator_t2954986095 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1/Enumerator<System.Object>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m3764657353_gshared (Enumerator_t2954986095 * __this, const RuntimeMethod* method);
// System.Object System.Collections.Generic.List`1/Enumerator<System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  RuntimeObject * Enumerator_System_Collections_IEnumerator_get_Current_m2660490385_gshared (Enumerator_t2954986095 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1/Enumerator<System.Object>::Dispose()
extern "C"  void Enumerator_Dispose_m1161557187_gshared (Enumerator_t2954986095 * __this, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Object>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m1031192525_gshared (Enumerator_t2954986095 * __this, const RuntimeMethod* method);
// T System.Collections.Generic.List`1/Enumerator<System.Object>::get_Current()
extern "C"  RuntimeObject * Enumerator_get_Current_m1155707170_gshared (Enumerator_t2954986095 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1/Enumerator<System.Reflection.CustomAttributeNamedArgument>::.ctor(System.Collections.Generic.List`1<T>)
extern "C"  void Enumerator__ctor_m3571010397_gshared (Enumerator_t1401966232 * __this, List_1_t3619054930 * ___l0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1/Enumerator<System.Reflection.CustomAttributeNamedArgument>::VerifyState()
extern "C"  void Enumerator_VerifyState_m1611858526_gshared (Enumerator_t1401966232 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1/Enumerator<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m239426658_gshared (Enumerator_t1401966232 * __this, const RuntimeMethod* method);
// System.Object System.Collections.Generic.List`1/Enumerator<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IEnumerator.get_Current()
extern "C"  RuntimeObject * Enumerator_System_Collections_IEnumerator_get_Current_m2488257730_gshared (Enumerator_t1401966232 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1/Enumerator<System.Reflection.CustomAttributeNamedArgument>::Dispose()
extern "C"  void Enumerator_Dispose_m2408556530_gshared (Enumerator_t1401966232 * __this, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Reflection.CustomAttributeNamedArgument>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m3607670212_gshared (Enumerator_t1401966232 * __this, const RuntimeMethod* method);
// T System.Collections.Generic.List`1/Enumerator<System.Reflection.CustomAttributeNamedArgument>::get_Current()
extern "C"  CustomAttributeNamedArgument_t468938427  Enumerator_get_Current_m2059411488_gshared (Enumerator_t1401966232 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1/Enumerator<System.Reflection.CustomAttributeTypedArgument>::.ctor(System.Collections.Generic.List`1<T>)
extern "C"  void Enumerator__ctor_m2676834247_gshared (Enumerator_t2583965159 * __this, List_1_t506086561 * ___l0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1/Enumerator<System.Reflection.CustomAttributeTypedArgument>::VerifyState()
extern "C"  void Enumerator_VerifyState_m145152094_gshared (Enumerator_t2583965159 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1/Enumerator<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m1497436992_gshared (Enumerator_t2583965159 * __this, const RuntimeMethod* method);
// System.Object System.Collections.Generic.List`1/Enumerator<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IEnumerator.get_Current()
extern "C"  RuntimeObject * Enumerator_System_Collections_IEnumerator_get_Current_m3245126976_gshared (Enumerator_t2583965159 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1/Enumerator<System.Reflection.CustomAttributeTypedArgument>::Dispose()
extern "C"  void Enumerator_Dispose_m356472712_gshared (Enumerator_t2583965159 * __this, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Reflection.CustomAttributeTypedArgument>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m2784663055_gshared (Enumerator_t2583965159 * __this, const RuntimeMethod* method);
// T System.Collections.Generic.List`1/Enumerator<System.Reflection.CustomAttributeTypedArgument>::get_Current()
extern "C"  CustomAttributeTypedArgument_t1650937354  Enumerator_get_Current_m2957508078_gshared (Enumerator_t2583965159 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.AnimatorClipInfo>::.ctor(System.Collections.Generic.List`1<T>)
extern "C"  void Enumerator__ctor_m1935377498_gshared (Enumerator_t4248893199 * __this, List_1_t2171014601 * ___l0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.AnimatorClipInfo>::VerifyState()
extern "C"  void Enumerator_VerifyState_m1238207820_gshared (Enumerator_t4248893199 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.AnimatorClipInfo>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m1345851403_gshared (Enumerator_t4248893199 * __this, const RuntimeMethod* method);
// System.Object System.Collections.Generic.List`1/Enumerator<UnityEngine.AnimatorClipInfo>::System.Collections.IEnumerator.get_Current()
extern "C"  RuntimeObject * Enumerator_System_Collections_IEnumerator_get_Current_m4227277968_gshared (Enumerator_t4248893199 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.AnimatorClipInfo>::Dispose()
extern "C"  void Enumerator_Dispose_m4053495003_gshared (Enumerator_t4248893199 * __this, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.AnimatorClipInfo>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m4009918722_gshared (Enumerator_t4248893199 * __this, const RuntimeMethod* method);
// T System.Collections.Generic.List`1/Enumerator<UnityEngine.AnimatorClipInfo>::get_Current()
extern "C"  AnimatorClipInfo_t3315865394  Enumerator_get_Current_m722728543_gshared (Enumerator_t4248893199 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Color32>::.ctor(System.Collections.Generic.List`1<T>)
extern "C"  void Enumerator__ctor_m2512276049_gshared (Enumerator_t1218460160 * __this, List_1_t3435548858 * ___l0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Color32>::VerifyState()
extern "C"  void Enumerator_VerifyState_m1773101339_gshared (Enumerator_t1218460160 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Color32>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m3478596844_gshared (Enumerator_t1218460160 * __this, const RuntimeMethod* method);
// System.Object System.Collections.Generic.List`1/Enumerator<UnityEngine.Color32>::System.Collections.IEnumerator.get_Current()
extern "C"  RuntimeObject * Enumerator_System_Collections_IEnumerator_get_Current_m3560914353_gshared (Enumerator_t1218460160 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Color32>::Dispose()
extern "C"  void Enumerator_Dispose_m424538614_gshared (Enumerator_t1218460160 * __this, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.Color32>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m1607965107_gshared (Enumerator_t1218460160 * __this, const RuntimeMethod* method);
// T System.Collections.Generic.List`1/Enumerator<UnityEngine.Color32>::get_Current()
extern "C"  Color32_t285432355  Enumerator_get_Current_m2594360256_gshared (Enumerator_t1218460160 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.EventSystems.RaycastResult>::.ctor(System.Collections.Generic.List`1<T>)
extern "C"  void Enumerator__ctor_m1667883307_gshared (Enumerator_t149250267 * __this, List_1_t2366338965 * ___l0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.EventSystems.RaycastResult>::VerifyState()
extern "C"  void Enumerator_VerifyState_m1061122233_gshared (Enumerator_t149250267 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.EventSystems.RaycastResult>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m1788010111_gshared (Enumerator_t149250267 * __this, const RuntimeMethod* method);
// System.Object System.Collections.Generic.List`1/Enumerator<UnityEngine.EventSystems.RaycastResult>::System.Collections.IEnumerator.get_Current()
extern "C"  RuntimeObject * Enumerator_System_Collections_IEnumerator_get_Current_m2533393695_gshared (Enumerator_t149250267 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.EventSystems.RaycastResult>::Dispose()
extern "C"  void Enumerator_Dispose_m1255392088_gshared (Enumerator_t149250267 * __this, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.EventSystems.RaycastResult>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m3293894458_gshared (Enumerator_t149250267 * __this, const RuntimeMethod* method);
// T System.Collections.Generic.List`1/Enumerator<UnityEngine.EventSystems.RaycastResult>::get_Current()
extern "C"  RaycastResult_t3511189758  Enumerator_get_Current_m3817118942_gshared (Enumerator_t149250267 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.UICharInfo>::.ctor(System.Collections.Generic.List`1<T>)
extern "C"  void Enumerator__ctor_m54715403_gshared (Enumerator_t3414180022 * __this, List_1_t1336301424 * ___l0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.UICharInfo>::VerifyState()
extern "C"  void Enumerator_VerifyState_m4097623086_gshared (Enumerator_t3414180022 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.UICharInfo>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m2616961580_gshared (Enumerator_t3414180022 * __this, const RuntimeMethod* method);
// System.Object System.Collections.Generic.List`1/Enumerator<UnityEngine.UICharInfo>::System.Collections.IEnumerator.get_Current()
extern "C"  RuntimeObject * Enumerator_System_Collections_IEnumerator_get_Current_m3312567892_gshared (Enumerator_t3414180022 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.UICharInfo>::Dispose()
extern "C"  void Enumerator_Dispose_m3669109220_gshared (Enumerator_t3414180022 * __this, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.UICharInfo>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m3154098104_gshared (Enumerator_t3414180022 * __this, const RuntimeMethod* method);
// T System.Collections.Generic.List`1/Enumerator<UnityEngine.UICharInfo>::get_Current()
extern "C"  UICharInfo_t2481152217  Enumerator_get_Current_m87748356_gshared (Enumerator_t3414180022 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.UILineInfo>::.ctor(System.Collections.Generic.List`1<T>)
extern "C"  void Enumerator__ctor_m4031094966_gshared (Enumerator_t1931539200 * __this, List_1_t4148627898 * ___l0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.UILineInfo>::VerifyState()
extern "C"  void Enumerator_VerifyState_m3089285984_gshared (Enumerator_t1931539200 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.UILineInfo>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m3435897786_gshared (Enumerator_t1931539200 * __this, const RuntimeMethod* method);
// System.Object System.Collections.Generic.List`1/Enumerator<UnityEngine.UILineInfo>::System.Collections.IEnumerator.get_Current()
extern "C"  RuntimeObject * Enumerator_System_Collections_IEnumerator_get_Current_m1832049754_gshared (Enumerator_t1931539200 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.UILineInfo>::Dispose()
extern "C"  void Enumerator_Dispose_m294736456_gshared (Enumerator_t1931539200 * __this, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.UILineInfo>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m2005060853_gshared (Enumerator_t1931539200 * __this, const RuntimeMethod* method);
// T System.Collections.Generic.List`1/Enumerator<UnityEngine.UILineInfo>::get_Current()
extern "C"  UILineInfo_t998511395  Enumerator_get_Current_m2658112968_gshared (Enumerator_t1931539200 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.UIVertex>::.ctor(System.Collections.Generic.List`1<T>)
extern "C"  void Enumerator__ctor_m615483673_gshared (Enumerator_t3719495245 * __this, List_1_t1641616647 * ___l0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.UIVertex>::VerifyState()
extern "C"  void Enumerator_VerifyState_m2357532199_gshared (Enumerator_t3719495245 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.UIVertex>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m3546593789_gshared (Enumerator_t3719495245 * __this, const RuntimeMethod* method);
// System.Object System.Collections.Generic.List`1/Enumerator<UnityEngine.UIVertex>::System.Collections.IEnumerator.get_Current()
extern "C"  RuntimeObject * Enumerator_System_Collections_IEnumerator_get_Current_m1198406995_gshared (Enumerator_t3719495245 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.UIVertex>::Dispose()
extern "C"  void Enumerator_Dispose_m18990885_gshared (Enumerator_t3719495245 * __this, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.UIVertex>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m1509398310_gshared (Enumerator_t3719495245 * __this, const RuntimeMethod* method);
// T System.Collections.Generic.List`1/Enumerator<UnityEngine.UIVertex>::get_Current()
extern "C"  UIVertex_t2786467440  Enumerator_get_Current_m2212027098_gshared (Enumerator_t3719495245 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Vector2>::.ctor(System.Collections.Generic.List`1<T>)
extern "C"  void Enumerator__ctor_m3347994582_gshared (Enumerator_t215393771 * __this, List_1_t2432482469 * ___l0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Vector2>::VerifyState()
extern "C"  void Enumerator_VerifyState_m3589629877_gshared (Enumerator_t215393771 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Vector2>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m2221689747_gshared (Enumerator_t215393771 * __this, const RuntimeMethod* method);
// System.Object System.Collections.Generic.List`1/Enumerator<UnityEngine.Vector2>::System.Collections.IEnumerator.get_Current()
extern "C"  RuntimeObject * Enumerator_System_Collections_IEnumerator_get_Current_m741422485_gshared (Enumerator_t215393771 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Vector2>::Dispose()
extern "C"  void Enumerator_Dispose_m3955780047_gshared (Enumerator_t215393771 * __this, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.Vector2>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m183044627_gshared (Enumerator_t215393771 * __this, const RuntimeMethod* method);
// T System.Collections.Generic.List`1/Enumerator<UnityEngine.Vector2>::get_Current()
extern "C"  Vector2_t3577333262  Enumerator_get_Current_m2292278728_gshared (Enumerator_t215393771 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Vector3>::.ctor(System.Collections.Generic.List`1<T>)
extern "C"  void Enumerator__ctor_m904992548_gshared (Enumerator_t3836558239 * __this, List_1_t1758679641 * ___l0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Vector3>::VerifyState()
extern "C"  void Enumerator_VerifyState_m2353471952_gshared (Enumerator_t3836558239 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Vector3>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m3019879980_gshared (Enumerator_t3836558239 * __this, const RuntimeMethod* method);
// System.Object System.Collections.Generic.List`1/Enumerator<UnityEngine.Vector3>::System.Collections.IEnumerator.get_Current()
extern "C"  RuntimeObject * Enumerator_System_Collections_IEnumerator_get_Current_m1440485986_gshared (Enumerator_t3836558239 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Vector3>::Dispose()
extern "C"  void Enumerator_Dispose_m523694157_gshared (Enumerator_t3836558239 * __this, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.Vector3>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m2371540642_gshared (Enumerator_t3836558239 * __this, const RuntimeMethod* method);
// T System.Collections.Generic.List`1/Enumerator<UnityEngine.Vector3>::get_Current()
extern "C"  Vector3_t2903530434  Enumerator_get_Current_m1590935863_gshared (Enumerator_t3836558239 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Vector4>::.ctor(System.Collections.Generic.List`1<T>)
extern "C"  void Enumerator__ctor_m2410038664_gshared (Enumerator_t3747700150 * __this, List_1_t1669821552 * ___l0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Vector4>::VerifyState()
extern "C"  void Enumerator_VerifyState_m4257143980_gshared (Enumerator_t3747700150 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Vector4>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m888757397_gshared (Enumerator_t3747700150 * __this, const RuntimeMethod* method);
// System.Object System.Collections.Generic.List`1/Enumerator<UnityEngine.Vector4>::System.Collections.IEnumerator.get_Current()
extern "C"  RuntimeObject * Enumerator_System_Collections_IEnumerator_get_Current_m2740647926_gshared (Enumerator_t3747700150 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Vector4>::Dispose()
extern "C"  void Enumerator_Dispose_m173057505_gshared (Enumerator_t3747700150 * __this, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.Vector4>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m675284825_gshared (Enumerator_t3747700150 * __this, const RuntimeMethod* method);
// T System.Collections.Generic.List`1/Enumerator<UnityEngine.Vector4>::get_Current()
extern "C"  Vector4_t2814672345  Enumerator_get_Current_m2462407644_gshared (Enumerator_t3747700150 * __this, const RuntimeMethod* method);

// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m3512996173(__this, p0, method) ((  void (*) (KeyValuePair_2_t2536236643 *, int32_t, const RuntimeMethod*))KeyValuePair_2_set_Key_m3512996173_gshared)(__this, p0, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m3337772920(__this, p0, method) ((  void (*) (KeyValuePair_2_t2536236643 *, RuntimeObject *, const RuntimeMethod*))KeyValuePair_2_set_Value_m3337772920_gshared)(__this, p0, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m3779811036(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t2536236643 *, int32_t, RuntimeObject *, const RuntimeMethod*))KeyValuePair_2__ctor_m3779811036_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>::get_Key()
#define KeyValuePair_2_get_Key_m15997923(__this, method) ((  int32_t (*) (KeyValuePair_2_t2536236643 *, const RuntimeMethod*))KeyValuePair_2_get_Key_m15997923_gshared)(__this, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>::get_Value()
#define KeyValuePair_2_get_Value_m699117648(__this, method) ((  RuntimeObject * (*) (KeyValuePair_2_t2536236643 *, const RuntimeMethod*))KeyValuePair_2_get_Value_m699117648_gshared)(__this, method)
// System.String System.Int32::ToString()
extern "C"  String_t* Int32_ToString_m3061240541 (int32_t* __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String System.String::Concat(System.String[])
extern "C"  String_t* String_Concat_m2562457838 (RuntimeObject * __this /* static, unused */, StringU5BU5D_t2298632947* ___values0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>::ToString()
#define KeyValuePair_2_ToString_m3144124502(__this, method) ((  String_t* (*) (KeyValuePair_2_t2536236643 *, const RuntimeMethod*))KeyValuePair_2_ToString_m3144124502_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.TrackableBehaviour/Status>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m34282199(__this, p0, method) ((  void (*) (KeyValuePair_2_t1872397222 *, int32_t, const RuntimeMethod*))KeyValuePair_2_set_Key_m34282199_gshared)(__this, p0, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.TrackableBehaviour/Status>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m1414687358(__this, p0, method) ((  void (*) (KeyValuePair_2_t1872397222 *, int32_t, const RuntimeMethod*))KeyValuePair_2_set_Value_m1414687358_gshared)(__this, p0, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.TrackableBehaviour/Status>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m3381747994(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t1872397222 *, int32_t, int32_t, const RuntimeMethod*))KeyValuePair_2__ctor_m3381747994_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.TrackableBehaviour/Status>::get_Key()
#define KeyValuePair_2_get_Key_m2266079803(__this, method) ((  int32_t (*) (KeyValuePair_2_t1872397222 *, const RuntimeMethod*))KeyValuePair_2_get_Key_m2266079803_gshared)(__this, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.TrackableBehaviour/Status>::get_Value()
#define KeyValuePair_2_get_Value_m2289943185(__this, method) ((  int32_t (*) (KeyValuePair_2_t1872397222 *, const RuntimeMethod*))KeyValuePair_2_get_Value_m2289943185_gshared)(__this, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.TrackableBehaviour/Status>::ToString()
#define KeyValuePair_2_ToString_m3380882685(__this, method) ((  String_t* (*) (KeyValuePair_2_t1872397222 *, const RuntimeMethod*))KeyValuePair_2_ToString_m3380882685_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m979245165(__this, p0, method) ((  void (*) (KeyValuePair_2_t432866562 *, int32_t, const RuntimeMethod*))KeyValuePair_2_set_Key_m979245165_gshared)(__this, p0, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m3827392229(__this, p0, method) ((  void (*) (KeyValuePair_2_t432866562 *, VirtualButtonData_t4213555505 , const RuntimeMethod*))KeyValuePair_2_set_Value_m3827392229_gshared)(__this, p0, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m113285641(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t432866562 *, int32_t, VirtualButtonData_t4213555505 , const RuntimeMethod*))KeyValuePair_2__ctor_m113285641_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::get_Key()
#define KeyValuePair_2_get_Key_m1292857486(__this, method) ((  int32_t (*) (KeyValuePair_2_t432866562 *, const RuntimeMethod*))KeyValuePair_2_get_Key_m1292857486_gshared)(__this, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::get_Value()
#define KeyValuePair_2_get_Value_m3345055990(__this, method) ((  VirtualButtonData_t4213555505  (*) (KeyValuePair_2_t432866562 *, const RuntimeMethod*))KeyValuePair_2_get_Value_m3345055990_gshared)(__this, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::ToString()
#define KeyValuePair_2_ToString_m3666435602(__this, method) ((  String_t* (*) (KeyValuePair_2_t432866562 *, const RuntimeMethod*))KeyValuePair_2_ToString_m3666435602_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.IntPtr,System.Object>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m2055790825(__this, p0, method) ((  void (*) (KeyValuePair_2_t651451063 *, IntPtr_t, const RuntimeMethod*))KeyValuePair_2_set_Key_m2055790825_gshared)(__this, p0, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.IntPtr,System.Object>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m1654871410(__this, p0, method) ((  void (*) (KeyValuePair_2_t651451063 *, RuntimeObject *, const RuntimeMethod*))KeyValuePair_2_set_Value_m1654871410_gshared)(__this, p0, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.IntPtr,System.Object>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m2085269269(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t651451063 *, IntPtr_t, RuntimeObject *, const RuntimeMethod*))KeyValuePair_2__ctor_m2085269269_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.IntPtr,System.Object>::get_Key()
#define KeyValuePair_2_get_Key_m2522224863(__this, method) ((  IntPtr_t (*) (KeyValuePair_2_t651451063 *, const RuntimeMethod*))KeyValuePair_2_get_Key_m2522224863_gshared)(__this, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.IntPtr,System.Object>::get_Value()
#define KeyValuePair_2_get_Value_m3322101809(__this, method) ((  RuntimeObject * (*) (KeyValuePair_2_t651451063 *, const RuntimeMethod*))KeyValuePair_2_get_Value_m3322101809_gshared)(__this, method)
// System.String System.IntPtr::ToString()
extern "C"  String_t* IntPtr_ToString_m4044351426 (IntPtr_t* __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String System.Collections.Generic.KeyValuePair`2<System.IntPtr,System.Object>::ToString()
#define KeyValuePair_2_ToString_m1274882003(__this, method) ((  String_t* (*) (KeyValuePair_2_t651451063 *, const RuntimeMethod*))KeyValuePair_2_ToString_m1274882003_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m717171400(__this, p0, method) ((  void (*) (KeyValuePair_2_t115616549 *, RuntimeObject *, const RuntimeMethod*))KeyValuePair_2_set_Key_m717171400_gshared)(__this, p0, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m781244180(__this, p0, method) ((  void (*) (KeyValuePair_2_t115616549 *, bool, const RuntimeMethod*))KeyValuePair_2_set_Value_m781244180_gshared)(__this, p0, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m2919652111(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t115616549 *, RuntimeObject *, bool, const RuntimeMethod*))KeyValuePair_2__ctor_m2919652111_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>::get_Key()
#define KeyValuePair_2_get_Key_m3969882595(__this, method) ((  RuntimeObject * (*) (KeyValuePair_2_t115616549 *, const RuntimeMethod*))KeyValuePair_2_get_Key_m3969882595_gshared)(__this, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>::get_Value()
#define KeyValuePair_2_get_Value_m4103818512(__this, method) ((  bool (*) (KeyValuePair_2_t115616549 *, const RuntimeMethod*))KeyValuePair_2_get_Value_m4103818512_gshared)(__this, method)
// System.String System.Boolean::ToString()
extern "C"  String_t* Boolean_ToString_m3961468639 (bool* __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>::ToString()
#define KeyValuePair_2_ToString_m3427190393(__this, method) ((  String_t* (*) (KeyValuePair_2_t115616549 *, const RuntimeMethod*))KeyValuePair_2_ToString_m3427190393_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m3774274141(__this, p0, method) ((  void (*) (KeyValuePair_2_t1402716069 *, RuntimeObject *, const RuntimeMethod*))KeyValuePair_2_set_Key_m3774274141_gshared)(__this, p0, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m541298045(__this, p0, method) ((  void (*) (KeyValuePair_2_t1402716069 *, int32_t, const RuntimeMethod*))KeyValuePair_2_set_Value_m541298045_gshared)(__this, p0, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m388328491(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t1402716069 *, RuntimeObject *, int32_t, const RuntimeMethod*))KeyValuePair_2__ctor_m388328491_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>::get_Key()
#define KeyValuePair_2_get_Key_m1087122466(__this, method) ((  RuntimeObject * (*) (KeyValuePair_2_t1402716069 *, const RuntimeMethod*))KeyValuePair_2_get_Key_m1087122466_gshared)(__this, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>::get_Value()
#define KeyValuePair_2_get_Value_m2458189361(__this, method) ((  int32_t (*) (KeyValuePair_2_t1402716069 *, const RuntimeMethod*))KeyValuePair_2_get_Value_m2458189361_gshared)(__this, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>::ToString()
#define KeyValuePair_2_ToString_m729476552(__this, method) ((  String_t* (*) (KeyValuePair_2_t1402716069 *, const RuntimeMethod*))KeyValuePair_2_ToString_m729476552_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m3122105053(__this, p0, method) ((  void (*) (KeyValuePair_2_t3481697066 *, RuntimeObject *, const RuntimeMethod*))KeyValuePair_2_set_Key_m3122105053_gshared)(__this, p0, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m2394843442(__this, p0, method) ((  void (*) (KeyValuePair_2_t3481697066 *, RuntimeObject *, const RuntimeMethod*))KeyValuePair_2_set_Value_m2394843442_gshared)(__this, p0, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m973662155(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t3481697066 *, RuntimeObject *, RuntimeObject *, const RuntimeMethod*))KeyValuePair_2__ctor_m973662155_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>::get_Key()
#define KeyValuePair_2_get_Key_m4217124106(__this, method) ((  RuntimeObject * (*) (KeyValuePair_2_t3481697066 *, const RuntimeMethod*))KeyValuePair_2_get_Key_m4217124106_gshared)(__this, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>::get_Value()
#define KeyValuePair_2_get_Value_m1734077897(__this, method) ((  RuntimeObject * (*) (KeyValuePair_2_t3481697066 *, const RuntimeMethod*))KeyValuePair_2_get_Value_m1734077897_gshared)(__this, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>::ToString()
#define KeyValuePair_2_ToString_m3779009088(__this, method) ((  String_t* (*) (KeyValuePair_2_t3481697066 *, const RuntimeMethod*))KeyValuePair_2_ToString_m3779009088_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt16>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m1444094661(__this, p0, method) ((  void (*) (KeyValuePair_2_t574427458 *, RuntimeObject *, const RuntimeMethod*))KeyValuePair_2_set_Key_m1444094661_gshared)(__this, p0, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt16>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m655597244(__this, p0, method) ((  void (*) (KeyValuePair_2_t574427458 *, uint16_t, const RuntimeMethod*))KeyValuePair_2_set_Value_m655597244_gshared)(__this, p0, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt16>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m1511411907(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t574427458 *, RuntimeObject *, uint16_t, const RuntimeMethod*))KeyValuePair_2__ctor_m1511411907_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt16>::get_Key()
#define KeyValuePair_2_get_Key_m2791227378(__this, method) ((  RuntimeObject * (*) (KeyValuePair_2_t574427458 *, const RuntimeMethod*))KeyValuePair_2_get_Key_m2791227378_gshared)(__this, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt16>::get_Value()
#define KeyValuePair_2_get_Value_m2138563648(__this, method) ((  uint16_t (*) (KeyValuePair_2_t574427458 *, const RuntimeMethod*))KeyValuePair_2_get_Value_m2138563648_gshared)(__this, method)
// System.String System.UInt16::ToString()
extern "C"  String_t* UInt16_ToString_m496372703 (uint16_t* __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt16>::ToString()
#define KeyValuePair_2_ToString_m1879012905(__this, method) ((  String_t* (*) (KeyValuePair_2_t574427458 *, const RuntimeMethod*))KeyValuePair_2_ToString_m1879012905_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,Vuforia.WebCamProfile/ProfileData>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m2963078532(__this, p0, method) ((  void (*) (KeyValuePair_2_t2200618205 *, RuntimeObject *, const RuntimeMethod*))KeyValuePair_2_set_Key_m2963078532_gshared)(__this, p0, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,Vuforia.WebCamProfile/ProfileData>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m3496892223(__this, p0, method) ((  void (*) (KeyValuePair_2_t2200618205 *, ProfileData_t740879429 , const RuntimeMethod*))KeyValuePair_2_set_Value_m3496892223_gshared)(__this, p0, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,Vuforia.WebCamProfile/ProfileData>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m3327461017(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t2200618205 *, RuntimeObject *, ProfileData_t740879429 , const RuntimeMethod*))KeyValuePair_2__ctor_m3327461017_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.Object,Vuforia.WebCamProfile/ProfileData>::get_Key()
#define KeyValuePair_2_get_Key_m956489385(__this, method) ((  RuntimeObject * (*) (KeyValuePair_2_t2200618205 *, const RuntimeMethod*))KeyValuePair_2_get_Key_m956489385_gshared)(__this, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.Object,Vuforia.WebCamProfile/ProfileData>::get_Value()
#define KeyValuePair_2_get_Value_m1073695936(__this, method) ((  ProfileData_t740879429  (*) (KeyValuePair_2_t2200618205 *, const RuntimeMethod*))KeyValuePair_2_get_Value_m1073695936_gshared)(__this, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.Object,Vuforia.WebCamProfile/ProfileData>::ToString()
#define KeyValuePair_2_ToString_m4250712101(__this, method) ((  String_t* (*) (KeyValuePair_2_t2200618205 *, const RuntimeMethod*))KeyValuePair_2_ToString_m4250712101_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<Vuforia.Image/PIXEL_FORMAT,System.Object>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m1462435752(__this, p0, method) ((  void (*) (KeyValuePair_2_t603276356 *, int32_t, const RuntimeMethod*))KeyValuePair_2_set_Key_m1462435752_gshared)(__this, p0, method)
// System.Void System.Collections.Generic.KeyValuePair`2<Vuforia.Image/PIXEL_FORMAT,System.Object>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m3866220215(__this, p0, method) ((  void (*) (KeyValuePair_2_t603276356 *, RuntimeObject *, const RuntimeMethod*))KeyValuePair_2_set_Value_m3866220215_gshared)(__this, p0, method)
// System.Void System.Collections.Generic.KeyValuePair`2<Vuforia.Image/PIXEL_FORMAT,System.Object>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m2367532738(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t603276356 *, int32_t, RuntimeObject *, const RuntimeMethod*))KeyValuePair_2__ctor_m2367532738_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<Vuforia.Image/PIXEL_FORMAT,System.Object>::get_Key()
#define KeyValuePair_2_get_Key_m3814775675(__this, method) ((  int32_t (*) (KeyValuePair_2_t603276356 *, const RuntimeMethod*))KeyValuePair_2_get_Key_m3814775675_gshared)(__this, method)
// TValue System.Collections.Generic.KeyValuePair`2<Vuforia.Image/PIXEL_FORMAT,System.Object>::get_Value()
#define KeyValuePair_2_get_Value_m1253090796(__this, method) ((  RuntimeObject * (*) (KeyValuePair_2_t603276356 *, const RuntimeMethod*))KeyValuePair_2_get_Value_m1253090796_gshared)(__this, method)
// System.String System.Collections.Generic.KeyValuePair`2<Vuforia.Image/PIXEL_FORMAT,System.Object>::ToString()
#define KeyValuePair_2_ToString_m2759420978(__this, method) ((  String_t* (*) (KeyValuePair_2_t603276356 *, const RuntimeMethod*))KeyValuePair_2_ToString_m2759420978_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.HoloLensExtendedTrackingManager/PoseAgeEntry>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m3192279472(__this, p0, method) ((  void (*) (KeyValuePair_2_t2609754974 *, TrackableIdPair_t1331427386 , const RuntimeMethod*))KeyValuePair_2_set_Key_m3192279472_gshared)(__this, p0, method)
// System.Void System.Collections.Generic.KeyValuePair`2<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.HoloLensExtendedTrackingManager/PoseAgeEntry>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m3595070817(__this, p0, method) ((  void (*) (KeyValuePair_2_t2609754974 *, PoseAgeEntry_t806437902 , const RuntimeMethod*))KeyValuePair_2_set_Value_m3595070817_gshared)(__this, p0, method)
// System.Void System.Collections.Generic.KeyValuePair`2<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.HoloLensExtendedTrackingManager/PoseAgeEntry>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m820146208(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t2609754974 *, TrackableIdPair_t1331427386 , PoseAgeEntry_t806437902 , const RuntimeMethod*))KeyValuePair_2__ctor_m820146208_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.HoloLensExtendedTrackingManager/PoseAgeEntry>::get_Key()
#define KeyValuePair_2_get_Key_m3017190976(__this, method) ((  TrackableIdPair_t1331427386  (*) (KeyValuePair_2_t2609754974 *, const RuntimeMethod*))KeyValuePair_2_get_Key_m3017190976_gshared)(__this, method)
// TValue System.Collections.Generic.KeyValuePair`2<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.HoloLensExtendedTrackingManager/PoseAgeEntry>::get_Value()
#define KeyValuePair_2_get_Value_m3794373347(__this, method) ((  PoseAgeEntry_t806437902  (*) (KeyValuePair_2_t2609754974 *, const RuntimeMethod*))KeyValuePair_2_get_Value_m3794373347_gshared)(__this, method)
// System.String System.Collections.Generic.KeyValuePair`2<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.HoloLensExtendedTrackingManager/PoseAgeEntry>::ToString()
#define KeyValuePair_2_ToString_m3066038920(__this, method) ((  String_t* (*) (KeyValuePair_2_t2609754974 *, const RuntimeMethod*))KeyValuePair_2_ToString_m3066038920_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.HoloLensExtendedTrackingManager/PoseInfo>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m3132306086(__this, p0, method) ((  void (*) (KeyValuePair_2_t4060628773 *, TrackableIdPair_t1331427386 , const RuntimeMethod*))KeyValuePair_2_set_Key_m3132306086_gshared)(__this, p0, method)
// System.Void System.Collections.Generic.KeyValuePair`2<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.HoloLensExtendedTrackingManager/PoseInfo>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m3890858544(__this, p0, method) ((  void (*) (KeyValuePair_2_t4060628773 *, PoseInfo_t2257311701 , const RuntimeMethod*))KeyValuePair_2_set_Value_m3890858544_gshared)(__this, p0, method)
// System.Void System.Collections.Generic.KeyValuePair`2<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.HoloLensExtendedTrackingManager/PoseInfo>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m3588891793(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t4060628773 *, TrackableIdPair_t1331427386 , PoseInfo_t2257311701 , const RuntimeMethod*))KeyValuePair_2__ctor_m3588891793_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.HoloLensExtendedTrackingManager/PoseInfo>::get_Key()
#define KeyValuePair_2_get_Key_m4245299421(__this, method) ((  TrackableIdPair_t1331427386  (*) (KeyValuePair_2_t4060628773 *, const RuntimeMethod*))KeyValuePair_2_get_Key_m4245299421_gshared)(__this, method)
// TValue System.Collections.Generic.KeyValuePair`2<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.HoloLensExtendedTrackingManager/PoseInfo>::get_Value()
#define KeyValuePair_2_get_Value_m3742287316(__this, method) ((  PoseInfo_t2257311701  (*) (KeyValuePair_2_t4060628773 *, const RuntimeMethod*))KeyValuePair_2_get_Value_m3742287316_gshared)(__this, method)
// System.String System.Collections.Generic.KeyValuePair`2<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.HoloLensExtendedTrackingManager/PoseInfo>::ToString()
#define KeyValuePair_2_ToString_m2795500392(__this, method) ((  String_t* (*) (KeyValuePair_2_t4060628773 *, const RuntimeMethod*))KeyValuePair_2_ToString_m2795500392_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.TrackableBehaviour/Status>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m2719103795(__this, p0, method) ((  void (*) (KeyValuePair_2_t3161435941 *, TrackableIdPair_t1331427386 , const RuntimeMethod*))KeyValuePair_2_set_Key_m2719103795_gshared)(__this, p0, method)
// System.Void System.Collections.Generic.KeyValuePair`2<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.TrackableBehaviour/Status>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m1544936261(__this, p0, method) ((  void (*) (KeyValuePair_2_t3161435941 *, int32_t, const RuntimeMethod*))KeyValuePair_2_set_Value_m1544936261_gshared)(__this, p0, method)
// System.Void System.Collections.Generic.KeyValuePair`2<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.TrackableBehaviour/Status>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m3910118697(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t3161435941 *, TrackableIdPair_t1331427386 , int32_t, const RuntimeMethod*))KeyValuePair_2__ctor_m3910118697_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.TrackableBehaviour/Status>::get_Key()
#define KeyValuePair_2_get_Key_m1540520745(__this, method) ((  TrackableIdPair_t1331427386  (*) (KeyValuePair_2_t3161435941 *, const RuntimeMethod*))KeyValuePair_2_get_Key_m1540520745_gshared)(__this, method)
// TValue System.Collections.Generic.KeyValuePair`2<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.TrackableBehaviour/Status>::get_Value()
#define KeyValuePair_2_get_Value_m3827826732(__this, method) ((  int32_t (*) (KeyValuePair_2_t3161435941 *, const RuntimeMethod*))KeyValuePair_2_get_Value_m3827826732_gshared)(__this, method)
// System.String System.Collections.Generic.KeyValuePair`2<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.TrackableBehaviour/Status>::ToString()
#define KeyValuePair_2_ToString_m1074719178(__this, method) ((  String_t* (*) (KeyValuePair_2_t3161435941 *, const RuntimeMethod*))KeyValuePair_2_ToString_m1074719178_gshared)(__this, method)
// System.Void System.Collections.Generic.LinkedList`1/Enumerator<System.Object>::.ctor(System.Collections.Generic.LinkedList`1<T>)
#define Enumerator__ctor_m3993907956(__this, ___parent0, method) ((  void (*) (Enumerator_t1983447509 *, LinkedList_1_t4198215526 *, const RuntimeMethod*))Enumerator__ctor_m3993907956_gshared)(__this, ___parent0, method)
// T System.Collections.Generic.LinkedList`1/Enumerator<System.Object>::get_Current()
#define Enumerator_get_Current_m3886014715(__this, method) ((  RuntimeObject * (*) (Enumerator_t1983447509 *, const RuntimeMethod*))Enumerator_get_Current_m3886014715_gshared)(__this, method)
// System.Object System.Collections.Generic.LinkedList`1/Enumerator<System.Object>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m99794827(__this, method) ((  RuntimeObject * (*) (Enumerator_t1983447509 *, const RuntimeMethod*))Enumerator_System_Collections_IEnumerator_get_Current_m99794827_gshared)(__this, method)
// System.Void System.ObjectDisposedException::.ctor(System.String)
extern "C"  void ObjectDisposedException__ctor_m4150714796 (ObjectDisposedException_t2553224159 * __this, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.InvalidOperationException::.ctor(System.String)
extern "C"  void InvalidOperationException__ctor_m4101245683 (InvalidOperationException_t2321794232 * __this, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.LinkedList`1/Enumerator<System.Object>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m2687040866(__this, method) ((  void (*) (Enumerator_t1983447509 *, const RuntimeMethod*))Enumerator_System_Collections_IEnumerator_Reset_m2687040866_gshared)(__this, method)
// System.Void System.InvalidOperationException::.ctor()
extern "C"  void InvalidOperationException__ctor_m3753870925 (InvalidOperationException_t2321794232 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.Generic.LinkedList`1/Enumerator<System.Object>::MoveNext()
#define Enumerator_MoveNext_m893837046(__this, method) ((  bool (*) (Enumerator_t1983447509 *, const RuntimeMethod*))Enumerator_MoveNext_m893837046_gshared)(__this, method)
// System.Void System.Collections.Generic.LinkedList`1/Enumerator<System.Object>::Dispose()
#define Enumerator_Dispose_m3834463819(__this, method) ((  void (*) (Enumerator_t1983447509 *, const RuntimeMethod*))Enumerator_Dispose_m3834463819_gshared)(__this, method)
// System.Void System.Collections.Generic.LinkedList`1/Enumerator<Vuforia.VuforiaManager/TrackableIdPair>::.ctor(System.Collections.Generic.LinkedList`1<T>)
#define Enumerator__ctor_m2514524852(__this, ___parent0, method) ((  void (*) (Enumerator_t1292916605 *, LinkedList_1_t3507684622 *, const RuntimeMethod*))Enumerator__ctor_m2514524852_gshared)(__this, ___parent0, method)
// T System.Collections.Generic.LinkedList`1/Enumerator<Vuforia.VuforiaManager/TrackableIdPair>::get_Current()
#define Enumerator_get_Current_m1428289307(__this, method) ((  TrackableIdPair_t1331427386  (*) (Enumerator_t1292916605 *, const RuntimeMethod*))Enumerator_get_Current_m1428289307_gshared)(__this, method)
// System.Object System.Collections.Generic.LinkedList`1/Enumerator<Vuforia.VuforiaManager/TrackableIdPair>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m1186992793(__this, method) ((  RuntimeObject * (*) (Enumerator_t1292916605 *, const RuntimeMethod*))Enumerator_System_Collections_IEnumerator_get_Current_m1186992793_gshared)(__this, method)
// System.Void System.Collections.Generic.LinkedList`1/Enumerator<Vuforia.VuforiaManager/TrackableIdPair>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m986577418(__this, method) ((  void (*) (Enumerator_t1292916605 *, const RuntimeMethod*))Enumerator_System_Collections_IEnumerator_Reset_m986577418_gshared)(__this, method)
// System.Boolean System.Collections.Generic.LinkedList`1/Enumerator<Vuforia.VuforiaManager/TrackableIdPair>::MoveNext()
#define Enumerator_MoveNext_m562864630(__this, method) ((  bool (*) (Enumerator_t1292916605 *, const RuntimeMethod*))Enumerator_MoveNext_m562864630_gshared)(__this, method)
// System.Void System.Collections.Generic.LinkedList`1/Enumerator<Vuforia.VuforiaManager/TrackableIdPair>::Dispose()
#define Enumerator_Dispose_m3863799224(__this, method) ((  void (*) (Enumerator_t1292916605 *, const RuntimeMethod*))Enumerator_Dispose_m3863799224_gshared)(__this, method)
// System.Void System.Object::.ctor()
extern "C"  void Object__ctor_m2906060009 (RuntimeObject * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.ArgumentException::.ctor(System.String)
extern "C"  void ArgumentException__ctor_m1186658332 (ArgumentException_t4028401650 * __this, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.ArgumentNullException::.ctor(System.String)
extern "C"  void ArgumentNullException__ctor_m1483904578 (ArgumentNullException_t2943536221 * __this, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Array::GetLowerBound(System.Int32)
extern "C"  int32_t Array_GetLowerBound_m2726004408 (RuntimeArray * __this, int32_t p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.ArgumentOutOfRangeException::.ctor(System.String)
extern "C"  void ArgumentOutOfRangeException__ctor_m1532465284 (ArgumentOutOfRangeException_t2174922486 * __this, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Array::get_Rank()
extern "C"  int32_t Array_get_Rank_m2275309645 (RuntimeArray * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.ArgumentException::.ctor(System.String,System.String)
extern "C"  void ArgumentException__ctor_m596893627 (ArgumentException_t4028401650 * __this, String_t* p0, String_t* p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Type System.Type::GetTypeFromHandle(System.RuntimeTypeHandle)
extern "C"  Type_t * Type_GetTypeFromHandle_m269683661 (RuntimeObject * __this /* static, unused */, RuntimeTypeHandle_t845633806  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Serialization.SerializationInfo::AddValue(System.String,System.Object,System.Type)
extern "C"  void SerializationInfo_AddValue_m22079889 (SerializationInfo_t1606602250 * __this, String_t* p0, RuntimeObject * p1, Type_t * p2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Serialization.SerializationInfo::AddValue(System.String,System.UInt32)
extern "C"  void SerializationInfo_AddValue_m3901584621 (SerializationInfo_t1606602250 * __this, String_t* p0, uint32_t p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Object System.Runtime.Serialization.SerializationInfo::GetValue(System.String,System.Type)
extern "C"  RuntimeObject * SerializationInfo_GetValue_m284139217 (SerializationInfo_t1606602250 * __this, String_t* p0, Type_t * p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.UInt32 System.Runtime.Serialization.SerializationInfo::GetUInt32(System.String)
extern "C"  uint32_t SerializationInfo_GetUInt32_m2443490372 (SerializationInfo_t1606602250 * __this, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.List`1/Enumerator<System.Int32>::.ctor(System.Collections.Generic.List`1<T>)
#define Enumerator__ctor_m2268946854(__this, ___l0, method) ((  void (*) (Enumerator_t876005098 *, List_1_t3093093796 *, const RuntimeMethod*))Enumerator__ctor_m2268946854_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<System.Int32>::VerifyState()
#define Enumerator_VerifyState_m2157842137(__this, method) ((  void (*) (Enumerator_t876005098 *, const RuntimeMethod*))Enumerator_VerifyState_m2157842137_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<System.Int32>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m748325870(__this, method) ((  void (*) (Enumerator_t876005098 *, const RuntimeMethod*))Enumerator_System_Collections_IEnumerator_Reset_m748325870_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<System.Int32>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m642494324(__this, method) ((  RuntimeObject * (*) (Enumerator_t876005098 *, const RuntimeMethod*))Enumerator_System_Collections_IEnumerator_get_Current_m642494324_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<System.Int32>::Dispose()
#define Enumerator_Dispose_m1755411338(__this, method) ((  void (*) (Enumerator_t876005098 *, const RuntimeMethod*))Enumerator_Dispose_m1755411338_gshared)(__this, method)
// System.Type System.Object::GetType()
extern "C"  Type_t * Object_GetType_m3774351292 (RuntimeObject * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Int32>::MoveNext()
#define Enumerator_MoveNext_m2261539925(__this, method) ((  bool (*) (Enumerator_t876005098 *, const RuntimeMethod*))Enumerator_MoveNext_m2261539925_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<System.Int32>::get_Current()
#define Enumerator_get_Current_m15219049(__this, method) ((  int32_t (*) (Enumerator_t876005098 *, const RuntimeMethod*))Enumerator_get_Current_m15219049_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<System.Object>::.ctor(System.Collections.Generic.List`1<T>)
#define Enumerator__ctor_m3388526419(__this, ___l0, method) ((  void (*) (Enumerator_t2954986095 *, List_1_t877107497 *, const RuntimeMethod*))Enumerator__ctor_m3388526419_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<System.Object>::VerifyState()
#define Enumerator_VerifyState_m2949725876(__this, method) ((  void (*) (Enumerator_t2954986095 *, const RuntimeMethod*))Enumerator_VerifyState_m2949725876_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<System.Object>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m3764657353(__this, method) ((  void (*) (Enumerator_t2954986095 *, const RuntimeMethod*))Enumerator_System_Collections_IEnumerator_Reset_m3764657353_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<System.Object>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m2660490385(__this, method) ((  RuntimeObject * (*) (Enumerator_t2954986095 *, const RuntimeMethod*))Enumerator_System_Collections_IEnumerator_get_Current_m2660490385_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<System.Object>::Dispose()
#define Enumerator_Dispose_m1161557187(__this, method) ((  void (*) (Enumerator_t2954986095 *, const RuntimeMethod*))Enumerator_Dispose_m1161557187_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Object>::MoveNext()
#define Enumerator_MoveNext_m1031192525(__this, method) ((  bool (*) (Enumerator_t2954986095 *, const RuntimeMethod*))Enumerator_MoveNext_m1031192525_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<System.Object>::get_Current()
#define Enumerator_get_Current_m1155707170(__this, method) ((  RuntimeObject * (*) (Enumerator_t2954986095 *, const RuntimeMethod*))Enumerator_get_Current_m1155707170_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<System.Reflection.CustomAttributeNamedArgument>::.ctor(System.Collections.Generic.List`1<T>)
#define Enumerator__ctor_m3571010397(__this, ___l0, method) ((  void (*) (Enumerator_t1401966232 *, List_1_t3619054930 *, const RuntimeMethod*))Enumerator__ctor_m3571010397_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<System.Reflection.CustomAttributeNamedArgument>::VerifyState()
#define Enumerator_VerifyState_m1611858526(__this, method) ((  void (*) (Enumerator_t1401966232 *, const RuntimeMethod*))Enumerator_VerifyState_m1611858526_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m239426658(__this, method) ((  void (*) (Enumerator_t1401966232 *, const RuntimeMethod*))Enumerator_System_Collections_IEnumerator_Reset_m239426658_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m2488257730(__this, method) ((  RuntimeObject * (*) (Enumerator_t1401966232 *, const RuntimeMethod*))Enumerator_System_Collections_IEnumerator_get_Current_m2488257730_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<System.Reflection.CustomAttributeNamedArgument>::Dispose()
#define Enumerator_Dispose_m2408556530(__this, method) ((  void (*) (Enumerator_t1401966232 *, const RuntimeMethod*))Enumerator_Dispose_m2408556530_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Reflection.CustomAttributeNamedArgument>::MoveNext()
#define Enumerator_MoveNext_m3607670212(__this, method) ((  bool (*) (Enumerator_t1401966232 *, const RuntimeMethod*))Enumerator_MoveNext_m3607670212_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<System.Reflection.CustomAttributeNamedArgument>::get_Current()
#define Enumerator_get_Current_m2059411488(__this, method) ((  CustomAttributeNamedArgument_t468938427  (*) (Enumerator_t1401966232 *, const RuntimeMethod*))Enumerator_get_Current_m2059411488_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<System.Reflection.CustomAttributeTypedArgument>::.ctor(System.Collections.Generic.List`1<T>)
#define Enumerator__ctor_m2676834247(__this, ___l0, method) ((  void (*) (Enumerator_t2583965159 *, List_1_t506086561 *, const RuntimeMethod*))Enumerator__ctor_m2676834247_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<System.Reflection.CustomAttributeTypedArgument>::VerifyState()
#define Enumerator_VerifyState_m145152094(__this, method) ((  void (*) (Enumerator_t2583965159 *, const RuntimeMethod*))Enumerator_VerifyState_m145152094_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m1497436992(__this, method) ((  void (*) (Enumerator_t2583965159 *, const RuntimeMethod*))Enumerator_System_Collections_IEnumerator_Reset_m1497436992_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m3245126976(__this, method) ((  RuntimeObject * (*) (Enumerator_t2583965159 *, const RuntimeMethod*))Enumerator_System_Collections_IEnumerator_get_Current_m3245126976_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<System.Reflection.CustomAttributeTypedArgument>::Dispose()
#define Enumerator_Dispose_m356472712(__this, method) ((  void (*) (Enumerator_t2583965159 *, const RuntimeMethod*))Enumerator_Dispose_m356472712_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Reflection.CustomAttributeTypedArgument>::MoveNext()
#define Enumerator_MoveNext_m2784663055(__this, method) ((  bool (*) (Enumerator_t2583965159 *, const RuntimeMethod*))Enumerator_MoveNext_m2784663055_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<System.Reflection.CustomAttributeTypedArgument>::get_Current()
#define Enumerator_get_Current_m2957508078(__this, method) ((  CustomAttributeTypedArgument_t1650937354  (*) (Enumerator_t2583965159 *, const RuntimeMethod*))Enumerator_get_Current_m2957508078_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.AnimatorClipInfo>::.ctor(System.Collections.Generic.List`1<T>)
#define Enumerator__ctor_m1935377498(__this, ___l0, method) ((  void (*) (Enumerator_t4248893199 *, List_1_t2171014601 *, const RuntimeMethod*))Enumerator__ctor_m1935377498_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.AnimatorClipInfo>::VerifyState()
#define Enumerator_VerifyState_m1238207820(__this, method) ((  void (*) (Enumerator_t4248893199 *, const RuntimeMethod*))Enumerator_VerifyState_m1238207820_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.AnimatorClipInfo>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m1345851403(__this, method) ((  void (*) (Enumerator_t4248893199 *, const RuntimeMethod*))Enumerator_System_Collections_IEnumerator_Reset_m1345851403_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<UnityEngine.AnimatorClipInfo>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m4227277968(__this, method) ((  RuntimeObject * (*) (Enumerator_t4248893199 *, const RuntimeMethod*))Enumerator_System_Collections_IEnumerator_get_Current_m4227277968_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.AnimatorClipInfo>::Dispose()
#define Enumerator_Dispose_m4053495003(__this, method) ((  void (*) (Enumerator_t4248893199 *, const RuntimeMethod*))Enumerator_Dispose_m4053495003_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.AnimatorClipInfo>::MoveNext()
#define Enumerator_MoveNext_m4009918722(__this, method) ((  bool (*) (Enumerator_t4248893199 *, const RuntimeMethod*))Enumerator_MoveNext_m4009918722_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<UnityEngine.AnimatorClipInfo>::get_Current()
#define Enumerator_get_Current_m722728543(__this, method) ((  AnimatorClipInfo_t3315865394  (*) (Enumerator_t4248893199 *, const RuntimeMethod*))Enumerator_get_Current_m722728543_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Color32>::.ctor(System.Collections.Generic.List`1<T>)
#define Enumerator__ctor_m2512276049(__this, ___l0, method) ((  void (*) (Enumerator_t1218460160 *, List_1_t3435548858 *, const RuntimeMethod*))Enumerator__ctor_m2512276049_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Color32>::VerifyState()
#define Enumerator_VerifyState_m1773101339(__this, method) ((  void (*) (Enumerator_t1218460160 *, const RuntimeMethod*))Enumerator_VerifyState_m1773101339_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Color32>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m3478596844(__this, method) ((  void (*) (Enumerator_t1218460160 *, const RuntimeMethod*))Enumerator_System_Collections_IEnumerator_Reset_m3478596844_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<UnityEngine.Color32>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m3560914353(__this, method) ((  RuntimeObject * (*) (Enumerator_t1218460160 *, const RuntimeMethod*))Enumerator_System_Collections_IEnumerator_get_Current_m3560914353_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Color32>::Dispose()
#define Enumerator_Dispose_m424538614(__this, method) ((  void (*) (Enumerator_t1218460160 *, const RuntimeMethod*))Enumerator_Dispose_m424538614_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.Color32>::MoveNext()
#define Enumerator_MoveNext_m1607965107(__this, method) ((  bool (*) (Enumerator_t1218460160 *, const RuntimeMethod*))Enumerator_MoveNext_m1607965107_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<UnityEngine.Color32>::get_Current()
#define Enumerator_get_Current_m2594360256(__this, method) ((  Color32_t285432355  (*) (Enumerator_t1218460160 *, const RuntimeMethod*))Enumerator_get_Current_m2594360256_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.EventSystems.RaycastResult>::.ctor(System.Collections.Generic.List`1<T>)
#define Enumerator__ctor_m1667883307(__this, ___l0, method) ((  void (*) (Enumerator_t149250267 *, List_1_t2366338965 *, const RuntimeMethod*))Enumerator__ctor_m1667883307_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.EventSystems.RaycastResult>::VerifyState()
#define Enumerator_VerifyState_m1061122233(__this, method) ((  void (*) (Enumerator_t149250267 *, const RuntimeMethod*))Enumerator_VerifyState_m1061122233_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.EventSystems.RaycastResult>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m1788010111(__this, method) ((  void (*) (Enumerator_t149250267 *, const RuntimeMethod*))Enumerator_System_Collections_IEnumerator_Reset_m1788010111_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<UnityEngine.EventSystems.RaycastResult>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m2533393695(__this, method) ((  RuntimeObject * (*) (Enumerator_t149250267 *, const RuntimeMethod*))Enumerator_System_Collections_IEnumerator_get_Current_m2533393695_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.EventSystems.RaycastResult>::Dispose()
#define Enumerator_Dispose_m1255392088(__this, method) ((  void (*) (Enumerator_t149250267 *, const RuntimeMethod*))Enumerator_Dispose_m1255392088_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.EventSystems.RaycastResult>::MoveNext()
#define Enumerator_MoveNext_m3293894458(__this, method) ((  bool (*) (Enumerator_t149250267 *, const RuntimeMethod*))Enumerator_MoveNext_m3293894458_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<UnityEngine.EventSystems.RaycastResult>::get_Current()
#define Enumerator_get_Current_m3817118942(__this, method) ((  RaycastResult_t3511189758  (*) (Enumerator_t149250267 *, const RuntimeMethod*))Enumerator_get_Current_m3817118942_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.UICharInfo>::.ctor(System.Collections.Generic.List`1<T>)
#define Enumerator__ctor_m54715403(__this, ___l0, method) ((  void (*) (Enumerator_t3414180022 *, List_1_t1336301424 *, const RuntimeMethod*))Enumerator__ctor_m54715403_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.UICharInfo>::VerifyState()
#define Enumerator_VerifyState_m4097623086(__this, method) ((  void (*) (Enumerator_t3414180022 *, const RuntimeMethod*))Enumerator_VerifyState_m4097623086_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.UICharInfo>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m2616961580(__this, method) ((  void (*) (Enumerator_t3414180022 *, const RuntimeMethod*))Enumerator_System_Collections_IEnumerator_Reset_m2616961580_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<UnityEngine.UICharInfo>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m3312567892(__this, method) ((  RuntimeObject * (*) (Enumerator_t3414180022 *, const RuntimeMethod*))Enumerator_System_Collections_IEnumerator_get_Current_m3312567892_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.UICharInfo>::Dispose()
#define Enumerator_Dispose_m3669109220(__this, method) ((  void (*) (Enumerator_t3414180022 *, const RuntimeMethod*))Enumerator_Dispose_m3669109220_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.UICharInfo>::MoveNext()
#define Enumerator_MoveNext_m3154098104(__this, method) ((  bool (*) (Enumerator_t3414180022 *, const RuntimeMethod*))Enumerator_MoveNext_m3154098104_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<UnityEngine.UICharInfo>::get_Current()
#define Enumerator_get_Current_m87748356(__this, method) ((  UICharInfo_t2481152217  (*) (Enumerator_t3414180022 *, const RuntimeMethod*))Enumerator_get_Current_m87748356_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.UILineInfo>::.ctor(System.Collections.Generic.List`1<T>)
#define Enumerator__ctor_m4031094966(__this, ___l0, method) ((  void (*) (Enumerator_t1931539200 *, List_1_t4148627898 *, const RuntimeMethod*))Enumerator__ctor_m4031094966_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.UILineInfo>::VerifyState()
#define Enumerator_VerifyState_m3089285984(__this, method) ((  void (*) (Enumerator_t1931539200 *, const RuntimeMethod*))Enumerator_VerifyState_m3089285984_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.UILineInfo>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m3435897786(__this, method) ((  void (*) (Enumerator_t1931539200 *, const RuntimeMethod*))Enumerator_System_Collections_IEnumerator_Reset_m3435897786_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<UnityEngine.UILineInfo>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m1832049754(__this, method) ((  RuntimeObject * (*) (Enumerator_t1931539200 *, const RuntimeMethod*))Enumerator_System_Collections_IEnumerator_get_Current_m1832049754_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.UILineInfo>::Dispose()
#define Enumerator_Dispose_m294736456(__this, method) ((  void (*) (Enumerator_t1931539200 *, const RuntimeMethod*))Enumerator_Dispose_m294736456_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.UILineInfo>::MoveNext()
#define Enumerator_MoveNext_m2005060853(__this, method) ((  bool (*) (Enumerator_t1931539200 *, const RuntimeMethod*))Enumerator_MoveNext_m2005060853_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<UnityEngine.UILineInfo>::get_Current()
#define Enumerator_get_Current_m2658112968(__this, method) ((  UILineInfo_t998511395  (*) (Enumerator_t1931539200 *, const RuntimeMethod*))Enumerator_get_Current_m2658112968_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.UIVertex>::.ctor(System.Collections.Generic.List`1<T>)
#define Enumerator__ctor_m615483673(__this, ___l0, method) ((  void (*) (Enumerator_t3719495245 *, List_1_t1641616647 *, const RuntimeMethod*))Enumerator__ctor_m615483673_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.UIVertex>::VerifyState()
#define Enumerator_VerifyState_m2357532199(__this, method) ((  void (*) (Enumerator_t3719495245 *, const RuntimeMethod*))Enumerator_VerifyState_m2357532199_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.UIVertex>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m3546593789(__this, method) ((  void (*) (Enumerator_t3719495245 *, const RuntimeMethod*))Enumerator_System_Collections_IEnumerator_Reset_m3546593789_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<UnityEngine.UIVertex>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m1198406995(__this, method) ((  RuntimeObject * (*) (Enumerator_t3719495245 *, const RuntimeMethod*))Enumerator_System_Collections_IEnumerator_get_Current_m1198406995_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.UIVertex>::Dispose()
#define Enumerator_Dispose_m18990885(__this, method) ((  void (*) (Enumerator_t3719495245 *, const RuntimeMethod*))Enumerator_Dispose_m18990885_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.UIVertex>::MoveNext()
#define Enumerator_MoveNext_m1509398310(__this, method) ((  bool (*) (Enumerator_t3719495245 *, const RuntimeMethod*))Enumerator_MoveNext_m1509398310_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<UnityEngine.UIVertex>::get_Current()
#define Enumerator_get_Current_m2212027098(__this, method) ((  UIVertex_t2786467440  (*) (Enumerator_t3719495245 *, const RuntimeMethod*))Enumerator_get_Current_m2212027098_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Vector2>::.ctor(System.Collections.Generic.List`1<T>)
#define Enumerator__ctor_m3347994582(__this, ___l0, method) ((  void (*) (Enumerator_t215393771 *, List_1_t2432482469 *, const RuntimeMethod*))Enumerator__ctor_m3347994582_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Vector2>::VerifyState()
#define Enumerator_VerifyState_m3589629877(__this, method) ((  void (*) (Enumerator_t215393771 *, const RuntimeMethod*))Enumerator_VerifyState_m3589629877_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Vector2>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m2221689747(__this, method) ((  void (*) (Enumerator_t215393771 *, const RuntimeMethod*))Enumerator_System_Collections_IEnumerator_Reset_m2221689747_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<UnityEngine.Vector2>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m741422485(__this, method) ((  RuntimeObject * (*) (Enumerator_t215393771 *, const RuntimeMethod*))Enumerator_System_Collections_IEnumerator_get_Current_m741422485_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Vector2>::Dispose()
#define Enumerator_Dispose_m3955780047(__this, method) ((  void (*) (Enumerator_t215393771 *, const RuntimeMethod*))Enumerator_Dispose_m3955780047_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.Vector2>::MoveNext()
#define Enumerator_MoveNext_m183044627(__this, method) ((  bool (*) (Enumerator_t215393771 *, const RuntimeMethod*))Enumerator_MoveNext_m183044627_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<UnityEngine.Vector2>::get_Current()
#define Enumerator_get_Current_m2292278728(__this, method) ((  Vector2_t3577333262  (*) (Enumerator_t215393771 *, const RuntimeMethod*))Enumerator_get_Current_m2292278728_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Vector3>::.ctor(System.Collections.Generic.List`1<T>)
#define Enumerator__ctor_m904992548(__this, ___l0, method) ((  void (*) (Enumerator_t3836558239 *, List_1_t1758679641 *, const RuntimeMethod*))Enumerator__ctor_m904992548_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Vector3>::VerifyState()
#define Enumerator_VerifyState_m2353471952(__this, method) ((  void (*) (Enumerator_t3836558239 *, const RuntimeMethod*))Enumerator_VerifyState_m2353471952_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Vector3>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m3019879980(__this, method) ((  void (*) (Enumerator_t3836558239 *, const RuntimeMethod*))Enumerator_System_Collections_IEnumerator_Reset_m3019879980_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<UnityEngine.Vector3>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m1440485986(__this, method) ((  RuntimeObject * (*) (Enumerator_t3836558239 *, const RuntimeMethod*))Enumerator_System_Collections_IEnumerator_get_Current_m1440485986_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Vector3>::Dispose()
#define Enumerator_Dispose_m523694157(__this, method) ((  void (*) (Enumerator_t3836558239 *, const RuntimeMethod*))Enumerator_Dispose_m523694157_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.Vector3>::MoveNext()
#define Enumerator_MoveNext_m2371540642(__this, method) ((  bool (*) (Enumerator_t3836558239 *, const RuntimeMethod*))Enumerator_MoveNext_m2371540642_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<UnityEngine.Vector3>::get_Current()
#define Enumerator_get_Current_m1590935863(__this, method) ((  Vector3_t2903530434  (*) (Enumerator_t3836558239 *, const RuntimeMethod*))Enumerator_get_Current_m1590935863_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Vector4>::.ctor(System.Collections.Generic.List`1<T>)
#define Enumerator__ctor_m2410038664(__this, ___l0, method) ((  void (*) (Enumerator_t3747700150 *, List_1_t1669821552 *, const RuntimeMethod*))Enumerator__ctor_m2410038664_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Vector4>::VerifyState()
#define Enumerator_VerifyState_m4257143980(__this, method) ((  void (*) (Enumerator_t3747700150 *, const RuntimeMethod*))Enumerator_VerifyState_m4257143980_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Vector4>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m888757397(__this, method) ((  void (*) (Enumerator_t3747700150 *, const RuntimeMethod*))Enumerator_System_Collections_IEnumerator_Reset_m888757397_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<UnityEngine.Vector4>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m2740647926(__this, method) ((  RuntimeObject * (*) (Enumerator_t3747700150 *, const RuntimeMethod*))Enumerator_System_Collections_IEnumerator_get_Current_m2740647926_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Vector4>::Dispose()
#define Enumerator_Dispose_m173057505(__this, method) ((  void (*) (Enumerator_t3747700150 *, const RuntimeMethod*))Enumerator_Dispose_m173057505_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.Vector4>::MoveNext()
#define Enumerator_MoveNext_m675284825(__this, method) ((  bool (*) (Enumerator_t3747700150 *, const RuntimeMethod*))Enumerator_MoveNext_m675284825_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<UnityEngine.Vector4>::get_Current()
#define Enumerator_get_Current_m2462407644(__this, method) ((  Vector4_t2814672345  (*) (Enumerator_t3747700150 *, const RuntimeMethod*))Enumerator_get_Current_m2462407644_gshared)(__this, method)
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m3779811036_gshared (KeyValuePair_2_t2536236643 * __this, int32_t ___key0, RuntimeObject * ___value1, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___key0;
		KeyValuePair_2_set_Key_m3512996173((KeyValuePair_2_t2536236643 *)__this, (int32_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		RuntimeObject * L_1 = ___value1;
		KeyValuePair_2_set_Value_m3337772920((KeyValuePair_2_t2536236643 *)__this, (RuntimeObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return;
	}
}
extern "C"  void KeyValuePair_2__ctor_m3779811036_AdjustorThunk (RuntimeObject * __this, int32_t ___key0, RuntimeObject * ___value1, const RuntimeMethod* method)
{
	KeyValuePair_2_t2536236643 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t2536236643 *>(__this + 1);
	KeyValuePair_2__ctor_m3779811036(_thisAdjusted, ___key0, ___value1, method);
}
// TKey System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>::get_Key()
extern "C"  int32_t KeyValuePair_2_get_Key_m15997923_gshared (KeyValuePair_2_t2536236643 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = (int32_t)__this->get_key_0();
		return L_0;
	}
}
extern "C"  int32_t KeyValuePair_2_get_Key_m15997923_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	KeyValuePair_2_t2536236643 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t2536236643 *>(__this + 1);
	return KeyValuePair_2_get_Key_m15997923(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>::set_Key(TKey)
extern "C"  void KeyValuePair_2_set_Key_m3512996173_gshared (KeyValuePair_2_t2536236643 * __this, int32_t ___value0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_key_0(L_0);
		return;
	}
}
extern "C"  void KeyValuePair_2_set_Key_m3512996173_AdjustorThunk (RuntimeObject * __this, int32_t ___value0, const RuntimeMethod* method)
{
	KeyValuePair_2_t2536236643 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t2536236643 *>(__this + 1);
	KeyValuePair_2_set_Key_m3512996173(_thisAdjusted, ___value0, method);
}
// TValue System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>::get_Value()
extern "C"  RuntimeObject * KeyValuePair_2_get_Value_m699117648_gshared (KeyValuePair_2_t2536236643 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = (RuntimeObject *)__this->get_value_1();
		return L_0;
	}
}
extern "C"  RuntimeObject * KeyValuePair_2_get_Value_m699117648_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	KeyValuePair_2_t2536236643 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t2536236643 *>(__this + 1);
	return KeyValuePair_2_get_Value_m699117648(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>::set_Value(TValue)
extern "C"  void KeyValuePair_2_set_Value_m3337772920_gshared (KeyValuePair_2_t2536236643 * __this, RuntimeObject * ___value0, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = ___value0;
		__this->set_value_1(L_0);
		return;
	}
}
extern "C"  void KeyValuePair_2_set_Value_m3337772920_AdjustorThunk (RuntimeObject * __this, RuntimeObject * ___value0, const RuntimeMethod* method)
{
	KeyValuePair_2_t2536236643 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t2536236643 *>(__this + 1);
	KeyValuePair_2_set_Value_m3337772920(_thisAdjusted, ___value0, method);
}
// System.String System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>::ToString()
extern "C"  String_t* KeyValuePair_2_ToString_m3144124502_gshared (KeyValuePair_2_t2536236643 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KeyValuePair_2_ToString_m3144124502_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	RuntimeObject * V_1 = NULL;
	int32_t G_B2_0 = 0;
	StringU5BU5D_t2298632947* G_B2_1 = NULL;
	StringU5BU5D_t2298632947* G_B2_2 = NULL;
	int32_t G_B1_0 = 0;
	StringU5BU5D_t2298632947* G_B1_1 = NULL;
	StringU5BU5D_t2298632947* G_B1_2 = NULL;
	String_t* G_B3_0 = NULL;
	int32_t G_B3_1 = 0;
	StringU5BU5D_t2298632947* G_B3_2 = NULL;
	StringU5BU5D_t2298632947* G_B3_3 = NULL;
	int32_t G_B5_0 = 0;
	StringU5BU5D_t2298632947* G_B5_1 = NULL;
	StringU5BU5D_t2298632947* G_B5_2 = NULL;
	int32_t G_B4_0 = 0;
	StringU5BU5D_t2298632947* G_B4_1 = NULL;
	StringU5BU5D_t2298632947* G_B4_2 = NULL;
	String_t* G_B6_0 = NULL;
	int32_t G_B6_1 = 0;
	StringU5BU5D_t2298632947* G_B6_2 = NULL;
	StringU5BU5D_t2298632947* G_B6_3 = NULL;
	{
		StringU5BU5D_t2298632947* L_0 = (StringU5BU5D_t2298632947*)((StringU5BU5D_t2298632947*)SZArrayNew(StringU5BU5D_t2298632947_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_0);
		ArrayElementTypeCheck (L_0, _stringLiteral3984347688);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral3984347688);
		StringU5BU5D_t2298632947* L_1 = (StringU5BU5D_t2298632947*)L_0;
		int32_t L_2 = KeyValuePair_2_get_Key_m15997923((KeyValuePair_2_t2536236643 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		G_B1_0 = 1;
		G_B1_1 = L_1;
		G_B1_2 = L_1;
	}
	{
		int32_t L_3 = KeyValuePair_2_get_Key_m15997923((KeyValuePair_2_t2536236643 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		V_0 = (int32_t)L_3;
		String_t* L_4 = Int32_ToString_m3061240541((int32_t*)(&V_0), /*hidden argument*/NULL);
		G_B3_0 = L_4;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		G_B3_3 = G_B1_2;
		goto IL_003e;
	}

IL_0039:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = ((String_t_StaticFields*)il2cpp_codegen_static_fields_for(String_t_il2cpp_TypeInfo_var))->get_Empty_2();
		G_B3_0 = L_5;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
		G_B3_3 = G_B2_2;
	}

IL_003e:
	{
		NullCheck(G_B3_2);
		ArrayElementTypeCheck (G_B3_2, G_B3_0);
		(G_B3_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B3_1), (String_t*)G_B3_0);
		StringU5BU5D_t2298632947* L_6 = (StringU5BU5D_t2298632947*)G_B3_3;
		NullCheck(L_6);
		ArrayElementTypeCheck (L_6, _stringLiteral1084017900);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral1084017900);
		StringU5BU5D_t2298632947* L_7 = (StringU5BU5D_t2298632947*)L_6;
		RuntimeObject * L_8 = KeyValuePair_2_get_Value_m699117648((KeyValuePair_2_t2536236643 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		G_B4_0 = 3;
		G_B4_1 = L_7;
		G_B4_2 = L_7;
		if (!L_8)
		{
			G_B5_0 = 3;
			G_B5_1 = L_7;
			G_B5_2 = L_7;
			goto IL_0072;
		}
	}
	{
		RuntimeObject * L_9 = KeyValuePair_2_get_Value_m699117648((KeyValuePair_2_t2536236643 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		V_1 = (RuntimeObject *)L_9;
		NullCheck((RuntimeObject *)(*(&V_1)));
		String_t* L_10 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (RuntimeObject *)(*(&V_1)));
		G_B6_0 = L_10;
		G_B6_1 = G_B4_0;
		G_B6_2 = G_B4_1;
		G_B6_3 = G_B4_2;
		goto IL_0077;
	}

IL_0072:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_11 = ((String_t_StaticFields*)il2cpp_codegen_static_fields_for(String_t_il2cpp_TypeInfo_var))->get_Empty_2();
		G_B6_0 = L_11;
		G_B6_1 = G_B5_0;
		G_B6_2 = G_B5_1;
		G_B6_3 = G_B5_2;
	}

IL_0077:
	{
		NullCheck(G_B6_2);
		ArrayElementTypeCheck (G_B6_2, G_B6_0);
		(G_B6_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B6_1), (String_t*)G_B6_0);
		StringU5BU5D_t2298632947* L_12 = (StringU5BU5D_t2298632947*)G_B6_3;
		NullCheck(L_12);
		ArrayElementTypeCheck (L_12, _stringLiteral1238787106);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteral1238787106);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_13 = String_Concat_m2562457838(NULL /*static, unused*/, (StringU5BU5D_t2298632947*)L_12, /*hidden argument*/NULL);
		return L_13;
	}
}
extern "C"  String_t* KeyValuePair_2_ToString_m3144124502_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	KeyValuePair_2_t2536236643 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t2536236643 *>(__this + 1);
	return KeyValuePair_2_ToString_m3144124502(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.TrackableBehaviour/Status>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m3381747994_gshared (KeyValuePair_2_t1872397222 * __this, int32_t ___key0, int32_t ___value1, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___key0;
		KeyValuePair_2_set_Key_m34282199((KeyValuePair_2_t1872397222 *)__this, (int32_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_1 = ___value1;
		KeyValuePair_2_set_Value_m1414687358((KeyValuePair_2_t1872397222 *)__this, (int32_t)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return;
	}
}
extern "C"  void KeyValuePair_2__ctor_m3381747994_AdjustorThunk (RuntimeObject * __this, int32_t ___key0, int32_t ___value1, const RuntimeMethod* method)
{
	KeyValuePair_2_t1872397222 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t1872397222 *>(__this + 1);
	KeyValuePair_2__ctor_m3381747994(_thisAdjusted, ___key0, ___value1, method);
}
// TKey System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.TrackableBehaviour/Status>::get_Key()
extern "C"  int32_t KeyValuePair_2_get_Key_m2266079803_gshared (KeyValuePair_2_t1872397222 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = (int32_t)__this->get_key_0();
		return L_0;
	}
}
extern "C"  int32_t KeyValuePair_2_get_Key_m2266079803_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	KeyValuePair_2_t1872397222 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t1872397222 *>(__this + 1);
	return KeyValuePair_2_get_Key_m2266079803(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.TrackableBehaviour/Status>::set_Key(TKey)
extern "C"  void KeyValuePair_2_set_Key_m34282199_gshared (KeyValuePair_2_t1872397222 * __this, int32_t ___value0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_key_0(L_0);
		return;
	}
}
extern "C"  void KeyValuePair_2_set_Key_m34282199_AdjustorThunk (RuntimeObject * __this, int32_t ___value0, const RuntimeMethod* method)
{
	KeyValuePair_2_t1872397222 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t1872397222 *>(__this + 1);
	KeyValuePair_2_set_Key_m34282199(_thisAdjusted, ___value0, method);
}
// TValue System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.TrackableBehaviour/Status>::get_Value()
extern "C"  int32_t KeyValuePair_2_get_Value_m2289943185_gshared (KeyValuePair_2_t1872397222 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = (int32_t)__this->get_value_1();
		return L_0;
	}
}
extern "C"  int32_t KeyValuePair_2_get_Value_m2289943185_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	KeyValuePair_2_t1872397222 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t1872397222 *>(__this + 1);
	return KeyValuePair_2_get_Value_m2289943185(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.TrackableBehaviour/Status>::set_Value(TValue)
extern "C"  void KeyValuePair_2_set_Value_m1414687358_gshared (KeyValuePair_2_t1872397222 * __this, int32_t ___value0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_value_1(L_0);
		return;
	}
}
extern "C"  void KeyValuePair_2_set_Value_m1414687358_AdjustorThunk (RuntimeObject * __this, int32_t ___value0, const RuntimeMethod* method)
{
	KeyValuePair_2_t1872397222 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t1872397222 *>(__this + 1);
	KeyValuePair_2_set_Value_m1414687358(_thisAdjusted, ___value0, method);
}
// System.String System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.TrackableBehaviour/Status>::ToString()
extern "C"  String_t* KeyValuePair_2_ToString_m3380882685_gshared (KeyValuePair_2_t1872397222 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KeyValuePair_2_ToString_m3380882685_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t G_B2_0 = 0;
	StringU5BU5D_t2298632947* G_B2_1 = NULL;
	StringU5BU5D_t2298632947* G_B2_2 = NULL;
	int32_t G_B1_0 = 0;
	StringU5BU5D_t2298632947* G_B1_1 = NULL;
	StringU5BU5D_t2298632947* G_B1_2 = NULL;
	String_t* G_B3_0 = NULL;
	int32_t G_B3_1 = 0;
	StringU5BU5D_t2298632947* G_B3_2 = NULL;
	StringU5BU5D_t2298632947* G_B3_3 = NULL;
	int32_t G_B5_0 = 0;
	StringU5BU5D_t2298632947* G_B5_1 = NULL;
	StringU5BU5D_t2298632947* G_B5_2 = NULL;
	int32_t G_B4_0 = 0;
	StringU5BU5D_t2298632947* G_B4_1 = NULL;
	StringU5BU5D_t2298632947* G_B4_2 = NULL;
	String_t* G_B6_0 = NULL;
	int32_t G_B6_1 = 0;
	StringU5BU5D_t2298632947* G_B6_2 = NULL;
	StringU5BU5D_t2298632947* G_B6_3 = NULL;
	{
		StringU5BU5D_t2298632947* L_0 = (StringU5BU5D_t2298632947*)((StringU5BU5D_t2298632947*)SZArrayNew(StringU5BU5D_t2298632947_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_0);
		ArrayElementTypeCheck (L_0, _stringLiteral3984347688);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral3984347688);
		StringU5BU5D_t2298632947* L_1 = (StringU5BU5D_t2298632947*)L_0;
		int32_t L_2 = KeyValuePair_2_get_Key_m2266079803((KeyValuePair_2_t1872397222 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		G_B1_0 = 1;
		G_B1_1 = L_1;
		G_B1_2 = L_1;
	}
	{
		int32_t L_3 = KeyValuePair_2_get_Key_m2266079803((KeyValuePair_2_t1872397222 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		V_0 = (int32_t)L_3;
		String_t* L_4 = Int32_ToString_m3061240541((int32_t*)(&V_0), /*hidden argument*/NULL);
		G_B3_0 = L_4;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		G_B3_3 = G_B1_2;
		goto IL_003e;
	}

IL_0039:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = ((String_t_StaticFields*)il2cpp_codegen_static_fields_for(String_t_il2cpp_TypeInfo_var))->get_Empty_2();
		G_B3_0 = L_5;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
		G_B3_3 = G_B2_2;
	}

IL_003e:
	{
		NullCheck(G_B3_2);
		ArrayElementTypeCheck (G_B3_2, G_B3_0);
		(G_B3_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B3_1), (String_t*)G_B3_0);
		StringU5BU5D_t2298632947* L_6 = (StringU5BU5D_t2298632947*)G_B3_3;
		NullCheck(L_6);
		ArrayElementTypeCheck (L_6, _stringLiteral1084017900);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral1084017900);
		StringU5BU5D_t2298632947* L_7 = (StringU5BU5D_t2298632947*)L_6;
		int32_t L_8 = KeyValuePair_2_get_Value_m2289943185((KeyValuePair_2_t1872397222 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		G_B4_0 = 3;
		G_B4_1 = L_7;
		G_B4_2 = L_7;
	}
	{
		int32_t L_9 = KeyValuePair_2_get_Value_m2289943185((KeyValuePair_2_t1872397222 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		V_1 = (int32_t)L_9;
		RuntimeObject * L_10 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), (&V_1));
		NullCheck((RuntimeObject *)L_10);
		String_t* L_11 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (RuntimeObject *)L_10);
		*(&V_1) = *(int32_t*)UnBox(L_10);
		G_B6_0 = L_11;
		G_B6_1 = G_B4_0;
		G_B6_2 = G_B4_1;
		G_B6_3 = G_B4_2;
		goto IL_0077;
	}

IL_0072:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_12 = ((String_t_StaticFields*)il2cpp_codegen_static_fields_for(String_t_il2cpp_TypeInfo_var))->get_Empty_2();
		G_B6_0 = L_12;
		G_B6_1 = G_B5_0;
		G_B6_2 = G_B5_1;
		G_B6_3 = G_B5_2;
	}

IL_0077:
	{
		NullCheck(G_B6_2);
		ArrayElementTypeCheck (G_B6_2, G_B6_0);
		(G_B6_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B6_1), (String_t*)G_B6_0);
		StringU5BU5D_t2298632947* L_13 = (StringU5BU5D_t2298632947*)G_B6_3;
		NullCheck(L_13);
		ArrayElementTypeCheck (L_13, _stringLiteral1238787106);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteral1238787106);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = String_Concat_m2562457838(NULL /*static, unused*/, (StringU5BU5D_t2298632947*)L_13, /*hidden argument*/NULL);
		return L_14;
	}
}
extern "C"  String_t* KeyValuePair_2_ToString_m3380882685_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	KeyValuePair_2_t1872397222 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t1872397222 *>(__this + 1);
	return KeyValuePair_2_ToString_m3380882685(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m113285641_gshared (KeyValuePair_2_t432866562 * __this, int32_t ___key0, VirtualButtonData_t4213555505  ___value1, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___key0;
		KeyValuePair_2_set_Key_m979245165((KeyValuePair_2_t432866562 *)__this, (int32_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		VirtualButtonData_t4213555505  L_1 = ___value1;
		KeyValuePair_2_set_Value_m3827392229((KeyValuePair_2_t432866562 *)__this, (VirtualButtonData_t4213555505 )L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return;
	}
}
extern "C"  void KeyValuePair_2__ctor_m113285641_AdjustorThunk (RuntimeObject * __this, int32_t ___key0, VirtualButtonData_t4213555505  ___value1, const RuntimeMethod* method)
{
	KeyValuePair_2_t432866562 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t432866562 *>(__this + 1);
	KeyValuePair_2__ctor_m113285641(_thisAdjusted, ___key0, ___value1, method);
}
// TKey System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::get_Key()
extern "C"  int32_t KeyValuePair_2_get_Key_m1292857486_gshared (KeyValuePair_2_t432866562 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = (int32_t)__this->get_key_0();
		return L_0;
	}
}
extern "C"  int32_t KeyValuePair_2_get_Key_m1292857486_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	KeyValuePair_2_t432866562 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t432866562 *>(__this + 1);
	return KeyValuePair_2_get_Key_m1292857486(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::set_Key(TKey)
extern "C"  void KeyValuePair_2_set_Key_m979245165_gshared (KeyValuePair_2_t432866562 * __this, int32_t ___value0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_key_0(L_0);
		return;
	}
}
extern "C"  void KeyValuePair_2_set_Key_m979245165_AdjustorThunk (RuntimeObject * __this, int32_t ___value0, const RuntimeMethod* method)
{
	KeyValuePair_2_t432866562 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t432866562 *>(__this + 1);
	KeyValuePair_2_set_Key_m979245165(_thisAdjusted, ___value0, method);
}
// TValue System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::get_Value()
extern "C"  VirtualButtonData_t4213555505  KeyValuePair_2_get_Value_m3345055990_gshared (KeyValuePair_2_t432866562 * __this, const RuntimeMethod* method)
{
	{
		VirtualButtonData_t4213555505  L_0 = (VirtualButtonData_t4213555505 )__this->get_value_1();
		return L_0;
	}
}
extern "C"  VirtualButtonData_t4213555505  KeyValuePair_2_get_Value_m3345055990_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	KeyValuePair_2_t432866562 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t432866562 *>(__this + 1);
	return KeyValuePair_2_get_Value_m3345055990(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::set_Value(TValue)
extern "C"  void KeyValuePair_2_set_Value_m3827392229_gshared (KeyValuePair_2_t432866562 * __this, VirtualButtonData_t4213555505  ___value0, const RuntimeMethod* method)
{
	{
		VirtualButtonData_t4213555505  L_0 = ___value0;
		__this->set_value_1(L_0);
		return;
	}
}
extern "C"  void KeyValuePair_2_set_Value_m3827392229_AdjustorThunk (RuntimeObject * __this, VirtualButtonData_t4213555505  ___value0, const RuntimeMethod* method)
{
	KeyValuePair_2_t432866562 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t432866562 *>(__this + 1);
	KeyValuePair_2_set_Value_m3827392229(_thisAdjusted, ___value0, method);
}
// System.String System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::ToString()
extern "C"  String_t* KeyValuePair_2_ToString_m3666435602_gshared (KeyValuePair_2_t432866562 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KeyValuePair_2_ToString_m3666435602_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	VirtualButtonData_t4213555505  V_1;
	memset(&V_1, 0, sizeof(V_1));
	int32_t G_B2_0 = 0;
	StringU5BU5D_t2298632947* G_B2_1 = NULL;
	StringU5BU5D_t2298632947* G_B2_2 = NULL;
	int32_t G_B1_0 = 0;
	StringU5BU5D_t2298632947* G_B1_1 = NULL;
	StringU5BU5D_t2298632947* G_B1_2 = NULL;
	String_t* G_B3_0 = NULL;
	int32_t G_B3_1 = 0;
	StringU5BU5D_t2298632947* G_B3_2 = NULL;
	StringU5BU5D_t2298632947* G_B3_3 = NULL;
	int32_t G_B5_0 = 0;
	StringU5BU5D_t2298632947* G_B5_1 = NULL;
	StringU5BU5D_t2298632947* G_B5_2 = NULL;
	int32_t G_B4_0 = 0;
	StringU5BU5D_t2298632947* G_B4_1 = NULL;
	StringU5BU5D_t2298632947* G_B4_2 = NULL;
	String_t* G_B6_0 = NULL;
	int32_t G_B6_1 = 0;
	StringU5BU5D_t2298632947* G_B6_2 = NULL;
	StringU5BU5D_t2298632947* G_B6_3 = NULL;
	{
		StringU5BU5D_t2298632947* L_0 = (StringU5BU5D_t2298632947*)((StringU5BU5D_t2298632947*)SZArrayNew(StringU5BU5D_t2298632947_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_0);
		ArrayElementTypeCheck (L_0, _stringLiteral3984347688);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral3984347688);
		StringU5BU5D_t2298632947* L_1 = (StringU5BU5D_t2298632947*)L_0;
		int32_t L_2 = KeyValuePair_2_get_Key_m1292857486((KeyValuePair_2_t432866562 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		G_B1_0 = 1;
		G_B1_1 = L_1;
		G_B1_2 = L_1;
	}
	{
		int32_t L_3 = KeyValuePair_2_get_Key_m1292857486((KeyValuePair_2_t432866562 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		V_0 = (int32_t)L_3;
		String_t* L_4 = Int32_ToString_m3061240541((int32_t*)(&V_0), /*hidden argument*/NULL);
		G_B3_0 = L_4;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		G_B3_3 = G_B1_2;
		goto IL_003e;
	}

IL_0039:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = ((String_t_StaticFields*)il2cpp_codegen_static_fields_for(String_t_il2cpp_TypeInfo_var))->get_Empty_2();
		G_B3_0 = L_5;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
		G_B3_3 = G_B2_2;
	}

IL_003e:
	{
		NullCheck(G_B3_2);
		ArrayElementTypeCheck (G_B3_2, G_B3_0);
		(G_B3_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B3_1), (String_t*)G_B3_0);
		StringU5BU5D_t2298632947* L_6 = (StringU5BU5D_t2298632947*)G_B3_3;
		NullCheck(L_6);
		ArrayElementTypeCheck (L_6, _stringLiteral1084017900);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral1084017900);
		StringU5BU5D_t2298632947* L_7 = (StringU5BU5D_t2298632947*)L_6;
		VirtualButtonData_t4213555505  L_8 = KeyValuePair_2_get_Value_m3345055990((KeyValuePair_2_t432866562 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		G_B4_0 = 3;
		G_B4_1 = L_7;
		G_B4_2 = L_7;
	}
	{
		VirtualButtonData_t4213555505  L_9 = KeyValuePair_2_get_Value_m3345055990((KeyValuePair_2_t432866562 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		V_1 = (VirtualButtonData_t4213555505 )L_9;
		RuntimeObject * L_10 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), (&V_1));
		NullCheck((RuntimeObject *)L_10);
		String_t* L_11 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (RuntimeObject *)L_10);
		*(&V_1) = *(VirtualButtonData_t4213555505 *)UnBox(L_10);
		G_B6_0 = L_11;
		G_B6_1 = G_B4_0;
		G_B6_2 = G_B4_1;
		G_B6_3 = G_B4_2;
		goto IL_0077;
	}

IL_0072:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_12 = ((String_t_StaticFields*)il2cpp_codegen_static_fields_for(String_t_il2cpp_TypeInfo_var))->get_Empty_2();
		G_B6_0 = L_12;
		G_B6_1 = G_B5_0;
		G_B6_2 = G_B5_1;
		G_B6_3 = G_B5_2;
	}

IL_0077:
	{
		NullCheck(G_B6_2);
		ArrayElementTypeCheck (G_B6_2, G_B6_0);
		(G_B6_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B6_1), (String_t*)G_B6_0);
		StringU5BU5D_t2298632947* L_13 = (StringU5BU5D_t2298632947*)G_B6_3;
		NullCheck(L_13);
		ArrayElementTypeCheck (L_13, _stringLiteral1238787106);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteral1238787106);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = String_Concat_m2562457838(NULL /*static, unused*/, (StringU5BU5D_t2298632947*)L_13, /*hidden argument*/NULL);
		return L_14;
	}
}
extern "C"  String_t* KeyValuePair_2_ToString_m3666435602_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	KeyValuePair_2_t432866562 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t432866562 *>(__this + 1);
	return KeyValuePair_2_ToString_m3666435602(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.IntPtr,System.Object>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m2085269269_gshared (KeyValuePair_2_t651451063 * __this, IntPtr_t ___key0, RuntimeObject * ___value1, const RuntimeMethod* method)
{
	{
		IntPtr_t L_0 = ___key0;
		KeyValuePair_2_set_Key_m2055790825((KeyValuePair_2_t651451063 *)__this, (IntPtr_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		RuntimeObject * L_1 = ___value1;
		KeyValuePair_2_set_Value_m1654871410((KeyValuePair_2_t651451063 *)__this, (RuntimeObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return;
	}
}
extern "C"  void KeyValuePair_2__ctor_m2085269269_AdjustorThunk (RuntimeObject * __this, IntPtr_t ___key0, RuntimeObject * ___value1, const RuntimeMethod* method)
{
	KeyValuePair_2_t651451063 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t651451063 *>(__this + 1);
	KeyValuePair_2__ctor_m2085269269(_thisAdjusted, ___key0, ___value1, method);
}
// TKey System.Collections.Generic.KeyValuePair`2<System.IntPtr,System.Object>::get_Key()
extern "C"  IntPtr_t KeyValuePair_2_get_Key_m2522224863_gshared (KeyValuePair_2_t651451063 * __this, const RuntimeMethod* method)
{
	{
		IntPtr_t L_0 = (IntPtr_t)__this->get_key_0();
		return L_0;
	}
}
extern "C"  IntPtr_t KeyValuePair_2_get_Key_m2522224863_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	KeyValuePair_2_t651451063 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t651451063 *>(__this + 1);
	return KeyValuePair_2_get_Key_m2522224863(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.IntPtr,System.Object>::set_Key(TKey)
extern "C"  void KeyValuePair_2_set_Key_m2055790825_gshared (KeyValuePair_2_t651451063 * __this, IntPtr_t ___value0, const RuntimeMethod* method)
{
	{
		IntPtr_t L_0 = ___value0;
		__this->set_key_0(L_0);
		return;
	}
}
extern "C"  void KeyValuePair_2_set_Key_m2055790825_AdjustorThunk (RuntimeObject * __this, IntPtr_t ___value0, const RuntimeMethod* method)
{
	KeyValuePair_2_t651451063 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t651451063 *>(__this + 1);
	KeyValuePair_2_set_Key_m2055790825(_thisAdjusted, ___value0, method);
}
// TValue System.Collections.Generic.KeyValuePair`2<System.IntPtr,System.Object>::get_Value()
extern "C"  RuntimeObject * KeyValuePair_2_get_Value_m3322101809_gshared (KeyValuePair_2_t651451063 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = (RuntimeObject *)__this->get_value_1();
		return L_0;
	}
}
extern "C"  RuntimeObject * KeyValuePair_2_get_Value_m3322101809_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	KeyValuePair_2_t651451063 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t651451063 *>(__this + 1);
	return KeyValuePair_2_get_Value_m3322101809(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.IntPtr,System.Object>::set_Value(TValue)
extern "C"  void KeyValuePair_2_set_Value_m1654871410_gshared (KeyValuePair_2_t651451063 * __this, RuntimeObject * ___value0, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = ___value0;
		__this->set_value_1(L_0);
		return;
	}
}
extern "C"  void KeyValuePair_2_set_Value_m1654871410_AdjustorThunk (RuntimeObject * __this, RuntimeObject * ___value0, const RuntimeMethod* method)
{
	KeyValuePair_2_t651451063 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t651451063 *>(__this + 1);
	KeyValuePair_2_set_Value_m1654871410(_thisAdjusted, ___value0, method);
}
// System.String System.Collections.Generic.KeyValuePair`2<System.IntPtr,System.Object>::ToString()
extern "C"  String_t* KeyValuePair_2_ToString_m1274882003_gshared (KeyValuePair_2_t651451063 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KeyValuePair_2_ToString_m1274882003_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IntPtr_t V_0;
	memset(&V_0, 0, sizeof(V_0));
	RuntimeObject * V_1 = NULL;
	int32_t G_B2_0 = 0;
	StringU5BU5D_t2298632947* G_B2_1 = NULL;
	StringU5BU5D_t2298632947* G_B2_2 = NULL;
	int32_t G_B1_0 = 0;
	StringU5BU5D_t2298632947* G_B1_1 = NULL;
	StringU5BU5D_t2298632947* G_B1_2 = NULL;
	String_t* G_B3_0 = NULL;
	int32_t G_B3_1 = 0;
	StringU5BU5D_t2298632947* G_B3_2 = NULL;
	StringU5BU5D_t2298632947* G_B3_3 = NULL;
	int32_t G_B5_0 = 0;
	StringU5BU5D_t2298632947* G_B5_1 = NULL;
	StringU5BU5D_t2298632947* G_B5_2 = NULL;
	int32_t G_B4_0 = 0;
	StringU5BU5D_t2298632947* G_B4_1 = NULL;
	StringU5BU5D_t2298632947* G_B4_2 = NULL;
	String_t* G_B6_0 = NULL;
	int32_t G_B6_1 = 0;
	StringU5BU5D_t2298632947* G_B6_2 = NULL;
	StringU5BU5D_t2298632947* G_B6_3 = NULL;
	{
		StringU5BU5D_t2298632947* L_0 = (StringU5BU5D_t2298632947*)((StringU5BU5D_t2298632947*)SZArrayNew(StringU5BU5D_t2298632947_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_0);
		ArrayElementTypeCheck (L_0, _stringLiteral3984347688);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral3984347688);
		StringU5BU5D_t2298632947* L_1 = (StringU5BU5D_t2298632947*)L_0;
		IntPtr_t L_2 = KeyValuePair_2_get_Key_m2522224863((KeyValuePair_2_t651451063 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		G_B1_0 = 1;
		G_B1_1 = L_1;
		G_B1_2 = L_1;
	}
	{
		IntPtr_t L_3 = KeyValuePair_2_get_Key_m2522224863((KeyValuePair_2_t651451063 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		V_0 = (IntPtr_t)L_3;
		String_t* L_4 = IntPtr_ToString_m4044351426((IntPtr_t*)(&V_0), /*hidden argument*/NULL);
		G_B3_0 = L_4;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		G_B3_3 = G_B1_2;
		goto IL_003e;
	}

IL_0039:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = ((String_t_StaticFields*)il2cpp_codegen_static_fields_for(String_t_il2cpp_TypeInfo_var))->get_Empty_2();
		G_B3_0 = L_5;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
		G_B3_3 = G_B2_2;
	}

IL_003e:
	{
		NullCheck(G_B3_2);
		ArrayElementTypeCheck (G_B3_2, G_B3_0);
		(G_B3_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B3_1), (String_t*)G_B3_0);
		StringU5BU5D_t2298632947* L_6 = (StringU5BU5D_t2298632947*)G_B3_3;
		NullCheck(L_6);
		ArrayElementTypeCheck (L_6, _stringLiteral1084017900);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral1084017900);
		StringU5BU5D_t2298632947* L_7 = (StringU5BU5D_t2298632947*)L_6;
		RuntimeObject * L_8 = KeyValuePair_2_get_Value_m3322101809((KeyValuePair_2_t651451063 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		G_B4_0 = 3;
		G_B4_1 = L_7;
		G_B4_2 = L_7;
		if (!L_8)
		{
			G_B5_0 = 3;
			G_B5_1 = L_7;
			G_B5_2 = L_7;
			goto IL_0072;
		}
	}
	{
		RuntimeObject * L_9 = KeyValuePair_2_get_Value_m3322101809((KeyValuePair_2_t651451063 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		V_1 = (RuntimeObject *)L_9;
		NullCheck((RuntimeObject *)(*(&V_1)));
		String_t* L_10 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (RuntimeObject *)(*(&V_1)));
		G_B6_0 = L_10;
		G_B6_1 = G_B4_0;
		G_B6_2 = G_B4_1;
		G_B6_3 = G_B4_2;
		goto IL_0077;
	}

IL_0072:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_11 = ((String_t_StaticFields*)il2cpp_codegen_static_fields_for(String_t_il2cpp_TypeInfo_var))->get_Empty_2();
		G_B6_0 = L_11;
		G_B6_1 = G_B5_0;
		G_B6_2 = G_B5_1;
		G_B6_3 = G_B5_2;
	}

IL_0077:
	{
		NullCheck(G_B6_2);
		ArrayElementTypeCheck (G_B6_2, G_B6_0);
		(G_B6_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B6_1), (String_t*)G_B6_0);
		StringU5BU5D_t2298632947* L_12 = (StringU5BU5D_t2298632947*)G_B6_3;
		NullCheck(L_12);
		ArrayElementTypeCheck (L_12, _stringLiteral1238787106);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteral1238787106);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_13 = String_Concat_m2562457838(NULL /*static, unused*/, (StringU5BU5D_t2298632947*)L_12, /*hidden argument*/NULL);
		return L_13;
	}
}
extern "C"  String_t* KeyValuePair_2_ToString_m1274882003_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	KeyValuePair_2_t651451063 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t651451063 *>(__this + 1);
	return KeyValuePair_2_ToString_m1274882003(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m2919652111_gshared (KeyValuePair_2_t115616549 * __this, RuntimeObject * ___key0, bool ___value1, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = ___key0;
		KeyValuePair_2_set_Key_m717171400((KeyValuePair_2_t115616549 *)__this, (RuntimeObject *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		bool L_1 = ___value1;
		KeyValuePair_2_set_Value_m781244180((KeyValuePair_2_t115616549 *)__this, (bool)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return;
	}
}
extern "C"  void KeyValuePair_2__ctor_m2919652111_AdjustorThunk (RuntimeObject * __this, RuntimeObject * ___key0, bool ___value1, const RuntimeMethod* method)
{
	KeyValuePair_2_t115616549 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t115616549 *>(__this + 1);
	KeyValuePair_2__ctor_m2919652111(_thisAdjusted, ___key0, ___value1, method);
}
// TKey System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>::get_Key()
extern "C"  RuntimeObject * KeyValuePair_2_get_Key_m3969882595_gshared (KeyValuePair_2_t115616549 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = (RuntimeObject *)__this->get_key_0();
		return L_0;
	}
}
extern "C"  RuntimeObject * KeyValuePair_2_get_Key_m3969882595_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	KeyValuePair_2_t115616549 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t115616549 *>(__this + 1);
	return KeyValuePair_2_get_Key_m3969882595(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>::set_Key(TKey)
extern "C"  void KeyValuePair_2_set_Key_m717171400_gshared (KeyValuePair_2_t115616549 * __this, RuntimeObject * ___value0, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = ___value0;
		__this->set_key_0(L_0);
		return;
	}
}
extern "C"  void KeyValuePair_2_set_Key_m717171400_AdjustorThunk (RuntimeObject * __this, RuntimeObject * ___value0, const RuntimeMethod* method)
{
	KeyValuePair_2_t115616549 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t115616549 *>(__this + 1);
	KeyValuePair_2_set_Key_m717171400(_thisAdjusted, ___value0, method);
}
// TValue System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>::get_Value()
extern "C"  bool KeyValuePair_2_get_Value_m4103818512_gshared (KeyValuePair_2_t115616549 * __this, const RuntimeMethod* method)
{
	{
		bool L_0 = (bool)__this->get_value_1();
		return L_0;
	}
}
extern "C"  bool KeyValuePair_2_get_Value_m4103818512_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	KeyValuePair_2_t115616549 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t115616549 *>(__this + 1);
	return KeyValuePair_2_get_Value_m4103818512(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>::set_Value(TValue)
extern "C"  void KeyValuePair_2_set_Value_m781244180_gshared (KeyValuePair_2_t115616549 * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		bool L_0 = ___value0;
		__this->set_value_1(L_0);
		return;
	}
}
extern "C"  void KeyValuePair_2_set_Value_m781244180_AdjustorThunk (RuntimeObject * __this, bool ___value0, const RuntimeMethod* method)
{
	KeyValuePair_2_t115616549 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t115616549 *>(__this + 1);
	KeyValuePair_2_set_Value_m781244180(_thisAdjusted, ___value0, method);
}
// System.String System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>::ToString()
extern "C"  String_t* KeyValuePair_2_ToString_m3427190393_gshared (KeyValuePair_2_t115616549 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KeyValuePair_2_ToString_m3427190393_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RuntimeObject * V_0 = NULL;
	bool V_1 = false;
	int32_t G_B2_0 = 0;
	StringU5BU5D_t2298632947* G_B2_1 = NULL;
	StringU5BU5D_t2298632947* G_B2_2 = NULL;
	int32_t G_B1_0 = 0;
	StringU5BU5D_t2298632947* G_B1_1 = NULL;
	StringU5BU5D_t2298632947* G_B1_2 = NULL;
	String_t* G_B3_0 = NULL;
	int32_t G_B3_1 = 0;
	StringU5BU5D_t2298632947* G_B3_2 = NULL;
	StringU5BU5D_t2298632947* G_B3_3 = NULL;
	int32_t G_B5_0 = 0;
	StringU5BU5D_t2298632947* G_B5_1 = NULL;
	StringU5BU5D_t2298632947* G_B5_2 = NULL;
	int32_t G_B4_0 = 0;
	StringU5BU5D_t2298632947* G_B4_1 = NULL;
	StringU5BU5D_t2298632947* G_B4_2 = NULL;
	String_t* G_B6_0 = NULL;
	int32_t G_B6_1 = 0;
	StringU5BU5D_t2298632947* G_B6_2 = NULL;
	StringU5BU5D_t2298632947* G_B6_3 = NULL;
	{
		StringU5BU5D_t2298632947* L_0 = (StringU5BU5D_t2298632947*)((StringU5BU5D_t2298632947*)SZArrayNew(StringU5BU5D_t2298632947_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_0);
		ArrayElementTypeCheck (L_0, _stringLiteral3984347688);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral3984347688);
		StringU5BU5D_t2298632947* L_1 = (StringU5BU5D_t2298632947*)L_0;
		RuntimeObject * L_2 = KeyValuePair_2_get_Key_m3969882595((KeyValuePair_2_t115616549 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		G_B1_0 = 1;
		G_B1_1 = L_1;
		G_B1_2 = L_1;
		if (!L_2)
		{
			G_B2_0 = 1;
			G_B2_1 = L_1;
			G_B2_2 = L_1;
			goto IL_0039;
		}
	}
	{
		RuntimeObject * L_3 = KeyValuePair_2_get_Key_m3969882595((KeyValuePair_2_t115616549 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		V_0 = (RuntimeObject *)L_3;
		NullCheck((RuntimeObject *)(*(&V_0)));
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (RuntimeObject *)(*(&V_0)));
		G_B3_0 = L_4;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		G_B3_3 = G_B1_2;
		goto IL_003e;
	}

IL_0039:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = ((String_t_StaticFields*)il2cpp_codegen_static_fields_for(String_t_il2cpp_TypeInfo_var))->get_Empty_2();
		G_B3_0 = L_5;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
		G_B3_3 = G_B2_2;
	}

IL_003e:
	{
		NullCheck(G_B3_2);
		ArrayElementTypeCheck (G_B3_2, G_B3_0);
		(G_B3_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B3_1), (String_t*)G_B3_0);
		StringU5BU5D_t2298632947* L_6 = (StringU5BU5D_t2298632947*)G_B3_3;
		NullCheck(L_6);
		ArrayElementTypeCheck (L_6, _stringLiteral1084017900);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral1084017900);
		StringU5BU5D_t2298632947* L_7 = (StringU5BU5D_t2298632947*)L_6;
		bool L_8 = KeyValuePair_2_get_Value_m4103818512((KeyValuePair_2_t115616549 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		G_B4_0 = 3;
		G_B4_1 = L_7;
		G_B4_2 = L_7;
	}
	{
		bool L_9 = KeyValuePair_2_get_Value_m4103818512((KeyValuePair_2_t115616549 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		V_1 = (bool)L_9;
		String_t* L_10 = Boolean_ToString_m3961468639((bool*)(&V_1), /*hidden argument*/NULL);
		G_B6_0 = L_10;
		G_B6_1 = G_B4_0;
		G_B6_2 = G_B4_1;
		G_B6_3 = G_B4_2;
		goto IL_0077;
	}

IL_0072:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_11 = ((String_t_StaticFields*)il2cpp_codegen_static_fields_for(String_t_il2cpp_TypeInfo_var))->get_Empty_2();
		G_B6_0 = L_11;
		G_B6_1 = G_B5_0;
		G_B6_2 = G_B5_1;
		G_B6_3 = G_B5_2;
	}

IL_0077:
	{
		NullCheck(G_B6_2);
		ArrayElementTypeCheck (G_B6_2, G_B6_0);
		(G_B6_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B6_1), (String_t*)G_B6_0);
		StringU5BU5D_t2298632947* L_12 = (StringU5BU5D_t2298632947*)G_B6_3;
		NullCheck(L_12);
		ArrayElementTypeCheck (L_12, _stringLiteral1238787106);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteral1238787106);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_13 = String_Concat_m2562457838(NULL /*static, unused*/, (StringU5BU5D_t2298632947*)L_12, /*hidden argument*/NULL);
		return L_13;
	}
}
extern "C"  String_t* KeyValuePair_2_ToString_m3427190393_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	KeyValuePair_2_t115616549 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t115616549 *>(__this + 1);
	return KeyValuePair_2_ToString_m3427190393(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m388328491_gshared (KeyValuePair_2_t1402716069 * __this, RuntimeObject * ___key0, int32_t ___value1, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = ___key0;
		KeyValuePair_2_set_Key_m3774274141((KeyValuePair_2_t1402716069 *)__this, (RuntimeObject *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_1 = ___value1;
		KeyValuePair_2_set_Value_m541298045((KeyValuePair_2_t1402716069 *)__this, (int32_t)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return;
	}
}
extern "C"  void KeyValuePair_2__ctor_m388328491_AdjustorThunk (RuntimeObject * __this, RuntimeObject * ___key0, int32_t ___value1, const RuntimeMethod* method)
{
	KeyValuePair_2_t1402716069 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t1402716069 *>(__this + 1);
	KeyValuePair_2__ctor_m388328491(_thisAdjusted, ___key0, ___value1, method);
}
// TKey System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>::get_Key()
extern "C"  RuntimeObject * KeyValuePair_2_get_Key_m1087122466_gshared (KeyValuePair_2_t1402716069 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = (RuntimeObject *)__this->get_key_0();
		return L_0;
	}
}
extern "C"  RuntimeObject * KeyValuePair_2_get_Key_m1087122466_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	KeyValuePair_2_t1402716069 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t1402716069 *>(__this + 1);
	return KeyValuePair_2_get_Key_m1087122466(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>::set_Key(TKey)
extern "C"  void KeyValuePair_2_set_Key_m3774274141_gshared (KeyValuePair_2_t1402716069 * __this, RuntimeObject * ___value0, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = ___value0;
		__this->set_key_0(L_0);
		return;
	}
}
extern "C"  void KeyValuePair_2_set_Key_m3774274141_AdjustorThunk (RuntimeObject * __this, RuntimeObject * ___value0, const RuntimeMethod* method)
{
	KeyValuePair_2_t1402716069 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t1402716069 *>(__this + 1);
	KeyValuePair_2_set_Key_m3774274141(_thisAdjusted, ___value0, method);
}
// TValue System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>::get_Value()
extern "C"  int32_t KeyValuePair_2_get_Value_m2458189361_gshared (KeyValuePair_2_t1402716069 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = (int32_t)__this->get_value_1();
		return L_0;
	}
}
extern "C"  int32_t KeyValuePair_2_get_Value_m2458189361_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	KeyValuePair_2_t1402716069 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t1402716069 *>(__this + 1);
	return KeyValuePair_2_get_Value_m2458189361(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>::set_Value(TValue)
extern "C"  void KeyValuePair_2_set_Value_m541298045_gshared (KeyValuePair_2_t1402716069 * __this, int32_t ___value0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_value_1(L_0);
		return;
	}
}
extern "C"  void KeyValuePair_2_set_Value_m541298045_AdjustorThunk (RuntimeObject * __this, int32_t ___value0, const RuntimeMethod* method)
{
	KeyValuePair_2_t1402716069 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t1402716069 *>(__this + 1);
	KeyValuePair_2_set_Value_m541298045(_thisAdjusted, ___value0, method);
}
// System.String System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>::ToString()
extern "C"  String_t* KeyValuePair_2_ToString_m729476552_gshared (KeyValuePair_2_t1402716069 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KeyValuePair_2_ToString_m729476552_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RuntimeObject * V_0 = NULL;
	int32_t V_1 = 0;
	int32_t G_B2_0 = 0;
	StringU5BU5D_t2298632947* G_B2_1 = NULL;
	StringU5BU5D_t2298632947* G_B2_2 = NULL;
	int32_t G_B1_0 = 0;
	StringU5BU5D_t2298632947* G_B1_1 = NULL;
	StringU5BU5D_t2298632947* G_B1_2 = NULL;
	String_t* G_B3_0 = NULL;
	int32_t G_B3_1 = 0;
	StringU5BU5D_t2298632947* G_B3_2 = NULL;
	StringU5BU5D_t2298632947* G_B3_3 = NULL;
	int32_t G_B5_0 = 0;
	StringU5BU5D_t2298632947* G_B5_1 = NULL;
	StringU5BU5D_t2298632947* G_B5_2 = NULL;
	int32_t G_B4_0 = 0;
	StringU5BU5D_t2298632947* G_B4_1 = NULL;
	StringU5BU5D_t2298632947* G_B4_2 = NULL;
	String_t* G_B6_0 = NULL;
	int32_t G_B6_1 = 0;
	StringU5BU5D_t2298632947* G_B6_2 = NULL;
	StringU5BU5D_t2298632947* G_B6_3 = NULL;
	{
		StringU5BU5D_t2298632947* L_0 = (StringU5BU5D_t2298632947*)((StringU5BU5D_t2298632947*)SZArrayNew(StringU5BU5D_t2298632947_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_0);
		ArrayElementTypeCheck (L_0, _stringLiteral3984347688);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral3984347688);
		StringU5BU5D_t2298632947* L_1 = (StringU5BU5D_t2298632947*)L_0;
		RuntimeObject * L_2 = KeyValuePair_2_get_Key_m1087122466((KeyValuePair_2_t1402716069 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		G_B1_0 = 1;
		G_B1_1 = L_1;
		G_B1_2 = L_1;
		if (!L_2)
		{
			G_B2_0 = 1;
			G_B2_1 = L_1;
			G_B2_2 = L_1;
			goto IL_0039;
		}
	}
	{
		RuntimeObject * L_3 = KeyValuePair_2_get_Key_m1087122466((KeyValuePair_2_t1402716069 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		V_0 = (RuntimeObject *)L_3;
		NullCheck((RuntimeObject *)(*(&V_0)));
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (RuntimeObject *)(*(&V_0)));
		G_B3_0 = L_4;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		G_B3_3 = G_B1_2;
		goto IL_003e;
	}

IL_0039:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = ((String_t_StaticFields*)il2cpp_codegen_static_fields_for(String_t_il2cpp_TypeInfo_var))->get_Empty_2();
		G_B3_0 = L_5;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
		G_B3_3 = G_B2_2;
	}

IL_003e:
	{
		NullCheck(G_B3_2);
		ArrayElementTypeCheck (G_B3_2, G_B3_0);
		(G_B3_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B3_1), (String_t*)G_B3_0);
		StringU5BU5D_t2298632947* L_6 = (StringU5BU5D_t2298632947*)G_B3_3;
		NullCheck(L_6);
		ArrayElementTypeCheck (L_6, _stringLiteral1084017900);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral1084017900);
		StringU5BU5D_t2298632947* L_7 = (StringU5BU5D_t2298632947*)L_6;
		int32_t L_8 = KeyValuePair_2_get_Value_m2458189361((KeyValuePair_2_t1402716069 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		G_B4_0 = 3;
		G_B4_1 = L_7;
		G_B4_2 = L_7;
	}
	{
		int32_t L_9 = KeyValuePair_2_get_Value_m2458189361((KeyValuePair_2_t1402716069 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		V_1 = (int32_t)L_9;
		String_t* L_10 = Int32_ToString_m3061240541((int32_t*)(&V_1), /*hidden argument*/NULL);
		G_B6_0 = L_10;
		G_B6_1 = G_B4_0;
		G_B6_2 = G_B4_1;
		G_B6_3 = G_B4_2;
		goto IL_0077;
	}

IL_0072:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_11 = ((String_t_StaticFields*)il2cpp_codegen_static_fields_for(String_t_il2cpp_TypeInfo_var))->get_Empty_2();
		G_B6_0 = L_11;
		G_B6_1 = G_B5_0;
		G_B6_2 = G_B5_1;
		G_B6_3 = G_B5_2;
	}

IL_0077:
	{
		NullCheck(G_B6_2);
		ArrayElementTypeCheck (G_B6_2, G_B6_0);
		(G_B6_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B6_1), (String_t*)G_B6_0);
		StringU5BU5D_t2298632947* L_12 = (StringU5BU5D_t2298632947*)G_B6_3;
		NullCheck(L_12);
		ArrayElementTypeCheck (L_12, _stringLiteral1238787106);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteral1238787106);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_13 = String_Concat_m2562457838(NULL /*static, unused*/, (StringU5BU5D_t2298632947*)L_12, /*hidden argument*/NULL);
		return L_13;
	}
}
extern "C"  String_t* KeyValuePair_2_ToString_m729476552_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	KeyValuePair_2_t1402716069 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t1402716069 *>(__this + 1);
	return KeyValuePair_2_ToString_m729476552(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m973662155_gshared (KeyValuePair_2_t3481697066 * __this, RuntimeObject * ___key0, RuntimeObject * ___value1, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = ___key0;
		KeyValuePair_2_set_Key_m3122105053((KeyValuePair_2_t3481697066 *)__this, (RuntimeObject *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		RuntimeObject * L_1 = ___value1;
		KeyValuePair_2_set_Value_m2394843442((KeyValuePair_2_t3481697066 *)__this, (RuntimeObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return;
	}
}
extern "C"  void KeyValuePair_2__ctor_m973662155_AdjustorThunk (RuntimeObject * __this, RuntimeObject * ___key0, RuntimeObject * ___value1, const RuntimeMethod* method)
{
	KeyValuePair_2_t3481697066 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t3481697066 *>(__this + 1);
	KeyValuePair_2__ctor_m973662155(_thisAdjusted, ___key0, ___value1, method);
}
// TKey System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>::get_Key()
extern "C"  RuntimeObject * KeyValuePair_2_get_Key_m4217124106_gshared (KeyValuePair_2_t3481697066 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = (RuntimeObject *)__this->get_key_0();
		return L_0;
	}
}
extern "C"  RuntimeObject * KeyValuePair_2_get_Key_m4217124106_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	KeyValuePair_2_t3481697066 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t3481697066 *>(__this + 1);
	return KeyValuePair_2_get_Key_m4217124106(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>::set_Key(TKey)
extern "C"  void KeyValuePair_2_set_Key_m3122105053_gshared (KeyValuePair_2_t3481697066 * __this, RuntimeObject * ___value0, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = ___value0;
		__this->set_key_0(L_0);
		return;
	}
}
extern "C"  void KeyValuePair_2_set_Key_m3122105053_AdjustorThunk (RuntimeObject * __this, RuntimeObject * ___value0, const RuntimeMethod* method)
{
	KeyValuePair_2_t3481697066 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t3481697066 *>(__this + 1);
	KeyValuePair_2_set_Key_m3122105053(_thisAdjusted, ___value0, method);
}
// TValue System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>::get_Value()
extern "C"  RuntimeObject * KeyValuePair_2_get_Value_m1734077897_gshared (KeyValuePair_2_t3481697066 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = (RuntimeObject *)__this->get_value_1();
		return L_0;
	}
}
extern "C"  RuntimeObject * KeyValuePair_2_get_Value_m1734077897_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	KeyValuePair_2_t3481697066 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t3481697066 *>(__this + 1);
	return KeyValuePair_2_get_Value_m1734077897(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>::set_Value(TValue)
extern "C"  void KeyValuePair_2_set_Value_m2394843442_gshared (KeyValuePair_2_t3481697066 * __this, RuntimeObject * ___value0, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = ___value0;
		__this->set_value_1(L_0);
		return;
	}
}
extern "C"  void KeyValuePair_2_set_Value_m2394843442_AdjustorThunk (RuntimeObject * __this, RuntimeObject * ___value0, const RuntimeMethod* method)
{
	KeyValuePair_2_t3481697066 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t3481697066 *>(__this + 1);
	KeyValuePair_2_set_Value_m2394843442(_thisAdjusted, ___value0, method);
}
// System.String System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>::ToString()
extern "C"  String_t* KeyValuePair_2_ToString_m3779009088_gshared (KeyValuePair_2_t3481697066 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KeyValuePair_2_ToString_m3779009088_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RuntimeObject * V_0 = NULL;
	RuntimeObject * V_1 = NULL;
	int32_t G_B2_0 = 0;
	StringU5BU5D_t2298632947* G_B2_1 = NULL;
	StringU5BU5D_t2298632947* G_B2_2 = NULL;
	int32_t G_B1_0 = 0;
	StringU5BU5D_t2298632947* G_B1_1 = NULL;
	StringU5BU5D_t2298632947* G_B1_2 = NULL;
	String_t* G_B3_0 = NULL;
	int32_t G_B3_1 = 0;
	StringU5BU5D_t2298632947* G_B3_2 = NULL;
	StringU5BU5D_t2298632947* G_B3_3 = NULL;
	int32_t G_B5_0 = 0;
	StringU5BU5D_t2298632947* G_B5_1 = NULL;
	StringU5BU5D_t2298632947* G_B5_2 = NULL;
	int32_t G_B4_0 = 0;
	StringU5BU5D_t2298632947* G_B4_1 = NULL;
	StringU5BU5D_t2298632947* G_B4_2 = NULL;
	String_t* G_B6_0 = NULL;
	int32_t G_B6_1 = 0;
	StringU5BU5D_t2298632947* G_B6_2 = NULL;
	StringU5BU5D_t2298632947* G_B6_3 = NULL;
	{
		StringU5BU5D_t2298632947* L_0 = (StringU5BU5D_t2298632947*)((StringU5BU5D_t2298632947*)SZArrayNew(StringU5BU5D_t2298632947_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_0);
		ArrayElementTypeCheck (L_0, _stringLiteral3984347688);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral3984347688);
		StringU5BU5D_t2298632947* L_1 = (StringU5BU5D_t2298632947*)L_0;
		RuntimeObject * L_2 = KeyValuePair_2_get_Key_m4217124106((KeyValuePair_2_t3481697066 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		G_B1_0 = 1;
		G_B1_1 = L_1;
		G_B1_2 = L_1;
		if (!L_2)
		{
			G_B2_0 = 1;
			G_B2_1 = L_1;
			G_B2_2 = L_1;
			goto IL_0039;
		}
	}
	{
		RuntimeObject * L_3 = KeyValuePair_2_get_Key_m4217124106((KeyValuePair_2_t3481697066 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		V_0 = (RuntimeObject *)L_3;
		NullCheck((RuntimeObject *)(*(&V_0)));
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (RuntimeObject *)(*(&V_0)));
		G_B3_0 = L_4;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		G_B3_3 = G_B1_2;
		goto IL_003e;
	}

IL_0039:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = ((String_t_StaticFields*)il2cpp_codegen_static_fields_for(String_t_il2cpp_TypeInfo_var))->get_Empty_2();
		G_B3_0 = L_5;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
		G_B3_3 = G_B2_2;
	}

IL_003e:
	{
		NullCheck(G_B3_2);
		ArrayElementTypeCheck (G_B3_2, G_B3_0);
		(G_B3_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B3_1), (String_t*)G_B3_0);
		StringU5BU5D_t2298632947* L_6 = (StringU5BU5D_t2298632947*)G_B3_3;
		NullCheck(L_6);
		ArrayElementTypeCheck (L_6, _stringLiteral1084017900);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral1084017900);
		StringU5BU5D_t2298632947* L_7 = (StringU5BU5D_t2298632947*)L_6;
		RuntimeObject * L_8 = KeyValuePair_2_get_Value_m1734077897((KeyValuePair_2_t3481697066 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		G_B4_0 = 3;
		G_B4_1 = L_7;
		G_B4_2 = L_7;
		if (!L_8)
		{
			G_B5_0 = 3;
			G_B5_1 = L_7;
			G_B5_2 = L_7;
			goto IL_0072;
		}
	}
	{
		RuntimeObject * L_9 = KeyValuePair_2_get_Value_m1734077897((KeyValuePair_2_t3481697066 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		V_1 = (RuntimeObject *)L_9;
		NullCheck((RuntimeObject *)(*(&V_1)));
		String_t* L_10 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (RuntimeObject *)(*(&V_1)));
		G_B6_0 = L_10;
		G_B6_1 = G_B4_0;
		G_B6_2 = G_B4_1;
		G_B6_3 = G_B4_2;
		goto IL_0077;
	}

IL_0072:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_11 = ((String_t_StaticFields*)il2cpp_codegen_static_fields_for(String_t_il2cpp_TypeInfo_var))->get_Empty_2();
		G_B6_0 = L_11;
		G_B6_1 = G_B5_0;
		G_B6_2 = G_B5_1;
		G_B6_3 = G_B5_2;
	}

IL_0077:
	{
		NullCheck(G_B6_2);
		ArrayElementTypeCheck (G_B6_2, G_B6_0);
		(G_B6_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B6_1), (String_t*)G_B6_0);
		StringU5BU5D_t2298632947* L_12 = (StringU5BU5D_t2298632947*)G_B6_3;
		NullCheck(L_12);
		ArrayElementTypeCheck (L_12, _stringLiteral1238787106);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteral1238787106);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_13 = String_Concat_m2562457838(NULL /*static, unused*/, (StringU5BU5D_t2298632947*)L_12, /*hidden argument*/NULL);
		return L_13;
	}
}
extern "C"  String_t* KeyValuePair_2_ToString_m3779009088_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	KeyValuePair_2_t3481697066 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t3481697066 *>(__this + 1);
	return KeyValuePair_2_ToString_m3779009088(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt16>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m1511411907_gshared (KeyValuePair_2_t574427458 * __this, RuntimeObject * ___key0, uint16_t ___value1, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = ___key0;
		KeyValuePair_2_set_Key_m1444094661((KeyValuePair_2_t574427458 *)__this, (RuntimeObject *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		uint16_t L_1 = ___value1;
		KeyValuePair_2_set_Value_m655597244((KeyValuePair_2_t574427458 *)__this, (uint16_t)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return;
	}
}
extern "C"  void KeyValuePair_2__ctor_m1511411907_AdjustorThunk (RuntimeObject * __this, RuntimeObject * ___key0, uint16_t ___value1, const RuntimeMethod* method)
{
	KeyValuePair_2_t574427458 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t574427458 *>(__this + 1);
	KeyValuePair_2__ctor_m1511411907(_thisAdjusted, ___key0, ___value1, method);
}
// TKey System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt16>::get_Key()
extern "C"  RuntimeObject * KeyValuePair_2_get_Key_m2791227378_gshared (KeyValuePair_2_t574427458 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = (RuntimeObject *)__this->get_key_0();
		return L_0;
	}
}
extern "C"  RuntimeObject * KeyValuePair_2_get_Key_m2791227378_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	KeyValuePair_2_t574427458 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t574427458 *>(__this + 1);
	return KeyValuePair_2_get_Key_m2791227378(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt16>::set_Key(TKey)
extern "C"  void KeyValuePair_2_set_Key_m1444094661_gshared (KeyValuePair_2_t574427458 * __this, RuntimeObject * ___value0, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = ___value0;
		__this->set_key_0(L_0);
		return;
	}
}
extern "C"  void KeyValuePair_2_set_Key_m1444094661_AdjustorThunk (RuntimeObject * __this, RuntimeObject * ___value0, const RuntimeMethod* method)
{
	KeyValuePair_2_t574427458 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t574427458 *>(__this + 1);
	KeyValuePair_2_set_Key_m1444094661(_thisAdjusted, ___value0, method);
}
// TValue System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt16>::get_Value()
extern "C"  uint16_t KeyValuePair_2_get_Value_m2138563648_gshared (KeyValuePair_2_t574427458 * __this, const RuntimeMethod* method)
{
	{
		uint16_t L_0 = (uint16_t)__this->get_value_1();
		return L_0;
	}
}
extern "C"  uint16_t KeyValuePair_2_get_Value_m2138563648_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	KeyValuePair_2_t574427458 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t574427458 *>(__this + 1);
	return KeyValuePair_2_get_Value_m2138563648(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt16>::set_Value(TValue)
extern "C"  void KeyValuePair_2_set_Value_m655597244_gshared (KeyValuePair_2_t574427458 * __this, uint16_t ___value0, const RuntimeMethod* method)
{
	{
		uint16_t L_0 = ___value0;
		__this->set_value_1(L_0);
		return;
	}
}
extern "C"  void KeyValuePair_2_set_Value_m655597244_AdjustorThunk (RuntimeObject * __this, uint16_t ___value0, const RuntimeMethod* method)
{
	KeyValuePair_2_t574427458 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t574427458 *>(__this + 1);
	KeyValuePair_2_set_Value_m655597244(_thisAdjusted, ___value0, method);
}
// System.String System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt16>::ToString()
extern "C"  String_t* KeyValuePair_2_ToString_m1879012905_gshared (KeyValuePair_2_t574427458 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KeyValuePair_2_ToString_m1879012905_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RuntimeObject * V_0 = NULL;
	uint16_t V_1 = 0;
	int32_t G_B2_0 = 0;
	StringU5BU5D_t2298632947* G_B2_1 = NULL;
	StringU5BU5D_t2298632947* G_B2_2 = NULL;
	int32_t G_B1_0 = 0;
	StringU5BU5D_t2298632947* G_B1_1 = NULL;
	StringU5BU5D_t2298632947* G_B1_2 = NULL;
	String_t* G_B3_0 = NULL;
	int32_t G_B3_1 = 0;
	StringU5BU5D_t2298632947* G_B3_2 = NULL;
	StringU5BU5D_t2298632947* G_B3_3 = NULL;
	int32_t G_B5_0 = 0;
	StringU5BU5D_t2298632947* G_B5_1 = NULL;
	StringU5BU5D_t2298632947* G_B5_2 = NULL;
	int32_t G_B4_0 = 0;
	StringU5BU5D_t2298632947* G_B4_1 = NULL;
	StringU5BU5D_t2298632947* G_B4_2 = NULL;
	String_t* G_B6_0 = NULL;
	int32_t G_B6_1 = 0;
	StringU5BU5D_t2298632947* G_B6_2 = NULL;
	StringU5BU5D_t2298632947* G_B6_3 = NULL;
	{
		StringU5BU5D_t2298632947* L_0 = (StringU5BU5D_t2298632947*)((StringU5BU5D_t2298632947*)SZArrayNew(StringU5BU5D_t2298632947_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_0);
		ArrayElementTypeCheck (L_0, _stringLiteral3984347688);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral3984347688);
		StringU5BU5D_t2298632947* L_1 = (StringU5BU5D_t2298632947*)L_0;
		RuntimeObject * L_2 = KeyValuePair_2_get_Key_m2791227378((KeyValuePair_2_t574427458 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		G_B1_0 = 1;
		G_B1_1 = L_1;
		G_B1_2 = L_1;
		if (!L_2)
		{
			G_B2_0 = 1;
			G_B2_1 = L_1;
			G_B2_2 = L_1;
			goto IL_0039;
		}
	}
	{
		RuntimeObject * L_3 = KeyValuePair_2_get_Key_m2791227378((KeyValuePair_2_t574427458 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		V_0 = (RuntimeObject *)L_3;
		NullCheck((RuntimeObject *)(*(&V_0)));
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (RuntimeObject *)(*(&V_0)));
		G_B3_0 = L_4;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		G_B3_3 = G_B1_2;
		goto IL_003e;
	}

IL_0039:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = ((String_t_StaticFields*)il2cpp_codegen_static_fields_for(String_t_il2cpp_TypeInfo_var))->get_Empty_2();
		G_B3_0 = L_5;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
		G_B3_3 = G_B2_2;
	}

IL_003e:
	{
		NullCheck(G_B3_2);
		ArrayElementTypeCheck (G_B3_2, G_B3_0);
		(G_B3_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B3_1), (String_t*)G_B3_0);
		StringU5BU5D_t2298632947* L_6 = (StringU5BU5D_t2298632947*)G_B3_3;
		NullCheck(L_6);
		ArrayElementTypeCheck (L_6, _stringLiteral1084017900);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral1084017900);
		StringU5BU5D_t2298632947* L_7 = (StringU5BU5D_t2298632947*)L_6;
		uint16_t L_8 = KeyValuePair_2_get_Value_m2138563648((KeyValuePair_2_t574427458 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		G_B4_0 = 3;
		G_B4_1 = L_7;
		G_B4_2 = L_7;
	}
	{
		uint16_t L_9 = KeyValuePair_2_get_Value_m2138563648((KeyValuePair_2_t574427458 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		V_1 = (uint16_t)L_9;
		String_t* L_10 = UInt16_ToString_m496372703((uint16_t*)(&V_1), /*hidden argument*/NULL);
		G_B6_0 = L_10;
		G_B6_1 = G_B4_0;
		G_B6_2 = G_B4_1;
		G_B6_3 = G_B4_2;
		goto IL_0077;
	}

IL_0072:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_11 = ((String_t_StaticFields*)il2cpp_codegen_static_fields_for(String_t_il2cpp_TypeInfo_var))->get_Empty_2();
		G_B6_0 = L_11;
		G_B6_1 = G_B5_0;
		G_B6_2 = G_B5_1;
		G_B6_3 = G_B5_2;
	}

IL_0077:
	{
		NullCheck(G_B6_2);
		ArrayElementTypeCheck (G_B6_2, G_B6_0);
		(G_B6_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B6_1), (String_t*)G_B6_0);
		StringU5BU5D_t2298632947* L_12 = (StringU5BU5D_t2298632947*)G_B6_3;
		NullCheck(L_12);
		ArrayElementTypeCheck (L_12, _stringLiteral1238787106);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteral1238787106);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_13 = String_Concat_m2562457838(NULL /*static, unused*/, (StringU5BU5D_t2298632947*)L_12, /*hidden argument*/NULL);
		return L_13;
	}
}
extern "C"  String_t* KeyValuePair_2_ToString_m1879012905_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	KeyValuePair_2_t574427458 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t574427458 *>(__this + 1);
	return KeyValuePair_2_ToString_m1879012905(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,Vuforia.WebCamProfile/ProfileData>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m3327461017_gshared (KeyValuePair_2_t2200618205 * __this, RuntimeObject * ___key0, ProfileData_t740879429  ___value1, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = ___key0;
		KeyValuePair_2_set_Key_m2963078532((KeyValuePair_2_t2200618205 *)__this, (RuntimeObject *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		ProfileData_t740879429  L_1 = ___value1;
		KeyValuePair_2_set_Value_m3496892223((KeyValuePair_2_t2200618205 *)__this, (ProfileData_t740879429 )L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return;
	}
}
extern "C"  void KeyValuePair_2__ctor_m3327461017_AdjustorThunk (RuntimeObject * __this, RuntimeObject * ___key0, ProfileData_t740879429  ___value1, const RuntimeMethod* method)
{
	KeyValuePair_2_t2200618205 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t2200618205 *>(__this + 1);
	KeyValuePair_2__ctor_m3327461017(_thisAdjusted, ___key0, ___value1, method);
}
// TKey System.Collections.Generic.KeyValuePair`2<System.Object,Vuforia.WebCamProfile/ProfileData>::get_Key()
extern "C"  RuntimeObject * KeyValuePair_2_get_Key_m956489385_gshared (KeyValuePair_2_t2200618205 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = (RuntimeObject *)__this->get_key_0();
		return L_0;
	}
}
extern "C"  RuntimeObject * KeyValuePair_2_get_Key_m956489385_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	KeyValuePair_2_t2200618205 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t2200618205 *>(__this + 1);
	return KeyValuePair_2_get_Key_m956489385(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,Vuforia.WebCamProfile/ProfileData>::set_Key(TKey)
extern "C"  void KeyValuePair_2_set_Key_m2963078532_gshared (KeyValuePair_2_t2200618205 * __this, RuntimeObject * ___value0, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = ___value0;
		__this->set_key_0(L_0);
		return;
	}
}
extern "C"  void KeyValuePair_2_set_Key_m2963078532_AdjustorThunk (RuntimeObject * __this, RuntimeObject * ___value0, const RuntimeMethod* method)
{
	KeyValuePair_2_t2200618205 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t2200618205 *>(__this + 1);
	KeyValuePair_2_set_Key_m2963078532(_thisAdjusted, ___value0, method);
}
// TValue System.Collections.Generic.KeyValuePair`2<System.Object,Vuforia.WebCamProfile/ProfileData>::get_Value()
extern "C"  ProfileData_t740879429  KeyValuePair_2_get_Value_m1073695936_gshared (KeyValuePair_2_t2200618205 * __this, const RuntimeMethod* method)
{
	{
		ProfileData_t740879429  L_0 = (ProfileData_t740879429 )__this->get_value_1();
		return L_0;
	}
}
extern "C"  ProfileData_t740879429  KeyValuePair_2_get_Value_m1073695936_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	KeyValuePair_2_t2200618205 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t2200618205 *>(__this + 1);
	return KeyValuePair_2_get_Value_m1073695936(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,Vuforia.WebCamProfile/ProfileData>::set_Value(TValue)
extern "C"  void KeyValuePair_2_set_Value_m3496892223_gshared (KeyValuePair_2_t2200618205 * __this, ProfileData_t740879429  ___value0, const RuntimeMethod* method)
{
	{
		ProfileData_t740879429  L_0 = ___value0;
		__this->set_value_1(L_0);
		return;
	}
}
extern "C"  void KeyValuePair_2_set_Value_m3496892223_AdjustorThunk (RuntimeObject * __this, ProfileData_t740879429  ___value0, const RuntimeMethod* method)
{
	KeyValuePair_2_t2200618205 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t2200618205 *>(__this + 1);
	KeyValuePair_2_set_Value_m3496892223(_thisAdjusted, ___value0, method);
}
// System.String System.Collections.Generic.KeyValuePair`2<System.Object,Vuforia.WebCamProfile/ProfileData>::ToString()
extern "C"  String_t* KeyValuePair_2_ToString_m4250712101_gshared (KeyValuePair_2_t2200618205 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KeyValuePair_2_ToString_m4250712101_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RuntimeObject * V_0 = NULL;
	ProfileData_t740879429  V_1;
	memset(&V_1, 0, sizeof(V_1));
	int32_t G_B2_0 = 0;
	StringU5BU5D_t2298632947* G_B2_1 = NULL;
	StringU5BU5D_t2298632947* G_B2_2 = NULL;
	int32_t G_B1_0 = 0;
	StringU5BU5D_t2298632947* G_B1_1 = NULL;
	StringU5BU5D_t2298632947* G_B1_2 = NULL;
	String_t* G_B3_0 = NULL;
	int32_t G_B3_1 = 0;
	StringU5BU5D_t2298632947* G_B3_2 = NULL;
	StringU5BU5D_t2298632947* G_B3_3 = NULL;
	int32_t G_B5_0 = 0;
	StringU5BU5D_t2298632947* G_B5_1 = NULL;
	StringU5BU5D_t2298632947* G_B5_2 = NULL;
	int32_t G_B4_0 = 0;
	StringU5BU5D_t2298632947* G_B4_1 = NULL;
	StringU5BU5D_t2298632947* G_B4_2 = NULL;
	String_t* G_B6_0 = NULL;
	int32_t G_B6_1 = 0;
	StringU5BU5D_t2298632947* G_B6_2 = NULL;
	StringU5BU5D_t2298632947* G_B6_3 = NULL;
	{
		StringU5BU5D_t2298632947* L_0 = (StringU5BU5D_t2298632947*)((StringU5BU5D_t2298632947*)SZArrayNew(StringU5BU5D_t2298632947_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_0);
		ArrayElementTypeCheck (L_0, _stringLiteral3984347688);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral3984347688);
		StringU5BU5D_t2298632947* L_1 = (StringU5BU5D_t2298632947*)L_0;
		RuntimeObject * L_2 = KeyValuePair_2_get_Key_m956489385((KeyValuePair_2_t2200618205 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		G_B1_0 = 1;
		G_B1_1 = L_1;
		G_B1_2 = L_1;
		if (!L_2)
		{
			G_B2_0 = 1;
			G_B2_1 = L_1;
			G_B2_2 = L_1;
			goto IL_0039;
		}
	}
	{
		RuntimeObject * L_3 = KeyValuePair_2_get_Key_m956489385((KeyValuePair_2_t2200618205 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		V_0 = (RuntimeObject *)L_3;
		NullCheck((RuntimeObject *)(*(&V_0)));
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (RuntimeObject *)(*(&V_0)));
		G_B3_0 = L_4;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		G_B3_3 = G_B1_2;
		goto IL_003e;
	}

IL_0039:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = ((String_t_StaticFields*)il2cpp_codegen_static_fields_for(String_t_il2cpp_TypeInfo_var))->get_Empty_2();
		G_B3_0 = L_5;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
		G_B3_3 = G_B2_2;
	}

IL_003e:
	{
		NullCheck(G_B3_2);
		ArrayElementTypeCheck (G_B3_2, G_B3_0);
		(G_B3_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B3_1), (String_t*)G_B3_0);
		StringU5BU5D_t2298632947* L_6 = (StringU5BU5D_t2298632947*)G_B3_3;
		NullCheck(L_6);
		ArrayElementTypeCheck (L_6, _stringLiteral1084017900);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral1084017900);
		StringU5BU5D_t2298632947* L_7 = (StringU5BU5D_t2298632947*)L_6;
		ProfileData_t740879429  L_8 = KeyValuePair_2_get_Value_m1073695936((KeyValuePair_2_t2200618205 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		G_B4_0 = 3;
		G_B4_1 = L_7;
		G_B4_2 = L_7;
	}
	{
		ProfileData_t740879429  L_9 = KeyValuePair_2_get_Value_m1073695936((KeyValuePair_2_t2200618205 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		V_1 = (ProfileData_t740879429 )L_9;
		RuntimeObject * L_10 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), (&V_1));
		NullCheck((RuntimeObject *)L_10);
		String_t* L_11 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (RuntimeObject *)L_10);
		*(&V_1) = *(ProfileData_t740879429 *)UnBox(L_10);
		G_B6_0 = L_11;
		G_B6_1 = G_B4_0;
		G_B6_2 = G_B4_1;
		G_B6_3 = G_B4_2;
		goto IL_0077;
	}

IL_0072:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_12 = ((String_t_StaticFields*)il2cpp_codegen_static_fields_for(String_t_il2cpp_TypeInfo_var))->get_Empty_2();
		G_B6_0 = L_12;
		G_B6_1 = G_B5_0;
		G_B6_2 = G_B5_1;
		G_B6_3 = G_B5_2;
	}

IL_0077:
	{
		NullCheck(G_B6_2);
		ArrayElementTypeCheck (G_B6_2, G_B6_0);
		(G_B6_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B6_1), (String_t*)G_B6_0);
		StringU5BU5D_t2298632947* L_13 = (StringU5BU5D_t2298632947*)G_B6_3;
		NullCheck(L_13);
		ArrayElementTypeCheck (L_13, _stringLiteral1238787106);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteral1238787106);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = String_Concat_m2562457838(NULL /*static, unused*/, (StringU5BU5D_t2298632947*)L_13, /*hidden argument*/NULL);
		return L_14;
	}
}
extern "C"  String_t* KeyValuePair_2_ToString_m4250712101_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	KeyValuePair_2_t2200618205 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t2200618205 *>(__this + 1);
	return KeyValuePair_2_ToString_m4250712101(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<Vuforia.Image/PIXEL_FORMAT,System.Object>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m2367532738_gshared (KeyValuePair_2_t603276356 * __this, int32_t ___key0, RuntimeObject * ___value1, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___key0;
		KeyValuePair_2_set_Key_m1462435752((KeyValuePair_2_t603276356 *)__this, (int32_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		RuntimeObject * L_1 = ___value1;
		KeyValuePair_2_set_Value_m3866220215((KeyValuePair_2_t603276356 *)__this, (RuntimeObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return;
	}
}
extern "C"  void KeyValuePair_2__ctor_m2367532738_AdjustorThunk (RuntimeObject * __this, int32_t ___key0, RuntimeObject * ___value1, const RuntimeMethod* method)
{
	KeyValuePair_2_t603276356 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t603276356 *>(__this + 1);
	KeyValuePair_2__ctor_m2367532738(_thisAdjusted, ___key0, ___value1, method);
}
// TKey System.Collections.Generic.KeyValuePair`2<Vuforia.Image/PIXEL_FORMAT,System.Object>::get_Key()
extern "C"  int32_t KeyValuePair_2_get_Key_m3814775675_gshared (KeyValuePair_2_t603276356 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = (int32_t)__this->get_key_0();
		return L_0;
	}
}
extern "C"  int32_t KeyValuePair_2_get_Key_m3814775675_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	KeyValuePair_2_t603276356 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t603276356 *>(__this + 1);
	return KeyValuePair_2_get_Key_m3814775675(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<Vuforia.Image/PIXEL_FORMAT,System.Object>::set_Key(TKey)
extern "C"  void KeyValuePair_2_set_Key_m1462435752_gshared (KeyValuePair_2_t603276356 * __this, int32_t ___value0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_key_0(L_0);
		return;
	}
}
extern "C"  void KeyValuePair_2_set_Key_m1462435752_AdjustorThunk (RuntimeObject * __this, int32_t ___value0, const RuntimeMethod* method)
{
	KeyValuePair_2_t603276356 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t603276356 *>(__this + 1);
	KeyValuePair_2_set_Key_m1462435752(_thisAdjusted, ___value0, method);
}
// TValue System.Collections.Generic.KeyValuePair`2<Vuforia.Image/PIXEL_FORMAT,System.Object>::get_Value()
extern "C"  RuntimeObject * KeyValuePair_2_get_Value_m1253090796_gshared (KeyValuePair_2_t603276356 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = (RuntimeObject *)__this->get_value_1();
		return L_0;
	}
}
extern "C"  RuntimeObject * KeyValuePair_2_get_Value_m1253090796_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	KeyValuePair_2_t603276356 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t603276356 *>(__this + 1);
	return KeyValuePair_2_get_Value_m1253090796(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<Vuforia.Image/PIXEL_FORMAT,System.Object>::set_Value(TValue)
extern "C"  void KeyValuePair_2_set_Value_m3866220215_gshared (KeyValuePair_2_t603276356 * __this, RuntimeObject * ___value0, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = ___value0;
		__this->set_value_1(L_0);
		return;
	}
}
extern "C"  void KeyValuePair_2_set_Value_m3866220215_AdjustorThunk (RuntimeObject * __this, RuntimeObject * ___value0, const RuntimeMethod* method)
{
	KeyValuePair_2_t603276356 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t603276356 *>(__this + 1);
	KeyValuePair_2_set_Value_m3866220215(_thisAdjusted, ___value0, method);
}
// System.String System.Collections.Generic.KeyValuePair`2<Vuforia.Image/PIXEL_FORMAT,System.Object>::ToString()
extern "C"  String_t* KeyValuePair_2_ToString_m2759420978_gshared (KeyValuePair_2_t603276356 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KeyValuePair_2_ToString_m2759420978_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	RuntimeObject * V_1 = NULL;
	int32_t G_B2_0 = 0;
	StringU5BU5D_t2298632947* G_B2_1 = NULL;
	StringU5BU5D_t2298632947* G_B2_2 = NULL;
	int32_t G_B1_0 = 0;
	StringU5BU5D_t2298632947* G_B1_1 = NULL;
	StringU5BU5D_t2298632947* G_B1_2 = NULL;
	String_t* G_B3_0 = NULL;
	int32_t G_B3_1 = 0;
	StringU5BU5D_t2298632947* G_B3_2 = NULL;
	StringU5BU5D_t2298632947* G_B3_3 = NULL;
	int32_t G_B5_0 = 0;
	StringU5BU5D_t2298632947* G_B5_1 = NULL;
	StringU5BU5D_t2298632947* G_B5_2 = NULL;
	int32_t G_B4_0 = 0;
	StringU5BU5D_t2298632947* G_B4_1 = NULL;
	StringU5BU5D_t2298632947* G_B4_2 = NULL;
	String_t* G_B6_0 = NULL;
	int32_t G_B6_1 = 0;
	StringU5BU5D_t2298632947* G_B6_2 = NULL;
	StringU5BU5D_t2298632947* G_B6_3 = NULL;
	{
		StringU5BU5D_t2298632947* L_0 = (StringU5BU5D_t2298632947*)((StringU5BU5D_t2298632947*)SZArrayNew(StringU5BU5D_t2298632947_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_0);
		ArrayElementTypeCheck (L_0, _stringLiteral3984347688);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral3984347688);
		StringU5BU5D_t2298632947* L_1 = (StringU5BU5D_t2298632947*)L_0;
		int32_t L_2 = KeyValuePair_2_get_Key_m3814775675((KeyValuePair_2_t603276356 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		G_B1_0 = 1;
		G_B1_1 = L_1;
		G_B1_2 = L_1;
	}
	{
		int32_t L_3 = KeyValuePair_2_get_Key_m3814775675((KeyValuePair_2_t603276356 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		V_0 = (int32_t)L_3;
		RuntimeObject * L_4 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (&V_0));
		NullCheck((RuntimeObject *)L_4);
		String_t* L_5 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (RuntimeObject *)L_4);
		*(&V_0) = *(int32_t*)UnBox(L_4);
		G_B3_0 = L_5;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		G_B3_3 = G_B1_2;
		goto IL_003e;
	}

IL_0039:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_6 = ((String_t_StaticFields*)il2cpp_codegen_static_fields_for(String_t_il2cpp_TypeInfo_var))->get_Empty_2();
		G_B3_0 = L_6;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
		G_B3_3 = G_B2_2;
	}

IL_003e:
	{
		NullCheck(G_B3_2);
		ArrayElementTypeCheck (G_B3_2, G_B3_0);
		(G_B3_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B3_1), (String_t*)G_B3_0);
		StringU5BU5D_t2298632947* L_7 = (StringU5BU5D_t2298632947*)G_B3_3;
		NullCheck(L_7);
		ArrayElementTypeCheck (L_7, _stringLiteral1084017900);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral1084017900);
		StringU5BU5D_t2298632947* L_8 = (StringU5BU5D_t2298632947*)L_7;
		RuntimeObject * L_9 = KeyValuePair_2_get_Value_m1253090796((KeyValuePair_2_t603276356 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		G_B4_0 = 3;
		G_B4_1 = L_8;
		G_B4_2 = L_8;
		if (!L_9)
		{
			G_B5_0 = 3;
			G_B5_1 = L_8;
			G_B5_2 = L_8;
			goto IL_0072;
		}
	}
	{
		RuntimeObject * L_10 = KeyValuePair_2_get_Value_m1253090796((KeyValuePair_2_t603276356 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		V_1 = (RuntimeObject *)L_10;
		NullCheck((RuntimeObject *)(*(&V_1)));
		String_t* L_11 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (RuntimeObject *)(*(&V_1)));
		G_B6_0 = L_11;
		G_B6_1 = G_B4_0;
		G_B6_2 = G_B4_1;
		G_B6_3 = G_B4_2;
		goto IL_0077;
	}

IL_0072:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_12 = ((String_t_StaticFields*)il2cpp_codegen_static_fields_for(String_t_il2cpp_TypeInfo_var))->get_Empty_2();
		G_B6_0 = L_12;
		G_B6_1 = G_B5_0;
		G_B6_2 = G_B5_1;
		G_B6_3 = G_B5_2;
	}

IL_0077:
	{
		NullCheck(G_B6_2);
		ArrayElementTypeCheck (G_B6_2, G_B6_0);
		(G_B6_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B6_1), (String_t*)G_B6_0);
		StringU5BU5D_t2298632947* L_13 = (StringU5BU5D_t2298632947*)G_B6_3;
		NullCheck(L_13);
		ArrayElementTypeCheck (L_13, _stringLiteral1238787106);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteral1238787106);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = String_Concat_m2562457838(NULL /*static, unused*/, (StringU5BU5D_t2298632947*)L_13, /*hidden argument*/NULL);
		return L_14;
	}
}
extern "C"  String_t* KeyValuePair_2_ToString_m2759420978_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	KeyValuePair_2_t603276356 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t603276356 *>(__this + 1);
	return KeyValuePair_2_ToString_m2759420978(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.HoloLensExtendedTrackingManager/PoseAgeEntry>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m820146208_gshared (KeyValuePair_2_t2609754974 * __this, TrackableIdPair_t1331427386  ___key0, PoseAgeEntry_t806437902  ___value1, const RuntimeMethod* method)
{
	{
		TrackableIdPair_t1331427386  L_0 = ___key0;
		KeyValuePair_2_set_Key_m3192279472((KeyValuePair_2_t2609754974 *)__this, (TrackableIdPair_t1331427386 )L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		PoseAgeEntry_t806437902  L_1 = ___value1;
		KeyValuePair_2_set_Value_m3595070817((KeyValuePair_2_t2609754974 *)__this, (PoseAgeEntry_t806437902 )L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return;
	}
}
extern "C"  void KeyValuePair_2__ctor_m820146208_AdjustorThunk (RuntimeObject * __this, TrackableIdPair_t1331427386  ___key0, PoseAgeEntry_t806437902  ___value1, const RuntimeMethod* method)
{
	KeyValuePair_2_t2609754974 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t2609754974 *>(__this + 1);
	KeyValuePair_2__ctor_m820146208(_thisAdjusted, ___key0, ___value1, method);
}
// TKey System.Collections.Generic.KeyValuePair`2<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.HoloLensExtendedTrackingManager/PoseAgeEntry>::get_Key()
extern "C"  TrackableIdPair_t1331427386  KeyValuePair_2_get_Key_m3017190976_gshared (KeyValuePair_2_t2609754974 * __this, const RuntimeMethod* method)
{
	{
		TrackableIdPair_t1331427386  L_0 = (TrackableIdPair_t1331427386 )__this->get_key_0();
		return L_0;
	}
}
extern "C"  TrackableIdPair_t1331427386  KeyValuePair_2_get_Key_m3017190976_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	KeyValuePair_2_t2609754974 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t2609754974 *>(__this + 1);
	return KeyValuePair_2_get_Key_m3017190976(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.HoloLensExtendedTrackingManager/PoseAgeEntry>::set_Key(TKey)
extern "C"  void KeyValuePair_2_set_Key_m3192279472_gshared (KeyValuePair_2_t2609754974 * __this, TrackableIdPair_t1331427386  ___value0, const RuntimeMethod* method)
{
	{
		TrackableIdPair_t1331427386  L_0 = ___value0;
		__this->set_key_0(L_0);
		return;
	}
}
extern "C"  void KeyValuePair_2_set_Key_m3192279472_AdjustorThunk (RuntimeObject * __this, TrackableIdPair_t1331427386  ___value0, const RuntimeMethod* method)
{
	KeyValuePair_2_t2609754974 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t2609754974 *>(__this + 1);
	KeyValuePair_2_set_Key_m3192279472(_thisAdjusted, ___value0, method);
}
// TValue System.Collections.Generic.KeyValuePair`2<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.HoloLensExtendedTrackingManager/PoseAgeEntry>::get_Value()
extern "C"  PoseAgeEntry_t806437902  KeyValuePair_2_get_Value_m3794373347_gshared (KeyValuePair_2_t2609754974 * __this, const RuntimeMethod* method)
{
	{
		PoseAgeEntry_t806437902  L_0 = (PoseAgeEntry_t806437902 )__this->get_value_1();
		return L_0;
	}
}
extern "C"  PoseAgeEntry_t806437902  KeyValuePair_2_get_Value_m3794373347_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	KeyValuePair_2_t2609754974 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t2609754974 *>(__this + 1);
	return KeyValuePair_2_get_Value_m3794373347(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.HoloLensExtendedTrackingManager/PoseAgeEntry>::set_Value(TValue)
extern "C"  void KeyValuePair_2_set_Value_m3595070817_gshared (KeyValuePair_2_t2609754974 * __this, PoseAgeEntry_t806437902  ___value0, const RuntimeMethod* method)
{
	{
		PoseAgeEntry_t806437902  L_0 = ___value0;
		__this->set_value_1(L_0);
		return;
	}
}
extern "C"  void KeyValuePair_2_set_Value_m3595070817_AdjustorThunk (RuntimeObject * __this, PoseAgeEntry_t806437902  ___value0, const RuntimeMethod* method)
{
	KeyValuePair_2_t2609754974 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t2609754974 *>(__this + 1);
	KeyValuePair_2_set_Value_m3595070817(_thisAdjusted, ___value0, method);
}
// System.String System.Collections.Generic.KeyValuePair`2<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.HoloLensExtendedTrackingManager/PoseAgeEntry>::ToString()
extern "C"  String_t* KeyValuePair_2_ToString_m3066038920_gshared (KeyValuePair_2_t2609754974 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KeyValuePair_2_ToString_m3066038920_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	TrackableIdPair_t1331427386  V_0;
	memset(&V_0, 0, sizeof(V_0));
	PoseAgeEntry_t806437902  V_1;
	memset(&V_1, 0, sizeof(V_1));
	int32_t G_B2_0 = 0;
	StringU5BU5D_t2298632947* G_B2_1 = NULL;
	StringU5BU5D_t2298632947* G_B2_2 = NULL;
	int32_t G_B1_0 = 0;
	StringU5BU5D_t2298632947* G_B1_1 = NULL;
	StringU5BU5D_t2298632947* G_B1_2 = NULL;
	String_t* G_B3_0 = NULL;
	int32_t G_B3_1 = 0;
	StringU5BU5D_t2298632947* G_B3_2 = NULL;
	StringU5BU5D_t2298632947* G_B3_3 = NULL;
	int32_t G_B5_0 = 0;
	StringU5BU5D_t2298632947* G_B5_1 = NULL;
	StringU5BU5D_t2298632947* G_B5_2 = NULL;
	int32_t G_B4_0 = 0;
	StringU5BU5D_t2298632947* G_B4_1 = NULL;
	StringU5BU5D_t2298632947* G_B4_2 = NULL;
	String_t* G_B6_0 = NULL;
	int32_t G_B6_1 = 0;
	StringU5BU5D_t2298632947* G_B6_2 = NULL;
	StringU5BU5D_t2298632947* G_B6_3 = NULL;
	{
		StringU5BU5D_t2298632947* L_0 = (StringU5BU5D_t2298632947*)((StringU5BU5D_t2298632947*)SZArrayNew(StringU5BU5D_t2298632947_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_0);
		ArrayElementTypeCheck (L_0, _stringLiteral3984347688);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral3984347688);
		StringU5BU5D_t2298632947* L_1 = (StringU5BU5D_t2298632947*)L_0;
		TrackableIdPair_t1331427386  L_2 = KeyValuePair_2_get_Key_m3017190976((KeyValuePair_2_t2609754974 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		G_B1_0 = 1;
		G_B1_1 = L_1;
		G_B1_2 = L_1;
	}
	{
		TrackableIdPair_t1331427386  L_3 = KeyValuePair_2_get_Key_m3017190976((KeyValuePair_2_t2609754974 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		V_0 = (TrackableIdPair_t1331427386 )L_3;
		RuntimeObject * L_4 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (&V_0));
		NullCheck((RuntimeObject *)L_4);
		String_t* L_5 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (RuntimeObject *)L_4);
		*(&V_0) = *(TrackableIdPair_t1331427386 *)UnBox(L_4);
		G_B3_0 = L_5;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		G_B3_3 = G_B1_2;
		goto IL_003e;
	}

IL_0039:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_6 = ((String_t_StaticFields*)il2cpp_codegen_static_fields_for(String_t_il2cpp_TypeInfo_var))->get_Empty_2();
		G_B3_0 = L_6;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
		G_B3_3 = G_B2_2;
	}

IL_003e:
	{
		NullCheck(G_B3_2);
		ArrayElementTypeCheck (G_B3_2, G_B3_0);
		(G_B3_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B3_1), (String_t*)G_B3_0);
		StringU5BU5D_t2298632947* L_7 = (StringU5BU5D_t2298632947*)G_B3_3;
		NullCheck(L_7);
		ArrayElementTypeCheck (L_7, _stringLiteral1084017900);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral1084017900);
		StringU5BU5D_t2298632947* L_8 = (StringU5BU5D_t2298632947*)L_7;
		PoseAgeEntry_t806437902  L_9 = KeyValuePair_2_get_Value_m3794373347((KeyValuePair_2_t2609754974 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		G_B4_0 = 3;
		G_B4_1 = L_8;
		G_B4_2 = L_8;
	}
	{
		PoseAgeEntry_t806437902  L_10 = KeyValuePair_2_get_Value_m3794373347((KeyValuePair_2_t2609754974 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		V_1 = (PoseAgeEntry_t806437902 )L_10;
		RuntimeObject * L_11 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), (&V_1));
		NullCheck((RuntimeObject *)L_11);
		String_t* L_12 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (RuntimeObject *)L_11);
		*(&V_1) = *(PoseAgeEntry_t806437902 *)UnBox(L_11);
		G_B6_0 = L_12;
		G_B6_1 = G_B4_0;
		G_B6_2 = G_B4_1;
		G_B6_3 = G_B4_2;
		goto IL_0077;
	}

IL_0072:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_13 = ((String_t_StaticFields*)il2cpp_codegen_static_fields_for(String_t_il2cpp_TypeInfo_var))->get_Empty_2();
		G_B6_0 = L_13;
		G_B6_1 = G_B5_0;
		G_B6_2 = G_B5_1;
		G_B6_3 = G_B5_2;
	}

IL_0077:
	{
		NullCheck(G_B6_2);
		ArrayElementTypeCheck (G_B6_2, G_B6_0);
		(G_B6_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B6_1), (String_t*)G_B6_0);
		StringU5BU5D_t2298632947* L_14 = (StringU5BU5D_t2298632947*)G_B6_3;
		NullCheck(L_14);
		ArrayElementTypeCheck (L_14, _stringLiteral1238787106);
		(L_14)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteral1238787106);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_15 = String_Concat_m2562457838(NULL /*static, unused*/, (StringU5BU5D_t2298632947*)L_14, /*hidden argument*/NULL);
		return L_15;
	}
}
extern "C"  String_t* KeyValuePair_2_ToString_m3066038920_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	KeyValuePair_2_t2609754974 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t2609754974 *>(__this + 1);
	return KeyValuePair_2_ToString_m3066038920(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.HoloLensExtendedTrackingManager/PoseInfo>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m3588891793_gshared (KeyValuePair_2_t4060628773 * __this, TrackableIdPair_t1331427386  ___key0, PoseInfo_t2257311701  ___value1, const RuntimeMethod* method)
{
	{
		TrackableIdPair_t1331427386  L_0 = ___key0;
		KeyValuePair_2_set_Key_m3132306086((KeyValuePair_2_t4060628773 *)__this, (TrackableIdPair_t1331427386 )L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		PoseInfo_t2257311701  L_1 = ___value1;
		KeyValuePair_2_set_Value_m3890858544((KeyValuePair_2_t4060628773 *)__this, (PoseInfo_t2257311701 )L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return;
	}
}
extern "C"  void KeyValuePair_2__ctor_m3588891793_AdjustorThunk (RuntimeObject * __this, TrackableIdPair_t1331427386  ___key0, PoseInfo_t2257311701  ___value1, const RuntimeMethod* method)
{
	KeyValuePair_2_t4060628773 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t4060628773 *>(__this + 1);
	KeyValuePair_2__ctor_m3588891793(_thisAdjusted, ___key0, ___value1, method);
}
// TKey System.Collections.Generic.KeyValuePair`2<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.HoloLensExtendedTrackingManager/PoseInfo>::get_Key()
extern "C"  TrackableIdPair_t1331427386  KeyValuePair_2_get_Key_m4245299421_gshared (KeyValuePair_2_t4060628773 * __this, const RuntimeMethod* method)
{
	{
		TrackableIdPair_t1331427386  L_0 = (TrackableIdPair_t1331427386 )__this->get_key_0();
		return L_0;
	}
}
extern "C"  TrackableIdPair_t1331427386  KeyValuePair_2_get_Key_m4245299421_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	KeyValuePair_2_t4060628773 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t4060628773 *>(__this + 1);
	return KeyValuePair_2_get_Key_m4245299421(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.HoloLensExtendedTrackingManager/PoseInfo>::set_Key(TKey)
extern "C"  void KeyValuePair_2_set_Key_m3132306086_gshared (KeyValuePair_2_t4060628773 * __this, TrackableIdPair_t1331427386  ___value0, const RuntimeMethod* method)
{
	{
		TrackableIdPair_t1331427386  L_0 = ___value0;
		__this->set_key_0(L_0);
		return;
	}
}
extern "C"  void KeyValuePair_2_set_Key_m3132306086_AdjustorThunk (RuntimeObject * __this, TrackableIdPair_t1331427386  ___value0, const RuntimeMethod* method)
{
	KeyValuePair_2_t4060628773 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t4060628773 *>(__this + 1);
	KeyValuePair_2_set_Key_m3132306086(_thisAdjusted, ___value0, method);
}
// TValue System.Collections.Generic.KeyValuePair`2<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.HoloLensExtendedTrackingManager/PoseInfo>::get_Value()
extern "C"  PoseInfo_t2257311701  KeyValuePair_2_get_Value_m3742287316_gshared (KeyValuePair_2_t4060628773 * __this, const RuntimeMethod* method)
{
	{
		PoseInfo_t2257311701  L_0 = (PoseInfo_t2257311701 )__this->get_value_1();
		return L_0;
	}
}
extern "C"  PoseInfo_t2257311701  KeyValuePair_2_get_Value_m3742287316_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	KeyValuePair_2_t4060628773 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t4060628773 *>(__this + 1);
	return KeyValuePair_2_get_Value_m3742287316(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.HoloLensExtendedTrackingManager/PoseInfo>::set_Value(TValue)
extern "C"  void KeyValuePair_2_set_Value_m3890858544_gshared (KeyValuePair_2_t4060628773 * __this, PoseInfo_t2257311701  ___value0, const RuntimeMethod* method)
{
	{
		PoseInfo_t2257311701  L_0 = ___value0;
		__this->set_value_1(L_0);
		return;
	}
}
extern "C"  void KeyValuePair_2_set_Value_m3890858544_AdjustorThunk (RuntimeObject * __this, PoseInfo_t2257311701  ___value0, const RuntimeMethod* method)
{
	KeyValuePair_2_t4060628773 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t4060628773 *>(__this + 1);
	KeyValuePair_2_set_Value_m3890858544(_thisAdjusted, ___value0, method);
}
// System.String System.Collections.Generic.KeyValuePair`2<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.HoloLensExtendedTrackingManager/PoseInfo>::ToString()
extern "C"  String_t* KeyValuePair_2_ToString_m2795500392_gshared (KeyValuePair_2_t4060628773 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KeyValuePair_2_ToString_m2795500392_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	TrackableIdPair_t1331427386  V_0;
	memset(&V_0, 0, sizeof(V_0));
	PoseInfo_t2257311701  V_1;
	memset(&V_1, 0, sizeof(V_1));
	int32_t G_B2_0 = 0;
	StringU5BU5D_t2298632947* G_B2_1 = NULL;
	StringU5BU5D_t2298632947* G_B2_2 = NULL;
	int32_t G_B1_0 = 0;
	StringU5BU5D_t2298632947* G_B1_1 = NULL;
	StringU5BU5D_t2298632947* G_B1_2 = NULL;
	String_t* G_B3_0 = NULL;
	int32_t G_B3_1 = 0;
	StringU5BU5D_t2298632947* G_B3_2 = NULL;
	StringU5BU5D_t2298632947* G_B3_3 = NULL;
	int32_t G_B5_0 = 0;
	StringU5BU5D_t2298632947* G_B5_1 = NULL;
	StringU5BU5D_t2298632947* G_B5_2 = NULL;
	int32_t G_B4_0 = 0;
	StringU5BU5D_t2298632947* G_B4_1 = NULL;
	StringU5BU5D_t2298632947* G_B4_2 = NULL;
	String_t* G_B6_0 = NULL;
	int32_t G_B6_1 = 0;
	StringU5BU5D_t2298632947* G_B6_2 = NULL;
	StringU5BU5D_t2298632947* G_B6_3 = NULL;
	{
		StringU5BU5D_t2298632947* L_0 = (StringU5BU5D_t2298632947*)((StringU5BU5D_t2298632947*)SZArrayNew(StringU5BU5D_t2298632947_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_0);
		ArrayElementTypeCheck (L_0, _stringLiteral3984347688);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral3984347688);
		StringU5BU5D_t2298632947* L_1 = (StringU5BU5D_t2298632947*)L_0;
		TrackableIdPair_t1331427386  L_2 = KeyValuePair_2_get_Key_m4245299421((KeyValuePair_2_t4060628773 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		G_B1_0 = 1;
		G_B1_1 = L_1;
		G_B1_2 = L_1;
	}
	{
		TrackableIdPair_t1331427386  L_3 = KeyValuePair_2_get_Key_m4245299421((KeyValuePair_2_t4060628773 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		V_0 = (TrackableIdPair_t1331427386 )L_3;
		RuntimeObject * L_4 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (&V_0));
		NullCheck((RuntimeObject *)L_4);
		String_t* L_5 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (RuntimeObject *)L_4);
		*(&V_0) = *(TrackableIdPair_t1331427386 *)UnBox(L_4);
		G_B3_0 = L_5;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		G_B3_3 = G_B1_2;
		goto IL_003e;
	}

IL_0039:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_6 = ((String_t_StaticFields*)il2cpp_codegen_static_fields_for(String_t_il2cpp_TypeInfo_var))->get_Empty_2();
		G_B3_0 = L_6;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
		G_B3_3 = G_B2_2;
	}

IL_003e:
	{
		NullCheck(G_B3_2);
		ArrayElementTypeCheck (G_B3_2, G_B3_0);
		(G_B3_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B3_1), (String_t*)G_B3_0);
		StringU5BU5D_t2298632947* L_7 = (StringU5BU5D_t2298632947*)G_B3_3;
		NullCheck(L_7);
		ArrayElementTypeCheck (L_7, _stringLiteral1084017900);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral1084017900);
		StringU5BU5D_t2298632947* L_8 = (StringU5BU5D_t2298632947*)L_7;
		PoseInfo_t2257311701  L_9 = KeyValuePair_2_get_Value_m3742287316((KeyValuePair_2_t4060628773 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		G_B4_0 = 3;
		G_B4_1 = L_8;
		G_B4_2 = L_8;
	}
	{
		PoseInfo_t2257311701  L_10 = KeyValuePair_2_get_Value_m3742287316((KeyValuePair_2_t4060628773 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		V_1 = (PoseInfo_t2257311701 )L_10;
		RuntimeObject * L_11 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), (&V_1));
		NullCheck((RuntimeObject *)L_11);
		String_t* L_12 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (RuntimeObject *)L_11);
		*(&V_1) = *(PoseInfo_t2257311701 *)UnBox(L_11);
		G_B6_0 = L_12;
		G_B6_1 = G_B4_0;
		G_B6_2 = G_B4_1;
		G_B6_3 = G_B4_2;
		goto IL_0077;
	}

IL_0072:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_13 = ((String_t_StaticFields*)il2cpp_codegen_static_fields_for(String_t_il2cpp_TypeInfo_var))->get_Empty_2();
		G_B6_0 = L_13;
		G_B6_1 = G_B5_0;
		G_B6_2 = G_B5_1;
		G_B6_3 = G_B5_2;
	}

IL_0077:
	{
		NullCheck(G_B6_2);
		ArrayElementTypeCheck (G_B6_2, G_B6_0);
		(G_B6_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B6_1), (String_t*)G_B6_0);
		StringU5BU5D_t2298632947* L_14 = (StringU5BU5D_t2298632947*)G_B6_3;
		NullCheck(L_14);
		ArrayElementTypeCheck (L_14, _stringLiteral1238787106);
		(L_14)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteral1238787106);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_15 = String_Concat_m2562457838(NULL /*static, unused*/, (StringU5BU5D_t2298632947*)L_14, /*hidden argument*/NULL);
		return L_15;
	}
}
extern "C"  String_t* KeyValuePair_2_ToString_m2795500392_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	KeyValuePair_2_t4060628773 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t4060628773 *>(__this + 1);
	return KeyValuePair_2_ToString_m2795500392(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.TrackableBehaviour/Status>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m3910118697_gshared (KeyValuePair_2_t3161435941 * __this, TrackableIdPair_t1331427386  ___key0, int32_t ___value1, const RuntimeMethod* method)
{
	{
		TrackableIdPair_t1331427386  L_0 = ___key0;
		KeyValuePair_2_set_Key_m2719103795((KeyValuePair_2_t3161435941 *)__this, (TrackableIdPair_t1331427386 )L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_1 = ___value1;
		KeyValuePair_2_set_Value_m1544936261((KeyValuePair_2_t3161435941 *)__this, (int32_t)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return;
	}
}
extern "C"  void KeyValuePair_2__ctor_m3910118697_AdjustorThunk (RuntimeObject * __this, TrackableIdPair_t1331427386  ___key0, int32_t ___value1, const RuntimeMethod* method)
{
	KeyValuePair_2_t3161435941 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t3161435941 *>(__this + 1);
	KeyValuePair_2__ctor_m3910118697(_thisAdjusted, ___key0, ___value1, method);
}
// TKey System.Collections.Generic.KeyValuePair`2<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.TrackableBehaviour/Status>::get_Key()
extern "C"  TrackableIdPair_t1331427386  KeyValuePair_2_get_Key_m1540520745_gshared (KeyValuePair_2_t3161435941 * __this, const RuntimeMethod* method)
{
	{
		TrackableIdPair_t1331427386  L_0 = (TrackableIdPair_t1331427386 )__this->get_key_0();
		return L_0;
	}
}
extern "C"  TrackableIdPair_t1331427386  KeyValuePair_2_get_Key_m1540520745_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	KeyValuePair_2_t3161435941 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t3161435941 *>(__this + 1);
	return KeyValuePair_2_get_Key_m1540520745(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.TrackableBehaviour/Status>::set_Key(TKey)
extern "C"  void KeyValuePair_2_set_Key_m2719103795_gshared (KeyValuePair_2_t3161435941 * __this, TrackableIdPair_t1331427386  ___value0, const RuntimeMethod* method)
{
	{
		TrackableIdPair_t1331427386  L_0 = ___value0;
		__this->set_key_0(L_0);
		return;
	}
}
extern "C"  void KeyValuePair_2_set_Key_m2719103795_AdjustorThunk (RuntimeObject * __this, TrackableIdPair_t1331427386  ___value0, const RuntimeMethod* method)
{
	KeyValuePair_2_t3161435941 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t3161435941 *>(__this + 1);
	KeyValuePair_2_set_Key_m2719103795(_thisAdjusted, ___value0, method);
}
// TValue System.Collections.Generic.KeyValuePair`2<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.TrackableBehaviour/Status>::get_Value()
extern "C"  int32_t KeyValuePair_2_get_Value_m3827826732_gshared (KeyValuePair_2_t3161435941 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = (int32_t)__this->get_value_1();
		return L_0;
	}
}
extern "C"  int32_t KeyValuePair_2_get_Value_m3827826732_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	KeyValuePair_2_t3161435941 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t3161435941 *>(__this + 1);
	return KeyValuePair_2_get_Value_m3827826732(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.TrackableBehaviour/Status>::set_Value(TValue)
extern "C"  void KeyValuePair_2_set_Value_m1544936261_gshared (KeyValuePair_2_t3161435941 * __this, int32_t ___value0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_value_1(L_0);
		return;
	}
}
extern "C"  void KeyValuePair_2_set_Value_m1544936261_AdjustorThunk (RuntimeObject * __this, int32_t ___value0, const RuntimeMethod* method)
{
	KeyValuePair_2_t3161435941 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t3161435941 *>(__this + 1);
	KeyValuePair_2_set_Value_m1544936261(_thisAdjusted, ___value0, method);
}
// System.String System.Collections.Generic.KeyValuePair`2<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.TrackableBehaviour/Status>::ToString()
extern "C"  String_t* KeyValuePair_2_ToString_m1074719178_gshared (KeyValuePair_2_t3161435941 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KeyValuePair_2_ToString_m1074719178_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	TrackableIdPair_t1331427386  V_0;
	memset(&V_0, 0, sizeof(V_0));
	int32_t V_1 = 0;
	int32_t G_B2_0 = 0;
	StringU5BU5D_t2298632947* G_B2_1 = NULL;
	StringU5BU5D_t2298632947* G_B2_2 = NULL;
	int32_t G_B1_0 = 0;
	StringU5BU5D_t2298632947* G_B1_1 = NULL;
	StringU5BU5D_t2298632947* G_B1_2 = NULL;
	String_t* G_B3_0 = NULL;
	int32_t G_B3_1 = 0;
	StringU5BU5D_t2298632947* G_B3_2 = NULL;
	StringU5BU5D_t2298632947* G_B3_3 = NULL;
	int32_t G_B5_0 = 0;
	StringU5BU5D_t2298632947* G_B5_1 = NULL;
	StringU5BU5D_t2298632947* G_B5_2 = NULL;
	int32_t G_B4_0 = 0;
	StringU5BU5D_t2298632947* G_B4_1 = NULL;
	StringU5BU5D_t2298632947* G_B4_2 = NULL;
	String_t* G_B6_0 = NULL;
	int32_t G_B6_1 = 0;
	StringU5BU5D_t2298632947* G_B6_2 = NULL;
	StringU5BU5D_t2298632947* G_B6_3 = NULL;
	{
		StringU5BU5D_t2298632947* L_0 = (StringU5BU5D_t2298632947*)((StringU5BU5D_t2298632947*)SZArrayNew(StringU5BU5D_t2298632947_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_0);
		ArrayElementTypeCheck (L_0, _stringLiteral3984347688);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral3984347688);
		StringU5BU5D_t2298632947* L_1 = (StringU5BU5D_t2298632947*)L_0;
		TrackableIdPair_t1331427386  L_2 = KeyValuePair_2_get_Key_m1540520745((KeyValuePair_2_t3161435941 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		G_B1_0 = 1;
		G_B1_1 = L_1;
		G_B1_2 = L_1;
	}
	{
		TrackableIdPair_t1331427386  L_3 = KeyValuePair_2_get_Key_m1540520745((KeyValuePair_2_t3161435941 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		V_0 = (TrackableIdPair_t1331427386 )L_3;
		RuntimeObject * L_4 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (&V_0));
		NullCheck((RuntimeObject *)L_4);
		String_t* L_5 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (RuntimeObject *)L_4);
		*(&V_0) = *(TrackableIdPair_t1331427386 *)UnBox(L_4);
		G_B3_0 = L_5;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		G_B3_3 = G_B1_2;
		goto IL_003e;
	}

IL_0039:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_6 = ((String_t_StaticFields*)il2cpp_codegen_static_fields_for(String_t_il2cpp_TypeInfo_var))->get_Empty_2();
		G_B3_0 = L_6;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
		G_B3_3 = G_B2_2;
	}

IL_003e:
	{
		NullCheck(G_B3_2);
		ArrayElementTypeCheck (G_B3_2, G_B3_0);
		(G_B3_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B3_1), (String_t*)G_B3_0);
		StringU5BU5D_t2298632947* L_7 = (StringU5BU5D_t2298632947*)G_B3_3;
		NullCheck(L_7);
		ArrayElementTypeCheck (L_7, _stringLiteral1084017900);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral1084017900);
		StringU5BU5D_t2298632947* L_8 = (StringU5BU5D_t2298632947*)L_7;
		int32_t L_9 = KeyValuePair_2_get_Value_m3827826732((KeyValuePair_2_t3161435941 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		G_B4_0 = 3;
		G_B4_1 = L_8;
		G_B4_2 = L_8;
	}
	{
		int32_t L_10 = KeyValuePair_2_get_Value_m3827826732((KeyValuePair_2_t3161435941 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		V_1 = (int32_t)L_10;
		RuntimeObject * L_11 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), (&V_1));
		NullCheck((RuntimeObject *)L_11);
		String_t* L_12 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (RuntimeObject *)L_11);
		*(&V_1) = *(int32_t*)UnBox(L_11);
		G_B6_0 = L_12;
		G_B6_1 = G_B4_0;
		G_B6_2 = G_B4_1;
		G_B6_3 = G_B4_2;
		goto IL_0077;
	}

IL_0072:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_13 = ((String_t_StaticFields*)il2cpp_codegen_static_fields_for(String_t_il2cpp_TypeInfo_var))->get_Empty_2();
		G_B6_0 = L_13;
		G_B6_1 = G_B5_0;
		G_B6_2 = G_B5_1;
		G_B6_3 = G_B5_2;
	}

IL_0077:
	{
		NullCheck(G_B6_2);
		ArrayElementTypeCheck (G_B6_2, G_B6_0);
		(G_B6_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B6_1), (String_t*)G_B6_0);
		StringU5BU5D_t2298632947* L_14 = (StringU5BU5D_t2298632947*)G_B6_3;
		NullCheck(L_14);
		ArrayElementTypeCheck (L_14, _stringLiteral1238787106);
		(L_14)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteral1238787106);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_15 = String_Concat_m2562457838(NULL /*static, unused*/, (StringU5BU5D_t2298632947*)L_14, /*hidden argument*/NULL);
		return L_15;
	}
}
extern "C"  String_t* KeyValuePair_2_ToString_m1074719178_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	KeyValuePair_2_t3161435941 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t3161435941 *>(__this + 1);
	return KeyValuePair_2_ToString_m1074719178(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.LinkedList`1/Enumerator<System.Object>::.ctor(System.Collections.Generic.LinkedList`1<T>)
extern "C"  void Enumerator__ctor_m3993907956_gshared (Enumerator_t1983447509 * __this, LinkedList_1_t4198215526 * ___parent0, const RuntimeMethod* method)
{
	{
		LinkedList_1_t4198215526 * L_0 = ___parent0;
		__this->set_list_0(L_0);
		__this->set_current_1((LinkedListNode_1_t1694065801 *)NULL);
		__this->set_index_2((-1));
		LinkedList_1_t4198215526 * L_1 = ___parent0;
		NullCheck(L_1);
		uint32_t L_2 = (uint32_t)L_1->get_version_1();
		__this->set_version_3(L_2);
		return;
	}
}
extern "C"  void Enumerator__ctor_m3993907956_AdjustorThunk (RuntimeObject * __this, LinkedList_1_t4198215526 * ___parent0, const RuntimeMethod* method)
{
	Enumerator_t1983447509 * _thisAdjusted = reinterpret_cast<Enumerator_t1983447509 *>(__this + 1);
	Enumerator__ctor_m3993907956(_thisAdjusted, ___parent0, method);
}
// System.Object System.Collections.Generic.LinkedList`1/Enumerator<System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  RuntimeObject * Enumerator_System_Collections_IEnumerator_get_Current_m99794827_gshared (Enumerator_t1983447509 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = Enumerator_get_Current_m3886014715((Enumerator_t1983447509 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return L_0;
	}
}
extern "C"  RuntimeObject * Enumerator_System_Collections_IEnumerator_get_Current_m99794827_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Enumerator_t1983447509 * _thisAdjusted = reinterpret_cast<Enumerator_t1983447509 *>(__this + 1);
	return Enumerator_System_Collections_IEnumerator_get_Current_m99794827(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.LinkedList`1/Enumerator<System.Object>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m2687040866_gshared (Enumerator_t1983447509 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_System_Collections_IEnumerator_Reset_m2687040866_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		LinkedList_1_t4198215526 * L_0 = (LinkedList_1_t4198215526 *)__this->get_list_0();
		if (L_0)
		{
			goto IL_0012;
		}
	}
	{
		ObjectDisposedException_t2553224159 * L_1 = (ObjectDisposedException_t2553224159 *)il2cpp_codegen_object_new(ObjectDisposedException_t2553224159_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m4150714796(L_1, (String_t*)NULL, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0012:
	{
		uint32_t L_2 = (uint32_t)__this->get_version_3();
		LinkedList_1_t4198215526 * L_3 = (LinkedList_1_t4198215526 *)__this->get_list_0();
		NullCheck(L_3);
		uint32_t L_4 = (uint32_t)L_3->get_version_1();
		if ((((int32_t)L_2) == ((int32_t)L_4)))
		{
			goto IL_0033;
		}
	}
	{
		InvalidOperationException_t2321794232 * L_5 = (InvalidOperationException_t2321794232 *)il2cpp_codegen_object_new(InvalidOperationException_t2321794232_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m4101245683(L_5, (String_t*)_stringLiteral4105241565, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0033:
	{
		__this->set_current_1((LinkedListNode_1_t1694065801 *)NULL);
		__this->set_index_2((-1));
		return;
	}
}
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m2687040866_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Enumerator_t1983447509 * _thisAdjusted = reinterpret_cast<Enumerator_t1983447509 *>(__this + 1);
	Enumerator_System_Collections_IEnumerator_Reset_m2687040866(_thisAdjusted, method);
}
// T System.Collections.Generic.LinkedList`1/Enumerator<System.Object>::get_Current()
extern "C"  RuntimeObject * Enumerator_get_Current_m3886014715_gshared (Enumerator_t1983447509 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_get_Current_m3886014715_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		LinkedList_1_t4198215526 * L_0 = (LinkedList_1_t4198215526 *)__this->get_list_0();
		if (L_0)
		{
			goto IL_0012;
		}
	}
	{
		ObjectDisposedException_t2553224159 * L_1 = (ObjectDisposedException_t2553224159 *)il2cpp_codegen_object_new(ObjectDisposedException_t2553224159_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m4150714796(L_1, (String_t*)NULL, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0012:
	{
		LinkedListNode_1_t1694065801 * L_2 = (LinkedListNode_1_t1694065801 *)__this->get_current_1();
		if (L_2)
		{
			goto IL_0023;
		}
	}
	{
		InvalidOperationException_t2321794232 * L_3 = (InvalidOperationException_t2321794232 *)il2cpp_codegen_object_new(InvalidOperationException_t2321794232_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m3753870925(L_3, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_0023:
	{
		LinkedListNode_1_t1694065801 * L_4 = (LinkedListNode_1_t1694065801 *)__this->get_current_1();
		NullCheck((LinkedListNode_1_t1694065801 *)L_4);
		RuntimeObject * L_5 = ((  RuntimeObject * (*) (LinkedListNode_1_t1694065801 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((LinkedListNode_1_t1694065801 *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_5;
	}
}
extern "C"  RuntimeObject * Enumerator_get_Current_m3886014715_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Enumerator_t1983447509 * _thisAdjusted = reinterpret_cast<Enumerator_t1983447509 *>(__this + 1);
	return Enumerator_get_Current_m3886014715(_thisAdjusted, method);
}
// System.Boolean System.Collections.Generic.LinkedList`1/Enumerator<System.Object>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m893837046_gshared (Enumerator_t1983447509 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_MoveNext_m893837046_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		LinkedList_1_t4198215526 * L_0 = (LinkedList_1_t4198215526 *)__this->get_list_0();
		if (L_0)
		{
			goto IL_0012;
		}
	}
	{
		ObjectDisposedException_t2553224159 * L_1 = (ObjectDisposedException_t2553224159 *)il2cpp_codegen_object_new(ObjectDisposedException_t2553224159_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m4150714796(L_1, (String_t*)NULL, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0012:
	{
		uint32_t L_2 = (uint32_t)__this->get_version_3();
		LinkedList_1_t4198215526 * L_3 = (LinkedList_1_t4198215526 *)__this->get_list_0();
		NullCheck(L_3);
		uint32_t L_4 = (uint32_t)L_3->get_version_1();
		if ((((int32_t)L_2) == ((int32_t)L_4)))
		{
			goto IL_0033;
		}
	}
	{
		InvalidOperationException_t2321794232 * L_5 = (InvalidOperationException_t2321794232 *)il2cpp_codegen_object_new(InvalidOperationException_t2321794232_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m4101245683(L_5, (String_t*)_stringLiteral4105241565, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0033:
	{
		LinkedListNode_1_t1694065801 * L_6 = (LinkedListNode_1_t1694065801 *)__this->get_current_1();
		if (L_6)
		{
			goto IL_0054;
		}
	}
	{
		LinkedList_1_t4198215526 * L_7 = (LinkedList_1_t4198215526 *)__this->get_list_0();
		NullCheck(L_7);
		LinkedListNode_1_t1694065801 * L_8 = (LinkedListNode_1_t1694065801 *)L_7->get_first_3();
		__this->set_current_1(L_8);
		goto IL_0082;
	}

IL_0054:
	{
		LinkedListNode_1_t1694065801 * L_9 = (LinkedListNode_1_t1694065801 *)__this->get_current_1();
		NullCheck(L_9);
		LinkedListNode_1_t1694065801 * L_10 = (LinkedListNode_1_t1694065801 *)L_9->get_forward_2();
		__this->set_current_1(L_10);
		LinkedListNode_1_t1694065801 * L_11 = (LinkedListNode_1_t1694065801 *)__this->get_current_1();
		LinkedList_1_t4198215526 * L_12 = (LinkedList_1_t4198215526 *)__this->get_list_0();
		NullCheck(L_12);
		LinkedListNode_1_t1694065801 * L_13 = (LinkedListNode_1_t1694065801 *)L_12->get_first_3();
		if ((!(((RuntimeObject*)(LinkedListNode_1_t1694065801 *)L_11) == ((RuntimeObject*)(LinkedListNode_1_t1694065801 *)L_13))))
		{
			goto IL_0082;
		}
	}
	{
		__this->set_current_1((LinkedListNode_1_t1694065801 *)NULL);
	}

IL_0082:
	{
		LinkedListNode_1_t1694065801 * L_14 = (LinkedListNode_1_t1694065801 *)__this->get_current_1();
		if (L_14)
		{
			goto IL_0096;
		}
	}
	{
		__this->set_index_2((-1));
		return (bool)0;
	}

IL_0096:
	{
		int32_t L_15 = (int32_t)__this->get_index_2();
		__this->set_index_2(((int32_t)((int32_t)L_15+(int32_t)1)));
		return (bool)1;
	}
}
extern "C"  bool Enumerator_MoveNext_m893837046_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Enumerator_t1983447509 * _thisAdjusted = reinterpret_cast<Enumerator_t1983447509 *>(__this + 1);
	return Enumerator_MoveNext_m893837046(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.LinkedList`1/Enumerator<System.Object>::Dispose()
extern "C"  void Enumerator_Dispose_m3834463819_gshared (Enumerator_t1983447509 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_Dispose_m3834463819_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		LinkedList_1_t4198215526 * L_0 = (LinkedList_1_t4198215526 *)__this->get_list_0();
		if (L_0)
		{
			goto IL_0012;
		}
	}
	{
		ObjectDisposedException_t2553224159 * L_1 = (ObjectDisposedException_t2553224159 *)il2cpp_codegen_object_new(ObjectDisposedException_t2553224159_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m4150714796(L_1, (String_t*)NULL, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0012:
	{
		__this->set_current_1((LinkedListNode_1_t1694065801 *)NULL);
		__this->set_list_0((LinkedList_1_t4198215526 *)NULL);
		return;
	}
}
extern "C"  void Enumerator_Dispose_m3834463819_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Enumerator_t1983447509 * _thisAdjusted = reinterpret_cast<Enumerator_t1983447509 *>(__this + 1);
	Enumerator_Dispose_m3834463819(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.LinkedList`1/Enumerator<Vuforia.VuforiaManager/TrackableIdPair>::.ctor(System.Collections.Generic.LinkedList`1<T>)
extern "C"  void Enumerator__ctor_m2514524852_gshared (Enumerator_t1292916605 * __this, LinkedList_1_t3507684622 * ___parent0, const RuntimeMethod* method)
{
	{
		LinkedList_1_t3507684622 * L_0 = ___parent0;
		__this->set_list_0(L_0);
		__this->set_current_1((LinkedListNode_1_t1003534897 *)NULL);
		__this->set_index_2((-1));
		LinkedList_1_t3507684622 * L_1 = ___parent0;
		NullCheck(L_1);
		uint32_t L_2 = (uint32_t)L_1->get_version_1();
		__this->set_version_3(L_2);
		return;
	}
}
extern "C"  void Enumerator__ctor_m2514524852_AdjustorThunk (RuntimeObject * __this, LinkedList_1_t3507684622 * ___parent0, const RuntimeMethod* method)
{
	Enumerator_t1292916605 * _thisAdjusted = reinterpret_cast<Enumerator_t1292916605 *>(__this + 1);
	Enumerator__ctor_m2514524852(_thisAdjusted, ___parent0, method);
}
// System.Object System.Collections.Generic.LinkedList`1/Enumerator<Vuforia.VuforiaManager/TrackableIdPair>::System.Collections.IEnumerator.get_Current()
extern "C"  RuntimeObject * Enumerator_System_Collections_IEnumerator_get_Current_m1186992793_gshared (Enumerator_t1292916605 * __this, const RuntimeMethod* method)
{
	{
		TrackableIdPair_t1331427386  L_0 = Enumerator_get_Current_m1428289307((Enumerator_t1292916605 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		TrackableIdPair_t1331427386  L_1 = L_0;
		RuntimeObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  RuntimeObject * Enumerator_System_Collections_IEnumerator_get_Current_m1186992793_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Enumerator_t1292916605 * _thisAdjusted = reinterpret_cast<Enumerator_t1292916605 *>(__this + 1);
	return Enumerator_System_Collections_IEnumerator_get_Current_m1186992793(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.LinkedList`1/Enumerator<Vuforia.VuforiaManager/TrackableIdPair>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m986577418_gshared (Enumerator_t1292916605 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_System_Collections_IEnumerator_Reset_m986577418_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		LinkedList_1_t3507684622 * L_0 = (LinkedList_1_t3507684622 *)__this->get_list_0();
		if (L_0)
		{
			goto IL_0012;
		}
	}
	{
		ObjectDisposedException_t2553224159 * L_1 = (ObjectDisposedException_t2553224159 *)il2cpp_codegen_object_new(ObjectDisposedException_t2553224159_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m4150714796(L_1, (String_t*)NULL, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0012:
	{
		uint32_t L_2 = (uint32_t)__this->get_version_3();
		LinkedList_1_t3507684622 * L_3 = (LinkedList_1_t3507684622 *)__this->get_list_0();
		NullCheck(L_3);
		uint32_t L_4 = (uint32_t)L_3->get_version_1();
		if ((((int32_t)L_2) == ((int32_t)L_4)))
		{
			goto IL_0033;
		}
	}
	{
		InvalidOperationException_t2321794232 * L_5 = (InvalidOperationException_t2321794232 *)il2cpp_codegen_object_new(InvalidOperationException_t2321794232_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m4101245683(L_5, (String_t*)_stringLiteral4105241565, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0033:
	{
		__this->set_current_1((LinkedListNode_1_t1003534897 *)NULL);
		__this->set_index_2((-1));
		return;
	}
}
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m986577418_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Enumerator_t1292916605 * _thisAdjusted = reinterpret_cast<Enumerator_t1292916605 *>(__this + 1);
	Enumerator_System_Collections_IEnumerator_Reset_m986577418(_thisAdjusted, method);
}
// T System.Collections.Generic.LinkedList`1/Enumerator<Vuforia.VuforiaManager/TrackableIdPair>::get_Current()
extern "C"  TrackableIdPair_t1331427386  Enumerator_get_Current_m1428289307_gshared (Enumerator_t1292916605 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_get_Current_m1428289307_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		LinkedList_1_t3507684622 * L_0 = (LinkedList_1_t3507684622 *)__this->get_list_0();
		if (L_0)
		{
			goto IL_0012;
		}
	}
	{
		ObjectDisposedException_t2553224159 * L_1 = (ObjectDisposedException_t2553224159 *)il2cpp_codegen_object_new(ObjectDisposedException_t2553224159_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m4150714796(L_1, (String_t*)NULL, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0012:
	{
		LinkedListNode_1_t1003534897 * L_2 = (LinkedListNode_1_t1003534897 *)__this->get_current_1();
		if (L_2)
		{
			goto IL_0023;
		}
	}
	{
		InvalidOperationException_t2321794232 * L_3 = (InvalidOperationException_t2321794232 *)il2cpp_codegen_object_new(InvalidOperationException_t2321794232_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m3753870925(L_3, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_0023:
	{
		LinkedListNode_1_t1003534897 * L_4 = (LinkedListNode_1_t1003534897 *)__this->get_current_1();
		NullCheck((LinkedListNode_1_t1003534897 *)L_4);
		TrackableIdPair_t1331427386  L_5 = ((  TrackableIdPair_t1331427386  (*) (LinkedListNode_1_t1003534897 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((LinkedListNode_1_t1003534897 *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_5;
	}
}
extern "C"  TrackableIdPair_t1331427386  Enumerator_get_Current_m1428289307_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Enumerator_t1292916605 * _thisAdjusted = reinterpret_cast<Enumerator_t1292916605 *>(__this + 1);
	return Enumerator_get_Current_m1428289307(_thisAdjusted, method);
}
// System.Boolean System.Collections.Generic.LinkedList`1/Enumerator<Vuforia.VuforiaManager/TrackableIdPair>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m562864630_gshared (Enumerator_t1292916605 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_MoveNext_m562864630_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		LinkedList_1_t3507684622 * L_0 = (LinkedList_1_t3507684622 *)__this->get_list_0();
		if (L_0)
		{
			goto IL_0012;
		}
	}
	{
		ObjectDisposedException_t2553224159 * L_1 = (ObjectDisposedException_t2553224159 *)il2cpp_codegen_object_new(ObjectDisposedException_t2553224159_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m4150714796(L_1, (String_t*)NULL, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0012:
	{
		uint32_t L_2 = (uint32_t)__this->get_version_3();
		LinkedList_1_t3507684622 * L_3 = (LinkedList_1_t3507684622 *)__this->get_list_0();
		NullCheck(L_3);
		uint32_t L_4 = (uint32_t)L_3->get_version_1();
		if ((((int32_t)L_2) == ((int32_t)L_4)))
		{
			goto IL_0033;
		}
	}
	{
		InvalidOperationException_t2321794232 * L_5 = (InvalidOperationException_t2321794232 *)il2cpp_codegen_object_new(InvalidOperationException_t2321794232_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m4101245683(L_5, (String_t*)_stringLiteral4105241565, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0033:
	{
		LinkedListNode_1_t1003534897 * L_6 = (LinkedListNode_1_t1003534897 *)__this->get_current_1();
		if (L_6)
		{
			goto IL_0054;
		}
	}
	{
		LinkedList_1_t3507684622 * L_7 = (LinkedList_1_t3507684622 *)__this->get_list_0();
		NullCheck(L_7);
		LinkedListNode_1_t1003534897 * L_8 = (LinkedListNode_1_t1003534897 *)L_7->get_first_3();
		__this->set_current_1(L_8);
		goto IL_0082;
	}

IL_0054:
	{
		LinkedListNode_1_t1003534897 * L_9 = (LinkedListNode_1_t1003534897 *)__this->get_current_1();
		NullCheck(L_9);
		LinkedListNode_1_t1003534897 * L_10 = (LinkedListNode_1_t1003534897 *)L_9->get_forward_2();
		__this->set_current_1(L_10);
		LinkedListNode_1_t1003534897 * L_11 = (LinkedListNode_1_t1003534897 *)__this->get_current_1();
		LinkedList_1_t3507684622 * L_12 = (LinkedList_1_t3507684622 *)__this->get_list_0();
		NullCheck(L_12);
		LinkedListNode_1_t1003534897 * L_13 = (LinkedListNode_1_t1003534897 *)L_12->get_first_3();
		if ((!(((RuntimeObject*)(LinkedListNode_1_t1003534897 *)L_11) == ((RuntimeObject*)(LinkedListNode_1_t1003534897 *)L_13))))
		{
			goto IL_0082;
		}
	}
	{
		__this->set_current_1((LinkedListNode_1_t1003534897 *)NULL);
	}

IL_0082:
	{
		LinkedListNode_1_t1003534897 * L_14 = (LinkedListNode_1_t1003534897 *)__this->get_current_1();
		if (L_14)
		{
			goto IL_0096;
		}
	}
	{
		__this->set_index_2((-1));
		return (bool)0;
	}

IL_0096:
	{
		int32_t L_15 = (int32_t)__this->get_index_2();
		__this->set_index_2(((int32_t)((int32_t)L_15+(int32_t)1)));
		return (bool)1;
	}
}
extern "C"  bool Enumerator_MoveNext_m562864630_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Enumerator_t1292916605 * _thisAdjusted = reinterpret_cast<Enumerator_t1292916605 *>(__this + 1);
	return Enumerator_MoveNext_m562864630(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.LinkedList`1/Enumerator<Vuforia.VuforiaManager/TrackableIdPair>::Dispose()
extern "C"  void Enumerator_Dispose_m3863799224_gshared (Enumerator_t1292916605 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_Dispose_m3863799224_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		LinkedList_1_t3507684622 * L_0 = (LinkedList_1_t3507684622 *)__this->get_list_0();
		if (L_0)
		{
			goto IL_0012;
		}
	}
	{
		ObjectDisposedException_t2553224159 * L_1 = (ObjectDisposedException_t2553224159 *)il2cpp_codegen_object_new(ObjectDisposedException_t2553224159_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m4150714796(L_1, (String_t*)NULL, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0012:
	{
		__this->set_current_1((LinkedListNode_1_t1003534897 *)NULL);
		__this->set_list_0((LinkedList_1_t3507684622 *)NULL);
		return;
	}
}
extern "C"  void Enumerator_Dispose_m3863799224_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Enumerator_t1292916605 * _thisAdjusted = reinterpret_cast<Enumerator_t1292916605 *>(__this + 1);
	Enumerator_Dispose_m3863799224(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.LinkedList`1<System.Object>::.ctor()
extern "C"  void LinkedList_1__ctor_m2557729568_gshared (LinkedList_1_t4198215526 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LinkedList_1__ctor_m2557729568_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	{
		NullCheck((RuntimeObject *)__this);
		Object__ctor_m2906060009((RuntimeObject *)__this, /*hidden argument*/NULL);
		RuntimeObject * L_0 = (RuntimeObject *)il2cpp_codegen_object_new(RuntimeObject_il2cpp_TypeInfo_var);
		Object__ctor_m2906060009(L_0, /*hidden argument*/NULL);
		__this->set_syncRoot_2(L_0);
		__this->set_first_3((LinkedListNode_1_t1694065801 *)NULL);
		int32_t L_1 = (int32_t)0;
		V_0 = (uint32_t)L_1;
		__this->set_version_1(L_1);
		uint32_t L_2 = V_0;
		__this->set_count_0(L_2);
		return;
	}
}
// System.Void System.Collections.Generic.LinkedList`1<System.Object>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void LinkedList_1__ctor_m631841948_gshared (LinkedList_1_t4198215526 * __this, SerializationInfo_t1606602250 * ___info0, StreamingContext_t4278693462  ___context1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LinkedList_1__ctor_m631841948_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NullCheck((LinkedList_1_t4198215526 *)__this);
		((  void (*) (LinkedList_1_t4198215526 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((LinkedList_1_t4198215526 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		SerializationInfo_t1606602250 * L_0 = ___info0;
		__this->set_si_4(L_0);
		RuntimeObject * L_1 = (RuntimeObject *)il2cpp_codegen_object_new(RuntimeObject_il2cpp_TypeInfo_var);
		Object__ctor_m2906060009(L_1, /*hidden argument*/NULL);
		__this->set_syncRoot_2(L_1);
		return;
	}
}
// System.Void System.Collections.Generic.LinkedList`1<System.Object>::System.Collections.Generic.ICollection<T>.Add(T)
extern "C"  void LinkedList_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m226917381_gshared (LinkedList_1_t4198215526 * __this, RuntimeObject * ___value0, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = ___value0;
		NullCheck((LinkedList_1_t4198215526 *)__this);
		((  LinkedListNode_1_t1694065801 * (*) (LinkedList_1_t4198215526 *, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((LinkedList_1_t4198215526 *)__this, (RuntimeObject *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		return;
	}
}
// System.Void System.Collections.Generic.LinkedList`1<System.Object>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void LinkedList_1_System_Collections_ICollection_CopyTo_m3341707420_gshared (LinkedList_1_t4198215526 * __this, RuntimeArray * ___array0, int32_t ___index1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LinkedList_1_System_Collections_ICollection_CopyTo_m3341707420_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ObjectU5BU5D_t3270211303* V_0 = NULL;
	{
		RuntimeArray * L_0 = ___array0;
		V_0 = (ObjectU5BU5D_t3270211303*)((ObjectU5BU5D_t3270211303*)IsInst((RuntimeObject*)L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2)));
		ObjectU5BU5D_t3270211303* L_1 = V_0;
		if (L_1)
		{
			goto IL_0018;
		}
	}
	{
		ArgumentException_t4028401650 * L_2 = (ArgumentException_t4028401650 *)il2cpp_codegen_object_new(ArgumentException_t4028401650_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m1186658332(L_2, (String_t*)_stringLiteral961150872, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0018:
	{
		ObjectU5BU5D_t3270211303* L_3 = V_0;
		int32_t L_4 = ___index1;
		NullCheck((LinkedList_1_t4198215526 *)__this);
		((  void (*) (LinkedList_1_t4198215526 *, ObjectU5BU5D_t3270211303*, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)((LinkedList_1_t4198215526 *)__this, (ObjectU5BU5D_t3270211303*)L_3, (int32_t)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		return;
	}
}
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.LinkedList`1<System.Object>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C"  RuntimeObject* LinkedList_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m1172543018_gshared (LinkedList_1_t4198215526 * __this, const RuntimeMethod* method)
{
	{
		NullCheck((LinkedList_1_t4198215526 *)__this);
		Enumerator_t1983447509  L_0 = ((  Enumerator_t1983447509  (*) (LinkedList_1_t4198215526 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)((LinkedList_1_t4198215526 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		Enumerator_t1983447509  L_1 = L_0;
		RuntimeObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5), &L_1);
		return (RuntimeObject*)L_2;
	}
}
// System.Collections.IEnumerator System.Collections.Generic.LinkedList`1<System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  RuntimeObject* LinkedList_1_System_Collections_IEnumerable_GetEnumerator_m2354000995_gshared (LinkedList_1_t4198215526 * __this, const RuntimeMethod* method)
{
	{
		NullCheck((LinkedList_1_t4198215526 *)__this);
		Enumerator_t1983447509  L_0 = ((  Enumerator_t1983447509  (*) (LinkedList_1_t4198215526 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)((LinkedList_1_t4198215526 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		Enumerator_t1983447509  L_1 = L_0;
		RuntimeObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5), &L_1);
		return (RuntimeObject*)L_2;
	}
}
// System.Boolean System.Collections.Generic.LinkedList`1<System.Object>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool LinkedList_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m2216536404_gshared (LinkedList_1_t4198215526 * __this, const RuntimeMethod* method)
{
	{
		return (bool)0;
	}
}
// System.Boolean System.Collections.Generic.LinkedList`1<System.Object>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool LinkedList_1_System_Collections_ICollection_get_IsSynchronized_m2382268684_gshared (LinkedList_1_t4198215526 * __this, const RuntimeMethod* method)
{
	{
		return (bool)0;
	}
}
// System.Object System.Collections.Generic.LinkedList`1<System.Object>::System.Collections.ICollection.get_SyncRoot()
extern "C"  RuntimeObject * LinkedList_1_System_Collections_ICollection_get_SyncRoot_m3879524626_gshared (LinkedList_1_t4198215526 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = (RuntimeObject *)__this->get_syncRoot_2();
		return L_0;
	}
}
// System.Void System.Collections.Generic.LinkedList`1<System.Object>::VerifyReferencedNode(System.Collections.Generic.LinkedListNode`1<T>)
extern "C"  void LinkedList_1_VerifyReferencedNode_m1279677459_gshared (LinkedList_1_t4198215526 * __this, LinkedListNode_1_t1694065801 * ___node0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LinkedList_1_VerifyReferencedNode_m1279677459_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		LinkedListNode_1_t1694065801 * L_0 = ___node0;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t2943536221 * L_1 = (ArgumentNullException_t2943536221 *)il2cpp_codegen_object_new(ArgumentNullException_t2943536221_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m1483904578(L_1, (String_t*)_stringLiteral3726057294, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0011:
	{
		LinkedListNode_1_t1694065801 * L_2 = ___node0;
		NullCheck((LinkedListNode_1_t1694065801 *)L_2);
		LinkedList_1_t4198215526 * L_3 = ((  LinkedList_1_t4198215526 * (*) (LinkedListNode_1_t1694065801 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((LinkedListNode_1_t1694065801 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		if ((((RuntimeObject*)(LinkedList_1_t4198215526 *)L_3) == ((RuntimeObject*)(LinkedList_1_t4198215526 *)__this)))
		{
			goto IL_0023;
		}
	}
	{
		InvalidOperationException_t2321794232 * L_4 = (InvalidOperationException_t2321794232 *)il2cpp_codegen_object_new(InvalidOperationException_t2321794232_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m3753870925(L_4, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_4);
	}

IL_0023:
	{
		return;
	}
}
// System.Collections.Generic.LinkedListNode`1<T> System.Collections.Generic.LinkedList`1<System.Object>::AddLast(T)
extern "C"  LinkedListNode_1_t1694065801 * LinkedList_1_AddLast_m3514266173_gshared (LinkedList_1_t4198215526 * __this, RuntimeObject * ___value0, const RuntimeMethod* method)
{
	LinkedListNode_1_t1694065801 * V_0 = NULL;
	{
		LinkedListNode_1_t1694065801 * L_0 = (LinkedListNode_1_t1694065801 *)__this->get_first_3();
		if (L_0)
		{
			goto IL_001f;
		}
	}
	{
		RuntimeObject * L_1 = ___value0;
		LinkedListNode_1_t1694065801 * L_2 = (LinkedListNode_1_t1694065801 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 7));
		((  void (*) (LinkedListNode_1_t1694065801 *, LinkedList_1_t4198215526 *, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)(L_2, (LinkedList_1_t4198215526 *)__this, (RuntimeObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		V_0 = (LinkedListNode_1_t1694065801 *)L_2;
		LinkedListNode_1_t1694065801 * L_3 = V_0;
		__this->set_first_3(L_3);
		goto IL_0038;
	}

IL_001f:
	{
		RuntimeObject * L_4 = ___value0;
		LinkedListNode_1_t1694065801 * L_5 = (LinkedListNode_1_t1694065801 *)__this->get_first_3();
		NullCheck(L_5);
		LinkedListNode_1_t1694065801 * L_6 = (LinkedListNode_1_t1694065801 *)L_5->get_back_3();
		LinkedListNode_1_t1694065801 * L_7 = (LinkedListNode_1_t1694065801 *)__this->get_first_3();
		LinkedListNode_1_t1694065801 * L_8 = (LinkedListNode_1_t1694065801 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 7));
		((  void (*) (LinkedListNode_1_t1694065801 *, LinkedList_1_t4198215526 *, RuntimeObject *, LinkedListNode_1_t1694065801 *, LinkedListNode_1_t1694065801 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9)->methodPointer)(L_8, (LinkedList_1_t4198215526 *)__this, (RuntimeObject *)L_4, (LinkedListNode_1_t1694065801 *)L_6, (LinkedListNode_1_t1694065801 *)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
		V_0 = (LinkedListNode_1_t1694065801 *)L_8;
	}

IL_0038:
	{
		uint32_t L_9 = (uint32_t)__this->get_count_0();
		__this->set_count_0(((int32_t)((int32_t)L_9+(int32_t)1)));
		uint32_t L_10 = (uint32_t)__this->get_version_1();
		__this->set_version_1(((int32_t)((int32_t)L_10+(int32_t)1)));
		LinkedListNode_1_t1694065801 * L_11 = V_0;
		return L_11;
	}
}
// System.Void System.Collections.Generic.LinkedList`1<System.Object>::Clear()
extern "C"  void LinkedList_1_Clear_m230549906_gshared (LinkedList_1_t4198215526 * __this, const RuntimeMethod* method)
{
	{
		goto IL_000b;
	}

IL_0005:
	{
		NullCheck((LinkedList_1_t4198215526 *)__this);
		((  void (*) (LinkedList_1_t4198215526 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10)->methodPointer)((LinkedList_1_t4198215526 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10));
	}

IL_000b:
	{
		LinkedListNode_1_t1694065801 * L_0 = (LinkedListNode_1_t1694065801 *)__this->get_first_3();
		if (L_0)
		{
			goto IL_0005;
		}
	}
	{
		return;
	}
}
// System.Boolean System.Collections.Generic.LinkedList`1<System.Object>::Contains(T)
extern "C"  bool LinkedList_1_Contains_m2666705016_gshared (LinkedList_1_t4198215526 * __this, RuntimeObject * ___value0, const RuntimeMethod* method)
{
	LinkedListNode_1_t1694065801 * V_0 = NULL;
	{
		LinkedListNode_1_t1694065801 * L_0 = (LinkedListNode_1_t1694065801 *)__this->get_first_3();
		V_0 = (LinkedListNode_1_t1694065801 *)L_0;
		LinkedListNode_1_t1694065801 * L_1 = V_0;
		if (L_1)
		{
			goto IL_000f;
		}
	}
	{
		return (bool)0;
	}

IL_000f:
	{
		LinkedListNode_1_t1694065801 * L_2 = V_0;
		NullCheck((LinkedListNode_1_t1694065801 *)L_2);
		RuntimeObject * L_3 = ((  RuntimeObject * (*) (LinkedListNode_1_t1694065801 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11)->methodPointer)((LinkedListNode_1_t1694065801 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11));
		NullCheck((RuntimeObject *)(*(&___value0)));
		bool L_4 = VirtFuncInvoker1< bool, RuntimeObject * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, (RuntimeObject *)(*(&___value0)), (RuntimeObject *)L_3);
		if (!L_4)
		{
			goto IL_002e;
		}
	}
	{
		return (bool)1;
	}

IL_002e:
	{
		LinkedListNode_1_t1694065801 * L_5 = V_0;
		NullCheck(L_5);
		LinkedListNode_1_t1694065801 * L_6 = (LinkedListNode_1_t1694065801 *)L_5->get_forward_2();
		V_0 = (LinkedListNode_1_t1694065801 *)L_6;
		LinkedListNode_1_t1694065801 * L_7 = V_0;
		LinkedListNode_1_t1694065801 * L_8 = (LinkedListNode_1_t1694065801 *)__this->get_first_3();
		if ((!(((RuntimeObject*)(LinkedListNode_1_t1694065801 *)L_7) == ((RuntimeObject*)(LinkedListNode_1_t1694065801 *)L_8))))
		{
			goto IL_000f;
		}
	}
	{
		return (bool)0;
	}
}
// System.Void System.Collections.Generic.LinkedList`1<System.Object>::CopyTo(T[],System.Int32)
extern "C"  void LinkedList_1_CopyTo_m3325928928_gshared (LinkedList_1_t4198215526 * __this, ObjectU5BU5D_t3270211303* ___array0, int32_t ___index1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LinkedList_1_CopyTo_m3325928928_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	LinkedListNode_1_t1694065801 * V_0 = NULL;
	{
		ObjectU5BU5D_t3270211303* L_0 = ___array0;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t2943536221 * L_1 = (ArgumentNullException_t2943536221 *)il2cpp_codegen_object_new(ArgumentNullException_t2943536221_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m1483904578(L_1, (String_t*)_stringLiteral961150872, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0011:
	{
		int32_t L_2 = ___index1;
		ObjectU5BU5D_t3270211303* L_3 = ___array0;
		NullCheck((RuntimeArray *)(RuntimeArray *)L_3);
		int32_t L_4 = Array_GetLowerBound_m2726004408((RuntimeArray *)(RuntimeArray *)L_3, (int32_t)0, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_2) < ((uint32_t)L_4))))
		{
			goto IL_0029;
		}
	}
	{
		ArgumentOutOfRangeException_t2174922486 * L_5 = (ArgumentOutOfRangeException_t2174922486 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t2174922486_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m1532465284(L_5, (String_t*)_stringLiteral3160537771, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0029:
	{
		ObjectU5BU5D_t3270211303* L_6 = ___array0;
		NullCheck((RuntimeArray *)(RuntimeArray *)L_6);
		int32_t L_7 = Array_get_Rank_m2275309645((RuntimeArray *)(RuntimeArray *)L_6, /*hidden argument*/NULL);
		if ((((int32_t)L_7) == ((int32_t)1)))
		{
			goto IL_0045;
		}
	}
	{
		ArgumentException_t4028401650 * L_8 = (ArgumentException_t4028401650 *)il2cpp_codegen_object_new(ArgumentException_t4028401650_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m596893627(L_8, (String_t*)_stringLiteral961150872, (String_t*)_stringLiteral2607546579, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_8);
	}

IL_0045:
	{
		ObjectU5BU5D_t3270211303* L_9 = ___array0;
		NullCheck(L_9);
		int32_t L_10 = ___index1;
		ObjectU5BU5D_t3270211303* L_11 = ___array0;
		NullCheck((RuntimeArray *)(RuntimeArray *)L_11);
		int32_t L_12 = Array_GetLowerBound_m2726004408((RuntimeArray *)(RuntimeArray *)L_11, (int32_t)0, /*hidden argument*/NULL);
		uint32_t L_13 = (uint32_t)__this->get_count_0();
		if ((((int64_t)(((int64_t)((int64_t)((int32_t)((int32_t)((int32_t)((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_9)->max_length))))-(int32_t)L_10))+(int32_t)L_12)))))) >= ((int64_t)(((int64_t)((uint64_t)L_13))))))
		{
			goto IL_006a;
		}
	}
	{
		ArgumentException_t4028401650 * L_14 = (ArgumentException_t4028401650 *)il2cpp_codegen_object_new(ArgumentException_t4028401650_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m1186658332(L_14, (String_t*)_stringLiteral1546211127, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_14);
	}

IL_006a:
	{
		LinkedListNode_1_t1694065801 * L_15 = (LinkedListNode_1_t1694065801 *)__this->get_first_3();
		V_0 = (LinkedListNode_1_t1694065801 *)L_15;
		LinkedListNode_1_t1694065801 * L_16 = (LinkedListNode_1_t1694065801 *)__this->get_first_3();
		if (L_16)
		{
			goto IL_007d;
		}
	}
	{
		return;
	}

IL_007d:
	{
		ObjectU5BU5D_t3270211303* L_17 = ___array0;
		int32_t L_18 = ___index1;
		LinkedListNode_1_t1694065801 * L_19 = V_0;
		NullCheck((LinkedListNode_1_t1694065801 *)L_19);
		RuntimeObject * L_20 = ((  RuntimeObject * (*) (LinkedListNode_1_t1694065801 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11)->methodPointer)((LinkedListNode_1_t1694065801 *)L_19, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11));
		NullCheck(L_17);
		(L_17)->SetAt(static_cast<il2cpp_array_size_t>(L_18), (RuntimeObject *)L_20);
		int32_t L_21 = ___index1;
		___index1 = (int32_t)((int32_t)((int32_t)L_21+(int32_t)1));
		LinkedListNode_1_t1694065801 * L_22 = V_0;
		NullCheck(L_22);
		LinkedListNode_1_t1694065801 * L_23 = (LinkedListNode_1_t1694065801 *)L_22->get_forward_2();
		V_0 = (LinkedListNode_1_t1694065801 *)L_23;
		LinkedListNode_1_t1694065801 * L_24 = V_0;
		LinkedListNode_1_t1694065801 * L_25 = (LinkedListNode_1_t1694065801 *)__this->get_first_3();
		if ((!(((RuntimeObject*)(LinkedListNode_1_t1694065801 *)L_24) == ((RuntimeObject*)(LinkedListNode_1_t1694065801 *)L_25))))
		{
			goto IL_007d;
		}
	}
	{
		return;
	}
}
// System.Collections.Generic.LinkedListNode`1<T> System.Collections.Generic.LinkedList`1<System.Object>::Find(T)
extern "C"  LinkedListNode_1_t1694065801 * LinkedList_1_Find_m2424278600_gshared (LinkedList_1_t4198215526 * __this, RuntimeObject * ___value0, const RuntimeMethod* method)
{
	LinkedListNode_1_t1694065801 * V_0 = NULL;
	{
		LinkedListNode_1_t1694065801 * L_0 = (LinkedListNode_1_t1694065801 *)__this->get_first_3();
		V_0 = (LinkedListNode_1_t1694065801 *)L_0;
		LinkedListNode_1_t1694065801 * L_1 = V_0;
		if (L_1)
		{
			goto IL_000f;
		}
	}
	{
		return (LinkedListNode_1_t1694065801 *)NULL;
	}

IL_000f:
	{
		RuntimeObject * L_2 = ___value0;
		if (L_2)
		{
			goto IL_002a;
		}
	}
	{
		LinkedListNode_1_t1694065801 * L_3 = V_0;
		NullCheck((LinkedListNode_1_t1694065801 *)L_3);
		RuntimeObject * L_4 = ((  RuntimeObject * (*) (LinkedListNode_1_t1694065801 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11)->methodPointer)((LinkedListNode_1_t1694065801 *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11));
		if (!L_4)
		{
			goto IL_0052;
		}
	}

IL_002a:
	{
		RuntimeObject * L_5 = ___value0;
		if (!L_5)
		{
			goto IL_0054;
		}
	}
	{
		LinkedListNode_1_t1694065801 * L_6 = V_0;
		NullCheck((LinkedListNode_1_t1694065801 *)L_6);
		RuntimeObject * L_7 = ((  RuntimeObject * (*) (LinkedListNode_1_t1694065801 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11)->methodPointer)((LinkedListNode_1_t1694065801 *)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11));
		NullCheck((RuntimeObject *)(*(&___value0)));
		bool L_8 = VirtFuncInvoker1< bool, RuntimeObject * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, (RuntimeObject *)(*(&___value0)), (RuntimeObject *)L_7);
		if (!L_8)
		{
			goto IL_0054;
		}
	}

IL_0052:
	{
		LinkedListNode_1_t1694065801 * L_9 = V_0;
		return L_9;
	}

IL_0054:
	{
		LinkedListNode_1_t1694065801 * L_10 = V_0;
		NullCheck(L_10);
		LinkedListNode_1_t1694065801 * L_11 = (LinkedListNode_1_t1694065801 *)L_10->get_forward_2();
		V_0 = (LinkedListNode_1_t1694065801 *)L_11;
		LinkedListNode_1_t1694065801 * L_12 = V_0;
		LinkedListNode_1_t1694065801 * L_13 = (LinkedListNode_1_t1694065801 *)__this->get_first_3();
		if ((!(((RuntimeObject*)(LinkedListNode_1_t1694065801 *)L_12) == ((RuntimeObject*)(LinkedListNode_1_t1694065801 *)L_13))))
		{
			goto IL_000f;
		}
	}
	{
		return (LinkedListNode_1_t1694065801 *)NULL;
	}
}
// System.Collections.Generic.LinkedList`1/Enumerator<T> System.Collections.Generic.LinkedList`1<System.Object>::GetEnumerator()
extern "C"  Enumerator_t1983447509  LinkedList_1_GetEnumerator_m3353259314_gshared (LinkedList_1_t4198215526 * __this, const RuntimeMethod* method)
{
	{
		Enumerator_t1983447509  L_0;
		memset(&L_0, 0, sizeof(L_0));
		Enumerator__ctor_m3993907956((&L_0), (LinkedList_1_t4198215526 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 13));
		return L_0;
	}
}
// System.Void System.Collections.Generic.LinkedList`1<System.Object>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void LinkedList_1_GetObjectData_m2710009630_gshared (LinkedList_1_t4198215526 * __this, SerializationInfo_t1606602250 * ___info0, StreamingContext_t4278693462  ___context1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LinkedList_1_GetObjectData_m2710009630_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ObjectU5BU5D_t3270211303* V_0 = NULL;
	{
		uint32_t L_0 = (uint32_t)__this->get_count_0();
		V_0 = (ObjectU5BU5D_t3270211303*)((ObjectU5BU5D_t3270211303*)SZArrayNew(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 14), (uint32_t)(((uintptr_t)L_0))));
		ObjectU5BU5D_t3270211303* L_1 = V_0;
		NullCheck((LinkedList_1_t4198215526 *)__this);
		((  void (*) (LinkedList_1_t4198215526 *, ObjectU5BU5D_t3270211303*, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)((LinkedList_1_t4198215526 *)__this, (ObjectU5BU5D_t3270211303*)L_1, (int32_t)0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		SerializationInfo_t1606602250 * L_2 = ___info0;
		ObjectU5BU5D_t3270211303* L_3 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_4 = Type_GetTypeFromHandle_m269683661(NULL /*static, unused*/, (RuntimeTypeHandle_t845633806 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 15)), /*hidden argument*/NULL);
		NullCheck((SerializationInfo_t1606602250 *)L_2);
		SerializationInfo_AddValue_m22079889((SerializationInfo_t1606602250 *)L_2, (String_t*)_stringLiteral481801365, (RuntimeObject *)(RuntimeObject *)L_3, (Type_t *)L_4, /*hidden argument*/NULL);
		SerializationInfo_t1606602250 * L_5 = ___info0;
		uint32_t L_6 = (uint32_t)__this->get_version_1();
		NullCheck((SerializationInfo_t1606602250 *)L_5);
		SerializationInfo_AddValue_m3901584621((SerializationInfo_t1606602250 *)L_5, (String_t*)_stringLiteral1332662376, (uint32_t)L_6, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Collections.Generic.LinkedList`1<System.Object>::OnDeserialization(System.Object)
extern "C"  void LinkedList_1_OnDeserialization_m1809459994_gshared (LinkedList_1_t4198215526 * __this, RuntimeObject * ___sender0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LinkedList_1_OnDeserialization_m1809459994_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ObjectU5BU5D_t3270211303* V_0 = NULL;
	RuntimeObject * V_1 = NULL;
	ObjectU5BU5D_t3270211303* V_2 = NULL;
	int32_t V_3 = 0;
	{
		SerializationInfo_t1606602250 * L_0 = (SerializationInfo_t1606602250 *)__this->get_si_4();
		if (!L_0)
		{
			goto IL_0074;
		}
	}
	{
		SerializationInfo_t1606602250 * L_1 = (SerializationInfo_t1606602250 *)__this->get_si_4();
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_2 = Type_GetTypeFromHandle_m269683661(NULL /*static, unused*/, (RuntimeTypeHandle_t845633806 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 15)), /*hidden argument*/NULL);
		NullCheck((SerializationInfo_t1606602250 *)L_1);
		RuntimeObject * L_3 = SerializationInfo_GetValue_m284139217((SerializationInfo_t1606602250 *)L_1, (String_t*)_stringLiteral481801365, (Type_t *)L_2, /*hidden argument*/NULL);
		V_0 = (ObjectU5BU5D_t3270211303*)((ObjectU5BU5D_t3270211303*)Castclass((RuntimeObject*)L_3, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2)));
		ObjectU5BU5D_t3270211303* L_4 = V_0;
		if (!L_4)
		{
			goto IL_0057;
		}
	}
	{
		ObjectU5BU5D_t3270211303* L_5 = V_0;
		V_2 = (ObjectU5BU5D_t3270211303*)L_5;
		V_3 = (int32_t)0;
		goto IL_004e;
	}

IL_003a:
	{
		ObjectU5BU5D_t3270211303* L_6 = V_2;
		int32_t L_7 = V_3;
		NullCheck(L_6);
		int32_t L_8 = L_7;
		RuntimeObject * L_9 = (L_6)->GetAt(static_cast<il2cpp_array_size_t>(L_8));
		V_1 = (RuntimeObject *)L_9;
		RuntimeObject * L_10 = V_1;
		NullCheck((LinkedList_1_t4198215526 *)__this);
		((  LinkedListNode_1_t1694065801 * (*) (LinkedList_1_t4198215526 *, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((LinkedList_1_t4198215526 *)__this, (RuntimeObject *)L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		int32_t L_11 = V_3;
		V_3 = (int32_t)((int32_t)((int32_t)L_11+(int32_t)1));
	}

IL_004e:
	{
		int32_t L_12 = V_3;
		ObjectU5BU5D_t3270211303* L_13 = V_2;
		NullCheck(L_13);
		if ((((int32_t)L_12) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_13)->max_length)))))))
		{
			goto IL_003a;
		}
	}

IL_0057:
	{
		SerializationInfo_t1606602250 * L_14 = (SerializationInfo_t1606602250 *)__this->get_si_4();
		NullCheck((SerializationInfo_t1606602250 *)L_14);
		uint32_t L_15 = SerializationInfo_GetUInt32_m2443490372((SerializationInfo_t1606602250 *)L_14, (String_t*)_stringLiteral1332662376, /*hidden argument*/NULL);
		__this->set_version_1(L_15);
		__this->set_si_4((SerializationInfo_t1606602250 *)NULL);
	}

IL_0074:
	{
		return;
	}
}
// System.Boolean System.Collections.Generic.LinkedList`1<System.Object>::Remove(T)
extern "C"  bool LinkedList_1_Remove_m1236833506_gshared (LinkedList_1_t4198215526 * __this, RuntimeObject * ___value0, const RuntimeMethod* method)
{
	LinkedListNode_1_t1694065801 * V_0 = NULL;
	{
		RuntimeObject * L_0 = ___value0;
		NullCheck((LinkedList_1_t4198215526 *)__this);
		LinkedListNode_1_t1694065801 * L_1 = ((  LinkedListNode_1_t1694065801 * (*) (LinkedList_1_t4198215526 *, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 16)->methodPointer)((LinkedList_1_t4198215526 *)__this, (RuntimeObject *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 16));
		V_0 = (LinkedListNode_1_t1694065801 *)L_1;
		LinkedListNode_1_t1694065801 * L_2 = V_0;
		if (L_2)
		{
			goto IL_0010;
		}
	}
	{
		return (bool)0;
	}

IL_0010:
	{
		LinkedListNode_1_t1694065801 * L_3 = V_0;
		NullCheck((LinkedList_1_t4198215526 *)__this);
		((  void (*) (LinkedList_1_t4198215526 *, LinkedListNode_1_t1694065801 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 17)->methodPointer)((LinkedList_1_t4198215526 *)__this, (LinkedListNode_1_t1694065801 *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 17));
		return (bool)1;
	}
}
// System.Void System.Collections.Generic.LinkedList`1<System.Object>::Remove(System.Collections.Generic.LinkedListNode`1<T>)
extern "C"  void LinkedList_1_Remove_m3767768786_gshared (LinkedList_1_t4198215526 * __this, LinkedListNode_1_t1694065801 * ___node0, const RuntimeMethod* method)
{
	{
		LinkedListNode_1_t1694065801 * L_0 = ___node0;
		NullCheck((LinkedList_1_t4198215526 *)__this);
		((  void (*) (LinkedList_1_t4198215526 *, LinkedListNode_1_t1694065801 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 18)->methodPointer)((LinkedList_1_t4198215526 *)__this, (LinkedListNode_1_t1694065801 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 18));
		uint32_t L_1 = (uint32_t)__this->get_count_0();
		__this->set_count_0(((int32_t)((int32_t)L_1-(int32_t)1)));
		uint32_t L_2 = (uint32_t)__this->get_count_0();
		if (L_2)
		{
			goto IL_0027;
		}
	}
	{
		__this->set_first_3((LinkedListNode_1_t1694065801 *)NULL);
	}

IL_0027:
	{
		LinkedListNode_1_t1694065801 * L_3 = ___node0;
		LinkedListNode_1_t1694065801 * L_4 = (LinkedListNode_1_t1694065801 *)__this->get_first_3();
		if ((!(((RuntimeObject*)(LinkedListNode_1_t1694065801 *)L_3) == ((RuntimeObject*)(LinkedListNode_1_t1694065801 *)L_4))))
		{
			goto IL_0044;
		}
	}
	{
		LinkedListNode_1_t1694065801 * L_5 = (LinkedListNode_1_t1694065801 *)__this->get_first_3();
		NullCheck(L_5);
		LinkedListNode_1_t1694065801 * L_6 = (LinkedListNode_1_t1694065801 *)L_5->get_forward_2();
		__this->set_first_3(L_6);
	}

IL_0044:
	{
		uint32_t L_7 = (uint32_t)__this->get_version_1();
		__this->set_version_1(((int32_t)((int32_t)L_7+(int32_t)1)));
		LinkedListNode_1_t1694065801 * L_8 = ___node0;
		NullCheck((LinkedListNode_1_t1694065801 *)L_8);
		((  void (*) (LinkedListNode_1_t1694065801 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 19)->methodPointer)((LinkedListNode_1_t1694065801 *)L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 19));
		return;
	}
}
// System.Void System.Collections.Generic.LinkedList`1<System.Object>::RemoveLast()
extern "C"  void LinkedList_1_RemoveLast_m3908692446_gshared (LinkedList_1_t4198215526 * __this, const RuntimeMethod* method)
{
	{
		LinkedListNode_1_t1694065801 * L_0 = (LinkedListNode_1_t1694065801 *)__this->get_first_3();
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		LinkedListNode_1_t1694065801 * L_1 = (LinkedListNode_1_t1694065801 *)__this->get_first_3();
		NullCheck(L_1);
		LinkedListNode_1_t1694065801 * L_2 = (LinkedListNode_1_t1694065801 *)L_1->get_back_3();
		NullCheck((LinkedList_1_t4198215526 *)__this);
		((  void (*) (LinkedList_1_t4198215526 *, LinkedListNode_1_t1694065801 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 17)->methodPointer)((LinkedList_1_t4198215526 *)__this, (LinkedListNode_1_t1694065801 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 17));
	}

IL_001c:
	{
		return;
	}
}
// System.Int32 System.Collections.Generic.LinkedList`1<System.Object>::get_Count()
extern "C"  int32_t LinkedList_1_get_Count_m3184351784_gshared (LinkedList_1_t4198215526 * __this, const RuntimeMethod* method)
{
	{
		uint32_t L_0 = (uint32_t)__this->get_count_0();
		return L_0;
	}
}
// System.Collections.Generic.LinkedListNode`1<T> System.Collections.Generic.LinkedList`1<System.Object>::get_First()
extern "C"  LinkedListNode_1_t1694065801 * LinkedList_1_get_First_m851636486_gshared (LinkedList_1_t4198215526 * __this, const RuntimeMethod* method)
{
	{
		LinkedListNode_1_t1694065801 * L_0 = (LinkedListNode_1_t1694065801 *)__this->get_first_3();
		return L_0;
	}
}
// System.Void System.Collections.Generic.LinkedList`1<Vuforia.VuforiaManager/TrackableIdPair>::.ctor()
extern "C"  void LinkedList_1__ctor_m1855644674_gshared (LinkedList_1_t3507684622 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LinkedList_1__ctor_m1855644674_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	{
		NullCheck((RuntimeObject *)__this);
		Object__ctor_m2906060009((RuntimeObject *)__this, /*hidden argument*/NULL);
		RuntimeObject * L_0 = (RuntimeObject *)il2cpp_codegen_object_new(RuntimeObject_il2cpp_TypeInfo_var);
		Object__ctor_m2906060009(L_0, /*hidden argument*/NULL);
		__this->set_syncRoot_2(L_0);
		__this->set_first_3((LinkedListNode_1_t1003534897 *)NULL);
		int32_t L_1 = (int32_t)0;
		V_0 = (uint32_t)L_1;
		__this->set_version_1(L_1);
		uint32_t L_2 = V_0;
		__this->set_count_0(L_2);
		return;
	}
}
// System.Void System.Collections.Generic.LinkedList`1<Vuforia.VuforiaManager/TrackableIdPair>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void LinkedList_1__ctor_m3871494995_gshared (LinkedList_1_t3507684622 * __this, SerializationInfo_t1606602250 * ___info0, StreamingContext_t4278693462  ___context1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LinkedList_1__ctor_m3871494995_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NullCheck((LinkedList_1_t3507684622 *)__this);
		((  void (*) (LinkedList_1_t3507684622 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((LinkedList_1_t3507684622 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		SerializationInfo_t1606602250 * L_0 = ___info0;
		__this->set_si_4(L_0);
		RuntimeObject * L_1 = (RuntimeObject *)il2cpp_codegen_object_new(RuntimeObject_il2cpp_TypeInfo_var);
		Object__ctor_m2906060009(L_1, /*hidden argument*/NULL);
		__this->set_syncRoot_2(L_1);
		return;
	}
}
// System.Void System.Collections.Generic.LinkedList`1<Vuforia.VuforiaManager/TrackableIdPair>::System.Collections.Generic.ICollection<T>.Add(T)
extern "C"  void LinkedList_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m2885976074_gshared (LinkedList_1_t3507684622 * __this, TrackableIdPair_t1331427386  ___value0, const RuntimeMethod* method)
{
	{
		TrackableIdPair_t1331427386  L_0 = ___value0;
		NullCheck((LinkedList_1_t3507684622 *)__this);
		((  LinkedListNode_1_t1003534897 * (*) (LinkedList_1_t3507684622 *, TrackableIdPair_t1331427386 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((LinkedList_1_t3507684622 *)__this, (TrackableIdPair_t1331427386 )L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		return;
	}
}
// System.Void System.Collections.Generic.LinkedList`1<Vuforia.VuforiaManager/TrackableIdPair>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void LinkedList_1_System_Collections_ICollection_CopyTo_m2990393639_gshared (LinkedList_1_t3507684622 * __this, RuntimeArray * ___array0, int32_t ___index1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LinkedList_1_System_Collections_ICollection_CopyTo_m2990393639_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	TrackableIdPairU5BU5D_t3613789599* V_0 = NULL;
	{
		RuntimeArray * L_0 = ___array0;
		V_0 = (TrackableIdPairU5BU5D_t3613789599*)((TrackableIdPairU5BU5D_t3613789599*)IsInst((RuntimeObject*)L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2)));
		TrackableIdPairU5BU5D_t3613789599* L_1 = V_0;
		if (L_1)
		{
			goto IL_0018;
		}
	}
	{
		ArgumentException_t4028401650 * L_2 = (ArgumentException_t4028401650 *)il2cpp_codegen_object_new(ArgumentException_t4028401650_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m1186658332(L_2, (String_t*)_stringLiteral961150872, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0018:
	{
		TrackableIdPairU5BU5D_t3613789599* L_3 = V_0;
		int32_t L_4 = ___index1;
		NullCheck((LinkedList_1_t3507684622 *)__this);
		((  void (*) (LinkedList_1_t3507684622 *, TrackableIdPairU5BU5D_t3613789599*, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)((LinkedList_1_t3507684622 *)__this, (TrackableIdPairU5BU5D_t3613789599*)L_3, (int32_t)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		return;
	}
}
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.LinkedList`1<Vuforia.VuforiaManager/TrackableIdPair>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C"  RuntimeObject* LinkedList_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m4082910416_gshared (LinkedList_1_t3507684622 * __this, const RuntimeMethod* method)
{
	{
		NullCheck((LinkedList_1_t3507684622 *)__this);
		Enumerator_t1292916605  L_0 = ((  Enumerator_t1292916605  (*) (LinkedList_1_t3507684622 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)((LinkedList_1_t3507684622 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		Enumerator_t1292916605  L_1 = L_0;
		RuntimeObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5), &L_1);
		return (RuntimeObject*)L_2;
	}
}
// System.Collections.IEnumerator System.Collections.Generic.LinkedList`1<Vuforia.VuforiaManager/TrackableIdPair>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  RuntimeObject* LinkedList_1_System_Collections_IEnumerable_GetEnumerator_m1933315053_gshared (LinkedList_1_t3507684622 * __this, const RuntimeMethod* method)
{
	{
		NullCheck((LinkedList_1_t3507684622 *)__this);
		Enumerator_t1292916605  L_0 = ((  Enumerator_t1292916605  (*) (LinkedList_1_t3507684622 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)((LinkedList_1_t3507684622 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		Enumerator_t1292916605  L_1 = L_0;
		RuntimeObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5), &L_1);
		return (RuntimeObject*)L_2;
	}
}
// System.Boolean System.Collections.Generic.LinkedList`1<Vuforia.VuforiaManager/TrackableIdPair>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool LinkedList_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m2232165652_gshared (LinkedList_1_t3507684622 * __this, const RuntimeMethod* method)
{
	{
		return (bool)0;
	}
}
// System.Boolean System.Collections.Generic.LinkedList`1<Vuforia.VuforiaManager/TrackableIdPair>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool LinkedList_1_System_Collections_ICollection_get_IsSynchronized_m2004479130_gshared (LinkedList_1_t3507684622 * __this, const RuntimeMethod* method)
{
	{
		return (bool)0;
	}
}
// System.Object System.Collections.Generic.LinkedList`1<Vuforia.VuforiaManager/TrackableIdPair>::System.Collections.ICollection.get_SyncRoot()
extern "C"  RuntimeObject * LinkedList_1_System_Collections_ICollection_get_SyncRoot_m2522888611_gshared (LinkedList_1_t3507684622 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = (RuntimeObject *)__this->get_syncRoot_2();
		return L_0;
	}
}
// System.Void System.Collections.Generic.LinkedList`1<Vuforia.VuforiaManager/TrackableIdPair>::VerifyReferencedNode(System.Collections.Generic.LinkedListNode`1<T>)
extern "C"  void LinkedList_1_VerifyReferencedNode_m2144741273_gshared (LinkedList_1_t3507684622 * __this, LinkedListNode_1_t1003534897 * ___node0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LinkedList_1_VerifyReferencedNode_m2144741273_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		LinkedListNode_1_t1003534897 * L_0 = ___node0;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t2943536221 * L_1 = (ArgumentNullException_t2943536221 *)il2cpp_codegen_object_new(ArgumentNullException_t2943536221_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m1483904578(L_1, (String_t*)_stringLiteral3726057294, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0011:
	{
		LinkedListNode_1_t1003534897 * L_2 = ___node0;
		NullCheck((LinkedListNode_1_t1003534897 *)L_2);
		LinkedList_1_t3507684622 * L_3 = ((  LinkedList_1_t3507684622 * (*) (LinkedListNode_1_t1003534897 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((LinkedListNode_1_t1003534897 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		if ((((RuntimeObject*)(LinkedList_1_t3507684622 *)L_3) == ((RuntimeObject*)(LinkedList_1_t3507684622 *)__this)))
		{
			goto IL_0023;
		}
	}
	{
		InvalidOperationException_t2321794232 * L_4 = (InvalidOperationException_t2321794232 *)il2cpp_codegen_object_new(InvalidOperationException_t2321794232_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m3753870925(L_4, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_4);
	}

IL_0023:
	{
		return;
	}
}
// System.Collections.Generic.LinkedListNode`1<T> System.Collections.Generic.LinkedList`1<Vuforia.VuforiaManager/TrackableIdPair>::AddLast(T)
extern "C"  LinkedListNode_1_t1003534897 * LinkedList_1_AddLast_m3073466748_gshared (LinkedList_1_t3507684622 * __this, TrackableIdPair_t1331427386  ___value0, const RuntimeMethod* method)
{
	LinkedListNode_1_t1003534897 * V_0 = NULL;
	{
		LinkedListNode_1_t1003534897 * L_0 = (LinkedListNode_1_t1003534897 *)__this->get_first_3();
		if (L_0)
		{
			goto IL_001f;
		}
	}
	{
		TrackableIdPair_t1331427386  L_1 = ___value0;
		LinkedListNode_1_t1003534897 * L_2 = (LinkedListNode_1_t1003534897 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 7));
		((  void (*) (LinkedListNode_1_t1003534897 *, LinkedList_1_t3507684622 *, TrackableIdPair_t1331427386 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)(L_2, (LinkedList_1_t3507684622 *)__this, (TrackableIdPair_t1331427386 )L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		V_0 = (LinkedListNode_1_t1003534897 *)L_2;
		LinkedListNode_1_t1003534897 * L_3 = V_0;
		__this->set_first_3(L_3);
		goto IL_0038;
	}

IL_001f:
	{
		TrackableIdPair_t1331427386  L_4 = ___value0;
		LinkedListNode_1_t1003534897 * L_5 = (LinkedListNode_1_t1003534897 *)__this->get_first_3();
		NullCheck(L_5);
		LinkedListNode_1_t1003534897 * L_6 = (LinkedListNode_1_t1003534897 *)L_5->get_back_3();
		LinkedListNode_1_t1003534897 * L_7 = (LinkedListNode_1_t1003534897 *)__this->get_first_3();
		LinkedListNode_1_t1003534897 * L_8 = (LinkedListNode_1_t1003534897 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 7));
		((  void (*) (LinkedListNode_1_t1003534897 *, LinkedList_1_t3507684622 *, TrackableIdPair_t1331427386 , LinkedListNode_1_t1003534897 *, LinkedListNode_1_t1003534897 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9)->methodPointer)(L_8, (LinkedList_1_t3507684622 *)__this, (TrackableIdPair_t1331427386 )L_4, (LinkedListNode_1_t1003534897 *)L_6, (LinkedListNode_1_t1003534897 *)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
		V_0 = (LinkedListNode_1_t1003534897 *)L_8;
	}

IL_0038:
	{
		uint32_t L_9 = (uint32_t)__this->get_count_0();
		__this->set_count_0(((int32_t)((int32_t)L_9+(int32_t)1)));
		uint32_t L_10 = (uint32_t)__this->get_version_1();
		__this->set_version_1(((int32_t)((int32_t)L_10+(int32_t)1)));
		LinkedListNode_1_t1003534897 * L_11 = V_0;
		return L_11;
	}
}
// System.Void System.Collections.Generic.LinkedList`1<Vuforia.VuforiaManager/TrackableIdPair>::Clear()
extern "C"  void LinkedList_1_Clear_m2822388346_gshared (LinkedList_1_t3507684622 * __this, const RuntimeMethod* method)
{
	{
		goto IL_000b;
	}

IL_0005:
	{
		NullCheck((LinkedList_1_t3507684622 *)__this);
		((  void (*) (LinkedList_1_t3507684622 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10)->methodPointer)((LinkedList_1_t3507684622 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10));
	}

IL_000b:
	{
		LinkedListNode_1_t1003534897 * L_0 = (LinkedListNode_1_t1003534897 *)__this->get_first_3();
		if (L_0)
		{
			goto IL_0005;
		}
	}
	{
		return;
	}
}
// System.Boolean System.Collections.Generic.LinkedList`1<Vuforia.VuforiaManager/TrackableIdPair>::Contains(T)
extern "C"  bool LinkedList_1_Contains_m308871557_gshared (LinkedList_1_t3507684622 * __this, TrackableIdPair_t1331427386  ___value0, const RuntimeMethod* method)
{
	LinkedListNode_1_t1003534897 * V_0 = NULL;
	{
		LinkedListNode_1_t1003534897 * L_0 = (LinkedListNode_1_t1003534897 *)__this->get_first_3();
		V_0 = (LinkedListNode_1_t1003534897 *)L_0;
		LinkedListNode_1_t1003534897 * L_1 = V_0;
		if (L_1)
		{
			goto IL_000f;
		}
	}
	{
		return (bool)0;
	}

IL_000f:
	{
		LinkedListNode_1_t1003534897 * L_2 = V_0;
		NullCheck((LinkedListNode_1_t1003534897 *)L_2);
		TrackableIdPair_t1331427386  L_3 = ((  TrackableIdPair_t1331427386  (*) (LinkedListNode_1_t1003534897 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11)->methodPointer)((LinkedListNode_1_t1003534897 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11));
		TrackableIdPair_t1331427386  L_4 = L_3;
		RuntimeObject * L_5 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 12), &L_4);
		RuntimeObject * L_6 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 12), (&___value0));
		NullCheck((RuntimeObject *)L_6);
		bool L_7 = VirtFuncInvoker1< bool, RuntimeObject * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, (RuntimeObject *)L_6, (RuntimeObject *)L_5);
		*(&___value0) = *(TrackableIdPair_t1331427386 *)UnBox(L_6);
		if (!L_7)
		{
			goto IL_002e;
		}
	}
	{
		return (bool)1;
	}

IL_002e:
	{
		LinkedListNode_1_t1003534897 * L_8 = V_0;
		NullCheck(L_8);
		LinkedListNode_1_t1003534897 * L_9 = (LinkedListNode_1_t1003534897 *)L_8->get_forward_2();
		V_0 = (LinkedListNode_1_t1003534897 *)L_9;
		LinkedListNode_1_t1003534897 * L_10 = V_0;
		LinkedListNode_1_t1003534897 * L_11 = (LinkedListNode_1_t1003534897 *)__this->get_first_3();
		if ((!(((RuntimeObject*)(LinkedListNode_1_t1003534897 *)L_10) == ((RuntimeObject*)(LinkedListNode_1_t1003534897 *)L_11))))
		{
			goto IL_000f;
		}
	}
	{
		return (bool)0;
	}
}
// System.Void System.Collections.Generic.LinkedList`1<Vuforia.VuforiaManager/TrackableIdPair>::CopyTo(T[],System.Int32)
extern "C"  void LinkedList_1_CopyTo_m2231156345_gshared (LinkedList_1_t3507684622 * __this, TrackableIdPairU5BU5D_t3613789599* ___array0, int32_t ___index1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LinkedList_1_CopyTo_m2231156345_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	LinkedListNode_1_t1003534897 * V_0 = NULL;
	{
		TrackableIdPairU5BU5D_t3613789599* L_0 = ___array0;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t2943536221 * L_1 = (ArgumentNullException_t2943536221 *)il2cpp_codegen_object_new(ArgumentNullException_t2943536221_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m1483904578(L_1, (String_t*)_stringLiteral961150872, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0011:
	{
		int32_t L_2 = ___index1;
		TrackableIdPairU5BU5D_t3613789599* L_3 = ___array0;
		NullCheck((RuntimeArray *)(RuntimeArray *)L_3);
		int32_t L_4 = Array_GetLowerBound_m2726004408((RuntimeArray *)(RuntimeArray *)L_3, (int32_t)0, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_2) < ((uint32_t)L_4))))
		{
			goto IL_0029;
		}
	}
	{
		ArgumentOutOfRangeException_t2174922486 * L_5 = (ArgumentOutOfRangeException_t2174922486 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t2174922486_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m1532465284(L_5, (String_t*)_stringLiteral3160537771, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0029:
	{
		TrackableIdPairU5BU5D_t3613789599* L_6 = ___array0;
		NullCheck((RuntimeArray *)(RuntimeArray *)L_6);
		int32_t L_7 = Array_get_Rank_m2275309645((RuntimeArray *)(RuntimeArray *)L_6, /*hidden argument*/NULL);
		if ((((int32_t)L_7) == ((int32_t)1)))
		{
			goto IL_0045;
		}
	}
	{
		ArgumentException_t4028401650 * L_8 = (ArgumentException_t4028401650 *)il2cpp_codegen_object_new(ArgumentException_t4028401650_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m596893627(L_8, (String_t*)_stringLiteral961150872, (String_t*)_stringLiteral2607546579, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_8);
	}

IL_0045:
	{
		TrackableIdPairU5BU5D_t3613789599* L_9 = ___array0;
		NullCheck(L_9);
		int32_t L_10 = ___index1;
		TrackableIdPairU5BU5D_t3613789599* L_11 = ___array0;
		NullCheck((RuntimeArray *)(RuntimeArray *)L_11);
		int32_t L_12 = Array_GetLowerBound_m2726004408((RuntimeArray *)(RuntimeArray *)L_11, (int32_t)0, /*hidden argument*/NULL);
		uint32_t L_13 = (uint32_t)__this->get_count_0();
		if ((((int64_t)(((int64_t)((int64_t)((int32_t)((int32_t)((int32_t)((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_9)->max_length))))-(int32_t)L_10))+(int32_t)L_12)))))) >= ((int64_t)(((int64_t)((uint64_t)L_13))))))
		{
			goto IL_006a;
		}
	}
	{
		ArgumentException_t4028401650 * L_14 = (ArgumentException_t4028401650 *)il2cpp_codegen_object_new(ArgumentException_t4028401650_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m1186658332(L_14, (String_t*)_stringLiteral1546211127, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_14);
	}

IL_006a:
	{
		LinkedListNode_1_t1003534897 * L_15 = (LinkedListNode_1_t1003534897 *)__this->get_first_3();
		V_0 = (LinkedListNode_1_t1003534897 *)L_15;
		LinkedListNode_1_t1003534897 * L_16 = (LinkedListNode_1_t1003534897 *)__this->get_first_3();
		if (L_16)
		{
			goto IL_007d;
		}
	}
	{
		return;
	}

IL_007d:
	{
		TrackableIdPairU5BU5D_t3613789599* L_17 = ___array0;
		int32_t L_18 = ___index1;
		LinkedListNode_1_t1003534897 * L_19 = V_0;
		NullCheck((LinkedListNode_1_t1003534897 *)L_19);
		TrackableIdPair_t1331427386  L_20 = ((  TrackableIdPair_t1331427386  (*) (LinkedListNode_1_t1003534897 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11)->methodPointer)((LinkedListNode_1_t1003534897 *)L_19, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11));
		NullCheck(L_17);
		(L_17)->SetAt(static_cast<il2cpp_array_size_t>(L_18), (TrackableIdPair_t1331427386 )L_20);
		int32_t L_21 = ___index1;
		___index1 = (int32_t)((int32_t)((int32_t)L_21+(int32_t)1));
		LinkedListNode_1_t1003534897 * L_22 = V_0;
		NullCheck(L_22);
		LinkedListNode_1_t1003534897 * L_23 = (LinkedListNode_1_t1003534897 *)L_22->get_forward_2();
		V_0 = (LinkedListNode_1_t1003534897 *)L_23;
		LinkedListNode_1_t1003534897 * L_24 = V_0;
		LinkedListNode_1_t1003534897 * L_25 = (LinkedListNode_1_t1003534897 *)__this->get_first_3();
		if ((!(((RuntimeObject*)(LinkedListNode_1_t1003534897 *)L_24) == ((RuntimeObject*)(LinkedListNode_1_t1003534897 *)L_25))))
		{
			goto IL_007d;
		}
	}
	{
		return;
	}
}
// System.Collections.Generic.LinkedListNode`1<T> System.Collections.Generic.LinkedList`1<Vuforia.VuforiaManager/TrackableIdPair>::Find(T)
extern "C"  LinkedListNode_1_t1003534897 * LinkedList_1_Find_m1891819819_gshared (LinkedList_1_t3507684622 * __this, TrackableIdPair_t1331427386  ___value0, const RuntimeMethod* method)
{
	LinkedListNode_1_t1003534897 * V_0 = NULL;
	{
		LinkedListNode_1_t1003534897 * L_0 = (LinkedListNode_1_t1003534897 *)__this->get_first_3();
		V_0 = (LinkedListNode_1_t1003534897 *)L_0;
		LinkedListNode_1_t1003534897 * L_1 = V_0;
		if (L_1)
		{
			goto IL_000f;
		}
	}
	{
		return (LinkedListNode_1_t1003534897 *)NULL;
	}

IL_000f:
	{
		goto IL_002a;
	}
	{
		LinkedListNode_1_t1003534897 * L_3 = V_0;
		NullCheck((LinkedListNode_1_t1003534897 *)L_3);
		TrackableIdPair_t1331427386  L_4 = ((  TrackableIdPair_t1331427386  (*) (LinkedListNode_1_t1003534897 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11)->methodPointer)((LinkedListNode_1_t1003534897 *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11));
	}

IL_002a:
	{
	}
	{
		LinkedListNode_1_t1003534897 * L_6 = V_0;
		NullCheck((LinkedListNode_1_t1003534897 *)L_6);
		TrackableIdPair_t1331427386  L_7 = ((  TrackableIdPair_t1331427386  (*) (LinkedListNode_1_t1003534897 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11)->methodPointer)((LinkedListNode_1_t1003534897 *)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11));
		TrackableIdPair_t1331427386  L_8 = L_7;
		RuntimeObject * L_9 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 12), &L_8);
		RuntimeObject * L_10 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 12), (&___value0));
		NullCheck((RuntimeObject *)L_10);
		bool L_11 = VirtFuncInvoker1< bool, RuntimeObject * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, (RuntimeObject *)L_10, (RuntimeObject *)L_9);
		*(&___value0) = *(TrackableIdPair_t1331427386 *)UnBox(L_10);
		if (!L_11)
		{
			goto IL_0054;
		}
	}

IL_0052:
	{
		LinkedListNode_1_t1003534897 * L_12 = V_0;
		return L_12;
	}

IL_0054:
	{
		LinkedListNode_1_t1003534897 * L_13 = V_0;
		NullCheck(L_13);
		LinkedListNode_1_t1003534897 * L_14 = (LinkedListNode_1_t1003534897 *)L_13->get_forward_2();
		V_0 = (LinkedListNode_1_t1003534897 *)L_14;
		LinkedListNode_1_t1003534897 * L_15 = V_0;
		LinkedListNode_1_t1003534897 * L_16 = (LinkedListNode_1_t1003534897 *)__this->get_first_3();
		if ((!(((RuntimeObject*)(LinkedListNode_1_t1003534897 *)L_15) == ((RuntimeObject*)(LinkedListNode_1_t1003534897 *)L_16))))
		{
			goto IL_000f;
		}
	}
	{
		return (LinkedListNode_1_t1003534897 *)NULL;
	}
}
// System.Collections.Generic.LinkedList`1/Enumerator<T> System.Collections.Generic.LinkedList`1<Vuforia.VuforiaManager/TrackableIdPair>::GetEnumerator()
extern "C"  Enumerator_t1292916605  LinkedList_1_GetEnumerator_m1066193812_gshared (LinkedList_1_t3507684622 * __this, const RuntimeMethod* method)
{
	{
		Enumerator_t1292916605  L_0;
		memset(&L_0, 0, sizeof(L_0));
		Enumerator__ctor_m2514524852((&L_0), (LinkedList_1_t3507684622 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 13));
		return L_0;
	}
}
// System.Void System.Collections.Generic.LinkedList`1<Vuforia.VuforiaManager/TrackableIdPair>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void LinkedList_1_GetObjectData_m1696023509_gshared (LinkedList_1_t3507684622 * __this, SerializationInfo_t1606602250 * ___info0, StreamingContext_t4278693462  ___context1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LinkedList_1_GetObjectData_m1696023509_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	TrackableIdPairU5BU5D_t3613789599* V_0 = NULL;
	{
		uint32_t L_0 = (uint32_t)__this->get_count_0();
		V_0 = (TrackableIdPairU5BU5D_t3613789599*)((TrackableIdPairU5BU5D_t3613789599*)SZArrayNew(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 14), (uint32_t)(((uintptr_t)L_0))));
		TrackableIdPairU5BU5D_t3613789599* L_1 = V_0;
		NullCheck((LinkedList_1_t3507684622 *)__this);
		((  void (*) (LinkedList_1_t3507684622 *, TrackableIdPairU5BU5D_t3613789599*, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)((LinkedList_1_t3507684622 *)__this, (TrackableIdPairU5BU5D_t3613789599*)L_1, (int32_t)0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		SerializationInfo_t1606602250 * L_2 = ___info0;
		TrackableIdPairU5BU5D_t3613789599* L_3 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_4 = Type_GetTypeFromHandle_m269683661(NULL /*static, unused*/, (RuntimeTypeHandle_t845633806 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 15)), /*hidden argument*/NULL);
		NullCheck((SerializationInfo_t1606602250 *)L_2);
		SerializationInfo_AddValue_m22079889((SerializationInfo_t1606602250 *)L_2, (String_t*)_stringLiteral481801365, (RuntimeObject *)(RuntimeObject *)L_3, (Type_t *)L_4, /*hidden argument*/NULL);
		SerializationInfo_t1606602250 * L_5 = ___info0;
		uint32_t L_6 = (uint32_t)__this->get_version_1();
		NullCheck((SerializationInfo_t1606602250 *)L_5);
		SerializationInfo_AddValue_m3901584621((SerializationInfo_t1606602250 *)L_5, (String_t*)_stringLiteral1332662376, (uint32_t)L_6, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Collections.Generic.LinkedList`1<Vuforia.VuforiaManager/TrackableIdPair>::OnDeserialization(System.Object)
extern "C"  void LinkedList_1_OnDeserialization_m2897007678_gshared (LinkedList_1_t3507684622 * __this, RuntimeObject * ___sender0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LinkedList_1_OnDeserialization_m2897007678_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	TrackableIdPairU5BU5D_t3613789599* V_0 = NULL;
	TrackableIdPair_t1331427386  V_1;
	memset(&V_1, 0, sizeof(V_1));
	TrackableIdPairU5BU5D_t3613789599* V_2 = NULL;
	int32_t V_3 = 0;
	{
		SerializationInfo_t1606602250 * L_0 = (SerializationInfo_t1606602250 *)__this->get_si_4();
		if (!L_0)
		{
			goto IL_0074;
		}
	}
	{
		SerializationInfo_t1606602250 * L_1 = (SerializationInfo_t1606602250 *)__this->get_si_4();
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_2 = Type_GetTypeFromHandle_m269683661(NULL /*static, unused*/, (RuntimeTypeHandle_t845633806 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 15)), /*hidden argument*/NULL);
		NullCheck((SerializationInfo_t1606602250 *)L_1);
		RuntimeObject * L_3 = SerializationInfo_GetValue_m284139217((SerializationInfo_t1606602250 *)L_1, (String_t*)_stringLiteral481801365, (Type_t *)L_2, /*hidden argument*/NULL);
		V_0 = (TrackableIdPairU5BU5D_t3613789599*)((TrackableIdPairU5BU5D_t3613789599*)Castclass((RuntimeObject*)L_3, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2)));
		TrackableIdPairU5BU5D_t3613789599* L_4 = V_0;
		if (!L_4)
		{
			goto IL_0057;
		}
	}
	{
		TrackableIdPairU5BU5D_t3613789599* L_5 = V_0;
		V_2 = (TrackableIdPairU5BU5D_t3613789599*)L_5;
		V_3 = (int32_t)0;
		goto IL_004e;
	}

IL_003a:
	{
		TrackableIdPairU5BU5D_t3613789599* L_6 = V_2;
		int32_t L_7 = V_3;
		NullCheck(L_6);
		int32_t L_8 = L_7;
		TrackableIdPair_t1331427386  L_9 = (L_6)->GetAt(static_cast<il2cpp_array_size_t>(L_8));
		V_1 = (TrackableIdPair_t1331427386 )L_9;
		TrackableIdPair_t1331427386  L_10 = V_1;
		NullCheck((LinkedList_1_t3507684622 *)__this);
		((  LinkedListNode_1_t1003534897 * (*) (LinkedList_1_t3507684622 *, TrackableIdPair_t1331427386 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((LinkedList_1_t3507684622 *)__this, (TrackableIdPair_t1331427386 )L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		int32_t L_11 = V_3;
		V_3 = (int32_t)((int32_t)((int32_t)L_11+(int32_t)1));
	}

IL_004e:
	{
		int32_t L_12 = V_3;
		TrackableIdPairU5BU5D_t3613789599* L_13 = V_2;
		NullCheck(L_13);
		if ((((int32_t)L_12) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_13)->max_length)))))))
		{
			goto IL_003a;
		}
	}

IL_0057:
	{
		SerializationInfo_t1606602250 * L_14 = (SerializationInfo_t1606602250 *)__this->get_si_4();
		NullCheck((SerializationInfo_t1606602250 *)L_14);
		uint32_t L_15 = SerializationInfo_GetUInt32_m2443490372((SerializationInfo_t1606602250 *)L_14, (String_t*)_stringLiteral1332662376, /*hidden argument*/NULL);
		__this->set_version_1(L_15);
		__this->set_si_4((SerializationInfo_t1606602250 *)NULL);
	}

IL_0074:
	{
		return;
	}
}
// System.Boolean System.Collections.Generic.LinkedList`1<Vuforia.VuforiaManager/TrackableIdPair>::Remove(T)
extern "C"  bool LinkedList_1_Remove_m3073859276_gshared (LinkedList_1_t3507684622 * __this, TrackableIdPair_t1331427386  ___value0, const RuntimeMethod* method)
{
	LinkedListNode_1_t1003534897 * V_0 = NULL;
	{
		TrackableIdPair_t1331427386  L_0 = ___value0;
		NullCheck((LinkedList_1_t3507684622 *)__this);
		LinkedListNode_1_t1003534897 * L_1 = ((  LinkedListNode_1_t1003534897 * (*) (LinkedList_1_t3507684622 *, TrackableIdPair_t1331427386 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 16)->methodPointer)((LinkedList_1_t3507684622 *)__this, (TrackableIdPair_t1331427386 )L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 16));
		V_0 = (LinkedListNode_1_t1003534897 *)L_1;
		LinkedListNode_1_t1003534897 * L_2 = V_0;
		if (L_2)
		{
			goto IL_0010;
		}
	}
	{
		return (bool)0;
	}

IL_0010:
	{
		LinkedListNode_1_t1003534897 * L_3 = V_0;
		NullCheck((LinkedList_1_t3507684622 *)__this);
		((  void (*) (LinkedList_1_t3507684622 *, LinkedListNode_1_t1003534897 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 17)->methodPointer)((LinkedList_1_t3507684622 *)__this, (LinkedListNode_1_t1003534897 *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 17));
		return (bool)1;
	}
}
// System.Void System.Collections.Generic.LinkedList`1<Vuforia.VuforiaManager/TrackableIdPair>::Remove(System.Collections.Generic.LinkedListNode`1<T>)
extern "C"  void LinkedList_1_Remove_m526213087_gshared (LinkedList_1_t3507684622 * __this, LinkedListNode_1_t1003534897 * ___node0, const RuntimeMethod* method)
{
	{
		LinkedListNode_1_t1003534897 * L_0 = ___node0;
		NullCheck((LinkedList_1_t3507684622 *)__this);
		((  void (*) (LinkedList_1_t3507684622 *, LinkedListNode_1_t1003534897 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 18)->methodPointer)((LinkedList_1_t3507684622 *)__this, (LinkedListNode_1_t1003534897 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 18));
		uint32_t L_1 = (uint32_t)__this->get_count_0();
		__this->set_count_0(((int32_t)((int32_t)L_1-(int32_t)1)));
		uint32_t L_2 = (uint32_t)__this->get_count_0();
		if (L_2)
		{
			goto IL_0027;
		}
	}
	{
		__this->set_first_3((LinkedListNode_1_t1003534897 *)NULL);
	}

IL_0027:
	{
		LinkedListNode_1_t1003534897 * L_3 = ___node0;
		LinkedListNode_1_t1003534897 * L_4 = (LinkedListNode_1_t1003534897 *)__this->get_first_3();
		if ((!(((RuntimeObject*)(LinkedListNode_1_t1003534897 *)L_3) == ((RuntimeObject*)(LinkedListNode_1_t1003534897 *)L_4))))
		{
			goto IL_0044;
		}
	}
	{
		LinkedListNode_1_t1003534897 * L_5 = (LinkedListNode_1_t1003534897 *)__this->get_first_3();
		NullCheck(L_5);
		LinkedListNode_1_t1003534897 * L_6 = (LinkedListNode_1_t1003534897 *)L_5->get_forward_2();
		__this->set_first_3(L_6);
	}

IL_0044:
	{
		uint32_t L_7 = (uint32_t)__this->get_version_1();
		__this->set_version_1(((int32_t)((int32_t)L_7+(int32_t)1)));
		LinkedListNode_1_t1003534897 * L_8 = ___node0;
		NullCheck((LinkedListNode_1_t1003534897 *)L_8);
		((  void (*) (LinkedListNode_1_t1003534897 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 19)->methodPointer)((LinkedListNode_1_t1003534897 *)L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 19));
		return;
	}
}
// System.Void System.Collections.Generic.LinkedList`1<Vuforia.VuforiaManager/TrackableIdPair>::RemoveLast()
extern "C"  void LinkedList_1_RemoveLast_m3044783523_gshared (LinkedList_1_t3507684622 * __this, const RuntimeMethod* method)
{
	{
		LinkedListNode_1_t1003534897 * L_0 = (LinkedListNode_1_t1003534897 *)__this->get_first_3();
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		LinkedListNode_1_t1003534897 * L_1 = (LinkedListNode_1_t1003534897 *)__this->get_first_3();
		NullCheck(L_1);
		LinkedListNode_1_t1003534897 * L_2 = (LinkedListNode_1_t1003534897 *)L_1->get_back_3();
		NullCheck((LinkedList_1_t3507684622 *)__this);
		((  void (*) (LinkedList_1_t3507684622 *, LinkedListNode_1_t1003534897 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 17)->methodPointer)((LinkedList_1_t3507684622 *)__this, (LinkedListNode_1_t1003534897 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 17));
	}

IL_001c:
	{
		return;
	}
}
// System.Int32 System.Collections.Generic.LinkedList`1<Vuforia.VuforiaManager/TrackableIdPair>::get_Count()
extern "C"  int32_t LinkedList_1_get_Count_m560184588_gshared (LinkedList_1_t3507684622 * __this, const RuntimeMethod* method)
{
	{
		uint32_t L_0 = (uint32_t)__this->get_count_0();
		return L_0;
	}
}
// System.Collections.Generic.LinkedListNode`1<T> System.Collections.Generic.LinkedList`1<Vuforia.VuforiaManager/TrackableIdPair>::get_First()
extern "C"  LinkedListNode_1_t1003534897 * LinkedList_1_get_First_m2487271612_gshared (LinkedList_1_t3507684622 * __this, const RuntimeMethod* method)
{
	{
		LinkedListNode_1_t1003534897 * L_0 = (LinkedListNode_1_t1003534897 *)__this->get_first_3();
		return L_0;
	}
}
// System.Void System.Collections.Generic.LinkedListNode`1<System.Object>::.ctor(System.Collections.Generic.LinkedList`1<T>,T)
extern "C"  void LinkedListNode_1__ctor_m2812753261_gshared (LinkedListNode_1_t1694065801 * __this, LinkedList_1_t4198215526 * ___list0, RuntimeObject * ___value1, const RuntimeMethod* method)
{
	LinkedListNode_1_t1694065801 * V_0 = NULL;
	{
		NullCheck((RuntimeObject *)__this);
		Object__ctor_m2906060009((RuntimeObject *)__this, /*hidden argument*/NULL);
		LinkedList_1_t4198215526 * L_0 = ___list0;
		__this->set_container_1(L_0);
		RuntimeObject * L_1 = ___value1;
		__this->set_item_0(L_1);
		V_0 = (LinkedListNode_1_t1694065801 *)__this;
		__this->set_forward_2(__this);
		LinkedListNode_1_t1694065801 * L_2 = V_0;
		__this->set_back_3(L_2);
		return;
	}
}
// System.Void System.Collections.Generic.LinkedListNode`1<System.Object>::.ctor(System.Collections.Generic.LinkedList`1<T>,T,System.Collections.Generic.LinkedListNode`1<T>,System.Collections.Generic.LinkedListNode`1<T>)
extern "C"  void LinkedListNode_1__ctor_m2921261439_gshared (LinkedListNode_1_t1694065801 * __this, LinkedList_1_t4198215526 * ___list0, RuntimeObject * ___value1, LinkedListNode_1_t1694065801 * ___previousNode2, LinkedListNode_1_t1694065801 * ___nextNode3, const RuntimeMethod* method)
{
	{
		NullCheck((RuntimeObject *)__this);
		Object__ctor_m2906060009((RuntimeObject *)__this, /*hidden argument*/NULL);
		LinkedList_1_t4198215526 * L_0 = ___list0;
		__this->set_container_1(L_0);
		RuntimeObject * L_1 = ___value1;
		__this->set_item_0(L_1);
		LinkedListNode_1_t1694065801 * L_2 = ___previousNode2;
		__this->set_back_3(L_2);
		LinkedListNode_1_t1694065801 * L_3 = ___nextNode3;
		__this->set_forward_2(L_3);
		LinkedListNode_1_t1694065801 * L_4 = ___previousNode2;
		NullCheck(L_4);
		L_4->set_forward_2(__this);
		LinkedListNode_1_t1694065801 * L_5 = ___nextNode3;
		NullCheck(L_5);
		L_5->set_back_3(__this);
		return;
	}
}
// System.Void System.Collections.Generic.LinkedListNode`1<System.Object>::Detach()
extern "C"  void LinkedListNode_1_Detach_m1784705028_gshared (LinkedListNode_1_t1694065801 * __this, const RuntimeMethod* method)
{
	LinkedListNode_1_t1694065801 * V_0 = NULL;
	{
		LinkedListNode_1_t1694065801 * L_0 = (LinkedListNode_1_t1694065801 *)__this->get_back_3();
		LinkedListNode_1_t1694065801 * L_1 = (LinkedListNode_1_t1694065801 *)__this->get_forward_2();
		NullCheck(L_0);
		L_0->set_forward_2(L_1);
		LinkedListNode_1_t1694065801 * L_2 = (LinkedListNode_1_t1694065801 *)__this->get_forward_2();
		LinkedListNode_1_t1694065801 * L_3 = (LinkedListNode_1_t1694065801 *)__this->get_back_3();
		NullCheck(L_2);
		L_2->set_back_3(L_3);
		V_0 = (LinkedListNode_1_t1694065801 *)NULL;
		__this->set_back_3((LinkedListNode_1_t1694065801 *)NULL);
		LinkedListNode_1_t1694065801 * L_4 = V_0;
		__this->set_forward_2(L_4);
		__this->set_container_1((LinkedList_1_t4198215526 *)NULL);
		return;
	}
}
// System.Collections.Generic.LinkedList`1<T> System.Collections.Generic.LinkedListNode`1<System.Object>::get_List()
extern "C"  LinkedList_1_t4198215526 * LinkedListNode_1_get_List_m3566043944_gshared (LinkedListNode_1_t1694065801 * __this, const RuntimeMethod* method)
{
	{
		LinkedList_1_t4198215526 * L_0 = (LinkedList_1_t4198215526 *)__this->get_container_1();
		return L_0;
	}
}
// System.Collections.Generic.LinkedListNode`1<T> System.Collections.Generic.LinkedListNode`1<System.Object>::get_Next()
extern "C"  LinkedListNode_1_t1694065801 * LinkedListNode_1_get_Next_m3059111788_gshared (LinkedListNode_1_t1694065801 * __this, const RuntimeMethod* method)
{
	LinkedListNode_1_t1694065801 * G_B4_0 = NULL;
	{
		LinkedList_1_t4198215526 * L_0 = (LinkedList_1_t4198215526 *)__this->get_container_1();
		if (!L_0)
		{
			goto IL_002c;
		}
	}
	{
		LinkedListNode_1_t1694065801 * L_1 = (LinkedListNode_1_t1694065801 *)__this->get_forward_2();
		LinkedList_1_t4198215526 * L_2 = (LinkedList_1_t4198215526 *)__this->get_container_1();
		NullCheck(L_2);
		LinkedListNode_1_t1694065801 * L_3 = (LinkedListNode_1_t1694065801 *)L_2->get_first_3();
		if ((((RuntimeObject*)(LinkedListNode_1_t1694065801 *)L_1) == ((RuntimeObject*)(LinkedListNode_1_t1694065801 *)L_3)))
		{
			goto IL_002c;
		}
	}
	{
		LinkedListNode_1_t1694065801 * L_4 = (LinkedListNode_1_t1694065801 *)__this->get_forward_2();
		G_B4_0 = L_4;
		goto IL_002d;
	}

IL_002c:
	{
		G_B4_0 = ((LinkedListNode_1_t1694065801 *)(NULL));
	}

IL_002d:
	{
		return G_B4_0;
	}
}
// T System.Collections.Generic.LinkedListNode`1<System.Object>::get_Value()
extern "C"  RuntimeObject * LinkedListNode_1_get_Value_m1321751551_gshared (LinkedListNode_1_t1694065801 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = (RuntimeObject *)__this->get_item_0();
		return L_0;
	}
}
// System.Void System.Collections.Generic.LinkedListNode`1<Vuforia.VuforiaManager/TrackableIdPair>::.ctor(System.Collections.Generic.LinkedList`1<T>,T)
extern "C"  void LinkedListNode_1__ctor_m663517517_gshared (LinkedListNode_1_t1003534897 * __this, LinkedList_1_t3507684622 * ___list0, TrackableIdPair_t1331427386  ___value1, const RuntimeMethod* method)
{
	LinkedListNode_1_t1003534897 * V_0 = NULL;
	{
		NullCheck((RuntimeObject *)__this);
		Object__ctor_m2906060009((RuntimeObject *)__this, /*hidden argument*/NULL);
		LinkedList_1_t3507684622 * L_0 = ___list0;
		__this->set_container_1(L_0);
		TrackableIdPair_t1331427386  L_1 = ___value1;
		__this->set_item_0(L_1);
		V_0 = (LinkedListNode_1_t1003534897 *)__this;
		__this->set_forward_2(__this);
		LinkedListNode_1_t1003534897 * L_2 = V_0;
		__this->set_back_3(L_2);
		return;
	}
}
// System.Void System.Collections.Generic.LinkedListNode`1<Vuforia.VuforiaManager/TrackableIdPair>::.ctor(System.Collections.Generic.LinkedList`1<T>,T,System.Collections.Generic.LinkedListNode`1<T>,System.Collections.Generic.LinkedListNode`1<T>)
extern "C"  void LinkedListNode_1__ctor_m2021129577_gshared (LinkedListNode_1_t1003534897 * __this, LinkedList_1_t3507684622 * ___list0, TrackableIdPair_t1331427386  ___value1, LinkedListNode_1_t1003534897 * ___previousNode2, LinkedListNode_1_t1003534897 * ___nextNode3, const RuntimeMethod* method)
{
	{
		NullCheck((RuntimeObject *)__this);
		Object__ctor_m2906060009((RuntimeObject *)__this, /*hidden argument*/NULL);
		LinkedList_1_t3507684622 * L_0 = ___list0;
		__this->set_container_1(L_0);
		TrackableIdPair_t1331427386  L_1 = ___value1;
		__this->set_item_0(L_1);
		LinkedListNode_1_t1003534897 * L_2 = ___previousNode2;
		__this->set_back_3(L_2);
		LinkedListNode_1_t1003534897 * L_3 = ___nextNode3;
		__this->set_forward_2(L_3);
		LinkedListNode_1_t1003534897 * L_4 = ___previousNode2;
		NullCheck(L_4);
		L_4->set_forward_2(__this);
		LinkedListNode_1_t1003534897 * L_5 = ___nextNode3;
		NullCheck(L_5);
		L_5->set_back_3(__this);
		return;
	}
}
// System.Void System.Collections.Generic.LinkedListNode`1<Vuforia.VuforiaManager/TrackableIdPair>::Detach()
extern "C"  void LinkedListNode_1_Detach_m3831425376_gshared (LinkedListNode_1_t1003534897 * __this, const RuntimeMethod* method)
{
	LinkedListNode_1_t1003534897 * V_0 = NULL;
	{
		LinkedListNode_1_t1003534897 * L_0 = (LinkedListNode_1_t1003534897 *)__this->get_back_3();
		LinkedListNode_1_t1003534897 * L_1 = (LinkedListNode_1_t1003534897 *)__this->get_forward_2();
		NullCheck(L_0);
		L_0->set_forward_2(L_1);
		LinkedListNode_1_t1003534897 * L_2 = (LinkedListNode_1_t1003534897 *)__this->get_forward_2();
		LinkedListNode_1_t1003534897 * L_3 = (LinkedListNode_1_t1003534897 *)__this->get_back_3();
		NullCheck(L_2);
		L_2->set_back_3(L_3);
		V_0 = (LinkedListNode_1_t1003534897 *)NULL;
		__this->set_back_3((LinkedListNode_1_t1003534897 *)NULL);
		LinkedListNode_1_t1003534897 * L_4 = V_0;
		__this->set_forward_2(L_4);
		__this->set_container_1((LinkedList_1_t3507684622 *)NULL);
		return;
	}
}
// System.Collections.Generic.LinkedList`1<T> System.Collections.Generic.LinkedListNode`1<Vuforia.VuforiaManager/TrackableIdPair>::get_List()
extern "C"  LinkedList_1_t3507684622 * LinkedListNode_1_get_List_m1711929732_gshared (LinkedListNode_1_t1003534897 * __this, const RuntimeMethod* method)
{
	{
		LinkedList_1_t3507684622 * L_0 = (LinkedList_1_t3507684622 *)__this->get_container_1();
		return L_0;
	}
}
// System.Collections.Generic.LinkedListNode`1<T> System.Collections.Generic.LinkedListNode`1<Vuforia.VuforiaManager/TrackableIdPair>::get_Next()
extern "C"  LinkedListNode_1_t1003534897 * LinkedListNode_1_get_Next_m3433668432_gshared (LinkedListNode_1_t1003534897 * __this, const RuntimeMethod* method)
{
	LinkedListNode_1_t1003534897 * G_B4_0 = NULL;
	{
		LinkedList_1_t3507684622 * L_0 = (LinkedList_1_t3507684622 *)__this->get_container_1();
		if (!L_0)
		{
			goto IL_002c;
		}
	}
	{
		LinkedListNode_1_t1003534897 * L_1 = (LinkedListNode_1_t1003534897 *)__this->get_forward_2();
		LinkedList_1_t3507684622 * L_2 = (LinkedList_1_t3507684622 *)__this->get_container_1();
		NullCheck(L_2);
		LinkedListNode_1_t1003534897 * L_3 = (LinkedListNode_1_t1003534897 *)L_2->get_first_3();
		if ((((RuntimeObject*)(LinkedListNode_1_t1003534897 *)L_1) == ((RuntimeObject*)(LinkedListNode_1_t1003534897 *)L_3)))
		{
			goto IL_002c;
		}
	}
	{
		LinkedListNode_1_t1003534897 * L_4 = (LinkedListNode_1_t1003534897 *)__this->get_forward_2();
		G_B4_0 = L_4;
		goto IL_002d;
	}

IL_002c:
	{
		G_B4_0 = ((LinkedListNode_1_t1003534897 *)(NULL));
	}

IL_002d:
	{
		return G_B4_0;
	}
}
// T System.Collections.Generic.LinkedListNode`1<Vuforia.VuforiaManager/TrackableIdPair>::get_Value()
extern "C"  TrackableIdPair_t1331427386  LinkedListNode_1_get_Value_m3404274805_gshared (LinkedListNode_1_t1003534897 * __this, const RuntimeMethod* method)
{
	{
		TrackableIdPair_t1331427386  L_0 = (TrackableIdPair_t1331427386 )__this->get_item_0();
		return L_0;
	}
}
// System.Void System.Collections.Generic.List`1/Enumerator<System.Int32>::.ctor(System.Collections.Generic.List`1<T>)
extern "C"  void Enumerator__ctor_m2268946854_gshared (Enumerator_t876005098 * __this, List_1_t3093093796 * ___l0, const RuntimeMethod* method)
{
	{
		List_1_t3093093796 * L_0 = ___l0;
		__this->set_l_0(L_0);
		List_1_t3093093796 * L_1 = ___l0;
		NullCheck(L_1);
		int32_t L_2 = (int32_t)L_1->get__version_3();
		__this->set_ver_2(L_2);
		return;
	}
}
extern "C"  void Enumerator__ctor_m2268946854_AdjustorThunk (RuntimeObject * __this, List_1_t3093093796 * ___l0, const RuntimeMethod* method)
{
	Enumerator_t876005098 * _thisAdjusted = reinterpret_cast<Enumerator_t876005098 *>(__this + 1);
	Enumerator__ctor_m2268946854(_thisAdjusted, ___l0, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<System.Int32>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m748325870_gshared (Enumerator_t876005098 * __this, const RuntimeMethod* method)
{
	{
		Enumerator_VerifyState_m2157842137((Enumerator_t876005098 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		__this->set_next_1(0);
		return;
	}
}
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m748325870_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Enumerator_t876005098 * _thisAdjusted = reinterpret_cast<Enumerator_t876005098 *>(__this + 1);
	Enumerator_System_Collections_IEnumerator_Reset_m748325870(_thisAdjusted, method);
}
// System.Object System.Collections.Generic.List`1/Enumerator<System.Int32>::System.Collections.IEnumerator.get_Current()
extern "C"  RuntimeObject * Enumerator_System_Collections_IEnumerator_get_Current_m642494324_gshared (Enumerator_t876005098 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_System_Collections_IEnumerator_get_Current_m642494324_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Enumerator_VerifyState_m2157842137((Enumerator_t876005098 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) > ((int32_t)0)))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t2321794232 * L_1 = (InvalidOperationException_t2321794232 *)il2cpp_codegen_object_new(InvalidOperationException_t2321794232_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m3753870925(L_1, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_current_3();
		int32_t L_3 = L_2;
		RuntimeObject * L_4 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_3);
		return L_4;
	}
}
extern "C"  RuntimeObject * Enumerator_System_Collections_IEnumerator_get_Current_m642494324_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Enumerator_t876005098 * _thisAdjusted = reinterpret_cast<Enumerator_t876005098 *>(__this + 1);
	return Enumerator_System_Collections_IEnumerator_get_Current_m642494324(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<System.Int32>::Dispose()
extern "C"  void Enumerator_Dispose_m1755411338_gshared (Enumerator_t876005098 * __this, const RuntimeMethod* method)
{
	{
		__this->set_l_0((List_1_t3093093796 *)NULL);
		return;
	}
}
extern "C"  void Enumerator_Dispose_m1755411338_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Enumerator_t876005098 * _thisAdjusted = reinterpret_cast<Enumerator_t876005098 *>(__this + 1);
	Enumerator_Dispose_m1755411338(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<System.Int32>::VerifyState()
extern "C"  void Enumerator_VerifyState_m2157842137_gshared (Enumerator_t876005098 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_VerifyState_m2157842137_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_t3093093796 * L_0 = (List_1_t3093093796 *)__this->get_l_0();
		if (L_0)
		{
			goto IL_0026;
		}
	}
	{
		Enumerator_t876005098  L_1 = (*(Enumerator_t876005098 *)__this);
		RuntimeObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_1);
		NullCheck((RuntimeObject *)L_2);
		Type_t * L_3 = Object_GetType_m3774351292((RuntimeObject *)L_2, /*hidden argument*/NULL);
		NullCheck((Type_t *)L_3);
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(18 /* System.String System.Type::get_FullName() */, (Type_t *)L_3);
		ObjectDisposedException_t2553224159 * L_5 = (ObjectDisposedException_t2553224159 *)il2cpp_codegen_object_new(ObjectDisposedException_t2553224159_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m4150714796(L_5, (String_t*)L_4, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0026:
	{
		int32_t L_6 = (int32_t)__this->get_ver_2();
		List_1_t3093093796 * L_7 = (List_1_t3093093796 *)__this->get_l_0();
		NullCheck(L_7);
		int32_t L_8 = (int32_t)L_7->get__version_3();
		if ((((int32_t)L_6) == ((int32_t)L_8)))
		{
			goto IL_0047;
		}
	}
	{
		InvalidOperationException_t2321794232 * L_9 = (InvalidOperationException_t2321794232 *)il2cpp_codegen_object_new(InvalidOperationException_t2321794232_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m4101245683(L_9, (String_t*)_stringLiteral556965375, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_9);
	}

IL_0047:
	{
		return;
	}
}
extern "C"  void Enumerator_VerifyState_m2157842137_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Enumerator_t876005098 * _thisAdjusted = reinterpret_cast<Enumerator_t876005098 *>(__this + 1);
	Enumerator_VerifyState_m2157842137(_thisAdjusted, method);
}
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Int32>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m2261539925_gshared (Enumerator_t876005098 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		Enumerator_VerifyState_m2157842137((Enumerator_t876005098 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		int32_t L_1 = (int32_t)__this->get_next_1();
		List_1_t3093093796 * L_2 = (List_1_t3093093796 *)__this->get_l_0();
		NullCheck(L_2);
		int32_t L_3 = (int32_t)L_2->get__size_2();
		if ((((int32_t)L_1) >= ((int32_t)L_3)))
		{
			goto IL_0053;
		}
	}
	{
		List_1_t3093093796 * L_4 = (List_1_t3093093796 *)__this->get_l_0();
		NullCheck(L_4);
		Int32U5BU5D_t2324750880* L_5 = (Int32U5BU5D_t2324750880*)L_4->get__items_1();
		int32_t L_6 = (int32_t)__this->get_next_1();
		int32_t L_7 = (int32_t)L_6;
		V_0 = (int32_t)L_7;
		__this->set_next_1(((int32_t)((int32_t)L_7+(int32_t)1)));
		int32_t L_8 = V_0;
		NullCheck(L_5);
		int32_t L_9 = L_8;
		int32_t L_10 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		__this->set_current_3(L_10);
		return (bool)1;
	}

IL_0053:
	{
		__this->set_next_1((-1));
		return (bool)0;
	}
}
extern "C"  bool Enumerator_MoveNext_m2261539925_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Enumerator_t876005098 * _thisAdjusted = reinterpret_cast<Enumerator_t876005098 *>(__this + 1);
	return Enumerator_MoveNext_m2261539925(_thisAdjusted, method);
}
// T System.Collections.Generic.List`1/Enumerator<System.Int32>::get_Current()
extern "C"  int32_t Enumerator_get_Current_m15219049_gshared (Enumerator_t876005098 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = (int32_t)__this->get_current_3();
		return L_0;
	}
}
extern "C"  int32_t Enumerator_get_Current_m15219049_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Enumerator_t876005098 * _thisAdjusted = reinterpret_cast<Enumerator_t876005098 *>(__this + 1);
	return Enumerator_get_Current_m15219049(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<System.Object>::.ctor(System.Collections.Generic.List`1<T>)
extern "C"  void Enumerator__ctor_m3388526419_gshared (Enumerator_t2954986095 * __this, List_1_t877107497 * ___l0, const RuntimeMethod* method)
{
	{
		List_1_t877107497 * L_0 = ___l0;
		__this->set_l_0(L_0);
		List_1_t877107497 * L_1 = ___l0;
		NullCheck(L_1);
		int32_t L_2 = (int32_t)L_1->get__version_3();
		__this->set_ver_2(L_2);
		return;
	}
}
extern "C"  void Enumerator__ctor_m3388526419_AdjustorThunk (RuntimeObject * __this, List_1_t877107497 * ___l0, const RuntimeMethod* method)
{
	Enumerator_t2954986095 * _thisAdjusted = reinterpret_cast<Enumerator_t2954986095 *>(__this + 1);
	Enumerator__ctor_m3388526419(_thisAdjusted, ___l0, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<System.Object>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m3764657353_gshared (Enumerator_t2954986095 * __this, const RuntimeMethod* method)
{
	{
		Enumerator_VerifyState_m2949725876((Enumerator_t2954986095 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		__this->set_next_1(0);
		return;
	}
}
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m3764657353_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Enumerator_t2954986095 * _thisAdjusted = reinterpret_cast<Enumerator_t2954986095 *>(__this + 1);
	Enumerator_System_Collections_IEnumerator_Reset_m3764657353(_thisAdjusted, method);
}
// System.Object System.Collections.Generic.List`1/Enumerator<System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  RuntimeObject * Enumerator_System_Collections_IEnumerator_get_Current_m2660490385_gshared (Enumerator_t2954986095 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_System_Collections_IEnumerator_get_Current_m2660490385_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Enumerator_VerifyState_m2949725876((Enumerator_t2954986095 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) > ((int32_t)0)))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t2321794232 * L_1 = (InvalidOperationException_t2321794232 *)il2cpp_codegen_object_new(InvalidOperationException_t2321794232_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m3753870925(L_1, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		RuntimeObject * L_2 = (RuntimeObject *)__this->get_current_3();
		return L_2;
	}
}
extern "C"  RuntimeObject * Enumerator_System_Collections_IEnumerator_get_Current_m2660490385_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Enumerator_t2954986095 * _thisAdjusted = reinterpret_cast<Enumerator_t2954986095 *>(__this + 1);
	return Enumerator_System_Collections_IEnumerator_get_Current_m2660490385(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<System.Object>::Dispose()
extern "C"  void Enumerator_Dispose_m1161557187_gshared (Enumerator_t2954986095 * __this, const RuntimeMethod* method)
{
	{
		__this->set_l_0((List_1_t877107497 *)NULL);
		return;
	}
}
extern "C"  void Enumerator_Dispose_m1161557187_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Enumerator_t2954986095 * _thisAdjusted = reinterpret_cast<Enumerator_t2954986095 *>(__this + 1);
	Enumerator_Dispose_m1161557187(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<System.Object>::VerifyState()
extern "C"  void Enumerator_VerifyState_m2949725876_gshared (Enumerator_t2954986095 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_VerifyState_m2949725876_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_t877107497 * L_0 = (List_1_t877107497 *)__this->get_l_0();
		if (L_0)
		{
			goto IL_0026;
		}
	}
	{
		Enumerator_t2954986095  L_1 = (*(Enumerator_t2954986095 *)__this);
		RuntimeObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_1);
		NullCheck((RuntimeObject *)L_2);
		Type_t * L_3 = Object_GetType_m3774351292((RuntimeObject *)L_2, /*hidden argument*/NULL);
		NullCheck((Type_t *)L_3);
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(18 /* System.String System.Type::get_FullName() */, (Type_t *)L_3);
		ObjectDisposedException_t2553224159 * L_5 = (ObjectDisposedException_t2553224159 *)il2cpp_codegen_object_new(ObjectDisposedException_t2553224159_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m4150714796(L_5, (String_t*)L_4, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0026:
	{
		int32_t L_6 = (int32_t)__this->get_ver_2();
		List_1_t877107497 * L_7 = (List_1_t877107497 *)__this->get_l_0();
		NullCheck(L_7);
		int32_t L_8 = (int32_t)L_7->get__version_3();
		if ((((int32_t)L_6) == ((int32_t)L_8)))
		{
			goto IL_0047;
		}
	}
	{
		InvalidOperationException_t2321794232 * L_9 = (InvalidOperationException_t2321794232 *)il2cpp_codegen_object_new(InvalidOperationException_t2321794232_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m4101245683(L_9, (String_t*)_stringLiteral556965375, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_9);
	}

IL_0047:
	{
		return;
	}
}
extern "C"  void Enumerator_VerifyState_m2949725876_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Enumerator_t2954986095 * _thisAdjusted = reinterpret_cast<Enumerator_t2954986095 *>(__this + 1);
	Enumerator_VerifyState_m2949725876(_thisAdjusted, method);
}
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Object>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m1031192525_gshared (Enumerator_t2954986095 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		Enumerator_VerifyState_m2949725876((Enumerator_t2954986095 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		int32_t L_1 = (int32_t)__this->get_next_1();
		List_1_t877107497 * L_2 = (List_1_t877107497 *)__this->get_l_0();
		NullCheck(L_2);
		int32_t L_3 = (int32_t)L_2->get__size_2();
		if ((((int32_t)L_1) >= ((int32_t)L_3)))
		{
			goto IL_0053;
		}
	}
	{
		List_1_t877107497 * L_4 = (List_1_t877107497 *)__this->get_l_0();
		NullCheck(L_4);
		ObjectU5BU5D_t3270211303* L_5 = (ObjectU5BU5D_t3270211303*)L_4->get__items_1();
		int32_t L_6 = (int32_t)__this->get_next_1();
		int32_t L_7 = (int32_t)L_6;
		V_0 = (int32_t)L_7;
		__this->set_next_1(((int32_t)((int32_t)L_7+(int32_t)1)));
		int32_t L_8 = V_0;
		NullCheck(L_5);
		int32_t L_9 = L_8;
		RuntimeObject * L_10 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		__this->set_current_3(L_10);
		return (bool)1;
	}

IL_0053:
	{
		__this->set_next_1((-1));
		return (bool)0;
	}
}
extern "C"  bool Enumerator_MoveNext_m1031192525_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Enumerator_t2954986095 * _thisAdjusted = reinterpret_cast<Enumerator_t2954986095 *>(__this + 1);
	return Enumerator_MoveNext_m1031192525(_thisAdjusted, method);
}
// T System.Collections.Generic.List`1/Enumerator<System.Object>::get_Current()
extern "C"  RuntimeObject * Enumerator_get_Current_m1155707170_gshared (Enumerator_t2954986095 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = (RuntimeObject *)__this->get_current_3();
		return L_0;
	}
}
extern "C"  RuntimeObject * Enumerator_get_Current_m1155707170_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Enumerator_t2954986095 * _thisAdjusted = reinterpret_cast<Enumerator_t2954986095 *>(__this + 1);
	return Enumerator_get_Current_m1155707170(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<System.Reflection.CustomAttributeNamedArgument>::.ctor(System.Collections.Generic.List`1<T>)
extern "C"  void Enumerator__ctor_m3571010397_gshared (Enumerator_t1401966232 * __this, List_1_t3619054930 * ___l0, const RuntimeMethod* method)
{
	{
		List_1_t3619054930 * L_0 = ___l0;
		__this->set_l_0(L_0);
		List_1_t3619054930 * L_1 = ___l0;
		NullCheck(L_1);
		int32_t L_2 = (int32_t)L_1->get__version_3();
		__this->set_ver_2(L_2);
		return;
	}
}
extern "C"  void Enumerator__ctor_m3571010397_AdjustorThunk (RuntimeObject * __this, List_1_t3619054930 * ___l0, const RuntimeMethod* method)
{
	Enumerator_t1401966232 * _thisAdjusted = reinterpret_cast<Enumerator_t1401966232 *>(__this + 1);
	Enumerator__ctor_m3571010397(_thisAdjusted, ___l0, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m239426658_gshared (Enumerator_t1401966232 * __this, const RuntimeMethod* method)
{
	{
		Enumerator_VerifyState_m1611858526((Enumerator_t1401966232 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		__this->set_next_1(0);
		return;
	}
}
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m239426658_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Enumerator_t1401966232 * _thisAdjusted = reinterpret_cast<Enumerator_t1401966232 *>(__this + 1);
	Enumerator_System_Collections_IEnumerator_Reset_m239426658(_thisAdjusted, method);
}
// System.Object System.Collections.Generic.List`1/Enumerator<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IEnumerator.get_Current()
extern "C"  RuntimeObject * Enumerator_System_Collections_IEnumerator_get_Current_m2488257730_gshared (Enumerator_t1401966232 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_System_Collections_IEnumerator_get_Current_m2488257730_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Enumerator_VerifyState_m1611858526((Enumerator_t1401966232 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) > ((int32_t)0)))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t2321794232 * L_1 = (InvalidOperationException_t2321794232 *)il2cpp_codegen_object_new(InvalidOperationException_t2321794232_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m3753870925(L_1, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		CustomAttributeNamedArgument_t468938427  L_2 = (CustomAttributeNamedArgument_t468938427 )__this->get_current_3();
		CustomAttributeNamedArgument_t468938427  L_3 = L_2;
		RuntimeObject * L_4 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_3);
		return L_4;
	}
}
extern "C"  RuntimeObject * Enumerator_System_Collections_IEnumerator_get_Current_m2488257730_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Enumerator_t1401966232 * _thisAdjusted = reinterpret_cast<Enumerator_t1401966232 *>(__this + 1);
	return Enumerator_System_Collections_IEnumerator_get_Current_m2488257730(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<System.Reflection.CustomAttributeNamedArgument>::Dispose()
extern "C"  void Enumerator_Dispose_m2408556530_gshared (Enumerator_t1401966232 * __this, const RuntimeMethod* method)
{
	{
		__this->set_l_0((List_1_t3619054930 *)NULL);
		return;
	}
}
extern "C"  void Enumerator_Dispose_m2408556530_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Enumerator_t1401966232 * _thisAdjusted = reinterpret_cast<Enumerator_t1401966232 *>(__this + 1);
	Enumerator_Dispose_m2408556530(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<System.Reflection.CustomAttributeNamedArgument>::VerifyState()
extern "C"  void Enumerator_VerifyState_m1611858526_gshared (Enumerator_t1401966232 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_VerifyState_m1611858526_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_t3619054930 * L_0 = (List_1_t3619054930 *)__this->get_l_0();
		if (L_0)
		{
			goto IL_0026;
		}
	}
	{
		Enumerator_t1401966232  L_1 = (*(Enumerator_t1401966232 *)__this);
		RuntimeObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_1);
		NullCheck((RuntimeObject *)L_2);
		Type_t * L_3 = Object_GetType_m3774351292((RuntimeObject *)L_2, /*hidden argument*/NULL);
		NullCheck((Type_t *)L_3);
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(18 /* System.String System.Type::get_FullName() */, (Type_t *)L_3);
		ObjectDisposedException_t2553224159 * L_5 = (ObjectDisposedException_t2553224159 *)il2cpp_codegen_object_new(ObjectDisposedException_t2553224159_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m4150714796(L_5, (String_t*)L_4, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0026:
	{
		int32_t L_6 = (int32_t)__this->get_ver_2();
		List_1_t3619054930 * L_7 = (List_1_t3619054930 *)__this->get_l_0();
		NullCheck(L_7);
		int32_t L_8 = (int32_t)L_7->get__version_3();
		if ((((int32_t)L_6) == ((int32_t)L_8)))
		{
			goto IL_0047;
		}
	}
	{
		InvalidOperationException_t2321794232 * L_9 = (InvalidOperationException_t2321794232 *)il2cpp_codegen_object_new(InvalidOperationException_t2321794232_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m4101245683(L_9, (String_t*)_stringLiteral556965375, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_9);
	}

IL_0047:
	{
		return;
	}
}
extern "C"  void Enumerator_VerifyState_m1611858526_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Enumerator_t1401966232 * _thisAdjusted = reinterpret_cast<Enumerator_t1401966232 *>(__this + 1);
	Enumerator_VerifyState_m1611858526(_thisAdjusted, method);
}
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Reflection.CustomAttributeNamedArgument>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m3607670212_gshared (Enumerator_t1401966232 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		Enumerator_VerifyState_m1611858526((Enumerator_t1401966232 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		int32_t L_1 = (int32_t)__this->get_next_1();
		List_1_t3619054930 * L_2 = (List_1_t3619054930 *)__this->get_l_0();
		NullCheck(L_2);
		int32_t L_3 = (int32_t)L_2->get__size_2();
		if ((((int32_t)L_1) >= ((int32_t)L_3)))
		{
			goto IL_0053;
		}
	}
	{
		List_1_t3619054930 * L_4 = (List_1_t3619054930 *)__this->get_l_0();
		NullCheck(L_4);
		CustomAttributeNamedArgumentU5BU5D_t2347765754* L_5 = (CustomAttributeNamedArgumentU5BU5D_t2347765754*)L_4->get__items_1();
		int32_t L_6 = (int32_t)__this->get_next_1();
		int32_t L_7 = (int32_t)L_6;
		V_0 = (int32_t)L_7;
		__this->set_next_1(((int32_t)((int32_t)L_7+(int32_t)1)));
		int32_t L_8 = V_0;
		NullCheck(L_5);
		int32_t L_9 = L_8;
		CustomAttributeNamedArgument_t468938427  L_10 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		__this->set_current_3(L_10);
		return (bool)1;
	}

IL_0053:
	{
		__this->set_next_1((-1));
		return (bool)0;
	}
}
extern "C"  bool Enumerator_MoveNext_m3607670212_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Enumerator_t1401966232 * _thisAdjusted = reinterpret_cast<Enumerator_t1401966232 *>(__this + 1);
	return Enumerator_MoveNext_m3607670212(_thisAdjusted, method);
}
// T System.Collections.Generic.List`1/Enumerator<System.Reflection.CustomAttributeNamedArgument>::get_Current()
extern "C"  CustomAttributeNamedArgument_t468938427  Enumerator_get_Current_m2059411488_gshared (Enumerator_t1401966232 * __this, const RuntimeMethod* method)
{
	{
		CustomAttributeNamedArgument_t468938427  L_0 = (CustomAttributeNamedArgument_t468938427 )__this->get_current_3();
		return L_0;
	}
}
extern "C"  CustomAttributeNamedArgument_t468938427  Enumerator_get_Current_m2059411488_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Enumerator_t1401966232 * _thisAdjusted = reinterpret_cast<Enumerator_t1401966232 *>(__this + 1);
	return Enumerator_get_Current_m2059411488(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<System.Reflection.CustomAttributeTypedArgument>::.ctor(System.Collections.Generic.List`1<T>)
extern "C"  void Enumerator__ctor_m2676834247_gshared (Enumerator_t2583965159 * __this, List_1_t506086561 * ___l0, const RuntimeMethod* method)
{
	{
		List_1_t506086561 * L_0 = ___l0;
		__this->set_l_0(L_0);
		List_1_t506086561 * L_1 = ___l0;
		NullCheck(L_1);
		int32_t L_2 = (int32_t)L_1->get__version_3();
		__this->set_ver_2(L_2);
		return;
	}
}
extern "C"  void Enumerator__ctor_m2676834247_AdjustorThunk (RuntimeObject * __this, List_1_t506086561 * ___l0, const RuntimeMethod* method)
{
	Enumerator_t2583965159 * _thisAdjusted = reinterpret_cast<Enumerator_t2583965159 *>(__this + 1);
	Enumerator__ctor_m2676834247(_thisAdjusted, ___l0, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m1497436992_gshared (Enumerator_t2583965159 * __this, const RuntimeMethod* method)
{
	{
		Enumerator_VerifyState_m145152094((Enumerator_t2583965159 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		__this->set_next_1(0);
		return;
	}
}
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m1497436992_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Enumerator_t2583965159 * _thisAdjusted = reinterpret_cast<Enumerator_t2583965159 *>(__this + 1);
	Enumerator_System_Collections_IEnumerator_Reset_m1497436992(_thisAdjusted, method);
}
// System.Object System.Collections.Generic.List`1/Enumerator<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IEnumerator.get_Current()
extern "C"  RuntimeObject * Enumerator_System_Collections_IEnumerator_get_Current_m3245126976_gshared (Enumerator_t2583965159 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_System_Collections_IEnumerator_get_Current_m3245126976_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Enumerator_VerifyState_m145152094((Enumerator_t2583965159 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) > ((int32_t)0)))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t2321794232 * L_1 = (InvalidOperationException_t2321794232 *)il2cpp_codegen_object_new(InvalidOperationException_t2321794232_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m3753870925(L_1, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		CustomAttributeTypedArgument_t1650937354  L_2 = (CustomAttributeTypedArgument_t1650937354 )__this->get_current_3();
		CustomAttributeTypedArgument_t1650937354  L_3 = L_2;
		RuntimeObject * L_4 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_3);
		return L_4;
	}
}
extern "C"  RuntimeObject * Enumerator_System_Collections_IEnumerator_get_Current_m3245126976_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Enumerator_t2583965159 * _thisAdjusted = reinterpret_cast<Enumerator_t2583965159 *>(__this + 1);
	return Enumerator_System_Collections_IEnumerator_get_Current_m3245126976(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<System.Reflection.CustomAttributeTypedArgument>::Dispose()
extern "C"  void Enumerator_Dispose_m356472712_gshared (Enumerator_t2583965159 * __this, const RuntimeMethod* method)
{
	{
		__this->set_l_0((List_1_t506086561 *)NULL);
		return;
	}
}
extern "C"  void Enumerator_Dispose_m356472712_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Enumerator_t2583965159 * _thisAdjusted = reinterpret_cast<Enumerator_t2583965159 *>(__this + 1);
	Enumerator_Dispose_m356472712(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<System.Reflection.CustomAttributeTypedArgument>::VerifyState()
extern "C"  void Enumerator_VerifyState_m145152094_gshared (Enumerator_t2583965159 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_VerifyState_m145152094_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_t506086561 * L_0 = (List_1_t506086561 *)__this->get_l_0();
		if (L_0)
		{
			goto IL_0026;
		}
	}
	{
		Enumerator_t2583965159  L_1 = (*(Enumerator_t2583965159 *)__this);
		RuntimeObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_1);
		NullCheck((RuntimeObject *)L_2);
		Type_t * L_3 = Object_GetType_m3774351292((RuntimeObject *)L_2, /*hidden argument*/NULL);
		NullCheck((Type_t *)L_3);
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(18 /* System.String System.Type::get_FullName() */, (Type_t *)L_3);
		ObjectDisposedException_t2553224159 * L_5 = (ObjectDisposedException_t2553224159 *)il2cpp_codegen_object_new(ObjectDisposedException_t2553224159_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m4150714796(L_5, (String_t*)L_4, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0026:
	{
		int32_t L_6 = (int32_t)__this->get_ver_2();
		List_1_t506086561 * L_7 = (List_1_t506086561 *)__this->get_l_0();
		NullCheck(L_7);
		int32_t L_8 = (int32_t)L_7->get__version_3();
		if ((((int32_t)L_6) == ((int32_t)L_8)))
		{
			goto IL_0047;
		}
	}
	{
		InvalidOperationException_t2321794232 * L_9 = (InvalidOperationException_t2321794232 *)il2cpp_codegen_object_new(InvalidOperationException_t2321794232_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m4101245683(L_9, (String_t*)_stringLiteral556965375, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_9);
	}

IL_0047:
	{
		return;
	}
}
extern "C"  void Enumerator_VerifyState_m145152094_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Enumerator_t2583965159 * _thisAdjusted = reinterpret_cast<Enumerator_t2583965159 *>(__this + 1);
	Enumerator_VerifyState_m145152094(_thisAdjusted, method);
}
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Reflection.CustomAttributeTypedArgument>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m2784663055_gshared (Enumerator_t2583965159 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		Enumerator_VerifyState_m145152094((Enumerator_t2583965159 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		int32_t L_1 = (int32_t)__this->get_next_1();
		List_1_t506086561 * L_2 = (List_1_t506086561 *)__this->get_l_0();
		NullCheck(L_2);
		int32_t L_3 = (int32_t)L_2->get__size_2();
		if ((((int32_t)L_1) >= ((int32_t)L_3)))
		{
			goto IL_0053;
		}
	}
	{
		List_1_t506086561 * L_4 = (List_1_t506086561 *)__this->get_l_0();
		NullCheck(L_4);
		CustomAttributeTypedArgumentU5BU5D_t3682997903* L_5 = (CustomAttributeTypedArgumentU5BU5D_t3682997903*)L_4->get__items_1();
		int32_t L_6 = (int32_t)__this->get_next_1();
		int32_t L_7 = (int32_t)L_6;
		V_0 = (int32_t)L_7;
		__this->set_next_1(((int32_t)((int32_t)L_7+(int32_t)1)));
		int32_t L_8 = V_0;
		NullCheck(L_5);
		int32_t L_9 = L_8;
		CustomAttributeTypedArgument_t1650937354  L_10 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		__this->set_current_3(L_10);
		return (bool)1;
	}

IL_0053:
	{
		__this->set_next_1((-1));
		return (bool)0;
	}
}
extern "C"  bool Enumerator_MoveNext_m2784663055_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Enumerator_t2583965159 * _thisAdjusted = reinterpret_cast<Enumerator_t2583965159 *>(__this + 1);
	return Enumerator_MoveNext_m2784663055(_thisAdjusted, method);
}
// T System.Collections.Generic.List`1/Enumerator<System.Reflection.CustomAttributeTypedArgument>::get_Current()
extern "C"  CustomAttributeTypedArgument_t1650937354  Enumerator_get_Current_m2957508078_gshared (Enumerator_t2583965159 * __this, const RuntimeMethod* method)
{
	{
		CustomAttributeTypedArgument_t1650937354  L_0 = (CustomAttributeTypedArgument_t1650937354 )__this->get_current_3();
		return L_0;
	}
}
extern "C"  CustomAttributeTypedArgument_t1650937354  Enumerator_get_Current_m2957508078_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Enumerator_t2583965159 * _thisAdjusted = reinterpret_cast<Enumerator_t2583965159 *>(__this + 1);
	return Enumerator_get_Current_m2957508078(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.AnimatorClipInfo>::.ctor(System.Collections.Generic.List`1<T>)
extern "C"  void Enumerator__ctor_m1935377498_gshared (Enumerator_t4248893199 * __this, List_1_t2171014601 * ___l0, const RuntimeMethod* method)
{
	{
		List_1_t2171014601 * L_0 = ___l0;
		__this->set_l_0(L_0);
		List_1_t2171014601 * L_1 = ___l0;
		NullCheck(L_1);
		int32_t L_2 = (int32_t)L_1->get__version_3();
		__this->set_ver_2(L_2);
		return;
	}
}
extern "C"  void Enumerator__ctor_m1935377498_AdjustorThunk (RuntimeObject * __this, List_1_t2171014601 * ___l0, const RuntimeMethod* method)
{
	Enumerator_t4248893199 * _thisAdjusted = reinterpret_cast<Enumerator_t4248893199 *>(__this + 1);
	Enumerator__ctor_m1935377498(_thisAdjusted, ___l0, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.AnimatorClipInfo>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m1345851403_gshared (Enumerator_t4248893199 * __this, const RuntimeMethod* method)
{
	{
		Enumerator_VerifyState_m1238207820((Enumerator_t4248893199 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		__this->set_next_1(0);
		return;
	}
}
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m1345851403_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Enumerator_t4248893199 * _thisAdjusted = reinterpret_cast<Enumerator_t4248893199 *>(__this + 1);
	Enumerator_System_Collections_IEnumerator_Reset_m1345851403(_thisAdjusted, method);
}
// System.Object System.Collections.Generic.List`1/Enumerator<UnityEngine.AnimatorClipInfo>::System.Collections.IEnumerator.get_Current()
extern "C"  RuntimeObject * Enumerator_System_Collections_IEnumerator_get_Current_m4227277968_gshared (Enumerator_t4248893199 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_System_Collections_IEnumerator_get_Current_m4227277968_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Enumerator_VerifyState_m1238207820((Enumerator_t4248893199 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) > ((int32_t)0)))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t2321794232 * L_1 = (InvalidOperationException_t2321794232 *)il2cpp_codegen_object_new(InvalidOperationException_t2321794232_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m3753870925(L_1, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		AnimatorClipInfo_t3315865394  L_2 = (AnimatorClipInfo_t3315865394 )__this->get_current_3();
		AnimatorClipInfo_t3315865394  L_3 = L_2;
		RuntimeObject * L_4 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_3);
		return L_4;
	}
}
extern "C"  RuntimeObject * Enumerator_System_Collections_IEnumerator_get_Current_m4227277968_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Enumerator_t4248893199 * _thisAdjusted = reinterpret_cast<Enumerator_t4248893199 *>(__this + 1);
	return Enumerator_System_Collections_IEnumerator_get_Current_m4227277968(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.AnimatorClipInfo>::Dispose()
extern "C"  void Enumerator_Dispose_m4053495003_gshared (Enumerator_t4248893199 * __this, const RuntimeMethod* method)
{
	{
		__this->set_l_0((List_1_t2171014601 *)NULL);
		return;
	}
}
extern "C"  void Enumerator_Dispose_m4053495003_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Enumerator_t4248893199 * _thisAdjusted = reinterpret_cast<Enumerator_t4248893199 *>(__this + 1);
	Enumerator_Dispose_m4053495003(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.AnimatorClipInfo>::VerifyState()
extern "C"  void Enumerator_VerifyState_m1238207820_gshared (Enumerator_t4248893199 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_VerifyState_m1238207820_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_t2171014601 * L_0 = (List_1_t2171014601 *)__this->get_l_0();
		if (L_0)
		{
			goto IL_0026;
		}
	}
	{
		Enumerator_t4248893199  L_1 = (*(Enumerator_t4248893199 *)__this);
		RuntimeObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_1);
		NullCheck((RuntimeObject *)L_2);
		Type_t * L_3 = Object_GetType_m3774351292((RuntimeObject *)L_2, /*hidden argument*/NULL);
		NullCheck((Type_t *)L_3);
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(18 /* System.String System.Type::get_FullName() */, (Type_t *)L_3);
		ObjectDisposedException_t2553224159 * L_5 = (ObjectDisposedException_t2553224159 *)il2cpp_codegen_object_new(ObjectDisposedException_t2553224159_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m4150714796(L_5, (String_t*)L_4, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0026:
	{
		int32_t L_6 = (int32_t)__this->get_ver_2();
		List_1_t2171014601 * L_7 = (List_1_t2171014601 *)__this->get_l_0();
		NullCheck(L_7);
		int32_t L_8 = (int32_t)L_7->get__version_3();
		if ((((int32_t)L_6) == ((int32_t)L_8)))
		{
			goto IL_0047;
		}
	}
	{
		InvalidOperationException_t2321794232 * L_9 = (InvalidOperationException_t2321794232 *)il2cpp_codegen_object_new(InvalidOperationException_t2321794232_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m4101245683(L_9, (String_t*)_stringLiteral556965375, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_9);
	}

IL_0047:
	{
		return;
	}
}
extern "C"  void Enumerator_VerifyState_m1238207820_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Enumerator_t4248893199 * _thisAdjusted = reinterpret_cast<Enumerator_t4248893199 *>(__this + 1);
	Enumerator_VerifyState_m1238207820(_thisAdjusted, method);
}
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.AnimatorClipInfo>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m4009918722_gshared (Enumerator_t4248893199 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		Enumerator_VerifyState_m1238207820((Enumerator_t4248893199 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		int32_t L_1 = (int32_t)__this->get_next_1();
		List_1_t2171014601 * L_2 = (List_1_t2171014601 *)__this->get_l_0();
		NullCheck(L_2);
		int32_t L_3 = (int32_t)L_2->get__size_2();
		if ((((int32_t)L_1) >= ((int32_t)L_3)))
		{
			goto IL_0053;
		}
	}
	{
		List_1_t2171014601 * L_4 = (List_1_t2171014601 *)__this->get_l_0();
		NullCheck(L_4);
		AnimatorClipInfoU5BU5D_t148995015* L_5 = (AnimatorClipInfoU5BU5D_t148995015*)L_4->get__items_1();
		int32_t L_6 = (int32_t)__this->get_next_1();
		int32_t L_7 = (int32_t)L_6;
		V_0 = (int32_t)L_7;
		__this->set_next_1(((int32_t)((int32_t)L_7+(int32_t)1)));
		int32_t L_8 = V_0;
		NullCheck(L_5);
		int32_t L_9 = L_8;
		AnimatorClipInfo_t3315865394  L_10 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		__this->set_current_3(L_10);
		return (bool)1;
	}

IL_0053:
	{
		__this->set_next_1((-1));
		return (bool)0;
	}
}
extern "C"  bool Enumerator_MoveNext_m4009918722_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Enumerator_t4248893199 * _thisAdjusted = reinterpret_cast<Enumerator_t4248893199 *>(__this + 1);
	return Enumerator_MoveNext_m4009918722(_thisAdjusted, method);
}
// T System.Collections.Generic.List`1/Enumerator<UnityEngine.AnimatorClipInfo>::get_Current()
extern "C"  AnimatorClipInfo_t3315865394  Enumerator_get_Current_m722728543_gshared (Enumerator_t4248893199 * __this, const RuntimeMethod* method)
{
	{
		AnimatorClipInfo_t3315865394  L_0 = (AnimatorClipInfo_t3315865394 )__this->get_current_3();
		return L_0;
	}
}
extern "C"  AnimatorClipInfo_t3315865394  Enumerator_get_Current_m722728543_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Enumerator_t4248893199 * _thisAdjusted = reinterpret_cast<Enumerator_t4248893199 *>(__this + 1);
	return Enumerator_get_Current_m722728543(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Color32>::.ctor(System.Collections.Generic.List`1<T>)
extern "C"  void Enumerator__ctor_m2512276049_gshared (Enumerator_t1218460160 * __this, List_1_t3435548858 * ___l0, const RuntimeMethod* method)
{
	{
		List_1_t3435548858 * L_0 = ___l0;
		__this->set_l_0(L_0);
		List_1_t3435548858 * L_1 = ___l0;
		NullCheck(L_1);
		int32_t L_2 = (int32_t)L_1->get__version_3();
		__this->set_ver_2(L_2);
		return;
	}
}
extern "C"  void Enumerator__ctor_m2512276049_AdjustorThunk (RuntimeObject * __this, List_1_t3435548858 * ___l0, const RuntimeMethod* method)
{
	Enumerator_t1218460160 * _thisAdjusted = reinterpret_cast<Enumerator_t1218460160 *>(__this + 1);
	Enumerator__ctor_m2512276049(_thisAdjusted, ___l0, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Color32>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m3478596844_gshared (Enumerator_t1218460160 * __this, const RuntimeMethod* method)
{
	{
		Enumerator_VerifyState_m1773101339((Enumerator_t1218460160 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		__this->set_next_1(0);
		return;
	}
}
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m3478596844_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Enumerator_t1218460160 * _thisAdjusted = reinterpret_cast<Enumerator_t1218460160 *>(__this + 1);
	Enumerator_System_Collections_IEnumerator_Reset_m3478596844(_thisAdjusted, method);
}
// System.Object System.Collections.Generic.List`1/Enumerator<UnityEngine.Color32>::System.Collections.IEnumerator.get_Current()
extern "C"  RuntimeObject * Enumerator_System_Collections_IEnumerator_get_Current_m3560914353_gshared (Enumerator_t1218460160 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_System_Collections_IEnumerator_get_Current_m3560914353_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Enumerator_VerifyState_m1773101339((Enumerator_t1218460160 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) > ((int32_t)0)))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t2321794232 * L_1 = (InvalidOperationException_t2321794232 *)il2cpp_codegen_object_new(InvalidOperationException_t2321794232_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m3753870925(L_1, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		Color32_t285432355  L_2 = (Color32_t285432355 )__this->get_current_3();
		Color32_t285432355  L_3 = L_2;
		RuntimeObject * L_4 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_3);
		return L_4;
	}
}
extern "C"  RuntimeObject * Enumerator_System_Collections_IEnumerator_get_Current_m3560914353_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Enumerator_t1218460160 * _thisAdjusted = reinterpret_cast<Enumerator_t1218460160 *>(__this + 1);
	return Enumerator_System_Collections_IEnumerator_get_Current_m3560914353(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Color32>::Dispose()
extern "C"  void Enumerator_Dispose_m424538614_gshared (Enumerator_t1218460160 * __this, const RuntimeMethod* method)
{
	{
		__this->set_l_0((List_1_t3435548858 *)NULL);
		return;
	}
}
extern "C"  void Enumerator_Dispose_m424538614_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Enumerator_t1218460160 * _thisAdjusted = reinterpret_cast<Enumerator_t1218460160 *>(__this + 1);
	Enumerator_Dispose_m424538614(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Color32>::VerifyState()
extern "C"  void Enumerator_VerifyState_m1773101339_gshared (Enumerator_t1218460160 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_VerifyState_m1773101339_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_t3435548858 * L_0 = (List_1_t3435548858 *)__this->get_l_0();
		if (L_0)
		{
			goto IL_0026;
		}
	}
	{
		Enumerator_t1218460160  L_1 = (*(Enumerator_t1218460160 *)__this);
		RuntimeObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_1);
		NullCheck((RuntimeObject *)L_2);
		Type_t * L_3 = Object_GetType_m3774351292((RuntimeObject *)L_2, /*hidden argument*/NULL);
		NullCheck((Type_t *)L_3);
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(18 /* System.String System.Type::get_FullName() */, (Type_t *)L_3);
		ObjectDisposedException_t2553224159 * L_5 = (ObjectDisposedException_t2553224159 *)il2cpp_codegen_object_new(ObjectDisposedException_t2553224159_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m4150714796(L_5, (String_t*)L_4, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0026:
	{
		int32_t L_6 = (int32_t)__this->get_ver_2();
		List_1_t3435548858 * L_7 = (List_1_t3435548858 *)__this->get_l_0();
		NullCheck(L_7);
		int32_t L_8 = (int32_t)L_7->get__version_3();
		if ((((int32_t)L_6) == ((int32_t)L_8)))
		{
			goto IL_0047;
		}
	}
	{
		InvalidOperationException_t2321794232 * L_9 = (InvalidOperationException_t2321794232 *)il2cpp_codegen_object_new(InvalidOperationException_t2321794232_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m4101245683(L_9, (String_t*)_stringLiteral556965375, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_9);
	}

IL_0047:
	{
		return;
	}
}
extern "C"  void Enumerator_VerifyState_m1773101339_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Enumerator_t1218460160 * _thisAdjusted = reinterpret_cast<Enumerator_t1218460160 *>(__this + 1);
	Enumerator_VerifyState_m1773101339(_thisAdjusted, method);
}
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.Color32>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m1607965107_gshared (Enumerator_t1218460160 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		Enumerator_VerifyState_m1773101339((Enumerator_t1218460160 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		int32_t L_1 = (int32_t)__this->get_next_1();
		List_1_t3435548858 * L_2 = (List_1_t3435548858 *)__this->get_l_0();
		NullCheck(L_2);
		int32_t L_3 = (int32_t)L_2->get__size_2();
		if ((((int32_t)L_1) >= ((int32_t)L_3)))
		{
			goto IL_0053;
		}
	}
	{
		List_1_t3435548858 * L_4 = (List_1_t3435548858 *)__this->get_l_0();
		NullCheck(L_4);
		Color32U5BU5D_t2266382834* L_5 = (Color32U5BU5D_t2266382834*)L_4->get__items_1();
		int32_t L_6 = (int32_t)__this->get_next_1();
		int32_t L_7 = (int32_t)L_6;
		V_0 = (int32_t)L_7;
		__this->set_next_1(((int32_t)((int32_t)L_7+(int32_t)1)));
		int32_t L_8 = V_0;
		NullCheck(L_5);
		int32_t L_9 = L_8;
		Color32_t285432355  L_10 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		__this->set_current_3(L_10);
		return (bool)1;
	}

IL_0053:
	{
		__this->set_next_1((-1));
		return (bool)0;
	}
}
extern "C"  bool Enumerator_MoveNext_m1607965107_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Enumerator_t1218460160 * _thisAdjusted = reinterpret_cast<Enumerator_t1218460160 *>(__this + 1);
	return Enumerator_MoveNext_m1607965107(_thisAdjusted, method);
}
// T System.Collections.Generic.List`1/Enumerator<UnityEngine.Color32>::get_Current()
extern "C"  Color32_t285432355  Enumerator_get_Current_m2594360256_gshared (Enumerator_t1218460160 * __this, const RuntimeMethod* method)
{
	{
		Color32_t285432355  L_0 = (Color32_t285432355 )__this->get_current_3();
		return L_0;
	}
}
extern "C"  Color32_t285432355  Enumerator_get_Current_m2594360256_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Enumerator_t1218460160 * _thisAdjusted = reinterpret_cast<Enumerator_t1218460160 *>(__this + 1);
	return Enumerator_get_Current_m2594360256(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.EventSystems.RaycastResult>::.ctor(System.Collections.Generic.List`1<T>)
extern "C"  void Enumerator__ctor_m1667883307_gshared (Enumerator_t149250267 * __this, List_1_t2366338965 * ___l0, const RuntimeMethod* method)
{
	{
		List_1_t2366338965 * L_0 = ___l0;
		__this->set_l_0(L_0);
		List_1_t2366338965 * L_1 = ___l0;
		NullCheck(L_1);
		int32_t L_2 = (int32_t)L_1->get__version_3();
		__this->set_ver_2(L_2);
		return;
	}
}
extern "C"  void Enumerator__ctor_m1667883307_AdjustorThunk (RuntimeObject * __this, List_1_t2366338965 * ___l0, const RuntimeMethod* method)
{
	Enumerator_t149250267 * _thisAdjusted = reinterpret_cast<Enumerator_t149250267 *>(__this + 1);
	Enumerator__ctor_m1667883307(_thisAdjusted, ___l0, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.EventSystems.RaycastResult>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m1788010111_gshared (Enumerator_t149250267 * __this, const RuntimeMethod* method)
{
	{
		Enumerator_VerifyState_m1061122233((Enumerator_t149250267 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		__this->set_next_1(0);
		return;
	}
}
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m1788010111_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Enumerator_t149250267 * _thisAdjusted = reinterpret_cast<Enumerator_t149250267 *>(__this + 1);
	Enumerator_System_Collections_IEnumerator_Reset_m1788010111(_thisAdjusted, method);
}
// System.Object System.Collections.Generic.List`1/Enumerator<UnityEngine.EventSystems.RaycastResult>::System.Collections.IEnumerator.get_Current()
extern "C"  RuntimeObject * Enumerator_System_Collections_IEnumerator_get_Current_m2533393695_gshared (Enumerator_t149250267 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_System_Collections_IEnumerator_get_Current_m2533393695_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Enumerator_VerifyState_m1061122233((Enumerator_t149250267 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) > ((int32_t)0)))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t2321794232 * L_1 = (InvalidOperationException_t2321794232 *)il2cpp_codegen_object_new(InvalidOperationException_t2321794232_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m3753870925(L_1, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		RaycastResult_t3511189758  L_2 = (RaycastResult_t3511189758 )__this->get_current_3();
		RaycastResult_t3511189758  L_3 = L_2;
		RuntimeObject * L_4 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_3);
		return L_4;
	}
}
extern "C"  RuntimeObject * Enumerator_System_Collections_IEnumerator_get_Current_m2533393695_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Enumerator_t149250267 * _thisAdjusted = reinterpret_cast<Enumerator_t149250267 *>(__this + 1);
	return Enumerator_System_Collections_IEnumerator_get_Current_m2533393695(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.EventSystems.RaycastResult>::Dispose()
extern "C"  void Enumerator_Dispose_m1255392088_gshared (Enumerator_t149250267 * __this, const RuntimeMethod* method)
{
	{
		__this->set_l_0((List_1_t2366338965 *)NULL);
		return;
	}
}
extern "C"  void Enumerator_Dispose_m1255392088_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Enumerator_t149250267 * _thisAdjusted = reinterpret_cast<Enumerator_t149250267 *>(__this + 1);
	Enumerator_Dispose_m1255392088(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.EventSystems.RaycastResult>::VerifyState()
extern "C"  void Enumerator_VerifyState_m1061122233_gshared (Enumerator_t149250267 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_VerifyState_m1061122233_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_t2366338965 * L_0 = (List_1_t2366338965 *)__this->get_l_0();
		if (L_0)
		{
			goto IL_0026;
		}
	}
	{
		Enumerator_t149250267  L_1 = (*(Enumerator_t149250267 *)__this);
		RuntimeObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_1);
		NullCheck((RuntimeObject *)L_2);
		Type_t * L_3 = Object_GetType_m3774351292((RuntimeObject *)L_2, /*hidden argument*/NULL);
		NullCheck((Type_t *)L_3);
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(18 /* System.String System.Type::get_FullName() */, (Type_t *)L_3);
		ObjectDisposedException_t2553224159 * L_5 = (ObjectDisposedException_t2553224159 *)il2cpp_codegen_object_new(ObjectDisposedException_t2553224159_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m4150714796(L_5, (String_t*)L_4, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0026:
	{
		int32_t L_6 = (int32_t)__this->get_ver_2();
		List_1_t2366338965 * L_7 = (List_1_t2366338965 *)__this->get_l_0();
		NullCheck(L_7);
		int32_t L_8 = (int32_t)L_7->get__version_3();
		if ((((int32_t)L_6) == ((int32_t)L_8)))
		{
			goto IL_0047;
		}
	}
	{
		InvalidOperationException_t2321794232 * L_9 = (InvalidOperationException_t2321794232 *)il2cpp_codegen_object_new(InvalidOperationException_t2321794232_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m4101245683(L_9, (String_t*)_stringLiteral556965375, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_9);
	}

IL_0047:
	{
		return;
	}
}
extern "C"  void Enumerator_VerifyState_m1061122233_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Enumerator_t149250267 * _thisAdjusted = reinterpret_cast<Enumerator_t149250267 *>(__this + 1);
	Enumerator_VerifyState_m1061122233(_thisAdjusted, method);
}
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.EventSystems.RaycastResult>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m3293894458_gshared (Enumerator_t149250267 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		Enumerator_VerifyState_m1061122233((Enumerator_t149250267 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		int32_t L_1 = (int32_t)__this->get_next_1();
		List_1_t2366338965 * L_2 = (List_1_t2366338965 *)__this->get_l_0();
		NullCheck(L_2);
		int32_t L_3 = (int32_t)L_2->get__size_2();
		if ((((int32_t)L_1) >= ((int32_t)L_3)))
		{
			goto IL_0053;
		}
	}
	{
		List_1_t2366338965 * L_4 = (List_1_t2366338965 *)__this->get_l_0();
		NullCheck(L_4);
		RaycastResultU5BU5D_t2863863115* L_5 = (RaycastResultU5BU5D_t2863863115*)L_4->get__items_1();
		int32_t L_6 = (int32_t)__this->get_next_1();
		int32_t L_7 = (int32_t)L_6;
		V_0 = (int32_t)L_7;
		__this->set_next_1(((int32_t)((int32_t)L_7+(int32_t)1)));
		int32_t L_8 = V_0;
		NullCheck(L_5);
		int32_t L_9 = L_8;
		RaycastResult_t3511189758  L_10 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		__this->set_current_3(L_10);
		return (bool)1;
	}

IL_0053:
	{
		__this->set_next_1((-1));
		return (bool)0;
	}
}
extern "C"  bool Enumerator_MoveNext_m3293894458_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Enumerator_t149250267 * _thisAdjusted = reinterpret_cast<Enumerator_t149250267 *>(__this + 1);
	return Enumerator_MoveNext_m3293894458(_thisAdjusted, method);
}
// T System.Collections.Generic.List`1/Enumerator<UnityEngine.EventSystems.RaycastResult>::get_Current()
extern "C"  RaycastResult_t3511189758  Enumerator_get_Current_m3817118942_gshared (Enumerator_t149250267 * __this, const RuntimeMethod* method)
{
	{
		RaycastResult_t3511189758  L_0 = (RaycastResult_t3511189758 )__this->get_current_3();
		return L_0;
	}
}
extern "C"  RaycastResult_t3511189758  Enumerator_get_Current_m3817118942_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Enumerator_t149250267 * _thisAdjusted = reinterpret_cast<Enumerator_t149250267 *>(__this + 1);
	return Enumerator_get_Current_m3817118942(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.UICharInfo>::.ctor(System.Collections.Generic.List`1<T>)
extern "C"  void Enumerator__ctor_m54715403_gshared (Enumerator_t3414180022 * __this, List_1_t1336301424 * ___l0, const RuntimeMethod* method)
{
	{
		List_1_t1336301424 * L_0 = ___l0;
		__this->set_l_0(L_0);
		List_1_t1336301424 * L_1 = ___l0;
		NullCheck(L_1);
		int32_t L_2 = (int32_t)L_1->get__version_3();
		__this->set_ver_2(L_2);
		return;
	}
}
extern "C"  void Enumerator__ctor_m54715403_AdjustorThunk (RuntimeObject * __this, List_1_t1336301424 * ___l0, const RuntimeMethod* method)
{
	Enumerator_t3414180022 * _thisAdjusted = reinterpret_cast<Enumerator_t3414180022 *>(__this + 1);
	Enumerator__ctor_m54715403(_thisAdjusted, ___l0, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.UICharInfo>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m2616961580_gshared (Enumerator_t3414180022 * __this, const RuntimeMethod* method)
{
	{
		Enumerator_VerifyState_m4097623086((Enumerator_t3414180022 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		__this->set_next_1(0);
		return;
	}
}
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m2616961580_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Enumerator_t3414180022 * _thisAdjusted = reinterpret_cast<Enumerator_t3414180022 *>(__this + 1);
	Enumerator_System_Collections_IEnumerator_Reset_m2616961580(_thisAdjusted, method);
}
// System.Object System.Collections.Generic.List`1/Enumerator<UnityEngine.UICharInfo>::System.Collections.IEnumerator.get_Current()
extern "C"  RuntimeObject * Enumerator_System_Collections_IEnumerator_get_Current_m3312567892_gshared (Enumerator_t3414180022 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_System_Collections_IEnumerator_get_Current_m3312567892_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Enumerator_VerifyState_m4097623086((Enumerator_t3414180022 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) > ((int32_t)0)))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t2321794232 * L_1 = (InvalidOperationException_t2321794232 *)il2cpp_codegen_object_new(InvalidOperationException_t2321794232_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m3753870925(L_1, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		UICharInfo_t2481152217  L_2 = (UICharInfo_t2481152217 )__this->get_current_3();
		UICharInfo_t2481152217  L_3 = L_2;
		RuntimeObject * L_4 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_3);
		return L_4;
	}
}
extern "C"  RuntimeObject * Enumerator_System_Collections_IEnumerator_get_Current_m3312567892_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Enumerator_t3414180022 * _thisAdjusted = reinterpret_cast<Enumerator_t3414180022 *>(__this + 1);
	return Enumerator_System_Collections_IEnumerator_get_Current_m3312567892(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.UICharInfo>::Dispose()
extern "C"  void Enumerator_Dispose_m3669109220_gshared (Enumerator_t3414180022 * __this, const RuntimeMethod* method)
{
	{
		__this->set_l_0((List_1_t1336301424 *)NULL);
		return;
	}
}
extern "C"  void Enumerator_Dispose_m3669109220_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Enumerator_t3414180022 * _thisAdjusted = reinterpret_cast<Enumerator_t3414180022 *>(__this + 1);
	Enumerator_Dispose_m3669109220(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.UICharInfo>::VerifyState()
extern "C"  void Enumerator_VerifyState_m4097623086_gshared (Enumerator_t3414180022 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_VerifyState_m4097623086_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_t1336301424 * L_0 = (List_1_t1336301424 *)__this->get_l_0();
		if (L_0)
		{
			goto IL_0026;
		}
	}
	{
		Enumerator_t3414180022  L_1 = (*(Enumerator_t3414180022 *)__this);
		RuntimeObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_1);
		NullCheck((RuntimeObject *)L_2);
		Type_t * L_3 = Object_GetType_m3774351292((RuntimeObject *)L_2, /*hidden argument*/NULL);
		NullCheck((Type_t *)L_3);
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(18 /* System.String System.Type::get_FullName() */, (Type_t *)L_3);
		ObjectDisposedException_t2553224159 * L_5 = (ObjectDisposedException_t2553224159 *)il2cpp_codegen_object_new(ObjectDisposedException_t2553224159_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m4150714796(L_5, (String_t*)L_4, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0026:
	{
		int32_t L_6 = (int32_t)__this->get_ver_2();
		List_1_t1336301424 * L_7 = (List_1_t1336301424 *)__this->get_l_0();
		NullCheck(L_7);
		int32_t L_8 = (int32_t)L_7->get__version_3();
		if ((((int32_t)L_6) == ((int32_t)L_8)))
		{
			goto IL_0047;
		}
	}
	{
		InvalidOperationException_t2321794232 * L_9 = (InvalidOperationException_t2321794232 *)il2cpp_codegen_object_new(InvalidOperationException_t2321794232_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m4101245683(L_9, (String_t*)_stringLiteral556965375, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_9);
	}

IL_0047:
	{
		return;
	}
}
extern "C"  void Enumerator_VerifyState_m4097623086_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Enumerator_t3414180022 * _thisAdjusted = reinterpret_cast<Enumerator_t3414180022 *>(__this + 1);
	Enumerator_VerifyState_m4097623086(_thisAdjusted, method);
}
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.UICharInfo>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m3154098104_gshared (Enumerator_t3414180022 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		Enumerator_VerifyState_m4097623086((Enumerator_t3414180022 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		int32_t L_1 = (int32_t)__this->get_next_1();
		List_1_t1336301424 * L_2 = (List_1_t1336301424 *)__this->get_l_0();
		NullCheck(L_2);
		int32_t L_3 = (int32_t)L_2->get__size_2();
		if ((((int32_t)L_1) >= ((int32_t)L_3)))
		{
			goto IL_0053;
		}
	}
	{
		List_1_t1336301424 * L_4 = (List_1_t1336301424 *)__this->get_l_0();
		NullCheck(L_4);
		UICharInfoU5BU5D_t3065457572* L_5 = (UICharInfoU5BU5D_t3065457572*)L_4->get__items_1();
		int32_t L_6 = (int32_t)__this->get_next_1();
		int32_t L_7 = (int32_t)L_6;
		V_0 = (int32_t)L_7;
		__this->set_next_1(((int32_t)((int32_t)L_7+(int32_t)1)));
		int32_t L_8 = V_0;
		NullCheck(L_5);
		int32_t L_9 = L_8;
		UICharInfo_t2481152217  L_10 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		__this->set_current_3(L_10);
		return (bool)1;
	}

IL_0053:
	{
		__this->set_next_1((-1));
		return (bool)0;
	}
}
extern "C"  bool Enumerator_MoveNext_m3154098104_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Enumerator_t3414180022 * _thisAdjusted = reinterpret_cast<Enumerator_t3414180022 *>(__this + 1);
	return Enumerator_MoveNext_m3154098104(_thisAdjusted, method);
}
// T System.Collections.Generic.List`1/Enumerator<UnityEngine.UICharInfo>::get_Current()
extern "C"  UICharInfo_t2481152217  Enumerator_get_Current_m87748356_gshared (Enumerator_t3414180022 * __this, const RuntimeMethod* method)
{
	{
		UICharInfo_t2481152217  L_0 = (UICharInfo_t2481152217 )__this->get_current_3();
		return L_0;
	}
}
extern "C"  UICharInfo_t2481152217  Enumerator_get_Current_m87748356_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Enumerator_t3414180022 * _thisAdjusted = reinterpret_cast<Enumerator_t3414180022 *>(__this + 1);
	return Enumerator_get_Current_m87748356(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.UILineInfo>::.ctor(System.Collections.Generic.List`1<T>)
extern "C"  void Enumerator__ctor_m4031094966_gshared (Enumerator_t1931539200 * __this, List_1_t4148627898 * ___l0, const RuntimeMethod* method)
{
	{
		List_1_t4148627898 * L_0 = ___l0;
		__this->set_l_0(L_0);
		List_1_t4148627898 * L_1 = ___l0;
		NullCheck(L_1);
		int32_t L_2 = (int32_t)L_1->get__version_3();
		__this->set_ver_2(L_2);
		return;
	}
}
extern "C"  void Enumerator__ctor_m4031094966_AdjustorThunk (RuntimeObject * __this, List_1_t4148627898 * ___l0, const RuntimeMethod* method)
{
	Enumerator_t1931539200 * _thisAdjusted = reinterpret_cast<Enumerator_t1931539200 *>(__this + 1);
	Enumerator__ctor_m4031094966(_thisAdjusted, ___l0, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.UILineInfo>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m3435897786_gshared (Enumerator_t1931539200 * __this, const RuntimeMethod* method)
{
	{
		Enumerator_VerifyState_m3089285984((Enumerator_t1931539200 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		__this->set_next_1(0);
		return;
	}
}
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m3435897786_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Enumerator_t1931539200 * _thisAdjusted = reinterpret_cast<Enumerator_t1931539200 *>(__this + 1);
	Enumerator_System_Collections_IEnumerator_Reset_m3435897786(_thisAdjusted, method);
}
// System.Object System.Collections.Generic.List`1/Enumerator<UnityEngine.UILineInfo>::System.Collections.IEnumerator.get_Current()
extern "C"  RuntimeObject * Enumerator_System_Collections_IEnumerator_get_Current_m1832049754_gshared (Enumerator_t1931539200 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_System_Collections_IEnumerator_get_Current_m1832049754_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Enumerator_VerifyState_m3089285984((Enumerator_t1931539200 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) > ((int32_t)0)))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t2321794232 * L_1 = (InvalidOperationException_t2321794232 *)il2cpp_codegen_object_new(InvalidOperationException_t2321794232_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m3753870925(L_1, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		UILineInfo_t998511395  L_2 = (UILineInfo_t998511395 )__this->get_current_3();
		UILineInfo_t998511395  L_3 = L_2;
		RuntimeObject * L_4 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_3);
		return L_4;
	}
}
extern "C"  RuntimeObject * Enumerator_System_Collections_IEnumerator_get_Current_m1832049754_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Enumerator_t1931539200 * _thisAdjusted = reinterpret_cast<Enumerator_t1931539200 *>(__this + 1);
	return Enumerator_System_Collections_IEnumerator_get_Current_m1832049754(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.UILineInfo>::Dispose()
extern "C"  void Enumerator_Dispose_m294736456_gshared (Enumerator_t1931539200 * __this, const RuntimeMethod* method)
{
	{
		__this->set_l_0((List_1_t4148627898 *)NULL);
		return;
	}
}
extern "C"  void Enumerator_Dispose_m294736456_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Enumerator_t1931539200 * _thisAdjusted = reinterpret_cast<Enumerator_t1931539200 *>(__this + 1);
	Enumerator_Dispose_m294736456(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.UILineInfo>::VerifyState()
extern "C"  void Enumerator_VerifyState_m3089285984_gshared (Enumerator_t1931539200 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_VerifyState_m3089285984_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_t4148627898 * L_0 = (List_1_t4148627898 *)__this->get_l_0();
		if (L_0)
		{
			goto IL_0026;
		}
	}
	{
		Enumerator_t1931539200  L_1 = (*(Enumerator_t1931539200 *)__this);
		RuntimeObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_1);
		NullCheck((RuntimeObject *)L_2);
		Type_t * L_3 = Object_GetType_m3774351292((RuntimeObject *)L_2, /*hidden argument*/NULL);
		NullCheck((Type_t *)L_3);
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(18 /* System.String System.Type::get_FullName() */, (Type_t *)L_3);
		ObjectDisposedException_t2553224159 * L_5 = (ObjectDisposedException_t2553224159 *)il2cpp_codegen_object_new(ObjectDisposedException_t2553224159_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m4150714796(L_5, (String_t*)L_4, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0026:
	{
		int32_t L_6 = (int32_t)__this->get_ver_2();
		List_1_t4148627898 * L_7 = (List_1_t4148627898 *)__this->get_l_0();
		NullCheck(L_7);
		int32_t L_8 = (int32_t)L_7->get__version_3();
		if ((((int32_t)L_6) == ((int32_t)L_8)))
		{
			goto IL_0047;
		}
	}
	{
		InvalidOperationException_t2321794232 * L_9 = (InvalidOperationException_t2321794232 *)il2cpp_codegen_object_new(InvalidOperationException_t2321794232_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m4101245683(L_9, (String_t*)_stringLiteral556965375, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_9);
	}

IL_0047:
	{
		return;
	}
}
extern "C"  void Enumerator_VerifyState_m3089285984_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Enumerator_t1931539200 * _thisAdjusted = reinterpret_cast<Enumerator_t1931539200 *>(__this + 1);
	Enumerator_VerifyState_m3089285984(_thisAdjusted, method);
}
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.UILineInfo>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m2005060853_gshared (Enumerator_t1931539200 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		Enumerator_VerifyState_m3089285984((Enumerator_t1931539200 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		int32_t L_1 = (int32_t)__this->get_next_1();
		List_1_t4148627898 * L_2 = (List_1_t4148627898 *)__this->get_l_0();
		NullCheck(L_2);
		int32_t L_3 = (int32_t)L_2->get__size_2();
		if ((((int32_t)L_1) >= ((int32_t)L_3)))
		{
			goto IL_0053;
		}
	}
	{
		List_1_t4148627898 * L_4 = (List_1_t4148627898 *)__this->get_l_0();
		NullCheck(L_4);
		UILineInfoU5BU5D_t38175986* L_5 = (UILineInfoU5BU5D_t38175986*)L_4->get__items_1();
		int32_t L_6 = (int32_t)__this->get_next_1();
		int32_t L_7 = (int32_t)L_6;
		V_0 = (int32_t)L_7;
		__this->set_next_1(((int32_t)((int32_t)L_7+(int32_t)1)));
		int32_t L_8 = V_0;
		NullCheck(L_5);
		int32_t L_9 = L_8;
		UILineInfo_t998511395  L_10 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		__this->set_current_3(L_10);
		return (bool)1;
	}

IL_0053:
	{
		__this->set_next_1((-1));
		return (bool)0;
	}
}
extern "C"  bool Enumerator_MoveNext_m2005060853_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Enumerator_t1931539200 * _thisAdjusted = reinterpret_cast<Enumerator_t1931539200 *>(__this + 1);
	return Enumerator_MoveNext_m2005060853(_thisAdjusted, method);
}
// T System.Collections.Generic.List`1/Enumerator<UnityEngine.UILineInfo>::get_Current()
extern "C"  UILineInfo_t998511395  Enumerator_get_Current_m2658112968_gshared (Enumerator_t1931539200 * __this, const RuntimeMethod* method)
{
	{
		UILineInfo_t998511395  L_0 = (UILineInfo_t998511395 )__this->get_current_3();
		return L_0;
	}
}
extern "C"  UILineInfo_t998511395  Enumerator_get_Current_m2658112968_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Enumerator_t1931539200 * _thisAdjusted = reinterpret_cast<Enumerator_t1931539200 *>(__this + 1);
	return Enumerator_get_Current_m2658112968(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.UIVertex>::.ctor(System.Collections.Generic.List`1<T>)
extern "C"  void Enumerator__ctor_m615483673_gshared (Enumerator_t3719495245 * __this, List_1_t1641616647 * ___l0, const RuntimeMethod* method)
{
	{
		List_1_t1641616647 * L_0 = ___l0;
		__this->set_l_0(L_0);
		List_1_t1641616647 * L_1 = ___l0;
		NullCheck(L_1);
		int32_t L_2 = (int32_t)L_1->get__version_3();
		__this->set_ver_2(L_2);
		return;
	}
}
extern "C"  void Enumerator__ctor_m615483673_AdjustorThunk (RuntimeObject * __this, List_1_t1641616647 * ___l0, const RuntimeMethod* method)
{
	Enumerator_t3719495245 * _thisAdjusted = reinterpret_cast<Enumerator_t3719495245 *>(__this + 1);
	Enumerator__ctor_m615483673(_thisAdjusted, ___l0, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.UIVertex>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m3546593789_gshared (Enumerator_t3719495245 * __this, const RuntimeMethod* method)
{
	{
		Enumerator_VerifyState_m2357532199((Enumerator_t3719495245 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		__this->set_next_1(0);
		return;
	}
}
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m3546593789_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Enumerator_t3719495245 * _thisAdjusted = reinterpret_cast<Enumerator_t3719495245 *>(__this + 1);
	Enumerator_System_Collections_IEnumerator_Reset_m3546593789(_thisAdjusted, method);
}
// System.Object System.Collections.Generic.List`1/Enumerator<UnityEngine.UIVertex>::System.Collections.IEnumerator.get_Current()
extern "C"  RuntimeObject * Enumerator_System_Collections_IEnumerator_get_Current_m1198406995_gshared (Enumerator_t3719495245 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_System_Collections_IEnumerator_get_Current_m1198406995_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Enumerator_VerifyState_m2357532199((Enumerator_t3719495245 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) > ((int32_t)0)))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t2321794232 * L_1 = (InvalidOperationException_t2321794232 *)il2cpp_codegen_object_new(InvalidOperationException_t2321794232_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m3753870925(L_1, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		UIVertex_t2786467440  L_2 = (UIVertex_t2786467440 )__this->get_current_3();
		UIVertex_t2786467440  L_3 = L_2;
		RuntimeObject * L_4 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_3);
		return L_4;
	}
}
extern "C"  RuntimeObject * Enumerator_System_Collections_IEnumerator_get_Current_m1198406995_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Enumerator_t3719495245 * _thisAdjusted = reinterpret_cast<Enumerator_t3719495245 *>(__this + 1);
	return Enumerator_System_Collections_IEnumerator_get_Current_m1198406995(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.UIVertex>::Dispose()
extern "C"  void Enumerator_Dispose_m18990885_gshared (Enumerator_t3719495245 * __this, const RuntimeMethod* method)
{
	{
		__this->set_l_0((List_1_t1641616647 *)NULL);
		return;
	}
}
extern "C"  void Enumerator_Dispose_m18990885_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Enumerator_t3719495245 * _thisAdjusted = reinterpret_cast<Enumerator_t3719495245 *>(__this + 1);
	Enumerator_Dispose_m18990885(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.UIVertex>::VerifyState()
extern "C"  void Enumerator_VerifyState_m2357532199_gshared (Enumerator_t3719495245 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_VerifyState_m2357532199_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_t1641616647 * L_0 = (List_1_t1641616647 *)__this->get_l_0();
		if (L_0)
		{
			goto IL_0026;
		}
	}
	{
		Enumerator_t3719495245  L_1 = (*(Enumerator_t3719495245 *)__this);
		RuntimeObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_1);
		NullCheck((RuntimeObject *)L_2);
		Type_t * L_3 = Object_GetType_m3774351292((RuntimeObject *)L_2, /*hidden argument*/NULL);
		NullCheck((Type_t *)L_3);
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(18 /* System.String System.Type::get_FullName() */, (Type_t *)L_3);
		ObjectDisposedException_t2553224159 * L_5 = (ObjectDisposedException_t2553224159 *)il2cpp_codegen_object_new(ObjectDisposedException_t2553224159_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m4150714796(L_5, (String_t*)L_4, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0026:
	{
		int32_t L_6 = (int32_t)__this->get_ver_2();
		List_1_t1641616647 * L_7 = (List_1_t1641616647 *)__this->get_l_0();
		NullCheck(L_7);
		int32_t L_8 = (int32_t)L_7->get__version_3();
		if ((((int32_t)L_6) == ((int32_t)L_8)))
		{
			goto IL_0047;
		}
	}
	{
		InvalidOperationException_t2321794232 * L_9 = (InvalidOperationException_t2321794232 *)il2cpp_codegen_object_new(InvalidOperationException_t2321794232_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m4101245683(L_9, (String_t*)_stringLiteral556965375, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_9);
	}

IL_0047:
	{
		return;
	}
}
extern "C"  void Enumerator_VerifyState_m2357532199_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Enumerator_t3719495245 * _thisAdjusted = reinterpret_cast<Enumerator_t3719495245 *>(__this + 1);
	Enumerator_VerifyState_m2357532199(_thisAdjusted, method);
}
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.UIVertex>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m1509398310_gshared (Enumerator_t3719495245 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		Enumerator_VerifyState_m2357532199((Enumerator_t3719495245 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		int32_t L_1 = (int32_t)__this->get_next_1();
		List_1_t1641616647 * L_2 = (List_1_t1641616647 *)__this->get_l_0();
		NullCheck(L_2);
		int32_t L_3 = (int32_t)L_2->get__size_2();
		if ((((int32_t)L_1) >= ((int32_t)L_3)))
		{
			goto IL_0053;
		}
	}
	{
		List_1_t1641616647 * L_4 = (List_1_t1641616647 *)__this->get_l_0();
		NullCheck(L_4);
		UIVertexU5BU5D_t227469777* L_5 = (UIVertexU5BU5D_t227469777*)L_4->get__items_1();
		int32_t L_6 = (int32_t)__this->get_next_1();
		int32_t L_7 = (int32_t)L_6;
		V_0 = (int32_t)L_7;
		__this->set_next_1(((int32_t)((int32_t)L_7+(int32_t)1)));
		int32_t L_8 = V_0;
		NullCheck(L_5);
		int32_t L_9 = L_8;
		UIVertex_t2786467440  L_10 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		__this->set_current_3(L_10);
		return (bool)1;
	}

IL_0053:
	{
		__this->set_next_1((-1));
		return (bool)0;
	}
}
extern "C"  bool Enumerator_MoveNext_m1509398310_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Enumerator_t3719495245 * _thisAdjusted = reinterpret_cast<Enumerator_t3719495245 *>(__this + 1);
	return Enumerator_MoveNext_m1509398310(_thisAdjusted, method);
}
// T System.Collections.Generic.List`1/Enumerator<UnityEngine.UIVertex>::get_Current()
extern "C"  UIVertex_t2786467440  Enumerator_get_Current_m2212027098_gshared (Enumerator_t3719495245 * __this, const RuntimeMethod* method)
{
	{
		UIVertex_t2786467440  L_0 = (UIVertex_t2786467440 )__this->get_current_3();
		return L_0;
	}
}
extern "C"  UIVertex_t2786467440  Enumerator_get_Current_m2212027098_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Enumerator_t3719495245 * _thisAdjusted = reinterpret_cast<Enumerator_t3719495245 *>(__this + 1);
	return Enumerator_get_Current_m2212027098(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Vector2>::.ctor(System.Collections.Generic.List`1<T>)
extern "C"  void Enumerator__ctor_m3347994582_gshared (Enumerator_t215393771 * __this, List_1_t2432482469 * ___l0, const RuntimeMethod* method)
{
	{
		List_1_t2432482469 * L_0 = ___l0;
		__this->set_l_0(L_0);
		List_1_t2432482469 * L_1 = ___l0;
		NullCheck(L_1);
		int32_t L_2 = (int32_t)L_1->get__version_3();
		__this->set_ver_2(L_2);
		return;
	}
}
extern "C"  void Enumerator__ctor_m3347994582_AdjustorThunk (RuntimeObject * __this, List_1_t2432482469 * ___l0, const RuntimeMethod* method)
{
	Enumerator_t215393771 * _thisAdjusted = reinterpret_cast<Enumerator_t215393771 *>(__this + 1);
	Enumerator__ctor_m3347994582(_thisAdjusted, ___l0, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Vector2>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m2221689747_gshared (Enumerator_t215393771 * __this, const RuntimeMethod* method)
{
	{
		Enumerator_VerifyState_m3589629877((Enumerator_t215393771 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		__this->set_next_1(0);
		return;
	}
}
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m2221689747_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Enumerator_t215393771 * _thisAdjusted = reinterpret_cast<Enumerator_t215393771 *>(__this + 1);
	Enumerator_System_Collections_IEnumerator_Reset_m2221689747(_thisAdjusted, method);
}
// System.Object System.Collections.Generic.List`1/Enumerator<UnityEngine.Vector2>::System.Collections.IEnumerator.get_Current()
extern "C"  RuntimeObject * Enumerator_System_Collections_IEnumerator_get_Current_m741422485_gshared (Enumerator_t215393771 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_System_Collections_IEnumerator_get_Current_m741422485_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Enumerator_VerifyState_m3589629877((Enumerator_t215393771 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) > ((int32_t)0)))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t2321794232 * L_1 = (InvalidOperationException_t2321794232 *)il2cpp_codegen_object_new(InvalidOperationException_t2321794232_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m3753870925(L_1, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		Vector2_t3577333262  L_2 = (Vector2_t3577333262 )__this->get_current_3();
		Vector2_t3577333262  L_3 = L_2;
		RuntimeObject * L_4 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_3);
		return L_4;
	}
}
extern "C"  RuntimeObject * Enumerator_System_Collections_IEnumerator_get_Current_m741422485_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Enumerator_t215393771 * _thisAdjusted = reinterpret_cast<Enumerator_t215393771 *>(__this + 1);
	return Enumerator_System_Collections_IEnumerator_get_Current_m741422485(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Vector2>::Dispose()
extern "C"  void Enumerator_Dispose_m3955780047_gshared (Enumerator_t215393771 * __this, const RuntimeMethod* method)
{
	{
		__this->set_l_0((List_1_t2432482469 *)NULL);
		return;
	}
}
extern "C"  void Enumerator_Dispose_m3955780047_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Enumerator_t215393771 * _thisAdjusted = reinterpret_cast<Enumerator_t215393771 *>(__this + 1);
	Enumerator_Dispose_m3955780047(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Vector2>::VerifyState()
extern "C"  void Enumerator_VerifyState_m3589629877_gshared (Enumerator_t215393771 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_VerifyState_m3589629877_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_t2432482469 * L_0 = (List_1_t2432482469 *)__this->get_l_0();
		if (L_0)
		{
			goto IL_0026;
		}
	}
	{
		Enumerator_t215393771  L_1 = (*(Enumerator_t215393771 *)__this);
		RuntimeObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_1);
		NullCheck((RuntimeObject *)L_2);
		Type_t * L_3 = Object_GetType_m3774351292((RuntimeObject *)L_2, /*hidden argument*/NULL);
		NullCheck((Type_t *)L_3);
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(18 /* System.String System.Type::get_FullName() */, (Type_t *)L_3);
		ObjectDisposedException_t2553224159 * L_5 = (ObjectDisposedException_t2553224159 *)il2cpp_codegen_object_new(ObjectDisposedException_t2553224159_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m4150714796(L_5, (String_t*)L_4, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0026:
	{
		int32_t L_6 = (int32_t)__this->get_ver_2();
		List_1_t2432482469 * L_7 = (List_1_t2432482469 *)__this->get_l_0();
		NullCheck(L_7);
		int32_t L_8 = (int32_t)L_7->get__version_3();
		if ((((int32_t)L_6) == ((int32_t)L_8)))
		{
			goto IL_0047;
		}
	}
	{
		InvalidOperationException_t2321794232 * L_9 = (InvalidOperationException_t2321794232 *)il2cpp_codegen_object_new(InvalidOperationException_t2321794232_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m4101245683(L_9, (String_t*)_stringLiteral556965375, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_9);
	}

IL_0047:
	{
		return;
	}
}
extern "C"  void Enumerator_VerifyState_m3589629877_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Enumerator_t215393771 * _thisAdjusted = reinterpret_cast<Enumerator_t215393771 *>(__this + 1);
	Enumerator_VerifyState_m3589629877(_thisAdjusted, method);
}
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.Vector2>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m183044627_gshared (Enumerator_t215393771 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		Enumerator_VerifyState_m3589629877((Enumerator_t215393771 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		int32_t L_1 = (int32_t)__this->get_next_1();
		List_1_t2432482469 * L_2 = (List_1_t2432482469 *)__this->get_l_0();
		NullCheck(L_2);
		int32_t L_3 = (int32_t)L_2->get__size_2();
		if ((((int32_t)L_1) >= ((int32_t)L_3)))
		{
			goto IL_0053;
		}
	}
	{
		List_1_t2432482469 * L_4 = (List_1_t2432482469 *)__this->get_l_0();
		NullCheck(L_4);
		Vector2U5BU5D_t3916253691* L_5 = (Vector2U5BU5D_t3916253691*)L_4->get__items_1();
		int32_t L_6 = (int32_t)__this->get_next_1();
		int32_t L_7 = (int32_t)L_6;
		V_0 = (int32_t)L_7;
		__this->set_next_1(((int32_t)((int32_t)L_7+(int32_t)1)));
		int32_t L_8 = V_0;
		NullCheck(L_5);
		int32_t L_9 = L_8;
		Vector2_t3577333262  L_10 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		__this->set_current_3(L_10);
		return (bool)1;
	}

IL_0053:
	{
		__this->set_next_1((-1));
		return (bool)0;
	}
}
extern "C"  bool Enumerator_MoveNext_m183044627_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Enumerator_t215393771 * _thisAdjusted = reinterpret_cast<Enumerator_t215393771 *>(__this + 1);
	return Enumerator_MoveNext_m183044627(_thisAdjusted, method);
}
// T System.Collections.Generic.List`1/Enumerator<UnityEngine.Vector2>::get_Current()
extern "C"  Vector2_t3577333262  Enumerator_get_Current_m2292278728_gshared (Enumerator_t215393771 * __this, const RuntimeMethod* method)
{
	{
		Vector2_t3577333262  L_0 = (Vector2_t3577333262 )__this->get_current_3();
		return L_0;
	}
}
extern "C"  Vector2_t3577333262  Enumerator_get_Current_m2292278728_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Enumerator_t215393771 * _thisAdjusted = reinterpret_cast<Enumerator_t215393771 *>(__this + 1);
	return Enumerator_get_Current_m2292278728(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Vector3>::.ctor(System.Collections.Generic.List`1<T>)
extern "C"  void Enumerator__ctor_m904992548_gshared (Enumerator_t3836558239 * __this, List_1_t1758679641 * ___l0, const RuntimeMethod* method)
{
	{
		List_1_t1758679641 * L_0 = ___l0;
		__this->set_l_0(L_0);
		List_1_t1758679641 * L_1 = ___l0;
		NullCheck(L_1);
		int32_t L_2 = (int32_t)L_1->get__version_3();
		__this->set_ver_2(L_2);
		return;
	}
}
extern "C"  void Enumerator__ctor_m904992548_AdjustorThunk (RuntimeObject * __this, List_1_t1758679641 * ___l0, const RuntimeMethod* method)
{
	Enumerator_t3836558239 * _thisAdjusted = reinterpret_cast<Enumerator_t3836558239 *>(__this + 1);
	Enumerator__ctor_m904992548(_thisAdjusted, ___l0, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Vector3>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m3019879980_gshared (Enumerator_t3836558239 * __this, const RuntimeMethod* method)
{
	{
		Enumerator_VerifyState_m2353471952((Enumerator_t3836558239 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		__this->set_next_1(0);
		return;
	}
}
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m3019879980_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Enumerator_t3836558239 * _thisAdjusted = reinterpret_cast<Enumerator_t3836558239 *>(__this + 1);
	Enumerator_System_Collections_IEnumerator_Reset_m3019879980(_thisAdjusted, method);
}
// System.Object System.Collections.Generic.List`1/Enumerator<UnityEngine.Vector3>::System.Collections.IEnumerator.get_Current()
extern "C"  RuntimeObject * Enumerator_System_Collections_IEnumerator_get_Current_m1440485986_gshared (Enumerator_t3836558239 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_System_Collections_IEnumerator_get_Current_m1440485986_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Enumerator_VerifyState_m2353471952((Enumerator_t3836558239 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) > ((int32_t)0)))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t2321794232 * L_1 = (InvalidOperationException_t2321794232 *)il2cpp_codegen_object_new(InvalidOperationException_t2321794232_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m3753870925(L_1, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		Vector3_t2903530434  L_2 = (Vector3_t2903530434 )__this->get_current_3();
		Vector3_t2903530434  L_3 = L_2;
		RuntimeObject * L_4 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_3);
		return L_4;
	}
}
extern "C"  RuntimeObject * Enumerator_System_Collections_IEnumerator_get_Current_m1440485986_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Enumerator_t3836558239 * _thisAdjusted = reinterpret_cast<Enumerator_t3836558239 *>(__this + 1);
	return Enumerator_System_Collections_IEnumerator_get_Current_m1440485986(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Vector3>::Dispose()
extern "C"  void Enumerator_Dispose_m523694157_gshared (Enumerator_t3836558239 * __this, const RuntimeMethod* method)
{
	{
		__this->set_l_0((List_1_t1758679641 *)NULL);
		return;
	}
}
extern "C"  void Enumerator_Dispose_m523694157_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Enumerator_t3836558239 * _thisAdjusted = reinterpret_cast<Enumerator_t3836558239 *>(__this + 1);
	Enumerator_Dispose_m523694157(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Vector3>::VerifyState()
extern "C"  void Enumerator_VerifyState_m2353471952_gshared (Enumerator_t3836558239 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_VerifyState_m2353471952_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_t1758679641 * L_0 = (List_1_t1758679641 *)__this->get_l_0();
		if (L_0)
		{
			goto IL_0026;
		}
	}
	{
		Enumerator_t3836558239  L_1 = (*(Enumerator_t3836558239 *)__this);
		RuntimeObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_1);
		NullCheck((RuntimeObject *)L_2);
		Type_t * L_3 = Object_GetType_m3774351292((RuntimeObject *)L_2, /*hidden argument*/NULL);
		NullCheck((Type_t *)L_3);
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(18 /* System.String System.Type::get_FullName() */, (Type_t *)L_3);
		ObjectDisposedException_t2553224159 * L_5 = (ObjectDisposedException_t2553224159 *)il2cpp_codegen_object_new(ObjectDisposedException_t2553224159_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m4150714796(L_5, (String_t*)L_4, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0026:
	{
		int32_t L_6 = (int32_t)__this->get_ver_2();
		List_1_t1758679641 * L_7 = (List_1_t1758679641 *)__this->get_l_0();
		NullCheck(L_7);
		int32_t L_8 = (int32_t)L_7->get__version_3();
		if ((((int32_t)L_6) == ((int32_t)L_8)))
		{
			goto IL_0047;
		}
	}
	{
		InvalidOperationException_t2321794232 * L_9 = (InvalidOperationException_t2321794232 *)il2cpp_codegen_object_new(InvalidOperationException_t2321794232_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m4101245683(L_9, (String_t*)_stringLiteral556965375, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_9);
	}

IL_0047:
	{
		return;
	}
}
extern "C"  void Enumerator_VerifyState_m2353471952_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Enumerator_t3836558239 * _thisAdjusted = reinterpret_cast<Enumerator_t3836558239 *>(__this + 1);
	Enumerator_VerifyState_m2353471952(_thisAdjusted, method);
}
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.Vector3>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m2371540642_gshared (Enumerator_t3836558239 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		Enumerator_VerifyState_m2353471952((Enumerator_t3836558239 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		int32_t L_1 = (int32_t)__this->get_next_1();
		List_1_t1758679641 * L_2 = (List_1_t1758679641 *)__this->get_l_0();
		NullCheck(L_2);
		int32_t L_3 = (int32_t)L_2->get__size_2();
		if ((((int32_t)L_1) >= ((int32_t)L_3)))
		{
			goto IL_0053;
		}
	}
	{
		List_1_t1758679641 * L_4 = (List_1_t1758679641 *)__this->get_l_0();
		NullCheck(L_4);
		Vector3U5BU5D_t981680887* L_5 = (Vector3U5BU5D_t981680887*)L_4->get__items_1();
		int32_t L_6 = (int32_t)__this->get_next_1();
		int32_t L_7 = (int32_t)L_6;
		V_0 = (int32_t)L_7;
		__this->set_next_1(((int32_t)((int32_t)L_7+(int32_t)1)));
		int32_t L_8 = V_0;
		NullCheck(L_5);
		int32_t L_9 = L_8;
		Vector3_t2903530434  L_10 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		__this->set_current_3(L_10);
		return (bool)1;
	}

IL_0053:
	{
		__this->set_next_1((-1));
		return (bool)0;
	}
}
extern "C"  bool Enumerator_MoveNext_m2371540642_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Enumerator_t3836558239 * _thisAdjusted = reinterpret_cast<Enumerator_t3836558239 *>(__this + 1);
	return Enumerator_MoveNext_m2371540642(_thisAdjusted, method);
}
// T System.Collections.Generic.List`1/Enumerator<UnityEngine.Vector3>::get_Current()
extern "C"  Vector3_t2903530434  Enumerator_get_Current_m1590935863_gshared (Enumerator_t3836558239 * __this, const RuntimeMethod* method)
{
	{
		Vector3_t2903530434  L_0 = (Vector3_t2903530434 )__this->get_current_3();
		return L_0;
	}
}
extern "C"  Vector3_t2903530434  Enumerator_get_Current_m1590935863_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Enumerator_t3836558239 * _thisAdjusted = reinterpret_cast<Enumerator_t3836558239 *>(__this + 1);
	return Enumerator_get_Current_m1590935863(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Vector4>::.ctor(System.Collections.Generic.List`1<T>)
extern "C"  void Enumerator__ctor_m2410038664_gshared (Enumerator_t3747700150 * __this, List_1_t1669821552 * ___l0, const RuntimeMethod* method)
{
	{
		List_1_t1669821552 * L_0 = ___l0;
		__this->set_l_0(L_0);
		List_1_t1669821552 * L_1 = ___l0;
		NullCheck(L_1);
		int32_t L_2 = (int32_t)L_1->get__version_3();
		__this->set_ver_2(L_2);
		return;
	}
}
extern "C"  void Enumerator__ctor_m2410038664_AdjustorThunk (RuntimeObject * __this, List_1_t1669821552 * ___l0, const RuntimeMethod* method)
{
	Enumerator_t3747700150 * _thisAdjusted = reinterpret_cast<Enumerator_t3747700150 *>(__this + 1);
	Enumerator__ctor_m2410038664(_thisAdjusted, ___l0, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Vector4>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m888757397_gshared (Enumerator_t3747700150 * __this, const RuntimeMethod* method)
{
	{
		Enumerator_VerifyState_m4257143980((Enumerator_t3747700150 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		__this->set_next_1(0);
		return;
	}
}
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m888757397_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Enumerator_t3747700150 * _thisAdjusted = reinterpret_cast<Enumerator_t3747700150 *>(__this + 1);
	Enumerator_System_Collections_IEnumerator_Reset_m888757397(_thisAdjusted, method);
}
// System.Object System.Collections.Generic.List`1/Enumerator<UnityEngine.Vector4>::System.Collections.IEnumerator.get_Current()
extern "C"  RuntimeObject * Enumerator_System_Collections_IEnumerator_get_Current_m2740647926_gshared (Enumerator_t3747700150 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_System_Collections_IEnumerator_get_Current_m2740647926_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Enumerator_VerifyState_m4257143980((Enumerator_t3747700150 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) > ((int32_t)0)))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t2321794232 * L_1 = (InvalidOperationException_t2321794232 *)il2cpp_codegen_object_new(InvalidOperationException_t2321794232_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m3753870925(L_1, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		Vector4_t2814672345  L_2 = (Vector4_t2814672345 )__this->get_current_3();
		Vector4_t2814672345  L_3 = L_2;
		RuntimeObject * L_4 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_3);
		return L_4;
	}
}
extern "C"  RuntimeObject * Enumerator_System_Collections_IEnumerator_get_Current_m2740647926_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Enumerator_t3747700150 * _thisAdjusted = reinterpret_cast<Enumerator_t3747700150 *>(__this + 1);
	return Enumerator_System_Collections_IEnumerator_get_Current_m2740647926(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Vector4>::Dispose()
extern "C"  void Enumerator_Dispose_m173057505_gshared (Enumerator_t3747700150 * __this, const RuntimeMethod* method)
{
	{
		__this->set_l_0((List_1_t1669821552 *)NULL);
		return;
	}
}
extern "C"  void Enumerator_Dispose_m173057505_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Enumerator_t3747700150 * _thisAdjusted = reinterpret_cast<Enumerator_t3747700150 *>(__this + 1);
	Enumerator_Dispose_m173057505(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Vector4>::VerifyState()
extern "C"  void Enumerator_VerifyState_m4257143980_gshared (Enumerator_t3747700150 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_VerifyState_m4257143980_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_t1669821552 * L_0 = (List_1_t1669821552 *)__this->get_l_0();
		if (L_0)
		{
			goto IL_0026;
		}
	}
	{
		Enumerator_t3747700150  L_1 = (*(Enumerator_t3747700150 *)__this);
		RuntimeObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_1);
		NullCheck((RuntimeObject *)L_2);
		Type_t * L_3 = Object_GetType_m3774351292((RuntimeObject *)L_2, /*hidden argument*/NULL);
		NullCheck((Type_t *)L_3);
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(18 /* System.String System.Type::get_FullName() */, (Type_t *)L_3);
		ObjectDisposedException_t2553224159 * L_5 = (ObjectDisposedException_t2553224159 *)il2cpp_codegen_object_new(ObjectDisposedException_t2553224159_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m4150714796(L_5, (String_t*)L_4, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0026:
	{
		int32_t L_6 = (int32_t)__this->get_ver_2();
		List_1_t1669821552 * L_7 = (List_1_t1669821552 *)__this->get_l_0();
		NullCheck(L_7);
		int32_t L_8 = (int32_t)L_7->get__version_3();
		if ((((int32_t)L_6) == ((int32_t)L_8)))
		{
			goto IL_0047;
		}
	}
	{
		InvalidOperationException_t2321794232 * L_9 = (InvalidOperationException_t2321794232 *)il2cpp_codegen_object_new(InvalidOperationException_t2321794232_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m4101245683(L_9, (String_t*)_stringLiteral556965375, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_9);
	}

IL_0047:
	{
		return;
	}
}
extern "C"  void Enumerator_VerifyState_m4257143980_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Enumerator_t3747700150 * _thisAdjusted = reinterpret_cast<Enumerator_t3747700150 *>(__this + 1);
	Enumerator_VerifyState_m4257143980(_thisAdjusted, method);
}
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.Vector4>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m675284825_gshared (Enumerator_t3747700150 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		Enumerator_VerifyState_m4257143980((Enumerator_t3747700150 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		int32_t L_1 = (int32_t)__this->get_next_1();
		List_1_t1669821552 * L_2 = (List_1_t1669821552 *)__this->get_l_0();
		NullCheck(L_2);
		int32_t L_3 = (int32_t)L_2->get__size_2();
		if ((((int32_t)L_1) >= ((int32_t)L_3)))
		{
			goto IL_0053;
		}
	}
	{
		List_1_t1669821552 * L_4 = (List_1_t1669821552 *)__this->get_l_0();
		NullCheck(L_4);
		Vector4U5BU5D_t2124741284* L_5 = (Vector4U5BU5D_t2124741284*)L_4->get__items_1();
		int32_t L_6 = (int32_t)__this->get_next_1();
		int32_t L_7 = (int32_t)L_6;
		V_0 = (int32_t)L_7;
		__this->set_next_1(((int32_t)((int32_t)L_7+(int32_t)1)));
		int32_t L_8 = V_0;
		NullCheck(L_5);
		int32_t L_9 = L_8;
		Vector4_t2814672345  L_10 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		__this->set_current_3(L_10);
		return (bool)1;
	}

IL_0053:
	{
		__this->set_next_1((-1));
		return (bool)0;
	}
}
extern "C"  bool Enumerator_MoveNext_m675284825_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Enumerator_t3747700150 * _thisAdjusted = reinterpret_cast<Enumerator_t3747700150 *>(__this + 1);
	return Enumerator_MoveNext_m675284825(_thisAdjusted, method);
}
// T System.Collections.Generic.List`1/Enumerator<UnityEngine.Vector4>::get_Current()
extern "C"  Vector4_t2814672345  Enumerator_get_Current_m2462407644_gshared (Enumerator_t3747700150 * __this, const RuntimeMethod* method)
{
	{
		Vector4_t2814672345  L_0 = (Vector4_t2814672345 )__this->get_current_3();
		return L_0;
	}
}
extern "C"  Vector4_t2814672345  Enumerator_get_Current_m2462407644_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Enumerator_t3747700150 * _thisAdjusted = reinterpret_cast<Enumerator_t3747700150 *>(__this + 1);
	return Enumerator_get_Current_m2462407644(_thisAdjusted, method);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif

﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "object-internals.h"

// Vuforia.IHoloLensApiAbstraction
struct IHoloLensApiAbstraction_t1176050841;
// System.String
struct String_t;
// UnityEngine.Object
struct Object_t1502412432;
// System.Collections.Generic.List`1<UnityEngine.Analytics.TrackableProperty/FieldWithTarget>
struct List_1_t2989823407;
// Vuforia.VuforiaAbstractBehaviour
struct VuforiaAbstractBehaviour_t153010168;
// UnityEngine.Camera
struct Camera_t3175186167;
// Vuforia.IVuforiaWrapper
struct IVuforiaWrapper_t3917309055;
// Vuforia.WebCamImpl
struct WebCamImpl_t90372774;
// System.Void
struct Void_t2217553113;
// System.Char[]
struct CharU5BU5D_t45629911;
// System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.TrackableBehaviour>
struct Dictionary_2_t3615086520;
// System.Collections.Generic.List`1<System.Int32>
struct List_1_t3093093796;
// System.Collections.Generic.List`1<Vuforia.TrackableBehaviour>
struct List_1_t1274228563;
// Vuforia.WordManagerImpl
struct WordManagerImpl_t2273252301;
// Vuforia.VuMarkManagerImpl
struct VuMarkManagerImpl_t2293130653;
// Vuforia.DeviceTrackingManager
struct DeviceTrackingManager_t2027984062;
// UnityEngine.GameObject
struct GameObject_t1811656094;
// Vuforia.IExtendedTrackingManager
struct IExtendedTrackingManager_t1537745431;
// Vuforia.ObjectTracker
struct ObjectTracker_t4023638979;
// Vuforia.TextTracker
struct TextTracker_t3669255194;
// Vuforia.SmartTerrainTracker
struct SmartTerrainTracker_t1505679812;
// Vuforia.DeviceTracker
struct DeviceTracker_t54803992;
// Vuforia.StateManager
struct StateManager_t231326187;
// System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>
struct List_1_t3363810910;
// System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.ImageTarget>
struct Dictionary_2_t1389033120;
// UnityEngine.Shader
struct Shader_t3778723916;
// UnityEngine.Texture
struct Texture_t85561421;
// UnityEngine.Camera[]
struct CameraU5BU5D_t1519774542;
// System.Int32[]
struct Int32U5BU5D_t2324750880;
// Vuforia.WebCamTexAdaptor
struct WebCamTexAdaptor_t433428952;
// Vuforia.TextureRenderer
struct TextureRenderer_t2855676147;
// UnityEngine.Texture2D
struct Texture2D_t415585320;
// System.Collections.Generic.Dictionary`2<System.String,Vuforia.WebCamProfile/ProfileData>
struct Dictionary_2_t1910768660;
// Vuforia.TrackableBehaviour
struct TrackableBehaviour_t2419079356;
// System.Collections.Generic.List`1<Vuforia.ITrackerEventHandler>
struct List_1_t1947635164;
// System.Collections.Generic.List`1<Vuforia.IVideoBackgroundEventHandler>
struct List_1_t485517045;
// System.Action
struct Action_t3619184611;
// System.Action`1<System.Boolean>
struct Action_1_t2936672411;
// Vuforia.ICameraConfiguration
struct ICameraConfiguration_t3187182883;
// Vuforia.DigitalEyewearARController
struct DigitalEyewearARController_t1877756253;
// Vuforia.VideoBackgroundManager
struct VideoBackgroundManager_t2068552549;
// UnityEngine.Material
struct Material_t1079520667;
// System.Collections.Generic.List`1<System.Type>
struct List_1_t3593106846;
// Vuforia.ImageTarget
struct ImageTarget_t193025956;
// Vuforia.DataSetImpl
struct DataSetImpl_t223764247;
// System.Collections.Generic.List`1<Vuforia.ISmartTerrainEventHandler>
struct List_1_t3446838688;
// System.Action`1<Vuforia.SmartTerrainInitializationInfo>
struct Action_1_t1488611326;
// System.Action`1<Vuforia.Prop>
struct Action_1_t1560204979;
// System.Action`1<Vuforia.Surface>
struct Action_1_t1020068010;
// Vuforia.Reconstruction
struct Reconstruction_t3574071110;
// System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.Surface>
struct Dictionary_2_t2230247832;
// System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.SurfaceAbstractBehaviour>
struct Dictionary_2_t1468548340;
// System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.Prop>
struct Dictionary_2_t2770384801;
// System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.PropAbstractBehaviour>
struct Dictionary_2_t3844889330;
// Vuforia.SurfaceAbstractBehaviour
struct SurfaceAbstractBehaviour_t272541176;
// Vuforia.VuforiaARController
struct VuforiaARController_t1328503142;
// Vuforia.BackgroundPlaneAbstractBehaviour
struct BackgroundPlaneAbstractBehaviour_t4259835756;
// System.Collections.Generic.HashSet`1<UnityEngine.MeshRenderer>
struct HashSet_1_t1919463348;
// System.Collections.Generic.List`1<Vuforia.IUserDefinedTargetEventHandler>
struct List_1_t4290827367;
// System.Collections.Generic.Dictionary`2<System.String,System.Object>
struct Dictionary_2_t3191847521;
// UnityEngine.Analytics.TrackableProperty
struct TrackableProperty_t10670078;
// System.Collections.Generic.List`1<Vuforia.ITextRecoEventHandler>
struct List_1_t4098171074;
// System.Collections.Generic.List`1<Vuforia.IVirtualButtonEventHandler>
struct List_1_t1800411770;
// Vuforia.VirtualButton
struct VirtualButton_t3666937711;
// Vuforia.Trackable
struct Trackable_t1140696582;
// System.Collections.Generic.List`1<Vuforia.ITrackableEventHandler>
struct List_1_t3589236026;
// Vuforia.ReconstructionFromTargetAbstractBehaviour
struct ReconstructionFromTargetAbstractBehaviour_t2676804737;
// Vuforia.SmartTerrainTrackable
struct SmartTerrainTrackable_t3286114719;
// UnityEngine.MeshFilter
struct MeshFilter_t137687116;
// UnityEngine.MeshCollider
struct MeshCollider_t4172739389;
// Vuforia.Word
struct Word_t200975816;
// Vuforia.MultiTarget
struct MultiTarget_t114888232;
// Vuforia.Prop
struct Prop_t1574377637;
// UnityEngine.BoxCollider
struct BoxCollider_t1861275623;
// System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VirtualButtonAbstractBehaviour>
struct Dictionary_2_t3161827512;




#ifndef U3CMODULEU3E_T160650890_H
#define U3CMODULEU3E_T160650890_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t160650890 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T160650890_H
#ifndef U3CMODULEU3E_T160650889_H
#define U3CMODULEU3E_T160650889_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t160650889 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T160650889_H
#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef EYEWEARUSERCALIBRATOR_T173744680_H
#define EYEWEARUSERCALIBRATOR_T173744680_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.EyewearUserCalibrator
struct  EyewearUserCalibrator_t173744680  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EYEWEARUSERCALIBRATOR_T173744680_H
#ifndef TRACKERMANAGER_T814515577_H
#define TRACKERMANAGER_T814515577_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.TrackerManager
struct  TrackerManager_t814515577  : public RuntimeObject
{
public:

public:
};

struct TrackerManager_t814515577_StaticFields
{
public:
	// Vuforia.TrackerManager Vuforia.TrackerManager::mInstance
	TrackerManager_t814515577 * ___mInstance_0;

public:
	inline static int32_t get_offset_of_mInstance_0() { return static_cast<int32_t>(offsetof(TrackerManager_t814515577_StaticFields, ___mInstance_0)); }
	inline TrackerManager_t814515577 * get_mInstance_0() const { return ___mInstance_0; }
	inline TrackerManager_t814515577 ** get_address_of_mInstance_0() { return &___mInstance_0; }
	inline void set_mInstance_0(TrackerManager_t814515577 * value)
	{
		___mInstance_0 = value;
		Il2CppCodeGenWriteBarrier((&___mInstance_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRACKERMANAGER_T814515577_H
#ifndef VALUETYPE_T1306072465_H
#define VALUETYPE_T1306072465_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t1306072465  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t1306072465_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t1306072465_marshaled_com
{
};
#endif // VALUETYPE_T1306072465_H
#ifndef VUFORIARENDERER_T969713742_H
#define VUFORIARENDERER_T969713742_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.VuforiaRenderer
struct  VuforiaRenderer_t969713742  : public RuntimeObject
{
public:

public:
};

struct VuforiaRenderer_t969713742_StaticFields
{
public:
	// Vuforia.VuforiaRenderer Vuforia.VuforiaRenderer::sInstance
	VuforiaRenderer_t969713742 * ___sInstance_0;

public:
	inline static int32_t get_offset_of_sInstance_0() { return static_cast<int32_t>(offsetof(VuforiaRenderer_t969713742_StaticFields, ___sInstance_0)); }
	inline VuforiaRenderer_t969713742 * get_sInstance_0() const { return ___sInstance_0; }
	inline VuforiaRenderer_t969713742 ** get_address_of_sInstance_0() { return &___sInstance_0; }
	inline void set_sInstance_0(VuforiaRenderer_t969713742 * value)
	{
		___sInstance_0 = value;
		Il2CppCodeGenWriteBarrier((&___sInstance_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VUFORIARENDERER_T969713742_H
#ifndef VUFORIAUNITY_T3212967305_H
#define VUFORIAUNITY_T3212967305_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.VuforiaUnity
struct  VuforiaUnity_t3212967305  : public RuntimeObject
{
public:

public:
};

struct VuforiaUnity_t3212967305_StaticFields
{
public:
	// Vuforia.IHoloLensApiAbstraction Vuforia.VuforiaUnity::mHoloLensApiAbstraction
	RuntimeObject* ___mHoloLensApiAbstraction_0;

public:
	inline static int32_t get_offset_of_mHoloLensApiAbstraction_0() { return static_cast<int32_t>(offsetof(VuforiaUnity_t3212967305_StaticFields, ___mHoloLensApiAbstraction_0)); }
	inline RuntimeObject* get_mHoloLensApiAbstraction_0() const { return ___mHoloLensApiAbstraction_0; }
	inline RuntimeObject** get_address_of_mHoloLensApiAbstraction_0() { return &___mHoloLensApiAbstraction_0; }
	inline void set_mHoloLensApiAbstraction_0(RuntimeObject* value)
	{
		___mHoloLensApiAbstraction_0 = value;
		Il2CppCodeGenWriteBarrier((&___mHoloLensApiAbstraction_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VUFORIAUNITY_T3212967305_H
#ifndef FIELDWITHTARGET_T4134674200_H
#define FIELDWITHTARGET_T4134674200_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.TrackableProperty/FieldWithTarget
struct  FieldWithTarget_t4134674200  : public RuntimeObject
{
public:
	// System.String UnityEngine.Analytics.TrackableProperty/FieldWithTarget::m_ParamName
	String_t* ___m_ParamName_0;
	// UnityEngine.Object UnityEngine.Analytics.TrackableProperty/FieldWithTarget::m_Target
	Object_t1502412432 * ___m_Target_1;
	// System.String UnityEngine.Analytics.TrackableProperty/FieldWithTarget::m_FieldPath
	String_t* ___m_FieldPath_2;
	// System.String UnityEngine.Analytics.TrackableProperty/FieldWithTarget::m_TypeString
	String_t* ___m_TypeString_3;
	// System.Boolean UnityEngine.Analytics.TrackableProperty/FieldWithTarget::m_DoStatic
	bool ___m_DoStatic_4;
	// System.String UnityEngine.Analytics.TrackableProperty/FieldWithTarget::m_StaticString
	String_t* ___m_StaticString_5;

public:
	inline static int32_t get_offset_of_m_ParamName_0() { return static_cast<int32_t>(offsetof(FieldWithTarget_t4134674200, ___m_ParamName_0)); }
	inline String_t* get_m_ParamName_0() const { return ___m_ParamName_0; }
	inline String_t** get_address_of_m_ParamName_0() { return &___m_ParamName_0; }
	inline void set_m_ParamName_0(String_t* value)
	{
		___m_ParamName_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_ParamName_0), value);
	}

	inline static int32_t get_offset_of_m_Target_1() { return static_cast<int32_t>(offsetof(FieldWithTarget_t4134674200, ___m_Target_1)); }
	inline Object_t1502412432 * get_m_Target_1() const { return ___m_Target_1; }
	inline Object_t1502412432 ** get_address_of_m_Target_1() { return &___m_Target_1; }
	inline void set_m_Target_1(Object_t1502412432 * value)
	{
		___m_Target_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_Target_1), value);
	}

	inline static int32_t get_offset_of_m_FieldPath_2() { return static_cast<int32_t>(offsetof(FieldWithTarget_t4134674200, ___m_FieldPath_2)); }
	inline String_t* get_m_FieldPath_2() const { return ___m_FieldPath_2; }
	inline String_t** get_address_of_m_FieldPath_2() { return &___m_FieldPath_2; }
	inline void set_m_FieldPath_2(String_t* value)
	{
		___m_FieldPath_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_FieldPath_2), value);
	}

	inline static int32_t get_offset_of_m_TypeString_3() { return static_cast<int32_t>(offsetof(FieldWithTarget_t4134674200, ___m_TypeString_3)); }
	inline String_t* get_m_TypeString_3() const { return ___m_TypeString_3; }
	inline String_t** get_address_of_m_TypeString_3() { return &___m_TypeString_3; }
	inline void set_m_TypeString_3(String_t* value)
	{
		___m_TypeString_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_TypeString_3), value);
	}

	inline static int32_t get_offset_of_m_DoStatic_4() { return static_cast<int32_t>(offsetof(FieldWithTarget_t4134674200, ___m_DoStatic_4)); }
	inline bool get_m_DoStatic_4() const { return ___m_DoStatic_4; }
	inline bool* get_address_of_m_DoStatic_4() { return &___m_DoStatic_4; }
	inline void set_m_DoStatic_4(bool value)
	{
		___m_DoStatic_4 = value;
	}

	inline static int32_t get_offset_of_m_StaticString_5() { return static_cast<int32_t>(offsetof(FieldWithTarget_t4134674200, ___m_StaticString_5)); }
	inline String_t* get_m_StaticString_5() const { return ___m_StaticString_5; }
	inline String_t** get_address_of_m_StaticString_5() { return &___m_StaticString_5; }
	inline void set_m_StaticString_5(String_t* value)
	{
		___m_StaticString_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_StaticString_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FIELDWITHTARGET_T4134674200_H
#ifndef TRACKABLEPROPERTY_T10670078_H
#define TRACKABLEPROPERTY_T10670078_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.TrackableProperty
struct  TrackableProperty_t10670078  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<UnityEngine.Analytics.TrackableProperty/FieldWithTarget> UnityEngine.Analytics.TrackableProperty::m_Fields
	List_1_t2989823407 * ___m_Fields_1;

public:
	inline static int32_t get_offset_of_m_Fields_1() { return static_cast<int32_t>(offsetof(TrackableProperty_t10670078, ___m_Fields_1)); }
	inline List_1_t2989823407 * get_m_Fields_1() const { return ___m_Fields_1; }
	inline List_1_t2989823407 ** get_address_of_m_Fields_1() { return &___m_Fields_1; }
	inline void set_m_Fields_1(List_1_t2989823407 * value)
	{
		___m_Fields_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_Fields_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRACKABLEPROPERTY_T10670078_H
#ifndef VUFORIANATIVEIOSWRAPPER_T49709812_H
#define VUFORIANATIVEIOSWRAPPER_T49709812_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.VuforiaNativeIosWrapper
struct  VuforiaNativeIosWrapper_t49709812  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VUFORIANATIVEIOSWRAPPER_T49709812_H
#ifndef IMAGE_T1445313791_H
#define IMAGE_T1445313791_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.Image
struct  Image_t1445313791  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IMAGE_T1445313791_H
#ifndef ARCONTROLLER_T1205915989_H
#define ARCONTROLLER_T1205915989_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.ARController
struct  ARController_t1205915989  : public RuntimeObject
{
public:
	// Vuforia.VuforiaAbstractBehaviour Vuforia.ARController::mVuforiaBehaviour
	VuforiaAbstractBehaviour_t153010168 * ___mVuforiaBehaviour_0;

public:
	inline static int32_t get_offset_of_mVuforiaBehaviour_0() { return static_cast<int32_t>(offsetof(ARController_t1205915989, ___mVuforiaBehaviour_0)); }
	inline VuforiaAbstractBehaviour_t153010168 * get_mVuforiaBehaviour_0() const { return ___mVuforiaBehaviour_0; }
	inline VuforiaAbstractBehaviour_t153010168 ** get_address_of_mVuforiaBehaviour_0() { return &___mVuforiaBehaviour_0; }
	inline void set_mVuforiaBehaviour_0(VuforiaAbstractBehaviour_t153010168 * value)
	{
		___mVuforiaBehaviour_0 = value;
		Il2CppCodeGenWriteBarrier((&___mVuforiaBehaviour_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARCONTROLLER_T1205915989_H
#ifndef TARGETFINDER_T2860227752_H
#define TARGETFINDER_T2860227752_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.TargetFinder
struct  TargetFinder_t2860227752  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TARGETFINDER_T2860227752_H
#ifndef EYEWEARCALIBRATIONPROFILEMANAGER_T1615252077_H
#define EYEWEARCALIBRATIONPROFILEMANAGER_T1615252077_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.EyewearCalibrationProfileManager
struct  EyewearCalibrationProfileManager_t1615252077  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EYEWEARCALIBRATIONPROFILEMANAGER_T1615252077_H
#ifndef VUFORIAMANAGER_T745032010_H
#define VUFORIAMANAGER_T745032010_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.VuforiaManager
struct  VuforiaManager_t745032010  : public RuntimeObject
{
public:

public:
};

struct VuforiaManager_t745032010_StaticFields
{
public:
	// Vuforia.VuforiaManager Vuforia.VuforiaManager::sInstance
	VuforiaManager_t745032010 * ___sInstance_0;

public:
	inline static int32_t get_offset_of_sInstance_0() { return static_cast<int32_t>(offsetof(VuforiaManager_t745032010_StaticFields, ___sInstance_0)); }
	inline VuforiaManager_t745032010 * get_sInstance_0() const { return ___sInstance_0; }
	inline VuforiaManager_t745032010 ** get_address_of_sInstance_0() { return &___sInstance_0; }
	inline void set_sInstance_0(VuforiaManager_t745032010 * value)
	{
		___sInstance_0 = value;
		Il2CppCodeGenWriteBarrier((&___sInstance_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VUFORIAMANAGER_T745032010_H
#ifndef TEXTURERENDERER_T2855676147_H
#define TEXTURERENDERER_T2855676147_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.TextureRenderer
struct  TextureRenderer_t2855676147  : public RuntimeObject
{
public:
	// UnityEngine.Camera Vuforia.TextureRenderer::mTextureBufferCamera
	Camera_t3175186167 * ___mTextureBufferCamera_0;
	// System.Int32 Vuforia.TextureRenderer::mTextureWidth
	int32_t ___mTextureWidth_1;
	// System.Int32 Vuforia.TextureRenderer::mTextureHeight
	int32_t ___mTextureHeight_2;

public:
	inline static int32_t get_offset_of_mTextureBufferCamera_0() { return static_cast<int32_t>(offsetof(TextureRenderer_t2855676147, ___mTextureBufferCamera_0)); }
	inline Camera_t3175186167 * get_mTextureBufferCamera_0() const { return ___mTextureBufferCamera_0; }
	inline Camera_t3175186167 ** get_address_of_mTextureBufferCamera_0() { return &___mTextureBufferCamera_0; }
	inline void set_mTextureBufferCamera_0(Camera_t3175186167 * value)
	{
		___mTextureBufferCamera_0 = value;
		Il2CppCodeGenWriteBarrier((&___mTextureBufferCamera_0), value);
	}

	inline static int32_t get_offset_of_mTextureWidth_1() { return static_cast<int32_t>(offsetof(TextureRenderer_t2855676147, ___mTextureWidth_1)); }
	inline int32_t get_mTextureWidth_1() const { return ___mTextureWidth_1; }
	inline int32_t* get_address_of_mTextureWidth_1() { return &___mTextureWidth_1; }
	inline void set_mTextureWidth_1(int32_t value)
	{
		___mTextureWidth_1 = value;
	}

	inline static int32_t get_offset_of_mTextureHeight_2() { return static_cast<int32_t>(offsetof(TextureRenderer_t2855676147, ___mTextureHeight_2)); }
	inline int32_t get_mTextureHeight_2() const { return ___mTextureHeight_2; }
	inline int32_t* get_address_of_mTextureHeight_2() { return &___mTextureHeight_2; }
	inline void set_mTextureHeight_2(int32_t value)
	{
		___mTextureHeight_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTURERENDERER_T2855676147_H
#ifndef WORDLIST_T3717410066_H
#define WORDLIST_T3717410066_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.WordList
struct  WordList_t3717410066  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WORDLIST_T3717410066_H
#ifndef VUFORIANULLWRAPPER_T2276749828_H
#define VUFORIANULLWRAPPER_T2276749828_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.VuforiaNullWrapper
struct  VuforiaNullWrapper_t2276749828  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VUFORIANULLWRAPPER_T2276749828_H
#ifndef VUFORIANATIVEWRAPPER_T4186723445_H
#define VUFORIANATIVEWRAPPER_T4186723445_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.VuforiaNativeWrapper
struct  VuforiaNativeWrapper_t4186723445  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VUFORIANATIVEWRAPPER_T4186723445_H
#ifndef WORDRESULT_T292368650_H
#define WORDRESULT_T292368650_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.WordResult
struct  WordResult_t292368650  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WORDRESULT_T292368650_H
#ifndef WORDMANAGER_T1528772797_H
#define WORDMANAGER_T1528772797_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.WordManager
struct  WordManager_t1528772797  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WORDMANAGER_T1528772797_H
#ifndef STATEMANAGER_T231326187_H
#define STATEMANAGER_T231326187_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.StateManager
struct  StateManager_t231326187  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STATEMANAGER_T231326187_H
#ifndef TRACKABLEIMPL_T3450720819_H
#define TRACKABLEIMPL_T3450720819_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.TrackableImpl
struct  TrackableImpl_t3450720819  : public RuntimeObject
{
public:
	// System.String Vuforia.TrackableImpl::<Name>k__BackingField
	String_t* ___U3CNameU3Ek__BackingField_0;
	// System.Int32 Vuforia.TrackableImpl::<ID>k__BackingField
	int32_t ___U3CIDU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CNameU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(TrackableImpl_t3450720819, ___U3CNameU3Ek__BackingField_0)); }
	inline String_t* get_U3CNameU3Ek__BackingField_0() const { return ___U3CNameU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CNameU3Ek__BackingField_0() { return &___U3CNameU3Ek__BackingField_0; }
	inline void set_U3CNameU3Ek__BackingField_0(String_t* value)
	{
		___U3CNameU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CNameU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CIDU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(TrackableImpl_t3450720819, ___U3CIDU3Ek__BackingField_1)); }
	inline int32_t get_U3CIDU3Ek__BackingField_1() const { return ___U3CIDU3Ek__BackingField_1; }
	inline int32_t* get_address_of_U3CIDU3Ek__BackingField_1() { return &___U3CIDU3Ek__BackingField_1; }
	inline void set_U3CIDU3Ek__BackingField_1(int32_t value)
	{
		___U3CIDU3Ek__BackingField_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRACKABLEIMPL_T3450720819_H
#ifndef VUFORIAWRAPPER_T3746480205_H
#define VUFORIAWRAPPER_T3746480205_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.VuforiaWrapper
struct  VuforiaWrapper_t3746480205  : public RuntimeObject
{
public:

public:
};

struct VuforiaWrapper_t3746480205_StaticFields
{
public:
	// Vuforia.IVuforiaWrapper Vuforia.VuforiaWrapper::sWrapper
	RuntimeObject* ___sWrapper_0;
	// Vuforia.IVuforiaWrapper Vuforia.VuforiaWrapper::sCamIndependentWrapper
	RuntimeObject* ___sCamIndependentWrapper_1;

public:
	inline static int32_t get_offset_of_sWrapper_0() { return static_cast<int32_t>(offsetof(VuforiaWrapper_t3746480205_StaticFields, ___sWrapper_0)); }
	inline RuntimeObject* get_sWrapper_0() const { return ___sWrapper_0; }
	inline RuntimeObject** get_address_of_sWrapper_0() { return &___sWrapper_0; }
	inline void set_sWrapper_0(RuntimeObject* value)
	{
		___sWrapper_0 = value;
		Il2CppCodeGenWriteBarrier((&___sWrapper_0), value);
	}

	inline static int32_t get_offset_of_sCamIndependentWrapper_1() { return static_cast<int32_t>(offsetof(VuforiaWrapper_t3746480205_StaticFields, ___sCamIndependentWrapper_1)); }
	inline RuntimeObject* get_sCamIndependentWrapper_1() const { return ___sCamIndependentWrapper_1; }
	inline RuntimeObject** get_address_of_sCamIndependentWrapper_1() { return &___sCamIndependentWrapper_1; }
	inline void set_sCamIndependentWrapper_1(RuntimeObject* value)
	{
		___sCamIndependentWrapper_1 = value;
		Il2CppCodeGenWriteBarrier((&___sCamIndependentWrapper_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VUFORIAWRAPPER_T3746480205_H
#ifndef TRACKABLESOURCE_T4159007682_H
#define TRACKABLESOURCE_T4159007682_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.TrackableSource
struct  TrackableSource_t4159007682  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRACKABLESOURCE_T4159007682_H
#ifndef TRACKER_T1731515739_H
#define TRACKER_T1731515739_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.Tracker
struct  Tracker_t1731515739  : public RuntimeObject
{
public:
	// System.Boolean Vuforia.Tracker::<IsActive>k__BackingField
	bool ___U3CIsActiveU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CIsActiveU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(Tracker_t1731515739, ___U3CIsActiveU3Ek__BackingField_0)); }
	inline bool get_U3CIsActiveU3Ek__BackingField_0() const { return ___U3CIsActiveU3Ek__BackingField_0; }
	inline bool* get_address_of_U3CIsActiveU3Ek__BackingField_0() { return &___U3CIsActiveU3Ek__BackingField_0; }
	inline void set_U3CIsActiveU3Ek__BackingField_0(bool value)
	{
		___U3CIsActiveU3Ek__BackingField_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRACKER_T1731515739_H
#ifndef TRACKABLEIDPAIR_T1331427386_H
#define TRACKABLEIDPAIR_T1331427386_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.VuforiaManager/TrackableIdPair
struct  TrackableIdPair_t1331427386 
{
public:
	// System.Int32 Vuforia.VuforiaManager/TrackableIdPair::TrackableId
	int32_t ___TrackableId_0;
	// System.Int32 Vuforia.VuforiaManager/TrackableIdPair::ResultId
	int32_t ___ResultId_1;

public:
	inline static int32_t get_offset_of_TrackableId_0() { return static_cast<int32_t>(offsetof(TrackableIdPair_t1331427386, ___TrackableId_0)); }
	inline int32_t get_TrackableId_0() const { return ___TrackableId_0; }
	inline int32_t* get_address_of_TrackableId_0() { return &___TrackableId_0; }
	inline void set_TrackableId_0(int32_t value)
	{
		___TrackableId_0 = value;
	}

	inline static int32_t get_offset_of_ResultId_1() { return static_cast<int32_t>(offsetof(TrackableIdPair_t1331427386, ___ResultId_1)); }
	inline int32_t get_ResultId_1() const { return ___ResultId_1; }
	inline int32_t* get_address_of_ResultId_1() { return &___ResultId_1; }
	inline void set_ResultId_1(int32_t value)
	{
		___ResultId_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRACKABLEIDPAIR_T1331427386_H
#ifndef VEC2I_T895597728_H
#define VEC2I_T895597728_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.VuforiaRenderer/Vec2I
#pragma pack(push, tp, 1)
struct  Vec2I_t895597728 
{
public:
	// System.Int32 Vuforia.VuforiaRenderer/Vec2I::x
	int32_t ___x_0;
	// System.Int32 Vuforia.VuforiaRenderer/Vec2I::y
	int32_t ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vec2I_t895597728, ___x_0)); }
	inline int32_t get_x_0() const { return ___x_0; }
	inline int32_t* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(int32_t value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vec2I_t895597728, ___y_1)); }
	inline int32_t get_y_1() const { return ___y_1; }
	inline int32_t* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(int32_t value)
	{
		___y_1 = value;
	}
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VEC2I_T895597728_H
#ifndef SIMPLETARGETDATA_T2990037841_H
#define SIMPLETARGETDATA_T2990037841_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.SimpleTargetData
#pragma pack(push, tp, 1)
struct  SimpleTargetData_t2990037841 
{
public:
	// System.Int32 Vuforia.SimpleTargetData::id
	int32_t ___id_0;
	// System.Int32 Vuforia.SimpleTargetData::unused
	int32_t ___unused_1;

public:
	inline static int32_t get_offset_of_id_0() { return static_cast<int32_t>(offsetof(SimpleTargetData_t2990037841, ___id_0)); }
	inline int32_t get_id_0() const { return ___id_0; }
	inline int32_t* get_address_of_id_0() { return &___id_0; }
	inline void set_id_0(int32_t value)
	{
		___id_0 = value;
	}

	inline static int32_t get_offset_of_unused_1() { return static_cast<int32_t>(offsetof(SimpleTargetData_t2990037841, ___unused_1)); }
	inline int32_t get_unused_1() const { return ___unused_1; }
	inline int32_t* get_address_of_unused_1() { return &___unused_1; }
	inline void set_unused_1(int32_t value)
	{
		___unused_1 = value;
	}
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SIMPLETARGETDATA_T2990037841_H
#ifndef TEXTTRACKER_T3669255194_H
#define TEXTTRACKER_T3669255194_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.TextTracker
struct  TextTracker_t3669255194  : public Tracker_t1731515739
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTTRACKER_T3669255194_H
#ifndef __STATICARRAYINITTYPESIZEU3D24_T913725550_H
#define __STATICARRAYINITTYPESIZEU3D24_T913725550_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=24
struct  __StaticArrayInitTypeSizeU3D24_t913725550 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D24_t913725550__padding[24];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // __STATICARRAYINITTYPESIZEU3D24_T913725550_H
#ifndef WEBCAMARCONTROLLER_T1321869434_H
#define WEBCAMARCONTROLLER_T1321869434_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.WebCamARController
struct  WebCamARController_t1321869434  : public ARController_t1205915989
{
public:
	// System.Int32 Vuforia.WebCamARController::RenderTextureLayer
	int32_t ___RenderTextureLayer_1;
	// System.String Vuforia.WebCamARController::mDeviceNameSetInEditor
	String_t* ___mDeviceNameSetInEditor_2;
	// System.Boolean Vuforia.WebCamARController::mFlipHorizontally
	bool ___mFlipHorizontally_3;
	// Vuforia.WebCamImpl Vuforia.WebCamARController::mWebCamImpl
	WebCamImpl_t90372774 * ___mWebCamImpl_4;

public:
	inline static int32_t get_offset_of_RenderTextureLayer_1() { return static_cast<int32_t>(offsetof(WebCamARController_t1321869434, ___RenderTextureLayer_1)); }
	inline int32_t get_RenderTextureLayer_1() const { return ___RenderTextureLayer_1; }
	inline int32_t* get_address_of_RenderTextureLayer_1() { return &___RenderTextureLayer_1; }
	inline void set_RenderTextureLayer_1(int32_t value)
	{
		___RenderTextureLayer_1 = value;
	}

	inline static int32_t get_offset_of_mDeviceNameSetInEditor_2() { return static_cast<int32_t>(offsetof(WebCamARController_t1321869434, ___mDeviceNameSetInEditor_2)); }
	inline String_t* get_mDeviceNameSetInEditor_2() const { return ___mDeviceNameSetInEditor_2; }
	inline String_t** get_address_of_mDeviceNameSetInEditor_2() { return &___mDeviceNameSetInEditor_2; }
	inline void set_mDeviceNameSetInEditor_2(String_t* value)
	{
		___mDeviceNameSetInEditor_2 = value;
		Il2CppCodeGenWriteBarrier((&___mDeviceNameSetInEditor_2), value);
	}

	inline static int32_t get_offset_of_mFlipHorizontally_3() { return static_cast<int32_t>(offsetof(WebCamARController_t1321869434, ___mFlipHorizontally_3)); }
	inline bool get_mFlipHorizontally_3() const { return ___mFlipHorizontally_3; }
	inline bool* get_address_of_mFlipHorizontally_3() { return &___mFlipHorizontally_3; }
	inline void set_mFlipHorizontally_3(bool value)
	{
		___mFlipHorizontally_3 = value;
	}

	inline static int32_t get_offset_of_mWebCamImpl_4() { return static_cast<int32_t>(offsetof(WebCamARController_t1321869434, ___mWebCamImpl_4)); }
	inline WebCamImpl_t90372774 * get_mWebCamImpl_4() const { return ___mWebCamImpl_4; }
	inline WebCamImpl_t90372774 ** get_address_of_mWebCamImpl_4() { return &___mWebCamImpl_4; }
	inline void set_mWebCamImpl_4(WebCamImpl_t90372774 * value)
	{
		___mWebCamImpl_4 = value;
		Il2CppCodeGenWriteBarrier((&___mWebCamImpl_4), value);
	}
};

struct WebCamARController_t1321869434_StaticFields
{
public:
	// Vuforia.WebCamARController Vuforia.WebCamARController::mInstance
	WebCamARController_t1321869434 * ___mInstance_5;
	// System.Object Vuforia.WebCamARController::mPadlock
	RuntimeObject * ___mPadlock_6;

public:
	inline static int32_t get_offset_of_mInstance_5() { return static_cast<int32_t>(offsetof(WebCamARController_t1321869434_StaticFields, ___mInstance_5)); }
	inline WebCamARController_t1321869434 * get_mInstance_5() const { return ___mInstance_5; }
	inline WebCamARController_t1321869434 ** get_address_of_mInstance_5() { return &___mInstance_5; }
	inline void set_mInstance_5(WebCamARController_t1321869434 * value)
	{
		___mInstance_5 = value;
		Il2CppCodeGenWriteBarrier((&___mInstance_5), value);
	}

	inline static int32_t get_offset_of_mPadlock_6() { return static_cast<int32_t>(offsetof(WebCamARController_t1321869434_StaticFields, ___mPadlock_6)); }
	inline RuntimeObject * get_mPadlock_6() const { return ___mPadlock_6; }
	inline RuntimeObject ** get_address_of_mPadlock_6() { return &___mPadlock_6; }
	inline void set_mPadlock_6(RuntimeObject * value)
	{
		___mPadlock_6 = value;
		Il2CppCodeGenWriteBarrier((&___mPadlock_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WEBCAMARCONTROLLER_T1321869434_H
#ifndef WORDLISTIMPL_T3600997366_H
#define WORDLISTIMPL_T3600997366_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.WordListImpl
struct  WordListImpl_t3600997366  : public WordList_t3717410066
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WORDLISTIMPL_T3600997366_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	IntPtr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline IntPtr_t get_Zero_1() const { return ___Zero_1; }
	inline IntPtr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(IntPtr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef RECT_T3436776195_H
#define RECT_T3436776195_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rect
struct  Rect_t3436776195 
{
public:
	// System.Single UnityEngine.Rect::m_XMin
	float ___m_XMin_0;
	// System.Single UnityEngine.Rect::m_YMin
	float ___m_YMin_1;
	// System.Single UnityEngine.Rect::m_Width
	float ___m_Width_2;
	// System.Single UnityEngine.Rect::m_Height
	float ___m_Height_3;

public:
	inline static int32_t get_offset_of_m_XMin_0() { return static_cast<int32_t>(offsetof(Rect_t3436776195, ___m_XMin_0)); }
	inline float get_m_XMin_0() const { return ___m_XMin_0; }
	inline float* get_address_of_m_XMin_0() { return &___m_XMin_0; }
	inline void set_m_XMin_0(float value)
	{
		___m_XMin_0 = value;
	}

	inline static int32_t get_offset_of_m_YMin_1() { return static_cast<int32_t>(offsetof(Rect_t3436776195, ___m_YMin_1)); }
	inline float get_m_YMin_1() const { return ___m_YMin_1; }
	inline float* get_address_of_m_YMin_1() { return &___m_YMin_1; }
	inline void set_m_YMin_1(float value)
	{
		___m_YMin_1 = value;
	}

	inline static int32_t get_offset_of_m_Width_2() { return static_cast<int32_t>(offsetof(Rect_t3436776195, ___m_Width_2)); }
	inline float get_m_Width_2() const { return ___m_Width_2; }
	inline float* get_address_of_m_Width_2() { return &___m_Width_2; }
	inline void set_m_Width_2(float value)
	{
		___m_Width_2 = value;
	}

	inline static int32_t get_offset_of_m_Height_3() { return static_cast<int32_t>(offsetof(Rect_t3436776195, ___m_Height_3)); }
	inline float get_m_Height_3() const { return ___m_Height_3; }
	inline float* get_address_of_m_Height_3() { return &___m_Height_3; }
	inline void set_m_Height_3(float value)
	{
		___m_Height_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RECT_T3436776195_H
#ifndef RECTANGLEDATA_T1094133618_H
#define RECTANGLEDATA_T1094133618_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.RectangleData
#pragma pack(push, tp, 1)
struct  RectangleData_t1094133618 
{
public:
	// System.Single Vuforia.RectangleData::leftTopX
	float ___leftTopX_0;
	// System.Single Vuforia.RectangleData::leftTopY
	float ___leftTopY_1;
	// System.Single Vuforia.RectangleData::rightBottomX
	float ___rightBottomX_2;
	// System.Single Vuforia.RectangleData::rightBottomY
	float ___rightBottomY_3;

public:
	inline static int32_t get_offset_of_leftTopX_0() { return static_cast<int32_t>(offsetof(RectangleData_t1094133618, ___leftTopX_0)); }
	inline float get_leftTopX_0() const { return ___leftTopX_0; }
	inline float* get_address_of_leftTopX_0() { return &___leftTopX_0; }
	inline void set_leftTopX_0(float value)
	{
		___leftTopX_0 = value;
	}

	inline static int32_t get_offset_of_leftTopY_1() { return static_cast<int32_t>(offsetof(RectangleData_t1094133618, ___leftTopY_1)); }
	inline float get_leftTopY_1() const { return ___leftTopY_1; }
	inline float* get_address_of_leftTopY_1() { return &___leftTopY_1; }
	inline void set_leftTopY_1(float value)
	{
		___leftTopY_1 = value;
	}

	inline static int32_t get_offset_of_rightBottomX_2() { return static_cast<int32_t>(offsetof(RectangleData_t1094133618, ___rightBottomX_2)); }
	inline float get_rightBottomX_2() const { return ___rightBottomX_2; }
	inline float* get_address_of_rightBottomX_2() { return &___rightBottomX_2; }
	inline void set_rightBottomX_2(float value)
	{
		___rightBottomX_2 = value;
	}

	inline static int32_t get_offset_of_rightBottomY_3() { return static_cast<int32_t>(offsetof(RectangleData_t1094133618, ___rightBottomY_3)); }
	inline float get_rightBottomY_3() const { return ___rightBottomY_3; }
	inline float* get_address_of_rightBottomY_3() { return &___rightBottomY_3; }
	inline void set_rightBottomY_3(float value)
	{
		___rightBottomY_3 = value;
	}
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RECTANGLEDATA_T1094133618_H
#ifndef VIDEOMODEDATA_T2436408626_H
#define VIDEOMODEDATA_T2436408626_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.CameraDevice/VideoModeData
#pragma pack(push, tp, 1)
struct  VideoModeData_t2436408626 
{
public:
	// System.Int32 Vuforia.CameraDevice/VideoModeData::width
	int32_t ___width_0;
	// System.Int32 Vuforia.CameraDevice/VideoModeData::height
	int32_t ___height_1;
	// System.Single Vuforia.CameraDevice/VideoModeData::frameRate
	float ___frameRate_2;
	// System.Int32 Vuforia.CameraDevice/VideoModeData::unused
	int32_t ___unused_3;

public:
	inline static int32_t get_offset_of_width_0() { return static_cast<int32_t>(offsetof(VideoModeData_t2436408626, ___width_0)); }
	inline int32_t get_width_0() const { return ___width_0; }
	inline int32_t* get_address_of_width_0() { return &___width_0; }
	inline void set_width_0(int32_t value)
	{
		___width_0 = value;
	}

	inline static int32_t get_offset_of_height_1() { return static_cast<int32_t>(offsetof(VideoModeData_t2436408626, ___height_1)); }
	inline int32_t get_height_1() const { return ___height_1; }
	inline int32_t* get_address_of_height_1() { return &___height_1; }
	inline void set_height_1(int32_t value)
	{
		___height_1 = value;
	}

	inline static int32_t get_offset_of_frameRate_2() { return static_cast<int32_t>(offsetof(VideoModeData_t2436408626, ___frameRate_2)); }
	inline float get_frameRate_2() const { return ___frameRate_2; }
	inline float* get_address_of_frameRate_2() { return &___frameRate_2; }
	inline void set_frameRate_2(float value)
	{
		___frameRate_2 = value;
	}

	inline static int32_t get_offset_of_unused_3() { return static_cast<int32_t>(offsetof(VideoModeData_t2436408626, ___unused_3)); }
	inline int32_t get_unused_3() const { return ___unused_3; }
	inline int32_t* get_address_of_unused_3() { return &___unused_3; }
	inline void set_unused_3(int32_t value)
	{
		___unused_3 = value;
	}
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VIDEOMODEDATA_T2436408626_H
#ifndef ENUM_T978401890_H
#define ENUM_T978401890_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t978401890  : public ValueType_t1306072465
{
public:

public:
};

struct Enum_t978401890_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t45629911* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t978401890_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t45629911* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t45629911** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t45629911* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t978401890_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t978401890_marshaled_com
{
};
#endif // ENUM_T978401890_H
#ifndef QUATERNION_T754065749_H
#define QUATERNION_T754065749_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Quaternion
struct  Quaternion_t754065749 
{
public:
	// System.Single UnityEngine.Quaternion::x
	float ___x_0;
	// System.Single UnityEngine.Quaternion::y
	float ___y_1;
	// System.Single UnityEngine.Quaternion::z
	float ___z_2;
	// System.Single UnityEngine.Quaternion::w
	float ___w_3;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Quaternion_t754065749, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Quaternion_t754065749, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(Quaternion_t754065749, ___z_2)); }
	inline float get_z_2() const { return ___z_2; }
	inline float* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(float value)
	{
		___z_2 = value;
	}

	inline static int32_t get_offset_of_w_3() { return static_cast<int32_t>(offsetof(Quaternion_t754065749, ___w_3)); }
	inline float get_w_3() const { return ___w_3; }
	inline float* get_address_of_w_3() { return &___w_3; }
	inline void set_w_3(float value)
	{
		___w_3 = value;
	}
};

struct Quaternion_t754065749_StaticFields
{
public:
	// UnityEngine.Quaternion UnityEngine.Quaternion::identityQuaternion
	Quaternion_t754065749  ___identityQuaternion_4;

public:
	inline static int32_t get_offset_of_identityQuaternion_4() { return static_cast<int32_t>(offsetof(Quaternion_t754065749_StaticFields, ___identityQuaternion_4)); }
	inline Quaternion_t754065749  get_identityQuaternion_4() const { return ___identityQuaternion_4; }
	inline Quaternion_t754065749 * get_address_of_identityQuaternion_4() { return &___identityQuaternion_4; }
	inline void set_identityQuaternion_4(Quaternion_t754065749  value)
	{
		___identityQuaternion_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUATERNION_T754065749_H
#ifndef OBJECTTRACKER_T4023638979_H
#define OBJECTTRACKER_T4023638979_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.ObjectTracker
struct  ObjectTracker_t4023638979  : public Tracker_t1731515739
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OBJECTTRACKER_T4023638979_H
#ifndef VECTOR2_T3577333262_H
#define VECTOR2_T3577333262_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2
struct  Vector2_t3577333262 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_t3577333262, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_t3577333262, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_t3577333262_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_t3577333262  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_t3577333262  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_t3577333262  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_t3577333262  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_t3577333262  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_t3577333262  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_t3577333262  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_t3577333262  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_t3577333262_StaticFields, ___zeroVector_2)); }
	inline Vector2_t3577333262  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_t3577333262 * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_t3577333262  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_t3577333262_StaticFields, ___oneVector_3)); }
	inline Vector2_t3577333262  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_t3577333262 * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_t3577333262  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_t3577333262_StaticFields, ___upVector_4)); }
	inline Vector2_t3577333262  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_t3577333262 * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_t3577333262  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_t3577333262_StaticFields, ___downVector_5)); }
	inline Vector2_t3577333262  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_t3577333262 * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_t3577333262  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_t3577333262_StaticFields, ___leftVector_6)); }
	inline Vector2_t3577333262  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_t3577333262 * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_t3577333262  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_t3577333262_StaticFields, ___rightVector_7)); }
	inline Vector2_t3577333262  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_t3577333262 * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_t3577333262  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_t3577333262_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_t3577333262  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_t3577333262 * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_t3577333262  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_t3577333262_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_t3577333262  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_t3577333262 * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_t3577333262  value)
	{
		___negativeInfinityVector_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_T3577333262_H
#ifndef SMARTTERRAINTRACKER_T1505679812_H
#define SMARTTERRAINTRACKER_T1505679812_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.SmartTerrainTracker
struct  SmartTerrainTracker_t1505679812  : public Tracker_t1731515739
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SMARTTERRAINTRACKER_T1505679812_H
#ifndef STATEMANAGERIMPL_T351768961_H
#define STATEMANAGERIMPL_T351768961_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.StateManagerImpl
struct  StateManagerImpl_t351768961  : public StateManager_t231326187
{
public:
	// System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.TrackableBehaviour> Vuforia.StateManagerImpl::mTrackableBehaviours
	Dictionary_2_t3615086520 * ___mTrackableBehaviours_0;
	// System.Collections.Generic.List`1<System.Int32> Vuforia.StateManagerImpl::mAutomaticallyCreatedBehaviours
	List_1_t3093093796 * ___mAutomaticallyCreatedBehaviours_1;
	// System.Collections.Generic.List`1<Vuforia.TrackableBehaviour> Vuforia.StateManagerImpl::mBehavioursMarkedForDeletion
	List_1_t1274228563 * ___mBehavioursMarkedForDeletion_2;
	// System.Collections.Generic.List`1<Vuforia.TrackableBehaviour> Vuforia.StateManagerImpl::mActiveTrackableBehaviours
	List_1_t1274228563 * ___mActiveTrackableBehaviours_3;
	// Vuforia.WordManagerImpl Vuforia.StateManagerImpl::mWordManager
	WordManagerImpl_t2273252301 * ___mWordManager_4;
	// Vuforia.VuMarkManagerImpl Vuforia.StateManagerImpl::mVuMarkManager
	VuMarkManagerImpl_t2293130653 * ___mVuMarkManager_5;
	// Vuforia.DeviceTrackingManager Vuforia.StateManagerImpl::mDeviceTrackingManager
	DeviceTrackingManager_t2027984062 * ___mDeviceTrackingManager_6;
	// UnityEngine.GameObject Vuforia.StateManagerImpl::mCameraPositioningHelper
	GameObject_t1811656094 * ___mCameraPositioningHelper_7;
	// Vuforia.IExtendedTrackingManager Vuforia.StateManagerImpl::mExtendedTrackingManager
	RuntimeObject* ___mExtendedTrackingManager_8;

public:
	inline static int32_t get_offset_of_mTrackableBehaviours_0() { return static_cast<int32_t>(offsetof(StateManagerImpl_t351768961, ___mTrackableBehaviours_0)); }
	inline Dictionary_2_t3615086520 * get_mTrackableBehaviours_0() const { return ___mTrackableBehaviours_0; }
	inline Dictionary_2_t3615086520 ** get_address_of_mTrackableBehaviours_0() { return &___mTrackableBehaviours_0; }
	inline void set_mTrackableBehaviours_0(Dictionary_2_t3615086520 * value)
	{
		___mTrackableBehaviours_0 = value;
		Il2CppCodeGenWriteBarrier((&___mTrackableBehaviours_0), value);
	}

	inline static int32_t get_offset_of_mAutomaticallyCreatedBehaviours_1() { return static_cast<int32_t>(offsetof(StateManagerImpl_t351768961, ___mAutomaticallyCreatedBehaviours_1)); }
	inline List_1_t3093093796 * get_mAutomaticallyCreatedBehaviours_1() const { return ___mAutomaticallyCreatedBehaviours_1; }
	inline List_1_t3093093796 ** get_address_of_mAutomaticallyCreatedBehaviours_1() { return &___mAutomaticallyCreatedBehaviours_1; }
	inline void set_mAutomaticallyCreatedBehaviours_1(List_1_t3093093796 * value)
	{
		___mAutomaticallyCreatedBehaviours_1 = value;
		Il2CppCodeGenWriteBarrier((&___mAutomaticallyCreatedBehaviours_1), value);
	}

	inline static int32_t get_offset_of_mBehavioursMarkedForDeletion_2() { return static_cast<int32_t>(offsetof(StateManagerImpl_t351768961, ___mBehavioursMarkedForDeletion_2)); }
	inline List_1_t1274228563 * get_mBehavioursMarkedForDeletion_2() const { return ___mBehavioursMarkedForDeletion_2; }
	inline List_1_t1274228563 ** get_address_of_mBehavioursMarkedForDeletion_2() { return &___mBehavioursMarkedForDeletion_2; }
	inline void set_mBehavioursMarkedForDeletion_2(List_1_t1274228563 * value)
	{
		___mBehavioursMarkedForDeletion_2 = value;
		Il2CppCodeGenWriteBarrier((&___mBehavioursMarkedForDeletion_2), value);
	}

	inline static int32_t get_offset_of_mActiveTrackableBehaviours_3() { return static_cast<int32_t>(offsetof(StateManagerImpl_t351768961, ___mActiveTrackableBehaviours_3)); }
	inline List_1_t1274228563 * get_mActiveTrackableBehaviours_3() const { return ___mActiveTrackableBehaviours_3; }
	inline List_1_t1274228563 ** get_address_of_mActiveTrackableBehaviours_3() { return &___mActiveTrackableBehaviours_3; }
	inline void set_mActiveTrackableBehaviours_3(List_1_t1274228563 * value)
	{
		___mActiveTrackableBehaviours_3 = value;
		Il2CppCodeGenWriteBarrier((&___mActiveTrackableBehaviours_3), value);
	}

	inline static int32_t get_offset_of_mWordManager_4() { return static_cast<int32_t>(offsetof(StateManagerImpl_t351768961, ___mWordManager_4)); }
	inline WordManagerImpl_t2273252301 * get_mWordManager_4() const { return ___mWordManager_4; }
	inline WordManagerImpl_t2273252301 ** get_address_of_mWordManager_4() { return &___mWordManager_4; }
	inline void set_mWordManager_4(WordManagerImpl_t2273252301 * value)
	{
		___mWordManager_4 = value;
		Il2CppCodeGenWriteBarrier((&___mWordManager_4), value);
	}

	inline static int32_t get_offset_of_mVuMarkManager_5() { return static_cast<int32_t>(offsetof(StateManagerImpl_t351768961, ___mVuMarkManager_5)); }
	inline VuMarkManagerImpl_t2293130653 * get_mVuMarkManager_5() const { return ___mVuMarkManager_5; }
	inline VuMarkManagerImpl_t2293130653 ** get_address_of_mVuMarkManager_5() { return &___mVuMarkManager_5; }
	inline void set_mVuMarkManager_5(VuMarkManagerImpl_t2293130653 * value)
	{
		___mVuMarkManager_5 = value;
		Il2CppCodeGenWriteBarrier((&___mVuMarkManager_5), value);
	}

	inline static int32_t get_offset_of_mDeviceTrackingManager_6() { return static_cast<int32_t>(offsetof(StateManagerImpl_t351768961, ___mDeviceTrackingManager_6)); }
	inline DeviceTrackingManager_t2027984062 * get_mDeviceTrackingManager_6() const { return ___mDeviceTrackingManager_6; }
	inline DeviceTrackingManager_t2027984062 ** get_address_of_mDeviceTrackingManager_6() { return &___mDeviceTrackingManager_6; }
	inline void set_mDeviceTrackingManager_6(DeviceTrackingManager_t2027984062 * value)
	{
		___mDeviceTrackingManager_6 = value;
		Il2CppCodeGenWriteBarrier((&___mDeviceTrackingManager_6), value);
	}

	inline static int32_t get_offset_of_mCameraPositioningHelper_7() { return static_cast<int32_t>(offsetof(StateManagerImpl_t351768961, ___mCameraPositioningHelper_7)); }
	inline GameObject_t1811656094 * get_mCameraPositioningHelper_7() const { return ___mCameraPositioningHelper_7; }
	inline GameObject_t1811656094 ** get_address_of_mCameraPositioningHelper_7() { return &___mCameraPositioningHelper_7; }
	inline void set_mCameraPositioningHelper_7(GameObject_t1811656094 * value)
	{
		___mCameraPositioningHelper_7 = value;
		Il2CppCodeGenWriteBarrier((&___mCameraPositioningHelper_7), value);
	}

	inline static int32_t get_offset_of_mExtendedTrackingManager_8() { return static_cast<int32_t>(offsetof(StateManagerImpl_t351768961, ___mExtendedTrackingManager_8)); }
	inline RuntimeObject* get_mExtendedTrackingManager_8() const { return ___mExtendedTrackingManager_8; }
	inline RuntimeObject** get_address_of_mExtendedTrackingManager_8() { return &___mExtendedTrackingManager_8; }
	inline void set_mExtendedTrackingManager_8(RuntimeObject* value)
	{
		___mExtendedTrackingManager_8 = value;
		Il2CppCodeGenWriteBarrier((&___mExtendedTrackingManager_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STATEMANAGERIMPL_T351768961_H
#ifndef MATRIX4X4_T4266809202_H
#define MATRIX4X4_T4266809202_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Matrix4x4
struct  Matrix4x4_t4266809202 
{
public:
	// System.Single UnityEngine.Matrix4x4::m00
	float ___m00_0;
	// System.Single UnityEngine.Matrix4x4::m10
	float ___m10_1;
	// System.Single UnityEngine.Matrix4x4::m20
	float ___m20_2;
	// System.Single UnityEngine.Matrix4x4::m30
	float ___m30_3;
	// System.Single UnityEngine.Matrix4x4::m01
	float ___m01_4;
	// System.Single UnityEngine.Matrix4x4::m11
	float ___m11_5;
	// System.Single UnityEngine.Matrix4x4::m21
	float ___m21_6;
	// System.Single UnityEngine.Matrix4x4::m31
	float ___m31_7;
	// System.Single UnityEngine.Matrix4x4::m02
	float ___m02_8;
	// System.Single UnityEngine.Matrix4x4::m12
	float ___m12_9;
	// System.Single UnityEngine.Matrix4x4::m22
	float ___m22_10;
	// System.Single UnityEngine.Matrix4x4::m32
	float ___m32_11;
	// System.Single UnityEngine.Matrix4x4::m03
	float ___m03_12;
	// System.Single UnityEngine.Matrix4x4::m13
	float ___m13_13;
	// System.Single UnityEngine.Matrix4x4::m23
	float ___m23_14;
	// System.Single UnityEngine.Matrix4x4::m33
	float ___m33_15;

public:
	inline static int32_t get_offset_of_m00_0() { return static_cast<int32_t>(offsetof(Matrix4x4_t4266809202, ___m00_0)); }
	inline float get_m00_0() const { return ___m00_0; }
	inline float* get_address_of_m00_0() { return &___m00_0; }
	inline void set_m00_0(float value)
	{
		___m00_0 = value;
	}

	inline static int32_t get_offset_of_m10_1() { return static_cast<int32_t>(offsetof(Matrix4x4_t4266809202, ___m10_1)); }
	inline float get_m10_1() const { return ___m10_1; }
	inline float* get_address_of_m10_1() { return &___m10_1; }
	inline void set_m10_1(float value)
	{
		___m10_1 = value;
	}

	inline static int32_t get_offset_of_m20_2() { return static_cast<int32_t>(offsetof(Matrix4x4_t4266809202, ___m20_2)); }
	inline float get_m20_2() const { return ___m20_2; }
	inline float* get_address_of_m20_2() { return &___m20_2; }
	inline void set_m20_2(float value)
	{
		___m20_2 = value;
	}

	inline static int32_t get_offset_of_m30_3() { return static_cast<int32_t>(offsetof(Matrix4x4_t4266809202, ___m30_3)); }
	inline float get_m30_3() const { return ___m30_3; }
	inline float* get_address_of_m30_3() { return &___m30_3; }
	inline void set_m30_3(float value)
	{
		___m30_3 = value;
	}

	inline static int32_t get_offset_of_m01_4() { return static_cast<int32_t>(offsetof(Matrix4x4_t4266809202, ___m01_4)); }
	inline float get_m01_4() const { return ___m01_4; }
	inline float* get_address_of_m01_4() { return &___m01_4; }
	inline void set_m01_4(float value)
	{
		___m01_4 = value;
	}

	inline static int32_t get_offset_of_m11_5() { return static_cast<int32_t>(offsetof(Matrix4x4_t4266809202, ___m11_5)); }
	inline float get_m11_5() const { return ___m11_5; }
	inline float* get_address_of_m11_5() { return &___m11_5; }
	inline void set_m11_5(float value)
	{
		___m11_5 = value;
	}

	inline static int32_t get_offset_of_m21_6() { return static_cast<int32_t>(offsetof(Matrix4x4_t4266809202, ___m21_6)); }
	inline float get_m21_6() const { return ___m21_6; }
	inline float* get_address_of_m21_6() { return &___m21_6; }
	inline void set_m21_6(float value)
	{
		___m21_6 = value;
	}

	inline static int32_t get_offset_of_m31_7() { return static_cast<int32_t>(offsetof(Matrix4x4_t4266809202, ___m31_7)); }
	inline float get_m31_7() const { return ___m31_7; }
	inline float* get_address_of_m31_7() { return &___m31_7; }
	inline void set_m31_7(float value)
	{
		___m31_7 = value;
	}

	inline static int32_t get_offset_of_m02_8() { return static_cast<int32_t>(offsetof(Matrix4x4_t4266809202, ___m02_8)); }
	inline float get_m02_8() const { return ___m02_8; }
	inline float* get_address_of_m02_8() { return &___m02_8; }
	inline void set_m02_8(float value)
	{
		___m02_8 = value;
	}

	inline static int32_t get_offset_of_m12_9() { return static_cast<int32_t>(offsetof(Matrix4x4_t4266809202, ___m12_9)); }
	inline float get_m12_9() const { return ___m12_9; }
	inline float* get_address_of_m12_9() { return &___m12_9; }
	inline void set_m12_9(float value)
	{
		___m12_9 = value;
	}

	inline static int32_t get_offset_of_m22_10() { return static_cast<int32_t>(offsetof(Matrix4x4_t4266809202, ___m22_10)); }
	inline float get_m22_10() const { return ___m22_10; }
	inline float* get_address_of_m22_10() { return &___m22_10; }
	inline void set_m22_10(float value)
	{
		___m22_10 = value;
	}

	inline static int32_t get_offset_of_m32_11() { return static_cast<int32_t>(offsetof(Matrix4x4_t4266809202, ___m32_11)); }
	inline float get_m32_11() const { return ___m32_11; }
	inline float* get_address_of_m32_11() { return &___m32_11; }
	inline void set_m32_11(float value)
	{
		___m32_11 = value;
	}

	inline static int32_t get_offset_of_m03_12() { return static_cast<int32_t>(offsetof(Matrix4x4_t4266809202, ___m03_12)); }
	inline float get_m03_12() const { return ___m03_12; }
	inline float* get_address_of_m03_12() { return &___m03_12; }
	inline void set_m03_12(float value)
	{
		___m03_12 = value;
	}

	inline static int32_t get_offset_of_m13_13() { return static_cast<int32_t>(offsetof(Matrix4x4_t4266809202, ___m13_13)); }
	inline float get_m13_13() const { return ___m13_13; }
	inline float* get_address_of_m13_13() { return &___m13_13; }
	inline void set_m13_13(float value)
	{
		___m13_13 = value;
	}

	inline static int32_t get_offset_of_m23_14() { return static_cast<int32_t>(offsetof(Matrix4x4_t4266809202, ___m23_14)); }
	inline float get_m23_14() const { return ___m23_14; }
	inline float* get_address_of_m23_14() { return &___m23_14; }
	inline void set_m23_14(float value)
	{
		___m23_14 = value;
	}

	inline static int32_t get_offset_of_m33_15() { return static_cast<int32_t>(offsetof(Matrix4x4_t4266809202, ___m33_15)); }
	inline float get_m33_15() const { return ___m33_15; }
	inline float* get_address_of_m33_15() { return &___m33_15; }
	inline void set_m33_15(float value)
	{
		___m33_15 = value;
	}
};

struct Matrix4x4_t4266809202_StaticFields
{
public:
	// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::zeroMatrix
	Matrix4x4_t4266809202  ___zeroMatrix_16;
	// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::identityMatrix
	Matrix4x4_t4266809202  ___identityMatrix_17;

public:
	inline static int32_t get_offset_of_zeroMatrix_16() { return static_cast<int32_t>(offsetof(Matrix4x4_t4266809202_StaticFields, ___zeroMatrix_16)); }
	inline Matrix4x4_t4266809202  get_zeroMatrix_16() const { return ___zeroMatrix_16; }
	inline Matrix4x4_t4266809202 * get_address_of_zeroMatrix_16() { return &___zeroMatrix_16; }
	inline void set_zeroMatrix_16(Matrix4x4_t4266809202  value)
	{
		___zeroMatrix_16 = value;
	}

	inline static int32_t get_offset_of_identityMatrix_17() { return static_cast<int32_t>(offsetof(Matrix4x4_t4266809202_StaticFields, ___identityMatrix_17)); }
	inline Matrix4x4_t4266809202  get_identityMatrix_17() const { return ___identityMatrix_17; }
	inline Matrix4x4_t4266809202 * get_address_of_identityMatrix_17() { return &___identityMatrix_17; }
	inline void set_identityMatrix_17(Matrix4x4_t4266809202  value)
	{
		___identityMatrix_17 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATRIX4X4_T4266809202_H
#ifndef VUFORIAMACROS_T2716100560_H
#define VUFORIAMACROS_T2716100560_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.VuforiaMacros
struct  VuforiaMacros_t2716100560 
{
public:
	union
	{
		struct
		{
		};
		uint8_t VuforiaMacros_t2716100560__padding[1];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VUFORIAMACROS_T2716100560_H
#ifndef TRACKERMANAGERIMPL_T3117777030_H
#define TRACKERMANAGERIMPL_T3117777030_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.TrackerManagerImpl
struct  TrackerManagerImpl_t3117777030  : public TrackerManager_t814515577
{
public:
	// Vuforia.ObjectTracker Vuforia.TrackerManagerImpl::mObjectTracker
	ObjectTracker_t4023638979 * ___mObjectTracker_1;
	// Vuforia.TextTracker Vuforia.TrackerManagerImpl::mTextTracker
	TextTracker_t3669255194 * ___mTextTracker_2;
	// Vuforia.SmartTerrainTracker Vuforia.TrackerManagerImpl::mSmartTerrainTracker
	SmartTerrainTracker_t1505679812 * ___mSmartTerrainTracker_3;
	// Vuforia.DeviceTracker Vuforia.TrackerManagerImpl::mDeviceTracker
	DeviceTracker_t54803992 * ___mDeviceTracker_4;
	// Vuforia.StateManager Vuforia.TrackerManagerImpl::mStateManager
	StateManager_t231326187 * ___mStateManager_5;

public:
	inline static int32_t get_offset_of_mObjectTracker_1() { return static_cast<int32_t>(offsetof(TrackerManagerImpl_t3117777030, ___mObjectTracker_1)); }
	inline ObjectTracker_t4023638979 * get_mObjectTracker_1() const { return ___mObjectTracker_1; }
	inline ObjectTracker_t4023638979 ** get_address_of_mObjectTracker_1() { return &___mObjectTracker_1; }
	inline void set_mObjectTracker_1(ObjectTracker_t4023638979 * value)
	{
		___mObjectTracker_1 = value;
		Il2CppCodeGenWriteBarrier((&___mObjectTracker_1), value);
	}

	inline static int32_t get_offset_of_mTextTracker_2() { return static_cast<int32_t>(offsetof(TrackerManagerImpl_t3117777030, ___mTextTracker_2)); }
	inline TextTracker_t3669255194 * get_mTextTracker_2() const { return ___mTextTracker_2; }
	inline TextTracker_t3669255194 ** get_address_of_mTextTracker_2() { return &___mTextTracker_2; }
	inline void set_mTextTracker_2(TextTracker_t3669255194 * value)
	{
		___mTextTracker_2 = value;
		Il2CppCodeGenWriteBarrier((&___mTextTracker_2), value);
	}

	inline static int32_t get_offset_of_mSmartTerrainTracker_3() { return static_cast<int32_t>(offsetof(TrackerManagerImpl_t3117777030, ___mSmartTerrainTracker_3)); }
	inline SmartTerrainTracker_t1505679812 * get_mSmartTerrainTracker_3() const { return ___mSmartTerrainTracker_3; }
	inline SmartTerrainTracker_t1505679812 ** get_address_of_mSmartTerrainTracker_3() { return &___mSmartTerrainTracker_3; }
	inline void set_mSmartTerrainTracker_3(SmartTerrainTracker_t1505679812 * value)
	{
		___mSmartTerrainTracker_3 = value;
		Il2CppCodeGenWriteBarrier((&___mSmartTerrainTracker_3), value);
	}

	inline static int32_t get_offset_of_mDeviceTracker_4() { return static_cast<int32_t>(offsetof(TrackerManagerImpl_t3117777030, ___mDeviceTracker_4)); }
	inline DeviceTracker_t54803992 * get_mDeviceTracker_4() const { return ___mDeviceTracker_4; }
	inline DeviceTracker_t54803992 ** get_address_of_mDeviceTracker_4() { return &___mDeviceTracker_4; }
	inline void set_mDeviceTracker_4(DeviceTracker_t54803992 * value)
	{
		___mDeviceTracker_4 = value;
		Il2CppCodeGenWriteBarrier((&___mDeviceTracker_4), value);
	}

	inline static int32_t get_offset_of_mStateManager_5() { return static_cast<int32_t>(offsetof(TrackerManagerImpl_t3117777030, ___mStateManager_5)); }
	inline StateManager_t231326187 * get_mStateManager_5() const { return ___mStateManager_5; }
	inline StateManager_t231326187 ** get_address_of_mStateManager_5() { return &___mStateManager_5; }
	inline void set_mStateManager_5(StateManager_t231326187 * value)
	{
		___mStateManager_5 = value;
		Il2CppCodeGenWriteBarrier((&___mStateManager_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRACKERMANAGERIMPL_T3117777030_H
#ifndef TARGETFINDERSTATE_T163528969_H
#define TARGETFINDERSTATE_T163528969_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.TargetFinderImpl/TargetFinderState
#pragma pack(push, tp, 1)
struct  TargetFinderState_t163528969 
{
public:
	// System.Int32 Vuforia.TargetFinderImpl/TargetFinderState::IsRequesting
	int32_t ___IsRequesting_0;
	// System.Int32 Vuforia.TargetFinderImpl/TargetFinderState::UpdateState
	int32_t ___UpdateState_1;
	// System.Int32 Vuforia.TargetFinderImpl/TargetFinderState::ResultCount
	int32_t ___ResultCount_2;
	// System.Int32 Vuforia.TargetFinderImpl/TargetFinderState::unused
	int32_t ___unused_3;

public:
	inline static int32_t get_offset_of_IsRequesting_0() { return static_cast<int32_t>(offsetof(TargetFinderState_t163528969, ___IsRequesting_0)); }
	inline int32_t get_IsRequesting_0() const { return ___IsRequesting_0; }
	inline int32_t* get_address_of_IsRequesting_0() { return &___IsRequesting_0; }
	inline void set_IsRequesting_0(int32_t value)
	{
		___IsRequesting_0 = value;
	}

	inline static int32_t get_offset_of_UpdateState_1() { return static_cast<int32_t>(offsetof(TargetFinderState_t163528969, ___UpdateState_1)); }
	inline int32_t get_UpdateState_1() const { return ___UpdateState_1; }
	inline int32_t* get_address_of_UpdateState_1() { return &___UpdateState_1; }
	inline void set_UpdateState_1(int32_t value)
	{
		___UpdateState_1 = value;
	}

	inline static int32_t get_offset_of_ResultCount_2() { return static_cast<int32_t>(offsetof(TargetFinderState_t163528969, ___ResultCount_2)); }
	inline int32_t get_ResultCount_2() const { return ___ResultCount_2; }
	inline int32_t* get_address_of_ResultCount_2() { return &___ResultCount_2; }
	inline void set_ResultCount_2(int32_t value)
	{
		___ResultCount_2 = value;
	}

	inline static int32_t get_offset_of_unused_3() { return static_cast<int32_t>(offsetof(TargetFinderState_t163528969, ___unused_3)); }
	inline int32_t get_unused_3() const { return ___unused_3; }
	inline int32_t* get_address_of_unused_3() { return &___unused_3; }
	inline void set_unused_3(int32_t value)
	{
		___unused_3 = value;
	}
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TARGETFINDERSTATE_T163528969_H
#ifndef VECTOR3_T2903530434_H
#define VECTOR3_T2903530434_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_t2903530434 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_1;
	// System.Single UnityEngine.Vector3::y
	float ___y_2;
	// System.Single UnityEngine.Vector3::z
	float ___z_3;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector3_t2903530434, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector3_t2903530434, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector3_t2903530434, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}
};

struct Vector3_t2903530434_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t2903530434  ___zeroVector_4;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t2903530434  ___oneVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t2903530434  ___upVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t2903530434  ___downVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t2903530434  ___leftVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t2903530434  ___rightVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t2903530434  ___forwardVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t2903530434  ___backVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t2903530434  ___positiveInfinityVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t2903530434  ___negativeInfinityVector_13;

public:
	inline static int32_t get_offset_of_zeroVector_4() { return static_cast<int32_t>(offsetof(Vector3_t2903530434_StaticFields, ___zeroVector_4)); }
	inline Vector3_t2903530434  get_zeroVector_4() const { return ___zeroVector_4; }
	inline Vector3_t2903530434 * get_address_of_zeroVector_4() { return &___zeroVector_4; }
	inline void set_zeroVector_4(Vector3_t2903530434  value)
	{
		___zeroVector_4 = value;
	}

	inline static int32_t get_offset_of_oneVector_5() { return static_cast<int32_t>(offsetof(Vector3_t2903530434_StaticFields, ___oneVector_5)); }
	inline Vector3_t2903530434  get_oneVector_5() const { return ___oneVector_5; }
	inline Vector3_t2903530434 * get_address_of_oneVector_5() { return &___oneVector_5; }
	inline void set_oneVector_5(Vector3_t2903530434  value)
	{
		___oneVector_5 = value;
	}

	inline static int32_t get_offset_of_upVector_6() { return static_cast<int32_t>(offsetof(Vector3_t2903530434_StaticFields, ___upVector_6)); }
	inline Vector3_t2903530434  get_upVector_6() const { return ___upVector_6; }
	inline Vector3_t2903530434 * get_address_of_upVector_6() { return &___upVector_6; }
	inline void set_upVector_6(Vector3_t2903530434  value)
	{
		___upVector_6 = value;
	}

	inline static int32_t get_offset_of_downVector_7() { return static_cast<int32_t>(offsetof(Vector3_t2903530434_StaticFields, ___downVector_7)); }
	inline Vector3_t2903530434  get_downVector_7() const { return ___downVector_7; }
	inline Vector3_t2903530434 * get_address_of_downVector_7() { return &___downVector_7; }
	inline void set_downVector_7(Vector3_t2903530434  value)
	{
		___downVector_7 = value;
	}

	inline static int32_t get_offset_of_leftVector_8() { return static_cast<int32_t>(offsetof(Vector3_t2903530434_StaticFields, ___leftVector_8)); }
	inline Vector3_t2903530434  get_leftVector_8() const { return ___leftVector_8; }
	inline Vector3_t2903530434 * get_address_of_leftVector_8() { return &___leftVector_8; }
	inline void set_leftVector_8(Vector3_t2903530434  value)
	{
		___leftVector_8 = value;
	}

	inline static int32_t get_offset_of_rightVector_9() { return static_cast<int32_t>(offsetof(Vector3_t2903530434_StaticFields, ___rightVector_9)); }
	inline Vector3_t2903530434  get_rightVector_9() const { return ___rightVector_9; }
	inline Vector3_t2903530434 * get_address_of_rightVector_9() { return &___rightVector_9; }
	inline void set_rightVector_9(Vector3_t2903530434  value)
	{
		___rightVector_9 = value;
	}

	inline static int32_t get_offset_of_forwardVector_10() { return static_cast<int32_t>(offsetof(Vector3_t2903530434_StaticFields, ___forwardVector_10)); }
	inline Vector3_t2903530434  get_forwardVector_10() const { return ___forwardVector_10; }
	inline Vector3_t2903530434 * get_address_of_forwardVector_10() { return &___forwardVector_10; }
	inline void set_forwardVector_10(Vector3_t2903530434  value)
	{
		___forwardVector_10 = value;
	}

	inline static int32_t get_offset_of_backVector_11() { return static_cast<int32_t>(offsetof(Vector3_t2903530434_StaticFields, ___backVector_11)); }
	inline Vector3_t2903530434  get_backVector_11() const { return ___backVector_11; }
	inline Vector3_t2903530434 * get_address_of_backVector_11() { return &___backVector_11; }
	inline void set_backVector_11(Vector3_t2903530434  value)
	{
		___backVector_11 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_12() { return static_cast<int32_t>(offsetof(Vector3_t2903530434_StaticFields, ___positiveInfinityVector_12)); }
	inline Vector3_t2903530434  get_positiveInfinityVector_12() const { return ___positiveInfinityVector_12; }
	inline Vector3_t2903530434 * get_address_of_positiveInfinityVector_12() { return &___positiveInfinityVector_12; }
	inline void set_positiveInfinityVector_12(Vector3_t2903530434  value)
	{
		___positiveInfinityVector_12 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t2903530434_StaticFields, ___negativeInfinityVector_13)); }
	inline Vector3_t2903530434  get_negativeInfinityVector_13() const { return ___negativeInfinityVector_13; }
	inline Vector3_t2903530434 * get_address_of_negativeInfinityVector_13() { return &___negativeInfinityVector_13; }
	inline void set_negativeInfinityVector_13(Vector3_t2903530434  value)
	{
		___negativeInfinityVector_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_T2903530434_H
#ifndef CLIPPING_MODE_T306275072_H
#define CLIPPING_MODE_T306275072_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.HideExcessAreaAbstractBehaviour/CLIPPING_MODE
struct  CLIPPING_MODE_t306275072 
{
public:
	// System.Int32 Vuforia.HideExcessAreaAbstractBehaviour/CLIPPING_MODE::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(CLIPPING_MODE_t306275072, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CLIPPING_MODE_T306275072_H
#ifndef FRAMEQUALITY_T1148757_H
#define FRAMEQUALITY_T1148757_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.ImageTargetBuilder/FrameQuality
struct  FrameQuality_t1148757 
{
public:
	// System.Int32 Vuforia.ImageTargetBuilder/FrameQuality::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(FrameQuality_t1148757, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FRAMEQUALITY_T1148757_H
#ifndef SCREENORIENTATION_T2818551509_H
#define SCREENORIENTATION_T2818551509_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ScreenOrientation
struct  ScreenOrientation_t2818551509 
{
public:
	// System.Int32 UnityEngine.ScreenOrientation::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ScreenOrientation_t2818551509, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCREENORIENTATION_T2818551509_H
#ifndef WORDFILTERMODE_T2466111678_H
#define WORDFILTERMODE_T2466111678_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.WordFilterMode
struct  WordFilterMode_t2466111678 
{
public:
	// System.Int32 Vuforia.WordFilterMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(WordFilterMode_t2466111678, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WORDFILTERMODE_T2466111678_H
#ifndef WORDPREFABCREATIONMODE_T2868429441_H
#define WORDPREFABCREATIONMODE_T2868429441_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.WordPrefabCreationMode
struct  WordPrefabCreationMode_t2868429441 
{
public:
	// System.Int32 Vuforia.WordPrefabCreationMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(WordPrefabCreationMode_t2868429441, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WORDPREFABCREATIONMODE_T2868429441_H
#ifndef U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T4060893175_H
#define U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T4060893175_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>
struct  U3CPrivateImplementationDetailsU3E_t4060893175  : public RuntimeObject
{
public:

public:
};

struct U3CPrivateImplementationDetailsU3E_t4060893175_StaticFields
{
public:
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=24 <PrivateImplementationDetails>::898C2022A0C02FCE602BF05E1C09BD48301606E5
	__StaticArrayInitTypeSizeU3D24_t913725550  ___898C2022A0C02FCE602BF05E1C09BD48301606E5_0;

public:
	inline static int32_t get_offset_of_U3898C2022A0C02FCE602BF05E1C09BD48301606E5_0() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t4060893175_StaticFields, ___898C2022A0C02FCE602BF05E1C09BD48301606E5_0)); }
	inline __StaticArrayInitTypeSizeU3D24_t913725550  get_U3898C2022A0C02FCE602BF05E1C09BD48301606E5_0() const { return ___898C2022A0C02FCE602BF05E1C09BD48301606E5_0; }
	inline __StaticArrayInitTypeSizeU3D24_t913725550 * get_address_of_U3898C2022A0C02FCE602BF05E1C09BD48301606E5_0() { return &___898C2022A0C02FCE602BF05E1C09BD48301606E5_0; }
	inline void set_U3898C2022A0C02FCE602BF05E1C09BD48301606E5_0(__StaticArrayInitTypeSizeU3D24_t913725550  value)
	{
		___898C2022A0C02FCE602BF05E1C09BD48301606E5_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T4060893175_H
#ifndef CAMERADIRECTION_T1337526284_H
#define CAMERADIRECTION_T1337526284_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.CameraDevice/CameraDirection
struct  CameraDirection_t1337526284 
{
public:
	// System.Int32 Vuforia.CameraDevice/CameraDirection::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(CameraDirection_t1337526284, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAMERADIRECTION_T1337526284_H
#ifndef CAMERADEVICEMODE_T1785007815_H
#define CAMERADEVICEMODE_T1785007815_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.CameraDevice/CameraDeviceMode
struct  CameraDeviceMode_t1785007815 
{
public:
	// System.Int32 Vuforia.CameraDevice/CameraDeviceMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(CameraDeviceMode_t1785007815, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAMERADEVICEMODE_T1785007815_H
#ifndef IMAGETARGETTYPE_T2769160834_H
#define IMAGETARGETTYPE_T2769160834_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.ImageTargetType
struct  ImageTargetType_t2769160834 
{
public:
	// System.Int32 Vuforia.ImageTargetType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ImageTargetType_t2769160834, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IMAGETARGETTYPE_T2769160834_H
#ifndef TRIGGER_T2488916005_H
#define TRIGGER_T2488916005_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.AnalyticsTracker/Trigger
struct  Trigger_t2488916005 
{
public:
	// System.Int32 UnityEngine.Analytics.AnalyticsTracker/Trigger::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Trigger_t2488916005, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRIGGER_T2488916005_H
#ifndef WORDTEMPLATEMODE_T620524771_H
#define WORDTEMPLATEMODE_T620524771_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.WordTemplateMode
struct  WordTemplateMode_t620524771 
{
public:
	// System.Int32 Vuforia.WordTemplateMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(WordTemplateMode_t620524771, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WORDTEMPLATEMODE_T620524771_H
#ifndef SENSITIVITY_T2611854098_H
#define SENSITIVITY_T2611854098_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.VirtualButton/Sensitivity
struct  Sensitivity_t2611854098 
{
public:
	// System.Int32 Vuforia.VirtualButton/Sensitivity::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Sensitivity_t2611854098, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SENSITIVITY_T2611854098_H
#ifndef OBJECT_T1502412432_H
#define OBJECT_T1502412432_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t1502412432  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	IntPtr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t1502412432, ___m_CachedPtr_0)); }
	inline IntPtr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline IntPtr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(IntPtr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t1502412432_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t1502412432_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t1502412432_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t1502412432_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T1502412432_H
#ifndef WORLDCENTERMODE_T2621860492_H
#define WORLDCENTERMODE_T2621860492_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.VuforiaARController/WorldCenterMode
struct  WorldCenterMode_t2621860492 
{
public:
	// System.Int32 Vuforia.VuforiaARController/WorldCenterMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(WorldCenterMode_t2621860492, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WORLDCENTERMODE_T2621860492_H
#ifndef PROFILEDATA_T740879429_H
#define PROFILEDATA_T740879429_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.WebCamProfile/ProfileData
struct  ProfileData_t740879429 
{
public:
	// Vuforia.VuforiaRenderer/Vec2I Vuforia.WebCamProfile/ProfileData::RequestedTextureSize
	Vec2I_t895597728  ___RequestedTextureSize_0;
	// Vuforia.VuforiaRenderer/Vec2I Vuforia.WebCamProfile/ProfileData::ResampledTextureSize
	Vec2I_t895597728  ___ResampledTextureSize_1;
	// System.Int32 Vuforia.WebCamProfile/ProfileData::RequestedFPS
	int32_t ___RequestedFPS_2;

public:
	inline static int32_t get_offset_of_RequestedTextureSize_0() { return static_cast<int32_t>(offsetof(ProfileData_t740879429, ___RequestedTextureSize_0)); }
	inline Vec2I_t895597728  get_RequestedTextureSize_0() const { return ___RequestedTextureSize_0; }
	inline Vec2I_t895597728 * get_address_of_RequestedTextureSize_0() { return &___RequestedTextureSize_0; }
	inline void set_RequestedTextureSize_0(Vec2I_t895597728  value)
	{
		___RequestedTextureSize_0 = value;
	}

	inline static int32_t get_offset_of_ResampledTextureSize_1() { return static_cast<int32_t>(offsetof(ProfileData_t740879429, ___ResampledTextureSize_1)); }
	inline Vec2I_t895597728  get_ResampledTextureSize_1() const { return ___ResampledTextureSize_1; }
	inline Vec2I_t895597728 * get_address_of_ResampledTextureSize_1() { return &___ResampledTextureSize_1; }
	inline void set_ResampledTextureSize_1(Vec2I_t895597728  value)
	{
		___ResampledTextureSize_1 = value;
	}

	inline static int32_t get_offset_of_RequestedFPS_2() { return static_cast<int32_t>(offsetof(ProfileData_t740879429, ___RequestedFPS_2)); }
	inline int32_t get_RequestedFPS_2() const { return ___RequestedFPS_2; }
	inline int32_t* get_address_of_RequestedFPS_2() { return &___RequestedFPS_2; }
	inline void set_RequestedFPS_2(int32_t value)
	{
		___RequestedFPS_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROFILEDATA_T740879429_H
#ifndef INITERROR_T3486678342_H
#define INITERROR_T3486678342_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.VuforiaUnity/InitError
struct  InitError_t3486678342 
{
public:
	// System.Int32 Vuforia.VuforiaUnity/InitError::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(InitError_t3486678342, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INITERROR_T3486678342_H
#ifndef VUFORIAHINT_T1542517147_H
#define VUFORIAHINT_T1542517147_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.VuforiaUnity/VuforiaHint
struct  VuforiaHint_t1542517147 
{
public:
	// System.Int32 Vuforia.VuforiaUnity/VuforiaHint::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(VuforiaHint_t1542517147, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VUFORIAHINT_T1542517147_H
#ifndef STORAGETYPE_T1466127932_H
#define STORAGETYPE_T1466127932_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.VuforiaUnity/StorageType
struct  StorageType_t1466127932 
{
public:
	// System.Int32 Vuforia.VuforiaUnity/StorageType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(StorageType_t1466127932, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STORAGETYPE_T1466127932_H
#ifndef FPSHINT_T440410800_H
#define FPSHINT_T440410800_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.VuforiaRenderer/FpsHint
struct  FpsHint_t440410800 
{
public:
	// System.Int32 Vuforia.VuforiaRenderer/FpsHint::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(FpsHint_t440410800, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FPSHINT_T440410800_H
#ifndef VIDEOBACKGROUNDREFLECTION_T728651611_H
#define VIDEOBACKGROUNDREFLECTION_T728651611_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.VuforiaRenderer/VideoBackgroundReflection
struct  VideoBackgroundReflection_t728651611 
{
public:
	// System.Int32 Vuforia.VuforiaRenderer/VideoBackgroundReflection::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(VideoBackgroundReflection_t728651611, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VIDEOBACKGROUNDREFLECTION_T728651611_H
#ifndef VIDEOBGCFGDATA_T3234105495_H
#define VIDEOBGCFGDATA_T3234105495_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.VuforiaRenderer/VideoBGCfgData
#pragma pack(push, tp, 1)
struct  VideoBGCfgData_t3234105495 
{
public:
	// Vuforia.VuforiaRenderer/Vec2I Vuforia.VuforiaRenderer/VideoBGCfgData::position
	Vec2I_t895597728  ___position_0;
	// Vuforia.VuforiaRenderer/Vec2I Vuforia.VuforiaRenderer/VideoBGCfgData::size
	Vec2I_t895597728  ___size_1;
	// System.Int32 Vuforia.VuforiaRenderer/VideoBGCfgData::enabled
	int32_t ___enabled_2;
	// System.Int32 Vuforia.VuforiaRenderer/VideoBGCfgData::reflectionInteger
	int32_t ___reflectionInteger_3;

public:
	inline static int32_t get_offset_of_position_0() { return static_cast<int32_t>(offsetof(VideoBGCfgData_t3234105495, ___position_0)); }
	inline Vec2I_t895597728  get_position_0() const { return ___position_0; }
	inline Vec2I_t895597728 * get_address_of_position_0() { return &___position_0; }
	inline void set_position_0(Vec2I_t895597728  value)
	{
		___position_0 = value;
	}

	inline static int32_t get_offset_of_size_1() { return static_cast<int32_t>(offsetof(VideoBGCfgData_t3234105495, ___size_1)); }
	inline Vec2I_t895597728  get_size_1() const { return ___size_1; }
	inline Vec2I_t895597728 * get_address_of_size_1() { return &___size_1; }
	inline void set_size_1(Vec2I_t895597728  value)
	{
		___size_1 = value;
	}

	inline static int32_t get_offset_of_enabled_2() { return static_cast<int32_t>(offsetof(VideoBGCfgData_t3234105495, ___enabled_2)); }
	inline int32_t get_enabled_2() const { return ___enabled_2; }
	inline int32_t* get_address_of_enabled_2() { return &___enabled_2; }
	inline void set_enabled_2(int32_t value)
	{
		___enabled_2 = value;
	}

	inline static int32_t get_offset_of_reflectionInteger_3() { return static_cast<int32_t>(offsetof(VideoBGCfgData_t3234105495, ___reflectionInteger_3)); }
	inline int32_t get_reflectionInteger_3() const { return ___reflectionInteger_3; }
	inline int32_t* get_address_of_reflectionInteger_3() { return &___reflectionInteger_3; }
	inline void set_reflectionInteger_3(int32_t value)
	{
		___reflectionInteger_3 = value;
	}
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VIDEOBGCFGDATA_T3234105495_H
#ifndef TRACKABLESOURCEIMPL_T2372561227_H
#define TRACKABLESOURCEIMPL_T2372561227_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.TrackableSourceImpl
struct  TrackableSourceImpl_t2372561227  : public TrackableSource_t4159007682
{
public:
	// System.IntPtr Vuforia.TrackableSourceImpl::<TrackableSourcePtr>k__BackingField
	IntPtr_t ___U3CTrackableSourcePtrU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CTrackableSourcePtrU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(TrackableSourceImpl_t2372561227, ___U3CTrackableSourcePtrU3Ek__BackingField_0)); }
	inline IntPtr_t get_U3CTrackableSourcePtrU3Ek__BackingField_0() const { return ___U3CTrackableSourcePtrU3Ek__BackingField_0; }
	inline IntPtr_t* get_address_of_U3CTrackableSourcePtrU3Ek__BackingField_0() { return &___U3CTrackableSourcePtrU3Ek__BackingField_0; }
	inline void set_U3CTrackableSourcePtrU3Ek__BackingField_0(IntPtr_t value)
	{
		___U3CTrackableSourcePtrU3Ek__BackingField_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRACKABLESOURCEIMPL_T2372561227_H
#ifndef VIDEOTEXTUREINFO_T773013061_H
#define VIDEOTEXTUREINFO_T773013061_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.VuforiaRenderer/VideoTextureInfo
#pragma pack(push, tp, 1)
struct  VideoTextureInfo_t773013061 
{
public:
	// Vuforia.VuforiaRenderer/Vec2I Vuforia.VuforiaRenderer/VideoTextureInfo::textureSize
	Vec2I_t895597728  ___textureSize_0;
	// Vuforia.VuforiaRenderer/Vec2I Vuforia.VuforiaRenderer/VideoTextureInfo::imageSize
	Vec2I_t895597728  ___imageSize_1;

public:
	inline static int32_t get_offset_of_textureSize_0() { return static_cast<int32_t>(offsetof(VideoTextureInfo_t773013061, ___textureSize_0)); }
	inline Vec2I_t895597728  get_textureSize_0() const { return ___textureSize_0; }
	inline Vec2I_t895597728 * get_address_of_textureSize_0() { return &___textureSize_0; }
	inline void set_textureSize_0(Vec2I_t895597728  value)
	{
		___textureSize_0 = value;
	}

	inline static int32_t get_offset_of_imageSize_1() { return static_cast<int32_t>(offsetof(VideoTextureInfo_t773013061, ___imageSize_1)); }
	inline Vec2I_t895597728  get_imageSize_1() const { return ___imageSize_1; }
	inline Vec2I_t895597728 * get_address_of_imageSize_1() { return &___imageSize_1; }
	inline void set_imageSize_1(Vec2I_t895597728  value)
	{
		___imageSize_1 = value;
	}
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VIDEOTEXTUREINFO_T773013061_H
#ifndef RENDERERAPI_T3250500906_H
#define RENDERERAPI_T3250500906_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.VuforiaRenderer/RendererAPI
struct  RendererAPI_t3250500906 
{
public:
	// System.Int32 Vuforia.VuforiaRenderer/RendererAPI::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(RendererAPI_t3250500906, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RENDERERAPI_T3250500906_H
#ifndef INITIALIZABLEBOOL_T766459865_H
#define INITIALIZABLEBOOL_T766459865_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.VuforiaRuntimeUtilities/InitializableBool
struct  InitializableBool_t766459865 
{
public:
	// System.Int32 Vuforia.VuforiaRuntimeUtilities/InitializableBool::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(InitializableBool_t766459865, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INITIALIZABLEBOOL_T766459865_H
#ifndef INITSTATE_T3280104792_H
#define INITSTATE_T3280104792_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.TargetFinder/InitState
struct  InitState_t3280104792 
{
public:
	// System.Int32 Vuforia.TargetFinder/InitState::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(InitState_t3280104792, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INITSTATE_T3280104792_H
#ifndef PIXEL_FORMAT_T1585627136_H
#define PIXEL_FORMAT_T1585627136_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.Image/PIXEL_FORMAT
struct  PIXEL_FORMAT_t1585627136 
{
public:
	// System.Int32 Vuforia.Image/PIXEL_FORMAT::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(PIXEL_FORMAT_t1585627136, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PIXEL_FORMAT_T1585627136_H
#ifndef FILTERMODE_T706444316_H
#define FILTERMODE_T706444316_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.TargetFinder/FilterMode
struct  FilterMode_t706444316 
{
public:
	// System.Int32 Vuforia.TargetFinder/FilterMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(FilterMode_t706444316, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FILTERMODE_T706444316_H
#ifndef TARGETSEARCHRESULT_T213694407_H
#define TARGETSEARCHRESULT_T213694407_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.TargetFinder/TargetSearchResult
struct  TargetSearchResult_t213694407 
{
public:
	// System.String Vuforia.TargetFinder/TargetSearchResult::TargetName
	String_t* ___TargetName_0;
	// System.String Vuforia.TargetFinder/TargetSearchResult::UniqueTargetId
	String_t* ___UniqueTargetId_1;
	// System.Single Vuforia.TargetFinder/TargetSearchResult::TargetSize
	float ___TargetSize_2;
	// System.String Vuforia.TargetFinder/TargetSearchResult::MetaData
	String_t* ___MetaData_3;
	// System.Byte Vuforia.TargetFinder/TargetSearchResult::TrackingRating
	uint8_t ___TrackingRating_4;
	// System.IntPtr Vuforia.TargetFinder/TargetSearchResult::TargetSearchResultPtr
	IntPtr_t ___TargetSearchResultPtr_5;

public:
	inline static int32_t get_offset_of_TargetName_0() { return static_cast<int32_t>(offsetof(TargetSearchResult_t213694407, ___TargetName_0)); }
	inline String_t* get_TargetName_0() const { return ___TargetName_0; }
	inline String_t** get_address_of_TargetName_0() { return &___TargetName_0; }
	inline void set_TargetName_0(String_t* value)
	{
		___TargetName_0 = value;
		Il2CppCodeGenWriteBarrier((&___TargetName_0), value);
	}

	inline static int32_t get_offset_of_UniqueTargetId_1() { return static_cast<int32_t>(offsetof(TargetSearchResult_t213694407, ___UniqueTargetId_1)); }
	inline String_t* get_UniqueTargetId_1() const { return ___UniqueTargetId_1; }
	inline String_t** get_address_of_UniqueTargetId_1() { return &___UniqueTargetId_1; }
	inline void set_UniqueTargetId_1(String_t* value)
	{
		___UniqueTargetId_1 = value;
		Il2CppCodeGenWriteBarrier((&___UniqueTargetId_1), value);
	}

	inline static int32_t get_offset_of_TargetSize_2() { return static_cast<int32_t>(offsetof(TargetSearchResult_t213694407, ___TargetSize_2)); }
	inline float get_TargetSize_2() const { return ___TargetSize_2; }
	inline float* get_address_of_TargetSize_2() { return &___TargetSize_2; }
	inline void set_TargetSize_2(float value)
	{
		___TargetSize_2 = value;
	}

	inline static int32_t get_offset_of_MetaData_3() { return static_cast<int32_t>(offsetof(TargetSearchResult_t213694407, ___MetaData_3)); }
	inline String_t* get_MetaData_3() const { return ___MetaData_3; }
	inline String_t** get_address_of_MetaData_3() { return &___MetaData_3; }
	inline void set_MetaData_3(String_t* value)
	{
		___MetaData_3 = value;
		Il2CppCodeGenWriteBarrier((&___MetaData_3), value);
	}

	inline static int32_t get_offset_of_TrackingRating_4() { return static_cast<int32_t>(offsetof(TargetSearchResult_t213694407, ___TrackingRating_4)); }
	inline uint8_t get_TrackingRating_4() const { return ___TrackingRating_4; }
	inline uint8_t* get_address_of_TrackingRating_4() { return &___TrackingRating_4; }
	inline void set_TrackingRating_4(uint8_t value)
	{
		___TrackingRating_4 = value;
	}

	inline static int32_t get_offset_of_TargetSearchResultPtr_5() { return static_cast<int32_t>(offsetof(TargetSearchResult_t213694407, ___TargetSearchResultPtr_5)); }
	inline IntPtr_t get_TargetSearchResultPtr_5() const { return ___TargetSearchResultPtr_5; }
	inline IntPtr_t* get_address_of_TargetSearchResultPtr_5() { return &___TargetSearchResultPtr_5; }
	inline void set_TargetSearchResultPtr_5(IntPtr_t value)
	{
		___TargetSearchResultPtr_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Vuforia.TargetFinder/TargetSearchResult
struct TargetSearchResult_t213694407_marshaled_pinvoke
{
	char* ___TargetName_0;
	char* ___UniqueTargetId_1;
	float ___TargetSize_2;
	char* ___MetaData_3;
	uint8_t ___TrackingRating_4;
	intptr_t ___TargetSearchResultPtr_5;
};
// Native definition for COM marshalling of Vuforia.TargetFinder/TargetSearchResult
struct TargetSearchResult_t213694407_marshaled_com
{
	Il2CppChar* ___TargetName_0;
	Il2CppChar* ___UniqueTargetId_1;
	float ___TargetSize_2;
	Il2CppChar* ___MetaData_3;
	uint8_t ___TrackingRating_4;
	intptr_t ___TargetSearchResultPtr_5;
};
#endif // TARGETSEARCHRESULT_T213694407_H
#ifndef INTERNALTARGETSEARCHRESULT_T377068014_H
#define INTERNALTARGETSEARCHRESULT_T377068014_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.TargetFinderImpl/InternalTargetSearchResult
#pragma pack(push, tp, 1)
struct  InternalTargetSearchResult_t377068014 
{
public:
	// System.IntPtr Vuforia.TargetFinderImpl/InternalTargetSearchResult::TargetNamePtr
	IntPtr_t ___TargetNamePtr_0;
	// System.IntPtr Vuforia.TargetFinderImpl/InternalTargetSearchResult::UniqueTargetIdPtr
	IntPtr_t ___UniqueTargetIdPtr_1;
	// System.IntPtr Vuforia.TargetFinderImpl/InternalTargetSearchResult::MetaDataPtr
	IntPtr_t ___MetaDataPtr_2;
	// System.IntPtr Vuforia.TargetFinderImpl/InternalTargetSearchResult::TargetSearchResultPtr
	IntPtr_t ___TargetSearchResultPtr_3;
	// System.Single Vuforia.TargetFinderImpl/InternalTargetSearchResult::TargetSize
	float ___TargetSize_4;
	// System.Int32 Vuforia.TargetFinderImpl/InternalTargetSearchResult::TrackingRating
	int32_t ___TrackingRating_5;

public:
	inline static int32_t get_offset_of_TargetNamePtr_0() { return static_cast<int32_t>(offsetof(InternalTargetSearchResult_t377068014, ___TargetNamePtr_0)); }
	inline IntPtr_t get_TargetNamePtr_0() const { return ___TargetNamePtr_0; }
	inline IntPtr_t* get_address_of_TargetNamePtr_0() { return &___TargetNamePtr_0; }
	inline void set_TargetNamePtr_0(IntPtr_t value)
	{
		___TargetNamePtr_0 = value;
	}

	inline static int32_t get_offset_of_UniqueTargetIdPtr_1() { return static_cast<int32_t>(offsetof(InternalTargetSearchResult_t377068014, ___UniqueTargetIdPtr_1)); }
	inline IntPtr_t get_UniqueTargetIdPtr_1() const { return ___UniqueTargetIdPtr_1; }
	inline IntPtr_t* get_address_of_UniqueTargetIdPtr_1() { return &___UniqueTargetIdPtr_1; }
	inline void set_UniqueTargetIdPtr_1(IntPtr_t value)
	{
		___UniqueTargetIdPtr_1 = value;
	}

	inline static int32_t get_offset_of_MetaDataPtr_2() { return static_cast<int32_t>(offsetof(InternalTargetSearchResult_t377068014, ___MetaDataPtr_2)); }
	inline IntPtr_t get_MetaDataPtr_2() const { return ___MetaDataPtr_2; }
	inline IntPtr_t* get_address_of_MetaDataPtr_2() { return &___MetaDataPtr_2; }
	inline void set_MetaDataPtr_2(IntPtr_t value)
	{
		___MetaDataPtr_2 = value;
	}

	inline static int32_t get_offset_of_TargetSearchResultPtr_3() { return static_cast<int32_t>(offsetof(InternalTargetSearchResult_t377068014, ___TargetSearchResultPtr_3)); }
	inline IntPtr_t get_TargetSearchResultPtr_3() const { return ___TargetSearchResultPtr_3; }
	inline IntPtr_t* get_address_of_TargetSearchResultPtr_3() { return &___TargetSearchResultPtr_3; }
	inline void set_TargetSearchResultPtr_3(IntPtr_t value)
	{
		___TargetSearchResultPtr_3 = value;
	}

	inline static int32_t get_offset_of_TargetSize_4() { return static_cast<int32_t>(offsetof(InternalTargetSearchResult_t377068014, ___TargetSize_4)); }
	inline float get_TargetSize_4() const { return ___TargetSize_4; }
	inline float* get_address_of_TargetSize_4() { return &___TargetSize_4; }
	inline void set_TargetSize_4(float value)
	{
		___TargetSize_4 = value;
	}

	inline static int32_t get_offset_of_TrackingRating_5() { return static_cast<int32_t>(offsetof(InternalTargetSearchResult_t377068014, ___TrackingRating_5)); }
	inline int32_t get_TrackingRating_5() const { return ___TrackingRating_5; }
	inline int32_t* get_address_of_TrackingRating_5() { return &___TrackingRating_5; }
	inline void set_TrackingRating_5(int32_t value)
	{
		___TrackingRating_5 = value;
	}
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTERNALTARGETSEARCHRESULT_T377068014_H
#ifndef TARGETFINDERIMPL_T3318134284_H
#define TARGETFINDERIMPL_T3318134284_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.TargetFinderImpl
struct  TargetFinderImpl_t3318134284  : public TargetFinder_t2860227752
{
public:
	// System.IntPtr Vuforia.TargetFinderImpl::mTargetFinderStatePtr
	IntPtr_t ___mTargetFinderStatePtr_0;
	// Vuforia.TargetFinderImpl/TargetFinderState Vuforia.TargetFinderImpl::mTargetFinderState
	TargetFinderState_t163528969  ___mTargetFinderState_1;
	// System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult> Vuforia.TargetFinderImpl::mNewResults
	List_1_t3363810910 * ___mNewResults_2;
	// System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.ImageTarget> Vuforia.TargetFinderImpl::mImageTargets
	Dictionary_2_t1389033120 * ___mImageTargets_3;

public:
	inline static int32_t get_offset_of_mTargetFinderStatePtr_0() { return static_cast<int32_t>(offsetof(TargetFinderImpl_t3318134284, ___mTargetFinderStatePtr_0)); }
	inline IntPtr_t get_mTargetFinderStatePtr_0() const { return ___mTargetFinderStatePtr_0; }
	inline IntPtr_t* get_address_of_mTargetFinderStatePtr_0() { return &___mTargetFinderStatePtr_0; }
	inline void set_mTargetFinderStatePtr_0(IntPtr_t value)
	{
		___mTargetFinderStatePtr_0 = value;
	}

	inline static int32_t get_offset_of_mTargetFinderState_1() { return static_cast<int32_t>(offsetof(TargetFinderImpl_t3318134284, ___mTargetFinderState_1)); }
	inline TargetFinderState_t163528969  get_mTargetFinderState_1() const { return ___mTargetFinderState_1; }
	inline TargetFinderState_t163528969 * get_address_of_mTargetFinderState_1() { return &___mTargetFinderState_1; }
	inline void set_mTargetFinderState_1(TargetFinderState_t163528969  value)
	{
		___mTargetFinderState_1 = value;
	}

	inline static int32_t get_offset_of_mNewResults_2() { return static_cast<int32_t>(offsetof(TargetFinderImpl_t3318134284, ___mNewResults_2)); }
	inline List_1_t3363810910 * get_mNewResults_2() const { return ___mNewResults_2; }
	inline List_1_t3363810910 ** get_address_of_mNewResults_2() { return &___mNewResults_2; }
	inline void set_mNewResults_2(List_1_t3363810910 * value)
	{
		___mNewResults_2 = value;
		Il2CppCodeGenWriteBarrier((&___mNewResults_2), value);
	}

	inline static int32_t get_offset_of_mImageTargets_3() { return static_cast<int32_t>(offsetof(TargetFinderImpl_t3318134284, ___mImageTargets_3)); }
	inline Dictionary_2_t1389033120 * get_mImageTargets_3() const { return ___mImageTargets_3; }
	inline Dictionary_2_t1389033120 ** get_address_of_mImageTargets_3() { return &___mImageTargets_3; }
	inline void set_mImageTargets_3(Dictionary_2_t1389033120 * value)
	{
		___mImageTargets_3 = value;
		Il2CppCodeGenWriteBarrier((&___mImageTargets_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TARGETFINDERIMPL_T3318134284_H
#ifndef STATUS_T1358118869_H
#define STATUS_T1358118869_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.TrackableBehaviour/Status
struct  Status_t1358118869 
{
public:
	// System.Int32 Vuforia.TrackableBehaviour/Status::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Status_t1358118869, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STATUS_T1358118869_H
#ifndef COORDINATESYSTEM_T10191807_H
#define COORDINATESYSTEM_T10191807_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.TrackableBehaviour/CoordinateSystem
struct  CoordinateSystem_t10191807 
{
public:
	// System.Int32 Vuforia.TrackableBehaviour/CoordinateSystem::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(CoordinateSystem_t10191807, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COORDINATESYSTEM_T10191807_H
#ifndef UPDATESTATE_T3694783787_H
#define UPDATESTATE_T3694783787_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.TargetFinder/UpdateState
struct  UpdateState_t3694783787 
{
public:
	// System.Int32 Vuforia.TargetFinder/UpdateState::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(UpdateState_t3694783787, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UPDATESTATE_T3694783787_H
#ifndef SURFACEUTILITIES_T184061569_H
#define SURFACEUTILITIES_T184061569_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.SurfaceUtilities
struct  SurfaceUtilities_t184061569  : public RuntimeObject
{
public:

public:
};

struct SurfaceUtilities_t184061569_StaticFields
{
public:
	// UnityEngine.ScreenOrientation Vuforia.SurfaceUtilities::mScreenOrientation
	int32_t ___mScreenOrientation_0;

public:
	inline static int32_t get_offset_of_mScreenOrientation_0() { return static_cast<int32_t>(offsetof(SurfaceUtilities_t184061569_StaticFields, ___mScreenOrientation_0)); }
	inline int32_t get_mScreenOrientation_0() const { return ___mScreenOrientation_0; }
	inline int32_t* get_address_of_mScreenOrientation_0() { return &___mScreenOrientation_0; }
	inline void set_mScreenOrientation_0(int32_t value)
	{
		___mScreenOrientation_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SURFACEUTILITIES_T184061569_H
#ifndef VIDEOBACKGROUNDMANAGER_T2068552549_H
#define VIDEOBACKGROUNDMANAGER_T2068552549_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.VideoBackgroundManager
struct  VideoBackgroundManager_t2068552549  : public ARController_t1205915989
{
public:
	// Vuforia.HideExcessAreaAbstractBehaviour/CLIPPING_MODE Vuforia.VideoBackgroundManager::mClippingMode
	int32_t ___mClippingMode_1;
	// UnityEngine.Shader Vuforia.VideoBackgroundManager::mMatteShader
	Shader_t3778723916 * ___mMatteShader_2;
	// System.Boolean Vuforia.VideoBackgroundManager::mVideoBackgroundEnabled
	bool ___mVideoBackgroundEnabled_3;
	// UnityEngine.Texture Vuforia.VideoBackgroundManager::mTexture
	Texture_t85561421 * ___mTexture_4;
	// System.Boolean Vuforia.VideoBackgroundManager::mVideoBgConfigChanged
	bool ___mVideoBgConfigChanged_5;
	// System.IntPtr Vuforia.VideoBackgroundManager::mNativeTexturePtr
	IntPtr_t ___mNativeTexturePtr_6;

public:
	inline static int32_t get_offset_of_mClippingMode_1() { return static_cast<int32_t>(offsetof(VideoBackgroundManager_t2068552549, ___mClippingMode_1)); }
	inline int32_t get_mClippingMode_1() const { return ___mClippingMode_1; }
	inline int32_t* get_address_of_mClippingMode_1() { return &___mClippingMode_1; }
	inline void set_mClippingMode_1(int32_t value)
	{
		___mClippingMode_1 = value;
	}

	inline static int32_t get_offset_of_mMatteShader_2() { return static_cast<int32_t>(offsetof(VideoBackgroundManager_t2068552549, ___mMatteShader_2)); }
	inline Shader_t3778723916 * get_mMatteShader_2() const { return ___mMatteShader_2; }
	inline Shader_t3778723916 ** get_address_of_mMatteShader_2() { return &___mMatteShader_2; }
	inline void set_mMatteShader_2(Shader_t3778723916 * value)
	{
		___mMatteShader_2 = value;
		Il2CppCodeGenWriteBarrier((&___mMatteShader_2), value);
	}

	inline static int32_t get_offset_of_mVideoBackgroundEnabled_3() { return static_cast<int32_t>(offsetof(VideoBackgroundManager_t2068552549, ___mVideoBackgroundEnabled_3)); }
	inline bool get_mVideoBackgroundEnabled_3() const { return ___mVideoBackgroundEnabled_3; }
	inline bool* get_address_of_mVideoBackgroundEnabled_3() { return &___mVideoBackgroundEnabled_3; }
	inline void set_mVideoBackgroundEnabled_3(bool value)
	{
		___mVideoBackgroundEnabled_3 = value;
	}

	inline static int32_t get_offset_of_mTexture_4() { return static_cast<int32_t>(offsetof(VideoBackgroundManager_t2068552549, ___mTexture_4)); }
	inline Texture_t85561421 * get_mTexture_4() const { return ___mTexture_4; }
	inline Texture_t85561421 ** get_address_of_mTexture_4() { return &___mTexture_4; }
	inline void set_mTexture_4(Texture_t85561421 * value)
	{
		___mTexture_4 = value;
		Il2CppCodeGenWriteBarrier((&___mTexture_4), value);
	}

	inline static int32_t get_offset_of_mVideoBgConfigChanged_5() { return static_cast<int32_t>(offsetof(VideoBackgroundManager_t2068552549, ___mVideoBgConfigChanged_5)); }
	inline bool get_mVideoBgConfigChanged_5() const { return ___mVideoBgConfigChanged_5; }
	inline bool* get_address_of_mVideoBgConfigChanged_5() { return &___mVideoBgConfigChanged_5; }
	inline void set_mVideoBgConfigChanged_5(bool value)
	{
		___mVideoBgConfigChanged_5 = value;
	}

	inline static int32_t get_offset_of_mNativeTexturePtr_6() { return static_cast<int32_t>(offsetof(VideoBackgroundManager_t2068552549, ___mNativeTexturePtr_6)); }
	inline IntPtr_t get_mNativeTexturePtr_6() const { return ___mNativeTexturePtr_6; }
	inline IntPtr_t* get_address_of_mNativeTexturePtr_6() { return &___mNativeTexturePtr_6; }
	inline void set_mNativeTexturePtr_6(IntPtr_t value)
	{
		___mNativeTexturePtr_6 = value;
	}
};

struct VideoBackgroundManager_t2068552549_StaticFields
{
public:
	// Vuforia.VideoBackgroundManager Vuforia.VideoBackgroundManager::mInstance
	VideoBackgroundManager_t2068552549 * ___mInstance_7;
	// System.Object Vuforia.VideoBackgroundManager::mPadlock
	RuntimeObject * ___mPadlock_8;

public:
	inline static int32_t get_offset_of_mInstance_7() { return static_cast<int32_t>(offsetof(VideoBackgroundManager_t2068552549_StaticFields, ___mInstance_7)); }
	inline VideoBackgroundManager_t2068552549 * get_mInstance_7() const { return ___mInstance_7; }
	inline VideoBackgroundManager_t2068552549 ** get_address_of_mInstance_7() { return &___mInstance_7; }
	inline void set_mInstance_7(VideoBackgroundManager_t2068552549 * value)
	{
		___mInstance_7 = value;
		Il2CppCodeGenWriteBarrier((&___mInstance_7), value);
	}

	inline static int32_t get_offset_of_mPadlock_8() { return static_cast<int32_t>(offsetof(VideoBackgroundManager_t2068552549_StaticFields, ___mPadlock_8)); }
	inline RuntimeObject * get_mPadlock_8() const { return ___mPadlock_8; }
	inline RuntimeObject ** get_address_of_mPadlock_8() { return &___mPadlock_8; }
	inline void set_mPadlock_8(RuntimeObject * value)
	{
		___mPadlock_8 = value;
		Il2CppCodeGenWriteBarrier((&___mPadlock_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VIDEOBACKGROUNDMANAGER_T2068552549_H
#ifndef VIRTUALBUTTON_T3666937711_H
#define VIRTUALBUTTON_T3666937711_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.VirtualButton
struct  VirtualButton_t3666937711  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VIRTUALBUTTON_T3666937711_H
#ifndef WEBCAMIMPL_T90372774_H
#define WEBCAMIMPL_T90372774_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.WebCamImpl
struct  WebCamImpl_t90372774  : public RuntimeObject
{
public:
	// UnityEngine.Camera[] Vuforia.WebCamImpl::mARCameras
	CameraU5BU5D_t1519774542* ___mARCameras_0;
	// System.Int32[] Vuforia.WebCamImpl::mOriginalCameraCullMask
	Int32U5BU5D_t2324750880* ___mOriginalCameraCullMask_1;
	// Vuforia.WebCamTexAdaptor Vuforia.WebCamImpl::mWebCamTexture
	WebCamTexAdaptor_t433428952 * ___mWebCamTexture_2;
	// Vuforia.CameraDevice/VideoModeData Vuforia.WebCamImpl::mVideoModeData
	VideoModeData_t2436408626  ___mVideoModeData_3;
	// Vuforia.VuforiaRenderer/VideoTextureInfo Vuforia.WebCamImpl::mVideoTextureInfo
	VideoTextureInfo_t773013061  ___mVideoTextureInfo_4;
	// Vuforia.TextureRenderer Vuforia.WebCamImpl::mTextureRenderer
	TextureRenderer_t2855676147 * ___mTextureRenderer_5;
	// UnityEngine.Texture2D Vuforia.WebCamImpl::mBufferReadTexture
	Texture2D_t415585320 * ___mBufferReadTexture_6;
	// UnityEngine.Rect Vuforia.WebCamImpl::mReadPixelsRect
	Rect_t3436776195  ___mReadPixelsRect_7;
	// Vuforia.WebCamProfile/ProfileData Vuforia.WebCamImpl::mWebCamProfile
	ProfileData_t740879429  ___mWebCamProfile_8;
	// System.Boolean Vuforia.WebCamImpl::mFlipHorizontally
	bool ___mFlipHorizontally_9;
	// System.Boolean Vuforia.WebCamImpl::mIsDirty
	bool ___mIsDirty_10;
	// System.Int32 Vuforia.WebCamImpl::mLastFrameIdx
	int32_t ___mLastFrameIdx_11;
	// System.Int32 Vuforia.WebCamImpl::mRenderTextureLayer
	int32_t ___mRenderTextureLayer_12;
	// System.Boolean Vuforia.WebCamImpl::mWebcamPlaying
	bool ___mWebcamPlaying_13;
	// System.Boolean Vuforia.WebCamImpl::<IsTextureSizeAvailable>k__BackingField
	bool ___U3CIsTextureSizeAvailableU3Ek__BackingField_14;

public:
	inline static int32_t get_offset_of_mARCameras_0() { return static_cast<int32_t>(offsetof(WebCamImpl_t90372774, ___mARCameras_0)); }
	inline CameraU5BU5D_t1519774542* get_mARCameras_0() const { return ___mARCameras_0; }
	inline CameraU5BU5D_t1519774542** get_address_of_mARCameras_0() { return &___mARCameras_0; }
	inline void set_mARCameras_0(CameraU5BU5D_t1519774542* value)
	{
		___mARCameras_0 = value;
		Il2CppCodeGenWriteBarrier((&___mARCameras_0), value);
	}

	inline static int32_t get_offset_of_mOriginalCameraCullMask_1() { return static_cast<int32_t>(offsetof(WebCamImpl_t90372774, ___mOriginalCameraCullMask_1)); }
	inline Int32U5BU5D_t2324750880* get_mOriginalCameraCullMask_1() const { return ___mOriginalCameraCullMask_1; }
	inline Int32U5BU5D_t2324750880** get_address_of_mOriginalCameraCullMask_1() { return &___mOriginalCameraCullMask_1; }
	inline void set_mOriginalCameraCullMask_1(Int32U5BU5D_t2324750880* value)
	{
		___mOriginalCameraCullMask_1 = value;
		Il2CppCodeGenWriteBarrier((&___mOriginalCameraCullMask_1), value);
	}

	inline static int32_t get_offset_of_mWebCamTexture_2() { return static_cast<int32_t>(offsetof(WebCamImpl_t90372774, ___mWebCamTexture_2)); }
	inline WebCamTexAdaptor_t433428952 * get_mWebCamTexture_2() const { return ___mWebCamTexture_2; }
	inline WebCamTexAdaptor_t433428952 ** get_address_of_mWebCamTexture_2() { return &___mWebCamTexture_2; }
	inline void set_mWebCamTexture_2(WebCamTexAdaptor_t433428952 * value)
	{
		___mWebCamTexture_2 = value;
		Il2CppCodeGenWriteBarrier((&___mWebCamTexture_2), value);
	}

	inline static int32_t get_offset_of_mVideoModeData_3() { return static_cast<int32_t>(offsetof(WebCamImpl_t90372774, ___mVideoModeData_3)); }
	inline VideoModeData_t2436408626  get_mVideoModeData_3() const { return ___mVideoModeData_3; }
	inline VideoModeData_t2436408626 * get_address_of_mVideoModeData_3() { return &___mVideoModeData_3; }
	inline void set_mVideoModeData_3(VideoModeData_t2436408626  value)
	{
		___mVideoModeData_3 = value;
	}

	inline static int32_t get_offset_of_mVideoTextureInfo_4() { return static_cast<int32_t>(offsetof(WebCamImpl_t90372774, ___mVideoTextureInfo_4)); }
	inline VideoTextureInfo_t773013061  get_mVideoTextureInfo_4() const { return ___mVideoTextureInfo_4; }
	inline VideoTextureInfo_t773013061 * get_address_of_mVideoTextureInfo_4() { return &___mVideoTextureInfo_4; }
	inline void set_mVideoTextureInfo_4(VideoTextureInfo_t773013061  value)
	{
		___mVideoTextureInfo_4 = value;
	}

	inline static int32_t get_offset_of_mTextureRenderer_5() { return static_cast<int32_t>(offsetof(WebCamImpl_t90372774, ___mTextureRenderer_5)); }
	inline TextureRenderer_t2855676147 * get_mTextureRenderer_5() const { return ___mTextureRenderer_5; }
	inline TextureRenderer_t2855676147 ** get_address_of_mTextureRenderer_5() { return &___mTextureRenderer_5; }
	inline void set_mTextureRenderer_5(TextureRenderer_t2855676147 * value)
	{
		___mTextureRenderer_5 = value;
		Il2CppCodeGenWriteBarrier((&___mTextureRenderer_5), value);
	}

	inline static int32_t get_offset_of_mBufferReadTexture_6() { return static_cast<int32_t>(offsetof(WebCamImpl_t90372774, ___mBufferReadTexture_6)); }
	inline Texture2D_t415585320 * get_mBufferReadTexture_6() const { return ___mBufferReadTexture_6; }
	inline Texture2D_t415585320 ** get_address_of_mBufferReadTexture_6() { return &___mBufferReadTexture_6; }
	inline void set_mBufferReadTexture_6(Texture2D_t415585320 * value)
	{
		___mBufferReadTexture_6 = value;
		Il2CppCodeGenWriteBarrier((&___mBufferReadTexture_6), value);
	}

	inline static int32_t get_offset_of_mReadPixelsRect_7() { return static_cast<int32_t>(offsetof(WebCamImpl_t90372774, ___mReadPixelsRect_7)); }
	inline Rect_t3436776195  get_mReadPixelsRect_7() const { return ___mReadPixelsRect_7; }
	inline Rect_t3436776195 * get_address_of_mReadPixelsRect_7() { return &___mReadPixelsRect_7; }
	inline void set_mReadPixelsRect_7(Rect_t3436776195  value)
	{
		___mReadPixelsRect_7 = value;
	}

	inline static int32_t get_offset_of_mWebCamProfile_8() { return static_cast<int32_t>(offsetof(WebCamImpl_t90372774, ___mWebCamProfile_8)); }
	inline ProfileData_t740879429  get_mWebCamProfile_8() const { return ___mWebCamProfile_8; }
	inline ProfileData_t740879429 * get_address_of_mWebCamProfile_8() { return &___mWebCamProfile_8; }
	inline void set_mWebCamProfile_8(ProfileData_t740879429  value)
	{
		___mWebCamProfile_8 = value;
	}

	inline static int32_t get_offset_of_mFlipHorizontally_9() { return static_cast<int32_t>(offsetof(WebCamImpl_t90372774, ___mFlipHorizontally_9)); }
	inline bool get_mFlipHorizontally_9() const { return ___mFlipHorizontally_9; }
	inline bool* get_address_of_mFlipHorizontally_9() { return &___mFlipHorizontally_9; }
	inline void set_mFlipHorizontally_9(bool value)
	{
		___mFlipHorizontally_9 = value;
	}

	inline static int32_t get_offset_of_mIsDirty_10() { return static_cast<int32_t>(offsetof(WebCamImpl_t90372774, ___mIsDirty_10)); }
	inline bool get_mIsDirty_10() const { return ___mIsDirty_10; }
	inline bool* get_address_of_mIsDirty_10() { return &___mIsDirty_10; }
	inline void set_mIsDirty_10(bool value)
	{
		___mIsDirty_10 = value;
	}

	inline static int32_t get_offset_of_mLastFrameIdx_11() { return static_cast<int32_t>(offsetof(WebCamImpl_t90372774, ___mLastFrameIdx_11)); }
	inline int32_t get_mLastFrameIdx_11() const { return ___mLastFrameIdx_11; }
	inline int32_t* get_address_of_mLastFrameIdx_11() { return &___mLastFrameIdx_11; }
	inline void set_mLastFrameIdx_11(int32_t value)
	{
		___mLastFrameIdx_11 = value;
	}

	inline static int32_t get_offset_of_mRenderTextureLayer_12() { return static_cast<int32_t>(offsetof(WebCamImpl_t90372774, ___mRenderTextureLayer_12)); }
	inline int32_t get_mRenderTextureLayer_12() const { return ___mRenderTextureLayer_12; }
	inline int32_t* get_address_of_mRenderTextureLayer_12() { return &___mRenderTextureLayer_12; }
	inline void set_mRenderTextureLayer_12(int32_t value)
	{
		___mRenderTextureLayer_12 = value;
	}

	inline static int32_t get_offset_of_mWebcamPlaying_13() { return static_cast<int32_t>(offsetof(WebCamImpl_t90372774, ___mWebcamPlaying_13)); }
	inline bool get_mWebcamPlaying_13() const { return ___mWebcamPlaying_13; }
	inline bool* get_address_of_mWebcamPlaying_13() { return &___mWebcamPlaying_13; }
	inline void set_mWebcamPlaying_13(bool value)
	{
		___mWebcamPlaying_13 = value;
	}

	inline static int32_t get_offset_of_U3CIsTextureSizeAvailableU3Ek__BackingField_14() { return static_cast<int32_t>(offsetof(WebCamImpl_t90372774, ___U3CIsTextureSizeAvailableU3Ek__BackingField_14)); }
	inline bool get_U3CIsTextureSizeAvailableU3Ek__BackingField_14() const { return ___U3CIsTextureSizeAvailableU3Ek__BackingField_14; }
	inline bool* get_address_of_U3CIsTextureSizeAvailableU3Ek__BackingField_14() { return &___U3CIsTextureSizeAvailableU3Ek__BackingField_14; }
	inline void set_U3CIsTextureSizeAvailableU3Ek__BackingField_14(bool value)
	{
		___U3CIsTextureSizeAvailableU3Ek__BackingField_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WEBCAMIMPL_T90372774_H
#ifndef VUFORIARUNTIMEUTILITIES_T1036756025_H
#define VUFORIARUNTIMEUTILITIES_T1036756025_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.VuforiaRuntimeUtilities
struct  VuforiaRuntimeUtilities_t1036756025  : public RuntimeObject
{
public:

public:
};

struct VuforiaRuntimeUtilities_t1036756025_StaticFields
{
public:
	// Vuforia.VuforiaRuntimeUtilities/InitializableBool Vuforia.VuforiaRuntimeUtilities::sWebCamUsed
	int32_t ___sWebCamUsed_0;
	// Vuforia.VuforiaRuntimeUtilities/InitializableBool Vuforia.VuforiaRuntimeUtilities::sNativePluginSupport
	int32_t ___sNativePluginSupport_1;

public:
	inline static int32_t get_offset_of_sWebCamUsed_0() { return static_cast<int32_t>(offsetof(VuforiaRuntimeUtilities_t1036756025_StaticFields, ___sWebCamUsed_0)); }
	inline int32_t get_sWebCamUsed_0() const { return ___sWebCamUsed_0; }
	inline int32_t* get_address_of_sWebCamUsed_0() { return &___sWebCamUsed_0; }
	inline void set_sWebCamUsed_0(int32_t value)
	{
		___sWebCamUsed_0 = value;
	}

	inline static int32_t get_offset_of_sNativePluginSupport_1() { return static_cast<int32_t>(offsetof(VuforiaRuntimeUtilities_t1036756025_StaticFields, ___sNativePluginSupport_1)); }
	inline int32_t get_sNativePluginSupport_1() const { return ___sNativePluginSupport_1; }
	inline int32_t* get_address_of_sNativePluginSupport_1() { return &___sNativePluginSupport_1; }
	inline void set_sNativePluginSupport_1(int32_t value)
	{
		___sNativePluginSupport_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VUFORIARUNTIMEUTILITIES_T1036756025_H
#ifndef COMPONENT_T4087199522_H
#define COMPONENT_T4087199522_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t4087199522  : public Object_t1502412432
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T4087199522_H
#ifndef PROFILECOLLECTION_T3131405081_H
#define PROFILECOLLECTION_T3131405081_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.WebCamProfile/ProfileCollection
struct  ProfileCollection_t3131405081 
{
public:
	// Vuforia.WebCamProfile/ProfileData Vuforia.WebCamProfile/ProfileCollection::DefaultProfile
	ProfileData_t740879429  ___DefaultProfile_0;
	// System.Collections.Generic.Dictionary`2<System.String,Vuforia.WebCamProfile/ProfileData> Vuforia.WebCamProfile/ProfileCollection::Profiles
	Dictionary_2_t1910768660 * ___Profiles_1;

public:
	inline static int32_t get_offset_of_DefaultProfile_0() { return static_cast<int32_t>(offsetof(ProfileCollection_t3131405081, ___DefaultProfile_0)); }
	inline ProfileData_t740879429  get_DefaultProfile_0() const { return ___DefaultProfile_0; }
	inline ProfileData_t740879429 * get_address_of_DefaultProfile_0() { return &___DefaultProfile_0; }
	inline void set_DefaultProfile_0(ProfileData_t740879429  value)
	{
		___DefaultProfile_0 = value;
	}

	inline static int32_t get_offset_of_Profiles_1() { return static_cast<int32_t>(offsetof(ProfileCollection_t3131405081, ___Profiles_1)); }
	inline Dictionary_2_t1910768660 * get_Profiles_1() const { return ___Profiles_1; }
	inline Dictionary_2_t1910768660 ** get_address_of_Profiles_1() { return &___Profiles_1; }
	inline void set_Profiles_1(Dictionary_2_t1910768660 * value)
	{
		___Profiles_1 = value;
		Il2CppCodeGenWriteBarrier((&___Profiles_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Vuforia.WebCamProfile/ProfileCollection
struct ProfileCollection_t3131405081_marshaled_pinvoke
{
	ProfileData_t740879429  ___DefaultProfile_0;
	Dictionary_2_t1910768660 * ___Profiles_1;
};
// Native definition for COM marshalling of Vuforia.WebCamProfile/ProfileCollection
struct ProfileCollection_t3131405081_marshaled_com
{
	ProfileData_t740879429  ___DefaultProfile_0;
	Dictionary_2_t1910768660 * ___Profiles_1;
};
#endif // PROFILECOLLECTION_T3131405081_H
#ifndef VUFORIAARCONTROLLER_T1328503142_H
#define VUFORIAARCONTROLLER_T1328503142_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.VuforiaARController
struct  VuforiaARController_t1328503142  : public ARController_t1205915989
{
public:
	// Vuforia.CameraDevice/CameraDeviceMode Vuforia.VuforiaARController::CameraDeviceModeSetting
	int32_t ___CameraDeviceModeSetting_1;
	// System.Int32 Vuforia.VuforiaARController::MaxSimultaneousImageTargets
	int32_t ___MaxSimultaneousImageTargets_2;
	// System.Int32 Vuforia.VuforiaARController::MaxSimultaneousObjectTargets
	int32_t ___MaxSimultaneousObjectTargets_3;
	// System.Boolean Vuforia.VuforiaARController::UseDelayedLoadingObjectTargets
	bool ___UseDelayedLoadingObjectTargets_4;
	// Vuforia.CameraDevice/CameraDirection Vuforia.VuforiaARController::CameraDirection
	int32_t ___CameraDirection_5;
	// Vuforia.VuforiaRenderer/VideoBackgroundReflection Vuforia.VuforiaARController::MirrorVideoBackground
	int32_t ___MirrorVideoBackground_6;
	// Vuforia.VuforiaARController/WorldCenterMode Vuforia.VuforiaARController::mWorldCenterMode
	int32_t ___mWorldCenterMode_7;
	// Vuforia.TrackableBehaviour Vuforia.VuforiaARController::mWorldCenter
	TrackableBehaviour_t2419079356 * ___mWorldCenter_8;
	// System.Collections.Generic.List`1<Vuforia.ITrackerEventHandler> Vuforia.VuforiaARController::mTrackerEventHandlers
	List_1_t1947635164 * ___mTrackerEventHandlers_9;
	// System.Collections.Generic.List`1<Vuforia.IVideoBackgroundEventHandler> Vuforia.VuforiaARController::mVideoBgEventHandlers
	List_1_t485517045 * ___mVideoBgEventHandlers_10;
	// System.Action Vuforia.VuforiaARController::mOnVuforiaInitialized
	Action_t3619184611 * ___mOnVuforiaInitialized_11;
	// System.Action Vuforia.VuforiaARController::mOnVuforiaStarted
	Action_t3619184611 * ___mOnVuforiaStarted_12;
	// System.Action Vuforia.VuforiaARController::mOnVuforiaDeinitialized
	Action_t3619184611 * ___mOnVuforiaDeinitialized_13;
	// System.Action Vuforia.VuforiaARController::mOnTrackablesUpdated
	Action_t3619184611 * ___mOnTrackablesUpdated_14;
	// System.Action Vuforia.VuforiaARController::mRenderOnUpdate
	Action_t3619184611 * ___mRenderOnUpdate_15;
	// System.Action`1<System.Boolean> Vuforia.VuforiaARController::mOnPause
	Action_1_t2936672411 * ___mOnPause_16;
	// System.Boolean Vuforia.VuforiaARController::mPaused
	bool ___mPaused_17;
	// System.Action Vuforia.VuforiaARController::mOnBackgroundTextureChanged
	Action_t3619184611 * ___mOnBackgroundTextureChanged_18;
	// System.Boolean Vuforia.VuforiaARController::mStartHasBeenInvoked
	bool ___mStartHasBeenInvoked_19;
	// System.Boolean Vuforia.VuforiaARController::mHasStarted
	bool ___mHasStarted_20;
	// System.Boolean Vuforia.VuforiaARController::mBackgroundTextureHasChanged
	bool ___mBackgroundTextureHasChanged_21;
	// Vuforia.ICameraConfiguration Vuforia.VuforiaARController::mCameraConfiguration
	RuntimeObject* ___mCameraConfiguration_22;
	// Vuforia.DigitalEyewearARController Vuforia.VuforiaARController::mEyewearBehaviour
	DigitalEyewearARController_t1877756253 * ___mEyewearBehaviour_23;
	// Vuforia.VideoBackgroundManager Vuforia.VuforiaARController::mVideoBackgroundMgr
	VideoBackgroundManager_t2068552549 * ___mVideoBackgroundMgr_24;
	// System.Boolean Vuforia.VuforiaARController::mCheckStopCamera
	bool ___mCheckStopCamera_25;
	// UnityEngine.Material Vuforia.VuforiaARController::mClearMaterial
	Material_t1079520667 * ___mClearMaterial_26;
	// System.Boolean Vuforia.VuforiaARController::mMetalRendering
	bool ___mMetalRendering_27;
	// System.Boolean Vuforia.VuforiaARController::mHasStartedOnce
	bool ___mHasStartedOnce_28;
	// System.Boolean Vuforia.VuforiaARController::mWasEnabledBeforePause
	bool ___mWasEnabledBeforePause_29;
	// System.Boolean Vuforia.VuforiaARController::mObjectTrackerWasActiveBeforePause
	bool ___mObjectTrackerWasActiveBeforePause_30;
	// System.Boolean Vuforia.VuforiaARController::mObjectTrackerWasActiveBeforeDisabling
	bool ___mObjectTrackerWasActiveBeforeDisabling_31;
	// System.Int32 Vuforia.VuforiaARController::mLastUpdatedFrame
	int32_t ___mLastUpdatedFrame_32;
	// System.Collections.Generic.List`1<System.Type> Vuforia.VuforiaARController::mTrackersRequestedToDeinit
	List_1_t3593106846 * ___mTrackersRequestedToDeinit_33;
	// System.Boolean Vuforia.VuforiaARController::mMissedToApplyLeftProjectionMatrix
	bool ___mMissedToApplyLeftProjectionMatrix_34;
	// System.Boolean Vuforia.VuforiaARController::mMissedToApplyRightProjectionMatrix
	bool ___mMissedToApplyRightProjectionMatrix_35;
	// UnityEngine.Matrix4x4 Vuforia.VuforiaARController::mLeftProjectMatrixToApply
	Matrix4x4_t4266809202  ___mLeftProjectMatrixToApply_36;
	// UnityEngine.Matrix4x4 Vuforia.VuforiaARController::mRightProjectMatrixToApply
	Matrix4x4_t4266809202  ___mRightProjectMatrixToApply_37;

public:
	inline static int32_t get_offset_of_CameraDeviceModeSetting_1() { return static_cast<int32_t>(offsetof(VuforiaARController_t1328503142, ___CameraDeviceModeSetting_1)); }
	inline int32_t get_CameraDeviceModeSetting_1() const { return ___CameraDeviceModeSetting_1; }
	inline int32_t* get_address_of_CameraDeviceModeSetting_1() { return &___CameraDeviceModeSetting_1; }
	inline void set_CameraDeviceModeSetting_1(int32_t value)
	{
		___CameraDeviceModeSetting_1 = value;
	}

	inline static int32_t get_offset_of_MaxSimultaneousImageTargets_2() { return static_cast<int32_t>(offsetof(VuforiaARController_t1328503142, ___MaxSimultaneousImageTargets_2)); }
	inline int32_t get_MaxSimultaneousImageTargets_2() const { return ___MaxSimultaneousImageTargets_2; }
	inline int32_t* get_address_of_MaxSimultaneousImageTargets_2() { return &___MaxSimultaneousImageTargets_2; }
	inline void set_MaxSimultaneousImageTargets_2(int32_t value)
	{
		___MaxSimultaneousImageTargets_2 = value;
	}

	inline static int32_t get_offset_of_MaxSimultaneousObjectTargets_3() { return static_cast<int32_t>(offsetof(VuforiaARController_t1328503142, ___MaxSimultaneousObjectTargets_3)); }
	inline int32_t get_MaxSimultaneousObjectTargets_3() const { return ___MaxSimultaneousObjectTargets_3; }
	inline int32_t* get_address_of_MaxSimultaneousObjectTargets_3() { return &___MaxSimultaneousObjectTargets_3; }
	inline void set_MaxSimultaneousObjectTargets_3(int32_t value)
	{
		___MaxSimultaneousObjectTargets_3 = value;
	}

	inline static int32_t get_offset_of_UseDelayedLoadingObjectTargets_4() { return static_cast<int32_t>(offsetof(VuforiaARController_t1328503142, ___UseDelayedLoadingObjectTargets_4)); }
	inline bool get_UseDelayedLoadingObjectTargets_4() const { return ___UseDelayedLoadingObjectTargets_4; }
	inline bool* get_address_of_UseDelayedLoadingObjectTargets_4() { return &___UseDelayedLoadingObjectTargets_4; }
	inline void set_UseDelayedLoadingObjectTargets_4(bool value)
	{
		___UseDelayedLoadingObjectTargets_4 = value;
	}

	inline static int32_t get_offset_of_CameraDirection_5() { return static_cast<int32_t>(offsetof(VuforiaARController_t1328503142, ___CameraDirection_5)); }
	inline int32_t get_CameraDirection_5() const { return ___CameraDirection_5; }
	inline int32_t* get_address_of_CameraDirection_5() { return &___CameraDirection_5; }
	inline void set_CameraDirection_5(int32_t value)
	{
		___CameraDirection_5 = value;
	}

	inline static int32_t get_offset_of_MirrorVideoBackground_6() { return static_cast<int32_t>(offsetof(VuforiaARController_t1328503142, ___MirrorVideoBackground_6)); }
	inline int32_t get_MirrorVideoBackground_6() const { return ___MirrorVideoBackground_6; }
	inline int32_t* get_address_of_MirrorVideoBackground_6() { return &___MirrorVideoBackground_6; }
	inline void set_MirrorVideoBackground_6(int32_t value)
	{
		___MirrorVideoBackground_6 = value;
	}

	inline static int32_t get_offset_of_mWorldCenterMode_7() { return static_cast<int32_t>(offsetof(VuforiaARController_t1328503142, ___mWorldCenterMode_7)); }
	inline int32_t get_mWorldCenterMode_7() const { return ___mWorldCenterMode_7; }
	inline int32_t* get_address_of_mWorldCenterMode_7() { return &___mWorldCenterMode_7; }
	inline void set_mWorldCenterMode_7(int32_t value)
	{
		___mWorldCenterMode_7 = value;
	}

	inline static int32_t get_offset_of_mWorldCenter_8() { return static_cast<int32_t>(offsetof(VuforiaARController_t1328503142, ___mWorldCenter_8)); }
	inline TrackableBehaviour_t2419079356 * get_mWorldCenter_8() const { return ___mWorldCenter_8; }
	inline TrackableBehaviour_t2419079356 ** get_address_of_mWorldCenter_8() { return &___mWorldCenter_8; }
	inline void set_mWorldCenter_8(TrackableBehaviour_t2419079356 * value)
	{
		___mWorldCenter_8 = value;
		Il2CppCodeGenWriteBarrier((&___mWorldCenter_8), value);
	}

	inline static int32_t get_offset_of_mTrackerEventHandlers_9() { return static_cast<int32_t>(offsetof(VuforiaARController_t1328503142, ___mTrackerEventHandlers_9)); }
	inline List_1_t1947635164 * get_mTrackerEventHandlers_9() const { return ___mTrackerEventHandlers_9; }
	inline List_1_t1947635164 ** get_address_of_mTrackerEventHandlers_9() { return &___mTrackerEventHandlers_9; }
	inline void set_mTrackerEventHandlers_9(List_1_t1947635164 * value)
	{
		___mTrackerEventHandlers_9 = value;
		Il2CppCodeGenWriteBarrier((&___mTrackerEventHandlers_9), value);
	}

	inline static int32_t get_offset_of_mVideoBgEventHandlers_10() { return static_cast<int32_t>(offsetof(VuforiaARController_t1328503142, ___mVideoBgEventHandlers_10)); }
	inline List_1_t485517045 * get_mVideoBgEventHandlers_10() const { return ___mVideoBgEventHandlers_10; }
	inline List_1_t485517045 ** get_address_of_mVideoBgEventHandlers_10() { return &___mVideoBgEventHandlers_10; }
	inline void set_mVideoBgEventHandlers_10(List_1_t485517045 * value)
	{
		___mVideoBgEventHandlers_10 = value;
		Il2CppCodeGenWriteBarrier((&___mVideoBgEventHandlers_10), value);
	}

	inline static int32_t get_offset_of_mOnVuforiaInitialized_11() { return static_cast<int32_t>(offsetof(VuforiaARController_t1328503142, ___mOnVuforiaInitialized_11)); }
	inline Action_t3619184611 * get_mOnVuforiaInitialized_11() const { return ___mOnVuforiaInitialized_11; }
	inline Action_t3619184611 ** get_address_of_mOnVuforiaInitialized_11() { return &___mOnVuforiaInitialized_11; }
	inline void set_mOnVuforiaInitialized_11(Action_t3619184611 * value)
	{
		___mOnVuforiaInitialized_11 = value;
		Il2CppCodeGenWriteBarrier((&___mOnVuforiaInitialized_11), value);
	}

	inline static int32_t get_offset_of_mOnVuforiaStarted_12() { return static_cast<int32_t>(offsetof(VuforiaARController_t1328503142, ___mOnVuforiaStarted_12)); }
	inline Action_t3619184611 * get_mOnVuforiaStarted_12() const { return ___mOnVuforiaStarted_12; }
	inline Action_t3619184611 ** get_address_of_mOnVuforiaStarted_12() { return &___mOnVuforiaStarted_12; }
	inline void set_mOnVuforiaStarted_12(Action_t3619184611 * value)
	{
		___mOnVuforiaStarted_12 = value;
		Il2CppCodeGenWriteBarrier((&___mOnVuforiaStarted_12), value);
	}

	inline static int32_t get_offset_of_mOnVuforiaDeinitialized_13() { return static_cast<int32_t>(offsetof(VuforiaARController_t1328503142, ___mOnVuforiaDeinitialized_13)); }
	inline Action_t3619184611 * get_mOnVuforiaDeinitialized_13() const { return ___mOnVuforiaDeinitialized_13; }
	inline Action_t3619184611 ** get_address_of_mOnVuforiaDeinitialized_13() { return &___mOnVuforiaDeinitialized_13; }
	inline void set_mOnVuforiaDeinitialized_13(Action_t3619184611 * value)
	{
		___mOnVuforiaDeinitialized_13 = value;
		Il2CppCodeGenWriteBarrier((&___mOnVuforiaDeinitialized_13), value);
	}

	inline static int32_t get_offset_of_mOnTrackablesUpdated_14() { return static_cast<int32_t>(offsetof(VuforiaARController_t1328503142, ___mOnTrackablesUpdated_14)); }
	inline Action_t3619184611 * get_mOnTrackablesUpdated_14() const { return ___mOnTrackablesUpdated_14; }
	inline Action_t3619184611 ** get_address_of_mOnTrackablesUpdated_14() { return &___mOnTrackablesUpdated_14; }
	inline void set_mOnTrackablesUpdated_14(Action_t3619184611 * value)
	{
		___mOnTrackablesUpdated_14 = value;
		Il2CppCodeGenWriteBarrier((&___mOnTrackablesUpdated_14), value);
	}

	inline static int32_t get_offset_of_mRenderOnUpdate_15() { return static_cast<int32_t>(offsetof(VuforiaARController_t1328503142, ___mRenderOnUpdate_15)); }
	inline Action_t3619184611 * get_mRenderOnUpdate_15() const { return ___mRenderOnUpdate_15; }
	inline Action_t3619184611 ** get_address_of_mRenderOnUpdate_15() { return &___mRenderOnUpdate_15; }
	inline void set_mRenderOnUpdate_15(Action_t3619184611 * value)
	{
		___mRenderOnUpdate_15 = value;
		Il2CppCodeGenWriteBarrier((&___mRenderOnUpdate_15), value);
	}

	inline static int32_t get_offset_of_mOnPause_16() { return static_cast<int32_t>(offsetof(VuforiaARController_t1328503142, ___mOnPause_16)); }
	inline Action_1_t2936672411 * get_mOnPause_16() const { return ___mOnPause_16; }
	inline Action_1_t2936672411 ** get_address_of_mOnPause_16() { return &___mOnPause_16; }
	inline void set_mOnPause_16(Action_1_t2936672411 * value)
	{
		___mOnPause_16 = value;
		Il2CppCodeGenWriteBarrier((&___mOnPause_16), value);
	}

	inline static int32_t get_offset_of_mPaused_17() { return static_cast<int32_t>(offsetof(VuforiaARController_t1328503142, ___mPaused_17)); }
	inline bool get_mPaused_17() const { return ___mPaused_17; }
	inline bool* get_address_of_mPaused_17() { return &___mPaused_17; }
	inline void set_mPaused_17(bool value)
	{
		___mPaused_17 = value;
	}

	inline static int32_t get_offset_of_mOnBackgroundTextureChanged_18() { return static_cast<int32_t>(offsetof(VuforiaARController_t1328503142, ___mOnBackgroundTextureChanged_18)); }
	inline Action_t3619184611 * get_mOnBackgroundTextureChanged_18() const { return ___mOnBackgroundTextureChanged_18; }
	inline Action_t3619184611 ** get_address_of_mOnBackgroundTextureChanged_18() { return &___mOnBackgroundTextureChanged_18; }
	inline void set_mOnBackgroundTextureChanged_18(Action_t3619184611 * value)
	{
		___mOnBackgroundTextureChanged_18 = value;
		Il2CppCodeGenWriteBarrier((&___mOnBackgroundTextureChanged_18), value);
	}

	inline static int32_t get_offset_of_mStartHasBeenInvoked_19() { return static_cast<int32_t>(offsetof(VuforiaARController_t1328503142, ___mStartHasBeenInvoked_19)); }
	inline bool get_mStartHasBeenInvoked_19() const { return ___mStartHasBeenInvoked_19; }
	inline bool* get_address_of_mStartHasBeenInvoked_19() { return &___mStartHasBeenInvoked_19; }
	inline void set_mStartHasBeenInvoked_19(bool value)
	{
		___mStartHasBeenInvoked_19 = value;
	}

	inline static int32_t get_offset_of_mHasStarted_20() { return static_cast<int32_t>(offsetof(VuforiaARController_t1328503142, ___mHasStarted_20)); }
	inline bool get_mHasStarted_20() const { return ___mHasStarted_20; }
	inline bool* get_address_of_mHasStarted_20() { return &___mHasStarted_20; }
	inline void set_mHasStarted_20(bool value)
	{
		___mHasStarted_20 = value;
	}

	inline static int32_t get_offset_of_mBackgroundTextureHasChanged_21() { return static_cast<int32_t>(offsetof(VuforiaARController_t1328503142, ___mBackgroundTextureHasChanged_21)); }
	inline bool get_mBackgroundTextureHasChanged_21() const { return ___mBackgroundTextureHasChanged_21; }
	inline bool* get_address_of_mBackgroundTextureHasChanged_21() { return &___mBackgroundTextureHasChanged_21; }
	inline void set_mBackgroundTextureHasChanged_21(bool value)
	{
		___mBackgroundTextureHasChanged_21 = value;
	}

	inline static int32_t get_offset_of_mCameraConfiguration_22() { return static_cast<int32_t>(offsetof(VuforiaARController_t1328503142, ___mCameraConfiguration_22)); }
	inline RuntimeObject* get_mCameraConfiguration_22() const { return ___mCameraConfiguration_22; }
	inline RuntimeObject** get_address_of_mCameraConfiguration_22() { return &___mCameraConfiguration_22; }
	inline void set_mCameraConfiguration_22(RuntimeObject* value)
	{
		___mCameraConfiguration_22 = value;
		Il2CppCodeGenWriteBarrier((&___mCameraConfiguration_22), value);
	}

	inline static int32_t get_offset_of_mEyewearBehaviour_23() { return static_cast<int32_t>(offsetof(VuforiaARController_t1328503142, ___mEyewearBehaviour_23)); }
	inline DigitalEyewearARController_t1877756253 * get_mEyewearBehaviour_23() const { return ___mEyewearBehaviour_23; }
	inline DigitalEyewearARController_t1877756253 ** get_address_of_mEyewearBehaviour_23() { return &___mEyewearBehaviour_23; }
	inline void set_mEyewearBehaviour_23(DigitalEyewearARController_t1877756253 * value)
	{
		___mEyewearBehaviour_23 = value;
		Il2CppCodeGenWriteBarrier((&___mEyewearBehaviour_23), value);
	}

	inline static int32_t get_offset_of_mVideoBackgroundMgr_24() { return static_cast<int32_t>(offsetof(VuforiaARController_t1328503142, ___mVideoBackgroundMgr_24)); }
	inline VideoBackgroundManager_t2068552549 * get_mVideoBackgroundMgr_24() const { return ___mVideoBackgroundMgr_24; }
	inline VideoBackgroundManager_t2068552549 ** get_address_of_mVideoBackgroundMgr_24() { return &___mVideoBackgroundMgr_24; }
	inline void set_mVideoBackgroundMgr_24(VideoBackgroundManager_t2068552549 * value)
	{
		___mVideoBackgroundMgr_24 = value;
		Il2CppCodeGenWriteBarrier((&___mVideoBackgroundMgr_24), value);
	}

	inline static int32_t get_offset_of_mCheckStopCamera_25() { return static_cast<int32_t>(offsetof(VuforiaARController_t1328503142, ___mCheckStopCamera_25)); }
	inline bool get_mCheckStopCamera_25() const { return ___mCheckStopCamera_25; }
	inline bool* get_address_of_mCheckStopCamera_25() { return &___mCheckStopCamera_25; }
	inline void set_mCheckStopCamera_25(bool value)
	{
		___mCheckStopCamera_25 = value;
	}

	inline static int32_t get_offset_of_mClearMaterial_26() { return static_cast<int32_t>(offsetof(VuforiaARController_t1328503142, ___mClearMaterial_26)); }
	inline Material_t1079520667 * get_mClearMaterial_26() const { return ___mClearMaterial_26; }
	inline Material_t1079520667 ** get_address_of_mClearMaterial_26() { return &___mClearMaterial_26; }
	inline void set_mClearMaterial_26(Material_t1079520667 * value)
	{
		___mClearMaterial_26 = value;
		Il2CppCodeGenWriteBarrier((&___mClearMaterial_26), value);
	}

	inline static int32_t get_offset_of_mMetalRendering_27() { return static_cast<int32_t>(offsetof(VuforiaARController_t1328503142, ___mMetalRendering_27)); }
	inline bool get_mMetalRendering_27() const { return ___mMetalRendering_27; }
	inline bool* get_address_of_mMetalRendering_27() { return &___mMetalRendering_27; }
	inline void set_mMetalRendering_27(bool value)
	{
		___mMetalRendering_27 = value;
	}

	inline static int32_t get_offset_of_mHasStartedOnce_28() { return static_cast<int32_t>(offsetof(VuforiaARController_t1328503142, ___mHasStartedOnce_28)); }
	inline bool get_mHasStartedOnce_28() const { return ___mHasStartedOnce_28; }
	inline bool* get_address_of_mHasStartedOnce_28() { return &___mHasStartedOnce_28; }
	inline void set_mHasStartedOnce_28(bool value)
	{
		___mHasStartedOnce_28 = value;
	}

	inline static int32_t get_offset_of_mWasEnabledBeforePause_29() { return static_cast<int32_t>(offsetof(VuforiaARController_t1328503142, ___mWasEnabledBeforePause_29)); }
	inline bool get_mWasEnabledBeforePause_29() const { return ___mWasEnabledBeforePause_29; }
	inline bool* get_address_of_mWasEnabledBeforePause_29() { return &___mWasEnabledBeforePause_29; }
	inline void set_mWasEnabledBeforePause_29(bool value)
	{
		___mWasEnabledBeforePause_29 = value;
	}

	inline static int32_t get_offset_of_mObjectTrackerWasActiveBeforePause_30() { return static_cast<int32_t>(offsetof(VuforiaARController_t1328503142, ___mObjectTrackerWasActiveBeforePause_30)); }
	inline bool get_mObjectTrackerWasActiveBeforePause_30() const { return ___mObjectTrackerWasActiveBeforePause_30; }
	inline bool* get_address_of_mObjectTrackerWasActiveBeforePause_30() { return &___mObjectTrackerWasActiveBeforePause_30; }
	inline void set_mObjectTrackerWasActiveBeforePause_30(bool value)
	{
		___mObjectTrackerWasActiveBeforePause_30 = value;
	}

	inline static int32_t get_offset_of_mObjectTrackerWasActiveBeforeDisabling_31() { return static_cast<int32_t>(offsetof(VuforiaARController_t1328503142, ___mObjectTrackerWasActiveBeforeDisabling_31)); }
	inline bool get_mObjectTrackerWasActiveBeforeDisabling_31() const { return ___mObjectTrackerWasActiveBeforeDisabling_31; }
	inline bool* get_address_of_mObjectTrackerWasActiveBeforeDisabling_31() { return &___mObjectTrackerWasActiveBeforeDisabling_31; }
	inline void set_mObjectTrackerWasActiveBeforeDisabling_31(bool value)
	{
		___mObjectTrackerWasActiveBeforeDisabling_31 = value;
	}

	inline static int32_t get_offset_of_mLastUpdatedFrame_32() { return static_cast<int32_t>(offsetof(VuforiaARController_t1328503142, ___mLastUpdatedFrame_32)); }
	inline int32_t get_mLastUpdatedFrame_32() const { return ___mLastUpdatedFrame_32; }
	inline int32_t* get_address_of_mLastUpdatedFrame_32() { return &___mLastUpdatedFrame_32; }
	inline void set_mLastUpdatedFrame_32(int32_t value)
	{
		___mLastUpdatedFrame_32 = value;
	}

	inline static int32_t get_offset_of_mTrackersRequestedToDeinit_33() { return static_cast<int32_t>(offsetof(VuforiaARController_t1328503142, ___mTrackersRequestedToDeinit_33)); }
	inline List_1_t3593106846 * get_mTrackersRequestedToDeinit_33() const { return ___mTrackersRequestedToDeinit_33; }
	inline List_1_t3593106846 ** get_address_of_mTrackersRequestedToDeinit_33() { return &___mTrackersRequestedToDeinit_33; }
	inline void set_mTrackersRequestedToDeinit_33(List_1_t3593106846 * value)
	{
		___mTrackersRequestedToDeinit_33 = value;
		Il2CppCodeGenWriteBarrier((&___mTrackersRequestedToDeinit_33), value);
	}

	inline static int32_t get_offset_of_mMissedToApplyLeftProjectionMatrix_34() { return static_cast<int32_t>(offsetof(VuforiaARController_t1328503142, ___mMissedToApplyLeftProjectionMatrix_34)); }
	inline bool get_mMissedToApplyLeftProjectionMatrix_34() const { return ___mMissedToApplyLeftProjectionMatrix_34; }
	inline bool* get_address_of_mMissedToApplyLeftProjectionMatrix_34() { return &___mMissedToApplyLeftProjectionMatrix_34; }
	inline void set_mMissedToApplyLeftProjectionMatrix_34(bool value)
	{
		___mMissedToApplyLeftProjectionMatrix_34 = value;
	}

	inline static int32_t get_offset_of_mMissedToApplyRightProjectionMatrix_35() { return static_cast<int32_t>(offsetof(VuforiaARController_t1328503142, ___mMissedToApplyRightProjectionMatrix_35)); }
	inline bool get_mMissedToApplyRightProjectionMatrix_35() const { return ___mMissedToApplyRightProjectionMatrix_35; }
	inline bool* get_address_of_mMissedToApplyRightProjectionMatrix_35() { return &___mMissedToApplyRightProjectionMatrix_35; }
	inline void set_mMissedToApplyRightProjectionMatrix_35(bool value)
	{
		___mMissedToApplyRightProjectionMatrix_35 = value;
	}

	inline static int32_t get_offset_of_mLeftProjectMatrixToApply_36() { return static_cast<int32_t>(offsetof(VuforiaARController_t1328503142, ___mLeftProjectMatrixToApply_36)); }
	inline Matrix4x4_t4266809202  get_mLeftProjectMatrixToApply_36() const { return ___mLeftProjectMatrixToApply_36; }
	inline Matrix4x4_t4266809202 * get_address_of_mLeftProjectMatrixToApply_36() { return &___mLeftProjectMatrixToApply_36; }
	inline void set_mLeftProjectMatrixToApply_36(Matrix4x4_t4266809202  value)
	{
		___mLeftProjectMatrixToApply_36 = value;
	}

	inline static int32_t get_offset_of_mRightProjectMatrixToApply_37() { return static_cast<int32_t>(offsetof(VuforiaARController_t1328503142, ___mRightProjectMatrixToApply_37)); }
	inline Matrix4x4_t4266809202  get_mRightProjectMatrixToApply_37() const { return ___mRightProjectMatrixToApply_37; }
	inline Matrix4x4_t4266809202 * get_address_of_mRightProjectMatrixToApply_37() { return &___mRightProjectMatrixToApply_37; }
	inline void set_mRightProjectMatrixToApply_37(Matrix4x4_t4266809202  value)
	{
		___mRightProjectMatrixToApply_37 = value;
	}
};

struct VuforiaARController_t1328503142_StaticFields
{
public:
	// Vuforia.VuforiaARController Vuforia.VuforiaARController::mInstance
	VuforiaARController_t1328503142 * ___mInstance_38;
	// System.Object Vuforia.VuforiaARController::mPadlock
	RuntimeObject * ___mPadlock_39;

public:
	inline static int32_t get_offset_of_mInstance_38() { return static_cast<int32_t>(offsetof(VuforiaARController_t1328503142_StaticFields, ___mInstance_38)); }
	inline VuforiaARController_t1328503142 * get_mInstance_38() const { return ___mInstance_38; }
	inline VuforiaARController_t1328503142 ** get_address_of_mInstance_38() { return &___mInstance_38; }
	inline void set_mInstance_38(VuforiaARController_t1328503142 * value)
	{
		___mInstance_38 = value;
		Il2CppCodeGenWriteBarrier((&___mInstance_38), value);
	}

	inline static int32_t get_offset_of_mPadlock_39() { return static_cast<int32_t>(offsetof(VuforiaARController_t1328503142_StaticFields, ___mPadlock_39)); }
	inline RuntimeObject * get_mPadlock_39() const { return ___mPadlock_39; }
	inline RuntimeObject ** get_address_of_mPadlock_39() { return &___mPadlock_39; }
	inline void set_mPadlock_39(RuntimeObject * value)
	{
		___mPadlock_39 = value;
		Il2CppCodeGenWriteBarrier((&___mPadlock_39), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VUFORIAARCONTROLLER_T1328503142_H
#ifndef BEHAVIOUR_T363748010_H
#define BEHAVIOUR_T363748010_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_t363748010  : public Component_t4087199522
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_T363748010_H
#ifndef WEBCAMPROFILE_T3641218551_H
#define WEBCAMPROFILE_T3641218551_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.WebCamProfile
struct  WebCamProfile_t3641218551  : public RuntimeObject
{
public:
	// Vuforia.WebCamProfile/ProfileCollection Vuforia.WebCamProfile::mProfileCollection
	ProfileCollection_t3131405081  ___mProfileCollection_0;

public:
	inline static int32_t get_offset_of_mProfileCollection_0() { return static_cast<int32_t>(offsetof(WebCamProfile_t3641218551, ___mProfileCollection_0)); }
	inline ProfileCollection_t3131405081  get_mProfileCollection_0() const { return ___mProfileCollection_0; }
	inline ProfileCollection_t3131405081 * get_address_of_mProfileCollection_0() { return &___mProfileCollection_0; }
	inline void set_mProfileCollection_0(ProfileCollection_t3131405081  value)
	{
		___mProfileCollection_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WEBCAMPROFILE_T3641218551_H
#ifndef VIRTUALBUTTONIMPL_T2261432068_H
#define VIRTUALBUTTONIMPL_T2261432068_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.VirtualButtonImpl
struct  VirtualButtonImpl_t2261432068  : public VirtualButton_t3666937711
{
public:
	// System.String Vuforia.VirtualButtonImpl::mName
	String_t* ___mName_1;
	// System.Int32 Vuforia.VirtualButtonImpl::mID
	int32_t ___mID_2;
	// Vuforia.RectangleData Vuforia.VirtualButtonImpl::mArea
	RectangleData_t1094133618  ___mArea_3;
	// System.Boolean Vuforia.VirtualButtonImpl::mIsEnabled
	bool ___mIsEnabled_4;
	// Vuforia.ImageTarget Vuforia.VirtualButtonImpl::mParentImageTarget
	RuntimeObject* ___mParentImageTarget_5;
	// Vuforia.DataSetImpl Vuforia.VirtualButtonImpl::mParentDataSet
	DataSetImpl_t223764247 * ___mParentDataSet_6;

public:
	inline static int32_t get_offset_of_mName_1() { return static_cast<int32_t>(offsetof(VirtualButtonImpl_t2261432068, ___mName_1)); }
	inline String_t* get_mName_1() const { return ___mName_1; }
	inline String_t** get_address_of_mName_1() { return &___mName_1; }
	inline void set_mName_1(String_t* value)
	{
		___mName_1 = value;
		Il2CppCodeGenWriteBarrier((&___mName_1), value);
	}

	inline static int32_t get_offset_of_mID_2() { return static_cast<int32_t>(offsetof(VirtualButtonImpl_t2261432068, ___mID_2)); }
	inline int32_t get_mID_2() const { return ___mID_2; }
	inline int32_t* get_address_of_mID_2() { return &___mID_2; }
	inline void set_mID_2(int32_t value)
	{
		___mID_2 = value;
	}

	inline static int32_t get_offset_of_mArea_3() { return static_cast<int32_t>(offsetof(VirtualButtonImpl_t2261432068, ___mArea_3)); }
	inline RectangleData_t1094133618  get_mArea_3() const { return ___mArea_3; }
	inline RectangleData_t1094133618 * get_address_of_mArea_3() { return &___mArea_3; }
	inline void set_mArea_3(RectangleData_t1094133618  value)
	{
		___mArea_3 = value;
	}

	inline static int32_t get_offset_of_mIsEnabled_4() { return static_cast<int32_t>(offsetof(VirtualButtonImpl_t2261432068, ___mIsEnabled_4)); }
	inline bool get_mIsEnabled_4() const { return ___mIsEnabled_4; }
	inline bool* get_address_of_mIsEnabled_4() { return &___mIsEnabled_4; }
	inline void set_mIsEnabled_4(bool value)
	{
		___mIsEnabled_4 = value;
	}

	inline static int32_t get_offset_of_mParentImageTarget_5() { return static_cast<int32_t>(offsetof(VirtualButtonImpl_t2261432068, ___mParentImageTarget_5)); }
	inline RuntimeObject* get_mParentImageTarget_5() const { return ___mParentImageTarget_5; }
	inline RuntimeObject** get_address_of_mParentImageTarget_5() { return &___mParentImageTarget_5; }
	inline void set_mParentImageTarget_5(RuntimeObject* value)
	{
		___mParentImageTarget_5 = value;
		Il2CppCodeGenWriteBarrier((&___mParentImageTarget_5), value);
	}

	inline static int32_t get_offset_of_mParentDataSet_6() { return static_cast<int32_t>(offsetof(VirtualButtonImpl_t2261432068, ___mParentDataSet_6)); }
	inline DataSetImpl_t223764247 * get_mParentDataSet_6() const { return ___mParentDataSet_6; }
	inline DataSetImpl_t223764247 ** get_address_of_mParentDataSet_6() { return &___mParentDataSet_6; }
	inline void set_mParentDataSet_6(DataSetImpl_t223764247 * value)
	{
		___mParentDataSet_6 = value;
		Il2CppCodeGenWriteBarrier((&___mParentDataSet_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VIRTUALBUTTONIMPL_T2261432068_H
#ifndef MONOBEHAVIOUR_T345688271_H
#define MONOBEHAVIOUR_T345688271_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t345688271  : public Behaviour_t363748010
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T345688271_H
#ifndef RECONSTRUCTIONABSTRACTBEHAVIOUR_T954468633_H
#define RECONSTRUCTIONABSTRACTBEHAVIOUR_T954468633_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.ReconstructionAbstractBehaviour
struct  ReconstructionAbstractBehaviour_t954468633  : public MonoBehaviour_t345688271
{
public:
	// System.Boolean Vuforia.ReconstructionAbstractBehaviour::mHasInitialized
	bool ___mHasInitialized_2;
	// System.Collections.Generic.List`1<Vuforia.ISmartTerrainEventHandler> Vuforia.ReconstructionAbstractBehaviour::mSmartTerrainEventHandlers
	List_1_t3446838688 * ___mSmartTerrainEventHandlers_3;
	// System.Action`1<Vuforia.SmartTerrainInitializationInfo> Vuforia.ReconstructionAbstractBehaviour::mOnInitialized
	Action_1_t1488611326 * ___mOnInitialized_4;
	// System.Action`1<Vuforia.Prop> Vuforia.ReconstructionAbstractBehaviour::mOnPropCreated
	Action_1_t1560204979 * ___mOnPropCreated_5;
	// System.Action`1<Vuforia.Prop> Vuforia.ReconstructionAbstractBehaviour::mOnPropUpdated
	Action_1_t1560204979 * ___mOnPropUpdated_6;
	// System.Action`1<Vuforia.Prop> Vuforia.ReconstructionAbstractBehaviour::mOnPropDeleted
	Action_1_t1560204979 * ___mOnPropDeleted_7;
	// System.Action`1<Vuforia.Surface> Vuforia.ReconstructionAbstractBehaviour::mOnSurfaceCreated
	Action_1_t1020068010 * ___mOnSurfaceCreated_8;
	// System.Action`1<Vuforia.Surface> Vuforia.ReconstructionAbstractBehaviour::mOnSurfaceUpdated
	Action_1_t1020068010 * ___mOnSurfaceUpdated_9;
	// System.Action`1<Vuforia.Surface> Vuforia.ReconstructionAbstractBehaviour::mOnSurfaceDeleted
	Action_1_t1020068010 * ___mOnSurfaceDeleted_10;
	// System.Boolean Vuforia.ReconstructionAbstractBehaviour::mInitializedInEditor
	bool ___mInitializedInEditor_11;
	// System.Boolean Vuforia.ReconstructionAbstractBehaviour::mMaximumExtentEnabled
	bool ___mMaximumExtentEnabled_12;
	// UnityEngine.Rect Vuforia.ReconstructionAbstractBehaviour::mMaximumExtent
	Rect_t3436776195  ___mMaximumExtent_13;
	// System.Boolean Vuforia.ReconstructionAbstractBehaviour::mAutomaticStart
	bool ___mAutomaticStart_14;
	// System.Boolean Vuforia.ReconstructionAbstractBehaviour::mNavMeshUpdates
	bool ___mNavMeshUpdates_15;
	// System.Single Vuforia.ReconstructionAbstractBehaviour::mNavMeshPadding
	float ___mNavMeshPadding_16;
	// Vuforia.Reconstruction Vuforia.ReconstructionAbstractBehaviour::mReconstruction
	RuntimeObject* ___mReconstruction_17;
	// System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.Surface> Vuforia.ReconstructionAbstractBehaviour::mSurfaces
	Dictionary_2_t2230247832 * ___mSurfaces_18;
	// System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.SurfaceAbstractBehaviour> Vuforia.ReconstructionAbstractBehaviour::mActiveSurfaceBehaviours
	Dictionary_2_t1468548340 * ___mActiveSurfaceBehaviours_19;
	// System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.Prop> Vuforia.ReconstructionAbstractBehaviour::mProps
	Dictionary_2_t2770384801 * ___mProps_20;
	// System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.PropAbstractBehaviour> Vuforia.ReconstructionAbstractBehaviour::mActivePropBehaviours
	Dictionary_2_t3844889330 * ___mActivePropBehaviours_21;
	// Vuforia.SurfaceAbstractBehaviour Vuforia.ReconstructionAbstractBehaviour::mPreviouslySetWorldCenterSurfaceTemplate
	SurfaceAbstractBehaviour_t272541176 * ___mPreviouslySetWorldCenterSurfaceTemplate_22;
	// System.Boolean Vuforia.ReconstructionAbstractBehaviour::mIgnoreNextUpdate
	bool ___mIgnoreNextUpdate_23;

public:
	inline static int32_t get_offset_of_mHasInitialized_2() { return static_cast<int32_t>(offsetof(ReconstructionAbstractBehaviour_t954468633, ___mHasInitialized_2)); }
	inline bool get_mHasInitialized_2() const { return ___mHasInitialized_2; }
	inline bool* get_address_of_mHasInitialized_2() { return &___mHasInitialized_2; }
	inline void set_mHasInitialized_2(bool value)
	{
		___mHasInitialized_2 = value;
	}

	inline static int32_t get_offset_of_mSmartTerrainEventHandlers_3() { return static_cast<int32_t>(offsetof(ReconstructionAbstractBehaviour_t954468633, ___mSmartTerrainEventHandlers_3)); }
	inline List_1_t3446838688 * get_mSmartTerrainEventHandlers_3() const { return ___mSmartTerrainEventHandlers_3; }
	inline List_1_t3446838688 ** get_address_of_mSmartTerrainEventHandlers_3() { return &___mSmartTerrainEventHandlers_3; }
	inline void set_mSmartTerrainEventHandlers_3(List_1_t3446838688 * value)
	{
		___mSmartTerrainEventHandlers_3 = value;
		Il2CppCodeGenWriteBarrier((&___mSmartTerrainEventHandlers_3), value);
	}

	inline static int32_t get_offset_of_mOnInitialized_4() { return static_cast<int32_t>(offsetof(ReconstructionAbstractBehaviour_t954468633, ___mOnInitialized_4)); }
	inline Action_1_t1488611326 * get_mOnInitialized_4() const { return ___mOnInitialized_4; }
	inline Action_1_t1488611326 ** get_address_of_mOnInitialized_4() { return &___mOnInitialized_4; }
	inline void set_mOnInitialized_4(Action_1_t1488611326 * value)
	{
		___mOnInitialized_4 = value;
		Il2CppCodeGenWriteBarrier((&___mOnInitialized_4), value);
	}

	inline static int32_t get_offset_of_mOnPropCreated_5() { return static_cast<int32_t>(offsetof(ReconstructionAbstractBehaviour_t954468633, ___mOnPropCreated_5)); }
	inline Action_1_t1560204979 * get_mOnPropCreated_5() const { return ___mOnPropCreated_5; }
	inline Action_1_t1560204979 ** get_address_of_mOnPropCreated_5() { return &___mOnPropCreated_5; }
	inline void set_mOnPropCreated_5(Action_1_t1560204979 * value)
	{
		___mOnPropCreated_5 = value;
		Il2CppCodeGenWriteBarrier((&___mOnPropCreated_5), value);
	}

	inline static int32_t get_offset_of_mOnPropUpdated_6() { return static_cast<int32_t>(offsetof(ReconstructionAbstractBehaviour_t954468633, ___mOnPropUpdated_6)); }
	inline Action_1_t1560204979 * get_mOnPropUpdated_6() const { return ___mOnPropUpdated_6; }
	inline Action_1_t1560204979 ** get_address_of_mOnPropUpdated_6() { return &___mOnPropUpdated_6; }
	inline void set_mOnPropUpdated_6(Action_1_t1560204979 * value)
	{
		___mOnPropUpdated_6 = value;
		Il2CppCodeGenWriteBarrier((&___mOnPropUpdated_6), value);
	}

	inline static int32_t get_offset_of_mOnPropDeleted_7() { return static_cast<int32_t>(offsetof(ReconstructionAbstractBehaviour_t954468633, ___mOnPropDeleted_7)); }
	inline Action_1_t1560204979 * get_mOnPropDeleted_7() const { return ___mOnPropDeleted_7; }
	inline Action_1_t1560204979 ** get_address_of_mOnPropDeleted_7() { return &___mOnPropDeleted_7; }
	inline void set_mOnPropDeleted_7(Action_1_t1560204979 * value)
	{
		___mOnPropDeleted_7 = value;
		Il2CppCodeGenWriteBarrier((&___mOnPropDeleted_7), value);
	}

	inline static int32_t get_offset_of_mOnSurfaceCreated_8() { return static_cast<int32_t>(offsetof(ReconstructionAbstractBehaviour_t954468633, ___mOnSurfaceCreated_8)); }
	inline Action_1_t1020068010 * get_mOnSurfaceCreated_8() const { return ___mOnSurfaceCreated_8; }
	inline Action_1_t1020068010 ** get_address_of_mOnSurfaceCreated_8() { return &___mOnSurfaceCreated_8; }
	inline void set_mOnSurfaceCreated_8(Action_1_t1020068010 * value)
	{
		___mOnSurfaceCreated_8 = value;
		Il2CppCodeGenWriteBarrier((&___mOnSurfaceCreated_8), value);
	}

	inline static int32_t get_offset_of_mOnSurfaceUpdated_9() { return static_cast<int32_t>(offsetof(ReconstructionAbstractBehaviour_t954468633, ___mOnSurfaceUpdated_9)); }
	inline Action_1_t1020068010 * get_mOnSurfaceUpdated_9() const { return ___mOnSurfaceUpdated_9; }
	inline Action_1_t1020068010 ** get_address_of_mOnSurfaceUpdated_9() { return &___mOnSurfaceUpdated_9; }
	inline void set_mOnSurfaceUpdated_9(Action_1_t1020068010 * value)
	{
		___mOnSurfaceUpdated_9 = value;
		Il2CppCodeGenWriteBarrier((&___mOnSurfaceUpdated_9), value);
	}

	inline static int32_t get_offset_of_mOnSurfaceDeleted_10() { return static_cast<int32_t>(offsetof(ReconstructionAbstractBehaviour_t954468633, ___mOnSurfaceDeleted_10)); }
	inline Action_1_t1020068010 * get_mOnSurfaceDeleted_10() const { return ___mOnSurfaceDeleted_10; }
	inline Action_1_t1020068010 ** get_address_of_mOnSurfaceDeleted_10() { return &___mOnSurfaceDeleted_10; }
	inline void set_mOnSurfaceDeleted_10(Action_1_t1020068010 * value)
	{
		___mOnSurfaceDeleted_10 = value;
		Il2CppCodeGenWriteBarrier((&___mOnSurfaceDeleted_10), value);
	}

	inline static int32_t get_offset_of_mInitializedInEditor_11() { return static_cast<int32_t>(offsetof(ReconstructionAbstractBehaviour_t954468633, ___mInitializedInEditor_11)); }
	inline bool get_mInitializedInEditor_11() const { return ___mInitializedInEditor_11; }
	inline bool* get_address_of_mInitializedInEditor_11() { return &___mInitializedInEditor_11; }
	inline void set_mInitializedInEditor_11(bool value)
	{
		___mInitializedInEditor_11 = value;
	}

	inline static int32_t get_offset_of_mMaximumExtentEnabled_12() { return static_cast<int32_t>(offsetof(ReconstructionAbstractBehaviour_t954468633, ___mMaximumExtentEnabled_12)); }
	inline bool get_mMaximumExtentEnabled_12() const { return ___mMaximumExtentEnabled_12; }
	inline bool* get_address_of_mMaximumExtentEnabled_12() { return &___mMaximumExtentEnabled_12; }
	inline void set_mMaximumExtentEnabled_12(bool value)
	{
		___mMaximumExtentEnabled_12 = value;
	}

	inline static int32_t get_offset_of_mMaximumExtent_13() { return static_cast<int32_t>(offsetof(ReconstructionAbstractBehaviour_t954468633, ___mMaximumExtent_13)); }
	inline Rect_t3436776195  get_mMaximumExtent_13() const { return ___mMaximumExtent_13; }
	inline Rect_t3436776195 * get_address_of_mMaximumExtent_13() { return &___mMaximumExtent_13; }
	inline void set_mMaximumExtent_13(Rect_t3436776195  value)
	{
		___mMaximumExtent_13 = value;
	}

	inline static int32_t get_offset_of_mAutomaticStart_14() { return static_cast<int32_t>(offsetof(ReconstructionAbstractBehaviour_t954468633, ___mAutomaticStart_14)); }
	inline bool get_mAutomaticStart_14() const { return ___mAutomaticStart_14; }
	inline bool* get_address_of_mAutomaticStart_14() { return &___mAutomaticStart_14; }
	inline void set_mAutomaticStart_14(bool value)
	{
		___mAutomaticStart_14 = value;
	}

	inline static int32_t get_offset_of_mNavMeshUpdates_15() { return static_cast<int32_t>(offsetof(ReconstructionAbstractBehaviour_t954468633, ___mNavMeshUpdates_15)); }
	inline bool get_mNavMeshUpdates_15() const { return ___mNavMeshUpdates_15; }
	inline bool* get_address_of_mNavMeshUpdates_15() { return &___mNavMeshUpdates_15; }
	inline void set_mNavMeshUpdates_15(bool value)
	{
		___mNavMeshUpdates_15 = value;
	}

	inline static int32_t get_offset_of_mNavMeshPadding_16() { return static_cast<int32_t>(offsetof(ReconstructionAbstractBehaviour_t954468633, ___mNavMeshPadding_16)); }
	inline float get_mNavMeshPadding_16() const { return ___mNavMeshPadding_16; }
	inline float* get_address_of_mNavMeshPadding_16() { return &___mNavMeshPadding_16; }
	inline void set_mNavMeshPadding_16(float value)
	{
		___mNavMeshPadding_16 = value;
	}

	inline static int32_t get_offset_of_mReconstruction_17() { return static_cast<int32_t>(offsetof(ReconstructionAbstractBehaviour_t954468633, ___mReconstruction_17)); }
	inline RuntimeObject* get_mReconstruction_17() const { return ___mReconstruction_17; }
	inline RuntimeObject** get_address_of_mReconstruction_17() { return &___mReconstruction_17; }
	inline void set_mReconstruction_17(RuntimeObject* value)
	{
		___mReconstruction_17 = value;
		Il2CppCodeGenWriteBarrier((&___mReconstruction_17), value);
	}

	inline static int32_t get_offset_of_mSurfaces_18() { return static_cast<int32_t>(offsetof(ReconstructionAbstractBehaviour_t954468633, ___mSurfaces_18)); }
	inline Dictionary_2_t2230247832 * get_mSurfaces_18() const { return ___mSurfaces_18; }
	inline Dictionary_2_t2230247832 ** get_address_of_mSurfaces_18() { return &___mSurfaces_18; }
	inline void set_mSurfaces_18(Dictionary_2_t2230247832 * value)
	{
		___mSurfaces_18 = value;
		Il2CppCodeGenWriteBarrier((&___mSurfaces_18), value);
	}

	inline static int32_t get_offset_of_mActiveSurfaceBehaviours_19() { return static_cast<int32_t>(offsetof(ReconstructionAbstractBehaviour_t954468633, ___mActiveSurfaceBehaviours_19)); }
	inline Dictionary_2_t1468548340 * get_mActiveSurfaceBehaviours_19() const { return ___mActiveSurfaceBehaviours_19; }
	inline Dictionary_2_t1468548340 ** get_address_of_mActiveSurfaceBehaviours_19() { return &___mActiveSurfaceBehaviours_19; }
	inline void set_mActiveSurfaceBehaviours_19(Dictionary_2_t1468548340 * value)
	{
		___mActiveSurfaceBehaviours_19 = value;
		Il2CppCodeGenWriteBarrier((&___mActiveSurfaceBehaviours_19), value);
	}

	inline static int32_t get_offset_of_mProps_20() { return static_cast<int32_t>(offsetof(ReconstructionAbstractBehaviour_t954468633, ___mProps_20)); }
	inline Dictionary_2_t2770384801 * get_mProps_20() const { return ___mProps_20; }
	inline Dictionary_2_t2770384801 ** get_address_of_mProps_20() { return &___mProps_20; }
	inline void set_mProps_20(Dictionary_2_t2770384801 * value)
	{
		___mProps_20 = value;
		Il2CppCodeGenWriteBarrier((&___mProps_20), value);
	}

	inline static int32_t get_offset_of_mActivePropBehaviours_21() { return static_cast<int32_t>(offsetof(ReconstructionAbstractBehaviour_t954468633, ___mActivePropBehaviours_21)); }
	inline Dictionary_2_t3844889330 * get_mActivePropBehaviours_21() const { return ___mActivePropBehaviours_21; }
	inline Dictionary_2_t3844889330 ** get_address_of_mActivePropBehaviours_21() { return &___mActivePropBehaviours_21; }
	inline void set_mActivePropBehaviours_21(Dictionary_2_t3844889330 * value)
	{
		___mActivePropBehaviours_21 = value;
		Il2CppCodeGenWriteBarrier((&___mActivePropBehaviours_21), value);
	}

	inline static int32_t get_offset_of_mPreviouslySetWorldCenterSurfaceTemplate_22() { return static_cast<int32_t>(offsetof(ReconstructionAbstractBehaviour_t954468633, ___mPreviouslySetWorldCenterSurfaceTemplate_22)); }
	inline SurfaceAbstractBehaviour_t272541176 * get_mPreviouslySetWorldCenterSurfaceTemplate_22() const { return ___mPreviouslySetWorldCenterSurfaceTemplate_22; }
	inline SurfaceAbstractBehaviour_t272541176 ** get_address_of_mPreviouslySetWorldCenterSurfaceTemplate_22() { return &___mPreviouslySetWorldCenterSurfaceTemplate_22; }
	inline void set_mPreviouslySetWorldCenterSurfaceTemplate_22(SurfaceAbstractBehaviour_t272541176 * value)
	{
		___mPreviouslySetWorldCenterSurfaceTemplate_22 = value;
		Il2CppCodeGenWriteBarrier((&___mPreviouslySetWorldCenterSurfaceTemplate_22), value);
	}

	inline static int32_t get_offset_of_mIgnoreNextUpdate_23() { return static_cast<int32_t>(offsetof(ReconstructionAbstractBehaviour_t954468633, ___mIgnoreNextUpdate_23)); }
	inline bool get_mIgnoreNextUpdate_23() const { return ___mIgnoreNextUpdate_23; }
	inline bool* get_address_of_mIgnoreNextUpdate_23() { return &___mIgnoreNextUpdate_23; }
	inline void set_mIgnoreNextUpdate_23(bool value)
	{
		___mIgnoreNextUpdate_23 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RECONSTRUCTIONABSTRACTBEHAVIOUR_T954468633_H
#ifndef VIDEOBACKGROUNDABSTRACTBEHAVIOUR_T3867888217_H
#define VIDEOBACKGROUNDABSTRACTBEHAVIOUR_T3867888217_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.VideoBackgroundAbstractBehaviour
struct  VideoBackgroundAbstractBehaviour_t3867888217  : public MonoBehaviour_t345688271
{
public:
	// System.Int32 Vuforia.VideoBackgroundAbstractBehaviour::mClearBuffers
	int32_t ___mClearBuffers_2;
	// System.Int32 Vuforia.VideoBackgroundAbstractBehaviour::mSkipStateUpdates
	int32_t ___mSkipStateUpdates_3;
	// Vuforia.VuforiaARController Vuforia.VideoBackgroundAbstractBehaviour::mVuforiaARController
	VuforiaARController_t1328503142 * ___mVuforiaARController_4;
	// UnityEngine.Camera Vuforia.VideoBackgroundAbstractBehaviour::mCamera
	Camera_t3175186167 * ___mCamera_5;
	// Vuforia.BackgroundPlaneAbstractBehaviour Vuforia.VideoBackgroundAbstractBehaviour::mBackgroundBehaviour
	BackgroundPlaneAbstractBehaviour_t4259835756 * ___mBackgroundBehaviour_6;
	// System.Single Vuforia.VideoBackgroundAbstractBehaviour::mStereoDepth
	float ___mStereoDepth_7;
	// System.Boolean Vuforia.VideoBackgroundAbstractBehaviour::mResetMatrix
	bool ___mResetMatrix_10;
	// UnityEngine.Vector2 Vuforia.VideoBackgroundAbstractBehaviour::mVuforiaFrustumSkew
	Vector2_t3577333262  ___mVuforiaFrustumSkew_11;
	// UnityEngine.Vector2 Vuforia.VideoBackgroundAbstractBehaviour::mCenterToEyeAxis
	Vector2_t3577333262  ___mCenterToEyeAxis_12;
	// System.Collections.Generic.HashSet`1<UnityEngine.MeshRenderer> Vuforia.VideoBackgroundAbstractBehaviour::mDisabledMeshRenderers
	HashSet_1_t1919463348 * ___mDisabledMeshRenderers_13;

public:
	inline static int32_t get_offset_of_mClearBuffers_2() { return static_cast<int32_t>(offsetof(VideoBackgroundAbstractBehaviour_t3867888217, ___mClearBuffers_2)); }
	inline int32_t get_mClearBuffers_2() const { return ___mClearBuffers_2; }
	inline int32_t* get_address_of_mClearBuffers_2() { return &___mClearBuffers_2; }
	inline void set_mClearBuffers_2(int32_t value)
	{
		___mClearBuffers_2 = value;
	}

	inline static int32_t get_offset_of_mSkipStateUpdates_3() { return static_cast<int32_t>(offsetof(VideoBackgroundAbstractBehaviour_t3867888217, ___mSkipStateUpdates_3)); }
	inline int32_t get_mSkipStateUpdates_3() const { return ___mSkipStateUpdates_3; }
	inline int32_t* get_address_of_mSkipStateUpdates_3() { return &___mSkipStateUpdates_3; }
	inline void set_mSkipStateUpdates_3(int32_t value)
	{
		___mSkipStateUpdates_3 = value;
	}

	inline static int32_t get_offset_of_mVuforiaARController_4() { return static_cast<int32_t>(offsetof(VideoBackgroundAbstractBehaviour_t3867888217, ___mVuforiaARController_4)); }
	inline VuforiaARController_t1328503142 * get_mVuforiaARController_4() const { return ___mVuforiaARController_4; }
	inline VuforiaARController_t1328503142 ** get_address_of_mVuforiaARController_4() { return &___mVuforiaARController_4; }
	inline void set_mVuforiaARController_4(VuforiaARController_t1328503142 * value)
	{
		___mVuforiaARController_4 = value;
		Il2CppCodeGenWriteBarrier((&___mVuforiaARController_4), value);
	}

	inline static int32_t get_offset_of_mCamera_5() { return static_cast<int32_t>(offsetof(VideoBackgroundAbstractBehaviour_t3867888217, ___mCamera_5)); }
	inline Camera_t3175186167 * get_mCamera_5() const { return ___mCamera_5; }
	inline Camera_t3175186167 ** get_address_of_mCamera_5() { return &___mCamera_5; }
	inline void set_mCamera_5(Camera_t3175186167 * value)
	{
		___mCamera_5 = value;
		Il2CppCodeGenWriteBarrier((&___mCamera_5), value);
	}

	inline static int32_t get_offset_of_mBackgroundBehaviour_6() { return static_cast<int32_t>(offsetof(VideoBackgroundAbstractBehaviour_t3867888217, ___mBackgroundBehaviour_6)); }
	inline BackgroundPlaneAbstractBehaviour_t4259835756 * get_mBackgroundBehaviour_6() const { return ___mBackgroundBehaviour_6; }
	inline BackgroundPlaneAbstractBehaviour_t4259835756 ** get_address_of_mBackgroundBehaviour_6() { return &___mBackgroundBehaviour_6; }
	inline void set_mBackgroundBehaviour_6(BackgroundPlaneAbstractBehaviour_t4259835756 * value)
	{
		___mBackgroundBehaviour_6 = value;
		Il2CppCodeGenWriteBarrier((&___mBackgroundBehaviour_6), value);
	}

	inline static int32_t get_offset_of_mStereoDepth_7() { return static_cast<int32_t>(offsetof(VideoBackgroundAbstractBehaviour_t3867888217, ___mStereoDepth_7)); }
	inline float get_mStereoDepth_7() const { return ___mStereoDepth_7; }
	inline float* get_address_of_mStereoDepth_7() { return &___mStereoDepth_7; }
	inline void set_mStereoDepth_7(float value)
	{
		___mStereoDepth_7 = value;
	}

	inline static int32_t get_offset_of_mResetMatrix_10() { return static_cast<int32_t>(offsetof(VideoBackgroundAbstractBehaviour_t3867888217, ___mResetMatrix_10)); }
	inline bool get_mResetMatrix_10() const { return ___mResetMatrix_10; }
	inline bool* get_address_of_mResetMatrix_10() { return &___mResetMatrix_10; }
	inline void set_mResetMatrix_10(bool value)
	{
		___mResetMatrix_10 = value;
	}

	inline static int32_t get_offset_of_mVuforiaFrustumSkew_11() { return static_cast<int32_t>(offsetof(VideoBackgroundAbstractBehaviour_t3867888217, ___mVuforiaFrustumSkew_11)); }
	inline Vector2_t3577333262  get_mVuforiaFrustumSkew_11() const { return ___mVuforiaFrustumSkew_11; }
	inline Vector2_t3577333262 * get_address_of_mVuforiaFrustumSkew_11() { return &___mVuforiaFrustumSkew_11; }
	inline void set_mVuforiaFrustumSkew_11(Vector2_t3577333262  value)
	{
		___mVuforiaFrustumSkew_11 = value;
	}

	inline static int32_t get_offset_of_mCenterToEyeAxis_12() { return static_cast<int32_t>(offsetof(VideoBackgroundAbstractBehaviour_t3867888217, ___mCenterToEyeAxis_12)); }
	inline Vector2_t3577333262  get_mCenterToEyeAxis_12() const { return ___mCenterToEyeAxis_12; }
	inline Vector2_t3577333262 * get_address_of_mCenterToEyeAxis_12() { return &___mCenterToEyeAxis_12; }
	inline void set_mCenterToEyeAxis_12(Vector2_t3577333262  value)
	{
		___mCenterToEyeAxis_12 = value;
	}

	inline static int32_t get_offset_of_mDisabledMeshRenderers_13() { return static_cast<int32_t>(offsetof(VideoBackgroundAbstractBehaviour_t3867888217, ___mDisabledMeshRenderers_13)); }
	inline HashSet_1_t1919463348 * get_mDisabledMeshRenderers_13() const { return ___mDisabledMeshRenderers_13; }
	inline HashSet_1_t1919463348 ** get_address_of_mDisabledMeshRenderers_13() { return &___mDisabledMeshRenderers_13; }
	inline void set_mDisabledMeshRenderers_13(HashSet_1_t1919463348 * value)
	{
		___mDisabledMeshRenderers_13 = value;
		Il2CppCodeGenWriteBarrier((&___mDisabledMeshRenderers_13), value);
	}
};

struct VideoBackgroundAbstractBehaviour_t3867888217_StaticFields
{
public:
	// System.Int32 Vuforia.VideoBackgroundAbstractBehaviour::mFrameCounter
	int32_t ___mFrameCounter_8;
	// System.Int32 Vuforia.VideoBackgroundAbstractBehaviour::mRenderCounter
	int32_t ___mRenderCounter_9;

public:
	inline static int32_t get_offset_of_mFrameCounter_8() { return static_cast<int32_t>(offsetof(VideoBackgroundAbstractBehaviour_t3867888217_StaticFields, ___mFrameCounter_8)); }
	inline int32_t get_mFrameCounter_8() const { return ___mFrameCounter_8; }
	inline int32_t* get_address_of_mFrameCounter_8() { return &___mFrameCounter_8; }
	inline void set_mFrameCounter_8(int32_t value)
	{
		___mFrameCounter_8 = value;
	}

	inline static int32_t get_offset_of_mRenderCounter_9() { return static_cast<int32_t>(offsetof(VideoBackgroundAbstractBehaviour_t3867888217_StaticFields, ___mRenderCounter_9)); }
	inline int32_t get_mRenderCounter_9() const { return ___mRenderCounter_9; }
	inline int32_t* get_address_of_mRenderCounter_9() { return &___mRenderCounter_9; }
	inline void set_mRenderCounter_9(int32_t value)
	{
		___mRenderCounter_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VIDEOBACKGROUNDABSTRACTBEHAVIOUR_T3867888217_H
#ifndef MASKOUTABSTRACTBEHAVIOUR_T2174599097_H
#define MASKOUTABSTRACTBEHAVIOUR_T2174599097_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.MaskOutAbstractBehaviour
struct  MaskOutAbstractBehaviour_t2174599097  : public MonoBehaviour_t345688271
{
public:
	// UnityEngine.Material Vuforia.MaskOutAbstractBehaviour::maskMaterial
	Material_t1079520667 * ___maskMaterial_2;

public:
	inline static int32_t get_offset_of_maskMaterial_2() { return static_cast<int32_t>(offsetof(MaskOutAbstractBehaviour_t2174599097, ___maskMaterial_2)); }
	inline Material_t1079520667 * get_maskMaterial_2() const { return ___maskMaterial_2; }
	inline Material_t1079520667 ** get_address_of_maskMaterial_2() { return &___maskMaterial_2; }
	inline void set_maskMaterial_2(Material_t1079520667 * value)
	{
		___maskMaterial_2 = value;
		Il2CppCodeGenWriteBarrier((&___maskMaterial_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MASKOUTABSTRACTBEHAVIOUR_T2174599097_H
#ifndef USERDEFINEDTARGETBUILDINGABSTRACTBEHAVIOUR_T2424076393_H
#define USERDEFINEDTARGETBUILDINGABSTRACTBEHAVIOUR_T2424076393_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.UserDefinedTargetBuildingAbstractBehaviour
struct  UserDefinedTargetBuildingAbstractBehaviour_t2424076393  : public MonoBehaviour_t345688271
{
public:
	// Vuforia.ObjectTracker Vuforia.UserDefinedTargetBuildingAbstractBehaviour::mObjectTracker
	ObjectTracker_t4023638979 * ___mObjectTracker_2;
	// Vuforia.ImageTargetBuilder/FrameQuality Vuforia.UserDefinedTargetBuildingAbstractBehaviour::mLastFrameQuality
	int32_t ___mLastFrameQuality_3;
	// System.Boolean Vuforia.UserDefinedTargetBuildingAbstractBehaviour::mCurrentlyScanning
	bool ___mCurrentlyScanning_4;
	// System.Boolean Vuforia.UserDefinedTargetBuildingAbstractBehaviour::mWasScanningBeforeDisable
	bool ___mWasScanningBeforeDisable_5;
	// System.Boolean Vuforia.UserDefinedTargetBuildingAbstractBehaviour::mCurrentlyBuilding
	bool ___mCurrentlyBuilding_6;
	// System.Boolean Vuforia.UserDefinedTargetBuildingAbstractBehaviour::mWasBuildingBeforeDisable
	bool ___mWasBuildingBeforeDisable_7;
	// System.Boolean Vuforia.UserDefinedTargetBuildingAbstractBehaviour::mOnInitializedCalled
	bool ___mOnInitializedCalled_8;
	// System.Collections.Generic.List`1<Vuforia.IUserDefinedTargetEventHandler> Vuforia.UserDefinedTargetBuildingAbstractBehaviour::mHandlers
	List_1_t4290827367 * ___mHandlers_9;
	// System.Boolean Vuforia.UserDefinedTargetBuildingAbstractBehaviour::StopTrackerWhileScanning
	bool ___StopTrackerWhileScanning_10;
	// System.Boolean Vuforia.UserDefinedTargetBuildingAbstractBehaviour::StartScanningAutomatically
	bool ___StartScanningAutomatically_11;
	// System.Boolean Vuforia.UserDefinedTargetBuildingAbstractBehaviour::StopScanningWhenFinshedBuilding
	bool ___StopScanningWhenFinshedBuilding_12;

public:
	inline static int32_t get_offset_of_mObjectTracker_2() { return static_cast<int32_t>(offsetof(UserDefinedTargetBuildingAbstractBehaviour_t2424076393, ___mObjectTracker_2)); }
	inline ObjectTracker_t4023638979 * get_mObjectTracker_2() const { return ___mObjectTracker_2; }
	inline ObjectTracker_t4023638979 ** get_address_of_mObjectTracker_2() { return &___mObjectTracker_2; }
	inline void set_mObjectTracker_2(ObjectTracker_t4023638979 * value)
	{
		___mObjectTracker_2 = value;
		Il2CppCodeGenWriteBarrier((&___mObjectTracker_2), value);
	}

	inline static int32_t get_offset_of_mLastFrameQuality_3() { return static_cast<int32_t>(offsetof(UserDefinedTargetBuildingAbstractBehaviour_t2424076393, ___mLastFrameQuality_3)); }
	inline int32_t get_mLastFrameQuality_3() const { return ___mLastFrameQuality_3; }
	inline int32_t* get_address_of_mLastFrameQuality_3() { return &___mLastFrameQuality_3; }
	inline void set_mLastFrameQuality_3(int32_t value)
	{
		___mLastFrameQuality_3 = value;
	}

	inline static int32_t get_offset_of_mCurrentlyScanning_4() { return static_cast<int32_t>(offsetof(UserDefinedTargetBuildingAbstractBehaviour_t2424076393, ___mCurrentlyScanning_4)); }
	inline bool get_mCurrentlyScanning_4() const { return ___mCurrentlyScanning_4; }
	inline bool* get_address_of_mCurrentlyScanning_4() { return &___mCurrentlyScanning_4; }
	inline void set_mCurrentlyScanning_4(bool value)
	{
		___mCurrentlyScanning_4 = value;
	}

	inline static int32_t get_offset_of_mWasScanningBeforeDisable_5() { return static_cast<int32_t>(offsetof(UserDefinedTargetBuildingAbstractBehaviour_t2424076393, ___mWasScanningBeforeDisable_5)); }
	inline bool get_mWasScanningBeforeDisable_5() const { return ___mWasScanningBeforeDisable_5; }
	inline bool* get_address_of_mWasScanningBeforeDisable_5() { return &___mWasScanningBeforeDisable_5; }
	inline void set_mWasScanningBeforeDisable_5(bool value)
	{
		___mWasScanningBeforeDisable_5 = value;
	}

	inline static int32_t get_offset_of_mCurrentlyBuilding_6() { return static_cast<int32_t>(offsetof(UserDefinedTargetBuildingAbstractBehaviour_t2424076393, ___mCurrentlyBuilding_6)); }
	inline bool get_mCurrentlyBuilding_6() const { return ___mCurrentlyBuilding_6; }
	inline bool* get_address_of_mCurrentlyBuilding_6() { return &___mCurrentlyBuilding_6; }
	inline void set_mCurrentlyBuilding_6(bool value)
	{
		___mCurrentlyBuilding_6 = value;
	}

	inline static int32_t get_offset_of_mWasBuildingBeforeDisable_7() { return static_cast<int32_t>(offsetof(UserDefinedTargetBuildingAbstractBehaviour_t2424076393, ___mWasBuildingBeforeDisable_7)); }
	inline bool get_mWasBuildingBeforeDisable_7() const { return ___mWasBuildingBeforeDisable_7; }
	inline bool* get_address_of_mWasBuildingBeforeDisable_7() { return &___mWasBuildingBeforeDisable_7; }
	inline void set_mWasBuildingBeforeDisable_7(bool value)
	{
		___mWasBuildingBeforeDisable_7 = value;
	}

	inline static int32_t get_offset_of_mOnInitializedCalled_8() { return static_cast<int32_t>(offsetof(UserDefinedTargetBuildingAbstractBehaviour_t2424076393, ___mOnInitializedCalled_8)); }
	inline bool get_mOnInitializedCalled_8() const { return ___mOnInitializedCalled_8; }
	inline bool* get_address_of_mOnInitializedCalled_8() { return &___mOnInitializedCalled_8; }
	inline void set_mOnInitializedCalled_8(bool value)
	{
		___mOnInitializedCalled_8 = value;
	}

	inline static int32_t get_offset_of_mHandlers_9() { return static_cast<int32_t>(offsetof(UserDefinedTargetBuildingAbstractBehaviour_t2424076393, ___mHandlers_9)); }
	inline List_1_t4290827367 * get_mHandlers_9() const { return ___mHandlers_9; }
	inline List_1_t4290827367 ** get_address_of_mHandlers_9() { return &___mHandlers_9; }
	inline void set_mHandlers_9(List_1_t4290827367 * value)
	{
		___mHandlers_9 = value;
		Il2CppCodeGenWriteBarrier((&___mHandlers_9), value);
	}

	inline static int32_t get_offset_of_StopTrackerWhileScanning_10() { return static_cast<int32_t>(offsetof(UserDefinedTargetBuildingAbstractBehaviour_t2424076393, ___StopTrackerWhileScanning_10)); }
	inline bool get_StopTrackerWhileScanning_10() const { return ___StopTrackerWhileScanning_10; }
	inline bool* get_address_of_StopTrackerWhileScanning_10() { return &___StopTrackerWhileScanning_10; }
	inline void set_StopTrackerWhileScanning_10(bool value)
	{
		___StopTrackerWhileScanning_10 = value;
	}

	inline static int32_t get_offset_of_StartScanningAutomatically_11() { return static_cast<int32_t>(offsetof(UserDefinedTargetBuildingAbstractBehaviour_t2424076393, ___StartScanningAutomatically_11)); }
	inline bool get_StartScanningAutomatically_11() const { return ___StartScanningAutomatically_11; }
	inline bool* get_address_of_StartScanningAutomatically_11() { return &___StartScanningAutomatically_11; }
	inline void set_StartScanningAutomatically_11(bool value)
	{
		___StartScanningAutomatically_11 = value;
	}

	inline static int32_t get_offset_of_StopScanningWhenFinshedBuilding_12() { return static_cast<int32_t>(offsetof(UserDefinedTargetBuildingAbstractBehaviour_t2424076393, ___StopScanningWhenFinshedBuilding_12)); }
	inline bool get_StopScanningWhenFinshedBuilding_12() const { return ___StopScanningWhenFinshedBuilding_12; }
	inline bool* get_address_of_StopScanningWhenFinshedBuilding_12() { return &___StopScanningWhenFinshedBuilding_12; }
	inline void set_StopScanningWhenFinshedBuilding_12(bool value)
	{
		___StopScanningWhenFinshedBuilding_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // USERDEFINEDTARGETBUILDINGABSTRACTBEHAVIOUR_T2424076393_H
#ifndef ANALYTICSTRACKER_T4207383808_H
#define ANALYTICSTRACKER_T4207383808_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.AnalyticsTracker
struct  AnalyticsTracker_t4207383808  : public MonoBehaviour_t345688271
{
public:
	// System.String UnityEngine.Analytics.AnalyticsTracker::m_EventName
	String_t* ___m_EventName_2;
	// System.Collections.Generic.Dictionary`2<System.String,System.Object> UnityEngine.Analytics.AnalyticsTracker::m_Dict
	Dictionary_2_t3191847521 * ___m_Dict_3;
	// System.Int32 UnityEngine.Analytics.AnalyticsTracker::m_PrevDictHash
	int32_t ___m_PrevDictHash_4;
	// UnityEngine.Analytics.TrackableProperty UnityEngine.Analytics.AnalyticsTracker::m_TrackableProperty
	TrackableProperty_t10670078 * ___m_TrackableProperty_5;
	// UnityEngine.Analytics.AnalyticsTracker/Trigger UnityEngine.Analytics.AnalyticsTracker::m_Trigger
	int32_t ___m_Trigger_6;

public:
	inline static int32_t get_offset_of_m_EventName_2() { return static_cast<int32_t>(offsetof(AnalyticsTracker_t4207383808, ___m_EventName_2)); }
	inline String_t* get_m_EventName_2() const { return ___m_EventName_2; }
	inline String_t** get_address_of_m_EventName_2() { return &___m_EventName_2; }
	inline void set_m_EventName_2(String_t* value)
	{
		___m_EventName_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_EventName_2), value);
	}

	inline static int32_t get_offset_of_m_Dict_3() { return static_cast<int32_t>(offsetof(AnalyticsTracker_t4207383808, ___m_Dict_3)); }
	inline Dictionary_2_t3191847521 * get_m_Dict_3() const { return ___m_Dict_3; }
	inline Dictionary_2_t3191847521 ** get_address_of_m_Dict_3() { return &___m_Dict_3; }
	inline void set_m_Dict_3(Dictionary_2_t3191847521 * value)
	{
		___m_Dict_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_Dict_3), value);
	}

	inline static int32_t get_offset_of_m_PrevDictHash_4() { return static_cast<int32_t>(offsetof(AnalyticsTracker_t4207383808, ___m_PrevDictHash_4)); }
	inline int32_t get_m_PrevDictHash_4() const { return ___m_PrevDictHash_4; }
	inline int32_t* get_address_of_m_PrevDictHash_4() { return &___m_PrevDictHash_4; }
	inline void set_m_PrevDictHash_4(int32_t value)
	{
		___m_PrevDictHash_4 = value;
	}

	inline static int32_t get_offset_of_m_TrackableProperty_5() { return static_cast<int32_t>(offsetof(AnalyticsTracker_t4207383808, ___m_TrackableProperty_5)); }
	inline TrackableProperty_t10670078 * get_m_TrackableProperty_5() const { return ___m_TrackableProperty_5; }
	inline TrackableProperty_t10670078 ** get_address_of_m_TrackableProperty_5() { return &___m_TrackableProperty_5; }
	inline void set_m_TrackableProperty_5(TrackableProperty_t10670078 * value)
	{
		___m_TrackableProperty_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_TrackableProperty_5), value);
	}

	inline static int32_t get_offset_of_m_Trigger_6() { return static_cast<int32_t>(offsetof(AnalyticsTracker_t4207383808, ___m_Trigger_6)); }
	inline int32_t get_m_Trigger_6() const { return ___m_Trigger_6; }
	inline int32_t* get_address_of_m_Trigger_6() { return &___m_Trigger_6; }
	inline void set_m_Trigger_6(int32_t value)
	{
		___m_Trigger_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANALYTICSTRACKER_T4207383808_H
#ifndef TEXTRECOABSTRACTBEHAVIOUR_T1751158196_H
#define TEXTRECOABSTRACTBEHAVIOUR_T1751158196_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.TextRecoAbstractBehaviour
struct  TextRecoAbstractBehaviour_t1751158196  : public MonoBehaviour_t345688271
{
public:
	// System.Boolean Vuforia.TextRecoAbstractBehaviour::mHasInitialized
	bool ___mHasInitialized_2;
	// System.Boolean Vuforia.TextRecoAbstractBehaviour::mTrackerWasActiveBeforePause
	bool ___mTrackerWasActiveBeforePause_3;
	// System.Boolean Vuforia.TextRecoAbstractBehaviour::mTrackerWasActiveBeforeDisabling
	bool ___mTrackerWasActiveBeforeDisabling_4;
	// System.String Vuforia.TextRecoAbstractBehaviour::mWordListFile
	String_t* ___mWordListFile_5;
	// System.String Vuforia.TextRecoAbstractBehaviour::mCustomWordListFile
	String_t* ___mCustomWordListFile_6;
	// System.String Vuforia.TextRecoAbstractBehaviour::mAdditionalCustomWords
	String_t* ___mAdditionalCustomWords_7;
	// Vuforia.WordFilterMode Vuforia.TextRecoAbstractBehaviour::mFilterMode
	int32_t ___mFilterMode_8;
	// System.String Vuforia.TextRecoAbstractBehaviour::mFilterListFile
	String_t* ___mFilterListFile_9;
	// System.String Vuforia.TextRecoAbstractBehaviour::mAdditionalFilterWords
	String_t* ___mAdditionalFilterWords_10;
	// Vuforia.WordPrefabCreationMode Vuforia.TextRecoAbstractBehaviour::mWordPrefabCreationMode
	int32_t ___mWordPrefabCreationMode_11;
	// System.Int32 Vuforia.TextRecoAbstractBehaviour::mMaximumWordInstances
	int32_t ___mMaximumWordInstances_12;
	// System.Collections.Generic.List`1<Vuforia.ITextRecoEventHandler> Vuforia.TextRecoAbstractBehaviour::mTextRecoEventHandlers
	List_1_t4098171074 * ___mTextRecoEventHandlers_13;

public:
	inline static int32_t get_offset_of_mHasInitialized_2() { return static_cast<int32_t>(offsetof(TextRecoAbstractBehaviour_t1751158196, ___mHasInitialized_2)); }
	inline bool get_mHasInitialized_2() const { return ___mHasInitialized_2; }
	inline bool* get_address_of_mHasInitialized_2() { return &___mHasInitialized_2; }
	inline void set_mHasInitialized_2(bool value)
	{
		___mHasInitialized_2 = value;
	}

	inline static int32_t get_offset_of_mTrackerWasActiveBeforePause_3() { return static_cast<int32_t>(offsetof(TextRecoAbstractBehaviour_t1751158196, ___mTrackerWasActiveBeforePause_3)); }
	inline bool get_mTrackerWasActiveBeforePause_3() const { return ___mTrackerWasActiveBeforePause_3; }
	inline bool* get_address_of_mTrackerWasActiveBeforePause_3() { return &___mTrackerWasActiveBeforePause_3; }
	inline void set_mTrackerWasActiveBeforePause_3(bool value)
	{
		___mTrackerWasActiveBeforePause_3 = value;
	}

	inline static int32_t get_offset_of_mTrackerWasActiveBeforeDisabling_4() { return static_cast<int32_t>(offsetof(TextRecoAbstractBehaviour_t1751158196, ___mTrackerWasActiveBeforeDisabling_4)); }
	inline bool get_mTrackerWasActiveBeforeDisabling_4() const { return ___mTrackerWasActiveBeforeDisabling_4; }
	inline bool* get_address_of_mTrackerWasActiveBeforeDisabling_4() { return &___mTrackerWasActiveBeforeDisabling_4; }
	inline void set_mTrackerWasActiveBeforeDisabling_4(bool value)
	{
		___mTrackerWasActiveBeforeDisabling_4 = value;
	}

	inline static int32_t get_offset_of_mWordListFile_5() { return static_cast<int32_t>(offsetof(TextRecoAbstractBehaviour_t1751158196, ___mWordListFile_5)); }
	inline String_t* get_mWordListFile_5() const { return ___mWordListFile_5; }
	inline String_t** get_address_of_mWordListFile_5() { return &___mWordListFile_5; }
	inline void set_mWordListFile_5(String_t* value)
	{
		___mWordListFile_5 = value;
		Il2CppCodeGenWriteBarrier((&___mWordListFile_5), value);
	}

	inline static int32_t get_offset_of_mCustomWordListFile_6() { return static_cast<int32_t>(offsetof(TextRecoAbstractBehaviour_t1751158196, ___mCustomWordListFile_6)); }
	inline String_t* get_mCustomWordListFile_6() const { return ___mCustomWordListFile_6; }
	inline String_t** get_address_of_mCustomWordListFile_6() { return &___mCustomWordListFile_6; }
	inline void set_mCustomWordListFile_6(String_t* value)
	{
		___mCustomWordListFile_6 = value;
		Il2CppCodeGenWriteBarrier((&___mCustomWordListFile_6), value);
	}

	inline static int32_t get_offset_of_mAdditionalCustomWords_7() { return static_cast<int32_t>(offsetof(TextRecoAbstractBehaviour_t1751158196, ___mAdditionalCustomWords_7)); }
	inline String_t* get_mAdditionalCustomWords_7() const { return ___mAdditionalCustomWords_7; }
	inline String_t** get_address_of_mAdditionalCustomWords_7() { return &___mAdditionalCustomWords_7; }
	inline void set_mAdditionalCustomWords_7(String_t* value)
	{
		___mAdditionalCustomWords_7 = value;
		Il2CppCodeGenWriteBarrier((&___mAdditionalCustomWords_7), value);
	}

	inline static int32_t get_offset_of_mFilterMode_8() { return static_cast<int32_t>(offsetof(TextRecoAbstractBehaviour_t1751158196, ___mFilterMode_8)); }
	inline int32_t get_mFilterMode_8() const { return ___mFilterMode_8; }
	inline int32_t* get_address_of_mFilterMode_8() { return &___mFilterMode_8; }
	inline void set_mFilterMode_8(int32_t value)
	{
		___mFilterMode_8 = value;
	}

	inline static int32_t get_offset_of_mFilterListFile_9() { return static_cast<int32_t>(offsetof(TextRecoAbstractBehaviour_t1751158196, ___mFilterListFile_9)); }
	inline String_t* get_mFilterListFile_9() const { return ___mFilterListFile_9; }
	inline String_t** get_address_of_mFilterListFile_9() { return &___mFilterListFile_9; }
	inline void set_mFilterListFile_9(String_t* value)
	{
		___mFilterListFile_9 = value;
		Il2CppCodeGenWriteBarrier((&___mFilterListFile_9), value);
	}

	inline static int32_t get_offset_of_mAdditionalFilterWords_10() { return static_cast<int32_t>(offsetof(TextRecoAbstractBehaviour_t1751158196, ___mAdditionalFilterWords_10)); }
	inline String_t* get_mAdditionalFilterWords_10() const { return ___mAdditionalFilterWords_10; }
	inline String_t** get_address_of_mAdditionalFilterWords_10() { return &___mAdditionalFilterWords_10; }
	inline void set_mAdditionalFilterWords_10(String_t* value)
	{
		___mAdditionalFilterWords_10 = value;
		Il2CppCodeGenWriteBarrier((&___mAdditionalFilterWords_10), value);
	}

	inline static int32_t get_offset_of_mWordPrefabCreationMode_11() { return static_cast<int32_t>(offsetof(TextRecoAbstractBehaviour_t1751158196, ___mWordPrefabCreationMode_11)); }
	inline int32_t get_mWordPrefabCreationMode_11() const { return ___mWordPrefabCreationMode_11; }
	inline int32_t* get_address_of_mWordPrefabCreationMode_11() { return &___mWordPrefabCreationMode_11; }
	inline void set_mWordPrefabCreationMode_11(int32_t value)
	{
		___mWordPrefabCreationMode_11 = value;
	}

	inline static int32_t get_offset_of_mMaximumWordInstances_12() { return static_cast<int32_t>(offsetof(TextRecoAbstractBehaviour_t1751158196, ___mMaximumWordInstances_12)); }
	inline int32_t get_mMaximumWordInstances_12() const { return ___mMaximumWordInstances_12; }
	inline int32_t* get_address_of_mMaximumWordInstances_12() { return &___mMaximumWordInstances_12; }
	inline void set_mMaximumWordInstances_12(int32_t value)
	{
		___mMaximumWordInstances_12 = value;
	}

	inline static int32_t get_offset_of_mTextRecoEventHandlers_13() { return static_cast<int32_t>(offsetof(TextRecoAbstractBehaviour_t1751158196, ___mTextRecoEventHandlers_13)); }
	inline List_1_t4098171074 * get_mTextRecoEventHandlers_13() const { return ___mTextRecoEventHandlers_13; }
	inline List_1_t4098171074 ** get_address_of_mTextRecoEventHandlers_13() { return &___mTextRecoEventHandlers_13; }
	inline void set_mTextRecoEventHandlers_13(List_1_t4098171074 * value)
	{
		___mTextRecoEventHandlers_13 = value;
		Il2CppCodeGenWriteBarrier((&___mTextRecoEventHandlers_13), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTRECOABSTRACTBEHAVIOUR_T1751158196_H
#ifndef VIRTUALBUTTONABSTRACTBEHAVIOUR_T1965820348_H
#define VIRTUALBUTTONABSTRACTBEHAVIOUR_T1965820348_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.VirtualButtonAbstractBehaviour
struct  VirtualButtonAbstractBehaviour_t1965820348  : public MonoBehaviour_t345688271
{
public:
	// System.String Vuforia.VirtualButtonAbstractBehaviour::mName
	String_t* ___mName_3;
	// Vuforia.VirtualButton/Sensitivity Vuforia.VirtualButtonAbstractBehaviour::mSensitivity
	int32_t ___mSensitivity_4;
	// System.Boolean Vuforia.VirtualButtonAbstractBehaviour::mHasUpdatedPose
	bool ___mHasUpdatedPose_5;
	// UnityEngine.Matrix4x4 Vuforia.VirtualButtonAbstractBehaviour::mPrevTransform
	Matrix4x4_t4266809202  ___mPrevTransform_6;
	// UnityEngine.GameObject Vuforia.VirtualButtonAbstractBehaviour::mPrevParent
	GameObject_t1811656094 * ___mPrevParent_7;
	// System.Boolean Vuforia.VirtualButtonAbstractBehaviour::mSensitivityDirty
	bool ___mSensitivityDirty_8;
	// Vuforia.VirtualButton/Sensitivity Vuforia.VirtualButtonAbstractBehaviour::mPreviousSensitivity
	int32_t ___mPreviousSensitivity_9;
	// System.Boolean Vuforia.VirtualButtonAbstractBehaviour::mPreviouslyEnabled
	bool ___mPreviouslyEnabled_10;
	// System.Boolean Vuforia.VirtualButtonAbstractBehaviour::mPressed
	bool ___mPressed_11;
	// System.Collections.Generic.List`1<Vuforia.IVirtualButtonEventHandler> Vuforia.VirtualButtonAbstractBehaviour::mHandlers
	List_1_t1800411770 * ___mHandlers_12;
	// UnityEngine.Vector2 Vuforia.VirtualButtonAbstractBehaviour::mLeftTop
	Vector2_t3577333262  ___mLeftTop_13;
	// UnityEngine.Vector2 Vuforia.VirtualButtonAbstractBehaviour::mRightBottom
	Vector2_t3577333262  ___mRightBottom_14;
	// System.Boolean Vuforia.VirtualButtonAbstractBehaviour::mUnregisterOnDestroy
	bool ___mUnregisterOnDestroy_15;
	// Vuforia.VirtualButton Vuforia.VirtualButtonAbstractBehaviour::mVirtualButton
	VirtualButton_t3666937711 * ___mVirtualButton_16;

public:
	inline static int32_t get_offset_of_mName_3() { return static_cast<int32_t>(offsetof(VirtualButtonAbstractBehaviour_t1965820348, ___mName_3)); }
	inline String_t* get_mName_3() const { return ___mName_3; }
	inline String_t** get_address_of_mName_3() { return &___mName_3; }
	inline void set_mName_3(String_t* value)
	{
		___mName_3 = value;
		Il2CppCodeGenWriteBarrier((&___mName_3), value);
	}

	inline static int32_t get_offset_of_mSensitivity_4() { return static_cast<int32_t>(offsetof(VirtualButtonAbstractBehaviour_t1965820348, ___mSensitivity_4)); }
	inline int32_t get_mSensitivity_4() const { return ___mSensitivity_4; }
	inline int32_t* get_address_of_mSensitivity_4() { return &___mSensitivity_4; }
	inline void set_mSensitivity_4(int32_t value)
	{
		___mSensitivity_4 = value;
	}

	inline static int32_t get_offset_of_mHasUpdatedPose_5() { return static_cast<int32_t>(offsetof(VirtualButtonAbstractBehaviour_t1965820348, ___mHasUpdatedPose_5)); }
	inline bool get_mHasUpdatedPose_5() const { return ___mHasUpdatedPose_5; }
	inline bool* get_address_of_mHasUpdatedPose_5() { return &___mHasUpdatedPose_5; }
	inline void set_mHasUpdatedPose_5(bool value)
	{
		___mHasUpdatedPose_5 = value;
	}

	inline static int32_t get_offset_of_mPrevTransform_6() { return static_cast<int32_t>(offsetof(VirtualButtonAbstractBehaviour_t1965820348, ___mPrevTransform_6)); }
	inline Matrix4x4_t4266809202  get_mPrevTransform_6() const { return ___mPrevTransform_6; }
	inline Matrix4x4_t4266809202 * get_address_of_mPrevTransform_6() { return &___mPrevTransform_6; }
	inline void set_mPrevTransform_6(Matrix4x4_t4266809202  value)
	{
		___mPrevTransform_6 = value;
	}

	inline static int32_t get_offset_of_mPrevParent_7() { return static_cast<int32_t>(offsetof(VirtualButtonAbstractBehaviour_t1965820348, ___mPrevParent_7)); }
	inline GameObject_t1811656094 * get_mPrevParent_7() const { return ___mPrevParent_7; }
	inline GameObject_t1811656094 ** get_address_of_mPrevParent_7() { return &___mPrevParent_7; }
	inline void set_mPrevParent_7(GameObject_t1811656094 * value)
	{
		___mPrevParent_7 = value;
		Il2CppCodeGenWriteBarrier((&___mPrevParent_7), value);
	}

	inline static int32_t get_offset_of_mSensitivityDirty_8() { return static_cast<int32_t>(offsetof(VirtualButtonAbstractBehaviour_t1965820348, ___mSensitivityDirty_8)); }
	inline bool get_mSensitivityDirty_8() const { return ___mSensitivityDirty_8; }
	inline bool* get_address_of_mSensitivityDirty_8() { return &___mSensitivityDirty_8; }
	inline void set_mSensitivityDirty_8(bool value)
	{
		___mSensitivityDirty_8 = value;
	}

	inline static int32_t get_offset_of_mPreviousSensitivity_9() { return static_cast<int32_t>(offsetof(VirtualButtonAbstractBehaviour_t1965820348, ___mPreviousSensitivity_9)); }
	inline int32_t get_mPreviousSensitivity_9() const { return ___mPreviousSensitivity_9; }
	inline int32_t* get_address_of_mPreviousSensitivity_9() { return &___mPreviousSensitivity_9; }
	inline void set_mPreviousSensitivity_9(int32_t value)
	{
		___mPreviousSensitivity_9 = value;
	}

	inline static int32_t get_offset_of_mPreviouslyEnabled_10() { return static_cast<int32_t>(offsetof(VirtualButtonAbstractBehaviour_t1965820348, ___mPreviouslyEnabled_10)); }
	inline bool get_mPreviouslyEnabled_10() const { return ___mPreviouslyEnabled_10; }
	inline bool* get_address_of_mPreviouslyEnabled_10() { return &___mPreviouslyEnabled_10; }
	inline void set_mPreviouslyEnabled_10(bool value)
	{
		___mPreviouslyEnabled_10 = value;
	}

	inline static int32_t get_offset_of_mPressed_11() { return static_cast<int32_t>(offsetof(VirtualButtonAbstractBehaviour_t1965820348, ___mPressed_11)); }
	inline bool get_mPressed_11() const { return ___mPressed_11; }
	inline bool* get_address_of_mPressed_11() { return &___mPressed_11; }
	inline void set_mPressed_11(bool value)
	{
		___mPressed_11 = value;
	}

	inline static int32_t get_offset_of_mHandlers_12() { return static_cast<int32_t>(offsetof(VirtualButtonAbstractBehaviour_t1965820348, ___mHandlers_12)); }
	inline List_1_t1800411770 * get_mHandlers_12() const { return ___mHandlers_12; }
	inline List_1_t1800411770 ** get_address_of_mHandlers_12() { return &___mHandlers_12; }
	inline void set_mHandlers_12(List_1_t1800411770 * value)
	{
		___mHandlers_12 = value;
		Il2CppCodeGenWriteBarrier((&___mHandlers_12), value);
	}

	inline static int32_t get_offset_of_mLeftTop_13() { return static_cast<int32_t>(offsetof(VirtualButtonAbstractBehaviour_t1965820348, ___mLeftTop_13)); }
	inline Vector2_t3577333262  get_mLeftTop_13() const { return ___mLeftTop_13; }
	inline Vector2_t3577333262 * get_address_of_mLeftTop_13() { return &___mLeftTop_13; }
	inline void set_mLeftTop_13(Vector2_t3577333262  value)
	{
		___mLeftTop_13 = value;
	}

	inline static int32_t get_offset_of_mRightBottom_14() { return static_cast<int32_t>(offsetof(VirtualButtonAbstractBehaviour_t1965820348, ___mRightBottom_14)); }
	inline Vector2_t3577333262  get_mRightBottom_14() const { return ___mRightBottom_14; }
	inline Vector2_t3577333262 * get_address_of_mRightBottom_14() { return &___mRightBottom_14; }
	inline void set_mRightBottom_14(Vector2_t3577333262  value)
	{
		___mRightBottom_14 = value;
	}

	inline static int32_t get_offset_of_mUnregisterOnDestroy_15() { return static_cast<int32_t>(offsetof(VirtualButtonAbstractBehaviour_t1965820348, ___mUnregisterOnDestroy_15)); }
	inline bool get_mUnregisterOnDestroy_15() const { return ___mUnregisterOnDestroy_15; }
	inline bool* get_address_of_mUnregisterOnDestroy_15() { return &___mUnregisterOnDestroy_15; }
	inline void set_mUnregisterOnDestroy_15(bool value)
	{
		___mUnregisterOnDestroy_15 = value;
	}

	inline static int32_t get_offset_of_mVirtualButton_16() { return static_cast<int32_t>(offsetof(VirtualButtonAbstractBehaviour_t1965820348, ___mVirtualButton_16)); }
	inline VirtualButton_t3666937711 * get_mVirtualButton_16() const { return ___mVirtualButton_16; }
	inline VirtualButton_t3666937711 ** get_address_of_mVirtualButton_16() { return &___mVirtualButton_16; }
	inline void set_mVirtualButton_16(VirtualButton_t3666937711 * value)
	{
		___mVirtualButton_16 = value;
		Il2CppCodeGenWriteBarrier((&___mVirtualButton_16), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VIRTUALBUTTONABSTRACTBEHAVIOUR_T1965820348_H
#ifndef TURNOFFABSTRACTBEHAVIOUR_T1066217039_H
#define TURNOFFABSTRACTBEHAVIOUR_T1066217039_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.TurnOffAbstractBehaviour
struct  TurnOffAbstractBehaviour_t1066217039  : public MonoBehaviour_t345688271
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TURNOFFABSTRACTBEHAVIOUR_T1066217039_H
#ifndef TRACKABLEBEHAVIOUR_T2419079356_H
#define TRACKABLEBEHAVIOUR_T2419079356_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.TrackableBehaviour
struct  TrackableBehaviour_t2419079356  : public MonoBehaviour_t345688271
{
public:
	// System.Double Vuforia.TrackableBehaviour::<TimeStamp>k__BackingField
	double ___U3CTimeStampU3Ek__BackingField_2;
	// System.String Vuforia.TrackableBehaviour::mTrackableName
	String_t* ___mTrackableName_3;
	// System.Boolean Vuforia.TrackableBehaviour::mPreserveChildSize
	bool ___mPreserveChildSize_4;
	// System.Boolean Vuforia.TrackableBehaviour::mInitializedInEditor
	bool ___mInitializedInEditor_5;
	// UnityEngine.Vector3 Vuforia.TrackableBehaviour::mPreviousScale
	Vector3_t2903530434  ___mPreviousScale_6;
	// Vuforia.TrackableBehaviour/Status Vuforia.TrackableBehaviour::mStatus
	int32_t ___mStatus_7;
	// Vuforia.Trackable Vuforia.TrackableBehaviour::mTrackable
	RuntimeObject* ___mTrackable_8;
	// System.Collections.Generic.List`1<Vuforia.ITrackableEventHandler> Vuforia.TrackableBehaviour::mTrackableEventHandlers
	List_1_t3589236026 * ___mTrackableEventHandlers_9;

public:
	inline static int32_t get_offset_of_U3CTimeStampU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(TrackableBehaviour_t2419079356, ___U3CTimeStampU3Ek__BackingField_2)); }
	inline double get_U3CTimeStampU3Ek__BackingField_2() const { return ___U3CTimeStampU3Ek__BackingField_2; }
	inline double* get_address_of_U3CTimeStampU3Ek__BackingField_2() { return &___U3CTimeStampU3Ek__BackingField_2; }
	inline void set_U3CTimeStampU3Ek__BackingField_2(double value)
	{
		___U3CTimeStampU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_mTrackableName_3() { return static_cast<int32_t>(offsetof(TrackableBehaviour_t2419079356, ___mTrackableName_3)); }
	inline String_t* get_mTrackableName_3() const { return ___mTrackableName_3; }
	inline String_t** get_address_of_mTrackableName_3() { return &___mTrackableName_3; }
	inline void set_mTrackableName_3(String_t* value)
	{
		___mTrackableName_3 = value;
		Il2CppCodeGenWriteBarrier((&___mTrackableName_3), value);
	}

	inline static int32_t get_offset_of_mPreserveChildSize_4() { return static_cast<int32_t>(offsetof(TrackableBehaviour_t2419079356, ___mPreserveChildSize_4)); }
	inline bool get_mPreserveChildSize_4() const { return ___mPreserveChildSize_4; }
	inline bool* get_address_of_mPreserveChildSize_4() { return &___mPreserveChildSize_4; }
	inline void set_mPreserveChildSize_4(bool value)
	{
		___mPreserveChildSize_4 = value;
	}

	inline static int32_t get_offset_of_mInitializedInEditor_5() { return static_cast<int32_t>(offsetof(TrackableBehaviour_t2419079356, ___mInitializedInEditor_5)); }
	inline bool get_mInitializedInEditor_5() const { return ___mInitializedInEditor_5; }
	inline bool* get_address_of_mInitializedInEditor_5() { return &___mInitializedInEditor_5; }
	inline void set_mInitializedInEditor_5(bool value)
	{
		___mInitializedInEditor_5 = value;
	}

	inline static int32_t get_offset_of_mPreviousScale_6() { return static_cast<int32_t>(offsetof(TrackableBehaviour_t2419079356, ___mPreviousScale_6)); }
	inline Vector3_t2903530434  get_mPreviousScale_6() const { return ___mPreviousScale_6; }
	inline Vector3_t2903530434 * get_address_of_mPreviousScale_6() { return &___mPreviousScale_6; }
	inline void set_mPreviousScale_6(Vector3_t2903530434  value)
	{
		___mPreviousScale_6 = value;
	}

	inline static int32_t get_offset_of_mStatus_7() { return static_cast<int32_t>(offsetof(TrackableBehaviour_t2419079356, ___mStatus_7)); }
	inline int32_t get_mStatus_7() const { return ___mStatus_7; }
	inline int32_t* get_address_of_mStatus_7() { return &___mStatus_7; }
	inline void set_mStatus_7(int32_t value)
	{
		___mStatus_7 = value;
	}

	inline static int32_t get_offset_of_mTrackable_8() { return static_cast<int32_t>(offsetof(TrackableBehaviour_t2419079356, ___mTrackable_8)); }
	inline RuntimeObject* get_mTrackable_8() const { return ___mTrackable_8; }
	inline RuntimeObject** get_address_of_mTrackable_8() { return &___mTrackable_8; }
	inline void set_mTrackable_8(RuntimeObject* value)
	{
		___mTrackable_8 = value;
		Il2CppCodeGenWriteBarrier((&___mTrackable_8), value);
	}

	inline static int32_t get_offset_of_mTrackableEventHandlers_9() { return static_cast<int32_t>(offsetof(TrackableBehaviour_t2419079356, ___mTrackableEventHandlers_9)); }
	inline List_1_t3589236026 * get_mTrackableEventHandlers_9() const { return ___mTrackableEventHandlers_9; }
	inline List_1_t3589236026 ** get_address_of_mTrackableEventHandlers_9() { return &___mTrackableEventHandlers_9; }
	inline void set_mTrackableEventHandlers_9(List_1_t3589236026 * value)
	{
		___mTrackableEventHandlers_9 = value;
		Il2CppCodeGenWriteBarrier((&___mTrackableEventHandlers_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRACKABLEBEHAVIOUR_T2419079356_H
#ifndef DATASETTRACKABLEBEHAVIOUR_T3960979584_H
#define DATASETTRACKABLEBEHAVIOUR_T3960979584_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.DataSetTrackableBehaviour
struct  DataSetTrackableBehaviour_t3960979584  : public TrackableBehaviour_t2419079356
{
public:
	// System.String Vuforia.DataSetTrackableBehaviour::mDataSetPath
	String_t* ___mDataSetPath_10;
	// System.Boolean Vuforia.DataSetTrackableBehaviour::mExtendedTracking
	bool ___mExtendedTracking_11;
	// System.Boolean Vuforia.DataSetTrackableBehaviour::mInitializeSmartTerrain
	bool ___mInitializeSmartTerrain_12;
	// Vuforia.ReconstructionFromTargetAbstractBehaviour Vuforia.DataSetTrackableBehaviour::mReconstructionToInitialize
	ReconstructionFromTargetAbstractBehaviour_t2676804737 * ___mReconstructionToInitialize_13;
	// UnityEngine.Vector3 Vuforia.DataSetTrackableBehaviour::mSmartTerrainOccluderBoundsMin
	Vector3_t2903530434  ___mSmartTerrainOccluderBoundsMin_14;
	// UnityEngine.Vector3 Vuforia.DataSetTrackableBehaviour::mSmartTerrainOccluderBoundsMax
	Vector3_t2903530434  ___mSmartTerrainOccluderBoundsMax_15;
	// System.Boolean Vuforia.DataSetTrackableBehaviour::mIsSmartTerrainOccluderOffset
	bool ___mIsSmartTerrainOccluderOffset_16;
	// UnityEngine.Vector3 Vuforia.DataSetTrackableBehaviour::mSmartTerrainOccluderOffset
	Vector3_t2903530434  ___mSmartTerrainOccluderOffset_17;
	// UnityEngine.Quaternion Vuforia.DataSetTrackableBehaviour::mSmartTerrainOccluderRotation
	Quaternion_t754065749  ___mSmartTerrainOccluderRotation_18;
	// System.Boolean Vuforia.DataSetTrackableBehaviour::mAutoSetOccluderFromTargetSize
	bool ___mAutoSetOccluderFromTargetSize_19;

public:
	inline static int32_t get_offset_of_mDataSetPath_10() { return static_cast<int32_t>(offsetof(DataSetTrackableBehaviour_t3960979584, ___mDataSetPath_10)); }
	inline String_t* get_mDataSetPath_10() const { return ___mDataSetPath_10; }
	inline String_t** get_address_of_mDataSetPath_10() { return &___mDataSetPath_10; }
	inline void set_mDataSetPath_10(String_t* value)
	{
		___mDataSetPath_10 = value;
		Il2CppCodeGenWriteBarrier((&___mDataSetPath_10), value);
	}

	inline static int32_t get_offset_of_mExtendedTracking_11() { return static_cast<int32_t>(offsetof(DataSetTrackableBehaviour_t3960979584, ___mExtendedTracking_11)); }
	inline bool get_mExtendedTracking_11() const { return ___mExtendedTracking_11; }
	inline bool* get_address_of_mExtendedTracking_11() { return &___mExtendedTracking_11; }
	inline void set_mExtendedTracking_11(bool value)
	{
		___mExtendedTracking_11 = value;
	}

	inline static int32_t get_offset_of_mInitializeSmartTerrain_12() { return static_cast<int32_t>(offsetof(DataSetTrackableBehaviour_t3960979584, ___mInitializeSmartTerrain_12)); }
	inline bool get_mInitializeSmartTerrain_12() const { return ___mInitializeSmartTerrain_12; }
	inline bool* get_address_of_mInitializeSmartTerrain_12() { return &___mInitializeSmartTerrain_12; }
	inline void set_mInitializeSmartTerrain_12(bool value)
	{
		___mInitializeSmartTerrain_12 = value;
	}

	inline static int32_t get_offset_of_mReconstructionToInitialize_13() { return static_cast<int32_t>(offsetof(DataSetTrackableBehaviour_t3960979584, ___mReconstructionToInitialize_13)); }
	inline ReconstructionFromTargetAbstractBehaviour_t2676804737 * get_mReconstructionToInitialize_13() const { return ___mReconstructionToInitialize_13; }
	inline ReconstructionFromTargetAbstractBehaviour_t2676804737 ** get_address_of_mReconstructionToInitialize_13() { return &___mReconstructionToInitialize_13; }
	inline void set_mReconstructionToInitialize_13(ReconstructionFromTargetAbstractBehaviour_t2676804737 * value)
	{
		___mReconstructionToInitialize_13 = value;
		Il2CppCodeGenWriteBarrier((&___mReconstructionToInitialize_13), value);
	}

	inline static int32_t get_offset_of_mSmartTerrainOccluderBoundsMin_14() { return static_cast<int32_t>(offsetof(DataSetTrackableBehaviour_t3960979584, ___mSmartTerrainOccluderBoundsMin_14)); }
	inline Vector3_t2903530434  get_mSmartTerrainOccluderBoundsMin_14() const { return ___mSmartTerrainOccluderBoundsMin_14; }
	inline Vector3_t2903530434 * get_address_of_mSmartTerrainOccluderBoundsMin_14() { return &___mSmartTerrainOccluderBoundsMin_14; }
	inline void set_mSmartTerrainOccluderBoundsMin_14(Vector3_t2903530434  value)
	{
		___mSmartTerrainOccluderBoundsMin_14 = value;
	}

	inline static int32_t get_offset_of_mSmartTerrainOccluderBoundsMax_15() { return static_cast<int32_t>(offsetof(DataSetTrackableBehaviour_t3960979584, ___mSmartTerrainOccluderBoundsMax_15)); }
	inline Vector3_t2903530434  get_mSmartTerrainOccluderBoundsMax_15() const { return ___mSmartTerrainOccluderBoundsMax_15; }
	inline Vector3_t2903530434 * get_address_of_mSmartTerrainOccluderBoundsMax_15() { return &___mSmartTerrainOccluderBoundsMax_15; }
	inline void set_mSmartTerrainOccluderBoundsMax_15(Vector3_t2903530434  value)
	{
		___mSmartTerrainOccluderBoundsMax_15 = value;
	}

	inline static int32_t get_offset_of_mIsSmartTerrainOccluderOffset_16() { return static_cast<int32_t>(offsetof(DataSetTrackableBehaviour_t3960979584, ___mIsSmartTerrainOccluderOffset_16)); }
	inline bool get_mIsSmartTerrainOccluderOffset_16() const { return ___mIsSmartTerrainOccluderOffset_16; }
	inline bool* get_address_of_mIsSmartTerrainOccluderOffset_16() { return &___mIsSmartTerrainOccluderOffset_16; }
	inline void set_mIsSmartTerrainOccluderOffset_16(bool value)
	{
		___mIsSmartTerrainOccluderOffset_16 = value;
	}

	inline static int32_t get_offset_of_mSmartTerrainOccluderOffset_17() { return static_cast<int32_t>(offsetof(DataSetTrackableBehaviour_t3960979584, ___mSmartTerrainOccluderOffset_17)); }
	inline Vector3_t2903530434  get_mSmartTerrainOccluderOffset_17() const { return ___mSmartTerrainOccluderOffset_17; }
	inline Vector3_t2903530434 * get_address_of_mSmartTerrainOccluderOffset_17() { return &___mSmartTerrainOccluderOffset_17; }
	inline void set_mSmartTerrainOccluderOffset_17(Vector3_t2903530434  value)
	{
		___mSmartTerrainOccluderOffset_17 = value;
	}

	inline static int32_t get_offset_of_mSmartTerrainOccluderRotation_18() { return static_cast<int32_t>(offsetof(DataSetTrackableBehaviour_t3960979584, ___mSmartTerrainOccluderRotation_18)); }
	inline Quaternion_t754065749  get_mSmartTerrainOccluderRotation_18() const { return ___mSmartTerrainOccluderRotation_18; }
	inline Quaternion_t754065749 * get_address_of_mSmartTerrainOccluderRotation_18() { return &___mSmartTerrainOccluderRotation_18; }
	inline void set_mSmartTerrainOccluderRotation_18(Quaternion_t754065749  value)
	{
		___mSmartTerrainOccluderRotation_18 = value;
	}

	inline static int32_t get_offset_of_mAutoSetOccluderFromTargetSize_19() { return static_cast<int32_t>(offsetof(DataSetTrackableBehaviour_t3960979584, ___mAutoSetOccluderFromTargetSize_19)); }
	inline bool get_mAutoSetOccluderFromTargetSize_19() const { return ___mAutoSetOccluderFromTargetSize_19; }
	inline bool* get_address_of_mAutoSetOccluderFromTargetSize_19() { return &___mAutoSetOccluderFromTargetSize_19; }
	inline void set_mAutoSetOccluderFromTargetSize_19(bool value)
	{
		___mAutoSetOccluderFromTargetSize_19 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATASETTRACKABLEBEHAVIOUR_T3960979584_H
#ifndef SMARTTERRAINTRACKABLEBEHAVIOUR_T4179131702_H
#define SMARTTERRAINTRACKABLEBEHAVIOUR_T4179131702_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.SmartTerrainTrackableBehaviour
struct  SmartTerrainTrackableBehaviour_t4179131702  : public TrackableBehaviour_t2419079356
{
public:
	// Vuforia.SmartTerrainTrackable Vuforia.SmartTerrainTrackableBehaviour::mSmartTerrainTrackable
	RuntimeObject* ___mSmartTerrainTrackable_10;
	// System.Boolean Vuforia.SmartTerrainTrackableBehaviour::mDisableAutomaticUpdates
	bool ___mDisableAutomaticUpdates_11;
	// UnityEngine.MeshFilter Vuforia.SmartTerrainTrackableBehaviour::mMeshFilterToUpdate
	MeshFilter_t137687116 * ___mMeshFilterToUpdate_12;
	// UnityEngine.MeshCollider Vuforia.SmartTerrainTrackableBehaviour::mMeshColliderToUpdate
	MeshCollider_t4172739389 * ___mMeshColliderToUpdate_13;

public:
	inline static int32_t get_offset_of_mSmartTerrainTrackable_10() { return static_cast<int32_t>(offsetof(SmartTerrainTrackableBehaviour_t4179131702, ___mSmartTerrainTrackable_10)); }
	inline RuntimeObject* get_mSmartTerrainTrackable_10() const { return ___mSmartTerrainTrackable_10; }
	inline RuntimeObject** get_address_of_mSmartTerrainTrackable_10() { return &___mSmartTerrainTrackable_10; }
	inline void set_mSmartTerrainTrackable_10(RuntimeObject* value)
	{
		___mSmartTerrainTrackable_10 = value;
		Il2CppCodeGenWriteBarrier((&___mSmartTerrainTrackable_10), value);
	}

	inline static int32_t get_offset_of_mDisableAutomaticUpdates_11() { return static_cast<int32_t>(offsetof(SmartTerrainTrackableBehaviour_t4179131702, ___mDisableAutomaticUpdates_11)); }
	inline bool get_mDisableAutomaticUpdates_11() const { return ___mDisableAutomaticUpdates_11; }
	inline bool* get_address_of_mDisableAutomaticUpdates_11() { return &___mDisableAutomaticUpdates_11; }
	inline void set_mDisableAutomaticUpdates_11(bool value)
	{
		___mDisableAutomaticUpdates_11 = value;
	}

	inline static int32_t get_offset_of_mMeshFilterToUpdate_12() { return static_cast<int32_t>(offsetof(SmartTerrainTrackableBehaviour_t4179131702, ___mMeshFilterToUpdate_12)); }
	inline MeshFilter_t137687116 * get_mMeshFilterToUpdate_12() const { return ___mMeshFilterToUpdate_12; }
	inline MeshFilter_t137687116 ** get_address_of_mMeshFilterToUpdate_12() { return &___mMeshFilterToUpdate_12; }
	inline void set_mMeshFilterToUpdate_12(MeshFilter_t137687116 * value)
	{
		___mMeshFilterToUpdate_12 = value;
		Il2CppCodeGenWriteBarrier((&___mMeshFilterToUpdate_12), value);
	}

	inline static int32_t get_offset_of_mMeshColliderToUpdate_13() { return static_cast<int32_t>(offsetof(SmartTerrainTrackableBehaviour_t4179131702, ___mMeshColliderToUpdate_13)); }
	inline MeshCollider_t4172739389 * get_mMeshColliderToUpdate_13() const { return ___mMeshColliderToUpdate_13; }
	inline MeshCollider_t4172739389 ** get_address_of_mMeshColliderToUpdate_13() { return &___mMeshColliderToUpdate_13; }
	inline void set_mMeshColliderToUpdate_13(MeshCollider_t4172739389 * value)
	{
		___mMeshColliderToUpdate_13 = value;
		Il2CppCodeGenWriteBarrier((&___mMeshColliderToUpdate_13), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SMARTTERRAINTRACKABLEBEHAVIOUR_T4179131702_H
#ifndef WORDABSTRACTBEHAVIOUR_T2339709601_H
#define WORDABSTRACTBEHAVIOUR_T2339709601_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.WordAbstractBehaviour
struct  WordAbstractBehaviour_t2339709601  : public TrackableBehaviour_t2419079356
{
public:
	// Vuforia.WordTemplateMode Vuforia.WordAbstractBehaviour::mMode
	int32_t ___mMode_10;
	// System.String Vuforia.WordAbstractBehaviour::mSpecificWord
	String_t* ___mSpecificWord_11;
	// Vuforia.Word Vuforia.WordAbstractBehaviour::mWord
	RuntimeObject* ___mWord_12;

public:
	inline static int32_t get_offset_of_mMode_10() { return static_cast<int32_t>(offsetof(WordAbstractBehaviour_t2339709601, ___mMode_10)); }
	inline int32_t get_mMode_10() const { return ___mMode_10; }
	inline int32_t* get_address_of_mMode_10() { return &___mMode_10; }
	inline void set_mMode_10(int32_t value)
	{
		___mMode_10 = value;
	}

	inline static int32_t get_offset_of_mSpecificWord_11() { return static_cast<int32_t>(offsetof(WordAbstractBehaviour_t2339709601, ___mSpecificWord_11)); }
	inline String_t* get_mSpecificWord_11() const { return ___mSpecificWord_11; }
	inline String_t** get_address_of_mSpecificWord_11() { return &___mSpecificWord_11; }
	inline void set_mSpecificWord_11(String_t* value)
	{
		___mSpecificWord_11 = value;
		Il2CppCodeGenWriteBarrier((&___mSpecificWord_11), value);
	}

	inline static int32_t get_offset_of_mWord_12() { return static_cast<int32_t>(offsetof(WordAbstractBehaviour_t2339709601, ___mWord_12)); }
	inline RuntimeObject* get_mWord_12() const { return ___mWord_12; }
	inline RuntimeObject** get_address_of_mWord_12() { return &___mWord_12; }
	inline void set_mWord_12(RuntimeObject* value)
	{
		___mWord_12 = value;
		Il2CppCodeGenWriteBarrier((&___mWord_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WORDABSTRACTBEHAVIOUR_T2339709601_H
#ifndef MULTITARGETABSTRACTBEHAVIOUR_T1527469731_H
#define MULTITARGETABSTRACTBEHAVIOUR_T1527469731_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.MultiTargetAbstractBehaviour
struct  MultiTargetAbstractBehaviour_t1527469731  : public DataSetTrackableBehaviour_t3960979584
{
public:
	// Vuforia.MultiTarget Vuforia.MultiTargetAbstractBehaviour::mMultiTarget
	RuntimeObject* ___mMultiTarget_20;

public:
	inline static int32_t get_offset_of_mMultiTarget_20() { return static_cast<int32_t>(offsetof(MultiTargetAbstractBehaviour_t1527469731, ___mMultiTarget_20)); }
	inline RuntimeObject* get_mMultiTarget_20() const { return ___mMultiTarget_20; }
	inline RuntimeObject** get_address_of_mMultiTarget_20() { return &___mMultiTarget_20; }
	inline void set_mMultiTarget_20(RuntimeObject* value)
	{
		___mMultiTarget_20 = value;
		Il2CppCodeGenWriteBarrier((&___mMultiTarget_20), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MULTITARGETABSTRACTBEHAVIOUR_T1527469731_H
#ifndef PROPABSTRACTBEHAVIOUR_T2648882166_H
#define PROPABSTRACTBEHAVIOUR_T2648882166_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.PropAbstractBehaviour
struct  PropAbstractBehaviour_t2648882166  : public SmartTerrainTrackableBehaviour_t4179131702
{
public:
	// Vuforia.Prop Vuforia.PropAbstractBehaviour::mProp
	RuntimeObject* ___mProp_14;
	// UnityEngine.BoxCollider Vuforia.PropAbstractBehaviour::mBoxColliderToUpdate
	BoxCollider_t1861275623 * ___mBoxColliderToUpdate_15;

public:
	inline static int32_t get_offset_of_mProp_14() { return static_cast<int32_t>(offsetof(PropAbstractBehaviour_t2648882166, ___mProp_14)); }
	inline RuntimeObject* get_mProp_14() const { return ___mProp_14; }
	inline RuntimeObject** get_address_of_mProp_14() { return &___mProp_14; }
	inline void set_mProp_14(RuntimeObject* value)
	{
		___mProp_14 = value;
		Il2CppCodeGenWriteBarrier((&___mProp_14), value);
	}

	inline static int32_t get_offset_of_mBoxColliderToUpdate_15() { return static_cast<int32_t>(offsetof(PropAbstractBehaviour_t2648882166, ___mBoxColliderToUpdate_15)); }
	inline BoxCollider_t1861275623 * get_mBoxColliderToUpdate_15() const { return ___mBoxColliderToUpdate_15; }
	inline BoxCollider_t1861275623 ** get_address_of_mBoxColliderToUpdate_15() { return &___mBoxColliderToUpdate_15; }
	inline void set_mBoxColliderToUpdate_15(BoxCollider_t1861275623 * value)
	{
		___mBoxColliderToUpdate_15 = value;
		Il2CppCodeGenWriteBarrier((&___mBoxColliderToUpdate_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROPABSTRACTBEHAVIOUR_T2648882166_H
#ifndef IMAGETARGETABSTRACTBEHAVIOUR_T355543434_H
#define IMAGETARGETABSTRACTBEHAVIOUR_T355543434_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.ImageTargetAbstractBehaviour
struct  ImageTargetAbstractBehaviour_t355543434  : public DataSetTrackableBehaviour_t3960979584
{
public:
	// System.Single Vuforia.ImageTargetAbstractBehaviour::mAspectRatio
	float ___mAspectRatio_20;
	// Vuforia.ImageTargetType Vuforia.ImageTargetAbstractBehaviour::mImageTargetType
	int32_t ___mImageTargetType_21;
	// System.Single Vuforia.ImageTargetAbstractBehaviour::mWidth
	float ___mWidth_22;
	// System.Single Vuforia.ImageTargetAbstractBehaviour::mHeight
	float ___mHeight_23;
	// Vuforia.ImageTarget Vuforia.ImageTargetAbstractBehaviour::mImageTarget
	RuntimeObject* ___mImageTarget_24;
	// System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VirtualButtonAbstractBehaviour> Vuforia.ImageTargetAbstractBehaviour::mVirtualButtonBehaviours
	Dictionary_2_t3161827512 * ___mVirtualButtonBehaviours_25;
	// System.Single Vuforia.ImageTargetAbstractBehaviour::mLastTransformScale
	float ___mLastTransformScale_26;
	// UnityEngine.Vector2 Vuforia.ImageTargetAbstractBehaviour::mLastSize
	Vector2_t3577333262  ___mLastSize_27;

public:
	inline static int32_t get_offset_of_mAspectRatio_20() { return static_cast<int32_t>(offsetof(ImageTargetAbstractBehaviour_t355543434, ___mAspectRatio_20)); }
	inline float get_mAspectRatio_20() const { return ___mAspectRatio_20; }
	inline float* get_address_of_mAspectRatio_20() { return &___mAspectRatio_20; }
	inline void set_mAspectRatio_20(float value)
	{
		___mAspectRatio_20 = value;
	}

	inline static int32_t get_offset_of_mImageTargetType_21() { return static_cast<int32_t>(offsetof(ImageTargetAbstractBehaviour_t355543434, ___mImageTargetType_21)); }
	inline int32_t get_mImageTargetType_21() const { return ___mImageTargetType_21; }
	inline int32_t* get_address_of_mImageTargetType_21() { return &___mImageTargetType_21; }
	inline void set_mImageTargetType_21(int32_t value)
	{
		___mImageTargetType_21 = value;
	}

	inline static int32_t get_offset_of_mWidth_22() { return static_cast<int32_t>(offsetof(ImageTargetAbstractBehaviour_t355543434, ___mWidth_22)); }
	inline float get_mWidth_22() const { return ___mWidth_22; }
	inline float* get_address_of_mWidth_22() { return &___mWidth_22; }
	inline void set_mWidth_22(float value)
	{
		___mWidth_22 = value;
	}

	inline static int32_t get_offset_of_mHeight_23() { return static_cast<int32_t>(offsetof(ImageTargetAbstractBehaviour_t355543434, ___mHeight_23)); }
	inline float get_mHeight_23() const { return ___mHeight_23; }
	inline float* get_address_of_mHeight_23() { return &___mHeight_23; }
	inline void set_mHeight_23(float value)
	{
		___mHeight_23 = value;
	}

	inline static int32_t get_offset_of_mImageTarget_24() { return static_cast<int32_t>(offsetof(ImageTargetAbstractBehaviour_t355543434, ___mImageTarget_24)); }
	inline RuntimeObject* get_mImageTarget_24() const { return ___mImageTarget_24; }
	inline RuntimeObject** get_address_of_mImageTarget_24() { return &___mImageTarget_24; }
	inline void set_mImageTarget_24(RuntimeObject* value)
	{
		___mImageTarget_24 = value;
		Il2CppCodeGenWriteBarrier((&___mImageTarget_24), value);
	}

	inline static int32_t get_offset_of_mVirtualButtonBehaviours_25() { return static_cast<int32_t>(offsetof(ImageTargetAbstractBehaviour_t355543434, ___mVirtualButtonBehaviours_25)); }
	inline Dictionary_2_t3161827512 * get_mVirtualButtonBehaviours_25() const { return ___mVirtualButtonBehaviours_25; }
	inline Dictionary_2_t3161827512 ** get_address_of_mVirtualButtonBehaviours_25() { return &___mVirtualButtonBehaviours_25; }
	inline void set_mVirtualButtonBehaviours_25(Dictionary_2_t3161827512 * value)
	{
		___mVirtualButtonBehaviours_25 = value;
		Il2CppCodeGenWriteBarrier((&___mVirtualButtonBehaviours_25), value);
	}

	inline static int32_t get_offset_of_mLastTransformScale_26() { return static_cast<int32_t>(offsetof(ImageTargetAbstractBehaviour_t355543434, ___mLastTransformScale_26)); }
	inline float get_mLastTransformScale_26() const { return ___mLastTransformScale_26; }
	inline float* get_address_of_mLastTransformScale_26() { return &___mLastTransformScale_26; }
	inline void set_mLastTransformScale_26(float value)
	{
		___mLastTransformScale_26 = value;
	}

	inline static int32_t get_offset_of_mLastSize_27() { return static_cast<int32_t>(offsetof(ImageTargetAbstractBehaviour_t355543434, ___mLastSize_27)); }
	inline Vector2_t3577333262  get_mLastSize_27() const { return ___mLastSize_27; }
	inline Vector2_t3577333262 * get_address_of_mLastSize_27() { return &___mLastSize_27; }
	inline void set_mLastSize_27(Vector2_t3577333262  value)
	{
		___mLastSize_27 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IMAGETARGETABSTRACTBEHAVIOUR_T355543434_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2000 = { sizeof (WordListImpl_t3600997366), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2001 = { sizeof (VuforiaNullWrapper_t2276749828), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2002 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2003 = { sizeof (VuforiaNativeWrapper_t4186723445), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2004 = { sizeof (VuforiaWrapper_t3746480205), -1, sizeof(VuforiaWrapper_t3746480205_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2004[2] = 
{
	VuforiaWrapper_t3746480205_StaticFields::get_offset_of_sWrapper_0(),
	VuforiaWrapper_t3746480205_StaticFields::get_offset_of_sCamIndependentWrapper_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2005 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2006 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2007 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2008 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2009 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2010 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2011 = { sizeof (ReconstructionAbstractBehaviour_t954468633), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2011[22] = 
{
	ReconstructionAbstractBehaviour_t954468633::get_offset_of_mHasInitialized_2(),
	ReconstructionAbstractBehaviour_t954468633::get_offset_of_mSmartTerrainEventHandlers_3(),
	ReconstructionAbstractBehaviour_t954468633::get_offset_of_mOnInitialized_4(),
	ReconstructionAbstractBehaviour_t954468633::get_offset_of_mOnPropCreated_5(),
	ReconstructionAbstractBehaviour_t954468633::get_offset_of_mOnPropUpdated_6(),
	ReconstructionAbstractBehaviour_t954468633::get_offset_of_mOnPropDeleted_7(),
	ReconstructionAbstractBehaviour_t954468633::get_offset_of_mOnSurfaceCreated_8(),
	ReconstructionAbstractBehaviour_t954468633::get_offset_of_mOnSurfaceUpdated_9(),
	ReconstructionAbstractBehaviour_t954468633::get_offset_of_mOnSurfaceDeleted_10(),
	ReconstructionAbstractBehaviour_t954468633::get_offset_of_mInitializedInEditor_11(),
	ReconstructionAbstractBehaviour_t954468633::get_offset_of_mMaximumExtentEnabled_12(),
	ReconstructionAbstractBehaviour_t954468633::get_offset_of_mMaximumExtent_13(),
	ReconstructionAbstractBehaviour_t954468633::get_offset_of_mAutomaticStart_14(),
	ReconstructionAbstractBehaviour_t954468633::get_offset_of_mNavMeshUpdates_15(),
	ReconstructionAbstractBehaviour_t954468633::get_offset_of_mNavMeshPadding_16(),
	ReconstructionAbstractBehaviour_t954468633::get_offset_of_mReconstruction_17(),
	ReconstructionAbstractBehaviour_t954468633::get_offset_of_mSurfaces_18(),
	ReconstructionAbstractBehaviour_t954468633::get_offset_of_mActiveSurfaceBehaviours_19(),
	ReconstructionAbstractBehaviour_t954468633::get_offset_of_mProps_20(),
	ReconstructionAbstractBehaviour_t954468633::get_offset_of_mActivePropBehaviours_21(),
	ReconstructionAbstractBehaviour_t954468633::get_offset_of_mPreviouslySetWorldCenterSurfaceTemplate_22(),
	ReconstructionAbstractBehaviour_t954468633::get_offset_of_mIgnoreNextUpdate_23(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2012 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2013 = { sizeof (PropAbstractBehaviour_t2648882166), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2013[2] = 
{
	PropAbstractBehaviour_t2648882166::get_offset_of_mProp_14(),
	PropAbstractBehaviour_t2648882166::get_offset_of_mBoxColliderToUpdate_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2014 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2015 = { sizeof (SmartTerrainTracker_t1505679812), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2016 = { sizeof (StateManager_t231326187), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2017 = { sizeof (StateManagerImpl_t351768961), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2017[9] = 
{
	StateManagerImpl_t351768961::get_offset_of_mTrackableBehaviours_0(),
	StateManagerImpl_t351768961::get_offset_of_mAutomaticallyCreatedBehaviours_1(),
	StateManagerImpl_t351768961::get_offset_of_mBehavioursMarkedForDeletion_2(),
	StateManagerImpl_t351768961::get_offset_of_mActiveTrackableBehaviours_3(),
	StateManagerImpl_t351768961::get_offset_of_mWordManager_4(),
	StateManagerImpl_t351768961::get_offset_of_mVuMarkManager_5(),
	StateManagerImpl_t351768961::get_offset_of_mDeviceTrackingManager_6(),
	StateManagerImpl_t351768961::get_offset_of_mCameraPositioningHelper_7(),
	StateManagerImpl_t351768961::get_offset_of_mExtendedTrackingManager_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2018 = { sizeof (TargetFinderImpl_t3318134284), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2018[4] = 
{
	TargetFinderImpl_t3318134284::get_offset_of_mTargetFinderStatePtr_0(),
	TargetFinderImpl_t3318134284::get_offset_of_mTargetFinderState_1(),
	TargetFinderImpl_t3318134284::get_offset_of_mNewResults_2(),
	TargetFinderImpl_t3318134284::get_offset_of_mImageTargets_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2019 = { sizeof (TargetFinderState_t163528969)+ sizeof (RuntimeObject), sizeof(TargetFinderState_t163528969 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2019[4] = 
{
	TargetFinderState_t163528969::get_offset_of_IsRequesting_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TargetFinderState_t163528969::get_offset_of_UpdateState_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TargetFinderState_t163528969::get_offset_of_ResultCount_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TargetFinderState_t163528969::get_offset_of_unused_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2020 = { sizeof (InternalTargetSearchResult_t377068014)+ sizeof (RuntimeObject), sizeof(InternalTargetSearchResult_t377068014 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2020[6] = 
{
	InternalTargetSearchResult_t377068014::get_offset_of_TargetNamePtr_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	InternalTargetSearchResult_t377068014::get_offset_of_UniqueTargetIdPtr_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	InternalTargetSearchResult_t377068014::get_offset_of_MetaDataPtr_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	InternalTargetSearchResult_t377068014::get_offset_of_TargetSearchResultPtr_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	InternalTargetSearchResult_t377068014::get_offset_of_TargetSize_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	InternalTargetSearchResult_t377068014::get_offset_of_TrackingRating_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2021 = { sizeof (TrackableSourceImpl_t2372561227), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2021[1] = 
{
	TrackableSourceImpl_t2372561227::get_offset_of_U3CTrackableSourcePtrU3Ek__BackingField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2022 = { sizeof (TextureRenderer_t2855676147), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2022[3] = 
{
	TextureRenderer_t2855676147::get_offset_of_mTextureBufferCamera_0(),
	TextureRenderer_t2855676147::get_offset_of_mTextureWidth_1(),
	TextureRenderer_t2855676147::get_offset_of_mTextureHeight_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2023 = { sizeof (TrackableImpl_t3450720819), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2023[2] = 
{
	TrackableImpl_t3450720819::get_offset_of_U3CNameU3Ek__BackingField_0(),
	TrackableImpl_t3450720819::get_offset_of_U3CIDU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2024 = { sizeof (TrackerManagerImpl_t3117777030), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2024[5] = 
{
	TrackerManagerImpl_t3117777030::get_offset_of_mObjectTracker_1(),
	TrackerManagerImpl_t3117777030::get_offset_of_mTextTracker_2(),
	TrackerManagerImpl_t3117777030::get_offset_of_mSmartTerrainTracker_3(),
	TrackerManagerImpl_t3117777030::get_offset_of_mDeviceTracker_4(),
	TrackerManagerImpl_t3117777030::get_offset_of_mStateManager_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2025 = { sizeof (VirtualButtonImpl_t2261432068), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2025[6] = 
{
	VirtualButtonImpl_t2261432068::get_offset_of_mName_1(),
	VirtualButtonImpl_t2261432068::get_offset_of_mID_2(),
	VirtualButtonImpl_t2261432068::get_offset_of_mArea_3(),
	VirtualButtonImpl_t2261432068::get_offset_of_mIsEnabled_4(),
	VirtualButtonImpl_t2261432068::get_offset_of_mParentImageTarget_5(),
	VirtualButtonImpl_t2261432068::get_offset_of_mParentDataSet_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2026 = { sizeof (WebCamImpl_t90372774), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2026[15] = 
{
	WebCamImpl_t90372774::get_offset_of_mARCameras_0(),
	WebCamImpl_t90372774::get_offset_of_mOriginalCameraCullMask_1(),
	WebCamImpl_t90372774::get_offset_of_mWebCamTexture_2(),
	WebCamImpl_t90372774::get_offset_of_mVideoModeData_3(),
	WebCamImpl_t90372774::get_offset_of_mVideoTextureInfo_4(),
	WebCamImpl_t90372774::get_offset_of_mTextureRenderer_5(),
	WebCamImpl_t90372774::get_offset_of_mBufferReadTexture_6(),
	WebCamImpl_t90372774::get_offset_of_mReadPixelsRect_7(),
	WebCamImpl_t90372774::get_offset_of_mWebCamProfile_8(),
	WebCamImpl_t90372774::get_offset_of_mFlipHorizontally_9(),
	WebCamImpl_t90372774::get_offset_of_mIsDirty_10(),
	WebCamImpl_t90372774::get_offset_of_mLastFrameIdx_11(),
	WebCamImpl_t90372774::get_offset_of_mRenderTextureLayer_12(),
	WebCamImpl_t90372774::get_offset_of_mWebcamPlaying_13(),
	WebCamImpl_t90372774::get_offset_of_U3CIsTextureSizeAvailableU3Ek__BackingField_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2027 = { sizeof (WebCamProfile_t3641218551), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2027[1] = 
{
	WebCamProfile_t3641218551::get_offset_of_mProfileCollection_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2028 = { sizeof (ProfileData_t740879429)+ sizeof (RuntimeObject), sizeof(ProfileData_t740879429 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2028[3] = 
{
	ProfileData_t740879429::get_offset_of_RequestedTextureSize_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ProfileData_t740879429::get_offset_of_ResampledTextureSize_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ProfileData_t740879429::get_offset_of_RequestedFPS_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2029 = { sizeof (ProfileCollection_t3131405081)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2029[2] = 
{
	ProfileCollection_t3131405081::get_offset_of_DefaultProfile_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ProfileCollection_t3131405081::get_offset_of_Profiles_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2030 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2031 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2032 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2033 = { sizeof (Image_t1445313791), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2034 = { sizeof (PIXEL_FORMAT_t1585627136)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2034[7] = 
{
	PIXEL_FORMAT_t1585627136::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2035 = { sizeof (ImageTargetAbstractBehaviour_t355543434), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2035[8] = 
{
	ImageTargetAbstractBehaviour_t355543434::get_offset_of_mAspectRatio_20(),
	ImageTargetAbstractBehaviour_t355543434::get_offset_of_mImageTargetType_21(),
	ImageTargetAbstractBehaviour_t355543434::get_offset_of_mWidth_22(),
	ImageTargetAbstractBehaviour_t355543434::get_offset_of_mHeight_23(),
	ImageTargetAbstractBehaviour_t355543434::get_offset_of_mImageTarget_24(),
	ImageTargetAbstractBehaviour_t355543434::get_offset_of_mVirtualButtonBehaviours_25(),
	ImageTargetAbstractBehaviour_t355543434::get_offset_of_mLastTransformScale_26(),
	ImageTargetAbstractBehaviour_t355543434::get_offset_of_mLastSize_27(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2036 = { sizeof (ObjectTracker_t4023638979), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2037 = { sizeof (MaskOutAbstractBehaviour_t2174599097), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2037[1] = 
{
	MaskOutAbstractBehaviour_t2174599097::get_offset_of_maskMaterial_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2038 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2039 = { sizeof (MultiTargetAbstractBehaviour_t1527469731), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2039[1] = 
{
	MultiTargetAbstractBehaviour_t1527469731::get_offset_of_mMultiTarget_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2040 = { sizeof (VuforiaUnity_t3212967305), -1, sizeof(VuforiaUnity_t3212967305_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2040[1] = 
{
	VuforiaUnity_t3212967305_StaticFields::get_offset_of_mHoloLensApiAbstraction_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2041 = { sizeof (InitError_t3486678342)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2041[12] = 
{
	InitError_t3486678342::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2042 = { sizeof (VuforiaHint_t1542517147)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2042[4] = 
{
	VuforiaHint_t1542517147::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2043 = { sizeof (StorageType_t1466127932)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2043[4] = 
{
	StorageType_t1466127932::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2044 = { sizeof (VuforiaARController_t1328503142), -1, sizeof(VuforiaARController_t1328503142_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2044[39] = 
{
	VuforiaARController_t1328503142::get_offset_of_CameraDeviceModeSetting_1(),
	VuforiaARController_t1328503142::get_offset_of_MaxSimultaneousImageTargets_2(),
	VuforiaARController_t1328503142::get_offset_of_MaxSimultaneousObjectTargets_3(),
	VuforiaARController_t1328503142::get_offset_of_UseDelayedLoadingObjectTargets_4(),
	VuforiaARController_t1328503142::get_offset_of_CameraDirection_5(),
	VuforiaARController_t1328503142::get_offset_of_MirrorVideoBackground_6(),
	VuforiaARController_t1328503142::get_offset_of_mWorldCenterMode_7(),
	VuforiaARController_t1328503142::get_offset_of_mWorldCenter_8(),
	VuforiaARController_t1328503142::get_offset_of_mTrackerEventHandlers_9(),
	VuforiaARController_t1328503142::get_offset_of_mVideoBgEventHandlers_10(),
	VuforiaARController_t1328503142::get_offset_of_mOnVuforiaInitialized_11(),
	VuforiaARController_t1328503142::get_offset_of_mOnVuforiaStarted_12(),
	VuforiaARController_t1328503142::get_offset_of_mOnVuforiaDeinitialized_13(),
	VuforiaARController_t1328503142::get_offset_of_mOnTrackablesUpdated_14(),
	VuforiaARController_t1328503142::get_offset_of_mRenderOnUpdate_15(),
	VuforiaARController_t1328503142::get_offset_of_mOnPause_16(),
	VuforiaARController_t1328503142::get_offset_of_mPaused_17(),
	VuforiaARController_t1328503142::get_offset_of_mOnBackgroundTextureChanged_18(),
	VuforiaARController_t1328503142::get_offset_of_mStartHasBeenInvoked_19(),
	VuforiaARController_t1328503142::get_offset_of_mHasStarted_20(),
	VuforiaARController_t1328503142::get_offset_of_mBackgroundTextureHasChanged_21(),
	VuforiaARController_t1328503142::get_offset_of_mCameraConfiguration_22(),
	VuforiaARController_t1328503142::get_offset_of_mEyewearBehaviour_23(),
	VuforiaARController_t1328503142::get_offset_of_mVideoBackgroundMgr_24(),
	VuforiaARController_t1328503142::get_offset_of_mCheckStopCamera_25(),
	VuforiaARController_t1328503142::get_offset_of_mClearMaterial_26(),
	VuforiaARController_t1328503142::get_offset_of_mMetalRendering_27(),
	VuforiaARController_t1328503142::get_offset_of_mHasStartedOnce_28(),
	VuforiaARController_t1328503142::get_offset_of_mWasEnabledBeforePause_29(),
	VuforiaARController_t1328503142::get_offset_of_mObjectTrackerWasActiveBeforePause_30(),
	VuforiaARController_t1328503142::get_offset_of_mObjectTrackerWasActiveBeforeDisabling_31(),
	VuforiaARController_t1328503142::get_offset_of_mLastUpdatedFrame_32(),
	VuforiaARController_t1328503142::get_offset_of_mTrackersRequestedToDeinit_33(),
	VuforiaARController_t1328503142::get_offset_of_mMissedToApplyLeftProjectionMatrix_34(),
	VuforiaARController_t1328503142::get_offset_of_mMissedToApplyRightProjectionMatrix_35(),
	VuforiaARController_t1328503142::get_offset_of_mLeftProjectMatrixToApply_36(),
	VuforiaARController_t1328503142::get_offset_of_mRightProjectMatrixToApply_37(),
	VuforiaARController_t1328503142_StaticFields::get_offset_of_mInstance_38(),
	VuforiaARController_t1328503142_StaticFields::get_offset_of_mPadlock_39(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2045 = { sizeof (WorldCenterMode_t2621860492)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2045[5] = 
{
	WorldCenterMode_t2621860492::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2046 = { sizeof (VuforiaMacros_t2716100560)+ sizeof (RuntimeObject), sizeof(VuforiaMacros_t2716100560 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2046[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2047 = { sizeof (VuforiaManager_t745032010), -1, sizeof(VuforiaManager_t745032010_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2047[1] = 
{
	VuforiaManager_t745032010_StaticFields::get_offset_of_sInstance_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2048 = { sizeof (TrackableIdPair_t1331427386)+ sizeof (RuntimeObject), sizeof(TrackableIdPair_t1331427386 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2048[2] = 
{
	TrackableIdPair_t1331427386::get_offset_of_TrackableId_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TrackableIdPair_t1331427386::get_offset_of_ResultId_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2049 = { sizeof (VuforiaRenderer_t969713742), -1, sizeof(VuforiaRenderer_t969713742_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2049[1] = 
{
	VuforiaRenderer_t969713742_StaticFields::get_offset_of_sInstance_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2050 = { sizeof (FpsHint_t440410800)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2050[5] = 
{
	FpsHint_t440410800::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2051 = { sizeof (VideoBackgroundReflection_t728651611)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2051[4] = 
{
	VideoBackgroundReflection_t728651611::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2052 = { sizeof (VideoBGCfgData_t3234105495)+ sizeof (RuntimeObject), sizeof(VideoBGCfgData_t3234105495 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2052[4] = 
{
	VideoBGCfgData_t3234105495::get_offset_of_position_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	VideoBGCfgData_t3234105495::get_offset_of_size_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	VideoBGCfgData_t3234105495::get_offset_of_enabled_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	VideoBGCfgData_t3234105495::get_offset_of_reflectionInteger_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2053 = { sizeof (Vec2I_t895597728)+ sizeof (RuntimeObject), sizeof(Vec2I_t895597728 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2053[2] = 
{
	Vec2I_t895597728::get_offset_of_x_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Vec2I_t895597728::get_offset_of_y_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2054 = { sizeof (VideoTextureInfo_t773013061)+ sizeof (RuntimeObject), sizeof(VideoTextureInfo_t773013061 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2054[2] = 
{
	VideoTextureInfo_t773013061::get_offset_of_textureSize_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	VideoTextureInfo_t773013061::get_offset_of_imageSize_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2055 = { sizeof (RendererAPI_t3250500906)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2055[5] = 
{
	RendererAPI_t3250500906::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2056 = { sizeof (VuforiaRuntimeUtilities_t1036756025), -1, sizeof(VuforiaRuntimeUtilities_t1036756025_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2056[2] = 
{
	VuforiaRuntimeUtilities_t1036756025_StaticFields::get_offset_of_sWebCamUsed_0(),
	VuforiaRuntimeUtilities_t1036756025_StaticFields::get_offset_of_sNativePluginSupport_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2057 = { sizeof (InitializableBool_t766459865)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2057[4] = 
{
	InitializableBool_t766459865::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2058 = { sizeof (SurfaceUtilities_t184061569), -1, sizeof(SurfaceUtilities_t184061569_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2058[1] = 
{
	SurfaceUtilities_t184061569_StaticFields::get_offset_of_mScreenOrientation_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2059 = { sizeof (TargetFinder_t2860227752), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2060 = { sizeof (InitState_t3280104792)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2060[6] = 
{
	InitState_t3280104792::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2061 = { sizeof (UpdateState_t3694783787)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2061[12] = 
{
	UpdateState_t3694783787::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2062 = { sizeof (FilterMode_t706444316)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2062[3] = 
{
	FilterMode_t706444316::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2063 = { sizeof (TargetSearchResult_t213694407)+ sizeof (RuntimeObject), sizeof(TargetSearchResult_t213694407_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2063[6] = 
{
	TargetSearchResult_t213694407::get_offset_of_TargetName_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TargetSearchResult_t213694407::get_offset_of_UniqueTargetId_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TargetSearchResult_t213694407::get_offset_of_TargetSize_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TargetSearchResult_t213694407::get_offset_of_MetaData_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TargetSearchResult_t213694407::get_offset_of_TrackingRating_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TargetSearchResult_t213694407::get_offset_of_TargetSearchResultPtr_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2064 = { sizeof (TextRecoAbstractBehaviour_t1751158196), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2064[12] = 
{
	TextRecoAbstractBehaviour_t1751158196::get_offset_of_mHasInitialized_2(),
	TextRecoAbstractBehaviour_t1751158196::get_offset_of_mTrackerWasActiveBeforePause_3(),
	TextRecoAbstractBehaviour_t1751158196::get_offset_of_mTrackerWasActiveBeforeDisabling_4(),
	TextRecoAbstractBehaviour_t1751158196::get_offset_of_mWordListFile_5(),
	TextRecoAbstractBehaviour_t1751158196::get_offset_of_mCustomWordListFile_6(),
	TextRecoAbstractBehaviour_t1751158196::get_offset_of_mAdditionalCustomWords_7(),
	TextRecoAbstractBehaviour_t1751158196::get_offset_of_mFilterMode_8(),
	TextRecoAbstractBehaviour_t1751158196::get_offset_of_mFilterListFile_9(),
	TextRecoAbstractBehaviour_t1751158196::get_offset_of_mAdditionalFilterWords_10(),
	TextRecoAbstractBehaviour_t1751158196::get_offset_of_mWordPrefabCreationMode_11(),
	TextRecoAbstractBehaviour_t1751158196::get_offset_of_mMaximumWordInstances_12(),
	TextRecoAbstractBehaviour_t1751158196::get_offset_of_mTextRecoEventHandlers_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2065 = { sizeof (TextTracker_t3669255194), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2066 = { sizeof (SimpleTargetData_t2990037841)+ sizeof (RuntimeObject), sizeof(SimpleTargetData_t2990037841 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2066[2] = 
{
	SimpleTargetData_t2990037841::get_offset_of_id_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SimpleTargetData_t2990037841::get_offset_of_unused_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2067 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2068 = { sizeof (TrackableBehaviour_t2419079356), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2068[8] = 
{
	TrackableBehaviour_t2419079356::get_offset_of_U3CTimeStampU3Ek__BackingField_2(),
	TrackableBehaviour_t2419079356::get_offset_of_mTrackableName_3(),
	TrackableBehaviour_t2419079356::get_offset_of_mPreserveChildSize_4(),
	TrackableBehaviour_t2419079356::get_offset_of_mInitializedInEditor_5(),
	TrackableBehaviour_t2419079356::get_offset_of_mPreviousScale_6(),
	TrackableBehaviour_t2419079356::get_offset_of_mStatus_7(),
	TrackableBehaviour_t2419079356::get_offset_of_mTrackable_8(),
	TrackableBehaviour_t2419079356::get_offset_of_mTrackableEventHandlers_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2069 = { sizeof (Status_t1358118869)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2069[7] = 
{
	Status_t1358118869::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2070 = { sizeof (CoordinateSystem_t10191807)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2070[4] = 
{
	CoordinateSystem_t10191807::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2071 = { sizeof (TrackableSource_t4159007682), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2072 = { sizeof (Tracker_t1731515739), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2072[1] = 
{
	Tracker_t1731515739::get_offset_of_U3CIsActiveU3Ek__BackingField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2073 = { sizeof (TrackerManager_t814515577), -1, sizeof(TrackerManager_t814515577_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2073[1] = 
{
	TrackerManager_t814515577_StaticFields::get_offset_of_mInstance_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2074 = { sizeof (TurnOffAbstractBehaviour_t1066217039), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2075 = { sizeof (UserDefinedTargetBuildingAbstractBehaviour_t2424076393), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2075[11] = 
{
	UserDefinedTargetBuildingAbstractBehaviour_t2424076393::get_offset_of_mObjectTracker_2(),
	UserDefinedTargetBuildingAbstractBehaviour_t2424076393::get_offset_of_mLastFrameQuality_3(),
	UserDefinedTargetBuildingAbstractBehaviour_t2424076393::get_offset_of_mCurrentlyScanning_4(),
	UserDefinedTargetBuildingAbstractBehaviour_t2424076393::get_offset_of_mWasScanningBeforeDisable_5(),
	UserDefinedTargetBuildingAbstractBehaviour_t2424076393::get_offset_of_mCurrentlyBuilding_6(),
	UserDefinedTargetBuildingAbstractBehaviour_t2424076393::get_offset_of_mWasBuildingBeforeDisable_7(),
	UserDefinedTargetBuildingAbstractBehaviour_t2424076393::get_offset_of_mOnInitializedCalled_8(),
	UserDefinedTargetBuildingAbstractBehaviour_t2424076393::get_offset_of_mHandlers_9(),
	UserDefinedTargetBuildingAbstractBehaviour_t2424076393::get_offset_of_StopTrackerWhileScanning_10(),
	UserDefinedTargetBuildingAbstractBehaviour_t2424076393::get_offset_of_StartScanningAutomatically_11(),
	UserDefinedTargetBuildingAbstractBehaviour_t2424076393::get_offset_of_StopScanningWhenFinshedBuilding_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2076 = { sizeof (VideoBackgroundAbstractBehaviour_t3867888217), -1, sizeof(VideoBackgroundAbstractBehaviour_t3867888217_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2076[12] = 
{
	VideoBackgroundAbstractBehaviour_t3867888217::get_offset_of_mClearBuffers_2(),
	VideoBackgroundAbstractBehaviour_t3867888217::get_offset_of_mSkipStateUpdates_3(),
	VideoBackgroundAbstractBehaviour_t3867888217::get_offset_of_mVuforiaARController_4(),
	VideoBackgroundAbstractBehaviour_t3867888217::get_offset_of_mCamera_5(),
	VideoBackgroundAbstractBehaviour_t3867888217::get_offset_of_mBackgroundBehaviour_6(),
	VideoBackgroundAbstractBehaviour_t3867888217::get_offset_of_mStereoDepth_7(),
	VideoBackgroundAbstractBehaviour_t3867888217_StaticFields::get_offset_of_mFrameCounter_8(),
	VideoBackgroundAbstractBehaviour_t3867888217_StaticFields::get_offset_of_mRenderCounter_9(),
	VideoBackgroundAbstractBehaviour_t3867888217::get_offset_of_mResetMatrix_10(),
	VideoBackgroundAbstractBehaviour_t3867888217::get_offset_of_mVuforiaFrustumSkew_11(),
	VideoBackgroundAbstractBehaviour_t3867888217::get_offset_of_mCenterToEyeAxis_12(),
	VideoBackgroundAbstractBehaviour_t3867888217::get_offset_of_mDisabledMeshRenderers_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2077 = { sizeof (VideoBackgroundManager_t2068552549), -1, sizeof(VideoBackgroundManager_t2068552549_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2077[8] = 
{
	VideoBackgroundManager_t2068552549::get_offset_of_mClippingMode_1(),
	VideoBackgroundManager_t2068552549::get_offset_of_mMatteShader_2(),
	VideoBackgroundManager_t2068552549::get_offset_of_mVideoBackgroundEnabled_3(),
	VideoBackgroundManager_t2068552549::get_offset_of_mTexture_4(),
	VideoBackgroundManager_t2068552549::get_offset_of_mVideoBgConfigChanged_5(),
	VideoBackgroundManager_t2068552549::get_offset_of_mNativeTexturePtr_6(),
	VideoBackgroundManager_t2068552549_StaticFields::get_offset_of_mInstance_7(),
	VideoBackgroundManager_t2068552549_StaticFields::get_offset_of_mPadlock_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2078 = { sizeof (VirtualButton_t3666937711), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2078[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2079 = { sizeof (Sensitivity_t2611854098)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2079[4] = 
{
	Sensitivity_t2611854098::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2080 = { sizeof (VirtualButtonAbstractBehaviour_t1965820348), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2080[15] = 
{
	0,
	VirtualButtonAbstractBehaviour_t1965820348::get_offset_of_mName_3(),
	VirtualButtonAbstractBehaviour_t1965820348::get_offset_of_mSensitivity_4(),
	VirtualButtonAbstractBehaviour_t1965820348::get_offset_of_mHasUpdatedPose_5(),
	VirtualButtonAbstractBehaviour_t1965820348::get_offset_of_mPrevTransform_6(),
	VirtualButtonAbstractBehaviour_t1965820348::get_offset_of_mPrevParent_7(),
	VirtualButtonAbstractBehaviour_t1965820348::get_offset_of_mSensitivityDirty_8(),
	VirtualButtonAbstractBehaviour_t1965820348::get_offset_of_mPreviousSensitivity_9(),
	VirtualButtonAbstractBehaviour_t1965820348::get_offset_of_mPreviouslyEnabled_10(),
	VirtualButtonAbstractBehaviour_t1965820348::get_offset_of_mPressed_11(),
	VirtualButtonAbstractBehaviour_t1965820348::get_offset_of_mHandlers_12(),
	VirtualButtonAbstractBehaviour_t1965820348::get_offset_of_mLeftTop_13(),
	VirtualButtonAbstractBehaviour_t1965820348::get_offset_of_mRightBottom_14(),
	VirtualButtonAbstractBehaviour_t1965820348::get_offset_of_mUnregisterOnDestroy_15(),
	VirtualButtonAbstractBehaviour_t1965820348::get_offset_of_mVirtualButton_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2081 = { sizeof (WebCamARController_t1321869434), -1, sizeof(WebCamARController_t1321869434_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2081[6] = 
{
	WebCamARController_t1321869434::get_offset_of_RenderTextureLayer_1(),
	WebCamARController_t1321869434::get_offset_of_mDeviceNameSetInEditor_2(),
	WebCamARController_t1321869434::get_offset_of_mFlipHorizontally_3(),
	WebCamARController_t1321869434::get_offset_of_mWebCamImpl_4(),
	WebCamARController_t1321869434_StaticFields::get_offset_of_mInstance_5(),
	WebCamARController_t1321869434_StaticFields::get_offset_of_mPadlock_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2082 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2083 = { sizeof (WordManager_t1528772797), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2084 = { sizeof (WordResult_t292368650), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2085 = { sizeof (WordTemplateMode_t620524771)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2085[3] = 
{
	WordTemplateMode_t620524771::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2086 = { sizeof (WordAbstractBehaviour_t2339709601), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2086[3] = 
{
	WordAbstractBehaviour_t2339709601::get_offset_of_mMode_10(),
	WordAbstractBehaviour_t2339709601::get_offset_of_mSpecificWord_11(),
	WordAbstractBehaviour_t2339709601::get_offset_of_mWord_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2087 = { sizeof (WordFilterMode_t2466111678)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2087[4] = 
{
	WordFilterMode_t2466111678::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2088 = { sizeof (WordList_t3717410066), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2089 = { sizeof (EyewearCalibrationProfileManager_t1615252077), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2090 = { sizeof (EyewearUserCalibrator_t173744680), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2091 = { sizeof (U3CPrivateImplementationDetailsU3E_t4060893175), -1, sizeof(U3CPrivateImplementationDetailsU3E_t4060893175_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2091[1] = 
{
	U3CPrivateImplementationDetailsU3E_t4060893175_StaticFields::get_offset_of_U3898C2022A0C02FCE602BF05E1C09BD48301606E5_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2092 = { sizeof (__StaticArrayInitTypeSizeU3D24_t913725550)+ sizeof (RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D24_t913725550 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2093 = { sizeof (U3CModuleU3E_t160650889), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2094 = { sizeof (VuforiaNativeIosWrapper_t49709812), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2095 = { sizeof (U3CModuleU3E_t160650890), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2096 = { sizeof (AnalyticsTracker_t4207383808), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2096[5] = 
{
	AnalyticsTracker_t4207383808::get_offset_of_m_EventName_2(),
	AnalyticsTracker_t4207383808::get_offset_of_m_Dict_3(),
	AnalyticsTracker_t4207383808::get_offset_of_m_PrevDictHash_4(),
	AnalyticsTracker_t4207383808::get_offset_of_m_TrackableProperty_5(),
	AnalyticsTracker_t4207383808::get_offset_of_m_Trigger_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2097 = { sizeof (Trigger_t2488916005)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2097[8] = 
{
	Trigger_t2488916005::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2098 = { sizeof (TrackableProperty_t10670078), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2098[2] = 
{
	0,
	TrackableProperty_t10670078::get_offset_of_m_Fields_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2099 = { sizeof (FieldWithTarget_t4134674200), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2099[6] = 
{
	FieldWithTarget_t4134674200::get_offset_of_m_ParamName_0(),
	FieldWithTarget_t4134674200::get_offset_of_m_Target_1(),
	FieldWithTarget_t4134674200::get_offset_of_m_FieldPath_2(),
	FieldWithTarget_t4134674200::get_offset_of_m_TypeString_3(),
	FieldWithTarget_t4134674200::get_offset_of_m_DoStatic_4(),
	FieldWithTarget_t4134674200::get_offset_of_m_StaticString_5(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif

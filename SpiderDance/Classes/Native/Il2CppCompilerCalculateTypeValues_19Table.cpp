﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "object-internals.h"

// Vuforia.IPlayModeEditorUtility
struct IPlayModeEditorUtility_t2508626539;
// Vuforia.IPremiumObjectFactory
struct IPremiumObjectFactory_t2470068923;
// System.Collections.Generic.Dictionary`2<System.Type,System.UInt16>
struct Dictionary_2_t1840647812;
// System.String
struct String_t;
// Vuforia.IBehaviourComponentFactory
struct IBehaviourComponentFactory_t2618036286;
// System.String[]
struct StringU5BU5D_t2298632947;
// Vuforia.VuforiaAbstractBehaviour
struct VuforiaAbstractBehaviour_t153010168;
// System.Void
struct Void_t2217553113;
// Vuforia.TrackableSource
struct TrackableSource_t4159007682;
// System.Collections.Generic.List`1<Vuforia.ReconstructionAbstractBehaviour>
struct List_1_t4104585136;
// System.Action
struct Action_t3619184611;
// System.Collections.Generic.List`1<System.String>
struct List_1_t1230370061;
// System.Char[]
struct CharU5BU5D_t45629911;
// UnityEngine.WebCamTexture
struct WebCamTexture_t2673512900;
// UnityEngine.AsyncOperation
struct AsyncOperation_t2744032732;
// Vuforia.SmartTerrainBuilderImpl
struct SmartTerrainBuilderImpl_t4234039031;
// Vuforia.WordList
struct WordList_t3717410066;
// System.Collections.Generic.List`1<Vuforia.DataSetImpl>
struct List_1_t3373880750;
// System.Collections.Generic.List`1<Vuforia.DataSet>
struct List_1_t298280400;
// Vuforia.ImageTargetBuilder
struct ImageTargetBuilder_t2469815077;
// Vuforia.TargetFinder
struct TargetFinder_t2860227752;
// Vuforia.DataSetImpl
struct DataSetImpl_t223764247;
// System.Int32[]
struct Int32U5BU5D_t2324750880;
// System.Byte[]
struct ByteU5BU5D_t3172826560;
// UnityEngine.Color32[]
struct Color32U5BU5D_t2266382834;
// UnityEngine.Shader
struct Shader_t3778723916;
// Vuforia.DigitalEyewearARController/SerializableViewerParameters
struct SerializableViewerParameters_t484734121;
// System.Collections.Generic.Dictionary`2<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image>
struct Dictionary_2_t708360668;
// System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>
struct List_1_t440776343;
// Vuforia.IWebCam
struct IWebCam_t2583213770;
// System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.Trackable>
struct Dictionary_2_t2336703746;
// System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VirtualButton>
struct Dictionary_2_t567977579;
// Vuforia.WorldCenterTrackableBehaviour
struct WorldCenterTrackableBehaviour_t2169158894;
// Vuforia.VuMarkAbstractBehaviour
struct VuMarkAbstractBehaviour_t2652615854;
// UnityEngine.Transform
struct Transform_t3316442598;
// Vuforia.VuforiaManagerImpl/TrackableResultData[]
struct TrackableResultDataU5BU5D_t314934480;
// Vuforia.VuforiaManagerImpl/WordData[]
struct WordDataU5BU5D_t2808570108;
// Vuforia.VuforiaManagerImpl/WordResultData[]
struct WordResultDataU5BU5D_t1055029336;
// Vuforia.VuforiaManagerImpl/VuMarkTargetData[]
struct VuMarkTargetDataU5BU5D_t2525391479;
// Vuforia.VuforiaManagerImpl/VuMarkTargetResultData[]
struct VuMarkTargetResultDataU5BU5D_t752671397;
// System.Collections.Generic.LinkedList`1<Vuforia.VuforiaManager/TrackableIdPair>
struct LinkedList_1_t3507684622;
// UnityEngine.Texture
struct Texture_t85561421;
// System.Collections.Generic.List`1<Vuforia.SmartTerrainTrackable>
struct List_1_t2141263926;
// UnityEngine.Mesh
struct Mesh_t996500909;
// Vuforia.SmartTerrainTrackable
struct SmartTerrainTrackable_t3286114719;
// Vuforia.Image
struct Image_t1445313791;
// Vuforia.RectangleData[]
struct RectangleDataU5BU5D_t2927157895;
// System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.WordResult>
struct Dictionary_2_t1488375814;
// System.Collections.Generic.List`1<Vuforia.WordResult>
struct List_1_t3442485153;
// System.Collections.Generic.List`1<Vuforia.Word>
struct List_1_t3351092319;
// System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.WordAbstractBehaviour>
struct Dictionary_2_t3535716765;
// System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>
struct List_1_t1194858808;
// System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>
struct Dictionary_2_t2364748039;
// Vuforia.VuforiaARController
struct VuforiaARController_t1328503142;
// Vuforia.Word
struct Word_t200975816;
// System.Action`1<Vuforia.VuforiaUnity/InitError>
struct Action_1_t3472505684;
// Vuforia.VuforiaAbstractConfiguration/GenericVuforiaConfiguration
struct GenericVuforiaConfiguration_t1574456586;
// Vuforia.VuforiaAbstractConfiguration/DigitalEyewearConfiguration
struct DigitalEyewearConfiguration_t3649657388;
// Vuforia.VuforiaAbstractConfiguration/DatabaseLoadConfiguration
struct DatabaseLoadConfiguration_t3827205120;
// Vuforia.VuforiaAbstractConfiguration/VideoBackgroundConfiguration
struct VideoBackgroundConfiguration_t2579534243;
// Vuforia.VuforiaAbstractConfiguration/SmartTerrainTrackerConfiguration
struct SmartTerrainTrackerConfiguration_t657164935;
// Vuforia.VuforiaAbstractConfiguration/DeviceTrackerConfiguration
struct DeviceTrackerConfiguration_t2458088029;
// Vuforia.VuforiaAbstractConfiguration/WebCamConfiguration
struct WebCamConfiguration_t2750821739;
// UnityEngine.Texture2D
struct Texture2D_t415585320;
// Vuforia.ReconstructionFromTarget
struct ReconstructionFromTarget_t1541138710;
// Vuforia.ReconstructionAbstractBehaviour
struct ReconstructionAbstractBehaviour_t954468633;
// Vuforia.Trackable
struct Trackable_t1140696582;
// System.Collections.Generic.List`1<Vuforia.ITrackableEventHandler>
struct List_1_t3589236026;
// Vuforia.TrackableBehaviour
struct TrackableBehaviour_t2419079356;
// UnityEngine.Camera
struct Camera_t3175186167;
// System.Action`1<Vuforia.VuforiaAbstractBehaviour>
struct Action_1_t138837510;
// System.Action`1<System.Boolean>
struct Action_1_t2936672411;
// UnityEngine.MeshFilter
struct MeshFilter_t137687116;
// UnityEngine.MeshCollider
struct MeshCollider_t4172739389;
// Vuforia.ReconstructionFromTargetAbstractBehaviour
struct ReconstructionFromTargetAbstractBehaviour_t2676804737;
// Vuforia.Surface
struct Surface_t1034240668;
// Vuforia.CylinderTarget
struct CylinderTarget_t1924254572;
// Vuforia.VuMarkTemplate
struct VuMarkTemplate_t2238627375;
// Vuforia.VuMarkTarget
struct VuMarkTarget_t4168832550;




#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef PLAYMODEEDITORUTILITY_T1443925904_H
#define PLAYMODEEDITORUTILITY_T1443925904_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.PlayModeEditorUtility
struct  PlayModeEditorUtility_t1443925904  : public RuntimeObject
{
public:

public:
};

struct PlayModeEditorUtility_t1443925904_StaticFields
{
public:
	// Vuforia.IPlayModeEditorUtility Vuforia.PlayModeEditorUtility::sInstance
	RuntimeObject* ___sInstance_0;

public:
	inline static int32_t get_offset_of_sInstance_0() { return static_cast<int32_t>(offsetof(PlayModeEditorUtility_t1443925904_StaticFields, ___sInstance_0)); }
	inline RuntimeObject* get_sInstance_0() const { return ___sInstance_0; }
	inline RuntimeObject** get_address_of_sInstance_0() { return &___sInstance_0; }
	inline void set_sInstance_0(RuntimeObject* value)
	{
		___sInstance_0 = value;
		Il2CppCodeGenWriteBarrier((&___sInstance_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYMODEEDITORUTILITY_T1443925904_H
#ifndef PREMIUMOBJECTFACTORY_T2114016913_H
#define PREMIUMOBJECTFACTORY_T2114016913_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.PremiumObjectFactory
struct  PremiumObjectFactory_t2114016913  : public RuntimeObject
{
public:

public:
};

struct PremiumObjectFactory_t2114016913_StaticFields
{
public:
	// Vuforia.IPremiumObjectFactory Vuforia.PremiumObjectFactory::sInstance
	RuntimeObject* ___sInstance_0;

public:
	inline static int32_t get_offset_of_sInstance_0() { return static_cast<int32_t>(offsetof(PremiumObjectFactory_t2114016913_StaticFields, ___sInstance_0)); }
	inline RuntimeObject* get_sInstance_0() const { return ___sInstance_0; }
	inline RuntimeObject** get_address_of_sInstance_0() { return &___sInstance_0; }
	inline void set_sInstance_0(RuntimeObject* value)
	{
		___sInstance_0 = value;
		Il2CppCodeGenWriteBarrier((&___sInstance_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PREMIUMOBJECTFACTORY_T2114016913_H
#ifndef VUFORIAMANAGER_T745032010_H
#define VUFORIAMANAGER_T745032010_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.VuforiaManager
struct  VuforiaManager_t745032010  : public RuntimeObject
{
public:

public:
};

struct VuforiaManager_t745032010_StaticFields
{
public:
	// Vuforia.VuforiaManager Vuforia.VuforiaManager::sInstance
	VuforiaManager_t745032010 * ___sInstance_0;

public:
	inline static int32_t get_offset_of_sInstance_0() { return static_cast<int32_t>(offsetof(VuforiaManager_t745032010_StaticFields, ___sInstance_0)); }
	inline VuforiaManager_t745032010 * get_sInstance_0() const { return ___sInstance_0; }
	inline VuforiaManager_t745032010 ** get_address_of_sInstance_0() { return &___sInstance_0; }
	inline void set_sInstance_0(VuforiaManager_t745032010 * value)
	{
		___sInstance_0 = value;
		Il2CppCodeGenWriteBarrier((&___sInstance_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VUFORIAMANAGER_T745032010_H
#ifndef NULLPREMIUMOBJECTFACTORY_T468302538_H
#define NULLPREMIUMOBJECTFACTORY_T468302538_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.PremiumObjectFactory/NullPremiumObjectFactory
struct  NullPremiumObjectFactory_t468302538  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLPREMIUMOBJECTFACTORY_T468302538_H
#ifndef CAMERADEVICE_T2431414939_H
#define CAMERADEVICE_T2431414939_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.CameraDevice
struct  CameraDevice_t2431414939  : public RuntimeObject
{
public:

public:
};

struct CameraDevice_t2431414939_StaticFields
{
public:
	// Vuforia.CameraDevice Vuforia.CameraDevice::mInstance
	CameraDevice_t2431414939 * ___mInstance_0;

public:
	inline static int32_t get_offset_of_mInstance_0() { return static_cast<int32_t>(offsetof(CameraDevice_t2431414939_StaticFields, ___mInstance_0)); }
	inline CameraDevice_t2431414939 * get_mInstance_0() const { return ___mInstance_0; }
	inline CameraDevice_t2431414939 ** get_address_of_mInstance_0() { return &___mInstance_0; }
	inline void set_mInstance_0(CameraDevice_t2431414939 * value)
	{
		___mInstance_0 = value;
		Il2CppCodeGenWriteBarrier((&___mInstance_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAMERADEVICE_T2431414939_H
#ifndef DATASET_T1443131193_H
#define DATASET_T1443131193_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.DataSet
struct  DataSet_t1443131193  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATASET_T1443131193_H
#ifndef NULLBEHAVIOURCOMPONENTFACTORY_T2275034854_H
#define NULLBEHAVIOURCOMPONENTFACTORY_T2275034854_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.BehaviourComponentFactory/NullBehaviourComponentFactory
struct  NullBehaviourComponentFactory_t2275034854  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLBEHAVIOURCOMPONENTFACTORY_T2275034854_H
#ifndef VUFORIARENDERER_T969713742_H
#define VUFORIARENDERER_T969713742_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.VuforiaRenderer
struct  VuforiaRenderer_t969713742  : public RuntimeObject
{
public:

public:
};

struct VuforiaRenderer_t969713742_StaticFields
{
public:
	// Vuforia.VuforiaRenderer Vuforia.VuforiaRenderer::sInstance
	VuforiaRenderer_t969713742 * ___sInstance_0;

public:
	inline static int32_t get_offset_of_sInstance_0() { return static_cast<int32_t>(offsetof(VuforiaRenderer_t969713742_StaticFields, ___sInstance_0)); }
	inline VuforiaRenderer_t969713742 * get_sInstance_0() const { return ___sInstance_0; }
	inline VuforiaRenderer_t969713742 ** get_address_of_sInstance_0() { return &___sInstance_0; }
	inline void set_sInstance_0(VuforiaRenderer_t969713742 * value)
	{
		___sInstance_0 = value;
		Il2CppCodeGenWriteBarrier((&___sInstance_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VUFORIARENDERER_T969713742_H
#ifndef TYPEMAPPING_T586580177_H
#define TYPEMAPPING_T586580177_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.TypeMapping
struct  TypeMapping_t586580177  : public RuntimeObject
{
public:

public:
};

struct TypeMapping_t586580177_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.Type,System.UInt16> Vuforia.TypeMapping::sTypes
	Dictionary_2_t1840647812 * ___sTypes_0;

public:
	inline static int32_t get_offset_of_sTypes_0() { return static_cast<int32_t>(offsetof(TypeMapping_t586580177_StaticFields, ___sTypes_0)); }
	inline Dictionary_2_t1840647812 * get_sTypes_0() const { return ___sTypes_0; }
	inline Dictionary_2_t1840647812 ** get_address_of_sTypes_0() { return &___sTypes_0; }
	inline void set_sTypes_0(Dictionary_2_t1840647812 * value)
	{
		___sTypes_0 = value;
		Il2CppCodeGenWriteBarrier((&___sTypes_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TYPEMAPPING_T586580177_H
#ifndef VUFORIAUNITYIMPL_T2202289649_H
#define VUFORIAUNITYIMPL_T2202289649_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.VuforiaUnityImpl
struct  VuforiaUnityImpl_t2202289649  : public RuntimeObject
{
public:

public:
};

struct VuforiaUnityImpl_t2202289649_StaticFields
{
public:
	// System.Boolean Vuforia.VuforiaUnityImpl::mRendererDirty
	bool ___mRendererDirty_3;
	// System.Int32 Vuforia.VuforiaUnityImpl::mWrapperType
	int32_t ___mWrapperType_4;

public:
	inline static int32_t get_offset_of_mRendererDirty_3() { return static_cast<int32_t>(offsetof(VuforiaUnityImpl_t2202289649_StaticFields, ___mRendererDirty_3)); }
	inline bool get_mRendererDirty_3() const { return ___mRendererDirty_3; }
	inline bool* get_address_of_mRendererDirty_3() { return &___mRendererDirty_3; }
	inline void set_mRendererDirty_3(bool value)
	{
		___mRendererDirty_3 = value;
	}

	inline static int32_t get_offset_of_mWrapperType_4() { return static_cast<int32_t>(offsetof(VuforiaUnityImpl_t2202289649_StaticFields, ___mWrapperType_4)); }
	inline int32_t get_mWrapperType_4() const { return ___mWrapperType_4; }
	inline int32_t* get_address_of_mWrapperType_4() { return &___mWrapperType_4; }
	inline void set_mWrapperType_4(int32_t value)
	{
		___mWrapperType_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VUFORIAUNITYIMPL_T2202289649_H
#ifndef SMARTTERRAINBUILDER_T1660152051_H
#define SMARTTERRAINBUILDER_T1660152051_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.SmartTerrainBuilder
struct  SmartTerrainBuilder_t1660152051  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SMARTTERRAINBUILDER_T1660152051_H
#ifndef TRACKABLEIMPL_T3450720819_H
#define TRACKABLEIMPL_T3450720819_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.TrackableImpl
struct  TrackableImpl_t3450720819  : public RuntimeObject
{
public:
	// System.String Vuforia.TrackableImpl::<Name>k__BackingField
	String_t* ___U3CNameU3Ek__BackingField_0;
	// System.Int32 Vuforia.TrackableImpl::<ID>k__BackingField
	int32_t ___U3CIDU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CNameU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(TrackableImpl_t3450720819, ___U3CNameU3Ek__BackingField_0)); }
	inline String_t* get_U3CNameU3Ek__BackingField_0() const { return ___U3CNameU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CNameU3Ek__BackingField_0() { return &___U3CNameU3Ek__BackingField_0; }
	inline void set_U3CNameU3Ek__BackingField_0(String_t* value)
	{
		___U3CNameU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CNameU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CIDU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(TrackableImpl_t3450720819, ___U3CIDU3Ek__BackingField_1)); }
	inline int32_t get_U3CIDU3Ek__BackingField_1() const { return ___U3CIDU3Ek__BackingField_1; }
	inline int32_t* get_address_of_U3CIDU3Ek__BackingField_1() { return &___U3CIDU3Ek__BackingField_1; }
	inline void set_U3CIDU3Ek__BackingField_1(int32_t value)
	{
		___U3CIDU3Ek__BackingField_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRACKABLEIMPL_T3450720819_H
#ifndef NULLPLAYMODEEDITORUTILITY_T854983833_H
#define NULLPLAYMODEEDITORUTILITY_T854983833_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.PlayModeEditorUtility/NullPlayModeEditorUtility
struct  NullPlayModeEditorUtility_t854983833  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLPLAYMODEEDITORUTILITY_T854983833_H
#ifndef BEHAVIOURCOMPONENTFACTORY_T3424659045_H
#define BEHAVIOURCOMPONENTFACTORY_T3424659045_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.BehaviourComponentFactory
struct  BehaviourComponentFactory_t3424659045  : public RuntimeObject
{
public:

public:
};

struct BehaviourComponentFactory_t3424659045_StaticFields
{
public:
	// Vuforia.IBehaviourComponentFactory Vuforia.BehaviourComponentFactory::sInstance
	RuntimeObject* ___sInstance_0;

public:
	inline static int32_t get_offset_of_sInstance_0() { return static_cast<int32_t>(offsetof(BehaviourComponentFactory_t3424659045_StaticFields, ___sInstance_0)); }
	inline RuntimeObject* get_sInstance_0() const { return ___sInstance_0; }
	inline RuntimeObject** get_address_of_sInstance_0() { return &___sInstance_0; }
	inline void set_sInstance_0(RuntimeObject* value)
	{
		___sInstance_0 = value;
		Il2CppCodeGenWriteBarrier((&___sInstance_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOURCOMPONENTFACTORY_T3424659045_H
#ifndef IMAGE_T1445313791_H
#define IMAGE_T1445313791_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.Image
struct  Image_t1445313791  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IMAGE_T1445313791_H
#ifndef WORDMANAGER_T1528772797_H
#define WORDMANAGER_T1528772797_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.WordManager
struct  WordManager_t1528772797  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WORDMANAGER_T1528772797_H
#ifndef WORDRESULT_T292368650_H
#define WORDRESULT_T292368650_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.WordResult
struct  WordResult_t292368650  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WORDRESULT_T292368650_H
#ifndef WEBCAMCONFIGURATION_T2750821739_H
#define WEBCAMCONFIGURATION_T2750821739_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.VuforiaAbstractConfiguration/WebCamConfiguration
struct  WebCamConfiguration_t2750821739  : public RuntimeObject
{
public:
	// System.String Vuforia.VuforiaAbstractConfiguration/WebCamConfiguration::deviceNameSetInEditor
	String_t* ___deviceNameSetInEditor_0;
	// System.Boolean Vuforia.VuforiaAbstractConfiguration/WebCamConfiguration::flipHorizontally
	bool ___flipHorizontally_1;
	// System.Boolean Vuforia.VuforiaAbstractConfiguration/WebCamConfiguration::turnOffWebCam
	bool ___turnOffWebCam_2;
	// System.Int32 Vuforia.VuforiaAbstractConfiguration/WebCamConfiguration::renderTextureLayer
	int32_t ___renderTextureLayer_3;

public:
	inline static int32_t get_offset_of_deviceNameSetInEditor_0() { return static_cast<int32_t>(offsetof(WebCamConfiguration_t2750821739, ___deviceNameSetInEditor_0)); }
	inline String_t* get_deviceNameSetInEditor_0() const { return ___deviceNameSetInEditor_0; }
	inline String_t** get_address_of_deviceNameSetInEditor_0() { return &___deviceNameSetInEditor_0; }
	inline void set_deviceNameSetInEditor_0(String_t* value)
	{
		___deviceNameSetInEditor_0 = value;
		Il2CppCodeGenWriteBarrier((&___deviceNameSetInEditor_0), value);
	}

	inline static int32_t get_offset_of_flipHorizontally_1() { return static_cast<int32_t>(offsetof(WebCamConfiguration_t2750821739, ___flipHorizontally_1)); }
	inline bool get_flipHorizontally_1() const { return ___flipHorizontally_1; }
	inline bool* get_address_of_flipHorizontally_1() { return &___flipHorizontally_1; }
	inline void set_flipHorizontally_1(bool value)
	{
		___flipHorizontally_1 = value;
	}

	inline static int32_t get_offset_of_turnOffWebCam_2() { return static_cast<int32_t>(offsetof(WebCamConfiguration_t2750821739, ___turnOffWebCam_2)); }
	inline bool get_turnOffWebCam_2() const { return ___turnOffWebCam_2; }
	inline bool* get_address_of_turnOffWebCam_2() { return &___turnOffWebCam_2; }
	inline void set_turnOffWebCam_2(bool value)
	{
		___turnOffWebCam_2 = value;
	}

	inline static int32_t get_offset_of_renderTextureLayer_3() { return static_cast<int32_t>(offsetof(WebCamConfiguration_t2750821739, ___renderTextureLayer_3)); }
	inline int32_t get_renderTextureLayer_3() const { return ___renderTextureLayer_3; }
	inline int32_t* get_address_of_renderTextureLayer_3() { return &___renderTextureLayer_3; }
	inline void set_renderTextureLayer_3(int32_t value)
	{
		___renderTextureLayer_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WEBCAMCONFIGURATION_T2750821739_H
#ifndef WEBCAMTEXADAPTOR_T433428952_H
#define WEBCAMTEXADAPTOR_T433428952_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.WebCamTexAdaptor
struct  WebCamTexAdaptor_t433428952  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WEBCAMTEXADAPTOR_T433428952_H
#ifndef TRACKERCONFIGURATION_T3022484256_H
#define TRACKERCONFIGURATION_T3022484256_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.VuforiaAbstractConfiguration/TrackerConfiguration
struct  TrackerConfiguration_t3022484256  : public RuntimeObject
{
public:
	// System.Boolean Vuforia.VuforiaAbstractConfiguration/TrackerConfiguration::autoInitTracker
	bool ___autoInitTracker_0;
	// System.Boolean Vuforia.VuforiaAbstractConfiguration/TrackerConfiguration::autoStartTracker
	bool ___autoStartTracker_1;

public:
	inline static int32_t get_offset_of_autoInitTracker_0() { return static_cast<int32_t>(offsetof(TrackerConfiguration_t3022484256, ___autoInitTracker_0)); }
	inline bool get_autoInitTracker_0() const { return ___autoInitTracker_0; }
	inline bool* get_address_of_autoInitTracker_0() { return &___autoInitTracker_0; }
	inline void set_autoInitTracker_0(bool value)
	{
		___autoInitTracker_0 = value;
	}

	inline static int32_t get_offset_of_autoStartTracker_1() { return static_cast<int32_t>(offsetof(TrackerConfiguration_t3022484256, ___autoStartTracker_1)); }
	inline bool get_autoStartTracker_1() const { return ___autoStartTracker_1; }
	inline bool* get_address_of_autoStartTracker_1() { return &___autoStartTracker_1; }
	inline void set_autoStartTracker_1(bool value)
	{
		___autoStartTracker_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRACKERCONFIGURATION_T3022484256_H
#ifndef IMAGETARGETBUILDER_T2469815077_H
#define IMAGETARGETBUILDER_T2469815077_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.ImageTargetBuilder
struct  ImageTargetBuilder_t2469815077  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IMAGETARGETBUILDER_T2469815077_H
#ifndef DATABASELOADCONFIGURATION_T3827205120_H
#define DATABASELOADCONFIGURATION_T3827205120_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.VuforiaAbstractConfiguration/DatabaseLoadConfiguration
struct  DatabaseLoadConfiguration_t3827205120  : public RuntimeObject
{
public:
	// System.String[] Vuforia.VuforiaAbstractConfiguration/DatabaseLoadConfiguration::dataSetsToLoad
	StringU5BU5D_t2298632947* ___dataSetsToLoad_0;
	// System.String[] Vuforia.VuforiaAbstractConfiguration/DatabaseLoadConfiguration::dataSetsToActivate
	StringU5BU5D_t2298632947* ___dataSetsToActivate_1;

public:
	inline static int32_t get_offset_of_dataSetsToLoad_0() { return static_cast<int32_t>(offsetof(DatabaseLoadConfiguration_t3827205120, ___dataSetsToLoad_0)); }
	inline StringU5BU5D_t2298632947* get_dataSetsToLoad_0() const { return ___dataSetsToLoad_0; }
	inline StringU5BU5D_t2298632947** get_address_of_dataSetsToLoad_0() { return &___dataSetsToLoad_0; }
	inline void set_dataSetsToLoad_0(StringU5BU5D_t2298632947* value)
	{
		___dataSetsToLoad_0 = value;
		Il2CppCodeGenWriteBarrier((&___dataSetsToLoad_0), value);
	}

	inline static int32_t get_offset_of_dataSetsToActivate_1() { return static_cast<int32_t>(offsetof(DatabaseLoadConfiguration_t3827205120, ___dataSetsToActivate_1)); }
	inline StringU5BU5D_t2298632947* get_dataSetsToActivate_1() const { return ___dataSetsToActivate_1; }
	inline StringU5BU5D_t2298632947** get_address_of_dataSetsToActivate_1() { return &___dataSetsToActivate_1; }
	inline void set_dataSetsToActivate_1(StringU5BU5D_t2298632947* value)
	{
		___dataSetsToActivate_1 = value;
		Il2CppCodeGenWriteBarrier((&___dataSetsToActivate_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATABASELOADCONFIGURATION_T3827205120_H
#ifndef TRACKER_T1731515739_H
#define TRACKER_T1731515739_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.Tracker
struct  Tracker_t1731515739  : public RuntimeObject
{
public:
	// System.Boolean Vuforia.Tracker::<IsActive>k__BackingField
	bool ___U3CIsActiveU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CIsActiveU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(Tracker_t1731515739, ___U3CIsActiveU3Ek__BackingField_0)); }
	inline bool get_U3CIsActiveU3Ek__BackingField_0() const { return ___U3CIsActiveU3Ek__BackingField_0; }
	inline bool* get_address_of_U3CIsActiveU3Ek__BackingField_0() { return &___U3CIsActiveU3Ek__BackingField_0; }
	inline void set_U3CIsActiveU3Ek__BackingField_0(bool value)
	{
		___U3CIsActiveU3Ek__BackingField_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRACKER_T1731515739_H
#ifndef ARCONTROLLER_T1205915989_H
#define ARCONTROLLER_T1205915989_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.ARController
struct  ARController_t1205915989  : public RuntimeObject
{
public:
	// Vuforia.VuforiaAbstractBehaviour Vuforia.ARController::mVuforiaBehaviour
	VuforiaAbstractBehaviour_t153010168 * ___mVuforiaBehaviour_0;

public:
	inline static int32_t get_offset_of_mVuforiaBehaviour_0() { return static_cast<int32_t>(offsetof(ARController_t1205915989, ___mVuforiaBehaviour_0)); }
	inline VuforiaAbstractBehaviour_t153010168 * get_mVuforiaBehaviour_0() const { return ___mVuforiaBehaviour_0; }
	inline VuforiaAbstractBehaviour_t153010168 ** get_address_of_mVuforiaBehaviour_0() { return &___mVuforiaBehaviour_0; }
	inline void set_mVuforiaBehaviour_0(VuforiaAbstractBehaviour_t153010168 * value)
	{
		___mVuforiaBehaviour_0 = value;
		Il2CppCodeGenWriteBarrier((&___mVuforiaBehaviour_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARCONTROLLER_T1205915989_H
#ifndef VALUETYPE_T1306072465_H
#define VALUETYPE_T1306072465_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t1306072465  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t1306072465_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t1306072465_marshaled_com
{
};
#endif // VALUETYPE_T1306072465_H
#ifndef VUMARKMANAGER_T2090159093_H
#define VUMARKMANAGER_T2090159093_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.VuMarkManager
struct  VuMarkManager_t2090159093  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VUMARKMANAGER_T2090159093_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	IntPtr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline IntPtr_t get_Zero_1() const { return ___Zero_1; }
	inline IntPtr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(IntPtr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef IMAGETARGETBUILDERIMPL_T761427244_H
#define IMAGETARGETBUILDERIMPL_T761427244_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.ImageTargetBuilderImpl
struct  ImageTargetBuilderImpl_t761427244  : public ImageTargetBuilder_t2469815077
{
public:
	// Vuforia.TrackableSource Vuforia.ImageTargetBuilderImpl::mTrackableSource
	TrackableSource_t4159007682 * ___mTrackableSource_0;
	// System.Boolean Vuforia.ImageTargetBuilderImpl::mIsScanning
	bool ___mIsScanning_1;

public:
	inline static int32_t get_offset_of_mTrackableSource_0() { return static_cast<int32_t>(offsetof(ImageTargetBuilderImpl_t761427244, ___mTrackableSource_0)); }
	inline TrackableSource_t4159007682 * get_mTrackableSource_0() const { return ___mTrackableSource_0; }
	inline TrackableSource_t4159007682 ** get_address_of_mTrackableSource_0() { return &___mTrackableSource_0; }
	inline void set_mTrackableSource_0(TrackableSource_t4159007682 * value)
	{
		___mTrackableSource_0 = value;
		Il2CppCodeGenWriteBarrier((&___mTrackableSource_0), value);
	}

	inline static int32_t get_offset_of_mIsScanning_1() { return static_cast<int32_t>(offsetof(ImageTargetBuilderImpl_t761427244, ___mIsScanning_1)); }
	inline bool get_mIsScanning_1() const { return ___mIsScanning_1; }
	inline bool* get_address_of_mIsScanning_1() { return &___mIsScanning_1; }
	inline void set_mIsScanning_1(bool value)
	{
		___mIsScanning_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IMAGETARGETBUILDERIMPL_T761427244_H
#ifndef VIRTUALBUTTONDATA_T4213555505_H
#define VIRTUALBUTTONDATA_T4213555505_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.VuforiaManagerImpl/VirtualButtonData
#pragma pack(push, tp, 1)
struct  VirtualButtonData_t4213555505 
{
public:
	// System.Int32 Vuforia.VuforiaManagerImpl/VirtualButtonData::id
	int32_t ___id_0;
	// System.Int32 Vuforia.VuforiaManagerImpl/VirtualButtonData::isPressed
	int32_t ___isPressed_1;

public:
	inline static int32_t get_offset_of_id_0() { return static_cast<int32_t>(offsetof(VirtualButtonData_t4213555505, ___id_0)); }
	inline int32_t get_id_0() const { return ___id_0; }
	inline int32_t* get_address_of_id_0() { return &___id_0; }
	inline void set_id_0(int32_t value)
	{
		___id_0 = value;
	}

	inline static int32_t get_offset_of_isPressed_1() { return static_cast<int32_t>(offsetof(VirtualButtonData_t4213555505, ___isPressed_1)); }
	inline int32_t get_isPressed_1() const { return ___isPressed_1; }
	inline int32_t* get_address_of_isPressed_1() { return &___isPressed_1; }
	inline void set_isPressed_1(int32_t value)
	{
		___isPressed_1 = value;
	}
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VIRTUALBUTTONDATA_T4213555505_H
#ifndef VECTOR3_T2903530434_H
#define VECTOR3_T2903530434_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_t2903530434 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_1;
	// System.Single UnityEngine.Vector3::y
	float ___y_2;
	// System.Single UnityEngine.Vector3::z
	float ___z_3;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector3_t2903530434, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector3_t2903530434, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector3_t2903530434, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}
};

struct Vector3_t2903530434_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t2903530434  ___zeroVector_4;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t2903530434  ___oneVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t2903530434  ___upVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t2903530434  ___downVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t2903530434  ___leftVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t2903530434  ___rightVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t2903530434  ___forwardVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t2903530434  ___backVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t2903530434  ___positiveInfinityVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t2903530434  ___negativeInfinityVector_13;

public:
	inline static int32_t get_offset_of_zeroVector_4() { return static_cast<int32_t>(offsetof(Vector3_t2903530434_StaticFields, ___zeroVector_4)); }
	inline Vector3_t2903530434  get_zeroVector_4() const { return ___zeroVector_4; }
	inline Vector3_t2903530434 * get_address_of_zeroVector_4() { return &___zeroVector_4; }
	inline void set_zeroVector_4(Vector3_t2903530434  value)
	{
		___zeroVector_4 = value;
	}

	inline static int32_t get_offset_of_oneVector_5() { return static_cast<int32_t>(offsetof(Vector3_t2903530434_StaticFields, ___oneVector_5)); }
	inline Vector3_t2903530434  get_oneVector_5() const { return ___oneVector_5; }
	inline Vector3_t2903530434 * get_address_of_oneVector_5() { return &___oneVector_5; }
	inline void set_oneVector_5(Vector3_t2903530434  value)
	{
		___oneVector_5 = value;
	}

	inline static int32_t get_offset_of_upVector_6() { return static_cast<int32_t>(offsetof(Vector3_t2903530434_StaticFields, ___upVector_6)); }
	inline Vector3_t2903530434  get_upVector_6() const { return ___upVector_6; }
	inline Vector3_t2903530434 * get_address_of_upVector_6() { return &___upVector_6; }
	inline void set_upVector_6(Vector3_t2903530434  value)
	{
		___upVector_6 = value;
	}

	inline static int32_t get_offset_of_downVector_7() { return static_cast<int32_t>(offsetof(Vector3_t2903530434_StaticFields, ___downVector_7)); }
	inline Vector3_t2903530434  get_downVector_7() const { return ___downVector_7; }
	inline Vector3_t2903530434 * get_address_of_downVector_7() { return &___downVector_7; }
	inline void set_downVector_7(Vector3_t2903530434  value)
	{
		___downVector_7 = value;
	}

	inline static int32_t get_offset_of_leftVector_8() { return static_cast<int32_t>(offsetof(Vector3_t2903530434_StaticFields, ___leftVector_8)); }
	inline Vector3_t2903530434  get_leftVector_8() const { return ___leftVector_8; }
	inline Vector3_t2903530434 * get_address_of_leftVector_8() { return &___leftVector_8; }
	inline void set_leftVector_8(Vector3_t2903530434  value)
	{
		___leftVector_8 = value;
	}

	inline static int32_t get_offset_of_rightVector_9() { return static_cast<int32_t>(offsetof(Vector3_t2903530434_StaticFields, ___rightVector_9)); }
	inline Vector3_t2903530434  get_rightVector_9() const { return ___rightVector_9; }
	inline Vector3_t2903530434 * get_address_of_rightVector_9() { return &___rightVector_9; }
	inline void set_rightVector_9(Vector3_t2903530434  value)
	{
		___rightVector_9 = value;
	}

	inline static int32_t get_offset_of_forwardVector_10() { return static_cast<int32_t>(offsetof(Vector3_t2903530434_StaticFields, ___forwardVector_10)); }
	inline Vector3_t2903530434  get_forwardVector_10() const { return ___forwardVector_10; }
	inline Vector3_t2903530434 * get_address_of_forwardVector_10() { return &___forwardVector_10; }
	inline void set_forwardVector_10(Vector3_t2903530434  value)
	{
		___forwardVector_10 = value;
	}

	inline static int32_t get_offset_of_backVector_11() { return static_cast<int32_t>(offsetof(Vector3_t2903530434_StaticFields, ___backVector_11)); }
	inline Vector3_t2903530434  get_backVector_11() const { return ___backVector_11; }
	inline Vector3_t2903530434 * get_address_of_backVector_11() { return &___backVector_11; }
	inline void set_backVector_11(Vector3_t2903530434  value)
	{
		___backVector_11 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_12() { return static_cast<int32_t>(offsetof(Vector3_t2903530434_StaticFields, ___positiveInfinityVector_12)); }
	inline Vector3_t2903530434  get_positiveInfinityVector_12() const { return ___positiveInfinityVector_12; }
	inline Vector3_t2903530434 * get_address_of_positiveInfinityVector_12() { return &___positiveInfinityVector_12; }
	inline void set_positiveInfinityVector_12(Vector3_t2903530434  value)
	{
		___positiveInfinityVector_12 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t2903530434_StaticFields, ___negativeInfinityVector_13)); }
	inline Vector3_t2903530434  get_negativeInfinityVector_13() const { return ___negativeInfinityVector_13; }
	inline Vector3_t2903530434 * get_address_of_negativeInfinityVector_13() { return &___negativeInfinityVector_13; }
	inline void set_negativeInfinityVector_13(Vector3_t2903530434  value)
	{
		___negativeInfinityVector_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_T2903530434_H
#ifndef SMARTTERRAINBUILDERIMPL_T4234039031_H
#define SMARTTERRAINBUILDERIMPL_T4234039031_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.SmartTerrainBuilderImpl
struct  SmartTerrainBuilderImpl_t4234039031  : public SmartTerrainBuilder_t1660152051
{
public:
	// System.Collections.Generic.List`1<Vuforia.ReconstructionAbstractBehaviour> Vuforia.SmartTerrainBuilderImpl::mReconstructionBehaviours
	List_1_t4104585136 * ___mReconstructionBehaviours_0;
	// System.Boolean Vuforia.SmartTerrainBuilderImpl::mIsInitialized
	bool ___mIsInitialized_1;

public:
	inline static int32_t get_offset_of_mReconstructionBehaviours_0() { return static_cast<int32_t>(offsetof(SmartTerrainBuilderImpl_t4234039031, ___mReconstructionBehaviours_0)); }
	inline List_1_t4104585136 * get_mReconstructionBehaviours_0() const { return ___mReconstructionBehaviours_0; }
	inline List_1_t4104585136 ** get_address_of_mReconstructionBehaviours_0() { return &___mReconstructionBehaviours_0; }
	inline void set_mReconstructionBehaviours_0(List_1_t4104585136 * value)
	{
		___mReconstructionBehaviours_0 = value;
		Il2CppCodeGenWriteBarrier((&___mReconstructionBehaviours_0), value);
	}

	inline static int32_t get_offset_of_mIsInitialized_1() { return static_cast<int32_t>(offsetof(SmartTerrainBuilderImpl_t4234039031, ___mIsInitialized_1)); }
	inline bool get_mIsInitialized_1() const { return ___mIsInitialized_1; }
	inline bool* get_address_of_mIsInitialized_1() { return &___mIsInitialized_1; }
	inline void set_mIsInitialized_1(bool value)
	{
		___mIsInitialized_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SMARTTERRAINBUILDERIMPL_T4234039031_H
#ifndef RECT_T3436776195_H
#define RECT_T3436776195_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rect
struct  Rect_t3436776195 
{
public:
	// System.Single UnityEngine.Rect::m_XMin
	float ___m_XMin_0;
	// System.Single UnityEngine.Rect::m_YMin
	float ___m_YMin_1;
	// System.Single UnityEngine.Rect::m_Width
	float ___m_Width_2;
	// System.Single UnityEngine.Rect::m_Height
	float ___m_Height_3;

public:
	inline static int32_t get_offset_of_m_XMin_0() { return static_cast<int32_t>(offsetof(Rect_t3436776195, ___m_XMin_0)); }
	inline float get_m_XMin_0() const { return ___m_XMin_0; }
	inline float* get_address_of_m_XMin_0() { return &___m_XMin_0; }
	inline void set_m_XMin_0(float value)
	{
		___m_XMin_0 = value;
	}

	inline static int32_t get_offset_of_m_YMin_1() { return static_cast<int32_t>(offsetof(Rect_t3436776195, ___m_YMin_1)); }
	inline float get_m_YMin_1() const { return ___m_YMin_1; }
	inline float* get_address_of_m_YMin_1() { return &___m_YMin_1; }
	inline void set_m_YMin_1(float value)
	{
		___m_YMin_1 = value;
	}

	inline static int32_t get_offset_of_m_Width_2() { return static_cast<int32_t>(offsetof(Rect_t3436776195, ___m_Width_2)); }
	inline float get_m_Width_2() const { return ___m_Width_2; }
	inline float* get_address_of_m_Width_2() { return &___m_Width_2; }
	inline void set_m_Width_2(float value)
	{
		___m_Width_2 = value;
	}

	inline static int32_t get_offset_of_m_Height_3() { return static_cast<int32_t>(offsetof(Rect_t3436776195, ___m_Height_3)); }
	inline float get_m_Height_3() const { return ___m_Height_3; }
	inline float* get_address_of_m_Height_3() { return &___m_Height_3; }
	inline void set_m_Height_3(float value)
	{
		___m_Height_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RECT_T3436776195_H
#ifndef VECTOR2_T3577333262_H
#define VECTOR2_T3577333262_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2
struct  Vector2_t3577333262 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_t3577333262, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_t3577333262, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_t3577333262_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_t3577333262  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_t3577333262  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_t3577333262  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_t3577333262  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_t3577333262  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_t3577333262  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_t3577333262  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_t3577333262  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_t3577333262_StaticFields, ___zeroVector_2)); }
	inline Vector2_t3577333262  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_t3577333262 * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_t3577333262  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_t3577333262_StaticFields, ___oneVector_3)); }
	inline Vector2_t3577333262  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_t3577333262 * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_t3577333262  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_t3577333262_StaticFields, ___upVector_4)); }
	inline Vector2_t3577333262  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_t3577333262 * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_t3577333262  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_t3577333262_StaticFields, ___downVector_5)); }
	inline Vector2_t3577333262  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_t3577333262 * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_t3577333262  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_t3577333262_StaticFields, ___leftVector_6)); }
	inline Vector2_t3577333262  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_t3577333262 * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_t3577333262  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_t3577333262_StaticFields, ___rightVector_7)); }
	inline Vector2_t3577333262  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_t3577333262 * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_t3577333262  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_t3577333262_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_t3577333262  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_t3577333262 * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_t3577333262  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_t3577333262_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_t3577333262  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_t3577333262 * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_t3577333262  value)
	{
		___negativeInfinityVector_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_T3577333262_H
#ifndef AUTOROTATIONSTATE_T545146858_H
#define AUTOROTATIONSTATE_T545146858_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.VuforiaManagerImpl/AutoRotationState
struct  AutoRotationState_t545146858 
{
public:
	// System.Boolean Vuforia.VuforiaManagerImpl/AutoRotationState::setOnPause
	bool ___setOnPause_0;
	// System.Boolean Vuforia.VuforiaManagerImpl/AutoRotationState::autorotateToPortrait
	bool ___autorotateToPortrait_1;
	// System.Boolean Vuforia.VuforiaManagerImpl/AutoRotationState::autorotateToPortraitUpsideDown
	bool ___autorotateToPortraitUpsideDown_2;
	// System.Boolean Vuforia.VuforiaManagerImpl/AutoRotationState::autorotateToLandscapeLeft
	bool ___autorotateToLandscapeLeft_3;
	// System.Boolean Vuforia.VuforiaManagerImpl/AutoRotationState::autorotateToLandscapeRight
	bool ___autorotateToLandscapeRight_4;

public:
	inline static int32_t get_offset_of_setOnPause_0() { return static_cast<int32_t>(offsetof(AutoRotationState_t545146858, ___setOnPause_0)); }
	inline bool get_setOnPause_0() const { return ___setOnPause_0; }
	inline bool* get_address_of_setOnPause_0() { return &___setOnPause_0; }
	inline void set_setOnPause_0(bool value)
	{
		___setOnPause_0 = value;
	}

	inline static int32_t get_offset_of_autorotateToPortrait_1() { return static_cast<int32_t>(offsetof(AutoRotationState_t545146858, ___autorotateToPortrait_1)); }
	inline bool get_autorotateToPortrait_1() const { return ___autorotateToPortrait_1; }
	inline bool* get_address_of_autorotateToPortrait_1() { return &___autorotateToPortrait_1; }
	inline void set_autorotateToPortrait_1(bool value)
	{
		___autorotateToPortrait_1 = value;
	}

	inline static int32_t get_offset_of_autorotateToPortraitUpsideDown_2() { return static_cast<int32_t>(offsetof(AutoRotationState_t545146858, ___autorotateToPortraitUpsideDown_2)); }
	inline bool get_autorotateToPortraitUpsideDown_2() const { return ___autorotateToPortraitUpsideDown_2; }
	inline bool* get_address_of_autorotateToPortraitUpsideDown_2() { return &___autorotateToPortraitUpsideDown_2; }
	inline void set_autorotateToPortraitUpsideDown_2(bool value)
	{
		___autorotateToPortraitUpsideDown_2 = value;
	}

	inline static int32_t get_offset_of_autorotateToLandscapeLeft_3() { return static_cast<int32_t>(offsetof(AutoRotationState_t545146858, ___autorotateToLandscapeLeft_3)); }
	inline bool get_autorotateToLandscapeLeft_3() const { return ___autorotateToLandscapeLeft_3; }
	inline bool* get_address_of_autorotateToLandscapeLeft_3() { return &___autorotateToLandscapeLeft_3; }
	inline void set_autorotateToLandscapeLeft_3(bool value)
	{
		___autorotateToLandscapeLeft_3 = value;
	}

	inline static int32_t get_offset_of_autorotateToLandscapeRight_4() { return static_cast<int32_t>(offsetof(AutoRotationState_t545146858, ___autorotateToLandscapeRight_4)); }
	inline bool get_autorotateToLandscapeRight_4() const { return ___autorotateToLandscapeRight_4; }
	inline bool* get_address_of_autorotateToLandscapeRight_4() { return &___autorotateToLandscapeRight_4; }
	inline void set_autorotateToLandscapeRight_4(bool value)
	{
		___autorotateToLandscapeRight_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Vuforia.VuforiaManagerImpl/AutoRotationState
struct AutoRotationState_t545146858_marshaled_pinvoke
{
	int32_t ___setOnPause_0;
	int32_t ___autorotateToPortrait_1;
	int32_t ___autorotateToPortraitUpsideDown_2;
	int32_t ___autorotateToLandscapeLeft_3;
	int32_t ___autorotateToLandscapeRight_4;
};
// Native definition for COM marshalling of Vuforia.VuforiaManagerImpl/AutoRotationState
struct AutoRotationState_t545146858_marshaled_com
{
	int32_t ___setOnPause_0;
	int32_t ___autorotateToPortrait_1;
	int32_t ___autorotateToPortraitUpsideDown_2;
	int32_t ___autorotateToLandscapeLeft_3;
	int32_t ___autorotateToLandscapeRight_4;
};
#endif // AUTOROTATIONSTATE_T545146858_H
#ifndef QUATERNION_T754065749_H
#define QUATERNION_T754065749_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Quaternion
struct  Quaternion_t754065749 
{
public:
	// System.Single UnityEngine.Quaternion::x
	float ___x_0;
	// System.Single UnityEngine.Quaternion::y
	float ___y_1;
	// System.Single UnityEngine.Quaternion::z
	float ___z_2;
	// System.Single UnityEngine.Quaternion::w
	float ___w_3;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Quaternion_t754065749, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Quaternion_t754065749, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(Quaternion_t754065749, ___z_2)); }
	inline float get_z_2() const { return ___z_2; }
	inline float* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(float value)
	{
		___z_2 = value;
	}

	inline static int32_t get_offset_of_w_3() { return static_cast<int32_t>(offsetof(Quaternion_t754065749, ___w_3)); }
	inline float get_w_3() const { return ___w_3; }
	inline float* get_address_of_w_3() { return &___w_3; }
	inline void set_w_3(float value)
	{
		___w_3 = value;
	}
};

struct Quaternion_t754065749_StaticFields
{
public:
	// UnityEngine.Quaternion UnityEngine.Quaternion::identityQuaternion
	Quaternion_t754065749  ___identityQuaternion_4;

public:
	inline static int32_t get_offset_of_identityQuaternion_4() { return static_cast<int32_t>(offsetof(Quaternion_t754065749_StaticFields, ___identityQuaternion_4)); }
	inline Quaternion_t754065749  get_identityQuaternion_4() const { return ___identityQuaternion_4; }
	inline Quaternion_t754065749 * get_address_of_identityQuaternion_4() { return &___identityQuaternion_4; }
	inline void set_identityQuaternion_4(Quaternion_t754065749  value)
	{
		___identityQuaternion_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUATERNION_T754065749_H
#ifndef SMARTTERRAINREVISIONDATA_T1976771789_H
#define SMARTTERRAINREVISIONDATA_T1976771789_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.VuforiaManagerImpl/SmartTerrainRevisionData
#pragma pack(push, tp, 1)
struct  SmartTerrainRevisionData_t1976771789 
{
public:
	// System.Int32 Vuforia.VuforiaManagerImpl/SmartTerrainRevisionData::id
	int32_t ___id_0;
	// System.Int32 Vuforia.VuforiaManagerImpl/SmartTerrainRevisionData::revision
	int32_t ___revision_1;

public:
	inline static int32_t get_offset_of_id_0() { return static_cast<int32_t>(offsetof(SmartTerrainRevisionData_t1976771789, ___id_0)); }
	inline int32_t get_id_0() const { return ___id_0; }
	inline int32_t* get_address_of_id_0() { return &___id_0; }
	inline void set_id_0(int32_t value)
	{
		___id_0 = value;
	}

	inline static int32_t get_offset_of_revision_1() { return static_cast<int32_t>(offsetof(SmartTerrainRevisionData_t1976771789, ___revision_1)); }
	inline int32_t get_revision_1() const { return ___revision_1; }
	inline int32_t* get_address_of_revision_1() { return &___revision_1; }
	inline void set_revision_1(int32_t value)
	{
		___revision_1 = value;
	}
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SMARTTERRAINREVISIONDATA_T1976771789_H
#ifndef VIDEOMODEDATA_T2436408626_H
#define VIDEOMODEDATA_T2436408626_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.CameraDevice/VideoModeData
#pragma pack(push, tp, 1)
struct  VideoModeData_t2436408626 
{
public:
	// System.Int32 Vuforia.CameraDevice/VideoModeData::width
	int32_t ___width_0;
	// System.Int32 Vuforia.CameraDevice/VideoModeData::height
	int32_t ___height_1;
	// System.Single Vuforia.CameraDevice/VideoModeData::frameRate
	float ___frameRate_2;
	// System.Int32 Vuforia.CameraDevice/VideoModeData::unused
	int32_t ___unused_3;

public:
	inline static int32_t get_offset_of_width_0() { return static_cast<int32_t>(offsetof(VideoModeData_t2436408626, ___width_0)); }
	inline int32_t get_width_0() const { return ___width_0; }
	inline int32_t* get_address_of_width_0() { return &___width_0; }
	inline void set_width_0(int32_t value)
	{
		___width_0 = value;
	}

	inline static int32_t get_offset_of_height_1() { return static_cast<int32_t>(offsetof(VideoModeData_t2436408626, ___height_1)); }
	inline int32_t get_height_1() const { return ___height_1; }
	inline int32_t* get_address_of_height_1() { return &___height_1; }
	inline void set_height_1(int32_t value)
	{
		___height_1 = value;
	}

	inline static int32_t get_offset_of_frameRate_2() { return static_cast<int32_t>(offsetof(VideoModeData_t2436408626, ___frameRate_2)); }
	inline float get_frameRate_2() const { return ___frameRate_2; }
	inline float* get_address_of_frameRate_2() { return &___frameRate_2; }
	inline void set_frameRate_2(float value)
	{
		___frameRate_2 = value;
	}

	inline static int32_t get_offset_of_unused_3() { return static_cast<int32_t>(offsetof(VideoModeData_t2436408626, ___unused_3)); }
	inline int32_t get_unused_3() const { return ___unused_3; }
	inline int32_t* get_address_of_unused_3() { return &___unused_3; }
	inline void set_unused_3(int32_t value)
	{
		___unused_3 = value;
	}
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VIDEOMODEDATA_T2436408626_H
#ifndef CAMERAFIELDDATA_T3856148331_H
#define CAMERAFIELDDATA_T3856148331_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.CameraDeviceImpl/CameraFieldData
struct  CameraFieldData_t3856148331 
{
public:
	// System.String Vuforia.CameraDeviceImpl/CameraFieldData::KeyValue
	String_t* ___KeyValue_0;
	// System.Int32 Vuforia.CameraDeviceImpl/CameraFieldData::DataType
	int32_t ___DataType_1;
	// System.Int32 Vuforia.CameraDeviceImpl/CameraFieldData::Unused
	int32_t ___Unused_2;

public:
	inline static int32_t get_offset_of_KeyValue_0() { return static_cast<int32_t>(offsetof(CameraFieldData_t3856148331, ___KeyValue_0)); }
	inline String_t* get_KeyValue_0() const { return ___KeyValue_0; }
	inline String_t** get_address_of_KeyValue_0() { return &___KeyValue_0; }
	inline void set_KeyValue_0(String_t* value)
	{
		___KeyValue_0 = value;
		Il2CppCodeGenWriteBarrier((&___KeyValue_0), value);
	}

	inline static int32_t get_offset_of_DataType_1() { return static_cast<int32_t>(offsetof(CameraFieldData_t3856148331, ___DataType_1)); }
	inline int32_t get_DataType_1() const { return ___DataType_1; }
	inline int32_t* get_address_of_DataType_1() { return &___DataType_1; }
	inline void set_DataType_1(int32_t value)
	{
		___DataType_1 = value;
	}

	inline static int32_t get_offset_of_Unused_2() { return static_cast<int32_t>(offsetof(CameraFieldData_t3856148331, ___Unused_2)); }
	inline int32_t get_Unused_2() const { return ___Unused_2; }
	inline int32_t* get_address_of_Unused_2() { return &___Unused_2; }
	inline void set_Unused_2(int32_t value)
	{
		___Unused_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Vuforia.CameraDeviceImpl/CameraFieldData
#pragma pack(push, tp, 1)
struct CameraFieldData_t3856148331_marshaled_pinvoke
{
	char* ___KeyValue_0;
	int32_t ___DataType_1;
	int32_t ___Unused_2;
};
#pragma pack(pop, tp)
// Native definition for COM marshalling of Vuforia.CameraDeviceImpl/CameraFieldData
#pragma pack(push, tp, 1)
struct CameraFieldData_t3856148331_marshaled_com
{
	Il2CppChar* ___KeyValue_0;
	int32_t ___DataType_1;
	int32_t ___Unused_2;
};
#pragma pack(pop, tp)
#endif // CAMERAFIELDDATA_T3856148331_H
#ifndef VEC2I_T895597728_H
#define VEC2I_T895597728_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.VuforiaRenderer/Vec2I
#pragma pack(push, tp, 1)
struct  Vec2I_t895597728 
{
public:
	// System.Int32 Vuforia.VuforiaRenderer/Vec2I::x
	int32_t ___x_0;
	// System.Int32 Vuforia.VuforiaRenderer/Vec2I::y
	int32_t ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vec2I_t895597728, ___x_0)); }
	inline int32_t get_x_0() const { return ___x_0; }
	inline int32_t* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(int32_t value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vec2I_t895597728, ___y_1)); }
	inline int32_t get_y_1() const { return ___y_1; }
	inline int32_t* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(int32_t value)
	{
		___y_1 = value;
	}
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VEC2I_T895597728_H
#ifndef TRACKABLEIDPAIR_T1331427386_H
#define TRACKABLEIDPAIR_T1331427386_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.VuforiaManager/TrackableIdPair
struct  TrackableIdPair_t1331427386 
{
public:
	// System.Int32 Vuforia.VuforiaManager/TrackableIdPair::TrackableId
	int32_t ___TrackableId_0;
	// System.Int32 Vuforia.VuforiaManager/TrackableIdPair::ResultId
	int32_t ___ResultId_1;

public:
	inline static int32_t get_offset_of_TrackableId_0() { return static_cast<int32_t>(offsetof(TrackableIdPair_t1331427386, ___TrackableId_0)); }
	inline int32_t get_TrackableId_0() const { return ___TrackableId_0; }
	inline int32_t* get_address_of_TrackableId_0() { return &___TrackableId_0; }
	inline void set_TrackableId_0(int32_t value)
	{
		___TrackableId_0 = value;
	}

	inline static int32_t get_offset_of_ResultId_1() { return static_cast<int32_t>(offsetof(TrackableIdPair_t1331427386, ___ResultId_1)); }
	inline int32_t get_ResultId_1() const { return ___ResultId_1; }
	inline int32_t* get_address_of_ResultId_1() { return &___ResultId_1; }
	inline void set_ResultId_1(int32_t value)
	{
		___ResultId_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRACKABLEIDPAIR_T1331427386_H
#ifndef SMARTTERRAINTRACKERARCONTROLLER_T1900914161_H
#define SMARTTERRAINTRACKERARCONTROLLER_T1900914161_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.SmartTerrainTrackerARController
struct  SmartTerrainTrackerARController_t1900914161  : public ARController_t1205915989
{
public:
	// System.Boolean Vuforia.SmartTerrainTrackerARController::mAutoInitTracker
	bool ___mAutoInitTracker_1;
	// System.Boolean Vuforia.SmartTerrainTrackerARController::mAutoStartTracker
	bool ___mAutoStartTracker_2;
	// System.Boolean Vuforia.SmartTerrainTrackerARController::mAutoInitBuilder
	bool ___mAutoInitBuilder_3;
	// System.Single Vuforia.SmartTerrainTrackerARController::mSceneUnitsToMillimeter
	float ___mSceneUnitsToMillimeter_4;
	// System.Action Vuforia.SmartTerrainTrackerARController::mTrackerStarted
	Action_t3619184611 * ___mTrackerStarted_5;
	// System.Boolean Vuforia.SmartTerrainTrackerARController::mTrackerWasActiveBeforePause
	bool ___mTrackerWasActiveBeforePause_6;
	// System.Boolean Vuforia.SmartTerrainTrackerARController::mTrackerWasActiveBeforeDisabling
	bool ___mTrackerWasActiveBeforeDisabling_7;

public:
	inline static int32_t get_offset_of_mAutoInitTracker_1() { return static_cast<int32_t>(offsetof(SmartTerrainTrackerARController_t1900914161, ___mAutoInitTracker_1)); }
	inline bool get_mAutoInitTracker_1() const { return ___mAutoInitTracker_1; }
	inline bool* get_address_of_mAutoInitTracker_1() { return &___mAutoInitTracker_1; }
	inline void set_mAutoInitTracker_1(bool value)
	{
		___mAutoInitTracker_1 = value;
	}

	inline static int32_t get_offset_of_mAutoStartTracker_2() { return static_cast<int32_t>(offsetof(SmartTerrainTrackerARController_t1900914161, ___mAutoStartTracker_2)); }
	inline bool get_mAutoStartTracker_2() const { return ___mAutoStartTracker_2; }
	inline bool* get_address_of_mAutoStartTracker_2() { return &___mAutoStartTracker_2; }
	inline void set_mAutoStartTracker_2(bool value)
	{
		___mAutoStartTracker_2 = value;
	}

	inline static int32_t get_offset_of_mAutoInitBuilder_3() { return static_cast<int32_t>(offsetof(SmartTerrainTrackerARController_t1900914161, ___mAutoInitBuilder_3)); }
	inline bool get_mAutoInitBuilder_3() const { return ___mAutoInitBuilder_3; }
	inline bool* get_address_of_mAutoInitBuilder_3() { return &___mAutoInitBuilder_3; }
	inline void set_mAutoInitBuilder_3(bool value)
	{
		___mAutoInitBuilder_3 = value;
	}

	inline static int32_t get_offset_of_mSceneUnitsToMillimeter_4() { return static_cast<int32_t>(offsetof(SmartTerrainTrackerARController_t1900914161, ___mSceneUnitsToMillimeter_4)); }
	inline float get_mSceneUnitsToMillimeter_4() const { return ___mSceneUnitsToMillimeter_4; }
	inline float* get_address_of_mSceneUnitsToMillimeter_4() { return &___mSceneUnitsToMillimeter_4; }
	inline void set_mSceneUnitsToMillimeter_4(float value)
	{
		___mSceneUnitsToMillimeter_4 = value;
	}

	inline static int32_t get_offset_of_mTrackerStarted_5() { return static_cast<int32_t>(offsetof(SmartTerrainTrackerARController_t1900914161, ___mTrackerStarted_5)); }
	inline Action_t3619184611 * get_mTrackerStarted_5() const { return ___mTrackerStarted_5; }
	inline Action_t3619184611 ** get_address_of_mTrackerStarted_5() { return &___mTrackerStarted_5; }
	inline void set_mTrackerStarted_5(Action_t3619184611 * value)
	{
		___mTrackerStarted_5 = value;
		Il2CppCodeGenWriteBarrier((&___mTrackerStarted_5), value);
	}

	inline static int32_t get_offset_of_mTrackerWasActiveBeforePause_6() { return static_cast<int32_t>(offsetof(SmartTerrainTrackerARController_t1900914161, ___mTrackerWasActiveBeforePause_6)); }
	inline bool get_mTrackerWasActiveBeforePause_6() const { return ___mTrackerWasActiveBeforePause_6; }
	inline bool* get_address_of_mTrackerWasActiveBeforePause_6() { return &___mTrackerWasActiveBeforePause_6; }
	inline void set_mTrackerWasActiveBeforePause_6(bool value)
	{
		___mTrackerWasActiveBeforePause_6 = value;
	}

	inline static int32_t get_offset_of_mTrackerWasActiveBeforeDisabling_7() { return static_cast<int32_t>(offsetof(SmartTerrainTrackerARController_t1900914161, ___mTrackerWasActiveBeforeDisabling_7)); }
	inline bool get_mTrackerWasActiveBeforeDisabling_7() const { return ___mTrackerWasActiveBeforeDisabling_7; }
	inline bool* get_address_of_mTrackerWasActiveBeforeDisabling_7() { return &___mTrackerWasActiveBeforeDisabling_7; }
	inline void set_mTrackerWasActiveBeforeDisabling_7(bool value)
	{
		___mTrackerWasActiveBeforeDisabling_7 = value;
	}
};

struct SmartTerrainTrackerARController_t1900914161_StaticFields
{
public:
	// Vuforia.SmartTerrainTrackerARController Vuforia.SmartTerrainTrackerARController::mInstance
	SmartTerrainTrackerARController_t1900914161 * ___mInstance_8;
	// System.Object Vuforia.SmartTerrainTrackerARController::mPadlock
	RuntimeObject * ___mPadlock_9;

public:
	inline static int32_t get_offset_of_mInstance_8() { return static_cast<int32_t>(offsetof(SmartTerrainTrackerARController_t1900914161_StaticFields, ___mInstance_8)); }
	inline SmartTerrainTrackerARController_t1900914161 * get_mInstance_8() const { return ___mInstance_8; }
	inline SmartTerrainTrackerARController_t1900914161 ** get_address_of_mInstance_8() { return &___mInstance_8; }
	inline void set_mInstance_8(SmartTerrainTrackerARController_t1900914161 * value)
	{
		___mInstance_8 = value;
		Il2CppCodeGenWriteBarrier((&___mInstance_8), value);
	}

	inline static int32_t get_offset_of_mPadlock_9() { return static_cast<int32_t>(offsetof(SmartTerrainTrackerARController_t1900914161_StaticFields, ___mPadlock_9)); }
	inline RuntimeObject * get_mPadlock_9() const { return ___mPadlock_9; }
	inline RuntimeObject ** get_address_of_mPadlock_9() { return &___mPadlock_9; }
	inline void set_mPadlock_9(RuntimeObject * value)
	{
		___mPadlock_9 = value;
		Il2CppCodeGenWriteBarrier((&___mPadlock_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SMARTTERRAINTRACKERARCONTROLLER_T1900914161_H
#ifndef OBJECTTRACKER_T4023638979_H
#define OBJECTTRACKER_T4023638979_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.ObjectTracker
struct  ObjectTracker_t4023638979  : public Tracker_t1731515739
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OBJECTTRACKER_T4023638979_H
#ifndef DATABASELOADARCONTROLLER_T1838845437_H
#define DATABASELOADARCONTROLLER_T1838845437_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.DatabaseLoadARController
struct  DatabaseLoadARController_t1838845437  : public ARController_t1205915989
{
public:
	// System.Boolean Vuforia.DatabaseLoadARController::mDatasetsLoaded
	bool ___mDatasetsLoaded_1;
	// System.Collections.Generic.List`1<System.String> Vuforia.DatabaseLoadARController::mExternalDatasetRoots
	List_1_t1230370061 * ___mExternalDatasetRoots_2;
	// System.String[] Vuforia.DatabaseLoadARController::mDataSetsToLoad
	StringU5BU5D_t2298632947* ___mDataSetsToLoad_3;
	// System.String[] Vuforia.DatabaseLoadARController::mDataSetsToActivate
	StringU5BU5D_t2298632947* ___mDataSetsToActivate_4;

public:
	inline static int32_t get_offset_of_mDatasetsLoaded_1() { return static_cast<int32_t>(offsetof(DatabaseLoadARController_t1838845437, ___mDatasetsLoaded_1)); }
	inline bool get_mDatasetsLoaded_1() const { return ___mDatasetsLoaded_1; }
	inline bool* get_address_of_mDatasetsLoaded_1() { return &___mDatasetsLoaded_1; }
	inline void set_mDatasetsLoaded_1(bool value)
	{
		___mDatasetsLoaded_1 = value;
	}

	inline static int32_t get_offset_of_mExternalDatasetRoots_2() { return static_cast<int32_t>(offsetof(DatabaseLoadARController_t1838845437, ___mExternalDatasetRoots_2)); }
	inline List_1_t1230370061 * get_mExternalDatasetRoots_2() const { return ___mExternalDatasetRoots_2; }
	inline List_1_t1230370061 ** get_address_of_mExternalDatasetRoots_2() { return &___mExternalDatasetRoots_2; }
	inline void set_mExternalDatasetRoots_2(List_1_t1230370061 * value)
	{
		___mExternalDatasetRoots_2 = value;
		Il2CppCodeGenWriteBarrier((&___mExternalDatasetRoots_2), value);
	}

	inline static int32_t get_offset_of_mDataSetsToLoad_3() { return static_cast<int32_t>(offsetof(DatabaseLoadARController_t1838845437, ___mDataSetsToLoad_3)); }
	inline StringU5BU5D_t2298632947* get_mDataSetsToLoad_3() const { return ___mDataSetsToLoad_3; }
	inline StringU5BU5D_t2298632947** get_address_of_mDataSetsToLoad_3() { return &___mDataSetsToLoad_3; }
	inline void set_mDataSetsToLoad_3(StringU5BU5D_t2298632947* value)
	{
		___mDataSetsToLoad_3 = value;
		Il2CppCodeGenWriteBarrier((&___mDataSetsToLoad_3), value);
	}

	inline static int32_t get_offset_of_mDataSetsToActivate_4() { return static_cast<int32_t>(offsetof(DatabaseLoadARController_t1838845437, ___mDataSetsToActivate_4)); }
	inline StringU5BU5D_t2298632947* get_mDataSetsToActivate_4() const { return ___mDataSetsToActivate_4; }
	inline StringU5BU5D_t2298632947** get_address_of_mDataSetsToActivate_4() { return &___mDataSetsToActivate_4; }
	inline void set_mDataSetsToActivate_4(StringU5BU5D_t2298632947* value)
	{
		___mDataSetsToActivate_4 = value;
		Il2CppCodeGenWriteBarrier((&___mDataSetsToActivate_4), value);
	}
};

struct DatabaseLoadARController_t1838845437_StaticFields
{
public:
	// Vuforia.DatabaseLoadARController Vuforia.DatabaseLoadARController::mInstance
	DatabaseLoadARController_t1838845437 * ___mInstance_5;
	// System.Object Vuforia.DatabaseLoadARController::mPadlock
	RuntimeObject * ___mPadlock_6;

public:
	inline static int32_t get_offset_of_mInstance_5() { return static_cast<int32_t>(offsetof(DatabaseLoadARController_t1838845437_StaticFields, ___mInstance_5)); }
	inline DatabaseLoadARController_t1838845437 * get_mInstance_5() const { return ___mInstance_5; }
	inline DatabaseLoadARController_t1838845437 ** get_address_of_mInstance_5() { return &___mInstance_5; }
	inline void set_mInstance_5(DatabaseLoadARController_t1838845437 * value)
	{
		___mInstance_5 = value;
		Il2CppCodeGenWriteBarrier((&___mInstance_5), value);
	}

	inline static int32_t get_offset_of_mPadlock_6() { return static_cast<int32_t>(offsetof(DatabaseLoadARController_t1838845437_StaticFields, ___mPadlock_6)); }
	inline RuntimeObject * get_mPadlock_6() const { return ___mPadlock_6; }
	inline RuntimeObject ** get_address_of_mPadlock_6() { return &___mPadlock_6; }
	inline void set_mPadlock_6(RuntimeObject * value)
	{
		___mPadlock_6 = value;
		Il2CppCodeGenWriteBarrier((&___mPadlock_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATABASELOADARCONTROLLER_T1838845437_H
#ifndef RECTANGLEDATA_T1094133618_H
#define RECTANGLEDATA_T1094133618_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.RectangleData
#pragma pack(push, tp, 1)
struct  RectangleData_t1094133618 
{
public:
	// System.Single Vuforia.RectangleData::leftTopX
	float ___leftTopX_0;
	// System.Single Vuforia.RectangleData::leftTopY
	float ___leftTopY_1;
	// System.Single Vuforia.RectangleData::rightBottomX
	float ___rightBottomX_2;
	// System.Single Vuforia.RectangleData::rightBottomY
	float ___rightBottomY_3;

public:
	inline static int32_t get_offset_of_leftTopX_0() { return static_cast<int32_t>(offsetof(RectangleData_t1094133618, ___leftTopX_0)); }
	inline float get_leftTopX_0() const { return ___leftTopX_0; }
	inline float* get_address_of_leftTopX_0() { return &___leftTopX_0; }
	inline void set_leftTopX_0(float value)
	{
		___leftTopX_0 = value;
	}

	inline static int32_t get_offset_of_leftTopY_1() { return static_cast<int32_t>(offsetof(RectangleData_t1094133618, ___leftTopY_1)); }
	inline float get_leftTopY_1() const { return ___leftTopY_1; }
	inline float* get_address_of_leftTopY_1() { return &___leftTopY_1; }
	inline void set_leftTopY_1(float value)
	{
		___leftTopY_1 = value;
	}

	inline static int32_t get_offset_of_rightBottomX_2() { return static_cast<int32_t>(offsetof(RectangleData_t1094133618, ___rightBottomX_2)); }
	inline float get_rightBottomX_2() const { return ___rightBottomX_2; }
	inline float* get_address_of_rightBottomX_2() { return &___rightBottomX_2; }
	inline void set_rightBottomX_2(float value)
	{
		___rightBottomX_2 = value;
	}

	inline static int32_t get_offset_of_rightBottomY_3() { return static_cast<int32_t>(offsetof(RectangleData_t1094133618, ___rightBottomY_3)); }
	inline float get_rightBottomY_3() const { return ___rightBottomY_3; }
	inline float* get_address_of_rightBottomY_3() { return &___rightBottomY_3; }
	inline void set_rightBottomY_3(float value)
	{
		___rightBottomY_3 = value;
	}
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RECTANGLEDATA_T1094133618_H
#ifndef SMARTTERRAININITIALIZATIONINFO_T1502783984_H
#define SMARTTERRAININITIALIZATIONINFO_T1502783984_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.SmartTerrainInitializationInfo
struct  SmartTerrainInitializationInfo_t1502783984 
{
public:
	union
	{
		struct
		{
		};
		uint8_t SmartTerrainInitializationInfo_t1502783984__padding[1];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SMARTTERRAININITIALIZATIONINFO_T1502783984_H
#ifndef TEXTTRACKER_T3669255194_H
#define TEXTTRACKER_T3669255194_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.TextTracker
struct  TextTracker_t3669255194  : public Tracker_t1731515739
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTTRACKER_T3669255194_H
#ifndef RECTANGLEINTDATA_T2989567989_H
#define RECTANGLEINTDATA_T2989567989_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.RectangleIntData
#pragma pack(push, tp, 1)
struct  RectangleIntData_t2989567989 
{
public:
	// System.Int32 Vuforia.RectangleIntData::leftTopX
	int32_t ___leftTopX_0;
	// System.Int32 Vuforia.RectangleIntData::leftTopY
	int32_t ___leftTopY_1;
	// System.Int32 Vuforia.RectangleIntData::rightBottomX
	int32_t ___rightBottomX_2;
	// System.Int32 Vuforia.RectangleIntData::rightBottomY
	int32_t ___rightBottomY_3;

public:
	inline static int32_t get_offset_of_leftTopX_0() { return static_cast<int32_t>(offsetof(RectangleIntData_t2989567989, ___leftTopX_0)); }
	inline int32_t get_leftTopX_0() const { return ___leftTopX_0; }
	inline int32_t* get_address_of_leftTopX_0() { return &___leftTopX_0; }
	inline void set_leftTopX_0(int32_t value)
	{
		___leftTopX_0 = value;
	}

	inline static int32_t get_offset_of_leftTopY_1() { return static_cast<int32_t>(offsetof(RectangleIntData_t2989567989, ___leftTopY_1)); }
	inline int32_t get_leftTopY_1() const { return ___leftTopY_1; }
	inline int32_t* get_address_of_leftTopY_1() { return &___leftTopY_1; }
	inline void set_leftTopY_1(int32_t value)
	{
		___leftTopY_1 = value;
	}

	inline static int32_t get_offset_of_rightBottomX_2() { return static_cast<int32_t>(offsetof(RectangleIntData_t2989567989, ___rightBottomX_2)); }
	inline int32_t get_rightBottomX_2() const { return ___rightBottomX_2; }
	inline int32_t* get_address_of_rightBottomX_2() { return &___rightBottomX_2; }
	inline void set_rightBottomX_2(int32_t value)
	{
		___rightBottomX_2 = value;
	}

	inline static int32_t get_offset_of_rightBottomY_3() { return static_cast<int32_t>(offsetof(RectangleIntData_t2989567989, ___rightBottomY_3)); }
	inline int32_t get_rightBottomY_3() const { return ___rightBottomY_3; }
	inline int32_t* get_address_of_rightBottomY_3() { return &___rightBottomY_3; }
	inline void set_rightBottomY_3(int32_t value)
	{
		___rightBottomY_3 = value;
	}
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RECTANGLEINTDATA_T2989567989_H
#ifndef TIMESPAN_T1805120695_H
#define TIMESPAN_T1805120695_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.TimeSpan
struct  TimeSpan_t1805120695 
{
public:
	// System.Int64 System.TimeSpan::_ticks
	int64_t ____ticks_3;

public:
	inline static int32_t get_offset_of__ticks_3() { return static_cast<int32_t>(offsetof(TimeSpan_t1805120695, ____ticks_3)); }
	inline int64_t get__ticks_3() const { return ____ticks_3; }
	inline int64_t* get_address_of__ticks_3() { return &____ticks_3; }
	inline void set__ticks_3(int64_t value)
	{
		____ticks_3 = value;
	}
};

struct TimeSpan_t1805120695_StaticFields
{
public:
	// System.TimeSpan System.TimeSpan::MaxValue
	TimeSpan_t1805120695  ___MaxValue_0;
	// System.TimeSpan System.TimeSpan::MinValue
	TimeSpan_t1805120695  ___MinValue_1;
	// System.TimeSpan System.TimeSpan::Zero
	TimeSpan_t1805120695  ___Zero_2;

public:
	inline static int32_t get_offset_of_MaxValue_0() { return static_cast<int32_t>(offsetof(TimeSpan_t1805120695_StaticFields, ___MaxValue_0)); }
	inline TimeSpan_t1805120695  get_MaxValue_0() const { return ___MaxValue_0; }
	inline TimeSpan_t1805120695 * get_address_of_MaxValue_0() { return &___MaxValue_0; }
	inline void set_MaxValue_0(TimeSpan_t1805120695  value)
	{
		___MaxValue_0 = value;
	}

	inline static int32_t get_offset_of_MinValue_1() { return static_cast<int32_t>(offsetof(TimeSpan_t1805120695_StaticFields, ___MinValue_1)); }
	inline TimeSpan_t1805120695  get_MinValue_1() const { return ___MinValue_1; }
	inline TimeSpan_t1805120695 * get_address_of_MinValue_1() { return &___MinValue_1; }
	inline void set_MinValue_1(TimeSpan_t1805120695  value)
	{
		___MinValue_1 = value;
	}

	inline static int32_t get_offset_of_Zero_2() { return static_cast<int32_t>(offsetof(TimeSpan_t1805120695_StaticFields, ___Zero_2)); }
	inline TimeSpan_t1805120695  get_Zero_2() const { return ___Zero_2; }
	inline TimeSpan_t1805120695 * get_address_of_Zero_2() { return &___Zero_2; }
	inline void set_Zero_2(TimeSpan_t1805120695  value)
	{
		___Zero_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TIMESPAN_T1805120695_H
#ifndef ENUM_T978401890_H
#define ENUM_T978401890_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t978401890  : public ValueType_t1306072465
{
public:

public:
};

struct Enum_t978401890_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t45629911* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t978401890_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t45629911* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t45629911** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t45629911* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t978401890_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t978401890_marshaled_com
{
};
#endif // ENUM_T978401890_H
#ifndef WEBCAMTEXADAPTORIMPL_T3288976692_H
#define WEBCAMTEXADAPTORIMPL_T3288976692_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.WebCamTexAdaptorImpl
struct  WebCamTexAdaptorImpl_t3288976692  : public WebCamTexAdaptor_t433428952
{
public:
	// UnityEngine.WebCamTexture Vuforia.WebCamTexAdaptorImpl::mWebCamTexture
	WebCamTexture_t2673512900 * ___mWebCamTexture_0;
	// UnityEngine.AsyncOperation Vuforia.WebCamTexAdaptorImpl::mCheckCameraPermissions
	AsyncOperation_t2744032732 * ___mCheckCameraPermissions_1;

public:
	inline static int32_t get_offset_of_mWebCamTexture_0() { return static_cast<int32_t>(offsetof(WebCamTexAdaptorImpl_t3288976692, ___mWebCamTexture_0)); }
	inline WebCamTexture_t2673512900 * get_mWebCamTexture_0() const { return ___mWebCamTexture_0; }
	inline WebCamTexture_t2673512900 ** get_address_of_mWebCamTexture_0() { return &___mWebCamTexture_0; }
	inline void set_mWebCamTexture_0(WebCamTexture_t2673512900 * value)
	{
		___mWebCamTexture_0 = value;
		Il2CppCodeGenWriteBarrier((&___mWebCamTexture_0), value);
	}

	inline static int32_t get_offset_of_mCheckCameraPermissions_1() { return static_cast<int32_t>(offsetof(WebCamTexAdaptorImpl_t3288976692, ___mCheckCameraPermissions_1)); }
	inline AsyncOperation_t2744032732 * get_mCheckCameraPermissions_1() const { return ___mCheckCameraPermissions_1; }
	inline AsyncOperation_t2744032732 ** get_address_of_mCheckCameraPermissions_1() { return &___mCheckCameraPermissions_1; }
	inline void set_mCheckCameraPermissions_1(AsyncOperation_t2744032732 * value)
	{
		___mCheckCameraPermissions_1 = value;
		Il2CppCodeGenWriteBarrier((&___mCheckCameraPermissions_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WEBCAMTEXADAPTORIMPL_T3288976692_H
#ifndef SMARTTERRAINTRACKERCONFIGURATION_T657164935_H
#define SMARTTERRAINTRACKERCONFIGURATION_T657164935_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.VuforiaAbstractConfiguration/SmartTerrainTrackerConfiguration
struct  SmartTerrainTrackerConfiguration_t657164935  : public TrackerConfiguration_t3022484256
{
public:
	// System.Boolean Vuforia.VuforiaAbstractConfiguration/SmartTerrainTrackerConfiguration::autoInitBuilder
	bool ___autoInitBuilder_2;
	// System.Single Vuforia.VuforiaAbstractConfiguration/SmartTerrainTrackerConfiguration::sceneUnitsToMillimeter
	float ___sceneUnitsToMillimeter_3;

public:
	inline static int32_t get_offset_of_autoInitBuilder_2() { return static_cast<int32_t>(offsetof(SmartTerrainTrackerConfiguration_t657164935, ___autoInitBuilder_2)); }
	inline bool get_autoInitBuilder_2() const { return ___autoInitBuilder_2; }
	inline bool* get_address_of_autoInitBuilder_2() { return &___autoInitBuilder_2; }
	inline void set_autoInitBuilder_2(bool value)
	{
		___autoInitBuilder_2 = value;
	}

	inline static int32_t get_offset_of_sceneUnitsToMillimeter_3() { return static_cast<int32_t>(offsetof(SmartTerrainTrackerConfiguration_t657164935, ___sceneUnitsToMillimeter_3)); }
	inline float get_sceneUnitsToMillimeter_3() const { return ___sceneUnitsToMillimeter_3; }
	inline float* get_address_of_sceneUnitsToMillimeter_3() { return &___sceneUnitsToMillimeter_3; }
	inline void set_sceneUnitsToMillimeter_3(float value)
	{
		___sceneUnitsToMillimeter_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SMARTTERRAINTRACKERCONFIGURATION_T657164935_H
#ifndef SMARTTERRAINTRACKER_T1505679812_H
#define SMARTTERRAINTRACKER_T1505679812_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.SmartTerrainTracker
struct  SmartTerrainTracker_t1505679812  : public Tracker_t1731515739
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SMARTTERRAINTRACKER_T1505679812_H
#ifndef CAMERADEVICEMODE_T1785007815_H
#define CAMERADEVICEMODE_T1785007815_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.CameraDevice/CameraDeviceMode
struct  CameraDeviceMode_t1785007815 
{
public:
	// System.Int32 Vuforia.CameraDevice/CameraDeviceMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(CameraDeviceMode_t1785007815, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAMERADEVICEMODE_T1785007815_H
#ifndef CAMERADIRECTION_T1337526284_H
#define CAMERADIRECTION_T1337526284_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.CameraDevice/CameraDirection
struct  CameraDirection_t1337526284 
{
public:
	// System.Int32 Vuforia.CameraDevice/CameraDirection::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(CameraDirection_t1337526284, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAMERADIRECTION_T1337526284_H
#ifndef U3CU3EC__DISPLAYCLASS86_0_T2077910410_H
#define U3CU3EC__DISPLAYCLASS86_0_T2077910410_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.VuforiaManagerImpl/<>c__DisplayClass86_0
struct  U3CU3Ec__DisplayClass86_0_t2077910410  : public RuntimeObject
{
public:
	// Vuforia.VuforiaManager/TrackableIdPair Vuforia.VuforiaManagerImpl/<>c__DisplayClass86_0::id
	TrackableIdPair_t1331427386  ___id_0;

public:
	inline static int32_t get_offset_of_id_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass86_0_t2077910410, ___id_0)); }
	inline TrackableIdPair_t1331427386  get_id_0() const { return ___id_0; }
	inline TrackableIdPair_t1331427386 * get_address_of_id_0() { return &___id_0; }
	inline void set_id_0(TrackableIdPair_t1331427386  value)
	{
		___id_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS86_0_T2077910410_H
#ifndef EYEWEARTYPE_T4127891436_H
#define EYEWEARTYPE_T4127891436_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.DigitalEyewearARController/EyewearType
struct  EyewearType_t4127891436 
{
public:
	// System.Int32 Vuforia.DigitalEyewearARController/EyewearType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(EyewearType_t4127891436, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EYEWEARTYPE_T4127891436_H
#ifndef RENDEREVENT_T2231315398_H
#define RENDEREVENT_T2231315398_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.VuforiaRendererImpl/RenderEvent
struct  RenderEvent_t2231315398 
{
public:
	// System.Int32 Vuforia.VuforiaRendererImpl/RenderEvent::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(RenderEvent_t2231315398, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RENDEREVENT_T2231315398_H
#ifndef UPDIRECTION_T1212967134_H
#define UPDIRECTION_T1212967134_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.TextTrackerImpl/UpDirection
struct  UpDirection_t1212967134 
{
public:
	// System.Int32 Vuforia.TextTrackerImpl/UpDirection::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(UpDirection_t1212967134, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UPDIRECTION_T1212967134_H
#ifndef INITERROR_T3486678342_H
#define INITERROR_T3486678342_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.VuforiaUnity/InitError
struct  InitError_t3486678342 
{
public:
	// System.Int32 Vuforia.VuforiaUnity/InitError::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(InitError_t3486678342, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INITERROR_T3486678342_H
#ifndef STEREOFRAMEWORK_T3060055999_H
#define STEREOFRAMEWORK_T3060055999_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.DigitalEyewearARController/StereoFramework
struct  StereoFramework_t3060055999 
{
public:
	// System.Int32 Vuforia.DigitalEyewearARController/StereoFramework::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(StereoFramework_t3060055999, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STEREOFRAMEWORK_T3060055999_H
#ifndef SEETHROUGHCONFIGURATION_T1856493377_H
#define SEETHROUGHCONFIGURATION_T1856493377_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.DigitalEyewearARController/SeeThroughConfiguration
struct  SeeThroughConfiguration_t1856493377 
{
public:
	// System.Int32 Vuforia.DigitalEyewearARController/SeeThroughConfiguration::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(SeeThroughConfiguration_t1856493377, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SEETHROUGHCONFIGURATION_T1856493377_H
#ifndef CLIPPING_MODE_T306275072_H
#define CLIPPING_MODE_T306275072_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.HideExcessAreaAbstractBehaviour/CLIPPING_MODE
struct  CLIPPING_MODE_t306275072 
{
public:
	// System.Int32 Vuforia.HideExcessAreaAbstractBehaviour/CLIPPING_MODE::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(CLIPPING_MODE_t306275072, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CLIPPING_MODE_T306275072_H
#ifndef WORDPREFABCREATIONMODE_T2868429441_H
#define WORDPREFABCREATIONMODE_T2868429441_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.WordPrefabCreationMode
struct  WordPrefabCreationMode_t2868429441 
{
public:
	// System.Int32 Vuforia.WordPrefabCreationMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(WordPrefabCreationMode_t2868429441, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WORDPREFABCREATIONMODE_T2868429441_H
#ifndef SMARTTERRAINTRACKERIMPL_T3354406207_H
#define SMARTTERRAINTRACKERIMPL_T3354406207_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.SmartTerrainTrackerImpl
struct  SmartTerrainTrackerImpl_t3354406207  : public SmartTerrainTracker_t1505679812
{
public:
	// System.Single Vuforia.SmartTerrainTrackerImpl::mScaleToMillimeter
	float ___mScaleToMillimeter_1;
	// Vuforia.SmartTerrainBuilderImpl Vuforia.SmartTerrainTrackerImpl::mSmartTerrainBuilder
	SmartTerrainBuilderImpl_t4234039031 * ___mSmartTerrainBuilder_2;

public:
	inline static int32_t get_offset_of_mScaleToMillimeter_1() { return static_cast<int32_t>(offsetof(SmartTerrainTrackerImpl_t3354406207, ___mScaleToMillimeter_1)); }
	inline float get_mScaleToMillimeter_1() const { return ___mScaleToMillimeter_1; }
	inline float* get_address_of_mScaleToMillimeter_1() { return &___mScaleToMillimeter_1; }
	inline void set_mScaleToMillimeter_1(float value)
	{
		___mScaleToMillimeter_1 = value;
	}

	inline static int32_t get_offset_of_mSmartTerrainBuilder_2() { return static_cast<int32_t>(offsetof(SmartTerrainTrackerImpl_t3354406207, ___mSmartTerrainBuilder_2)); }
	inline SmartTerrainBuilderImpl_t4234039031 * get_mSmartTerrainBuilder_2() const { return ___mSmartTerrainBuilder_2; }
	inline SmartTerrainBuilderImpl_t4234039031 ** get_address_of_mSmartTerrainBuilder_2() { return &___mSmartTerrainBuilder_2; }
	inline void set_mSmartTerrainBuilder_2(SmartTerrainBuilderImpl_t4234039031 * value)
	{
		___mSmartTerrainBuilder_2 = value;
		Il2CppCodeGenWriteBarrier((&___mSmartTerrainBuilder_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SMARTTERRAINTRACKERIMPL_T3354406207_H
#ifndef TEXTTRACKERIMPL_T1798165375_H
#define TEXTTRACKERIMPL_T1798165375_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.TextTrackerImpl
struct  TextTrackerImpl_t1798165375  : public TextTracker_t3669255194
{
public:
	// Vuforia.WordList Vuforia.TextTrackerImpl::mWordList
	WordList_t3717410066 * ___mWordList_1;

public:
	inline static int32_t get_offset_of_mWordList_1() { return static_cast<int32_t>(offsetof(TextTrackerImpl_t1798165375, ___mWordList_1)); }
	inline WordList_t3717410066 * get_mWordList_1() const { return ___mWordList_1; }
	inline WordList_t3717410066 ** get_address_of_mWordList_1() { return &___mWordList_1; }
	inline void set_mWordList_1(WordList_t3717410066 * value)
	{
		___mWordList_1 = value;
		Il2CppCodeGenWriteBarrier((&___mWordList_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTTRACKERIMPL_T1798165375_H
#ifndef WORLDCENTERMODE_T2621860492_H
#define WORLDCENTERMODE_T2621860492_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.VuforiaARController/WorldCenterMode
struct  WorldCenterMode_t2621860492 
{
public:
	// System.Int32 Vuforia.VuforiaARController/WorldCenterMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(WorldCenterMode_t2621860492, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WORLDCENTERMODE_T2621860492_H
#ifndef MODEL_CORRECTION_MODE_T3621708838_H
#define MODEL_CORRECTION_MODE_T3621708838_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.RotationalDeviceTracker/MODEL_CORRECTION_MODE
struct  MODEL_CORRECTION_MODE_t3621708838 
{
public:
	// System.Int32 Vuforia.RotationalDeviceTracker/MODEL_CORRECTION_MODE::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(MODEL_CORRECTION_MODE_t3621708838, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MODEL_CORRECTION_MODE_T3621708838_H
#ifndef FRAMESTATE_T3340962930_H
#define FRAMESTATE_T3340962930_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.VuforiaManagerImpl/FrameState
#pragma pack(push, tp, 1)
struct  FrameState_t3340962930 
{
public:
	// System.IntPtr Vuforia.VuforiaManagerImpl/FrameState::trackableDataArray
	IntPtr_t ___trackableDataArray_0;
	// System.IntPtr Vuforia.VuforiaManagerImpl/FrameState::vbDataArray
	IntPtr_t ___vbDataArray_1;
	// System.IntPtr Vuforia.VuforiaManagerImpl/FrameState::wordResultArray
	IntPtr_t ___wordResultArray_2;
	// System.IntPtr Vuforia.VuforiaManagerImpl/FrameState::newWordDataArray
	IntPtr_t ___newWordDataArray_3;
	// System.IntPtr Vuforia.VuforiaManagerImpl/FrameState::vuMarkResultArray
	IntPtr_t ___vuMarkResultArray_4;
	// System.IntPtr Vuforia.VuforiaManagerImpl/FrameState::newVuMarkDataArray
	IntPtr_t ___newVuMarkDataArray_5;
	// System.IntPtr Vuforia.VuforiaManagerImpl/FrameState::propTrackableDataArray
	IntPtr_t ___propTrackableDataArray_6;
	// System.IntPtr Vuforia.VuforiaManagerImpl/FrameState::smartTerrainRevisionsArray
	IntPtr_t ___smartTerrainRevisionsArray_7;
	// System.IntPtr Vuforia.VuforiaManagerImpl/FrameState::updatedSurfacesArray
	IntPtr_t ___updatedSurfacesArray_8;
	// System.IntPtr Vuforia.VuforiaManagerImpl/FrameState::updatedPropsArray
	IntPtr_t ___updatedPropsArray_9;
	// System.Int32 Vuforia.VuforiaManagerImpl/FrameState::numTrackableResults
	int32_t ___numTrackableResults_10;
	// System.Int32 Vuforia.VuforiaManagerImpl/FrameState::numVirtualButtonResults
	int32_t ___numVirtualButtonResults_11;
	// System.Int32 Vuforia.VuforiaManagerImpl/FrameState::frameIndex
	int32_t ___frameIndex_12;
	// System.Int32 Vuforia.VuforiaManagerImpl/FrameState::numWordResults
	int32_t ___numWordResults_13;
	// System.Int32 Vuforia.VuforiaManagerImpl/FrameState::numNewWords
	int32_t ___numNewWords_14;
	// System.Int32 Vuforia.VuforiaManagerImpl/FrameState::numVuMarkResults
	int32_t ___numVuMarkResults_15;
	// System.Int32 Vuforia.VuforiaManagerImpl/FrameState::numNewVuMarks
	int32_t ___numNewVuMarks_16;
	// System.Int32 Vuforia.VuforiaManagerImpl/FrameState::numPropTrackableResults
	int32_t ___numPropTrackableResults_17;
	// System.Int32 Vuforia.VuforiaManagerImpl/FrameState::numSmartTerrainRevisions
	int32_t ___numSmartTerrainRevisions_18;
	// System.Int32 Vuforia.VuforiaManagerImpl/FrameState::numUpdatedSurfaces
	int32_t ___numUpdatedSurfaces_19;
	// System.Int32 Vuforia.VuforiaManagerImpl/FrameState::numUpdatedProps
	int32_t ___numUpdatedProps_20;
	// System.Int32 Vuforia.VuforiaManagerImpl/FrameState::deviceTrackableId
	int32_t ___deviceTrackableId_21;

public:
	inline static int32_t get_offset_of_trackableDataArray_0() { return static_cast<int32_t>(offsetof(FrameState_t3340962930, ___trackableDataArray_0)); }
	inline IntPtr_t get_trackableDataArray_0() const { return ___trackableDataArray_0; }
	inline IntPtr_t* get_address_of_trackableDataArray_0() { return &___trackableDataArray_0; }
	inline void set_trackableDataArray_0(IntPtr_t value)
	{
		___trackableDataArray_0 = value;
	}

	inline static int32_t get_offset_of_vbDataArray_1() { return static_cast<int32_t>(offsetof(FrameState_t3340962930, ___vbDataArray_1)); }
	inline IntPtr_t get_vbDataArray_1() const { return ___vbDataArray_1; }
	inline IntPtr_t* get_address_of_vbDataArray_1() { return &___vbDataArray_1; }
	inline void set_vbDataArray_1(IntPtr_t value)
	{
		___vbDataArray_1 = value;
	}

	inline static int32_t get_offset_of_wordResultArray_2() { return static_cast<int32_t>(offsetof(FrameState_t3340962930, ___wordResultArray_2)); }
	inline IntPtr_t get_wordResultArray_2() const { return ___wordResultArray_2; }
	inline IntPtr_t* get_address_of_wordResultArray_2() { return &___wordResultArray_2; }
	inline void set_wordResultArray_2(IntPtr_t value)
	{
		___wordResultArray_2 = value;
	}

	inline static int32_t get_offset_of_newWordDataArray_3() { return static_cast<int32_t>(offsetof(FrameState_t3340962930, ___newWordDataArray_3)); }
	inline IntPtr_t get_newWordDataArray_3() const { return ___newWordDataArray_3; }
	inline IntPtr_t* get_address_of_newWordDataArray_3() { return &___newWordDataArray_3; }
	inline void set_newWordDataArray_3(IntPtr_t value)
	{
		___newWordDataArray_3 = value;
	}

	inline static int32_t get_offset_of_vuMarkResultArray_4() { return static_cast<int32_t>(offsetof(FrameState_t3340962930, ___vuMarkResultArray_4)); }
	inline IntPtr_t get_vuMarkResultArray_4() const { return ___vuMarkResultArray_4; }
	inline IntPtr_t* get_address_of_vuMarkResultArray_4() { return &___vuMarkResultArray_4; }
	inline void set_vuMarkResultArray_4(IntPtr_t value)
	{
		___vuMarkResultArray_4 = value;
	}

	inline static int32_t get_offset_of_newVuMarkDataArray_5() { return static_cast<int32_t>(offsetof(FrameState_t3340962930, ___newVuMarkDataArray_5)); }
	inline IntPtr_t get_newVuMarkDataArray_5() const { return ___newVuMarkDataArray_5; }
	inline IntPtr_t* get_address_of_newVuMarkDataArray_5() { return &___newVuMarkDataArray_5; }
	inline void set_newVuMarkDataArray_5(IntPtr_t value)
	{
		___newVuMarkDataArray_5 = value;
	}

	inline static int32_t get_offset_of_propTrackableDataArray_6() { return static_cast<int32_t>(offsetof(FrameState_t3340962930, ___propTrackableDataArray_6)); }
	inline IntPtr_t get_propTrackableDataArray_6() const { return ___propTrackableDataArray_6; }
	inline IntPtr_t* get_address_of_propTrackableDataArray_6() { return &___propTrackableDataArray_6; }
	inline void set_propTrackableDataArray_6(IntPtr_t value)
	{
		___propTrackableDataArray_6 = value;
	}

	inline static int32_t get_offset_of_smartTerrainRevisionsArray_7() { return static_cast<int32_t>(offsetof(FrameState_t3340962930, ___smartTerrainRevisionsArray_7)); }
	inline IntPtr_t get_smartTerrainRevisionsArray_7() const { return ___smartTerrainRevisionsArray_7; }
	inline IntPtr_t* get_address_of_smartTerrainRevisionsArray_7() { return &___smartTerrainRevisionsArray_7; }
	inline void set_smartTerrainRevisionsArray_7(IntPtr_t value)
	{
		___smartTerrainRevisionsArray_7 = value;
	}

	inline static int32_t get_offset_of_updatedSurfacesArray_8() { return static_cast<int32_t>(offsetof(FrameState_t3340962930, ___updatedSurfacesArray_8)); }
	inline IntPtr_t get_updatedSurfacesArray_8() const { return ___updatedSurfacesArray_8; }
	inline IntPtr_t* get_address_of_updatedSurfacesArray_8() { return &___updatedSurfacesArray_8; }
	inline void set_updatedSurfacesArray_8(IntPtr_t value)
	{
		___updatedSurfacesArray_8 = value;
	}

	inline static int32_t get_offset_of_updatedPropsArray_9() { return static_cast<int32_t>(offsetof(FrameState_t3340962930, ___updatedPropsArray_9)); }
	inline IntPtr_t get_updatedPropsArray_9() const { return ___updatedPropsArray_9; }
	inline IntPtr_t* get_address_of_updatedPropsArray_9() { return &___updatedPropsArray_9; }
	inline void set_updatedPropsArray_9(IntPtr_t value)
	{
		___updatedPropsArray_9 = value;
	}

	inline static int32_t get_offset_of_numTrackableResults_10() { return static_cast<int32_t>(offsetof(FrameState_t3340962930, ___numTrackableResults_10)); }
	inline int32_t get_numTrackableResults_10() const { return ___numTrackableResults_10; }
	inline int32_t* get_address_of_numTrackableResults_10() { return &___numTrackableResults_10; }
	inline void set_numTrackableResults_10(int32_t value)
	{
		___numTrackableResults_10 = value;
	}

	inline static int32_t get_offset_of_numVirtualButtonResults_11() { return static_cast<int32_t>(offsetof(FrameState_t3340962930, ___numVirtualButtonResults_11)); }
	inline int32_t get_numVirtualButtonResults_11() const { return ___numVirtualButtonResults_11; }
	inline int32_t* get_address_of_numVirtualButtonResults_11() { return &___numVirtualButtonResults_11; }
	inline void set_numVirtualButtonResults_11(int32_t value)
	{
		___numVirtualButtonResults_11 = value;
	}

	inline static int32_t get_offset_of_frameIndex_12() { return static_cast<int32_t>(offsetof(FrameState_t3340962930, ___frameIndex_12)); }
	inline int32_t get_frameIndex_12() const { return ___frameIndex_12; }
	inline int32_t* get_address_of_frameIndex_12() { return &___frameIndex_12; }
	inline void set_frameIndex_12(int32_t value)
	{
		___frameIndex_12 = value;
	}

	inline static int32_t get_offset_of_numWordResults_13() { return static_cast<int32_t>(offsetof(FrameState_t3340962930, ___numWordResults_13)); }
	inline int32_t get_numWordResults_13() const { return ___numWordResults_13; }
	inline int32_t* get_address_of_numWordResults_13() { return &___numWordResults_13; }
	inline void set_numWordResults_13(int32_t value)
	{
		___numWordResults_13 = value;
	}

	inline static int32_t get_offset_of_numNewWords_14() { return static_cast<int32_t>(offsetof(FrameState_t3340962930, ___numNewWords_14)); }
	inline int32_t get_numNewWords_14() const { return ___numNewWords_14; }
	inline int32_t* get_address_of_numNewWords_14() { return &___numNewWords_14; }
	inline void set_numNewWords_14(int32_t value)
	{
		___numNewWords_14 = value;
	}

	inline static int32_t get_offset_of_numVuMarkResults_15() { return static_cast<int32_t>(offsetof(FrameState_t3340962930, ___numVuMarkResults_15)); }
	inline int32_t get_numVuMarkResults_15() const { return ___numVuMarkResults_15; }
	inline int32_t* get_address_of_numVuMarkResults_15() { return &___numVuMarkResults_15; }
	inline void set_numVuMarkResults_15(int32_t value)
	{
		___numVuMarkResults_15 = value;
	}

	inline static int32_t get_offset_of_numNewVuMarks_16() { return static_cast<int32_t>(offsetof(FrameState_t3340962930, ___numNewVuMarks_16)); }
	inline int32_t get_numNewVuMarks_16() const { return ___numNewVuMarks_16; }
	inline int32_t* get_address_of_numNewVuMarks_16() { return &___numNewVuMarks_16; }
	inline void set_numNewVuMarks_16(int32_t value)
	{
		___numNewVuMarks_16 = value;
	}

	inline static int32_t get_offset_of_numPropTrackableResults_17() { return static_cast<int32_t>(offsetof(FrameState_t3340962930, ___numPropTrackableResults_17)); }
	inline int32_t get_numPropTrackableResults_17() const { return ___numPropTrackableResults_17; }
	inline int32_t* get_address_of_numPropTrackableResults_17() { return &___numPropTrackableResults_17; }
	inline void set_numPropTrackableResults_17(int32_t value)
	{
		___numPropTrackableResults_17 = value;
	}

	inline static int32_t get_offset_of_numSmartTerrainRevisions_18() { return static_cast<int32_t>(offsetof(FrameState_t3340962930, ___numSmartTerrainRevisions_18)); }
	inline int32_t get_numSmartTerrainRevisions_18() const { return ___numSmartTerrainRevisions_18; }
	inline int32_t* get_address_of_numSmartTerrainRevisions_18() { return &___numSmartTerrainRevisions_18; }
	inline void set_numSmartTerrainRevisions_18(int32_t value)
	{
		___numSmartTerrainRevisions_18 = value;
	}

	inline static int32_t get_offset_of_numUpdatedSurfaces_19() { return static_cast<int32_t>(offsetof(FrameState_t3340962930, ___numUpdatedSurfaces_19)); }
	inline int32_t get_numUpdatedSurfaces_19() const { return ___numUpdatedSurfaces_19; }
	inline int32_t* get_address_of_numUpdatedSurfaces_19() { return &___numUpdatedSurfaces_19; }
	inline void set_numUpdatedSurfaces_19(int32_t value)
	{
		___numUpdatedSurfaces_19 = value;
	}

	inline static int32_t get_offset_of_numUpdatedProps_20() { return static_cast<int32_t>(offsetof(FrameState_t3340962930, ___numUpdatedProps_20)); }
	inline int32_t get_numUpdatedProps_20() const { return ___numUpdatedProps_20; }
	inline int32_t* get_address_of_numUpdatedProps_20() { return &___numUpdatedProps_20; }
	inline void set_numUpdatedProps_20(int32_t value)
	{
		___numUpdatedProps_20 = value;
	}

	inline static int32_t get_offset_of_deviceTrackableId_21() { return static_cast<int32_t>(offsetof(FrameState_t3340962930, ___deviceTrackableId_21)); }
	inline int32_t get_deviceTrackableId_21() const { return ___deviceTrackableId_21; }
	inline int32_t* get_address_of_deviceTrackableId_21() { return &___deviceTrackableId_21; }
	inline void set_deviceTrackableId_21(int32_t value)
	{
		___deviceTrackableId_21 = value;
	}
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FRAMESTATE_T3340962930_H
#ifndef MESHDATA_T939646676_H
#define MESHDATA_T939646676_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.VuforiaManagerImpl/MeshData
#pragma pack(push, tp, 1)
struct  MeshData_t939646676 
{
public:
	// System.IntPtr Vuforia.VuforiaManagerImpl/MeshData::positionsArray
	IntPtr_t ___positionsArray_0;
	// System.IntPtr Vuforia.VuforiaManagerImpl/MeshData::normalsArray
	IntPtr_t ___normalsArray_1;
	// System.IntPtr Vuforia.VuforiaManagerImpl/MeshData::texCoordsArray
	IntPtr_t ___texCoordsArray_2;
	// System.IntPtr Vuforia.VuforiaManagerImpl/MeshData::triangleIdxArray
	IntPtr_t ___triangleIdxArray_3;
	// System.Int32 Vuforia.VuforiaManagerImpl/MeshData::numVertexValues
	int32_t ___numVertexValues_4;
	// System.Int32 Vuforia.VuforiaManagerImpl/MeshData::hasNormals
	int32_t ___hasNormals_5;
	// System.Int32 Vuforia.VuforiaManagerImpl/MeshData::hasTexCoords
	int32_t ___hasTexCoords_6;
	// System.Int32 Vuforia.VuforiaManagerImpl/MeshData::numTriangleIndices
	int32_t ___numTriangleIndices_7;

public:
	inline static int32_t get_offset_of_positionsArray_0() { return static_cast<int32_t>(offsetof(MeshData_t939646676, ___positionsArray_0)); }
	inline IntPtr_t get_positionsArray_0() const { return ___positionsArray_0; }
	inline IntPtr_t* get_address_of_positionsArray_0() { return &___positionsArray_0; }
	inline void set_positionsArray_0(IntPtr_t value)
	{
		___positionsArray_0 = value;
	}

	inline static int32_t get_offset_of_normalsArray_1() { return static_cast<int32_t>(offsetof(MeshData_t939646676, ___normalsArray_1)); }
	inline IntPtr_t get_normalsArray_1() const { return ___normalsArray_1; }
	inline IntPtr_t* get_address_of_normalsArray_1() { return &___normalsArray_1; }
	inline void set_normalsArray_1(IntPtr_t value)
	{
		___normalsArray_1 = value;
	}

	inline static int32_t get_offset_of_texCoordsArray_2() { return static_cast<int32_t>(offsetof(MeshData_t939646676, ___texCoordsArray_2)); }
	inline IntPtr_t get_texCoordsArray_2() const { return ___texCoordsArray_2; }
	inline IntPtr_t* get_address_of_texCoordsArray_2() { return &___texCoordsArray_2; }
	inline void set_texCoordsArray_2(IntPtr_t value)
	{
		___texCoordsArray_2 = value;
	}

	inline static int32_t get_offset_of_triangleIdxArray_3() { return static_cast<int32_t>(offsetof(MeshData_t939646676, ___triangleIdxArray_3)); }
	inline IntPtr_t get_triangleIdxArray_3() const { return ___triangleIdxArray_3; }
	inline IntPtr_t* get_address_of_triangleIdxArray_3() { return &___triangleIdxArray_3; }
	inline void set_triangleIdxArray_3(IntPtr_t value)
	{
		___triangleIdxArray_3 = value;
	}

	inline static int32_t get_offset_of_numVertexValues_4() { return static_cast<int32_t>(offsetof(MeshData_t939646676, ___numVertexValues_4)); }
	inline int32_t get_numVertexValues_4() const { return ___numVertexValues_4; }
	inline int32_t* get_address_of_numVertexValues_4() { return &___numVertexValues_4; }
	inline void set_numVertexValues_4(int32_t value)
	{
		___numVertexValues_4 = value;
	}

	inline static int32_t get_offset_of_hasNormals_5() { return static_cast<int32_t>(offsetof(MeshData_t939646676, ___hasNormals_5)); }
	inline int32_t get_hasNormals_5() const { return ___hasNormals_5; }
	inline int32_t* get_address_of_hasNormals_5() { return &___hasNormals_5; }
	inline void set_hasNormals_5(int32_t value)
	{
		___hasNormals_5 = value;
	}

	inline static int32_t get_offset_of_hasTexCoords_6() { return static_cast<int32_t>(offsetof(MeshData_t939646676, ___hasTexCoords_6)); }
	inline int32_t get_hasTexCoords_6() const { return ___hasTexCoords_6; }
	inline int32_t* get_address_of_hasTexCoords_6() { return &___hasTexCoords_6; }
	inline void set_hasTexCoords_6(int32_t value)
	{
		___hasTexCoords_6 = value;
	}

	inline static int32_t get_offset_of_numTriangleIndices_7() { return static_cast<int32_t>(offsetof(MeshData_t939646676, ___numTriangleIndices_7)); }
	inline int32_t get_numTriangleIndices_7() const { return ___numTriangleIndices_7; }
	inline int32_t* get_address_of_numTriangleIndices_7() { return &___numTriangleIndices_7; }
	inline void set_numTriangleIndices_7(int32_t value)
	{
		___numTriangleIndices_7 = value;
	}
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MESHDATA_T939646676_H
#ifndef DATETIMEKIND_T1076887453_H
#define DATETIMEKIND_T1076887453_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.DateTimeKind
struct  DateTimeKind_t1076887453 
{
public:
	// System.Int32 System.DateTimeKind::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(DateTimeKind_t1076887453, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATETIMEKIND_T1076887453_H
#ifndef OBJECT_T1502412432_H
#define OBJECT_T1502412432_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t1502412432  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	IntPtr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t1502412432, ___m_CachedPtr_0)); }
	inline IntPtr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline IntPtr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(IntPtr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t1502412432_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t1502412432_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t1502412432_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t1502412432_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T1502412432_H
#ifndef STATUS_T1358118869_H
#define STATUS_T1358118869_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.TrackableBehaviour/Status
struct  Status_t1358118869 
{
public:
	// System.Int32 Vuforia.TrackableBehaviour/Status::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Status_t1358118869, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STATUS_T1358118869_H
#ifndef INSTANCEIDTYPE_T3450543871_H
#define INSTANCEIDTYPE_T3450543871_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.InstanceIdType
struct  InstanceIdType_t3450543871 
{
public:
	// System.Int32 Vuforia.InstanceIdType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(InstanceIdType_t3450543871, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INSTANCEIDTYPE_T3450543871_H
#ifndef VIDEOBGCFGDATA_T3234105495_H
#define VIDEOBGCFGDATA_T3234105495_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.VuforiaRenderer/VideoBGCfgData
#pragma pack(push, tp, 1)
struct  VideoBGCfgData_t3234105495 
{
public:
	// Vuforia.VuforiaRenderer/Vec2I Vuforia.VuforiaRenderer/VideoBGCfgData::position
	Vec2I_t895597728  ___position_0;
	// Vuforia.VuforiaRenderer/Vec2I Vuforia.VuforiaRenderer/VideoBGCfgData::size
	Vec2I_t895597728  ___size_1;
	// System.Int32 Vuforia.VuforiaRenderer/VideoBGCfgData::enabled
	int32_t ___enabled_2;
	// System.Int32 Vuforia.VuforiaRenderer/VideoBGCfgData::reflectionInteger
	int32_t ___reflectionInteger_3;

public:
	inline static int32_t get_offset_of_position_0() { return static_cast<int32_t>(offsetof(VideoBGCfgData_t3234105495, ___position_0)); }
	inline Vec2I_t895597728  get_position_0() const { return ___position_0; }
	inline Vec2I_t895597728 * get_address_of_position_0() { return &___position_0; }
	inline void set_position_0(Vec2I_t895597728  value)
	{
		___position_0 = value;
	}

	inline static int32_t get_offset_of_size_1() { return static_cast<int32_t>(offsetof(VideoBGCfgData_t3234105495, ___size_1)); }
	inline Vec2I_t895597728  get_size_1() const { return ___size_1; }
	inline Vec2I_t895597728 * get_address_of_size_1() { return &___size_1; }
	inline void set_size_1(Vec2I_t895597728  value)
	{
		___size_1 = value;
	}

	inline static int32_t get_offset_of_enabled_2() { return static_cast<int32_t>(offsetof(VideoBGCfgData_t3234105495, ___enabled_2)); }
	inline int32_t get_enabled_2() const { return ___enabled_2; }
	inline int32_t* get_address_of_enabled_2() { return &___enabled_2; }
	inline void set_enabled_2(int32_t value)
	{
		___enabled_2 = value;
	}

	inline static int32_t get_offset_of_reflectionInteger_3() { return static_cast<int32_t>(offsetof(VideoBGCfgData_t3234105495, ___reflectionInteger_3)); }
	inline int32_t get_reflectionInteger_3() const { return ___reflectionInteger_3; }
	inline int32_t* get_address_of_reflectionInteger_3() { return &___reflectionInteger_3; }
	inline void set_reflectionInteger_3(int32_t value)
	{
		___reflectionInteger_3 = value;
	}
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VIDEOBGCFGDATA_T3234105495_H
#ifndef STORAGETYPE_T2925054281_H
#define STORAGETYPE_T2925054281_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.DataSet/StorageType
struct  StorageType_t2925054281 
{
public:
	// System.Int32 Vuforia.DataSet/StorageType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(StorageType_t2925054281, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STORAGETYPE_T2925054281_H
#ifndef ORIENTEDBOUNDINGBOX_T4288440438_H
#define ORIENTEDBOUNDINGBOX_T4288440438_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.OrientedBoundingBox
struct  OrientedBoundingBox_t4288440438 
{
public:
	// UnityEngine.Vector2 Vuforia.OrientedBoundingBox::<Center>k__BackingField
	Vector2_t3577333262  ___U3CCenterU3Ek__BackingField_0;
	// UnityEngine.Vector2 Vuforia.OrientedBoundingBox::<HalfExtents>k__BackingField
	Vector2_t3577333262  ___U3CHalfExtentsU3Ek__BackingField_1;
	// System.Single Vuforia.OrientedBoundingBox::<Rotation>k__BackingField
	float ___U3CRotationU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CCenterU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(OrientedBoundingBox_t4288440438, ___U3CCenterU3Ek__BackingField_0)); }
	inline Vector2_t3577333262  get_U3CCenterU3Ek__BackingField_0() const { return ___U3CCenterU3Ek__BackingField_0; }
	inline Vector2_t3577333262 * get_address_of_U3CCenterU3Ek__BackingField_0() { return &___U3CCenterU3Ek__BackingField_0; }
	inline void set_U3CCenterU3Ek__BackingField_0(Vector2_t3577333262  value)
	{
		___U3CCenterU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CHalfExtentsU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(OrientedBoundingBox_t4288440438, ___U3CHalfExtentsU3Ek__BackingField_1)); }
	inline Vector2_t3577333262  get_U3CHalfExtentsU3Ek__BackingField_1() const { return ___U3CHalfExtentsU3Ek__BackingField_1; }
	inline Vector2_t3577333262 * get_address_of_U3CHalfExtentsU3Ek__BackingField_1() { return &___U3CHalfExtentsU3Ek__BackingField_1; }
	inline void set_U3CHalfExtentsU3Ek__BackingField_1(Vector2_t3577333262  value)
	{
		___U3CHalfExtentsU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CRotationU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(OrientedBoundingBox_t4288440438, ___U3CRotationU3Ek__BackingField_2)); }
	inline float get_U3CRotationU3Ek__BackingField_2() const { return ___U3CRotationU3Ek__BackingField_2; }
	inline float* get_address_of_U3CRotationU3Ek__BackingField_2() { return &___U3CRotationU3Ek__BackingField_2; }
	inline void set_U3CRotationU3Ek__BackingField_2(float value)
	{
		___U3CRotationU3Ek__BackingField_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ORIENTEDBOUNDINGBOX_T4288440438_H
#ifndef ORIENTEDBOUNDINGBOX3D_T19466890_H
#define ORIENTEDBOUNDINGBOX3D_T19466890_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.OrientedBoundingBox3D
struct  OrientedBoundingBox3D_t19466890 
{
public:
	// UnityEngine.Vector3 Vuforia.OrientedBoundingBox3D::<Center>k__BackingField
	Vector3_t2903530434  ___U3CCenterU3Ek__BackingField_0;
	// UnityEngine.Vector3 Vuforia.OrientedBoundingBox3D::<HalfExtents>k__BackingField
	Vector3_t2903530434  ___U3CHalfExtentsU3Ek__BackingField_1;
	// System.Single Vuforia.OrientedBoundingBox3D::<RotationY>k__BackingField
	float ___U3CRotationYU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CCenterU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(OrientedBoundingBox3D_t19466890, ___U3CCenterU3Ek__BackingField_0)); }
	inline Vector3_t2903530434  get_U3CCenterU3Ek__BackingField_0() const { return ___U3CCenterU3Ek__BackingField_0; }
	inline Vector3_t2903530434 * get_address_of_U3CCenterU3Ek__BackingField_0() { return &___U3CCenterU3Ek__BackingField_0; }
	inline void set_U3CCenterU3Ek__BackingField_0(Vector3_t2903530434  value)
	{
		___U3CCenterU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CHalfExtentsU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(OrientedBoundingBox3D_t19466890, ___U3CHalfExtentsU3Ek__BackingField_1)); }
	inline Vector3_t2903530434  get_U3CHalfExtentsU3Ek__BackingField_1() const { return ___U3CHalfExtentsU3Ek__BackingField_1; }
	inline Vector3_t2903530434 * get_address_of_U3CHalfExtentsU3Ek__BackingField_1() { return &___U3CHalfExtentsU3Ek__BackingField_1; }
	inline void set_U3CHalfExtentsU3Ek__BackingField_1(Vector3_t2903530434  value)
	{
		___U3CHalfExtentsU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CRotationYU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(OrientedBoundingBox3D_t19466890, ___U3CRotationYU3Ek__BackingField_2)); }
	inline float get_U3CRotationYU3Ek__BackingField_2() const { return ___U3CRotationYU3Ek__BackingField_2; }
	inline float* get_address_of_U3CRotationYU3Ek__BackingField_2() { return &___U3CRotationYU3Ek__BackingField_2; }
	inline void set_U3CRotationYU3Ek__BackingField_2(float value)
	{
		___U3CRotationYU3Ek__BackingField_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ORIENTEDBOUNDINGBOX3D_T19466890_H
#ifndef CLOUDRECOIMAGETARGETIMPL_T3516871077_H
#define CLOUDRECOIMAGETARGETIMPL_T3516871077_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.CloudRecoImageTargetImpl
struct  CloudRecoImageTargetImpl_t3516871077  : public TrackableImpl_t3450720819
{
public:
	// UnityEngine.Vector3 Vuforia.CloudRecoImageTargetImpl::mSize
	Vector3_t2903530434  ___mSize_2;

public:
	inline static int32_t get_offset_of_mSize_2() { return static_cast<int32_t>(offsetof(CloudRecoImageTargetImpl_t3516871077, ___mSize_2)); }
	inline Vector3_t2903530434  get_mSize_2() const { return ___mSize_2; }
	inline Vector3_t2903530434 * get_address_of_mSize_2() { return &___mSize_2; }
	inline void set_mSize_2(Vector3_t2903530434  value)
	{
		___mSize_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CLOUDRECOIMAGETARGETIMPL_T3516871077_H
#ifndef IMAGETARGETTYPE_T2769160834_H
#define IMAGETARGETTYPE_T2769160834_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.ImageTargetType
struct  ImageTargetType_t2769160834 
{
public:
	// System.Int32 Vuforia.ImageTargetType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ImageTargetType_t2769160834, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IMAGETARGETTYPE_T2769160834_H
#ifndef INSTANCEIDDATA_T2286842551_H
#define INSTANCEIDDATA_T2286842551_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.VuforiaManagerImpl/InstanceIdData
#pragma pack(push, tp, 1)
struct  InstanceIdData_t2286842551 
{
public:
	// System.UInt64 Vuforia.VuforiaManagerImpl/InstanceIdData::numericValue
	uint64_t ___numericValue_0;
	// System.IntPtr Vuforia.VuforiaManagerImpl/InstanceIdData::buffer
	IntPtr_t ___buffer_1;
	// System.IntPtr Vuforia.VuforiaManagerImpl/InstanceIdData::reserved
	IntPtr_t ___reserved_2;
	// System.UInt32 Vuforia.VuforiaManagerImpl/InstanceIdData::dataLength
	uint32_t ___dataLength_3;
	// System.Int32 Vuforia.VuforiaManagerImpl/InstanceIdData::dataType
	int32_t ___dataType_4;

public:
	inline static int32_t get_offset_of_numericValue_0() { return static_cast<int32_t>(offsetof(InstanceIdData_t2286842551, ___numericValue_0)); }
	inline uint64_t get_numericValue_0() const { return ___numericValue_0; }
	inline uint64_t* get_address_of_numericValue_0() { return &___numericValue_0; }
	inline void set_numericValue_0(uint64_t value)
	{
		___numericValue_0 = value;
	}

	inline static int32_t get_offset_of_buffer_1() { return static_cast<int32_t>(offsetof(InstanceIdData_t2286842551, ___buffer_1)); }
	inline IntPtr_t get_buffer_1() const { return ___buffer_1; }
	inline IntPtr_t* get_address_of_buffer_1() { return &___buffer_1; }
	inline void set_buffer_1(IntPtr_t value)
	{
		___buffer_1 = value;
	}

	inline static int32_t get_offset_of_reserved_2() { return static_cast<int32_t>(offsetof(InstanceIdData_t2286842551, ___reserved_2)); }
	inline IntPtr_t get_reserved_2() const { return ___reserved_2; }
	inline IntPtr_t* get_address_of_reserved_2() { return &___reserved_2; }
	inline void set_reserved_2(IntPtr_t value)
	{
		___reserved_2 = value;
	}

	inline static int32_t get_offset_of_dataLength_3() { return static_cast<int32_t>(offsetof(InstanceIdData_t2286842551, ___dataLength_3)); }
	inline uint32_t get_dataLength_3() const { return ___dataLength_3; }
	inline uint32_t* get_address_of_dataLength_3() { return &___dataLength_3; }
	inline void set_dataLength_3(uint32_t value)
	{
		___dataLength_3 = value;
	}

	inline static int32_t get_offset_of_dataType_4() { return static_cast<int32_t>(offsetof(InstanceIdData_t2286842551, ___dataType_4)); }
	inline int32_t get_dataType_4() const { return ___dataType_4; }
	inline int32_t* get_address_of_dataType_4() { return &___dataType_4; }
	inline void set_dataType_4(int32_t value)
	{
		___dataType_4 = value;
	}
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INSTANCEIDDATA_T2286842551_H
#ifndef IMAGETARGETDATA_T1209528890_H
#define IMAGETARGETDATA_T1209528890_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.ImageTargetData
#pragma pack(push, tp, 1)
struct  ImageTargetData_t1209528890 
{
public:
	// System.Int32 Vuforia.ImageTargetData::id
	int32_t ___id_0;
	// UnityEngine.Vector3 Vuforia.ImageTargetData::size
	Vector3_t2903530434  ___size_1;

public:
	inline static int32_t get_offset_of_id_0() { return static_cast<int32_t>(offsetof(ImageTargetData_t1209528890, ___id_0)); }
	inline int32_t get_id_0() const { return ___id_0; }
	inline int32_t* get_address_of_id_0() { return &___id_0; }
	inline void set_id_0(int32_t value)
	{
		___id_0 = value;
	}

	inline static int32_t get_offset_of_size_1() { return static_cast<int32_t>(offsetof(ImageTargetData_t1209528890, ___size_1)); }
	inline Vector3_t2903530434  get_size_1() const { return ___size_1; }
	inline Vector3_t2903530434 * get_address_of_size_1() { return &___size_1; }
	inline void set_size_1(Vector3_t2903530434  value)
	{
		___size_1 = value;
	}
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IMAGETARGETDATA_T1209528890_H
#ifndef PIXEL_FORMAT_T1585627136_H
#define PIXEL_FORMAT_T1585627136_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.Image/PIXEL_FORMAT
struct  PIXEL_FORMAT_t1585627136 
{
public:
	// System.Int32 Vuforia.Image/PIXEL_FORMAT::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(PIXEL_FORMAT_t1585627136, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PIXEL_FORMAT_T1585627136_H
#ifndef STORAGETYPE_T1466127932_H
#define STORAGETYPE_T1466127932_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.VuforiaUnity/StorageType
struct  StorageType_t1466127932 
{
public:
	// System.Int32 Vuforia.VuforiaUnity/StorageType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(StorageType_t1466127932, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STORAGETYPE_T1466127932_H
#ifndef OBJECTTRACKERIMPL_T89795342_H
#define OBJECTTRACKERIMPL_T89795342_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.ObjectTrackerImpl
struct  ObjectTrackerImpl_t89795342  : public ObjectTracker_t4023638979
{
public:
	// System.Collections.Generic.List`1<Vuforia.DataSetImpl> Vuforia.ObjectTrackerImpl::mActiveDataSets
	List_1_t3373880750 * ___mActiveDataSets_1;
	// System.Collections.Generic.List`1<Vuforia.DataSet> Vuforia.ObjectTrackerImpl::mDataSets
	List_1_t298280400 * ___mDataSets_2;
	// Vuforia.ImageTargetBuilder Vuforia.ObjectTrackerImpl::mImageTargetBuilder
	ImageTargetBuilder_t2469815077 * ___mImageTargetBuilder_3;
	// Vuforia.TargetFinder Vuforia.ObjectTrackerImpl::mTargetFinder
	TargetFinder_t2860227752 * ___mTargetFinder_4;

public:
	inline static int32_t get_offset_of_mActiveDataSets_1() { return static_cast<int32_t>(offsetof(ObjectTrackerImpl_t89795342, ___mActiveDataSets_1)); }
	inline List_1_t3373880750 * get_mActiveDataSets_1() const { return ___mActiveDataSets_1; }
	inline List_1_t3373880750 ** get_address_of_mActiveDataSets_1() { return &___mActiveDataSets_1; }
	inline void set_mActiveDataSets_1(List_1_t3373880750 * value)
	{
		___mActiveDataSets_1 = value;
		Il2CppCodeGenWriteBarrier((&___mActiveDataSets_1), value);
	}

	inline static int32_t get_offset_of_mDataSets_2() { return static_cast<int32_t>(offsetof(ObjectTrackerImpl_t89795342, ___mDataSets_2)); }
	inline List_1_t298280400 * get_mDataSets_2() const { return ___mDataSets_2; }
	inline List_1_t298280400 ** get_address_of_mDataSets_2() { return &___mDataSets_2; }
	inline void set_mDataSets_2(List_1_t298280400 * value)
	{
		___mDataSets_2 = value;
		Il2CppCodeGenWriteBarrier((&___mDataSets_2), value);
	}

	inline static int32_t get_offset_of_mImageTargetBuilder_3() { return static_cast<int32_t>(offsetof(ObjectTrackerImpl_t89795342, ___mImageTargetBuilder_3)); }
	inline ImageTargetBuilder_t2469815077 * get_mImageTargetBuilder_3() const { return ___mImageTargetBuilder_3; }
	inline ImageTargetBuilder_t2469815077 ** get_address_of_mImageTargetBuilder_3() { return &___mImageTargetBuilder_3; }
	inline void set_mImageTargetBuilder_3(ImageTargetBuilder_t2469815077 * value)
	{
		___mImageTargetBuilder_3 = value;
		Il2CppCodeGenWriteBarrier((&___mImageTargetBuilder_3), value);
	}

	inline static int32_t get_offset_of_mTargetFinder_4() { return static_cast<int32_t>(offsetof(ObjectTrackerImpl_t89795342, ___mTargetFinder_4)); }
	inline TargetFinder_t2860227752 * get_mTargetFinder_4() const { return ___mTargetFinder_4; }
	inline TargetFinder_t2860227752 ** get_address_of_mTargetFinder_4() { return &___mTargetFinder_4; }
	inline void set_mTargetFinder_4(TargetFinder_t2860227752 * value)
	{
		___mTargetFinder_4 = value;
		Il2CppCodeGenWriteBarrier((&___mTargetFinder_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OBJECTTRACKERIMPL_T89795342_H
#ifndef VIDEOBACKGROUNDREFLECTION_T728651611_H
#define VIDEOBACKGROUNDREFLECTION_T728651611_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.VuforiaRenderer/VideoBackgroundReflection
struct  VideoBackgroundReflection_t728651611 
{
public:
	// System.Int32 Vuforia.VuforiaRenderer/VideoBackgroundReflection::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(VideoBackgroundReflection_t728651611, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VIDEOBACKGROUNDREFLECTION_T728651611_H
#ifndef POSEDATA_T1816720573_H
#define POSEDATA_T1816720573_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.VuforiaManagerImpl/PoseData
#pragma pack(push, tp, 1)
struct  PoseData_t1816720573 
{
public:
	// UnityEngine.Vector3 Vuforia.VuforiaManagerImpl/PoseData::position
	Vector3_t2903530434  ___position_0;
	// UnityEngine.Quaternion Vuforia.VuforiaManagerImpl/PoseData::orientation
	Quaternion_t754065749  ___orientation_1;
	// System.Int32 Vuforia.VuforiaManagerImpl/PoseData::csInteger
	int32_t ___csInteger_2;

public:
	inline static int32_t get_offset_of_position_0() { return static_cast<int32_t>(offsetof(PoseData_t1816720573, ___position_0)); }
	inline Vector3_t2903530434  get_position_0() const { return ___position_0; }
	inline Vector3_t2903530434 * get_address_of_position_0() { return &___position_0; }
	inline void set_position_0(Vector3_t2903530434  value)
	{
		___position_0 = value;
	}

	inline static int32_t get_offset_of_orientation_1() { return static_cast<int32_t>(offsetof(PoseData_t1816720573, ___orientation_1)); }
	inline Quaternion_t754065749  get_orientation_1() const { return ___orientation_1; }
	inline Quaternion_t754065749 * get_address_of_orientation_1() { return &___orientation_1; }
	inline void set_orientation_1(Quaternion_t754065749  value)
	{
		___orientation_1 = value;
	}

	inline static int32_t get_offset_of_csInteger_2() { return static_cast<int32_t>(offsetof(PoseData_t1816720573, ___csInteger_2)); }
	inline int32_t get_csInteger_2() const { return ___csInteger_2; }
	inline int32_t* get_address_of_csInteger_2() { return &___csInteger_2; }
	inline void set_csInteger_2(int32_t value)
	{
		___csInteger_2 = value;
	}
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSEDATA_T1816720573_H
#ifndef OBJECTTARGETIMPL_T1592445137_H
#define OBJECTTARGETIMPL_T1592445137_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.ObjectTargetImpl
struct  ObjectTargetImpl_t1592445137  : public TrackableImpl_t3450720819
{
public:
	// UnityEngine.Vector3 Vuforia.ObjectTargetImpl::mSize
	Vector3_t2903530434  ___mSize_2;
	// Vuforia.DataSetImpl Vuforia.ObjectTargetImpl::mDataSet
	DataSetImpl_t223764247 * ___mDataSet_3;

public:
	inline static int32_t get_offset_of_mSize_2() { return static_cast<int32_t>(offsetof(ObjectTargetImpl_t1592445137, ___mSize_2)); }
	inline Vector3_t2903530434  get_mSize_2() const { return ___mSize_2; }
	inline Vector3_t2903530434 * get_address_of_mSize_2() { return &___mSize_2; }
	inline void set_mSize_2(Vector3_t2903530434  value)
	{
		___mSize_2 = value;
	}

	inline static int32_t get_offset_of_mDataSet_3() { return static_cast<int32_t>(offsetof(ObjectTargetImpl_t1592445137, ___mDataSet_3)); }
	inline DataSetImpl_t223764247 * get_mDataSet_3() const { return ___mDataSet_3; }
	inline DataSetImpl_t223764247 ** get_address_of_mDataSet_3() { return &___mDataSet_3; }
	inline void set_mDataSet_3(DataSetImpl_t223764247 * value)
	{
		___mDataSet_3 = value;
		Il2CppCodeGenWriteBarrier((&___mDataSet_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OBJECTTARGETIMPL_T1592445137_H
#ifndef OBB2D_T1682428419_H
#define OBB2D_T1682428419_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.VuforiaManagerImpl/Obb2D
#pragma pack(push, tp, 1)
struct  Obb2D_t1682428419 
{
public:
	// UnityEngine.Vector2 Vuforia.VuforiaManagerImpl/Obb2D::center
	Vector2_t3577333262  ___center_0;
	// UnityEngine.Vector2 Vuforia.VuforiaManagerImpl/Obb2D::halfExtents
	Vector2_t3577333262  ___halfExtents_1;
	// System.Single Vuforia.VuforiaManagerImpl/Obb2D::rotation
	float ___rotation_2;
	// System.Int32 Vuforia.VuforiaManagerImpl/Obb2D::unused
	int32_t ___unused_3;

public:
	inline static int32_t get_offset_of_center_0() { return static_cast<int32_t>(offsetof(Obb2D_t1682428419, ___center_0)); }
	inline Vector2_t3577333262  get_center_0() const { return ___center_0; }
	inline Vector2_t3577333262 * get_address_of_center_0() { return &___center_0; }
	inline void set_center_0(Vector2_t3577333262  value)
	{
		___center_0 = value;
	}

	inline static int32_t get_offset_of_halfExtents_1() { return static_cast<int32_t>(offsetof(Obb2D_t1682428419, ___halfExtents_1)); }
	inline Vector2_t3577333262  get_halfExtents_1() const { return ___halfExtents_1; }
	inline Vector2_t3577333262 * get_address_of_halfExtents_1() { return &___halfExtents_1; }
	inline void set_halfExtents_1(Vector2_t3577333262  value)
	{
		___halfExtents_1 = value;
	}

	inline static int32_t get_offset_of_rotation_2() { return static_cast<int32_t>(offsetof(Obb2D_t1682428419, ___rotation_2)); }
	inline float get_rotation_2() const { return ___rotation_2; }
	inline float* get_address_of_rotation_2() { return &___rotation_2; }
	inline void set_rotation_2(float value)
	{
		___rotation_2 = value;
	}

	inline static int32_t get_offset_of_unused_3() { return static_cast<int32_t>(offsetof(Obb2D_t1682428419, ___unused_3)); }
	inline int32_t get_unused_3() const { return ___unused_3; }
	inline int32_t* get_address_of_unused_3() { return &___unused_3; }
	inline void set_unused_3(int32_t value)
	{
		___unused_3 = value;
	}
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OBB2D_T1682428419_H
#ifndef OBB3D_T1746725078_H
#define OBB3D_T1746725078_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.VuforiaManagerImpl/Obb3D
#pragma pack(push, tp, 1)
struct  Obb3D_t1746725078 
{
public:
	// UnityEngine.Vector3 Vuforia.VuforiaManagerImpl/Obb3D::center
	Vector3_t2903530434  ___center_0;
	// UnityEngine.Vector3 Vuforia.VuforiaManagerImpl/Obb3D::halfExtents
	Vector3_t2903530434  ___halfExtents_1;
	// System.Single Vuforia.VuforiaManagerImpl/Obb3D::rotationZ
	float ___rotationZ_2;
	// System.Int32 Vuforia.VuforiaManagerImpl/Obb3D::unused
	int32_t ___unused_3;

public:
	inline static int32_t get_offset_of_center_0() { return static_cast<int32_t>(offsetof(Obb3D_t1746725078, ___center_0)); }
	inline Vector3_t2903530434  get_center_0() const { return ___center_0; }
	inline Vector3_t2903530434 * get_address_of_center_0() { return &___center_0; }
	inline void set_center_0(Vector3_t2903530434  value)
	{
		___center_0 = value;
	}

	inline static int32_t get_offset_of_halfExtents_1() { return static_cast<int32_t>(offsetof(Obb3D_t1746725078, ___halfExtents_1)); }
	inline Vector3_t2903530434  get_halfExtents_1() const { return ___halfExtents_1; }
	inline Vector3_t2903530434 * get_address_of_halfExtents_1() { return &___halfExtents_1; }
	inline void set_halfExtents_1(Vector3_t2903530434  value)
	{
		___halfExtents_1 = value;
	}

	inline static int32_t get_offset_of_rotationZ_2() { return static_cast<int32_t>(offsetof(Obb3D_t1746725078, ___rotationZ_2)); }
	inline float get_rotationZ_2() const { return ___rotationZ_2; }
	inline float* get_address_of_rotationZ_2() { return &___rotationZ_2; }
	inline void set_rotationZ_2(float value)
	{
		___rotationZ_2 = value;
	}

	inline static int32_t get_offset_of_unused_3() { return static_cast<int32_t>(offsetof(Obb3D_t1746725078, ___unused_3)); }
	inline int32_t get_unused_3() const { return ___unused_3; }
	inline int32_t* get_address_of_unused_3() { return &___unused_3; }
	inline void set_unused_3(int32_t value)
	{
		___unused_3 = value;
	}
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OBB3D_T1746725078_H
#ifndef WORDDATA_T2202059873_H
#define WORDDATA_T2202059873_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.VuforiaManagerImpl/WordData
#pragma pack(push, tp, 1)
struct  WordData_t2202059873 
{
public:
	// System.IntPtr Vuforia.VuforiaManagerImpl/WordData::stringValue
	IntPtr_t ___stringValue_0;
	// System.Int32 Vuforia.VuforiaManagerImpl/WordData::id
	int32_t ___id_1;
	// UnityEngine.Vector2 Vuforia.VuforiaManagerImpl/WordData::size
	Vector2_t3577333262  ___size_2;
	// System.Int32 Vuforia.VuforiaManagerImpl/WordData::unused
	int32_t ___unused_3;

public:
	inline static int32_t get_offset_of_stringValue_0() { return static_cast<int32_t>(offsetof(WordData_t2202059873, ___stringValue_0)); }
	inline IntPtr_t get_stringValue_0() const { return ___stringValue_0; }
	inline IntPtr_t* get_address_of_stringValue_0() { return &___stringValue_0; }
	inline void set_stringValue_0(IntPtr_t value)
	{
		___stringValue_0 = value;
	}

	inline static int32_t get_offset_of_id_1() { return static_cast<int32_t>(offsetof(WordData_t2202059873, ___id_1)); }
	inline int32_t get_id_1() const { return ___id_1; }
	inline int32_t* get_address_of_id_1() { return &___id_1; }
	inline void set_id_1(int32_t value)
	{
		___id_1 = value;
	}

	inline static int32_t get_offset_of_size_2() { return static_cast<int32_t>(offsetof(WordData_t2202059873, ___size_2)); }
	inline Vector2_t3577333262  get_size_2() const { return ___size_2; }
	inline Vector2_t3577333262 * get_address_of_size_2() { return &___size_2; }
	inline void set_size_2(Vector2_t3577333262  value)
	{
		___size_2 = value;
	}

	inline static int32_t get_offset_of_unused_3() { return static_cast<int32_t>(offsetof(WordData_t2202059873, ___unused_3)); }
	inline int32_t get_unused_3() const { return ___unused_3; }
	inline int32_t* get_address_of_unused_3() { return &___unused_3; }
	inline void set_unused_3(int32_t value)
	{
		___unused_3 = value;
	}
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WORDDATA_T2202059873_H
#ifndef IMAGEHEADERDATA_T1829409304_H
#define IMAGEHEADERDATA_T1829409304_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.VuforiaManagerImpl/ImageHeaderData
#pragma pack(push, tp, 1)
struct  ImageHeaderData_t1829409304 
{
public:
	// System.IntPtr Vuforia.VuforiaManagerImpl/ImageHeaderData::data
	IntPtr_t ___data_0;
	// System.Int32 Vuforia.VuforiaManagerImpl/ImageHeaderData::width
	int32_t ___width_1;
	// System.Int32 Vuforia.VuforiaManagerImpl/ImageHeaderData::height
	int32_t ___height_2;
	// System.Int32 Vuforia.VuforiaManagerImpl/ImageHeaderData::stride
	int32_t ___stride_3;
	// System.Int32 Vuforia.VuforiaManagerImpl/ImageHeaderData::bufferWidth
	int32_t ___bufferWidth_4;
	// System.Int32 Vuforia.VuforiaManagerImpl/ImageHeaderData::bufferHeight
	int32_t ___bufferHeight_5;
	// System.Int32 Vuforia.VuforiaManagerImpl/ImageHeaderData::format
	int32_t ___format_6;
	// System.Int32 Vuforia.VuforiaManagerImpl/ImageHeaderData::reallocate
	int32_t ___reallocate_7;
	// System.Int32 Vuforia.VuforiaManagerImpl/ImageHeaderData::updated
	int32_t ___updated_8;

public:
	inline static int32_t get_offset_of_data_0() { return static_cast<int32_t>(offsetof(ImageHeaderData_t1829409304, ___data_0)); }
	inline IntPtr_t get_data_0() const { return ___data_0; }
	inline IntPtr_t* get_address_of_data_0() { return &___data_0; }
	inline void set_data_0(IntPtr_t value)
	{
		___data_0 = value;
	}

	inline static int32_t get_offset_of_width_1() { return static_cast<int32_t>(offsetof(ImageHeaderData_t1829409304, ___width_1)); }
	inline int32_t get_width_1() const { return ___width_1; }
	inline int32_t* get_address_of_width_1() { return &___width_1; }
	inline void set_width_1(int32_t value)
	{
		___width_1 = value;
	}

	inline static int32_t get_offset_of_height_2() { return static_cast<int32_t>(offsetof(ImageHeaderData_t1829409304, ___height_2)); }
	inline int32_t get_height_2() const { return ___height_2; }
	inline int32_t* get_address_of_height_2() { return &___height_2; }
	inline void set_height_2(int32_t value)
	{
		___height_2 = value;
	}

	inline static int32_t get_offset_of_stride_3() { return static_cast<int32_t>(offsetof(ImageHeaderData_t1829409304, ___stride_3)); }
	inline int32_t get_stride_3() const { return ___stride_3; }
	inline int32_t* get_address_of_stride_3() { return &___stride_3; }
	inline void set_stride_3(int32_t value)
	{
		___stride_3 = value;
	}

	inline static int32_t get_offset_of_bufferWidth_4() { return static_cast<int32_t>(offsetof(ImageHeaderData_t1829409304, ___bufferWidth_4)); }
	inline int32_t get_bufferWidth_4() const { return ___bufferWidth_4; }
	inline int32_t* get_address_of_bufferWidth_4() { return &___bufferWidth_4; }
	inline void set_bufferWidth_4(int32_t value)
	{
		___bufferWidth_4 = value;
	}

	inline static int32_t get_offset_of_bufferHeight_5() { return static_cast<int32_t>(offsetof(ImageHeaderData_t1829409304, ___bufferHeight_5)); }
	inline int32_t get_bufferHeight_5() const { return ___bufferHeight_5; }
	inline int32_t* get_address_of_bufferHeight_5() { return &___bufferHeight_5; }
	inline void set_bufferHeight_5(int32_t value)
	{
		___bufferHeight_5 = value;
	}

	inline static int32_t get_offset_of_format_6() { return static_cast<int32_t>(offsetof(ImageHeaderData_t1829409304, ___format_6)); }
	inline int32_t get_format_6() const { return ___format_6; }
	inline int32_t* get_address_of_format_6() { return &___format_6; }
	inline void set_format_6(int32_t value)
	{
		___format_6 = value;
	}

	inline static int32_t get_offset_of_reallocate_7() { return static_cast<int32_t>(offsetof(ImageHeaderData_t1829409304, ___reallocate_7)); }
	inline int32_t get_reallocate_7() const { return ___reallocate_7; }
	inline int32_t* get_address_of_reallocate_7() { return &___reallocate_7; }
	inline void set_reallocate_7(int32_t value)
	{
		___reallocate_7 = value;
	}

	inline static int32_t get_offset_of_updated_8() { return static_cast<int32_t>(offsetof(ImageHeaderData_t1829409304, ___updated_8)); }
	inline int32_t get_updated_8() const { return ___updated_8; }
	inline int32_t* get_address_of_updated_8() { return &___updated_8; }
	inline void set_updated_8(int32_t value)
	{
		___updated_8 = value;
	}
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IMAGEHEADERDATA_T1829409304_H
#ifndef FRAMEQUALITY_T1148757_H
#define FRAMEQUALITY_T1148757_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.ImageTargetBuilder/FrameQuality
struct  FrameQuality_t1148757 
{
public:
	// System.Int32 Vuforia.ImageTargetBuilder/FrameQuality::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(FrameQuality_t1148757, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FRAMEQUALITY_T1148757_H
#ifndef DISTORTIONRENDERINGMODE_T4279873297_H
#define DISTORTIONRENDERINGMODE_T4279873297_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.DistortionRenderingMode
struct  DistortionRenderingMode_t4279873297 
{
public:
	// System.Int32 Vuforia.DistortionRenderingMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(DistortionRenderingMode_t4279873297, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DISTORTIONRENDERINGMODE_T4279873297_H
#ifndef DATETIME_T2376405012_H
#define DATETIME_T2376405012_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.DateTime
struct  DateTime_t2376405012 
{
public:
	// System.TimeSpan System.DateTime::ticks
	TimeSpan_t1805120695  ___ticks_0;
	// System.DateTimeKind System.DateTime::kind
	int32_t ___kind_1;

public:
	inline static int32_t get_offset_of_ticks_0() { return static_cast<int32_t>(offsetof(DateTime_t2376405012, ___ticks_0)); }
	inline TimeSpan_t1805120695  get_ticks_0() const { return ___ticks_0; }
	inline TimeSpan_t1805120695 * get_address_of_ticks_0() { return &___ticks_0; }
	inline void set_ticks_0(TimeSpan_t1805120695  value)
	{
		___ticks_0 = value;
	}

	inline static int32_t get_offset_of_kind_1() { return static_cast<int32_t>(offsetof(DateTime_t2376405012, ___kind_1)); }
	inline int32_t get_kind_1() const { return ___kind_1; }
	inline int32_t* get_address_of_kind_1() { return &___kind_1; }
	inline void set_kind_1(int32_t value)
	{
		___kind_1 = value;
	}
};

struct DateTime_t2376405012_StaticFields
{
public:
	// System.DateTime System.DateTime::MaxValue
	DateTime_t2376405012  ___MaxValue_2;
	// System.DateTime System.DateTime::MinValue
	DateTime_t2376405012  ___MinValue_3;
	// System.String[] System.DateTime::ParseTimeFormats
	StringU5BU5D_t2298632947* ___ParseTimeFormats_4;
	// System.String[] System.DateTime::ParseYearDayMonthFormats
	StringU5BU5D_t2298632947* ___ParseYearDayMonthFormats_5;
	// System.String[] System.DateTime::ParseYearMonthDayFormats
	StringU5BU5D_t2298632947* ___ParseYearMonthDayFormats_6;
	// System.String[] System.DateTime::ParseDayMonthYearFormats
	StringU5BU5D_t2298632947* ___ParseDayMonthYearFormats_7;
	// System.String[] System.DateTime::ParseMonthDayYearFormats
	StringU5BU5D_t2298632947* ___ParseMonthDayYearFormats_8;
	// System.String[] System.DateTime::MonthDayShortFormats
	StringU5BU5D_t2298632947* ___MonthDayShortFormats_9;
	// System.String[] System.DateTime::DayMonthShortFormats
	StringU5BU5D_t2298632947* ___DayMonthShortFormats_10;
	// System.Int32[] System.DateTime::daysmonth
	Int32U5BU5D_t2324750880* ___daysmonth_11;
	// System.Int32[] System.DateTime::daysmonthleap
	Int32U5BU5D_t2324750880* ___daysmonthleap_12;
	// System.Object System.DateTime::to_local_time_span_object
	RuntimeObject * ___to_local_time_span_object_13;
	// System.Int64 System.DateTime::last_now
	int64_t ___last_now_14;

public:
	inline static int32_t get_offset_of_MaxValue_2() { return static_cast<int32_t>(offsetof(DateTime_t2376405012_StaticFields, ___MaxValue_2)); }
	inline DateTime_t2376405012  get_MaxValue_2() const { return ___MaxValue_2; }
	inline DateTime_t2376405012 * get_address_of_MaxValue_2() { return &___MaxValue_2; }
	inline void set_MaxValue_2(DateTime_t2376405012  value)
	{
		___MaxValue_2 = value;
	}

	inline static int32_t get_offset_of_MinValue_3() { return static_cast<int32_t>(offsetof(DateTime_t2376405012_StaticFields, ___MinValue_3)); }
	inline DateTime_t2376405012  get_MinValue_3() const { return ___MinValue_3; }
	inline DateTime_t2376405012 * get_address_of_MinValue_3() { return &___MinValue_3; }
	inline void set_MinValue_3(DateTime_t2376405012  value)
	{
		___MinValue_3 = value;
	}

	inline static int32_t get_offset_of_ParseTimeFormats_4() { return static_cast<int32_t>(offsetof(DateTime_t2376405012_StaticFields, ___ParseTimeFormats_4)); }
	inline StringU5BU5D_t2298632947* get_ParseTimeFormats_4() const { return ___ParseTimeFormats_4; }
	inline StringU5BU5D_t2298632947** get_address_of_ParseTimeFormats_4() { return &___ParseTimeFormats_4; }
	inline void set_ParseTimeFormats_4(StringU5BU5D_t2298632947* value)
	{
		___ParseTimeFormats_4 = value;
		Il2CppCodeGenWriteBarrier((&___ParseTimeFormats_4), value);
	}

	inline static int32_t get_offset_of_ParseYearDayMonthFormats_5() { return static_cast<int32_t>(offsetof(DateTime_t2376405012_StaticFields, ___ParseYearDayMonthFormats_5)); }
	inline StringU5BU5D_t2298632947* get_ParseYearDayMonthFormats_5() const { return ___ParseYearDayMonthFormats_5; }
	inline StringU5BU5D_t2298632947** get_address_of_ParseYearDayMonthFormats_5() { return &___ParseYearDayMonthFormats_5; }
	inline void set_ParseYearDayMonthFormats_5(StringU5BU5D_t2298632947* value)
	{
		___ParseYearDayMonthFormats_5 = value;
		Il2CppCodeGenWriteBarrier((&___ParseYearDayMonthFormats_5), value);
	}

	inline static int32_t get_offset_of_ParseYearMonthDayFormats_6() { return static_cast<int32_t>(offsetof(DateTime_t2376405012_StaticFields, ___ParseYearMonthDayFormats_6)); }
	inline StringU5BU5D_t2298632947* get_ParseYearMonthDayFormats_6() const { return ___ParseYearMonthDayFormats_6; }
	inline StringU5BU5D_t2298632947** get_address_of_ParseYearMonthDayFormats_6() { return &___ParseYearMonthDayFormats_6; }
	inline void set_ParseYearMonthDayFormats_6(StringU5BU5D_t2298632947* value)
	{
		___ParseYearMonthDayFormats_6 = value;
		Il2CppCodeGenWriteBarrier((&___ParseYearMonthDayFormats_6), value);
	}

	inline static int32_t get_offset_of_ParseDayMonthYearFormats_7() { return static_cast<int32_t>(offsetof(DateTime_t2376405012_StaticFields, ___ParseDayMonthYearFormats_7)); }
	inline StringU5BU5D_t2298632947* get_ParseDayMonthYearFormats_7() const { return ___ParseDayMonthYearFormats_7; }
	inline StringU5BU5D_t2298632947** get_address_of_ParseDayMonthYearFormats_7() { return &___ParseDayMonthYearFormats_7; }
	inline void set_ParseDayMonthYearFormats_7(StringU5BU5D_t2298632947* value)
	{
		___ParseDayMonthYearFormats_7 = value;
		Il2CppCodeGenWriteBarrier((&___ParseDayMonthYearFormats_7), value);
	}

	inline static int32_t get_offset_of_ParseMonthDayYearFormats_8() { return static_cast<int32_t>(offsetof(DateTime_t2376405012_StaticFields, ___ParseMonthDayYearFormats_8)); }
	inline StringU5BU5D_t2298632947* get_ParseMonthDayYearFormats_8() const { return ___ParseMonthDayYearFormats_8; }
	inline StringU5BU5D_t2298632947** get_address_of_ParseMonthDayYearFormats_8() { return &___ParseMonthDayYearFormats_8; }
	inline void set_ParseMonthDayYearFormats_8(StringU5BU5D_t2298632947* value)
	{
		___ParseMonthDayYearFormats_8 = value;
		Il2CppCodeGenWriteBarrier((&___ParseMonthDayYearFormats_8), value);
	}

	inline static int32_t get_offset_of_MonthDayShortFormats_9() { return static_cast<int32_t>(offsetof(DateTime_t2376405012_StaticFields, ___MonthDayShortFormats_9)); }
	inline StringU5BU5D_t2298632947* get_MonthDayShortFormats_9() const { return ___MonthDayShortFormats_9; }
	inline StringU5BU5D_t2298632947** get_address_of_MonthDayShortFormats_9() { return &___MonthDayShortFormats_9; }
	inline void set_MonthDayShortFormats_9(StringU5BU5D_t2298632947* value)
	{
		___MonthDayShortFormats_9 = value;
		Il2CppCodeGenWriteBarrier((&___MonthDayShortFormats_9), value);
	}

	inline static int32_t get_offset_of_DayMonthShortFormats_10() { return static_cast<int32_t>(offsetof(DateTime_t2376405012_StaticFields, ___DayMonthShortFormats_10)); }
	inline StringU5BU5D_t2298632947* get_DayMonthShortFormats_10() const { return ___DayMonthShortFormats_10; }
	inline StringU5BU5D_t2298632947** get_address_of_DayMonthShortFormats_10() { return &___DayMonthShortFormats_10; }
	inline void set_DayMonthShortFormats_10(StringU5BU5D_t2298632947* value)
	{
		___DayMonthShortFormats_10 = value;
		Il2CppCodeGenWriteBarrier((&___DayMonthShortFormats_10), value);
	}

	inline static int32_t get_offset_of_daysmonth_11() { return static_cast<int32_t>(offsetof(DateTime_t2376405012_StaticFields, ___daysmonth_11)); }
	inline Int32U5BU5D_t2324750880* get_daysmonth_11() const { return ___daysmonth_11; }
	inline Int32U5BU5D_t2324750880** get_address_of_daysmonth_11() { return &___daysmonth_11; }
	inline void set_daysmonth_11(Int32U5BU5D_t2324750880* value)
	{
		___daysmonth_11 = value;
		Il2CppCodeGenWriteBarrier((&___daysmonth_11), value);
	}

	inline static int32_t get_offset_of_daysmonthleap_12() { return static_cast<int32_t>(offsetof(DateTime_t2376405012_StaticFields, ___daysmonthleap_12)); }
	inline Int32U5BU5D_t2324750880* get_daysmonthleap_12() const { return ___daysmonthleap_12; }
	inline Int32U5BU5D_t2324750880** get_address_of_daysmonthleap_12() { return &___daysmonthleap_12; }
	inline void set_daysmonthleap_12(Int32U5BU5D_t2324750880* value)
	{
		___daysmonthleap_12 = value;
		Il2CppCodeGenWriteBarrier((&___daysmonthleap_12), value);
	}

	inline static int32_t get_offset_of_to_local_time_span_object_13() { return static_cast<int32_t>(offsetof(DateTime_t2376405012_StaticFields, ___to_local_time_span_object_13)); }
	inline RuntimeObject * get_to_local_time_span_object_13() const { return ___to_local_time_span_object_13; }
	inline RuntimeObject ** get_address_of_to_local_time_span_object_13() { return &___to_local_time_span_object_13; }
	inline void set_to_local_time_span_object_13(RuntimeObject * value)
	{
		___to_local_time_span_object_13 = value;
		Il2CppCodeGenWriteBarrier((&___to_local_time_span_object_13), value);
	}

	inline static int32_t get_offset_of_last_now_14() { return static_cast<int32_t>(offsetof(DateTime_t2376405012_StaticFields, ___last_now_14)); }
	inline int64_t get_last_now_14() const { return ___last_now_14; }
	inline int64_t* get_address_of_last_now_14() { return &___last_now_14; }
	inline void set_last_now_14(int64_t value)
	{
		___last_now_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATETIME_T2376405012_H
#ifndef COMPONENT_T4087199522_H
#define COMPONENT_T4087199522_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t4087199522  : public Object_t1502412432
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T4087199522_H
#ifndef IMAGEIMPL_T3918258889_H
#define IMAGEIMPL_T3918258889_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.ImageImpl
struct  ImageImpl_t3918258889  : public Image_t1445313791
{
public:
	// System.Int32 Vuforia.ImageImpl::mWidth
	int32_t ___mWidth_0;
	// System.Int32 Vuforia.ImageImpl::mHeight
	int32_t ___mHeight_1;
	// System.Int32 Vuforia.ImageImpl::mStride
	int32_t ___mStride_2;
	// System.Int32 Vuforia.ImageImpl::mBufferWidth
	int32_t ___mBufferWidth_3;
	// System.Int32 Vuforia.ImageImpl::mBufferHeight
	int32_t ___mBufferHeight_4;
	// Vuforia.Image/PIXEL_FORMAT Vuforia.ImageImpl::mPixelFormat
	int32_t ___mPixelFormat_5;
	// System.Byte[] Vuforia.ImageImpl::mData
	ByteU5BU5D_t3172826560* ___mData_6;
	// System.IntPtr Vuforia.ImageImpl::mUnmanagedData
	IntPtr_t ___mUnmanagedData_7;
	// System.Boolean Vuforia.ImageImpl::mDataSet
	bool ___mDataSet_8;
	// UnityEngine.Color32[] Vuforia.ImageImpl::mPixel32
	Color32U5BU5D_t2266382834* ___mPixel32_9;

public:
	inline static int32_t get_offset_of_mWidth_0() { return static_cast<int32_t>(offsetof(ImageImpl_t3918258889, ___mWidth_0)); }
	inline int32_t get_mWidth_0() const { return ___mWidth_0; }
	inline int32_t* get_address_of_mWidth_0() { return &___mWidth_0; }
	inline void set_mWidth_0(int32_t value)
	{
		___mWidth_0 = value;
	}

	inline static int32_t get_offset_of_mHeight_1() { return static_cast<int32_t>(offsetof(ImageImpl_t3918258889, ___mHeight_1)); }
	inline int32_t get_mHeight_1() const { return ___mHeight_1; }
	inline int32_t* get_address_of_mHeight_1() { return &___mHeight_1; }
	inline void set_mHeight_1(int32_t value)
	{
		___mHeight_1 = value;
	}

	inline static int32_t get_offset_of_mStride_2() { return static_cast<int32_t>(offsetof(ImageImpl_t3918258889, ___mStride_2)); }
	inline int32_t get_mStride_2() const { return ___mStride_2; }
	inline int32_t* get_address_of_mStride_2() { return &___mStride_2; }
	inline void set_mStride_2(int32_t value)
	{
		___mStride_2 = value;
	}

	inline static int32_t get_offset_of_mBufferWidth_3() { return static_cast<int32_t>(offsetof(ImageImpl_t3918258889, ___mBufferWidth_3)); }
	inline int32_t get_mBufferWidth_3() const { return ___mBufferWidth_3; }
	inline int32_t* get_address_of_mBufferWidth_3() { return &___mBufferWidth_3; }
	inline void set_mBufferWidth_3(int32_t value)
	{
		___mBufferWidth_3 = value;
	}

	inline static int32_t get_offset_of_mBufferHeight_4() { return static_cast<int32_t>(offsetof(ImageImpl_t3918258889, ___mBufferHeight_4)); }
	inline int32_t get_mBufferHeight_4() const { return ___mBufferHeight_4; }
	inline int32_t* get_address_of_mBufferHeight_4() { return &___mBufferHeight_4; }
	inline void set_mBufferHeight_4(int32_t value)
	{
		___mBufferHeight_4 = value;
	}

	inline static int32_t get_offset_of_mPixelFormat_5() { return static_cast<int32_t>(offsetof(ImageImpl_t3918258889, ___mPixelFormat_5)); }
	inline int32_t get_mPixelFormat_5() const { return ___mPixelFormat_5; }
	inline int32_t* get_address_of_mPixelFormat_5() { return &___mPixelFormat_5; }
	inline void set_mPixelFormat_5(int32_t value)
	{
		___mPixelFormat_5 = value;
	}

	inline static int32_t get_offset_of_mData_6() { return static_cast<int32_t>(offsetof(ImageImpl_t3918258889, ___mData_6)); }
	inline ByteU5BU5D_t3172826560* get_mData_6() const { return ___mData_6; }
	inline ByteU5BU5D_t3172826560** get_address_of_mData_6() { return &___mData_6; }
	inline void set_mData_6(ByteU5BU5D_t3172826560* value)
	{
		___mData_6 = value;
		Il2CppCodeGenWriteBarrier((&___mData_6), value);
	}

	inline static int32_t get_offset_of_mUnmanagedData_7() { return static_cast<int32_t>(offsetof(ImageImpl_t3918258889, ___mUnmanagedData_7)); }
	inline IntPtr_t get_mUnmanagedData_7() const { return ___mUnmanagedData_7; }
	inline IntPtr_t* get_address_of_mUnmanagedData_7() { return &___mUnmanagedData_7; }
	inline void set_mUnmanagedData_7(IntPtr_t value)
	{
		___mUnmanagedData_7 = value;
	}

	inline static int32_t get_offset_of_mDataSet_8() { return static_cast<int32_t>(offsetof(ImageImpl_t3918258889, ___mDataSet_8)); }
	inline bool get_mDataSet_8() const { return ___mDataSet_8; }
	inline bool* get_address_of_mDataSet_8() { return &___mDataSet_8; }
	inline void set_mDataSet_8(bool value)
	{
		___mDataSet_8 = value;
	}

	inline static int32_t get_offset_of_mPixel32_9() { return static_cast<int32_t>(offsetof(ImageImpl_t3918258889, ___mPixel32_9)); }
	inline Color32U5BU5D_t2266382834* get_mPixel32_9() const { return ___mPixel32_9; }
	inline Color32U5BU5D_t2266382834** get_address_of_mPixel32_9() { return &___mPixel32_9; }
	inline void set_mPixel32_9(Color32U5BU5D_t2266382834* value)
	{
		___mPixel32_9 = value;
		Il2CppCodeGenWriteBarrier((&___mPixel32_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IMAGEIMPL_T3918258889_H
#ifndef VIDEOBACKGROUNDCONFIGURATION_T2579534243_H
#define VIDEOBACKGROUNDCONFIGURATION_T2579534243_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.VuforiaAbstractConfiguration/VideoBackgroundConfiguration
struct  VideoBackgroundConfiguration_t2579534243  : public RuntimeObject
{
public:
	// Vuforia.HideExcessAreaAbstractBehaviour/CLIPPING_MODE Vuforia.VuforiaAbstractConfiguration/VideoBackgroundConfiguration::clippingMode
	int32_t ___clippingMode_0;
	// UnityEngine.Shader Vuforia.VuforiaAbstractConfiguration/VideoBackgroundConfiguration::matteShader
	Shader_t3778723916 * ___matteShader_1;
	// System.Boolean Vuforia.VuforiaAbstractConfiguration/VideoBackgroundConfiguration::videoBackgroundEnabled
	bool ___videoBackgroundEnabled_2;

public:
	inline static int32_t get_offset_of_clippingMode_0() { return static_cast<int32_t>(offsetof(VideoBackgroundConfiguration_t2579534243, ___clippingMode_0)); }
	inline int32_t get_clippingMode_0() const { return ___clippingMode_0; }
	inline int32_t* get_address_of_clippingMode_0() { return &___clippingMode_0; }
	inline void set_clippingMode_0(int32_t value)
	{
		___clippingMode_0 = value;
	}

	inline static int32_t get_offset_of_matteShader_1() { return static_cast<int32_t>(offsetof(VideoBackgroundConfiguration_t2579534243, ___matteShader_1)); }
	inline Shader_t3778723916 * get_matteShader_1() const { return ___matteShader_1; }
	inline Shader_t3778723916 ** get_address_of_matteShader_1() { return &___matteShader_1; }
	inline void set_matteShader_1(Shader_t3778723916 * value)
	{
		___matteShader_1 = value;
		Il2CppCodeGenWriteBarrier((&___matteShader_1), value);
	}

	inline static int32_t get_offset_of_videoBackgroundEnabled_2() { return static_cast<int32_t>(offsetof(VideoBackgroundConfiguration_t2579534243, ___videoBackgroundEnabled_2)); }
	inline bool get_videoBackgroundEnabled_2() const { return ___videoBackgroundEnabled_2; }
	inline bool* get_address_of_videoBackgroundEnabled_2() { return &___videoBackgroundEnabled_2; }
	inline void set_videoBackgroundEnabled_2(bool value)
	{
		___videoBackgroundEnabled_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VIDEOBACKGROUNDCONFIGURATION_T2579534243_H
#ifndef DIGITALEYEWEARCONFIGURATION_T3649657388_H
#define DIGITALEYEWEARCONFIGURATION_T3649657388_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.VuforiaAbstractConfiguration/DigitalEyewearConfiguration
struct  DigitalEyewearConfiguration_t3649657388  : public RuntimeObject
{
public:
	// System.Single Vuforia.VuforiaAbstractConfiguration/DigitalEyewearConfiguration::cameraOffset
	float ___cameraOffset_0;
	// Vuforia.DistortionRenderingMode Vuforia.VuforiaAbstractConfiguration/DigitalEyewearConfiguration::distortionRenderingMode
	int32_t ___distortionRenderingMode_1;
	// System.Int32 Vuforia.VuforiaAbstractConfiguration/DigitalEyewearConfiguration::distortionRenderingLayer
	int32_t ___distortionRenderingLayer_2;
	// Vuforia.DigitalEyewearARController/EyewearType Vuforia.VuforiaAbstractConfiguration/DigitalEyewearConfiguration::eyewearType
	int32_t ___eyewearType_3;
	// Vuforia.DigitalEyewearARController/StereoFramework Vuforia.VuforiaAbstractConfiguration/DigitalEyewearConfiguration::stereoFramework
	int32_t ___stereoFramework_4;
	// Vuforia.DigitalEyewearARController/SeeThroughConfiguration Vuforia.VuforiaAbstractConfiguration/DigitalEyewearConfiguration::seeThroughConfiguration
	int32_t ___seeThroughConfiguration_5;
	// System.String Vuforia.VuforiaAbstractConfiguration/DigitalEyewearConfiguration::viewerName
	String_t* ___viewerName_6;
	// System.String Vuforia.VuforiaAbstractConfiguration/DigitalEyewearConfiguration::viewerManufacturer
	String_t* ___viewerManufacturer_7;
	// System.Boolean Vuforia.VuforiaAbstractConfiguration/DigitalEyewearConfiguration::useCustomViewer
	bool ___useCustomViewer_8;
	// Vuforia.DigitalEyewearARController/SerializableViewerParameters Vuforia.VuforiaAbstractConfiguration/DigitalEyewearConfiguration::customViewer
	SerializableViewerParameters_t484734121 * ___customViewer_9;

public:
	inline static int32_t get_offset_of_cameraOffset_0() { return static_cast<int32_t>(offsetof(DigitalEyewearConfiguration_t3649657388, ___cameraOffset_0)); }
	inline float get_cameraOffset_0() const { return ___cameraOffset_0; }
	inline float* get_address_of_cameraOffset_0() { return &___cameraOffset_0; }
	inline void set_cameraOffset_0(float value)
	{
		___cameraOffset_0 = value;
	}

	inline static int32_t get_offset_of_distortionRenderingMode_1() { return static_cast<int32_t>(offsetof(DigitalEyewearConfiguration_t3649657388, ___distortionRenderingMode_1)); }
	inline int32_t get_distortionRenderingMode_1() const { return ___distortionRenderingMode_1; }
	inline int32_t* get_address_of_distortionRenderingMode_1() { return &___distortionRenderingMode_1; }
	inline void set_distortionRenderingMode_1(int32_t value)
	{
		___distortionRenderingMode_1 = value;
	}

	inline static int32_t get_offset_of_distortionRenderingLayer_2() { return static_cast<int32_t>(offsetof(DigitalEyewearConfiguration_t3649657388, ___distortionRenderingLayer_2)); }
	inline int32_t get_distortionRenderingLayer_2() const { return ___distortionRenderingLayer_2; }
	inline int32_t* get_address_of_distortionRenderingLayer_2() { return &___distortionRenderingLayer_2; }
	inline void set_distortionRenderingLayer_2(int32_t value)
	{
		___distortionRenderingLayer_2 = value;
	}

	inline static int32_t get_offset_of_eyewearType_3() { return static_cast<int32_t>(offsetof(DigitalEyewearConfiguration_t3649657388, ___eyewearType_3)); }
	inline int32_t get_eyewearType_3() const { return ___eyewearType_3; }
	inline int32_t* get_address_of_eyewearType_3() { return &___eyewearType_3; }
	inline void set_eyewearType_3(int32_t value)
	{
		___eyewearType_3 = value;
	}

	inline static int32_t get_offset_of_stereoFramework_4() { return static_cast<int32_t>(offsetof(DigitalEyewearConfiguration_t3649657388, ___stereoFramework_4)); }
	inline int32_t get_stereoFramework_4() const { return ___stereoFramework_4; }
	inline int32_t* get_address_of_stereoFramework_4() { return &___stereoFramework_4; }
	inline void set_stereoFramework_4(int32_t value)
	{
		___stereoFramework_4 = value;
	}

	inline static int32_t get_offset_of_seeThroughConfiguration_5() { return static_cast<int32_t>(offsetof(DigitalEyewearConfiguration_t3649657388, ___seeThroughConfiguration_5)); }
	inline int32_t get_seeThroughConfiguration_5() const { return ___seeThroughConfiguration_5; }
	inline int32_t* get_address_of_seeThroughConfiguration_5() { return &___seeThroughConfiguration_5; }
	inline void set_seeThroughConfiguration_5(int32_t value)
	{
		___seeThroughConfiguration_5 = value;
	}

	inline static int32_t get_offset_of_viewerName_6() { return static_cast<int32_t>(offsetof(DigitalEyewearConfiguration_t3649657388, ___viewerName_6)); }
	inline String_t* get_viewerName_6() const { return ___viewerName_6; }
	inline String_t** get_address_of_viewerName_6() { return &___viewerName_6; }
	inline void set_viewerName_6(String_t* value)
	{
		___viewerName_6 = value;
		Il2CppCodeGenWriteBarrier((&___viewerName_6), value);
	}

	inline static int32_t get_offset_of_viewerManufacturer_7() { return static_cast<int32_t>(offsetof(DigitalEyewearConfiguration_t3649657388, ___viewerManufacturer_7)); }
	inline String_t* get_viewerManufacturer_7() const { return ___viewerManufacturer_7; }
	inline String_t** get_address_of_viewerManufacturer_7() { return &___viewerManufacturer_7; }
	inline void set_viewerManufacturer_7(String_t* value)
	{
		___viewerManufacturer_7 = value;
		Il2CppCodeGenWriteBarrier((&___viewerManufacturer_7), value);
	}

	inline static int32_t get_offset_of_useCustomViewer_8() { return static_cast<int32_t>(offsetof(DigitalEyewearConfiguration_t3649657388, ___useCustomViewer_8)); }
	inline bool get_useCustomViewer_8() const { return ___useCustomViewer_8; }
	inline bool* get_address_of_useCustomViewer_8() { return &___useCustomViewer_8; }
	inline void set_useCustomViewer_8(bool value)
	{
		___useCustomViewer_8 = value;
	}

	inline static int32_t get_offset_of_customViewer_9() { return static_cast<int32_t>(offsetof(DigitalEyewearConfiguration_t3649657388, ___customViewer_9)); }
	inline SerializableViewerParameters_t484734121 * get_customViewer_9() const { return ___customViewer_9; }
	inline SerializableViewerParameters_t484734121 ** get_address_of_customViewer_9() { return &___customViewer_9; }
	inline void set_customViewer_9(SerializableViewerParameters_t484734121 * value)
	{
		___customViewer_9 = value;
		Il2CppCodeGenWriteBarrier((&___customViewer_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DIGITALEYEWEARCONFIGURATION_T3649657388_H
#ifndef GENERICVUFORIACONFIGURATION_T1574456586_H
#define GENERICVUFORIACONFIGURATION_T1574456586_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.VuforiaAbstractConfiguration/GenericVuforiaConfiguration
struct  GenericVuforiaConfiguration_t1574456586  : public RuntimeObject
{
public:
	// System.String Vuforia.VuforiaAbstractConfiguration/GenericVuforiaConfiguration::vuforiaLicenseKey
	String_t* ___vuforiaLicenseKey_0;
	// System.Boolean Vuforia.VuforiaAbstractConfiguration/GenericVuforiaConfiguration::delayedInitialization
	bool ___delayedInitialization_1;
	// Vuforia.CameraDevice/CameraDeviceMode Vuforia.VuforiaAbstractConfiguration/GenericVuforiaConfiguration::cameraDeviceModeSetting
	int32_t ___cameraDeviceModeSetting_2;
	// System.Int32 Vuforia.VuforiaAbstractConfiguration/GenericVuforiaConfiguration::maxSimultaneousImageTargets
	int32_t ___maxSimultaneousImageTargets_3;
	// System.Int32 Vuforia.VuforiaAbstractConfiguration/GenericVuforiaConfiguration::maxSimultaneousObjectTargets
	int32_t ___maxSimultaneousObjectTargets_4;
	// System.Boolean Vuforia.VuforiaAbstractConfiguration/GenericVuforiaConfiguration::useDelayedLoadingObjectTargets
	bool ___useDelayedLoadingObjectTargets_5;
	// Vuforia.CameraDevice/CameraDirection Vuforia.VuforiaAbstractConfiguration/GenericVuforiaConfiguration::cameraDirection
	int32_t ___cameraDirection_6;
	// Vuforia.VuforiaRenderer/VideoBackgroundReflection Vuforia.VuforiaAbstractConfiguration/GenericVuforiaConfiguration::mirrorVideoBackground
	int32_t ___mirrorVideoBackground_7;

public:
	inline static int32_t get_offset_of_vuforiaLicenseKey_0() { return static_cast<int32_t>(offsetof(GenericVuforiaConfiguration_t1574456586, ___vuforiaLicenseKey_0)); }
	inline String_t* get_vuforiaLicenseKey_0() const { return ___vuforiaLicenseKey_0; }
	inline String_t** get_address_of_vuforiaLicenseKey_0() { return &___vuforiaLicenseKey_0; }
	inline void set_vuforiaLicenseKey_0(String_t* value)
	{
		___vuforiaLicenseKey_0 = value;
		Il2CppCodeGenWriteBarrier((&___vuforiaLicenseKey_0), value);
	}

	inline static int32_t get_offset_of_delayedInitialization_1() { return static_cast<int32_t>(offsetof(GenericVuforiaConfiguration_t1574456586, ___delayedInitialization_1)); }
	inline bool get_delayedInitialization_1() const { return ___delayedInitialization_1; }
	inline bool* get_address_of_delayedInitialization_1() { return &___delayedInitialization_1; }
	inline void set_delayedInitialization_1(bool value)
	{
		___delayedInitialization_1 = value;
	}

	inline static int32_t get_offset_of_cameraDeviceModeSetting_2() { return static_cast<int32_t>(offsetof(GenericVuforiaConfiguration_t1574456586, ___cameraDeviceModeSetting_2)); }
	inline int32_t get_cameraDeviceModeSetting_2() const { return ___cameraDeviceModeSetting_2; }
	inline int32_t* get_address_of_cameraDeviceModeSetting_2() { return &___cameraDeviceModeSetting_2; }
	inline void set_cameraDeviceModeSetting_2(int32_t value)
	{
		___cameraDeviceModeSetting_2 = value;
	}

	inline static int32_t get_offset_of_maxSimultaneousImageTargets_3() { return static_cast<int32_t>(offsetof(GenericVuforiaConfiguration_t1574456586, ___maxSimultaneousImageTargets_3)); }
	inline int32_t get_maxSimultaneousImageTargets_3() const { return ___maxSimultaneousImageTargets_3; }
	inline int32_t* get_address_of_maxSimultaneousImageTargets_3() { return &___maxSimultaneousImageTargets_3; }
	inline void set_maxSimultaneousImageTargets_3(int32_t value)
	{
		___maxSimultaneousImageTargets_3 = value;
	}

	inline static int32_t get_offset_of_maxSimultaneousObjectTargets_4() { return static_cast<int32_t>(offsetof(GenericVuforiaConfiguration_t1574456586, ___maxSimultaneousObjectTargets_4)); }
	inline int32_t get_maxSimultaneousObjectTargets_4() const { return ___maxSimultaneousObjectTargets_4; }
	inline int32_t* get_address_of_maxSimultaneousObjectTargets_4() { return &___maxSimultaneousObjectTargets_4; }
	inline void set_maxSimultaneousObjectTargets_4(int32_t value)
	{
		___maxSimultaneousObjectTargets_4 = value;
	}

	inline static int32_t get_offset_of_useDelayedLoadingObjectTargets_5() { return static_cast<int32_t>(offsetof(GenericVuforiaConfiguration_t1574456586, ___useDelayedLoadingObjectTargets_5)); }
	inline bool get_useDelayedLoadingObjectTargets_5() const { return ___useDelayedLoadingObjectTargets_5; }
	inline bool* get_address_of_useDelayedLoadingObjectTargets_5() { return &___useDelayedLoadingObjectTargets_5; }
	inline void set_useDelayedLoadingObjectTargets_5(bool value)
	{
		___useDelayedLoadingObjectTargets_5 = value;
	}

	inline static int32_t get_offset_of_cameraDirection_6() { return static_cast<int32_t>(offsetof(GenericVuforiaConfiguration_t1574456586, ___cameraDirection_6)); }
	inline int32_t get_cameraDirection_6() const { return ___cameraDirection_6; }
	inline int32_t* get_address_of_cameraDirection_6() { return &___cameraDirection_6; }
	inline void set_cameraDirection_6(int32_t value)
	{
		___cameraDirection_6 = value;
	}

	inline static int32_t get_offset_of_mirrorVideoBackground_7() { return static_cast<int32_t>(offsetof(GenericVuforiaConfiguration_t1574456586, ___mirrorVideoBackground_7)); }
	inline int32_t get_mirrorVideoBackground_7() const { return ___mirrorVideoBackground_7; }
	inline int32_t* get_address_of_mirrorVideoBackground_7() { return &___mirrorVideoBackground_7; }
	inline void set_mirrorVideoBackground_7(int32_t value)
	{
		___mirrorVideoBackground_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GENERICVUFORIACONFIGURATION_T1574456586_H
#ifndef CYLINDERTARGETIMPL_T2417802664_H
#define CYLINDERTARGETIMPL_T2417802664_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.CylinderTargetImpl
struct  CylinderTargetImpl_t2417802664  : public ObjectTargetImpl_t1592445137
{
public:
	// System.Single Vuforia.CylinderTargetImpl::mSideLength
	float ___mSideLength_4;
	// System.Single Vuforia.CylinderTargetImpl::mTopDiameter
	float ___mTopDiameter_5;
	// System.Single Vuforia.CylinderTargetImpl::mBottomDiameter
	float ___mBottomDiameter_6;

public:
	inline static int32_t get_offset_of_mSideLength_4() { return static_cast<int32_t>(offsetof(CylinderTargetImpl_t2417802664, ___mSideLength_4)); }
	inline float get_mSideLength_4() const { return ___mSideLength_4; }
	inline float* get_address_of_mSideLength_4() { return &___mSideLength_4; }
	inline void set_mSideLength_4(float value)
	{
		___mSideLength_4 = value;
	}

	inline static int32_t get_offset_of_mTopDiameter_5() { return static_cast<int32_t>(offsetof(CylinderTargetImpl_t2417802664, ___mTopDiameter_5)); }
	inline float get_mTopDiameter_5() const { return ___mTopDiameter_5; }
	inline float* get_address_of_mTopDiameter_5() { return &___mTopDiameter_5; }
	inline void set_mTopDiameter_5(float value)
	{
		___mTopDiameter_5 = value;
	}

	inline static int32_t get_offset_of_mBottomDiameter_6() { return static_cast<int32_t>(offsetof(CylinderTargetImpl_t2417802664, ___mBottomDiameter_6)); }
	inline float get_mBottomDiameter_6() const { return ___mBottomDiameter_6; }
	inline float* get_address_of_mBottomDiameter_6() { return &___mBottomDiameter_6; }
	inline void set_mBottomDiameter_6(float value)
	{
		___mBottomDiameter_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CYLINDERTARGETIMPL_T2417802664_H
#ifndef CAMERADEVICEIMPL_T987560882_H
#define CAMERADEVICEIMPL_T987560882_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.CameraDeviceImpl
struct  CameraDeviceImpl_t987560882  : public CameraDevice_t2431414939
{
public:
	// System.Collections.Generic.Dictionary`2<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image> Vuforia.CameraDeviceImpl::mCameraImages
	Dictionary_2_t708360668 * ___mCameraImages_1;
	// System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT> Vuforia.CameraDeviceImpl::mForcedCameraFormats
	List_1_t440776343 * ___mForcedCameraFormats_2;
	// System.Boolean Vuforia.CameraDeviceImpl::mCameraReady
	bool ___mCameraReady_4;
	// System.Boolean Vuforia.CameraDeviceImpl::mIsDirty
	bool ___mIsDirty_5;
	// Vuforia.CameraDevice/CameraDirection Vuforia.CameraDeviceImpl::mActualCameraDirection
	int32_t ___mActualCameraDirection_6;
	// Vuforia.CameraDevice/CameraDirection Vuforia.CameraDeviceImpl::mSelectedCameraDirection
	int32_t ___mSelectedCameraDirection_7;
	// Vuforia.CameraDevice/CameraDeviceMode Vuforia.CameraDeviceImpl::mCameraDeviceMode
	int32_t ___mCameraDeviceMode_8;
	// Vuforia.CameraDevice/VideoModeData Vuforia.CameraDeviceImpl::mVideoModeData
	VideoModeData_t2436408626  ___mVideoModeData_9;
	// System.Boolean Vuforia.CameraDeviceImpl::mVideoModeDataNeedsUpdate
	bool ___mVideoModeDataNeedsUpdate_10;
	// System.Boolean Vuforia.CameraDeviceImpl::mHasCameraDeviceModeBeenSet
	bool ___mHasCameraDeviceModeBeenSet_11;
	// System.Boolean Vuforia.CameraDeviceImpl::mCameraActive
	bool ___mCameraActive_12;

public:
	inline static int32_t get_offset_of_mCameraImages_1() { return static_cast<int32_t>(offsetof(CameraDeviceImpl_t987560882, ___mCameraImages_1)); }
	inline Dictionary_2_t708360668 * get_mCameraImages_1() const { return ___mCameraImages_1; }
	inline Dictionary_2_t708360668 ** get_address_of_mCameraImages_1() { return &___mCameraImages_1; }
	inline void set_mCameraImages_1(Dictionary_2_t708360668 * value)
	{
		___mCameraImages_1 = value;
		Il2CppCodeGenWriteBarrier((&___mCameraImages_1), value);
	}

	inline static int32_t get_offset_of_mForcedCameraFormats_2() { return static_cast<int32_t>(offsetof(CameraDeviceImpl_t987560882, ___mForcedCameraFormats_2)); }
	inline List_1_t440776343 * get_mForcedCameraFormats_2() const { return ___mForcedCameraFormats_2; }
	inline List_1_t440776343 ** get_address_of_mForcedCameraFormats_2() { return &___mForcedCameraFormats_2; }
	inline void set_mForcedCameraFormats_2(List_1_t440776343 * value)
	{
		___mForcedCameraFormats_2 = value;
		Il2CppCodeGenWriteBarrier((&___mForcedCameraFormats_2), value);
	}

	inline static int32_t get_offset_of_mCameraReady_4() { return static_cast<int32_t>(offsetof(CameraDeviceImpl_t987560882, ___mCameraReady_4)); }
	inline bool get_mCameraReady_4() const { return ___mCameraReady_4; }
	inline bool* get_address_of_mCameraReady_4() { return &___mCameraReady_4; }
	inline void set_mCameraReady_4(bool value)
	{
		___mCameraReady_4 = value;
	}

	inline static int32_t get_offset_of_mIsDirty_5() { return static_cast<int32_t>(offsetof(CameraDeviceImpl_t987560882, ___mIsDirty_5)); }
	inline bool get_mIsDirty_5() const { return ___mIsDirty_5; }
	inline bool* get_address_of_mIsDirty_5() { return &___mIsDirty_5; }
	inline void set_mIsDirty_5(bool value)
	{
		___mIsDirty_5 = value;
	}

	inline static int32_t get_offset_of_mActualCameraDirection_6() { return static_cast<int32_t>(offsetof(CameraDeviceImpl_t987560882, ___mActualCameraDirection_6)); }
	inline int32_t get_mActualCameraDirection_6() const { return ___mActualCameraDirection_6; }
	inline int32_t* get_address_of_mActualCameraDirection_6() { return &___mActualCameraDirection_6; }
	inline void set_mActualCameraDirection_6(int32_t value)
	{
		___mActualCameraDirection_6 = value;
	}

	inline static int32_t get_offset_of_mSelectedCameraDirection_7() { return static_cast<int32_t>(offsetof(CameraDeviceImpl_t987560882, ___mSelectedCameraDirection_7)); }
	inline int32_t get_mSelectedCameraDirection_7() const { return ___mSelectedCameraDirection_7; }
	inline int32_t* get_address_of_mSelectedCameraDirection_7() { return &___mSelectedCameraDirection_7; }
	inline void set_mSelectedCameraDirection_7(int32_t value)
	{
		___mSelectedCameraDirection_7 = value;
	}

	inline static int32_t get_offset_of_mCameraDeviceMode_8() { return static_cast<int32_t>(offsetof(CameraDeviceImpl_t987560882, ___mCameraDeviceMode_8)); }
	inline int32_t get_mCameraDeviceMode_8() const { return ___mCameraDeviceMode_8; }
	inline int32_t* get_address_of_mCameraDeviceMode_8() { return &___mCameraDeviceMode_8; }
	inline void set_mCameraDeviceMode_8(int32_t value)
	{
		___mCameraDeviceMode_8 = value;
	}

	inline static int32_t get_offset_of_mVideoModeData_9() { return static_cast<int32_t>(offsetof(CameraDeviceImpl_t987560882, ___mVideoModeData_9)); }
	inline VideoModeData_t2436408626  get_mVideoModeData_9() const { return ___mVideoModeData_9; }
	inline VideoModeData_t2436408626 * get_address_of_mVideoModeData_9() { return &___mVideoModeData_9; }
	inline void set_mVideoModeData_9(VideoModeData_t2436408626  value)
	{
		___mVideoModeData_9 = value;
	}

	inline static int32_t get_offset_of_mVideoModeDataNeedsUpdate_10() { return static_cast<int32_t>(offsetof(CameraDeviceImpl_t987560882, ___mVideoModeDataNeedsUpdate_10)); }
	inline bool get_mVideoModeDataNeedsUpdate_10() const { return ___mVideoModeDataNeedsUpdate_10; }
	inline bool* get_address_of_mVideoModeDataNeedsUpdate_10() { return &___mVideoModeDataNeedsUpdate_10; }
	inline void set_mVideoModeDataNeedsUpdate_10(bool value)
	{
		___mVideoModeDataNeedsUpdate_10 = value;
	}

	inline static int32_t get_offset_of_mHasCameraDeviceModeBeenSet_11() { return static_cast<int32_t>(offsetof(CameraDeviceImpl_t987560882, ___mHasCameraDeviceModeBeenSet_11)); }
	inline bool get_mHasCameraDeviceModeBeenSet_11() const { return ___mHasCameraDeviceModeBeenSet_11; }
	inline bool* get_address_of_mHasCameraDeviceModeBeenSet_11() { return &___mHasCameraDeviceModeBeenSet_11; }
	inline void set_mHasCameraDeviceModeBeenSet_11(bool value)
	{
		___mHasCameraDeviceModeBeenSet_11 = value;
	}

	inline static int32_t get_offset_of_mCameraActive_12() { return static_cast<int32_t>(offsetof(CameraDeviceImpl_t987560882, ___mCameraActive_12)); }
	inline bool get_mCameraActive_12() const { return ___mCameraActive_12; }
	inline bool* get_address_of_mCameraActive_12() { return &___mCameraActive_12; }
	inline void set_mCameraActive_12(bool value)
	{
		___mCameraActive_12 = value;
	}
};

struct CameraDeviceImpl_t987560882_StaticFields
{
public:
	// Vuforia.IWebCam Vuforia.CameraDeviceImpl::mWebCam
	RuntimeObject* ___mWebCam_3;

public:
	inline static int32_t get_offset_of_mWebCam_3() { return static_cast<int32_t>(offsetof(CameraDeviceImpl_t987560882_StaticFields, ___mWebCam_3)); }
	inline RuntimeObject* get_mWebCam_3() const { return ___mWebCam_3; }
	inline RuntimeObject** get_address_of_mWebCam_3() { return &___mWebCam_3; }
	inline void set_mWebCam_3(RuntimeObject* value)
	{
		___mWebCam_3 = value;
		Il2CppCodeGenWriteBarrier((&___mWebCam_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAMERADEVICEIMPL_T987560882_H
#ifndef DATASETIMPL_T223764247_H
#define DATASETIMPL_T223764247_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.DataSetImpl
struct  DataSetImpl_t223764247  : public DataSet_t1443131193
{
public:
	// System.IntPtr Vuforia.DataSetImpl::mDataSetPtr
	IntPtr_t ___mDataSetPtr_0;
	// System.String Vuforia.DataSetImpl::mPath
	String_t* ___mPath_1;
	// Vuforia.VuforiaUnity/StorageType Vuforia.DataSetImpl::mStorageType
	int32_t ___mStorageType_2;
	// System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.Trackable> Vuforia.DataSetImpl::mTrackablesDict
	Dictionary_2_t2336703746 * ___mTrackablesDict_3;

public:
	inline static int32_t get_offset_of_mDataSetPtr_0() { return static_cast<int32_t>(offsetof(DataSetImpl_t223764247, ___mDataSetPtr_0)); }
	inline IntPtr_t get_mDataSetPtr_0() const { return ___mDataSetPtr_0; }
	inline IntPtr_t* get_address_of_mDataSetPtr_0() { return &___mDataSetPtr_0; }
	inline void set_mDataSetPtr_0(IntPtr_t value)
	{
		___mDataSetPtr_0 = value;
	}

	inline static int32_t get_offset_of_mPath_1() { return static_cast<int32_t>(offsetof(DataSetImpl_t223764247, ___mPath_1)); }
	inline String_t* get_mPath_1() const { return ___mPath_1; }
	inline String_t** get_address_of_mPath_1() { return &___mPath_1; }
	inline void set_mPath_1(String_t* value)
	{
		___mPath_1 = value;
		Il2CppCodeGenWriteBarrier((&___mPath_1), value);
	}

	inline static int32_t get_offset_of_mStorageType_2() { return static_cast<int32_t>(offsetof(DataSetImpl_t223764247, ___mStorageType_2)); }
	inline int32_t get_mStorageType_2() const { return ___mStorageType_2; }
	inline int32_t* get_address_of_mStorageType_2() { return &___mStorageType_2; }
	inline void set_mStorageType_2(int32_t value)
	{
		___mStorageType_2 = value;
	}

	inline static int32_t get_offset_of_mTrackablesDict_3() { return static_cast<int32_t>(offsetof(DataSetImpl_t223764247, ___mTrackablesDict_3)); }
	inline Dictionary_2_t2336703746 * get_mTrackablesDict_3() const { return ___mTrackablesDict_3; }
	inline Dictionary_2_t2336703746 ** get_address_of_mTrackablesDict_3() { return &___mTrackablesDict_3; }
	inline void set_mTrackablesDict_3(Dictionary_2_t2336703746 * value)
	{
		___mTrackablesDict_3 = value;
		Il2CppCodeGenWriteBarrier((&___mTrackablesDict_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATASETIMPL_T223764247_H
#ifndef SCRIPTABLEOBJECT_T3635579074_H
#define SCRIPTABLEOBJECT_T3635579074_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ScriptableObject
struct  ScriptableObject_t3635579074  : public Object_t1502412432
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_t3635579074_marshaled_pinvoke : public Object_t1502412432_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_t3635579074_marshaled_com : public Object_t1502412432_marshaled_com
{
};
#endif // SCRIPTABLEOBJECT_T3635579074_H
#ifndef IMAGETARGETIMPL_T1523268301_H
#define IMAGETARGETIMPL_T1523268301_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.ImageTargetImpl
struct  ImageTargetImpl_t1523268301  : public ObjectTargetImpl_t1592445137
{
public:
	// Vuforia.ImageTargetType Vuforia.ImageTargetImpl::mImageTargetType
	int32_t ___mImageTargetType_4;
	// System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VirtualButton> Vuforia.ImageTargetImpl::mVirtualButtons
	Dictionary_2_t567977579 * ___mVirtualButtons_5;

public:
	inline static int32_t get_offset_of_mImageTargetType_4() { return static_cast<int32_t>(offsetof(ImageTargetImpl_t1523268301, ___mImageTargetType_4)); }
	inline int32_t get_mImageTargetType_4() const { return ___mImageTargetType_4; }
	inline int32_t* get_address_of_mImageTargetType_4() { return &___mImageTargetType_4; }
	inline void set_mImageTargetType_4(int32_t value)
	{
		___mImageTargetType_4 = value;
	}

	inline static int32_t get_offset_of_mVirtualButtons_5() { return static_cast<int32_t>(offsetof(ImageTargetImpl_t1523268301, ___mVirtualButtons_5)); }
	inline Dictionary_2_t567977579 * get_mVirtualButtons_5() const { return ___mVirtualButtons_5; }
	inline Dictionary_2_t567977579 ** get_address_of_mVirtualButtons_5() { return &___mVirtualButtons_5; }
	inline void set_mVirtualButtons_5(Dictionary_2_t567977579 * value)
	{
		___mVirtualButtons_5 = value;
		Il2CppCodeGenWriteBarrier((&___mVirtualButtons_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IMAGETARGETIMPL_T1523268301_H
#ifndef MULTITARGETIMPL_T109769909_H
#define MULTITARGETIMPL_T109769909_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.MultiTargetImpl
struct  MultiTargetImpl_t109769909  : public ObjectTargetImpl_t1592445137
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MULTITARGETIMPL_T109769909_H
#ifndef VUFORIAMANAGERIMPL_T627862524_H
#define VUFORIAMANAGERIMPL_T627862524_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.VuforiaManagerImpl
struct  VuforiaManagerImpl_t627862524  : public VuforiaManager_t745032010
{
public:
	// System.Boolean Vuforia.VuforiaManagerImpl::<VideoBackgroundTextureSet>k__BackingField
	bool ___U3CVideoBackgroundTextureSetU3Ek__BackingField_1;
	// Vuforia.VuforiaARController/WorldCenterMode Vuforia.VuforiaManagerImpl::mWorldCenterMode
	int32_t ___mWorldCenterMode_2;
	// Vuforia.WorldCenterTrackableBehaviour Vuforia.VuforiaManagerImpl::mWorldCenter
	RuntimeObject* ___mWorldCenter_3;
	// Vuforia.VuMarkAbstractBehaviour Vuforia.VuforiaManagerImpl::mVuMarkWorldCenter
	VuMarkAbstractBehaviour_t2652615854 * ___mVuMarkWorldCenter_4;
	// UnityEngine.Transform Vuforia.VuforiaManagerImpl::mARCameraTransform
	Transform_t3316442598 * ___mARCameraTransform_5;
	// UnityEngine.Transform Vuforia.VuforiaManagerImpl::mCentralAnchorPoint
	Transform_t3316442598 * ___mCentralAnchorPoint_6;
	// UnityEngine.Transform Vuforia.VuforiaManagerImpl::mParentAnchorPoint
	Transform_t3316442598 * ___mParentAnchorPoint_7;
	// Vuforia.VuforiaManagerImpl/TrackableResultData[] Vuforia.VuforiaManagerImpl::mTrackableResultDataArray
	TrackableResultDataU5BU5D_t314934480* ___mTrackableResultDataArray_8;
	// Vuforia.VuforiaManagerImpl/WordData[] Vuforia.VuforiaManagerImpl::mWordDataArray
	WordDataU5BU5D_t2808570108* ___mWordDataArray_9;
	// Vuforia.VuforiaManagerImpl/WordResultData[] Vuforia.VuforiaManagerImpl::mWordResultDataArray
	WordResultDataU5BU5D_t1055029336* ___mWordResultDataArray_10;
	// Vuforia.VuforiaManagerImpl/VuMarkTargetData[] Vuforia.VuforiaManagerImpl::mVuMarkDataArray
	VuMarkTargetDataU5BU5D_t2525391479* ___mVuMarkDataArray_11;
	// Vuforia.VuforiaManagerImpl/VuMarkTargetResultData[] Vuforia.VuforiaManagerImpl::mVuMarkResultDataArray
	VuMarkTargetResultDataU5BU5D_t752671397* ___mVuMarkResultDataArray_12;
	// System.Collections.Generic.LinkedList`1<Vuforia.VuforiaManager/TrackableIdPair> Vuforia.VuforiaManagerImpl::mTrackableFoundQueue
	LinkedList_1_t3507684622 * ___mTrackableFoundQueue_13;
	// System.IntPtr Vuforia.VuforiaManagerImpl::mImageHeaderData
	IntPtr_t ___mImageHeaderData_14;
	// System.Int32 Vuforia.VuforiaManagerImpl::mNumImageHeaders
	int32_t ___mNumImageHeaders_15;
	// System.Int32 Vuforia.VuforiaManagerImpl::mInjectedFrameIdx
	int32_t ___mInjectedFrameIdx_16;
	// System.IntPtr Vuforia.VuforiaManagerImpl::mLastProcessedFrameStatePtr
	IntPtr_t ___mLastProcessedFrameStatePtr_17;
	// System.Boolean Vuforia.VuforiaManagerImpl::mInitialized
	bool ___mInitialized_18;
	// System.Boolean Vuforia.VuforiaManagerImpl::mPaused
	bool ___mPaused_19;
	// Vuforia.VuforiaManagerImpl/FrameState Vuforia.VuforiaManagerImpl::mFrameState
	FrameState_t3340962930  ___mFrameState_20;
	// Vuforia.VuforiaManagerImpl/AutoRotationState Vuforia.VuforiaManagerImpl::mAutoRotationState
	AutoRotationState_t545146858  ___mAutoRotationState_21;
	// System.Boolean Vuforia.VuforiaManagerImpl::mVideoBackgroundNeedsRedrawing
	bool ___mVideoBackgroundNeedsRedrawing_22;
	// System.Int32 Vuforia.VuforiaManagerImpl::mDiscardStatesForRendering
	int32_t ___mDiscardStatesForRendering_23;
	// System.Int32 Vuforia.VuforiaManagerImpl::mLastFrameIdx
	int32_t ___mLastFrameIdx_24;
	// System.Boolean Vuforia.VuforiaManagerImpl::mIsSeeThroughDevice
	bool ___mIsSeeThroughDevice_25;

public:
	inline static int32_t get_offset_of_U3CVideoBackgroundTextureSetU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(VuforiaManagerImpl_t627862524, ___U3CVideoBackgroundTextureSetU3Ek__BackingField_1)); }
	inline bool get_U3CVideoBackgroundTextureSetU3Ek__BackingField_1() const { return ___U3CVideoBackgroundTextureSetU3Ek__BackingField_1; }
	inline bool* get_address_of_U3CVideoBackgroundTextureSetU3Ek__BackingField_1() { return &___U3CVideoBackgroundTextureSetU3Ek__BackingField_1; }
	inline void set_U3CVideoBackgroundTextureSetU3Ek__BackingField_1(bool value)
	{
		___U3CVideoBackgroundTextureSetU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_mWorldCenterMode_2() { return static_cast<int32_t>(offsetof(VuforiaManagerImpl_t627862524, ___mWorldCenterMode_2)); }
	inline int32_t get_mWorldCenterMode_2() const { return ___mWorldCenterMode_2; }
	inline int32_t* get_address_of_mWorldCenterMode_2() { return &___mWorldCenterMode_2; }
	inline void set_mWorldCenterMode_2(int32_t value)
	{
		___mWorldCenterMode_2 = value;
	}

	inline static int32_t get_offset_of_mWorldCenter_3() { return static_cast<int32_t>(offsetof(VuforiaManagerImpl_t627862524, ___mWorldCenter_3)); }
	inline RuntimeObject* get_mWorldCenter_3() const { return ___mWorldCenter_3; }
	inline RuntimeObject** get_address_of_mWorldCenter_3() { return &___mWorldCenter_3; }
	inline void set_mWorldCenter_3(RuntimeObject* value)
	{
		___mWorldCenter_3 = value;
		Il2CppCodeGenWriteBarrier((&___mWorldCenter_3), value);
	}

	inline static int32_t get_offset_of_mVuMarkWorldCenter_4() { return static_cast<int32_t>(offsetof(VuforiaManagerImpl_t627862524, ___mVuMarkWorldCenter_4)); }
	inline VuMarkAbstractBehaviour_t2652615854 * get_mVuMarkWorldCenter_4() const { return ___mVuMarkWorldCenter_4; }
	inline VuMarkAbstractBehaviour_t2652615854 ** get_address_of_mVuMarkWorldCenter_4() { return &___mVuMarkWorldCenter_4; }
	inline void set_mVuMarkWorldCenter_4(VuMarkAbstractBehaviour_t2652615854 * value)
	{
		___mVuMarkWorldCenter_4 = value;
		Il2CppCodeGenWriteBarrier((&___mVuMarkWorldCenter_4), value);
	}

	inline static int32_t get_offset_of_mARCameraTransform_5() { return static_cast<int32_t>(offsetof(VuforiaManagerImpl_t627862524, ___mARCameraTransform_5)); }
	inline Transform_t3316442598 * get_mARCameraTransform_5() const { return ___mARCameraTransform_5; }
	inline Transform_t3316442598 ** get_address_of_mARCameraTransform_5() { return &___mARCameraTransform_5; }
	inline void set_mARCameraTransform_5(Transform_t3316442598 * value)
	{
		___mARCameraTransform_5 = value;
		Il2CppCodeGenWriteBarrier((&___mARCameraTransform_5), value);
	}

	inline static int32_t get_offset_of_mCentralAnchorPoint_6() { return static_cast<int32_t>(offsetof(VuforiaManagerImpl_t627862524, ___mCentralAnchorPoint_6)); }
	inline Transform_t3316442598 * get_mCentralAnchorPoint_6() const { return ___mCentralAnchorPoint_6; }
	inline Transform_t3316442598 ** get_address_of_mCentralAnchorPoint_6() { return &___mCentralAnchorPoint_6; }
	inline void set_mCentralAnchorPoint_6(Transform_t3316442598 * value)
	{
		___mCentralAnchorPoint_6 = value;
		Il2CppCodeGenWriteBarrier((&___mCentralAnchorPoint_6), value);
	}

	inline static int32_t get_offset_of_mParentAnchorPoint_7() { return static_cast<int32_t>(offsetof(VuforiaManagerImpl_t627862524, ___mParentAnchorPoint_7)); }
	inline Transform_t3316442598 * get_mParentAnchorPoint_7() const { return ___mParentAnchorPoint_7; }
	inline Transform_t3316442598 ** get_address_of_mParentAnchorPoint_7() { return &___mParentAnchorPoint_7; }
	inline void set_mParentAnchorPoint_7(Transform_t3316442598 * value)
	{
		___mParentAnchorPoint_7 = value;
		Il2CppCodeGenWriteBarrier((&___mParentAnchorPoint_7), value);
	}

	inline static int32_t get_offset_of_mTrackableResultDataArray_8() { return static_cast<int32_t>(offsetof(VuforiaManagerImpl_t627862524, ___mTrackableResultDataArray_8)); }
	inline TrackableResultDataU5BU5D_t314934480* get_mTrackableResultDataArray_8() const { return ___mTrackableResultDataArray_8; }
	inline TrackableResultDataU5BU5D_t314934480** get_address_of_mTrackableResultDataArray_8() { return &___mTrackableResultDataArray_8; }
	inline void set_mTrackableResultDataArray_8(TrackableResultDataU5BU5D_t314934480* value)
	{
		___mTrackableResultDataArray_8 = value;
		Il2CppCodeGenWriteBarrier((&___mTrackableResultDataArray_8), value);
	}

	inline static int32_t get_offset_of_mWordDataArray_9() { return static_cast<int32_t>(offsetof(VuforiaManagerImpl_t627862524, ___mWordDataArray_9)); }
	inline WordDataU5BU5D_t2808570108* get_mWordDataArray_9() const { return ___mWordDataArray_9; }
	inline WordDataU5BU5D_t2808570108** get_address_of_mWordDataArray_9() { return &___mWordDataArray_9; }
	inline void set_mWordDataArray_9(WordDataU5BU5D_t2808570108* value)
	{
		___mWordDataArray_9 = value;
		Il2CppCodeGenWriteBarrier((&___mWordDataArray_9), value);
	}

	inline static int32_t get_offset_of_mWordResultDataArray_10() { return static_cast<int32_t>(offsetof(VuforiaManagerImpl_t627862524, ___mWordResultDataArray_10)); }
	inline WordResultDataU5BU5D_t1055029336* get_mWordResultDataArray_10() const { return ___mWordResultDataArray_10; }
	inline WordResultDataU5BU5D_t1055029336** get_address_of_mWordResultDataArray_10() { return &___mWordResultDataArray_10; }
	inline void set_mWordResultDataArray_10(WordResultDataU5BU5D_t1055029336* value)
	{
		___mWordResultDataArray_10 = value;
		Il2CppCodeGenWriteBarrier((&___mWordResultDataArray_10), value);
	}

	inline static int32_t get_offset_of_mVuMarkDataArray_11() { return static_cast<int32_t>(offsetof(VuforiaManagerImpl_t627862524, ___mVuMarkDataArray_11)); }
	inline VuMarkTargetDataU5BU5D_t2525391479* get_mVuMarkDataArray_11() const { return ___mVuMarkDataArray_11; }
	inline VuMarkTargetDataU5BU5D_t2525391479** get_address_of_mVuMarkDataArray_11() { return &___mVuMarkDataArray_11; }
	inline void set_mVuMarkDataArray_11(VuMarkTargetDataU5BU5D_t2525391479* value)
	{
		___mVuMarkDataArray_11 = value;
		Il2CppCodeGenWriteBarrier((&___mVuMarkDataArray_11), value);
	}

	inline static int32_t get_offset_of_mVuMarkResultDataArray_12() { return static_cast<int32_t>(offsetof(VuforiaManagerImpl_t627862524, ___mVuMarkResultDataArray_12)); }
	inline VuMarkTargetResultDataU5BU5D_t752671397* get_mVuMarkResultDataArray_12() const { return ___mVuMarkResultDataArray_12; }
	inline VuMarkTargetResultDataU5BU5D_t752671397** get_address_of_mVuMarkResultDataArray_12() { return &___mVuMarkResultDataArray_12; }
	inline void set_mVuMarkResultDataArray_12(VuMarkTargetResultDataU5BU5D_t752671397* value)
	{
		___mVuMarkResultDataArray_12 = value;
		Il2CppCodeGenWriteBarrier((&___mVuMarkResultDataArray_12), value);
	}

	inline static int32_t get_offset_of_mTrackableFoundQueue_13() { return static_cast<int32_t>(offsetof(VuforiaManagerImpl_t627862524, ___mTrackableFoundQueue_13)); }
	inline LinkedList_1_t3507684622 * get_mTrackableFoundQueue_13() const { return ___mTrackableFoundQueue_13; }
	inline LinkedList_1_t3507684622 ** get_address_of_mTrackableFoundQueue_13() { return &___mTrackableFoundQueue_13; }
	inline void set_mTrackableFoundQueue_13(LinkedList_1_t3507684622 * value)
	{
		___mTrackableFoundQueue_13 = value;
		Il2CppCodeGenWriteBarrier((&___mTrackableFoundQueue_13), value);
	}

	inline static int32_t get_offset_of_mImageHeaderData_14() { return static_cast<int32_t>(offsetof(VuforiaManagerImpl_t627862524, ___mImageHeaderData_14)); }
	inline IntPtr_t get_mImageHeaderData_14() const { return ___mImageHeaderData_14; }
	inline IntPtr_t* get_address_of_mImageHeaderData_14() { return &___mImageHeaderData_14; }
	inline void set_mImageHeaderData_14(IntPtr_t value)
	{
		___mImageHeaderData_14 = value;
	}

	inline static int32_t get_offset_of_mNumImageHeaders_15() { return static_cast<int32_t>(offsetof(VuforiaManagerImpl_t627862524, ___mNumImageHeaders_15)); }
	inline int32_t get_mNumImageHeaders_15() const { return ___mNumImageHeaders_15; }
	inline int32_t* get_address_of_mNumImageHeaders_15() { return &___mNumImageHeaders_15; }
	inline void set_mNumImageHeaders_15(int32_t value)
	{
		___mNumImageHeaders_15 = value;
	}

	inline static int32_t get_offset_of_mInjectedFrameIdx_16() { return static_cast<int32_t>(offsetof(VuforiaManagerImpl_t627862524, ___mInjectedFrameIdx_16)); }
	inline int32_t get_mInjectedFrameIdx_16() const { return ___mInjectedFrameIdx_16; }
	inline int32_t* get_address_of_mInjectedFrameIdx_16() { return &___mInjectedFrameIdx_16; }
	inline void set_mInjectedFrameIdx_16(int32_t value)
	{
		___mInjectedFrameIdx_16 = value;
	}

	inline static int32_t get_offset_of_mLastProcessedFrameStatePtr_17() { return static_cast<int32_t>(offsetof(VuforiaManagerImpl_t627862524, ___mLastProcessedFrameStatePtr_17)); }
	inline IntPtr_t get_mLastProcessedFrameStatePtr_17() const { return ___mLastProcessedFrameStatePtr_17; }
	inline IntPtr_t* get_address_of_mLastProcessedFrameStatePtr_17() { return &___mLastProcessedFrameStatePtr_17; }
	inline void set_mLastProcessedFrameStatePtr_17(IntPtr_t value)
	{
		___mLastProcessedFrameStatePtr_17 = value;
	}

	inline static int32_t get_offset_of_mInitialized_18() { return static_cast<int32_t>(offsetof(VuforiaManagerImpl_t627862524, ___mInitialized_18)); }
	inline bool get_mInitialized_18() const { return ___mInitialized_18; }
	inline bool* get_address_of_mInitialized_18() { return &___mInitialized_18; }
	inline void set_mInitialized_18(bool value)
	{
		___mInitialized_18 = value;
	}

	inline static int32_t get_offset_of_mPaused_19() { return static_cast<int32_t>(offsetof(VuforiaManagerImpl_t627862524, ___mPaused_19)); }
	inline bool get_mPaused_19() const { return ___mPaused_19; }
	inline bool* get_address_of_mPaused_19() { return &___mPaused_19; }
	inline void set_mPaused_19(bool value)
	{
		___mPaused_19 = value;
	}

	inline static int32_t get_offset_of_mFrameState_20() { return static_cast<int32_t>(offsetof(VuforiaManagerImpl_t627862524, ___mFrameState_20)); }
	inline FrameState_t3340962930  get_mFrameState_20() const { return ___mFrameState_20; }
	inline FrameState_t3340962930 * get_address_of_mFrameState_20() { return &___mFrameState_20; }
	inline void set_mFrameState_20(FrameState_t3340962930  value)
	{
		___mFrameState_20 = value;
	}

	inline static int32_t get_offset_of_mAutoRotationState_21() { return static_cast<int32_t>(offsetof(VuforiaManagerImpl_t627862524, ___mAutoRotationState_21)); }
	inline AutoRotationState_t545146858  get_mAutoRotationState_21() const { return ___mAutoRotationState_21; }
	inline AutoRotationState_t545146858 * get_address_of_mAutoRotationState_21() { return &___mAutoRotationState_21; }
	inline void set_mAutoRotationState_21(AutoRotationState_t545146858  value)
	{
		___mAutoRotationState_21 = value;
	}

	inline static int32_t get_offset_of_mVideoBackgroundNeedsRedrawing_22() { return static_cast<int32_t>(offsetof(VuforiaManagerImpl_t627862524, ___mVideoBackgroundNeedsRedrawing_22)); }
	inline bool get_mVideoBackgroundNeedsRedrawing_22() const { return ___mVideoBackgroundNeedsRedrawing_22; }
	inline bool* get_address_of_mVideoBackgroundNeedsRedrawing_22() { return &___mVideoBackgroundNeedsRedrawing_22; }
	inline void set_mVideoBackgroundNeedsRedrawing_22(bool value)
	{
		___mVideoBackgroundNeedsRedrawing_22 = value;
	}

	inline static int32_t get_offset_of_mDiscardStatesForRendering_23() { return static_cast<int32_t>(offsetof(VuforiaManagerImpl_t627862524, ___mDiscardStatesForRendering_23)); }
	inline int32_t get_mDiscardStatesForRendering_23() const { return ___mDiscardStatesForRendering_23; }
	inline int32_t* get_address_of_mDiscardStatesForRendering_23() { return &___mDiscardStatesForRendering_23; }
	inline void set_mDiscardStatesForRendering_23(int32_t value)
	{
		___mDiscardStatesForRendering_23 = value;
	}

	inline static int32_t get_offset_of_mLastFrameIdx_24() { return static_cast<int32_t>(offsetof(VuforiaManagerImpl_t627862524, ___mLastFrameIdx_24)); }
	inline int32_t get_mLastFrameIdx_24() const { return ___mLastFrameIdx_24; }
	inline int32_t* get_address_of_mLastFrameIdx_24() { return &___mLastFrameIdx_24; }
	inline void set_mLastFrameIdx_24(int32_t value)
	{
		___mLastFrameIdx_24 = value;
	}

	inline static int32_t get_offset_of_mIsSeeThroughDevice_25() { return static_cast<int32_t>(offsetof(VuforiaManagerImpl_t627862524, ___mIsSeeThroughDevice_25)); }
	inline bool get_mIsSeeThroughDevice_25() const { return ___mIsSeeThroughDevice_25; }
	inline bool* get_address_of_mIsSeeThroughDevice_25() { return &___mIsSeeThroughDevice_25; }
	inline void set_mIsSeeThroughDevice_25(bool value)
	{
		___mIsSeeThroughDevice_25 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VUFORIAMANAGERIMPL_T627862524_H
#ifndef DEVICETRACKERCONFIGURATION_T2458088029_H
#define DEVICETRACKERCONFIGURATION_T2458088029_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.VuforiaAbstractConfiguration/DeviceTrackerConfiguration
struct  DeviceTrackerConfiguration_t2458088029  : public TrackerConfiguration_t3022484256
{
public:
	// System.Boolean Vuforia.VuforiaAbstractConfiguration/DeviceTrackerConfiguration::posePrediction
	bool ___posePrediction_2;
	// Vuforia.RotationalDeviceTracker/MODEL_CORRECTION_MODE Vuforia.VuforiaAbstractConfiguration/DeviceTrackerConfiguration::modelCorrectionMode
	int32_t ___modelCorrectionMode_3;
	// System.Boolean Vuforia.VuforiaAbstractConfiguration/DeviceTrackerConfiguration::modelTransformEnabled
	bool ___modelTransformEnabled_4;
	// UnityEngine.Vector3 Vuforia.VuforiaAbstractConfiguration/DeviceTrackerConfiguration::modelTransform
	Vector3_t2903530434  ___modelTransform_5;

public:
	inline static int32_t get_offset_of_posePrediction_2() { return static_cast<int32_t>(offsetof(DeviceTrackerConfiguration_t2458088029, ___posePrediction_2)); }
	inline bool get_posePrediction_2() const { return ___posePrediction_2; }
	inline bool* get_address_of_posePrediction_2() { return &___posePrediction_2; }
	inline void set_posePrediction_2(bool value)
	{
		___posePrediction_2 = value;
	}

	inline static int32_t get_offset_of_modelCorrectionMode_3() { return static_cast<int32_t>(offsetof(DeviceTrackerConfiguration_t2458088029, ___modelCorrectionMode_3)); }
	inline int32_t get_modelCorrectionMode_3() const { return ___modelCorrectionMode_3; }
	inline int32_t* get_address_of_modelCorrectionMode_3() { return &___modelCorrectionMode_3; }
	inline void set_modelCorrectionMode_3(int32_t value)
	{
		___modelCorrectionMode_3 = value;
	}

	inline static int32_t get_offset_of_modelTransformEnabled_4() { return static_cast<int32_t>(offsetof(DeviceTrackerConfiguration_t2458088029, ___modelTransformEnabled_4)); }
	inline bool get_modelTransformEnabled_4() const { return ___modelTransformEnabled_4; }
	inline bool* get_address_of_modelTransformEnabled_4() { return &___modelTransformEnabled_4; }
	inline void set_modelTransformEnabled_4(bool value)
	{
		___modelTransformEnabled_4 = value;
	}

	inline static int32_t get_offset_of_modelTransform_5() { return static_cast<int32_t>(offsetof(DeviceTrackerConfiguration_t2458088029, ___modelTransform_5)); }
	inline Vector3_t2903530434  get_modelTransform_5() const { return ___modelTransform_5; }
	inline Vector3_t2903530434 * get_address_of_modelTransform_5() { return &___modelTransform_5; }
	inline void set_modelTransform_5(Vector3_t2903530434  value)
	{
		___modelTransform_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEVICETRACKERCONFIGURATION_T2458088029_H
#ifndef WORDRESULTDATA_T3823211317_H
#define WORDRESULTDATA_T3823211317_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.VuforiaManagerImpl/WordResultData
#pragma pack(push, tp, 1)
struct  WordResultData_t3823211317 
{
public:
	// Vuforia.VuforiaManagerImpl/PoseData Vuforia.VuforiaManagerImpl/WordResultData::pose
	PoseData_t1816720573  ___pose_0;
	// System.Double Vuforia.VuforiaManagerImpl/WordResultData::timeStamp
	double ___timeStamp_1;
	// System.Int32 Vuforia.VuforiaManagerImpl/WordResultData::statusInteger
	int32_t ___statusInteger_2;
	// System.Int32 Vuforia.VuforiaManagerImpl/WordResultData::id
	int32_t ___id_3;
	// Vuforia.VuforiaManagerImpl/Obb2D Vuforia.VuforiaManagerImpl/WordResultData::orientedBoundingBox
	Obb2D_t1682428419  ___orientedBoundingBox_4;

public:
	inline static int32_t get_offset_of_pose_0() { return static_cast<int32_t>(offsetof(WordResultData_t3823211317, ___pose_0)); }
	inline PoseData_t1816720573  get_pose_0() const { return ___pose_0; }
	inline PoseData_t1816720573 * get_address_of_pose_0() { return &___pose_0; }
	inline void set_pose_0(PoseData_t1816720573  value)
	{
		___pose_0 = value;
	}

	inline static int32_t get_offset_of_timeStamp_1() { return static_cast<int32_t>(offsetof(WordResultData_t3823211317, ___timeStamp_1)); }
	inline double get_timeStamp_1() const { return ___timeStamp_1; }
	inline double* get_address_of_timeStamp_1() { return &___timeStamp_1; }
	inline void set_timeStamp_1(double value)
	{
		___timeStamp_1 = value;
	}

	inline static int32_t get_offset_of_statusInteger_2() { return static_cast<int32_t>(offsetof(WordResultData_t3823211317, ___statusInteger_2)); }
	inline int32_t get_statusInteger_2() const { return ___statusInteger_2; }
	inline int32_t* get_address_of_statusInteger_2() { return &___statusInteger_2; }
	inline void set_statusInteger_2(int32_t value)
	{
		___statusInteger_2 = value;
	}

	inline static int32_t get_offset_of_id_3() { return static_cast<int32_t>(offsetof(WordResultData_t3823211317, ___id_3)); }
	inline int32_t get_id_3() const { return ___id_3; }
	inline int32_t* get_address_of_id_3() { return &___id_3; }
	inline void set_id_3(int32_t value)
	{
		___id_3 = value;
	}

	inline static int32_t get_offset_of_orientedBoundingBox_4() { return static_cast<int32_t>(offsetof(WordResultData_t3823211317, ___orientedBoundingBox_4)); }
	inline Obb2D_t1682428419  get_orientedBoundingBox_4() const { return ___orientedBoundingBox_4; }
	inline Obb2D_t1682428419 * get_address_of_orientedBoundingBox_4() { return &___orientedBoundingBox_4; }
	inline void set_orientedBoundingBox_4(Obb2D_t1682428419  value)
	{
		___orientedBoundingBox_4 = value;
	}
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WORDRESULTDATA_T3823211317_H
#ifndef TRACKABLERESULTDATA_T4095399453_H
#define TRACKABLERESULTDATA_T4095399453_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.VuforiaManagerImpl/TrackableResultData
#pragma pack(push, tp, 1)
struct  TrackableResultData_t4095399453 
{
public:
	// Vuforia.VuforiaManagerImpl/PoseData Vuforia.VuforiaManagerImpl/TrackableResultData::pose
	PoseData_t1816720573  ___pose_0;
	// System.Double Vuforia.VuforiaManagerImpl/TrackableResultData::timeStamp
	double ___timeStamp_1;
	// System.Int32 Vuforia.VuforiaManagerImpl/TrackableResultData::statusInteger
	int32_t ___statusInteger_2;
	// System.Int32 Vuforia.VuforiaManagerImpl/TrackableResultData::id
	int32_t ___id_3;

public:
	inline static int32_t get_offset_of_pose_0() { return static_cast<int32_t>(offsetof(TrackableResultData_t4095399453, ___pose_0)); }
	inline PoseData_t1816720573  get_pose_0() const { return ___pose_0; }
	inline PoseData_t1816720573 * get_address_of_pose_0() { return &___pose_0; }
	inline void set_pose_0(PoseData_t1816720573  value)
	{
		___pose_0 = value;
	}

	inline static int32_t get_offset_of_timeStamp_1() { return static_cast<int32_t>(offsetof(TrackableResultData_t4095399453, ___timeStamp_1)); }
	inline double get_timeStamp_1() const { return ___timeStamp_1; }
	inline double* get_address_of_timeStamp_1() { return &___timeStamp_1; }
	inline void set_timeStamp_1(double value)
	{
		___timeStamp_1 = value;
	}

	inline static int32_t get_offset_of_statusInteger_2() { return static_cast<int32_t>(offsetof(TrackableResultData_t4095399453, ___statusInteger_2)); }
	inline int32_t get_statusInteger_2() const { return ___statusInteger_2; }
	inline int32_t* get_address_of_statusInteger_2() { return &___statusInteger_2; }
	inline void set_statusInteger_2(int32_t value)
	{
		___statusInteger_2 = value;
	}

	inline static int32_t get_offset_of_id_3() { return static_cast<int32_t>(offsetof(TrackableResultData_t4095399453, ___id_3)); }
	inline int32_t get_id_3() const { return ___id_3; }
	inline int32_t* get_address_of_id_3() { return &___id_3; }
	inline void set_id_3(int32_t value)
	{
		___id_3 = value;
	}
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRACKABLERESULTDATA_T4095399453_H
#ifndef SURFACEDATA_T2491312487_H
#define SURFACEDATA_T2491312487_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.VuforiaManagerImpl/SurfaceData
#pragma pack(push, tp, 1)
struct  SurfaceData_t2491312487 
{
public:
	// System.IntPtr Vuforia.VuforiaManagerImpl/SurfaceData::meshBoundaryArray
	IntPtr_t ___meshBoundaryArray_0;
	// Vuforia.VuforiaManagerImpl/MeshData Vuforia.VuforiaManagerImpl/SurfaceData::meshData
	MeshData_t939646676  ___meshData_1;
	// Vuforia.VuforiaManagerImpl/MeshData Vuforia.VuforiaManagerImpl/SurfaceData::navMeshData
	MeshData_t939646676  ___navMeshData_2;
	// Vuforia.RectangleData Vuforia.VuforiaManagerImpl/SurfaceData::boundingBox
	RectangleData_t1094133618  ___boundingBox_3;
	// Vuforia.VuforiaManagerImpl/PoseData Vuforia.VuforiaManagerImpl/SurfaceData::localPose
	PoseData_t1816720573  ___localPose_4;
	// System.Int32 Vuforia.VuforiaManagerImpl/SurfaceData::id
	int32_t ___id_5;
	// System.Int32 Vuforia.VuforiaManagerImpl/SurfaceData::parentID
	int32_t ___parentID_6;
	// System.Int32 Vuforia.VuforiaManagerImpl/SurfaceData::numBoundaryIndices
	int32_t ___numBoundaryIndices_7;
	// System.Int32 Vuforia.VuforiaManagerImpl/SurfaceData::revision
	int32_t ___revision_8;

public:
	inline static int32_t get_offset_of_meshBoundaryArray_0() { return static_cast<int32_t>(offsetof(SurfaceData_t2491312487, ___meshBoundaryArray_0)); }
	inline IntPtr_t get_meshBoundaryArray_0() const { return ___meshBoundaryArray_0; }
	inline IntPtr_t* get_address_of_meshBoundaryArray_0() { return &___meshBoundaryArray_0; }
	inline void set_meshBoundaryArray_0(IntPtr_t value)
	{
		___meshBoundaryArray_0 = value;
	}

	inline static int32_t get_offset_of_meshData_1() { return static_cast<int32_t>(offsetof(SurfaceData_t2491312487, ___meshData_1)); }
	inline MeshData_t939646676  get_meshData_1() const { return ___meshData_1; }
	inline MeshData_t939646676 * get_address_of_meshData_1() { return &___meshData_1; }
	inline void set_meshData_1(MeshData_t939646676  value)
	{
		___meshData_1 = value;
	}

	inline static int32_t get_offset_of_navMeshData_2() { return static_cast<int32_t>(offsetof(SurfaceData_t2491312487, ___navMeshData_2)); }
	inline MeshData_t939646676  get_navMeshData_2() const { return ___navMeshData_2; }
	inline MeshData_t939646676 * get_address_of_navMeshData_2() { return &___navMeshData_2; }
	inline void set_navMeshData_2(MeshData_t939646676  value)
	{
		___navMeshData_2 = value;
	}

	inline static int32_t get_offset_of_boundingBox_3() { return static_cast<int32_t>(offsetof(SurfaceData_t2491312487, ___boundingBox_3)); }
	inline RectangleData_t1094133618  get_boundingBox_3() const { return ___boundingBox_3; }
	inline RectangleData_t1094133618 * get_address_of_boundingBox_3() { return &___boundingBox_3; }
	inline void set_boundingBox_3(RectangleData_t1094133618  value)
	{
		___boundingBox_3 = value;
	}

	inline static int32_t get_offset_of_localPose_4() { return static_cast<int32_t>(offsetof(SurfaceData_t2491312487, ___localPose_4)); }
	inline PoseData_t1816720573  get_localPose_4() const { return ___localPose_4; }
	inline PoseData_t1816720573 * get_address_of_localPose_4() { return &___localPose_4; }
	inline void set_localPose_4(PoseData_t1816720573  value)
	{
		___localPose_4 = value;
	}

	inline static int32_t get_offset_of_id_5() { return static_cast<int32_t>(offsetof(SurfaceData_t2491312487, ___id_5)); }
	inline int32_t get_id_5() const { return ___id_5; }
	inline int32_t* get_address_of_id_5() { return &___id_5; }
	inline void set_id_5(int32_t value)
	{
		___id_5 = value;
	}

	inline static int32_t get_offset_of_parentID_6() { return static_cast<int32_t>(offsetof(SurfaceData_t2491312487, ___parentID_6)); }
	inline int32_t get_parentID_6() const { return ___parentID_6; }
	inline int32_t* get_address_of_parentID_6() { return &___parentID_6; }
	inline void set_parentID_6(int32_t value)
	{
		___parentID_6 = value;
	}

	inline static int32_t get_offset_of_numBoundaryIndices_7() { return static_cast<int32_t>(offsetof(SurfaceData_t2491312487, ___numBoundaryIndices_7)); }
	inline int32_t get_numBoundaryIndices_7() const { return ___numBoundaryIndices_7; }
	inline int32_t* get_address_of_numBoundaryIndices_7() { return &___numBoundaryIndices_7; }
	inline void set_numBoundaryIndices_7(int32_t value)
	{
		___numBoundaryIndices_7 = value;
	}

	inline static int32_t get_offset_of_revision_8() { return static_cast<int32_t>(offsetof(SurfaceData_t2491312487, ___revision_8)); }
	inline int32_t get_revision_8() const { return ___revision_8; }
	inline int32_t* get_address_of_revision_8() { return &___revision_8; }
	inline void set_revision_8(int32_t value)
	{
		___revision_8 = value;
	}
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SURFACEDATA_T2491312487_H
#ifndef PROPDATA_T948013865_H
#define PROPDATA_T948013865_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.VuforiaManagerImpl/PropData
#pragma pack(push, tp, 1)
struct  PropData_t948013865 
{
public:
	// Vuforia.VuforiaManagerImpl/MeshData Vuforia.VuforiaManagerImpl/PropData::meshData
	MeshData_t939646676  ___meshData_0;
	// System.Int32 Vuforia.VuforiaManagerImpl/PropData::id
	int32_t ___id_1;
	// System.Int32 Vuforia.VuforiaManagerImpl/PropData::parentID
	int32_t ___parentID_2;
	// Vuforia.VuforiaManagerImpl/Obb3D Vuforia.VuforiaManagerImpl/PropData::boundingBox
	Obb3D_t1746725078  ___boundingBox_3;
	// UnityEngine.Vector2 Vuforia.VuforiaManagerImpl/PropData::localPosition
	Vector2_t3577333262  ___localPosition_4;
	// Vuforia.VuforiaManagerImpl/PoseData Vuforia.VuforiaManagerImpl/PropData::localPose
	PoseData_t1816720573  ___localPose_5;
	// System.Int32 Vuforia.VuforiaManagerImpl/PropData::revision
	int32_t ___revision_6;
	// System.Int32 Vuforia.VuforiaManagerImpl/PropData::unused
	int32_t ___unused_7;

public:
	inline static int32_t get_offset_of_meshData_0() { return static_cast<int32_t>(offsetof(PropData_t948013865, ___meshData_0)); }
	inline MeshData_t939646676  get_meshData_0() const { return ___meshData_0; }
	inline MeshData_t939646676 * get_address_of_meshData_0() { return &___meshData_0; }
	inline void set_meshData_0(MeshData_t939646676  value)
	{
		___meshData_0 = value;
	}

	inline static int32_t get_offset_of_id_1() { return static_cast<int32_t>(offsetof(PropData_t948013865, ___id_1)); }
	inline int32_t get_id_1() const { return ___id_1; }
	inline int32_t* get_address_of_id_1() { return &___id_1; }
	inline void set_id_1(int32_t value)
	{
		___id_1 = value;
	}

	inline static int32_t get_offset_of_parentID_2() { return static_cast<int32_t>(offsetof(PropData_t948013865, ___parentID_2)); }
	inline int32_t get_parentID_2() const { return ___parentID_2; }
	inline int32_t* get_address_of_parentID_2() { return &___parentID_2; }
	inline void set_parentID_2(int32_t value)
	{
		___parentID_2 = value;
	}

	inline static int32_t get_offset_of_boundingBox_3() { return static_cast<int32_t>(offsetof(PropData_t948013865, ___boundingBox_3)); }
	inline Obb3D_t1746725078  get_boundingBox_3() const { return ___boundingBox_3; }
	inline Obb3D_t1746725078 * get_address_of_boundingBox_3() { return &___boundingBox_3; }
	inline void set_boundingBox_3(Obb3D_t1746725078  value)
	{
		___boundingBox_3 = value;
	}

	inline static int32_t get_offset_of_localPosition_4() { return static_cast<int32_t>(offsetof(PropData_t948013865, ___localPosition_4)); }
	inline Vector2_t3577333262  get_localPosition_4() const { return ___localPosition_4; }
	inline Vector2_t3577333262 * get_address_of_localPosition_4() { return &___localPosition_4; }
	inline void set_localPosition_4(Vector2_t3577333262  value)
	{
		___localPosition_4 = value;
	}

	inline static int32_t get_offset_of_localPose_5() { return static_cast<int32_t>(offsetof(PropData_t948013865, ___localPose_5)); }
	inline PoseData_t1816720573  get_localPose_5() const { return ___localPose_5; }
	inline PoseData_t1816720573 * get_address_of_localPose_5() { return &___localPose_5; }
	inline void set_localPose_5(PoseData_t1816720573  value)
	{
		___localPose_5 = value;
	}

	inline static int32_t get_offset_of_revision_6() { return static_cast<int32_t>(offsetof(PropData_t948013865, ___revision_6)); }
	inline int32_t get_revision_6() const { return ___revision_6; }
	inline int32_t* get_address_of_revision_6() { return &___revision_6; }
	inline void set_revision_6(int32_t value)
	{
		___revision_6 = value;
	}

	inline static int32_t get_offset_of_unused_7() { return static_cast<int32_t>(offsetof(PropData_t948013865, ___unused_7)); }
	inline int32_t get_unused_7() const { return ___unused_7; }
	inline int32_t* get_address_of_unused_7() { return &___unused_7; }
	inline void set_unused_7(int32_t value)
	{
		___unused_7 = value;
	}
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROPDATA_T948013865_H
#ifndef VUMARKTARGETRESULTDATA_T2086971180_H
#define VUMARKTARGETRESULTDATA_T2086971180_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.VuforiaManagerImpl/VuMarkTargetResultData
#pragma pack(push, tp, 1)
struct  VuMarkTargetResultData_t2086971180 
{
public:
	// Vuforia.VuforiaManagerImpl/PoseData Vuforia.VuforiaManagerImpl/VuMarkTargetResultData::pose
	PoseData_t1816720573  ___pose_0;
	// System.Double Vuforia.VuforiaManagerImpl/VuMarkTargetResultData::timeStamp
	double ___timeStamp_1;
	// System.Int32 Vuforia.VuforiaManagerImpl/VuMarkTargetResultData::statusInteger
	int32_t ___statusInteger_2;
	// System.Int32 Vuforia.VuforiaManagerImpl/VuMarkTargetResultData::targetID
	int32_t ___targetID_3;
	// System.Int32 Vuforia.VuforiaManagerImpl/VuMarkTargetResultData::resultID
	int32_t ___resultID_4;
	// System.Int32 Vuforia.VuforiaManagerImpl/VuMarkTargetResultData::unused
	int32_t ___unused_5;

public:
	inline static int32_t get_offset_of_pose_0() { return static_cast<int32_t>(offsetof(VuMarkTargetResultData_t2086971180, ___pose_0)); }
	inline PoseData_t1816720573  get_pose_0() const { return ___pose_0; }
	inline PoseData_t1816720573 * get_address_of_pose_0() { return &___pose_0; }
	inline void set_pose_0(PoseData_t1816720573  value)
	{
		___pose_0 = value;
	}

	inline static int32_t get_offset_of_timeStamp_1() { return static_cast<int32_t>(offsetof(VuMarkTargetResultData_t2086971180, ___timeStamp_1)); }
	inline double get_timeStamp_1() const { return ___timeStamp_1; }
	inline double* get_address_of_timeStamp_1() { return &___timeStamp_1; }
	inline void set_timeStamp_1(double value)
	{
		___timeStamp_1 = value;
	}

	inline static int32_t get_offset_of_statusInteger_2() { return static_cast<int32_t>(offsetof(VuMarkTargetResultData_t2086971180, ___statusInteger_2)); }
	inline int32_t get_statusInteger_2() const { return ___statusInteger_2; }
	inline int32_t* get_address_of_statusInteger_2() { return &___statusInteger_2; }
	inline void set_statusInteger_2(int32_t value)
	{
		___statusInteger_2 = value;
	}

	inline static int32_t get_offset_of_targetID_3() { return static_cast<int32_t>(offsetof(VuMarkTargetResultData_t2086971180, ___targetID_3)); }
	inline int32_t get_targetID_3() const { return ___targetID_3; }
	inline int32_t* get_address_of_targetID_3() { return &___targetID_3; }
	inline void set_targetID_3(int32_t value)
	{
		___targetID_3 = value;
	}

	inline static int32_t get_offset_of_resultID_4() { return static_cast<int32_t>(offsetof(VuMarkTargetResultData_t2086971180, ___resultID_4)); }
	inline int32_t get_resultID_4() const { return ___resultID_4; }
	inline int32_t* get_address_of_resultID_4() { return &___resultID_4; }
	inline void set_resultID_4(int32_t value)
	{
		___resultID_4 = value;
	}

	inline static int32_t get_offset_of_unused_5() { return static_cast<int32_t>(offsetof(VuMarkTargetResultData_t2086971180, ___unused_5)); }
	inline int32_t get_unused_5() const { return ___unused_5; }
	inline int32_t* get_address_of_unused_5() { return &___unused_5; }
	inline void set_unused_5(int32_t value)
	{
		___unused_5 = value;
	}
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VUMARKTARGETRESULTDATA_T2086971180_H
#ifndef VUFORIARENDERERIMPL_T3463553574_H
#define VUFORIARENDERERIMPL_T3463553574_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.VuforiaRendererImpl
struct  VuforiaRendererImpl_t3463553574  : public VuforiaRenderer_t969713742
{
public:
	// Vuforia.VuforiaRenderer/VideoBGCfgData Vuforia.VuforiaRendererImpl::mVideoBGConfig
	VideoBGCfgData_t3234105495  ___mVideoBGConfig_1;
	// System.Boolean Vuforia.VuforiaRendererImpl::mVideoBGConfigSet
	bool ___mVideoBGConfigSet_2;
	// UnityEngine.Texture Vuforia.VuforiaRendererImpl::mVideoBackgroundTexture
	Texture_t85561421 * ___mVideoBackgroundTexture_3;
	// System.Boolean Vuforia.VuforiaRendererImpl::mBackgroundTextureHasChanged
	bool ___mBackgroundTextureHasChanged_4;
	// Vuforia.VuforiaRenderer/VideoBackgroundReflection Vuforia.VuforiaRendererImpl::mLastSetReflection
	int32_t ___mLastSetReflection_5;
	// System.IntPtr Vuforia.VuforiaRendererImpl::mNativeRenderingCallback
	IntPtr_t ___mNativeRenderingCallback_6;

public:
	inline static int32_t get_offset_of_mVideoBGConfig_1() { return static_cast<int32_t>(offsetof(VuforiaRendererImpl_t3463553574, ___mVideoBGConfig_1)); }
	inline VideoBGCfgData_t3234105495  get_mVideoBGConfig_1() const { return ___mVideoBGConfig_1; }
	inline VideoBGCfgData_t3234105495 * get_address_of_mVideoBGConfig_1() { return &___mVideoBGConfig_1; }
	inline void set_mVideoBGConfig_1(VideoBGCfgData_t3234105495  value)
	{
		___mVideoBGConfig_1 = value;
	}

	inline static int32_t get_offset_of_mVideoBGConfigSet_2() { return static_cast<int32_t>(offsetof(VuforiaRendererImpl_t3463553574, ___mVideoBGConfigSet_2)); }
	inline bool get_mVideoBGConfigSet_2() const { return ___mVideoBGConfigSet_2; }
	inline bool* get_address_of_mVideoBGConfigSet_2() { return &___mVideoBGConfigSet_2; }
	inline void set_mVideoBGConfigSet_2(bool value)
	{
		___mVideoBGConfigSet_2 = value;
	}

	inline static int32_t get_offset_of_mVideoBackgroundTexture_3() { return static_cast<int32_t>(offsetof(VuforiaRendererImpl_t3463553574, ___mVideoBackgroundTexture_3)); }
	inline Texture_t85561421 * get_mVideoBackgroundTexture_3() const { return ___mVideoBackgroundTexture_3; }
	inline Texture_t85561421 ** get_address_of_mVideoBackgroundTexture_3() { return &___mVideoBackgroundTexture_3; }
	inline void set_mVideoBackgroundTexture_3(Texture_t85561421 * value)
	{
		___mVideoBackgroundTexture_3 = value;
		Il2CppCodeGenWriteBarrier((&___mVideoBackgroundTexture_3), value);
	}

	inline static int32_t get_offset_of_mBackgroundTextureHasChanged_4() { return static_cast<int32_t>(offsetof(VuforiaRendererImpl_t3463553574, ___mBackgroundTextureHasChanged_4)); }
	inline bool get_mBackgroundTextureHasChanged_4() const { return ___mBackgroundTextureHasChanged_4; }
	inline bool* get_address_of_mBackgroundTextureHasChanged_4() { return &___mBackgroundTextureHasChanged_4; }
	inline void set_mBackgroundTextureHasChanged_4(bool value)
	{
		___mBackgroundTextureHasChanged_4 = value;
	}

	inline static int32_t get_offset_of_mLastSetReflection_5() { return static_cast<int32_t>(offsetof(VuforiaRendererImpl_t3463553574, ___mLastSetReflection_5)); }
	inline int32_t get_mLastSetReflection_5() const { return ___mLastSetReflection_5; }
	inline int32_t* get_address_of_mLastSetReflection_5() { return &___mLastSetReflection_5; }
	inline void set_mLastSetReflection_5(int32_t value)
	{
		___mLastSetReflection_5 = value;
	}

	inline static int32_t get_offset_of_mNativeRenderingCallback_6() { return static_cast<int32_t>(offsetof(VuforiaRendererImpl_t3463553574, ___mNativeRenderingCallback_6)); }
	inline IntPtr_t get_mNativeRenderingCallback_6() const { return ___mNativeRenderingCallback_6; }
	inline IntPtr_t* get_address_of_mNativeRenderingCallback_6() { return &___mNativeRenderingCallback_6; }
	inline void set_mNativeRenderingCallback_6(IntPtr_t value)
	{
		___mNativeRenderingCallback_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VUFORIARENDERERIMPL_T3463553574_H
#ifndef SMARTTERRAINTRACKABLEIMPL_T945956068_H
#define SMARTTERRAINTRACKABLEIMPL_T945956068_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.SmartTerrainTrackableImpl
struct  SmartTerrainTrackableImpl_t945956068  : public TrackableImpl_t3450720819
{
public:
	// System.Collections.Generic.List`1<Vuforia.SmartTerrainTrackable> Vuforia.SmartTerrainTrackableImpl::mChildren
	List_1_t2141263926 * ___mChildren_2;
	// UnityEngine.Mesh Vuforia.SmartTerrainTrackableImpl::mMesh
	Mesh_t996500909 * ___mMesh_3;
	// System.Int32 Vuforia.SmartTerrainTrackableImpl::mMeshRevision
	int32_t ___mMeshRevision_4;
	// Vuforia.VuforiaManagerImpl/PoseData Vuforia.SmartTerrainTrackableImpl::mLocalPose
	PoseData_t1816720573  ___mLocalPose_5;
	// Vuforia.SmartTerrainTrackable Vuforia.SmartTerrainTrackableImpl::<Parent>k__BackingField
	RuntimeObject* ___U3CParentU3Ek__BackingField_6;

public:
	inline static int32_t get_offset_of_mChildren_2() { return static_cast<int32_t>(offsetof(SmartTerrainTrackableImpl_t945956068, ___mChildren_2)); }
	inline List_1_t2141263926 * get_mChildren_2() const { return ___mChildren_2; }
	inline List_1_t2141263926 ** get_address_of_mChildren_2() { return &___mChildren_2; }
	inline void set_mChildren_2(List_1_t2141263926 * value)
	{
		___mChildren_2 = value;
		Il2CppCodeGenWriteBarrier((&___mChildren_2), value);
	}

	inline static int32_t get_offset_of_mMesh_3() { return static_cast<int32_t>(offsetof(SmartTerrainTrackableImpl_t945956068, ___mMesh_3)); }
	inline Mesh_t996500909 * get_mMesh_3() const { return ___mMesh_3; }
	inline Mesh_t996500909 ** get_address_of_mMesh_3() { return &___mMesh_3; }
	inline void set_mMesh_3(Mesh_t996500909 * value)
	{
		___mMesh_3 = value;
		Il2CppCodeGenWriteBarrier((&___mMesh_3), value);
	}

	inline static int32_t get_offset_of_mMeshRevision_4() { return static_cast<int32_t>(offsetof(SmartTerrainTrackableImpl_t945956068, ___mMeshRevision_4)); }
	inline int32_t get_mMeshRevision_4() const { return ___mMeshRevision_4; }
	inline int32_t* get_address_of_mMeshRevision_4() { return &___mMeshRevision_4; }
	inline void set_mMeshRevision_4(int32_t value)
	{
		___mMeshRevision_4 = value;
	}

	inline static int32_t get_offset_of_mLocalPose_5() { return static_cast<int32_t>(offsetof(SmartTerrainTrackableImpl_t945956068, ___mLocalPose_5)); }
	inline PoseData_t1816720573  get_mLocalPose_5() const { return ___mLocalPose_5; }
	inline PoseData_t1816720573 * get_address_of_mLocalPose_5() { return &___mLocalPose_5; }
	inline void set_mLocalPose_5(PoseData_t1816720573  value)
	{
		___mLocalPose_5 = value;
	}

	inline static int32_t get_offset_of_U3CParentU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(SmartTerrainTrackableImpl_t945956068, ___U3CParentU3Ek__BackingField_6)); }
	inline RuntimeObject* get_U3CParentU3Ek__BackingField_6() const { return ___U3CParentU3Ek__BackingField_6; }
	inline RuntimeObject** get_address_of_U3CParentU3Ek__BackingField_6() { return &___U3CParentU3Ek__BackingField_6; }
	inline void set_U3CParentU3Ek__BackingField_6(RuntimeObject* value)
	{
		___U3CParentU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CParentU3Ek__BackingField_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SMARTTERRAINTRACKABLEIMPL_T945956068_H
#ifndef WORDIMPL_T2212611807_H
#define WORDIMPL_T2212611807_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.WordImpl
struct  WordImpl_t2212611807  : public TrackableImpl_t3450720819
{
public:
	// System.String Vuforia.WordImpl::mText
	String_t* ___mText_2;
	// UnityEngine.Vector2 Vuforia.WordImpl::mSize
	Vector2_t3577333262  ___mSize_3;
	// Vuforia.Image Vuforia.WordImpl::mLetterMask
	Image_t1445313791 * ___mLetterMask_4;
	// Vuforia.VuforiaManagerImpl/ImageHeaderData Vuforia.WordImpl::mLetterImageHeader
	ImageHeaderData_t1829409304  ___mLetterImageHeader_5;
	// Vuforia.RectangleData[] Vuforia.WordImpl::mLetterBoundingBoxes
	RectangleDataU5BU5D_t2927157895* ___mLetterBoundingBoxes_6;

public:
	inline static int32_t get_offset_of_mText_2() { return static_cast<int32_t>(offsetof(WordImpl_t2212611807, ___mText_2)); }
	inline String_t* get_mText_2() const { return ___mText_2; }
	inline String_t** get_address_of_mText_2() { return &___mText_2; }
	inline void set_mText_2(String_t* value)
	{
		___mText_2 = value;
		Il2CppCodeGenWriteBarrier((&___mText_2), value);
	}

	inline static int32_t get_offset_of_mSize_3() { return static_cast<int32_t>(offsetof(WordImpl_t2212611807, ___mSize_3)); }
	inline Vector2_t3577333262  get_mSize_3() const { return ___mSize_3; }
	inline Vector2_t3577333262 * get_address_of_mSize_3() { return &___mSize_3; }
	inline void set_mSize_3(Vector2_t3577333262  value)
	{
		___mSize_3 = value;
	}

	inline static int32_t get_offset_of_mLetterMask_4() { return static_cast<int32_t>(offsetof(WordImpl_t2212611807, ___mLetterMask_4)); }
	inline Image_t1445313791 * get_mLetterMask_4() const { return ___mLetterMask_4; }
	inline Image_t1445313791 ** get_address_of_mLetterMask_4() { return &___mLetterMask_4; }
	inline void set_mLetterMask_4(Image_t1445313791 * value)
	{
		___mLetterMask_4 = value;
		Il2CppCodeGenWriteBarrier((&___mLetterMask_4), value);
	}

	inline static int32_t get_offset_of_mLetterImageHeader_5() { return static_cast<int32_t>(offsetof(WordImpl_t2212611807, ___mLetterImageHeader_5)); }
	inline ImageHeaderData_t1829409304  get_mLetterImageHeader_5() const { return ___mLetterImageHeader_5; }
	inline ImageHeaderData_t1829409304 * get_address_of_mLetterImageHeader_5() { return &___mLetterImageHeader_5; }
	inline void set_mLetterImageHeader_5(ImageHeaderData_t1829409304  value)
	{
		___mLetterImageHeader_5 = value;
	}

	inline static int32_t get_offset_of_mLetterBoundingBoxes_6() { return static_cast<int32_t>(offsetof(WordImpl_t2212611807, ___mLetterBoundingBoxes_6)); }
	inline RectangleDataU5BU5D_t2927157895* get_mLetterBoundingBoxes_6() const { return ___mLetterBoundingBoxes_6; }
	inline RectangleDataU5BU5D_t2927157895** get_address_of_mLetterBoundingBoxes_6() { return &___mLetterBoundingBoxes_6; }
	inline void set_mLetterBoundingBoxes_6(RectangleDataU5BU5D_t2927157895* value)
	{
		___mLetterBoundingBoxes_6 = value;
		Il2CppCodeGenWriteBarrier((&___mLetterBoundingBoxes_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WORDIMPL_T2212611807_H
#ifndef WORDMANAGERIMPL_T2273252301_H
#define WORDMANAGERIMPL_T2273252301_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.WordManagerImpl
struct  WordManagerImpl_t2273252301  : public WordManager_t1528772797
{
public:
	// System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.WordResult> Vuforia.WordManagerImpl::mTrackedWords
	Dictionary_2_t1488375814 * ___mTrackedWords_0;
	// System.Collections.Generic.List`1<Vuforia.WordResult> Vuforia.WordManagerImpl::mNewWords
	List_1_t3442485153 * ___mNewWords_1;
	// System.Collections.Generic.List`1<Vuforia.Word> Vuforia.WordManagerImpl::mLostWords
	List_1_t3351092319 * ___mLostWords_2;
	// System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.WordAbstractBehaviour> Vuforia.WordManagerImpl::mActiveWordBehaviours
	Dictionary_2_t3535716765 * ___mActiveWordBehaviours_3;
	// System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour> Vuforia.WordManagerImpl::mWordBehavioursMarkedForDeletion
	List_1_t1194858808 * ___mWordBehavioursMarkedForDeletion_4;
	// System.Collections.Generic.List`1<Vuforia.Word> Vuforia.WordManagerImpl::mWaitingQueue
	List_1_t3351092319 * ___mWaitingQueue_5;
	// System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>> Vuforia.WordManagerImpl::mWordBehaviours
	Dictionary_2_t2364748039 * ___mWordBehaviours_7;
	// System.Boolean Vuforia.WordManagerImpl::mAutomaticTemplate
	bool ___mAutomaticTemplate_8;
	// System.Int32 Vuforia.WordManagerImpl::mMaxInstances
	int32_t ___mMaxInstances_9;
	// Vuforia.WordPrefabCreationMode Vuforia.WordManagerImpl::mWordPrefabCreationMode
	int32_t ___mWordPrefabCreationMode_10;
	// Vuforia.VuforiaARController Vuforia.WordManagerImpl::mVuforiaBehaviour
	VuforiaARController_t1328503142 * ___mVuforiaBehaviour_11;

public:
	inline static int32_t get_offset_of_mTrackedWords_0() { return static_cast<int32_t>(offsetof(WordManagerImpl_t2273252301, ___mTrackedWords_0)); }
	inline Dictionary_2_t1488375814 * get_mTrackedWords_0() const { return ___mTrackedWords_0; }
	inline Dictionary_2_t1488375814 ** get_address_of_mTrackedWords_0() { return &___mTrackedWords_0; }
	inline void set_mTrackedWords_0(Dictionary_2_t1488375814 * value)
	{
		___mTrackedWords_0 = value;
		Il2CppCodeGenWriteBarrier((&___mTrackedWords_0), value);
	}

	inline static int32_t get_offset_of_mNewWords_1() { return static_cast<int32_t>(offsetof(WordManagerImpl_t2273252301, ___mNewWords_1)); }
	inline List_1_t3442485153 * get_mNewWords_1() const { return ___mNewWords_1; }
	inline List_1_t3442485153 ** get_address_of_mNewWords_1() { return &___mNewWords_1; }
	inline void set_mNewWords_1(List_1_t3442485153 * value)
	{
		___mNewWords_1 = value;
		Il2CppCodeGenWriteBarrier((&___mNewWords_1), value);
	}

	inline static int32_t get_offset_of_mLostWords_2() { return static_cast<int32_t>(offsetof(WordManagerImpl_t2273252301, ___mLostWords_2)); }
	inline List_1_t3351092319 * get_mLostWords_2() const { return ___mLostWords_2; }
	inline List_1_t3351092319 ** get_address_of_mLostWords_2() { return &___mLostWords_2; }
	inline void set_mLostWords_2(List_1_t3351092319 * value)
	{
		___mLostWords_2 = value;
		Il2CppCodeGenWriteBarrier((&___mLostWords_2), value);
	}

	inline static int32_t get_offset_of_mActiveWordBehaviours_3() { return static_cast<int32_t>(offsetof(WordManagerImpl_t2273252301, ___mActiveWordBehaviours_3)); }
	inline Dictionary_2_t3535716765 * get_mActiveWordBehaviours_3() const { return ___mActiveWordBehaviours_3; }
	inline Dictionary_2_t3535716765 ** get_address_of_mActiveWordBehaviours_3() { return &___mActiveWordBehaviours_3; }
	inline void set_mActiveWordBehaviours_3(Dictionary_2_t3535716765 * value)
	{
		___mActiveWordBehaviours_3 = value;
		Il2CppCodeGenWriteBarrier((&___mActiveWordBehaviours_3), value);
	}

	inline static int32_t get_offset_of_mWordBehavioursMarkedForDeletion_4() { return static_cast<int32_t>(offsetof(WordManagerImpl_t2273252301, ___mWordBehavioursMarkedForDeletion_4)); }
	inline List_1_t1194858808 * get_mWordBehavioursMarkedForDeletion_4() const { return ___mWordBehavioursMarkedForDeletion_4; }
	inline List_1_t1194858808 ** get_address_of_mWordBehavioursMarkedForDeletion_4() { return &___mWordBehavioursMarkedForDeletion_4; }
	inline void set_mWordBehavioursMarkedForDeletion_4(List_1_t1194858808 * value)
	{
		___mWordBehavioursMarkedForDeletion_4 = value;
		Il2CppCodeGenWriteBarrier((&___mWordBehavioursMarkedForDeletion_4), value);
	}

	inline static int32_t get_offset_of_mWaitingQueue_5() { return static_cast<int32_t>(offsetof(WordManagerImpl_t2273252301, ___mWaitingQueue_5)); }
	inline List_1_t3351092319 * get_mWaitingQueue_5() const { return ___mWaitingQueue_5; }
	inline List_1_t3351092319 ** get_address_of_mWaitingQueue_5() { return &___mWaitingQueue_5; }
	inline void set_mWaitingQueue_5(List_1_t3351092319 * value)
	{
		___mWaitingQueue_5 = value;
		Il2CppCodeGenWriteBarrier((&___mWaitingQueue_5), value);
	}

	inline static int32_t get_offset_of_mWordBehaviours_7() { return static_cast<int32_t>(offsetof(WordManagerImpl_t2273252301, ___mWordBehaviours_7)); }
	inline Dictionary_2_t2364748039 * get_mWordBehaviours_7() const { return ___mWordBehaviours_7; }
	inline Dictionary_2_t2364748039 ** get_address_of_mWordBehaviours_7() { return &___mWordBehaviours_7; }
	inline void set_mWordBehaviours_7(Dictionary_2_t2364748039 * value)
	{
		___mWordBehaviours_7 = value;
		Il2CppCodeGenWriteBarrier((&___mWordBehaviours_7), value);
	}

	inline static int32_t get_offset_of_mAutomaticTemplate_8() { return static_cast<int32_t>(offsetof(WordManagerImpl_t2273252301, ___mAutomaticTemplate_8)); }
	inline bool get_mAutomaticTemplate_8() const { return ___mAutomaticTemplate_8; }
	inline bool* get_address_of_mAutomaticTemplate_8() { return &___mAutomaticTemplate_8; }
	inline void set_mAutomaticTemplate_8(bool value)
	{
		___mAutomaticTemplate_8 = value;
	}

	inline static int32_t get_offset_of_mMaxInstances_9() { return static_cast<int32_t>(offsetof(WordManagerImpl_t2273252301, ___mMaxInstances_9)); }
	inline int32_t get_mMaxInstances_9() const { return ___mMaxInstances_9; }
	inline int32_t* get_address_of_mMaxInstances_9() { return &___mMaxInstances_9; }
	inline void set_mMaxInstances_9(int32_t value)
	{
		___mMaxInstances_9 = value;
	}

	inline static int32_t get_offset_of_mWordPrefabCreationMode_10() { return static_cast<int32_t>(offsetof(WordManagerImpl_t2273252301, ___mWordPrefabCreationMode_10)); }
	inline int32_t get_mWordPrefabCreationMode_10() const { return ___mWordPrefabCreationMode_10; }
	inline int32_t* get_address_of_mWordPrefabCreationMode_10() { return &___mWordPrefabCreationMode_10; }
	inline void set_mWordPrefabCreationMode_10(int32_t value)
	{
		___mWordPrefabCreationMode_10 = value;
	}

	inline static int32_t get_offset_of_mVuforiaBehaviour_11() { return static_cast<int32_t>(offsetof(WordManagerImpl_t2273252301, ___mVuforiaBehaviour_11)); }
	inline VuforiaARController_t1328503142 * get_mVuforiaBehaviour_11() const { return ___mVuforiaBehaviour_11; }
	inline VuforiaARController_t1328503142 ** get_address_of_mVuforiaBehaviour_11() { return &___mVuforiaBehaviour_11; }
	inline void set_mVuforiaBehaviour_11(VuforiaARController_t1328503142 * value)
	{
		___mVuforiaBehaviour_11 = value;
		Il2CppCodeGenWriteBarrier((&___mVuforiaBehaviour_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WORDMANAGERIMPL_T2273252301_H
#ifndef WORDRESULTIMPL_T2776230186_H
#define WORDRESULTIMPL_T2776230186_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.WordResultImpl
struct  WordResultImpl_t2776230186  : public WordResult_t292368650
{
public:
	// Vuforia.OrientedBoundingBox Vuforia.WordResultImpl::mObb
	OrientedBoundingBox_t4288440438  ___mObb_0;
	// UnityEngine.Vector3 Vuforia.WordResultImpl::mPosition
	Vector3_t2903530434  ___mPosition_1;
	// UnityEngine.Quaternion Vuforia.WordResultImpl::mOrientation
	Quaternion_t754065749  ___mOrientation_2;
	// Vuforia.Word Vuforia.WordResultImpl::mWord
	RuntimeObject* ___mWord_3;
	// Vuforia.TrackableBehaviour/Status Vuforia.WordResultImpl::mStatus
	int32_t ___mStatus_4;

public:
	inline static int32_t get_offset_of_mObb_0() { return static_cast<int32_t>(offsetof(WordResultImpl_t2776230186, ___mObb_0)); }
	inline OrientedBoundingBox_t4288440438  get_mObb_0() const { return ___mObb_0; }
	inline OrientedBoundingBox_t4288440438 * get_address_of_mObb_0() { return &___mObb_0; }
	inline void set_mObb_0(OrientedBoundingBox_t4288440438  value)
	{
		___mObb_0 = value;
	}

	inline static int32_t get_offset_of_mPosition_1() { return static_cast<int32_t>(offsetof(WordResultImpl_t2776230186, ___mPosition_1)); }
	inline Vector3_t2903530434  get_mPosition_1() const { return ___mPosition_1; }
	inline Vector3_t2903530434 * get_address_of_mPosition_1() { return &___mPosition_1; }
	inline void set_mPosition_1(Vector3_t2903530434  value)
	{
		___mPosition_1 = value;
	}

	inline static int32_t get_offset_of_mOrientation_2() { return static_cast<int32_t>(offsetof(WordResultImpl_t2776230186, ___mOrientation_2)); }
	inline Quaternion_t754065749  get_mOrientation_2() const { return ___mOrientation_2; }
	inline Quaternion_t754065749 * get_address_of_mOrientation_2() { return &___mOrientation_2; }
	inline void set_mOrientation_2(Quaternion_t754065749  value)
	{
		___mOrientation_2 = value;
	}

	inline static int32_t get_offset_of_mWord_3() { return static_cast<int32_t>(offsetof(WordResultImpl_t2776230186, ___mWord_3)); }
	inline RuntimeObject* get_mWord_3() const { return ___mWord_3; }
	inline RuntimeObject** get_address_of_mWord_3() { return &___mWord_3; }
	inline void set_mWord_3(RuntimeObject* value)
	{
		___mWord_3 = value;
		Il2CppCodeGenWriteBarrier((&___mWord_3), value);
	}

	inline static int32_t get_offset_of_mStatus_4() { return static_cast<int32_t>(offsetof(WordResultImpl_t2776230186, ___mStatus_4)); }
	inline int32_t get_mStatus_4() const { return ___mStatus_4; }
	inline int32_t* get_address_of_mStatus_4() { return &___mStatus_4; }
	inline void set_mStatus_4(int32_t value)
	{
		___mStatus_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WORDRESULTIMPL_T2776230186_H
#ifndef VUMARKTARGETDATA_T2973346370_H
#define VUMARKTARGETDATA_T2973346370_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.VuforiaManagerImpl/VuMarkTargetData
#pragma pack(push, tp, 1)
struct  VuMarkTargetData_t2973346370 
{
public:
	// Vuforia.VuforiaManagerImpl/InstanceIdData Vuforia.VuforiaManagerImpl/VuMarkTargetData::instanceId
	InstanceIdData_t2286842551  ___instanceId_0;
	// System.Int32 Vuforia.VuforiaManagerImpl/VuMarkTargetData::id
	int32_t ___id_1;
	// System.Int32 Vuforia.VuforiaManagerImpl/VuMarkTargetData::templateId
	int32_t ___templateId_2;
	// UnityEngine.Vector3 Vuforia.VuforiaManagerImpl/VuMarkTargetData::size
	Vector3_t2903530434  ___size_3;
	// System.Int32 Vuforia.VuforiaManagerImpl/VuMarkTargetData::unused
	int32_t ___unused_4;

public:
	inline static int32_t get_offset_of_instanceId_0() { return static_cast<int32_t>(offsetof(VuMarkTargetData_t2973346370, ___instanceId_0)); }
	inline InstanceIdData_t2286842551  get_instanceId_0() const { return ___instanceId_0; }
	inline InstanceIdData_t2286842551 * get_address_of_instanceId_0() { return &___instanceId_0; }
	inline void set_instanceId_0(InstanceIdData_t2286842551  value)
	{
		___instanceId_0 = value;
	}

	inline static int32_t get_offset_of_id_1() { return static_cast<int32_t>(offsetof(VuMarkTargetData_t2973346370, ___id_1)); }
	inline int32_t get_id_1() const { return ___id_1; }
	inline int32_t* get_address_of_id_1() { return &___id_1; }
	inline void set_id_1(int32_t value)
	{
		___id_1 = value;
	}

	inline static int32_t get_offset_of_templateId_2() { return static_cast<int32_t>(offsetof(VuMarkTargetData_t2973346370, ___templateId_2)); }
	inline int32_t get_templateId_2() const { return ___templateId_2; }
	inline int32_t* get_address_of_templateId_2() { return &___templateId_2; }
	inline void set_templateId_2(int32_t value)
	{
		___templateId_2 = value;
	}

	inline static int32_t get_offset_of_size_3() { return static_cast<int32_t>(offsetof(VuMarkTargetData_t2973346370, ___size_3)); }
	inline Vector3_t2903530434  get_size_3() const { return ___size_3; }
	inline Vector3_t2903530434 * get_address_of_size_3() { return &___size_3; }
	inline void set_size_3(Vector3_t2903530434  value)
	{
		___size_3 = value;
	}

	inline static int32_t get_offset_of_unused_4() { return static_cast<int32_t>(offsetof(VuMarkTargetData_t2973346370, ___unused_4)); }
	inline int32_t get_unused_4() const { return ___unused_4; }
	inline int32_t* get_address_of_unused_4() { return &___unused_4; }
	inline void set_unused_4(int32_t value)
	{
		___unused_4 = value;
	}
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VUMARKTARGETDATA_T2973346370_H
#ifndef VUFORIARUNTIME_T2131788666_H
#define VUFORIARUNTIME_T2131788666_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.VuforiaRuntime
struct  VuforiaRuntime_t2131788666  : public RuntimeObject
{
public:
	// System.Action`1<Vuforia.VuforiaUnity/InitError> Vuforia.VuforiaRuntime::mOnVuforiaInitError
	Action_1_t3472505684 * ___mOnVuforiaInitError_0;
	// System.Boolean Vuforia.VuforiaRuntime::mFailedToInitialize
	bool ___mFailedToInitialize_1;
	// Vuforia.VuforiaUnity/InitError Vuforia.VuforiaRuntime::mInitError
	int32_t ___mInitError_2;
	// System.Boolean Vuforia.VuforiaRuntime::mHasInitialized
	bool ___mHasInitialized_3;

public:
	inline static int32_t get_offset_of_mOnVuforiaInitError_0() { return static_cast<int32_t>(offsetof(VuforiaRuntime_t2131788666, ___mOnVuforiaInitError_0)); }
	inline Action_1_t3472505684 * get_mOnVuforiaInitError_0() const { return ___mOnVuforiaInitError_0; }
	inline Action_1_t3472505684 ** get_address_of_mOnVuforiaInitError_0() { return &___mOnVuforiaInitError_0; }
	inline void set_mOnVuforiaInitError_0(Action_1_t3472505684 * value)
	{
		___mOnVuforiaInitError_0 = value;
		Il2CppCodeGenWriteBarrier((&___mOnVuforiaInitError_0), value);
	}

	inline static int32_t get_offset_of_mFailedToInitialize_1() { return static_cast<int32_t>(offsetof(VuforiaRuntime_t2131788666, ___mFailedToInitialize_1)); }
	inline bool get_mFailedToInitialize_1() const { return ___mFailedToInitialize_1; }
	inline bool* get_address_of_mFailedToInitialize_1() { return &___mFailedToInitialize_1; }
	inline void set_mFailedToInitialize_1(bool value)
	{
		___mFailedToInitialize_1 = value;
	}

	inline static int32_t get_offset_of_mInitError_2() { return static_cast<int32_t>(offsetof(VuforiaRuntime_t2131788666, ___mInitError_2)); }
	inline int32_t get_mInitError_2() const { return ___mInitError_2; }
	inline int32_t* get_address_of_mInitError_2() { return &___mInitError_2; }
	inline void set_mInitError_2(int32_t value)
	{
		___mInitError_2 = value;
	}

	inline static int32_t get_offset_of_mHasInitialized_3() { return static_cast<int32_t>(offsetof(VuforiaRuntime_t2131788666, ___mHasInitialized_3)); }
	inline bool get_mHasInitialized_3() const { return ___mHasInitialized_3; }
	inline bool* get_address_of_mHasInitialized_3() { return &___mHasInitialized_3; }
	inline void set_mHasInitialized_3(bool value)
	{
		___mHasInitialized_3 = value;
	}
};

struct VuforiaRuntime_t2131788666_StaticFields
{
public:
	// Vuforia.VuforiaRuntime Vuforia.VuforiaRuntime::mInstance
	VuforiaRuntime_t2131788666 * ___mInstance_4;
	// System.Object Vuforia.VuforiaRuntime::mPadlock
	RuntimeObject * ___mPadlock_5;

public:
	inline static int32_t get_offset_of_mInstance_4() { return static_cast<int32_t>(offsetof(VuforiaRuntime_t2131788666_StaticFields, ___mInstance_4)); }
	inline VuforiaRuntime_t2131788666 * get_mInstance_4() const { return ___mInstance_4; }
	inline VuforiaRuntime_t2131788666 ** get_address_of_mInstance_4() { return &___mInstance_4; }
	inline void set_mInstance_4(VuforiaRuntime_t2131788666 * value)
	{
		___mInstance_4 = value;
		Il2CppCodeGenWriteBarrier((&___mInstance_4), value);
	}

	inline static int32_t get_offset_of_mPadlock_5() { return static_cast<int32_t>(offsetof(VuforiaRuntime_t2131788666_StaticFields, ___mPadlock_5)); }
	inline RuntimeObject * get_mPadlock_5() const { return ___mPadlock_5; }
	inline RuntimeObject ** get_address_of_mPadlock_5() { return &___mPadlock_5; }
	inline void set_mPadlock_5(RuntimeObject * value)
	{
		___mPadlock_5 = value;
		Il2CppCodeGenWriteBarrier((&___mPadlock_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VUFORIARUNTIME_T2131788666_H
#ifndef VUFORIAABSTRACTCONFIGURATION_T974438950_H
#define VUFORIAABSTRACTCONFIGURATION_T974438950_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.VuforiaAbstractConfiguration
struct  VuforiaAbstractConfiguration_t974438950  : public ScriptableObject_t3635579074
{
public:
	// Vuforia.VuforiaAbstractConfiguration/GenericVuforiaConfiguration Vuforia.VuforiaAbstractConfiguration::vuforia
	GenericVuforiaConfiguration_t1574456586 * ___vuforia_4;
	// Vuforia.VuforiaAbstractConfiguration/DigitalEyewearConfiguration Vuforia.VuforiaAbstractConfiguration::digitalEyewear
	DigitalEyewearConfiguration_t3649657388 * ___digitalEyewear_5;
	// Vuforia.VuforiaAbstractConfiguration/DatabaseLoadConfiguration Vuforia.VuforiaAbstractConfiguration::databaseLoad
	DatabaseLoadConfiguration_t3827205120 * ___databaseLoad_6;
	// Vuforia.VuforiaAbstractConfiguration/VideoBackgroundConfiguration Vuforia.VuforiaAbstractConfiguration::videoBackground
	VideoBackgroundConfiguration_t2579534243 * ___videoBackground_7;
	// Vuforia.VuforiaAbstractConfiguration/SmartTerrainTrackerConfiguration Vuforia.VuforiaAbstractConfiguration::smartTerrainTracker
	SmartTerrainTrackerConfiguration_t657164935 * ___smartTerrainTracker_8;
	// Vuforia.VuforiaAbstractConfiguration/DeviceTrackerConfiguration Vuforia.VuforiaAbstractConfiguration::deviceTracker
	DeviceTrackerConfiguration_t2458088029 * ___deviceTracker_9;
	// Vuforia.VuforiaAbstractConfiguration/WebCamConfiguration Vuforia.VuforiaAbstractConfiguration::webcam
	WebCamConfiguration_t2750821739 * ___webcam_10;

public:
	inline static int32_t get_offset_of_vuforia_4() { return static_cast<int32_t>(offsetof(VuforiaAbstractConfiguration_t974438950, ___vuforia_4)); }
	inline GenericVuforiaConfiguration_t1574456586 * get_vuforia_4() const { return ___vuforia_4; }
	inline GenericVuforiaConfiguration_t1574456586 ** get_address_of_vuforia_4() { return &___vuforia_4; }
	inline void set_vuforia_4(GenericVuforiaConfiguration_t1574456586 * value)
	{
		___vuforia_4 = value;
		Il2CppCodeGenWriteBarrier((&___vuforia_4), value);
	}

	inline static int32_t get_offset_of_digitalEyewear_5() { return static_cast<int32_t>(offsetof(VuforiaAbstractConfiguration_t974438950, ___digitalEyewear_5)); }
	inline DigitalEyewearConfiguration_t3649657388 * get_digitalEyewear_5() const { return ___digitalEyewear_5; }
	inline DigitalEyewearConfiguration_t3649657388 ** get_address_of_digitalEyewear_5() { return &___digitalEyewear_5; }
	inline void set_digitalEyewear_5(DigitalEyewearConfiguration_t3649657388 * value)
	{
		___digitalEyewear_5 = value;
		Il2CppCodeGenWriteBarrier((&___digitalEyewear_5), value);
	}

	inline static int32_t get_offset_of_databaseLoad_6() { return static_cast<int32_t>(offsetof(VuforiaAbstractConfiguration_t974438950, ___databaseLoad_6)); }
	inline DatabaseLoadConfiguration_t3827205120 * get_databaseLoad_6() const { return ___databaseLoad_6; }
	inline DatabaseLoadConfiguration_t3827205120 ** get_address_of_databaseLoad_6() { return &___databaseLoad_6; }
	inline void set_databaseLoad_6(DatabaseLoadConfiguration_t3827205120 * value)
	{
		___databaseLoad_6 = value;
		Il2CppCodeGenWriteBarrier((&___databaseLoad_6), value);
	}

	inline static int32_t get_offset_of_videoBackground_7() { return static_cast<int32_t>(offsetof(VuforiaAbstractConfiguration_t974438950, ___videoBackground_7)); }
	inline VideoBackgroundConfiguration_t2579534243 * get_videoBackground_7() const { return ___videoBackground_7; }
	inline VideoBackgroundConfiguration_t2579534243 ** get_address_of_videoBackground_7() { return &___videoBackground_7; }
	inline void set_videoBackground_7(VideoBackgroundConfiguration_t2579534243 * value)
	{
		___videoBackground_7 = value;
		Il2CppCodeGenWriteBarrier((&___videoBackground_7), value);
	}

	inline static int32_t get_offset_of_smartTerrainTracker_8() { return static_cast<int32_t>(offsetof(VuforiaAbstractConfiguration_t974438950, ___smartTerrainTracker_8)); }
	inline SmartTerrainTrackerConfiguration_t657164935 * get_smartTerrainTracker_8() const { return ___smartTerrainTracker_8; }
	inline SmartTerrainTrackerConfiguration_t657164935 ** get_address_of_smartTerrainTracker_8() { return &___smartTerrainTracker_8; }
	inline void set_smartTerrainTracker_8(SmartTerrainTrackerConfiguration_t657164935 * value)
	{
		___smartTerrainTracker_8 = value;
		Il2CppCodeGenWriteBarrier((&___smartTerrainTracker_8), value);
	}

	inline static int32_t get_offset_of_deviceTracker_9() { return static_cast<int32_t>(offsetof(VuforiaAbstractConfiguration_t974438950, ___deviceTracker_9)); }
	inline DeviceTrackerConfiguration_t2458088029 * get_deviceTracker_9() const { return ___deviceTracker_9; }
	inline DeviceTrackerConfiguration_t2458088029 ** get_address_of_deviceTracker_9() { return &___deviceTracker_9; }
	inline void set_deviceTracker_9(DeviceTrackerConfiguration_t2458088029 * value)
	{
		___deviceTracker_9 = value;
		Il2CppCodeGenWriteBarrier((&___deviceTracker_9), value);
	}

	inline static int32_t get_offset_of_webcam_10() { return static_cast<int32_t>(offsetof(VuforiaAbstractConfiguration_t974438950, ___webcam_10)); }
	inline WebCamConfiguration_t2750821739 * get_webcam_10() const { return ___webcam_10; }
	inline WebCamConfiguration_t2750821739 ** get_address_of_webcam_10() { return &___webcam_10; }
	inline void set_webcam_10(WebCamConfiguration_t2750821739 * value)
	{
		___webcam_10 = value;
		Il2CppCodeGenWriteBarrier((&___webcam_10), value);
	}
};

struct VuforiaAbstractConfiguration_t974438950_StaticFields
{
public:
	// Vuforia.VuforiaAbstractConfiguration Vuforia.VuforiaAbstractConfiguration::mInstance
	VuforiaAbstractConfiguration_t974438950 * ___mInstance_2;
	// System.Object Vuforia.VuforiaAbstractConfiguration::mPadlock
	RuntimeObject * ___mPadlock_3;

public:
	inline static int32_t get_offset_of_mInstance_2() { return static_cast<int32_t>(offsetof(VuforiaAbstractConfiguration_t974438950_StaticFields, ___mInstance_2)); }
	inline VuforiaAbstractConfiguration_t974438950 * get_mInstance_2() const { return ___mInstance_2; }
	inline VuforiaAbstractConfiguration_t974438950 ** get_address_of_mInstance_2() { return &___mInstance_2; }
	inline void set_mInstance_2(VuforiaAbstractConfiguration_t974438950 * value)
	{
		___mInstance_2 = value;
		Il2CppCodeGenWriteBarrier((&___mInstance_2), value);
	}

	inline static int32_t get_offset_of_mPadlock_3() { return static_cast<int32_t>(offsetof(VuforiaAbstractConfiguration_t974438950_StaticFields, ___mPadlock_3)); }
	inline RuntimeObject * get_mPadlock_3() const { return ___mPadlock_3; }
	inline RuntimeObject ** get_address_of_mPadlock_3() { return &___mPadlock_3; }
	inline void set_mPadlock_3(RuntimeObject * value)
	{
		___mPadlock_3 = value;
		Il2CppCodeGenWriteBarrier((&___mPadlock_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VUFORIAABSTRACTCONFIGURATION_T974438950_H
#ifndef NULLWEBCAMTEXADAPTOR_T712631648_H
#define NULLWEBCAMTEXADAPTOR_T712631648_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.NullWebCamTexAdaptor
struct  NullWebCamTexAdaptor_t712631648  : public WebCamTexAdaptor_t433428952
{
public:
	// UnityEngine.Texture2D Vuforia.NullWebCamTexAdaptor::mTexture
	Texture2D_t415585320 * ___mTexture_0;
	// System.Boolean Vuforia.NullWebCamTexAdaptor::mPseudoPlaying
	bool ___mPseudoPlaying_1;
	// System.Double Vuforia.NullWebCamTexAdaptor::mMsBetweenFrames
	double ___mMsBetweenFrames_2;
	// System.DateTime Vuforia.NullWebCamTexAdaptor::mLastFrame
	DateTime_t2376405012  ___mLastFrame_3;

public:
	inline static int32_t get_offset_of_mTexture_0() { return static_cast<int32_t>(offsetof(NullWebCamTexAdaptor_t712631648, ___mTexture_0)); }
	inline Texture2D_t415585320 * get_mTexture_0() const { return ___mTexture_0; }
	inline Texture2D_t415585320 ** get_address_of_mTexture_0() { return &___mTexture_0; }
	inline void set_mTexture_0(Texture2D_t415585320 * value)
	{
		___mTexture_0 = value;
		Il2CppCodeGenWriteBarrier((&___mTexture_0), value);
	}

	inline static int32_t get_offset_of_mPseudoPlaying_1() { return static_cast<int32_t>(offsetof(NullWebCamTexAdaptor_t712631648, ___mPseudoPlaying_1)); }
	inline bool get_mPseudoPlaying_1() const { return ___mPseudoPlaying_1; }
	inline bool* get_address_of_mPseudoPlaying_1() { return &___mPseudoPlaying_1; }
	inline void set_mPseudoPlaying_1(bool value)
	{
		___mPseudoPlaying_1 = value;
	}

	inline static int32_t get_offset_of_mMsBetweenFrames_2() { return static_cast<int32_t>(offsetof(NullWebCamTexAdaptor_t712631648, ___mMsBetweenFrames_2)); }
	inline double get_mMsBetweenFrames_2() const { return ___mMsBetweenFrames_2; }
	inline double* get_address_of_mMsBetweenFrames_2() { return &___mMsBetweenFrames_2; }
	inline void set_mMsBetweenFrames_2(double value)
	{
		___mMsBetweenFrames_2 = value;
	}

	inline static int32_t get_offset_of_mLastFrame_3() { return static_cast<int32_t>(offsetof(NullWebCamTexAdaptor_t712631648, ___mLastFrame_3)); }
	inline DateTime_t2376405012  get_mLastFrame_3() const { return ___mLastFrame_3; }
	inline DateTime_t2376405012 * get_address_of_mLastFrame_3() { return &___mLastFrame_3; }
	inline void set_mLastFrame_3(DateTime_t2376405012  value)
	{
		___mLastFrame_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLWEBCAMTEXADAPTOR_T712631648_H
#ifndef SURFACEIMPL_T1060658458_H
#define SURFACEIMPL_T1060658458_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.SurfaceImpl
struct  SurfaceImpl_t1060658458  : public SmartTerrainTrackableImpl_t945956068
{
public:
	// UnityEngine.Mesh Vuforia.SurfaceImpl::mNavMesh
	Mesh_t996500909 * ___mNavMesh_7;
	// System.Int32[] Vuforia.SurfaceImpl::mMeshBoundaries
	Int32U5BU5D_t2324750880* ___mMeshBoundaries_8;
	// UnityEngine.Rect Vuforia.SurfaceImpl::mBoundingBox
	Rect_t3436776195  ___mBoundingBox_9;
	// System.Single Vuforia.SurfaceImpl::mSurfaceArea
	float ___mSurfaceArea_10;
	// System.Boolean Vuforia.SurfaceImpl::mAreaNeedsUpdate
	bool ___mAreaNeedsUpdate_11;

public:
	inline static int32_t get_offset_of_mNavMesh_7() { return static_cast<int32_t>(offsetof(SurfaceImpl_t1060658458, ___mNavMesh_7)); }
	inline Mesh_t996500909 * get_mNavMesh_7() const { return ___mNavMesh_7; }
	inline Mesh_t996500909 ** get_address_of_mNavMesh_7() { return &___mNavMesh_7; }
	inline void set_mNavMesh_7(Mesh_t996500909 * value)
	{
		___mNavMesh_7 = value;
		Il2CppCodeGenWriteBarrier((&___mNavMesh_7), value);
	}

	inline static int32_t get_offset_of_mMeshBoundaries_8() { return static_cast<int32_t>(offsetof(SurfaceImpl_t1060658458, ___mMeshBoundaries_8)); }
	inline Int32U5BU5D_t2324750880* get_mMeshBoundaries_8() const { return ___mMeshBoundaries_8; }
	inline Int32U5BU5D_t2324750880** get_address_of_mMeshBoundaries_8() { return &___mMeshBoundaries_8; }
	inline void set_mMeshBoundaries_8(Int32U5BU5D_t2324750880* value)
	{
		___mMeshBoundaries_8 = value;
		Il2CppCodeGenWriteBarrier((&___mMeshBoundaries_8), value);
	}

	inline static int32_t get_offset_of_mBoundingBox_9() { return static_cast<int32_t>(offsetof(SurfaceImpl_t1060658458, ___mBoundingBox_9)); }
	inline Rect_t3436776195  get_mBoundingBox_9() const { return ___mBoundingBox_9; }
	inline Rect_t3436776195 * get_address_of_mBoundingBox_9() { return &___mBoundingBox_9; }
	inline void set_mBoundingBox_9(Rect_t3436776195  value)
	{
		___mBoundingBox_9 = value;
	}

	inline static int32_t get_offset_of_mSurfaceArea_10() { return static_cast<int32_t>(offsetof(SurfaceImpl_t1060658458, ___mSurfaceArea_10)); }
	inline float get_mSurfaceArea_10() const { return ___mSurfaceArea_10; }
	inline float* get_address_of_mSurfaceArea_10() { return &___mSurfaceArea_10; }
	inline void set_mSurfaceArea_10(float value)
	{
		___mSurfaceArea_10 = value;
	}

	inline static int32_t get_offset_of_mAreaNeedsUpdate_11() { return static_cast<int32_t>(offsetof(SurfaceImpl_t1060658458, ___mAreaNeedsUpdate_11)); }
	inline bool get_mAreaNeedsUpdate_11() const { return ___mAreaNeedsUpdate_11; }
	inline bool* get_address_of_mAreaNeedsUpdate_11() { return &___mAreaNeedsUpdate_11; }
	inline void set_mAreaNeedsUpdate_11(bool value)
	{
		___mAreaNeedsUpdate_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SURFACEIMPL_T1060658458_H
#ifndef PROPIMPL_T1422751624_H
#define PROPIMPL_T1422751624_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.PropImpl
struct  PropImpl_t1422751624  : public SmartTerrainTrackableImpl_t945956068
{
public:
	// Vuforia.OrientedBoundingBox3D Vuforia.PropImpl::mOrientedBoundingBox3D
	OrientedBoundingBox3D_t19466890  ___mOrientedBoundingBox3D_7;

public:
	inline static int32_t get_offset_of_mOrientedBoundingBox3D_7() { return static_cast<int32_t>(offsetof(PropImpl_t1422751624, ___mOrientedBoundingBox3D_7)); }
	inline OrientedBoundingBox3D_t19466890  get_mOrientedBoundingBox3D_7() const { return ___mOrientedBoundingBox3D_7; }
	inline OrientedBoundingBox3D_t19466890 * get_address_of_mOrientedBoundingBox3D_7() { return &___mOrientedBoundingBox3D_7; }
	inline void set_mOrientedBoundingBox3D_7(OrientedBoundingBox3D_t19466890  value)
	{
		___mOrientedBoundingBox3D_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROPIMPL_T1422751624_H
#ifndef BEHAVIOUR_T363748010_H
#define BEHAVIOUR_T363748010_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_t363748010  : public Component_t4087199522
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_T363748010_H
#ifndef MONOBEHAVIOUR_T345688271_H
#define MONOBEHAVIOUR_T345688271_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t345688271  : public Behaviour_t363748010
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T345688271_H
#ifndef VUFORIADEINITBEHAVIOUR_T1122908686_H
#define VUFORIADEINITBEHAVIOUR_T1122908686_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.VuforiaDeinitBehaviour
struct  VuforiaDeinitBehaviour_t1122908686  : public MonoBehaviour_t345688271
{
public:

public:
};

struct VuforiaDeinitBehaviour_t1122908686_StaticFields
{
public:
	// System.Boolean Vuforia.VuforiaDeinitBehaviour::mAppIsQuitting
	bool ___mAppIsQuitting_2;

public:
	inline static int32_t get_offset_of_mAppIsQuitting_2() { return static_cast<int32_t>(offsetof(VuforiaDeinitBehaviour_t1122908686_StaticFields, ___mAppIsQuitting_2)); }
	inline bool get_mAppIsQuitting_2() const { return ___mAppIsQuitting_2; }
	inline bool* get_address_of_mAppIsQuitting_2() { return &___mAppIsQuitting_2; }
	inline void set_mAppIsQuitting_2(bool value)
	{
		___mAppIsQuitting_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VUFORIADEINITBEHAVIOUR_T1122908686_H
#ifndef RECONSTRUCTIONFROMTARGETABSTRACTBEHAVIOUR_T2676804737_H
#define RECONSTRUCTIONFROMTARGETABSTRACTBEHAVIOUR_T2676804737_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.ReconstructionFromTargetAbstractBehaviour
struct  ReconstructionFromTargetAbstractBehaviour_t2676804737  : public MonoBehaviour_t345688271
{
public:
	// Vuforia.ReconstructionFromTarget Vuforia.ReconstructionFromTargetAbstractBehaviour::mReconstructionFromTarget
	RuntimeObject* ___mReconstructionFromTarget_2;
	// Vuforia.ReconstructionAbstractBehaviour Vuforia.ReconstructionFromTargetAbstractBehaviour::mReconstructionBehaviour
	ReconstructionAbstractBehaviour_t954468633 * ___mReconstructionBehaviour_3;

public:
	inline static int32_t get_offset_of_mReconstructionFromTarget_2() { return static_cast<int32_t>(offsetof(ReconstructionFromTargetAbstractBehaviour_t2676804737, ___mReconstructionFromTarget_2)); }
	inline RuntimeObject* get_mReconstructionFromTarget_2() const { return ___mReconstructionFromTarget_2; }
	inline RuntimeObject** get_address_of_mReconstructionFromTarget_2() { return &___mReconstructionFromTarget_2; }
	inline void set_mReconstructionFromTarget_2(RuntimeObject* value)
	{
		___mReconstructionFromTarget_2 = value;
		Il2CppCodeGenWriteBarrier((&___mReconstructionFromTarget_2), value);
	}

	inline static int32_t get_offset_of_mReconstructionBehaviour_3() { return static_cast<int32_t>(offsetof(ReconstructionFromTargetAbstractBehaviour_t2676804737, ___mReconstructionBehaviour_3)); }
	inline ReconstructionAbstractBehaviour_t954468633 * get_mReconstructionBehaviour_3() const { return ___mReconstructionBehaviour_3; }
	inline ReconstructionAbstractBehaviour_t954468633 ** get_address_of_mReconstructionBehaviour_3() { return &___mReconstructionBehaviour_3; }
	inline void set_mReconstructionBehaviour_3(ReconstructionAbstractBehaviour_t954468633 * value)
	{
		___mReconstructionBehaviour_3 = value;
		Il2CppCodeGenWriteBarrier((&___mReconstructionBehaviour_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RECONSTRUCTIONFROMTARGETABSTRACTBEHAVIOUR_T2676804737_H
#ifndef TRACKABLEBEHAVIOUR_T2419079356_H
#define TRACKABLEBEHAVIOUR_T2419079356_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.TrackableBehaviour
struct  TrackableBehaviour_t2419079356  : public MonoBehaviour_t345688271
{
public:
	// System.Double Vuforia.TrackableBehaviour::<TimeStamp>k__BackingField
	double ___U3CTimeStampU3Ek__BackingField_2;
	// System.String Vuforia.TrackableBehaviour::mTrackableName
	String_t* ___mTrackableName_3;
	// System.Boolean Vuforia.TrackableBehaviour::mPreserveChildSize
	bool ___mPreserveChildSize_4;
	// System.Boolean Vuforia.TrackableBehaviour::mInitializedInEditor
	bool ___mInitializedInEditor_5;
	// UnityEngine.Vector3 Vuforia.TrackableBehaviour::mPreviousScale
	Vector3_t2903530434  ___mPreviousScale_6;
	// Vuforia.TrackableBehaviour/Status Vuforia.TrackableBehaviour::mStatus
	int32_t ___mStatus_7;
	// Vuforia.Trackable Vuforia.TrackableBehaviour::mTrackable
	RuntimeObject* ___mTrackable_8;
	// System.Collections.Generic.List`1<Vuforia.ITrackableEventHandler> Vuforia.TrackableBehaviour::mTrackableEventHandlers
	List_1_t3589236026 * ___mTrackableEventHandlers_9;

public:
	inline static int32_t get_offset_of_U3CTimeStampU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(TrackableBehaviour_t2419079356, ___U3CTimeStampU3Ek__BackingField_2)); }
	inline double get_U3CTimeStampU3Ek__BackingField_2() const { return ___U3CTimeStampU3Ek__BackingField_2; }
	inline double* get_address_of_U3CTimeStampU3Ek__BackingField_2() { return &___U3CTimeStampU3Ek__BackingField_2; }
	inline void set_U3CTimeStampU3Ek__BackingField_2(double value)
	{
		___U3CTimeStampU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_mTrackableName_3() { return static_cast<int32_t>(offsetof(TrackableBehaviour_t2419079356, ___mTrackableName_3)); }
	inline String_t* get_mTrackableName_3() const { return ___mTrackableName_3; }
	inline String_t** get_address_of_mTrackableName_3() { return &___mTrackableName_3; }
	inline void set_mTrackableName_3(String_t* value)
	{
		___mTrackableName_3 = value;
		Il2CppCodeGenWriteBarrier((&___mTrackableName_3), value);
	}

	inline static int32_t get_offset_of_mPreserveChildSize_4() { return static_cast<int32_t>(offsetof(TrackableBehaviour_t2419079356, ___mPreserveChildSize_4)); }
	inline bool get_mPreserveChildSize_4() const { return ___mPreserveChildSize_4; }
	inline bool* get_address_of_mPreserveChildSize_4() { return &___mPreserveChildSize_4; }
	inline void set_mPreserveChildSize_4(bool value)
	{
		___mPreserveChildSize_4 = value;
	}

	inline static int32_t get_offset_of_mInitializedInEditor_5() { return static_cast<int32_t>(offsetof(TrackableBehaviour_t2419079356, ___mInitializedInEditor_5)); }
	inline bool get_mInitializedInEditor_5() const { return ___mInitializedInEditor_5; }
	inline bool* get_address_of_mInitializedInEditor_5() { return &___mInitializedInEditor_5; }
	inline void set_mInitializedInEditor_5(bool value)
	{
		___mInitializedInEditor_5 = value;
	}

	inline static int32_t get_offset_of_mPreviousScale_6() { return static_cast<int32_t>(offsetof(TrackableBehaviour_t2419079356, ___mPreviousScale_6)); }
	inline Vector3_t2903530434  get_mPreviousScale_6() const { return ___mPreviousScale_6; }
	inline Vector3_t2903530434 * get_address_of_mPreviousScale_6() { return &___mPreviousScale_6; }
	inline void set_mPreviousScale_6(Vector3_t2903530434  value)
	{
		___mPreviousScale_6 = value;
	}

	inline static int32_t get_offset_of_mStatus_7() { return static_cast<int32_t>(offsetof(TrackableBehaviour_t2419079356, ___mStatus_7)); }
	inline int32_t get_mStatus_7() const { return ___mStatus_7; }
	inline int32_t* get_address_of_mStatus_7() { return &___mStatus_7; }
	inline void set_mStatus_7(int32_t value)
	{
		___mStatus_7 = value;
	}

	inline static int32_t get_offset_of_mTrackable_8() { return static_cast<int32_t>(offsetof(TrackableBehaviour_t2419079356, ___mTrackable_8)); }
	inline RuntimeObject* get_mTrackable_8() const { return ___mTrackable_8; }
	inline RuntimeObject** get_address_of_mTrackable_8() { return &___mTrackable_8; }
	inline void set_mTrackable_8(RuntimeObject* value)
	{
		___mTrackable_8 = value;
		Il2CppCodeGenWriteBarrier((&___mTrackable_8), value);
	}

	inline static int32_t get_offset_of_mTrackableEventHandlers_9() { return static_cast<int32_t>(offsetof(TrackableBehaviour_t2419079356, ___mTrackableEventHandlers_9)); }
	inline List_1_t3589236026 * get_mTrackableEventHandlers_9() const { return ___mTrackableEventHandlers_9; }
	inline List_1_t3589236026 ** get_address_of_mTrackableEventHandlers_9() { return &___mTrackableEventHandlers_9; }
	inline void set_mTrackableEventHandlers_9(List_1_t3589236026 * value)
	{
		___mTrackableEventHandlers_9 = value;
		Il2CppCodeGenWriteBarrier((&___mTrackableEventHandlers_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRACKABLEBEHAVIOUR_T2419079356_H
#ifndef VUFORIAABSTRACTBEHAVIOUR_T153010168_H
#define VUFORIAABSTRACTBEHAVIOUR_T153010168_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.VuforiaAbstractBehaviour
struct  VuforiaAbstractBehaviour_t153010168  : public MonoBehaviour_t345688271
{
public:
	// Vuforia.VuforiaARController/WorldCenterMode Vuforia.VuforiaAbstractBehaviour::mWorldCenterMode
	int32_t ___mWorldCenterMode_2;
	// Vuforia.TrackableBehaviour Vuforia.VuforiaAbstractBehaviour::mWorldCenter
	TrackableBehaviour_t2419079356 * ___mWorldCenter_3;
	// UnityEngine.Transform Vuforia.VuforiaAbstractBehaviour::mCentralAnchorPoint
	Transform_t3316442598 * ___mCentralAnchorPoint_4;
	// UnityEngine.Transform Vuforia.VuforiaAbstractBehaviour::mParentAnchorPoint
	Transform_t3316442598 * ___mParentAnchorPoint_5;
	// UnityEngine.Camera Vuforia.VuforiaAbstractBehaviour::mPrimaryCamera
	Camera_t3175186167 * ___mPrimaryCamera_6;
	// UnityEngine.Camera Vuforia.VuforiaAbstractBehaviour::mSecondaryCamera
	Camera_t3175186167 * ___mSecondaryCamera_7;
	// System.Boolean Vuforia.VuforiaAbstractBehaviour::mWereBindingFieldsExposed
	bool ___mWereBindingFieldsExposed_8;
	// System.Action Vuforia.VuforiaAbstractBehaviour::AwakeEvent
	Action_t3619184611 * ___AwakeEvent_11;
	// System.Action Vuforia.VuforiaAbstractBehaviour::OnEnableEvent
	Action_t3619184611 * ___OnEnableEvent_12;
	// System.Action Vuforia.VuforiaAbstractBehaviour::StartEvent
	Action_t3619184611 * ___StartEvent_13;
	// System.Action Vuforia.VuforiaAbstractBehaviour::UpdateEvent
	Action_t3619184611 * ___UpdateEvent_14;
	// System.Action Vuforia.VuforiaAbstractBehaviour::OnLevelWasLoadedEvent
	Action_t3619184611 * ___OnLevelWasLoadedEvent_15;
	// System.Action`1<System.Boolean> Vuforia.VuforiaAbstractBehaviour::OnApplicationPauseEvent
	Action_1_t2936672411 * ___OnApplicationPauseEvent_16;
	// System.Action Vuforia.VuforiaAbstractBehaviour::OnDisableEvent
	Action_t3619184611 * ___OnDisableEvent_17;
	// System.Action Vuforia.VuforiaAbstractBehaviour::OnDestroyEvent
	Action_t3619184611 * ___OnDestroyEvent_18;

public:
	inline static int32_t get_offset_of_mWorldCenterMode_2() { return static_cast<int32_t>(offsetof(VuforiaAbstractBehaviour_t153010168, ___mWorldCenterMode_2)); }
	inline int32_t get_mWorldCenterMode_2() const { return ___mWorldCenterMode_2; }
	inline int32_t* get_address_of_mWorldCenterMode_2() { return &___mWorldCenterMode_2; }
	inline void set_mWorldCenterMode_2(int32_t value)
	{
		___mWorldCenterMode_2 = value;
	}

	inline static int32_t get_offset_of_mWorldCenter_3() { return static_cast<int32_t>(offsetof(VuforiaAbstractBehaviour_t153010168, ___mWorldCenter_3)); }
	inline TrackableBehaviour_t2419079356 * get_mWorldCenter_3() const { return ___mWorldCenter_3; }
	inline TrackableBehaviour_t2419079356 ** get_address_of_mWorldCenter_3() { return &___mWorldCenter_3; }
	inline void set_mWorldCenter_3(TrackableBehaviour_t2419079356 * value)
	{
		___mWorldCenter_3 = value;
		Il2CppCodeGenWriteBarrier((&___mWorldCenter_3), value);
	}

	inline static int32_t get_offset_of_mCentralAnchorPoint_4() { return static_cast<int32_t>(offsetof(VuforiaAbstractBehaviour_t153010168, ___mCentralAnchorPoint_4)); }
	inline Transform_t3316442598 * get_mCentralAnchorPoint_4() const { return ___mCentralAnchorPoint_4; }
	inline Transform_t3316442598 ** get_address_of_mCentralAnchorPoint_4() { return &___mCentralAnchorPoint_4; }
	inline void set_mCentralAnchorPoint_4(Transform_t3316442598 * value)
	{
		___mCentralAnchorPoint_4 = value;
		Il2CppCodeGenWriteBarrier((&___mCentralAnchorPoint_4), value);
	}

	inline static int32_t get_offset_of_mParentAnchorPoint_5() { return static_cast<int32_t>(offsetof(VuforiaAbstractBehaviour_t153010168, ___mParentAnchorPoint_5)); }
	inline Transform_t3316442598 * get_mParentAnchorPoint_5() const { return ___mParentAnchorPoint_5; }
	inline Transform_t3316442598 ** get_address_of_mParentAnchorPoint_5() { return &___mParentAnchorPoint_5; }
	inline void set_mParentAnchorPoint_5(Transform_t3316442598 * value)
	{
		___mParentAnchorPoint_5 = value;
		Il2CppCodeGenWriteBarrier((&___mParentAnchorPoint_5), value);
	}

	inline static int32_t get_offset_of_mPrimaryCamera_6() { return static_cast<int32_t>(offsetof(VuforiaAbstractBehaviour_t153010168, ___mPrimaryCamera_6)); }
	inline Camera_t3175186167 * get_mPrimaryCamera_6() const { return ___mPrimaryCamera_6; }
	inline Camera_t3175186167 ** get_address_of_mPrimaryCamera_6() { return &___mPrimaryCamera_6; }
	inline void set_mPrimaryCamera_6(Camera_t3175186167 * value)
	{
		___mPrimaryCamera_6 = value;
		Il2CppCodeGenWriteBarrier((&___mPrimaryCamera_6), value);
	}

	inline static int32_t get_offset_of_mSecondaryCamera_7() { return static_cast<int32_t>(offsetof(VuforiaAbstractBehaviour_t153010168, ___mSecondaryCamera_7)); }
	inline Camera_t3175186167 * get_mSecondaryCamera_7() const { return ___mSecondaryCamera_7; }
	inline Camera_t3175186167 ** get_address_of_mSecondaryCamera_7() { return &___mSecondaryCamera_7; }
	inline void set_mSecondaryCamera_7(Camera_t3175186167 * value)
	{
		___mSecondaryCamera_7 = value;
		Il2CppCodeGenWriteBarrier((&___mSecondaryCamera_7), value);
	}

	inline static int32_t get_offset_of_mWereBindingFieldsExposed_8() { return static_cast<int32_t>(offsetof(VuforiaAbstractBehaviour_t153010168, ___mWereBindingFieldsExposed_8)); }
	inline bool get_mWereBindingFieldsExposed_8() const { return ___mWereBindingFieldsExposed_8; }
	inline bool* get_address_of_mWereBindingFieldsExposed_8() { return &___mWereBindingFieldsExposed_8; }
	inline void set_mWereBindingFieldsExposed_8(bool value)
	{
		___mWereBindingFieldsExposed_8 = value;
	}

	inline static int32_t get_offset_of_AwakeEvent_11() { return static_cast<int32_t>(offsetof(VuforiaAbstractBehaviour_t153010168, ___AwakeEvent_11)); }
	inline Action_t3619184611 * get_AwakeEvent_11() const { return ___AwakeEvent_11; }
	inline Action_t3619184611 ** get_address_of_AwakeEvent_11() { return &___AwakeEvent_11; }
	inline void set_AwakeEvent_11(Action_t3619184611 * value)
	{
		___AwakeEvent_11 = value;
		Il2CppCodeGenWriteBarrier((&___AwakeEvent_11), value);
	}

	inline static int32_t get_offset_of_OnEnableEvent_12() { return static_cast<int32_t>(offsetof(VuforiaAbstractBehaviour_t153010168, ___OnEnableEvent_12)); }
	inline Action_t3619184611 * get_OnEnableEvent_12() const { return ___OnEnableEvent_12; }
	inline Action_t3619184611 ** get_address_of_OnEnableEvent_12() { return &___OnEnableEvent_12; }
	inline void set_OnEnableEvent_12(Action_t3619184611 * value)
	{
		___OnEnableEvent_12 = value;
		Il2CppCodeGenWriteBarrier((&___OnEnableEvent_12), value);
	}

	inline static int32_t get_offset_of_StartEvent_13() { return static_cast<int32_t>(offsetof(VuforiaAbstractBehaviour_t153010168, ___StartEvent_13)); }
	inline Action_t3619184611 * get_StartEvent_13() const { return ___StartEvent_13; }
	inline Action_t3619184611 ** get_address_of_StartEvent_13() { return &___StartEvent_13; }
	inline void set_StartEvent_13(Action_t3619184611 * value)
	{
		___StartEvent_13 = value;
		Il2CppCodeGenWriteBarrier((&___StartEvent_13), value);
	}

	inline static int32_t get_offset_of_UpdateEvent_14() { return static_cast<int32_t>(offsetof(VuforiaAbstractBehaviour_t153010168, ___UpdateEvent_14)); }
	inline Action_t3619184611 * get_UpdateEvent_14() const { return ___UpdateEvent_14; }
	inline Action_t3619184611 ** get_address_of_UpdateEvent_14() { return &___UpdateEvent_14; }
	inline void set_UpdateEvent_14(Action_t3619184611 * value)
	{
		___UpdateEvent_14 = value;
		Il2CppCodeGenWriteBarrier((&___UpdateEvent_14), value);
	}

	inline static int32_t get_offset_of_OnLevelWasLoadedEvent_15() { return static_cast<int32_t>(offsetof(VuforiaAbstractBehaviour_t153010168, ___OnLevelWasLoadedEvent_15)); }
	inline Action_t3619184611 * get_OnLevelWasLoadedEvent_15() const { return ___OnLevelWasLoadedEvent_15; }
	inline Action_t3619184611 ** get_address_of_OnLevelWasLoadedEvent_15() { return &___OnLevelWasLoadedEvent_15; }
	inline void set_OnLevelWasLoadedEvent_15(Action_t3619184611 * value)
	{
		___OnLevelWasLoadedEvent_15 = value;
		Il2CppCodeGenWriteBarrier((&___OnLevelWasLoadedEvent_15), value);
	}

	inline static int32_t get_offset_of_OnApplicationPauseEvent_16() { return static_cast<int32_t>(offsetof(VuforiaAbstractBehaviour_t153010168, ___OnApplicationPauseEvent_16)); }
	inline Action_1_t2936672411 * get_OnApplicationPauseEvent_16() const { return ___OnApplicationPauseEvent_16; }
	inline Action_1_t2936672411 ** get_address_of_OnApplicationPauseEvent_16() { return &___OnApplicationPauseEvent_16; }
	inline void set_OnApplicationPauseEvent_16(Action_1_t2936672411 * value)
	{
		___OnApplicationPauseEvent_16 = value;
		Il2CppCodeGenWriteBarrier((&___OnApplicationPauseEvent_16), value);
	}

	inline static int32_t get_offset_of_OnDisableEvent_17() { return static_cast<int32_t>(offsetof(VuforiaAbstractBehaviour_t153010168, ___OnDisableEvent_17)); }
	inline Action_t3619184611 * get_OnDisableEvent_17() const { return ___OnDisableEvent_17; }
	inline Action_t3619184611 ** get_address_of_OnDisableEvent_17() { return &___OnDisableEvent_17; }
	inline void set_OnDisableEvent_17(Action_t3619184611 * value)
	{
		___OnDisableEvent_17 = value;
		Il2CppCodeGenWriteBarrier((&___OnDisableEvent_17), value);
	}

	inline static int32_t get_offset_of_OnDestroyEvent_18() { return static_cast<int32_t>(offsetof(VuforiaAbstractBehaviour_t153010168, ___OnDestroyEvent_18)); }
	inline Action_t3619184611 * get_OnDestroyEvent_18() const { return ___OnDestroyEvent_18; }
	inline Action_t3619184611 ** get_address_of_OnDestroyEvent_18() { return &___OnDestroyEvent_18; }
	inline void set_OnDestroyEvent_18(Action_t3619184611 * value)
	{
		___OnDestroyEvent_18 = value;
		Il2CppCodeGenWriteBarrier((&___OnDestroyEvent_18), value);
	}
};

struct VuforiaAbstractBehaviour_t153010168_StaticFields
{
public:
	// System.Action`1<Vuforia.VuforiaAbstractBehaviour> Vuforia.VuforiaAbstractBehaviour::BehaviourCreated
	Action_1_t138837510 * ___BehaviourCreated_9;
	// System.Action`1<Vuforia.VuforiaAbstractBehaviour> Vuforia.VuforiaAbstractBehaviour::BehaviourDestroyed
	Action_1_t138837510 * ___BehaviourDestroyed_10;

public:
	inline static int32_t get_offset_of_BehaviourCreated_9() { return static_cast<int32_t>(offsetof(VuforiaAbstractBehaviour_t153010168_StaticFields, ___BehaviourCreated_9)); }
	inline Action_1_t138837510 * get_BehaviourCreated_9() const { return ___BehaviourCreated_9; }
	inline Action_1_t138837510 ** get_address_of_BehaviourCreated_9() { return &___BehaviourCreated_9; }
	inline void set_BehaviourCreated_9(Action_1_t138837510 * value)
	{
		___BehaviourCreated_9 = value;
		Il2CppCodeGenWriteBarrier((&___BehaviourCreated_9), value);
	}

	inline static int32_t get_offset_of_BehaviourDestroyed_10() { return static_cast<int32_t>(offsetof(VuforiaAbstractBehaviour_t153010168_StaticFields, ___BehaviourDestroyed_10)); }
	inline Action_1_t138837510 * get_BehaviourDestroyed_10() const { return ___BehaviourDestroyed_10; }
	inline Action_1_t138837510 ** get_address_of_BehaviourDestroyed_10() { return &___BehaviourDestroyed_10; }
	inline void set_BehaviourDestroyed_10(Action_1_t138837510 * value)
	{
		___BehaviourDestroyed_10 = value;
		Il2CppCodeGenWriteBarrier((&___BehaviourDestroyed_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VUFORIAABSTRACTBEHAVIOUR_T153010168_H
#ifndef SMARTTERRAINTRACKABLEBEHAVIOUR_T4179131702_H
#define SMARTTERRAINTRACKABLEBEHAVIOUR_T4179131702_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.SmartTerrainTrackableBehaviour
struct  SmartTerrainTrackableBehaviour_t4179131702  : public TrackableBehaviour_t2419079356
{
public:
	// Vuforia.SmartTerrainTrackable Vuforia.SmartTerrainTrackableBehaviour::mSmartTerrainTrackable
	RuntimeObject* ___mSmartTerrainTrackable_10;
	// System.Boolean Vuforia.SmartTerrainTrackableBehaviour::mDisableAutomaticUpdates
	bool ___mDisableAutomaticUpdates_11;
	// UnityEngine.MeshFilter Vuforia.SmartTerrainTrackableBehaviour::mMeshFilterToUpdate
	MeshFilter_t137687116 * ___mMeshFilterToUpdate_12;
	// UnityEngine.MeshCollider Vuforia.SmartTerrainTrackableBehaviour::mMeshColliderToUpdate
	MeshCollider_t4172739389 * ___mMeshColliderToUpdate_13;

public:
	inline static int32_t get_offset_of_mSmartTerrainTrackable_10() { return static_cast<int32_t>(offsetof(SmartTerrainTrackableBehaviour_t4179131702, ___mSmartTerrainTrackable_10)); }
	inline RuntimeObject* get_mSmartTerrainTrackable_10() const { return ___mSmartTerrainTrackable_10; }
	inline RuntimeObject** get_address_of_mSmartTerrainTrackable_10() { return &___mSmartTerrainTrackable_10; }
	inline void set_mSmartTerrainTrackable_10(RuntimeObject* value)
	{
		___mSmartTerrainTrackable_10 = value;
		Il2CppCodeGenWriteBarrier((&___mSmartTerrainTrackable_10), value);
	}

	inline static int32_t get_offset_of_mDisableAutomaticUpdates_11() { return static_cast<int32_t>(offsetof(SmartTerrainTrackableBehaviour_t4179131702, ___mDisableAutomaticUpdates_11)); }
	inline bool get_mDisableAutomaticUpdates_11() const { return ___mDisableAutomaticUpdates_11; }
	inline bool* get_address_of_mDisableAutomaticUpdates_11() { return &___mDisableAutomaticUpdates_11; }
	inline void set_mDisableAutomaticUpdates_11(bool value)
	{
		___mDisableAutomaticUpdates_11 = value;
	}

	inline static int32_t get_offset_of_mMeshFilterToUpdate_12() { return static_cast<int32_t>(offsetof(SmartTerrainTrackableBehaviour_t4179131702, ___mMeshFilterToUpdate_12)); }
	inline MeshFilter_t137687116 * get_mMeshFilterToUpdate_12() const { return ___mMeshFilterToUpdate_12; }
	inline MeshFilter_t137687116 ** get_address_of_mMeshFilterToUpdate_12() { return &___mMeshFilterToUpdate_12; }
	inline void set_mMeshFilterToUpdate_12(MeshFilter_t137687116 * value)
	{
		___mMeshFilterToUpdate_12 = value;
		Il2CppCodeGenWriteBarrier((&___mMeshFilterToUpdate_12), value);
	}

	inline static int32_t get_offset_of_mMeshColliderToUpdate_13() { return static_cast<int32_t>(offsetof(SmartTerrainTrackableBehaviour_t4179131702, ___mMeshColliderToUpdate_13)); }
	inline MeshCollider_t4172739389 * get_mMeshColliderToUpdate_13() const { return ___mMeshColliderToUpdate_13; }
	inline MeshCollider_t4172739389 ** get_address_of_mMeshColliderToUpdate_13() { return &___mMeshColliderToUpdate_13; }
	inline void set_mMeshColliderToUpdate_13(MeshCollider_t4172739389 * value)
	{
		___mMeshColliderToUpdate_13 = value;
		Il2CppCodeGenWriteBarrier((&___mMeshColliderToUpdate_13), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SMARTTERRAINTRACKABLEBEHAVIOUR_T4179131702_H
#ifndef DATASETTRACKABLEBEHAVIOUR_T3960979584_H
#define DATASETTRACKABLEBEHAVIOUR_T3960979584_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.DataSetTrackableBehaviour
struct  DataSetTrackableBehaviour_t3960979584  : public TrackableBehaviour_t2419079356
{
public:
	// System.String Vuforia.DataSetTrackableBehaviour::mDataSetPath
	String_t* ___mDataSetPath_10;
	// System.Boolean Vuforia.DataSetTrackableBehaviour::mExtendedTracking
	bool ___mExtendedTracking_11;
	// System.Boolean Vuforia.DataSetTrackableBehaviour::mInitializeSmartTerrain
	bool ___mInitializeSmartTerrain_12;
	// Vuforia.ReconstructionFromTargetAbstractBehaviour Vuforia.DataSetTrackableBehaviour::mReconstructionToInitialize
	ReconstructionFromTargetAbstractBehaviour_t2676804737 * ___mReconstructionToInitialize_13;
	// UnityEngine.Vector3 Vuforia.DataSetTrackableBehaviour::mSmartTerrainOccluderBoundsMin
	Vector3_t2903530434  ___mSmartTerrainOccluderBoundsMin_14;
	// UnityEngine.Vector3 Vuforia.DataSetTrackableBehaviour::mSmartTerrainOccluderBoundsMax
	Vector3_t2903530434  ___mSmartTerrainOccluderBoundsMax_15;
	// System.Boolean Vuforia.DataSetTrackableBehaviour::mIsSmartTerrainOccluderOffset
	bool ___mIsSmartTerrainOccluderOffset_16;
	// UnityEngine.Vector3 Vuforia.DataSetTrackableBehaviour::mSmartTerrainOccluderOffset
	Vector3_t2903530434  ___mSmartTerrainOccluderOffset_17;
	// UnityEngine.Quaternion Vuforia.DataSetTrackableBehaviour::mSmartTerrainOccluderRotation
	Quaternion_t754065749  ___mSmartTerrainOccluderRotation_18;
	// System.Boolean Vuforia.DataSetTrackableBehaviour::mAutoSetOccluderFromTargetSize
	bool ___mAutoSetOccluderFromTargetSize_19;

public:
	inline static int32_t get_offset_of_mDataSetPath_10() { return static_cast<int32_t>(offsetof(DataSetTrackableBehaviour_t3960979584, ___mDataSetPath_10)); }
	inline String_t* get_mDataSetPath_10() const { return ___mDataSetPath_10; }
	inline String_t** get_address_of_mDataSetPath_10() { return &___mDataSetPath_10; }
	inline void set_mDataSetPath_10(String_t* value)
	{
		___mDataSetPath_10 = value;
		Il2CppCodeGenWriteBarrier((&___mDataSetPath_10), value);
	}

	inline static int32_t get_offset_of_mExtendedTracking_11() { return static_cast<int32_t>(offsetof(DataSetTrackableBehaviour_t3960979584, ___mExtendedTracking_11)); }
	inline bool get_mExtendedTracking_11() const { return ___mExtendedTracking_11; }
	inline bool* get_address_of_mExtendedTracking_11() { return &___mExtendedTracking_11; }
	inline void set_mExtendedTracking_11(bool value)
	{
		___mExtendedTracking_11 = value;
	}

	inline static int32_t get_offset_of_mInitializeSmartTerrain_12() { return static_cast<int32_t>(offsetof(DataSetTrackableBehaviour_t3960979584, ___mInitializeSmartTerrain_12)); }
	inline bool get_mInitializeSmartTerrain_12() const { return ___mInitializeSmartTerrain_12; }
	inline bool* get_address_of_mInitializeSmartTerrain_12() { return &___mInitializeSmartTerrain_12; }
	inline void set_mInitializeSmartTerrain_12(bool value)
	{
		___mInitializeSmartTerrain_12 = value;
	}

	inline static int32_t get_offset_of_mReconstructionToInitialize_13() { return static_cast<int32_t>(offsetof(DataSetTrackableBehaviour_t3960979584, ___mReconstructionToInitialize_13)); }
	inline ReconstructionFromTargetAbstractBehaviour_t2676804737 * get_mReconstructionToInitialize_13() const { return ___mReconstructionToInitialize_13; }
	inline ReconstructionFromTargetAbstractBehaviour_t2676804737 ** get_address_of_mReconstructionToInitialize_13() { return &___mReconstructionToInitialize_13; }
	inline void set_mReconstructionToInitialize_13(ReconstructionFromTargetAbstractBehaviour_t2676804737 * value)
	{
		___mReconstructionToInitialize_13 = value;
		Il2CppCodeGenWriteBarrier((&___mReconstructionToInitialize_13), value);
	}

	inline static int32_t get_offset_of_mSmartTerrainOccluderBoundsMin_14() { return static_cast<int32_t>(offsetof(DataSetTrackableBehaviour_t3960979584, ___mSmartTerrainOccluderBoundsMin_14)); }
	inline Vector3_t2903530434  get_mSmartTerrainOccluderBoundsMin_14() const { return ___mSmartTerrainOccluderBoundsMin_14; }
	inline Vector3_t2903530434 * get_address_of_mSmartTerrainOccluderBoundsMin_14() { return &___mSmartTerrainOccluderBoundsMin_14; }
	inline void set_mSmartTerrainOccluderBoundsMin_14(Vector3_t2903530434  value)
	{
		___mSmartTerrainOccluderBoundsMin_14 = value;
	}

	inline static int32_t get_offset_of_mSmartTerrainOccluderBoundsMax_15() { return static_cast<int32_t>(offsetof(DataSetTrackableBehaviour_t3960979584, ___mSmartTerrainOccluderBoundsMax_15)); }
	inline Vector3_t2903530434  get_mSmartTerrainOccluderBoundsMax_15() const { return ___mSmartTerrainOccluderBoundsMax_15; }
	inline Vector3_t2903530434 * get_address_of_mSmartTerrainOccluderBoundsMax_15() { return &___mSmartTerrainOccluderBoundsMax_15; }
	inline void set_mSmartTerrainOccluderBoundsMax_15(Vector3_t2903530434  value)
	{
		___mSmartTerrainOccluderBoundsMax_15 = value;
	}

	inline static int32_t get_offset_of_mIsSmartTerrainOccluderOffset_16() { return static_cast<int32_t>(offsetof(DataSetTrackableBehaviour_t3960979584, ___mIsSmartTerrainOccluderOffset_16)); }
	inline bool get_mIsSmartTerrainOccluderOffset_16() const { return ___mIsSmartTerrainOccluderOffset_16; }
	inline bool* get_address_of_mIsSmartTerrainOccluderOffset_16() { return &___mIsSmartTerrainOccluderOffset_16; }
	inline void set_mIsSmartTerrainOccluderOffset_16(bool value)
	{
		___mIsSmartTerrainOccluderOffset_16 = value;
	}

	inline static int32_t get_offset_of_mSmartTerrainOccluderOffset_17() { return static_cast<int32_t>(offsetof(DataSetTrackableBehaviour_t3960979584, ___mSmartTerrainOccluderOffset_17)); }
	inline Vector3_t2903530434  get_mSmartTerrainOccluderOffset_17() const { return ___mSmartTerrainOccluderOffset_17; }
	inline Vector3_t2903530434 * get_address_of_mSmartTerrainOccluderOffset_17() { return &___mSmartTerrainOccluderOffset_17; }
	inline void set_mSmartTerrainOccluderOffset_17(Vector3_t2903530434  value)
	{
		___mSmartTerrainOccluderOffset_17 = value;
	}

	inline static int32_t get_offset_of_mSmartTerrainOccluderRotation_18() { return static_cast<int32_t>(offsetof(DataSetTrackableBehaviour_t3960979584, ___mSmartTerrainOccluderRotation_18)); }
	inline Quaternion_t754065749  get_mSmartTerrainOccluderRotation_18() const { return ___mSmartTerrainOccluderRotation_18; }
	inline Quaternion_t754065749 * get_address_of_mSmartTerrainOccluderRotation_18() { return &___mSmartTerrainOccluderRotation_18; }
	inline void set_mSmartTerrainOccluderRotation_18(Quaternion_t754065749  value)
	{
		___mSmartTerrainOccluderRotation_18 = value;
	}

	inline static int32_t get_offset_of_mAutoSetOccluderFromTargetSize_19() { return static_cast<int32_t>(offsetof(DataSetTrackableBehaviour_t3960979584, ___mAutoSetOccluderFromTargetSize_19)); }
	inline bool get_mAutoSetOccluderFromTargetSize_19() const { return ___mAutoSetOccluderFromTargetSize_19; }
	inline bool* get_address_of_mAutoSetOccluderFromTargetSize_19() { return &___mAutoSetOccluderFromTargetSize_19; }
	inline void set_mAutoSetOccluderFromTargetSize_19(bool value)
	{
		___mAutoSetOccluderFromTargetSize_19 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATASETTRACKABLEBEHAVIOUR_T3960979584_H
#ifndef SURFACEABSTRACTBEHAVIOUR_T272541176_H
#define SURFACEABSTRACTBEHAVIOUR_T272541176_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.SurfaceAbstractBehaviour
struct  SurfaceAbstractBehaviour_t272541176  : public SmartTerrainTrackableBehaviour_t4179131702
{
public:
	// Vuforia.Surface Vuforia.SurfaceAbstractBehaviour::mSurface
	RuntimeObject* ___mSurface_14;

public:
	inline static int32_t get_offset_of_mSurface_14() { return static_cast<int32_t>(offsetof(SurfaceAbstractBehaviour_t272541176, ___mSurface_14)); }
	inline RuntimeObject* get_mSurface_14() const { return ___mSurface_14; }
	inline RuntimeObject** get_address_of_mSurface_14() { return &___mSurface_14; }
	inline void set_mSurface_14(RuntimeObject* value)
	{
		___mSurface_14 = value;
		Il2CppCodeGenWriteBarrier((&___mSurface_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SURFACEABSTRACTBEHAVIOUR_T272541176_H
#ifndef CYLINDERTARGETABSTRACTBEHAVIOUR_T2158579059_H
#define CYLINDERTARGETABSTRACTBEHAVIOUR_T2158579059_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.CylinderTargetAbstractBehaviour
struct  CylinderTargetAbstractBehaviour_t2158579059  : public DataSetTrackableBehaviour_t3960979584
{
public:
	// Vuforia.CylinderTarget Vuforia.CylinderTargetAbstractBehaviour::mCylinderTarget
	RuntimeObject* ___mCylinderTarget_20;
	// System.Single Vuforia.CylinderTargetAbstractBehaviour::mTopDiameterRatio
	float ___mTopDiameterRatio_21;
	// System.Single Vuforia.CylinderTargetAbstractBehaviour::mBottomDiameterRatio
	float ___mBottomDiameterRatio_22;
	// System.Single Vuforia.CylinderTargetAbstractBehaviour::mSideLength
	float ___mSideLength_23;
	// System.Single Vuforia.CylinderTargetAbstractBehaviour::mTopDiameter
	float ___mTopDiameter_24;
	// System.Single Vuforia.CylinderTargetAbstractBehaviour::mBottomDiameter
	float ___mBottomDiameter_25;
	// System.Int32 Vuforia.CylinderTargetAbstractBehaviour::mFrameIndex
	int32_t ___mFrameIndex_26;
	// System.Int32 Vuforia.CylinderTargetAbstractBehaviour::mUpdateFrameIndex
	int32_t ___mUpdateFrameIndex_27;
	// System.Single Vuforia.CylinderTargetAbstractBehaviour::mFutureScale
	float ___mFutureScale_28;
	// System.Single Vuforia.CylinderTargetAbstractBehaviour::mLastTransformScale
	float ___mLastTransformScale_29;

public:
	inline static int32_t get_offset_of_mCylinderTarget_20() { return static_cast<int32_t>(offsetof(CylinderTargetAbstractBehaviour_t2158579059, ___mCylinderTarget_20)); }
	inline RuntimeObject* get_mCylinderTarget_20() const { return ___mCylinderTarget_20; }
	inline RuntimeObject** get_address_of_mCylinderTarget_20() { return &___mCylinderTarget_20; }
	inline void set_mCylinderTarget_20(RuntimeObject* value)
	{
		___mCylinderTarget_20 = value;
		Il2CppCodeGenWriteBarrier((&___mCylinderTarget_20), value);
	}

	inline static int32_t get_offset_of_mTopDiameterRatio_21() { return static_cast<int32_t>(offsetof(CylinderTargetAbstractBehaviour_t2158579059, ___mTopDiameterRatio_21)); }
	inline float get_mTopDiameterRatio_21() const { return ___mTopDiameterRatio_21; }
	inline float* get_address_of_mTopDiameterRatio_21() { return &___mTopDiameterRatio_21; }
	inline void set_mTopDiameterRatio_21(float value)
	{
		___mTopDiameterRatio_21 = value;
	}

	inline static int32_t get_offset_of_mBottomDiameterRatio_22() { return static_cast<int32_t>(offsetof(CylinderTargetAbstractBehaviour_t2158579059, ___mBottomDiameterRatio_22)); }
	inline float get_mBottomDiameterRatio_22() const { return ___mBottomDiameterRatio_22; }
	inline float* get_address_of_mBottomDiameterRatio_22() { return &___mBottomDiameterRatio_22; }
	inline void set_mBottomDiameterRatio_22(float value)
	{
		___mBottomDiameterRatio_22 = value;
	}

	inline static int32_t get_offset_of_mSideLength_23() { return static_cast<int32_t>(offsetof(CylinderTargetAbstractBehaviour_t2158579059, ___mSideLength_23)); }
	inline float get_mSideLength_23() const { return ___mSideLength_23; }
	inline float* get_address_of_mSideLength_23() { return &___mSideLength_23; }
	inline void set_mSideLength_23(float value)
	{
		___mSideLength_23 = value;
	}

	inline static int32_t get_offset_of_mTopDiameter_24() { return static_cast<int32_t>(offsetof(CylinderTargetAbstractBehaviour_t2158579059, ___mTopDiameter_24)); }
	inline float get_mTopDiameter_24() const { return ___mTopDiameter_24; }
	inline float* get_address_of_mTopDiameter_24() { return &___mTopDiameter_24; }
	inline void set_mTopDiameter_24(float value)
	{
		___mTopDiameter_24 = value;
	}

	inline static int32_t get_offset_of_mBottomDiameter_25() { return static_cast<int32_t>(offsetof(CylinderTargetAbstractBehaviour_t2158579059, ___mBottomDiameter_25)); }
	inline float get_mBottomDiameter_25() const { return ___mBottomDiameter_25; }
	inline float* get_address_of_mBottomDiameter_25() { return &___mBottomDiameter_25; }
	inline void set_mBottomDiameter_25(float value)
	{
		___mBottomDiameter_25 = value;
	}

	inline static int32_t get_offset_of_mFrameIndex_26() { return static_cast<int32_t>(offsetof(CylinderTargetAbstractBehaviour_t2158579059, ___mFrameIndex_26)); }
	inline int32_t get_mFrameIndex_26() const { return ___mFrameIndex_26; }
	inline int32_t* get_address_of_mFrameIndex_26() { return &___mFrameIndex_26; }
	inline void set_mFrameIndex_26(int32_t value)
	{
		___mFrameIndex_26 = value;
	}

	inline static int32_t get_offset_of_mUpdateFrameIndex_27() { return static_cast<int32_t>(offsetof(CylinderTargetAbstractBehaviour_t2158579059, ___mUpdateFrameIndex_27)); }
	inline int32_t get_mUpdateFrameIndex_27() const { return ___mUpdateFrameIndex_27; }
	inline int32_t* get_address_of_mUpdateFrameIndex_27() { return &___mUpdateFrameIndex_27; }
	inline void set_mUpdateFrameIndex_27(int32_t value)
	{
		___mUpdateFrameIndex_27 = value;
	}

	inline static int32_t get_offset_of_mFutureScale_28() { return static_cast<int32_t>(offsetof(CylinderTargetAbstractBehaviour_t2158579059, ___mFutureScale_28)); }
	inline float get_mFutureScale_28() const { return ___mFutureScale_28; }
	inline float* get_address_of_mFutureScale_28() { return &___mFutureScale_28; }
	inline void set_mFutureScale_28(float value)
	{
		___mFutureScale_28 = value;
	}

	inline static int32_t get_offset_of_mLastTransformScale_29() { return static_cast<int32_t>(offsetof(CylinderTargetAbstractBehaviour_t2158579059, ___mLastTransformScale_29)); }
	inline float get_mLastTransformScale_29() const { return ___mLastTransformScale_29; }
	inline float* get_address_of_mLastTransformScale_29() { return &___mLastTransformScale_29; }
	inline void set_mLastTransformScale_29(float value)
	{
		___mLastTransformScale_29 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CYLINDERTARGETABSTRACTBEHAVIOUR_T2158579059_H
#ifndef VUMARKABSTRACTBEHAVIOUR_T2652615854_H
#define VUMARKABSTRACTBEHAVIOUR_T2652615854_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.VuMarkAbstractBehaviour
struct  VuMarkAbstractBehaviour_t2652615854  : public DataSetTrackableBehaviour_t3960979584
{
public:
	// System.Single Vuforia.VuMarkAbstractBehaviour::mAspectRatio
	float ___mAspectRatio_20;
	// System.Single Vuforia.VuMarkAbstractBehaviour::mWidth
	float ___mWidth_21;
	// System.Single Vuforia.VuMarkAbstractBehaviour::mHeight
	float ___mHeight_22;
	// System.String Vuforia.VuMarkAbstractBehaviour::mPreviewImage
	String_t* ___mPreviewImage_23;
	// Vuforia.InstanceIdType Vuforia.VuMarkAbstractBehaviour::mIdType
	int32_t ___mIdType_24;
	// System.Int32 Vuforia.VuMarkAbstractBehaviour::mIdLength
	int32_t ___mIdLength_25;
	// UnityEngine.Rect Vuforia.VuMarkAbstractBehaviour::mBoundingBox
	Rect_t3436776195  ___mBoundingBox_26;
	// UnityEngine.Vector2 Vuforia.VuMarkAbstractBehaviour::mOrigin
	Vector2_t3577333262  ___mOrigin_27;
	// System.Boolean Vuforia.VuMarkAbstractBehaviour::mTrackingFromRuntimeAppearance
	bool ___mTrackingFromRuntimeAppearance_28;
	// Vuforia.VuMarkTemplate Vuforia.VuMarkAbstractBehaviour::mVuMarkTemplate
	RuntimeObject* ___mVuMarkTemplate_29;
	// Vuforia.VuMarkTarget Vuforia.VuMarkAbstractBehaviour::mVuMarkTarget
	RuntimeObject* ___mVuMarkTarget_30;
	// System.Int32 Vuforia.VuMarkAbstractBehaviour::mVuMarkResultId
	int32_t ___mVuMarkResultId_31;
	// System.Action Vuforia.VuMarkAbstractBehaviour::mOnTargetAssigned
	Action_t3619184611 * ___mOnTargetAssigned_32;
	// System.Action Vuforia.VuMarkAbstractBehaviour::mOnTargetLost
	Action_t3619184611 * ___mOnTargetLost_33;
	// System.Single Vuforia.VuMarkAbstractBehaviour::mLastTransformScale
	float ___mLastTransformScale_34;
	// UnityEngine.Vector2 Vuforia.VuMarkAbstractBehaviour::mLastSize
	Vector2_t3577333262  ___mLastSize_35;

public:
	inline static int32_t get_offset_of_mAspectRatio_20() { return static_cast<int32_t>(offsetof(VuMarkAbstractBehaviour_t2652615854, ___mAspectRatio_20)); }
	inline float get_mAspectRatio_20() const { return ___mAspectRatio_20; }
	inline float* get_address_of_mAspectRatio_20() { return &___mAspectRatio_20; }
	inline void set_mAspectRatio_20(float value)
	{
		___mAspectRatio_20 = value;
	}

	inline static int32_t get_offset_of_mWidth_21() { return static_cast<int32_t>(offsetof(VuMarkAbstractBehaviour_t2652615854, ___mWidth_21)); }
	inline float get_mWidth_21() const { return ___mWidth_21; }
	inline float* get_address_of_mWidth_21() { return &___mWidth_21; }
	inline void set_mWidth_21(float value)
	{
		___mWidth_21 = value;
	}

	inline static int32_t get_offset_of_mHeight_22() { return static_cast<int32_t>(offsetof(VuMarkAbstractBehaviour_t2652615854, ___mHeight_22)); }
	inline float get_mHeight_22() const { return ___mHeight_22; }
	inline float* get_address_of_mHeight_22() { return &___mHeight_22; }
	inline void set_mHeight_22(float value)
	{
		___mHeight_22 = value;
	}

	inline static int32_t get_offset_of_mPreviewImage_23() { return static_cast<int32_t>(offsetof(VuMarkAbstractBehaviour_t2652615854, ___mPreviewImage_23)); }
	inline String_t* get_mPreviewImage_23() const { return ___mPreviewImage_23; }
	inline String_t** get_address_of_mPreviewImage_23() { return &___mPreviewImage_23; }
	inline void set_mPreviewImage_23(String_t* value)
	{
		___mPreviewImage_23 = value;
		Il2CppCodeGenWriteBarrier((&___mPreviewImage_23), value);
	}

	inline static int32_t get_offset_of_mIdType_24() { return static_cast<int32_t>(offsetof(VuMarkAbstractBehaviour_t2652615854, ___mIdType_24)); }
	inline int32_t get_mIdType_24() const { return ___mIdType_24; }
	inline int32_t* get_address_of_mIdType_24() { return &___mIdType_24; }
	inline void set_mIdType_24(int32_t value)
	{
		___mIdType_24 = value;
	}

	inline static int32_t get_offset_of_mIdLength_25() { return static_cast<int32_t>(offsetof(VuMarkAbstractBehaviour_t2652615854, ___mIdLength_25)); }
	inline int32_t get_mIdLength_25() const { return ___mIdLength_25; }
	inline int32_t* get_address_of_mIdLength_25() { return &___mIdLength_25; }
	inline void set_mIdLength_25(int32_t value)
	{
		___mIdLength_25 = value;
	}

	inline static int32_t get_offset_of_mBoundingBox_26() { return static_cast<int32_t>(offsetof(VuMarkAbstractBehaviour_t2652615854, ___mBoundingBox_26)); }
	inline Rect_t3436776195  get_mBoundingBox_26() const { return ___mBoundingBox_26; }
	inline Rect_t3436776195 * get_address_of_mBoundingBox_26() { return &___mBoundingBox_26; }
	inline void set_mBoundingBox_26(Rect_t3436776195  value)
	{
		___mBoundingBox_26 = value;
	}

	inline static int32_t get_offset_of_mOrigin_27() { return static_cast<int32_t>(offsetof(VuMarkAbstractBehaviour_t2652615854, ___mOrigin_27)); }
	inline Vector2_t3577333262  get_mOrigin_27() const { return ___mOrigin_27; }
	inline Vector2_t3577333262 * get_address_of_mOrigin_27() { return &___mOrigin_27; }
	inline void set_mOrigin_27(Vector2_t3577333262  value)
	{
		___mOrigin_27 = value;
	}

	inline static int32_t get_offset_of_mTrackingFromRuntimeAppearance_28() { return static_cast<int32_t>(offsetof(VuMarkAbstractBehaviour_t2652615854, ___mTrackingFromRuntimeAppearance_28)); }
	inline bool get_mTrackingFromRuntimeAppearance_28() const { return ___mTrackingFromRuntimeAppearance_28; }
	inline bool* get_address_of_mTrackingFromRuntimeAppearance_28() { return &___mTrackingFromRuntimeAppearance_28; }
	inline void set_mTrackingFromRuntimeAppearance_28(bool value)
	{
		___mTrackingFromRuntimeAppearance_28 = value;
	}

	inline static int32_t get_offset_of_mVuMarkTemplate_29() { return static_cast<int32_t>(offsetof(VuMarkAbstractBehaviour_t2652615854, ___mVuMarkTemplate_29)); }
	inline RuntimeObject* get_mVuMarkTemplate_29() const { return ___mVuMarkTemplate_29; }
	inline RuntimeObject** get_address_of_mVuMarkTemplate_29() { return &___mVuMarkTemplate_29; }
	inline void set_mVuMarkTemplate_29(RuntimeObject* value)
	{
		___mVuMarkTemplate_29 = value;
		Il2CppCodeGenWriteBarrier((&___mVuMarkTemplate_29), value);
	}

	inline static int32_t get_offset_of_mVuMarkTarget_30() { return static_cast<int32_t>(offsetof(VuMarkAbstractBehaviour_t2652615854, ___mVuMarkTarget_30)); }
	inline RuntimeObject* get_mVuMarkTarget_30() const { return ___mVuMarkTarget_30; }
	inline RuntimeObject** get_address_of_mVuMarkTarget_30() { return &___mVuMarkTarget_30; }
	inline void set_mVuMarkTarget_30(RuntimeObject* value)
	{
		___mVuMarkTarget_30 = value;
		Il2CppCodeGenWriteBarrier((&___mVuMarkTarget_30), value);
	}

	inline static int32_t get_offset_of_mVuMarkResultId_31() { return static_cast<int32_t>(offsetof(VuMarkAbstractBehaviour_t2652615854, ___mVuMarkResultId_31)); }
	inline int32_t get_mVuMarkResultId_31() const { return ___mVuMarkResultId_31; }
	inline int32_t* get_address_of_mVuMarkResultId_31() { return &___mVuMarkResultId_31; }
	inline void set_mVuMarkResultId_31(int32_t value)
	{
		___mVuMarkResultId_31 = value;
	}

	inline static int32_t get_offset_of_mOnTargetAssigned_32() { return static_cast<int32_t>(offsetof(VuMarkAbstractBehaviour_t2652615854, ___mOnTargetAssigned_32)); }
	inline Action_t3619184611 * get_mOnTargetAssigned_32() const { return ___mOnTargetAssigned_32; }
	inline Action_t3619184611 ** get_address_of_mOnTargetAssigned_32() { return &___mOnTargetAssigned_32; }
	inline void set_mOnTargetAssigned_32(Action_t3619184611 * value)
	{
		___mOnTargetAssigned_32 = value;
		Il2CppCodeGenWriteBarrier((&___mOnTargetAssigned_32), value);
	}

	inline static int32_t get_offset_of_mOnTargetLost_33() { return static_cast<int32_t>(offsetof(VuMarkAbstractBehaviour_t2652615854, ___mOnTargetLost_33)); }
	inline Action_t3619184611 * get_mOnTargetLost_33() const { return ___mOnTargetLost_33; }
	inline Action_t3619184611 ** get_address_of_mOnTargetLost_33() { return &___mOnTargetLost_33; }
	inline void set_mOnTargetLost_33(Action_t3619184611 * value)
	{
		___mOnTargetLost_33 = value;
		Il2CppCodeGenWriteBarrier((&___mOnTargetLost_33), value);
	}

	inline static int32_t get_offset_of_mLastTransformScale_34() { return static_cast<int32_t>(offsetof(VuMarkAbstractBehaviour_t2652615854, ___mLastTransformScale_34)); }
	inline float get_mLastTransformScale_34() const { return ___mLastTransformScale_34; }
	inline float* get_address_of_mLastTransformScale_34() { return &___mLastTransformScale_34; }
	inline void set_mLastTransformScale_34(float value)
	{
		___mLastTransformScale_34 = value;
	}

	inline static int32_t get_offset_of_mLastSize_35() { return static_cast<int32_t>(offsetof(VuMarkAbstractBehaviour_t2652615854, ___mLastSize_35)); }
	inline Vector2_t3577333262  get_mLastSize_35() const { return ___mLastSize_35; }
	inline Vector2_t3577333262 * get_address_of_mLastSize_35() { return &___mLastSize_35; }
	inline void set_mLastSize_35(Vector2_t3577333262  value)
	{
		___mLastSize_35 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VUMARKABSTRACTBEHAVIOUR_T2652615854_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1900 = { sizeof (VuforiaAbstractBehaviour_t153010168), -1, sizeof(VuforiaAbstractBehaviour_t153010168_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1900[17] = 
{
	VuforiaAbstractBehaviour_t153010168::get_offset_of_mWorldCenterMode_2(),
	VuforiaAbstractBehaviour_t153010168::get_offset_of_mWorldCenter_3(),
	VuforiaAbstractBehaviour_t153010168::get_offset_of_mCentralAnchorPoint_4(),
	VuforiaAbstractBehaviour_t153010168::get_offset_of_mParentAnchorPoint_5(),
	VuforiaAbstractBehaviour_t153010168::get_offset_of_mPrimaryCamera_6(),
	VuforiaAbstractBehaviour_t153010168::get_offset_of_mSecondaryCamera_7(),
	VuforiaAbstractBehaviour_t153010168::get_offset_of_mWereBindingFieldsExposed_8(),
	VuforiaAbstractBehaviour_t153010168_StaticFields::get_offset_of_BehaviourCreated_9(),
	VuforiaAbstractBehaviour_t153010168_StaticFields::get_offset_of_BehaviourDestroyed_10(),
	VuforiaAbstractBehaviour_t153010168::get_offset_of_AwakeEvent_11(),
	VuforiaAbstractBehaviour_t153010168::get_offset_of_OnEnableEvent_12(),
	VuforiaAbstractBehaviour_t153010168::get_offset_of_StartEvent_13(),
	VuforiaAbstractBehaviour_t153010168::get_offset_of_UpdateEvent_14(),
	VuforiaAbstractBehaviour_t153010168::get_offset_of_OnLevelWasLoadedEvent_15(),
	VuforiaAbstractBehaviour_t153010168::get_offset_of_OnApplicationPauseEvent_16(),
	VuforiaAbstractBehaviour_t153010168::get_offset_of_OnDisableEvent_17(),
	VuforiaAbstractBehaviour_t153010168::get_offset_of_OnDestroyEvent_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1901 = { sizeof (VuforiaAbstractConfiguration_t974438950), -1, sizeof(VuforiaAbstractConfiguration_t974438950_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1901[9] = 
{
	VuforiaAbstractConfiguration_t974438950_StaticFields::get_offset_of_mInstance_2(),
	VuforiaAbstractConfiguration_t974438950_StaticFields::get_offset_of_mPadlock_3(),
	VuforiaAbstractConfiguration_t974438950::get_offset_of_vuforia_4(),
	VuforiaAbstractConfiguration_t974438950::get_offset_of_digitalEyewear_5(),
	VuforiaAbstractConfiguration_t974438950::get_offset_of_databaseLoad_6(),
	VuforiaAbstractConfiguration_t974438950::get_offset_of_videoBackground_7(),
	VuforiaAbstractConfiguration_t974438950::get_offset_of_smartTerrainTracker_8(),
	VuforiaAbstractConfiguration_t974438950::get_offset_of_deviceTracker_9(),
	VuforiaAbstractConfiguration_t974438950::get_offset_of_webcam_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1902 = { sizeof (GenericVuforiaConfiguration_t1574456586), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1902[8] = 
{
	GenericVuforiaConfiguration_t1574456586::get_offset_of_vuforiaLicenseKey_0(),
	GenericVuforiaConfiguration_t1574456586::get_offset_of_delayedInitialization_1(),
	GenericVuforiaConfiguration_t1574456586::get_offset_of_cameraDeviceModeSetting_2(),
	GenericVuforiaConfiguration_t1574456586::get_offset_of_maxSimultaneousImageTargets_3(),
	GenericVuforiaConfiguration_t1574456586::get_offset_of_maxSimultaneousObjectTargets_4(),
	GenericVuforiaConfiguration_t1574456586::get_offset_of_useDelayedLoadingObjectTargets_5(),
	GenericVuforiaConfiguration_t1574456586::get_offset_of_cameraDirection_6(),
	GenericVuforiaConfiguration_t1574456586::get_offset_of_mirrorVideoBackground_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1903 = { sizeof (DigitalEyewearConfiguration_t3649657388), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1903[10] = 
{
	DigitalEyewearConfiguration_t3649657388::get_offset_of_cameraOffset_0(),
	DigitalEyewearConfiguration_t3649657388::get_offset_of_distortionRenderingMode_1(),
	DigitalEyewearConfiguration_t3649657388::get_offset_of_distortionRenderingLayer_2(),
	DigitalEyewearConfiguration_t3649657388::get_offset_of_eyewearType_3(),
	DigitalEyewearConfiguration_t3649657388::get_offset_of_stereoFramework_4(),
	DigitalEyewearConfiguration_t3649657388::get_offset_of_seeThroughConfiguration_5(),
	DigitalEyewearConfiguration_t3649657388::get_offset_of_viewerName_6(),
	DigitalEyewearConfiguration_t3649657388::get_offset_of_viewerManufacturer_7(),
	DigitalEyewearConfiguration_t3649657388::get_offset_of_useCustomViewer_8(),
	DigitalEyewearConfiguration_t3649657388::get_offset_of_customViewer_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1904 = { sizeof (DatabaseLoadConfiguration_t3827205120), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1904[2] = 
{
	DatabaseLoadConfiguration_t3827205120::get_offset_of_dataSetsToLoad_0(),
	DatabaseLoadConfiguration_t3827205120::get_offset_of_dataSetsToActivate_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1905 = { sizeof (VideoBackgroundConfiguration_t2579534243), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1905[3] = 
{
	VideoBackgroundConfiguration_t2579534243::get_offset_of_clippingMode_0(),
	VideoBackgroundConfiguration_t2579534243::get_offset_of_matteShader_1(),
	VideoBackgroundConfiguration_t2579534243::get_offset_of_videoBackgroundEnabled_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1906 = { sizeof (TrackerConfiguration_t3022484256), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1906[2] = 
{
	TrackerConfiguration_t3022484256::get_offset_of_autoInitTracker_0(),
	TrackerConfiguration_t3022484256::get_offset_of_autoStartTracker_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1907 = { sizeof (SmartTerrainTrackerConfiguration_t657164935), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1907[2] = 
{
	SmartTerrainTrackerConfiguration_t657164935::get_offset_of_autoInitBuilder_2(),
	SmartTerrainTrackerConfiguration_t657164935::get_offset_of_sceneUnitsToMillimeter_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1908 = { sizeof (DeviceTrackerConfiguration_t2458088029), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1908[4] = 
{
	DeviceTrackerConfiguration_t2458088029::get_offset_of_posePrediction_2(),
	DeviceTrackerConfiguration_t2458088029::get_offset_of_modelCorrectionMode_3(),
	DeviceTrackerConfiguration_t2458088029::get_offset_of_modelTransformEnabled_4(),
	DeviceTrackerConfiguration_t2458088029::get_offset_of_modelTransform_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1909 = { sizeof (WebCamConfiguration_t2750821739), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1909[4] = 
{
	WebCamConfiguration_t2750821739::get_offset_of_deviceNameSetInEditor_0(),
	WebCamConfiguration_t2750821739::get_offset_of_flipHorizontally_1(),
	WebCamConfiguration_t2750821739::get_offset_of_turnOffWebCam_2(),
	WebCamConfiguration_t2750821739::get_offset_of_renderTextureLayer_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1910 = { sizeof (VuforiaDeinitBehaviour_t1122908686), -1, sizeof(VuforiaDeinitBehaviour_t1122908686_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1910[1] = 
{
	VuforiaDeinitBehaviour_t1122908686_StaticFields::get_offset_of_mAppIsQuitting_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1911 = { sizeof (VuforiaRuntime_t2131788666), -1, sizeof(VuforiaRuntime_t2131788666_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1911[6] = 
{
	VuforiaRuntime_t2131788666::get_offset_of_mOnVuforiaInitError_0(),
	VuforiaRuntime_t2131788666::get_offset_of_mFailedToInitialize_1(),
	VuforiaRuntime_t2131788666::get_offset_of_mInitError_2(),
	VuforiaRuntime_t2131788666::get_offset_of_mHasInitialized_3(),
	VuforiaRuntime_t2131788666_StaticFields::get_offset_of_mInstance_4(),
	VuforiaRuntime_t2131788666_StaticFields::get_offset_of_mPadlock_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1912 = { sizeof (VuMarkAbstractBehaviour_t2652615854), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1912[16] = 
{
	VuMarkAbstractBehaviour_t2652615854::get_offset_of_mAspectRatio_20(),
	VuMarkAbstractBehaviour_t2652615854::get_offset_of_mWidth_21(),
	VuMarkAbstractBehaviour_t2652615854::get_offset_of_mHeight_22(),
	VuMarkAbstractBehaviour_t2652615854::get_offset_of_mPreviewImage_23(),
	VuMarkAbstractBehaviour_t2652615854::get_offset_of_mIdType_24(),
	VuMarkAbstractBehaviour_t2652615854::get_offset_of_mIdLength_25(),
	VuMarkAbstractBehaviour_t2652615854::get_offset_of_mBoundingBox_26(),
	VuMarkAbstractBehaviour_t2652615854::get_offset_of_mOrigin_27(),
	VuMarkAbstractBehaviour_t2652615854::get_offset_of_mTrackingFromRuntimeAppearance_28(),
	VuMarkAbstractBehaviour_t2652615854::get_offset_of_mVuMarkTemplate_29(),
	VuMarkAbstractBehaviour_t2652615854::get_offset_of_mVuMarkTarget_30(),
	VuMarkAbstractBehaviour_t2652615854::get_offset_of_mVuMarkResultId_31(),
	VuMarkAbstractBehaviour_t2652615854::get_offset_of_mOnTargetAssigned_32(),
	VuMarkAbstractBehaviour_t2652615854::get_offset_of_mOnTargetLost_33(),
	VuMarkAbstractBehaviour_t2652615854::get_offset_of_mLastTransformScale_34(),
	VuMarkAbstractBehaviour_t2652615854::get_offset_of_mLastSize_35(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1913 = { sizeof (VuMarkManager_t2090159093), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1914 = { sizeof (InstanceIdType_t3450543871)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1914[4] = 
{
	InstanceIdType_t3450543871::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1915 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1916 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1917 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1918 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1919 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1920 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1921 = { sizeof (ReconstructionFromTargetAbstractBehaviour_t2676804737), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1921[2] = 
{
	ReconstructionFromTargetAbstractBehaviour_t2676804737::get_offset_of_mReconstructionFromTarget_2(),
	ReconstructionFromTargetAbstractBehaviour_t2676804737::get_offset_of_mReconstructionBehaviour_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1922 = { sizeof (SmartTerrainBuilder_t1660152051), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1923 = { sizeof (SmartTerrainInitializationInfo_t1502783984)+ sizeof (RuntimeObject), sizeof(SmartTerrainInitializationInfo_t1502783984 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1924 = { sizeof (SmartTerrainTrackableBehaviour_t4179131702), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1924[4] = 
{
	SmartTerrainTrackableBehaviour_t4179131702::get_offset_of_mSmartTerrainTrackable_10(),
	SmartTerrainTrackableBehaviour_t4179131702::get_offset_of_mDisableAutomaticUpdates_11(),
	SmartTerrainTrackableBehaviour_t4179131702::get_offset_of_mMeshFilterToUpdate_12(),
	SmartTerrainTrackableBehaviour_t4179131702::get_offset_of_mMeshColliderToUpdate_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1925 = { sizeof (SmartTerrainTrackerARController_t1900914161), -1, sizeof(SmartTerrainTrackerARController_t1900914161_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1925[9] = 
{
	SmartTerrainTrackerARController_t1900914161::get_offset_of_mAutoInitTracker_1(),
	SmartTerrainTrackerARController_t1900914161::get_offset_of_mAutoStartTracker_2(),
	SmartTerrainTrackerARController_t1900914161::get_offset_of_mAutoInitBuilder_3(),
	SmartTerrainTrackerARController_t1900914161::get_offset_of_mSceneUnitsToMillimeter_4(),
	SmartTerrainTrackerARController_t1900914161::get_offset_of_mTrackerStarted_5(),
	SmartTerrainTrackerARController_t1900914161::get_offset_of_mTrackerWasActiveBeforePause_6(),
	SmartTerrainTrackerARController_t1900914161::get_offset_of_mTrackerWasActiveBeforeDisabling_7(),
	SmartTerrainTrackerARController_t1900914161_StaticFields::get_offset_of_mInstance_8(),
	SmartTerrainTrackerARController_t1900914161_StaticFields::get_offset_of_mPadlock_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1926 = { sizeof (SurfaceAbstractBehaviour_t272541176), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1926[1] = 
{
	SurfaceAbstractBehaviour_t272541176::get_offset_of_mSurface_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1927 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1928 = { sizeof (CylinderTargetAbstractBehaviour_t2158579059), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1928[10] = 
{
	CylinderTargetAbstractBehaviour_t2158579059::get_offset_of_mCylinderTarget_20(),
	CylinderTargetAbstractBehaviour_t2158579059::get_offset_of_mTopDiameterRatio_21(),
	CylinderTargetAbstractBehaviour_t2158579059::get_offset_of_mBottomDiameterRatio_22(),
	CylinderTargetAbstractBehaviour_t2158579059::get_offset_of_mSideLength_23(),
	CylinderTargetAbstractBehaviour_t2158579059::get_offset_of_mTopDiameter_24(),
	CylinderTargetAbstractBehaviour_t2158579059::get_offset_of_mBottomDiameter_25(),
	CylinderTargetAbstractBehaviour_t2158579059::get_offset_of_mFrameIndex_26(),
	CylinderTargetAbstractBehaviour_t2158579059::get_offset_of_mUpdateFrameIndex_27(),
	CylinderTargetAbstractBehaviour_t2158579059::get_offset_of_mFutureScale_28(),
	CylinderTargetAbstractBehaviour_t2158579059::get_offset_of_mLastTransformScale_29(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1929 = { sizeof (DataSet_t1443131193), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1930 = { sizeof (StorageType_t2925054281)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1930[4] = 
{
	StorageType_t2925054281::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1931 = { sizeof (DatabaseLoadARController_t1838845437), -1, sizeof(DatabaseLoadARController_t1838845437_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1931[6] = 
{
	DatabaseLoadARController_t1838845437::get_offset_of_mDatasetsLoaded_1(),
	DatabaseLoadARController_t1838845437::get_offset_of_mExternalDatasetRoots_2(),
	DatabaseLoadARController_t1838845437::get_offset_of_mDataSetsToLoad_3(),
	DatabaseLoadARController_t1838845437::get_offset_of_mDataSetsToActivate_4(),
	DatabaseLoadARController_t1838845437_StaticFields::get_offset_of_mInstance_5(),
	DatabaseLoadARController_t1838845437_StaticFields::get_offset_of_mPadlock_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1932 = { sizeof (DataSetTrackableBehaviour_t3960979584), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1932[10] = 
{
	DataSetTrackableBehaviour_t3960979584::get_offset_of_mDataSetPath_10(),
	DataSetTrackableBehaviour_t3960979584::get_offset_of_mExtendedTracking_11(),
	DataSetTrackableBehaviour_t3960979584::get_offset_of_mInitializeSmartTerrain_12(),
	DataSetTrackableBehaviour_t3960979584::get_offset_of_mReconstructionToInitialize_13(),
	DataSetTrackableBehaviour_t3960979584::get_offset_of_mSmartTerrainOccluderBoundsMin_14(),
	DataSetTrackableBehaviour_t3960979584::get_offset_of_mSmartTerrainOccluderBoundsMax_15(),
	DataSetTrackableBehaviour_t3960979584::get_offset_of_mIsSmartTerrainOccluderOffset_16(),
	DataSetTrackableBehaviour_t3960979584::get_offset_of_mSmartTerrainOccluderOffset_17(),
	DataSetTrackableBehaviour_t3960979584::get_offset_of_mSmartTerrainOccluderRotation_18(),
	DataSetTrackableBehaviour_t3960979584::get_offset_of_mAutoSetOccluderFromTargetSize_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1933 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1934 = { sizeof (RectangleData_t1094133618)+ sizeof (RuntimeObject), sizeof(RectangleData_t1094133618 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1934[4] = 
{
	RectangleData_t1094133618::get_offset_of_leftTopX_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	RectangleData_t1094133618::get_offset_of_leftTopY_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	RectangleData_t1094133618::get_offset_of_rightBottomX_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	RectangleData_t1094133618::get_offset_of_rightBottomY_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1935 = { sizeof (RectangleIntData_t2989567989)+ sizeof (RuntimeObject), sizeof(RectangleIntData_t2989567989 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1935[4] = 
{
	RectangleIntData_t2989567989::get_offset_of_leftTopX_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	RectangleIntData_t2989567989::get_offset_of_leftTopY_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	RectangleIntData_t2989567989::get_offset_of_rightBottomX_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	RectangleIntData_t2989567989::get_offset_of_rightBottomY_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1936 = { sizeof (OrientedBoundingBox_t4288440438)+ sizeof (RuntimeObject), sizeof(OrientedBoundingBox_t4288440438 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1936[3] = 
{
	OrientedBoundingBox_t4288440438::get_offset_of_U3CCenterU3Ek__BackingField_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	OrientedBoundingBox_t4288440438::get_offset_of_U3CHalfExtentsU3Ek__BackingField_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	OrientedBoundingBox_t4288440438::get_offset_of_U3CRotationU3Ek__BackingField_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1937 = { sizeof (OrientedBoundingBox3D_t19466890)+ sizeof (RuntimeObject), sizeof(OrientedBoundingBox3D_t19466890 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1937[3] = 
{
	OrientedBoundingBox3D_t19466890::get_offset_of_U3CCenterU3Ek__BackingField_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	OrientedBoundingBox3D_t19466890::get_offset_of_U3CHalfExtentsU3Ek__BackingField_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	OrientedBoundingBox3D_t19466890::get_offset_of_U3CRotationYU3Ek__BackingField_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1938 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1939 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1940 = { sizeof (BehaviourComponentFactory_t3424659045), -1, sizeof(BehaviourComponentFactory_t3424659045_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1940[1] = 
{
	BehaviourComponentFactory_t3424659045_StaticFields::get_offset_of_sInstance_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1941 = { sizeof (NullBehaviourComponentFactory_t2275034854), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1942 = { sizeof (CloudRecoImageTargetImpl_t3516871077), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1942[1] = 
{
	CloudRecoImageTargetImpl_t3516871077::get_offset_of_mSize_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1943 = { sizeof (CylinderTargetImpl_t2417802664), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1943[3] = 
{
	CylinderTargetImpl_t2417802664::get_offset_of_mSideLength_4(),
	CylinderTargetImpl_t2417802664::get_offset_of_mTopDiameter_5(),
	CylinderTargetImpl_t2417802664::get_offset_of_mBottomDiameter_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1944 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1945 = { sizeof (ImageTargetType_t2769160834)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1945[4] = 
{
	ImageTargetType_t2769160834::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1946 = { sizeof (ImageTargetData_t1209528890)+ sizeof (RuntimeObject), sizeof(ImageTargetData_t1209528890 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1946[2] = 
{
	ImageTargetData_t1209528890::get_offset_of_id_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ImageTargetData_t1209528890::get_offset_of_size_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1947 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1948 = { sizeof (ImageTargetBuilder_t2469815077), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1949 = { sizeof (FrameQuality_t1148757)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1949[5] = 
{
	FrameQuality_t1148757::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1950 = { sizeof (CameraDeviceImpl_t987560882), -1, sizeof(CameraDeviceImpl_t987560882_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1950[12] = 
{
	CameraDeviceImpl_t987560882::get_offset_of_mCameraImages_1(),
	CameraDeviceImpl_t987560882::get_offset_of_mForcedCameraFormats_2(),
	CameraDeviceImpl_t987560882_StaticFields::get_offset_of_mWebCam_3(),
	CameraDeviceImpl_t987560882::get_offset_of_mCameraReady_4(),
	CameraDeviceImpl_t987560882::get_offset_of_mIsDirty_5(),
	CameraDeviceImpl_t987560882::get_offset_of_mActualCameraDirection_6(),
	CameraDeviceImpl_t987560882::get_offset_of_mSelectedCameraDirection_7(),
	CameraDeviceImpl_t987560882::get_offset_of_mCameraDeviceMode_8(),
	CameraDeviceImpl_t987560882::get_offset_of_mVideoModeData_9(),
	CameraDeviceImpl_t987560882::get_offset_of_mVideoModeDataNeedsUpdate_10(),
	CameraDeviceImpl_t987560882::get_offset_of_mHasCameraDeviceModeBeenSet_11(),
	CameraDeviceImpl_t987560882::get_offset_of_mCameraActive_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1951 = { sizeof (CameraFieldData_t3856148331)+ sizeof (RuntimeObject), sizeof(CameraFieldData_t3856148331_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1951[3] = 
{
	CameraFieldData_t3856148331::get_offset_of_KeyValue_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	CameraFieldData_t3856148331::get_offset_of_DataType_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	CameraFieldData_t3856148331::get_offset_of_Unused_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1952 = { sizeof (DataSetImpl_t223764247), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1952[5] = 
{
	DataSetImpl_t223764247::get_offset_of_mDataSetPtr_0(),
	DataSetImpl_t223764247::get_offset_of_mPath_1(),
	DataSetImpl_t223764247::get_offset_of_mStorageType_2(),
	DataSetImpl_t223764247::get_offset_of_mTrackablesDict_3(),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1953 = { sizeof (ImageImpl_t3918258889), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1953[10] = 
{
	ImageImpl_t3918258889::get_offset_of_mWidth_0(),
	ImageImpl_t3918258889::get_offset_of_mHeight_1(),
	ImageImpl_t3918258889::get_offset_of_mStride_2(),
	ImageImpl_t3918258889::get_offset_of_mBufferWidth_3(),
	ImageImpl_t3918258889::get_offset_of_mBufferHeight_4(),
	ImageImpl_t3918258889::get_offset_of_mPixelFormat_5(),
	ImageImpl_t3918258889::get_offset_of_mData_6(),
	ImageImpl_t3918258889::get_offset_of_mUnmanagedData_7(),
	ImageImpl_t3918258889::get_offset_of_mDataSet_8(),
	ImageImpl_t3918258889::get_offset_of_mPixel32_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1954 = { sizeof (ImageTargetBuilderImpl_t761427244), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1954[2] = 
{
	ImageTargetBuilderImpl_t761427244::get_offset_of_mTrackableSource_0(),
	ImageTargetBuilderImpl_t761427244::get_offset_of_mIsScanning_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1955 = { sizeof (ImageTargetImpl_t1523268301), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1955[2] = 
{
	ImageTargetImpl_t1523268301::get_offset_of_mImageTargetType_4(),
	ImageTargetImpl_t1523268301::get_offset_of_mVirtualButtons_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1956 = { sizeof (ObjectTrackerImpl_t89795342), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1956[4] = 
{
	ObjectTrackerImpl_t89795342::get_offset_of_mActiveDataSets_1(),
	ObjectTrackerImpl_t89795342::get_offset_of_mDataSets_2(),
	ObjectTrackerImpl_t89795342::get_offset_of_mImageTargetBuilder_3(),
	ObjectTrackerImpl_t89795342::get_offset_of_mTargetFinder_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1957 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1958 = { sizeof (MultiTargetImpl_t109769909), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1959 = { sizeof (NullWebCamTexAdaptor_t712631648), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1959[5] = 
{
	NullWebCamTexAdaptor_t712631648::get_offset_of_mTexture_0(),
	NullWebCamTexAdaptor_t712631648::get_offset_of_mPseudoPlaying_1(),
	NullWebCamTexAdaptor_t712631648::get_offset_of_mMsBetweenFrames_2(),
	NullWebCamTexAdaptor_t712631648::get_offset_of_mLastFrame_3(),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1960 = { sizeof (PlayModeEditorUtility_t1443925904), -1, sizeof(PlayModeEditorUtility_t1443925904_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1960[1] = 
{
	PlayModeEditorUtility_t1443925904_StaticFields::get_offset_of_sInstance_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1961 = { sizeof (NullPlayModeEditorUtility_t854983833), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1962 = { sizeof (PremiumObjectFactory_t2114016913), -1, sizeof(PremiumObjectFactory_t2114016913_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1962[1] = 
{
	PremiumObjectFactory_t2114016913_StaticFields::get_offset_of_sInstance_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1963 = { sizeof (NullPremiumObjectFactory_t468302538), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1964 = { sizeof (VuforiaManagerImpl_t627862524), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1964[25] = 
{
	VuforiaManagerImpl_t627862524::get_offset_of_U3CVideoBackgroundTextureSetU3Ek__BackingField_1(),
	VuforiaManagerImpl_t627862524::get_offset_of_mWorldCenterMode_2(),
	VuforiaManagerImpl_t627862524::get_offset_of_mWorldCenter_3(),
	VuforiaManagerImpl_t627862524::get_offset_of_mVuMarkWorldCenter_4(),
	VuforiaManagerImpl_t627862524::get_offset_of_mARCameraTransform_5(),
	VuforiaManagerImpl_t627862524::get_offset_of_mCentralAnchorPoint_6(),
	VuforiaManagerImpl_t627862524::get_offset_of_mParentAnchorPoint_7(),
	VuforiaManagerImpl_t627862524::get_offset_of_mTrackableResultDataArray_8(),
	VuforiaManagerImpl_t627862524::get_offset_of_mWordDataArray_9(),
	VuforiaManagerImpl_t627862524::get_offset_of_mWordResultDataArray_10(),
	VuforiaManagerImpl_t627862524::get_offset_of_mVuMarkDataArray_11(),
	VuforiaManagerImpl_t627862524::get_offset_of_mVuMarkResultDataArray_12(),
	VuforiaManagerImpl_t627862524::get_offset_of_mTrackableFoundQueue_13(),
	VuforiaManagerImpl_t627862524::get_offset_of_mImageHeaderData_14(),
	VuforiaManagerImpl_t627862524::get_offset_of_mNumImageHeaders_15(),
	VuforiaManagerImpl_t627862524::get_offset_of_mInjectedFrameIdx_16(),
	VuforiaManagerImpl_t627862524::get_offset_of_mLastProcessedFrameStatePtr_17(),
	VuforiaManagerImpl_t627862524::get_offset_of_mInitialized_18(),
	VuforiaManagerImpl_t627862524::get_offset_of_mPaused_19(),
	VuforiaManagerImpl_t627862524::get_offset_of_mFrameState_20(),
	VuforiaManagerImpl_t627862524::get_offset_of_mAutoRotationState_21(),
	VuforiaManagerImpl_t627862524::get_offset_of_mVideoBackgroundNeedsRedrawing_22(),
	VuforiaManagerImpl_t627862524::get_offset_of_mDiscardStatesForRendering_23(),
	VuforiaManagerImpl_t627862524::get_offset_of_mLastFrameIdx_24(),
	VuforiaManagerImpl_t627862524::get_offset_of_mIsSeeThroughDevice_25(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1965 = { sizeof (PoseData_t1816720573)+ sizeof (RuntimeObject), sizeof(PoseData_t1816720573 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1965[3] = 
{
	PoseData_t1816720573::get_offset_of_position_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	PoseData_t1816720573::get_offset_of_orientation_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	PoseData_t1816720573::get_offset_of_csInteger_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1966 = { sizeof (TrackableResultData_t4095399453)+ sizeof (RuntimeObject), sizeof(TrackableResultData_t4095399453 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1966[4] = 
{
	TrackableResultData_t4095399453::get_offset_of_pose_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TrackableResultData_t4095399453::get_offset_of_timeStamp_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TrackableResultData_t4095399453::get_offset_of_statusInteger_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TrackableResultData_t4095399453::get_offset_of_id_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1967 = { sizeof (VirtualButtonData_t4213555505)+ sizeof (RuntimeObject), sizeof(VirtualButtonData_t4213555505 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1967[2] = 
{
	VirtualButtonData_t4213555505::get_offset_of_id_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	VirtualButtonData_t4213555505::get_offset_of_isPressed_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1968 = { sizeof (Obb2D_t1682428419)+ sizeof (RuntimeObject), sizeof(Obb2D_t1682428419 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1968[4] = 
{
	Obb2D_t1682428419::get_offset_of_center_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Obb2D_t1682428419::get_offset_of_halfExtents_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Obb2D_t1682428419::get_offset_of_rotation_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Obb2D_t1682428419::get_offset_of_unused_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1969 = { sizeof (Obb3D_t1746725078)+ sizeof (RuntimeObject), sizeof(Obb3D_t1746725078 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1969[4] = 
{
	Obb3D_t1746725078::get_offset_of_center_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Obb3D_t1746725078::get_offset_of_halfExtents_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Obb3D_t1746725078::get_offset_of_rotationZ_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Obb3D_t1746725078::get_offset_of_unused_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1970 = { sizeof (WordResultData_t3823211317)+ sizeof (RuntimeObject), sizeof(WordResultData_t3823211317 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1970[5] = 
{
	WordResultData_t3823211317::get_offset_of_pose_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordResultData_t3823211317::get_offset_of_timeStamp_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordResultData_t3823211317::get_offset_of_statusInteger_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordResultData_t3823211317::get_offset_of_id_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordResultData_t3823211317::get_offset_of_orientedBoundingBox_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1971 = { sizeof (WordData_t2202059873)+ sizeof (RuntimeObject), sizeof(WordData_t2202059873 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1971[4] = 
{
	WordData_t2202059873::get_offset_of_stringValue_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordData_t2202059873::get_offset_of_id_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordData_t2202059873::get_offset_of_size_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordData_t2202059873::get_offset_of_unused_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1972 = { sizeof (ImageHeaderData_t1829409304)+ sizeof (RuntimeObject), sizeof(ImageHeaderData_t1829409304 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1972[9] = 
{
	ImageHeaderData_t1829409304::get_offset_of_data_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ImageHeaderData_t1829409304::get_offset_of_width_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ImageHeaderData_t1829409304::get_offset_of_height_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ImageHeaderData_t1829409304::get_offset_of_stride_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ImageHeaderData_t1829409304::get_offset_of_bufferWidth_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ImageHeaderData_t1829409304::get_offset_of_bufferHeight_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ImageHeaderData_t1829409304::get_offset_of_format_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ImageHeaderData_t1829409304::get_offset_of_reallocate_7() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ImageHeaderData_t1829409304::get_offset_of_updated_8() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1973 = { sizeof (MeshData_t939646676)+ sizeof (RuntimeObject), sizeof(MeshData_t939646676 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1973[8] = 
{
	MeshData_t939646676::get_offset_of_positionsArray_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	MeshData_t939646676::get_offset_of_normalsArray_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	MeshData_t939646676::get_offset_of_texCoordsArray_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	MeshData_t939646676::get_offset_of_triangleIdxArray_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	MeshData_t939646676::get_offset_of_numVertexValues_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	MeshData_t939646676::get_offset_of_hasNormals_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	MeshData_t939646676::get_offset_of_hasTexCoords_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
	MeshData_t939646676::get_offset_of_numTriangleIndices_7() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1974 = { sizeof (InstanceIdData_t2286842551)+ sizeof (RuntimeObject), sizeof(InstanceIdData_t2286842551 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1974[5] = 
{
	InstanceIdData_t2286842551::get_offset_of_numericValue_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	InstanceIdData_t2286842551::get_offset_of_buffer_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	InstanceIdData_t2286842551::get_offset_of_reserved_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	InstanceIdData_t2286842551::get_offset_of_dataLength_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	InstanceIdData_t2286842551::get_offset_of_dataType_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1975 = { sizeof (VuMarkTargetData_t2973346370)+ sizeof (RuntimeObject), sizeof(VuMarkTargetData_t2973346370 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1975[5] = 
{
	VuMarkTargetData_t2973346370::get_offset_of_instanceId_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	VuMarkTargetData_t2973346370::get_offset_of_id_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	VuMarkTargetData_t2973346370::get_offset_of_templateId_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	VuMarkTargetData_t2973346370::get_offset_of_size_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	VuMarkTargetData_t2973346370::get_offset_of_unused_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1976 = { sizeof (SmartTerrainRevisionData_t1976771789)+ sizeof (RuntimeObject), sizeof(SmartTerrainRevisionData_t1976771789 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1976[2] = 
{
	SmartTerrainRevisionData_t1976771789::get_offset_of_id_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SmartTerrainRevisionData_t1976771789::get_offset_of_revision_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1977 = { sizeof (SurfaceData_t2491312487)+ sizeof (RuntimeObject), sizeof(SurfaceData_t2491312487 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1977[9] = 
{
	SurfaceData_t2491312487::get_offset_of_meshBoundaryArray_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SurfaceData_t2491312487::get_offset_of_meshData_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SurfaceData_t2491312487::get_offset_of_navMeshData_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SurfaceData_t2491312487::get_offset_of_boundingBox_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SurfaceData_t2491312487::get_offset_of_localPose_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SurfaceData_t2491312487::get_offset_of_id_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SurfaceData_t2491312487::get_offset_of_parentID_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SurfaceData_t2491312487::get_offset_of_numBoundaryIndices_7() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SurfaceData_t2491312487::get_offset_of_revision_8() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1978 = { sizeof (PropData_t948013865)+ sizeof (RuntimeObject), sizeof(PropData_t948013865 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1978[8] = 
{
	PropData_t948013865::get_offset_of_meshData_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	PropData_t948013865::get_offset_of_id_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	PropData_t948013865::get_offset_of_parentID_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	PropData_t948013865::get_offset_of_boundingBox_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	PropData_t948013865::get_offset_of_localPosition_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	PropData_t948013865::get_offset_of_localPose_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	PropData_t948013865::get_offset_of_revision_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
	PropData_t948013865::get_offset_of_unused_7() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1979 = { sizeof (VuMarkTargetResultData_t2086971180)+ sizeof (RuntimeObject), sizeof(VuMarkTargetResultData_t2086971180 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1979[6] = 
{
	VuMarkTargetResultData_t2086971180::get_offset_of_pose_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	VuMarkTargetResultData_t2086971180::get_offset_of_timeStamp_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	VuMarkTargetResultData_t2086971180::get_offset_of_statusInteger_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	VuMarkTargetResultData_t2086971180::get_offset_of_targetID_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	VuMarkTargetResultData_t2086971180::get_offset_of_resultID_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	VuMarkTargetResultData_t2086971180::get_offset_of_unused_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1980 = { sizeof (FrameState_t3340962930)+ sizeof (RuntimeObject), sizeof(FrameState_t3340962930 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1980[22] = 
{
	FrameState_t3340962930::get_offset_of_trackableDataArray_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	FrameState_t3340962930::get_offset_of_vbDataArray_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	FrameState_t3340962930::get_offset_of_wordResultArray_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	FrameState_t3340962930::get_offset_of_newWordDataArray_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	FrameState_t3340962930::get_offset_of_vuMarkResultArray_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	FrameState_t3340962930::get_offset_of_newVuMarkDataArray_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	FrameState_t3340962930::get_offset_of_propTrackableDataArray_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
	FrameState_t3340962930::get_offset_of_smartTerrainRevisionsArray_7() + static_cast<int32_t>(sizeof(RuntimeObject)),
	FrameState_t3340962930::get_offset_of_updatedSurfacesArray_8() + static_cast<int32_t>(sizeof(RuntimeObject)),
	FrameState_t3340962930::get_offset_of_updatedPropsArray_9() + static_cast<int32_t>(sizeof(RuntimeObject)),
	FrameState_t3340962930::get_offset_of_numTrackableResults_10() + static_cast<int32_t>(sizeof(RuntimeObject)),
	FrameState_t3340962930::get_offset_of_numVirtualButtonResults_11() + static_cast<int32_t>(sizeof(RuntimeObject)),
	FrameState_t3340962930::get_offset_of_frameIndex_12() + static_cast<int32_t>(sizeof(RuntimeObject)),
	FrameState_t3340962930::get_offset_of_numWordResults_13() + static_cast<int32_t>(sizeof(RuntimeObject)),
	FrameState_t3340962930::get_offset_of_numNewWords_14() + static_cast<int32_t>(sizeof(RuntimeObject)),
	FrameState_t3340962930::get_offset_of_numVuMarkResults_15() + static_cast<int32_t>(sizeof(RuntimeObject)),
	FrameState_t3340962930::get_offset_of_numNewVuMarks_16() + static_cast<int32_t>(sizeof(RuntimeObject)),
	FrameState_t3340962930::get_offset_of_numPropTrackableResults_17() + static_cast<int32_t>(sizeof(RuntimeObject)),
	FrameState_t3340962930::get_offset_of_numSmartTerrainRevisions_18() + static_cast<int32_t>(sizeof(RuntimeObject)),
	FrameState_t3340962930::get_offset_of_numUpdatedSurfaces_19() + static_cast<int32_t>(sizeof(RuntimeObject)),
	FrameState_t3340962930::get_offset_of_numUpdatedProps_20() + static_cast<int32_t>(sizeof(RuntimeObject)),
	FrameState_t3340962930::get_offset_of_deviceTrackableId_21() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1981 = { sizeof (AutoRotationState_t545146858)+ sizeof (RuntimeObject), sizeof(AutoRotationState_t545146858_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1981[5] = 
{
	AutoRotationState_t545146858::get_offset_of_setOnPause_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	AutoRotationState_t545146858::get_offset_of_autorotateToPortrait_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	AutoRotationState_t545146858::get_offset_of_autorotateToPortraitUpsideDown_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	AutoRotationState_t545146858::get_offset_of_autorotateToLandscapeLeft_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	AutoRotationState_t545146858::get_offset_of_autorotateToLandscapeRight_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1982 = { sizeof (U3CU3Ec__DisplayClass86_0_t2077910410), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1982[1] = 
{
	U3CU3Ec__DisplayClass86_0_t2077910410::get_offset_of_id_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1983 = { sizeof (VuforiaRendererImpl_t3463553574), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1983[6] = 
{
	VuforiaRendererImpl_t3463553574::get_offset_of_mVideoBGConfig_1(),
	VuforiaRendererImpl_t3463553574::get_offset_of_mVideoBGConfigSet_2(),
	VuforiaRendererImpl_t3463553574::get_offset_of_mVideoBackgroundTexture_3(),
	VuforiaRendererImpl_t3463553574::get_offset_of_mBackgroundTextureHasChanged_4(),
	VuforiaRendererImpl_t3463553574::get_offset_of_mLastSetReflection_5(),
	VuforiaRendererImpl_t3463553574::get_offset_of_mNativeRenderingCallback_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1984 = { sizeof (RenderEvent_t2231315398)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1984[7] = 
{
	RenderEvent_t2231315398::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1985 = { sizeof (VuforiaUnityImpl_t2202289649), -1, sizeof(VuforiaUnityImpl_t2202289649_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1985[5] = 
{
	0,
	0,
	0,
	VuforiaUnityImpl_t2202289649_StaticFields::get_offset_of_mRendererDirty_3(),
	VuforiaUnityImpl_t2202289649_StaticFields::get_offset_of_mWrapperType_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1986 = { sizeof (SurfaceImpl_t1060658458), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1986[5] = 
{
	SurfaceImpl_t1060658458::get_offset_of_mNavMesh_7(),
	SurfaceImpl_t1060658458::get_offset_of_mMeshBoundaries_8(),
	SurfaceImpl_t1060658458::get_offset_of_mBoundingBox_9(),
	SurfaceImpl_t1060658458::get_offset_of_mSurfaceArea_10(),
	SurfaceImpl_t1060658458::get_offset_of_mAreaNeedsUpdate_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1987 = { sizeof (SmartTerrainBuilderImpl_t4234039031), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1987[2] = 
{
	SmartTerrainBuilderImpl_t4234039031::get_offset_of_mReconstructionBehaviours_0(),
	SmartTerrainBuilderImpl_t4234039031::get_offset_of_mIsInitialized_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1988 = { sizeof (PropImpl_t1422751624), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1988[1] = 
{
	PropImpl_t1422751624::get_offset_of_mOrientedBoundingBox3D_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1989 = { sizeof (SmartTerrainTrackableImpl_t945956068), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1989[5] = 
{
	SmartTerrainTrackableImpl_t945956068::get_offset_of_mChildren_2(),
	SmartTerrainTrackableImpl_t945956068::get_offset_of_mMesh_3(),
	SmartTerrainTrackableImpl_t945956068::get_offset_of_mMeshRevision_4(),
	SmartTerrainTrackableImpl_t945956068::get_offset_of_mLocalPose_5(),
	SmartTerrainTrackableImpl_t945956068::get_offset_of_U3CParentU3Ek__BackingField_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1990 = { sizeof (SmartTerrainTrackerImpl_t3354406207), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1990[2] = 
{
	SmartTerrainTrackerImpl_t3354406207::get_offset_of_mScaleToMillimeter_1(),
	SmartTerrainTrackerImpl_t3354406207::get_offset_of_mSmartTerrainBuilder_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1991 = { sizeof (TextTrackerImpl_t1798165375), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1991[1] = 
{
	TextTrackerImpl_t1798165375::get_offset_of_mWordList_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1992 = { sizeof (UpDirection_t1212967134)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1992[5] = 
{
	UpDirection_t1212967134::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1993 = { sizeof (TypeMapping_t586580177), -1, sizeof(TypeMapping_t586580177_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1993[1] = 
{
	TypeMapping_t586580177_StaticFields::get_offset_of_sTypes_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1994 = { sizeof (WebCamTexAdaptor_t433428952), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1995 = { sizeof (WebCamTexAdaptorImpl_t3288976692), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1995[2] = 
{
	WebCamTexAdaptorImpl_t3288976692::get_offset_of_mWebCamTexture_0(),
	WebCamTexAdaptorImpl_t3288976692::get_offset_of_mCheckCameraPermissions_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1996 = { sizeof (WordImpl_t2212611807), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1996[5] = 
{
	WordImpl_t2212611807::get_offset_of_mText_2(),
	WordImpl_t2212611807::get_offset_of_mSize_3(),
	WordImpl_t2212611807::get_offset_of_mLetterMask_4(),
	WordImpl_t2212611807::get_offset_of_mLetterImageHeader_5(),
	WordImpl_t2212611807::get_offset_of_mLetterBoundingBoxes_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1997 = { sizeof (WordPrefabCreationMode_t2868429441)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1997[3] = 
{
	WordPrefabCreationMode_t2868429441::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1998 = { sizeof (WordManagerImpl_t2273252301), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1998[12] = 
{
	WordManagerImpl_t2273252301::get_offset_of_mTrackedWords_0(),
	WordManagerImpl_t2273252301::get_offset_of_mNewWords_1(),
	WordManagerImpl_t2273252301::get_offset_of_mLostWords_2(),
	WordManagerImpl_t2273252301::get_offset_of_mActiveWordBehaviours_3(),
	WordManagerImpl_t2273252301::get_offset_of_mWordBehavioursMarkedForDeletion_4(),
	WordManagerImpl_t2273252301::get_offset_of_mWaitingQueue_5(),
	0,
	WordManagerImpl_t2273252301::get_offset_of_mWordBehaviours_7(),
	WordManagerImpl_t2273252301::get_offset_of_mAutomaticTemplate_8(),
	WordManagerImpl_t2273252301::get_offset_of_mMaxInstances_9(),
	WordManagerImpl_t2273252301::get_offset_of_mWordPrefabCreationMode_10(),
	WordManagerImpl_t2273252301::get_offset_of_mVuforiaBehaviour_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1999 = { sizeof (WordResultImpl_t2776230186), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1999[5] = 
{
	WordResultImpl_t2776230186::get_offset_of_mObb_0(),
	WordResultImpl_t2776230186::get_offset_of_mPosition_1(),
	WordResultImpl_t2776230186::get_offset_of_mOrientation_2(),
	WordResultImpl_t2776230186::get_offset_of_mWord_3(),
	WordResultImpl_t2776230186::get_offset_of_mStatus_4(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif

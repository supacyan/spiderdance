﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "object-internals.h"

// UnityEngine.NetworkView
struct NetworkView_t3440562342;
// System.String
struct String_t;
// UnityEngine.Object
struct Object_t1502412432;
// UnityEngine.Transform
struct Transform_t3316442598;
// UnityEngine.Object[]
struct ObjectU5BU5D_t760538417;
// System.Type
struct Type_t;
// System.ArgumentException
struct ArgumentException_t4028401650;
// UnityEngine.ParticleSystem
struct ParticleSystem_t1912383059;
// UnityEngine.AnimationCurve
struct AnimationCurve_t2184836714;
// UnityEngine.RaycastHit[]
struct RaycastHitU5BU5D_t4180454940;
// System.Collections.Generic.List`1<UnityEngine.Rigidbody2D>
struct List_1_t3403087445;
// System.Collections.Generic.List`1<System.Object>
struct List_1_t877107497;
// System.Object[]
struct ObjectU5BU5D_t3270211303;
// UnityEngine.Playables.PlayableAsset
struct PlayableAsset_t2449544405;
// UnityEngine.ScriptableObject
struct ScriptableObject_t3635579074;
// UnityEngine.GameObject
struct GameObject_t1811656094;
// UnityEngine.Playables.PlayableBehaviour
struct PlayableBehaviour_t1007788125;
// System.InvalidCastException
struct InvalidCastException_t1996460962;
// UnityEngine.PreferBinarySerialization
struct PreferBinarySerialization_t3755844044;
// System.Attribute
struct Attribute_t841672618;
// UnityEngine.PropertyAttribute
struct PropertyAttribute_t1939035372;
// UnityEngine.RangeAttribute
struct RangeAttribute_t3376577711;
// UnityEngine.Collider
struct Collider_t2301021182;
// UnityEngine.Rigidbody
struct Rigidbody_t2492269564;
// UnityEngine.Collider2D
struct Collider2D_t1623106781;
// UnityEngine.RectOffset
struct RectOffset_t1315541452;
// UnityEngine.RectTransform
struct RectTransform_t3962994911;
// UnityEngine.RectTransform/ReapplyDrivenProperties
struct ReapplyDrivenProperties_t565817470;
// System.Delegate
struct Delegate_t537306192;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t981680887;
// UnityEngine.Component
struct Component_t4087199522;
// System.IAsyncResult
struct IAsyncResult_t1913953129;
// System.AsyncCallback
struct AsyncCallback_t1057242265;
// UnityEngine.Camera
struct Camera_t3175186167;
// UnityEngine.Canvas
struct Canvas_t3744170841;
// UnityEngine.RemoteSettings/UpdatedEventHandler
struct UpdatedEventHandler_t729869569;
// UnityEngine.Renderer
struct Renderer_t1303575294;
// UnityEngine.Material
struct Material_t1079520667;
// UnityEngine.Material[]
struct MaterialU5BU5D_t1437145498;
// UnityEngine.RenderTexture
struct RenderTexture_t2315324624;
// UnityEngine.Texture
struct Texture_t85561421;
// UnityEngine.RequireComponent
struct RequireComponent_t2076945239;
// UnityEngine.ResourceRequest
struct ResourceRequest_t963543698;
// UnityEngine.AsyncOperation
struct AsyncOperation_t2744032732;
// UnityEngine.RPC
struct RPC_t1601042701;
// UnityEngine.RuntimeInitializeOnLoadMethodAttribute
struct RuntimeInitializeOnLoadMethodAttribute_t1540303992;
// UnityEngine.Scripting.PreserveAttribute
struct PreserveAttribute_t3468091719;
// UnityEngine.Events.UnityAction`2<UnityEngine.SceneManagement.Scene,UnityEngine.SceneManagement.LoadSceneMode>
struct UnityAction_2_t2064023076;
// UnityEngine.Events.UnityAction`1<UnityEngine.SceneManagement.Scene>
struct UnityAction_1_t3896475417;
// UnityEngine.Events.UnityAction`2<UnityEngine.SceneManagement.Scene,UnityEngine.SceneManagement.Scene>
struct UnityAction_2_t121587842;
// UnityEngine.Scripting.APIUpdating.MovedFromAttribute
struct MovedFromAttribute_t2669718911;
// UnityEngine.Scripting.GeneratedByOldBindingsGeneratorAttribute
struct GeneratedByOldBindingsGeneratorAttribute_t2914791882;
// UnityEngine.Scripting.RequiredByNativeCodeAttribute
struct RequiredByNativeCodeAttribute_t814971270;
// UnityEngine.Scripting.UsedByNativeCodeAttribute
struct UsedByNativeCodeAttribute_t1334661680;
// UnityEngine.ScrollViewState
struct ScrollViewState_t1314793348;
// UnityEngine.SelectionBaseAttribute
struct SelectionBaseAttribute_t2246906333;
// UnityEngine.Camera[]
struct CameraU5BU5D_t1519774542;
// UnityEngine.GUILayer
struct GUILayer_t4095959459;
// UnityEngine.GUIElement
struct GUIElement_t4082881042;
// UnityEngine.Serialization.FormerlySerializedAsAttribute
struct FormerlySerializedAsAttribute_t1937332047;
// UnityEngine.SerializeField
struct SerializeField_t1027808064;
// UnityEngine.SerializePrivateVariables
struct SerializePrivateVariables_t409785951;
// System.Collections.IEnumerator
struct IEnumerator_t2306197993;
// UnityEngine.Shader
struct Shader_t3778723916;
// UnityEngine.SharedBetweenAnimatorsAttribute
struct SharedBetweenAnimatorsAttribute_t1887607685;
// UnityEngine.SliderState
struct SliderState_t3254875344;
// UnityEngine.Rigidbody2D[]
struct Rigidbody2DU5BU5D_t3935624075;
// UnityEngine.SendMouseEvents/HitInfo[]
struct HitInfoU5BU5D_t1076684586;
// System.Globalization.CultureInfo
struct CultureInfo_t4043279873;
// System.Globalization.NumberFormatInfo
struct NumberFormatInfo_t3680167716;
// System.Globalization.DateTimeFormatInfo
struct DateTimeFormatInfo_t354846801;
// System.Globalization.TextInfo
struct TextInfo_t2655029620;
// System.Globalization.CompareInfo
struct CompareInfo_t2456649274;
// System.Globalization.Calendar[]
struct CalendarU5BU5D_t237754405;
// System.Globalization.Calendar
struct Calendar_t3114948524;
// System.Byte[]
struct ByteU5BU5D_t3172826560;
// System.Collections.Hashtable
struct Hashtable_t1690131612;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_t1112866524;
// System.Int32
struct Int32_t4237944589;
// System.Void
struct Void_t2217553113;
// System.IntPtr[]
struct IntPtrU5BU5D_t439965300;
// System.Collections.IDictionary
struct IDictionary_t3485892299;
// System.Char[]
struct CharU5BU5D_t45629911;
// System.Boolean[]
struct BooleanU5BU5D_t3096172704;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.DelegateData
struct DelegateData_t3370401926;
// UnityEngine.Playables.PlayableBinding[]
struct PlayableBindingU5BU5D_t3242831679;
// System.Int32[]
struct Int32U5BU5D_t2324750880;
// System.Type[]
struct TypeU5BU5D_t3854702846;
// System.Reflection.MemberFilter
struct MemberFilter_t4208935459;
// UnityEngine.Canvas/WillRenderCanvases
struct WillRenderCanvases_t1524812347;
// UnityEngine.Camera/CameraCallback
struct CameraCallback_t832885096;

extern RuntimeClass* NetworkViewID_t1404889299_il2cpp_TypeInfo_var;
extern const uint32_t NetworkViewID_Equals_m1195240327_MetadataUsageId;
extern RuntimeClass* Object_t1502412432_il2cpp_TypeInfo_var;
extern const uint32_t Object_Internal_InstantiateSingle_m664294789_MetadataUsageId;
extern const uint32_t Object_Internal_InstantiateSingleWithParent_m3862473257_MetadataUsageId;
extern const uint32_t Object_Destroy_m1956460533_MetadataUsageId;
extern const uint32_t Object_DestroyImmediate_m2065130163_MetadataUsageId;
extern const uint32_t Object_DestroyObject_m1837997780_MetadataUsageId;
extern RuntimeClass* IntPtr_t_il2cpp_TypeInfo_var;
extern const uint32_t Object_GetInstanceID_m3083837515_MetadataUsageId;
extern const uint32_t Object_Equals_m4179840469_MetadataUsageId;
extern const uint32_t Object_op_Implicit_m2314733868_MetadataUsageId;
extern const uint32_t Object_CompareBaseObjects_m3108723731_MetadataUsageId;
extern const uint32_t Object_IsNativeObjectAlive_m3228292766_MetadataUsageId;
extern RuntimeClass* ScriptableObject_t3635579074_il2cpp_TypeInfo_var;
extern RuntimeClass* ArgumentException_t4028401650_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral867517236;
extern Il2CppCodeGenString* _stringLiteral3130680061;
extern const uint32_t Object_Instantiate_m1272509960_MetadataUsageId;
extern const uint32_t Object_Instantiate_m1165988047_MetadataUsageId;
extern const uint32_t Object_Instantiate_m856891812_MetadataUsageId;
extern const uint32_t Object_Instantiate_m2869586105_MetadataUsageId;
extern const uint32_t Object_Instantiate_m914959561_MetadataUsageId;
extern const uint32_t Object_CheckNullArgument_m2850461954_MetadataUsageId;
extern const uint32_t Object_FindObjectOfType_m56264973_MetadataUsageId;
extern const uint32_t Object_op_Equality_m2342070775_MetadataUsageId;
extern const uint32_t Object_op_Inequality_m1918954159_MetadataUsageId;
extern const uint32_t Object__cctor_m2578442528_MetadataUsageId;
extern RuntimeClass* MinMaxCurve_t2961159436_il2cpp_TypeInfo_var;
extern const uint32_t MainModule_get_startLifetime_m2433047371_MetadataUsageId;
struct AnimationCurve_t2184836714_marshaled_pinvoke;
struct AnimationCurve_t2184836714;;
struct AnimationCurve_t2184836714_marshaled_pinvoke;;
extern RuntimeClass* AnimationCurve_t2184836714_il2cpp_TypeInfo_var;
extern const uint32_t MinMaxCurve_t2961159436_pinvoke_FromNativeMethodDefinition_MetadataUsageId;
struct AnimationCurve_t2184836714_marshaled_com;
struct AnimationCurve_t2184836714_marshaled_com;;
extern RuntimeClass* List_1_t3403087445_il2cpp_TypeInfo_var;
extern RuntimeClass* Physics2D_t1358151724_il2cpp_TypeInfo_var;
extern const RuntimeMethod* List_1__ctor_m1977615932_RuntimeMethod_var;
extern const uint32_t Physics2D__cctor_m3391912526_MetadataUsageId;
extern RuntimeClass* Vector3_t2903530434_il2cpp_TypeInfo_var;
extern const uint32_t Plane__ctor_m2922395188_MetadataUsageId;
extern RuntimeClass* Mathf_t4094287654_il2cpp_TypeInfo_var;
extern const uint32_t Plane_Raycast_m235007613_MetadataUsageId;
extern RuntimeClass* ObjectU5BU5D_t3270211303_il2cpp_TypeInfo_var;
extern RuntimeClass* Single_t3045349759_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1289564811;
extern const uint32_t Plane_ToString_m2513597252_MetadataUsageId;
extern RuntimeClass* Playable_t735025802_il2cpp_TypeInfo_var;
extern const uint32_t Playable_get_Null_m1400628986_MetadataUsageId;
extern const RuntimeMethod* PlayableExtensions_SetInputCount_TisPlayable_t735025802_m769118885_RuntimeMethod_var;
extern const uint32_t Playable_Create_m1247326070_MetadataUsageId;
extern const uint32_t Playable__cctor_m698787543_MetadataUsageId;
extern RuntimeClass* PlayableBinding_t3706191642_il2cpp_TypeInfo_var;
extern const uint32_t PlayableAsset_get_duration_m952498650_MetadataUsageId;
extern const uint32_t PlayableAsset_Internal_CreatePlayable_m4205524028_MetadataUsageId;
struct Object_t1502412432_marshaled_pinvoke;
struct Object_t1502412432;;
struct Object_t1502412432_marshaled_pinvoke;;
struct Object_t1502412432_marshaled_com;
struct Object_t1502412432_marshaled_com;;
extern RuntimeClass* PlayableBindingU5BU5D_t3242831679_il2cpp_TypeInfo_var;
extern const uint32_t PlayableBinding__cctor_m3291100432_MetadataUsageId;
extern RuntimeClass* PlayableHandle_t1286828706_il2cpp_TypeInfo_var;
extern const uint32_t PlayableHandle_get_Null_m1252146945_MetadataUsageId;
extern const uint32_t PlayableHandle_Equals_m2914066680_MetadataUsageId;
extern RuntimeClass* PlayableOutput_t3724158364_il2cpp_TypeInfo_var;
extern const uint32_t PlayableOutput_get_Null_m2632115234_MetadataUsageId;
extern const uint32_t PlayableOutput__cctor_m1909945595_MetadataUsageId;
extern RuntimeClass* PlayableOutputHandle_t2200222555_il2cpp_TypeInfo_var;
extern const uint32_t PlayableOutputHandle_get_Null_m3317868945_MetadataUsageId;
extern const uint32_t PlayableOutputHandle_Equals_m1190900403_MetadataUsageId;
extern RuntimeClass* InvalidCastException_t1996460962_il2cpp_TypeInfo_var;
extern const RuntimeMethod* PlayableOutputHandle_IsPlayableOutputOfType_TisScriptPlayableOutput_t49585950_m1665456405_RuntimeMethod_var;
extern Il2CppCodeGenString* _stringLiteral90747689;
extern const uint32_t ScriptPlayableOutput__ctor_m1317995783_MetadataUsageId;
extern RuntimeClass* Quaternion_t754065749_il2cpp_TypeInfo_var;
extern const uint32_t Quaternion_AngleAxis_m3400718822_MetadataUsageId;
extern const uint32_t Quaternion_ToAngleAxis_m636701083_MetadataUsageId;
extern const uint32_t Quaternion_LookRotation_m2938427707_MetadataUsageId;
extern const uint32_t Quaternion_LookRotation_m274887101_MetadataUsageId;
extern const uint32_t Quaternion_Slerp_m2698179433_MetadataUsageId;
extern const uint32_t Quaternion_Inverse_m3223872676_MetadataUsageId;
extern const uint32_t Quaternion_get_eulerAngles_m3862017976_MetadataUsageId;
extern const uint32_t Quaternion_Euler_m631510244_MetadataUsageId;
extern const uint32_t Quaternion_Euler_m238783557_MetadataUsageId;
extern const uint32_t Quaternion_Internal_ToEulerRad_m4233285968_MetadataUsageId;
extern const uint32_t Quaternion_Internal_FromEulerRad_m2740790710_MetadataUsageId;
extern const uint32_t Quaternion_Internal_ToAxisAngleRad_m1871010446_MetadataUsageId;
extern const uint32_t Quaternion_get_identity_m4047182200_MetadataUsageId;
extern const uint32_t Quaternion_op_Equality_m2930003662_MetadataUsageId;
extern const uint32_t Quaternion_op_Inequality_m1867215411_MetadataUsageId;
extern const uint32_t Quaternion_Angle_m713725543_MetadataUsageId;
extern const uint32_t Quaternion_Equals_m3533589735_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral37643425;
extern const uint32_t Quaternion_ToString_m329630960_MetadataUsageId;
extern const uint32_t Quaternion__cctor_m3598117699_MetadataUsageId;
extern const uint32_t Ray_GetPoint_m3472514115_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral2680542856;
extern const uint32_t Ray_ToString_m1456301833_MetadataUsageId;
extern const uint32_t RaycastHit_get_rigidbody_m3749375064_MetadataUsageId;
extern RuntimeClass* Rect_t3436776195_il2cpp_TypeInfo_var;
extern const uint32_t Rect_Equals_m375994694_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral4073655457;
extern const uint32_t Rect_ToString_m1731978317_MetadataUsageId;
extern RuntimeClass* Il2CppComObject_il2cpp_TypeInfo_var;
extern const uint32_t RectOffset_t1315541452_pinvoke_FromNativeMethodDefinition_MetadataUsageId;
extern const uint32_t RectOffset_t1315541452_com_FromNativeMethodDefinition_MetadataUsageId;
extern RuntimeClass* Int32_t4237944589_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2841341723;
extern const uint32_t RectOffset_ToString_m108828025_MetadataUsageId;
extern RuntimeClass* RectTransform_t3962994911_il2cpp_TypeInfo_var;
extern RuntimeClass* ReapplyDrivenProperties_t565817470_il2cpp_TypeInfo_var;
extern const uint32_t RectTransform_add_reapplyDrivenProperties_m3435041181_MetadataUsageId;
extern const uint32_t RectTransform_remove_reapplyDrivenProperties_m3549895845_MetadataUsageId;
extern const uint32_t RectTransform_SendReapplyDrivenProperties_m1201286970_MetadataUsageId;
extern RuntimeClass* Debug_t3525713211_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4219001008;
extern const uint32_t RectTransform_GetLocalCorners_m1896055443_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral3917415373;
extern const uint32_t RectTransform_GetWorldCorners_m2181990217_MetadataUsageId;
extern RuntimeClass* Vector2_t3577333262_il2cpp_TypeInfo_var;
extern const uint32_t RectTransform_set_offsetMin_m4052016319_MetadataUsageId;
extern const uint32_t RectTransform_set_offsetMax_m1291583879_MetadataUsageId;
extern const uint32_t RectTransform_GetParentSize_m3415414519_MetadataUsageId;
extern RuntimeClass* RectTransformUtility_t535428424_il2cpp_TypeInfo_var;
extern const uint32_t RectTransformUtility_ScreenPointToWorldPointInRectangle_m4112832593_MetadataUsageId;
extern const uint32_t RectTransformUtility_ScreenPointToLocalPointInRectangle_m4033027037_MetadataUsageId;
extern const uint32_t RectTransformUtility_ScreenPointToRay_m339743857_MetadataUsageId;
extern const uint32_t RectTransformUtility_FlipLayoutOnAxis_m1442291361_MetadataUsageId;
extern const uint32_t RectTransformUtility_FlipLayoutAxes_m225875364_MetadataUsageId;
extern const uint32_t RectTransformUtility_RectangleContainsScreenPoint_m1829762538_MetadataUsageId;
extern const uint32_t RectTransformUtility_PixelAdjustPoint_m368852017_MetadataUsageId;
extern const uint32_t RectTransformUtility_PixelAdjustRect_m861557880_MetadataUsageId;
extern RuntimeClass* Vector3U5BU5D_t981680887_il2cpp_TypeInfo_var;
extern const uint32_t RectTransformUtility__cctor_m977985261_MetadataUsageId;
extern RuntimeClass* RemoteSettings_t2746965213_il2cpp_TypeInfo_var;
extern const uint32_t RemoteSettings_CallOnUpdate_m1780483590_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral2874801222;
extern Il2CppCodeGenString* _stringLiteral2103610803;
extern Il2CppCodeGenString* _stringLiteral2311410166;
extern Il2CppCodeGenString* _stringLiteral308001844;
extern Il2CppCodeGenString* _stringLiteral3426330824;
extern Il2CppCodeGenString* _stringLiteral1909265924;
extern Il2CppCodeGenString* _stringLiteral2615600362;
extern Il2CppCodeGenString* _stringLiteral810757830;
extern Il2CppCodeGenString* _stringLiteral2448784868;
extern Il2CppCodeGenString* _stringLiteral2308451020;
extern const uint32_t RenderTexture_ValidateRenderTextureDesc_m3206735494_MetadataUsageId;
extern RuntimeClass* RenderTextureDescriptor_t3096142983_il2cpp_TypeInfo_var;
extern const uint32_t RenderTextureDescriptor__ctor_m430880500_MetadataUsageId;
extern const uint32_t RenderTextureDescriptor_get_depthBufferBits_m1512939583_MetadataUsageId;
extern RuntimeClass* Int32U5BU5D_t2324750880_il2cpp_TypeInfo_var;
extern const uint32_t RenderTextureDescriptor__cctor_m1159501317_MetadataUsageId;
extern const RuntimeType* Object_t1502412432_0_0_0_var;
extern RuntimeClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t Resources_Load_m2818004787_MetadataUsageId;
extern RuntimeClass* Scene_t348657329_il2cpp_TypeInfo_var;
extern const uint32_t Scene_Equals_m714854343_MetadataUsageId;
extern RuntimeClass* SceneManager_t1729095165_il2cpp_TypeInfo_var;
extern const RuntimeMethod* UnityAction_2_Invoke_m2774749000_RuntimeMethod_var;
extern const uint32_t SceneManager_Internal_SceneLoaded_m3575317911_MetadataUsageId;
extern const RuntimeMethod* UnityAction_1_Invoke_m2393581145_RuntimeMethod_var;
extern const uint32_t SceneManager_Internal_SceneUnloaded_m2896774791_MetadataUsageId;
extern const RuntimeMethod* UnityAction_2_Invoke_m3185242217_RuntimeMethod_var;
extern const uint32_t SceneManager_Internal_ActiveSceneChanged_m1556810644_MetadataUsageId;
extern const uint32_t ScriptableObject__ctor_m4110805243_MetadataUsageId;
extern RuntimeClass* SendMouseEvents_t1825902553_il2cpp_TypeInfo_var;
extern const uint32_t SendMouseEvents_SetMouseMoved_m4214977923_MetadataUsageId;
extern RuntimeClass* Input_t3439709668_il2cpp_TypeInfo_var;
extern RuntimeClass* CameraU5BU5D_t1519774542_il2cpp_TypeInfo_var;
extern RuntimeClass* HitInfo_t2000020299_il2cpp_TypeInfo_var;
extern const RuntimeMethod* Component_GetComponent_TisGUILayer_t4095959459_m2359659865_RuntimeMethod_var;
extern const uint32_t SendMouseEvents_DoSendMouseEvents_m2460376797_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral1678730234;
extern Il2CppCodeGenString* _stringLiteral2097995814;
extern Il2CppCodeGenString* _stringLiteral2124819158;
extern Il2CppCodeGenString* _stringLiteral322303585;
extern Il2CppCodeGenString* _stringLiteral1932353065;
extern Il2CppCodeGenString* _stringLiteral2750800491;
extern Il2CppCodeGenString* _stringLiteral2317408620;
extern const uint32_t SendMouseEvents_SendEvents_m3207122301_MetadataUsageId;
extern RuntimeClass* HitInfoU5BU5D_t1076684586_il2cpp_TypeInfo_var;
extern const uint32_t SendMouseEvents__cctor_m1904736527_MetadataUsageId;
extern const uint32_t HitInfo_op_Implicit_m2296455248_MetadataUsageId;
extern const uint32_t HitInfo_Compare_m3834232273_MetadataUsageId;
extern RuntimeClass* IEnumerator_t2306197993_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral933600062;
extern Il2CppCodeGenString* _stringLiteral2173215484;
extern const uint32_t SetupCoroutine_InvokeMoveNext_m3258232562_MetadataUsageId;
extern const uint32_t SetupCoroutine_InvokeMember_m1034957027_MetadataUsageId;

struct ObjectU5BU5D_t760538417;
struct RaycastHitU5BU5D_t4180454940;
struct ObjectU5BU5D_t3270211303;
struct PlayableBindingU5BU5D_t3242831679;
struct Vector3U5BU5D_t981680887;
struct MaterialU5BU5D_t1437145498;
struct Int32U5BU5D_t2324750880;
struct CameraU5BU5D_t1519774542;
struct HitInfoU5BU5D_t1076684586;
struct ParameterModifierU5BU5D_t3421420242;
struct StringU5BU5D_t2298632947;


#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
struct Il2CppArrayBounds;
#ifndef RUNTIMEARRAY_H
#define RUNTIMEARRAY_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Array

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEARRAY_H
#ifndef LIST_1_T3403087445_H
#define LIST_1_T3403087445_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1<UnityEngine.Rigidbody2D>
struct  List_1_t3403087445  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	Rigidbody2DU5BU5D_t3935624075* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t3403087445, ____items_1)); }
	inline Rigidbody2DU5BU5D_t3935624075* get__items_1() const { return ____items_1; }
	inline Rigidbody2DU5BU5D_t3935624075** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(Rigidbody2DU5BU5D_t3935624075* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((&____items_1), value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t3403087445, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t3403087445, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}
};

struct List_1_t3403087445_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::EmptyArray
	Rigidbody2DU5BU5D_t3935624075* ___EmptyArray_4;

public:
	inline static int32_t get_offset_of_EmptyArray_4() { return static_cast<int32_t>(offsetof(List_1_t3403087445_StaticFields, ___EmptyArray_4)); }
	inline Rigidbody2DU5BU5D_t3935624075* get_EmptyArray_4() const { return ___EmptyArray_4; }
	inline Rigidbody2DU5BU5D_t3935624075** get_address_of_EmptyArray_4() { return &___EmptyArray_4; }
	inline void set_EmptyArray_4(Rigidbody2DU5BU5D_t3935624075* value)
	{
		___EmptyArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIST_1_T3403087445_H
#ifndef AUDIOPLAYABLEGRAPHEXTENSIONS_T3751460607_H
#define AUDIOPLAYABLEGRAPHEXTENSIONS_T3751460607_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Playables.AudioPlayableGraphExtensions
struct  AudioPlayableGraphExtensions_t3751460607  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AUDIOPLAYABLEGRAPHEXTENSIONS_T3751460607_H
#ifndef PLAYABLEBEHAVIOUR_T1007788125_H
#define PLAYABLEBEHAVIOUR_T1007788125_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Playables.PlayableBehaviour
struct  PlayableBehaviour_t1007788125  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYABLEBEHAVIOUR_T1007788125_H
#ifndef PLAYABLEEXTENSIONS_T1964461736_H
#define PLAYABLEEXTENSIONS_T1964461736_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Playables.PlayableExtensions
struct  PlayableExtensions_t1964461736  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYABLEEXTENSIONS_T1964461736_H
#ifndef ATTRIBUTE_T841672618_H
#define ATTRIBUTE_T841672618_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Attribute
struct  Attribute_t841672618  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ATTRIBUTE_T841672618_H
#ifndef RANDOM_T347659896_H
#define RANDOM_T347659896_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Random
struct  Random_t347659896  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RANDOM_T347659896_H
#ifndef YIELDINSTRUCTION_T1893592390_H
#define YIELDINSTRUCTION_T1893592390_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.YieldInstruction
struct  YieldInstruction_t1893592390  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.YieldInstruction
struct YieldInstruction_t1893592390_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.YieldInstruction
struct YieldInstruction_t1893592390_marshaled_com
{
};
#endif // YIELDINSTRUCTION_T1893592390_H
#ifndef RECTTRANSFORMUTILITY_T535428424_H
#define RECTTRANSFORMUTILITY_T535428424_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.RectTransformUtility
struct  RectTransformUtility_t535428424  : public RuntimeObject
{
public:

public:
};

struct RectTransformUtility_t535428424_StaticFields
{
public:
	// UnityEngine.Vector3[] UnityEngine.RectTransformUtility::s_Corners
	Vector3U5BU5D_t981680887* ___s_Corners_0;

public:
	inline static int32_t get_offset_of_s_Corners_0() { return static_cast<int32_t>(offsetof(RectTransformUtility_t535428424_StaticFields, ___s_Corners_0)); }
	inline Vector3U5BU5D_t981680887* get_s_Corners_0() const { return ___s_Corners_0; }
	inline Vector3U5BU5D_t981680887** get_address_of_s_Corners_0() { return &___s_Corners_0; }
	inline void set_s_Corners_0(Vector3U5BU5D_t981680887* value)
	{
		___s_Corners_0 = value;
		Il2CppCodeGenWriteBarrier((&___s_Corners_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RECTTRANSFORMUTILITY_T535428424_H
#ifndef REMOTESETTINGS_T2746965213_H
#define REMOTESETTINGS_T2746965213_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.RemoteSettings
struct  RemoteSettings_t2746965213  : public RuntimeObject
{
public:

public:
};

struct RemoteSettings_t2746965213_StaticFields
{
public:
	// UnityEngine.RemoteSettings/UpdatedEventHandler UnityEngine.RemoteSettings::Updated
	UpdatedEventHandler_t729869569 * ___Updated_0;

public:
	inline static int32_t get_offset_of_Updated_0() { return static_cast<int32_t>(offsetof(RemoteSettings_t2746965213_StaticFields, ___Updated_0)); }
	inline UpdatedEventHandler_t729869569 * get_Updated_0() const { return ___Updated_0; }
	inline UpdatedEventHandler_t729869569 ** get_address_of_Updated_0() { return &___Updated_0; }
	inline void set_Updated_0(UpdatedEventHandler_t729869569 * value)
	{
		___Updated_0 = value;
		Il2CppCodeGenWriteBarrier((&___Updated_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REMOTESETTINGS_T2746965213_H
#ifndef RESOURCES_T1756866475_H
#define RESOURCES_T1756866475_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Resources
struct  Resources_t1756866475  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RESOURCES_T1756866475_H
#ifndef SCENEMANAGER_T1729095165_H
#define SCENEMANAGER_T1729095165_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.SceneManagement.SceneManager
struct  SceneManager_t1729095165  : public RuntimeObject
{
public:

public:
};

struct SceneManager_t1729095165_StaticFields
{
public:
	// UnityEngine.Events.UnityAction`2<UnityEngine.SceneManagement.Scene,UnityEngine.SceneManagement.LoadSceneMode> UnityEngine.SceneManagement.SceneManager::sceneLoaded
	UnityAction_2_t2064023076 * ___sceneLoaded_0;
	// UnityEngine.Events.UnityAction`1<UnityEngine.SceneManagement.Scene> UnityEngine.SceneManagement.SceneManager::sceneUnloaded
	UnityAction_1_t3896475417 * ___sceneUnloaded_1;
	// UnityEngine.Events.UnityAction`2<UnityEngine.SceneManagement.Scene,UnityEngine.SceneManagement.Scene> UnityEngine.SceneManagement.SceneManager::activeSceneChanged
	UnityAction_2_t121587842 * ___activeSceneChanged_2;

public:
	inline static int32_t get_offset_of_sceneLoaded_0() { return static_cast<int32_t>(offsetof(SceneManager_t1729095165_StaticFields, ___sceneLoaded_0)); }
	inline UnityAction_2_t2064023076 * get_sceneLoaded_0() const { return ___sceneLoaded_0; }
	inline UnityAction_2_t2064023076 ** get_address_of_sceneLoaded_0() { return &___sceneLoaded_0; }
	inline void set_sceneLoaded_0(UnityAction_2_t2064023076 * value)
	{
		___sceneLoaded_0 = value;
		Il2CppCodeGenWriteBarrier((&___sceneLoaded_0), value);
	}

	inline static int32_t get_offset_of_sceneUnloaded_1() { return static_cast<int32_t>(offsetof(SceneManager_t1729095165_StaticFields, ___sceneUnloaded_1)); }
	inline UnityAction_1_t3896475417 * get_sceneUnloaded_1() const { return ___sceneUnloaded_1; }
	inline UnityAction_1_t3896475417 ** get_address_of_sceneUnloaded_1() { return &___sceneUnloaded_1; }
	inline void set_sceneUnloaded_1(UnityAction_1_t3896475417 * value)
	{
		___sceneUnloaded_1 = value;
		Il2CppCodeGenWriteBarrier((&___sceneUnloaded_1), value);
	}

	inline static int32_t get_offset_of_activeSceneChanged_2() { return static_cast<int32_t>(offsetof(SceneManager_t1729095165_StaticFields, ___activeSceneChanged_2)); }
	inline UnityAction_2_t121587842 * get_activeSceneChanged_2() const { return ___activeSceneChanged_2; }
	inline UnityAction_2_t121587842 ** get_address_of_activeSceneChanged_2() { return &___activeSceneChanged_2; }
	inline void set_activeSceneChanged_2(UnityAction_2_t121587842 * value)
	{
		___activeSceneChanged_2 = value;
		Il2CppCodeGenWriteBarrier((&___activeSceneChanged_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCENEMANAGER_T1729095165_H
#ifndef SCREEN_T1275303481_H
#define SCREEN_T1275303481_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Screen
struct  Screen_t1275303481  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCREEN_T1275303481_H
#ifndef SCROLLVIEWSTATE_T1314793348_H
#define SCROLLVIEWSTATE_T1314793348_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ScrollViewState
struct  ScrollViewState_t1314793348  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCROLLVIEWSTATE_T1314793348_H
#ifndef SENDMOUSEEVENTS_T1825902553_H
#define SENDMOUSEEVENTS_T1825902553_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.SendMouseEvents
struct  SendMouseEvents_t1825902553  : public RuntimeObject
{
public:

public:
};

struct SendMouseEvents_t1825902553_StaticFields
{
public:
	// System.Boolean UnityEngine.SendMouseEvents::s_MouseUsed
	bool ___s_MouseUsed_0;
	// UnityEngine.SendMouseEvents/HitInfo[] UnityEngine.SendMouseEvents::m_LastHit
	HitInfoU5BU5D_t1076684586* ___m_LastHit_1;
	// UnityEngine.SendMouseEvents/HitInfo[] UnityEngine.SendMouseEvents::m_MouseDownHit
	HitInfoU5BU5D_t1076684586* ___m_MouseDownHit_2;
	// UnityEngine.SendMouseEvents/HitInfo[] UnityEngine.SendMouseEvents::m_CurrentHit
	HitInfoU5BU5D_t1076684586* ___m_CurrentHit_3;
	// UnityEngine.Camera[] UnityEngine.SendMouseEvents::m_Cameras
	CameraU5BU5D_t1519774542* ___m_Cameras_4;

public:
	inline static int32_t get_offset_of_s_MouseUsed_0() { return static_cast<int32_t>(offsetof(SendMouseEvents_t1825902553_StaticFields, ___s_MouseUsed_0)); }
	inline bool get_s_MouseUsed_0() const { return ___s_MouseUsed_0; }
	inline bool* get_address_of_s_MouseUsed_0() { return &___s_MouseUsed_0; }
	inline void set_s_MouseUsed_0(bool value)
	{
		___s_MouseUsed_0 = value;
	}

	inline static int32_t get_offset_of_m_LastHit_1() { return static_cast<int32_t>(offsetof(SendMouseEvents_t1825902553_StaticFields, ___m_LastHit_1)); }
	inline HitInfoU5BU5D_t1076684586* get_m_LastHit_1() const { return ___m_LastHit_1; }
	inline HitInfoU5BU5D_t1076684586** get_address_of_m_LastHit_1() { return &___m_LastHit_1; }
	inline void set_m_LastHit_1(HitInfoU5BU5D_t1076684586* value)
	{
		___m_LastHit_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_LastHit_1), value);
	}

	inline static int32_t get_offset_of_m_MouseDownHit_2() { return static_cast<int32_t>(offsetof(SendMouseEvents_t1825902553_StaticFields, ___m_MouseDownHit_2)); }
	inline HitInfoU5BU5D_t1076684586* get_m_MouseDownHit_2() const { return ___m_MouseDownHit_2; }
	inline HitInfoU5BU5D_t1076684586** get_address_of_m_MouseDownHit_2() { return &___m_MouseDownHit_2; }
	inline void set_m_MouseDownHit_2(HitInfoU5BU5D_t1076684586* value)
	{
		___m_MouseDownHit_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_MouseDownHit_2), value);
	}

	inline static int32_t get_offset_of_m_CurrentHit_3() { return static_cast<int32_t>(offsetof(SendMouseEvents_t1825902553_StaticFields, ___m_CurrentHit_3)); }
	inline HitInfoU5BU5D_t1076684586* get_m_CurrentHit_3() const { return ___m_CurrentHit_3; }
	inline HitInfoU5BU5D_t1076684586** get_address_of_m_CurrentHit_3() { return &___m_CurrentHit_3; }
	inline void set_m_CurrentHit_3(HitInfoU5BU5D_t1076684586* value)
	{
		___m_CurrentHit_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_CurrentHit_3), value);
	}

	inline static int32_t get_offset_of_m_Cameras_4() { return static_cast<int32_t>(offsetof(SendMouseEvents_t1825902553_StaticFields, ___m_Cameras_4)); }
	inline CameraU5BU5D_t1519774542* get_m_Cameras_4() const { return ___m_Cameras_4; }
	inline CameraU5BU5D_t1519774542** get_address_of_m_Cameras_4() { return &___m_Cameras_4; }
	inline void set_m_Cameras_4(CameraU5BU5D_t1519774542* value)
	{
		___m_Cameras_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_Cameras_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SENDMOUSEEVENTS_T1825902553_H
#ifndef SETUPCOROUTINE_T3653648753_H
#define SETUPCOROUTINE_T3653648753_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.SetupCoroutine
struct  SetupCoroutine_t3653648753  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SETUPCOROUTINE_T3653648753_H
#ifndef BINDER_T401576136_H
#define BINDER_T401576136_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.Binder
struct  Binder_t401576136  : public RuntimeObject
{
public:

public:
};

struct Binder_t401576136_StaticFields
{
public:
	// System.Reflection.Binder System.Reflection.Binder::default_binder
	Binder_t401576136 * ___default_binder_0;

public:
	inline static int32_t get_offset_of_default_binder_0() { return static_cast<int32_t>(offsetof(Binder_t401576136_StaticFields, ___default_binder_0)); }
	inline Binder_t401576136 * get_default_binder_0() const { return ___default_binder_0; }
	inline Binder_t401576136 ** get_address_of_default_binder_0() { return &___default_binder_0; }
	inline void set_default_binder_0(Binder_t401576136 * value)
	{
		___default_binder_0 = value;
		Il2CppCodeGenWriteBarrier((&___default_binder_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BINDER_T401576136_H
#ifndef CULTUREINFO_T4043279873_H
#define CULTUREINFO_T4043279873_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Globalization.CultureInfo
struct  CultureInfo_t4043279873  : public RuntimeObject
{
public:
	// System.Boolean System.Globalization.CultureInfo::m_isReadOnly
	bool ___m_isReadOnly_7;
	// System.Int32 System.Globalization.CultureInfo::cultureID
	int32_t ___cultureID_8;
	// System.Int32 System.Globalization.CultureInfo::parent_lcid
	int32_t ___parent_lcid_9;
	// System.Int32 System.Globalization.CultureInfo::specific_lcid
	int32_t ___specific_lcid_10;
	// System.Int32 System.Globalization.CultureInfo::datetime_index
	int32_t ___datetime_index_11;
	// System.Int32 System.Globalization.CultureInfo::number_index
	int32_t ___number_index_12;
	// System.Boolean System.Globalization.CultureInfo::m_useUserOverride
	bool ___m_useUserOverride_13;
	// System.Globalization.NumberFormatInfo modreq(System.Runtime.CompilerServices.IsVolatile) System.Globalization.CultureInfo::numInfo
	NumberFormatInfo_t3680167716 * ___numInfo_14;
	// System.Globalization.DateTimeFormatInfo modreq(System.Runtime.CompilerServices.IsVolatile) System.Globalization.CultureInfo::dateTimeInfo
	DateTimeFormatInfo_t354846801 * ___dateTimeInfo_15;
	// System.Globalization.TextInfo modreq(System.Runtime.CompilerServices.IsVolatile) System.Globalization.CultureInfo::textInfo
	TextInfo_t2655029620 * ___textInfo_16;
	// System.String System.Globalization.CultureInfo::m_name
	String_t* ___m_name_17;
	// System.String System.Globalization.CultureInfo::displayname
	String_t* ___displayname_18;
	// System.String System.Globalization.CultureInfo::englishname
	String_t* ___englishname_19;
	// System.String System.Globalization.CultureInfo::nativename
	String_t* ___nativename_20;
	// System.String System.Globalization.CultureInfo::iso3lang
	String_t* ___iso3lang_21;
	// System.String System.Globalization.CultureInfo::iso2lang
	String_t* ___iso2lang_22;
	// System.String System.Globalization.CultureInfo::icu_name
	String_t* ___icu_name_23;
	// System.String System.Globalization.CultureInfo::win3lang
	String_t* ___win3lang_24;
	// System.String System.Globalization.CultureInfo::territory
	String_t* ___territory_25;
	// System.Globalization.CompareInfo modreq(System.Runtime.CompilerServices.IsVolatile) System.Globalization.CultureInfo::compareInfo
	CompareInfo_t2456649274 * ___compareInfo_26;
	// System.Int32* System.Globalization.CultureInfo::calendar_data
	int32_t* ___calendar_data_27;
	// System.Void* System.Globalization.CultureInfo::textinfo_data
	void* ___textinfo_data_28;
	// System.Globalization.Calendar[] System.Globalization.CultureInfo::optional_calendars
	CalendarU5BU5D_t237754405* ___optional_calendars_29;
	// System.Globalization.CultureInfo System.Globalization.CultureInfo::parent_culture
	CultureInfo_t4043279873 * ___parent_culture_30;
	// System.Int32 System.Globalization.CultureInfo::m_dataItem
	int32_t ___m_dataItem_31;
	// System.Globalization.Calendar System.Globalization.CultureInfo::calendar
	Calendar_t3114948524 * ___calendar_32;
	// System.Boolean System.Globalization.CultureInfo::constructed
	bool ___constructed_33;
	// System.Byte[] System.Globalization.CultureInfo::cached_serialized_form
	ByteU5BU5D_t3172826560* ___cached_serialized_form_34;

public:
	inline static int32_t get_offset_of_m_isReadOnly_7() { return static_cast<int32_t>(offsetof(CultureInfo_t4043279873, ___m_isReadOnly_7)); }
	inline bool get_m_isReadOnly_7() const { return ___m_isReadOnly_7; }
	inline bool* get_address_of_m_isReadOnly_7() { return &___m_isReadOnly_7; }
	inline void set_m_isReadOnly_7(bool value)
	{
		___m_isReadOnly_7 = value;
	}

	inline static int32_t get_offset_of_cultureID_8() { return static_cast<int32_t>(offsetof(CultureInfo_t4043279873, ___cultureID_8)); }
	inline int32_t get_cultureID_8() const { return ___cultureID_8; }
	inline int32_t* get_address_of_cultureID_8() { return &___cultureID_8; }
	inline void set_cultureID_8(int32_t value)
	{
		___cultureID_8 = value;
	}

	inline static int32_t get_offset_of_parent_lcid_9() { return static_cast<int32_t>(offsetof(CultureInfo_t4043279873, ___parent_lcid_9)); }
	inline int32_t get_parent_lcid_9() const { return ___parent_lcid_9; }
	inline int32_t* get_address_of_parent_lcid_9() { return &___parent_lcid_9; }
	inline void set_parent_lcid_9(int32_t value)
	{
		___parent_lcid_9 = value;
	}

	inline static int32_t get_offset_of_specific_lcid_10() { return static_cast<int32_t>(offsetof(CultureInfo_t4043279873, ___specific_lcid_10)); }
	inline int32_t get_specific_lcid_10() const { return ___specific_lcid_10; }
	inline int32_t* get_address_of_specific_lcid_10() { return &___specific_lcid_10; }
	inline void set_specific_lcid_10(int32_t value)
	{
		___specific_lcid_10 = value;
	}

	inline static int32_t get_offset_of_datetime_index_11() { return static_cast<int32_t>(offsetof(CultureInfo_t4043279873, ___datetime_index_11)); }
	inline int32_t get_datetime_index_11() const { return ___datetime_index_11; }
	inline int32_t* get_address_of_datetime_index_11() { return &___datetime_index_11; }
	inline void set_datetime_index_11(int32_t value)
	{
		___datetime_index_11 = value;
	}

	inline static int32_t get_offset_of_number_index_12() { return static_cast<int32_t>(offsetof(CultureInfo_t4043279873, ___number_index_12)); }
	inline int32_t get_number_index_12() const { return ___number_index_12; }
	inline int32_t* get_address_of_number_index_12() { return &___number_index_12; }
	inline void set_number_index_12(int32_t value)
	{
		___number_index_12 = value;
	}

	inline static int32_t get_offset_of_m_useUserOverride_13() { return static_cast<int32_t>(offsetof(CultureInfo_t4043279873, ___m_useUserOverride_13)); }
	inline bool get_m_useUserOverride_13() const { return ___m_useUserOverride_13; }
	inline bool* get_address_of_m_useUserOverride_13() { return &___m_useUserOverride_13; }
	inline void set_m_useUserOverride_13(bool value)
	{
		___m_useUserOverride_13 = value;
	}

	inline static int32_t get_offset_of_numInfo_14() { return static_cast<int32_t>(offsetof(CultureInfo_t4043279873, ___numInfo_14)); }
	inline NumberFormatInfo_t3680167716 * get_numInfo_14() const { return ___numInfo_14; }
	inline NumberFormatInfo_t3680167716 ** get_address_of_numInfo_14() { return &___numInfo_14; }
	inline void set_numInfo_14(NumberFormatInfo_t3680167716 * value)
	{
		___numInfo_14 = value;
		Il2CppCodeGenWriteBarrier((&___numInfo_14), value);
	}

	inline static int32_t get_offset_of_dateTimeInfo_15() { return static_cast<int32_t>(offsetof(CultureInfo_t4043279873, ___dateTimeInfo_15)); }
	inline DateTimeFormatInfo_t354846801 * get_dateTimeInfo_15() const { return ___dateTimeInfo_15; }
	inline DateTimeFormatInfo_t354846801 ** get_address_of_dateTimeInfo_15() { return &___dateTimeInfo_15; }
	inline void set_dateTimeInfo_15(DateTimeFormatInfo_t354846801 * value)
	{
		___dateTimeInfo_15 = value;
		Il2CppCodeGenWriteBarrier((&___dateTimeInfo_15), value);
	}

	inline static int32_t get_offset_of_textInfo_16() { return static_cast<int32_t>(offsetof(CultureInfo_t4043279873, ___textInfo_16)); }
	inline TextInfo_t2655029620 * get_textInfo_16() const { return ___textInfo_16; }
	inline TextInfo_t2655029620 ** get_address_of_textInfo_16() { return &___textInfo_16; }
	inline void set_textInfo_16(TextInfo_t2655029620 * value)
	{
		___textInfo_16 = value;
		Il2CppCodeGenWriteBarrier((&___textInfo_16), value);
	}

	inline static int32_t get_offset_of_m_name_17() { return static_cast<int32_t>(offsetof(CultureInfo_t4043279873, ___m_name_17)); }
	inline String_t* get_m_name_17() const { return ___m_name_17; }
	inline String_t** get_address_of_m_name_17() { return &___m_name_17; }
	inline void set_m_name_17(String_t* value)
	{
		___m_name_17 = value;
		Il2CppCodeGenWriteBarrier((&___m_name_17), value);
	}

	inline static int32_t get_offset_of_displayname_18() { return static_cast<int32_t>(offsetof(CultureInfo_t4043279873, ___displayname_18)); }
	inline String_t* get_displayname_18() const { return ___displayname_18; }
	inline String_t** get_address_of_displayname_18() { return &___displayname_18; }
	inline void set_displayname_18(String_t* value)
	{
		___displayname_18 = value;
		Il2CppCodeGenWriteBarrier((&___displayname_18), value);
	}

	inline static int32_t get_offset_of_englishname_19() { return static_cast<int32_t>(offsetof(CultureInfo_t4043279873, ___englishname_19)); }
	inline String_t* get_englishname_19() const { return ___englishname_19; }
	inline String_t** get_address_of_englishname_19() { return &___englishname_19; }
	inline void set_englishname_19(String_t* value)
	{
		___englishname_19 = value;
		Il2CppCodeGenWriteBarrier((&___englishname_19), value);
	}

	inline static int32_t get_offset_of_nativename_20() { return static_cast<int32_t>(offsetof(CultureInfo_t4043279873, ___nativename_20)); }
	inline String_t* get_nativename_20() const { return ___nativename_20; }
	inline String_t** get_address_of_nativename_20() { return &___nativename_20; }
	inline void set_nativename_20(String_t* value)
	{
		___nativename_20 = value;
		Il2CppCodeGenWriteBarrier((&___nativename_20), value);
	}

	inline static int32_t get_offset_of_iso3lang_21() { return static_cast<int32_t>(offsetof(CultureInfo_t4043279873, ___iso3lang_21)); }
	inline String_t* get_iso3lang_21() const { return ___iso3lang_21; }
	inline String_t** get_address_of_iso3lang_21() { return &___iso3lang_21; }
	inline void set_iso3lang_21(String_t* value)
	{
		___iso3lang_21 = value;
		Il2CppCodeGenWriteBarrier((&___iso3lang_21), value);
	}

	inline static int32_t get_offset_of_iso2lang_22() { return static_cast<int32_t>(offsetof(CultureInfo_t4043279873, ___iso2lang_22)); }
	inline String_t* get_iso2lang_22() const { return ___iso2lang_22; }
	inline String_t** get_address_of_iso2lang_22() { return &___iso2lang_22; }
	inline void set_iso2lang_22(String_t* value)
	{
		___iso2lang_22 = value;
		Il2CppCodeGenWriteBarrier((&___iso2lang_22), value);
	}

	inline static int32_t get_offset_of_icu_name_23() { return static_cast<int32_t>(offsetof(CultureInfo_t4043279873, ___icu_name_23)); }
	inline String_t* get_icu_name_23() const { return ___icu_name_23; }
	inline String_t** get_address_of_icu_name_23() { return &___icu_name_23; }
	inline void set_icu_name_23(String_t* value)
	{
		___icu_name_23 = value;
		Il2CppCodeGenWriteBarrier((&___icu_name_23), value);
	}

	inline static int32_t get_offset_of_win3lang_24() { return static_cast<int32_t>(offsetof(CultureInfo_t4043279873, ___win3lang_24)); }
	inline String_t* get_win3lang_24() const { return ___win3lang_24; }
	inline String_t** get_address_of_win3lang_24() { return &___win3lang_24; }
	inline void set_win3lang_24(String_t* value)
	{
		___win3lang_24 = value;
		Il2CppCodeGenWriteBarrier((&___win3lang_24), value);
	}

	inline static int32_t get_offset_of_territory_25() { return static_cast<int32_t>(offsetof(CultureInfo_t4043279873, ___territory_25)); }
	inline String_t* get_territory_25() const { return ___territory_25; }
	inline String_t** get_address_of_territory_25() { return &___territory_25; }
	inline void set_territory_25(String_t* value)
	{
		___territory_25 = value;
		Il2CppCodeGenWriteBarrier((&___territory_25), value);
	}

	inline static int32_t get_offset_of_compareInfo_26() { return static_cast<int32_t>(offsetof(CultureInfo_t4043279873, ___compareInfo_26)); }
	inline CompareInfo_t2456649274 * get_compareInfo_26() const { return ___compareInfo_26; }
	inline CompareInfo_t2456649274 ** get_address_of_compareInfo_26() { return &___compareInfo_26; }
	inline void set_compareInfo_26(CompareInfo_t2456649274 * value)
	{
		___compareInfo_26 = value;
		Il2CppCodeGenWriteBarrier((&___compareInfo_26), value);
	}

	inline static int32_t get_offset_of_calendar_data_27() { return static_cast<int32_t>(offsetof(CultureInfo_t4043279873, ___calendar_data_27)); }
	inline int32_t* get_calendar_data_27() const { return ___calendar_data_27; }
	inline int32_t** get_address_of_calendar_data_27() { return &___calendar_data_27; }
	inline void set_calendar_data_27(int32_t* value)
	{
		___calendar_data_27 = value;
	}

	inline static int32_t get_offset_of_textinfo_data_28() { return static_cast<int32_t>(offsetof(CultureInfo_t4043279873, ___textinfo_data_28)); }
	inline void* get_textinfo_data_28() const { return ___textinfo_data_28; }
	inline void** get_address_of_textinfo_data_28() { return &___textinfo_data_28; }
	inline void set_textinfo_data_28(void* value)
	{
		___textinfo_data_28 = value;
	}

	inline static int32_t get_offset_of_optional_calendars_29() { return static_cast<int32_t>(offsetof(CultureInfo_t4043279873, ___optional_calendars_29)); }
	inline CalendarU5BU5D_t237754405* get_optional_calendars_29() const { return ___optional_calendars_29; }
	inline CalendarU5BU5D_t237754405** get_address_of_optional_calendars_29() { return &___optional_calendars_29; }
	inline void set_optional_calendars_29(CalendarU5BU5D_t237754405* value)
	{
		___optional_calendars_29 = value;
		Il2CppCodeGenWriteBarrier((&___optional_calendars_29), value);
	}

	inline static int32_t get_offset_of_parent_culture_30() { return static_cast<int32_t>(offsetof(CultureInfo_t4043279873, ___parent_culture_30)); }
	inline CultureInfo_t4043279873 * get_parent_culture_30() const { return ___parent_culture_30; }
	inline CultureInfo_t4043279873 ** get_address_of_parent_culture_30() { return &___parent_culture_30; }
	inline void set_parent_culture_30(CultureInfo_t4043279873 * value)
	{
		___parent_culture_30 = value;
		Il2CppCodeGenWriteBarrier((&___parent_culture_30), value);
	}

	inline static int32_t get_offset_of_m_dataItem_31() { return static_cast<int32_t>(offsetof(CultureInfo_t4043279873, ___m_dataItem_31)); }
	inline int32_t get_m_dataItem_31() const { return ___m_dataItem_31; }
	inline int32_t* get_address_of_m_dataItem_31() { return &___m_dataItem_31; }
	inline void set_m_dataItem_31(int32_t value)
	{
		___m_dataItem_31 = value;
	}

	inline static int32_t get_offset_of_calendar_32() { return static_cast<int32_t>(offsetof(CultureInfo_t4043279873, ___calendar_32)); }
	inline Calendar_t3114948524 * get_calendar_32() const { return ___calendar_32; }
	inline Calendar_t3114948524 ** get_address_of_calendar_32() { return &___calendar_32; }
	inline void set_calendar_32(Calendar_t3114948524 * value)
	{
		___calendar_32 = value;
		Il2CppCodeGenWriteBarrier((&___calendar_32), value);
	}

	inline static int32_t get_offset_of_constructed_33() { return static_cast<int32_t>(offsetof(CultureInfo_t4043279873, ___constructed_33)); }
	inline bool get_constructed_33() const { return ___constructed_33; }
	inline bool* get_address_of_constructed_33() { return &___constructed_33; }
	inline void set_constructed_33(bool value)
	{
		___constructed_33 = value;
	}

	inline static int32_t get_offset_of_cached_serialized_form_34() { return static_cast<int32_t>(offsetof(CultureInfo_t4043279873, ___cached_serialized_form_34)); }
	inline ByteU5BU5D_t3172826560* get_cached_serialized_form_34() const { return ___cached_serialized_form_34; }
	inline ByteU5BU5D_t3172826560** get_address_of_cached_serialized_form_34() { return &___cached_serialized_form_34; }
	inline void set_cached_serialized_form_34(ByteU5BU5D_t3172826560* value)
	{
		___cached_serialized_form_34 = value;
		Il2CppCodeGenWriteBarrier((&___cached_serialized_form_34), value);
	}
};

struct CultureInfo_t4043279873_StaticFields
{
public:
	// System.Globalization.CultureInfo modreq(System.Runtime.CompilerServices.IsVolatile) System.Globalization.CultureInfo::invariant_culture_info
	CultureInfo_t4043279873 * ___invariant_culture_info_4;
	// System.Object System.Globalization.CultureInfo::shared_table_lock
	RuntimeObject * ___shared_table_lock_5;
	// System.Int32 System.Globalization.CultureInfo::BootstrapCultureID
	int32_t ___BootstrapCultureID_6;
	// System.String System.Globalization.CultureInfo::MSG_READONLY
	String_t* ___MSG_READONLY_35;
	// System.Collections.Hashtable System.Globalization.CultureInfo::shared_by_number
	Hashtable_t1690131612 * ___shared_by_number_36;
	// System.Collections.Hashtable System.Globalization.CultureInfo::shared_by_name
	Hashtable_t1690131612 * ___shared_by_name_37;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.Globalization.CultureInfo::<>f__switch$map19
	Dictionary_2_t1112866524 * ___U3CU3Ef__switchU24map19_38;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.Globalization.CultureInfo::<>f__switch$map1A
	Dictionary_2_t1112866524 * ___U3CU3Ef__switchU24map1A_39;

public:
	inline static int32_t get_offset_of_invariant_culture_info_4() { return static_cast<int32_t>(offsetof(CultureInfo_t4043279873_StaticFields, ___invariant_culture_info_4)); }
	inline CultureInfo_t4043279873 * get_invariant_culture_info_4() const { return ___invariant_culture_info_4; }
	inline CultureInfo_t4043279873 ** get_address_of_invariant_culture_info_4() { return &___invariant_culture_info_4; }
	inline void set_invariant_culture_info_4(CultureInfo_t4043279873 * value)
	{
		___invariant_culture_info_4 = value;
		Il2CppCodeGenWriteBarrier((&___invariant_culture_info_4), value);
	}

	inline static int32_t get_offset_of_shared_table_lock_5() { return static_cast<int32_t>(offsetof(CultureInfo_t4043279873_StaticFields, ___shared_table_lock_5)); }
	inline RuntimeObject * get_shared_table_lock_5() const { return ___shared_table_lock_5; }
	inline RuntimeObject ** get_address_of_shared_table_lock_5() { return &___shared_table_lock_5; }
	inline void set_shared_table_lock_5(RuntimeObject * value)
	{
		___shared_table_lock_5 = value;
		Il2CppCodeGenWriteBarrier((&___shared_table_lock_5), value);
	}

	inline static int32_t get_offset_of_BootstrapCultureID_6() { return static_cast<int32_t>(offsetof(CultureInfo_t4043279873_StaticFields, ___BootstrapCultureID_6)); }
	inline int32_t get_BootstrapCultureID_6() const { return ___BootstrapCultureID_6; }
	inline int32_t* get_address_of_BootstrapCultureID_6() { return &___BootstrapCultureID_6; }
	inline void set_BootstrapCultureID_6(int32_t value)
	{
		___BootstrapCultureID_6 = value;
	}

	inline static int32_t get_offset_of_MSG_READONLY_35() { return static_cast<int32_t>(offsetof(CultureInfo_t4043279873_StaticFields, ___MSG_READONLY_35)); }
	inline String_t* get_MSG_READONLY_35() const { return ___MSG_READONLY_35; }
	inline String_t** get_address_of_MSG_READONLY_35() { return &___MSG_READONLY_35; }
	inline void set_MSG_READONLY_35(String_t* value)
	{
		___MSG_READONLY_35 = value;
		Il2CppCodeGenWriteBarrier((&___MSG_READONLY_35), value);
	}

	inline static int32_t get_offset_of_shared_by_number_36() { return static_cast<int32_t>(offsetof(CultureInfo_t4043279873_StaticFields, ___shared_by_number_36)); }
	inline Hashtable_t1690131612 * get_shared_by_number_36() const { return ___shared_by_number_36; }
	inline Hashtable_t1690131612 ** get_address_of_shared_by_number_36() { return &___shared_by_number_36; }
	inline void set_shared_by_number_36(Hashtable_t1690131612 * value)
	{
		___shared_by_number_36 = value;
		Il2CppCodeGenWriteBarrier((&___shared_by_number_36), value);
	}

	inline static int32_t get_offset_of_shared_by_name_37() { return static_cast<int32_t>(offsetof(CultureInfo_t4043279873_StaticFields, ___shared_by_name_37)); }
	inline Hashtable_t1690131612 * get_shared_by_name_37() const { return ___shared_by_name_37; }
	inline Hashtable_t1690131612 ** get_address_of_shared_by_name_37() { return &___shared_by_name_37; }
	inline void set_shared_by_name_37(Hashtable_t1690131612 * value)
	{
		___shared_by_name_37 = value;
		Il2CppCodeGenWriteBarrier((&___shared_by_name_37), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map19_38() { return static_cast<int32_t>(offsetof(CultureInfo_t4043279873_StaticFields, ___U3CU3Ef__switchU24map19_38)); }
	inline Dictionary_2_t1112866524 * get_U3CU3Ef__switchU24map19_38() const { return ___U3CU3Ef__switchU24map19_38; }
	inline Dictionary_2_t1112866524 ** get_address_of_U3CU3Ef__switchU24map19_38() { return &___U3CU3Ef__switchU24map19_38; }
	inline void set_U3CU3Ef__switchU24map19_38(Dictionary_2_t1112866524 * value)
	{
		___U3CU3Ef__switchU24map19_38 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__switchU24map19_38), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map1A_39() { return static_cast<int32_t>(offsetof(CultureInfo_t4043279873_StaticFields, ___U3CU3Ef__switchU24map1A_39)); }
	inline Dictionary_2_t1112866524 * get_U3CU3Ef__switchU24map1A_39() const { return ___U3CU3Ef__switchU24map1A_39; }
	inline Dictionary_2_t1112866524 ** get_address_of_U3CU3Ef__switchU24map1A_39() { return &___U3CU3Ef__switchU24map1A_39; }
	inline void set_U3CU3Ef__switchU24map1A_39(Dictionary_2_t1112866524 * value)
	{
		___U3CU3Ef__switchU24map1A_39 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__switchU24map1A_39), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CULTUREINFO_T4043279873_H
#ifndef SLIDERSTATE_T3254875344_H
#define SLIDERSTATE_T3254875344_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.SliderState
struct  SliderState_t3254875344  : public RuntimeObject
{
public:
	// System.Single UnityEngine.SliderState::dragStartPos
	float ___dragStartPos_0;
	// System.Single UnityEngine.SliderState::dragStartValue
	float ___dragStartValue_1;
	// System.Boolean UnityEngine.SliderState::isDragging
	bool ___isDragging_2;

public:
	inline static int32_t get_offset_of_dragStartPos_0() { return static_cast<int32_t>(offsetof(SliderState_t3254875344, ___dragStartPos_0)); }
	inline float get_dragStartPos_0() const { return ___dragStartPos_0; }
	inline float* get_address_of_dragStartPos_0() { return &___dragStartPos_0; }
	inline void set_dragStartPos_0(float value)
	{
		___dragStartPos_0 = value;
	}

	inline static int32_t get_offset_of_dragStartValue_1() { return static_cast<int32_t>(offsetof(SliderState_t3254875344, ___dragStartValue_1)); }
	inline float get_dragStartValue_1() const { return ___dragStartValue_1; }
	inline float* get_address_of_dragStartValue_1() { return &___dragStartValue_1; }
	inline void set_dragStartValue_1(float value)
	{
		___dragStartValue_1 = value;
	}

	inline static int32_t get_offset_of_isDragging_2() { return static_cast<int32_t>(offsetof(SliderState_t3254875344, ___isDragging_2)); }
	inline bool get_isDragging_2() const { return ___isDragging_2; }
	inline bool* get_address_of_isDragging_2() { return &___isDragging_2; }
	inline void set_isDragging_2(bool value)
	{
		___isDragging_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SLIDERSTATE_T3254875344_H
#ifndef VALUETYPE_T1306072465_H
#define VALUETYPE_T1306072465_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t1306072465  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t1306072465_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t1306072465_marshaled_com
{
};
#endif // VALUETYPE_T1306072465_H
#ifndef MEMBERINFO_T_H
#define MEMBERINFO_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.MemberInfo
struct  MemberInfo_t  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MEMBERINFO_T_H
#ifndef PHYSICS2D_T1358151724_H
#define PHYSICS2D_T1358151724_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Physics2D
struct  Physics2D_t1358151724  : public RuntimeObject
{
public:

public:
};

struct Physics2D_t1358151724_StaticFields
{
public:
	// System.Collections.Generic.List`1<UnityEngine.Rigidbody2D> UnityEngine.Physics2D::m_LastDisabledRigidbody2D
	List_1_t3403087445 * ___m_LastDisabledRigidbody2D_0;

public:
	inline static int32_t get_offset_of_m_LastDisabledRigidbody2D_0() { return static_cast<int32_t>(offsetof(Physics2D_t1358151724_StaticFields, ___m_LastDisabledRigidbody2D_0)); }
	inline List_1_t3403087445 * get_m_LastDisabledRigidbody2D_0() const { return ___m_LastDisabledRigidbody2D_0; }
	inline List_1_t3403087445 ** get_address_of_m_LastDisabledRigidbody2D_0() { return &___m_LastDisabledRigidbody2D_0; }
	inline void set_m_LastDisabledRigidbody2D_0(List_1_t3403087445 * value)
	{
		___m_LastDisabledRigidbody2D_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_LastDisabledRigidbody2D_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PHYSICS2D_T1358151724_H
#ifndef PHYSICS_T908271923_H
#define PHYSICS_T908271923_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Physics
struct  Physics_t908271923  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PHYSICS_T908271923_H
#ifndef EXCEPTION_T1975590229_H
#define EXCEPTION_T1975590229_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Exception
struct  Exception_t1975590229  : public RuntimeObject
{
public:
	// System.IntPtr[] System.Exception::trace_ips
	IntPtrU5BU5D_t439965300* ___trace_ips_0;
	// System.Exception System.Exception::inner_exception
	Exception_t1975590229 * ___inner_exception_1;
	// System.String System.Exception::message
	String_t* ___message_2;
	// System.String System.Exception::help_link
	String_t* ___help_link_3;
	// System.String System.Exception::class_name
	String_t* ___class_name_4;
	// System.String System.Exception::stack_trace
	String_t* ___stack_trace_5;
	// System.String System.Exception::_remoteStackTraceString
	String_t* ____remoteStackTraceString_6;
	// System.Int32 System.Exception::remote_stack_index
	int32_t ___remote_stack_index_7;
	// System.Int32 System.Exception::hresult
	int32_t ___hresult_8;
	// System.String System.Exception::source
	String_t* ___source_9;
	// System.Collections.IDictionary System.Exception::_data
	RuntimeObject* ____data_10;

public:
	inline static int32_t get_offset_of_trace_ips_0() { return static_cast<int32_t>(offsetof(Exception_t1975590229, ___trace_ips_0)); }
	inline IntPtrU5BU5D_t439965300* get_trace_ips_0() const { return ___trace_ips_0; }
	inline IntPtrU5BU5D_t439965300** get_address_of_trace_ips_0() { return &___trace_ips_0; }
	inline void set_trace_ips_0(IntPtrU5BU5D_t439965300* value)
	{
		___trace_ips_0 = value;
		Il2CppCodeGenWriteBarrier((&___trace_ips_0), value);
	}

	inline static int32_t get_offset_of_inner_exception_1() { return static_cast<int32_t>(offsetof(Exception_t1975590229, ___inner_exception_1)); }
	inline Exception_t1975590229 * get_inner_exception_1() const { return ___inner_exception_1; }
	inline Exception_t1975590229 ** get_address_of_inner_exception_1() { return &___inner_exception_1; }
	inline void set_inner_exception_1(Exception_t1975590229 * value)
	{
		___inner_exception_1 = value;
		Il2CppCodeGenWriteBarrier((&___inner_exception_1), value);
	}

	inline static int32_t get_offset_of_message_2() { return static_cast<int32_t>(offsetof(Exception_t1975590229, ___message_2)); }
	inline String_t* get_message_2() const { return ___message_2; }
	inline String_t** get_address_of_message_2() { return &___message_2; }
	inline void set_message_2(String_t* value)
	{
		___message_2 = value;
		Il2CppCodeGenWriteBarrier((&___message_2), value);
	}

	inline static int32_t get_offset_of_help_link_3() { return static_cast<int32_t>(offsetof(Exception_t1975590229, ___help_link_3)); }
	inline String_t* get_help_link_3() const { return ___help_link_3; }
	inline String_t** get_address_of_help_link_3() { return &___help_link_3; }
	inline void set_help_link_3(String_t* value)
	{
		___help_link_3 = value;
		Il2CppCodeGenWriteBarrier((&___help_link_3), value);
	}

	inline static int32_t get_offset_of_class_name_4() { return static_cast<int32_t>(offsetof(Exception_t1975590229, ___class_name_4)); }
	inline String_t* get_class_name_4() const { return ___class_name_4; }
	inline String_t** get_address_of_class_name_4() { return &___class_name_4; }
	inline void set_class_name_4(String_t* value)
	{
		___class_name_4 = value;
		Il2CppCodeGenWriteBarrier((&___class_name_4), value);
	}

	inline static int32_t get_offset_of_stack_trace_5() { return static_cast<int32_t>(offsetof(Exception_t1975590229, ___stack_trace_5)); }
	inline String_t* get_stack_trace_5() const { return ___stack_trace_5; }
	inline String_t** get_address_of_stack_trace_5() { return &___stack_trace_5; }
	inline void set_stack_trace_5(String_t* value)
	{
		___stack_trace_5 = value;
		Il2CppCodeGenWriteBarrier((&___stack_trace_5), value);
	}

	inline static int32_t get_offset_of__remoteStackTraceString_6() { return static_cast<int32_t>(offsetof(Exception_t1975590229, ____remoteStackTraceString_6)); }
	inline String_t* get__remoteStackTraceString_6() const { return ____remoteStackTraceString_6; }
	inline String_t** get_address_of__remoteStackTraceString_6() { return &____remoteStackTraceString_6; }
	inline void set__remoteStackTraceString_6(String_t* value)
	{
		____remoteStackTraceString_6 = value;
		Il2CppCodeGenWriteBarrier((&____remoteStackTraceString_6), value);
	}

	inline static int32_t get_offset_of_remote_stack_index_7() { return static_cast<int32_t>(offsetof(Exception_t1975590229, ___remote_stack_index_7)); }
	inline int32_t get_remote_stack_index_7() const { return ___remote_stack_index_7; }
	inline int32_t* get_address_of_remote_stack_index_7() { return &___remote_stack_index_7; }
	inline void set_remote_stack_index_7(int32_t value)
	{
		___remote_stack_index_7 = value;
	}

	inline static int32_t get_offset_of_hresult_8() { return static_cast<int32_t>(offsetof(Exception_t1975590229, ___hresult_8)); }
	inline int32_t get_hresult_8() const { return ___hresult_8; }
	inline int32_t* get_address_of_hresult_8() { return &___hresult_8; }
	inline void set_hresult_8(int32_t value)
	{
		___hresult_8 = value;
	}

	inline static int32_t get_offset_of_source_9() { return static_cast<int32_t>(offsetof(Exception_t1975590229, ___source_9)); }
	inline String_t* get_source_9() const { return ___source_9; }
	inline String_t** get_address_of_source_9() { return &___source_9; }
	inline void set_source_9(String_t* value)
	{
		___source_9 = value;
		Il2CppCodeGenWriteBarrier((&___source_9), value);
	}

	inline static int32_t get_offset_of__data_10() { return static_cast<int32_t>(offsetof(Exception_t1975590229, ____data_10)); }
	inline RuntimeObject* get__data_10() const { return ____data_10; }
	inline RuntimeObject** get_address_of__data_10() { return &____data_10; }
	inline void set__data_10(RuntimeObject* value)
	{
		____data_10 = value;
		Il2CppCodeGenWriteBarrier((&____data_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXCEPTION_T1975590229_H
#ifndef STRING_T_H
#define STRING_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.String
struct  String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::length
	int32_t ___length_0;
	// System.Char System.String::start_char
	Il2CppChar ___start_char_1;

public:
	inline static int32_t get_offset_of_length_0() { return static_cast<int32_t>(offsetof(String_t, ___length_0)); }
	inline int32_t get_length_0() const { return ___length_0; }
	inline int32_t* get_address_of_length_0() { return &___length_0; }
	inline void set_length_0(int32_t value)
	{
		___length_0 = value;
	}

	inline static int32_t get_offset_of_start_char_1() { return static_cast<int32_t>(offsetof(String_t, ___start_char_1)); }
	inline Il2CppChar get_start_char_1() const { return ___start_char_1; }
	inline Il2CppChar* get_address_of_start_char_1() { return &___start_char_1; }
	inline void set_start_char_1(Il2CppChar value)
	{
		___start_char_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_2;
	// System.Char[] System.String::WhiteChars
	CharU5BU5D_t45629911* ___WhiteChars_3;

public:
	inline static int32_t get_offset_of_Empty_2() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_2)); }
	inline String_t* get_Empty_2() const { return ___Empty_2; }
	inline String_t** get_address_of_Empty_2() { return &___Empty_2; }
	inline void set_Empty_2(String_t* value)
	{
		___Empty_2 = value;
		Il2CppCodeGenWriteBarrier((&___Empty_2), value);
	}

	inline static int32_t get_offset_of_WhiteChars_3() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___WhiteChars_3)); }
	inline CharU5BU5D_t45629911* get_WhiteChars_3() const { return ___WhiteChars_3; }
	inline CharU5BU5D_t45629911** get_address_of_WhiteChars_3() { return &___WhiteChars_3; }
	inline void set_WhiteChars_3(CharU5BU5D_t45629911* value)
	{
		___WhiteChars_3 = value;
		Il2CppCodeGenWriteBarrier((&___WhiteChars_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STRING_T_H
#ifndef INT64_T2934711825_H
#define INT64_T2934711825_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Int64
struct  Int64_t2934711825 
{
public:
	// System.Int64 System.Int64::m_value
	int64_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Int64_t2934711825, ___m_value_0)); }
	inline int64_t get_m_value_0() const { return ___m_value_0; }
	inline int64_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(int64_t value)
	{
		___m_value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT64_T2934711825_H
#ifndef USEDBYNATIVECODEATTRIBUTE_T1334661680_H
#define USEDBYNATIVECODEATTRIBUTE_T1334661680_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Scripting.UsedByNativeCodeAttribute
struct  UsedByNativeCodeAttribute_t1334661680  : public Attribute_t841672618
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // USEDBYNATIVECODEATTRIBUTE_T1334661680_H
#ifndef REQUIREDBYNATIVECODEATTRIBUTE_T814971270_H
#define REQUIREDBYNATIVECODEATTRIBUTE_T814971270_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Scripting.RequiredByNativeCodeAttribute
struct  RequiredByNativeCodeAttribute_t814971270  : public Attribute_t841672618
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REQUIREDBYNATIVECODEATTRIBUTE_T814971270_H
#ifndef GENERATEDBYOLDBINDINGSGENERATORATTRIBUTE_T2914791882_H
#define GENERATEDBYOLDBINDINGSGENERATORATTRIBUTE_T2914791882_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Scripting.GeneratedByOldBindingsGeneratorAttribute
struct  GeneratedByOldBindingsGeneratorAttribute_t2914791882  : public Attribute_t841672618
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GENERATEDBYOLDBINDINGSGENERATORATTRIBUTE_T2914791882_H
#ifndef MOVEDFROMATTRIBUTE_T2669718911_H
#define MOVEDFROMATTRIBUTE_T2669718911_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Scripting.APIUpdating.MovedFromAttribute
struct  MovedFromAttribute_t2669718911  : public Attribute_t841672618
{
public:
	// System.String UnityEngine.Scripting.APIUpdating.MovedFromAttribute::<Namespace>k__BackingField
	String_t* ___U3CNamespaceU3Ek__BackingField_0;
	// System.Boolean UnityEngine.Scripting.APIUpdating.MovedFromAttribute::<IsInDifferentAssembly>k__BackingField
	bool ___U3CIsInDifferentAssemblyU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CNamespaceU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(MovedFromAttribute_t2669718911, ___U3CNamespaceU3Ek__BackingField_0)); }
	inline String_t* get_U3CNamespaceU3Ek__BackingField_0() const { return ___U3CNamespaceU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CNamespaceU3Ek__BackingField_0() { return &___U3CNamespaceU3Ek__BackingField_0; }
	inline void set_U3CNamespaceU3Ek__BackingField_0(String_t* value)
	{
		___U3CNamespaceU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CNamespaceU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CIsInDifferentAssemblyU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(MovedFromAttribute_t2669718911, ___U3CIsInDifferentAssemblyU3Ek__BackingField_1)); }
	inline bool get_U3CIsInDifferentAssemblyU3Ek__BackingField_1() const { return ___U3CIsInDifferentAssemblyU3Ek__BackingField_1; }
	inline bool* get_address_of_U3CIsInDifferentAssemblyU3Ek__BackingField_1() { return &___U3CIsInDifferentAssemblyU3Ek__BackingField_1; }
	inline void set_U3CIsInDifferentAssemblyU3Ek__BackingField_1(bool value)
	{
		___U3CIsInDifferentAssemblyU3Ek__BackingField_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MOVEDFROMATTRIBUTE_T2669718911_H
#ifndef PREFERBINARYSERIALIZATION_T3755844044_H
#define PREFERBINARYSERIALIZATION_T3755844044_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PreferBinarySerialization
struct  PreferBinarySerialization_t3755844044  : public Attribute_t841672618
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PREFERBINARYSERIALIZATION_T3755844044_H
#ifndef VECTOR3_T2903530434_H
#define VECTOR3_T2903530434_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_t2903530434 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_1;
	// System.Single UnityEngine.Vector3::y
	float ___y_2;
	// System.Single UnityEngine.Vector3::z
	float ___z_3;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector3_t2903530434, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector3_t2903530434, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector3_t2903530434, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}
};

struct Vector3_t2903530434_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t2903530434  ___zeroVector_4;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t2903530434  ___oneVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t2903530434  ___upVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t2903530434  ___downVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t2903530434  ___leftVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t2903530434  ___rightVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t2903530434  ___forwardVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t2903530434  ___backVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t2903530434  ___positiveInfinityVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t2903530434  ___negativeInfinityVector_13;

public:
	inline static int32_t get_offset_of_zeroVector_4() { return static_cast<int32_t>(offsetof(Vector3_t2903530434_StaticFields, ___zeroVector_4)); }
	inline Vector3_t2903530434  get_zeroVector_4() const { return ___zeroVector_4; }
	inline Vector3_t2903530434 * get_address_of_zeroVector_4() { return &___zeroVector_4; }
	inline void set_zeroVector_4(Vector3_t2903530434  value)
	{
		___zeroVector_4 = value;
	}

	inline static int32_t get_offset_of_oneVector_5() { return static_cast<int32_t>(offsetof(Vector3_t2903530434_StaticFields, ___oneVector_5)); }
	inline Vector3_t2903530434  get_oneVector_5() const { return ___oneVector_5; }
	inline Vector3_t2903530434 * get_address_of_oneVector_5() { return &___oneVector_5; }
	inline void set_oneVector_5(Vector3_t2903530434  value)
	{
		___oneVector_5 = value;
	}

	inline static int32_t get_offset_of_upVector_6() { return static_cast<int32_t>(offsetof(Vector3_t2903530434_StaticFields, ___upVector_6)); }
	inline Vector3_t2903530434  get_upVector_6() const { return ___upVector_6; }
	inline Vector3_t2903530434 * get_address_of_upVector_6() { return &___upVector_6; }
	inline void set_upVector_6(Vector3_t2903530434  value)
	{
		___upVector_6 = value;
	}

	inline static int32_t get_offset_of_downVector_7() { return static_cast<int32_t>(offsetof(Vector3_t2903530434_StaticFields, ___downVector_7)); }
	inline Vector3_t2903530434  get_downVector_7() const { return ___downVector_7; }
	inline Vector3_t2903530434 * get_address_of_downVector_7() { return &___downVector_7; }
	inline void set_downVector_7(Vector3_t2903530434  value)
	{
		___downVector_7 = value;
	}

	inline static int32_t get_offset_of_leftVector_8() { return static_cast<int32_t>(offsetof(Vector3_t2903530434_StaticFields, ___leftVector_8)); }
	inline Vector3_t2903530434  get_leftVector_8() const { return ___leftVector_8; }
	inline Vector3_t2903530434 * get_address_of_leftVector_8() { return &___leftVector_8; }
	inline void set_leftVector_8(Vector3_t2903530434  value)
	{
		___leftVector_8 = value;
	}

	inline static int32_t get_offset_of_rightVector_9() { return static_cast<int32_t>(offsetof(Vector3_t2903530434_StaticFields, ___rightVector_9)); }
	inline Vector3_t2903530434  get_rightVector_9() const { return ___rightVector_9; }
	inline Vector3_t2903530434 * get_address_of_rightVector_9() { return &___rightVector_9; }
	inline void set_rightVector_9(Vector3_t2903530434  value)
	{
		___rightVector_9 = value;
	}

	inline static int32_t get_offset_of_forwardVector_10() { return static_cast<int32_t>(offsetof(Vector3_t2903530434_StaticFields, ___forwardVector_10)); }
	inline Vector3_t2903530434  get_forwardVector_10() const { return ___forwardVector_10; }
	inline Vector3_t2903530434 * get_address_of_forwardVector_10() { return &___forwardVector_10; }
	inline void set_forwardVector_10(Vector3_t2903530434  value)
	{
		___forwardVector_10 = value;
	}

	inline static int32_t get_offset_of_backVector_11() { return static_cast<int32_t>(offsetof(Vector3_t2903530434_StaticFields, ___backVector_11)); }
	inline Vector3_t2903530434  get_backVector_11() const { return ___backVector_11; }
	inline Vector3_t2903530434 * get_address_of_backVector_11() { return &___backVector_11; }
	inline void set_backVector_11(Vector3_t2903530434  value)
	{
		___backVector_11 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_12() { return static_cast<int32_t>(offsetof(Vector3_t2903530434_StaticFields, ___positiveInfinityVector_12)); }
	inline Vector3_t2903530434  get_positiveInfinityVector_12() const { return ___positiveInfinityVector_12; }
	inline Vector3_t2903530434 * get_address_of_positiveInfinityVector_12() { return &___positiveInfinityVector_12; }
	inline void set_positiveInfinityVector_12(Vector3_t2903530434  value)
	{
		___positiveInfinityVector_12 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t2903530434_StaticFields, ___negativeInfinityVector_13)); }
	inline Vector3_t2903530434  get_negativeInfinityVector_13() const { return ___negativeInfinityVector_13; }
	inline Vector3_t2903530434 * get_address_of_negativeInfinityVector_13() { return &___negativeInfinityVector_13; }
	inline void set_negativeInfinityVector_13(Vector3_t2903530434  value)
	{
		___negativeInfinityVector_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_T2903530434_H
#ifndef SINGLE_T3045349759_H
#define SINGLE_T3045349759_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Single
struct  Single_t3045349759 
{
public:
	// System.Single System.Single::m_value
	float ___m_value_7;

public:
	inline static int32_t get_offset_of_m_value_7() { return static_cast<int32_t>(offsetof(Single_t3045349759, ___m_value_7)); }
	inline float get_m_value_7() const { return ___m_value_7; }
	inline float* get_address_of_m_value_7() { return &___m_value_7; }
	inline void set_m_value_7(float value)
	{
		___m_value_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLE_T3045349759_H
#ifndef PROPERTYATTRIBUTE_T1939035372_H
#define PROPERTYATTRIBUTE_T1939035372_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PropertyAttribute
struct  PropertyAttribute_t1939035372  : public Attribute_t841672618
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROPERTYATTRIBUTE_T1939035372_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	IntPtr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline IntPtr_t get_Zero_1() const { return ___Zero_1; }
	inline IntPtr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(IntPtr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef RANGEINT_T1885873663_H
#define RANGEINT_T1885873663_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.RangeInt
struct  RangeInt_t1885873663 
{
public:
	// System.Int32 UnityEngine.RangeInt::start
	int32_t ___start_0;
	// System.Int32 UnityEngine.RangeInt::length
	int32_t ___length_1;

public:
	inline static int32_t get_offset_of_start_0() { return static_cast<int32_t>(offsetof(RangeInt_t1885873663, ___start_0)); }
	inline int32_t get_start_0() const { return ___start_0; }
	inline int32_t* get_address_of_start_0() { return &___start_0; }
	inline void set_start_0(int32_t value)
	{
		___start_0 = value;
	}

	inline static int32_t get_offset_of_length_1() { return static_cast<int32_t>(offsetof(RangeInt_t1885873663, ___length_1)); }
	inline int32_t get_length_1() const { return ___length_1; }
	inline int32_t* get_address_of_length_1() { return &___length_1; }
	inline void set_length_1(int32_t value)
	{
		___length_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RANGEINT_T1885873663_H
#ifndef VECTOR2_T3577333262_H
#define VECTOR2_T3577333262_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2
struct  Vector2_t3577333262 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_t3577333262, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_t3577333262, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_t3577333262_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_t3577333262  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_t3577333262  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_t3577333262  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_t3577333262  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_t3577333262  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_t3577333262  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_t3577333262  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_t3577333262  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_t3577333262_StaticFields, ___zeroVector_2)); }
	inline Vector2_t3577333262  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_t3577333262 * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_t3577333262  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_t3577333262_StaticFields, ___oneVector_3)); }
	inline Vector2_t3577333262  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_t3577333262 * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_t3577333262  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_t3577333262_StaticFields, ___upVector_4)); }
	inline Vector2_t3577333262  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_t3577333262 * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_t3577333262  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_t3577333262_StaticFields, ___downVector_5)); }
	inline Vector2_t3577333262  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_t3577333262 * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_t3577333262  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_t3577333262_StaticFields, ___leftVector_6)); }
	inline Vector2_t3577333262  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_t3577333262 * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_t3577333262  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_t3577333262_StaticFields, ___rightVector_7)); }
	inline Vector2_t3577333262  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_t3577333262 * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_t3577333262  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_t3577333262_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_t3577333262  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_t3577333262 * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_t3577333262  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_t3577333262_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_t3577333262  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_t3577333262 * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_t3577333262  value)
	{
		___negativeInfinityVector_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_T3577333262_H
#ifndef PRESERVEATTRIBUTE_T3468091719_H
#define PRESERVEATTRIBUTE_T3468091719_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Scripting.PreserveAttribute
struct  PreserveAttribute_t3468091719  : public Attribute_t841672618
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PRESERVEATTRIBUTE_T3468091719_H
#ifndef RPC_T1601042701_H
#define RPC_T1601042701_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.RPC
struct  RPC_t1601042701  : public Attribute_t841672618
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RPC_T1601042701_H
#ifndef REQUIRECOMPONENT_T2076945239_H
#define REQUIRECOMPONENT_T2076945239_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.RequireComponent
struct  RequireComponent_t2076945239  : public Attribute_t841672618
{
public:
	// System.Type UnityEngine.RequireComponent::m_Type0
	Type_t * ___m_Type0_0;
	// System.Type UnityEngine.RequireComponent::m_Type1
	Type_t * ___m_Type1_1;
	// System.Type UnityEngine.RequireComponent::m_Type2
	Type_t * ___m_Type2_2;

public:
	inline static int32_t get_offset_of_m_Type0_0() { return static_cast<int32_t>(offsetof(RequireComponent_t2076945239, ___m_Type0_0)); }
	inline Type_t * get_m_Type0_0() const { return ___m_Type0_0; }
	inline Type_t ** get_address_of_m_Type0_0() { return &___m_Type0_0; }
	inline void set_m_Type0_0(Type_t * value)
	{
		___m_Type0_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Type0_0), value);
	}

	inline static int32_t get_offset_of_m_Type1_1() { return static_cast<int32_t>(offsetof(RequireComponent_t2076945239, ___m_Type1_1)); }
	inline Type_t * get_m_Type1_1() const { return ___m_Type1_1; }
	inline Type_t ** get_address_of_m_Type1_1() { return &___m_Type1_1; }
	inline void set_m_Type1_1(Type_t * value)
	{
		___m_Type1_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_Type1_1), value);
	}

	inline static int32_t get_offset_of_m_Type2_2() { return static_cast<int32_t>(offsetof(RequireComponent_t2076945239, ___m_Type2_2)); }
	inline Type_t * get_m_Type2_2() const { return ___m_Type2_2; }
	inline Type_t ** get_address_of_m_Type2_2() { return &___m_Type2_2; }
	inline void set_m_Type2_2(Type_t * value)
	{
		___m_Type2_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_Type2_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REQUIRECOMPONENT_T2076945239_H
#ifndef RECT_T3436776195_H
#define RECT_T3436776195_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rect
struct  Rect_t3436776195 
{
public:
	// System.Single UnityEngine.Rect::m_XMin
	float ___m_XMin_0;
	// System.Single UnityEngine.Rect::m_YMin
	float ___m_YMin_1;
	// System.Single UnityEngine.Rect::m_Width
	float ___m_Width_2;
	// System.Single UnityEngine.Rect::m_Height
	float ___m_Height_3;

public:
	inline static int32_t get_offset_of_m_XMin_0() { return static_cast<int32_t>(offsetof(Rect_t3436776195, ___m_XMin_0)); }
	inline float get_m_XMin_0() const { return ___m_XMin_0; }
	inline float* get_address_of_m_XMin_0() { return &___m_XMin_0; }
	inline void set_m_XMin_0(float value)
	{
		___m_XMin_0 = value;
	}

	inline static int32_t get_offset_of_m_YMin_1() { return static_cast<int32_t>(offsetof(Rect_t3436776195, ___m_YMin_1)); }
	inline float get_m_YMin_1() const { return ___m_YMin_1; }
	inline float* get_address_of_m_YMin_1() { return &___m_YMin_1; }
	inline void set_m_YMin_1(float value)
	{
		___m_YMin_1 = value;
	}

	inline static int32_t get_offset_of_m_Width_2() { return static_cast<int32_t>(offsetof(Rect_t3436776195, ___m_Width_2)); }
	inline float get_m_Width_2() const { return ___m_Width_2; }
	inline float* get_address_of_m_Width_2() { return &___m_Width_2; }
	inline void set_m_Width_2(float value)
	{
		___m_Width_2 = value;
	}

	inline static int32_t get_offset_of_m_Height_3() { return static_cast<int32_t>(offsetof(Rect_t3436776195, ___m_Height_3)); }
	inline float get_m_Height_3() const { return ___m_Height_3; }
	inline float* get_address_of_m_Height_3() { return &___m_Height_3; }
	inline void set_m_Height_3(float value)
	{
		___m_Height_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RECT_T3436776195_H
#ifndef SCENE_T348657329_H
#define SCENE_T348657329_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.SceneManagement.Scene
struct  Scene_t348657329 
{
public:
	// System.Int32 UnityEngine.SceneManagement.Scene::m_Handle
	int32_t ___m_Handle_0;

public:
	inline static int32_t get_offset_of_m_Handle_0() { return static_cast<int32_t>(offsetof(Scene_t348657329, ___m_Handle_0)); }
	inline int32_t get_m_Handle_0() const { return ___m_Handle_0; }
	inline int32_t* get_address_of_m_Handle_0() { return &___m_Handle_0; }
	inline void set_m_Handle_0(int32_t value)
	{
		___m_Handle_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCENE_T348657329_H
#ifndef INT32_T4237944589_H
#define INT32_T4237944589_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Int32
struct  Int32_t4237944589 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Int32_t4237944589, ___m_value_2)); }
	inline int32_t get_m_value_2() const { return ___m_value_2; }
	inline int32_t* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(int32_t value)
	{
		___m_value_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT32_T4237944589_H
#ifndef SELECTIONBASEATTRIBUTE_T2246906333_H
#define SELECTIONBASEATTRIBUTE_T2246906333_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.SelectionBaseAttribute
struct  SelectionBaseAttribute_t2246906333  : public Attribute_t841672618
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SELECTIONBASEATTRIBUTE_T2246906333_H
#ifndef BOOLEAN_T2950845069_H
#define BOOLEAN_T2950845069_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Boolean
struct  Boolean_t2950845069 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Boolean_t2950845069, ___m_value_2)); }
	inline bool get_m_value_2() const { return ___m_value_2; }
	inline bool* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(bool value)
	{
		___m_value_2 = value;
	}
};

struct Boolean_t2950845069_StaticFields
{
public:
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_0;
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_1;

public:
	inline static int32_t get_offset_of_FalseString_0() { return static_cast<int32_t>(offsetof(Boolean_t2950845069_StaticFields, ___FalseString_0)); }
	inline String_t* get_FalseString_0() const { return ___FalseString_0; }
	inline String_t** get_address_of_FalseString_0() { return &___FalseString_0; }
	inline void set_FalseString_0(String_t* value)
	{
		___FalseString_0 = value;
		Il2CppCodeGenWriteBarrier((&___FalseString_0), value);
	}

	inline static int32_t get_offset_of_TrueString_1() { return static_cast<int32_t>(offsetof(Boolean_t2950845069_StaticFields, ___TrueString_1)); }
	inline String_t* get_TrueString_1() const { return ___TrueString_1; }
	inline String_t** get_address_of_TrueString_1() { return &___TrueString_1; }
	inline void set_TrueString_1(String_t* value)
	{
		___TrueString_1 = value;
		Il2CppCodeGenWriteBarrier((&___TrueString_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOOLEAN_T2950845069_H
#ifndef SYSTEMEXCEPTION_T870616472_H
#define SYSTEMEXCEPTION_T870616472_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.SystemException
struct  SystemException_t870616472  : public Exception_t1975590229
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SYSTEMEXCEPTION_T870616472_H
#ifndef ENUM_T978401890_H
#define ENUM_T978401890_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t978401890  : public ValueType_t1306072465
{
public:

public:
};

struct Enum_t978401890_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t45629911* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t978401890_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t45629911* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t45629911** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t45629911* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t978401890_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t978401890_marshaled_com
{
};
#endif // ENUM_T978401890_H
#ifndef EMISSIONMODULE_T216550349_H
#define EMISSIONMODULE_T216550349_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ParticleSystem/EmissionModule
struct  EmissionModule_t216550349 
{
public:
	// UnityEngine.ParticleSystem UnityEngine.ParticleSystem/EmissionModule::m_ParticleSystem
	ParticleSystem_t1912383059 * ___m_ParticleSystem_0;

public:
	inline static int32_t get_offset_of_m_ParticleSystem_0() { return static_cast<int32_t>(offsetof(EmissionModule_t216550349, ___m_ParticleSystem_0)); }
	inline ParticleSystem_t1912383059 * get_m_ParticleSystem_0() const { return ___m_ParticleSystem_0; }
	inline ParticleSystem_t1912383059 ** get_address_of_m_ParticleSystem_0() { return &___m_ParticleSystem_0; }
	inline void set_m_ParticleSystem_0(ParticleSystem_t1912383059 * value)
	{
		___m_ParticleSystem_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_ParticleSystem_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.ParticleSystem/EmissionModule
struct EmissionModule_t216550349_marshaled_pinvoke
{
	ParticleSystem_t1912383059 * ___m_ParticleSystem_0;
};
// Native definition for COM marshalling of UnityEngine.ParticleSystem/EmissionModule
struct EmissionModule_t216550349_marshaled_com
{
	ParticleSystem_t1912383059 * ___m_ParticleSystem_0;
};
#endif // EMISSIONMODULE_T216550349_H
#ifndef NETWORKVIEWID_T1404889299_H
#define NETWORKVIEWID_T1404889299_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.NetworkViewID
struct  NetworkViewID_t1404889299 
{
public:
	// System.Int32 UnityEngine.NetworkViewID::a
	int32_t ___a_0;
	// System.Int32 UnityEngine.NetworkViewID::b
	int32_t ___b_1;
	// System.Int32 UnityEngine.NetworkViewID::c
	int32_t ___c_2;

public:
	inline static int32_t get_offset_of_a_0() { return static_cast<int32_t>(offsetof(NetworkViewID_t1404889299, ___a_0)); }
	inline int32_t get_a_0() const { return ___a_0; }
	inline int32_t* get_address_of_a_0() { return &___a_0; }
	inline void set_a_0(int32_t value)
	{
		___a_0 = value;
	}

	inline static int32_t get_offset_of_b_1() { return static_cast<int32_t>(offsetof(NetworkViewID_t1404889299, ___b_1)); }
	inline int32_t get_b_1() const { return ___b_1; }
	inline int32_t* get_address_of_b_1() { return &___b_1; }
	inline void set_b_1(int32_t value)
	{
		___b_1 = value;
	}

	inline static int32_t get_offset_of_c_2() { return static_cast<int32_t>(offsetof(NetworkViewID_t1404889299, ___c_2)); }
	inline int32_t get_c_2() const { return ___c_2; }
	inline int32_t* get_address_of_c_2() { return &___c_2; }
	inline void set_c_2(int32_t value)
	{
		___c_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NETWORKVIEWID_T1404889299_H
#ifndef VOID_T2217553113_H
#define VOID_T2217553113_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t2217553113 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T2217553113_H
#ifndef RENDERBUFFERHELPER_T1663815297_H
#define RENDERBUFFERHELPER_T1663815297_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.RenderBufferHelper
struct  RenderBufferHelper_t1663815297 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RENDERBUFFERHELPER_T1663815297_H
#ifndef MAINMODULE_T2858435356_H
#define MAINMODULE_T2858435356_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ParticleSystem/MainModule
struct  MainModule_t2858435356 
{
public:
	// UnityEngine.ParticleSystem UnityEngine.ParticleSystem/MainModule::m_ParticleSystem
	ParticleSystem_t1912383059 * ___m_ParticleSystem_0;

public:
	inline static int32_t get_offset_of_m_ParticleSystem_0() { return static_cast<int32_t>(offsetof(MainModule_t2858435356, ___m_ParticleSystem_0)); }
	inline ParticleSystem_t1912383059 * get_m_ParticleSystem_0() const { return ___m_ParticleSystem_0; }
	inline ParticleSystem_t1912383059 ** get_address_of_m_ParticleSystem_0() { return &___m_ParticleSystem_0; }
	inline void set_m_ParticleSystem_0(ParticleSystem_t1912383059 * value)
	{
		___m_ParticleSystem_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_ParticleSystem_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.ParticleSystem/MainModule
struct MainModule_t2858435356_marshaled_pinvoke
{
	ParticleSystem_t1912383059 * ___m_ParticleSystem_0;
};
// Native definition for COM marshalling of UnityEngine.ParticleSystem/MainModule
struct MainModule_t2858435356_marshaled_com
{
	ParticleSystem_t1912383059 * ___m_ParticleSystem_0;
};
#endif // MAINMODULE_T2858435356_H
#ifndef SHAREDBETWEENANIMATORSATTRIBUTE_T1887607685_H
#define SHAREDBETWEENANIMATORSATTRIBUTE_T1887607685_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.SharedBetweenAnimatorsAttribute
struct  SharedBetweenAnimatorsAttribute_t1887607685  : public Attribute_t841672618
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SHAREDBETWEENANIMATORSATTRIBUTE_T1887607685_H
#ifndef DOUBLE_T1313887800_H
#define DOUBLE_T1313887800_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Double
struct  Double_t1313887800 
{
public:
	// System.Double System.Double::m_value
	double ___m_value_13;

public:
	inline static int32_t get_offset_of_m_value_13() { return static_cast<int32_t>(offsetof(Double_t1313887800, ___m_value_13)); }
	inline double get_m_value_13() const { return ___m_value_13; }
	inline double* get_address_of_m_value_13() { return &___m_value_13; }
	inline void set_m_value_13(double value)
	{
		___m_value_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DOUBLE_T1313887800_H
#ifndef PARAMETERMODIFIER_T2577558211_H
#define PARAMETERMODIFIER_T2577558211_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.ParameterModifier
struct  ParameterModifier_t2577558211 
{
public:
	// System.Boolean[] System.Reflection.ParameterModifier::_byref
	BooleanU5BU5D_t3096172704* ____byref_0;

public:
	inline static int32_t get_offset_of__byref_0() { return static_cast<int32_t>(offsetof(ParameterModifier_t2577558211, ____byref_0)); }
	inline BooleanU5BU5D_t3096172704* get__byref_0() const { return ____byref_0; }
	inline BooleanU5BU5D_t3096172704** get_address_of__byref_0() { return &____byref_0; }
	inline void set__byref_0(BooleanU5BU5D_t3096172704* value)
	{
		____byref_0 = value;
		Il2CppCodeGenWriteBarrier((&____byref_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Reflection.ParameterModifier
struct ParameterModifier_t2577558211_marshaled_pinvoke
{
	int32_t* ____byref_0;
};
// Native definition for COM marshalling of System.Reflection.ParameterModifier
struct ParameterModifier_t2577558211_marshaled_com
{
	int32_t* ____byref_0;
};
#endif // PARAMETERMODIFIER_T2577558211_H
#ifndef QUATERNION_T754065749_H
#define QUATERNION_T754065749_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Quaternion
struct  Quaternion_t754065749 
{
public:
	// System.Single UnityEngine.Quaternion::x
	float ___x_0;
	// System.Single UnityEngine.Quaternion::y
	float ___y_1;
	// System.Single UnityEngine.Quaternion::z
	float ___z_2;
	// System.Single UnityEngine.Quaternion::w
	float ___w_3;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Quaternion_t754065749, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Quaternion_t754065749, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(Quaternion_t754065749, ___z_2)); }
	inline float get_z_2() const { return ___z_2; }
	inline float* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(float value)
	{
		___z_2 = value;
	}

	inline static int32_t get_offset_of_w_3() { return static_cast<int32_t>(offsetof(Quaternion_t754065749, ___w_3)); }
	inline float get_w_3() const { return ___w_3; }
	inline float* get_address_of_w_3() { return &___w_3; }
	inline void set_w_3(float value)
	{
		___w_3 = value;
	}
};

struct Quaternion_t754065749_StaticFields
{
public:
	// UnityEngine.Quaternion UnityEngine.Quaternion::identityQuaternion
	Quaternion_t754065749  ___identityQuaternion_4;

public:
	inline static int32_t get_offset_of_identityQuaternion_4() { return static_cast<int32_t>(offsetof(Quaternion_t754065749_StaticFields, ___identityQuaternion_4)); }
	inline Quaternion_t754065749  get_identityQuaternion_4() const { return ___identityQuaternion_4; }
	inline Quaternion_t754065749 * get_address_of_identityQuaternion_4() { return &___identityQuaternion_4; }
	inline void set_identityQuaternion_4(Quaternion_t754065749  value)
	{
		___identityQuaternion_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUATERNION_T754065749_H
#ifndef NETWORKPLAYER_T4008288529_H
#define NETWORKPLAYER_T4008288529_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.NetworkPlayer
struct  NetworkPlayer_t4008288529 
{
public:
	// System.Int32 UnityEngine.NetworkPlayer::index
	int32_t ___index_0;

public:
	inline static int32_t get_offset_of_index_0() { return static_cast<int32_t>(offsetof(NetworkPlayer_t4008288529, ___index_0)); }
	inline int32_t get_index_0() const { return ___index_0; }
	inline int32_t* get_address_of_index_0() { return &___index_0; }
	inline void set_index_0(int32_t value)
	{
		___index_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NETWORKPLAYER_T4008288529_H
#ifndef SERIALIZEPRIVATEVARIABLES_T409785951_H
#define SERIALIZEPRIVATEVARIABLES_T409785951_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.SerializePrivateVariables
struct  SerializePrivateVariables_t409785951  : public Attribute_t841672618
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERIALIZEPRIVATEVARIABLES_T409785951_H
#ifndef SERIALIZEFIELD_T1027808064_H
#define SERIALIZEFIELD_T1027808064_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.SerializeField
struct  SerializeField_t1027808064  : public Attribute_t841672618
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERIALIZEFIELD_T1027808064_H
#ifndef FORMERLYSERIALIZEDASATTRIBUTE_T1937332047_H
#define FORMERLYSERIALIZEDASATTRIBUTE_T1937332047_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Serialization.FormerlySerializedAsAttribute
struct  FormerlySerializedAsAttribute_t1937332047  : public Attribute_t841672618
{
public:
	// System.String UnityEngine.Serialization.FormerlySerializedAsAttribute::m_oldName
	String_t* ___m_oldName_0;

public:
	inline static int32_t get_offset_of_m_oldName_0() { return static_cast<int32_t>(offsetof(FormerlySerializedAsAttribute_t1937332047, ___m_oldName_0)); }
	inline String_t* get_m_oldName_0() const { return ___m_oldName_0; }
	inline String_t** get_address_of_m_oldName_0() { return &___m_oldName_0; }
	inline void set_m_oldName_0(String_t* value)
	{
		___m_oldName_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_oldName_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FORMERLYSERIALIZEDASATTRIBUTE_T1937332047_H
#ifndef HITINFO_T2000020299_H
#define HITINFO_T2000020299_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.SendMouseEvents/HitInfo
struct  HitInfo_t2000020299 
{
public:
	// UnityEngine.GameObject UnityEngine.SendMouseEvents/HitInfo::target
	GameObject_t1811656094 * ___target_0;
	// UnityEngine.Camera UnityEngine.SendMouseEvents/HitInfo::camera
	Camera_t3175186167 * ___camera_1;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(HitInfo_t2000020299, ___target_0)); }
	inline GameObject_t1811656094 * get_target_0() const { return ___target_0; }
	inline GameObject_t1811656094 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(GameObject_t1811656094 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}

	inline static int32_t get_offset_of_camera_1() { return static_cast<int32_t>(offsetof(HitInfo_t2000020299, ___camera_1)); }
	inline Camera_t3175186167 * get_camera_1() const { return ___camera_1; }
	inline Camera_t3175186167 ** get_address_of_camera_1() { return &___camera_1; }
	inline void set_camera_1(Camera_t3175186167 * value)
	{
		___camera_1 = value;
		Il2CppCodeGenWriteBarrier((&___camera_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.SendMouseEvents/HitInfo
struct HitInfo_t2000020299_marshaled_pinvoke
{
	GameObject_t1811656094 * ___target_0;
	Camera_t3175186167 * ___camera_1;
};
// Native definition for COM marshalling of UnityEngine.SendMouseEvents/HitInfo
struct HitInfo_t2000020299_marshaled_com
{
	GameObject_t1811656094 * ___target_0;
	Camera_t3175186167 * ___camera_1;
};
#endif // HITINFO_T2000020299_H
#ifndef RENDERTEXTUREFORMAT_T956599168_H
#define RENDERTEXTUREFORMAT_T956599168_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.RenderTextureFormat
struct  RenderTextureFormat_t956599168 
{
public:
	// System.Int32 UnityEngine.RenderTextureFormat::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(RenderTextureFormat_t956599168, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RENDERTEXTUREFORMAT_T956599168_H
#ifndef RENDERTEXTUREMEMORYLESS_T3175398901_H
#define RENDERTEXTUREMEMORYLESS_T3175398901_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.RenderTextureMemoryless
struct  RenderTextureMemoryless_t3175398901 
{
public:
	// System.Int32 UnityEngine.RenderTextureMemoryless::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(RenderTextureMemoryless_t3175398901, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RENDERTEXTUREMEMORYLESS_T3175398901_H
#ifndef SHADOWSAMPLINGMODE_T4271830836_H
#define SHADOWSAMPLINGMODE_T4271830836_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.ShadowSamplingMode
struct  ShadowSamplingMode_t4271830836 
{
public:
	// System.Int32 UnityEngine.Rendering.ShadowSamplingMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ShadowSamplingMode_t4271830836, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SHADOWSAMPLINGMODE_T4271830836_H
#ifndef VRTEXTUREUSAGE_T3813539688_H
#define VRTEXTUREUSAGE_T3813539688_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.VRTextureUsage
struct  VRTextureUsage_t3813539688 
{
public:
	// System.Int32 UnityEngine.VRTextureUsage::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(VRTextureUsage_t3813539688, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VRTEXTUREUSAGE_T3813539688_H
#ifndef RENDERTEXTURECREATIONFLAGS_T1463454578_H
#define RENDERTEXTURECREATIONFLAGS_T1463454578_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.RenderTextureCreationFlags
struct  RenderTextureCreationFlags_t1463454578 
{
public:
	// System.Int32 UnityEngine.RenderTextureCreationFlags::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(RenderTextureCreationFlags_t1463454578, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RENDERTEXTURECREATIONFLAGS_T1463454578_H
#ifndef RENDERMODE_T1025336267_H
#define RENDERMODE_T1025336267_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.RenderMode
struct  RenderMode_t1025336267 
{
public:
	// System.Int32 UnityEngine.RenderMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(RenderMode_t1025336267, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RENDERMODE_T1025336267_H
#ifndef ASYNCOPERATION_T2744032732_H
#define ASYNCOPERATION_T2744032732_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.AsyncOperation
struct  AsyncOperation_t2744032732  : public YieldInstruction_t1893592390
{
public:
	// System.IntPtr UnityEngine.AsyncOperation::m_Ptr
	IntPtr_t ___m_Ptr_0;

public:
	inline static int32_t get_offset_of_m_Ptr_0() { return static_cast<int32_t>(offsetof(AsyncOperation_t2744032732, ___m_Ptr_0)); }
	inline IntPtr_t get_m_Ptr_0() const { return ___m_Ptr_0; }
	inline IntPtr_t* get_address_of_m_Ptr_0() { return &___m_Ptr_0; }
	inline void set_m_Ptr_0(IntPtr_t value)
	{
		___m_Ptr_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.AsyncOperation
struct AsyncOperation_t2744032732_marshaled_pinvoke : public YieldInstruction_t1893592390_marshaled_pinvoke
{
	intptr_t ___m_Ptr_0;
};
// Native definition for COM marshalling of UnityEngine.AsyncOperation
struct AsyncOperation_t2744032732_marshaled_com : public YieldInstruction_t1893592390_marshaled_com
{
	intptr_t ___m_Ptr_0;
};
#endif // ASYNCOPERATION_T2744032732_H
#ifndef RENDERTEXTUREREADWRITE_T1480882696_H
#define RENDERTEXTUREREADWRITE_T1480882696_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.RenderTextureReadWrite
struct  RenderTextureReadWrite_t1480882696 
{
public:
	// System.Int32 UnityEngine.RenderTextureReadWrite::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(RenderTextureReadWrite_t1480882696, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RENDERTEXTUREREADWRITE_T1480882696_H
#ifndef TEXTUREDIMENSION_T4022415301_H
#define TEXTUREDIMENSION_T4022415301_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.TextureDimension
struct  TextureDimension_t4022415301 
{
public:
	// System.Int32 UnityEngine.Rendering.TextureDimension::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TextureDimension_t4022415301, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTUREDIMENSION_T4022415301_H
#ifndef PARTICLESYSTEMCURVEMODE_T63798414_H
#define PARTICLESYSTEMCURVEMODE_T63798414_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ParticleSystemCurveMode
struct  ParticleSystemCurveMode_t63798414 
{
public:
	// System.Int32 UnityEngine.ParticleSystemCurveMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ParticleSystemCurveMode_t63798414, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PARTICLESYSTEMCURVEMODE_T63798414_H
#ifndef RIGIDBODYCONSTRAINTS_T3629201422_H
#define RIGIDBODYCONSTRAINTS_T3629201422_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.RigidbodyConstraints
struct  RigidbodyConstraints_t3629201422 
{
public:
	// System.Int32 UnityEngine.RigidbodyConstraints::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(RigidbodyConstraints_t3629201422, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RIGIDBODYCONSTRAINTS_T3629201422_H
#ifndef FORCEMODE_T771962840_H
#define FORCEMODE_T771962840_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ForceMode
struct  ForceMode_t771962840 
{
public:
	// System.Int32 UnityEngine.ForceMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ForceMode_t771962840, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FORCEMODE_T771962840_H
#ifndef RUNTIMEINITIALIZELOADTYPE_T1795860744_H
#define RUNTIMEINITIALIZELOADTYPE_T1795860744_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.RuntimeInitializeLoadType
struct  RuntimeInitializeLoadType_t1795860744 
{
public:
	// System.Int32 UnityEngine.RuntimeInitializeLoadType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(RuntimeInitializeLoadType_t1795860744, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEINITIALIZELOADTYPE_T1795860744_H
#ifndef RUNTIMEPLATFORM_T978892161_H
#define RUNTIMEPLATFORM_T978892161_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.RuntimePlatform
struct  RuntimePlatform_t978892161 
{
public:
	// System.Int32 UnityEngine.RuntimePlatform::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(RuntimePlatform_t978892161, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEPLATFORM_T978892161_H
#ifndef LOADSCENEMODE_T2291092563_H
#define LOADSCENEMODE_T2291092563_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.SceneManagement.LoadSceneMode
struct  LoadSceneMode_t2291092563 
{
public:
	// System.Int32 UnityEngine.SceneManagement.LoadSceneMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(LoadSceneMode_t2291092563, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOADSCENEMODE_T2291092563_H
#ifndef COMPAREFUNCTION_T184930794_H
#define COMPAREFUNCTION_T184930794_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.CompareFunction
struct  CompareFunction_t184930794 
{
public:
	// System.Int32 UnityEngine.Rendering.CompareFunction::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(CompareFunction_t184930794, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPAREFUNCTION_T184930794_H
#ifndef SCREENORIENTATION_T2818551509_H
#define SCREENORIENTATION_T2818551509_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ScreenOrientation
struct  ScreenOrientation_t2818551509 
{
public:
	// System.Int32 UnityEngine.ScreenOrientation::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ScreenOrientation_t2818551509, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCREENORIENTATION_T2818551509_H
#ifndef OBJECT_T1502412432_H
#define OBJECT_T1502412432_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t1502412432  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	IntPtr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t1502412432, ___m_CachedPtr_0)); }
	inline IntPtr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline IntPtr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(IntPtr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t1502412432_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t1502412432_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t1502412432_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t1502412432_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T1502412432_H
#ifndef SENDMESSAGEOPTIONS_T2885049786_H
#define SENDMESSAGEOPTIONS_T2885049786_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.SendMessageOptions
struct  SendMessageOptions_t2885049786 
{
public:
	// System.Int32 UnityEngine.SendMessageOptions::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(SendMessageOptions_t2885049786, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SENDMESSAGEOPTIONS_T2885049786_H
#ifndef CAMERACLEARFLAGS_T750872274_H
#define CAMERACLEARFLAGS_T750872274_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.CameraClearFlags
struct  CameraClearFlags_t750872274 
{
public:
	// System.Int32 UnityEngine.CameraClearFlags::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(CameraClearFlags_t750872274, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAMERACLEARFLAGS_T750872274_H
#ifndef BINDINGFLAGS_T1805189573_H
#define BINDINGFLAGS_T1805189573_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.BindingFlags
struct  BindingFlags_t1805189573 
{
public:
	// System.Int32 System.Reflection.BindingFlags::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(BindingFlags_t1805189573, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BINDINGFLAGS_T1805189573_H
#ifndef SKELETONBONE_T3530196780_H
#define SKELETONBONE_T3530196780_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.SkeletonBone
struct  SkeletonBone_t3530196780 
{
public:
	// System.String UnityEngine.SkeletonBone::name
	String_t* ___name_0;
	// System.String UnityEngine.SkeletonBone::parentName
	String_t* ___parentName_1;
	// UnityEngine.Vector3 UnityEngine.SkeletonBone::position
	Vector3_t2903530434  ___position_2;
	// UnityEngine.Quaternion UnityEngine.SkeletonBone::rotation
	Quaternion_t754065749  ___rotation_3;
	// UnityEngine.Vector3 UnityEngine.SkeletonBone::scale
	Vector3_t2903530434  ___scale_4;

public:
	inline static int32_t get_offset_of_name_0() { return static_cast<int32_t>(offsetof(SkeletonBone_t3530196780, ___name_0)); }
	inline String_t* get_name_0() const { return ___name_0; }
	inline String_t** get_address_of_name_0() { return &___name_0; }
	inline void set_name_0(String_t* value)
	{
		___name_0 = value;
		Il2CppCodeGenWriteBarrier((&___name_0), value);
	}

	inline static int32_t get_offset_of_parentName_1() { return static_cast<int32_t>(offsetof(SkeletonBone_t3530196780, ___parentName_1)); }
	inline String_t* get_parentName_1() const { return ___parentName_1; }
	inline String_t** get_address_of_parentName_1() { return &___parentName_1; }
	inline void set_parentName_1(String_t* value)
	{
		___parentName_1 = value;
		Il2CppCodeGenWriteBarrier((&___parentName_1), value);
	}

	inline static int32_t get_offset_of_position_2() { return static_cast<int32_t>(offsetof(SkeletonBone_t3530196780, ___position_2)); }
	inline Vector3_t2903530434  get_position_2() const { return ___position_2; }
	inline Vector3_t2903530434 * get_address_of_position_2() { return &___position_2; }
	inline void set_position_2(Vector3_t2903530434  value)
	{
		___position_2 = value;
	}

	inline static int32_t get_offset_of_rotation_3() { return static_cast<int32_t>(offsetof(SkeletonBone_t3530196780, ___rotation_3)); }
	inline Quaternion_t754065749  get_rotation_3() const { return ___rotation_3; }
	inline Quaternion_t754065749 * get_address_of_rotation_3() { return &___rotation_3; }
	inline void set_rotation_3(Quaternion_t754065749  value)
	{
		___rotation_3 = value;
	}

	inline static int32_t get_offset_of_scale_4() { return static_cast<int32_t>(offsetof(SkeletonBone_t3530196780, ___scale_4)); }
	inline Vector3_t2903530434  get_scale_4() const { return ___scale_4; }
	inline Vector3_t2903530434 * get_address_of_scale_4() { return &___scale_4; }
	inline void set_scale_4(Vector3_t2903530434  value)
	{
		___scale_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.SkeletonBone
struct SkeletonBone_t3530196780_marshaled_pinvoke
{
	char* ___name_0;
	char* ___parentName_1;
	Vector3_t2903530434  ___position_2;
	Quaternion_t754065749  ___rotation_3;
	Vector3_t2903530434  ___scale_4;
};
// Native definition for COM marshalling of UnityEngine.SkeletonBone
struct SkeletonBone_t3530196780_marshaled_com
{
	Il2CppChar* ___name_0;
	Il2CppChar* ___parentName_1;
	Vector3_t2903530434  ___position_2;
	Quaternion_t754065749  ___rotation_3;
	Vector3_t2903530434  ___scale_4;
};
#endif // SKELETONBONE_T3530196780_H
#ifndef RUNTIMETYPEHANDLE_T845633806_H
#define RUNTIMETYPEHANDLE_T845633806_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.RuntimeTypeHandle
struct  RuntimeTypeHandle_t845633806 
{
public:
	// System.IntPtr System.RuntimeTypeHandle::value
	IntPtr_t ___value_0;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(RuntimeTypeHandle_t845633806, ___value_0)); }
	inline IntPtr_t get_value_0() const { return ___value_0; }
	inline IntPtr_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(IntPtr_t value)
	{
		___value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMETYPEHANDLE_T845633806_H
#ifndef COLORWRITEMASK_T3809791996_H
#define COLORWRITEMASK_T3809791996_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.ColorWriteMask
struct  ColorWriteMask_t3809791996 
{
public:
	// System.Int32 UnityEngine.Rendering.ColorWriteMask::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ColorWriteMask_t3809791996, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLORWRITEMASK_T3809791996_H
#ifndef STENCILOP_T1575186622_H
#define STENCILOP_T1575186622_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.StencilOp
struct  StencilOp_t1575186622 
{
public:
	// System.Int32 UnityEngine.Rendering.StencilOp::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(StencilOp_t1575186622, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STENCILOP_T1575186622_H
#ifndef PLAYABLEOUTPUTHANDLE_T2200222555_H
#define PLAYABLEOUTPUTHANDLE_T2200222555_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Playables.PlayableOutputHandle
struct  PlayableOutputHandle_t2200222555 
{
public:
	// System.IntPtr UnityEngine.Playables.PlayableOutputHandle::m_Handle
	IntPtr_t ___m_Handle_0;
	// System.Int32 UnityEngine.Playables.PlayableOutputHandle::m_Version
	int32_t ___m_Version_1;

public:
	inline static int32_t get_offset_of_m_Handle_0() { return static_cast<int32_t>(offsetof(PlayableOutputHandle_t2200222555, ___m_Handle_0)); }
	inline IntPtr_t get_m_Handle_0() const { return ___m_Handle_0; }
	inline IntPtr_t* get_address_of_m_Handle_0() { return &___m_Handle_0; }
	inline void set_m_Handle_0(IntPtr_t value)
	{
		___m_Handle_0 = value;
	}

	inline static int32_t get_offset_of_m_Version_1() { return static_cast<int32_t>(offsetof(PlayableOutputHandle_t2200222555, ___m_Version_1)); }
	inline int32_t get_m_Version_1() const { return ___m_Version_1; }
	inline int32_t* get_address_of_m_Version_1() { return &___m_Version_1; }
	inline void set_m_Version_1(int32_t value)
	{
		___m_Version_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYABLEOUTPUTHANDLE_T2200222555_H
#ifndef FLAGS_T1291424464_H
#define FLAGS_T1291424464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Playables.FrameData/Flags
struct  Flags_t1291424464 
{
public:
	// System.Int32 UnityEngine.Playables.FrameData/Flags::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Flags_t1291424464, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FLAGS_T1291424464_H
#ifndef INVALIDCASTEXCEPTION_T1996460962_H
#define INVALIDCASTEXCEPTION_T1996460962_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.InvalidCastException
struct  InvalidCastException_t1996460962  : public SystemException_t870616472
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INVALIDCASTEXCEPTION_T1996460962_H
#ifndef RAYCASTHIT2D_T1105181991_H
#define RAYCASTHIT2D_T1105181991_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.RaycastHit2D
struct  RaycastHit2D_t1105181991 
{
public:
	// UnityEngine.Vector2 UnityEngine.RaycastHit2D::m_Centroid
	Vector2_t3577333262  ___m_Centroid_0;
	// UnityEngine.Vector2 UnityEngine.RaycastHit2D::m_Point
	Vector2_t3577333262  ___m_Point_1;
	// UnityEngine.Vector2 UnityEngine.RaycastHit2D::m_Normal
	Vector2_t3577333262  ___m_Normal_2;
	// System.Single UnityEngine.RaycastHit2D::m_Distance
	float ___m_Distance_3;
	// System.Single UnityEngine.RaycastHit2D::m_Fraction
	float ___m_Fraction_4;
	// UnityEngine.Collider2D UnityEngine.RaycastHit2D::m_Collider
	Collider2D_t1623106781 * ___m_Collider_5;

public:
	inline static int32_t get_offset_of_m_Centroid_0() { return static_cast<int32_t>(offsetof(RaycastHit2D_t1105181991, ___m_Centroid_0)); }
	inline Vector2_t3577333262  get_m_Centroid_0() const { return ___m_Centroid_0; }
	inline Vector2_t3577333262 * get_address_of_m_Centroid_0() { return &___m_Centroid_0; }
	inline void set_m_Centroid_0(Vector2_t3577333262  value)
	{
		___m_Centroid_0 = value;
	}

	inline static int32_t get_offset_of_m_Point_1() { return static_cast<int32_t>(offsetof(RaycastHit2D_t1105181991, ___m_Point_1)); }
	inline Vector2_t3577333262  get_m_Point_1() const { return ___m_Point_1; }
	inline Vector2_t3577333262 * get_address_of_m_Point_1() { return &___m_Point_1; }
	inline void set_m_Point_1(Vector2_t3577333262  value)
	{
		___m_Point_1 = value;
	}

	inline static int32_t get_offset_of_m_Normal_2() { return static_cast<int32_t>(offsetof(RaycastHit2D_t1105181991, ___m_Normal_2)); }
	inline Vector2_t3577333262  get_m_Normal_2() const { return ___m_Normal_2; }
	inline Vector2_t3577333262 * get_address_of_m_Normal_2() { return &___m_Normal_2; }
	inline void set_m_Normal_2(Vector2_t3577333262  value)
	{
		___m_Normal_2 = value;
	}

	inline static int32_t get_offset_of_m_Distance_3() { return static_cast<int32_t>(offsetof(RaycastHit2D_t1105181991, ___m_Distance_3)); }
	inline float get_m_Distance_3() const { return ___m_Distance_3; }
	inline float* get_address_of_m_Distance_3() { return &___m_Distance_3; }
	inline void set_m_Distance_3(float value)
	{
		___m_Distance_3 = value;
	}

	inline static int32_t get_offset_of_m_Fraction_4() { return static_cast<int32_t>(offsetof(RaycastHit2D_t1105181991, ___m_Fraction_4)); }
	inline float get_m_Fraction_4() const { return ___m_Fraction_4; }
	inline float* get_address_of_m_Fraction_4() { return &___m_Fraction_4; }
	inline void set_m_Fraction_4(float value)
	{
		___m_Fraction_4 = value;
	}

	inline static int32_t get_offset_of_m_Collider_5() { return static_cast<int32_t>(offsetof(RaycastHit2D_t1105181991, ___m_Collider_5)); }
	inline Collider2D_t1623106781 * get_m_Collider_5() const { return ___m_Collider_5; }
	inline Collider2D_t1623106781 ** get_address_of_m_Collider_5() { return &___m_Collider_5; }
	inline void set_m_Collider_5(Collider2D_t1623106781 * value)
	{
		___m_Collider_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_Collider_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.RaycastHit2D
struct RaycastHit2D_t1105181991_marshaled_pinvoke
{
	Vector2_t3577333262  ___m_Centroid_0;
	Vector2_t3577333262  ___m_Point_1;
	Vector2_t3577333262  ___m_Normal_2;
	float ___m_Distance_3;
	float ___m_Fraction_4;
	Collider2D_t1623106781 * ___m_Collider_5;
};
// Native definition for COM marshalling of UnityEngine.RaycastHit2D
struct RaycastHit2D_t1105181991_marshaled_com
{
	Vector2_t3577333262  ___m_Centroid_0;
	Vector2_t3577333262  ___m_Point_1;
	Vector2_t3577333262  ___m_Normal_2;
	float ___m_Distance_3;
	float ___m_Fraction_4;
	Collider2D_t1623106781 * ___m_Collider_5;
};
#endif // RAYCASTHIT2D_T1105181991_H
#ifndef RANGEATTRIBUTE_T3376577711_H
#define RANGEATTRIBUTE_T3376577711_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.RangeAttribute
struct  RangeAttribute_t3376577711  : public PropertyAttribute_t1939035372
{
public:
	// System.Single UnityEngine.RangeAttribute::min
	float ___min_0;
	// System.Single UnityEngine.RangeAttribute::max
	float ___max_1;

public:
	inline static int32_t get_offset_of_min_0() { return static_cast<int32_t>(offsetof(RangeAttribute_t3376577711, ___min_0)); }
	inline float get_min_0() const { return ___min_0; }
	inline float* get_address_of_min_0() { return &___min_0; }
	inline void set_min_0(float value)
	{
		___min_0 = value;
	}

	inline static int32_t get_offset_of_max_1() { return static_cast<int32_t>(offsetof(RangeAttribute_t3376577711, ___max_1)); }
	inline float get_max_1() const { return ___max_1; }
	inline float* get_address_of_max_1() { return &___max_1; }
	inline void set_max_1(float value)
	{
		___max_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RANGEATTRIBUTE_T3376577711_H
#ifndef RAY_T3821377119_H
#define RAY_T3821377119_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Ray
struct  Ray_t3821377119 
{
public:
	// UnityEngine.Vector3 UnityEngine.Ray::m_Origin
	Vector3_t2903530434  ___m_Origin_0;
	// UnityEngine.Vector3 UnityEngine.Ray::m_Direction
	Vector3_t2903530434  ___m_Direction_1;

public:
	inline static int32_t get_offset_of_m_Origin_0() { return static_cast<int32_t>(offsetof(Ray_t3821377119, ___m_Origin_0)); }
	inline Vector3_t2903530434  get_m_Origin_0() const { return ___m_Origin_0; }
	inline Vector3_t2903530434 * get_address_of_m_Origin_0() { return &___m_Origin_0; }
	inline void set_m_Origin_0(Vector3_t2903530434  value)
	{
		___m_Origin_0 = value;
	}

	inline static int32_t get_offset_of_m_Direction_1() { return static_cast<int32_t>(offsetof(Ray_t3821377119, ___m_Direction_1)); }
	inline Vector3_t2903530434  get_m_Direction_1() const { return ___m_Direction_1; }
	inline Vector3_t2903530434 * get_address_of_m_Direction_1() { return &___m_Direction_1; }
	inline void set_m_Direction_1(Vector3_t2903530434  value)
	{
		___m_Direction_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RAY_T3821377119_H
#ifndef RECTOFFSET_T1315541452_H
#define RECTOFFSET_T1315541452_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.RectOffset
struct  RectOffset_t1315541452  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.RectOffset::m_Ptr
	IntPtr_t ___m_Ptr_0;
	// System.Object UnityEngine.RectOffset::m_SourceStyle
	RuntimeObject * ___m_SourceStyle_1;

public:
	inline static int32_t get_offset_of_m_Ptr_0() { return static_cast<int32_t>(offsetof(RectOffset_t1315541452, ___m_Ptr_0)); }
	inline IntPtr_t get_m_Ptr_0() const { return ___m_Ptr_0; }
	inline IntPtr_t* get_address_of_m_Ptr_0() { return &___m_Ptr_0; }
	inline void set_m_Ptr_0(IntPtr_t value)
	{
		___m_Ptr_0 = value;
	}

	inline static int32_t get_offset_of_m_SourceStyle_1() { return static_cast<int32_t>(offsetof(RectOffset_t1315541452, ___m_SourceStyle_1)); }
	inline RuntimeObject * get_m_SourceStyle_1() const { return ___m_SourceStyle_1; }
	inline RuntimeObject ** get_address_of_m_SourceStyle_1() { return &___m_SourceStyle_1; }
	inline void set_m_SourceStyle_1(RuntimeObject * value)
	{
		___m_SourceStyle_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_SourceStyle_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.RectOffset
struct RectOffset_t1315541452_marshaled_pinvoke
{
	intptr_t ___m_Ptr_0;
	Il2CppIUnknown* ___m_SourceStyle_1;
};
// Native definition for COM marshalling of UnityEngine.RectOffset
struct RectOffset_t1315541452_marshaled_com
{
	intptr_t ___m_Ptr_0;
	Il2CppIUnknown* ___m_SourceStyle_1;
};
#endif // RECTOFFSET_T1315541452_H
#ifndef RAYCASTHIT_T2327766465_H
#define RAYCASTHIT_T2327766465_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.RaycastHit
struct  RaycastHit_t2327766465 
{
public:
	// UnityEngine.Vector3 UnityEngine.RaycastHit::m_Point
	Vector3_t2903530434  ___m_Point_0;
	// UnityEngine.Vector3 UnityEngine.RaycastHit::m_Normal
	Vector3_t2903530434  ___m_Normal_1;
	// System.Int32 UnityEngine.RaycastHit::m_FaceID
	int32_t ___m_FaceID_2;
	// System.Single UnityEngine.RaycastHit::m_Distance
	float ___m_Distance_3;
	// UnityEngine.Vector2 UnityEngine.RaycastHit::m_UV
	Vector2_t3577333262  ___m_UV_4;
	// UnityEngine.Collider UnityEngine.RaycastHit::m_Collider
	Collider_t2301021182 * ___m_Collider_5;

public:
	inline static int32_t get_offset_of_m_Point_0() { return static_cast<int32_t>(offsetof(RaycastHit_t2327766465, ___m_Point_0)); }
	inline Vector3_t2903530434  get_m_Point_0() const { return ___m_Point_0; }
	inline Vector3_t2903530434 * get_address_of_m_Point_0() { return &___m_Point_0; }
	inline void set_m_Point_0(Vector3_t2903530434  value)
	{
		___m_Point_0 = value;
	}

	inline static int32_t get_offset_of_m_Normal_1() { return static_cast<int32_t>(offsetof(RaycastHit_t2327766465, ___m_Normal_1)); }
	inline Vector3_t2903530434  get_m_Normal_1() const { return ___m_Normal_1; }
	inline Vector3_t2903530434 * get_address_of_m_Normal_1() { return &___m_Normal_1; }
	inline void set_m_Normal_1(Vector3_t2903530434  value)
	{
		___m_Normal_1 = value;
	}

	inline static int32_t get_offset_of_m_FaceID_2() { return static_cast<int32_t>(offsetof(RaycastHit_t2327766465, ___m_FaceID_2)); }
	inline int32_t get_m_FaceID_2() const { return ___m_FaceID_2; }
	inline int32_t* get_address_of_m_FaceID_2() { return &___m_FaceID_2; }
	inline void set_m_FaceID_2(int32_t value)
	{
		___m_FaceID_2 = value;
	}

	inline static int32_t get_offset_of_m_Distance_3() { return static_cast<int32_t>(offsetof(RaycastHit_t2327766465, ___m_Distance_3)); }
	inline float get_m_Distance_3() const { return ___m_Distance_3; }
	inline float* get_address_of_m_Distance_3() { return &___m_Distance_3; }
	inline void set_m_Distance_3(float value)
	{
		___m_Distance_3 = value;
	}

	inline static int32_t get_offset_of_m_UV_4() { return static_cast<int32_t>(offsetof(RaycastHit_t2327766465, ___m_UV_4)); }
	inline Vector2_t3577333262  get_m_UV_4() const { return ___m_UV_4; }
	inline Vector2_t3577333262 * get_address_of_m_UV_4() { return &___m_UV_4; }
	inline void set_m_UV_4(Vector2_t3577333262  value)
	{
		___m_UV_4 = value;
	}

	inline static int32_t get_offset_of_m_Collider_5() { return static_cast<int32_t>(offsetof(RaycastHit_t2327766465, ___m_Collider_5)); }
	inline Collider_t2301021182 * get_m_Collider_5() const { return ___m_Collider_5; }
	inline Collider_t2301021182 ** get_address_of_m_Collider_5() { return &___m_Collider_5; }
	inline void set_m_Collider_5(Collider_t2301021182 * value)
	{
		___m_Collider_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_Collider_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.RaycastHit
struct RaycastHit_t2327766465_marshaled_pinvoke
{
	Vector3_t2903530434  ___m_Point_0;
	Vector3_t2903530434  ___m_Normal_1;
	int32_t ___m_FaceID_2;
	float ___m_Distance_3;
	Vector2_t3577333262  ___m_UV_4;
	Collider_t2301021182 * ___m_Collider_5;
};
// Native definition for COM marshalling of UnityEngine.RaycastHit
struct RaycastHit_t2327766465_marshaled_com
{
	Vector3_t2903530434  ___m_Point_0;
	Vector3_t2903530434  ___m_Normal_1;
	int32_t ___m_FaceID_2;
	float ___m_Distance_3;
	Vector2_t3577333262  ___m_UV_4;
	Collider_t2301021182 * ___m_Collider_5;
};
#endif // RAYCASTHIT_T2327766465_H
#ifndef PLAYABLEGRAPH_T1110114554_H
#define PLAYABLEGRAPH_T1110114554_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Playables.PlayableGraph
struct  PlayableGraph_t1110114554 
{
public:
	// System.IntPtr UnityEngine.Playables.PlayableGraph::m_Handle
	IntPtr_t ___m_Handle_0;
	// System.Int32 UnityEngine.Playables.PlayableGraph::m_Version
	int32_t ___m_Version_1;

public:
	inline static int32_t get_offset_of_m_Handle_0() { return static_cast<int32_t>(offsetof(PlayableGraph_t1110114554, ___m_Handle_0)); }
	inline IntPtr_t get_m_Handle_0() const { return ___m_Handle_0; }
	inline IntPtr_t* get_address_of_m_Handle_0() { return &___m_Handle_0; }
	inline void set_m_Handle_0(IntPtr_t value)
	{
		___m_Handle_0 = value;
	}

	inline static int32_t get_offset_of_m_Version_1() { return static_cast<int32_t>(offsetof(PlayableGraph_t1110114554, ___m_Version_1)); }
	inline int32_t get_m_Version_1() const { return ___m_Version_1; }
	inline int32_t* get_address_of_m_Version_1() { return &___m_Version_1; }
	inline void set_m_Version_1(int32_t value)
	{
		___m_Version_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYABLEGRAPH_T1110114554_H
#ifndef DELEGATE_T537306192_H
#define DELEGATE_T537306192_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t537306192  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	IntPtr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	IntPtr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	IntPtr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::method_code
	IntPtr_t ___method_code_5;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_6;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_7;
	// System.DelegateData System.Delegate::data
	DelegateData_t3370401926 * ___data_8;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t537306192, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t537306192, ___invoke_impl_1)); }
	inline IntPtr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline IntPtr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(IntPtr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t537306192, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t537306192, ___method_3)); }
	inline IntPtr_t get_method_3() const { return ___method_3; }
	inline IntPtr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(IntPtr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t537306192, ___delegate_trampoline_4)); }
	inline IntPtr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline IntPtr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(IntPtr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_method_code_5() { return static_cast<int32_t>(offsetof(Delegate_t537306192, ___method_code_5)); }
	inline IntPtr_t get_method_code_5() const { return ___method_code_5; }
	inline IntPtr_t* get_address_of_method_code_5() { return &___method_code_5; }
	inline void set_method_code_5(IntPtr_t value)
	{
		___method_code_5 = value;
	}

	inline static int32_t get_offset_of_method_info_6() { return static_cast<int32_t>(offsetof(Delegate_t537306192, ___method_info_6)); }
	inline MethodInfo_t * get_method_info_6() const { return ___method_info_6; }
	inline MethodInfo_t ** get_address_of_method_info_6() { return &___method_info_6; }
	inline void set_method_info_6(MethodInfo_t * value)
	{
		___method_info_6 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_6), value);
	}

	inline static int32_t get_offset_of_original_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t537306192, ___original_method_info_7)); }
	inline MethodInfo_t * get_original_method_info_7() const { return ___original_method_info_7; }
	inline MethodInfo_t ** get_address_of_original_method_info_7() { return &___original_method_info_7; }
	inline void set_original_method_info_7(MethodInfo_t * value)
	{
		___original_method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_7), value);
	}

	inline static int32_t get_offset_of_data_8() { return static_cast<int32_t>(offsetof(Delegate_t537306192, ___data_8)); }
	inline DelegateData_t3370401926 * get_data_8() const { return ___data_8; }
	inline DelegateData_t3370401926 ** get_address_of_data_8() { return &___data_8; }
	inline void set_data_8(DelegateData_t3370401926 * value)
	{
		___data_8 = value;
		Il2CppCodeGenWriteBarrier((&___data_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DELEGATE_T537306192_H
#ifndef PLAYSTATE_T3943666923_H
#define PLAYSTATE_T3943666923_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Playables.PlayState
struct  PlayState_t3943666923 
{
public:
	// System.Int32 UnityEngine.Playables.PlayState::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(PlayState_t3943666923, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYSTATE_T3943666923_H
#ifndef COLORSPACE_T3962029982_H
#define COLORSPACE_T3962029982_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ColorSpace
struct  ColorSpace_t3962029982 
{
public:
	// System.Int32 UnityEngine.ColorSpace::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ColorSpace_t3962029982, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLORSPACE_T3962029982_H
#ifndef EDGE_T4129550824_H
#define EDGE_T4129550824_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.RectTransform/Edge
struct  Edge_t4129550824 
{
public:
	// System.Int32 UnityEngine.RectTransform/Edge::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Edge_t4129550824, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EDGE_T4129550824_H
#ifndef AXIS_T148506003_H
#define AXIS_T148506003_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.RectTransform/Axis
struct  Axis_t148506003 
{
public:
	// System.Int32 UnityEngine.RectTransform/Axis::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Axis_t148506003, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AXIS_T148506003_H
#ifndef ARGUMENTEXCEPTION_T4028401650_H
#define ARGUMENTEXCEPTION_T4028401650_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ArgumentException
struct  ArgumentException_t4028401650  : public SystemException_t870616472
{
public:
	// System.String System.ArgumentException::param_name
	String_t* ___param_name_12;

public:
	inline static int32_t get_offset_of_param_name_12() { return static_cast<int32_t>(offsetof(ArgumentException_t4028401650, ___param_name_12)); }
	inline String_t* get_param_name_12() const { return ___param_name_12; }
	inline String_t** get_address_of_param_name_12() { return &___param_name_12; }
	inline void set_param_name_12(String_t* value)
	{
		___param_name_12 = value;
		Il2CppCodeGenWriteBarrier((&___param_name_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARGUMENTEXCEPTION_T4028401650_H
#ifndef HIDEFLAGS_T447623476_H
#define HIDEFLAGS_T447623476_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.HideFlags
struct  HideFlags_t447623476 
{
public:
	// System.Int32 UnityEngine.HideFlags::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(HideFlags_t447623476, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HIDEFLAGS_T447623476_H
#ifndef PRIMITIVETYPE_T4274309238_H
#define PRIMITIVETYPE_T4274309238_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PrimitiveType
struct  PrimitiveType_t4274309238 
{
public:
	// System.Int32 UnityEngine.PrimitiveType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(PrimitiveType_t4274309238, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PRIMITIVETYPE_T4274309238_H
#ifndef QUERYTRIGGERINTERACTION_T3339225157_H
#define QUERYTRIGGERINTERACTION_T3339225157_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.QueryTriggerInteraction
struct  QueryTriggerInteraction_t3339225157 
{
public:
	// System.Int32 UnityEngine.QueryTriggerInteraction::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(QueryTriggerInteraction_t3339225157, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUERYTRIGGERINTERACTION_T3339225157_H
#ifndef DATASTREAMTYPE_T3237056429_H
#define DATASTREAMTYPE_T3237056429_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Playables.DataStreamType
struct  DataStreamType_t3237056429 
{
public:
	// System.Int32 UnityEngine.Playables.DataStreamType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(DataStreamType_t3237056429, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATASTREAMTYPE_T3237056429_H
#ifndef ANIMATIONCURVE_T2184836714_H
#define ANIMATIONCURVE_T2184836714_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.AnimationCurve
struct  AnimationCurve_t2184836714  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.AnimationCurve::m_Ptr
	IntPtr_t ___m_Ptr_0;

public:
	inline static int32_t get_offset_of_m_Ptr_0() { return static_cast<int32_t>(offsetof(AnimationCurve_t2184836714, ___m_Ptr_0)); }
	inline IntPtr_t get_m_Ptr_0() const { return ___m_Ptr_0; }
	inline IntPtr_t* get_address_of_m_Ptr_0() { return &___m_Ptr_0; }
	inline void set_m_Ptr_0(IntPtr_t value)
	{
		___m_Ptr_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.AnimationCurve
struct AnimationCurve_t2184836714_marshaled_pinvoke
{
	intptr_t ___m_Ptr_0;
};
// Native definition for COM marshalling of UnityEngine.AnimationCurve
struct AnimationCurve_t2184836714_marshaled_com
{
	intptr_t ___m_Ptr_0;
};
#endif // ANIMATIONCURVE_T2184836714_H
#ifndef PLAYABLEHANDLE_T1286828706_H
#define PLAYABLEHANDLE_T1286828706_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Playables.PlayableHandle
struct  PlayableHandle_t1286828706 
{
public:
	// System.IntPtr UnityEngine.Playables.PlayableHandle::m_Handle
	IntPtr_t ___m_Handle_0;
	// System.Int32 UnityEngine.Playables.PlayableHandle::m_Version
	int32_t ___m_Version_1;

public:
	inline static int32_t get_offset_of_m_Handle_0() { return static_cast<int32_t>(offsetof(PlayableHandle_t1286828706, ___m_Handle_0)); }
	inline IntPtr_t get_m_Handle_0() const { return ___m_Handle_0; }
	inline IntPtr_t* get_address_of_m_Handle_0() { return &___m_Handle_0; }
	inline void set_m_Handle_0(IntPtr_t value)
	{
		___m_Handle_0 = value;
	}

	inline static int32_t get_offset_of_m_Version_1() { return static_cast<int32_t>(offsetof(PlayableHandle_t1286828706, ___m_Version_1)); }
	inline int32_t get_m_Version_1() const { return ___m_Version_1; }
	inline int32_t* get_address_of_m_Version_1() { return &___m_Version_1; }
	inline void set_m_Version_1(int32_t value)
	{
		___m_Version_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYABLEHANDLE_T1286828706_H
#ifndef OPERATINGSYSTEMFAMILY_T2447930521_H
#define OPERATINGSYSTEMFAMILY_T2447930521_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.OperatingSystemFamily
struct  OperatingSystemFamily_t2447930521 
{
public:
	// System.Int32 UnityEngine.OperatingSystemFamily::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(OperatingSystemFamily_t2447930521, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OPERATINGSYSTEMFAMILY_T2447930521_H
#ifndef RENDERBUFFER_T3998806177_H
#define RENDERBUFFER_T3998806177_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.RenderBuffer
struct  RenderBuffer_t3998806177 
{
public:
	// System.Int32 UnityEngine.RenderBuffer::m_RenderTextureInstanceID
	int32_t ___m_RenderTextureInstanceID_0;
	// System.IntPtr UnityEngine.RenderBuffer::m_BufferPtr
	IntPtr_t ___m_BufferPtr_1;

public:
	inline static int32_t get_offset_of_m_RenderTextureInstanceID_0() { return static_cast<int32_t>(offsetof(RenderBuffer_t3998806177, ___m_RenderTextureInstanceID_0)); }
	inline int32_t get_m_RenderTextureInstanceID_0() const { return ___m_RenderTextureInstanceID_0; }
	inline int32_t* get_address_of_m_RenderTextureInstanceID_0() { return &___m_RenderTextureInstanceID_0; }
	inline void set_m_RenderTextureInstanceID_0(int32_t value)
	{
		___m_RenderTextureInstanceID_0 = value;
	}

	inline static int32_t get_offset_of_m_BufferPtr_1() { return static_cast<int32_t>(offsetof(RenderBuffer_t3998806177, ___m_BufferPtr_1)); }
	inline IntPtr_t get_m_BufferPtr_1() const { return ___m_BufferPtr_1; }
	inline IntPtr_t* get_address_of_m_BufferPtr_1() { return &___m_BufferPtr_1; }
	inline void set_m_BufferPtr_1(IntPtr_t value)
	{
		___m_BufferPtr_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RENDERBUFFER_T3998806177_H
#ifndef PLAYMODE_T3836662961_H
#define PLAYMODE_T3836662961_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PlayMode
struct  PlayMode_t3836662961 
{
public:
	// System.Int32 UnityEngine.PlayMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(PlayMode_t3836662961, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYMODE_T3836662961_H
#ifndef PLANE_T2300588969_H
#define PLANE_T2300588969_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Plane
struct  Plane_t2300588969 
{
public:
	// UnityEngine.Vector3 UnityEngine.Plane::m_Normal
	Vector3_t2903530434  ___m_Normal_0;
	// System.Single UnityEngine.Plane::m_Distance
	float ___m_Distance_1;

public:
	inline static int32_t get_offset_of_m_Normal_0() { return static_cast<int32_t>(offsetof(Plane_t2300588969, ___m_Normal_0)); }
	inline Vector3_t2903530434  get_m_Normal_0() const { return ___m_Normal_0; }
	inline Vector3_t2903530434 * get_address_of_m_Normal_0() { return &___m_Normal_0; }
	inline void set_m_Normal_0(Vector3_t2903530434  value)
	{
		___m_Normal_0 = value;
	}

	inline static int32_t get_offset_of_m_Distance_1() { return static_cast<int32_t>(offsetof(Plane_t2300588969, ___m_Distance_1)); }
	inline float get_m_Distance_1() const { return ___m_Distance_1; }
	inline float* get_address_of_m_Distance_1() { return &___m_Distance_1; }
	inline void set_m_Distance_1(float value)
	{
		___m_Distance_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLANE_T2300588969_H
#ifndef MATERIAL_T1079520667_H
#define MATERIAL_T1079520667_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Material
struct  Material_t1079520667  : public Object_t1502412432
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATERIAL_T1079520667_H
#ifndef PLAYABLEOUTPUT_T3724158364_H
#define PLAYABLEOUTPUT_T3724158364_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Playables.PlayableOutput
struct  PlayableOutput_t3724158364 
{
public:
	// UnityEngine.Playables.PlayableOutputHandle UnityEngine.Playables.PlayableOutput::m_Handle
	PlayableOutputHandle_t2200222555  ___m_Handle_0;

public:
	inline static int32_t get_offset_of_m_Handle_0() { return static_cast<int32_t>(offsetof(PlayableOutput_t3724158364, ___m_Handle_0)); }
	inline PlayableOutputHandle_t2200222555  get_m_Handle_0() const { return ___m_Handle_0; }
	inline PlayableOutputHandle_t2200222555 * get_address_of_m_Handle_0() { return &___m_Handle_0; }
	inline void set_m_Handle_0(PlayableOutputHandle_t2200222555  value)
	{
		___m_Handle_0 = value;
	}
};

struct PlayableOutput_t3724158364_StaticFields
{
public:
	// UnityEngine.Playables.PlayableOutput UnityEngine.Playables.PlayableOutput::m_NullPlayableOutput
	PlayableOutput_t3724158364  ___m_NullPlayableOutput_1;

public:
	inline static int32_t get_offset_of_m_NullPlayableOutput_1() { return static_cast<int32_t>(offsetof(PlayableOutput_t3724158364_StaticFields, ___m_NullPlayableOutput_1)); }
	inline PlayableOutput_t3724158364  get_m_NullPlayableOutput_1() const { return ___m_NullPlayableOutput_1; }
	inline PlayableOutput_t3724158364 * get_address_of_m_NullPlayableOutput_1() { return &___m_NullPlayableOutput_1; }
	inline void set_m_NullPlayableOutput_1(PlayableOutput_t3724158364  value)
	{
		___m_NullPlayableOutput_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYABLEOUTPUT_T3724158364_H
#ifndef SCRIPTABLEOBJECT_T3635579074_H
#define SCRIPTABLEOBJECT_T3635579074_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ScriptableObject
struct  ScriptableObject_t3635579074  : public Object_t1502412432
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_t3635579074_marshaled_pinvoke : public Object_t1502412432_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_t3635579074_marshaled_com : public Object_t1502412432_marshaled_com
{
};
#endif // SCRIPTABLEOBJECT_T3635579074_H
#ifndef GAMEOBJECT_T1811656094_H
#define GAMEOBJECT_T1811656094_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.GameObject
struct  GameObject_t1811656094  : public Object_t1502412432
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAMEOBJECT_T1811656094_H
#ifndef PLAYABLEBINDING_T3706191642_H
#define PLAYABLEBINDING_T3706191642_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Playables.PlayableBinding
struct  PlayableBinding_t3706191642 
{
public:
	union
	{
		struct
		{
			// System.String UnityEngine.Playables.PlayableBinding::<streamName>k__BackingField
			String_t* ___U3CstreamNameU3Ek__BackingField_2;
			// UnityEngine.Playables.DataStreamType UnityEngine.Playables.PlayableBinding::<streamType>k__BackingField
			int32_t ___U3CstreamTypeU3Ek__BackingField_3;
			// UnityEngine.Object UnityEngine.Playables.PlayableBinding::<sourceObject>k__BackingField
			Object_t1502412432 * ___U3CsourceObjectU3Ek__BackingField_4;
			// System.Type UnityEngine.Playables.PlayableBinding::<sourceBindingType>k__BackingField
			Type_t * ___U3CsourceBindingTypeU3Ek__BackingField_5;
		};
		uint8_t PlayableBinding_t3706191642__padding[1];
	};

public:
	inline static int32_t get_offset_of_U3CstreamNameU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(PlayableBinding_t3706191642, ___U3CstreamNameU3Ek__BackingField_2)); }
	inline String_t* get_U3CstreamNameU3Ek__BackingField_2() const { return ___U3CstreamNameU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3CstreamNameU3Ek__BackingField_2() { return &___U3CstreamNameU3Ek__BackingField_2; }
	inline void set_U3CstreamNameU3Ek__BackingField_2(String_t* value)
	{
		___U3CstreamNameU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CstreamNameU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3CstreamTypeU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(PlayableBinding_t3706191642, ___U3CstreamTypeU3Ek__BackingField_3)); }
	inline int32_t get_U3CstreamTypeU3Ek__BackingField_3() const { return ___U3CstreamTypeU3Ek__BackingField_3; }
	inline int32_t* get_address_of_U3CstreamTypeU3Ek__BackingField_3() { return &___U3CstreamTypeU3Ek__BackingField_3; }
	inline void set_U3CstreamTypeU3Ek__BackingField_3(int32_t value)
	{
		___U3CstreamTypeU3Ek__BackingField_3 = value;
	}

	inline static int32_t get_offset_of_U3CsourceObjectU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(PlayableBinding_t3706191642, ___U3CsourceObjectU3Ek__BackingField_4)); }
	inline Object_t1502412432 * get_U3CsourceObjectU3Ek__BackingField_4() const { return ___U3CsourceObjectU3Ek__BackingField_4; }
	inline Object_t1502412432 ** get_address_of_U3CsourceObjectU3Ek__BackingField_4() { return &___U3CsourceObjectU3Ek__BackingField_4; }
	inline void set_U3CsourceObjectU3Ek__BackingField_4(Object_t1502412432 * value)
	{
		___U3CsourceObjectU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CsourceObjectU3Ek__BackingField_4), value);
	}

	inline static int32_t get_offset_of_U3CsourceBindingTypeU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(PlayableBinding_t3706191642, ___U3CsourceBindingTypeU3Ek__BackingField_5)); }
	inline Type_t * get_U3CsourceBindingTypeU3Ek__BackingField_5() const { return ___U3CsourceBindingTypeU3Ek__BackingField_5; }
	inline Type_t ** get_address_of_U3CsourceBindingTypeU3Ek__BackingField_5() { return &___U3CsourceBindingTypeU3Ek__BackingField_5; }
	inline void set_U3CsourceBindingTypeU3Ek__BackingField_5(Type_t * value)
	{
		___U3CsourceBindingTypeU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CsourceBindingTypeU3Ek__BackingField_5), value);
	}
};

struct PlayableBinding_t3706191642_StaticFields
{
public:
	// UnityEngine.Playables.PlayableBinding[] UnityEngine.Playables.PlayableBinding::None
	PlayableBindingU5BU5D_t3242831679* ___None_0;
	// System.Double UnityEngine.Playables.PlayableBinding::DefaultDuration
	double ___DefaultDuration_1;

public:
	inline static int32_t get_offset_of_None_0() { return static_cast<int32_t>(offsetof(PlayableBinding_t3706191642_StaticFields, ___None_0)); }
	inline PlayableBindingU5BU5D_t3242831679* get_None_0() const { return ___None_0; }
	inline PlayableBindingU5BU5D_t3242831679** get_address_of_None_0() { return &___None_0; }
	inline void set_None_0(PlayableBindingU5BU5D_t3242831679* value)
	{
		___None_0 = value;
		Il2CppCodeGenWriteBarrier((&___None_0), value);
	}

	inline static int32_t get_offset_of_DefaultDuration_1() { return static_cast<int32_t>(offsetof(PlayableBinding_t3706191642_StaticFields, ___DefaultDuration_1)); }
	inline double get_DefaultDuration_1() const { return ___DefaultDuration_1; }
	inline double* get_address_of_DefaultDuration_1() { return &___DefaultDuration_1; }
	inline void set_DefaultDuration_1(double value)
	{
		___DefaultDuration_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Playables.PlayableBinding
struct PlayableBinding_t3706191642_marshaled_pinvoke
{
	union
	{
		struct
		{
			char* ___U3CstreamNameU3Ek__BackingField_2;
			int32_t ___U3CstreamTypeU3Ek__BackingField_3;
			Object_t1502412432_marshaled_pinvoke ___U3CsourceObjectU3Ek__BackingField_4;
			Type_t * ___U3CsourceBindingTypeU3Ek__BackingField_5;
		};
		uint8_t PlayableBinding_t3706191642__padding[1];
	};
};
// Native definition for COM marshalling of UnityEngine.Playables.PlayableBinding
struct PlayableBinding_t3706191642_marshaled_com
{
	union
	{
		struct
		{
			Il2CppChar* ___U3CstreamNameU3Ek__BackingField_2;
			int32_t ___U3CstreamTypeU3Ek__BackingField_3;
			Object_t1502412432_marshaled_com* ___U3CsourceObjectU3Ek__BackingField_4;
			Type_t * ___U3CsourceBindingTypeU3Ek__BackingField_5;
		};
		uint8_t PlayableBinding_t3706191642__padding[1];
	};
};
#endif // PLAYABLEBINDING_T3706191642_H
#ifndef FRAMEDATA_T2240265764_H
#define FRAMEDATA_T2240265764_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Playables.FrameData
struct  FrameData_t2240265764 
{
public:
	// System.UInt64 UnityEngine.Playables.FrameData::m_FrameID
	uint64_t ___m_FrameID_0;
	// System.Double UnityEngine.Playables.FrameData::m_DeltaTime
	double ___m_DeltaTime_1;
	// System.Single UnityEngine.Playables.FrameData::m_Weight
	float ___m_Weight_2;
	// System.Single UnityEngine.Playables.FrameData::m_EffectiveWeight
	float ___m_EffectiveWeight_3;
	// System.Single UnityEngine.Playables.FrameData::m_EffectiveSpeed
	float ___m_EffectiveSpeed_4;
	// UnityEngine.Playables.FrameData/Flags UnityEngine.Playables.FrameData::m_Flags
	int32_t ___m_Flags_5;

public:
	inline static int32_t get_offset_of_m_FrameID_0() { return static_cast<int32_t>(offsetof(FrameData_t2240265764, ___m_FrameID_0)); }
	inline uint64_t get_m_FrameID_0() const { return ___m_FrameID_0; }
	inline uint64_t* get_address_of_m_FrameID_0() { return &___m_FrameID_0; }
	inline void set_m_FrameID_0(uint64_t value)
	{
		___m_FrameID_0 = value;
	}

	inline static int32_t get_offset_of_m_DeltaTime_1() { return static_cast<int32_t>(offsetof(FrameData_t2240265764, ___m_DeltaTime_1)); }
	inline double get_m_DeltaTime_1() const { return ___m_DeltaTime_1; }
	inline double* get_address_of_m_DeltaTime_1() { return &___m_DeltaTime_1; }
	inline void set_m_DeltaTime_1(double value)
	{
		___m_DeltaTime_1 = value;
	}

	inline static int32_t get_offset_of_m_Weight_2() { return static_cast<int32_t>(offsetof(FrameData_t2240265764, ___m_Weight_2)); }
	inline float get_m_Weight_2() const { return ___m_Weight_2; }
	inline float* get_address_of_m_Weight_2() { return &___m_Weight_2; }
	inline void set_m_Weight_2(float value)
	{
		___m_Weight_2 = value;
	}

	inline static int32_t get_offset_of_m_EffectiveWeight_3() { return static_cast<int32_t>(offsetof(FrameData_t2240265764, ___m_EffectiveWeight_3)); }
	inline float get_m_EffectiveWeight_3() const { return ___m_EffectiveWeight_3; }
	inline float* get_address_of_m_EffectiveWeight_3() { return &___m_EffectiveWeight_3; }
	inline void set_m_EffectiveWeight_3(float value)
	{
		___m_EffectiveWeight_3 = value;
	}

	inline static int32_t get_offset_of_m_EffectiveSpeed_4() { return static_cast<int32_t>(offsetof(FrameData_t2240265764, ___m_EffectiveSpeed_4)); }
	inline float get_m_EffectiveSpeed_4() const { return ___m_EffectiveSpeed_4; }
	inline float* get_address_of_m_EffectiveSpeed_4() { return &___m_EffectiveSpeed_4; }
	inline void set_m_EffectiveSpeed_4(float value)
	{
		___m_EffectiveSpeed_4 = value;
	}

	inline static int32_t get_offset_of_m_Flags_5() { return static_cast<int32_t>(offsetof(FrameData_t2240265764, ___m_Flags_5)); }
	inline int32_t get_m_Flags_5() const { return ___m_Flags_5; }
	inline int32_t* get_address_of_m_Flags_5() { return &___m_Flags_5; }
	inline void set_m_Flags_5(int32_t value)
	{
		___m_Flags_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FRAMEDATA_T2240265764_H
#ifndef SCRIPTPLAYABLEOUTPUT_T49585950_H
#define SCRIPTPLAYABLEOUTPUT_T49585950_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Playables.ScriptPlayableOutput
struct  ScriptPlayableOutput_t49585950 
{
public:
	// UnityEngine.Playables.PlayableOutputHandle UnityEngine.Playables.ScriptPlayableOutput::m_Handle
	PlayableOutputHandle_t2200222555  ___m_Handle_0;

public:
	inline static int32_t get_offset_of_m_Handle_0() { return static_cast<int32_t>(offsetof(ScriptPlayableOutput_t49585950, ___m_Handle_0)); }
	inline PlayableOutputHandle_t2200222555  get_m_Handle_0() const { return ___m_Handle_0; }
	inline PlayableOutputHandle_t2200222555 * get_address_of_m_Handle_0() { return &___m_Handle_0; }
	inline void set_m_Handle_0(PlayableOutputHandle_t2200222555  value)
	{
		___m_Handle_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCRIPTPLAYABLEOUTPUT_T49585950_H
#ifndef SHADER_T3778723916_H
#define SHADER_T3778723916_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Shader
struct  Shader_t3778723916  : public Object_t1502412432
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SHADER_T3778723916_H
#ifndef PLAYABLE_T735025802_H
#define PLAYABLE_T735025802_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Playables.Playable
struct  Playable_t735025802 
{
public:
	// UnityEngine.Playables.PlayableHandle UnityEngine.Playables.Playable::m_Handle
	PlayableHandle_t1286828706  ___m_Handle_0;

public:
	inline static int32_t get_offset_of_m_Handle_0() { return static_cast<int32_t>(offsetof(Playable_t735025802, ___m_Handle_0)); }
	inline PlayableHandle_t1286828706  get_m_Handle_0() const { return ___m_Handle_0; }
	inline PlayableHandle_t1286828706 * get_address_of_m_Handle_0() { return &___m_Handle_0; }
	inline void set_m_Handle_0(PlayableHandle_t1286828706  value)
	{
		___m_Handle_0 = value;
	}
};

struct Playable_t735025802_StaticFields
{
public:
	// UnityEngine.Playables.Playable UnityEngine.Playables.Playable::m_NullPlayable
	Playable_t735025802  ___m_NullPlayable_1;

public:
	inline static int32_t get_offset_of_m_NullPlayable_1() { return static_cast<int32_t>(offsetof(Playable_t735025802_StaticFields, ___m_NullPlayable_1)); }
	inline Playable_t735025802  get_m_NullPlayable_1() const { return ___m_NullPlayable_1; }
	inline Playable_t735025802 * get_address_of_m_NullPlayable_1() { return &___m_NullPlayable_1; }
	inline void set_m_NullPlayable_1(Playable_t735025802  value)
	{
		___m_NullPlayable_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYABLE_T735025802_H
#ifndef RUNTIMEINITIALIZEONLOADMETHODATTRIBUTE_T1540303992_H
#define RUNTIMEINITIALIZEONLOADMETHODATTRIBUTE_T1540303992_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.RuntimeInitializeOnLoadMethodAttribute
struct  RuntimeInitializeOnLoadMethodAttribute_t1540303992  : public PreserveAttribute_t3468091719
{
public:
	// UnityEngine.RuntimeInitializeLoadType UnityEngine.RuntimeInitializeOnLoadMethodAttribute::<loadType>k__BackingField
	int32_t ___U3CloadTypeU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CloadTypeU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(RuntimeInitializeOnLoadMethodAttribute_t1540303992, ___U3CloadTypeU3Ek__BackingField_0)); }
	inline int32_t get_U3CloadTypeU3Ek__BackingField_0() const { return ___U3CloadTypeU3Ek__BackingField_0; }
	inline int32_t* get_address_of_U3CloadTypeU3Ek__BackingField_0() { return &___U3CloadTypeU3Ek__BackingField_0; }
	inline void set_U3CloadTypeU3Ek__BackingField_0(int32_t value)
	{
		___U3CloadTypeU3Ek__BackingField_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEINITIALIZEONLOADMETHODATTRIBUTE_T1540303992_H
#ifndef RUNTIMEANIMATORCONTROLLER_T3799638084_H
#define RUNTIMEANIMATORCONTROLLER_T3799638084_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.RuntimeAnimatorController
struct  RuntimeAnimatorController_t3799638084  : public Object_t1502412432
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEANIMATORCONTROLLER_T3799638084_H
#ifndef RESOURCEREQUEST_T963543698_H
#define RESOURCEREQUEST_T963543698_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ResourceRequest
struct  ResourceRequest_t963543698  : public AsyncOperation_t2744032732
{
public:
	// System.String UnityEngine.ResourceRequest::m_Path
	String_t* ___m_Path_1;
	// System.Type UnityEngine.ResourceRequest::m_Type
	Type_t * ___m_Type_2;

public:
	inline static int32_t get_offset_of_m_Path_1() { return static_cast<int32_t>(offsetof(ResourceRequest_t963543698, ___m_Path_1)); }
	inline String_t* get_m_Path_1() const { return ___m_Path_1; }
	inline String_t** get_address_of_m_Path_1() { return &___m_Path_1; }
	inline void set_m_Path_1(String_t* value)
	{
		___m_Path_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_Path_1), value);
	}

	inline static int32_t get_offset_of_m_Type_2() { return static_cast<int32_t>(offsetof(ResourceRequest_t963543698, ___m_Type_2)); }
	inline Type_t * get_m_Type_2() const { return ___m_Type_2; }
	inline Type_t ** get_address_of_m_Type_2() { return &___m_Type_2; }
	inline void set_m_Type_2(Type_t * value)
	{
		___m_Type_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_Type_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.ResourceRequest
struct ResourceRequest_t963543698_marshaled_pinvoke : public AsyncOperation_t2744032732_marshaled_pinvoke
{
	char* ___m_Path_1;
	Type_t * ___m_Type_2;
};
// Native definition for COM marshalling of UnityEngine.ResourceRequest
struct ResourceRequest_t963543698_marshaled_com : public AsyncOperation_t2744032732_marshaled_com
{
	Il2CppChar* ___m_Path_1;
	Type_t * ___m_Type_2;
};
#endif // RESOURCEREQUEST_T963543698_H
#ifndef COMPONENT_T4087199522_H
#define COMPONENT_T4087199522_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t4087199522  : public Object_t1502412432
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T4087199522_H
#ifndef RENDERTEXTUREDESCRIPTOR_T3096142983_H
#define RENDERTEXTUREDESCRIPTOR_T3096142983_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.RenderTextureDescriptor
struct  RenderTextureDescriptor_t3096142983 
{
public:
	// System.Int32 UnityEngine.RenderTextureDescriptor::<width>k__BackingField
	int32_t ___U3CwidthU3Ek__BackingField_0;
	// System.Int32 UnityEngine.RenderTextureDescriptor::<height>k__BackingField
	int32_t ___U3CheightU3Ek__BackingField_1;
	// System.Int32 UnityEngine.RenderTextureDescriptor::<msaaSamples>k__BackingField
	int32_t ___U3CmsaaSamplesU3Ek__BackingField_2;
	// System.Int32 UnityEngine.RenderTextureDescriptor::<volumeDepth>k__BackingField
	int32_t ___U3CvolumeDepthU3Ek__BackingField_3;
	// UnityEngine.RenderTextureFormat UnityEngine.RenderTextureDescriptor::<colorFormat>k__BackingField
	int32_t ___U3CcolorFormatU3Ek__BackingField_4;
	// System.Int32 UnityEngine.RenderTextureDescriptor::_depthBufferBits
	int32_t ____depthBufferBits_5;
	// UnityEngine.Rendering.TextureDimension UnityEngine.RenderTextureDescriptor::<dimension>k__BackingField
	int32_t ___U3CdimensionU3Ek__BackingField_7;
	// UnityEngine.Rendering.ShadowSamplingMode UnityEngine.RenderTextureDescriptor::<shadowSamplingMode>k__BackingField
	int32_t ___U3CshadowSamplingModeU3Ek__BackingField_8;
	// UnityEngine.VRTextureUsage UnityEngine.RenderTextureDescriptor::<vrUsage>k__BackingField
	int32_t ___U3CvrUsageU3Ek__BackingField_9;
	// UnityEngine.RenderTextureCreationFlags UnityEngine.RenderTextureDescriptor::_flags
	int32_t ____flags_10;
	// UnityEngine.RenderTextureMemoryless UnityEngine.RenderTextureDescriptor::<memoryless>k__BackingField
	int32_t ___U3CmemorylessU3Ek__BackingField_11;

public:
	inline static int32_t get_offset_of_U3CwidthU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(RenderTextureDescriptor_t3096142983, ___U3CwidthU3Ek__BackingField_0)); }
	inline int32_t get_U3CwidthU3Ek__BackingField_0() const { return ___U3CwidthU3Ek__BackingField_0; }
	inline int32_t* get_address_of_U3CwidthU3Ek__BackingField_0() { return &___U3CwidthU3Ek__BackingField_0; }
	inline void set_U3CwidthU3Ek__BackingField_0(int32_t value)
	{
		___U3CwidthU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CheightU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(RenderTextureDescriptor_t3096142983, ___U3CheightU3Ek__BackingField_1)); }
	inline int32_t get_U3CheightU3Ek__BackingField_1() const { return ___U3CheightU3Ek__BackingField_1; }
	inline int32_t* get_address_of_U3CheightU3Ek__BackingField_1() { return &___U3CheightU3Ek__BackingField_1; }
	inline void set_U3CheightU3Ek__BackingField_1(int32_t value)
	{
		___U3CheightU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CmsaaSamplesU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(RenderTextureDescriptor_t3096142983, ___U3CmsaaSamplesU3Ek__BackingField_2)); }
	inline int32_t get_U3CmsaaSamplesU3Ek__BackingField_2() const { return ___U3CmsaaSamplesU3Ek__BackingField_2; }
	inline int32_t* get_address_of_U3CmsaaSamplesU3Ek__BackingField_2() { return &___U3CmsaaSamplesU3Ek__BackingField_2; }
	inline void set_U3CmsaaSamplesU3Ek__BackingField_2(int32_t value)
	{
		___U3CmsaaSamplesU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3CvolumeDepthU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(RenderTextureDescriptor_t3096142983, ___U3CvolumeDepthU3Ek__BackingField_3)); }
	inline int32_t get_U3CvolumeDepthU3Ek__BackingField_3() const { return ___U3CvolumeDepthU3Ek__BackingField_3; }
	inline int32_t* get_address_of_U3CvolumeDepthU3Ek__BackingField_3() { return &___U3CvolumeDepthU3Ek__BackingField_3; }
	inline void set_U3CvolumeDepthU3Ek__BackingField_3(int32_t value)
	{
		___U3CvolumeDepthU3Ek__BackingField_3 = value;
	}

	inline static int32_t get_offset_of_U3CcolorFormatU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(RenderTextureDescriptor_t3096142983, ___U3CcolorFormatU3Ek__BackingField_4)); }
	inline int32_t get_U3CcolorFormatU3Ek__BackingField_4() const { return ___U3CcolorFormatU3Ek__BackingField_4; }
	inline int32_t* get_address_of_U3CcolorFormatU3Ek__BackingField_4() { return &___U3CcolorFormatU3Ek__BackingField_4; }
	inline void set_U3CcolorFormatU3Ek__BackingField_4(int32_t value)
	{
		___U3CcolorFormatU3Ek__BackingField_4 = value;
	}

	inline static int32_t get_offset_of__depthBufferBits_5() { return static_cast<int32_t>(offsetof(RenderTextureDescriptor_t3096142983, ____depthBufferBits_5)); }
	inline int32_t get__depthBufferBits_5() const { return ____depthBufferBits_5; }
	inline int32_t* get_address_of__depthBufferBits_5() { return &____depthBufferBits_5; }
	inline void set__depthBufferBits_5(int32_t value)
	{
		____depthBufferBits_5 = value;
	}

	inline static int32_t get_offset_of_U3CdimensionU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(RenderTextureDescriptor_t3096142983, ___U3CdimensionU3Ek__BackingField_7)); }
	inline int32_t get_U3CdimensionU3Ek__BackingField_7() const { return ___U3CdimensionU3Ek__BackingField_7; }
	inline int32_t* get_address_of_U3CdimensionU3Ek__BackingField_7() { return &___U3CdimensionU3Ek__BackingField_7; }
	inline void set_U3CdimensionU3Ek__BackingField_7(int32_t value)
	{
		___U3CdimensionU3Ek__BackingField_7 = value;
	}

	inline static int32_t get_offset_of_U3CshadowSamplingModeU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(RenderTextureDescriptor_t3096142983, ___U3CshadowSamplingModeU3Ek__BackingField_8)); }
	inline int32_t get_U3CshadowSamplingModeU3Ek__BackingField_8() const { return ___U3CshadowSamplingModeU3Ek__BackingField_8; }
	inline int32_t* get_address_of_U3CshadowSamplingModeU3Ek__BackingField_8() { return &___U3CshadowSamplingModeU3Ek__BackingField_8; }
	inline void set_U3CshadowSamplingModeU3Ek__BackingField_8(int32_t value)
	{
		___U3CshadowSamplingModeU3Ek__BackingField_8 = value;
	}

	inline static int32_t get_offset_of_U3CvrUsageU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(RenderTextureDescriptor_t3096142983, ___U3CvrUsageU3Ek__BackingField_9)); }
	inline int32_t get_U3CvrUsageU3Ek__BackingField_9() const { return ___U3CvrUsageU3Ek__BackingField_9; }
	inline int32_t* get_address_of_U3CvrUsageU3Ek__BackingField_9() { return &___U3CvrUsageU3Ek__BackingField_9; }
	inline void set_U3CvrUsageU3Ek__BackingField_9(int32_t value)
	{
		___U3CvrUsageU3Ek__BackingField_9 = value;
	}

	inline static int32_t get_offset_of__flags_10() { return static_cast<int32_t>(offsetof(RenderTextureDescriptor_t3096142983, ____flags_10)); }
	inline int32_t get__flags_10() const { return ____flags_10; }
	inline int32_t* get_address_of__flags_10() { return &____flags_10; }
	inline void set__flags_10(int32_t value)
	{
		____flags_10 = value;
	}

	inline static int32_t get_offset_of_U3CmemorylessU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(RenderTextureDescriptor_t3096142983, ___U3CmemorylessU3Ek__BackingField_11)); }
	inline int32_t get_U3CmemorylessU3Ek__BackingField_11() const { return ___U3CmemorylessU3Ek__BackingField_11; }
	inline int32_t* get_address_of_U3CmemorylessU3Ek__BackingField_11() { return &___U3CmemorylessU3Ek__BackingField_11; }
	inline void set_U3CmemorylessU3Ek__BackingField_11(int32_t value)
	{
		___U3CmemorylessU3Ek__BackingField_11 = value;
	}
};

struct RenderTextureDescriptor_t3096142983_StaticFields
{
public:
	// System.Int32[] UnityEngine.RenderTextureDescriptor::depthFormatBits
	Int32U5BU5D_t2324750880* ___depthFormatBits_6;

public:
	inline static int32_t get_offset_of_depthFormatBits_6() { return static_cast<int32_t>(offsetof(RenderTextureDescriptor_t3096142983_StaticFields, ___depthFormatBits_6)); }
	inline Int32U5BU5D_t2324750880* get_depthFormatBits_6() const { return ___depthFormatBits_6; }
	inline Int32U5BU5D_t2324750880** get_address_of_depthFormatBits_6() { return &___depthFormatBits_6; }
	inline void set_depthFormatBits_6(Int32U5BU5D_t2324750880* value)
	{
		___depthFormatBits_6 = value;
		Il2CppCodeGenWriteBarrier((&___depthFormatBits_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RENDERTEXTUREDESCRIPTOR_T3096142983_H
#ifndef TYPE_T_H
#define TYPE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Type
struct  Type_t  : public MemberInfo_t
{
public:
	// System.RuntimeTypeHandle System.Type::_impl
	RuntimeTypeHandle_t845633806  ____impl_1;

public:
	inline static int32_t get_offset_of__impl_1() { return static_cast<int32_t>(offsetof(Type_t, ____impl_1)); }
	inline RuntimeTypeHandle_t845633806  get__impl_1() const { return ____impl_1; }
	inline RuntimeTypeHandle_t845633806 * get_address_of__impl_1() { return &____impl_1; }
	inline void set__impl_1(RuntimeTypeHandle_t845633806  value)
	{
		____impl_1 = value;
	}
};

struct Type_t_StaticFields
{
public:
	// System.Char System.Type::Delimiter
	Il2CppChar ___Delimiter_2;
	// System.Type[] System.Type::EmptyTypes
	TypeU5BU5D_t3854702846* ___EmptyTypes_3;
	// System.Reflection.MemberFilter System.Type::FilterAttribute
	MemberFilter_t4208935459 * ___FilterAttribute_4;
	// System.Reflection.MemberFilter System.Type::FilterName
	MemberFilter_t4208935459 * ___FilterName_5;
	// System.Reflection.MemberFilter System.Type::FilterNameIgnoreCase
	MemberFilter_t4208935459 * ___FilterNameIgnoreCase_6;
	// System.Object System.Type::Missing
	RuntimeObject * ___Missing_7;

public:
	inline static int32_t get_offset_of_Delimiter_2() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Delimiter_2)); }
	inline Il2CppChar get_Delimiter_2() const { return ___Delimiter_2; }
	inline Il2CppChar* get_address_of_Delimiter_2() { return &___Delimiter_2; }
	inline void set_Delimiter_2(Il2CppChar value)
	{
		___Delimiter_2 = value;
	}

	inline static int32_t get_offset_of_EmptyTypes_3() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___EmptyTypes_3)); }
	inline TypeU5BU5D_t3854702846* get_EmptyTypes_3() const { return ___EmptyTypes_3; }
	inline TypeU5BU5D_t3854702846** get_address_of_EmptyTypes_3() { return &___EmptyTypes_3; }
	inline void set_EmptyTypes_3(TypeU5BU5D_t3854702846* value)
	{
		___EmptyTypes_3 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyTypes_3), value);
	}

	inline static int32_t get_offset_of_FilterAttribute_4() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterAttribute_4)); }
	inline MemberFilter_t4208935459 * get_FilterAttribute_4() const { return ___FilterAttribute_4; }
	inline MemberFilter_t4208935459 ** get_address_of_FilterAttribute_4() { return &___FilterAttribute_4; }
	inline void set_FilterAttribute_4(MemberFilter_t4208935459 * value)
	{
		___FilterAttribute_4 = value;
		Il2CppCodeGenWriteBarrier((&___FilterAttribute_4), value);
	}

	inline static int32_t get_offset_of_FilterName_5() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterName_5)); }
	inline MemberFilter_t4208935459 * get_FilterName_5() const { return ___FilterName_5; }
	inline MemberFilter_t4208935459 ** get_address_of_FilterName_5() { return &___FilterName_5; }
	inline void set_FilterName_5(MemberFilter_t4208935459 * value)
	{
		___FilterName_5 = value;
		Il2CppCodeGenWriteBarrier((&___FilterName_5), value);
	}

	inline static int32_t get_offset_of_FilterNameIgnoreCase_6() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterNameIgnoreCase_6)); }
	inline MemberFilter_t4208935459 * get_FilterNameIgnoreCase_6() const { return ___FilterNameIgnoreCase_6; }
	inline MemberFilter_t4208935459 ** get_address_of_FilterNameIgnoreCase_6() { return &___FilterNameIgnoreCase_6; }
	inline void set_FilterNameIgnoreCase_6(MemberFilter_t4208935459 * value)
	{
		___FilterNameIgnoreCase_6 = value;
		Il2CppCodeGenWriteBarrier((&___FilterNameIgnoreCase_6), value);
	}

	inline static int32_t get_offset_of_Missing_7() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Missing_7)); }
	inline RuntimeObject * get_Missing_7() const { return ___Missing_7; }
	inline RuntimeObject ** get_address_of_Missing_7() { return &___Missing_7; }
	inline void set_Missing_7(RuntimeObject * value)
	{
		___Missing_7 = value;
		Il2CppCodeGenWriteBarrier((&___Missing_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TYPE_T_H
#ifndef TEXTURE_T85561421_H
#define TEXTURE_T85561421_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Texture
struct  Texture_t85561421  : public Object_t1502412432
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTURE_T85561421_H
#ifndef PHYSICMATERIAL_T1617475977_H
#define PHYSICMATERIAL_T1617475977_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PhysicMaterial
struct  PhysicMaterial_t1617475977  : public Object_t1502412432
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PHYSICMATERIAL_T1617475977_H
#ifndef MULTICASTDELEGATE_T4139169907_H
#define MULTICASTDELEGATE_T4139169907_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t4139169907  : public Delegate_t537306192
{
public:
	// System.MulticastDelegate System.MulticastDelegate::prev
	MulticastDelegate_t4139169907 * ___prev_9;
	// System.MulticastDelegate System.MulticastDelegate::kpm_next
	MulticastDelegate_t4139169907 * ___kpm_next_10;

public:
	inline static int32_t get_offset_of_prev_9() { return static_cast<int32_t>(offsetof(MulticastDelegate_t4139169907, ___prev_9)); }
	inline MulticastDelegate_t4139169907 * get_prev_9() const { return ___prev_9; }
	inline MulticastDelegate_t4139169907 ** get_address_of_prev_9() { return &___prev_9; }
	inline void set_prev_9(MulticastDelegate_t4139169907 * value)
	{
		___prev_9 = value;
		Il2CppCodeGenWriteBarrier((&___prev_9), value);
	}

	inline static int32_t get_offset_of_kpm_next_10() { return static_cast<int32_t>(offsetof(MulticastDelegate_t4139169907, ___kpm_next_10)); }
	inline MulticastDelegate_t4139169907 * get_kpm_next_10() const { return ___kpm_next_10; }
	inline MulticastDelegate_t4139169907 ** get_address_of_kpm_next_10() { return &___kpm_next_10; }
	inline void set_kpm_next_10(MulticastDelegate_t4139169907 * value)
	{
		___kpm_next_10 = value;
		Il2CppCodeGenWriteBarrier((&___kpm_next_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MULTICASTDELEGATE_T4139169907_H
#ifndef MINMAXCURVE_T2961159436_H
#define MINMAXCURVE_T2961159436_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ParticleSystem/MinMaxCurve
struct  MinMaxCurve_t2961159436 
{
public:
	// UnityEngine.ParticleSystemCurveMode UnityEngine.ParticleSystem/MinMaxCurve::m_Mode
	int32_t ___m_Mode_0;
	// System.Single UnityEngine.ParticleSystem/MinMaxCurve::m_CurveMultiplier
	float ___m_CurveMultiplier_1;
	// UnityEngine.AnimationCurve UnityEngine.ParticleSystem/MinMaxCurve::m_CurveMin
	AnimationCurve_t2184836714 * ___m_CurveMin_2;
	// UnityEngine.AnimationCurve UnityEngine.ParticleSystem/MinMaxCurve::m_CurveMax
	AnimationCurve_t2184836714 * ___m_CurveMax_3;
	// System.Single UnityEngine.ParticleSystem/MinMaxCurve::m_ConstantMin
	float ___m_ConstantMin_4;
	// System.Single UnityEngine.ParticleSystem/MinMaxCurve::m_ConstantMax
	float ___m_ConstantMax_5;

public:
	inline static int32_t get_offset_of_m_Mode_0() { return static_cast<int32_t>(offsetof(MinMaxCurve_t2961159436, ___m_Mode_0)); }
	inline int32_t get_m_Mode_0() const { return ___m_Mode_0; }
	inline int32_t* get_address_of_m_Mode_0() { return &___m_Mode_0; }
	inline void set_m_Mode_0(int32_t value)
	{
		___m_Mode_0 = value;
	}

	inline static int32_t get_offset_of_m_CurveMultiplier_1() { return static_cast<int32_t>(offsetof(MinMaxCurve_t2961159436, ___m_CurveMultiplier_1)); }
	inline float get_m_CurveMultiplier_1() const { return ___m_CurveMultiplier_1; }
	inline float* get_address_of_m_CurveMultiplier_1() { return &___m_CurveMultiplier_1; }
	inline void set_m_CurveMultiplier_1(float value)
	{
		___m_CurveMultiplier_1 = value;
	}

	inline static int32_t get_offset_of_m_CurveMin_2() { return static_cast<int32_t>(offsetof(MinMaxCurve_t2961159436, ___m_CurveMin_2)); }
	inline AnimationCurve_t2184836714 * get_m_CurveMin_2() const { return ___m_CurveMin_2; }
	inline AnimationCurve_t2184836714 ** get_address_of_m_CurveMin_2() { return &___m_CurveMin_2; }
	inline void set_m_CurveMin_2(AnimationCurve_t2184836714 * value)
	{
		___m_CurveMin_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_CurveMin_2), value);
	}

	inline static int32_t get_offset_of_m_CurveMax_3() { return static_cast<int32_t>(offsetof(MinMaxCurve_t2961159436, ___m_CurveMax_3)); }
	inline AnimationCurve_t2184836714 * get_m_CurveMax_3() const { return ___m_CurveMax_3; }
	inline AnimationCurve_t2184836714 ** get_address_of_m_CurveMax_3() { return &___m_CurveMax_3; }
	inline void set_m_CurveMax_3(AnimationCurve_t2184836714 * value)
	{
		___m_CurveMax_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_CurveMax_3), value);
	}

	inline static int32_t get_offset_of_m_ConstantMin_4() { return static_cast<int32_t>(offsetof(MinMaxCurve_t2961159436, ___m_ConstantMin_4)); }
	inline float get_m_ConstantMin_4() const { return ___m_ConstantMin_4; }
	inline float* get_address_of_m_ConstantMin_4() { return &___m_ConstantMin_4; }
	inline void set_m_ConstantMin_4(float value)
	{
		___m_ConstantMin_4 = value;
	}

	inline static int32_t get_offset_of_m_ConstantMax_5() { return static_cast<int32_t>(offsetof(MinMaxCurve_t2961159436, ___m_ConstantMax_5)); }
	inline float get_m_ConstantMax_5() const { return ___m_ConstantMax_5; }
	inline float* get_address_of_m_ConstantMax_5() { return &___m_ConstantMax_5; }
	inline void set_m_ConstantMax_5(float value)
	{
		___m_ConstantMax_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.ParticleSystem/MinMaxCurve
struct MinMaxCurve_t2961159436_marshaled_pinvoke
{
	int32_t ___m_Mode_0;
	float ___m_CurveMultiplier_1;
	AnimationCurve_t2184836714_marshaled_pinvoke ___m_CurveMin_2;
	AnimationCurve_t2184836714_marshaled_pinvoke ___m_CurveMax_3;
	float ___m_ConstantMin_4;
	float ___m_ConstantMax_5;
};
// Native definition for COM marshalling of UnityEngine.ParticleSystem/MinMaxCurve
struct MinMaxCurve_t2961159436_marshaled_com
{
	int32_t ___m_Mode_0;
	float ___m_CurveMultiplier_1;
	AnimationCurve_t2184836714_marshaled_com* ___m_CurveMin_2;
	AnimationCurve_t2184836714_marshaled_com* ___m_CurveMax_3;
	float ___m_ConstantMin_4;
	float ___m_ConstantMax_5;
};
#endif // MINMAXCURVE_T2961159436_H
#ifndef QUALITYSETTINGS_T1628298989_H
#define QUALITYSETTINGS_T1628298989_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.QualitySettings
struct  QualitySettings_t1628298989  : public Object_t1502412432
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUALITYSETTINGS_T1628298989_H
#ifndef PARTICLESYSTEM_T1912383059_H
#define PARTICLESYSTEM_T1912383059_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ParticleSystem
struct  ParticleSystem_t1912383059  : public Component_t4087199522
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PARTICLESYSTEM_T1912383059_H
#ifndef BEHAVIOUR_T363748010_H
#define BEHAVIOUR_T363748010_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_t363748010  : public Component_t4087199522
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_T363748010_H
#ifndef RENDERER_T1303575294_H
#define RENDERER_T1303575294_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Renderer
struct  Renderer_t1303575294  : public Component_t4087199522
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RENDERER_T1303575294_H
#ifndef TRANSFORM_T3316442598_H
#define TRANSFORM_T3316442598_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Transform
struct  Transform_t3316442598  : public Component_t4087199522
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRANSFORM_T3316442598_H
#ifndef ASYNCCALLBACK_T1057242265_H
#define ASYNCCALLBACK_T1057242265_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.AsyncCallback
struct  AsyncCallback_t1057242265  : public MulticastDelegate_t4139169907
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASYNCCALLBACK_T1057242265_H
#ifndef PLAYABLEASSET_T2449544405_H
#define PLAYABLEASSET_T2449544405_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Playables.PlayableAsset
struct  PlayableAsset_t2449544405  : public ScriptableObject_t3635579074
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYABLEASSET_T2449544405_H
#ifndef UNITYACTION_2_T121587842_H
#define UNITYACTION_2_T121587842_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityAction`2<UnityEngine.SceneManagement.Scene,UnityEngine.SceneManagement.Scene>
struct  UnityAction_2_t121587842  : public MulticastDelegate_t4139169907
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYACTION_2_T121587842_H
#ifndef UNITYACTION_1_T3896475417_H
#define UNITYACTION_1_T3896475417_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityAction`1<UnityEngine.SceneManagement.Scene>
struct  UnityAction_1_t3896475417  : public MulticastDelegate_t4139169907
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYACTION_1_T3896475417_H
#ifndef UNITYACTION_2_T2064023076_H
#define UNITYACTION_2_T2064023076_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityAction`2<UnityEngine.SceneManagement.Scene,UnityEngine.SceneManagement.LoadSceneMode>
struct  UnityAction_2_t2064023076  : public MulticastDelegate_t4139169907
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYACTION_2_T2064023076_H
#ifndef COLLIDER_T2301021182_H
#define COLLIDER_T2301021182_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Collider
struct  Collider_t2301021182  : public Component_t4087199522
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLLIDER_T2301021182_H
#ifndef RIGIDBODY_T2492269564_H
#define RIGIDBODY_T2492269564_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rigidbody
struct  Rigidbody_t2492269564  : public Component_t4087199522
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RIGIDBODY_T2492269564_H
#ifndef RIGIDBODY2D_T252970942_H
#define RIGIDBODY2D_T252970942_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rigidbody2D
struct  Rigidbody2D_t252970942  : public Component_t4087199522
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RIGIDBODY2D_T252970942_H
#ifndef UPDATEDEVENTHANDLER_T729869569_H
#define UPDATEDEVENTHANDLER_T729869569_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.RemoteSettings/UpdatedEventHandler
struct  UpdatedEventHandler_t729869569  : public MulticastDelegate_t4139169907
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UPDATEDEVENTHANDLER_T729869569_H
#ifndef REAPPLYDRIVENPROPERTIES_T565817470_H
#define REAPPLYDRIVENPROPERTIES_T565817470_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.RectTransform/ReapplyDrivenProperties
struct  ReapplyDrivenProperties_t565817470  : public MulticastDelegate_t4139169907
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REAPPLYDRIVENPROPERTIES_T565817470_H
#ifndef RENDERTEXTURE_T2315324624_H
#define RENDERTEXTURE_T2315324624_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.RenderTexture
struct  RenderTexture_t2315324624  : public Texture_t85561421
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RENDERTEXTURE_T2315324624_H
#ifndef CANVAS_T3744170841_H
#define CANVAS_T3744170841_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Canvas
struct  Canvas_t3744170841  : public Behaviour_t363748010
{
public:

public:
};

struct Canvas_t3744170841_StaticFields
{
public:
	// UnityEngine.Canvas/WillRenderCanvases UnityEngine.Canvas::willRenderCanvases
	WillRenderCanvases_t1524812347 * ___willRenderCanvases_2;

public:
	inline static int32_t get_offset_of_willRenderCanvases_2() { return static_cast<int32_t>(offsetof(Canvas_t3744170841_StaticFields, ___willRenderCanvases_2)); }
	inline WillRenderCanvases_t1524812347 * get_willRenderCanvases_2() const { return ___willRenderCanvases_2; }
	inline WillRenderCanvases_t1524812347 ** get_address_of_willRenderCanvases_2() { return &___willRenderCanvases_2; }
	inline void set_willRenderCanvases_2(WillRenderCanvases_t1524812347 * value)
	{
		___willRenderCanvases_2 = value;
		Il2CppCodeGenWriteBarrier((&___willRenderCanvases_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CANVAS_T3744170841_H
#ifndef COLLIDER2D_T1623106781_H
#define COLLIDER2D_T1623106781_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Collider2D
struct  Collider2D_t1623106781  : public Behaviour_t363748010
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLLIDER2D_T1623106781_H
#ifndef CAMERA_T3175186167_H
#define CAMERA_T3175186167_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Camera
struct  Camera_t3175186167  : public Behaviour_t363748010
{
public:

public:
};

struct Camera_t3175186167_StaticFields
{
public:
	// UnityEngine.Camera/CameraCallback UnityEngine.Camera::onPreCull
	CameraCallback_t832885096 * ___onPreCull_2;
	// UnityEngine.Camera/CameraCallback UnityEngine.Camera::onPreRender
	CameraCallback_t832885096 * ___onPreRender_3;
	// UnityEngine.Camera/CameraCallback UnityEngine.Camera::onPostRender
	CameraCallback_t832885096 * ___onPostRender_4;

public:
	inline static int32_t get_offset_of_onPreCull_2() { return static_cast<int32_t>(offsetof(Camera_t3175186167_StaticFields, ___onPreCull_2)); }
	inline CameraCallback_t832885096 * get_onPreCull_2() const { return ___onPreCull_2; }
	inline CameraCallback_t832885096 ** get_address_of_onPreCull_2() { return &___onPreCull_2; }
	inline void set_onPreCull_2(CameraCallback_t832885096 * value)
	{
		___onPreCull_2 = value;
		Il2CppCodeGenWriteBarrier((&___onPreCull_2), value);
	}

	inline static int32_t get_offset_of_onPreRender_3() { return static_cast<int32_t>(offsetof(Camera_t3175186167_StaticFields, ___onPreRender_3)); }
	inline CameraCallback_t832885096 * get_onPreRender_3() const { return ___onPreRender_3; }
	inline CameraCallback_t832885096 ** get_address_of_onPreRender_3() { return &___onPreRender_3; }
	inline void set_onPreRender_3(CameraCallback_t832885096 * value)
	{
		___onPreRender_3 = value;
		Il2CppCodeGenWriteBarrier((&___onPreRender_3), value);
	}

	inline static int32_t get_offset_of_onPostRender_4() { return static_cast<int32_t>(offsetof(Camera_t3175186167_StaticFields, ___onPostRender_4)); }
	inline CameraCallback_t832885096 * get_onPostRender_4() const { return ___onPostRender_4; }
	inline CameraCallback_t832885096 ** get_address_of_onPostRender_4() { return &___onPostRender_4; }
	inline void set_onPostRender_4(CameraCallback_t832885096 * value)
	{
		___onPostRender_4 = value;
		Il2CppCodeGenWriteBarrier((&___onPostRender_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAMERA_T3175186167_H
#ifndef RECTTRANSFORM_T3962994911_H
#define RECTTRANSFORM_T3962994911_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.RectTransform
struct  RectTransform_t3962994911  : public Transform_t3316442598
{
public:

public:
};

struct RectTransform_t3962994911_StaticFields
{
public:
	// UnityEngine.RectTransform/ReapplyDrivenProperties UnityEngine.RectTransform::reapplyDrivenProperties
	ReapplyDrivenProperties_t565817470 * ___reapplyDrivenProperties_2;

public:
	inline static int32_t get_offset_of_reapplyDrivenProperties_2() { return static_cast<int32_t>(offsetof(RectTransform_t3962994911_StaticFields, ___reapplyDrivenProperties_2)); }
	inline ReapplyDrivenProperties_t565817470 * get_reapplyDrivenProperties_2() const { return ___reapplyDrivenProperties_2; }
	inline ReapplyDrivenProperties_t565817470 ** get_address_of_reapplyDrivenProperties_2() { return &___reapplyDrivenProperties_2; }
	inline void set_reapplyDrivenProperties_2(ReapplyDrivenProperties_t565817470 * value)
	{
		___reapplyDrivenProperties_2 = value;
		Il2CppCodeGenWriteBarrier((&___reapplyDrivenProperties_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RECTTRANSFORM_T3962994911_H
#ifndef GUIELEMENT_T4082881042_H
#define GUIELEMENT_T4082881042_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.GUIElement
struct  GUIElement_t4082881042  : public Behaviour_t363748010
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GUIELEMENT_T4082881042_H
#ifndef NETWORKVIEW_T3440562342_H
#define NETWORKVIEW_T3440562342_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.NetworkView
struct  NetworkView_t3440562342  : public Behaviour_t363748010
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NETWORKVIEW_T3440562342_H
#ifndef GUILAYER_T4095959459_H
#define GUILAYER_T4095959459_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.GUILayer
struct  GUILayer_t4095959459  : public Behaviour_t363748010
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GUILAYER_T4095959459_H
// UnityEngine.Object[]
struct ObjectU5BU5D_t760538417  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Object_t1502412432 * m_Items[1];

public:
	inline Object_t1502412432 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Object_t1502412432 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Object_t1502412432 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline Object_t1502412432 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Object_t1502412432 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Object_t1502412432 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UnityEngine.RaycastHit[]
struct RaycastHitU5BU5D_t4180454940  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) RaycastHit_t2327766465  m_Items[1];

public:
	inline RaycastHit_t2327766465  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline RaycastHit_t2327766465 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, RaycastHit_t2327766465  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline RaycastHit_t2327766465  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline RaycastHit_t2327766465 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, RaycastHit_t2327766465  value)
	{
		m_Items[index] = value;
	}
};
// System.Object[]
struct ObjectU5BU5D_t3270211303  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) RuntimeObject * m_Items[1];

public:
	inline RuntimeObject * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline RuntimeObject ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, RuntimeObject * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline RuntimeObject * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline RuntimeObject ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, RuntimeObject * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UnityEngine.Playables.PlayableBinding[]
struct PlayableBindingU5BU5D_t3242831679  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) PlayableBinding_t3706191642  m_Items[1];

public:
	inline PlayableBinding_t3706191642  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline PlayableBinding_t3706191642 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, PlayableBinding_t3706191642  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline PlayableBinding_t3706191642  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline PlayableBinding_t3706191642 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, PlayableBinding_t3706191642  value)
	{
		m_Items[index] = value;
	}
};
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t981680887  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Vector3_t2903530434  m_Items[1];

public:
	inline Vector3_t2903530434  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Vector3_t2903530434 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Vector3_t2903530434  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline Vector3_t2903530434  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Vector3_t2903530434 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Vector3_t2903530434  value)
	{
		m_Items[index] = value;
	}
};
// UnityEngine.Material[]
struct MaterialU5BU5D_t1437145498  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Material_t1079520667 * m_Items[1];

public:
	inline Material_t1079520667 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Material_t1079520667 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Material_t1079520667 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline Material_t1079520667 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Material_t1079520667 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Material_t1079520667 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Int32[]
struct Int32U5BU5D_t2324750880  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) int32_t m_Items[1];

public:
	inline int32_t GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline int32_t* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, int32_t value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline int32_t GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline int32_t* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, int32_t value)
	{
		m_Items[index] = value;
	}
};
// UnityEngine.Camera[]
struct CameraU5BU5D_t1519774542  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Camera_t3175186167 * m_Items[1];

public:
	inline Camera_t3175186167 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Camera_t3175186167 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Camera_t3175186167 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline Camera_t3175186167 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Camera_t3175186167 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Camera_t3175186167 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UnityEngine.SendMouseEvents/HitInfo[]
struct HitInfoU5BU5D_t1076684586  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) HitInfo_t2000020299  m_Items[1];

public:
	inline HitInfo_t2000020299  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline HitInfo_t2000020299 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, HitInfo_t2000020299  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline HitInfo_t2000020299  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline HitInfo_t2000020299 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, HitInfo_t2000020299  value)
	{
		m_Items[index] = value;
	}
};
// System.Reflection.ParameterModifier[]
struct ParameterModifierU5BU5D_t3421420242  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) ParameterModifier_t2577558211  m_Items[1];

public:
	inline ParameterModifier_t2577558211  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline ParameterModifier_t2577558211 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, ParameterModifier_t2577558211  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline ParameterModifier_t2577558211  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline ParameterModifier_t2577558211 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, ParameterModifier_t2577558211  value)
	{
		m_Items[index] = value;
	}
};
// System.String[]
struct StringU5BU5D_t2298632947  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) String_t* m_Items[1];

public:
	inline String_t* GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline String_t** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, String_t* value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline String_t* GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline String_t** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, String_t* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};

extern "C" void AnimationCurve_t2184836714_marshal_pinvoke(const AnimationCurve_t2184836714& unmarshaled, AnimationCurve_t2184836714_marshaled_pinvoke& marshaled);
extern "C" void AnimationCurve_t2184836714_marshal_pinvoke_back(const AnimationCurve_t2184836714_marshaled_pinvoke& marshaled, AnimationCurve_t2184836714& unmarshaled);
extern "C" void AnimationCurve_t2184836714_marshal_pinvoke_cleanup(AnimationCurve_t2184836714_marshaled_pinvoke& marshaled);
extern "C" void AnimationCurve_t2184836714_marshal_com(const AnimationCurve_t2184836714& unmarshaled, AnimationCurve_t2184836714_marshaled_com& marshaled);
extern "C" void AnimationCurve_t2184836714_marshal_com_back(const AnimationCurve_t2184836714_marshaled_com& marshaled, AnimationCurve_t2184836714& unmarshaled);
extern "C" void AnimationCurve_t2184836714_marshal_com_cleanup(AnimationCurve_t2184836714_marshaled_com& marshaled);
extern "C" void Object_t1502412432_marshal_pinvoke(const Object_t1502412432& unmarshaled, Object_t1502412432_marshaled_pinvoke& marshaled);
extern "C" void Object_t1502412432_marshal_pinvoke_back(const Object_t1502412432_marshaled_pinvoke& marshaled, Object_t1502412432& unmarshaled);
extern "C" void Object_t1502412432_marshal_pinvoke_cleanup(Object_t1502412432_marshaled_pinvoke& marshaled);
extern "C" void Object_t1502412432_marshal_com(const Object_t1502412432& unmarshaled, Object_t1502412432_marshaled_com& marshaled);
extern "C" void Object_t1502412432_marshal_com_back(const Object_t1502412432_marshaled_com& marshaled, Object_t1502412432& unmarshaled);
extern "C" void Object_t1502412432_marshal_com_cleanup(Object_t1502412432_marshaled_com& marshaled);

// System.Void System.Collections.Generic.List`1<System.Object>::.ctor()
extern "C"  void List_1__ctor_m3059358503_gshared (List_1_t877107497 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Playables.PlayableExtensions::SetInputCount<UnityEngine.Playables.Playable>(U,System.Int32)
extern "C"  void PlayableExtensions_SetInputCount_TisPlayable_t735025802_m769118885_gshared (RuntimeObject * __this /* static, unused */, Playable_t735025802  ___playable0, int32_t ___value1, const RuntimeMethod* method);
// System.Boolean UnityEngine.Playables.PlayableOutputHandle::IsPlayableOutputOfType<UnityEngine.Playables.ScriptPlayableOutput>()
extern "C"  bool PlayableOutputHandle_IsPlayableOutputOfType_TisScriptPlayableOutput_t49585950_m1665456405_gshared (PlayableOutputHandle_t2200222555 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Events.UnityAction`2<UnityEngine.SceneManagement.Scene,UnityEngine.SceneManagement.LoadSceneMode>::Invoke(T0,T1)
extern "C"  void UnityAction_2_Invoke_m2774749000_gshared (UnityAction_2_t2064023076 * __this, Scene_t348657329  p0, int32_t p1, const RuntimeMethod* method);
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.SceneManagement.Scene>::Invoke(T0)
extern "C"  void UnityAction_1_Invoke_m2393581145_gshared (UnityAction_1_t3896475417 * __this, Scene_t348657329  p0, const RuntimeMethod* method);
// System.Void UnityEngine.Events.UnityAction`2<UnityEngine.SceneManagement.Scene,UnityEngine.SceneManagement.Scene>::Invoke(T0,T1)
extern "C"  void UnityAction_2_Invoke_m3185242217_gshared (UnityAction_2_t121587842 * __this, Scene_t348657329  p0, Scene_t348657329  p1, const RuntimeMethod* method);
// T UnityEngine.Component::GetComponent<System.Object>()
extern "C"  RuntimeObject * Component_GetComponent_TisRuntimeObject_m2412552191_gshared (Component_t4087199522 * __this, const RuntimeMethod* method);

// UnityEngine.NetworkView UnityEngine.NetworkView::INTERNAL_CALL_Find(UnityEngine.NetworkViewID&)
extern "C"  NetworkView_t3440562342 * NetworkView_INTERNAL_CALL_Find_m2156794560 (RuntimeObject * __this /* static, unused */, NetworkViewID_t1404889299 * ___viewID0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.NetworkViewID::INTERNAL_get_unassigned(UnityEngine.NetworkViewID&)
extern "C"  void NetworkViewID_INTERNAL_get_unassigned_m1006212221 (RuntimeObject * __this /* static, unused */, NetworkViewID_t1404889299 * ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.NetworkViewID::INTERNAL_CALL_Internal_IsMine(UnityEngine.NetworkViewID&)
extern "C"  bool NetworkViewID_INTERNAL_CALL_Internal_IsMine_m265888996 (RuntimeObject * __this /* static, unused */, NetworkViewID_t1404889299 * ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.NetworkViewID::INTERNAL_CALL_Internal_GetOwner(UnityEngine.NetworkViewID&,UnityEngine.NetworkPlayer&)
extern "C"  void NetworkViewID_INTERNAL_CALL_Internal_GetOwner_m3916819407 (RuntimeObject * __this /* static, unused */, NetworkViewID_t1404889299 * ___value0, NetworkPlayer_t4008288529 * ___player1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.NetworkViewID::INTERNAL_CALL_Internal_GetString(UnityEngine.NetworkViewID&)
extern "C"  String_t* NetworkViewID_INTERNAL_CALL_Internal_GetString_m4269197340 (RuntimeObject * __this /* static, unused */, NetworkViewID_t1404889299 * ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.NetworkViewID::INTERNAL_CALL_Internal_Compare(UnityEngine.NetworkViewID&,UnityEngine.NetworkViewID&)
extern "C"  bool NetworkViewID_INTERNAL_CALL_Internal_Compare_m2042094818 (RuntimeObject * __this /* static, unused */, NetworkViewID_t1404889299 * ___lhs0, NetworkViewID_t1404889299 * ___rhs1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.NetworkViewID::Internal_Compare(UnityEngine.NetworkViewID,UnityEngine.NetworkViewID)
extern "C"  bool NetworkViewID_Internal_Compare_m2810921315 (RuntimeObject * __this /* static, unused */, NetworkViewID_t1404889299  ___lhs0, NetworkViewID_t1404889299  ___rhs1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.NetworkViewID::GetHashCode()
extern "C"  int32_t NetworkViewID_GetHashCode_m1329632673 (NetworkViewID_t1404889299 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.NetworkViewID::Equals(System.Object)
extern "C"  bool NetworkViewID_Equals_m1195240327 (NetworkViewID_t1404889299 * __this, RuntimeObject * ___other0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.NetworkViewID::Internal_IsMine(UnityEngine.NetworkViewID)
extern "C"  bool NetworkViewID_Internal_IsMine_m1453090357 (RuntimeObject * __this /* static, unused */, NetworkViewID_t1404889299  ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.NetworkViewID::get_isMine()
extern "C"  bool NetworkViewID_get_isMine_m35938188 (NetworkViewID_t1404889299 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.NetworkViewID::Internal_GetOwner(UnityEngine.NetworkViewID,UnityEngine.NetworkPlayer&)
extern "C"  void NetworkViewID_Internal_GetOwner_m2176248212 (RuntimeObject * __this /* static, unused */, NetworkViewID_t1404889299  ___value0, NetworkPlayer_t4008288529 * ___player1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.NetworkPlayer UnityEngine.NetworkViewID::get_owner()
extern "C"  NetworkPlayer_t4008288529  NetworkViewID_get_owner_m2196279096 (NetworkViewID_t1404889299 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.NetworkViewID::Internal_GetString(UnityEngine.NetworkViewID)
extern "C"  String_t* NetworkViewID_Internal_GetString_m963601455 (RuntimeObject * __this /* static, unused */, NetworkViewID_t1404889299  ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.NetworkViewID::ToString()
extern "C"  String_t* NetworkViewID_ToString_m2913258028 (NetworkViewID_t1404889299 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Object::.ctor()
extern "C"  void Object__ctor_m2906060009 (RuntimeObject * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Object UnityEngine.Object::INTERNAL_CALL_Internal_InstantiateSingle(UnityEngine.Object,UnityEngine.Vector3&,UnityEngine.Quaternion&)
extern "C"  Object_t1502412432 * Object_INTERNAL_CALL_Internal_InstantiateSingle_m133179633 (RuntimeObject * __this /* static, unused */, Object_t1502412432 * ___data0, Vector3_t2903530434 * ___pos1, Quaternion_t754065749 * ___rot2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Object UnityEngine.Object::INTERNAL_CALL_Internal_InstantiateSingleWithParent(UnityEngine.Object,UnityEngine.Transform,UnityEngine.Vector3&,UnityEngine.Quaternion&)
extern "C"  Object_t1502412432 * Object_INTERNAL_CALL_Internal_InstantiateSingleWithParent_m3758362270 (RuntimeObject * __this /* static, unused */, Object_t1502412432 * ___data0, Transform_t3316442598 * ___parent1, Vector3_t2903530434 * ___pos2, Quaternion_t754065749 * ___rot3, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Object::Destroy(UnityEngine.Object,System.Single)
extern "C"  void Object_Destroy_m3194071194 (RuntimeObject * __this /* static, unused */, Object_t1502412432 * ___obj0, float ___t1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Object::DestroyImmediate(UnityEngine.Object,System.Boolean)
extern "C"  void Object_DestroyImmediate_m3321698354 (RuntimeObject * __this /* static, unused */, Object_t1502412432 * ___obj0, bool ___allowDestroyingAssets1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Object::DestroyObject(UnityEngine.Object,System.Single)
extern "C"  void Object_DestroyObject_m2199032474 (RuntimeObject * __this /* static, unused */, Object_t1502412432 * ___obj0, float ___t1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.IntPtr::op_Equality(System.IntPtr,System.IntPtr)
extern "C"  bool IntPtr_op_Equality_m1136306456 (RuntimeObject * __this /* static, unused */, IntPtr_t p0, IntPtr_t p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Object::GetOffsetOfInstanceIDInCPlusPlusObject()
extern "C"  int32_t Object_GetOffsetOfInstanceIDInCPlusPlusObject_m2713419181 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int64 System.IntPtr::ToInt64()
extern "C"  int64_t IntPtr_ToInt64_m616010533 (IntPtr_t* __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.IntPtr::.ctor(System.Int64)
extern "C"  void IntPtr__ctor_m2295693947 (IntPtr_t* __this, int64_t p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void* System.IntPtr::op_Explicit(System.IntPtr)
extern "C"  void* IntPtr_op_Explicit_m532509050 (RuntimeObject * __this /* static, unused */, IntPtr_t p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Object::GetHashCode()
extern "C"  int32_t Object_GetHashCode_m203225167 (RuntimeObject * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Object::op_Equality(UnityEngine.Object,UnityEngine.Object)
extern "C"  bool Object_op_Equality_m2342070775 (RuntimeObject * __this /* static, unused */, Object_t1502412432 * ___x0, Object_t1502412432 * ___y1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Object::CompareBaseObjects(UnityEngine.Object,UnityEngine.Object)
extern "C"  bool Object_CompareBaseObjects_m3108723731 (RuntimeObject * __this /* static, unused */, Object_t1502412432 * ___lhs0, Object_t1502412432 * ___rhs1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Object::IsNativeObjectAlive(UnityEngine.Object)
extern "C"  bool Object_IsNativeObjectAlive_m3228292766 (RuntimeObject * __this /* static, unused */, Object_t1502412432 * ___o0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Object::ReferenceEquals(System.Object,System.Object)
extern "C"  bool Object_ReferenceEquals_m2014193643 (RuntimeObject * __this /* static, unused */, RuntimeObject * p0, RuntimeObject * p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.IntPtr UnityEngine.Object::GetCachedPtr()
extern "C"  IntPtr_t Object_GetCachedPtr_m627724031 (Object_t1502412432 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.IntPtr::op_Inequality(System.IntPtr,System.IntPtr)
extern "C"  bool IntPtr_op_Inequality_m1842201359 (RuntimeObject * __this /* static, unused */, IntPtr_t p0, IntPtr_t p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Object::CheckNullArgument(System.Object,System.String)
extern "C"  void Object_CheckNullArgument_m2850461954 (RuntimeObject * __this /* static, unused */, RuntimeObject * ___arg0, String_t* ___message1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.ArgumentException::.ctor(System.String)
extern "C"  void ArgumentException__ctor_m1186658332 (ArgumentException_t4028401650 * __this, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Object UnityEngine.Object::Internal_InstantiateSingle(UnityEngine.Object,UnityEngine.Vector3,UnityEngine.Quaternion)
extern "C"  Object_t1502412432 * Object_Internal_InstantiateSingle_m664294789 (RuntimeObject * __this /* static, unused */, Object_t1502412432 * ___data0, Vector3_t2903530434  ___pos1, Quaternion_t754065749  ___rot2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Object UnityEngine.Object::Internal_InstantiateSingleWithParent(UnityEngine.Object,UnityEngine.Transform,UnityEngine.Vector3,UnityEngine.Quaternion)
extern "C"  Object_t1502412432 * Object_Internal_InstantiateSingleWithParent_m3862473257 (RuntimeObject * __this /* static, unused */, Object_t1502412432 * ___data0, Transform_t3316442598 * ___parent1, Vector3_t2903530434  ___pos2, Quaternion_t754065749  ___rot3, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Object UnityEngine.Object::Internal_CloneSingle(UnityEngine.Object)
extern "C"  Object_t1502412432 * Object_Internal_CloneSingle_m2326967574 (RuntimeObject * __this /* static, unused */, Object_t1502412432 * ___data0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Object UnityEngine.Object::Instantiate(UnityEngine.Object,UnityEngine.Transform,System.Boolean)
extern "C"  Object_t1502412432 * Object_Instantiate_m914959561 (RuntimeObject * __this /* static, unused */, Object_t1502412432 * ___original0, Transform_t3316442598 * ___parent1, bool ___instantiateInWorldSpace2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Object UnityEngine.Object::Internal_CloneSingleWithParent(UnityEngine.Object,UnityEngine.Transform,System.Boolean)
extern "C"  Object_t1502412432 * Object_Internal_CloneSingleWithParent_m973191133 (RuntimeObject * __this /* static, unused */, Object_t1502412432 * ___data0, Transform_t3316442598 * ___parent1, bool ___worldPositionStays2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Object[] UnityEngine.Object::FindObjectsOfType(System.Type)
extern "C"  ObjectU5BU5D_t760538417* Object_FindObjectsOfType_m1430252051 (RuntimeObject * __this /* static, unused */, Type_t * ___type0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ParticleSystem/MainModule::.ctor(UnityEngine.ParticleSystem)
extern "C"  void MainModule__ctor_m2253476806 (MainModule_t2858435356 * __this, ParticleSystem_t1912383059 * ___particleSystem0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ParticleSystem/EmissionModule::.ctor(UnityEngine.ParticleSystem)
extern "C"  void EmissionModule__ctor_m1223807977 (EmissionModule_t216550349 * __this, ParticleSystem_t1912383059 * ___particleSystem0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ParticleSystem/EmissionModule::SetEnabled(UnityEngine.ParticleSystem,System.Boolean)
extern "C"  void EmissionModule_SetEnabled_m1349267330 (RuntimeObject * __this /* static, unused */, ParticleSystem_t1912383059 * ___system0, bool ___value1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ParticleSystem/EmissionModule::set_enabled(System.Boolean)
extern "C"  void EmissionModule_set_enabled_m2770978577 (EmissionModule_t216550349 * __this, bool ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ParticleSystem/MainModule::GetStartLifetime(UnityEngine.ParticleSystem,UnityEngine.ParticleSystem/MinMaxCurve&)
extern "C"  void MainModule_GetStartLifetime_m1262381546 (RuntimeObject * __this /* static, unused */, ParticleSystem_t1912383059 * ___system0, MinMaxCurve_t2961159436 * ___curve1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.ParticleSystem/MinMaxCurve UnityEngine.ParticleSystem/MainModule::get_startLifetime()
extern "C"  MinMaxCurve_t2961159436  MainModule_get_startLifetime_m2433047371 (MainModule_t2858435356 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AnimationCurve::.ctor()
extern "C"  void AnimationCurve__ctor_m1175796134 (AnimationCurve_t2184836714 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.ParticleSystem/MinMaxCurve::get_constant()
extern "C"  float MinMaxCurve_get_constant_m2411819109 (MinMaxCurve_t2961159436 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Physics::INTERNAL_get_gravity(UnityEngine.Vector3&)
extern "C"  void Physics_INTERNAL_get_gravity_m4188242439 (RuntimeObject * __this /* static, unused */, Vector3_t2903530434 * ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Physics::Raycast(UnityEngine.Vector3,UnityEngine.Vector3,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern "C"  bool Physics_Raycast_m3039943821 (RuntimeObject * __this /* static, unused */, Vector3_t2903530434  ___origin0, Vector3_t2903530434  ___direction1, float ___maxDistance2, int32_t ___layerMask3, int32_t ___queryTriggerInteraction4, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Physics::Internal_RaycastTest(UnityEngine.Vector3,UnityEngine.Vector3,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern "C"  bool Physics_Internal_RaycastTest_m304866704 (RuntimeObject * __this /* static, unused */, Vector3_t2903530434  ___origin0, Vector3_t2903530434  ___direction1, float ___maxDistance2, int32_t ___layermask3, int32_t ___queryTriggerInteraction4, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Physics::Raycast(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.RaycastHit&,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern "C"  bool Physics_Raycast_m3270369209 (RuntimeObject * __this /* static, unused */, Vector3_t2903530434  ___origin0, Vector3_t2903530434  ___direction1, RaycastHit_t2327766465 * ___hitInfo2, float ___maxDistance3, int32_t ___layerMask4, int32_t ___queryTriggerInteraction5, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Physics::Internal_Raycast(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.RaycastHit&,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern "C"  bool Physics_Internal_Raycast_m1340688750 (RuntimeObject * __this /* static, unused */, Vector3_t2903530434  ___origin0, Vector3_t2903530434  ___direction1, RaycastHit_t2327766465 * ___hitInfo2, float ___maxDistance3, int32_t ___layermask4, int32_t ___queryTriggerInteraction5, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Physics::Raycast(UnityEngine.Ray,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern "C"  bool Physics_Raycast_m3725218301 (RuntimeObject * __this /* static, unused */, Ray_t3821377119  ___ray0, float ___maxDistance1, int32_t ___layerMask2, int32_t ___queryTriggerInteraction3, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Ray::get_origin()
extern "C"  Vector3_t2903530434  Ray_get_origin_m126064229 (Ray_t3821377119 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Ray::get_direction()
extern "C"  Vector3_t2903530434  Ray_get_direction_m3512869925 (Ray_t3821377119 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Physics::Raycast(UnityEngine.Ray,UnityEngine.RaycastHit&,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern "C"  bool Physics_Raycast_m3944844260 (RuntimeObject * __this /* static, unused */, Ray_t3821377119  ___ray0, RaycastHit_t2327766465 * ___hitInfo1, float ___maxDistance2, int32_t ___layerMask3, int32_t ___queryTriggerInteraction4, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.RaycastHit[] UnityEngine.Physics::RaycastAll(UnityEngine.Ray,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern "C"  RaycastHitU5BU5D_t4180454940* Physics_RaycastAll_m571393075 (RuntimeObject * __this /* static, unused */, Ray_t3821377119  ___ray0, float ___maxDistance1, int32_t ___layerMask2, int32_t ___queryTriggerInteraction3, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.RaycastHit[] UnityEngine.Physics::RaycastAll(UnityEngine.Vector3,UnityEngine.Vector3,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern "C"  RaycastHitU5BU5D_t4180454940* Physics_RaycastAll_m3392506883 (RuntimeObject * __this /* static, unused */, Vector3_t2903530434  ___origin0, Vector3_t2903530434  ___direction1, float ___maxDistance2, int32_t ___layermask3, int32_t ___queryTriggerInteraction4, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.RaycastHit[] UnityEngine.Physics::INTERNAL_CALL_RaycastAll(UnityEngine.Vector3&,UnityEngine.Vector3&,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern "C"  RaycastHitU5BU5D_t4180454940* Physics_INTERNAL_CALL_RaycastAll_m3160329193 (RuntimeObject * __this /* static, unused */, Vector3_t2903530434 * ___origin0, Vector3_t2903530434 * ___direction1, float ___maxDistance2, int32_t ___layermask3, int32_t ___queryTriggerInteraction4, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Physics::Internal_CapsuleCast(UnityEngine.Vector3,UnityEngine.Vector3,System.Single,UnityEngine.Vector3,UnityEngine.RaycastHit&,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern "C"  bool Physics_Internal_CapsuleCast_m1511147988 (RuntimeObject * __this /* static, unused */, Vector3_t2903530434  ___point10, Vector3_t2903530434  ___point21, float ___radius2, Vector3_t2903530434  ___direction3, RaycastHit_t2327766465 * ___hitInfo4, float ___maxDistance5, int32_t ___layermask6, int32_t ___queryTriggerInteraction7, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Physics::INTERNAL_CALL_Internal_Raycast(UnityEngine.Vector3&,UnityEngine.Vector3&,UnityEngine.RaycastHit&,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern "C"  bool Physics_INTERNAL_CALL_Internal_Raycast_m3189866168 (RuntimeObject * __this /* static, unused */, Vector3_t2903530434 * ___origin0, Vector3_t2903530434 * ___direction1, RaycastHit_t2327766465 * ___hitInfo2, float ___maxDistance3, int32_t ___layermask4, int32_t ___queryTriggerInteraction5, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Physics::INTERNAL_CALL_Internal_CapsuleCast(UnityEngine.Vector3&,UnityEngine.Vector3&,System.Single,UnityEngine.Vector3&,UnityEngine.RaycastHit&,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern "C"  bool Physics_INTERNAL_CALL_Internal_CapsuleCast_m1943493709 (RuntimeObject * __this /* static, unused */, Vector3_t2903530434 * ___point10, Vector3_t2903530434 * ___point21, float ___radius2, Vector3_t2903530434 * ___direction3, RaycastHit_t2327766465 * ___hitInfo4, float ___maxDistance5, int32_t ___layermask6, int32_t ___queryTriggerInteraction7, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Physics::INTERNAL_CALL_Internal_RaycastTest(UnityEngine.Vector3&,UnityEngine.Vector3&,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern "C"  bool Physics_INTERNAL_CALL_Internal_RaycastTest_m2228775343 (RuntimeObject * __this /* static, unused */, Vector3_t2903530434 * ___origin0, Vector3_t2903530434 * ___direction1, float ___maxDistance2, int32_t ___layermask3, int32_t ___queryTriggerInteraction4, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.List`1<UnityEngine.Rigidbody2D>::.ctor()
#define List_1__ctor_m1977615932(__this, method) ((  void (*) (List_1_t3403087445 *, const RuntimeMethod*))List_1__ctor_m3059358503_gshared)(__this, method)
// UnityEngine.Vector3 UnityEngine.Vector3::Normalize(UnityEngine.Vector3)
extern "C"  Vector3_t2903530434  Vector3_Normalize_m1717215557 (RuntimeObject * __this /* static, unused */, Vector3_t2903530434  ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Vector3::Dot(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  float Vector3_Dot_m2210511857 (RuntimeObject * __this /* static, unused */, Vector3_t2903530434  ___lhs0, Vector3_t2903530434  ___rhs1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Plane::.ctor(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  void Plane__ctor_m2922395188 (Plane_t2300588969 * __this, Vector3_t2903530434  ___inNormal0, Vector3_t2903530434  ___inPoint1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Mathf::Approximately(System.Single,System.Single)
extern "C"  bool Mathf_Approximately_m3781428536 (RuntimeObject * __this /* static, unused */, float ___a0, float ___b1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Plane::Raycast(UnityEngine.Ray,System.Single&)
extern "C"  bool Plane_Raycast_m235007613 (Plane_t2300588969 * __this, Ray_t3821377119  ___ray0, float* ___enter1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.UnityString::Format(System.String,System.Object[])
extern "C"  String_t* UnityString_Format_m2401112968 (RuntimeObject * __this /* static, unused */, String_t* ___fmt0, ObjectU5BU5D_t3270211303* ___args1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.Plane::ToString()
extern "C"  String_t* Plane_ToString_m2513597252 (Plane_t2300588969 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Playables.AudioPlayableGraphExtensions::INTERNAL_CALL_InternalCreateAudioOutput(UnityEngine.Playables.PlayableGraph&,System.String,UnityEngine.Playables.PlayableOutputHandle&)
extern "C"  bool AudioPlayableGraphExtensions_INTERNAL_CALL_InternalCreateAudioOutput_m344357075 (RuntimeObject * __this /* static, unused */, PlayableGraph_t1110114554 * ___graph0, String_t* ___name1, PlayableOutputHandle_t2200222555 * ___handle2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Playables.Playable::.ctor(UnityEngine.Playables.PlayableHandle)
extern "C"  void Playable__ctor_m1598574293 (Playable_t735025802 * __this, PlayableHandle_t1286828706  ___handle0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Playables.PlayableHandle UnityEngine.Playables.PlayableGraph::CreatePlayableHandle()
extern "C"  PlayableHandle_t1286828706  PlayableGraph_CreatePlayableHandle_m1321329662 (PlayableGraph_t1110114554 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Playables.PlayableExtensions::SetInputCount<UnityEngine.Playables.Playable>(U,System.Int32)
#define PlayableExtensions_SetInputCount_TisPlayable_t735025802_m769118885(__this /* static, unused */, ___playable0, ___value1, method) ((  void (*) (RuntimeObject * /* static, unused */, Playable_t735025802 , int32_t, const RuntimeMethod*))PlayableExtensions_SetInputCount_TisPlayable_t735025802_m769118885_gshared)(__this /* static, unused */, ___playable0, ___value1, method)
// UnityEngine.Playables.PlayableHandle UnityEngine.Playables.Playable::GetHandle()
extern "C"  PlayableHandle_t1286828706  Playable_GetHandle_m849086377 (Playable_t735025802 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Type UnityEngine.Playables.PlayableHandle::GetPlayableType()
extern "C"  Type_t * PlayableHandle_GetPlayableType_m1758692074 (PlayableHandle_t1286828706 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Type UnityEngine.Playables.Playable::GetPlayableType()
extern "C"  Type_t * Playable_GetPlayableType_m12689584 (Playable_t735025802 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Playables.PlayableHandle::op_Equality(UnityEngine.Playables.PlayableHandle,UnityEngine.Playables.PlayableHandle)
extern "C"  bool PlayableHandle_op_Equality_m2726566843 (RuntimeObject * __this /* static, unused */, PlayableHandle_t1286828706  ___x0, PlayableHandle_t1286828706  ___y1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Playables.Playable::Equals(UnityEngine.Playables.Playable)
extern "C"  bool Playable_Equals_m2449635747 (Playable_t735025802 * __this, Playable_t735025802  ___other0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Playables.PlayableHandle UnityEngine.Playables.PlayableHandle::get_Null()
extern "C"  PlayableHandle_t1286828706  PlayableHandle_get_Null_m1252146945 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ScriptableObject::.ctor()
extern "C"  void ScriptableObject__ctor_m4110805243 (ScriptableObject_t3635579074 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Playables.Playable UnityEngine.Playables.Playable::get_Null()
extern "C"  Playable_t735025802  Playable_get_Null_m1400628986 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void* System.IntPtr::ToPointer()
extern "C"  void* IntPtr_ToPointer_m745807476 (IntPtr_t* __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Object System.Object::MemberwiseClone()
extern "C"  RuntimeObject * Object_MemberwiseClone_m500616104 (RuntimeObject * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Playables.PlayableGraph::INTERNAL_CALL_CreateScriptOutputInternal(UnityEngine.Playables.PlayableGraph&,System.String,UnityEngine.Playables.PlayableOutputHandle&)
extern "C"  bool PlayableGraph_INTERNAL_CALL_CreateScriptOutputInternal_m3041400947 (RuntimeObject * __this /* static, unused */, PlayableGraph_t1110114554 * ___graph0, String_t* ___name1, PlayableOutputHandle_t2200222555 * ___handle2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Playables.PlayableGraph::CreatePlayableHandleInternal(UnityEngine.Playables.PlayableGraph&,UnityEngine.Playables.PlayableHandle&)
extern "C"  bool PlayableGraph_CreatePlayableHandleInternal_m568622090 (RuntimeObject * __this /* static, unused */, PlayableGraph_t1110114554 * ___graph0, PlayableHandle_t1286828706 * ___handle1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Playables.PlayableGraph::INTERNAL_CALL_CreatePlayableHandleInternal(UnityEngine.Playables.PlayableGraph&,UnityEngine.Playables.PlayableHandle&)
extern "C"  bool PlayableGraph_INTERNAL_CALL_CreatePlayableHandleInternal_m2720952989 (RuntimeObject * __this /* static, unused */, PlayableGraph_t1110114554 * ___graph0, PlayableHandle_t1286828706 * ___handle1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Playables.PlayableHandle::IsValidInternal(UnityEngine.Playables.PlayableHandle&)
extern "C"  bool PlayableHandle_IsValidInternal_m3486814698 (RuntimeObject * __this /* static, unused */, PlayableHandle_t1286828706 * ___playable0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Playables.PlayableHandle::IsValid()
extern "C"  bool PlayableHandle_IsValid_m169138349 (PlayableHandle_t1286828706 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Playables.PlayableHandle::INTERNAL_CALL_IsValidInternal(UnityEngine.Playables.PlayableHandle&)
extern "C"  bool PlayableHandle_INTERNAL_CALL_IsValidInternal_m3801525674 (RuntimeObject * __this /* static, unused */, PlayableHandle_t1286828706 * ___playable0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Type UnityEngine.Playables.PlayableHandle::INTERNAL_CALL_GetPlayableTypeOf(UnityEngine.Playables.PlayableHandle&)
extern "C"  Type_t * PlayableHandle_INTERNAL_CALL_GetPlayableTypeOf_m725853957 (RuntimeObject * __this /* static, unused */, PlayableHandle_t1286828706 * ___playable0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Type UnityEngine.Playables.PlayableHandle::GetPlayableTypeOf(UnityEngine.Playables.PlayableHandle&)
extern "C"  Type_t * PlayableHandle_GetPlayableTypeOf_m194458495 (RuntimeObject * __this /* static, unused */, PlayableHandle_t1286828706 * ___playable0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Playables.PlayableHandle::GetInputCountInternal(UnityEngine.Playables.PlayableHandle&)
extern "C"  int32_t PlayableHandle_GetInputCountInternal_m3943905105 (RuntimeObject * __this /* static, unused */, PlayableHandle_t1286828706 * ___playable0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Playables.PlayableHandle::GetInputCount()
extern "C"  int32_t PlayableHandle_GetInputCount_m3374229925 (PlayableHandle_t1286828706 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Playables.PlayableHandle::SetInputCountInternal(UnityEngine.Playables.PlayableHandle&,System.Int32)
extern "C"  void PlayableHandle_SetInputCountInternal_m54801121 (RuntimeObject * __this /* static, unused */, PlayableHandle_t1286828706 * ___playable0, int32_t ___count1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Playables.PlayableHandle::SetInputCount(System.Int32)
extern "C"  void PlayableHandle_SetInputCount_m525897292 (PlayableHandle_t1286828706 * __this, int32_t ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Playables.PlayState UnityEngine.Playables.PlayableHandle::GetPlayStateInternal(UnityEngine.Playables.PlayableHandle&)
extern "C"  int32_t PlayableHandle_GetPlayStateInternal_m3140903434 (RuntimeObject * __this /* static, unused */, PlayableHandle_t1286828706 * ___playable0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Playables.PlayState UnityEngine.Playables.PlayableHandle::GetPlayState()
extern "C"  int32_t PlayableHandle_GetPlayState_m2902646787 (PlayableHandle_t1286828706 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Playables.PlayableHandle::SetPlayStateInternal(UnityEngine.Playables.PlayableHandle&,UnityEngine.Playables.PlayState)
extern "C"  void PlayableHandle_SetPlayStateInternal_m3078842473 (RuntimeObject * __this /* static, unused */, PlayableHandle_t1286828706 * ___playable0, int32_t ___playState1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Playables.PlayableHandle::SetPlayState(UnityEngine.Playables.PlayState)
extern "C"  void PlayableHandle_SetPlayState_m1076495759 (PlayableHandle_t1286828706 * __this, int32_t ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Playables.PlayableHandle::SetTimeInternal(UnityEngine.Playables.PlayableHandle&,System.Double)
extern "C"  void PlayableHandle_SetTimeInternal_m3645103821 (RuntimeObject * __this /* static, unused */, PlayableHandle_t1286828706 * ___playable0, double ___time1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Playables.PlayableHandle::SetTime(System.Double)
extern "C"  void PlayableHandle_SetTime_m260977007 (PlayableHandle_t1286828706 * __this, double ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Playables.PlayState UnityEngine.Playables.PlayableHandle::INTERNAL_CALL_GetPlayStateInternal(UnityEngine.Playables.PlayableHandle&)
extern "C"  int32_t PlayableHandle_INTERNAL_CALL_GetPlayStateInternal_m3759265247 (RuntimeObject * __this /* static, unused */, PlayableHandle_t1286828706 * ___playable0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Playables.PlayableHandle::INTERNAL_CALL_SetPlayStateInternal(UnityEngine.Playables.PlayableHandle&,UnityEngine.Playables.PlayState)
extern "C"  void PlayableHandle_INTERNAL_CALL_SetPlayStateInternal_m4286520759 (RuntimeObject * __this /* static, unused */, PlayableHandle_t1286828706 * ___playable0, int32_t ___playState1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Playables.PlayableHandle::INTERNAL_CALL_SetTimeInternal(UnityEngine.Playables.PlayableHandle&,System.Double)
extern "C"  void PlayableHandle_INTERNAL_CALL_SetTimeInternal_m717980664 (RuntimeObject * __this /* static, unused */, PlayableHandle_t1286828706 * ___playable0, double ___time1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Playables.PlayableHandle::SetDurationInternal(UnityEngine.Playables.PlayableHandle&,System.Double)
extern "C"  void PlayableHandle_SetDurationInternal_m607008419 (RuntimeObject * __this /* static, unused */, PlayableHandle_t1286828706 * ___playable0, double ___duration1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Playables.PlayableHandle::SetDuration(System.Double)
extern "C"  void PlayableHandle_SetDuration_m2070808009 (PlayableHandle_t1286828706 * __this, double ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Playables.PlayableHandle::INTERNAL_CALL_SetDurationInternal(UnityEngine.Playables.PlayableHandle&,System.Double)
extern "C"  void PlayableHandle_INTERNAL_CALL_SetDurationInternal_m2218811829 (RuntimeObject * __this /* static, unused */, PlayableHandle_t1286828706 * ___playable0, double ___duration1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Playables.PlayableHandle::INTERNAL_CALL_GetInputCountInternal(UnityEngine.Playables.PlayableHandle&)
extern "C"  int32_t PlayableHandle_INTERNAL_CALL_GetInputCountInternal_m2293249710 (RuntimeObject * __this /* static, unused */, PlayableHandle_t1286828706 * ___playable0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Playables.PlayableHandle::INTERNAL_CALL_SetInputCountInternal(UnityEngine.Playables.PlayableHandle&,System.Int32)
extern "C"  void PlayableHandle_INTERNAL_CALL_SetInputCountInternal_m1892728837 (RuntimeObject * __this /* static, unused */, PlayableHandle_t1286828706 * ___playable0, int32_t ___count1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Playables.PlayableHandle::CompareVersion(UnityEngine.Playables.PlayableHandle,UnityEngine.Playables.PlayableHandle)
extern "C"  bool PlayableHandle_CompareVersion_m1642417156 (RuntimeObject * __this /* static, unused */, PlayableHandle_t1286828706  ___lhs0, PlayableHandle_t1286828706  ___rhs1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Playables.PlayableHandle::Equals(System.Object)
extern "C"  bool PlayableHandle_Equals_m2914066680 (PlayableHandle_t1286828706 * __this, RuntimeObject * ___p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.IntPtr::GetHashCode()
extern "C"  int32_t IntPtr_GetHashCode_m4257728765 (IntPtr_t* __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Int32::GetHashCode()
extern "C"  int32_t Int32_GetHashCode_m3835255044 (int32_t* __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Playables.PlayableHandle::GetHashCode()
extern "C"  int32_t PlayableHandle_GetHashCode_m3846748325 (PlayableHandle_t1286828706 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Playables.PlayableOutput::.ctor(UnityEngine.Playables.PlayableOutputHandle)
extern "C"  void PlayableOutput__ctor_m2618420498 (PlayableOutput_t3724158364 * __this, PlayableOutputHandle_t2200222555  ___handle0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Playables.PlayableOutputHandle UnityEngine.Playables.PlayableOutput::GetHandle()
extern "C"  PlayableOutputHandle_t2200222555  PlayableOutput_GetHandle_m1843256230 (PlayableOutput_t3724158364 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Type UnityEngine.Playables.PlayableOutputHandle::GetPlayableOutputTypeOf(UnityEngine.Playables.PlayableOutputHandle&)
extern "C"  Type_t * PlayableOutputHandle_GetPlayableOutputTypeOf_m540740563 (RuntimeObject * __this /* static, unused */, PlayableOutputHandle_t2200222555 * ___handle0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Type UnityEngine.Playables.PlayableOutput::GetPlayableOutputType()
extern "C"  Type_t * PlayableOutput_GetPlayableOutputType_m3110179864 (PlayableOutput_t3724158364 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Playables.PlayableOutputHandle::op_Equality(UnityEngine.Playables.PlayableOutputHandle,UnityEngine.Playables.PlayableOutputHandle)
extern "C"  bool PlayableOutputHandle_op_Equality_m854247697 (RuntimeObject * __this /* static, unused */, PlayableOutputHandle_t2200222555  ___lhs0, PlayableOutputHandle_t2200222555  ___rhs1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Playables.PlayableOutput::Equals(UnityEngine.Playables.PlayableOutput)
extern "C"  bool PlayableOutput_Equals_m3710317608 (PlayableOutput_t3724158364 * __this, PlayableOutput_t3724158364  ___other0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Playables.PlayableOutputHandle UnityEngine.Playables.PlayableOutputHandle::get_Null()
extern "C"  PlayableOutputHandle_t2200222555  PlayableOutputHandle_get_Null_m3317868945 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Playables.PlayableOutputHandle::IsValidInternal(UnityEngine.Playables.PlayableOutputHandle&)
extern "C"  bool PlayableOutputHandle_IsValidInternal_m1664352510 (RuntimeObject * __this /* static, unused */, PlayableOutputHandle_t2200222555 * ___handle0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Playables.PlayableOutputHandle::IsValid()
extern "C"  bool PlayableOutputHandle_IsValid_m3337521099 (PlayableOutputHandle_t2200222555 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Playables.PlayableOutputHandle::INTERNAL_CALL_IsValidInternal(UnityEngine.Playables.PlayableOutputHandle&)
extern "C"  bool PlayableOutputHandle_INTERNAL_CALL_IsValidInternal_m103905867 (RuntimeObject * __this /* static, unused */, PlayableOutputHandle_t2200222555 * ___handle0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Type UnityEngine.Playables.PlayableOutputHandle::INTERNAL_CALL_GetPlayableOutputTypeOf(UnityEngine.Playables.PlayableOutputHandle&)
extern "C"  Type_t * PlayableOutputHandle_INTERNAL_CALL_GetPlayableOutputTypeOf_m445862965 (RuntimeObject * __this /* static, unused */, PlayableOutputHandle_t2200222555 * ___handle0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Playables.PlayableOutputHandle::GetHashCode()
extern "C"  int32_t PlayableOutputHandle_GetHashCode_m2925011117 (PlayableOutputHandle_t2200222555 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Playables.PlayableOutputHandle::CompareVersion(UnityEngine.Playables.PlayableOutputHandle,UnityEngine.Playables.PlayableOutputHandle)
extern "C"  bool PlayableOutputHandle_CompareVersion_m3007568050 (RuntimeObject * __this /* static, unused */, PlayableOutputHandle_t2200222555  ___lhs0, PlayableOutputHandle_t2200222555  ___rhs1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Playables.PlayableOutputHandle::Equals(System.Object)
extern "C"  bool PlayableOutputHandle_Equals_m1190900403 (PlayableOutputHandle_t2200222555 * __this, RuntimeObject * ___p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Playables.PlayableOutputHandle::IsPlayableOutputOfType<UnityEngine.Playables.ScriptPlayableOutput>()
#define PlayableOutputHandle_IsPlayableOutputOfType_TisScriptPlayableOutput_t49585950_m1665456405(__this, method) ((  bool (*) (PlayableOutputHandle_t2200222555 *, const RuntimeMethod*))PlayableOutputHandle_IsPlayableOutputOfType_TisScriptPlayableOutput_t49585950_m1665456405_gshared)(__this, method)
// System.Void System.InvalidCastException::.ctor(System.String)
extern "C"  void InvalidCastException__ctor_m1075299501 (InvalidCastException_t1996460962 * __this, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Playables.ScriptPlayableOutput::.ctor(UnityEngine.Playables.PlayableOutputHandle)
extern "C"  void ScriptPlayableOutput__ctor_m1317995783 (ScriptPlayableOutput_t49585950 * __this, PlayableOutputHandle_t2200222555  ___handle0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Playables.PlayableGraph::CreateScriptOutputInternal(UnityEngine.Playables.PlayableGraph&,System.String,UnityEngine.Playables.PlayableOutputHandle&)
extern "C"  bool PlayableGraph_CreateScriptOutputInternal_m3787016967 (RuntimeObject * __this /* static, unused */, PlayableGraph_t1110114554 * ___graph0, String_t* ___name1, PlayableOutputHandle_t2200222555 * ___handle2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Playables.ScriptPlayableOutput UnityEngine.Playables.ScriptPlayableOutput::get_Null()
extern "C"  ScriptPlayableOutput_t49585950  ScriptPlayableOutput_get_Null_m1376196995 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Playables.PlayableOutputHandle UnityEngine.Playables.ScriptPlayableOutput::GetHandle()
extern "C"  PlayableOutputHandle_t2200222555  ScriptPlayableOutput_GetHandle_m2320625387 (ScriptPlayableOutput_t49585950 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Attribute::.ctor()
extern "C"  void Attribute__ctor_m1283917294 (Attribute_t841672618 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Quaternion::.ctor(System.Single,System.Single,System.Single,System.Single)
extern "C"  void Quaternion__ctor_m1252814061 (Quaternion_t754065749 * __this, float ___x0, float ___y1, float ___z2, float ___w3, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Quaternion::INTERNAL_CALL_AngleAxis(System.Single,UnityEngine.Vector3&,UnityEngine.Quaternion&)
extern "C"  void Quaternion_INTERNAL_CALL_AngleAxis_m2203684919 (RuntimeObject * __this /* static, unused */, float ___angle0, Vector3_t2903530434 * ___axis1, Quaternion_t754065749 * ___value2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Quaternion::Internal_ToAxisAngleRad(UnityEngine.Quaternion,UnityEngine.Vector3&,System.Single&)
extern "C"  void Quaternion_Internal_ToAxisAngleRad_m1871010446 (RuntimeObject * __this /* static, unused */, Quaternion_t754065749  ___q0, Vector3_t2903530434 * ___axis1, float* ___angle2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Quaternion::ToAngleAxis(System.Single&,UnityEngine.Vector3&)
extern "C"  void Quaternion_ToAngleAxis_m636701083 (Quaternion_t754065749 * __this, float* ___angle0, Vector3_t2903530434 * ___axis1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Quaternion::INTERNAL_CALL_LookRotation(UnityEngine.Vector3&,UnityEngine.Vector3&,UnityEngine.Quaternion&)
extern "C"  void Quaternion_INTERNAL_CALL_LookRotation_m1039555483 (RuntimeObject * __this /* static, unused */, Vector3_t2903530434 * ___forward0, Vector3_t2903530434 * ___upwards1, Quaternion_t754065749 * ___value2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Vector3::get_up()
extern "C"  Vector3_t2903530434  Vector3_get_up_m1398813076 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Quaternion::INTERNAL_CALL_Slerp(UnityEngine.Quaternion&,UnityEngine.Quaternion&,System.Single,UnityEngine.Quaternion&)
extern "C"  void Quaternion_INTERNAL_CALL_Slerp_m3884730431 (RuntimeObject * __this /* static, unused */, Quaternion_t754065749 * ___a0, Quaternion_t754065749 * ___b1, float ___t2, Quaternion_t754065749 * ___value3, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Quaternion::INTERNAL_CALL_Inverse(UnityEngine.Quaternion&,UnityEngine.Quaternion&)
extern "C"  void Quaternion_INTERNAL_CALL_Inverse_m2903549735 (RuntimeObject * __this /* static, unused */, Quaternion_t754065749 * ___rotation0, Quaternion_t754065749 * ___value1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Quaternion::Internal_ToEulerRad(UnityEngine.Quaternion)
extern "C"  Vector3_t2903530434  Quaternion_Internal_ToEulerRad_m4233285968 (RuntimeObject * __this /* static, unused */, Quaternion_t754065749  ___rotation0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Vector3::op_Multiply(UnityEngine.Vector3,System.Single)
extern "C"  Vector3_t2903530434  Vector3_op_Multiply_m2063944565 (RuntimeObject * __this /* static, unused */, Vector3_t2903530434  ___a0, float ___d1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Quaternion::Internal_MakePositive(UnityEngine.Vector3)
extern "C"  Vector3_t2903530434  Quaternion_Internal_MakePositive_m3017293218 (RuntimeObject * __this /* static, unused */, Vector3_t2903530434  ___euler0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Quaternion::get_eulerAngles()
extern "C"  Vector3_t2903530434  Quaternion_get_eulerAngles_m3862017976 (Quaternion_t754065749 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Vector3::.ctor(System.Single,System.Single,System.Single)
extern "C"  void Vector3__ctor_m4053220054 (Vector3_t2903530434 * __this, float ___x0, float ___y1, float ___z2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Quaternion UnityEngine.Quaternion::Internal_FromEulerRad(UnityEngine.Vector3)
extern "C"  Quaternion_t754065749  Quaternion_Internal_FromEulerRad_m2740790710 (RuntimeObject * __this /* static, unused */, Vector3_t2903530434  ___euler0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Quaternion::INTERNAL_CALL_Internal_ToEulerRad(UnityEngine.Quaternion&,UnityEngine.Vector3&)
extern "C"  void Quaternion_INTERNAL_CALL_Internal_ToEulerRad_m1031640151 (RuntimeObject * __this /* static, unused */, Quaternion_t754065749 * ___rotation0, Vector3_t2903530434 * ___value1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Quaternion::INTERNAL_CALL_Internal_FromEulerRad(UnityEngine.Vector3&,UnityEngine.Quaternion&)
extern "C"  void Quaternion_INTERNAL_CALL_Internal_FromEulerRad_m2531566785 (RuntimeObject * __this /* static, unused */, Vector3_t2903530434 * ___euler0, Quaternion_t754065749 * ___value1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Quaternion::INTERNAL_CALL_Internal_ToAxisAngleRad(UnityEngine.Quaternion&,UnityEngine.Vector3&,System.Single&)
extern "C"  void Quaternion_INTERNAL_CALL_Internal_ToAxisAngleRad_m1174021168 (RuntimeObject * __this /* static, unused */, Quaternion_t754065749 * ___q0, Vector3_t2903530434 * ___axis1, float* ___angle2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Quaternion::Dot(UnityEngine.Quaternion,UnityEngine.Quaternion)
extern "C"  float Quaternion_Dot_m3512793703 (RuntimeObject * __this /* static, unused */, Quaternion_t754065749  ___a0, Quaternion_t754065749  ___b1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Quaternion::op_Equality(UnityEngine.Quaternion,UnityEngine.Quaternion)
extern "C"  bool Quaternion_op_Equality_m2930003662 (RuntimeObject * __this /* static, unused */, Quaternion_t754065749  ___lhs0, Quaternion_t754065749  ___rhs1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Mathf::Min(System.Single,System.Single)
extern "C"  float Mathf_Min_m3720534012 (RuntimeObject * __this /* static, unused */, float ___a0, float ___b1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Single::GetHashCode()
extern "C"  int32_t Single_GetHashCode_m2049990008 (float* __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Quaternion::GetHashCode()
extern "C"  int32_t Quaternion_GetHashCode_m2311280009 (Quaternion_t754065749 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Single::Equals(System.Single)
extern "C"  bool Single_Equals_m4183737958 (float* __this, float p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Quaternion::Equals(System.Object)
extern "C"  bool Quaternion_Equals_m3533589735 (Quaternion_t754065749 * __this, RuntimeObject * ___other0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.Quaternion::ToString()
extern "C"  String_t* Quaternion_ToString_m329630960 (Quaternion_t754065749 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Random::RandomRangeInt(System.Int32,System.Int32)
extern "C"  int32_t Random_RandomRangeInt_m1371228312 (RuntimeObject * __this /* static, unused */, int32_t ___min0, int32_t ___max1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.PropertyAttribute::.ctor()
extern "C"  void PropertyAttribute__ctor_m2084867108 (PropertyAttribute_t1939035372 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.RangeInt::get_end()
extern "C"  int32_t RangeInt_get_end_m3898523404 (RangeInt_t1885873663 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Vector3::get_normalized()
extern "C"  Vector3_t2903530434  Vector3_get_normalized_m2996377041 (Vector3_t2903530434 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Ray::.ctor(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  void Ray__ctor_m1620751038 (Ray_t3821377119 * __this, Vector3_t2903530434  ___origin0, Vector3_t2903530434  ___direction1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Vector3::op_Addition(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  Vector3_t2903530434  Vector3_op_Addition_m630862342 (RuntimeObject * __this /* static, unused */, Vector3_t2903530434  ___a0, Vector3_t2903530434  ___b1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Ray::GetPoint(System.Single)
extern "C"  Vector3_t2903530434  Ray_GetPoint_m3472514115 (Ray_t3821377119 * __this, float ___distance0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.Ray::ToString()
extern "C"  String_t* Ray_ToString_m1456301833 (Ray_t3821377119 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.RaycastHit::get_point()
extern "C"  Vector3_t2903530434  RaycastHit_get_point_m2039617676 (RaycastHit_t2327766465 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.RaycastHit::get_normal()
extern "C"  Vector3_t2903530434  RaycastHit_get_normal_m1382249399 (RaycastHit_t2327766465 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.RaycastHit::get_distance()
extern "C"  float RaycastHit_get_distance_m3119928599 (RaycastHit_t2327766465 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Collider UnityEngine.RaycastHit::get_collider()
extern "C"  Collider_t2301021182 * RaycastHit_get_collider_m362701715 (RaycastHit_t2327766465 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Object::op_Inequality(UnityEngine.Object,UnityEngine.Object)
extern "C"  bool Object_op_Inequality_m1918954159 (RuntimeObject * __this /* static, unused */, Object_t1502412432 * ___x0, Object_t1502412432 * ___y1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Rigidbody UnityEngine.Collider::get_attachedRigidbody()
extern "C"  Rigidbody_t2492269564 * Collider_get_attachedRigidbody_m2035898500 (Collider_t2301021182 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Rigidbody UnityEngine.RaycastHit::get_rigidbody()
extern "C"  Rigidbody_t2492269564 * RaycastHit_get_rigidbody_m3749375064 (RaycastHit_t2327766465 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.RaycastHit2D::get_point()
extern "C"  Vector2_t3577333262  RaycastHit2D_get_point_m2858777941 (RaycastHit2D_t1105181991 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.RaycastHit2D::get_normal()
extern "C"  Vector2_t3577333262  RaycastHit2D_get_normal_m496919313 (RaycastHit2D_t1105181991 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.RaycastHit2D::get_distance()
extern "C"  float RaycastHit2D_get_distance_m2267927945 (RaycastHit2D_t1105181991 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Collider2D UnityEngine.RaycastHit2D::get_collider()
extern "C"  Collider2D_t1623106781 * RaycastHit2D_get_collider_m1920199818 (RaycastHit2D_t1105181991 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Rect::.ctor(System.Single,System.Single,System.Single,System.Single)
extern "C"  void Rect__ctor_m2728905368 (Rect_t3436776195 * __this, float ___x0, float ___y1, float ___width2, float ___height3, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Rect::.ctor(UnityEngine.Rect)
extern "C"  void Rect__ctor_m398740561 (Rect_t3436776195 * __this, Rect_t3436776195  ___source0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Rect::get_x()
extern "C"  float Rect_get_x_m2895292749 (Rect_t3436776195 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Rect::set_x(System.Single)
extern "C"  void Rect_set_x_m3831764163 (Rect_t3436776195 * __this, float ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Rect::get_y()
extern "C"  float Rect_get_y_m782433804 (Rect_t3436776195 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Rect::set_y(System.Single)
extern "C"  void Rect_set_y_m2194139077 (Rect_t3436776195 * __this, float ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Vector2::.ctor(System.Single,System.Single)
extern "C"  void Vector2__ctor_m2914153465 (Vector2_t3577333262 * __this, float ___x0, float ___y1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.Rect::get_position()
extern "C"  Vector2_t3577333262  Rect_get_position_m3279877841 (Rect_t3436776195 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.Rect::get_center()
extern "C"  Vector2_t3577333262  Rect_get_center_m3371486321 (Rect_t3436776195 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Rect::get_xMin()
extern "C"  float Rect_get_xMin_m3183485224 (Rect_t3436776195 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Rect::get_yMin()
extern "C"  float Rect_get_yMin_m2574684533 (Rect_t3436776195 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.Rect::get_min()
extern "C"  Vector2_t3577333262  Rect_get_min_m3933677767 (Rect_t3436776195 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Rect::get_xMax()
extern "C"  float Rect_get_xMax_m59812867 (Rect_t3436776195 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Rect::get_yMax()
extern "C"  float Rect_get_yMax_m439704772 (Rect_t3436776195 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.Rect::get_max()
extern "C"  Vector2_t3577333262  Rect_get_max_m390465090 (Rect_t3436776195 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Rect::get_width()
extern "C"  float Rect_get_width_m3471045153 (Rect_t3436776195 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Rect::set_width(System.Single)
extern "C"  void Rect_set_width_m3846748752 (Rect_t3436776195 * __this, float ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Rect::get_height()
extern "C"  float Rect_get_height_m16400436 (Rect_t3436776195 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Rect::set_height(System.Single)
extern "C"  void Rect_set_height_m2188665011 (Rect_t3436776195 * __this, float ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.Rect::get_size()
extern "C"  Vector2_t3577333262  Rect_get_size_m2976914550 (Rect_t3436776195 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Rect::set_xMin(System.Single)
extern "C"  void Rect_set_xMin_m2463982144 (Rect_t3436776195 * __this, float ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Rect::set_yMin(System.Single)
extern "C"  void Rect_set_yMin_m3739546123 (Rect_t3436776195 * __this, float ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Rect::set_xMax(System.Single)
extern "C"  void Rect_set_xMax_m3820368702 (Rect_t3436776195 * __this, float ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Rect::set_yMax(System.Single)
extern "C"  void Rect_set_yMax_m3039219194 (Rect_t3436776195 * __this, float ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Rect::Contains(UnityEngine.Vector2)
extern "C"  bool Rect_Contains_m2229411356 (Rect_t3436776195 * __this, Vector2_t3577333262  ___point0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Rect::Contains(UnityEngine.Vector3)
extern "C"  bool Rect_Contains_m3887880031 (Rect_t3436776195 * __this, Vector3_t2903530434  ___point0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Rect::Overlaps(UnityEngine.Rect)
extern "C"  bool Rect_Overlaps_m4279688513 (Rect_t3436776195 * __this, Rect_t3436776195  ___other0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Rect UnityEngine.Rect::OrderMinMax(UnityEngine.Rect)
extern "C"  Rect_t3436776195  Rect_OrderMinMax_m4189518724 (RuntimeObject * __this /* static, unused */, Rect_t3436776195  ___rect0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Rect::Overlaps(UnityEngine.Rect,System.Boolean)
extern "C"  bool Rect_Overlaps_m3859656221 (Rect_t3436776195 * __this, Rect_t3436776195  ___other0, bool ___allowInverse1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Rect::op_Equality(UnityEngine.Rect,UnityEngine.Rect)
extern "C"  bool Rect_op_Equality_m3697585315 (RuntimeObject * __this /* static, unused */, Rect_t3436776195  ___lhs0, Rect_t3436776195  ___rhs1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Rect::GetHashCode()
extern "C"  int32_t Rect_GetHashCode_m3134604048 (Rect_t3436776195 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Rect::Equals(System.Object)
extern "C"  bool Rect_Equals_m375994694 (Rect_t3436776195 * __this, RuntimeObject * ___other0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.Rect::ToString()
extern "C"  String_t* Rect_ToString_m1731978317 (Rect_t3436776195 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RectOffset::Init()
extern "C"  void RectOffset_Init_m3849069081 (RectOffset_t1315541452 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RectOffset::Cleanup()
extern "C"  void RectOffset_Cleanup_m3041375459 (RectOffset_t1315541452 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Object::Finalize()
extern "C"  void Object_Finalize_m2705829212 (RuntimeObject * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.RectOffset::get_left()
extern "C"  int32_t RectOffset_get_left_m643107766 (RectOffset_t1315541452 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.RectOffset::get_right()
extern "C"  int32_t RectOffset_get_right_m3289664570 (RectOffset_t1315541452 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.RectOffset::get_top()
extern "C"  int32_t RectOffset_get_top_m1081769127 (RectOffset_t1315541452 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.RectOffset::get_bottom()
extern "C"  int32_t RectOffset_get_bottom_m2823267338 (RectOffset_t1315541452 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RectTransform::INTERNAL_get_rect(UnityEngine.Rect&)
extern "C"  void RectTransform_INTERNAL_get_rect_m752273476 (RectTransform_t3962994911 * __this, Rect_t3436776195 * ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RectTransform::INTERNAL_get_anchorMin(UnityEngine.Vector2&)
extern "C"  void RectTransform_INTERNAL_get_anchorMin_m740746116 (RectTransform_t3962994911 * __this, Vector2_t3577333262 * ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RectTransform::INTERNAL_set_anchorMin(UnityEngine.Vector2&)
extern "C"  void RectTransform_INTERNAL_set_anchorMin_m4106319920 (RectTransform_t3962994911 * __this, Vector2_t3577333262 * ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RectTransform::INTERNAL_get_anchorMax(UnityEngine.Vector2&)
extern "C"  void RectTransform_INTERNAL_get_anchorMax_m1541525480 (RectTransform_t3962994911 * __this, Vector2_t3577333262 * ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RectTransform::INTERNAL_set_anchorMax(UnityEngine.Vector2&)
extern "C"  void RectTransform_INTERNAL_set_anchorMax_m2865678990 (RectTransform_t3962994911 * __this, Vector2_t3577333262 * ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RectTransform::INTERNAL_get_anchoredPosition(UnityEngine.Vector2&)
extern "C"  void RectTransform_INTERNAL_get_anchoredPosition_m1013122670 (RectTransform_t3962994911 * __this, Vector2_t3577333262 * ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RectTransform::INTERNAL_set_anchoredPosition(UnityEngine.Vector2&)
extern "C"  void RectTransform_INTERNAL_set_anchoredPosition_m3369370392 (RectTransform_t3962994911 * __this, Vector2_t3577333262 * ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RectTransform::INTERNAL_get_sizeDelta(UnityEngine.Vector2&)
extern "C"  void RectTransform_INTERNAL_get_sizeDelta_m263664391 (RectTransform_t3962994911 * __this, Vector2_t3577333262 * ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RectTransform::INTERNAL_set_sizeDelta(UnityEngine.Vector2&)
extern "C"  void RectTransform_INTERNAL_set_sizeDelta_m3207534479 (RectTransform_t3962994911 * __this, Vector2_t3577333262 * ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RectTransform::INTERNAL_get_pivot(UnityEngine.Vector2&)
extern "C"  void RectTransform_INTERNAL_get_pivot_m3673734192 (RectTransform_t3962994911 * __this, Vector2_t3577333262 * ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RectTransform::INTERNAL_set_pivot(UnityEngine.Vector2&)
extern "C"  void RectTransform_INTERNAL_set_pivot_m3445627362 (RectTransform_t3962994911 * __this, Vector2_t3577333262 * ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Delegate System.Delegate::Combine(System.Delegate,System.Delegate)
extern "C"  Delegate_t537306192 * Delegate_Combine_m408928526 (RuntimeObject * __this /* static, unused */, Delegate_t537306192 * p0, Delegate_t537306192 * p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Delegate System.Delegate::Remove(System.Delegate,System.Delegate)
extern "C"  Delegate_t537306192 * Delegate_Remove_m2220107885 (RuntimeObject * __this /* static, unused */, Delegate_t537306192 * p0, Delegate_t537306192 * p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RectTransform/ReapplyDrivenProperties::Invoke(UnityEngine.RectTransform)
extern "C"  void ReapplyDrivenProperties_Invoke_m4155422273 (ReapplyDrivenProperties_t565817470 * __this, RectTransform_t3962994911 * ___driven0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Debug::LogError(System.Object)
extern "C"  void Debug_LogError_m1848790000 (RuntimeObject * __this /* static, unused */, RuntimeObject * ___message0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Rect UnityEngine.RectTransform::get_rect()
extern "C"  Rect_t3436776195  RectTransform_get_rect_m3374483472 (RectTransform_t3962994911 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RectTransform::GetLocalCorners(UnityEngine.Vector3[])
extern "C"  void RectTransform_GetLocalCorners_m1896055443 (RectTransform_t3962994911 * __this, Vector3U5BU5D_t981680887* ___fourCornersArray0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform UnityEngine.Component::get_transform()
extern "C"  Transform_t3316442598 * Component_get_transform_m2038396632 (Component_t4087199522 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Transform::TransformPoint(UnityEngine.Vector3)
extern "C"  Vector3_t2903530434  Transform_TransformPoint_m2770276166 (Transform_t3316442598 * __this, Vector3_t2903530434  ___position0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.RectTransform::get_anchoredPosition()
extern "C"  Vector2_t3577333262  RectTransform_get_anchoredPosition_m1269783566 (RectTransform_t3962994911 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.RectTransform::get_sizeDelta()
extern "C"  Vector2_t3577333262  RectTransform_get_sizeDelta_m2500894686 (RectTransform_t3962994911 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.RectTransform::get_pivot()
extern "C"  Vector2_t3577333262  RectTransform_get_pivot_m115984824 (RectTransform_t3962994911 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.Vector2::Scale(UnityEngine.Vector2,UnityEngine.Vector2)
extern "C"  Vector2_t3577333262  Vector2_Scale_m1829241572 (RuntimeObject * __this /* static, unused */, Vector2_t3577333262  ___a0, Vector2_t3577333262  ___b1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.Vector2::op_Subtraction(UnityEngine.Vector2,UnityEngine.Vector2)
extern "C"  Vector2_t3577333262  Vector2_op_Subtraction_m1470240788 (RuntimeObject * __this /* static, unused */, Vector2_t3577333262  ___a0, Vector2_t3577333262  ___b1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RectTransform::set_sizeDelta(UnityEngine.Vector2)
extern "C"  void RectTransform_set_sizeDelta_m1072091878 (RectTransform_t3962994911 * __this, Vector2_t3577333262  ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.Vector2::get_one()
extern "C"  Vector2_t3577333262  Vector2_get_one_m4238015175 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.Vector2::op_Addition(UnityEngine.Vector2,UnityEngine.Vector2)
extern "C"  Vector2_t3577333262  Vector2_op_Addition_m2696862189 (RuntimeObject * __this /* static, unused */, Vector2_t3577333262  ___a0, Vector2_t3577333262  ___b1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RectTransform::set_anchoredPosition(UnityEngine.Vector2)
extern "C"  void RectTransform_set_anchoredPosition_m3723613293 (RectTransform_t3962994911 * __this, Vector2_t3577333262  ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.RectTransform::get_anchorMin()
extern "C"  Vector2_t3577333262  RectTransform_get_anchorMin_m2315738783 (RectTransform_t3962994911 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Vector2::set_Item(System.Int32,System.Single)
extern "C"  void Vector2_set_Item_m140300536 (Vector2_t3577333262 * __this, int32_t ___index0, float ___value1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RectTransform::set_anchorMin(UnityEngine.Vector2)
extern "C"  void RectTransform_set_anchorMin_m1073640259 (RectTransform_t3962994911 * __this, Vector2_t3577333262  ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.RectTransform::get_anchorMax()
extern "C"  Vector2_t3577333262  RectTransform_get_anchorMax_m2215014239 (RectTransform_t3962994911 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RectTransform::set_anchorMax(UnityEngine.Vector2)
extern "C"  void RectTransform_set_anchorMax_m1901263563 (RectTransform_t3962994911 * __this, Vector2_t3577333262  ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Vector2::get_Item(System.Int32)
extern "C"  float Vector2_get_Item_m373581785 (Vector2_t3577333262 * __this, int32_t ___index0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.RectTransform::GetParentSize()
extern "C"  Vector2_t3577333262  RectTransform_GetParentSize_m3415414519 (RectTransform_t3962994911 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform UnityEngine.Transform::get_parent()
extern "C"  Transform_t3316442598 * Transform_get_parent_m273289079 (Transform_t3316442598 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Object::op_Implicit(UnityEngine.Object)
extern "C"  bool Object_op_Implicit_m2314733868 (RuntimeObject * __this /* static, unused */, Object_t1502412432 * ___exists0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.Vector2::get_zero()
extern "C"  Vector2_t3577333262  Vector2_get_zero_m4146031368 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Vector2::op_Implicit(UnityEngine.Vector2)
extern "C"  Vector3_t2903530434  Vector2_op_Implicit_m3445183035 (RuntimeObject * __this /* static, unused */, Vector2_t3577333262  ___v0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Ray UnityEngine.RectTransformUtility::ScreenPointToRay(UnityEngine.Camera,UnityEngine.Vector2)
extern "C"  Ray_t3821377119  RectTransformUtility_ScreenPointToRay_m339743857 (RuntimeObject * __this /* static, unused */, Camera_t3175186167 * ___cam0, Vector2_t3577333262  ___screenPos1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Quaternion UnityEngine.Transform::get_rotation()
extern "C"  Quaternion_t754065749  Transform_get_rotation_m2679542584 (Transform_t3316442598 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Vector3::get_back()
extern "C"  Vector3_t2903530434  Vector3_get_back_m440222366 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Quaternion::op_Multiply(UnityEngine.Quaternion,UnityEngine.Vector3)
extern "C"  Vector3_t2903530434  Quaternion_op_Multiply_m1825091092 (RuntimeObject * __this /* static, unused */, Quaternion_t754065749  ___rotation0, Vector3_t2903530434  ___point1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Transform::get_position()
extern "C"  Vector3_t2903530434  Transform_get_position_m1291471697 (Transform_t3316442598 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.RectTransformUtility::ScreenPointToWorldPointInRectangle(UnityEngine.RectTransform,UnityEngine.Vector2,UnityEngine.Camera,UnityEngine.Vector3&)
extern "C"  bool RectTransformUtility_ScreenPointToWorldPointInRectangle_m4112832593 (RuntimeObject * __this /* static, unused */, RectTransform_t3962994911 * ___rect0, Vector2_t3577333262  ___screenPoint1, Camera_t3175186167 * ___cam2, Vector3_t2903530434 * ___worldPoint3, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Transform::InverseTransformPoint(UnityEngine.Vector3)
extern "C"  Vector3_t2903530434  Transform_InverseTransformPoint_m573231499 (Transform_t3316442598 * __this, Vector3_t2903530434  ___position0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.Vector2::op_Implicit(UnityEngine.Vector3)
extern "C"  Vector2_t3577333262  Vector2_op_Implicit_m214149158 (RuntimeObject * __this /* static, unused */, Vector3_t2903530434  ___v0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Ray UnityEngine.Camera::ScreenPointToRay(UnityEngine.Vector3)
extern "C"  Ray_t3821377119  Camera_ScreenPointToRay_m3401639554 (Camera_t3175186167 * __this, Vector3_t2903530434  ___position0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Vector3::get_forward()
extern "C"  Vector3_t2903530434  Vector3_get_forward_m2156180237 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform UnityEngine.Transform::GetChild(System.Int32)
extern "C"  Transform_t3316442598 * Transform_GetChild_m357825681 (Transform_t3316442598 * __this, int32_t ___index0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RectTransformUtility::FlipLayoutOnAxis(UnityEngine.RectTransform,System.Int32,System.Boolean,System.Boolean)
extern "C"  void RectTransformUtility_FlipLayoutOnAxis_m1442291361 (RuntimeObject * __this /* static, unused */, RectTransform_t3962994911 * ___rect0, int32_t ___axis1, bool ___keepPositioning2, bool ___recursive3, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Transform::get_childCount()
extern "C"  int32_t Transform_get_childCount_m1083485410 (Transform_t3316442598 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RectTransform::set_pivot(UnityEngine.Vector2)
extern "C"  void RectTransform_set_pivot_m815166536 (RectTransform_t3962994911 * __this, Vector2_t3577333262  ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RectTransformUtility::FlipLayoutAxes(UnityEngine.RectTransform,System.Boolean,System.Boolean)
extern "C"  void RectTransformUtility_FlipLayoutAxes_m225875364 (RuntimeObject * __this /* static, unused */, RectTransform_t3962994911 * ___rect0, bool ___keepPositioning1, bool ___recursive2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.RectTransformUtility::GetTransposed(UnityEngine.Vector2)
extern "C"  Vector2_t3577333262  RectTransformUtility_GetTransposed_m3431509790 (RuntimeObject * __this /* static, unused */, Vector2_t3577333262  ___input0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.RectTransformUtility::INTERNAL_CALL_RectangleContainsScreenPoint(UnityEngine.RectTransform,UnityEngine.Vector2&,UnityEngine.Camera)
extern "C"  bool RectTransformUtility_INTERNAL_CALL_RectangleContainsScreenPoint_m333423143 (RuntimeObject * __this /* static, unused */, RectTransform_t3962994911 * ___rect0, Vector2_t3577333262 * ___screenPoint1, Camera_t3175186167 * ___cam2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RectTransformUtility::INTERNAL_CALL_PixelAdjustPoint(UnityEngine.Vector2&,UnityEngine.Transform,UnityEngine.Canvas,UnityEngine.Vector2&)
extern "C"  void RectTransformUtility_INTERNAL_CALL_PixelAdjustPoint_m3569618632 (RuntimeObject * __this /* static, unused */, Vector2_t3577333262 * ___point0, Transform_t3316442598 * ___elementTransform1, Canvas_t3744170841 * ___canvas2, Vector2_t3577333262 * ___value3, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RectTransformUtility::INTERNAL_CALL_PixelAdjustRect(UnityEngine.RectTransform,UnityEngine.Canvas,UnityEngine.Rect&)
extern "C"  void RectTransformUtility_INTERNAL_CALL_PixelAdjustRect_m3517741635 (RuntimeObject * __this /* static, unused */, RectTransform_t3962994911 * ___rectTransform0, Canvas_t3744170841 * ___canvas1, Rect_t3436776195 * ___value2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RemoteSettings/UpdatedEventHandler::Invoke()
extern "C"  void UpdatedEventHandler_Invoke_m4098777110 (UpdatedEventHandler_t729869569 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.IntPtr UnityEngine.RenderBufferHelper::GetNativeRenderBufferPtr(System.IntPtr)
extern "C"  IntPtr_t RenderBufferHelper_GetNativeRenderBufferPtr_m2083301505 (RuntimeObject * __this /* static, unused */, IntPtr_t ___rb0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.IntPtr UnityEngine.RenderBuffer::GetNativeRenderBufferPtr()
extern "C"  IntPtr_t RenderBuffer_GetNativeRenderBufferPtr_m2881121766 (RenderBuffer_t3998806177 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RenderBufferHelper::INTERNAL_CALL_GetNativeRenderBufferPtr(System.IntPtr,System.IntPtr&)
extern "C"  void RenderBufferHelper_INTERNAL_CALL_GetNativeRenderBufferPtr_m1589378208 (RuntimeObject * __this /* static, unused */, IntPtr_t ___rb0, IntPtr_t* ___value1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Texture::.ctor()
extern "C"  void Texture__ctor_m3045778021 (Texture_t85561421 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RenderTexture::Internal_CreateRenderTexture(UnityEngine.RenderTexture)
extern "C"  void RenderTexture_Internal_CreateRenderTexture_m295205310 (RuntimeObject * __this /* static, unused */, RenderTexture_t2315324624 * ___rt0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RenderTexture::set_depth(System.Int32)
extern "C"  void RenderTexture_set_depth_m307040776 (RenderTexture_t2315324624 * __this, int32_t ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RenderTexture::set_format(UnityEngine.RenderTextureFormat)
extern "C"  void RenderTexture_set_format_m402584206 (RenderTexture_t2315324624 * __this, int32_t ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.ColorSpace UnityEngine.QualitySettings::get_activeColorSpace()
extern "C"  int32_t QualitySettings_get_activeColorSpace_m3960038853 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RenderTexture::Internal_SetSRGBReadWrite(UnityEngine.RenderTexture,System.Boolean)
extern "C"  void RenderTexture_Internal_SetSRGBReadWrite_m2250546398 (RuntimeObject * __this /* static, unused */, RenderTexture_t2315324624 * ___mono0, bool ___sRGB1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.RenderTexture UnityEngine.RenderTexture::GetTemporary(System.Int32,System.Int32,System.Int32,UnityEngine.RenderTextureFormat,UnityEngine.RenderTextureReadWrite,System.Int32,UnityEngine.RenderTextureMemoryless)
extern "C"  RenderTexture_t2315324624 * RenderTexture_GetTemporary_m2189383508 (RuntimeObject * __this /* static, unused */, int32_t ___width0, int32_t ___height1, int32_t ___depthBuffer2, int32_t ___format3, int32_t ___readWrite4, int32_t ___antiAliasing5, int32_t ___memorylessMode6, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RenderTextureDescriptor::.ctor(System.Int32,System.Int32)
extern "C"  void RenderTextureDescriptor__ctor_m3297691984 (RenderTextureDescriptor_t3096142983 * __this, int32_t ___width0, int32_t ___height1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RenderTextureDescriptor::set_depthBufferBits(System.Int32)
extern "C"  void RenderTextureDescriptor_set_depthBufferBits_m3455337282 (RenderTextureDescriptor_t3096142983 * __this, int32_t ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RenderTextureDescriptor::set_vrUsage(UnityEngine.VRTextureUsage)
extern "C"  void RenderTextureDescriptor_set_vrUsage_m187535372 (RenderTextureDescriptor_t3096142983 * __this, int32_t ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RenderTextureDescriptor::set_colorFormat(UnityEngine.RenderTextureFormat)
extern "C"  void RenderTextureDescriptor_set_colorFormat_m2687483655 (RenderTextureDescriptor_t3096142983 * __this, int32_t ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RenderTextureDescriptor::set_sRGB(System.Boolean)
extern "C"  void RenderTextureDescriptor_set_sRGB_m3197683802 (RenderTextureDescriptor_t3096142983 * __this, bool ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RenderTextureDescriptor::set_msaaSamples(System.Int32)
extern "C"  void RenderTextureDescriptor_set_msaaSamples_m585653155 (RenderTextureDescriptor_t3096142983 * __this, int32_t ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RenderTextureDescriptor::set_memoryless(UnityEngine.RenderTextureMemoryless)
extern "C"  void RenderTextureDescriptor_set_memoryless_m2217229932 (RenderTextureDescriptor_t3096142983 * __this, int32_t ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.RenderTexture UnityEngine.RenderTexture::GetTemporary(UnityEngine.RenderTextureDescriptor)
extern "C"  RenderTexture_t2315324624 * RenderTexture_GetTemporary_m2400612209 (RuntimeObject * __this /* static, unused */, RenderTextureDescriptor_t3096142983  ___desc0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.RenderTexture UnityEngine.RenderTexture::INTERNAL_CALL_GetTemporary_Internal(UnityEngine.RenderTextureDescriptor&)
extern "C"  RenderTexture_t2315324624 * RenderTexture_INTERNAL_CALL_GetTemporary_Internal_m1897569625 (RuntimeObject * __this /* static, unused */, RenderTextureDescriptor_t3096142983 * ___desc0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.RenderTexture::Internal_GetWidth(UnityEngine.RenderTexture)
extern "C"  int32_t RenderTexture_Internal_GetWidth_m1045162166 (RuntimeObject * __this /* static, unused */, RenderTexture_t2315324624 * ___mono0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RenderTexture::Internal_SetWidth(UnityEngine.RenderTexture,System.Int32)
extern "C"  void RenderTexture_Internal_SetWidth_m3641274632 (RuntimeObject * __this /* static, unused */, RenderTexture_t2315324624 * ___mono0, int32_t ___width1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.RenderTexture::Internal_GetHeight(UnityEngine.RenderTexture)
extern "C"  int32_t RenderTexture_Internal_GetHeight_m2844568649 (RuntimeObject * __this /* static, unused */, RenderTexture_t2315324624 * ___mono0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RenderTexture::Internal_SetHeight(UnityEngine.RenderTexture,System.Int32)
extern "C"  void RenderTexture_Internal_SetHeight_m3853247974 (RuntimeObject * __this /* static, unused */, RenderTexture_t2315324624 * ___mono0, int32_t ___width1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.RenderTexture::INTERNAL_CALL_Create(UnityEngine.RenderTexture)
extern "C"  bool RenderTexture_INTERNAL_CALL_Create_m2261583106 (RuntimeObject * __this /* static, unused */, RenderTexture_t2315324624 * ___self0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RenderTexture::INTERNAL_CALL_Release(UnityEngine.RenderTexture)
extern "C"  void RenderTexture_INTERNAL_CALL_Release_m2125671618 (RuntimeObject * __this /* static, unused */, RenderTexture_t2315324624 * ___self0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.RenderTexture::INTERNAL_CALL_IsCreated(UnityEngine.RenderTexture)
extern "C"  bool RenderTexture_INTERNAL_CALL_IsCreated_m2284061715 (RuntimeObject * __this /* static, unused */, RenderTexture_t2315324624 * ___self0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RenderTexture::GetColorBuffer(UnityEngine.RenderBuffer&)
extern "C"  void RenderTexture_GetColorBuffer_m1930834545 (RenderTexture_t2315324624 * __this, RenderBuffer_t3998806177 * ___res0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RenderTexture::GetDepthBuffer(UnityEngine.RenderBuffer&)
extern "C"  void RenderTexture_GetDepthBuffer_m2229287778 (RenderTexture_t2315324624 * __this, RenderBuffer_t3998806177 * ___res0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RenderTexture::ValidateRenderTextureDesc(UnityEngine.RenderTextureDescriptor)
extern "C"  void RenderTexture_ValidateRenderTextureDesc_m3206735494 (RuntimeObject * __this /* static, unused */, RenderTextureDescriptor_t3096142983  ___desc0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RenderTextureDescriptor::set_createdFromScript(System.Boolean)
extern "C"  void RenderTextureDescriptor_set_createdFromScript_m3731502648 (RenderTextureDescriptor_t3096142983 * __this, bool ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.RenderTexture UnityEngine.RenderTexture::GetTemporary_Internal(UnityEngine.RenderTextureDescriptor)
extern "C"  RenderTexture_t2315324624 * RenderTexture_GetTemporary_Internal_m689233990 (RuntimeObject * __this /* static, unused */, RenderTextureDescriptor_t3096142983  ___desc0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.RenderTextureDescriptor::get_width()
extern "C"  int32_t RenderTextureDescriptor_get_width_m253831787 (RenderTextureDescriptor_t3096142983 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.ArgumentException::.ctor(System.String,System.String)
extern "C"  void ArgumentException__ctor_m596893627 (ArgumentException_t4028401650 * __this, String_t* p0, String_t* p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.RenderTextureDescriptor::get_height()
extern "C"  int32_t RenderTextureDescriptor_get_height_m3307205944 (RenderTextureDescriptor_t3096142983 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.RenderTextureDescriptor::get_volumeDepth()
extern "C"  int32_t RenderTextureDescriptor_get_volumeDepth_m3010468377 (RenderTextureDescriptor_t3096142983 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.RenderTextureDescriptor::get_msaaSamples()
extern "C"  int32_t RenderTextureDescriptor_get_msaaSamples_m284599142 (RenderTextureDescriptor_t3096142983 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.RenderTextureDescriptor::get_depthBufferBits()
extern "C"  int32_t RenderTextureDescriptor_get_depthBufferBits_m1512939583 (RenderTextureDescriptor_t3096142983 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RenderTextureDescriptor::.ctor(System.Int32,System.Int32,UnityEngine.RenderTextureFormat,System.Int32)
extern "C"  void RenderTextureDescriptor__ctor_m430880500 (RenderTextureDescriptor_t3096142983 * __this, int32_t ___width0, int32_t ___height1, int32_t ___colorFormat2, int32_t ___depthBufferBits3, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RenderTextureDescriptor::set_width(System.Int32)
extern "C"  void RenderTextureDescriptor_set_width_m1163384803 (RenderTextureDescriptor_t3096142983 * __this, int32_t ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RenderTextureDescriptor::set_height(System.Int32)
extern "C"  void RenderTextureDescriptor_set_height_m773633459 (RenderTextureDescriptor_t3096142983 * __this, int32_t ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RenderTextureDescriptor::set_volumeDepth(System.Int32)
extern "C"  void RenderTextureDescriptor_set_volumeDepth_m3887969168 (RenderTextureDescriptor_t3096142983 * __this, int32_t ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RenderTextureDescriptor::set_dimension(UnityEngine.Rendering.TextureDimension)
extern "C"  void RenderTextureDescriptor_set_dimension_m2285702884 (RenderTextureDescriptor_t3096142983 * __this, int32_t ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RenderTextureDescriptor::set_shadowSamplingMode(UnityEngine.Rendering.ShadowSamplingMode)
extern "C"  void RenderTextureDescriptor_set_shadowSamplingMode_m3567853727 (RenderTextureDescriptor_t3096142983 * __this, int32_t ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RenderTextureDescriptor::SetOrClearRenderTextureCreationFlag(System.Boolean,UnityEngine.RenderTextureCreationFlags)
extern "C"  void RenderTextureDescriptor_SetOrClearRenderTextureCreationFlag_m1589951879 (RenderTextureDescriptor_t3096142983 * __this, bool ___value0, int32_t ___flag1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AsyncOperation::.ctor()
extern "C"  void AsyncOperation__ctor_m3138147554 (AsyncOperation_t2744032732 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Object UnityEngine.Resources::Load(System.String,System.Type)
extern "C"  Object_t1502412432 * Resources_Load_m1847693841 (RuntimeObject * __this /* static, unused */, String_t* ___path0, Type_t * ___systemTypeInstance1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Type System.Type::GetTypeFromHandle(System.RuntimeTypeHandle)
extern "C"  Type_t * Type_GetTypeFromHandle_m269683661 (RuntimeObject * __this /* static, unused */, RuntimeTypeHandle_t845633806  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Rigidbody::INTERNAL_get_velocity(UnityEngine.Vector3&)
extern "C"  void Rigidbody_INTERNAL_get_velocity_m3020139032 (Rigidbody_t2492269564 * __this, Vector3_t2903530434 * ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Rigidbody::INTERNAL_set_velocity(UnityEngine.Vector3&)
extern "C"  void Rigidbody_INTERNAL_set_velocity_m485389597 (Rigidbody_t2492269564 * __this, Vector3_t2903530434 * ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Rigidbody::INTERNAL_set_angularVelocity(UnityEngine.Vector3&)
extern "C"  void Rigidbody_INTERNAL_set_angularVelocity_m2459330916 (Rigidbody_t2492269564 * __this, Vector3_t2903530434 * ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Rigidbody::INTERNAL_CALL_AddForce(UnityEngine.Rigidbody,UnityEngine.Vector3&,UnityEngine.ForceMode)
extern "C"  void Rigidbody_INTERNAL_CALL_AddForce_m3048330967 (RuntimeObject * __this /* static, unused */, Rigidbody_t2492269564 * ___self0, Vector3_t2903530434 * ___force1, int32_t ___mode2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Rigidbody::INTERNAL_CALL_AddTorque(UnityEngine.Rigidbody,UnityEngine.Vector3&,UnityEngine.ForceMode)
extern "C"  void Rigidbody_INTERNAL_CALL_AddTorque_m2788891887 (RuntimeObject * __this /* static, unused */, Rigidbody_t2492269564 * ___self0, Vector3_t2903530434 * ___torque1, int32_t ___mode2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Rigidbody::INTERNAL_CALL_AddForceAtPosition(UnityEngine.Rigidbody,UnityEngine.Vector3&,UnityEngine.Vector3&,UnityEngine.ForceMode)
extern "C"  void Rigidbody_INTERNAL_CALL_AddForceAtPosition_m517980457 (RuntimeObject * __this /* static, unused */, Rigidbody_t2492269564 * ___self0, Vector3_t2903530434 * ___force1, Vector3_t2903530434 * ___position2, int32_t ___mode3, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Rigidbody::INTERNAL_get_position(UnityEngine.Vector3&)
extern "C"  void Rigidbody_INTERNAL_get_position_m3982538921 (Rigidbody_t2492269564 * __this, Vector3_t2903530434 * ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Rigidbody::INTERNAL_CALL_Sleep(UnityEngine.Rigidbody)
extern "C"  void Rigidbody_INTERNAL_CALL_Sleep_m3183242862 (RuntimeObject * __this /* static, unused */, Rigidbody_t2492269564 * ___self0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Scripting.PreserveAttribute::.ctor()
extern "C"  void PreserveAttribute__ctor_m237382176 (PreserveAttribute_t3468091719 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RuntimeInitializeOnLoadMethodAttribute::set_loadType(UnityEngine.RuntimeInitializeLoadType)
extern "C"  void RuntimeInitializeOnLoadMethodAttribute_set_loadType_m128702405 (RuntimeInitializeOnLoadMethodAttribute_t1540303992 * __this, int32_t ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.SceneManagement.Scene::get_handle()
extern "C"  int32_t Scene_get_handle_m215941749 (Scene_t348657329 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.SceneManagement.Scene::GetNameInternal(System.Int32)
extern "C"  String_t* Scene_GetNameInternal_m2651924806 (RuntimeObject * __this /* static, unused */, int32_t ___sceneHandle0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.SceneManagement.Scene::get_name()
extern "C"  String_t* Scene_get_name_m928930484 (Scene_t348657329 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.SceneManagement.Scene::GetHashCode()
extern "C"  int32_t Scene_GetHashCode_m1444517911 (Scene_t348657329 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.SceneManagement.Scene::Equals(System.Object)
extern "C"  bool Scene_Equals_m714854343 (Scene_t348657329 * __this, RuntimeObject * ___other0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.SceneManagement.SceneManager::INTERNAL_CALL_GetSceneAt(System.Int32,UnityEngine.SceneManagement.Scene&)
extern "C"  void SceneManager_INTERNAL_CALL_GetSceneAt_m312730705 (RuntimeObject * __this /* static, unused */, int32_t ___index0, Scene_t348657329 * ___value1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.SceneManagement.SceneManager::LoadScene(System.String,UnityEngine.SceneManagement.LoadSceneMode)
extern "C"  void SceneManager_LoadScene_m2739829582 (RuntimeObject * __this /* static, unused */, String_t* ___sceneName0, int32_t ___mode1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.AsyncOperation UnityEngine.SceneManagement.SceneManager::LoadSceneAsyncNameIndexInternal(System.String,System.Int32,System.Boolean,System.Boolean)
extern "C"  AsyncOperation_t2744032732 * SceneManager_LoadSceneAsyncNameIndexInternal_m2649939818 (RuntimeObject * __this /* static, unused */, String_t* ___sceneName0, int32_t ___sceneBuildIndex1, bool ___isAdditive2, bool ___mustCompleteNextFrame3, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Events.UnityAction`2<UnityEngine.SceneManagement.Scene,UnityEngine.SceneManagement.LoadSceneMode>::Invoke(T0,T1)
#define UnityAction_2_Invoke_m2774749000(__this, p0, p1, method) ((  void (*) (UnityAction_2_t2064023076 *, Scene_t348657329 , int32_t, const RuntimeMethod*))UnityAction_2_Invoke_m2774749000_gshared)(__this, p0, p1, method)
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.SceneManagement.Scene>::Invoke(T0)
#define UnityAction_1_Invoke_m2393581145(__this, p0, method) ((  void (*) (UnityAction_1_t3896475417 *, Scene_t348657329 , const RuntimeMethod*))UnityAction_1_Invoke_m2393581145_gshared)(__this, p0, method)
// System.Void UnityEngine.Events.UnityAction`2<UnityEngine.SceneManagement.Scene,UnityEngine.SceneManagement.Scene>::Invoke(T0,T1)
#define UnityAction_2_Invoke_m3185242217(__this, p0, p1, method) ((  void (*) (UnityAction_2_t121587842 *, Scene_t348657329 , Scene_t348657329 , const RuntimeMethod*))UnityAction_2_Invoke_m3185242217_gshared)(__this, p0, p1, method)
// System.Void UnityEngine.Object::.ctor()
extern "C"  void Object__ctor_m3993713475 (Object_t1502412432 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ScriptableObject::Internal_CreateScriptableObject(UnityEngine.ScriptableObject)
extern "C"  void ScriptableObject_Internal_CreateScriptableObject_m2602267800 (RuntimeObject * __this /* static, unused */, ScriptableObject_t3635579074 * ___self0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.ScriptableObject UnityEngine.ScriptableObject::CreateInstanceFromType(System.Type)
extern "C"  ScriptableObject_t3635579074 * ScriptableObject_CreateInstanceFromType_m390136550 (RuntimeObject * __this /* static, unused */, Type_t * ___type0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Scripting.APIUpdating.MovedFromAttribute::.ctor(System.String,System.Boolean)
extern "C"  void MovedFromAttribute__ctor_m1751114112 (MovedFromAttribute_t2669718911 * __this, String_t* ___sourceNamespace0, bool ___isInDifferentAssembly1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Scripting.APIUpdating.MovedFromAttribute::set_Namespace(System.String)
extern "C"  void MovedFromAttribute_set_Namespace_m2723306931 (MovedFromAttribute_t2669718911 * __this, String_t* ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Scripting.APIUpdating.MovedFromAttribute::set_IsInDifferentAssembly(System.Boolean)
extern "C"  void MovedFromAttribute_set_IsInDifferentAssembly_m655900810 (MovedFromAttribute_t2669718911 * __this, bool ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Input::get_mousePosition()
extern "C"  Vector3_t2903530434  Input_get_mousePosition_m1483823421 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Camera::get_allCamerasCount()
extern "C"  int32_t Camera_get_allCamerasCount_m1126717630 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Camera::GetAllCameras(UnityEngine.Camera[])
extern "C"  int32_t Camera_GetAllCameras_m1072741975 (RuntimeObject * __this /* static, unused */, CameraU5BU5D_t1519774542* ___cameras0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.RenderTexture UnityEngine.Camera::get_targetTexture()
extern "C"  RenderTexture_t2315324624 * Camera_get_targetTexture_m1786790398 (Camera_t3175186167 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Rect UnityEngine.Camera::get_pixelRect()
extern "C"  Rect_t3436776195  Camera_get_pixelRect_m1533752293 (Camera_t3175186167 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// T UnityEngine.Component::GetComponent<UnityEngine.GUILayer>()
#define Component_GetComponent_TisGUILayer_t4095959459_m2359659865(__this, method) ((  GUILayer_t4095959459 * (*) (Component_t4087199522 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m2412552191_gshared)(__this, method)
// UnityEngine.GUIElement UnityEngine.GUILayer::HitTest(UnityEngine.Vector3)
extern "C"  GUIElement_t4082881042 * GUILayer_HitTest_m1319441216 (GUILayer_t4095959459 * __this, Vector3_t2903530434  ___screenPosition0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject UnityEngine.Component::get_gameObject()
extern "C"  GameObject_t1811656094 * Component_get_gameObject_m2283027183 (Component_t4087199522 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Camera::get_eventMask()
extern "C"  int32_t Camera_get_eventMask_m3829863520 (Camera_t3175186167 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Camera::get_farClipPlane()
extern "C"  float Camera_get_farClipPlane_m1290388625 (Camera_t3175186167 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Camera::get_nearClipPlane()
extern "C"  float Camera_get_nearClipPlane_m57381064 (Camera_t3175186167 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Camera::get_cullingMask()
extern "C"  int32_t Camera_get_cullingMask_m2080661144 (Camera_t3175186167 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject UnityEngine.Camera::RaycastTry(UnityEngine.Ray,System.Single,System.Int32)
extern "C"  GameObject_t1811656094 * Camera_RaycastTry_m1516696014 (Camera_t3175186167 * __this, Ray_t3821377119  ___ray0, float ___distance1, int32_t ___layerMask2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.CameraClearFlags UnityEngine.Camera::get_clearFlags()
extern "C"  int32_t Camera_get_clearFlags_m2110532879 (Camera_t3175186167 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject UnityEngine.Camera::RaycastTry2D(UnityEngine.Ray,System.Single,System.Int32)
extern "C"  GameObject_t1811656094 * Camera_RaycastTry2D_m3135506373 (Camera_t3175186167 * __this, Ray_t3821377119  ___ray0, float ___distance1, int32_t ___layerMask2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.SendMouseEvents::SendEvents(System.Int32,UnityEngine.SendMouseEvents/HitInfo)
extern "C"  void SendMouseEvents_SendEvents_m3207122301 (RuntimeObject * __this /* static, unused */, int32_t ___i0, HitInfo_t2000020299  ___hit1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Input::GetMouseButtonDown(System.Int32)
extern "C"  bool Input_GetMouseButtonDown_m800484706 (RuntimeObject * __this /* static, unused */, int32_t ___button0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Input::GetMouseButton(System.Int32)
extern "C"  bool Input_GetMouseButton_m2523735708 (RuntimeObject * __this /* static, unused */, int32_t ___button0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.SendMouseEvents/HitInfo::op_Implicit(UnityEngine.SendMouseEvents/HitInfo)
extern "C"  bool HitInfo_op_Implicit_m2296455248 (RuntimeObject * __this /* static, unused */, HitInfo_t2000020299  ___exists0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.SendMouseEvents/HitInfo::SendMessage(System.String)
extern "C"  void HitInfo_SendMessage_m2268517799 (HitInfo_t2000020299 * __this, String_t* ___name0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.SendMouseEvents/HitInfo::Compare(UnityEngine.SendMouseEvents/HitInfo,UnityEngine.SendMouseEvents/HitInfo)
extern "C"  bool HitInfo_Compare_m3834232273 (RuntimeObject * __this /* static, unused */, HitInfo_t2000020299  ___lhs0, HitInfo_t2000020299  ___rhs1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GameObject::SendMessage(System.String,System.Object,UnityEngine.SendMessageOptions)
extern "C"  void GameObject_SendMessage_m2018546670 (GameObject_t1811656094 * __this, String_t* ___methodName0, RuntimeObject * ___value1, int32_t ___options2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Type System.Object::GetType()
extern "C"  Type_t * Object_GetType_m3774351292 (RuntimeObject * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.SkeletonBone::get_transformModified()
extern "C"  int32_t SkeletonBone_get_transformModified_m866685587 (SkeletonBone_t3530196780 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.SkeletonBone::set_transformModified(System.Int32)
extern "C"  void SkeletonBone_set_transformModified_m1750125318 (SkeletonBone_t3530196780 * __this, int32_t ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// UnityEngine.NetworkView UnityEngine.NetworkView::Find(UnityEngine.NetworkViewID)
extern "C"  NetworkView_t3440562342 * NetworkView_Find_m2306754569 (RuntimeObject * __this /* static, unused */, NetworkViewID_t1404889299  ___viewID0, const RuntimeMethod* method)
{
	NetworkView_t3440562342 * V_0 = NULL;
	{
		NetworkView_t3440562342 * L_0 = NetworkView_INTERNAL_CALL_Find_m2156794560(NULL /*static, unused*/, (&___viewID0), /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_000e;
	}

IL_000e:
	{
		NetworkView_t3440562342 * L_1 = V_0;
		return L_1;
	}
}
// UnityEngine.NetworkView UnityEngine.NetworkView::INTERNAL_CALL_Find(UnityEngine.NetworkViewID&)
extern "C"  NetworkView_t3440562342 * NetworkView_INTERNAL_CALL_Find_m2156794560 (RuntimeObject * __this /* static, unused */, NetworkViewID_t1404889299 * ___viewID0, const RuntimeMethod* method)
{
	typedef NetworkView_t3440562342 * (*NetworkView_INTERNAL_CALL_Find_m2156794560_ftn) (NetworkViewID_t1404889299 *);
	static NetworkView_INTERNAL_CALL_Find_m2156794560_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (NetworkView_INTERNAL_CALL_Find_m2156794560_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.NetworkView::INTERNAL_CALL_Find(UnityEngine.NetworkViewID&)");
	NetworkView_t3440562342 * retVal = _il2cpp_icall_func(___viewID0);
	return retVal;
}
// UnityEngine.NetworkViewID UnityEngine.NetworkViewID::get_unassigned()
extern "C"  NetworkViewID_t1404889299  NetworkViewID_get_unassigned_m344998103 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	NetworkViewID_t1404889299  V_0;
	memset(&V_0, 0, sizeof(V_0));
	NetworkViewID_t1404889299  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		NetworkViewID_INTERNAL_get_unassigned_m1006212221(NULL /*static, unused*/, (&V_0), /*hidden argument*/NULL);
		NetworkViewID_t1404889299  L_0 = V_0;
		V_1 = L_0;
		goto IL_000f;
	}

IL_000f:
	{
		NetworkViewID_t1404889299  L_1 = V_1;
		return L_1;
	}
}
// System.Void UnityEngine.NetworkViewID::INTERNAL_get_unassigned(UnityEngine.NetworkViewID&)
extern "C"  void NetworkViewID_INTERNAL_get_unassigned_m1006212221 (RuntimeObject * __this /* static, unused */, NetworkViewID_t1404889299 * ___value0, const RuntimeMethod* method)
{
	typedef void (*NetworkViewID_INTERNAL_get_unassigned_m1006212221_ftn) (NetworkViewID_t1404889299 *);
	static NetworkViewID_INTERNAL_get_unassigned_m1006212221_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (NetworkViewID_INTERNAL_get_unassigned_m1006212221_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.NetworkViewID::INTERNAL_get_unassigned(UnityEngine.NetworkViewID&)");
	_il2cpp_icall_func(___value0);
}
// System.Boolean UnityEngine.NetworkViewID::Internal_IsMine(UnityEngine.NetworkViewID)
extern "C"  bool NetworkViewID_Internal_IsMine_m1453090357 (RuntimeObject * __this /* static, unused */, NetworkViewID_t1404889299  ___value0, const RuntimeMethod* method)
{
	bool V_0 = false;
	{
		bool L_0 = NetworkViewID_INTERNAL_CALL_Internal_IsMine_m265888996(NULL /*static, unused*/, (&___value0), /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_000e;
	}

IL_000e:
	{
		bool L_1 = V_0;
		return L_1;
	}
}
// System.Boolean UnityEngine.NetworkViewID::INTERNAL_CALL_Internal_IsMine(UnityEngine.NetworkViewID&)
extern "C"  bool NetworkViewID_INTERNAL_CALL_Internal_IsMine_m265888996 (RuntimeObject * __this /* static, unused */, NetworkViewID_t1404889299 * ___value0, const RuntimeMethod* method)
{
	typedef bool (*NetworkViewID_INTERNAL_CALL_Internal_IsMine_m265888996_ftn) (NetworkViewID_t1404889299 *);
	static NetworkViewID_INTERNAL_CALL_Internal_IsMine_m265888996_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (NetworkViewID_INTERNAL_CALL_Internal_IsMine_m265888996_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.NetworkViewID::INTERNAL_CALL_Internal_IsMine(UnityEngine.NetworkViewID&)");
	bool retVal = _il2cpp_icall_func(___value0);
	return retVal;
}
// System.Void UnityEngine.NetworkViewID::Internal_GetOwner(UnityEngine.NetworkViewID,UnityEngine.NetworkPlayer&)
extern "C"  void NetworkViewID_Internal_GetOwner_m2176248212 (RuntimeObject * __this /* static, unused */, NetworkViewID_t1404889299  ___value0, NetworkPlayer_t4008288529 * ___player1, const RuntimeMethod* method)
{
	{
		NetworkPlayer_t4008288529 * L_0 = ___player1;
		NetworkViewID_INTERNAL_CALL_Internal_GetOwner_m3916819407(NULL /*static, unused*/, (&___value0), L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.NetworkViewID::INTERNAL_CALL_Internal_GetOwner(UnityEngine.NetworkViewID&,UnityEngine.NetworkPlayer&)
extern "C"  void NetworkViewID_INTERNAL_CALL_Internal_GetOwner_m3916819407 (RuntimeObject * __this /* static, unused */, NetworkViewID_t1404889299 * ___value0, NetworkPlayer_t4008288529 * ___player1, const RuntimeMethod* method)
{
	typedef void (*NetworkViewID_INTERNAL_CALL_Internal_GetOwner_m3916819407_ftn) (NetworkViewID_t1404889299 *, NetworkPlayer_t4008288529 *);
	static NetworkViewID_INTERNAL_CALL_Internal_GetOwner_m3916819407_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (NetworkViewID_INTERNAL_CALL_Internal_GetOwner_m3916819407_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.NetworkViewID::INTERNAL_CALL_Internal_GetOwner(UnityEngine.NetworkViewID&,UnityEngine.NetworkPlayer&)");
	_il2cpp_icall_func(___value0, ___player1);
}
// System.String UnityEngine.NetworkViewID::Internal_GetString(UnityEngine.NetworkViewID)
extern "C"  String_t* NetworkViewID_Internal_GetString_m963601455 (RuntimeObject * __this /* static, unused */, NetworkViewID_t1404889299  ___value0, const RuntimeMethod* method)
{
	String_t* V_0 = NULL;
	{
		String_t* L_0 = NetworkViewID_INTERNAL_CALL_Internal_GetString_m4269197340(NULL /*static, unused*/, (&___value0), /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_000e;
	}

IL_000e:
	{
		String_t* L_1 = V_0;
		return L_1;
	}
}
// System.String UnityEngine.NetworkViewID::INTERNAL_CALL_Internal_GetString(UnityEngine.NetworkViewID&)
extern "C"  String_t* NetworkViewID_INTERNAL_CALL_Internal_GetString_m4269197340 (RuntimeObject * __this /* static, unused */, NetworkViewID_t1404889299 * ___value0, const RuntimeMethod* method)
{
	typedef String_t* (*NetworkViewID_INTERNAL_CALL_Internal_GetString_m4269197340_ftn) (NetworkViewID_t1404889299 *);
	static NetworkViewID_INTERNAL_CALL_Internal_GetString_m4269197340_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (NetworkViewID_INTERNAL_CALL_Internal_GetString_m4269197340_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.NetworkViewID::INTERNAL_CALL_Internal_GetString(UnityEngine.NetworkViewID&)");
	String_t* retVal = _il2cpp_icall_func(___value0);
	return retVal;
}
// System.Boolean UnityEngine.NetworkViewID::Internal_Compare(UnityEngine.NetworkViewID,UnityEngine.NetworkViewID)
extern "C"  bool NetworkViewID_Internal_Compare_m2810921315 (RuntimeObject * __this /* static, unused */, NetworkViewID_t1404889299  ___lhs0, NetworkViewID_t1404889299  ___rhs1, const RuntimeMethod* method)
{
	bool V_0 = false;
	{
		bool L_0 = NetworkViewID_INTERNAL_CALL_Internal_Compare_m2042094818(NULL /*static, unused*/, (&___lhs0), (&___rhs1), /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_0010;
	}

IL_0010:
	{
		bool L_1 = V_0;
		return L_1;
	}
}
// System.Boolean UnityEngine.NetworkViewID::INTERNAL_CALL_Internal_Compare(UnityEngine.NetworkViewID&,UnityEngine.NetworkViewID&)
extern "C"  bool NetworkViewID_INTERNAL_CALL_Internal_Compare_m2042094818 (RuntimeObject * __this /* static, unused */, NetworkViewID_t1404889299 * ___lhs0, NetworkViewID_t1404889299 * ___rhs1, const RuntimeMethod* method)
{
	typedef bool (*NetworkViewID_INTERNAL_CALL_Internal_Compare_m2042094818_ftn) (NetworkViewID_t1404889299 *, NetworkViewID_t1404889299 *);
	static NetworkViewID_INTERNAL_CALL_Internal_Compare_m2042094818_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (NetworkViewID_INTERNAL_CALL_Internal_Compare_m2042094818_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.NetworkViewID::INTERNAL_CALL_Internal_Compare(UnityEngine.NetworkViewID&,UnityEngine.NetworkViewID&)");
	bool retVal = _il2cpp_icall_func(___lhs0, ___rhs1);
	return retVal;
}
// System.Boolean UnityEngine.NetworkViewID::op_Equality(UnityEngine.NetworkViewID,UnityEngine.NetworkViewID)
extern "C"  bool NetworkViewID_op_Equality_m3872203871 (RuntimeObject * __this /* static, unused */, NetworkViewID_t1404889299  ___lhs0, NetworkViewID_t1404889299  ___rhs1, const RuntimeMethod* method)
{
	bool V_0 = false;
	{
		NetworkViewID_t1404889299  L_0 = ___lhs0;
		NetworkViewID_t1404889299  L_1 = ___rhs1;
		bool L_2 = NetworkViewID_Internal_Compare_m2810921315(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_000e;
	}

IL_000e:
	{
		bool L_3 = V_0;
		return L_3;
	}
}
// System.Boolean UnityEngine.NetworkViewID::op_Inequality(UnityEngine.NetworkViewID,UnityEngine.NetworkViewID)
extern "C"  bool NetworkViewID_op_Inequality_m2673504712 (RuntimeObject * __this /* static, unused */, NetworkViewID_t1404889299  ___lhs0, NetworkViewID_t1404889299  ___rhs1, const RuntimeMethod* method)
{
	bool V_0 = false;
	{
		NetworkViewID_t1404889299  L_0 = ___lhs0;
		NetworkViewID_t1404889299  L_1 = ___rhs1;
		bool L_2 = NetworkViewID_Internal_Compare_m2810921315(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		V_0 = (bool)((((int32_t)L_2) == ((int32_t)0))? 1 : 0);
		goto IL_0011;
	}

IL_0011:
	{
		bool L_3 = V_0;
		return L_3;
	}
}
// System.Int32 UnityEngine.NetworkViewID::GetHashCode()
extern "C"  int32_t NetworkViewID_GetHashCode_m1329632673 (NetworkViewID_t1404889299 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_a_0();
		int32_t L_1 = __this->get_b_1();
		int32_t L_2 = __this->get_c_2();
		V_0 = ((int32_t)((int32_t)((int32_t)((int32_t)L_0^(int32_t)L_1))^(int32_t)L_2));
		goto IL_001b;
	}

IL_001b:
	{
		int32_t L_3 = V_0;
		return L_3;
	}
}
extern "C"  int32_t NetworkViewID_GetHashCode_m1329632673_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	NetworkViewID_t1404889299 * _thisAdjusted = reinterpret_cast<NetworkViewID_t1404889299 *>(__this + 1);
	return NetworkViewID_GetHashCode_m1329632673(_thisAdjusted, method);
}
// System.Boolean UnityEngine.NetworkViewID::Equals(System.Object)
extern "C"  bool NetworkViewID_Equals_m1195240327 (NetworkViewID_t1404889299 * __this, RuntimeObject * ___other0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NetworkViewID_Equals_m1195240327_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	NetworkViewID_t1404889299  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		RuntimeObject * L_0 = ___other0;
		if (((RuntimeObject *)IsInstSealed((RuntimeObject*)L_0, NetworkViewID_t1404889299_il2cpp_TypeInfo_var)))
		{
			goto IL_0013;
		}
	}
	{
		V_0 = (bool)0;
		goto IL_002c;
	}

IL_0013:
	{
		RuntimeObject * L_1 = ___other0;
		V_1 = ((*(NetworkViewID_t1404889299 *)((NetworkViewID_t1404889299 *)UnBox(L_1, NetworkViewID_t1404889299_il2cpp_TypeInfo_var))));
		NetworkViewID_t1404889299  L_2 = V_1;
		bool L_3 = NetworkViewID_Internal_Compare_m2810921315(NULL /*static, unused*/, (*(NetworkViewID_t1404889299 *)__this), L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		goto IL_002c;
	}

IL_002c:
	{
		bool L_4 = V_0;
		return L_4;
	}
}
extern "C"  bool NetworkViewID_Equals_m1195240327_AdjustorThunk (RuntimeObject * __this, RuntimeObject * ___other0, const RuntimeMethod* method)
{
	NetworkViewID_t1404889299 * _thisAdjusted = reinterpret_cast<NetworkViewID_t1404889299 *>(__this + 1);
	return NetworkViewID_Equals_m1195240327(_thisAdjusted, ___other0, method);
}
// System.Boolean UnityEngine.NetworkViewID::get_isMine()
extern "C"  bool NetworkViewID_get_isMine_m35938188 (NetworkViewID_t1404889299 * __this, const RuntimeMethod* method)
{
	bool V_0 = false;
	{
		bool L_0 = NetworkViewID_Internal_IsMine_m1453090357(NULL /*static, unused*/, (*(NetworkViewID_t1404889299 *)__this), /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_0012;
	}

IL_0012:
	{
		bool L_1 = V_0;
		return L_1;
	}
}
extern "C"  bool NetworkViewID_get_isMine_m35938188_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	NetworkViewID_t1404889299 * _thisAdjusted = reinterpret_cast<NetworkViewID_t1404889299 *>(__this + 1);
	return NetworkViewID_get_isMine_m35938188(_thisAdjusted, method);
}
// UnityEngine.NetworkPlayer UnityEngine.NetworkViewID::get_owner()
extern "C"  NetworkPlayer_t4008288529  NetworkViewID_get_owner_m2196279096 (NetworkViewID_t1404889299 * __this, const RuntimeMethod* method)
{
	NetworkPlayer_t4008288529  V_0;
	memset(&V_0, 0, sizeof(V_0));
	NetworkPlayer_t4008288529  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		NetworkViewID_Internal_GetOwner_m2176248212(NULL /*static, unused*/, (*(NetworkViewID_t1404889299 *)__this), (&V_0), /*hidden argument*/NULL);
		NetworkPlayer_t4008288529  L_0 = V_0;
		V_1 = L_0;
		goto IL_0015;
	}

IL_0015:
	{
		NetworkPlayer_t4008288529  L_1 = V_1;
		return L_1;
	}
}
extern "C"  NetworkPlayer_t4008288529  NetworkViewID_get_owner_m2196279096_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	NetworkViewID_t1404889299 * _thisAdjusted = reinterpret_cast<NetworkViewID_t1404889299 *>(__this + 1);
	return NetworkViewID_get_owner_m2196279096(_thisAdjusted, method);
}
// System.String UnityEngine.NetworkViewID::ToString()
extern "C"  String_t* NetworkViewID_ToString_m2913258028 (NetworkViewID_t1404889299 * __this, const RuntimeMethod* method)
{
	String_t* V_0 = NULL;
	{
		String_t* L_0 = NetworkViewID_Internal_GetString_m963601455(NULL /*static, unused*/, (*(NetworkViewID_t1404889299 *)__this), /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_0012;
	}

IL_0012:
	{
		String_t* L_1 = V_0;
		return L_1;
	}
}
extern "C"  String_t* NetworkViewID_ToString_m2913258028_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	NetworkViewID_t1404889299 * _thisAdjusted = reinterpret_cast<NetworkViewID_t1404889299 *>(__this + 1);
	return NetworkViewID_ToString_m2913258028(_thisAdjusted, method);
}
// Conversion methods for marshalling of: UnityEngine.Object
extern "C" void Object_t1502412432_marshal_pinvoke(const Object_t1502412432& unmarshaled, Object_t1502412432_marshaled_pinvoke& marshaled)
{
	marshaled.___m_CachedPtr_0 = reinterpret_cast<intptr_t>((unmarshaled.get_m_CachedPtr_0()).get_m_value_0());
}
extern "C" void Object_t1502412432_marshal_pinvoke_back(const Object_t1502412432_marshaled_pinvoke& marshaled, Object_t1502412432& unmarshaled)
{
	IntPtr_t unmarshaled_m_CachedPtr_temp_0;
	memset(&unmarshaled_m_CachedPtr_temp_0, 0, sizeof(unmarshaled_m_CachedPtr_temp_0));
	IntPtr_t unmarshaled_m_CachedPtr_temp_0_temp;
	unmarshaled_m_CachedPtr_temp_0_temp.set_m_value_0(reinterpret_cast<void*>((intptr_t)(marshaled.___m_CachedPtr_0)));
	unmarshaled_m_CachedPtr_temp_0 = unmarshaled_m_CachedPtr_temp_0_temp;
	unmarshaled.set_m_CachedPtr_0(unmarshaled_m_CachedPtr_temp_0);
}
// Conversion method for clean up from marshalling of: UnityEngine.Object
extern "C" void Object_t1502412432_marshal_pinvoke_cleanup(Object_t1502412432_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.Object
extern "C" void Object_t1502412432_marshal_com(const Object_t1502412432& unmarshaled, Object_t1502412432_marshaled_com& marshaled)
{
	marshaled.___m_CachedPtr_0 = reinterpret_cast<intptr_t>((unmarshaled.get_m_CachedPtr_0()).get_m_value_0());
}
extern "C" void Object_t1502412432_marshal_com_back(const Object_t1502412432_marshaled_com& marshaled, Object_t1502412432& unmarshaled)
{
	IntPtr_t unmarshaled_m_CachedPtr_temp_0;
	memset(&unmarshaled_m_CachedPtr_temp_0, 0, sizeof(unmarshaled_m_CachedPtr_temp_0));
	IntPtr_t unmarshaled_m_CachedPtr_temp_0_temp;
	unmarshaled_m_CachedPtr_temp_0_temp.set_m_value_0(reinterpret_cast<void*>((intptr_t)(marshaled.___m_CachedPtr_0)));
	unmarshaled_m_CachedPtr_temp_0 = unmarshaled_m_CachedPtr_temp_0_temp;
	unmarshaled.set_m_CachedPtr_0(unmarshaled_m_CachedPtr_temp_0);
}
// Conversion method for clean up from marshalling of: UnityEngine.Object
extern "C" void Object_t1502412432_marshal_com_cleanup(Object_t1502412432_marshaled_com& marshaled)
{
}
// System.Void UnityEngine.Object::.ctor()
extern "C"  void Object__ctor_m3993713475 (Object_t1502412432 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m2906060009(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Object UnityEngine.Object::Internal_CloneSingle(UnityEngine.Object)
extern "C"  Object_t1502412432 * Object_Internal_CloneSingle_m2326967574 (RuntimeObject * __this /* static, unused */, Object_t1502412432 * ___data0, const RuntimeMethod* method)
{
	typedef Object_t1502412432 * (*Object_Internal_CloneSingle_m2326967574_ftn) (Object_t1502412432 *);
	static Object_Internal_CloneSingle_m2326967574_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Object_Internal_CloneSingle_m2326967574_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Object::Internal_CloneSingle(UnityEngine.Object)");
	Object_t1502412432 * retVal = _il2cpp_icall_func(___data0);
	return retVal;
}
// UnityEngine.Object UnityEngine.Object::Internal_CloneSingleWithParent(UnityEngine.Object,UnityEngine.Transform,System.Boolean)
extern "C"  Object_t1502412432 * Object_Internal_CloneSingleWithParent_m973191133 (RuntimeObject * __this /* static, unused */, Object_t1502412432 * ___data0, Transform_t3316442598 * ___parent1, bool ___worldPositionStays2, const RuntimeMethod* method)
{
	typedef Object_t1502412432 * (*Object_Internal_CloneSingleWithParent_m973191133_ftn) (Object_t1502412432 *, Transform_t3316442598 *, bool);
	static Object_Internal_CloneSingleWithParent_m973191133_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Object_Internal_CloneSingleWithParent_m973191133_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Object::Internal_CloneSingleWithParent(UnityEngine.Object,UnityEngine.Transform,System.Boolean)");
	Object_t1502412432 * retVal = _il2cpp_icall_func(___data0, ___parent1, ___worldPositionStays2);
	return retVal;
}
// UnityEngine.Object UnityEngine.Object::Internal_InstantiateSingle(UnityEngine.Object,UnityEngine.Vector3,UnityEngine.Quaternion)
extern "C"  Object_t1502412432 * Object_Internal_InstantiateSingle_m664294789 (RuntimeObject * __this /* static, unused */, Object_t1502412432 * ___data0, Vector3_t2903530434  ___pos1, Quaternion_t754065749  ___rot2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Object_Internal_InstantiateSingle_m664294789_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Object_t1502412432 * V_0 = NULL;
	{
		Object_t1502412432 * L_0 = ___data0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1502412432_il2cpp_TypeInfo_var);
		Object_t1502412432 * L_1 = Object_INTERNAL_CALL_Internal_InstantiateSingle_m133179633(NULL /*static, unused*/, L_0, (&___pos1), (&___rot2), /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_0011;
	}

IL_0011:
	{
		Object_t1502412432 * L_2 = V_0;
		return L_2;
	}
}
// UnityEngine.Object UnityEngine.Object::INTERNAL_CALL_Internal_InstantiateSingle(UnityEngine.Object,UnityEngine.Vector3&,UnityEngine.Quaternion&)
extern "C"  Object_t1502412432 * Object_INTERNAL_CALL_Internal_InstantiateSingle_m133179633 (RuntimeObject * __this /* static, unused */, Object_t1502412432 * ___data0, Vector3_t2903530434 * ___pos1, Quaternion_t754065749 * ___rot2, const RuntimeMethod* method)
{
	typedef Object_t1502412432 * (*Object_INTERNAL_CALL_Internal_InstantiateSingle_m133179633_ftn) (Object_t1502412432 *, Vector3_t2903530434 *, Quaternion_t754065749 *);
	static Object_INTERNAL_CALL_Internal_InstantiateSingle_m133179633_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Object_INTERNAL_CALL_Internal_InstantiateSingle_m133179633_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Object::INTERNAL_CALL_Internal_InstantiateSingle(UnityEngine.Object,UnityEngine.Vector3&,UnityEngine.Quaternion&)");
	Object_t1502412432 * retVal = _il2cpp_icall_func(___data0, ___pos1, ___rot2);
	return retVal;
}
// UnityEngine.Object UnityEngine.Object::Internal_InstantiateSingleWithParent(UnityEngine.Object,UnityEngine.Transform,UnityEngine.Vector3,UnityEngine.Quaternion)
extern "C"  Object_t1502412432 * Object_Internal_InstantiateSingleWithParent_m3862473257 (RuntimeObject * __this /* static, unused */, Object_t1502412432 * ___data0, Transform_t3316442598 * ___parent1, Vector3_t2903530434  ___pos2, Quaternion_t754065749  ___rot3, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Object_Internal_InstantiateSingleWithParent_m3862473257_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Object_t1502412432 * V_0 = NULL;
	{
		Object_t1502412432 * L_0 = ___data0;
		Transform_t3316442598 * L_1 = ___parent1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1502412432_il2cpp_TypeInfo_var);
		Object_t1502412432 * L_2 = Object_INTERNAL_CALL_Internal_InstantiateSingleWithParent_m3758362270(NULL /*static, unused*/, L_0, L_1, (&___pos2), (&___rot3), /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_0012;
	}

IL_0012:
	{
		Object_t1502412432 * L_3 = V_0;
		return L_3;
	}
}
// UnityEngine.Object UnityEngine.Object::INTERNAL_CALL_Internal_InstantiateSingleWithParent(UnityEngine.Object,UnityEngine.Transform,UnityEngine.Vector3&,UnityEngine.Quaternion&)
extern "C"  Object_t1502412432 * Object_INTERNAL_CALL_Internal_InstantiateSingleWithParent_m3758362270 (RuntimeObject * __this /* static, unused */, Object_t1502412432 * ___data0, Transform_t3316442598 * ___parent1, Vector3_t2903530434 * ___pos2, Quaternion_t754065749 * ___rot3, const RuntimeMethod* method)
{
	typedef Object_t1502412432 * (*Object_INTERNAL_CALL_Internal_InstantiateSingleWithParent_m3758362270_ftn) (Object_t1502412432 *, Transform_t3316442598 *, Vector3_t2903530434 *, Quaternion_t754065749 *);
	static Object_INTERNAL_CALL_Internal_InstantiateSingleWithParent_m3758362270_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Object_INTERNAL_CALL_Internal_InstantiateSingleWithParent_m3758362270_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Object::INTERNAL_CALL_Internal_InstantiateSingleWithParent(UnityEngine.Object,UnityEngine.Transform,UnityEngine.Vector3&,UnityEngine.Quaternion&)");
	Object_t1502412432 * retVal = _il2cpp_icall_func(___data0, ___parent1, ___pos2, ___rot3);
	return retVal;
}
// System.Int32 UnityEngine.Object::GetOffsetOfInstanceIDInCPlusPlusObject()
extern "C"  int32_t Object_GetOffsetOfInstanceIDInCPlusPlusObject_m2713419181 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	typedef int32_t (*Object_GetOffsetOfInstanceIDInCPlusPlusObject_m2713419181_ftn) ();
	static Object_GetOffsetOfInstanceIDInCPlusPlusObject_m2713419181_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Object_GetOffsetOfInstanceIDInCPlusPlusObject_m2713419181_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Object::GetOffsetOfInstanceIDInCPlusPlusObject()");
	int32_t retVal = _il2cpp_icall_func();
	return retVal;
}
// System.Void UnityEngine.Object::EnsureRunningOnMainThread()
extern "C"  void Object_EnsureRunningOnMainThread_m2360275789 (Object_t1502412432 * __this, const RuntimeMethod* method)
{
	typedef void (*Object_EnsureRunningOnMainThread_m2360275789_ftn) (Object_t1502412432 *);
	static Object_EnsureRunningOnMainThread_m2360275789_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Object_EnsureRunningOnMainThread_m2360275789_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Object::EnsureRunningOnMainThread()");
	_il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Object::Destroy(UnityEngine.Object,System.Single)
extern "C"  void Object_Destroy_m3194071194 (RuntimeObject * __this /* static, unused */, Object_t1502412432 * ___obj0, float ___t1, const RuntimeMethod* method)
{
	typedef void (*Object_Destroy_m3194071194_ftn) (Object_t1502412432 *, float);
	static Object_Destroy_m3194071194_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Object_Destroy_m3194071194_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Object::Destroy(UnityEngine.Object,System.Single)");
	_il2cpp_icall_func(___obj0, ___t1);
}
// System.Void UnityEngine.Object::Destroy(UnityEngine.Object)
extern "C"  void Object_Destroy_m1956460533 (RuntimeObject * __this /* static, unused */, Object_t1502412432 * ___obj0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Object_Destroy_m1956460533_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	{
		V_0 = (0.0f);
		Object_t1502412432 * L_0 = ___obj0;
		float L_1 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1502412432_il2cpp_TypeInfo_var);
		Object_Destroy_m3194071194(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Object::DestroyImmediate(UnityEngine.Object,System.Boolean)
extern "C"  void Object_DestroyImmediate_m3321698354 (RuntimeObject * __this /* static, unused */, Object_t1502412432 * ___obj0, bool ___allowDestroyingAssets1, const RuntimeMethod* method)
{
	typedef void (*Object_DestroyImmediate_m3321698354_ftn) (Object_t1502412432 *, bool);
	static Object_DestroyImmediate_m3321698354_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Object_DestroyImmediate_m3321698354_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Object::DestroyImmediate(UnityEngine.Object,System.Boolean)");
	_il2cpp_icall_func(___obj0, ___allowDestroyingAssets1);
}
// System.Void UnityEngine.Object::DestroyImmediate(UnityEngine.Object)
extern "C"  void Object_DestroyImmediate_m2065130163 (RuntimeObject * __this /* static, unused */, Object_t1502412432 * ___obj0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Object_DestroyImmediate_m2065130163_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	{
		V_0 = (bool)0;
		Object_t1502412432 * L_0 = ___obj0;
		bool L_1 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1502412432_il2cpp_TypeInfo_var);
		Object_DestroyImmediate_m3321698354(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Object[] UnityEngine.Object::FindObjectsOfType(System.Type)
extern "C"  ObjectU5BU5D_t760538417* Object_FindObjectsOfType_m1430252051 (RuntimeObject * __this /* static, unused */, Type_t * ___type0, const RuntimeMethod* method)
{
	typedef ObjectU5BU5D_t760538417* (*Object_FindObjectsOfType_m1430252051_ftn) (Type_t *);
	static Object_FindObjectsOfType_m1430252051_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Object_FindObjectsOfType_m1430252051_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Object::FindObjectsOfType(System.Type)");
	ObjectU5BU5D_t760538417* retVal = _il2cpp_icall_func(___type0);
	return retVal;
}
// System.String UnityEngine.Object::get_name()
extern "C"  String_t* Object_get_name_m27432942 (Object_t1502412432 * __this, const RuntimeMethod* method)
{
	typedef String_t* (*Object_get_name_m27432942_ftn) (Object_t1502412432 *);
	static Object_get_name_m27432942_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Object_get_name_m27432942_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Object::get_name()");
	String_t* retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// System.Void UnityEngine.Object::set_name(System.String)
extern "C"  void Object_set_name_m224801159 (Object_t1502412432 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	typedef void (*Object_set_name_m224801159_ftn) (Object_t1502412432 *, String_t*);
	static Object_set_name_m224801159_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Object_set_name_m224801159_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Object::set_name(System.String)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.Object::DontDestroyOnLoad(UnityEngine.Object)
extern "C"  void Object_DontDestroyOnLoad_m2684895394 (RuntimeObject * __this /* static, unused */, Object_t1502412432 * ___target0, const RuntimeMethod* method)
{
	typedef void (*Object_DontDestroyOnLoad_m2684895394_ftn) (Object_t1502412432 *);
	static Object_DontDestroyOnLoad_m2684895394_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Object_DontDestroyOnLoad_m2684895394_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Object::DontDestroyOnLoad(UnityEngine.Object)");
	_il2cpp_icall_func(___target0);
}
// UnityEngine.HideFlags UnityEngine.Object::get_hideFlags()
extern "C"  int32_t Object_get_hideFlags_m2005647399 (Object_t1502412432 * __this, const RuntimeMethod* method)
{
	typedef int32_t (*Object_get_hideFlags_m2005647399_ftn) (Object_t1502412432 *);
	static Object_get_hideFlags_m2005647399_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Object_get_hideFlags_m2005647399_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Object::get_hideFlags()");
	int32_t retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// System.Void UnityEngine.Object::set_hideFlags(UnityEngine.HideFlags)
extern "C"  void Object_set_hideFlags_m3713818453 (Object_t1502412432 * __this, int32_t ___value0, const RuntimeMethod* method)
{
	typedef void (*Object_set_hideFlags_m3713818453_ftn) (Object_t1502412432 *, int32_t);
	static Object_set_hideFlags_m3713818453_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Object_set_hideFlags_m3713818453_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Object::set_hideFlags(UnityEngine.HideFlags)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.Object::DestroyObject(UnityEngine.Object,System.Single)
extern "C"  void Object_DestroyObject_m2199032474 (RuntimeObject * __this /* static, unused */, Object_t1502412432 * ___obj0, float ___t1, const RuntimeMethod* method)
{
	typedef void (*Object_DestroyObject_m2199032474_ftn) (Object_t1502412432 *, float);
	static Object_DestroyObject_m2199032474_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Object_DestroyObject_m2199032474_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Object::DestroyObject(UnityEngine.Object,System.Single)");
	_il2cpp_icall_func(___obj0, ___t1);
}
// System.Void UnityEngine.Object::DestroyObject(UnityEngine.Object)
extern "C"  void Object_DestroyObject_m1837997780 (RuntimeObject * __this /* static, unused */, Object_t1502412432 * ___obj0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Object_DestroyObject_m1837997780_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	{
		V_0 = (0.0f);
		Object_t1502412432 * L_0 = ___obj0;
		float L_1 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1502412432_il2cpp_TypeInfo_var);
		Object_DestroyObject_m2199032474(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Object[] UnityEngine.Object::FindSceneObjectsOfType(System.Type)
extern "C"  ObjectU5BU5D_t760538417* Object_FindSceneObjectsOfType_m2031891302 (RuntimeObject * __this /* static, unused */, Type_t * ___type0, const RuntimeMethod* method)
{
	typedef ObjectU5BU5D_t760538417* (*Object_FindSceneObjectsOfType_m2031891302_ftn) (Type_t *);
	static Object_FindSceneObjectsOfType_m2031891302_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Object_FindSceneObjectsOfType_m2031891302_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Object::FindSceneObjectsOfType(System.Type)");
	ObjectU5BU5D_t760538417* retVal = _il2cpp_icall_func(___type0);
	return retVal;
}
// UnityEngine.Object[] UnityEngine.Object::FindObjectsOfTypeIncludingAssets(System.Type)
extern "C"  ObjectU5BU5D_t760538417* Object_FindObjectsOfTypeIncludingAssets_m2261747034 (RuntimeObject * __this /* static, unused */, Type_t * ___type0, const RuntimeMethod* method)
{
	typedef ObjectU5BU5D_t760538417* (*Object_FindObjectsOfTypeIncludingAssets_m2261747034_ftn) (Type_t *);
	static Object_FindObjectsOfTypeIncludingAssets_m2261747034_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Object_FindObjectsOfTypeIncludingAssets_m2261747034_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Object::FindObjectsOfTypeIncludingAssets(System.Type)");
	ObjectU5BU5D_t760538417* retVal = _il2cpp_icall_func(___type0);
	return retVal;
}
// System.String UnityEngine.Object::ToString()
extern "C"  String_t* Object_ToString_m671274826 (Object_t1502412432 * __this, const RuntimeMethod* method)
{
	typedef String_t* (*Object_ToString_m671274826_ftn) (Object_t1502412432 *);
	static Object_ToString_m671274826_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Object_ToString_m671274826_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Object::ToString()");
	String_t* retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// System.Boolean UnityEngine.Object::DoesObjectWithInstanceIDExist(System.Int32)
extern "C"  bool Object_DoesObjectWithInstanceIDExist_m4176389106 (RuntimeObject * __this /* static, unused */, int32_t ___instanceID0, const RuntimeMethod* method)
{
	typedef bool (*Object_DoesObjectWithInstanceIDExist_m4176389106_ftn) (int32_t);
	static Object_DoesObjectWithInstanceIDExist_m4176389106_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Object_DoesObjectWithInstanceIDExist_m4176389106_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Object::DoesObjectWithInstanceIDExist(System.Int32)");
	bool retVal = _il2cpp_icall_func(___instanceID0);
	return retVal;
}
// System.Int32 UnityEngine.Object::GetInstanceID()
extern "C"  int32_t Object_GetInstanceID_m3083837515 (Object_t1502412432 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Object_GetInstanceID_m3083837515_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		IntPtr_t L_0 = __this->get_m_CachedPtr_0();
		IntPtr_t L_1 = ((IntPtr_t_StaticFields*)il2cpp_codegen_static_fields_for(IntPtr_t_il2cpp_TypeInfo_var))->get_Zero_1();
		bool L_2 = IntPtr_op_Equality_m1136306456(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_001d;
		}
	}
	{
		V_0 = 0;
		goto IL_0056;
	}

IL_001d:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1502412432_il2cpp_TypeInfo_var);
		int32_t L_3 = ((Object_t1502412432_StaticFields*)il2cpp_codegen_static_fields_for(Object_t1502412432_il2cpp_TypeInfo_var))->get_OffsetOfInstanceIDInCPlusPlusObject_1();
		if ((!(((uint32_t)L_3) == ((uint32_t)(-1)))))
		{
			goto IL_0032;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1502412432_il2cpp_TypeInfo_var);
		int32_t L_4 = Object_GetOffsetOfInstanceIDInCPlusPlusObject_m2713419181(NULL /*static, unused*/, /*hidden argument*/NULL);
		((Object_t1502412432_StaticFields*)il2cpp_codegen_static_fields_for(Object_t1502412432_il2cpp_TypeInfo_var))->set_OffsetOfInstanceIDInCPlusPlusObject_1(L_4);
	}

IL_0032:
	{
		IntPtr_t* L_5 = __this->get_address_of_m_CachedPtr_0();
		int64_t L_6 = IntPtr_ToInt64_m616010533(L_5, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1502412432_il2cpp_TypeInfo_var);
		int32_t L_7 = ((Object_t1502412432_StaticFields*)il2cpp_codegen_static_fields_for(Object_t1502412432_il2cpp_TypeInfo_var))->get_OffsetOfInstanceIDInCPlusPlusObject_1();
		IntPtr_t L_8;
		memset(&L_8, 0, sizeof(L_8));
		IntPtr__ctor_m2295693947((&L_8), ((int64_t)((int64_t)L_6+(int64_t)(((int64_t)((int64_t)L_7))))), /*hidden argument*/NULL);
		void* L_9 = IntPtr_op_Explicit_m532509050(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		V_0 = (*((int32_t*)L_9));
		goto IL_0056;
	}

IL_0056:
	{
		int32_t L_10 = V_0;
		return L_10;
	}
}
// System.Int32 UnityEngine.Object::GetHashCode()
extern "C"  int32_t Object_GetHashCode_m3137810806 (Object_t1502412432 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = Object_GetHashCode_m203225167(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		int32_t L_1 = V_0;
		return L_1;
	}
}
// System.Boolean UnityEngine.Object::Equals(System.Object)
extern "C"  bool Object_Equals_m4179840469 (Object_t1502412432 * __this, RuntimeObject * ___other0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Object_Equals_m4179840469_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Object_t1502412432 * V_0 = NULL;
	bool V_1 = false;
	{
		RuntimeObject * L_0 = ___other0;
		V_0 = ((Object_t1502412432 *)IsInstClass((RuntimeObject*)L_0, Object_t1502412432_il2cpp_TypeInfo_var));
		Object_t1502412432 * L_1 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1502412432_il2cpp_TypeInfo_var);
		bool L_2 = Object_op_Equality_m2342070775(NULL /*static, unused*/, L_1, (Object_t1502412432 *)NULL, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_002c;
		}
	}
	{
		RuntimeObject * L_3 = ___other0;
		if (!L_3)
		{
			goto IL_002c;
		}
	}
	{
		RuntimeObject * L_4 = ___other0;
		if (((Object_t1502412432 *)IsInstClass((RuntimeObject*)L_4, Object_t1502412432_il2cpp_TypeInfo_var)))
		{
			goto IL_002c;
		}
	}
	{
		V_1 = (bool)0;
		goto IL_0039;
	}

IL_002c:
	{
		Object_t1502412432 * L_5 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1502412432_il2cpp_TypeInfo_var);
		bool L_6 = Object_CompareBaseObjects_m3108723731(NULL /*static, unused*/, __this, L_5, /*hidden argument*/NULL);
		V_1 = L_6;
		goto IL_0039;
	}

IL_0039:
	{
		bool L_7 = V_1;
		return L_7;
	}
}
// System.Boolean UnityEngine.Object::op_Implicit(UnityEngine.Object)
extern "C"  bool Object_op_Implicit_m2314733868 (RuntimeObject * __this /* static, unused */, Object_t1502412432 * ___exists0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Object_op_Implicit_m2314733868_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	{
		Object_t1502412432 * L_0 = ___exists0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1502412432_il2cpp_TypeInfo_var);
		bool L_1 = Object_CompareBaseObjects_m3108723731(NULL /*static, unused*/, L_0, (Object_t1502412432 *)NULL, /*hidden argument*/NULL);
		V_0 = (bool)((((int32_t)L_1) == ((int32_t)0))? 1 : 0);
		goto IL_0011;
	}

IL_0011:
	{
		bool L_2 = V_0;
		return L_2;
	}
}
// System.Boolean UnityEngine.Object::CompareBaseObjects(UnityEngine.Object,UnityEngine.Object)
extern "C"  bool Object_CompareBaseObjects_m3108723731 (RuntimeObject * __this /* static, unused */, Object_t1502412432 * ___lhs0, Object_t1502412432 * ___rhs1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Object_CompareBaseObjects_m3108723731_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	bool V_1 = false;
	bool V_2 = false;
	{
		Object_t1502412432 * L_0 = ___lhs0;
		V_0 = (bool)((((RuntimeObject*)(Object_t1502412432 *)L_0) == ((RuntimeObject*)(RuntimeObject *)NULL))? 1 : 0);
		Object_t1502412432 * L_1 = ___rhs1;
		V_1 = (bool)((((RuntimeObject*)(Object_t1502412432 *)L_1) == ((RuntimeObject*)(RuntimeObject *)NULL))? 1 : 0);
		bool L_2 = V_1;
		if (!L_2)
		{
			goto IL_001e;
		}
	}
	{
		bool L_3 = V_0;
		if (!L_3)
		{
			goto IL_001e;
		}
	}
	{
		V_2 = (bool)1;
		goto IL_0055;
	}

IL_001e:
	{
		bool L_4 = V_1;
		if (!L_4)
		{
			goto IL_0033;
		}
	}
	{
		Object_t1502412432 * L_5 = ___lhs0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1502412432_il2cpp_TypeInfo_var);
		bool L_6 = Object_IsNativeObjectAlive_m3228292766(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		V_2 = (bool)((((int32_t)L_6) == ((int32_t)0))? 1 : 0);
		goto IL_0055;
	}

IL_0033:
	{
		bool L_7 = V_0;
		if (!L_7)
		{
			goto IL_0048;
		}
	}
	{
		Object_t1502412432 * L_8 = ___rhs1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1502412432_il2cpp_TypeInfo_var);
		bool L_9 = Object_IsNativeObjectAlive_m3228292766(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		V_2 = (bool)((((int32_t)L_9) == ((int32_t)0))? 1 : 0);
		goto IL_0055;
	}

IL_0048:
	{
		Object_t1502412432 * L_10 = ___lhs0;
		Object_t1502412432 * L_11 = ___rhs1;
		bool L_12 = Object_ReferenceEquals_m2014193643(NULL /*static, unused*/, L_10, L_11, /*hidden argument*/NULL);
		V_2 = L_12;
		goto IL_0055;
	}

IL_0055:
	{
		bool L_13 = V_2;
		return L_13;
	}
}
// System.Boolean UnityEngine.Object::IsNativeObjectAlive(UnityEngine.Object)
extern "C"  bool Object_IsNativeObjectAlive_m3228292766 (RuntimeObject * __this /* static, unused */, Object_t1502412432 * ___o0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Object_IsNativeObjectAlive_m3228292766_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	{
		Object_t1502412432 * L_0 = ___o0;
		NullCheck(L_0);
		IntPtr_t L_1 = Object_GetCachedPtr_m627724031(L_0, /*hidden argument*/NULL);
		IntPtr_t L_2 = ((IntPtr_t_StaticFields*)il2cpp_codegen_static_fields_for(IntPtr_t_il2cpp_TypeInfo_var))->get_Zero_1();
		bool L_3 = IntPtr_op_Inequality_m1842201359(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		goto IL_0017;
	}

IL_0017:
	{
		bool L_4 = V_0;
		return L_4;
	}
}
// System.IntPtr UnityEngine.Object::GetCachedPtr()
extern "C"  IntPtr_t Object_GetCachedPtr_m627724031 (Object_t1502412432 * __this, const RuntimeMethod* method)
{
	IntPtr_t V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		IntPtr_t L_0 = __this->get_m_CachedPtr_0();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		IntPtr_t L_1 = V_0;
		return L_1;
	}
}
// UnityEngine.Object UnityEngine.Object::Instantiate(UnityEngine.Object,UnityEngine.Vector3,UnityEngine.Quaternion)
extern "C"  Object_t1502412432 * Object_Instantiate_m1272509960 (RuntimeObject * __this /* static, unused */, Object_t1502412432 * ___original0, Vector3_t2903530434  ___position1, Quaternion_t754065749  ___rotation2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Object_Instantiate_m1272509960_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Object_t1502412432 * V_0 = NULL;
	{
		Object_t1502412432 * L_0 = ___original0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1502412432_il2cpp_TypeInfo_var);
		Object_CheckNullArgument_m2850461954(NULL /*static, unused*/, L_0, _stringLiteral867517236, /*hidden argument*/NULL);
		Object_t1502412432 * L_1 = ___original0;
		if (!((ScriptableObject_t3635579074 *)IsInstClass((RuntimeObject*)L_1, ScriptableObject_t3635579074_il2cpp_TypeInfo_var)))
		{
			goto IL_0022;
		}
	}
	{
		ArgumentException_t4028401650 * L_2 = (ArgumentException_t4028401650 *)il2cpp_codegen_object_new(ArgumentException_t4028401650_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m1186658332(L_2, _stringLiteral3130680061, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0022:
	{
		Object_t1502412432 * L_3 = ___original0;
		Vector3_t2903530434  L_4 = ___position1;
		Quaternion_t754065749  L_5 = ___rotation2;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1502412432_il2cpp_TypeInfo_var);
		Object_t1502412432 * L_6 = Object_Internal_InstantiateSingle_m664294789(NULL /*static, unused*/, L_3, L_4, L_5, /*hidden argument*/NULL);
		V_0 = L_6;
		goto IL_0030;
	}

IL_0030:
	{
		Object_t1502412432 * L_7 = V_0;
		return L_7;
	}
}
// UnityEngine.Object UnityEngine.Object::Instantiate(UnityEngine.Object,UnityEngine.Vector3,UnityEngine.Quaternion,UnityEngine.Transform)
extern "C"  Object_t1502412432 * Object_Instantiate_m1165988047 (RuntimeObject * __this /* static, unused */, Object_t1502412432 * ___original0, Vector3_t2903530434  ___position1, Quaternion_t754065749  ___rotation2, Transform_t3316442598 * ___parent3, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Object_Instantiate_m1165988047_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Object_t1502412432 * V_0 = NULL;
	{
		Transform_t3316442598 * L_0 = ___parent3;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1502412432_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_m2342070775(NULL /*static, unused*/, L_0, (Object_t1502412432 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001b;
		}
	}
	{
		Object_t1502412432 * L_2 = ___original0;
		Vector3_t2903530434  L_3 = ___position1;
		Quaternion_t754065749  L_4 = ___rotation2;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1502412432_il2cpp_TypeInfo_var);
		Object_t1502412432 * L_5 = Object_Internal_InstantiateSingle_m664294789(NULL /*static, unused*/, L_2, L_3, L_4, /*hidden argument*/NULL);
		V_0 = L_5;
		goto IL_0035;
	}

IL_001b:
	{
		Object_t1502412432 * L_6 = ___original0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1502412432_il2cpp_TypeInfo_var);
		Object_CheckNullArgument_m2850461954(NULL /*static, unused*/, L_6, _stringLiteral867517236, /*hidden argument*/NULL);
		Object_t1502412432 * L_7 = ___original0;
		Transform_t3316442598 * L_8 = ___parent3;
		Vector3_t2903530434  L_9 = ___position1;
		Quaternion_t754065749  L_10 = ___rotation2;
		Object_t1502412432 * L_11 = Object_Internal_InstantiateSingleWithParent_m3862473257(NULL /*static, unused*/, L_7, L_8, L_9, L_10, /*hidden argument*/NULL);
		V_0 = L_11;
		goto IL_0035;
	}

IL_0035:
	{
		Object_t1502412432 * L_12 = V_0;
		return L_12;
	}
}
// UnityEngine.Object UnityEngine.Object::Instantiate(UnityEngine.Object)
extern "C"  Object_t1502412432 * Object_Instantiate_m856891812 (RuntimeObject * __this /* static, unused */, Object_t1502412432 * ___original0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Object_Instantiate_m856891812_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Object_t1502412432 * V_0 = NULL;
	{
		Object_t1502412432 * L_0 = ___original0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1502412432_il2cpp_TypeInfo_var);
		Object_CheckNullArgument_m2850461954(NULL /*static, unused*/, L_0, _stringLiteral867517236, /*hidden argument*/NULL);
		Object_t1502412432 * L_1 = ___original0;
		Object_t1502412432 * L_2 = Object_Internal_CloneSingle_m2326967574(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_0018;
	}

IL_0018:
	{
		Object_t1502412432 * L_3 = V_0;
		return L_3;
	}
}
// UnityEngine.Object UnityEngine.Object::Instantiate(UnityEngine.Object,UnityEngine.Transform)
extern "C"  Object_t1502412432 * Object_Instantiate_m2869586105 (RuntimeObject * __this /* static, unused */, Object_t1502412432 * ___original0, Transform_t3316442598 * ___parent1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Object_Instantiate_m2869586105_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Object_t1502412432 * V_0 = NULL;
	{
		Object_t1502412432 * L_0 = ___original0;
		Transform_t3316442598 * L_1 = ___parent1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1502412432_il2cpp_TypeInfo_var);
		Object_t1502412432 * L_2 = Object_Instantiate_m914959561(NULL /*static, unused*/, L_0, L_1, (bool)0, /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_000f;
	}

IL_000f:
	{
		Object_t1502412432 * L_3 = V_0;
		return L_3;
	}
}
// UnityEngine.Object UnityEngine.Object::Instantiate(UnityEngine.Object,UnityEngine.Transform,System.Boolean)
extern "C"  Object_t1502412432 * Object_Instantiate_m914959561 (RuntimeObject * __this /* static, unused */, Object_t1502412432 * ___original0, Transform_t3316442598 * ___parent1, bool ___instantiateInWorldSpace2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Object_Instantiate_m914959561_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Object_t1502412432 * V_0 = NULL;
	{
		Transform_t3316442598 * L_0 = ___parent1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1502412432_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_m2342070775(NULL /*static, unused*/, L_0, (Object_t1502412432 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0019;
		}
	}
	{
		Object_t1502412432 * L_2 = ___original0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1502412432_il2cpp_TypeInfo_var);
		Object_t1502412432 * L_3 = Object_Internal_CloneSingle_m2326967574(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		goto IL_0032;
	}

IL_0019:
	{
		Object_t1502412432 * L_4 = ___original0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1502412432_il2cpp_TypeInfo_var);
		Object_CheckNullArgument_m2850461954(NULL /*static, unused*/, L_4, _stringLiteral867517236, /*hidden argument*/NULL);
		Object_t1502412432 * L_5 = ___original0;
		Transform_t3316442598 * L_6 = ___parent1;
		bool L_7 = ___instantiateInWorldSpace2;
		Object_t1502412432 * L_8 = Object_Internal_CloneSingleWithParent_m973191133(NULL /*static, unused*/, L_5, L_6, L_7, /*hidden argument*/NULL);
		V_0 = L_8;
		goto IL_0032;
	}

IL_0032:
	{
		Object_t1502412432 * L_9 = V_0;
		return L_9;
	}
}
// System.Void UnityEngine.Object::CheckNullArgument(System.Object,System.String)
extern "C"  void Object_CheckNullArgument_m2850461954 (RuntimeObject * __this /* static, unused */, RuntimeObject * ___arg0, String_t* ___message1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Object_CheckNullArgument_m2850461954_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		RuntimeObject * L_0 = ___arg0;
		if (L_0)
		{
			goto IL_000e;
		}
	}
	{
		String_t* L_1 = ___message1;
		ArgumentException_t4028401650 * L_2 = (ArgumentException_t4028401650 *)il2cpp_codegen_object_new(ArgumentException_t4028401650_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m1186658332(L_2, L_1, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_000e:
	{
		return;
	}
}
// UnityEngine.Object UnityEngine.Object::FindObjectOfType(System.Type)
extern "C"  Object_t1502412432 * Object_FindObjectOfType_m56264973 (RuntimeObject * __this /* static, unused */, Type_t * ___type0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Object_FindObjectOfType_m56264973_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ObjectU5BU5D_t760538417* V_0 = NULL;
	Object_t1502412432 * V_1 = NULL;
	{
		Type_t * L_0 = ___type0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1502412432_il2cpp_TypeInfo_var);
		ObjectU5BU5D_t760538417* L_1 = Object_FindObjectsOfType_m1430252051(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		ObjectU5BU5D_t760538417* L_2 = V_0;
		NullCheck(L_2);
		if ((((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_2)->max_length))))) <= ((int32_t)0)))
		{
			goto IL_001a;
		}
	}
	{
		ObjectU5BU5D_t760538417* L_3 = V_0;
		NullCheck(L_3);
		int32_t L_4 = 0;
		Object_t1502412432 * L_5 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_4));
		V_1 = L_5;
		goto IL_0021;
	}

IL_001a:
	{
		V_1 = (Object_t1502412432 *)NULL;
		goto IL_0021;
	}

IL_0021:
	{
		Object_t1502412432 * L_6 = V_1;
		return L_6;
	}
}
// System.Boolean UnityEngine.Object::op_Equality(UnityEngine.Object,UnityEngine.Object)
extern "C"  bool Object_op_Equality_m2342070775 (RuntimeObject * __this /* static, unused */, Object_t1502412432 * ___x0, Object_t1502412432 * ___y1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Object_op_Equality_m2342070775_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	{
		Object_t1502412432 * L_0 = ___x0;
		Object_t1502412432 * L_1 = ___y1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1502412432_il2cpp_TypeInfo_var);
		bool L_2 = Object_CompareBaseObjects_m3108723731(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_000e;
	}

IL_000e:
	{
		bool L_3 = V_0;
		return L_3;
	}
}
// System.Boolean UnityEngine.Object::op_Inequality(UnityEngine.Object,UnityEngine.Object)
extern "C"  bool Object_op_Inequality_m1918954159 (RuntimeObject * __this /* static, unused */, Object_t1502412432 * ___x0, Object_t1502412432 * ___y1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Object_op_Inequality_m1918954159_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	{
		Object_t1502412432 * L_0 = ___x0;
		Object_t1502412432 * L_1 = ___y1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1502412432_il2cpp_TypeInfo_var);
		bool L_2 = Object_CompareBaseObjects_m3108723731(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		V_0 = (bool)((((int32_t)L_2) == ((int32_t)0))? 1 : 0);
		goto IL_0011;
	}

IL_0011:
	{
		bool L_3 = V_0;
		return L_3;
	}
}
// System.Void UnityEngine.Object::.cctor()
extern "C"  void Object__cctor_m2578442528 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Object__cctor_m2578442528_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		((Object_t1502412432_StaticFields*)il2cpp_codegen_static_fields_for(Object_t1502412432_il2cpp_TypeInfo_var))->set_OffsetOfInstanceIDInCPlusPlusObject_1((-1));
		return;
	}
}
// UnityEngine.ParticleSystem/MainModule UnityEngine.ParticleSystem::get_main()
extern "C"  MainModule_t2858435356  ParticleSystem_get_main_m2209723276 (ParticleSystem_t1912383059 * __this, const RuntimeMethod* method)
{
	MainModule_t2858435356  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		MainModule_t2858435356  L_0;
		memset(&L_0, 0, sizeof(L_0));
		MainModule__ctor_m2253476806((&L_0), __this, /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		MainModule_t2858435356  L_1 = V_0;
		return L_1;
	}
}
// UnityEngine.ParticleSystem/EmissionModule UnityEngine.ParticleSystem::get_emission()
extern "C"  EmissionModule_t216550349  ParticleSystem_get_emission_m1229628441 (ParticleSystem_t1912383059 * __this, const RuntimeMethod* method)
{
	EmissionModule_t216550349  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		EmissionModule_t216550349  L_0;
		memset(&L_0, 0, sizeof(L_0));
		EmissionModule__ctor_m1223807977((&L_0), __this, /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		EmissionModule_t216550349  L_1 = V_0;
		return L_1;
	}
}
// Conversion methods for marshalling of: UnityEngine.ParticleSystem/EmissionModule
extern "C" void EmissionModule_t216550349_marshal_pinvoke(const EmissionModule_t216550349& unmarshaled, EmissionModule_t216550349_marshaled_pinvoke& marshaled)
{
	Il2CppCodeGenException* ___m_ParticleSystem_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_ParticleSystem' of type 'EmissionModule': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_ParticleSystem_0Exception);
}
extern "C" void EmissionModule_t216550349_marshal_pinvoke_back(const EmissionModule_t216550349_marshaled_pinvoke& marshaled, EmissionModule_t216550349& unmarshaled)
{
	Il2CppCodeGenException* ___m_ParticleSystem_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_ParticleSystem' of type 'EmissionModule': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_ParticleSystem_0Exception);
}
// Conversion method for clean up from marshalling of: UnityEngine.ParticleSystem/EmissionModule
extern "C" void EmissionModule_t216550349_marshal_pinvoke_cleanup(EmissionModule_t216550349_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.ParticleSystem/EmissionModule
extern "C" void EmissionModule_t216550349_marshal_com(const EmissionModule_t216550349& unmarshaled, EmissionModule_t216550349_marshaled_com& marshaled)
{
	Il2CppCodeGenException* ___m_ParticleSystem_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_ParticleSystem' of type 'EmissionModule': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_ParticleSystem_0Exception);
}
extern "C" void EmissionModule_t216550349_marshal_com_back(const EmissionModule_t216550349_marshaled_com& marshaled, EmissionModule_t216550349& unmarshaled)
{
	Il2CppCodeGenException* ___m_ParticleSystem_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_ParticleSystem' of type 'EmissionModule': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_ParticleSystem_0Exception);
}
// Conversion method for clean up from marshalling of: UnityEngine.ParticleSystem/EmissionModule
extern "C" void EmissionModule_t216550349_marshal_com_cleanup(EmissionModule_t216550349_marshaled_com& marshaled)
{
}
// System.Void UnityEngine.ParticleSystem/EmissionModule::.ctor(UnityEngine.ParticleSystem)
extern "C"  void EmissionModule__ctor_m1223807977 (EmissionModule_t216550349 * __this, ParticleSystem_t1912383059 * ___particleSystem0, const RuntimeMethod* method)
{
	{
		ParticleSystem_t1912383059 * L_0 = ___particleSystem0;
		__this->set_m_ParticleSystem_0(L_0);
		return;
	}
}
extern "C"  void EmissionModule__ctor_m1223807977_AdjustorThunk (RuntimeObject * __this, ParticleSystem_t1912383059 * ___particleSystem0, const RuntimeMethod* method)
{
	EmissionModule_t216550349 * _thisAdjusted = reinterpret_cast<EmissionModule_t216550349 *>(__this + 1);
	EmissionModule__ctor_m1223807977(_thisAdjusted, ___particleSystem0, method);
}
// System.Void UnityEngine.ParticleSystem/EmissionModule::set_enabled(System.Boolean)
extern "C"  void EmissionModule_set_enabled_m2770978577 (EmissionModule_t216550349 * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		ParticleSystem_t1912383059 * L_0 = __this->get_m_ParticleSystem_0();
		bool L_1 = ___value0;
		EmissionModule_SetEnabled_m1349267330(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
extern "C"  void EmissionModule_set_enabled_m2770978577_AdjustorThunk (RuntimeObject * __this, bool ___value0, const RuntimeMethod* method)
{
	EmissionModule_t216550349 * _thisAdjusted = reinterpret_cast<EmissionModule_t216550349 *>(__this + 1);
	EmissionModule_set_enabled_m2770978577(_thisAdjusted, ___value0, method);
}
// System.Void UnityEngine.ParticleSystem/EmissionModule::SetEnabled(UnityEngine.ParticleSystem,System.Boolean)
extern "C"  void EmissionModule_SetEnabled_m1349267330 (RuntimeObject * __this /* static, unused */, ParticleSystem_t1912383059 * ___system0, bool ___value1, const RuntimeMethod* method)
{
	typedef void (*EmissionModule_SetEnabled_m1349267330_ftn) (ParticleSystem_t1912383059 *, bool);
	static EmissionModule_SetEnabled_m1349267330_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (EmissionModule_SetEnabled_m1349267330_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystem/EmissionModule::SetEnabled(UnityEngine.ParticleSystem,System.Boolean)");
	_il2cpp_icall_func(___system0, ___value1);
}
// Conversion methods for marshalling of: UnityEngine.ParticleSystem/MainModule
extern "C" void MainModule_t2858435356_marshal_pinvoke(const MainModule_t2858435356& unmarshaled, MainModule_t2858435356_marshaled_pinvoke& marshaled)
{
	Il2CppCodeGenException* ___m_ParticleSystem_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_ParticleSystem' of type 'MainModule': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_ParticleSystem_0Exception);
}
extern "C" void MainModule_t2858435356_marshal_pinvoke_back(const MainModule_t2858435356_marshaled_pinvoke& marshaled, MainModule_t2858435356& unmarshaled)
{
	Il2CppCodeGenException* ___m_ParticleSystem_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_ParticleSystem' of type 'MainModule': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_ParticleSystem_0Exception);
}
// Conversion method for clean up from marshalling of: UnityEngine.ParticleSystem/MainModule
extern "C" void MainModule_t2858435356_marshal_pinvoke_cleanup(MainModule_t2858435356_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.ParticleSystem/MainModule
extern "C" void MainModule_t2858435356_marshal_com(const MainModule_t2858435356& unmarshaled, MainModule_t2858435356_marshaled_com& marshaled)
{
	Il2CppCodeGenException* ___m_ParticleSystem_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_ParticleSystem' of type 'MainModule': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_ParticleSystem_0Exception);
}
extern "C" void MainModule_t2858435356_marshal_com_back(const MainModule_t2858435356_marshaled_com& marshaled, MainModule_t2858435356& unmarshaled)
{
	Il2CppCodeGenException* ___m_ParticleSystem_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_ParticleSystem' of type 'MainModule': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_ParticleSystem_0Exception);
}
// Conversion method for clean up from marshalling of: UnityEngine.ParticleSystem/MainModule
extern "C" void MainModule_t2858435356_marshal_com_cleanup(MainModule_t2858435356_marshaled_com& marshaled)
{
}
// System.Void UnityEngine.ParticleSystem/MainModule::.ctor(UnityEngine.ParticleSystem)
extern "C"  void MainModule__ctor_m2253476806 (MainModule_t2858435356 * __this, ParticleSystem_t1912383059 * ___particleSystem0, const RuntimeMethod* method)
{
	{
		ParticleSystem_t1912383059 * L_0 = ___particleSystem0;
		__this->set_m_ParticleSystem_0(L_0);
		return;
	}
}
extern "C"  void MainModule__ctor_m2253476806_AdjustorThunk (RuntimeObject * __this, ParticleSystem_t1912383059 * ___particleSystem0, const RuntimeMethod* method)
{
	MainModule_t2858435356 * _thisAdjusted = reinterpret_cast<MainModule_t2858435356 *>(__this + 1);
	MainModule__ctor_m2253476806(_thisAdjusted, ___particleSystem0, method);
}
// UnityEngine.ParticleSystem/MinMaxCurve UnityEngine.ParticleSystem/MainModule::get_startLifetime()
extern "C"  MinMaxCurve_t2961159436  MainModule_get_startLifetime_m2433047371 (MainModule_t2858435356 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MainModule_get_startLifetime_m2433047371_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	MinMaxCurve_t2961159436  V_0;
	memset(&V_0, 0, sizeof(V_0));
	MinMaxCurve_t2961159436  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		Initobj (MinMaxCurve_t2961159436_il2cpp_TypeInfo_var, (&V_0));
		ParticleSystem_t1912383059 * L_0 = __this->get_m_ParticleSystem_0();
		MainModule_GetStartLifetime_m1262381546(NULL /*static, unused*/, L_0, (&V_0), /*hidden argument*/NULL);
		MinMaxCurve_t2961159436  L_1 = V_0;
		V_1 = L_1;
		goto IL_001d;
	}

IL_001d:
	{
		MinMaxCurve_t2961159436  L_2 = V_1;
		return L_2;
	}
}
extern "C"  MinMaxCurve_t2961159436  MainModule_get_startLifetime_m2433047371_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	MainModule_t2858435356 * _thisAdjusted = reinterpret_cast<MainModule_t2858435356 *>(__this + 1);
	return MainModule_get_startLifetime_m2433047371(_thisAdjusted, method);
}
// System.Void UnityEngine.ParticleSystem/MainModule::GetStartLifetime(UnityEngine.ParticleSystem,UnityEngine.ParticleSystem/MinMaxCurve&)
extern "C"  void MainModule_GetStartLifetime_m1262381546 (RuntimeObject * __this /* static, unused */, ParticleSystem_t1912383059 * ___system0, MinMaxCurve_t2961159436 * ___curve1, const RuntimeMethod* method)
{
	typedef void (*MainModule_GetStartLifetime_m1262381546_ftn) (ParticleSystem_t1912383059 *, MinMaxCurve_t2961159436 *);
	static MainModule_GetStartLifetime_m1262381546_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (MainModule_GetStartLifetime_m1262381546_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystem/MainModule::GetStartLifetime(UnityEngine.ParticleSystem,UnityEngine.ParticleSystem/MinMaxCurve&)");
	_il2cpp_icall_func(___system0, ___curve1);
}




// Conversion methods for marshalling of: UnityEngine.ParticleSystem/MinMaxCurve
extern "C" void MinMaxCurve_t2961159436_marshal_pinvoke(const MinMaxCurve_t2961159436& unmarshaled, MinMaxCurve_t2961159436_marshaled_pinvoke& marshaled)
{
	marshaled.___m_Mode_0 = unmarshaled.get_m_Mode_0();
	marshaled.___m_CurveMultiplier_1 = unmarshaled.get_m_CurveMultiplier_1();
	if (unmarshaled.get_m_CurveMin_2() != NULL) AnimationCurve_t2184836714_marshal_pinvoke(*unmarshaled.get_m_CurveMin_2(), marshaled.___m_CurveMin_2);
	if (unmarshaled.get_m_CurveMax_3() != NULL) AnimationCurve_t2184836714_marshal_pinvoke(*unmarshaled.get_m_CurveMax_3(), marshaled.___m_CurveMax_3);
	marshaled.___m_ConstantMin_4 = unmarshaled.get_m_ConstantMin_4();
	marshaled.___m_ConstantMax_5 = unmarshaled.get_m_ConstantMax_5();
}
extern "C" void MinMaxCurve_t2961159436_marshal_pinvoke_back(const MinMaxCurve_t2961159436_marshaled_pinvoke& marshaled, MinMaxCurve_t2961159436& unmarshaled)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MinMaxCurve_t2961159436_pinvoke_FromNativeMethodDefinition_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t unmarshaled_m_Mode_temp_0 = 0;
	unmarshaled_m_Mode_temp_0 = marshaled.___m_Mode_0;
	unmarshaled.set_m_Mode_0(unmarshaled_m_Mode_temp_0);
	float unmarshaled_m_CurveMultiplier_temp_1 = 0.0f;
	unmarshaled_m_CurveMultiplier_temp_1 = marshaled.___m_CurveMultiplier_1;
	unmarshaled.set_m_CurveMultiplier_1(unmarshaled_m_CurveMultiplier_temp_1);
	unmarshaled.set_m_CurveMin_2((AnimationCurve_t2184836714 *)il2cpp_codegen_object_new(AnimationCurve_t2184836714_il2cpp_TypeInfo_var));
	AnimationCurve__ctor_m1175796134(unmarshaled.get_m_CurveMin_2(), NULL);
	AnimationCurve_t2184836714_marshal_pinvoke_back(marshaled.___m_CurveMin_2, *unmarshaled.get_m_CurveMin_2());
	unmarshaled.set_m_CurveMax_3((AnimationCurve_t2184836714 *)il2cpp_codegen_object_new(AnimationCurve_t2184836714_il2cpp_TypeInfo_var));
	AnimationCurve__ctor_m1175796134(unmarshaled.get_m_CurveMax_3(), NULL);
	AnimationCurve_t2184836714_marshal_pinvoke_back(marshaled.___m_CurveMax_3, *unmarshaled.get_m_CurveMax_3());
	float unmarshaled_m_ConstantMin_temp_4 = 0.0f;
	unmarshaled_m_ConstantMin_temp_4 = marshaled.___m_ConstantMin_4;
	unmarshaled.set_m_ConstantMin_4(unmarshaled_m_ConstantMin_temp_4);
	float unmarshaled_m_ConstantMax_temp_5 = 0.0f;
	unmarshaled_m_ConstantMax_temp_5 = marshaled.___m_ConstantMax_5;
	unmarshaled.set_m_ConstantMax_5(unmarshaled_m_ConstantMax_temp_5);
}
// Conversion method for clean up from marshalling of: UnityEngine.ParticleSystem/MinMaxCurve
extern "C" void MinMaxCurve_t2961159436_marshal_pinvoke_cleanup(MinMaxCurve_t2961159436_marshaled_pinvoke& marshaled)
{
	AnimationCurve_t2184836714_marshal_pinvoke_cleanup(marshaled.___m_CurveMin_2);
	AnimationCurve_t2184836714_marshal_pinvoke_cleanup(marshaled.___m_CurveMax_3);
}




// Conversion methods for marshalling of: UnityEngine.ParticleSystem/MinMaxCurve
extern "C" void MinMaxCurve_t2961159436_marshal_com(const MinMaxCurve_t2961159436& unmarshaled, MinMaxCurve_t2961159436_marshaled_com& marshaled)
{
	marshaled.___m_Mode_0 = unmarshaled.get_m_Mode_0();
	marshaled.___m_CurveMultiplier_1 = unmarshaled.get_m_CurveMultiplier_1();
	if (unmarshaled.get_m_CurveMin_2() != NULL) AnimationCurve_t2184836714_marshal_com(*unmarshaled.get_m_CurveMin_2(), *marshaled.___m_CurveMin_2);
	if (unmarshaled.get_m_CurveMax_3() != NULL) AnimationCurve_t2184836714_marshal_com(*unmarshaled.get_m_CurveMax_3(), *marshaled.___m_CurveMax_3);
	marshaled.___m_ConstantMin_4 = unmarshaled.get_m_ConstantMin_4();
	marshaled.___m_ConstantMax_5 = unmarshaled.get_m_ConstantMax_5();
}
extern "C" void MinMaxCurve_t2961159436_marshal_com_back(const MinMaxCurve_t2961159436_marshaled_com& marshaled, MinMaxCurve_t2961159436& unmarshaled)
{
	int32_t unmarshaled_m_Mode_temp_0 = 0;
	unmarshaled_m_Mode_temp_0 = marshaled.___m_Mode_0;
	unmarshaled.set_m_Mode_0(unmarshaled_m_Mode_temp_0);
	float unmarshaled_m_CurveMultiplier_temp_1 = 0.0f;
	unmarshaled_m_CurveMultiplier_temp_1 = marshaled.___m_CurveMultiplier_1;
	unmarshaled.set_m_CurveMultiplier_1(unmarshaled_m_CurveMultiplier_temp_1);
	if (unmarshaled.get_m_CurveMin_2() != NULL)
	{
		AnimationCurve__ctor_m1175796134(unmarshaled.get_m_CurveMin_2(), NULL);
		AnimationCurve_t2184836714_marshal_com_back(*marshaled.___m_CurveMin_2, *unmarshaled.get_m_CurveMin_2());
	}
	if (unmarshaled.get_m_CurveMax_3() != NULL)
	{
		AnimationCurve__ctor_m1175796134(unmarshaled.get_m_CurveMax_3(), NULL);
		AnimationCurve_t2184836714_marshal_com_back(*marshaled.___m_CurveMax_3, *unmarshaled.get_m_CurveMax_3());
	}
	float unmarshaled_m_ConstantMin_temp_4 = 0.0f;
	unmarshaled_m_ConstantMin_temp_4 = marshaled.___m_ConstantMin_4;
	unmarshaled.set_m_ConstantMin_4(unmarshaled_m_ConstantMin_temp_4);
	float unmarshaled_m_ConstantMax_temp_5 = 0.0f;
	unmarshaled_m_ConstantMax_temp_5 = marshaled.___m_ConstantMax_5;
	unmarshaled.set_m_ConstantMax_5(unmarshaled_m_ConstantMax_temp_5);
}
// Conversion method for clean up from marshalling of: UnityEngine.ParticleSystem/MinMaxCurve
extern "C" void MinMaxCurve_t2961159436_marshal_com_cleanup(MinMaxCurve_t2961159436_marshaled_com& marshaled)
{
	if (&(*marshaled.___m_CurveMin_2) != NULL) AnimationCurve_t2184836714_marshal_com_cleanup(*marshaled.___m_CurveMin_2);
	if (&(*marshaled.___m_CurveMax_3) != NULL) AnimationCurve_t2184836714_marshal_com_cleanup(*marshaled.___m_CurveMax_3);
}
// System.Single UnityEngine.ParticleSystem/MinMaxCurve::get_constant()
extern "C"  float MinMaxCurve_get_constant_m2411819109 (MinMaxCurve_t2961159436 * __this, const RuntimeMethod* method)
{
	float V_0 = 0.0f;
	{
		float L_0 = __this->get_m_ConstantMax_5();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		float L_1 = V_0;
		return L_1;
	}
}
extern "C"  float MinMaxCurve_get_constant_m2411819109_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	MinMaxCurve_t2961159436 * _thisAdjusted = reinterpret_cast<MinMaxCurve_t2961159436 *>(__this + 1);
	return MinMaxCurve_get_constant_m2411819109(_thisAdjusted, method);
}
// UnityEngine.Vector3 UnityEngine.Physics::get_gravity()
extern "C"  Vector3_t2903530434  Physics_get_gravity_m2176313118 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	Vector3_t2903530434  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector3_t2903530434  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		Physics_INTERNAL_get_gravity_m4188242439(NULL /*static, unused*/, (&V_0), /*hidden argument*/NULL);
		Vector3_t2903530434  L_0 = V_0;
		V_1 = L_0;
		goto IL_000f;
	}

IL_000f:
	{
		Vector3_t2903530434  L_1 = V_1;
		return L_1;
	}
}
// System.Void UnityEngine.Physics::INTERNAL_get_gravity(UnityEngine.Vector3&)
extern "C"  void Physics_INTERNAL_get_gravity_m4188242439 (RuntimeObject * __this /* static, unused */, Vector3_t2903530434 * ___value0, const RuntimeMethod* method)
{
	typedef void (*Physics_INTERNAL_get_gravity_m4188242439_ftn) (Vector3_t2903530434 *);
	static Physics_INTERNAL_get_gravity_m4188242439_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Physics_INTERNAL_get_gravity_m4188242439_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Physics::INTERNAL_get_gravity(UnityEngine.Vector3&)");
	_il2cpp_icall_func(___value0);
}
// System.Boolean UnityEngine.Physics::Raycast(UnityEngine.Vector3,UnityEngine.Vector3,System.Single,System.Int32)
extern "C"  bool Physics_Raycast_m523426354 (RuntimeObject * __this /* static, unused */, Vector3_t2903530434  ___origin0, Vector3_t2903530434  ___direction1, float ___maxDistance2, int32_t ___layerMask3, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	bool V_1 = false;
	{
		V_0 = 0;
		Vector3_t2903530434  L_0 = ___origin0;
		Vector3_t2903530434  L_1 = ___direction1;
		float L_2 = ___maxDistance2;
		int32_t L_3 = ___layerMask3;
		int32_t L_4 = V_0;
		bool L_5 = Physics_Raycast_m3039943821(NULL /*static, unused*/, L_0, L_1, L_2, L_3, L_4, /*hidden argument*/NULL);
		V_1 = L_5;
		goto IL_0013;
	}

IL_0013:
	{
		bool L_6 = V_1;
		return L_6;
	}
}
// System.Boolean UnityEngine.Physics::Raycast(UnityEngine.Vector3,UnityEngine.Vector3,System.Single)
extern "C"  bool Physics_Raycast_m4096335612 (RuntimeObject * __this /* static, unused */, Vector3_t2903530434  ___origin0, Vector3_t2903530434  ___direction1, float ___maxDistance2, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	bool V_2 = false;
	{
		V_0 = 0;
		V_1 = ((int32_t)-5);
		Vector3_t2903530434  L_0 = ___origin0;
		Vector3_t2903530434  L_1 = ___direction1;
		float L_2 = ___maxDistance2;
		int32_t L_3 = V_1;
		int32_t L_4 = V_0;
		bool L_5 = Physics_Raycast_m3039943821(NULL /*static, unused*/, L_0, L_1, L_2, L_3, L_4, /*hidden argument*/NULL);
		V_2 = L_5;
		goto IL_0016;
	}

IL_0016:
	{
		bool L_6 = V_2;
		return L_6;
	}
}
// System.Boolean UnityEngine.Physics::Raycast(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  bool Physics_Raycast_m2003637126 (RuntimeObject * __this /* static, unused */, Vector3_t2903530434  ___origin0, Vector3_t2903530434  ___direction1, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	float V_2 = 0.0f;
	bool V_3 = false;
	{
		V_0 = 0;
		V_1 = ((int32_t)-5);
		V_2 = (std::numeric_limits<float>::infinity());
		Vector3_t2903530434  L_0 = ___origin0;
		Vector3_t2903530434  L_1 = ___direction1;
		float L_2 = V_2;
		int32_t L_3 = V_1;
		int32_t L_4 = V_0;
		bool L_5 = Physics_Raycast_m3039943821(NULL /*static, unused*/, L_0, L_1, L_2, L_3, L_4, /*hidden argument*/NULL);
		V_3 = L_5;
		goto IL_001c;
	}

IL_001c:
	{
		bool L_6 = V_3;
		return L_6;
	}
}
// System.Boolean UnityEngine.Physics::Raycast(UnityEngine.Vector3,UnityEngine.Vector3,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern "C"  bool Physics_Raycast_m3039943821 (RuntimeObject * __this /* static, unused */, Vector3_t2903530434  ___origin0, Vector3_t2903530434  ___direction1, float ___maxDistance2, int32_t ___layerMask3, int32_t ___queryTriggerInteraction4, const RuntimeMethod* method)
{
	bool V_0 = false;
	{
		Vector3_t2903530434  L_0 = ___origin0;
		Vector3_t2903530434  L_1 = ___direction1;
		float L_2 = ___maxDistance2;
		int32_t L_3 = ___layerMask3;
		int32_t L_4 = ___queryTriggerInteraction4;
		bool L_5 = Physics_Internal_RaycastTest_m304866704(NULL /*static, unused*/, L_0, L_1, L_2, L_3, L_4, /*hidden argument*/NULL);
		V_0 = L_5;
		goto IL_0012;
	}

IL_0012:
	{
		bool L_6 = V_0;
		return L_6;
	}
}
// System.Boolean UnityEngine.Physics::Raycast(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.RaycastHit&,System.Single,System.Int32)
extern "C"  bool Physics_Raycast_m2622901317 (RuntimeObject * __this /* static, unused */, Vector3_t2903530434  ___origin0, Vector3_t2903530434  ___direction1, RaycastHit_t2327766465 * ___hitInfo2, float ___maxDistance3, int32_t ___layerMask4, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	bool V_1 = false;
	{
		V_0 = 0;
		Vector3_t2903530434  L_0 = ___origin0;
		Vector3_t2903530434  L_1 = ___direction1;
		RaycastHit_t2327766465 * L_2 = ___hitInfo2;
		float L_3 = ___maxDistance3;
		int32_t L_4 = ___layerMask4;
		int32_t L_5 = V_0;
		bool L_6 = Physics_Raycast_m3270369209(NULL /*static, unused*/, L_0, L_1, L_2, L_3, L_4, L_5, /*hidden argument*/NULL);
		V_1 = L_6;
		goto IL_0015;
	}

IL_0015:
	{
		bool L_7 = V_1;
		return L_7;
	}
}
// System.Boolean UnityEngine.Physics::Raycast(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.RaycastHit&,System.Single)
extern "C"  bool Physics_Raycast_m4141840351 (RuntimeObject * __this /* static, unused */, Vector3_t2903530434  ___origin0, Vector3_t2903530434  ___direction1, RaycastHit_t2327766465 * ___hitInfo2, float ___maxDistance3, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	bool V_2 = false;
	{
		V_0 = 0;
		V_1 = ((int32_t)-5);
		Vector3_t2903530434  L_0 = ___origin0;
		Vector3_t2903530434  L_1 = ___direction1;
		RaycastHit_t2327766465 * L_2 = ___hitInfo2;
		float L_3 = ___maxDistance3;
		int32_t L_4 = V_1;
		int32_t L_5 = V_0;
		bool L_6 = Physics_Raycast_m3270369209(NULL /*static, unused*/, L_0, L_1, L_2, L_3, L_4, L_5, /*hidden argument*/NULL);
		V_2 = L_6;
		goto IL_0017;
	}

IL_0017:
	{
		bool L_7 = V_2;
		return L_7;
	}
}
// System.Boolean UnityEngine.Physics::Raycast(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.RaycastHit&)
extern "C"  bool Physics_Raycast_m3052581095 (RuntimeObject * __this /* static, unused */, Vector3_t2903530434  ___origin0, Vector3_t2903530434  ___direction1, RaycastHit_t2327766465 * ___hitInfo2, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	float V_2 = 0.0f;
	bool V_3 = false;
	{
		V_0 = 0;
		V_1 = ((int32_t)-5);
		V_2 = (std::numeric_limits<float>::infinity());
		Vector3_t2903530434  L_0 = ___origin0;
		Vector3_t2903530434  L_1 = ___direction1;
		RaycastHit_t2327766465 * L_2 = ___hitInfo2;
		float L_3 = V_2;
		int32_t L_4 = V_1;
		int32_t L_5 = V_0;
		bool L_6 = Physics_Raycast_m3270369209(NULL /*static, unused*/, L_0, L_1, L_2, L_3, L_4, L_5, /*hidden argument*/NULL);
		V_3 = L_6;
		goto IL_001d;
	}

IL_001d:
	{
		bool L_7 = V_3;
		return L_7;
	}
}
// System.Boolean UnityEngine.Physics::Raycast(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.RaycastHit&,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern "C"  bool Physics_Raycast_m3270369209 (RuntimeObject * __this /* static, unused */, Vector3_t2903530434  ___origin0, Vector3_t2903530434  ___direction1, RaycastHit_t2327766465 * ___hitInfo2, float ___maxDistance3, int32_t ___layerMask4, int32_t ___queryTriggerInteraction5, const RuntimeMethod* method)
{
	bool V_0 = false;
	{
		Vector3_t2903530434  L_0 = ___origin0;
		Vector3_t2903530434  L_1 = ___direction1;
		RaycastHit_t2327766465 * L_2 = ___hitInfo2;
		float L_3 = ___maxDistance3;
		int32_t L_4 = ___layerMask4;
		int32_t L_5 = ___queryTriggerInteraction5;
		bool L_6 = Physics_Internal_Raycast_m1340688750(NULL /*static, unused*/, L_0, L_1, L_2, L_3, L_4, L_5, /*hidden argument*/NULL);
		V_0 = L_6;
		goto IL_0014;
	}

IL_0014:
	{
		bool L_7 = V_0;
		return L_7;
	}
}
// System.Boolean UnityEngine.Physics::Raycast(UnityEngine.Ray,System.Single,System.Int32)
extern "C"  bool Physics_Raycast_m1976546014 (RuntimeObject * __this /* static, unused */, Ray_t3821377119  ___ray0, float ___maxDistance1, int32_t ___layerMask2, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	bool V_1 = false;
	{
		V_0 = 0;
		Ray_t3821377119  L_0 = ___ray0;
		float L_1 = ___maxDistance1;
		int32_t L_2 = ___layerMask2;
		int32_t L_3 = V_0;
		bool L_4 = Physics_Raycast_m3725218301(NULL /*static, unused*/, L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		V_1 = L_4;
		goto IL_0012;
	}

IL_0012:
	{
		bool L_5 = V_1;
		return L_5;
	}
}
// System.Boolean UnityEngine.Physics::Raycast(UnityEngine.Ray,System.Single)
extern "C"  bool Physics_Raycast_m3689381594 (RuntimeObject * __this /* static, unused */, Ray_t3821377119  ___ray0, float ___maxDistance1, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	bool V_2 = false;
	{
		V_0 = 0;
		V_1 = ((int32_t)-5);
		Ray_t3821377119  L_0 = ___ray0;
		float L_1 = ___maxDistance1;
		int32_t L_2 = V_1;
		int32_t L_3 = V_0;
		bool L_4 = Physics_Raycast_m3725218301(NULL /*static, unused*/, L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		V_2 = L_4;
		goto IL_0015;
	}

IL_0015:
	{
		bool L_5 = V_2;
		return L_5;
	}
}
// System.Boolean UnityEngine.Physics::Raycast(UnityEngine.Ray)
extern "C"  bool Physics_Raycast_m2993421489 (RuntimeObject * __this /* static, unused */, Ray_t3821377119  ___ray0, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	float V_2 = 0.0f;
	bool V_3 = false;
	{
		V_0 = 0;
		V_1 = ((int32_t)-5);
		V_2 = (std::numeric_limits<float>::infinity());
		Ray_t3821377119  L_0 = ___ray0;
		float L_1 = V_2;
		int32_t L_2 = V_1;
		int32_t L_3 = V_0;
		bool L_4 = Physics_Raycast_m3725218301(NULL /*static, unused*/, L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		V_3 = L_4;
		goto IL_001b;
	}

IL_001b:
	{
		bool L_5 = V_3;
		return L_5;
	}
}
// System.Boolean UnityEngine.Physics::Raycast(UnityEngine.Ray,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern "C"  bool Physics_Raycast_m3725218301 (RuntimeObject * __this /* static, unused */, Ray_t3821377119  ___ray0, float ___maxDistance1, int32_t ___layerMask2, int32_t ___queryTriggerInteraction3, const RuntimeMethod* method)
{
	bool V_0 = false;
	{
		Vector3_t2903530434  L_0 = Ray_get_origin_m126064229((&___ray0), /*hidden argument*/NULL);
		Vector3_t2903530434  L_1 = Ray_get_direction_m3512869925((&___ray0), /*hidden argument*/NULL);
		float L_2 = ___maxDistance1;
		int32_t L_3 = ___layerMask2;
		int32_t L_4 = ___queryTriggerInteraction3;
		bool L_5 = Physics_Raycast_m3039943821(NULL /*static, unused*/, L_0, L_1, L_2, L_3, L_4, /*hidden argument*/NULL);
		V_0 = L_5;
		goto IL_001d;
	}

IL_001d:
	{
		bool L_6 = V_0;
		return L_6;
	}
}
// System.Boolean UnityEngine.Physics::Raycast(UnityEngine.Ray,UnityEngine.RaycastHit&,System.Single,System.Int32)
extern "C"  bool Physics_Raycast_m3907558097 (RuntimeObject * __this /* static, unused */, Ray_t3821377119  ___ray0, RaycastHit_t2327766465 * ___hitInfo1, float ___maxDistance2, int32_t ___layerMask3, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	bool V_1 = false;
	{
		V_0 = 0;
		Ray_t3821377119  L_0 = ___ray0;
		RaycastHit_t2327766465 * L_1 = ___hitInfo1;
		float L_2 = ___maxDistance2;
		int32_t L_3 = ___layerMask3;
		int32_t L_4 = V_0;
		bool L_5 = Physics_Raycast_m3944844260(NULL /*static, unused*/, L_0, L_1, L_2, L_3, L_4, /*hidden argument*/NULL);
		V_1 = L_5;
		goto IL_0013;
	}

IL_0013:
	{
		bool L_6 = V_1;
		return L_6;
	}
}
// System.Boolean UnityEngine.Physics::Raycast(UnityEngine.Ray,UnityEngine.RaycastHit&,System.Single)
extern "C"  bool Physics_Raycast_m39101816 (RuntimeObject * __this /* static, unused */, Ray_t3821377119  ___ray0, RaycastHit_t2327766465 * ___hitInfo1, float ___maxDistance2, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	bool V_2 = false;
	{
		V_0 = 0;
		V_1 = ((int32_t)-5);
		Ray_t3821377119  L_0 = ___ray0;
		RaycastHit_t2327766465 * L_1 = ___hitInfo1;
		float L_2 = ___maxDistance2;
		int32_t L_3 = V_1;
		int32_t L_4 = V_0;
		bool L_5 = Physics_Raycast_m3944844260(NULL /*static, unused*/, L_0, L_1, L_2, L_3, L_4, /*hidden argument*/NULL);
		V_2 = L_5;
		goto IL_0016;
	}

IL_0016:
	{
		bool L_6 = V_2;
		return L_6;
	}
}
// System.Boolean UnityEngine.Physics::Raycast(UnityEngine.Ray,UnityEngine.RaycastHit&)
extern "C"  bool Physics_Raycast_m3713720331 (RuntimeObject * __this /* static, unused */, Ray_t3821377119  ___ray0, RaycastHit_t2327766465 * ___hitInfo1, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	float V_2 = 0.0f;
	bool V_3 = false;
	{
		V_0 = 0;
		V_1 = ((int32_t)-5);
		V_2 = (std::numeric_limits<float>::infinity());
		Ray_t3821377119  L_0 = ___ray0;
		RaycastHit_t2327766465 * L_1 = ___hitInfo1;
		float L_2 = V_2;
		int32_t L_3 = V_1;
		int32_t L_4 = V_0;
		bool L_5 = Physics_Raycast_m3944844260(NULL /*static, unused*/, L_0, L_1, L_2, L_3, L_4, /*hidden argument*/NULL);
		V_3 = L_5;
		goto IL_001c;
	}

IL_001c:
	{
		bool L_6 = V_3;
		return L_6;
	}
}
// System.Boolean UnityEngine.Physics::Raycast(UnityEngine.Ray,UnityEngine.RaycastHit&,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern "C"  bool Physics_Raycast_m3944844260 (RuntimeObject * __this /* static, unused */, Ray_t3821377119  ___ray0, RaycastHit_t2327766465 * ___hitInfo1, float ___maxDistance2, int32_t ___layerMask3, int32_t ___queryTriggerInteraction4, const RuntimeMethod* method)
{
	bool V_0 = false;
	{
		Vector3_t2903530434  L_0 = Ray_get_origin_m126064229((&___ray0), /*hidden argument*/NULL);
		Vector3_t2903530434  L_1 = Ray_get_direction_m3512869925((&___ray0), /*hidden argument*/NULL);
		RaycastHit_t2327766465 * L_2 = ___hitInfo1;
		float L_3 = ___maxDistance2;
		int32_t L_4 = ___layerMask3;
		int32_t L_5 = ___queryTriggerInteraction4;
		bool L_6 = Physics_Raycast_m3270369209(NULL /*static, unused*/, L_0, L_1, L_2, L_3, L_4, L_5, /*hidden argument*/NULL);
		V_0 = L_6;
		goto IL_001f;
	}

IL_001f:
	{
		bool L_7 = V_0;
		return L_7;
	}
}
// UnityEngine.RaycastHit[] UnityEngine.Physics::RaycastAll(UnityEngine.Ray,System.Single,System.Int32)
extern "C"  RaycastHitU5BU5D_t4180454940* Physics_RaycastAll_m1956997019 (RuntimeObject * __this /* static, unused */, Ray_t3821377119  ___ray0, float ___maxDistance1, int32_t ___layerMask2, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	RaycastHitU5BU5D_t4180454940* V_1 = NULL;
	{
		V_0 = 0;
		Ray_t3821377119  L_0 = ___ray0;
		float L_1 = ___maxDistance1;
		int32_t L_2 = ___layerMask2;
		int32_t L_3 = V_0;
		RaycastHitU5BU5D_t4180454940* L_4 = Physics_RaycastAll_m571393075(NULL /*static, unused*/, L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		V_1 = L_4;
		goto IL_0012;
	}

IL_0012:
	{
		RaycastHitU5BU5D_t4180454940* L_5 = V_1;
		return L_5;
	}
}
// UnityEngine.RaycastHit[] UnityEngine.Physics::RaycastAll(UnityEngine.Ray,System.Single)
extern "C"  RaycastHitU5BU5D_t4180454940* Physics_RaycastAll_m1628569404 (RuntimeObject * __this /* static, unused */, Ray_t3821377119  ___ray0, float ___maxDistance1, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	RaycastHitU5BU5D_t4180454940* V_2 = NULL;
	{
		V_0 = 0;
		V_1 = ((int32_t)-5);
		Ray_t3821377119  L_0 = ___ray0;
		float L_1 = ___maxDistance1;
		int32_t L_2 = V_1;
		int32_t L_3 = V_0;
		RaycastHitU5BU5D_t4180454940* L_4 = Physics_RaycastAll_m571393075(NULL /*static, unused*/, L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		V_2 = L_4;
		goto IL_0015;
	}

IL_0015:
	{
		RaycastHitU5BU5D_t4180454940* L_5 = V_2;
		return L_5;
	}
}
// UnityEngine.RaycastHit[] UnityEngine.Physics::RaycastAll(UnityEngine.Ray)
extern "C"  RaycastHitU5BU5D_t4180454940* Physics_RaycastAll_m3611061632 (RuntimeObject * __this /* static, unused */, Ray_t3821377119  ___ray0, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	float V_2 = 0.0f;
	RaycastHitU5BU5D_t4180454940* V_3 = NULL;
	{
		V_0 = 0;
		V_1 = ((int32_t)-5);
		V_2 = (std::numeric_limits<float>::infinity());
		Ray_t3821377119  L_0 = ___ray0;
		float L_1 = V_2;
		int32_t L_2 = V_1;
		int32_t L_3 = V_0;
		RaycastHitU5BU5D_t4180454940* L_4 = Physics_RaycastAll_m571393075(NULL /*static, unused*/, L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		V_3 = L_4;
		goto IL_001b;
	}

IL_001b:
	{
		RaycastHitU5BU5D_t4180454940* L_5 = V_3;
		return L_5;
	}
}
// UnityEngine.RaycastHit[] UnityEngine.Physics::RaycastAll(UnityEngine.Ray,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern "C"  RaycastHitU5BU5D_t4180454940* Physics_RaycastAll_m571393075 (RuntimeObject * __this /* static, unused */, Ray_t3821377119  ___ray0, float ___maxDistance1, int32_t ___layerMask2, int32_t ___queryTriggerInteraction3, const RuntimeMethod* method)
{
	RaycastHitU5BU5D_t4180454940* V_0 = NULL;
	{
		Vector3_t2903530434  L_0 = Ray_get_origin_m126064229((&___ray0), /*hidden argument*/NULL);
		Vector3_t2903530434  L_1 = Ray_get_direction_m3512869925((&___ray0), /*hidden argument*/NULL);
		float L_2 = ___maxDistance1;
		int32_t L_3 = ___layerMask2;
		int32_t L_4 = ___queryTriggerInteraction3;
		RaycastHitU5BU5D_t4180454940* L_5 = Physics_RaycastAll_m3392506883(NULL /*static, unused*/, L_0, L_1, L_2, L_3, L_4, /*hidden argument*/NULL);
		V_0 = L_5;
		goto IL_001d;
	}

IL_001d:
	{
		RaycastHitU5BU5D_t4180454940* L_6 = V_0;
		return L_6;
	}
}
// UnityEngine.RaycastHit[] UnityEngine.Physics::RaycastAll(UnityEngine.Vector3,UnityEngine.Vector3,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern "C"  RaycastHitU5BU5D_t4180454940* Physics_RaycastAll_m3392506883 (RuntimeObject * __this /* static, unused */, Vector3_t2903530434  ___origin0, Vector3_t2903530434  ___direction1, float ___maxDistance2, int32_t ___layermask3, int32_t ___queryTriggerInteraction4, const RuntimeMethod* method)
{
	RaycastHitU5BU5D_t4180454940* V_0 = NULL;
	{
		float L_0 = ___maxDistance2;
		int32_t L_1 = ___layermask3;
		int32_t L_2 = ___queryTriggerInteraction4;
		RaycastHitU5BU5D_t4180454940* L_3 = Physics_INTERNAL_CALL_RaycastAll_m3160329193(NULL /*static, unused*/, (&___origin0), (&___direction1), L_0, L_1, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		goto IL_0014;
	}

IL_0014:
	{
		RaycastHitU5BU5D_t4180454940* L_4 = V_0;
		return L_4;
	}
}
// UnityEngine.RaycastHit[] UnityEngine.Physics::RaycastAll(UnityEngine.Vector3,UnityEngine.Vector3,System.Single,System.Int32)
extern "C"  RaycastHitU5BU5D_t4180454940* Physics_RaycastAll_m1504772842 (RuntimeObject * __this /* static, unused */, Vector3_t2903530434  ___origin0, Vector3_t2903530434  ___direction1, float ___maxDistance2, int32_t ___layermask3, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	RaycastHitU5BU5D_t4180454940* V_1 = NULL;
	{
		V_0 = 0;
		float L_0 = ___maxDistance2;
		int32_t L_1 = ___layermask3;
		int32_t L_2 = V_0;
		RaycastHitU5BU5D_t4180454940* L_3 = Physics_INTERNAL_CALL_RaycastAll_m3160329193(NULL /*static, unused*/, (&___origin0), (&___direction1), L_0, L_1, L_2, /*hidden argument*/NULL);
		V_1 = L_3;
		goto IL_0015;
	}

IL_0015:
	{
		RaycastHitU5BU5D_t4180454940* L_4 = V_1;
		return L_4;
	}
}
// UnityEngine.RaycastHit[] UnityEngine.Physics::RaycastAll(UnityEngine.Vector3,UnityEngine.Vector3,System.Single)
extern "C"  RaycastHitU5BU5D_t4180454940* Physics_RaycastAll_m3311115260 (RuntimeObject * __this /* static, unused */, Vector3_t2903530434  ___origin0, Vector3_t2903530434  ___direction1, float ___maxDistance2, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	RaycastHitU5BU5D_t4180454940* V_2 = NULL;
	{
		V_0 = 0;
		V_1 = ((int32_t)-5);
		float L_0 = ___maxDistance2;
		int32_t L_1 = V_1;
		int32_t L_2 = V_0;
		RaycastHitU5BU5D_t4180454940* L_3 = Physics_INTERNAL_CALL_RaycastAll_m3160329193(NULL /*static, unused*/, (&___origin0), (&___direction1), L_0, L_1, L_2, /*hidden argument*/NULL);
		V_2 = L_3;
		goto IL_0018;
	}

IL_0018:
	{
		RaycastHitU5BU5D_t4180454940* L_4 = V_2;
		return L_4;
	}
}
// UnityEngine.RaycastHit[] UnityEngine.Physics::RaycastAll(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  RaycastHitU5BU5D_t4180454940* Physics_RaycastAll_m4177492710 (RuntimeObject * __this /* static, unused */, Vector3_t2903530434  ___origin0, Vector3_t2903530434  ___direction1, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	float V_2 = 0.0f;
	RaycastHitU5BU5D_t4180454940* V_3 = NULL;
	{
		V_0 = 0;
		V_1 = ((int32_t)-5);
		V_2 = (std::numeric_limits<float>::infinity());
		float L_0 = V_2;
		int32_t L_1 = V_1;
		int32_t L_2 = V_0;
		RaycastHitU5BU5D_t4180454940* L_3 = Physics_INTERNAL_CALL_RaycastAll_m3160329193(NULL /*static, unused*/, (&___origin0), (&___direction1), L_0, L_1, L_2, /*hidden argument*/NULL);
		V_3 = L_3;
		goto IL_001e;
	}

IL_001e:
	{
		RaycastHitU5BU5D_t4180454940* L_4 = V_3;
		return L_4;
	}
}
// UnityEngine.RaycastHit[] UnityEngine.Physics::INTERNAL_CALL_RaycastAll(UnityEngine.Vector3&,UnityEngine.Vector3&,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern "C"  RaycastHitU5BU5D_t4180454940* Physics_INTERNAL_CALL_RaycastAll_m3160329193 (RuntimeObject * __this /* static, unused */, Vector3_t2903530434 * ___origin0, Vector3_t2903530434 * ___direction1, float ___maxDistance2, int32_t ___layermask3, int32_t ___queryTriggerInteraction4, const RuntimeMethod* method)
{
	typedef RaycastHitU5BU5D_t4180454940* (*Physics_INTERNAL_CALL_RaycastAll_m3160329193_ftn) (Vector3_t2903530434 *, Vector3_t2903530434 *, float, int32_t, int32_t);
	static Physics_INTERNAL_CALL_RaycastAll_m3160329193_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Physics_INTERNAL_CALL_RaycastAll_m3160329193_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Physics::INTERNAL_CALL_RaycastAll(UnityEngine.Vector3&,UnityEngine.Vector3&,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)");
	RaycastHitU5BU5D_t4180454940* retVal = _il2cpp_icall_func(___origin0, ___direction1, ___maxDistance2, ___layermask3, ___queryTriggerInteraction4);
	return retVal;
}
// System.Boolean UnityEngine.Physics::SphereCast(UnityEngine.Vector3,System.Single,UnityEngine.Vector3,UnityEngine.RaycastHit&,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern "C"  bool Physics_SphereCast_m2090859560 (RuntimeObject * __this /* static, unused */, Vector3_t2903530434  ___origin0, float ___radius1, Vector3_t2903530434  ___direction2, RaycastHit_t2327766465 * ___hitInfo3, float ___maxDistance4, int32_t ___layerMask5, int32_t ___queryTriggerInteraction6, const RuntimeMethod* method)
{
	bool V_0 = false;
	{
		Vector3_t2903530434  L_0 = ___origin0;
		Vector3_t2903530434  L_1 = ___origin0;
		float L_2 = ___radius1;
		Vector3_t2903530434  L_3 = ___direction2;
		RaycastHit_t2327766465 * L_4 = ___hitInfo3;
		float L_5 = ___maxDistance4;
		int32_t L_6 = ___layerMask5;
		int32_t L_7 = ___queryTriggerInteraction6;
		bool L_8 = Physics_Internal_CapsuleCast_m1511147988(NULL /*static, unused*/, L_0, L_1, L_2, L_3, L_4, L_5, L_6, L_7, /*hidden argument*/NULL);
		V_0 = L_8;
		goto IL_0017;
	}

IL_0017:
	{
		bool L_9 = V_0;
		return L_9;
	}
}
// System.Boolean UnityEngine.Physics::SphereCast(UnityEngine.Ray,System.Single,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern "C"  bool Physics_SphereCast_m3397621151 (RuntimeObject * __this /* static, unused */, Ray_t3821377119  ___ray0, float ___radius1, float ___maxDistance2, int32_t ___layerMask3, int32_t ___queryTriggerInteraction4, const RuntimeMethod* method)
{
	RaycastHit_t2327766465  V_0;
	memset(&V_0, 0, sizeof(V_0));
	bool V_1 = false;
	{
		Vector3_t2903530434  L_0 = Ray_get_origin_m126064229((&___ray0), /*hidden argument*/NULL);
		Vector3_t2903530434  L_1 = Ray_get_origin_m126064229((&___ray0), /*hidden argument*/NULL);
		float L_2 = ___radius1;
		Vector3_t2903530434  L_3 = Ray_get_direction_m3512869925((&___ray0), /*hidden argument*/NULL);
		float L_4 = ___maxDistance2;
		int32_t L_5 = ___layerMask3;
		int32_t L_6 = ___queryTriggerInteraction4;
		bool L_7 = Physics_Internal_CapsuleCast_m1511147988(NULL /*static, unused*/, L_0, L_1, L_2, L_3, (&V_0), L_4, L_5, L_6, /*hidden argument*/NULL);
		V_1 = L_7;
		goto IL_0028;
	}

IL_0028:
	{
		bool L_8 = V_1;
		return L_8;
	}
}
// System.Boolean UnityEngine.Physics::Internal_Raycast(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.RaycastHit&,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern "C"  bool Physics_Internal_Raycast_m1340688750 (RuntimeObject * __this /* static, unused */, Vector3_t2903530434  ___origin0, Vector3_t2903530434  ___direction1, RaycastHit_t2327766465 * ___hitInfo2, float ___maxDistance3, int32_t ___layermask4, int32_t ___queryTriggerInteraction5, const RuntimeMethod* method)
{
	bool V_0 = false;
	{
		RaycastHit_t2327766465 * L_0 = ___hitInfo2;
		float L_1 = ___maxDistance3;
		int32_t L_2 = ___layermask4;
		int32_t L_3 = ___queryTriggerInteraction5;
		bool L_4 = Physics_INTERNAL_CALL_Internal_Raycast_m3189866168(NULL /*static, unused*/, (&___origin0), (&___direction1), L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		goto IL_0016;
	}

IL_0016:
	{
		bool L_5 = V_0;
		return L_5;
	}
}
// System.Boolean UnityEngine.Physics::INTERNAL_CALL_Internal_Raycast(UnityEngine.Vector3&,UnityEngine.Vector3&,UnityEngine.RaycastHit&,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern "C"  bool Physics_INTERNAL_CALL_Internal_Raycast_m3189866168 (RuntimeObject * __this /* static, unused */, Vector3_t2903530434 * ___origin0, Vector3_t2903530434 * ___direction1, RaycastHit_t2327766465 * ___hitInfo2, float ___maxDistance3, int32_t ___layermask4, int32_t ___queryTriggerInteraction5, const RuntimeMethod* method)
{
	typedef bool (*Physics_INTERNAL_CALL_Internal_Raycast_m3189866168_ftn) (Vector3_t2903530434 *, Vector3_t2903530434 *, RaycastHit_t2327766465 *, float, int32_t, int32_t);
	static Physics_INTERNAL_CALL_Internal_Raycast_m3189866168_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Physics_INTERNAL_CALL_Internal_Raycast_m3189866168_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Physics::INTERNAL_CALL_Internal_Raycast(UnityEngine.Vector3&,UnityEngine.Vector3&,UnityEngine.RaycastHit&,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)");
	bool retVal = _il2cpp_icall_func(___origin0, ___direction1, ___hitInfo2, ___maxDistance3, ___layermask4, ___queryTriggerInteraction5);
	return retVal;
}
// System.Boolean UnityEngine.Physics::Internal_CapsuleCast(UnityEngine.Vector3,UnityEngine.Vector3,System.Single,UnityEngine.Vector3,UnityEngine.RaycastHit&,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern "C"  bool Physics_Internal_CapsuleCast_m1511147988 (RuntimeObject * __this /* static, unused */, Vector3_t2903530434  ___point10, Vector3_t2903530434  ___point21, float ___radius2, Vector3_t2903530434  ___direction3, RaycastHit_t2327766465 * ___hitInfo4, float ___maxDistance5, int32_t ___layermask6, int32_t ___queryTriggerInteraction7, const RuntimeMethod* method)
{
	bool V_0 = false;
	{
		float L_0 = ___radius2;
		RaycastHit_t2327766465 * L_1 = ___hitInfo4;
		float L_2 = ___maxDistance5;
		int32_t L_3 = ___layermask6;
		int32_t L_4 = ___queryTriggerInteraction7;
		bool L_5 = Physics_INTERNAL_CALL_Internal_CapsuleCast_m1943493709(NULL /*static, unused*/, (&___point10), (&___point21), L_0, (&___direction3), L_1, L_2, L_3, L_4, /*hidden argument*/NULL);
		V_0 = L_5;
		goto IL_001b;
	}

IL_001b:
	{
		bool L_6 = V_0;
		return L_6;
	}
}
// System.Boolean UnityEngine.Physics::INTERNAL_CALL_Internal_CapsuleCast(UnityEngine.Vector3&,UnityEngine.Vector3&,System.Single,UnityEngine.Vector3&,UnityEngine.RaycastHit&,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern "C"  bool Physics_INTERNAL_CALL_Internal_CapsuleCast_m1943493709 (RuntimeObject * __this /* static, unused */, Vector3_t2903530434 * ___point10, Vector3_t2903530434 * ___point21, float ___radius2, Vector3_t2903530434 * ___direction3, RaycastHit_t2327766465 * ___hitInfo4, float ___maxDistance5, int32_t ___layermask6, int32_t ___queryTriggerInteraction7, const RuntimeMethod* method)
{
	typedef bool (*Physics_INTERNAL_CALL_Internal_CapsuleCast_m1943493709_ftn) (Vector3_t2903530434 *, Vector3_t2903530434 *, float, Vector3_t2903530434 *, RaycastHit_t2327766465 *, float, int32_t, int32_t);
	static Physics_INTERNAL_CALL_Internal_CapsuleCast_m1943493709_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Physics_INTERNAL_CALL_Internal_CapsuleCast_m1943493709_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Physics::INTERNAL_CALL_Internal_CapsuleCast(UnityEngine.Vector3&,UnityEngine.Vector3&,System.Single,UnityEngine.Vector3&,UnityEngine.RaycastHit&,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)");
	bool retVal = _il2cpp_icall_func(___point10, ___point21, ___radius2, ___direction3, ___hitInfo4, ___maxDistance5, ___layermask6, ___queryTriggerInteraction7);
	return retVal;
}
// System.Boolean UnityEngine.Physics::Internal_RaycastTest(UnityEngine.Vector3,UnityEngine.Vector3,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern "C"  bool Physics_Internal_RaycastTest_m304866704 (RuntimeObject * __this /* static, unused */, Vector3_t2903530434  ___origin0, Vector3_t2903530434  ___direction1, float ___maxDistance2, int32_t ___layermask3, int32_t ___queryTriggerInteraction4, const RuntimeMethod* method)
{
	bool V_0 = false;
	{
		float L_0 = ___maxDistance2;
		int32_t L_1 = ___layermask3;
		int32_t L_2 = ___queryTriggerInteraction4;
		bool L_3 = Physics_INTERNAL_CALL_Internal_RaycastTest_m2228775343(NULL /*static, unused*/, (&___origin0), (&___direction1), L_0, L_1, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		goto IL_0014;
	}

IL_0014:
	{
		bool L_4 = V_0;
		return L_4;
	}
}
// System.Boolean UnityEngine.Physics::INTERNAL_CALL_Internal_RaycastTest(UnityEngine.Vector3&,UnityEngine.Vector3&,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern "C"  bool Physics_INTERNAL_CALL_Internal_RaycastTest_m2228775343 (RuntimeObject * __this /* static, unused */, Vector3_t2903530434 * ___origin0, Vector3_t2903530434 * ___direction1, float ___maxDistance2, int32_t ___layermask3, int32_t ___queryTriggerInteraction4, const RuntimeMethod* method)
{
	typedef bool (*Physics_INTERNAL_CALL_Internal_RaycastTest_m2228775343_ftn) (Vector3_t2903530434 *, Vector3_t2903530434 *, float, int32_t, int32_t);
	static Physics_INTERNAL_CALL_Internal_RaycastTest_m2228775343_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Physics_INTERNAL_CALL_Internal_RaycastTest_m2228775343_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Physics::INTERNAL_CALL_Internal_RaycastTest(UnityEngine.Vector3&,UnityEngine.Vector3&,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)");
	bool retVal = _il2cpp_icall_func(___origin0, ___direction1, ___maxDistance2, ___layermask3, ___queryTriggerInteraction4);
	return retVal;
}
// System.Void UnityEngine.Physics2D::.cctor()
extern "C"  void Physics2D__cctor_m3391912526 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Physics2D__cctor_m3391912526_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_t3403087445 * L_0 = (List_1_t3403087445 *)il2cpp_codegen_object_new(List_1_t3403087445_il2cpp_TypeInfo_var);
		List_1__ctor_m1977615932(L_0, /*hidden argument*/List_1__ctor_m1977615932_RuntimeMethod_var);
		((Physics2D_t1358151724_StaticFields*)il2cpp_codegen_static_fields_for(Physics2D_t1358151724_il2cpp_TypeInfo_var))->set_m_LastDisabledRigidbody2D_0(L_0);
		return;
	}
}
// System.Void UnityEngine.Plane::.ctor(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  void Plane__ctor_m2922395188 (Plane_t2300588969 * __this, Vector3_t2903530434  ___inNormal0, Vector3_t2903530434  ___inPoint1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Plane__ctor_m2922395188_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Vector3_t2903530434  L_0 = ___inNormal0;
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t2903530434_il2cpp_TypeInfo_var);
		Vector3_t2903530434  L_1 = Vector3_Normalize_m1717215557(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		__this->set_m_Normal_0(L_1);
		Vector3_t2903530434  L_2 = ___inNormal0;
		Vector3_t2903530434  L_3 = ___inPoint1;
		float L_4 = Vector3_Dot_m2210511857(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		__this->set_m_Distance_1(((-L_4)));
		return;
	}
}
extern "C"  void Plane__ctor_m2922395188_AdjustorThunk (RuntimeObject * __this, Vector3_t2903530434  ___inNormal0, Vector3_t2903530434  ___inPoint1, const RuntimeMethod* method)
{
	Plane_t2300588969 * _thisAdjusted = reinterpret_cast<Plane_t2300588969 *>(__this + 1);
	Plane__ctor_m2922395188(_thisAdjusted, ___inNormal0, ___inPoint1, method);
}
// System.Boolean UnityEngine.Plane::Raycast(UnityEngine.Ray,System.Single&)
extern "C"  bool Plane_Raycast_m235007613 (Plane_t2300588969 * __this, Ray_t3821377119  ___ray0, float* ___enter1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Plane_Raycast_m235007613_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	bool V_2 = false;
	{
		Vector3_t2903530434  L_0 = Ray_get_direction_m3512869925((&___ray0), /*hidden argument*/NULL);
		Vector3_t2903530434  L_1 = __this->get_m_Normal_0();
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t2903530434_il2cpp_TypeInfo_var);
		float L_2 = Vector3_Dot_m2210511857(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		Vector3_t2903530434  L_3 = Ray_get_origin_m126064229((&___ray0), /*hidden argument*/NULL);
		Vector3_t2903530434  L_4 = __this->get_m_Normal_0();
		float L_5 = Vector3_Dot_m2210511857(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		float L_6 = __this->get_m_Distance_1();
		V_1 = ((float)((float)((-L_5))-(float)L_6));
		float L_7 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t4094287654_il2cpp_TypeInfo_var);
		bool L_8 = Mathf_Approximately_m3781428536(NULL /*static, unused*/, L_7, (0.0f), /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_004e;
		}
	}
	{
		float* L_9 = ___enter1;
		*((float*)(L_9)) = (float)(0.0f);
		V_2 = (bool)0;
		goto IL_0062;
	}

IL_004e:
	{
		float* L_10 = ___enter1;
		float L_11 = V_1;
		float L_12 = V_0;
		*((float*)(L_10)) = (float)((float)((float)L_11/(float)L_12));
		float* L_13 = ___enter1;
		V_2 = (bool)((((float)(*((float*)L_13))) > ((float)(0.0f)))? 1 : 0);
		goto IL_0062;
	}

IL_0062:
	{
		bool L_14 = V_2;
		return L_14;
	}
}
extern "C"  bool Plane_Raycast_m235007613_AdjustorThunk (RuntimeObject * __this, Ray_t3821377119  ___ray0, float* ___enter1, const RuntimeMethod* method)
{
	Plane_t2300588969 * _thisAdjusted = reinterpret_cast<Plane_t2300588969 *>(__this + 1);
	return Plane_Raycast_m235007613(_thisAdjusted, ___ray0, ___enter1, method);
}
// System.String UnityEngine.Plane::ToString()
extern "C"  String_t* Plane_ToString_m2513597252 (Plane_t2300588969 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Plane_ToString_m2513597252_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	{
		ObjectU5BU5D_t3270211303* L_0 = ((ObjectU5BU5D_t3270211303*)SZArrayNew(ObjectU5BU5D_t3270211303_il2cpp_TypeInfo_var, (uint32_t)4));
		Vector3_t2903530434 * L_1 = __this->get_address_of_m_Normal_0();
		float L_2 = L_1->get_x_1();
		float L_3 = L_2;
		RuntimeObject * L_4 = Box(Single_t3045349759_il2cpp_TypeInfo_var, &L_3);
		NullCheck(L_0);
		ArrayElementTypeCheck (L_0, L_4);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (RuntimeObject *)L_4);
		ObjectU5BU5D_t3270211303* L_5 = L_0;
		Vector3_t2903530434 * L_6 = __this->get_address_of_m_Normal_0();
		float L_7 = L_6->get_y_2();
		float L_8 = L_7;
		RuntimeObject * L_9 = Box(Single_t3045349759_il2cpp_TypeInfo_var, &L_8);
		NullCheck(L_5);
		ArrayElementTypeCheck (L_5, L_9);
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(1), (RuntimeObject *)L_9);
		ObjectU5BU5D_t3270211303* L_10 = L_5;
		Vector3_t2903530434 * L_11 = __this->get_address_of_m_Normal_0();
		float L_12 = L_11->get_z_3();
		float L_13 = L_12;
		RuntimeObject * L_14 = Box(Single_t3045349759_il2cpp_TypeInfo_var, &L_13);
		NullCheck(L_10);
		ArrayElementTypeCheck (L_10, L_14);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(2), (RuntimeObject *)L_14);
		ObjectU5BU5D_t3270211303* L_15 = L_10;
		float L_16 = __this->get_m_Distance_1();
		float L_17 = L_16;
		RuntimeObject * L_18 = Box(Single_t3045349759_il2cpp_TypeInfo_var, &L_17);
		NullCheck(L_15);
		ArrayElementTypeCheck (L_15, L_18);
		(L_15)->SetAt(static_cast<il2cpp_array_size_t>(3), (RuntimeObject *)L_18);
		String_t* L_19 = UnityString_Format_m2401112968(NULL /*static, unused*/, _stringLiteral1289564811, L_15, /*hidden argument*/NULL);
		V_0 = L_19;
		goto IL_005e;
	}

IL_005e:
	{
		String_t* L_20 = V_0;
		return L_20;
	}
}
extern "C"  String_t* Plane_ToString_m2513597252_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Plane_t2300588969 * _thisAdjusted = reinterpret_cast<Plane_t2300588969 *>(__this + 1);
	return Plane_ToString_m2513597252(_thisAdjusted, method);
}
// System.Boolean UnityEngine.Playables.AudioPlayableGraphExtensions::InternalCreateAudioOutput(UnityEngine.Playables.PlayableGraph&,System.String,UnityEngine.Playables.PlayableOutputHandle&)
extern "C"  bool AudioPlayableGraphExtensions_InternalCreateAudioOutput_m1894900323 (RuntimeObject * __this /* static, unused */, PlayableGraph_t1110114554 * ___graph0, String_t* ___name1, PlayableOutputHandle_t2200222555 * ___handle2, const RuntimeMethod* method)
{
	bool V_0 = false;
	{
		PlayableGraph_t1110114554 * L_0 = ___graph0;
		String_t* L_1 = ___name1;
		PlayableOutputHandle_t2200222555 * L_2 = ___handle2;
		bool L_3 = AudioPlayableGraphExtensions_INTERNAL_CALL_InternalCreateAudioOutput_m344357075(NULL /*static, unused*/, L_0, L_1, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		goto IL_000f;
	}

IL_000f:
	{
		bool L_4 = V_0;
		return L_4;
	}
}
// System.Boolean UnityEngine.Playables.AudioPlayableGraphExtensions::INTERNAL_CALL_InternalCreateAudioOutput(UnityEngine.Playables.PlayableGraph&,System.String,UnityEngine.Playables.PlayableOutputHandle&)
extern "C"  bool AudioPlayableGraphExtensions_INTERNAL_CALL_InternalCreateAudioOutput_m344357075 (RuntimeObject * __this /* static, unused */, PlayableGraph_t1110114554 * ___graph0, String_t* ___name1, PlayableOutputHandle_t2200222555 * ___handle2, const RuntimeMethod* method)
{
	typedef bool (*AudioPlayableGraphExtensions_INTERNAL_CALL_InternalCreateAudioOutput_m344357075_ftn) (PlayableGraph_t1110114554 *, String_t*, PlayableOutputHandle_t2200222555 *);
	static AudioPlayableGraphExtensions_INTERNAL_CALL_InternalCreateAudioOutput_m344357075_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AudioPlayableGraphExtensions_INTERNAL_CALL_InternalCreateAudioOutput_m344357075_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Playables.AudioPlayableGraphExtensions::INTERNAL_CALL_InternalCreateAudioOutput(UnityEngine.Playables.PlayableGraph&,System.String,UnityEngine.Playables.PlayableOutputHandle&)");
	bool retVal = _il2cpp_icall_func(___graph0, ___name1, ___handle2);
	return retVal;
}
// System.Void UnityEngine.Playables.Playable::.ctor(UnityEngine.Playables.PlayableHandle)
extern "C"  void Playable__ctor_m1598574293 (Playable_t735025802 * __this, PlayableHandle_t1286828706  ___handle0, const RuntimeMethod* method)
{
	{
		PlayableHandle_t1286828706  L_0 = ___handle0;
		__this->set_m_Handle_0(L_0);
		return;
	}
}
extern "C"  void Playable__ctor_m1598574293_AdjustorThunk (RuntimeObject * __this, PlayableHandle_t1286828706  ___handle0, const RuntimeMethod* method)
{
	Playable_t735025802 * _thisAdjusted = reinterpret_cast<Playable_t735025802 *>(__this + 1);
	Playable__ctor_m1598574293(_thisAdjusted, ___handle0, method);
}
// UnityEngine.Playables.Playable UnityEngine.Playables.Playable::get_Null()
extern "C"  Playable_t735025802  Playable_get_Null_m1400628986 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Playable_get_Null_m1400628986_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Playable_t735025802  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		IL2CPP_RUNTIME_CLASS_INIT(Playable_t735025802_il2cpp_TypeInfo_var);
		Playable_t735025802  L_0 = ((Playable_t735025802_StaticFields*)il2cpp_codegen_static_fields_for(Playable_t735025802_il2cpp_TypeInfo_var))->get_m_NullPlayable_1();
		V_0 = L_0;
		goto IL_000c;
	}

IL_000c:
	{
		Playable_t735025802  L_1 = V_0;
		return L_1;
	}
}
// UnityEngine.Playables.Playable UnityEngine.Playables.Playable::Create(UnityEngine.Playables.PlayableGraph,System.Int32)
extern "C"  Playable_t735025802  Playable_Create_m1247326070 (RuntimeObject * __this /* static, unused */, PlayableGraph_t1110114554  ___graph0, int32_t ___inputCount1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Playable_Create_m1247326070_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Playable_t735025802  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Playable_t735025802  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		PlayableHandle_t1286828706  L_0 = PlayableGraph_CreatePlayableHandle_m1321329662((&___graph0), /*hidden argument*/NULL);
		Playable__ctor_m1598574293((&V_0), L_0, /*hidden argument*/NULL);
		Playable_t735025802  L_1 = V_0;
		int32_t L_2 = ___inputCount1;
		PlayableExtensions_SetInputCount_TisPlayable_t735025802_m769118885(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/PlayableExtensions_SetInputCount_TisPlayable_t735025802_m769118885_RuntimeMethod_var);
		Playable_t735025802  L_3 = V_0;
		V_1 = L_3;
		goto IL_001d;
	}

IL_001d:
	{
		Playable_t735025802  L_4 = V_1;
		return L_4;
	}
}
// UnityEngine.Playables.PlayableHandle UnityEngine.Playables.Playable::GetHandle()
extern "C"  PlayableHandle_t1286828706  Playable_GetHandle_m849086377 (Playable_t735025802 * __this, const RuntimeMethod* method)
{
	PlayableHandle_t1286828706  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		PlayableHandle_t1286828706  L_0 = __this->get_m_Handle_0();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		PlayableHandle_t1286828706  L_1 = V_0;
		return L_1;
	}
}
extern "C"  PlayableHandle_t1286828706  Playable_GetHandle_m849086377_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Playable_t735025802 * _thisAdjusted = reinterpret_cast<Playable_t735025802 *>(__this + 1);
	return Playable_GetHandle_m849086377(_thisAdjusted, method);
}
// System.Type UnityEngine.Playables.Playable::GetPlayableType()
extern "C"  Type_t * Playable_GetPlayableType_m12689584 (Playable_t735025802 * __this, const RuntimeMethod* method)
{
	PlayableHandle_t1286828706  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Type_t * V_1 = NULL;
	{
		PlayableHandle_t1286828706  L_0 = Playable_GetHandle_m849086377(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		Type_t * L_1 = PlayableHandle_GetPlayableType_m1758692074((&V_0), /*hidden argument*/NULL);
		V_1 = L_1;
		goto IL_0015;
	}

IL_0015:
	{
		Type_t * L_2 = V_1;
		return L_2;
	}
}
extern "C"  Type_t * Playable_GetPlayableType_m12689584_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Playable_t735025802 * _thisAdjusted = reinterpret_cast<Playable_t735025802 *>(__this + 1);
	return Playable_GetPlayableType_m12689584(_thisAdjusted, method);
}
// System.Boolean UnityEngine.Playables.Playable::Equals(UnityEngine.Playables.Playable)
extern "C"  bool Playable_Equals_m2449635747 (Playable_t735025802 * __this, Playable_t735025802  ___other0, const RuntimeMethod* method)
{
	bool V_0 = false;
	{
		PlayableHandle_t1286828706  L_0 = Playable_GetHandle_m849086377(__this, /*hidden argument*/NULL);
		PlayableHandle_t1286828706  L_1 = Playable_GetHandle_m849086377((&___other0), /*hidden argument*/NULL);
		bool L_2 = PlayableHandle_op_Equality_m2726566843(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_0019;
	}

IL_0019:
	{
		bool L_3 = V_0;
		return L_3;
	}
}
extern "C"  bool Playable_Equals_m2449635747_AdjustorThunk (RuntimeObject * __this, Playable_t735025802  ___other0, const RuntimeMethod* method)
{
	Playable_t735025802 * _thisAdjusted = reinterpret_cast<Playable_t735025802 *>(__this + 1);
	return Playable_Equals_m2449635747(_thisAdjusted, ___other0, method);
}
// System.Void UnityEngine.Playables.Playable::.cctor()
extern "C"  void Playable__cctor_m698787543 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Playable__cctor_m698787543_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		PlayableHandle_t1286828706  L_0 = PlayableHandle_get_Null_m1252146945(NULL /*static, unused*/, /*hidden argument*/NULL);
		Playable_t735025802  L_1;
		memset(&L_1, 0, sizeof(L_1));
		Playable__ctor_m1598574293((&L_1), L_0, /*hidden argument*/NULL);
		((Playable_t735025802_StaticFields*)il2cpp_codegen_static_fields_for(Playable_t735025802_il2cpp_TypeInfo_var))->set_m_NullPlayable_1(L_1);
		return;
	}
}
// System.Void UnityEngine.Playables.PlayableAsset::.ctor()
extern "C"  void PlayableAsset__ctor_m3257919054 (PlayableAsset_t2449544405 * __this, const RuntimeMethod* method)
{
	{
		ScriptableObject__ctor_m4110805243(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Double UnityEngine.Playables.PlayableAsset::get_duration()
extern "C"  double PlayableAsset_get_duration_m952498650 (PlayableAsset_t2449544405 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayableAsset_get_duration_m952498650_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	double V_0 = 0.0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayableBinding_t3706191642_il2cpp_TypeInfo_var);
		double L_0 = ((PlayableBinding_t3706191642_StaticFields*)il2cpp_codegen_static_fields_for(PlayableBinding_t3706191642_il2cpp_TypeInfo_var))->get_DefaultDuration_1();
		V_0 = L_0;
		goto IL_000c;
	}

IL_000c:
	{
		double L_1 = V_0;
		return L_1;
	}
}
// System.Void UnityEngine.Playables.PlayableAsset::Internal_CreatePlayable(UnityEngine.Playables.PlayableAsset,UnityEngine.Playables.PlayableGraph,UnityEngine.GameObject,System.IntPtr)
extern "C"  void PlayableAsset_Internal_CreatePlayable_m4205524028 (RuntimeObject * __this /* static, unused */, PlayableAsset_t2449544405 * ___asset0, PlayableGraph_t1110114554  ___graph1, GameObject_t1811656094 * ___go2, IntPtr_t ___ptr3, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayableAsset_Internal_CreatePlayable_m4205524028_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Playable_t735025802  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Playable_t735025802 * V_1 = NULL;
	{
		PlayableAsset_t2449544405 * L_0 = ___asset0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1502412432_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_m2342070775(NULL /*static, unused*/, L_0, (Object_t1502412432 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0018;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Playable_t735025802_il2cpp_TypeInfo_var);
		Playable_t735025802  L_2 = Playable_get_Null_m1400628986(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_0021;
	}

IL_0018:
	{
		PlayableAsset_t2449544405 * L_3 = ___asset0;
		PlayableGraph_t1110114554  L_4 = ___graph1;
		GameObject_t1811656094 * L_5 = ___go2;
		NullCheck(L_3);
		Playable_t735025802  L_6 = VirtFuncInvoker2< Playable_t735025802 , PlayableGraph_t1110114554 , GameObject_t1811656094 * >::Invoke(4 /* UnityEngine.Playables.Playable UnityEngine.Playables.PlayableAsset::CreatePlayable(UnityEngine.Playables.PlayableGraph,UnityEngine.GameObject) */, L_3, L_4, L_5);
		V_0 = L_6;
	}

IL_0021:
	{
		void* L_7 = IntPtr_ToPointer_m745807476((&___ptr3), /*hidden argument*/NULL);
		V_1 = (Playable_t735025802 *)L_7;
		Playable_t735025802 * L_8 = V_1;
		Playable_t735025802  L_9 = V_0;
		*(Playable_t735025802 *)L_8 = L_9;
		return;
	}
}
// System.Void UnityEngine.Playables.PlayableAsset::Internal_GetPlayableAssetDuration(UnityEngine.Playables.PlayableAsset,System.IntPtr)
extern "C"  void PlayableAsset_Internal_GetPlayableAssetDuration_m3564992697 (RuntimeObject * __this /* static, unused */, PlayableAsset_t2449544405 * ___asset0, IntPtr_t ___ptrToDouble1, const RuntimeMethod* method)
{
	double V_0 = 0.0;
	double* V_1 = NULL;
	{
		PlayableAsset_t2449544405 * L_0 = ___asset0;
		NullCheck(L_0);
		double L_1 = VirtFuncInvoker0< double >::Invoke(5 /* System.Double UnityEngine.Playables.PlayableAsset::get_duration() */, L_0);
		V_0 = L_1;
		void* L_2 = IntPtr_ToPointer_m745807476((&___ptrToDouble1), /*hidden argument*/NULL);
		V_1 = (double*)L_2;
		double* L_3 = V_1;
		double L_4 = V_0;
		*((double*)(L_3)) = (double)L_4;
		return;
	}
}
// System.Void UnityEngine.Playables.PlayableBehaviour::.ctor()
extern "C"  void PlayableBehaviour__ctor_m956721147 (PlayableBehaviour_t1007788125 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m2906060009(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Playables.PlayableBehaviour::OnGraphStart(UnityEngine.Playables.Playable)
extern "C"  void PlayableBehaviour_OnGraphStart_m1238511900 (PlayableBehaviour_t1007788125 * __this, Playable_t735025802  ___playable0, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Void UnityEngine.Playables.PlayableBehaviour::OnGraphStop(UnityEngine.Playables.Playable)
extern "C"  void PlayableBehaviour_OnGraphStop_m2582814231 (PlayableBehaviour_t1007788125 * __this, Playable_t735025802  ___playable0, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Void UnityEngine.Playables.PlayableBehaviour::OnPlayableCreate(UnityEngine.Playables.Playable)
extern "C"  void PlayableBehaviour_OnPlayableCreate_m836212194 (PlayableBehaviour_t1007788125 * __this, Playable_t735025802  ___playable0, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Void UnityEngine.Playables.PlayableBehaviour::OnPlayableDestroy(UnityEngine.Playables.Playable)
extern "C"  void PlayableBehaviour_OnPlayableDestroy_m2266837032 (PlayableBehaviour_t1007788125 * __this, Playable_t735025802  ___playable0, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Void UnityEngine.Playables.PlayableBehaviour::OnBehaviourPlay(UnityEngine.Playables.Playable,UnityEngine.Playables.FrameData)
extern "C"  void PlayableBehaviour_OnBehaviourPlay_m838229352 (PlayableBehaviour_t1007788125 * __this, Playable_t735025802  ___playable0, FrameData_t2240265764  ___info1, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Void UnityEngine.Playables.PlayableBehaviour::OnBehaviourPause(UnityEngine.Playables.Playable,UnityEngine.Playables.FrameData)
extern "C"  void PlayableBehaviour_OnBehaviourPause_m2218655296 (PlayableBehaviour_t1007788125 * __this, Playable_t735025802  ___playable0, FrameData_t2240265764  ___info1, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Void UnityEngine.Playables.PlayableBehaviour::PrepareFrame(UnityEngine.Playables.Playable,UnityEngine.Playables.FrameData)
extern "C"  void PlayableBehaviour_PrepareFrame_m182375349 (PlayableBehaviour_t1007788125 * __this, Playable_t735025802  ___playable0, FrameData_t2240265764  ___info1, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Void UnityEngine.Playables.PlayableBehaviour::ProcessFrame(UnityEngine.Playables.Playable,UnityEngine.Playables.FrameData,System.Object)
extern "C"  void PlayableBehaviour_ProcessFrame_m1614807512 (PlayableBehaviour_t1007788125 * __this, Playable_t735025802  ___playable0, FrameData_t2240265764  ___info1, RuntimeObject * ___playerData2, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Object UnityEngine.Playables.PlayableBehaviour::Clone()
extern "C"  RuntimeObject * PlayableBehaviour_Clone_m482894514 (PlayableBehaviour_t1007788125 * __this, const RuntimeMethod* method)
{
	RuntimeObject * V_0 = NULL;
	{
		RuntimeObject * L_0 = Object_MemberwiseClone_m500616104(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		RuntimeObject * L_1 = V_0;
		return L_1;
	}
}


// Conversion methods for marshalling of: UnityEngine.Playables.PlayableBinding
extern "C" void PlayableBinding_t3706191642_marshal_pinvoke(const PlayableBinding_t3706191642& unmarshaled, PlayableBinding_t3706191642_marshaled_pinvoke& marshaled)
{
	Il2CppCodeGenException* ___U3CsourceBindingTypeU3Ek__BackingField_5Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field '<sourceBindingType>k__BackingField' of type 'PlayableBinding': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___U3CsourceBindingTypeU3Ek__BackingField_5Exception);
}
extern "C" void PlayableBinding_t3706191642_marshal_pinvoke_back(const PlayableBinding_t3706191642_marshaled_pinvoke& marshaled, PlayableBinding_t3706191642& unmarshaled)
{
	Il2CppCodeGenException* ___U3CsourceBindingTypeU3Ek__BackingField_5Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field '<sourceBindingType>k__BackingField' of type 'PlayableBinding': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___U3CsourceBindingTypeU3Ek__BackingField_5Exception);
}
// Conversion method for clean up from marshalling of: UnityEngine.Playables.PlayableBinding
extern "C" void PlayableBinding_t3706191642_marshal_pinvoke_cleanup(PlayableBinding_t3706191642_marshaled_pinvoke& marshaled)
{
}


// Conversion methods for marshalling of: UnityEngine.Playables.PlayableBinding
extern "C" void PlayableBinding_t3706191642_marshal_com(const PlayableBinding_t3706191642& unmarshaled, PlayableBinding_t3706191642_marshaled_com& marshaled)
{
	Il2CppCodeGenException* ___U3CsourceBindingTypeU3Ek__BackingField_5Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field '<sourceBindingType>k__BackingField' of type 'PlayableBinding': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___U3CsourceBindingTypeU3Ek__BackingField_5Exception);
}
extern "C" void PlayableBinding_t3706191642_marshal_com_back(const PlayableBinding_t3706191642_marshaled_com& marshaled, PlayableBinding_t3706191642& unmarshaled)
{
	Il2CppCodeGenException* ___U3CsourceBindingTypeU3Ek__BackingField_5Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field '<sourceBindingType>k__BackingField' of type 'PlayableBinding': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___U3CsourceBindingTypeU3Ek__BackingField_5Exception);
}
// Conversion method for clean up from marshalling of: UnityEngine.Playables.PlayableBinding
extern "C" void PlayableBinding_t3706191642_marshal_com_cleanup(PlayableBinding_t3706191642_marshaled_com& marshaled)
{
}
// System.Void UnityEngine.Playables.PlayableBinding::.cctor()
extern "C"  void PlayableBinding__cctor_m3291100432 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayableBinding__cctor_m3291100432_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		((PlayableBinding_t3706191642_StaticFields*)il2cpp_codegen_static_fields_for(PlayableBinding_t3706191642_il2cpp_TypeInfo_var))->set_None_0(((PlayableBindingU5BU5D_t3242831679*)SZArrayNew(PlayableBindingU5BU5D_t3242831679_il2cpp_TypeInfo_var, (uint32_t)0)));
		((PlayableBinding_t3706191642_StaticFields*)il2cpp_codegen_static_fields_for(PlayableBinding_t3706191642_il2cpp_TypeInfo_var))->set_DefaultDuration_1((std::numeric_limits<double>::infinity()));
		return;
	}
}
// System.Boolean UnityEngine.Playables.PlayableGraph::CreateScriptOutputInternal(UnityEngine.Playables.PlayableGraph&,System.String,UnityEngine.Playables.PlayableOutputHandle&)
extern "C"  bool PlayableGraph_CreateScriptOutputInternal_m3787016967 (RuntimeObject * __this /* static, unused */, PlayableGraph_t1110114554 * ___graph0, String_t* ___name1, PlayableOutputHandle_t2200222555 * ___handle2, const RuntimeMethod* method)
{
	bool V_0 = false;
	{
		PlayableGraph_t1110114554 * L_0 = ___graph0;
		String_t* L_1 = ___name1;
		PlayableOutputHandle_t2200222555 * L_2 = ___handle2;
		bool L_3 = PlayableGraph_INTERNAL_CALL_CreateScriptOutputInternal_m3041400947(NULL /*static, unused*/, L_0, L_1, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		goto IL_000f;
	}

IL_000f:
	{
		bool L_4 = V_0;
		return L_4;
	}
}
// System.Boolean UnityEngine.Playables.PlayableGraph::INTERNAL_CALL_CreateScriptOutputInternal(UnityEngine.Playables.PlayableGraph&,System.String,UnityEngine.Playables.PlayableOutputHandle&)
extern "C"  bool PlayableGraph_INTERNAL_CALL_CreateScriptOutputInternal_m3041400947 (RuntimeObject * __this /* static, unused */, PlayableGraph_t1110114554 * ___graph0, String_t* ___name1, PlayableOutputHandle_t2200222555 * ___handle2, const RuntimeMethod* method)
{
	typedef bool (*PlayableGraph_INTERNAL_CALL_CreateScriptOutputInternal_m3041400947_ftn) (PlayableGraph_t1110114554 *, String_t*, PlayableOutputHandle_t2200222555 *);
	static PlayableGraph_INTERNAL_CALL_CreateScriptOutputInternal_m3041400947_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (PlayableGraph_INTERNAL_CALL_CreateScriptOutputInternal_m3041400947_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Playables.PlayableGraph::INTERNAL_CALL_CreateScriptOutputInternal(UnityEngine.Playables.PlayableGraph&,System.String,UnityEngine.Playables.PlayableOutputHandle&)");
	bool retVal = _il2cpp_icall_func(___graph0, ___name1, ___handle2);
	return retVal;
}
// UnityEngine.Playables.PlayableHandle UnityEngine.Playables.PlayableGraph::CreatePlayableHandle()
extern "C"  PlayableHandle_t1286828706  PlayableGraph_CreatePlayableHandle_m1321329662 (PlayableGraph_t1110114554 * __this, const RuntimeMethod* method)
{
	PlayableHandle_t1286828706  V_0;
	memset(&V_0, 0, sizeof(V_0));
	PlayableHandle_t1286828706  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		PlayableHandle_t1286828706  L_0 = PlayableHandle_get_Null_m1252146945(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		bool L_1 = PlayableGraph_CreatePlayableHandleInternal_m568622090(NULL /*static, unused*/, __this, (&V_0), /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_001f;
		}
	}
	{
		PlayableHandle_t1286828706  L_2 = PlayableHandle_get_Null_m1252146945(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_1 = L_2;
		goto IL_0026;
	}

IL_001f:
	{
		PlayableHandle_t1286828706  L_3 = V_0;
		V_1 = L_3;
		goto IL_0026;
	}

IL_0026:
	{
		PlayableHandle_t1286828706  L_4 = V_1;
		return L_4;
	}
}
extern "C"  PlayableHandle_t1286828706  PlayableGraph_CreatePlayableHandle_m1321329662_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	PlayableGraph_t1110114554 * _thisAdjusted = reinterpret_cast<PlayableGraph_t1110114554 *>(__this + 1);
	return PlayableGraph_CreatePlayableHandle_m1321329662(_thisAdjusted, method);
}
// System.Boolean UnityEngine.Playables.PlayableGraph::CreatePlayableHandleInternal(UnityEngine.Playables.PlayableGraph&,UnityEngine.Playables.PlayableHandle&)
extern "C"  bool PlayableGraph_CreatePlayableHandleInternal_m568622090 (RuntimeObject * __this /* static, unused */, PlayableGraph_t1110114554 * ___graph0, PlayableHandle_t1286828706 * ___handle1, const RuntimeMethod* method)
{
	bool V_0 = false;
	{
		PlayableGraph_t1110114554 * L_0 = ___graph0;
		PlayableHandle_t1286828706 * L_1 = ___handle1;
		bool L_2 = PlayableGraph_INTERNAL_CALL_CreatePlayableHandleInternal_m2720952989(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_000e;
	}

IL_000e:
	{
		bool L_3 = V_0;
		return L_3;
	}
}
// System.Boolean UnityEngine.Playables.PlayableGraph::INTERNAL_CALL_CreatePlayableHandleInternal(UnityEngine.Playables.PlayableGraph&,UnityEngine.Playables.PlayableHandle&)
extern "C"  bool PlayableGraph_INTERNAL_CALL_CreatePlayableHandleInternal_m2720952989 (RuntimeObject * __this /* static, unused */, PlayableGraph_t1110114554 * ___graph0, PlayableHandle_t1286828706 * ___handle1, const RuntimeMethod* method)
{
	typedef bool (*PlayableGraph_INTERNAL_CALL_CreatePlayableHandleInternal_m2720952989_ftn) (PlayableGraph_t1110114554 *, PlayableHandle_t1286828706 *);
	static PlayableGraph_INTERNAL_CALL_CreatePlayableHandleInternal_m2720952989_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (PlayableGraph_INTERNAL_CALL_CreatePlayableHandleInternal_m2720952989_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Playables.PlayableGraph::INTERNAL_CALL_CreatePlayableHandleInternal(UnityEngine.Playables.PlayableGraph&,UnityEngine.Playables.PlayableHandle&)");
	bool retVal = _il2cpp_icall_func(___graph0, ___handle1);
	return retVal;
}
// System.Boolean UnityEngine.Playables.PlayableHandle::IsValid()
extern "C"  bool PlayableHandle_IsValid_m169138349 (PlayableHandle_t1286828706 * __this, const RuntimeMethod* method)
{
	bool V_0 = false;
	{
		bool L_0 = PlayableHandle_IsValidInternal_m3486814698(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		bool L_1 = V_0;
		return L_1;
	}
}
extern "C"  bool PlayableHandle_IsValid_m169138349_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	PlayableHandle_t1286828706 * _thisAdjusted = reinterpret_cast<PlayableHandle_t1286828706 *>(__this + 1);
	return PlayableHandle_IsValid_m169138349(_thisAdjusted, method);
}
// System.Boolean UnityEngine.Playables.PlayableHandle::IsValidInternal(UnityEngine.Playables.PlayableHandle&)
extern "C"  bool PlayableHandle_IsValidInternal_m3486814698 (RuntimeObject * __this /* static, unused */, PlayableHandle_t1286828706 * ___playable0, const RuntimeMethod* method)
{
	bool V_0 = false;
	{
		PlayableHandle_t1286828706 * L_0 = ___playable0;
		bool L_1 = PlayableHandle_INTERNAL_CALL_IsValidInternal_m3801525674(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_000d;
	}

IL_000d:
	{
		bool L_2 = V_0;
		return L_2;
	}
}
// System.Boolean UnityEngine.Playables.PlayableHandle::INTERNAL_CALL_IsValidInternal(UnityEngine.Playables.PlayableHandle&)
extern "C"  bool PlayableHandle_INTERNAL_CALL_IsValidInternal_m3801525674 (RuntimeObject * __this /* static, unused */, PlayableHandle_t1286828706 * ___playable0, const RuntimeMethod* method)
{
	typedef bool (*PlayableHandle_INTERNAL_CALL_IsValidInternal_m3801525674_ftn) (PlayableHandle_t1286828706 *);
	static PlayableHandle_INTERNAL_CALL_IsValidInternal_m3801525674_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (PlayableHandle_INTERNAL_CALL_IsValidInternal_m3801525674_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Playables.PlayableHandle::INTERNAL_CALL_IsValidInternal(UnityEngine.Playables.PlayableHandle&)");
	bool retVal = _il2cpp_icall_func(___playable0);
	return retVal;
}
// System.Type UnityEngine.Playables.PlayableHandle::GetPlayableTypeOf(UnityEngine.Playables.PlayableHandle&)
extern "C"  Type_t * PlayableHandle_GetPlayableTypeOf_m194458495 (RuntimeObject * __this /* static, unused */, PlayableHandle_t1286828706 * ___playable0, const RuntimeMethod* method)
{
	Type_t * V_0 = NULL;
	{
		PlayableHandle_t1286828706 * L_0 = ___playable0;
		Type_t * L_1 = PlayableHandle_INTERNAL_CALL_GetPlayableTypeOf_m725853957(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_000d;
	}

IL_000d:
	{
		Type_t * L_2 = V_0;
		return L_2;
	}
}
// System.Type UnityEngine.Playables.PlayableHandle::INTERNAL_CALL_GetPlayableTypeOf(UnityEngine.Playables.PlayableHandle&)
extern "C"  Type_t * PlayableHandle_INTERNAL_CALL_GetPlayableTypeOf_m725853957 (RuntimeObject * __this /* static, unused */, PlayableHandle_t1286828706 * ___playable0, const RuntimeMethod* method)
{
	typedef Type_t * (*PlayableHandle_INTERNAL_CALL_GetPlayableTypeOf_m725853957_ftn) (PlayableHandle_t1286828706 *);
	static PlayableHandle_INTERNAL_CALL_GetPlayableTypeOf_m725853957_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (PlayableHandle_INTERNAL_CALL_GetPlayableTypeOf_m725853957_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Playables.PlayableHandle::INTERNAL_CALL_GetPlayableTypeOf(UnityEngine.Playables.PlayableHandle&)");
	Type_t * retVal = _il2cpp_icall_func(___playable0);
	return retVal;
}
// System.Type UnityEngine.Playables.PlayableHandle::GetPlayableType()
extern "C"  Type_t * PlayableHandle_GetPlayableType_m1758692074 (PlayableHandle_t1286828706 * __this, const RuntimeMethod* method)
{
	Type_t * V_0 = NULL;
	{
		Type_t * L_0 = PlayableHandle_GetPlayableTypeOf_m194458495(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		Type_t * L_1 = V_0;
		return L_1;
	}
}
extern "C"  Type_t * PlayableHandle_GetPlayableType_m1758692074_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	PlayableHandle_t1286828706 * _thisAdjusted = reinterpret_cast<PlayableHandle_t1286828706 *>(__this + 1);
	return PlayableHandle_GetPlayableType_m1758692074(_thisAdjusted, method);
}
// UnityEngine.Playables.PlayableHandle UnityEngine.Playables.PlayableHandle::get_Null()
extern "C"  PlayableHandle_t1286828706  PlayableHandle_get_Null_m1252146945 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayableHandle_get_Null_m1252146945_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	PlayableHandle_t1286828706  V_0;
	memset(&V_0, 0, sizeof(V_0));
	PlayableHandle_t1286828706  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		Initobj (PlayableHandle_t1286828706_il2cpp_TypeInfo_var, (&V_0));
		(&V_0)->set_m_Version_1(((int32_t)10));
		PlayableHandle_t1286828706  L_0 = V_0;
		V_1 = L_0;
		goto IL_0019;
	}

IL_0019:
	{
		PlayableHandle_t1286828706  L_1 = V_1;
		return L_1;
	}
}
// System.Int32 UnityEngine.Playables.PlayableHandle::GetInputCount()
extern "C"  int32_t PlayableHandle_GetInputCount_m3374229925 (PlayableHandle_t1286828706 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = PlayableHandle_GetInputCountInternal_m3943905105(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		int32_t L_1 = V_0;
		return L_1;
	}
}
extern "C"  int32_t PlayableHandle_GetInputCount_m3374229925_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	PlayableHandle_t1286828706 * _thisAdjusted = reinterpret_cast<PlayableHandle_t1286828706 *>(__this + 1);
	return PlayableHandle_GetInputCount_m3374229925(_thisAdjusted, method);
}
// System.Void UnityEngine.Playables.PlayableHandle::SetInputCount(System.Int32)
extern "C"  void PlayableHandle_SetInputCount_m525897292 (PlayableHandle_t1286828706 * __this, int32_t ___value0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___value0;
		PlayableHandle_SetInputCountInternal_m54801121(NULL /*static, unused*/, __this, L_0, /*hidden argument*/NULL);
		return;
	}
}
extern "C"  void PlayableHandle_SetInputCount_m525897292_AdjustorThunk (RuntimeObject * __this, int32_t ___value0, const RuntimeMethod* method)
{
	PlayableHandle_t1286828706 * _thisAdjusted = reinterpret_cast<PlayableHandle_t1286828706 *>(__this + 1);
	PlayableHandle_SetInputCount_m525897292(_thisAdjusted, ___value0, method);
}
// UnityEngine.Playables.PlayState UnityEngine.Playables.PlayableHandle::GetPlayState()
extern "C"  int32_t PlayableHandle_GetPlayState_m2902646787 (PlayableHandle_t1286828706 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = PlayableHandle_GetPlayStateInternal_m3140903434(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		int32_t L_1 = V_0;
		return L_1;
	}
}
extern "C"  int32_t PlayableHandle_GetPlayState_m2902646787_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	PlayableHandle_t1286828706 * _thisAdjusted = reinterpret_cast<PlayableHandle_t1286828706 *>(__this + 1);
	return PlayableHandle_GetPlayState_m2902646787(_thisAdjusted, method);
}
// System.Void UnityEngine.Playables.PlayableHandle::SetPlayState(UnityEngine.Playables.PlayState)
extern "C"  void PlayableHandle_SetPlayState_m1076495759 (PlayableHandle_t1286828706 * __this, int32_t ___value0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___value0;
		PlayableHandle_SetPlayStateInternal_m3078842473(NULL /*static, unused*/, __this, L_0, /*hidden argument*/NULL);
		return;
	}
}
extern "C"  void PlayableHandle_SetPlayState_m1076495759_AdjustorThunk (RuntimeObject * __this, int32_t ___value0, const RuntimeMethod* method)
{
	PlayableHandle_t1286828706 * _thisAdjusted = reinterpret_cast<PlayableHandle_t1286828706 *>(__this + 1);
	PlayableHandle_SetPlayState_m1076495759(_thisAdjusted, ___value0, method);
}
// System.Void UnityEngine.Playables.PlayableHandle::SetTime(System.Double)
extern "C"  void PlayableHandle_SetTime_m260977007 (PlayableHandle_t1286828706 * __this, double ___value0, const RuntimeMethod* method)
{
	{
		double L_0 = ___value0;
		PlayableHandle_SetTimeInternal_m3645103821(NULL /*static, unused*/, __this, L_0, /*hidden argument*/NULL);
		return;
	}
}
extern "C"  void PlayableHandle_SetTime_m260977007_AdjustorThunk (RuntimeObject * __this, double ___value0, const RuntimeMethod* method)
{
	PlayableHandle_t1286828706 * _thisAdjusted = reinterpret_cast<PlayableHandle_t1286828706 *>(__this + 1);
	PlayableHandle_SetTime_m260977007(_thisAdjusted, ___value0, method);
}
// UnityEngine.Playables.PlayState UnityEngine.Playables.PlayableHandle::GetPlayStateInternal(UnityEngine.Playables.PlayableHandle&)
extern "C"  int32_t PlayableHandle_GetPlayStateInternal_m3140903434 (RuntimeObject * __this /* static, unused */, PlayableHandle_t1286828706 * ___playable0, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		PlayableHandle_t1286828706 * L_0 = ___playable0;
		int32_t L_1 = PlayableHandle_INTERNAL_CALL_GetPlayStateInternal_m3759265247(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_000d;
	}

IL_000d:
	{
		int32_t L_2 = V_0;
		return L_2;
	}
}
// UnityEngine.Playables.PlayState UnityEngine.Playables.PlayableHandle::INTERNAL_CALL_GetPlayStateInternal(UnityEngine.Playables.PlayableHandle&)
extern "C"  int32_t PlayableHandle_INTERNAL_CALL_GetPlayStateInternal_m3759265247 (RuntimeObject * __this /* static, unused */, PlayableHandle_t1286828706 * ___playable0, const RuntimeMethod* method)
{
	typedef int32_t (*PlayableHandle_INTERNAL_CALL_GetPlayStateInternal_m3759265247_ftn) (PlayableHandle_t1286828706 *);
	static PlayableHandle_INTERNAL_CALL_GetPlayStateInternal_m3759265247_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (PlayableHandle_INTERNAL_CALL_GetPlayStateInternal_m3759265247_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Playables.PlayableHandle::INTERNAL_CALL_GetPlayStateInternal(UnityEngine.Playables.PlayableHandle&)");
	int32_t retVal = _il2cpp_icall_func(___playable0);
	return retVal;
}
// System.Void UnityEngine.Playables.PlayableHandle::SetPlayStateInternal(UnityEngine.Playables.PlayableHandle&,UnityEngine.Playables.PlayState)
extern "C"  void PlayableHandle_SetPlayStateInternal_m3078842473 (RuntimeObject * __this /* static, unused */, PlayableHandle_t1286828706 * ___playable0, int32_t ___playState1, const RuntimeMethod* method)
{
	{
		PlayableHandle_t1286828706 * L_0 = ___playable0;
		int32_t L_1 = ___playState1;
		PlayableHandle_INTERNAL_CALL_SetPlayStateInternal_m4286520759(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Playables.PlayableHandle::INTERNAL_CALL_SetPlayStateInternal(UnityEngine.Playables.PlayableHandle&,UnityEngine.Playables.PlayState)
extern "C"  void PlayableHandle_INTERNAL_CALL_SetPlayStateInternal_m4286520759 (RuntimeObject * __this /* static, unused */, PlayableHandle_t1286828706 * ___playable0, int32_t ___playState1, const RuntimeMethod* method)
{
	typedef void (*PlayableHandle_INTERNAL_CALL_SetPlayStateInternal_m4286520759_ftn) (PlayableHandle_t1286828706 *, int32_t);
	static PlayableHandle_INTERNAL_CALL_SetPlayStateInternal_m4286520759_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (PlayableHandle_INTERNAL_CALL_SetPlayStateInternal_m4286520759_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Playables.PlayableHandle::INTERNAL_CALL_SetPlayStateInternal(UnityEngine.Playables.PlayableHandle&,UnityEngine.Playables.PlayState)");
	_il2cpp_icall_func(___playable0, ___playState1);
}
// System.Void UnityEngine.Playables.PlayableHandle::SetTimeInternal(UnityEngine.Playables.PlayableHandle&,System.Double)
extern "C"  void PlayableHandle_SetTimeInternal_m3645103821 (RuntimeObject * __this /* static, unused */, PlayableHandle_t1286828706 * ___playable0, double ___time1, const RuntimeMethod* method)
{
	{
		PlayableHandle_t1286828706 * L_0 = ___playable0;
		double L_1 = ___time1;
		PlayableHandle_INTERNAL_CALL_SetTimeInternal_m717980664(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Playables.PlayableHandle::INTERNAL_CALL_SetTimeInternal(UnityEngine.Playables.PlayableHandle&,System.Double)
extern "C"  void PlayableHandle_INTERNAL_CALL_SetTimeInternal_m717980664 (RuntimeObject * __this /* static, unused */, PlayableHandle_t1286828706 * ___playable0, double ___time1, const RuntimeMethod* method)
{
	typedef void (*PlayableHandle_INTERNAL_CALL_SetTimeInternal_m717980664_ftn) (PlayableHandle_t1286828706 *, double);
	static PlayableHandle_INTERNAL_CALL_SetTimeInternal_m717980664_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (PlayableHandle_INTERNAL_CALL_SetTimeInternal_m717980664_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Playables.PlayableHandle::INTERNAL_CALL_SetTimeInternal(UnityEngine.Playables.PlayableHandle&,System.Double)");
	_il2cpp_icall_func(___playable0, ___time1);
}
// System.Void UnityEngine.Playables.PlayableHandle::SetDuration(System.Double)
extern "C"  void PlayableHandle_SetDuration_m2070808009 (PlayableHandle_t1286828706 * __this, double ___value0, const RuntimeMethod* method)
{
	{
		double L_0 = ___value0;
		PlayableHandle_SetDurationInternal_m607008419(NULL /*static, unused*/, __this, L_0, /*hidden argument*/NULL);
		return;
	}
}
extern "C"  void PlayableHandle_SetDuration_m2070808009_AdjustorThunk (RuntimeObject * __this, double ___value0, const RuntimeMethod* method)
{
	PlayableHandle_t1286828706 * _thisAdjusted = reinterpret_cast<PlayableHandle_t1286828706 *>(__this + 1);
	PlayableHandle_SetDuration_m2070808009(_thisAdjusted, ___value0, method);
}
// System.Void UnityEngine.Playables.PlayableHandle::SetDurationInternal(UnityEngine.Playables.PlayableHandle&,System.Double)
extern "C"  void PlayableHandle_SetDurationInternal_m607008419 (RuntimeObject * __this /* static, unused */, PlayableHandle_t1286828706 * ___playable0, double ___duration1, const RuntimeMethod* method)
{
	{
		PlayableHandle_t1286828706 * L_0 = ___playable0;
		double L_1 = ___duration1;
		PlayableHandle_INTERNAL_CALL_SetDurationInternal_m2218811829(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Playables.PlayableHandle::INTERNAL_CALL_SetDurationInternal(UnityEngine.Playables.PlayableHandle&,System.Double)
extern "C"  void PlayableHandle_INTERNAL_CALL_SetDurationInternal_m2218811829 (RuntimeObject * __this /* static, unused */, PlayableHandle_t1286828706 * ___playable0, double ___duration1, const RuntimeMethod* method)
{
	typedef void (*PlayableHandle_INTERNAL_CALL_SetDurationInternal_m2218811829_ftn) (PlayableHandle_t1286828706 *, double);
	static PlayableHandle_INTERNAL_CALL_SetDurationInternal_m2218811829_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (PlayableHandle_INTERNAL_CALL_SetDurationInternal_m2218811829_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Playables.PlayableHandle::INTERNAL_CALL_SetDurationInternal(UnityEngine.Playables.PlayableHandle&,System.Double)");
	_il2cpp_icall_func(___playable0, ___duration1);
}
// System.Int32 UnityEngine.Playables.PlayableHandle::GetInputCountInternal(UnityEngine.Playables.PlayableHandle&)
extern "C"  int32_t PlayableHandle_GetInputCountInternal_m3943905105 (RuntimeObject * __this /* static, unused */, PlayableHandle_t1286828706 * ___playable0, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		PlayableHandle_t1286828706 * L_0 = ___playable0;
		int32_t L_1 = PlayableHandle_INTERNAL_CALL_GetInputCountInternal_m2293249710(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_000d;
	}

IL_000d:
	{
		int32_t L_2 = V_0;
		return L_2;
	}
}
// System.Int32 UnityEngine.Playables.PlayableHandle::INTERNAL_CALL_GetInputCountInternal(UnityEngine.Playables.PlayableHandle&)
extern "C"  int32_t PlayableHandle_INTERNAL_CALL_GetInputCountInternal_m2293249710 (RuntimeObject * __this /* static, unused */, PlayableHandle_t1286828706 * ___playable0, const RuntimeMethod* method)
{
	typedef int32_t (*PlayableHandle_INTERNAL_CALL_GetInputCountInternal_m2293249710_ftn) (PlayableHandle_t1286828706 *);
	static PlayableHandle_INTERNAL_CALL_GetInputCountInternal_m2293249710_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (PlayableHandle_INTERNAL_CALL_GetInputCountInternal_m2293249710_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Playables.PlayableHandle::INTERNAL_CALL_GetInputCountInternal(UnityEngine.Playables.PlayableHandle&)");
	int32_t retVal = _il2cpp_icall_func(___playable0);
	return retVal;
}
// System.Void UnityEngine.Playables.PlayableHandle::SetInputCountInternal(UnityEngine.Playables.PlayableHandle&,System.Int32)
extern "C"  void PlayableHandle_SetInputCountInternal_m54801121 (RuntimeObject * __this /* static, unused */, PlayableHandle_t1286828706 * ___playable0, int32_t ___count1, const RuntimeMethod* method)
{
	{
		PlayableHandle_t1286828706 * L_0 = ___playable0;
		int32_t L_1 = ___count1;
		PlayableHandle_INTERNAL_CALL_SetInputCountInternal_m1892728837(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Playables.PlayableHandle::INTERNAL_CALL_SetInputCountInternal(UnityEngine.Playables.PlayableHandle&,System.Int32)
extern "C"  void PlayableHandle_INTERNAL_CALL_SetInputCountInternal_m1892728837 (RuntimeObject * __this /* static, unused */, PlayableHandle_t1286828706 * ___playable0, int32_t ___count1, const RuntimeMethod* method)
{
	typedef void (*PlayableHandle_INTERNAL_CALL_SetInputCountInternal_m1892728837_ftn) (PlayableHandle_t1286828706 *, int32_t);
	static PlayableHandle_INTERNAL_CALL_SetInputCountInternal_m1892728837_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (PlayableHandle_INTERNAL_CALL_SetInputCountInternal_m1892728837_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Playables.PlayableHandle::INTERNAL_CALL_SetInputCountInternal(UnityEngine.Playables.PlayableHandle&,System.Int32)");
	_il2cpp_icall_func(___playable0, ___count1);
}
// System.Boolean UnityEngine.Playables.PlayableHandle::op_Equality(UnityEngine.Playables.PlayableHandle,UnityEngine.Playables.PlayableHandle)
extern "C"  bool PlayableHandle_op_Equality_m2726566843 (RuntimeObject * __this /* static, unused */, PlayableHandle_t1286828706  ___x0, PlayableHandle_t1286828706  ___y1, const RuntimeMethod* method)
{
	bool V_0 = false;
	{
		PlayableHandle_t1286828706  L_0 = ___x0;
		PlayableHandle_t1286828706  L_1 = ___y1;
		bool L_2 = PlayableHandle_CompareVersion_m1642417156(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_000e;
	}

IL_000e:
	{
		bool L_3 = V_0;
		return L_3;
	}
}
// System.Boolean UnityEngine.Playables.PlayableHandle::Equals(System.Object)
extern "C"  bool PlayableHandle_Equals_m2914066680 (PlayableHandle_t1286828706 * __this, RuntimeObject * ___p0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayableHandle_Equals_m2914066680_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	{
		RuntimeObject * L_0 = ___p0;
		if (((RuntimeObject *)IsInstSealed((RuntimeObject*)L_0, PlayableHandle_t1286828706_il2cpp_TypeInfo_var)))
		{
			goto IL_0013;
		}
	}
	{
		V_0 = (bool)0;
		goto IL_002a;
	}

IL_0013:
	{
		RuntimeObject * L_1 = ___p0;
		bool L_2 = PlayableHandle_CompareVersion_m1642417156(NULL /*static, unused*/, (*(PlayableHandle_t1286828706 *)__this), ((*(PlayableHandle_t1286828706 *)((PlayableHandle_t1286828706 *)UnBox(L_1, PlayableHandle_t1286828706_il2cpp_TypeInfo_var)))), /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_002a;
	}

IL_002a:
	{
		bool L_3 = V_0;
		return L_3;
	}
}
extern "C"  bool PlayableHandle_Equals_m2914066680_AdjustorThunk (RuntimeObject * __this, RuntimeObject * ___p0, const RuntimeMethod* method)
{
	PlayableHandle_t1286828706 * _thisAdjusted = reinterpret_cast<PlayableHandle_t1286828706 *>(__this + 1);
	return PlayableHandle_Equals_m2914066680(_thisAdjusted, ___p0, method);
}
// System.Int32 UnityEngine.Playables.PlayableHandle::GetHashCode()
extern "C"  int32_t PlayableHandle_GetHashCode_m3846748325 (PlayableHandle_t1286828706 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		IntPtr_t* L_0 = __this->get_address_of_m_Handle_0();
		int32_t L_1 = IntPtr_GetHashCode_m4257728765(L_0, /*hidden argument*/NULL);
		int32_t* L_2 = __this->get_address_of_m_Version_1();
		int32_t L_3 = Int32_GetHashCode_m3835255044(L_2, /*hidden argument*/NULL);
		V_0 = ((int32_t)((int32_t)L_1^(int32_t)L_3));
		goto IL_002a;
	}

IL_002a:
	{
		int32_t L_4 = V_0;
		return L_4;
	}
}
extern "C"  int32_t PlayableHandle_GetHashCode_m3846748325_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	PlayableHandle_t1286828706 * _thisAdjusted = reinterpret_cast<PlayableHandle_t1286828706 *>(__this + 1);
	return PlayableHandle_GetHashCode_m3846748325(_thisAdjusted, method);
}
// System.Boolean UnityEngine.Playables.PlayableHandle::CompareVersion(UnityEngine.Playables.PlayableHandle,UnityEngine.Playables.PlayableHandle)
extern "C"  bool PlayableHandle_CompareVersion_m1642417156 (RuntimeObject * __this /* static, unused */, PlayableHandle_t1286828706  ___lhs0, PlayableHandle_t1286828706  ___rhs1, const RuntimeMethod* method)
{
	bool V_0 = false;
	int32_t G_B3_0 = 0;
	{
		IntPtr_t L_0 = (&___lhs0)->get_m_Handle_0();
		IntPtr_t L_1 = (&___rhs1)->get_m_Handle_0();
		bool L_2 = IntPtr_op_Equality_m1136306456(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_002b;
		}
	}
	{
		int32_t L_3 = (&___lhs0)->get_m_Version_1();
		int32_t L_4 = (&___rhs1)->get_m_Version_1();
		G_B3_0 = ((((int32_t)L_3) == ((int32_t)L_4))? 1 : 0);
		goto IL_002c;
	}

IL_002b:
	{
		G_B3_0 = 0;
	}

IL_002c:
	{
		V_0 = (bool)G_B3_0;
		goto IL_0032;
	}

IL_0032:
	{
		bool L_5 = V_0;
		return L_5;
	}
}
// System.Void UnityEngine.Playables.PlayableOutput::.ctor(UnityEngine.Playables.PlayableOutputHandle)
extern "C"  void PlayableOutput__ctor_m2618420498 (PlayableOutput_t3724158364 * __this, PlayableOutputHandle_t2200222555  ___handle0, const RuntimeMethod* method)
{
	{
		PlayableOutputHandle_t2200222555  L_0 = ___handle0;
		__this->set_m_Handle_0(L_0);
		return;
	}
}
extern "C"  void PlayableOutput__ctor_m2618420498_AdjustorThunk (RuntimeObject * __this, PlayableOutputHandle_t2200222555  ___handle0, const RuntimeMethod* method)
{
	PlayableOutput_t3724158364 * _thisAdjusted = reinterpret_cast<PlayableOutput_t3724158364 *>(__this + 1);
	PlayableOutput__ctor_m2618420498(_thisAdjusted, ___handle0, method);
}
// UnityEngine.Playables.PlayableOutput UnityEngine.Playables.PlayableOutput::get_Null()
extern "C"  PlayableOutput_t3724158364  PlayableOutput_get_Null_m2632115234 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayableOutput_get_Null_m2632115234_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	PlayableOutput_t3724158364  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayableOutput_t3724158364_il2cpp_TypeInfo_var);
		PlayableOutput_t3724158364  L_0 = ((PlayableOutput_t3724158364_StaticFields*)il2cpp_codegen_static_fields_for(PlayableOutput_t3724158364_il2cpp_TypeInfo_var))->get_m_NullPlayableOutput_1();
		V_0 = L_0;
		goto IL_000c;
	}

IL_000c:
	{
		PlayableOutput_t3724158364  L_1 = V_0;
		return L_1;
	}
}
// UnityEngine.Playables.PlayableOutputHandle UnityEngine.Playables.PlayableOutput::GetHandle()
extern "C"  PlayableOutputHandle_t2200222555  PlayableOutput_GetHandle_m1843256230 (PlayableOutput_t3724158364 * __this, const RuntimeMethod* method)
{
	PlayableOutputHandle_t2200222555  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		PlayableOutputHandle_t2200222555  L_0 = __this->get_m_Handle_0();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		PlayableOutputHandle_t2200222555  L_1 = V_0;
		return L_1;
	}
}
extern "C"  PlayableOutputHandle_t2200222555  PlayableOutput_GetHandle_m1843256230_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	PlayableOutput_t3724158364 * _thisAdjusted = reinterpret_cast<PlayableOutput_t3724158364 *>(__this + 1);
	return PlayableOutput_GetHandle_m1843256230(_thisAdjusted, method);
}
// System.Type UnityEngine.Playables.PlayableOutput::GetPlayableOutputType()
extern "C"  Type_t * PlayableOutput_GetPlayableOutputType_m3110179864 (PlayableOutput_t3724158364 * __this, const RuntimeMethod* method)
{
	Type_t * V_0 = NULL;
	{
		PlayableOutputHandle_t2200222555 * L_0 = __this->get_address_of_m_Handle_0();
		Type_t * L_1 = PlayableOutputHandle_GetPlayableOutputTypeOf_m540740563(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_0012;
	}

IL_0012:
	{
		Type_t * L_2 = V_0;
		return L_2;
	}
}
extern "C"  Type_t * PlayableOutput_GetPlayableOutputType_m3110179864_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	PlayableOutput_t3724158364 * _thisAdjusted = reinterpret_cast<PlayableOutput_t3724158364 *>(__this + 1);
	return PlayableOutput_GetPlayableOutputType_m3110179864(_thisAdjusted, method);
}
// System.Boolean UnityEngine.Playables.PlayableOutput::Equals(UnityEngine.Playables.PlayableOutput)
extern "C"  bool PlayableOutput_Equals_m3710317608 (PlayableOutput_t3724158364 * __this, PlayableOutput_t3724158364  ___other0, const RuntimeMethod* method)
{
	bool V_0 = false;
	{
		PlayableOutputHandle_t2200222555  L_0 = PlayableOutput_GetHandle_m1843256230(__this, /*hidden argument*/NULL);
		PlayableOutputHandle_t2200222555  L_1 = PlayableOutput_GetHandle_m1843256230((&___other0), /*hidden argument*/NULL);
		bool L_2 = PlayableOutputHandle_op_Equality_m854247697(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_0019;
	}

IL_0019:
	{
		bool L_3 = V_0;
		return L_3;
	}
}
extern "C"  bool PlayableOutput_Equals_m3710317608_AdjustorThunk (RuntimeObject * __this, PlayableOutput_t3724158364  ___other0, const RuntimeMethod* method)
{
	PlayableOutput_t3724158364 * _thisAdjusted = reinterpret_cast<PlayableOutput_t3724158364 *>(__this + 1);
	return PlayableOutput_Equals_m3710317608(_thisAdjusted, ___other0, method);
}
// System.Void UnityEngine.Playables.PlayableOutput::.cctor()
extern "C"  void PlayableOutput__cctor_m1909945595 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayableOutput__cctor_m1909945595_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		PlayableOutputHandle_t2200222555  L_0 = PlayableOutputHandle_get_Null_m3317868945(NULL /*static, unused*/, /*hidden argument*/NULL);
		PlayableOutput_t3724158364  L_1;
		memset(&L_1, 0, sizeof(L_1));
		PlayableOutput__ctor_m2618420498((&L_1), L_0, /*hidden argument*/NULL);
		((PlayableOutput_t3724158364_StaticFields*)il2cpp_codegen_static_fields_for(PlayableOutput_t3724158364_il2cpp_TypeInfo_var))->set_m_NullPlayableOutput_1(L_1);
		return;
	}
}
// System.Boolean UnityEngine.Playables.PlayableOutputHandle::IsValid()
extern "C"  bool PlayableOutputHandle_IsValid_m3337521099 (PlayableOutputHandle_t2200222555 * __this, const RuntimeMethod* method)
{
	bool V_0 = false;
	{
		bool L_0 = PlayableOutputHandle_IsValidInternal_m1664352510(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		bool L_1 = V_0;
		return L_1;
	}
}
extern "C"  bool PlayableOutputHandle_IsValid_m3337521099_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	PlayableOutputHandle_t2200222555 * _thisAdjusted = reinterpret_cast<PlayableOutputHandle_t2200222555 *>(__this + 1);
	return PlayableOutputHandle_IsValid_m3337521099(_thisAdjusted, method);
}
// System.Boolean UnityEngine.Playables.PlayableOutputHandle::IsValidInternal(UnityEngine.Playables.PlayableOutputHandle&)
extern "C"  bool PlayableOutputHandle_IsValidInternal_m1664352510 (RuntimeObject * __this /* static, unused */, PlayableOutputHandle_t2200222555 * ___handle0, const RuntimeMethod* method)
{
	bool V_0 = false;
	{
		PlayableOutputHandle_t2200222555 * L_0 = ___handle0;
		bool L_1 = PlayableOutputHandle_INTERNAL_CALL_IsValidInternal_m103905867(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_000d;
	}

IL_000d:
	{
		bool L_2 = V_0;
		return L_2;
	}
}
// System.Boolean UnityEngine.Playables.PlayableOutputHandle::INTERNAL_CALL_IsValidInternal(UnityEngine.Playables.PlayableOutputHandle&)
extern "C"  bool PlayableOutputHandle_INTERNAL_CALL_IsValidInternal_m103905867 (RuntimeObject * __this /* static, unused */, PlayableOutputHandle_t2200222555 * ___handle0, const RuntimeMethod* method)
{
	typedef bool (*PlayableOutputHandle_INTERNAL_CALL_IsValidInternal_m103905867_ftn) (PlayableOutputHandle_t2200222555 *);
	static PlayableOutputHandle_INTERNAL_CALL_IsValidInternal_m103905867_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (PlayableOutputHandle_INTERNAL_CALL_IsValidInternal_m103905867_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Playables.PlayableOutputHandle::INTERNAL_CALL_IsValidInternal(UnityEngine.Playables.PlayableOutputHandle&)");
	bool retVal = _il2cpp_icall_func(___handle0);
	return retVal;
}
// UnityEngine.Playables.PlayableOutputHandle UnityEngine.Playables.PlayableOutputHandle::get_Null()
extern "C"  PlayableOutputHandle_t2200222555  PlayableOutputHandle_get_Null_m3317868945 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayableOutputHandle_get_Null_m3317868945_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	PlayableOutputHandle_t2200222555  V_0;
	memset(&V_0, 0, sizeof(V_0));
	PlayableOutputHandle_t2200222555  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		Initobj (PlayableOutputHandle_t2200222555_il2cpp_TypeInfo_var, (&V_0));
		(&V_0)->set_m_Version_1(((int32_t)2147483647LL));
		PlayableOutputHandle_t2200222555  L_0 = V_0;
		V_1 = L_0;
		goto IL_001c;
	}

IL_001c:
	{
		PlayableOutputHandle_t2200222555  L_1 = V_1;
		return L_1;
	}
}
// System.Type UnityEngine.Playables.PlayableOutputHandle::GetPlayableOutputTypeOf(UnityEngine.Playables.PlayableOutputHandle&)
extern "C"  Type_t * PlayableOutputHandle_GetPlayableOutputTypeOf_m540740563 (RuntimeObject * __this /* static, unused */, PlayableOutputHandle_t2200222555 * ___handle0, const RuntimeMethod* method)
{
	Type_t * V_0 = NULL;
	{
		PlayableOutputHandle_t2200222555 * L_0 = ___handle0;
		Type_t * L_1 = PlayableOutputHandle_INTERNAL_CALL_GetPlayableOutputTypeOf_m445862965(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_000d;
	}

IL_000d:
	{
		Type_t * L_2 = V_0;
		return L_2;
	}
}
// System.Type UnityEngine.Playables.PlayableOutputHandle::INTERNAL_CALL_GetPlayableOutputTypeOf(UnityEngine.Playables.PlayableOutputHandle&)
extern "C"  Type_t * PlayableOutputHandle_INTERNAL_CALL_GetPlayableOutputTypeOf_m445862965 (RuntimeObject * __this /* static, unused */, PlayableOutputHandle_t2200222555 * ___handle0, const RuntimeMethod* method)
{
	typedef Type_t * (*PlayableOutputHandle_INTERNAL_CALL_GetPlayableOutputTypeOf_m445862965_ftn) (PlayableOutputHandle_t2200222555 *);
	static PlayableOutputHandle_INTERNAL_CALL_GetPlayableOutputTypeOf_m445862965_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (PlayableOutputHandle_INTERNAL_CALL_GetPlayableOutputTypeOf_m445862965_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Playables.PlayableOutputHandle::INTERNAL_CALL_GetPlayableOutputTypeOf(UnityEngine.Playables.PlayableOutputHandle&)");
	Type_t * retVal = _il2cpp_icall_func(___handle0);
	return retVal;
}
// System.Int32 UnityEngine.Playables.PlayableOutputHandle::GetHashCode()
extern "C"  int32_t PlayableOutputHandle_GetHashCode_m2925011117 (PlayableOutputHandle_t2200222555 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		IntPtr_t* L_0 = __this->get_address_of_m_Handle_0();
		int32_t L_1 = IntPtr_GetHashCode_m4257728765(L_0, /*hidden argument*/NULL);
		int32_t* L_2 = __this->get_address_of_m_Version_1();
		int32_t L_3 = Int32_GetHashCode_m3835255044(L_2, /*hidden argument*/NULL);
		V_0 = ((int32_t)((int32_t)L_1^(int32_t)L_3));
		goto IL_002a;
	}

IL_002a:
	{
		int32_t L_4 = V_0;
		return L_4;
	}
}
extern "C"  int32_t PlayableOutputHandle_GetHashCode_m2925011117_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	PlayableOutputHandle_t2200222555 * _thisAdjusted = reinterpret_cast<PlayableOutputHandle_t2200222555 *>(__this + 1);
	return PlayableOutputHandle_GetHashCode_m2925011117(_thisAdjusted, method);
}
// System.Boolean UnityEngine.Playables.PlayableOutputHandle::op_Equality(UnityEngine.Playables.PlayableOutputHandle,UnityEngine.Playables.PlayableOutputHandle)
extern "C"  bool PlayableOutputHandle_op_Equality_m854247697 (RuntimeObject * __this /* static, unused */, PlayableOutputHandle_t2200222555  ___lhs0, PlayableOutputHandle_t2200222555  ___rhs1, const RuntimeMethod* method)
{
	bool V_0 = false;
	{
		PlayableOutputHandle_t2200222555  L_0 = ___lhs0;
		PlayableOutputHandle_t2200222555  L_1 = ___rhs1;
		bool L_2 = PlayableOutputHandle_CompareVersion_m3007568050(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_000e;
	}

IL_000e:
	{
		bool L_3 = V_0;
		return L_3;
	}
}
// System.Boolean UnityEngine.Playables.PlayableOutputHandle::Equals(System.Object)
extern "C"  bool PlayableOutputHandle_Equals_m1190900403 (PlayableOutputHandle_t2200222555 * __this, RuntimeObject * ___p0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayableOutputHandle_Equals_m1190900403_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	int32_t G_B3_0 = 0;
	{
		RuntimeObject * L_0 = ___p0;
		if (!((RuntimeObject *)IsInstSealed((RuntimeObject*)L_0, PlayableOutputHandle_t2200222555_il2cpp_TypeInfo_var)))
		{
			goto IL_001f;
		}
	}
	{
		RuntimeObject * L_1 = ___p0;
		bool L_2 = PlayableOutputHandle_CompareVersion_m3007568050(NULL /*static, unused*/, (*(PlayableOutputHandle_t2200222555 *)__this), ((*(PlayableOutputHandle_t2200222555 *)((PlayableOutputHandle_t2200222555 *)UnBox(L_1, PlayableOutputHandle_t2200222555_il2cpp_TypeInfo_var)))), /*hidden argument*/NULL);
		G_B3_0 = ((int32_t)(L_2));
		goto IL_0020;
	}

IL_001f:
	{
		G_B3_0 = 0;
	}

IL_0020:
	{
		V_0 = (bool)G_B3_0;
		goto IL_0026;
	}

IL_0026:
	{
		bool L_3 = V_0;
		return L_3;
	}
}
extern "C"  bool PlayableOutputHandle_Equals_m1190900403_AdjustorThunk (RuntimeObject * __this, RuntimeObject * ___p0, const RuntimeMethod* method)
{
	PlayableOutputHandle_t2200222555 * _thisAdjusted = reinterpret_cast<PlayableOutputHandle_t2200222555 *>(__this + 1);
	return PlayableOutputHandle_Equals_m1190900403(_thisAdjusted, ___p0, method);
}
// System.Boolean UnityEngine.Playables.PlayableOutputHandle::CompareVersion(UnityEngine.Playables.PlayableOutputHandle,UnityEngine.Playables.PlayableOutputHandle)
extern "C"  bool PlayableOutputHandle_CompareVersion_m3007568050 (RuntimeObject * __this /* static, unused */, PlayableOutputHandle_t2200222555  ___lhs0, PlayableOutputHandle_t2200222555  ___rhs1, const RuntimeMethod* method)
{
	bool V_0 = false;
	int32_t G_B3_0 = 0;
	{
		IntPtr_t L_0 = (&___lhs0)->get_m_Handle_0();
		IntPtr_t L_1 = (&___rhs1)->get_m_Handle_0();
		bool L_2 = IntPtr_op_Equality_m1136306456(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_002b;
		}
	}
	{
		int32_t L_3 = (&___lhs0)->get_m_Version_1();
		int32_t L_4 = (&___rhs1)->get_m_Version_1();
		G_B3_0 = ((((int32_t)L_3) == ((int32_t)L_4))? 1 : 0);
		goto IL_002c;
	}

IL_002b:
	{
		G_B3_0 = 0;
	}

IL_002c:
	{
		V_0 = (bool)G_B3_0;
		goto IL_0032;
	}

IL_0032:
	{
		bool L_5 = V_0;
		return L_5;
	}
}
// System.Void UnityEngine.Playables.ScriptPlayableOutput::.ctor(UnityEngine.Playables.PlayableOutputHandle)
extern "C"  void ScriptPlayableOutput__ctor_m1317995783 (ScriptPlayableOutput_t49585950 * __this, PlayableOutputHandle_t2200222555  ___handle0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ScriptPlayableOutput__ctor_m1317995783_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = PlayableOutputHandle_IsValid_m3337521099((&___handle0), /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0026;
		}
	}
	{
		bool L_1 = PlayableOutputHandle_IsPlayableOutputOfType_TisScriptPlayableOutput_t49585950_m1665456405((&___handle0), /*hidden argument*/PlayableOutputHandle_IsPlayableOutputOfType_TisScriptPlayableOutput_t49585950_m1665456405_RuntimeMethod_var);
		if (L_1)
		{
			goto IL_0025;
		}
	}
	{
		InvalidCastException_t1996460962 * L_2 = (InvalidCastException_t1996460962 *)il2cpp_codegen_object_new(InvalidCastException_t1996460962_il2cpp_TypeInfo_var);
		InvalidCastException__ctor_m1075299501(L_2, _stringLiteral90747689, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0025:
	{
	}

IL_0026:
	{
		PlayableOutputHandle_t2200222555  L_3 = ___handle0;
		__this->set_m_Handle_0(L_3);
		return;
	}
}
extern "C"  void ScriptPlayableOutput__ctor_m1317995783_AdjustorThunk (RuntimeObject * __this, PlayableOutputHandle_t2200222555  ___handle0, const RuntimeMethod* method)
{
	ScriptPlayableOutput_t49585950 * _thisAdjusted = reinterpret_cast<ScriptPlayableOutput_t49585950 *>(__this + 1);
	ScriptPlayableOutput__ctor_m1317995783(_thisAdjusted, ___handle0, method);
}
// UnityEngine.Playables.ScriptPlayableOutput UnityEngine.Playables.ScriptPlayableOutput::Create(UnityEngine.Playables.PlayableGraph,System.String)
extern "C"  ScriptPlayableOutput_t49585950  ScriptPlayableOutput_Create_m2905762973 (RuntimeObject * __this /* static, unused */, PlayableGraph_t1110114554  ___graph0, String_t* ___name1, const RuntimeMethod* method)
{
	PlayableOutputHandle_t2200222555  V_0;
	memset(&V_0, 0, sizeof(V_0));
	ScriptPlayableOutput_t49585950  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		String_t* L_0 = ___name1;
		bool L_1 = PlayableGraph_CreateScriptOutputInternal_m3787016967(NULL /*static, unused*/, (&___graph0), L_0, (&V_0), /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_001b;
		}
	}
	{
		ScriptPlayableOutput_t49585950  L_2 = ScriptPlayableOutput_get_Null_m1376196995(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_1 = L_2;
		goto IL_0027;
	}

IL_001b:
	{
		PlayableOutputHandle_t2200222555  L_3 = V_0;
		ScriptPlayableOutput_t49585950  L_4;
		memset(&L_4, 0, sizeof(L_4));
		ScriptPlayableOutput__ctor_m1317995783((&L_4), L_3, /*hidden argument*/NULL);
		V_1 = L_4;
		goto IL_0027;
	}

IL_0027:
	{
		ScriptPlayableOutput_t49585950  L_5 = V_1;
		return L_5;
	}
}
// UnityEngine.Playables.ScriptPlayableOutput UnityEngine.Playables.ScriptPlayableOutput::get_Null()
extern "C"  ScriptPlayableOutput_t49585950  ScriptPlayableOutput_get_Null_m1376196995 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	ScriptPlayableOutput_t49585950  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		PlayableOutputHandle_t2200222555  L_0 = PlayableOutputHandle_get_Null_m3317868945(NULL /*static, unused*/, /*hidden argument*/NULL);
		ScriptPlayableOutput_t49585950  L_1;
		memset(&L_1, 0, sizeof(L_1));
		ScriptPlayableOutput__ctor_m1317995783((&L_1), L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_0011;
	}

IL_0011:
	{
		ScriptPlayableOutput_t49585950  L_2 = V_0;
		return L_2;
	}
}
// UnityEngine.Playables.PlayableOutputHandle UnityEngine.Playables.ScriptPlayableOutput::GetHandle()
extern "C"  PlayableOutputHandle_t2200222555  ScriptPlayableOutput_GetHandle_m2320625387 (ScriptPlayableOutput_t49585950 * __this, const RuntimeMethod* method)
{
	PlayableOutputHandle_t2200222555  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		PlayableOutputHandle_t2200222555  L_0 = __this->get_m_Handle_0();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		PlayableOutputHandle_t2200222555  L_1 = V_0;
		return L_1;
	}
}
extern "C"  PlayableOutputHandle_t2200222555  ScriptPlayableOutput_GetHandle_m2320625387_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	ScriptPlayableOutput_t49585950 * _thisAdjusted = reinterpret_cast<ScriptPlayableOutput_t49585950 *>(__this + 1);
	return ScriptPlayableOutput_GetHandle_m2320625387(_thisAdjusted, method);
}
// UnityEngine.Playables.PlayableOutput UnityEngine.Playables.ScriptPlayableOutput::op_Implicit(UnityEngine.Playables.ScriptPlayableOutput)
extern "C"  PlayableOutput_t3724158364  ScriptPlayableOutput_op_Implicit_m2273590727 (RuntimeObject * __this /* static, unused */, ScriptPlayableOutput_t49585950  ___output0, const RuntimeMethod* method)
{
	PlayableOutput_t3724158364  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		PlayableOutputHandle_t2200222555  L_0 = ScriptPlayableOutput_GetHandle_m2320625387((&___output0), /*hidden argument*/NULL);
		PlayableOutput_t3724158364  L_1;
		memset(&L_1, 0, sizeof(L_1));
		PlayableOutput__ctor_m2618420498((&L_1), L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_0013;
	}

IL_0013:
	{
		PlayableOutput_t3724158364  L_2 = V_0;
		return L_2;
	}
}
// UnityEngine.Playables.ScriptPlayableOutput UnityEngine.Playables.ScriptPlayableOutput::op_Explicit(UnityEngine.Playables.PlayableOutput)
extern "C"  ScriptPlayableOutput_t49585950  ScriptPlayableOutput_op_Explicit_m2603906233 (RuntimeObject * __this /* static, unused */, PlayableOutput_t3724158364  ___output0, const RuntimeMethod* method)
{
	ScriptPlayableOutput_t49585950  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		PlayableOutputHandle_t2200222555  L_0 = PlayableOutput_GetHandle_m1843256230((&___output0), /*hidden argument*/NULL);
		ScriptPlayableOutput_t49585950  L_1;
		memset(&L_1, 0, sizeof(L_1));
		ScriptPlayableOutput__ctor_m1317995783((&L_1), L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_0013;
	}

IL_0013:
	{
		ScriptPlayableOutput_t49585950  L_2 = V_0;
		return L_2;
	}
}
// System.Void UnityEngine.PreferBinarySerialization::.ctor()
extern "C"  void PreferBinarySerialization__ctor_m3133271298 (PreferBinarySerialization_t3755844044 * __this, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m1283917294(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.PropertyAttribute::.ctor()
extern "C"  void PropertyAttribute__ctor_m2084867108 (PropertyAttribute_t1939035372 * __this, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m1283917294(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.QualitySettings::set_shadowDistance(System.Single)
extern "C"  void QualitySettings_set_shadowDistance_m1518218078 (RuntimeObject * __this /* static, unused */, float ___value0, const RuntimeMethod* method)
{
	typedef void (*QualitySettings_set_shadowDistance_m1518218078_ftn) (float);
	static QualitySettings_set_shadowDistance_m1518218078_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (QualitySettings_set_shadowDistance_m1518218078_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.QualitySettings::set_shadowDistance(System.Single)");
	_il2cpp_icall_func(___value0);
}
// System.Int32 UnityEngine.QualitySettings::get_antiAliasing()
extern "C"  int32_t QualitySettings_get_antiAliasing_m1825181306 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	typedef int32_t (*QualitySettings_get_antiAliasing_m1825181306_ftn) ();
	static QualitySettings_get_antiAliasing_m1825181306_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (QualitySettings_get_antiAliasing_m1825181306_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.QualitySettings::get_antiAliasing()");
	int32_t retVal = _il2cpp_icall_func();
	return retVal;
}
// UnityEngine.ColorSpace UnityEngine.QualitySettings::get_activeColorSpace()
extern "C"  int32_t QualitySettings_get_activeColorSpace_m3960038853 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	typedef int32_t (*QualitySettings_get_activeColorSpace_m3960038853_ftn) ();
	static QualitySettings_get_activeColorSpace_m3960038853_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (QualitySettings_get_activeColorSpace_m3960038853_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.QualitySettings::get_activeColorSpace()");
	int32_t retVal = _il2cpp_icall_func();
	return retVal;
}
// System.Void UnityEngine.Quaternion::.ctor(System.Single,System.Single,System.Single,System.Single)
extern "C"  void Quaternion__ctor_m1252814061 (Quaternion_t754065749 * __this, float ___x0, float ___y1, float ___z2, float ___w3, const RuntimeMethod* method)
{
	{
		float L_0 = ___x0;
		__this->set_x_0(L_0);
		float L_1 = ___y1;
		__this->set_y_1(L_1);
		float L_2 = ___z2;
		__this->set_z_2(L_2);
		float L_3 = ___w3;
		__this->set_w_3(L_3);
		return;
	}
}
extern "C"  void Quaternion__ctor_m1252814061_AdjustorThunk (RuntimeObject * __this, float ___x0, float ___y1, float ___z2, float ___w3, const RuntimeMethod* method)
{
	Quaternion_t754065749 * _thisAdjusted = reinterpret_cast<Quaternion_t754065749 *>(__this + 1);
	Quaternion__ctor_m1252814061(_thisAdjusted, ___x0, ___y1, ___z2, ___w3, method);
}
// UnityEngine.Quaternion UnityEngine.Quaternion::AngleAxis(System.Single,UnityEngine.Vector3)
extern "C"  Quaternion_t754065749  Quaternion_AngleAxis_m3400718822 (RuntimeObject * __this /* static, unused */, float ___angle0, Vector3_t2903530434  ___axis1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Quaternion_AngleAxis_m3400718822_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Quaternion_t754065749  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Quaternion_t754065749  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		float L_0 = ___angle0;
		IL2CPP_RUNTIME_CLASS_INIT(Quaternion_t754065749_il2cpp_TypeInfo_var);
		Quaternion_INTERNAL_CALL_AngleAxis_m2203684919(NULL /*static, unused*/, L_0, (&___axis1), (&V_0), /*hidden argument*/NULL);
		Quaternion_t754065749  L_1 = V_0;
		V_1 = L_1;
		goto IL_0012;
	}

IL_0012:
	{
		Quaternion_t754065749  L_2 = V_1;
		return L_2;
	}
}
// System.Void UnityEngine.Quaternion::INTERNAL_CALL_AngleAxis(System.Single,UnityEngine.Vector3&,UnityEngine.Quaternion&)
extern "C"  void Quaternion_INTERNAL_CALL_AngleAxis_m2203684919 (RuntimeObject * __this /* static, unused */, float ___angle0, Vector3_t2903530434 * ___axis1, Quaternion_t754065749 * ___value2, const RuntimeMethod* method)
{
	typedef void (*Quaternion_INTERNAL_CALL_AngleAxis_m2203684919_ftn) (float, Vector3_t2903530434 *, Quaternion_t754065749 *);
	static Quaternion_INTERNAL_CALL_AngleAxis_m2203684919_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Quaternion_INTERNAL_CALL_AngleAxis_m2203684919_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Quaternion::INTERNAL_CALL_AngleAxis(System.Single,UnityEngine.Vector3&,UnityEngine.Quaternion&)");
	_il2cpp_icall_func(___angle0, ___axis1, ___value2);
}
// System.Void UnityEngine.Quaternion::ToAngleAxis(System.Single&,UnityEngine.Vector3&)
extern "C"  void Quaternion_ToAngleAxis_m636701083 (Quaternion_t754065749 * __this, float* ___angle0, Vector3_t2903530434 * ___axis1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Quaternion_ToAngleAxis_m636701083_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Vector3_t2903530434 * L_0 = ___axis1;
		float* L_1 = ___angle0;
		IL2CPP_RUNTIME_CLASS_INIT(Quaternion_t754065749_il2cpp_TypeInfo_var);
		Quaternion_Internal_ToAxisAngleRad_m1871010446(NULL /*static, unused*/, (*(Quaternion_t754065749 *)__this), L_0, L_1, /*hidden argument*/NULL);
		float* L_2 = ___angle0;
		float* L_3 = ___angle0;
		*((float*)(L_2)) = (float)((float)((float)(*((float*)L_3))*(float)(57.29578f)));
		return;
	}
}
extern "C"  void Quaternion_ToAngleAxis_m636701083_AdjustorThunk (RuntimeObject * __this, float* ___angle0, Vector3_t2903530434 * ___axis1, const RuntimeMethod* method)
{
	Quaternion_t754065749 * _thisAdjusted = reinterpret_cast<Quaternion_t754065749 *>(__this + 1);
	Quaternion_ToAngleAxis_m636701083(_thisAdjusted, ___angle0, ___axis1, method);
}
// UnityEngine.Quaternion UnityEngine.Quaternion::LookRotation(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  Quaternion_t754065749  Quaternion_LookRotation_m2938427707 (RuntimeObject * __this /* static, unused */, Vector3_t2903530434  ___forward0, Vector3_t2903530434  ___upwards1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Quaternion_LookRotation_m2938427707_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Quaternion_t754065749  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Quaternion_t754065749  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		IL2CPP_RUNTIME_CLASS_INIT(Quaternion_t754065749_il2cpp_TypeInfo_var);
		Quaternion_INTERNAL_CALL_LookRotation_m1039555483(NULL /*static, unused*/, (&___forward0), (&___upwards1), (&V_0), /*hidden argument*/NULL);
		Quaternion_t754065749  L_0 = V_0;
		V_1 = L_0;
		goto IL_0013;
	}

IL_0013:
	{
		Quaternion_t754065749  L_1 = V_1;
		return L_1;
	}
}
// UnityEngine.Quaternion UnityEngine.Quaternion::LookRotation(UnityEngine.Vector3)
extern "C"  Quaternion_t754065749  Quaternion_LookRotation_m274887101 (RuntimeObject * __this /* static, unused */, Vector3_t2903530434  ___forward0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Quaternion_LookRotation_m274887101_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t2903530434  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Quaternion_t754065749  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Quaternion_t754065749  V_2;
	memset(&V_2, 0, sizeof(V_2));
	{
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t2903530434_il2cpp_TypeInfo_var);
		Vector3_t2903530434  L_0 = Vector3_get_up_m1398813076(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		IL2CPP_RUNTIME_CLASS_INIT(Quaternion_t754065749_il2cpp_TypeInfo_var);
		Quaternion_INTERNAL_CALL_LookRotation_m1039555483(NULL /*static, unused*/, (&___forward0), (&V_0), (&V_1), /*hidden argument*/NULL);
		Quaternion_t754065749  L_1 = V_1;
		V_2 = L_1;
		goto IL_0019;
	}

IL_0019:
	{
		Quaternion_t754065749  L_2 = V_2;
		return L_2;
	}
}
// System.Void UnityEngine.Quaternion::INTERNAL_CALL_LookRotation(UnityEngine.Vector3&,UnityEngine.Vector3&,UnityEngine.Quaternion&)
extern "C"  void Quaternion_INTERNAL_CALL_LookRotation_m1039555483 (RuntimeObject * __this /* static, unused */, Vector3_t2903530434 * ___forward0, Vector3_t2903530434 * ___upwards1, Quaternion_t754065749 * ___value2, const RuntimeMethod* method)
{
	typedef void (*Quaternion_INTERNAL_CALL_LookRotation_m1039555483_ftn) (Vector3_t2903530434 *, Vector3_t2903530434 *, Quaternion_t754065749 *);
	static Quaternion_INTERNAL_CALL_LookRotation_m1039555483_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Quaternion_INTERNAL_CALL_LookRotation_m1039555483_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Quaternion::INTERNAL_CALL_LookRotation(UnityEngine.Vector3&,UnityEngine.Vector3&,UnityEngine.Quaternion&)");
	_il2cpp_icall_func(___forward0, ___upwards1, ___value2);
}
// UnityEngine.Quaternion UnityEngine.Quaternion::Slerp(UnityEngine.Quaternion,UnityEngine.Quaternion,System.Single)
extern "C"  Quaternion_t754065749  Quaternion_Slerp_m2698179433 (RuntimeObject * __this /* static, unused */, Quaternion_t754065749  ___a0, Quaternion_t754065749  ___b1, float ___t2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Quaternion_Slerp_m2698179433_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Quaternion_t754065749  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Quaternion_t754065749  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		float L_0 = ___t2;
		IL2CPP_RUNTIME_CLASS_INIT(Quaternion_t754065749_il2cpp_TypeInfo_var);
		Quaternion_INTERNAL_CALL_Slerp_m3884730431(NULL /*static, unused*/, (&___a0), (&___b1), L_0, (&V_0), /*hidden argument*/NULL);
		Quaternion_t754065749  L_1 = V_0;
		V_1 = L_1;
		goto IL_0014;
	}

IL_0014:
	{
		Quaternion_t754065749  L_2 = V_1;
		return L_2;
	}
}
// System.Void UnityEngine.Quaternion::INTERNAL_CALL_Slerp(UnityEngine.Quaternion&,UnityEngine.Quaternion&,System.Single,UnityEngine.Quaternion&)
extern "C"  void Quaternion_INTERNAL_CALL_Slerp_m3884730431 (RuntimeObject * __this /* static, unused */, Quaternion_t754065749 * ___a0, Quaternion_t754065749 * ___b1, float ___t2, Quaternion_t754065749 * ___value3, const RuntimeMethod* method)
{
	typedef void (*Quaternion_INTERNAL_CALL_Slerp_m3884730431_ftn) (Quaternion_t754065749 *, Quaternion_t754065749 *, float, Quaternion_t754065749 *);
	static Quaternion_INTERNAL_CALL_Slerp_m3884730431_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Quaternion_INTERNAL_CALL_Slerp_m3884730431_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Quaternion::INTERNAL_CALL_Slerp(UnityEngine.Quaternion&,UnityEngine.Quaternion&,System.Single,UnityEngine.Quaternion&)");
	_il2cpp_icall_func(___a0, ___b1, ___t2, ___value3);
}
// UnityEngine.Quaternion UnityEngine.Quaternion::Inverse(UnityEngine.Quaternion)
extern "C"  Quaternion_t754065749  Quaternion_Inverse_m3223872676 (RuntimeObject * __this /* static, unused */, Quaternion_t754065749  ___rotation0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Quaternion_Inverse_m3223872676_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Quaternion_t754065749  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Quaternion_t754065749  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		IL2CPP_RUNTIME_CLASS_INIT(Quaternion_t754065749_il2cpp_TypeInfo_var);
		Quaternion_INTERNAL_CALL_Inverse_m2903549735(NULL /*static, unused*/, (&___rotation0), (&V_0), /*hidden argument*/NULL);
		Quaternion_t754065749  L_0 = V_0;
		V_1 = L_0;
		goto IL_0011;
	}

IL_0011:
	{
		Quaternion_t754065749  L_1 = V_1;
		return L_1;
	}
}
// System.Void UnityEngine.Quaternion::INTERNAL_CALL_Inverse(UnityEngine.Quaternion&,UnityEngine.Quaternion&)
extern "C"  void Quaternion_INTERNAL_CALL_Inverse_m2903549735 (RuntimeObject * __this /* static, unused */, Quaternion_t754065749 * ___rotation0, Quaternion_t754065749 * ___value1, const RuntimeMethod* method)
{
	typedef void (*Quaternion_INTERNAL_CALL_Inverse_m2903549735_ftn) (Quaternion_t754065749 *, Quaternion_t754065749 *);
	static Quaternion_INTERNAL_CALL_Inverse_m2903549735_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Quaternion_INTERNAL_CALL_Inverse_m2903549735_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Quaternion::INTERNAL_CALL_Inverse(UnityEngine.Quaternion&,UnityEngine.Quaternion&)");
	_il2cpp_icall_func(___rotation0, ___value1);
}
// UnityEngine.Vector3 UnityEngine.Quaternion::get_eulerAngles()
extern "C"  Vector3_t2903530434  Quaternion_get_eulerAngles_m3862017976 (Quaternion_t754065749 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Quaternion_get_eulerAngles_m3862017976_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t2903530434  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		IL2CPP_RUNTIME_CLASS_INIT(Quaternion_t754065749_il2cpp_TypeInfo_var);
		Vector3_t2903530434  L_0 = Quaternion_Internal_ToEulerRad_m4233285968(NULL /*static, unused*/, (*(Quaternion_t754065749 *)__this), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t2903530434_il2cpp_TypeInfo_var);
		Vector3_t2903530434  L_1 = Vector3_op_Multiply_m2063944565(NULL /*static, unused*/, L_0, (57.29578f), /*hidden argument*/NULL);
		Vector3_t2903530434  L_2 = Quaternion_Internal_MakePositive_m3017293218(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_0021;
	}

IL_0021:
	{
		Vector3_t2903530434  L_3 = V_0;
		return L_3;
	}
}
extern "C"  Vector3_t2903530434  Quaternion_get_eulerAngles_m3862017976_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Quaternion_t754065749 * _thisAdjusted = reinterpret_cast<Quaternion_t754065749 *>(__this + 1);
	return Quaternion_get_eulerAngles_m3862017976(_thisAdjusted, method);
}
// UnityEngine.Quaternion UnityEngine.Quaternion::Euler(System.Single,System.Single,System.Single)
extern "C"  Quaternion_t754065749  Quaternion_Euler_m631510244 (RuntimeObject * __this /* static, unused */, float ___x0, float ___y1, float ___z2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Quaternion_Euler_m631510244_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Quaternion_t754065749  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		float L_0 = ___x0;
		float L_1 = ___y1;
		float L_2 = ___z2;
		Vector3_t2903530434  L_3;
		memset(&L_3, 0, sizeof(L_3));
		Vector3__ctor_m4053220054((&L_3), L_0, L_1, L_2, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t2903530434_il2cpp_TypeInfo_var);
		Vector3_t2903530434  L_4 = Vector3_op_Multiply_m2063944565(NULL /*static, unused*/, L_3, (0.0174532924f), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Quaternion_t754065749_il2cpp_TypeInfo_var);
		Quaternion_t754065749  L_5 = Quaternion_Internal_FromEulerRad_m2740790710(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		V_0 = L_5;
		goto IL_001e;
	}

IL_001e:
	{
		Quaternion_t754065749  L_6 = V_0;
		return L_6;
	}
}
// UnityEngine.Quaternion UnityEngine.Quaternion::Euler(UnityEngine.Vector3)
extern "C"  Quaternion_t754065749  Quaternion_Euler_m238783557 (RuntimeObject * __this /* static, unused */, Vector3_t2903530434  ___euler0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Quaternion_Euler_m238783557_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Quaternion_t754065749  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Vector3_t2903530434  L_0 = ___euler0;
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t2903530434_il2cpp_TypeInfo_var);
		Vector3_t2903530434  L_1 = Vector3_op_Multiply_m2063944565(NULL /*static, unused*/, L_0, (0.0174532924f), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Quaternion_t754065749_il2cpp_TypeInfo_var);
		Quaternion_t754065749  L_2 = Quaternion_Internal_FromEulerRad_m2740790710(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_0017;
	}

IL_0017:
	{
		Quaternion_t754065749  L_3 = V_0;
		return L_3;
	}
}
// UnityEngine.Vector3 UnityEngine.Quaternion::Internal_ToEulerRad(UnityEngine.Quaternion)
extern "C"  Vector3_t2903530434  Quaternion_Internal_ToEulerRad_m4233285968 (RuntimeObject * __this /* static, unused */, Quaternion_t754065749  ___rotation0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Quaternion_Internal_ToEulerRad_m4233285968_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t2903530434  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector3_t2903530434  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		IL2CPP_RUNTIME_CLASS_INIT(Quaternion_t754065749_il2cpp_TypeInfo_var);
		Quaternion_INTERNAL_CALL_Internal_ToEulerRad_m1031640151(NULL /*static, unused*/, (&___rotation0), (&V_0), /*hidden argument*/NULL);
		Vector3_t2903530434  L_0 = V_0;
		V_1 = L_0;
		goto IL_0011;
	}

IL_0011:
	{
		Vector3_t2903530434  L_1 = V_1;
		return L_1;
	}
}
// System.Void UnityEngine.Quaternion::INTERNAL_CALL_Internal_ToEulerRad(UnityEngine.Quaternion&,UnityEngine.Vector3&)
extern "C"  void Quaternion_INTERNAL_CALL_Internal_ToEulerRad_m1031640151 (RuntimeObject * __this /* static, unused */, Quaternion_t754065749 * ___rotation0, Vector3_t2903530434 * ___value1, const RuntimeMethod* method)
{
	typedef void (*Quaternion_INTERNAL_CALL_Internal_ToEulerRad_m1031640151_ftn) (Quaternion_t754065749 *, Vector3_t2903530434 *);
	static Quaternion_INTERNAL_CALL_Internal_ToEulerRad_m1031640151_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Quaternion_INTERNAL_CALL_Internal_ToEulerRad_m1031640151_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Quaternion::INTERNAL_CALL_Internal_ToEulerRad(UnityEngine.Quaternion&,UnityEngine.Vector3&)");
	_il2cpp_icall_func(___rotation0, ___value1);
}
// UnityEngine.Quaternion UnityEngine.Quaternion::Internal_FromEulerRad(UnityEngine.Vector3)
extern "C"  Quaternion_t754065749  Quaternion_Internal_FromEulerRad_m2740790710 (RuntimeObject * __this /* static, unused */, Vector3_t2903530434  ___euler0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Quaternion_Internal_FromEulerRad_m2740790710_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Quaternion_t754065749  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Quaternion_t754065749  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		IL2CPP_RUNTIME_CLASS_INIT(Quaternion_t754065749_il2cpp_TypeInfo_var);
		Quaternion_INTERNAL_CALL_Internal_FromEulerRad_m2531566785(NULL /*static, unused*/, (&___euler0), (&V_0), /*hidden argument*/NULL);
		Quaternion_t754065749  L_0 = V_0;
		V_1 = L_0;
		goto IL_0011;
	}

IL_0011:
	{
		Quaternion_t754065749  L_1 = V_1;
		return L_1;
	}
}
// System.Void UnityEngine.Quaternion::INTERNAL_CALL_Internal_FromEulerRad(UnityEngine.Vector3&,UnityEngine.Quaternion&)
extern "C"  void Quaternion_INTERNAL_CALL_Internal_FromEulerRad_m2531566785 (RuntimeObject * __this /* static, unused */, Vector3_t2903530434 * ___euler0, Quaternion_t754065749 * ___value1, const RuntimeMethod* method)
{
	typedef void (*Quaternion_INTERNAL_CALL_Internal_FromEulerRad_m2531566785_ftn) (Vector3_t2903530434 *, Quaternion_t754065749 *);
	static Quaternion_INTERNAL_CALL_Internal_FromEulerRad_m2531566785_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Quaternion_INTERNAL_CALL_Internal_FromEulerRad_m2531566785_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Quaternion::INTERNAL_CALL_Internal_FromEulerRad(UnityEngine.Vector3&,UnityEngine.Quaternion&)");
	_il2cpp_icall_func(___euler0, ___value1);
}
// System.Void UnityEngine.Quaternion::Internal_ToAxisAngleRad(UnityEngine.Quaternion,UnityEngine.Vector3&,System.Single&)
extern "C"  void Quaternion_Internal_ToAxisAngleRad_m1871010446 (RuntimeObject * __this /* static, unused */, Quaternion_t754065749  ___q0, Vector3_t2903530434 * ___axis1, float* ___angle2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Quaternion_Internal_ToAxisAngleRad_m1871010446_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Vector3_t2903530434 * L_0 = ___axis1;
		float* L_1 = ___angle2;
		IL2CPP_RUNTIME_CLASS_INIT(Quaternion_t754065749_il2cpp_TypeInfo_var);
		Quaternion_INTERNAL_CALL_Internal_ToAxisAngleRad_m1174021168(NULL /*static, unused*/, (&___q0), L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Quaternion::INTERNAL_CALL_Internal_ToAxisAngleRad(UnityEngine.Quaternion&,UnityEngine.Vector3&,System.Single&)
extern "C"  void Quaternion_INTERNAL_CALL_Internal_ToAxisAngleRad_m1174021168 (RuntimeObject * __this /* static, unused */, Quaternion_t754065749 * ___q0, Vector3_t2903530434 * ___axis1, float* ___angle2, const RuntimeMethod* method)
{
	typedef void (*Quaternion_INTERNAL_CALL_Internal_ToAxisAngleRad_m1174021168_ftn) (Quaternion_t754065749 *, Vector3_t2903530434 *, float*);
	static Quaternion_INTERNAL_CALL_Internal_ToAxisAngleRad_m1174021168_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Quaternion_INTERNAL_CALL_Internal_ToAxisAngleRad_m1174021168_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Quaternion::INTERNAL_CALL_Internal_ToAxisAngleRad(UnityEngine.Quaternion&,UnityEngine.Vector3&,System.Single&)");
	_il2cpp_icall_func(___q0, ___axis1, ___angle2);
}
// UnityEngine.Quaternion UnityEngine.Quaternion::get_identity()
extern "C"  Quaternion_t754065749  Quaternion_get_identity_m4047182200 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Quaternion_get_identity_m4047182200_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Quaternion_t754065749  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		IL2CPP_RUNTIME_CLASS_INIT(Quaternion_t754065749_il2cpp_TypeInfo_var);
		Quaternion_t754065749  L_0 = ((Quaternion_t754065749_StaticFields*)il2cpp_codegen_static_fields_for(Quaternion_t754065749_il2cpp_TypeInfo_var))->get_identityQuaternion_4();
		V_0 = L_0;
		goto IL_000c;
	}

IL_000c:
	{
		Quaternion_t754065749  L_1 = V_0;
		return L_1;
	}
}
// UnityEngine.Quaternion UnityEngine.Quaternion::op_Multiply(UnityEngine.Quaternion,UnityEngine.Quaternion)
extern "C"  Quaternion_t754065749  Quaternion_op_Multiply_m2737699320 (RuntimeObject * __this /* static, unused */, Quaternion_t754065749  ___lhs0, Quaternion_t754065749  ___rhs1, const RuntimeMethod* method)
{
	Quaternion_t754065749  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		float L_0 = (&___lhs0)->get_w_3();
		float L_1 = (&___rhs1)->get_x_0();
		float L_2 = (&___lhs0)->get_x_0();
		float L_3 = (&___rhs1)->get_w_3();
		float L_4 = (&___lhs0)->get_y_1();
		float L_5 = (&___rhs1)->get_z_2();
		float L_6 = (&___lhs0)->get_z_2();
		float L_7 = (&___rhs1)->get_y_1();
		float L_8 = (&___lhs0)->get_w_3();
		float L_9 = (&___rhs1)->get_y_1();
		float L_10 = (&___lhs0)->get_y_1();
		float L_11 = (&___rhs1)->get_w_3();
		float L_12 = (&___lhs0)->get_z_2();
		float L_13 = (&___rhs1)->get_x_0();
		float L_14 = (&___lhs0)->get_x_0();
		float L_15 = (&___rhs1)->get_z_2();
		float L_16 = (&___lhs0)->get_w_3();
		float L_17 = (&___rhs1)->get_z_2();
		float L_18 = (&___lhs0)->get_z_2();
		float L_19 = (&___rhs1)->get_w_3();
		float L_20 = (&___lhs0)->get_x_0();
		float L_21 = (&___rhs1)->get_y_1();
		float L_22 = (&___lhs0)->get_y_1();
		float L_23 = (&___rhs1)->get_x_0();
		float L_24 = (&___lhs0)->get_w_3();
		float L_25 = (&___rhs1)->get_w_3();
		float L_26 = (&___lhs0)->get_x_0();
		float L_27 = (&___rhs1)->get_x_0();
		float L_28 = (&___lhs0)->get_y_1();
		float L_29 = (&___rhs1)->get_y_1();
		float L_30 = (&___lhs0)->get_z_2();
		float L_31 = (&___rhs1)->get_z_2();
		Quaternion_t754065749  L_32;
		memset(&L_32, 0, sizeof(L_32));
		Quaternion__ctor_m1252814061((&L_32), ((float)((float)((float)((float)((float)((float)((float)((float)L_0*(float)L_1))+(float)((float)((float)L_2*(float)L_3))))+(float)((float)((float)L_4*(float)L_5))))-(float)((float)((float)L_6*(float)L_7)))), ((float)((float)((float)((float)((float)((float)((float)((float)L_8*(float)L_9))+(float)((float)((float)L_10*(float)L_11))))+(float)((float)((float)L_12*(float)L_13))))-(float)((float)((float)L_14*(float)L_15)))), ((float)((float)((float)((float)((float)((float)((float)((float)L_16*(float)L_17))+(float)((float)((float)L_18*(float)L_19))))+(float)((float)((float)L_20*(float)L_21))))-(float)((float)((float)L_22*(float)L_23)))), ((float)((float)((float)((float)((float)((float)((float)((float)L_24*(float)L_25))-(float)((float)((float)L_26*(float)L_27))))-(float)((float)((float)L_28*(float)L_29))))-(float)((float)((float)L_30*(float)L_31)))), /*hidden argument*/NULL);
		V_0 = L_32;
		goto IL_0108;
	}

IL_0108:
	{
		Quaternion_t754065749  L_33 = V_0;
		return L_33;
	}
}
// UnityEngine.Vector3 UnityEngine.Quaternion::op_Multiply(UnityEngine.Quaternion,UnityEngine.Vector3)
extern "C"  Vector3_t2903530434  Quaternion_op_Multiply_m1825091092 (RuntimeObject * __this /* static, unused */, Quaternion_t754065749  ___rotation0, Vector3_t2903530434  ___point1, const RuntimeMethod* method)
{
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	float V_3 = 0.0f;
	float V_4 = 0.0f;
	float V_5 = 0.0f;
	float V_6 = 0.0f;
	float V_7 = 0.0f;
	float V_8 = 0.0f;
	float V_9 = 0.0f;
	float V_10 = 0.0f;
	float V_11 = 0.0f;
	Vector3_t2903530434  V_12;
	memset(&V_12, 0, sizeof(V_12));
	Vector3_t2903530434  V_13;
	memset(&V_13, 0, sizeof(V_13));
	{
		float L_0 = (&___rotation0)->get_x_0();
		V_0 = ((float)((float)L_0*(float)(2.0f)));
		float L_1 = (&___rotation0)->get_y_1();
		V_1 = ((float)((float)L_1*(float)(2.0f)));
		float L_2 = (&___rotation0)->get_z_2();
		V_2 = ((float)((float)L_2*(float)(2.0f)));
		float L_3 = (&___rotation0)->get_x_0();
		float L_4 = V_0;
		V_3 = ((float)((float)L_3*(float)L_4));
		float L_5 = (&___rotation0)->get_y_1();
		float L_6 = V_1;
		V_4 = ((float)((float)L_5*(float)L_6));
		float L_7 = (&___rotation0)->get_z_2();
		float L_8 = V_2;
		V_5 = ((float)((float)L_7*(float)L_8));
		float L_9 = (&___rotation0)->get_x_0();
		float L_10 = V_1;
		V_6 = ((float)((float)L_9*(float)L_10));
		float L_11 = (&___rotation0)->get_x_0();
		float L_12 = V_2;
		V_7 = ((float)((float)L_11*(float)L_12));
		float L_13 = (&___rotation0)->get_y_1();
		float L_14 = V_2;
		V_8 = ((float)((float)L_13*(float)L_14));
		float L_15 = (&___rotation0)->get_w_3();
		float L_16 = V_0;
		V_9 = ((float)((float)L_15*(float)L_16));
		float L_17 = (&___rotation0)->get_w_3();
		float L_18 = V_1;
		V_10 = ((float)((float)L_17*(float)L_18));
		float L_19 = (&___rotation0)->get_w_3();
		float L_20 = V_2;
		V_11 = ((float)((float)L_19*(float)L_20));
		float L_21 = V_4;
		float L_22 = V_5;
		float L_23 = (&___point1)->get_x_1();
		float L_24 = V_6;
		float L_25 = V_11;
		float L_26 = (&___point1)->get_y_2();
		float L_27 = V_7;
		float L_28 = V_10;
		float L_29 = (&___point1)->get_z_3();
		(&V_12)->set_x_1(((float)((float)((float)((float)((float)((float)((float)((float)(1.0f)-(float)((float)((float)L_21+(float)L_22))))*(float)L_23))+(float)((float)((float)((float)((float)L_24-(float)L_25))*(float)L_26))))+(float)((float)((float)((float)((float)L_27+(float)L_28))*(float)L_29)))));
		float L_30 = V_6;
		float L_31 = V_11;
		float L_32 = (&___point1)->get_x_1();
		float L_33 = V_3;
		float L_34 = V_5;
		float L_35 = (&___point1)->get_y_2();
		float L_36 = V_8;
		float L_37 = V_9;
		float L_38 = (&___point1)->get_z_3();
		(&V_12)->set_y_2(((float)((float)((float)((float)((float)((float)((float)((float)L_30+(float)L_31))*(float)L_32))+(float)((float)((float)((float)((float)(1.0f)-(float)((float)((float)L_33+(float)L_34))))*(float)L_35))))+(float)((float)((float)((float)((float)L_36-(float)L_37))*(float)L_38)))));
		float L_39 = V_7;
		float L_40 = V_10;
		float L_41 = (&___point1)->get_x_1();
		float L_42 = V_8;
		float L_43 = V_9;
		float L_44 = (&___point1)->get_y_2();
		float L_45 = V_3;
		float L_46 = V_4;
		float L_47 = (&___point1)->get_z_3();
		(&V_12)->set_z_3(((float)((float)((float)((float)((float)((float)((float)((float)L_39-(float)L_40))*(float)L_41))+(float)((float)((float)((float)((float)L_42+(float)L_43))*(float)L_44))))+(float)((float)((float)((float)((float)(1.0f)-(float)((float)((float)L_45+(float)L_46))))*(float)L_47)))));
		Vector3_t2903530434  L_48 = V_12;
		V_13 = L_48;
		goto IL_0136;
	}

IL_0136:
	{
		Vector3_t2903530434  L_49 = V_13;
		return L_49;
	}
}
// System.Boolean UnityEngine.Quaternion::op_Equality(UnityEngine.Quaternion,UnityEngine.Quaternion)
extern "C"  bool Quaternion_op_Equality_m2930003662 (RuntimeObject * __this /* static, unused */, Quaternion_t754065749  ___lhs0, Quaternion_t754065749  ___rhs1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Quaternion_op_Equality_m2930003662_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	{
		Quaternion_t754065749  L_0 = ___lhs0;
		Quaternion_t754065749  L_1 = ___rhs1;
		IL2CPP_RUNTIME_CLASS_INIT(Quaternion_t754065749_il2cpp_TypeInfo_var);
		float L_2 = Quaternion_Dot_m3512793703(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		V_0 = (bool)((((float)L_2) > ((float)(0.999999f)))? 1 : 0);
		goto IL_0015;
	}

IL_0015:
	{
		bool L_3 = V_0;
		return L_3;
	}
}
// System.Boolean UnityEngine.Quaternion::op_Inequality(UnityEngine.Quaternion,UnityEngine.Quaternion)
extern "C"  bool Quaternion_op_Inequality_m1867215411 (RuntimeObject * __this /* static, unused */, Quaternion_t754065749  ___lhs0, Quaternion_t754065749  ___rhs1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Quaternion_op_Inequality_m1867215411_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	{
		Quaternion_t754065749  L_0 = ___lhs0;
		Quaternion_t754065749  L_1 = ___rhs1;
		IL2CPP_RUNTIME_CLASS_INIT(Quaternion_t754065749_il2cpp_TypeInfo_var);
		bool L_2 = Quaternion_op_Equality_m2930003662(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		V_0 = (bool)((((int32_t)L_2) == ((int32_t)0))? 1 : 0);
		goto IL_0011;
	}

IL_0011:
	{
		bool L_3 = V_0;
		return L_3;
	}
}
// System.Single UnityEngine.Quaternion::Dot(UnityEngine.Quaternion,UnityEngine.Quaternion)
extern "C"  float Quaternion_Dot_m3512793703 (RuntimeObject * __this /* static, unused */, Quaternion_t754065749  ___a0, Quaternion_t754065749  ___b1, const RuntimeMethod* method)
{
	float V_0 = 0.0f;
	{
		float L_0 = (&___a0)->get_x_0();
		float L_1 = (&___b1)->get_x_0();
		float L_2 = (&___a0)->get_y_1();
		float L_3 = (&___b1)->get_y_1();
		float L_4 = (&___a0)->get_z_2();
		float L_5 = (&___b1)->get_z_2();
		float L_6 = (&___a0)->get_w_3();
		float L_7 = (&___b1)->get_w_3();
		V_0 = ((float)((float)((float)((float)((float)((float)((float)((float)L_0*(float)L_1))+(float)((float)((float)L_2*(float)L_3))))+(float)((float)((float)L_4*(float)L_5))))+(float)((float)((float)L_6*(float)L_7))));
		goto IL_0046;
	}

IL_0046:
	{
		float L_8 = V_0;
		return L_8;
	}
}
// System.Single UnityEngine.Quaternion::Angle(UnityEngine.Quaternion,UnityEngine.Quaternion)
extern "C"  float Quaternion_Angle_m713725543 (RuntimeObject * __this /* static, unused */, Quaternion_t754065749  ___a0, Quaternion_t754065749  ___b1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Quaternion_Angle_m713725543_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	{
		Quaternion_t754065749  L_0 = ___a0;
		Quaternion_t754065749  L_1 = ___b1;
		IL2CPP_RUNTIME_CLASS_INIT(Quaternion_t754065749_il2cpp_TypeInfo_var);
		float L_2 = Quaternion_Dot_m3512793703(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		float L_3 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t4094287654_il2cpp_TypeInfo_var);
		float L_4 = fabsf(L_3);
		float L_5 = Mathf_Min_m3720534012(NULL /*static, unused*/, L_4, (1.0f), /*hidden argument*/NULL);
		float L_6 = acosf(L_5);
		V_1 = ((float)((float)((float)((float)L_6*(float)(2.0f)))*(float)(57.29578f)));
		goto IL_0030;
	}

IL_0030:
	{
		float L_7 = V_1;
		return L_7;
	}
}
// UnityEngine.Vector3 UnityEngine.Quaternion::Internal_MakePositive(UnityEngine.Vector3)
extern "C"  Vector3_t2903530434  Quaternion_Internal_MakePositive_m3017293218 (RuntimeObject * __this /* static, unused */, Vector3_t2903530434  ___euler0, const RuntimeMethod* method)
{
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	Vector3_t2903530434  V_2;
	memset(&V_2, 0, sizeof(V_2));
	{
		V_0 = (-0.005729578f);
		float L_0 = V_0;
		V_1 = ((float)((float)(360.0f)+(float)L_0));
		float L_1 = (&___euler0)->get_x_1();
		float L_2 = V_0;
		if ((!(((float)L_1) < ((float)L_2))))
		{
			goto IL_0034;
		}
	}
	{
		Vector3_t2903530434 * L_3 = (&___euler0);
		float L_4 = L_3->get_x_1();
		L_3->set_x_1(((float)((float)L_4+(float)(360.0f))));
		goto IL_0054;
	}

IL_0034:
	{
		float L_5 = (&___euler0)->get_x_1();
		float L_6 = V_1;
		if ((!(((float)L_5) > ((float)L_6))))
		{
			goto IL_0054;
		}
	}
	{
		Vector3_t2903530434 * L_7 = (&___euler0);
		float L_8 = L_7->get_x_1();
		L_7->set_x_1(((float)((float)L_8-(float)(360.0f))));
	}

IL_0054:
	{
		float L_9 = (&___euler0)->get_y_2();
		float L_10 = V_0;
		if ((!(((float)L_9) < ((float)L_10))))
		{
			goto IL_0079;
		}
	}
	{
		Vector3_t2903530434 * L_11 = (&___euler0);
		float L_12 = L_11->get_y_2();
		L_11->set_y_2(((float)((float)L_12+(float)(360.0f))));
		goto IL_0099;
	}

IL_0079:
	{
		float L_13 = (&___euler0)->get_y_2();
		float L_14 = V_1;
		if ((!(((float)L_13) > ((float)L_14))))
		{
			goto IL_0099;
		}
	}
	{
		Vector3_t2903530434 * L_15 = (&___euler0);
		float L_16 = L_15->get_y_2();
		L_15->set_y_2(((float)((float)L_16-(float)(360.0f))));
	}

IL_0099:
	{
		float L_17 = (&___euler0)->get_z_3();
		float L_18 = V_0;
		if ((!(((float)L_17) < ((float)L_18))))
		{
			goto IL_00be;
		}
	}
	{
		Vector3_t2903530434 * L_19 = (&___euler0);
		float L_20 = L_19->get_z_3();
		L_19->set_z_3(((float)((float)L_20+(float)(360.0f))));
		goto IL_00de;
	}

IL_00be:
	{
		float L_21 = (&___euler0)->get_z_3();
		float L_22 = V_1;
		if ((!(((float)L_21) > ((float)L_22))))
		{
			goto IL_00de;
		}
	}
	{
		Vector3_t2903530434 * L_23 = (&___euler0);
		float L_24 = L_23->get_z_3();
		L_23->set_z_3(((float)((float)L_24-(float)(360.0f))));
	}

IL_00de:
	{
		Vector3_t2903530434  L_25 = ___euler0;
		V_2 = L_25;
		goto IL_00e5;
	}

IL_00e5:
	{
		Vector3_t2903530434  L_26 = V_2;
		return L_26;
	}
}
// System.Int32 UnityEngine.Quaternion::GetHashCode()
extern "C"  int32_t Quaternion_GetHashCode_m2311280009 (Quaternion_t754065749 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		float* L_0 = __this->get_address_of_x_0();
		int32_t L_1 = Single_GetHashCode_m2049990008(L_0, /*hidden argument*/NULL);
		float* L_2 = __this->get_address_of_y_1();
		int32_t L_3 = Single_GetHashCode_m2049990008(L_2, /*hidden argument*/NULL);
		float* L_4 = __this->get_address_of_z_2();
		int32_t L_5 = Single_GetHashCode_m2049990008(L_4, /*hidden argument*/NULL);
		float* L_6 = __this->get_address_of_w_3();
		int32_t L_7 = Single_GetHashCode_m2049990008(L_6, /*hidden argument*/NULL);
		V_0 = ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_1^(int32_t)((int32_t)((int32_t)L_3<<(int32_t)2))))^(int32_t)((int32_t)((int32_t)L_5>>(int32_t)2))))^(int32_t)((int32_t)((int32_t)L_7>>(int32_t)1))));
		goto IL_0054;
	}

IL_0054:
	{
		int32_t L_8 = V_0;
		return L_8;
	}
}
extern "C"  int32_t Quaternion_GetHashCode_m2311280009_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Quaternion_t754065749 * _thisAdjusted = reinterpret_cast<Quaternion_t754065749 *>(__this + 1);
	return Quaternion_GetHashCode_m2311280009(_thisAdjusted, method);
}
// System.Boolean UnityEngine.Quaternion::Equals(System.Object)
extern "C"  bool Quaternion_Equals_m3533589735 (Quaternion_t754065749 * __this, RuntimeObject * ___other0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Quaternion_Equals_m3533589735_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	Quaternion_t754065749  V_1;
	memset(&V_1, 0, sizeof(V_1));
	int32_t G_B7_0 = 0;
	{
		RuntimeObject * L_0 = ___other0;
		if (((RuntimeObject *)IsInstSealed((RuntimeObject*)L_0, Quaternion_t754065749_il2cpp_TypeInfo_var)))
		{
			goto IL_0013;
		}
	}
	{
		V_0 = (bool)0;
		goto IL_007a;
	}

IL_0013:
	{
		RuntimeObject * L_1 = ___other0;
		V_1 = ((*(Quaternion_t754065749 *)((Quaternion_t754065749 *)UnBox(L_1, Quaternion_t754065749_il2cpp_TypeInfo_var))));
		float* L_2 = __this->get_address_of_x_0();
		float L_3 = (&V_1)->get_x_0();
		bool L_4 = Single_Equals_m4183737958(L_2, L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0073;
		}
	}
	{
		float* L_5 = __this->get_address_of_y_1();
		float L_6 = (&V_1)->get_y_1();
		bool L_7 = Single_Equals_m4183737958(L_5, L_6, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0073;
		}
	}
	{
		float* L_8 = __this->get_address_of_z_2();
		float L_9 = (&V_1)->get_z_2();
		bool L_10 = Single_Equals_m4183737958(L_8, L_9, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_0073;
		}
	}
	{
		float* L_11 = __this->get_address_of_w_3();
		float L_12 = (&V_1)->get_w_3();
		bool L_13 = Single_Equals_m4183737958(L_11, L_12, /*hidden argument*/NULL);
		G_B7_0 = ((int32_t)(L_13));
		goto IL_0074;
	}

IL_0073:
	{
		G_B7_0 = 0;
	}

IL_0074:
	{
		V_0 = (bool)G_B7_0;
		goto IL_007a;
	}

IL_007a:
	{
		bool L_14 = V_0;
		return L_14;
	}
}
extern "C"  bool Quaternion_Equals_m3533589735_AdjustorThunk (RuntimeObject * __this, RuntimeObject * ___other0, const RuntimeMethod* method)
{
	Quaternion_t754065749 * _thisAdjusted = reinterpret_cast<Quaternion_t754065749 *>(__this + 1);
	return Quaternion_Equals_m3533589735(_thisAdjusted, ___other0, method);
}
// System.String UnityEngine.Quaternion::ToString()
extern "C"  String_t* Quaternion_ToString_m329630960 (Quaternion_t754065749 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Quaternion_ToString_m329630960_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	{
		ObjectU5BU5D_t3270211303* L_0 = ((ObjectU5BU5D_t3270211303*)SZArrayNew(ObjectU5BU5D_t3270211303_il2cpp_TypeInfo_var, (uint32_t)4));
		float L_1 = __this->get_x_0();
		float L_2 = L_1;
		RuntimeObject * L_3 = Box(Single_t3045349759_il2cpp_TypeInfo_var, &L_2);
		NullCheck(L_0);
		ArrayElementTypeCheck (L_0, L_3);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (RuntimeObject *)L_3);
		ObjectU5BU5D_t3270211303* L_4 = L_0;
		float L_5 = __this->get_y_1();
		float L_6 = L_5;
		RuntimeObject * L_7 = Box(Single_t3045349759_il2cpp_TypeInfo_var, &L_6);
		NullCheck(L_4);
		ArrayElementTypeCheck (L_4, L_7);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(1), (RuntimeObject *)L_7);
		ObjectU5BU5D_t3270211303* L_8 = L_4;
		float L_9 = __this->get_z_2();
		float L_10 = L_9;
		RuntimeObject * L_11 = Box(Single_t3045349759_il2cpp_TypeInfo_var, &L_10);
		NullCheck(L_8);
		ArrayElementTypeCheck (L_8, L_11);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(2), (RuntimeObject *)L_11);
		ObjectU5BU5D_t3270211303* L_12 = L_8;
		float L_13 = __this->get_w_3();
		float L_14 = L_13;
		RuntimeObject * L_15 = Box(Single_t3045349759_il2cpp_TypeInfo_var, &L_14);
		NullCheck(L_12);
		ArrayElementTypeCheck (L_12, L_15);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(3), (RuntimeObject *)L_15);
		String_t* L_16 = UnityString_Format_m2401112968(NULL /*static, unused*/, _stringLiteral37643425, L_12, /*hidden argument*/NULL);
		V_0 = L_16;
		goto IL_004f;
	}

IL_004f:
	{
		String_t* L_17 = V_0;
		return L_17;
	}
}
extern "C"  String_t* Quaternion_ToString_m329630960_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Quaternion_t754065749 * _thisAdjusted = reinterpret_cast<Quaternion_t754065749 *>(__this + 1);
	return Quaternion_ToString_m329630960(_thisAdjusted, method);
}
// System.Void UnityEngine.Quaternion::.cctor()
extern "C"  void Quaternion__cctor_m3598117699 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Quaternion__cctor_m3598117699_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Quaternion_t754065749  L_0;
		memset(&L_0, 0, sizeof(L_0));
		Quaternion__ctor_m1252814061((&L_0), (0.0f), (0.0f), (0.0f), (1.0f), /*hidden argument*/NULL);
		((Quaternion_t754065749_StaticFields*)il2cpp_codegen_static_fields_for(Quaternion_t754065749_il2cpp_TypeInfo_var))->set_identityQuaternion_4(L_0);
		return;
	}
}
// System.Single UnityEngine.Random::Range(System.Single,System.Single)
extern "C"  float Random_Range_m1704748801 (RuntimeObject * __this /* static, unused */, float ___min0, float ___max1, const RuntimeMethod* method)
{
	typedef float (*Random_Range_m1704748801_ftn) (float, float);
	static Random_Range_m1704748801_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Random_Range_m1704748801_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Random::Range(System.Single,System.Single)");
	float retVal = _il2cpp_icall_func(___min0, ___max1);
	return retVal;
}
// System.Int32 UnityEngine.Random::Range(System.Int32,System.Int32)
extern "C"  int32_t Random_Range_m3403795131 (RuntimeObject * __this /* static, unused */, int32_t ___min0, int32_t ___max1, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = ___min0;
		int32_t L_1 = ___max1;
		int32_t L_2 = Random_RandomRangeInt_m1371228312(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_000e;
	}

IL_000e:
	{
		int32_t L_3 = V_0;
		return L_3;
	}
}
// System.Int32 UnityEngine.Random::RandomRangeInt(System.Int32,System.Int32)
extern "C"  int32_t Random_RandomRangeInt_m1371228312 (RuntimeObject * __this /* static, unused */, int32_t ___min0, int32_t ___max1, const RuntimeMethod* method)
{
	typedef int32_t (*Random_RandomRangeInt_m1371228312_ftn) (int32_t, int32_t);
	static Random_RandomRangeInt_m1371228312_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Random_RandomRangeInt_m1371228312_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Random::RandomRangeInt(System.Int32,System.Int32)");
	int32_t retVal = _il2cpp_icall_func(___min0, ___max1);
	return retVal;
}
// System.Void UnityEngine.RangeAttribute::.ctor(System.Single,System.Single)
extern "C"  void RangeAttribute__ctor_m1629113428 (RangeAttribute_t3376577711 * __this, float ___min0, float ___max1, const RuntimeMethod* method)
{
	{
		PropertyAttribute__ctor_m2084867108(__this, /*hidden argument*/NULL);
		float L_0 = ___min0;
		__this->set_min_0(L_0);
		float L_1 = ___max1;
		__this->set_max_1(L_1);
		return;
	}
}
// System.Int32 UnityEngine.RangeInt::get_end()
extern "C"  int32_t RangeInt_get_end_m3898523404 (RangeInt_t1885873663 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_start_0();
		int32_t L_1 = __this->get_length_1();
		V_0 = ((int32_t)((int32_t)L_0+(int32_t)L_1));
		goto IL_0014;
	}

IL_0014:
	{
		int32_t L_2 = V_0;
		return L_2;
	}
}
extern "C"  int32_t RangeInt_get_end_m3898523404_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	RangeInt_t1885873663 * _thisAdjusted = reinterpret_cast<RangeInt_t1885873663 *>(__this + 1);
	return RangeInt_get_end_m3898523404(_thisAdjusted, method);
}
// System.Void UnityEngine.Ray::.ctor(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  void Ray__ctor_m1620751038 (Ray_t3821377119 * __this, Vector3_t2903530434  ___origin0, Vector3_t2903530434  ___direction1, const RuntimeMethod* method)
{
	{
		Vector3_t2903530434  L_0 = ___origin0;
		__this->set_m_Origin_0(L_0);
		Vector3_t2903530434  L_1 = Vector3_get_normalized_m2996377041((&___direction1), /*hidden argument*/NULL);
		__this->set_m_Direction_1(L_1);
		return;
	}
}
extern "C"  void Ray__ctor_m1620751038_AdjustorThunk (RuntimeObject * __this, Vector3_t2903530434  ___origin0, Vector3_t2903530434  ___direction1, const RuntimeMethod* method)
{
	Ray_t3821377119 * _thisAdjusted = reinterpret_cast<Ray_t3821377119 *>(__this + 1);
	Ray__ctor_m1620751038(_thisAdjusted, ___origin0, ___direction1, method);
}
// UnityEngine.Vector3 UnityEngine.Ray::get_origin()
extern "C"  Vector3_t2903530434  Ray_get_origin_m126064229 (Ray_t3821377119 * __this, const RuntimeMethod* method)
{
	Vector3_t2903530434  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Vector3_t2903530434  L_0 = __this->get_m_Origin_0();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		Vector3_t2903530434  L_1 = V_0;
		return L_1;
	}
}
extern "C"  Vector3_t2903530434  Ray_get_origin_m126064229_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Ray_t3821377119 * _thisAdjusted = reinterpret_cast<Ray_t3821377119 *>(__this + 1);
	return Ray_get_origin_m126064229(_thisAdjusted, method);
}
// UnityEngine.Vector3 UnityEngine.Ray::get_direction()
extern "C"  Vector3_t2903530434  Ray_get_direction_m3512869925 (Ray_t3821377119 * __this, const RuntimeMethod* method)
{
	Vector3_t2903530434  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Vector3_t2903530434  L_0 = __this->get_m_Direction_1();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		Vector3_t2903530434  L_1 = V_0;
		return L_1;
	}
}
extern "C"  Vector3_t2903530434  Ray_get_direction_m3512869925_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Ray_t3821377119 * _thisAdjusted = reinterpret_cast<Ray_t3821377119 *>(__this + 1);
	return Ray_get_direction_m3512869925(_thisAdjusted, method);
}
// UnityEngine.Vector3 UnityEngine.Ray::GetPoint(System.Single)
extern "C"  Vector3_t2903530434  Ray_GetPoint_m3472514115 (Ray_t3821377119 * __this, float ___distance0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Ray_GetPoint_m3472514115_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t2903530434  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Vector3_t2903530434  L_0 = __this->get_m_Origin_0();
		Vector3_t2903530434  L_1 = __this->get_m_Direction_1();
		float L_2 = ___distance0;
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t2903530434_il2cpp_TypeInfo_var);
		Vector3_t2903530434  L_3 = Vector3_op_Multiply_m2063944565(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		Vector3_t2903530434  L_4 = Vector3_op_Addition_m630862342(NULL /*static, unused*/, L_0, L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		goto IL_001e;
	}

IL_001e:
	{
		Vector3_t2903530434  L_5 = V_0;
		return L_5;
	}
}
extern "C"  Vector3_t2903530434  Ray_GetPoint_m3472514115_AdjustorThunk (RuntimeObject * __this, float ___distance0, const RuntimeMethod* method)
{
	Ray_t3821377119 * _thisAdjusted = reinterpret_cast<Ray_t3821377119 *>(__this + 1);
	return Ray_GetPoint_m3472514115(_thisAdjusted, ___distance0, method);
}
// System.String UnityEngine.Ray::ToString()
extern "C"  String_t* Ray_ToString_m1456301833 (Ray_t3821377119 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Ray_ToString_m1456301833_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	{
		ObjectU5BU5D_t3270211303* L_0 = ((ObjectU5BU5D_t3270211303*)SZArrayNew(ObjectU5BU5D_t3270211303_il2cpp_TypeInfo_var, (uint32_t)2));
		Vector3_t2903530434  L_1 = __this->get_m_Origin_0();
		Vector3_t2903530434  L_2 = L_1;
		RuntimeObject * L_3 = Box(Vector3_t2903530434_il2cpp_TypeInfo_var, &L_2);
		NullCheck(L_0);
		ArrayElementTypeCheck (L_0, L_3);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (RuntimeObject *)L_3);
		ObjectU5BU5D_t3270211303* L_4 = L_0;
		Vector3_t2903530434  L_5 = __this->get_m_Direction_1();
		Vector3_t2903530434  L_6 = L_5;
		RuntimeObject * L_7 = Box(Vector3_t2903530434_il2cpp_TypeInfo_var, &L_6);
		NullCheck(L_4);
		ArrayElementTypeCheck (L_4, L_7);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(1), (RuntimeObject *)L_7);
		String_t* L_8 = UnityString_Format_m2401112968(NULL /*static, unused*/, _stringLiteral2680542856, L_4, /*hidden argument*/NULL);
		V_0 = L_8;
		goto IL_0033;
	}

IL_0033:
	{
		String_t* L_9 = V_0;
		return L_9;
	}
}
extern "C"  String_t* Ray_ToString_m1456301833_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Ray_t3821377119 * _thisAdjusted = reinterpret_cast<Ray_t3821377119 *>(__this + 1);
	return Ray_ToString_m1456301833(_thisAdjusted, method);
}
// Conversion methods for marshalling of: UnityEngine.RaycastHit
extern "C" void RaycastHit_t2327766465_marshal_pinvoke(const RaycastHit_t2327766465& unmarshaled, RaycastHit_t2327766465_marshaled_pinvoke& marshaled)
{
	Il2CppCodeGenException* ___m_Collider_5Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_Collider' of type 'RaycastHit': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_Collider_5Exception);
}
extern "C" void RaycastHit_t2327766465_marshal_pinvoke_back(const RaycastHit_t2327766465_marshaled_pinvoke& marshaled, RaycastHit_t2327766465& unmarshaled)
{
	Il2CppCodeGenException* ___m_Collider_5Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_Collider' of type 'RaycastHit': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_Collider_5Exception);
}
// Conversion method for clean up from marshalling of: UnityEngine.RaycastHit
extern "C" void RaycastHit_t2327766465_marshal_pinvoke_cleanup(RaycastHit_t2327766465_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.RaycastHit
extern "C" void RaycastHit_t2327766465_marshal_com(const RaycastHit_t2327766465& unmarshaled, RaycastHit_t2327766465_marshaled_com& marshaled)
{
	Il2CppCodeGenException* ___m_Collider_5Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_Collider' of type 'RaycastHit': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_Collider_5Exception);
}
extern "C" void RaycastHit_t2327766465_marshal_com_back(const RaycastHit_t2327766465_marshaled_com& marshaled, RaycastHit_t2327766465& unmarshaled)
{
	Il2CppCodeGenException* ___m_Collider_5Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_Collider' of type 'RaycastHit': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_Collider_5Exception);
}
// Conversion method for clean up from marshalling of: UnityEngine.RaycastHit
extern "C" void RaycastHit_t2327766465_marshal_com_cleanup(RaycastHit_t2327766465_marshaled_com& marshaled)
{
}
// UnityEngine.Vector3 UnityEngine.RaycastHit::get_point()
extern "C"  Vector3_t2903530434  RaycastHit_get_point_m2039617676 (RaycastHit_t2327766465 * __this, const RuntimeMethod* method)
{
	Vector3_t2903530434  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Vector3_t2903530434  L_0 = __this->get_m_Point_0();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		Vector3_t2903530434  L_1 = V_0;
		return L_1;
	}
}
extern "C"  Vector3_t2903530434  RaycastHit_get_point_m2039617676_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	RaycastHit_t2327766465 * _thisAdjusted = reinterpret_cast<RaycastHit_t2327766465 *>(__this + 1);
	return RaycastHit_get_point_m2039617676(_thisAdjusted, method);
}
// UnityEngine.Vector3 UnityEngine.RaycastHit::get_normal()
extern "C"  Vector3_t2903530434  RaycastHit_get_normal_m1382249399 (RaycastHit_t2327766465 * __this, const RuntimeMethod* method)
{
	Vector3_t2903530434  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Vector3_t2903530434  L_0 = __this->get_m_Normal_1();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		Vector3_t2903530434  L_1 = V_0;
		return L_1;
	}
}
extern "C"  Vector3_t2903530434  RaycastHit_get_normal_m1382249399_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	RaycastHit_t2327766465 * _thisAdjusted = reinterpret_cast<RaycastHit_t2327766465 *>(__this + 1);
	return RaycastHit_get_normal_m1382249399(_thisAdjusted, method);
}
// System.Single UnityEngine.RaycastHit::get_distance()
extern "C"  float RaycastHit_get_distance_m3119928599 (RaycastHit_t2327766465 * __this, const RuntimeMethod* method)
{
	float V_0 = 0.0f;
	{
		float L_0 = __this->get_m_Distance_3();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		float L_1 = V_0;
		return L_1;
	}
}
extern "C"  float RaycastHit_get_distance_m3119928599_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	RaycastHit_t2327766465 * _thisAdjusted = reinterpret_cast<RaycastHit_t2327766465 *>(__this + 1);
	return RaycastHit_get_distance_m3119928599(_thisAdjusted, method);
}
// UnityEngine.Collider UnityEngine.RaycastHit::get_collider()
extern "C"  Collider_t2301021182 * RaycastHit_get_collider_m362701715 (RaycastHit_t2327766465 * __this, const RuntimeMethod* method)
{
	Collider_t2301021182 * V_0 = NULL;
	{
		Collider_t2301021182 * L_0 = __this->get_m_Collider_5();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		Collider_t2301021182 * L_1 = V_0;
		return L_1;
	}
}
extern "C"  Collider_t2301021182 * RaycastHit_get_collider_m362701715_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	RaycastHit_t2327766465 * _thisAdjusted = reinterpret_cast<RaycastHit_t2327766465 *>(__this + 1);
	return RaycastHit_get_collider_m362701715(_thisAdjusted, method);
}
// UnityEngine.Rigidbody UnityEngine.RaycastHit::get_rigidbody()
extern "C"  Rigidbody_t2492269564 * RaycastHit_get_rigidbody_m3749375064 (RaycastHit_t2327766465 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RaycastHit_get_rigidbody_m3749375064_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Rigidbody_t2492269564 * V_0 = NULL;
	Rigidbody_t2492269564 * G_B3_0 = NULL;
	{
		Collider_t2301021182 * L_0 = RaycastHit_get_collider_m362701715(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1502412432_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m1918954159(NULL /*static, unused*/, L_0, (Object_t1502412432 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0022;
		}
	}
	{
		Collider_t2301021182 * L_2 = RaycastHit_get_collider_m362701715(__this, /*hidden argument*/NULL);
		NullCheck(L_2);
		Rigidbody_t2492269564 * L_3 = Collider_get_attachedRigidbody_m2035898500(L_2, /*hidden argument*/NULL);
		G_B3_0 = L_3;
		goto IL_0023;
	}

IL_0022:
	{
		G_B3_0 = ((Rigidbody_t2492269564 *)(NULL));
	}

IL_0023:
	{
		V_0 = G_B3_0;
		goto IL_0029;
	}

IL_0029:
	{
		Rigidbody_t2492269564 * L_4 = V_0;
		return L_4;
	}
}
extern "C"  Rigidbody_t2492269564 * RaycastHit_get_rigidbody_m3749375064_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	RaycastHit_t2327766465 * _thisAdjusted = reinterpret_cast<RaycastHit_t2327766465 *>(__this + 1);
	return RaycastHit_get_rigidbody_m3749375064(_thisAdjusted, method);
}
// Conversion methods for marshalling of: UnityEngine.RaycastHit2D
extern "C" void RaycastHit2D_t1105181991_marshal_pinvoke(const RaycastHit2D_t1105181991& unmarshaled, RaycastHit2D_t1105181991_marshaled_pinvoke& marshaled)
{
	Il2CppCodeGenException* ___m_Collider_5Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_Collider' of type 'RaycastHit2D': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_Collider_5Exception);
}
extern "C" void RaycastHit2D_t1105181991_marshal_pinvoke_back(const RaycastHit2D_t1105181991_marshaled_pinvoke& marshaled, RaycastHit2D_t1105181991& unmarshaled)
{
	Il2CppCodeGenException* ___m_Collider_5Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_Collider' of type 'RaycastHit2D': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_Collider_5Exception);
}
// Conversion method for clean up from marshalling of: UnityEngine.RaycastHit2D
extern "C" void RaycastHit2D_t1105181991_marshal_pinvoke_cleanup(RaycastHit2D_t1105181991_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.RaycastHit2D
extern "C" void RaycastHit2D_t1105181991_marshal_com(const RaycastHit2D_t1105181991& unmarshaled, RaycastHit2D_t1105181991_marshaled_com& marshaled)
{
	Il2CppCodeGenException* ___m_Collider_5Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_Collider' of type 'RaycastHit2D': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_Collider_5Exception);
}
extern "C" void RaycastHit2D_t1105181991_marshal_com_back(const RaycastHit2D_t1105181991_marshaled_com& marshaled, RaycastHit2D_t1105181991& unmarshaled)
{
	Il2CppCodeGenException* ___m_Collider_5Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_Collider' of type 'RaycastHit2D': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_Collider_5Exception);
}
// Conversion method for clean up from marshalling of: UnityEngine.RaycastHit2D
extern "C" void RaycastHit2D_t1105181991_marshal_com_cleanup(RaycastHit2D_t1105181991_marshaled_com& marshaled)
{
}
// UnityEngine.Vector2 UnityEngine.RaycastHit2D::get_point()
extern "C"  Vector2_t3577333262  RaycastHit2D_get_point_m2858777941 (RaycastHit2D_t1105181991 * __this, const RuntimeMethod* method)
{
	Vector2_t3577333262  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Vector2_t3577333262  L_0 = __this->get_m_Point_1();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		Vector2_t3577333262  L_1 = V_0;
		return L_1;
	}
}
extern "C"  Vector2_t3577333262  RaycastHit2D_get_point_m2858777941_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	RaycastHit2D_t1105181991 * _thisAdjusted = reinterpret_cast<RaycastHit2D_t1105181991 *>(__this + 1);
	return RaycastHit2D_get_point_m2858777941(_thisAdjusted, method);
}
// UnityEngine.Vector2 UnityEngine.RaycastHit2D::get_normal()
extern "C"  Vector2_t3577333262  RaycastHit2D_get_normal_m496919313 (RaycastHit2D_t1105181991 * __this, const RuntimeMethod* method)
{
	Vector2_t3577333262  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Vector2_t3577333262  L_0 = __this->get_m_Normal_2();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		Vector2_t3577333262  L_1 = V_0;
		return L_1;
	}
}
extern "C"  Vector2_t3577333262  RaycastHit2D_get_normal_m496919313_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	RaycastHit2D_t1105181991 * _thisAdjusted = reinterpret_cast<RaycastHit2D_t1105181991 *>(__this + 1);
	return RaycastHit2D_get_normal_m496919313(_thisAdjusted, method);
}
// System.Single UnityEngine.RaycastHit2D::get_distance()
extern "C"  float RaycastHit2D_get_distance_m2267927945 (RaycastHit2D_t1105181991 * __this, const RuntimeMethod* method)
{
	float V_0 = 0.0f;
	{
		float L_0 = __this->get_m_Distance_3();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		float L_1 = V_0;
		return L_1;
	}
}
extern "C"  float RaycastHit2D_get_distance_m2267927945_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	RaycastHit2D_t1105181991 * _thisAdjusted = reinterpret_cast<RaycastHit2D_t1105181991 *>(__this + 1);
	return RaycastHit2D_get_distance_m2267927945(_thisAdjusted, method);
}
// UnityEngine.Collider2D UnityEngine.RaycastHit2D::get_collider()
extern "C"  Collider2D_t1623106781 * RaycastHit2D_get_collider_m1920199818 (RaycastHit2D_t1105181991 * __this, const RuntimeMethod* method)
{
	Collider2D_t1623106781 * V_0 = NULL;
	{
		Collider2D_t1623106781 * L_0 = __this->get_m_Collider_5();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		Collider2D_t1623106781 * L_1 = V_0;
		return L_1;
	}
}
extern "C"  Collider2D_t1623106781 * RaycastHit2D_get_collider_m1920199818_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	RaycastHit2D_t1105181991 * _thisAdjusted = reinterpret_cast<RaycastHit2D_t1105181991 *>(__this + 1);
	return RaycastHit2D_get_collider_m1920199818(_thisAdjusted, method);
}
// System.Void UnityEngine.Rect::.ctor(System.Single,System.Single,System.Single,System.Single)
extern "C"  void Rect__ctor_m2728905368 (Rect_t3436776195 * __this, float ___x0, float ___y1, float ___width2, float ___height3, const RuntimeMethod* method)
{
	{
		float L_0 = ___x0;
		__this->set_m_XMin_0(L_0);
		float L_1 = ___y1;
		__this->set_m_YMin_1(L_1);
		float L_2 = ___width2;
		__this->set_m_Width_2(L_2);
		float L_3 = ___height3;
		__this->set_m_Height_3(L_3);
		return;
	}
}
extern "C"  void Rect__ctor_m2728905368_AdjustorThunk (RuntimeObject * __this, float ___x0, float ___y1, float ___width2, float ___height3, const RuntimeMethod* method)
{
	Rect_t3436776195 * _thisAdjusted = reinterpret_cast<Rect_t3436776195 *>(__this + 1);
	Rect__ctor_m2728905368(_thisAdjusted, ___x0, ___y1, ___width2, ___height3, method);
}
// System.Void UnityEngine.Rect::.ctor(UnityEngine.Rect)
extern "C"  void Rect__ctor_m398740561 (Rect_t3436776195 * __this, Rect_t3436776195  ___source0, const RuntimeMethod* method)
{
	{
		float L_0 = (&___source0)->get_m_XMin_0();
		__this->set_m_XMin_0(L_0);
		float L_1 = (&___source0)->get_m_YMin_1();
		__this->set_m_YMin_1(L_1);
		float L_2 = (&___source0)->get_m_Width_2();
		__this->set_m_Width_2(L_2);
		float L_3 = (&___source0)->get_m_Height_3();
		__this->set_m_Height_3(L_3);
		return;
	}
}
extern "C"  void Rect__ctor_m398740561_AdjustorThunk (RuntimeObject * __this, Rect_t3436776195  ___source0, const RuntimeMethod* method)
{
	Rect_t3436776195 * _thisAdjusted = reinterpret_cast<Rect_t3436776195 *>(__this + 1);
	Rect__ctor_m398740561(_thisAdjusted, ___source0, method);
}
// System.Single UnityEngine.Rect::get_x()
extern "C"  float Rect_get_x_m2895292749 (Rect_t3436776195 * __this, const RuntimeMethod* method)
{
	float V_0 = 0.0f;
	{
		float L_0 = __this->get_m_XMin_0();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		float L_1 = V_0;
		return L_1;
	}
}
extern "C"  float Rect_get_x_m2895292749_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Rect_t3436776195 * _thisAdjusted = reinterpret_cast<Rect_t3436776195 *>(__this + 1);
	return Rect_get_x_m2895292749(_thisAdjusted, method);
}
// System.Void UnityEngine.Rect::set_x(System.Single)
extern "C"  void Rect_set_x_m3831764163 (Rect_t3436776195 * __this, float ___value0, const RuntimeMethod* method)
{
	{
		float L_0 = ___value0;
		__this->set_m_XMin_0(L_0);
		return;
	}
}
extern "C"  void Rect_set_x_m3831764163_AdjustorThunk (RuntimeObject * __this, float ___value0, const RuntimeMethod* method)
{
	Rect_t3436776195 * _thisAdjusted = reinterpret_cast<Rect_t3436776195 *>(__this + 1);
	Rect_set_x_m3831764163(_thisAdjusted, ___value0, method);
}
// System.Single UnityEngine.Rect::get_y()
extern "C"  float Rect_get_y_m782433804 (Rect_t3436776195 * __this, const RuntimeMethod* method)
{
	float V_0 = 0.0f;
	{
		float L_0 = __this->get_m_YMin_1();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		float L_1 = V_0;
		return L_1;
	}
}
extern "C"  float Rect_get_y_m782433804_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Rect_t3436776195 * _thisAdjusted = reinterpret_cast<Rect_t3436776195 *>(__this + 1);
	return Rect_get_y_m782433804(_thisAdjusted, method);
}
// System.Void UnityEngine.Rect::set_y(System.Single)
extern "C"  void Rect_set_y_m2194139077 (Rect_t3436776195 * __this, float ___value0, const RuntimeMethod* method)
{
	{
		float L_0 = ___value0;
		__this->set_m_YMin_1(L_0);
		return;
	}
}
extern "C"  void Rect_set_y_m2194139077_AdjustorThunk (RuntimeObject * __this, float ___value0, const RuntimeMethod* method)
{
	Rect_t3436776195 * _thisAdjusted = reinterpret_cast<Rect_t3436776195 *>(__this + 1);
	Rect_set_y_m2194139077(_thisAdjusted, ___value0, method);
}
// UnityEngine.Vector2 UnityEngine.Rect::get_position()
extern "C"  Vector2_t3577333262  Rect_get_position_m3279877841 (Rect_t3436776195 * __this, const RuntimeMethod* method)
{
	Vector2_t3577333262  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		float L_0 = __this->get_m_XMin_0();
		float L_1 = __this->get_m_YMin_1();
		Vector2_t3577333262  L_2;
		memset(&L_2, 0, sizeof(L_2));
		Vector2__ctor_m2914153465((&L_2), L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_0018;
	}

IL_0018:
	{
		Vector2_t3577333262  L_3 = V_0;
		return L_3;
	}
}
extern "C"  Vector2_t3577333262  Rect_get_position_m3279877841_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Rect_t3436776195 * _thisAdjusted = reinterpret_cast<Rect_t3436776195 *>(__this + 1);
	return Rect_get_position_m3279877841(_thisAdjusted, method);
}
// UnityEngine.Vector2 UnityEngine.Rect::get_center()
extern "C"  Vector2_t3577333262  Rect_get_center_m3371486321 (Rect_t3436776195 * __this, const RuntimeMethod* method)
{
	Vector2_t3577333262  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		float L_0 = Rect_get_x_m2895292749(__this, /*hidden argument*/NULL);
		float L_1 = __this->get_m_Width_2();
		float L_2 = Rect_get_y_m782433804(__this, /*hidden argument*/NULL);
		float L_3 = __this->get_m_Height_3();
		Vector2_t3577333262  L_4;
		memset(&L_4, 0, sizeof(L_4));
		Vector2__ctor_m2914153465((&L_4), ((float)((float)L_0+(float)((float)((float)L_1/(float)(2.0f))))), ((float)((float)L_2+(float)((float)((float)L_3/(float)(2.0f))))), /*hidden argument*/NULL);
		V_0 = L_4;
		goto IL_0032;
	}

IL_0032:
	{
		Vector2_t3577333262  L_5 = V_0;
		return L_5;
	}
}
extern "C"  Vector2_t3577333262  Rect_get_center_m3371486321_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Rect_t3436776195 * _thisAdjusted = reinterpret_cast<Rect_t3436776195 *>(__this + 1);
	return Rect_get_center_m3371486321(_thisAdjusted, method);
}
// UnityEngine.Vector2 UnityEngine.Rect::get_min()
extern "C"  Vector2_t3577333262  Rect_get_min_m3933677767 (Rect_t3436776195 * __this, const RuntimeMethod* method)
{
	Vector2_t3577333262  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		float L_0 = Rect_get_xMin_m3183485224(__this, /*hidden argument*/NULL);
		float L_1 = Rect_get_yMin_m2574684533(__this, /*hidden argument*/NULL);
		Vector2_t3577333262  L_2;
		memset(&L_2, 0, sizeof(L_2));
		Vector2__ctor_m2914153465((&L_2), L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_0018;
	}

IL_0018:
	{
		Vector2_t3577333262  L_3 = V_0;
		return L_3;
	}
}
extern "C"  Vector2_t3577333262  Rect_get_min_m3933677767_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Rect_t3436776195 * _thisAdjusted = reinterpret_cast<Rect_t3436776195 *>(__this + 1);
	return Rect_get_min_m3933677767(_thisAdjusted, method);
}
// UnityEngine.Vector2 UnityEngine.Rect::get_max()
extern "C"  Vector2_t3577333262  Rect_get_max_m390465090 (Rect_t3436776195 * __this, const RuntimeMethod* method)
{
	Vector2_t3577333262  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		float L_0 = Rect_get_xMax_m59812867(__this, /*hidden argument*/NULL);
		float L_1 = Rect_get_yMax_m439704772(__this, /*hidden argument*/NULL);
		Vector2_t3577333262  L_2;
		memset(&L_2, 0, sizeof(L_2));
		Vector2__ctor_m2914153465((&L_2), L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_0018;
	}

IL_0018:
	{
		Vector2_t3577333262  L_3 = V_0;
		return L_3;
	}
}
extern "C"  Vector2_t3577333262  Rect_get_max_m390465090_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Rect_t3436776195 * _thisAdjusted = reinterpret_cast<Rect_t3436776195 *>(__this + 1);
	return Rect_get_max_m390465090(_thisAdjusted, method);
}
// System.Single UnityEngine.Rect::get_width()
extern "C"  float Rect_get_width_m3471045153 (Rect_t3436776195 * __this, const RuntimeMethod* method)
{
	float V_0 = 0.0f;
	{
		float L_0 = __this->get_m_Width_2();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		float L_1 = V_0;
		return L_1;
	}
}
extern "C"  float Rect_get_width_m3471045153_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Rect_t3436776195 * _thisAdjusted = reinterpret_cast<Rect_t3436776195 *>(__this + 1);
	return Rect_get_width_m3471045153(_thisAdjusted, method);
}
// System.Void UnityEngine.Rect::set_width(System.Single)
extern "C"  void Rect_set_width_m3846748752 (Rect_t3436776195 * __this, float ___value0, const RuntimeMethod* method)
{
	{
		float L_0 = ___value0;
		__this->set_m_Width_2(L_0);
		return;
	}
}
extern "C"  void Rect_set_width_m3846748752_AdjustorThunk (RuntimeObject * __this, float ___value0, const RuntimeMethod* method)
{
	Rect_t3436776195 * _thisAdjusted = reinterpret_cast<Rect_t3436776195 *>(__this + 1);
	Rect_set_width_m3846748752(_thisAdjusted, ___value0, method);
}
// System.Single UnityEngine.Rect::get_height()
extern "C"  float Rect_get_height_m16400436 (Rect_t3436776195 * __this, const RuntimeMethod* method)
{
	float V_0 = 0.0f;
	{
		float L_0 = __this->get_m_Height_3();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		float L_1 = V_0;
		return L_1;
	}
}
extern "C"  float Rect_get_height_m16400436_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Rect_t3436776195 * _thisAdjusted = reinterpret_cast<Rect_t3436776195 *>(__this + 1);
	return Rect_get_height_m16400436(_thisAdjusted, method);
}
// System.Void UnityEngine.Rect::set_height(System.Single)
extern "C"  void Rect_set_height_m2188665011 (Rect_t3436776195 * __this, float ___value0, const RuntimeMethod* method)
{
	{
		float L_0 = ___value0;
		__this->set_m_Height_3(L_0);
		return;
	}
}
extern "C"  void Rect_set_height_m2188665011_AdjustorThunk (RuntimeObject * __this, float ___value0, const RuntimeMethod* method)
{
	Rect_t3436776195 * _thisAdjusted = reinterpret_cast<Rect_t3436776195 *>(__this + 1);
	Rect_set_height_m2188665011(_thisAdjusted, ___value0, method);
}
// UnityEngine.Vector2 UnityEngine.Rect::get_size()
extern "C"  Vector2_t3577333262  Rect_get_size_m2976914550 (Rect_t3436776195 * __this, const RuntimeMethod* method)
{
	Vector2_t3577333262  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		float L_0 = __this->get_m_Width_2();
		float L_1 = __this->get_m_Height_3();
		Vector2_t3577333262  L_2;
		memset(&L_2, 0, sizeof(L_2));
		Vector2__ctor_m2914153465((&L_2), L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_0018;
	}

IL_0018:
	{
		Vector2_t3577333262  L_3 = V_0;
		return L_3;
	}
}
extern "C"  Vector2_t3577333262  Rect_get_size_m2976914550_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Rect_t3436776195 * _thisAdjusted = reinterpret_cast<Rect_t3436776195 *>(__this + 1);
	return Rect_get_size_m2976914550(_thisAdjusted, method);
}
// System.Single UnityEngine.Rect::get_xMin()
extern "C"  float Rect_get_xMin_m3183485224 (Rect_t3436776195 * __this, const RuntimeMethod* method)
{
	float V_0 = 0.0f;
	{
		float L_0 = __this->get_m_XMin_0();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		float L_1 = V_0;
		return L_1;
	}
}
extern "C"  float Rect_get_xMin_m3183485224_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Rect_t3436776195 * _thisAdjusted = reinterpret_cast<Rect_t3436776195 *>(__this + 1);
	return Rect_get_xMin_m3183485224(_thisAdjusted, method);
}
// System.Void UnityEngine.Rect::set_xMin(System.Single)
extern "C"  void Rect_set_xMin_m2463982144 (Rect_t3436776195 * __this, float ___value0, const RuntimeMethod* method)
{
	float V_0 = 0.0f;
	{
		float L_0 = Rect_get_xMax_m59812867(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		float L_1 = ___value0;
		__this->set_m_XMin_0(L_1);
		float L_2 = V_0;
		float L_3 = __this->get_m_XMin_0();
		__this->set_m_Width_2(((float)((float)L_2-(float)L_3)));
		return;
	}
}
extern "C"  void Rect_set_xMin_m2463982144_AdjustorThunk (RuntimeObject * __this, float ___value0, const RuntimeMethod* method)
{
	Rect_t3436776195 * _thisAdjusted = reinterpret_cast<Rect_t3436776195 *>(__this + 1);
	Rect_set_xMin_m2463982144(_thisAdjusted, ___value0, method);
}
// System.Single UnityEngine.Rect::get_yMin()
extern "C"  float Rect_get_yMin_m2574684533 (Rect_t3436776195 * __this, const RuntimeMethod* method)
{
	float V_0 = 0.0f;
	{
		float L_0 = __this->get_m_YMin_1();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		float L_1 = V_0;
		return L_1;
	}
}
extern "C"  float Rect_get_yMin_m2574684533_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Rect_t3436776195 * _thisAdjusted = reinterpret_cast<Rect_t3436776195 *>(__this + 1);
	return Rect_get_yMin_m2574684533(_thisAdjusted, method);
}
// System.Void UnityEngine.Rect::set_yMin(System.Single)
extern "C"  void Rect_set_yMin_m3739546123 (Rect_t3436776195 * __this, float ___value0, const RuntimeMethod* method)
{
	float V_0 = 0.0f;
	{
		float L_0 = Rect_get_yMax_m439704772(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		float L_1 = ___value0;
		__this->set_m_YMin_1(L_1);
		float L_2 = V_0;
		float L_3 = __this->get_m_YMin_1();
		__this->set_m_Height_3(((float)((float)L_2-(float)L_3)));
		return;
	}
}
extern "C"  void Rect_set_yMin_m3739546123_AdjustorThunk (RuntimeObject * __this, float ___value0, const RuntimeMethod* method)
{
	Rect_t3436776195 * _thisAdjusted = reinterpret_cast<Rect_t3436776195 *>(__this + 1);
	Rect_set_yMin_m3739546123(_thisAdjusted, ___value0, method);
}
// System.Single UnityEngine.Rect::get_xMax()
extern "C"  float Rect_get_xMax_m59812867 (Rect_t3436776195 * __this, const RuntimeMethod* method)
{
	float V_0 = 0.0f;
	{
		float L_0 = __this->get_m_Width_2();
		float L_1 = __this->get_m_XMin_0();
		V_0 = ((float)((float)L_0+(float)L_1));
		goto IL_0014;
	}

IL_0014:
	{
		float L_2 = V_0;
		return L_2;
	}
}
extern "C"  float Rect_get_xMax_m59812867_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Rect_t3436776195 * _thisAdjusted = reinterpret_cast<Rect_t3436776195 *>(__this + 1);
	return Rect_get_xMax_m59812867(_thisAdjusted, method);
}
// System.Void UnityEngine.Rect::set_xMax(System.Single)
extern "C"  void Rect_set_xMax_m3820368702 (Rect_t3436776195 * __this, float ___value0, const RuntimeMethod* method)
{
	{
		float L_0 = ___value0;
		float L_1 = __this->get_m_XMin_0();
		__this->set_m_Width_2(((float)((float)L_0-(float)L_1)));
		return;
	}
}
extern "C"  void Rect_set_xMax_m3820368702_AdjustorThunk (RuntimeObject * __this, float ___value0, const RuntimeMethod* method)
{
	Rect_t3436776195 * _thisAdjusted = reinterpret_cast<Rect_t3436776195 *>(__this + 1);
	Rect_set_xMax_m3820368702(_thisAdjusted, ___value0, method);
}
// System.Single UnityEngine.Rect::get_yMax()
extern "C"  float Rect_get_yMax_m439704772 (Rect_t3436776195 * __this, const RuntimeMethod* method)
{
	float V_0 = 0.0f;
	{
		float L_0 = __this->get_m_Height_3();
		float L_1 = __this->get_m_YMin_1();
		V_0 = ((float)((float)L_0+(float)L_1));
		goto IL_0014;
	}

IL_0014:
	{
		float L_2 = V_0;
		return L_2;
	}
}
extern "C"  float Rect_get_yMax_m439704772_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Rect_t3436776195 * _thisAdjusted = reinterpret_cast<Rect_t3436776195 *>(__this + 1);
	return Rect_get_yMax_m439704772(_thisAdjusted, method);
}
// System.Void UnityEngine.Rect::set_yMax(System.Single)
extern "C"  void Rect_set_yMax_m3039219194 (Rect_t3436776195 * __this, float ___value0, const RuntimeMethod* method)
{
	{
		float L_0 = ___value0;
		float L_1 = __this->get_m_YMin_1();
		__this->set_m_Height_3(((float)((float)L_0-(float)L_1)));
		return;
	}
}
extern "C"  void Rect_set_yMax_m3039219194_AdjustorThunk (RuntimeObject * __this, float ___value0, const RuntimeMethod* method)
{
	Rect_t3436776195 * _thisAdjusted = reinterpret_cast<Rect_t3436776195 *>(__this + 1);
	Rect_set_yMax_m3039219194(_thisAdjusted, ___value0, method);
}
// System.Boolean UnityEngine.Rect::Contains(UnityEngine.Vector2)
extern "C"  bool Rect_Contains_m2229411356 (Rect_t3436776195 * __this, Vector2_t3577333262  ___point0, const RuntimeMethod* method)
{
	bool V_0 = false;
	int32_t G_B5_0 = 0;
	{
		float L_0 = (&___point0)->get_x_0();
		float L_1 = Rect_get_xMin_m3183485224(__this, /*hidden argument*/NULL);
		if ((!(((float)L_0) >= ((float)L_1))))
		{
			goto IL_0048;
		}
	}
	{
		float L_2 = (&___point0)->get_x_0();
		float L_3 = Rect_get_xMax_m59812867(__this, /*hidden argument*/NULL);
		if ((!(((float)L_2) < ((float)L_3))))
		{
			goto IL_0048;
		}
	}
	{
		float L_4 = (&___point0)->get_y_1();
		float L_5 = Rect_get_yMin_m2574684533(__this, /*hidden argument*/NULL);
		if ((!(((float)L_4) >= ((float)L_5))))
		{
			goto IL_0048;
		}
	}
	{
		float L_6 = (&___point0)->get_y_1();
		float L_7 = Rect_get_yMax_m439704772(__this, /*hidden argument*/NULL);
		G_B5_0 = ((((float)L_6) < ((float)L_7))? 1 : 0);
		goto IL_0049;
	}

IL_0048:
	{
		G_B5_0 = 0;
	}

IL_0049:
	{
		V_0 = (bool)G_B5_0;
		goto IL_004f;
	}

IL_004f:
	{
		bool L_8 = V_0;
		return L_8;
	}
}
extern "C"  bool Rect_Contains_m2229411356_AdjustorThunk (RuntimeObject * __this, Vector2_t3577333262  ___point0, const RuntimeMethod* method)
{
	Rect_t3436776195 * _thisAdjusted = reinterpret_cast<Rect_t3436776195 *>(__this + 1);
	return Rect_Contains_m2229411356(_thisAdjusted, ___point0, method);
}
// System.Boolean UnityEngine.Rect::Contains(UnityEngine.Vector3)
extern "C"  bool Rect_Contains_m3887880031 (Rect_t3436776195 * __this, Vector3_t2903530434  ___point0, const RuntimeMethod* method)
{
	bool V_0 = false;
	int32_t G_B5_0 = 0;
	{
		float L_0 = (&___point0)->get_x_1();
		float L_1 = Rect_get_xMin_m3183485224(__this, /*hidden argument*/NULL);
		if ((!(((float)L_0) >= ((float)L_1))))
		{
			goto IL_0048;
		}
	}
	{
		float L_2 = (&___point0)->get_x_1();
		float L_3 = Rect_get_xMax_m59812867(__this, /*hidden argument*/NULL);
		if ((!(((float)L_2) < ((float)L_3))))
		{
			goto IL_0048;
		}
	}
	{
		float L_4 = (&___point0)->get_y_2();
		float L_5 = Rect_get_yMin_m2574684533(__this, /*hidden argument*/NULL);
		if ((!(((float)L_4) >= ((float)L_5))))
		{
			goto IL_0048;
		}
	}
	{
		float L_6 = (&___point0)->get_y_2();
		float L_7 = Rect_get_yMax_m439704772(__this, /*hidden argument*/NULL);
		G_B5_0 = ((((float)L_6) < ((float)L_7))? 1 : 0);
		goto IL_0049;
	}

IL_0048:
	{
		G_B5_0 = 0;
	}

IL_0049:
	{
		V_0 = (bool)G_B5_0;
		goto IL_004f;
	}

IL_004f:
	{
		bool L_8 = V_0;
		return L_8;
	}
}
extern "C"  bool Rect_Contains_m3887880031_AdjustorThunk (RuntimeObject * __this, Vector3_t2903530434  ___point0, const RuntimeMethod* method)
{
	Rect_t3436776195 * _thisAdjusted = reinterpret_cast<Rect_t3436776195 *>(__this + 1);
	return Rect_Contains_m3887880031(_thisAdjusted, ___point0, method);
}
// UnityEngine.Rect UnityEngine.Rect::OrderMinMax(UnityEngine.Rect)
extern "C"  Rect_t3436776195  Rect_OrderMinMax_m4189518724 (RuntimeObject * __this /* static, unused */, Rect_t3436776195  ___rect0, const RuntimeMethod* method)
{
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	Rect_t3436776195  V_2;
	memset(&V_2, 0, sizeof(V_2));
	{
		float L_0 = Rect_get_xMin_m3183485224((&___rect0), /*hidden argument*/NULL);
		float L_1 = Rect_get_xMax_m59812867((&___rect0), /*hidden argument*/NULL);
		if ((!(((float)L_0) > ((float)L_1))))
		{
			goto IL_0034;
		}
	}
	{
		float L_2 = Rect_get_xMin_m3183485224((&___rect0), /*hidden argument*/NULL);
		V_0 = L_2;
		float L_3 = Rect_get_xMax_m59812867((&___rect0), /*hidden argument*/NULL);
		Rect_set_xMin_m2463982144((&___rect0), L_3, /*hidden argument*/NULL);
		float L_4 = V_0;
		Rect_set_xMax_m3820368702((&___rect0), L_4, /*hidden argument*/NULL);
	}

IL_0034:
	{
		float L_5 = Rect_get_yMin_m2574684533((&___rect0), /*hidden argument*/NULL);
		float L_6 = Rect_get_yMax_m439704772((&___rect0), /*hidden argument*/NULL);
		if ((!(((float)L_5) > ((float)L_6))))
		{
			goto IL_0067;
		}
	}
	{
		float L_7 = Rect_get_yMin_m2574684533((&___rect0), /*hidden argument*/NULL);
		V_1 = L_7;
		float L_8 = Rect_get_yMax_m439704772((&___rect0), /*hidden argument*/NULL);
		Rect_set_yMin_m3739546123((&___rect0), L_8, /*hidden argument*/NULL);
		float L_9 = V_1;
		Rect_set_yMax_m3039219194((&___rect0), L_9, /*hidden argument*/NULL);
	}

IL_0067:
	{
		Rect_t3436776195  L_10 = ___rect0;
		V_2 = L_10;
		goto IL_006e;
	}

IL_006e:
	{
		Rect_t3436776195  L_11 = V_2;
		return L_11;
	}
}
// System.Boolean UnityEngine.Rect::Overlaps(UnityEngine.Rect)
extern "C"  bool Rect_Overlaps_m4279688513 (Rect_t3436776195 * __this, Rect_t3436776195  ___other0, const RuntimeMethod* method)
{
	bool V_0 = false;
	int32_t G_B5_0 = 0;
	{
		float L_0 = Rect_get_xMax_m59812867((&___other0), /*hidden argument*/NULL);
		float L_1 = Rect_get_xMin_m3183485224(__this, /*hidden argument*/NULL);
		if ((!(((float)L_0) > ((float)L_1))))
		{
			goto IL_0048;
		}
	}
	{
		float L_2 = Rect_get_xMin_m3183485224((&___other0), /*hidden argument*/NULL);
		float L_3 = Rect_get_xMax_m59812867(__this, /*hidden argument*/NULL);
		if ((!(((float)L_2) < ((float)L_3))))
		{
			goto IL_0048;
		}
	}
	{
		float L_4 = Rect_get_yMax_m439704772((&___other0), /*hidden argument*/NULL);
		float L_5 = Rect_get_yMin_m2574684533(__this, /*hidden argument*/NULL);
		if ((!(((float)L_4) > ((float)L_5))))
		{
			goto IL_0048;
		}
	}
	{
		float L_6 = Rect_get_yMin_m2574684533((&___other0), /*hidden argument*/NULL);
		float L_7 = Rect_get_yMax_m439704772(__this, /*hidden argument*/NULL);
		G_B5_0 = ((((float)L_6) < ((float)L_7))? 1 : 0);
		goto IL_0049;
	}

IL_0048:
	{
		G_B5_0 = 0;
	}

IL_0049:
	{
		V_0 = (bool)G_B5_0;
		goto IL_004f;
	}

IL_004f:
	{
		bool L_8 = V_0;
		return L_8;
	}
}
extern "C"  bool Rect_Overlaps_m4279688513_AdjustorThunk (RuntimeObject * __this, Rect_t3436776195  ___other0, const RuntimeMethod* method)
{
	Rect_t3436776195 * _thisAdjusted = reinterpret_cast<Rect_t3436776195 *>(__this + 1);
	return Rect_Overlaps_m4279688513(_thisAdjusted, ___other0, method);
}
// System.Boolean UnityEngine.Rect::Overlaps(UnityEngine.Rect,System.Boolean)
extern "C"  bool Rect_Overlaps_m3859656221 (Rect_t3436776195 * __this, Rect_t3436776195  ___other0, bool ___allowInverse1, const RuntimeMethod* method)
{
	Rect_t3436776195  V_0;
	memset(&V_0, 0, sizeof(V_0));
	bool V_1 = false;
	{
		V_0 = (*(Rect_t3436776195 *)__this);
		bool L_0 = ___allowInverse1;
		if (!L_0)
		{
			goto IL_001f;
		}
	}
	{
		Rect_t3436776195  L_1 = V_0;
		Rect_t3436776195  L_2 = Rect_OrderMinMax_m4189518724(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		Rect_t3436776195  L_3 = ___other0;
		Rect_t3436776195  L_4 = Rect_OrderMinMax_m4189518724(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		___other0 = L_4;
	}

IL_001f:
	{
		Rect_t3436776195  L_5 = ___other0;
		bool L_6 = Rect_Overlaps_m4279688513((&V_0), L_5, /*hidden argument*/NULL);
		V_1 = L_6;
		goto IL_002d;
	}

IL_002d:
	{
		bool L_7 = V_1;
		return L_7;
	}
}
extern "C"  bool Rect_Overlaps_m3859656221_AdjustorThunk (RuntimeObject * __this, Rect_t3436776195  ___other0, bool ___allowInverse1, const RuntimeMethod* method)
{
	Rect_t3436776195 * _thisAdjusted = reinterpret_cast<Rect_t3436776195 *>(__this + 1);
	return Rect_Overlaps_m3859656221(_thisAdjusted, ___other0, ___allowInverse1, method);
}
// System.Boolean UnityEngine.Rect::op_Inequality(UnityEngine.Rect,UnityEngine.Rect)
extern "C"  bool Rect_op_Inequality_m4011901724 (RuntimeObject * __this /* static, unused */, Rect_t3436776195  ___lhs0, Rect_t3436776195  ___rhs1, const RuntimeMethod* method)
{
	bool V_0 = false;
	{
		Rect_t3436776195  L_0 = ___lhs0;
		Rect_t3436776195  L_1 = ___rhs1;
		bool L_2 = Rect_op_Equality_m3697585315(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		V_0 = (bool)((((int32_t)L_2) == ((int32_t)0))? 1 : 0);
		goto IL_0011;
	}

IL_0011:
	{
		bool L_3 = V_0;
		return L_3;
	}
}
// System.Boolean UnityEngine.Rect::op_Equality(UnityEngine.Rect,UnityEngine.Rect)
extern "C"  bool Rect_op_Equality_m3697585315 (RuntimeObject * __this /* static, unused */, Rect_t3436776195  ___lhs0, Rect_t3436776195  ___rhs1, const RuntimeMethod* method)
{
	bool V_0 = false;
	int32_t G_B5_0 = 0;
	{
		float L_0 = Rect_get_x_m2895292749((&___lhs0), /*hidden argument*/NULL);
		float L_1 = Rect_get_x_m2895292749((&___rhs1), /*hidden argument*/NULL);
		if ((!(((float)L_0) == ((float)L_1))))
		{
			goto IL_004c;
		}
	}
	{
		float L_2 = Rect_get_y_m782433804((&___lhs0), /*hidden argument*/NULL);
		float L_3 = Rect_get_y_m782433804((&___rhs1), /*hidden argument*/NULL);
		if ((!(((float)L_2) == ((float)L_3))))
		{
			goto IL_004c;
		}
	}
	{
		float L_4 = Rect_get_width_m3471045153((&___lhs0), /*hidden argument*/NULL);
		float L_5 = Rect_get_width_m3471045153((&___rhs1), /*hidden argument*/NULL);
		if ((!(((float)L_4) == ((float)L_5))))
		{
			goto IL_004c;
		}
	}
	{
		float L_6 = Rect_get_height_m16400436((&___lhs0), /*hidden argument*/NULL);
		float L_7 = Rect_get_height_m16400436((&___rhs1), /*hidden argument*/NULL);
		G_B5_0 = ((((float)L_6) == ((float)L_7))? 1 : 0);
		goto IL_004d;
	}

IL_004c:
	{
		G_B5_0 = 0;
	}

IL_004d:
	{
		V_0 = (bool)G_B5_0;
		goto IL_0053;
	}

IL_0053:
	{
		bool L_8 = V_0;
		return L_8;
	}
}
// System.Int32 UnityEngine.Rect::GetHashCode()
extern "C"  int32_t Rect_GetHashCode_m3134604048 (Rect_t3436776195 * __this, const RuntimeMethod* method)
{
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	float V_3 = 0.0f;
	int32_t V_4 = 0;
	{
		float L_0 = Rect_get_x_m2895292749(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		int32_t L_1 = Single_GetHashCode_m2049990008((&V_0), /*hidden argument*/NULL);
		float L_2 = Rect_get_width_m3471045153(__this, /*hidden argument*/NULL);
		V_1 = L_2;
		int32_t L_3 = Single_GetHashCode_m2049990008((&V_1), /*hidden argument*/NULL);
		float L_4 = Rect_get_y_m782433804(__this, /*hidden argument*/NULL);
		V_2 = L_4;
		int32_t L_5 = Single_GetHashCode_m2049990008((&V_2), /*hidden argument*/NULL);
		float L_6 = Rect_get_height_m16400436(__this, /*hidden argument*/NULL);
		V_3 = L_6;
		int32_t L_7 = Single_GetHashCode_m2049990008((&V_3), /*hidden argument*/NULL);
		V_4 = ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_1^(int32_t)((int32_t)((int32_t)L_3<<(int32_t)2))))^(int32_t)((int32_t)((int32_t)L_5>>(int32_t)2))))^(int32_t)((int32_t)((int32_t)L_7>>(int32_t)1))));
		goto IL_0061;
	}

IL_0061:
	{
		int32_t L_8 = V_4;
		return L_8;
	}
}
extern "C"  int32_t Rect_GetHashCode_m3134604048_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Rect_t3436776195 * _thisAdjusted = reinterpret_cast<Rect_t3436776195 *>(__this + 1);
	return Rect_GetHashCode_m3134604048(_thisAdjusted, method);
}
// System.Boolean UnityEngine.Rect::Equals(System.Object)
extern "C"  bool Rect_Equals_m375994694 (Rect_t3436776195 * __this, RuntimeObject * ___other0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Rect_Equals_m375994694_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	Rect_t3436776195  V_1;
	memset(&V_1, 0, sizeof(V_1));
	float V_2 = 0.0f;
	float V_3 = 0.0f;
	float V_4 = 0.0f;
	float V_5 = 0.0f;
	int32_t G_B7_0 = 0;
	{
		RuntimeObject * L_0 = ___other0;
		if (((RuntimeObject *)IsInstSealed((RuntimeObject*)L_0, Rect_t3436776195_il2cpp_TypeInfo_var)))
		{
			goto IL_0013;
		}
	}
	{
		V_0 = (bool)0;
		goto IL_0088;
	}

IL_0013:
	{
		RuntimeObject * L_1 = ___other0;
		V_1 = ((*(Rect_t3436776195 *)((Rect_t3436776195 *)UnBox(L_1, Rect_t3436776195_il2cpp_TypeInfo_var))));
		float L_2 = Rect_get_x_m2895292749(__this, /*hidden argument*/NULL);
		V_2 = L_2;
		float L_3 = Rect_get_x_m2895292749((&V_1), /*hidden argument*/NULL);
		bool L_4 = Single_Equals_m4183737958((&V_2), L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0081;
		}
	}
	{
		float L_5 = Rect_get_y_m782433804(__this, /*hidden argument*/NULL);
		V_3 = L_5;
		float L_6 = Rect_get_y_m782433804((&V_1), /*hidden argument*/NULL);
		bool L_7 = Single_Equals_m4183737958((&V_3), L_6, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0081;
		}
	}
	{
		float L_8 = Rect_get_width_m3471045153(__this, /*hidden argument*/NULL);
		V_4 = L_8;
		float L_9 = Rect_get_width_m3471045153((&V_1), /*hidden argument*/NULL);
		bool L_10 = Single_Equals_m4183737958((&V_4), L_9, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_0081;
		}
	}
	{
		float L_11 = Rect_get_height_m16400436(__this, /*hidden argument*/NULL);
		V_5 = L_11;
		float L_12 = Rect_get_height_m16400436((&V_1), /*hidden argument*/NULL);
		bool L_13 = Single_Equals_m4183737958((&V_5), L_12, /*hidden argument*/NULL);
		G_B7_0 = ((int32_t)(L_13));
		goto IL_0082;
	}

IL_0081:
	{
		G_B7_0 = 0;
	}

IL_0082:
	{
		V_0 = (bool)G_B7_0;
		goto IL_0088;
	}

IL_0088:
	{
		bool L_14 = V_0;
		return L_14;
	}
}
extern "C"  bool Rect_Equals_m375994694_AdjustorThunk (RuntimeObject * __this, RuntimeObject * ___other0, const RuntimeMethod* method)
{
	Rect_t3436776195 * _thisAdjusted = reinterpret_cast<Rect_t3436776195 *>(__this + 1);
	return Rect_Equals_m375994694(_thisAdjusted, ___other0, method);
}
// System.String UnityEngine.Rect::ToString()
extern "C"  String_t* Rect_ToString_m1731978317 (Rect_t3436776195 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Rect_ToString_m1731978317_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	{
		ObjectU5BU5D_t3270211303* L_0 = ((ObjectU5BU5D_t3270211303*)SZArrayNew(ObjectU5BU5D_t3270211303_il2cpp_TypeInfo_var, (uint32_t)4));
		float L_1 = Rect_get_x_m2895292749(__this, /*hidden argument*/NULL);
		float L_2 = L_1;
		RuntimeObject * L_3 = Box(Single_t3045349759_il2cpp_TypeInfo_var, &L_2);
		NullCheck(L_0);
		ArrayElementTypeCheck (L_0, L_3);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (RuntimeObject *)L_3);
		ObjectU5BU5D_t3270211303* L_4 = L_0;
		float L_5 = Rect_get_y_m782433804(__this, /*hidden argument*/NULL);
		float L_6 = L_5;
		RuntimeObject * L_7 = Box(Single_t3045349759_il2cpp_TypeInfo_var, &L_6);
		NullCheck(L_4);
		ArrayElementTypeCheck (L_4, L_7);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(1), (RuntimeObject *)L_7);
		ObjectU5BU5D_t3270211303* L_8 = L_4;
		float L_9 = Rect_get_width_m3471045153(__this, /*hidden argument*/NULL);
		float L_10 = L_9;
		RuntimeObject * L_11 = Box(Single_t3045349759_il2cpp_TypeInfo_var, &L_10);
		NullCheck(L_8);
		ArrayElementTypeCheck (L_8, L_11);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(2), (RuntimeObject *)L_11);
		ObjectU5BU5D_t3270211303* L_12 = L_8;
		float L_13 = Rect_get_height_m16400436(__this, /*hidden argument*/NULL);
		float L_14 = L_13;
		RuntimeObject * L_15 = Box(Single_t3045349759_il2cpp_TypeInfo_var, &L_14);
		NullCheck(L_12);
		ArrayElementTypeCheck (L_12, L_15);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(3), (RuntimeObject *)L_15);
		String_t* L_16 = UnityString_Format_m2401112968(NULL /*static, unused*/, _stringLiteral4073655457, L_12, /*hidden argument*/NULL);
		V_0 = L_16;
		goto IL_004f;
	}

IL_004f:
	{
		String_t* L_17 = V_0;
		return L_17;
	}
}
extern "C"  String_t* Rect_ToString_m1731978317_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Rect_t3436776195 * _thisAdjusted = reinterpret_cast<Rect_t3436776195 *>(__this + 1);
	return Rect_ToString_m1731978317(_thisAdjusted, method);
}
// Conversion methods for marshalling of: UnityEngine.RectOffset
extern "C" void RectOffset_t1315541452_marshal_pinvoke(const RectOffset_t1315541452& unmarshaled, RectOffset_t1315541452_marshaled_pinvoke& marshaled)
{
	marshaled.___m_Ptr_0 = reinterpret_cast<intptr_t>((unmarshaled.get_m_Ptr_0()).get_m_value_0());
	if (unmarshaled.get_m_SourceStyle_1() != NULL)
	{
		if (il2cpp_codegen_is_import_or_windows_runtime(unmarshaled.get_m_SourceStyle_1()))
		{
			il2cpp_hresult_t hr = ((Il2CppComObject *)unmarshaled.get_m_SourceStyle_1())->identity->QueryInterface(Il2CppIUnknown::IID, reinterpret_cast<void**>(&marshaled.___m_SourceStyle_1));
			il2cpp_codegen_com_raise_exception_if_failed(hr, false);
		}
		else
		{
			marshaled.___m_SourceStyle_1 = il2cpp_codegen_com_get_or_create_ccw<Il2CppIUnknown>(unmarshaled.get_m_SourceStyle_1());
		}
	}
	else
	{
		marshaled.___m_SourceStyle_1 = NULL;
	}
}
extern "C" void RectOffset_t1315541452_marshal_pinvoke_back(const RectOffset_t1315541452_marshaled_pinvoke& marshaled, RectOffset_t1315541452& unmarshaled)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RectOffset_t1315541452_pinvoke_FromNativeMethodDefinition_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IntPtr_t unmarshaled_m_Ptr_temp_0;
	memset(&unmarshaled_m_Ptr_temp_0, 0, sizeof(unmarshaled_m_Ptr_temp_0));
	IntPtr_t unmarshaled_m_Ptr_temp_0_temp;
	unmarshaled_m_Ptr_temp_0_temp.set_m_value_0(reinterpret_cast<void*>((intptr_t)(marshaled.___m_Ptr_0)));
	unmarshaled_m_Ptr_temp_0 = unmarshaled_m_Ptr_temp_0_temp;
	unmarshaled.set_m_Ptr_0(unmarshaled_m_Ptr_temp_0);
	if (marshaled.___m_SourceStyle_1 != NULL)
	{
		unmarshaled.set_m_SourceStyle_1(il2cpp_codegen_com_get_or_create_rcw_from_iunknown<RuntimeObject>(marshaled.___m_SourceStyle_1, Il2CppComObject_il2cpp_TypeInfo_var));
	}
	else
	{
		unmarshaled.set_m_SourceStyle_1(NULL);
	}
}
// Conversion method for clean up from marshalling of: UnityEngine.RectOffset
extern "C" void RectOffset_t1315541452_marshal_pinvoke_cleanup(RectOffset_t1315541452_marshaled_pinvoke& marshaled)
{
	if (marshaled.___m_SourceStyle_1 != NULL)
	{
		(marshaled.___m_SourceStyle_1)->Release();
		marshaled.___m_SourceStyle_1 = NULL;
	}
}
// Conversion methods for marshalling of: UnityEngine.RectOffset
extern "C" void RectOffset_t1315541452_marshal_com(const RectOffset_t1315541452& unmarshaled, RectOffset_t1315541452_marshaled_com& marshaled)
{
	marshaled.___m_Ptr_0 = reinterpret_cast<intptr_t>((unmarshaled.get_m_Ptr_0()).get_m_value_0());
	if (unmarshaled.get_m_SourceStyle_1() != NULL)
	{
		if (il2cpp_codegen_is_import_or_windows_runtime(unmarshaled.get_m_SourceStyle_1()))
		{
			il2cpp_hresult_t hr = ((Il2CppComObject *)unmarshaled.get_m_SourceStyle_1())->identity->QueryInterface(Il2CppIUnknown::IID, reinterpret_cast<void**>(&marshaled.___m_SourceStyle_1));
			il2cpp_codegen_com_raise_exception_if_failed(hr, true);
		}
		else
		{
			marshaled.___m_SourceStyle_1 = il2cpp_codegen_com_get_or_create_ccw<Il2CppIUnknown>(unmarshaled.get_m_SourceStyle_1());
		}
	}
	else
	{
		marshaled.___m_SourceStyle_1 = NULL;
	}
}
extern "C" void RectOffset_t1315541452_marshal_com_back(const RectOffset_t1315541452_marshaled_com& marshaled, RectOffset_t1315541452& unmarshaled)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RectOffset_t1315541452_com_FromNativeMethodDefinition_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IntPtr_t unmarshaled_m_Ptr_temp_0;
	memset(&unmarshaled_m_Ptr_temp_0, 0, sizeof(unmarshaled_m_Ptr_temp_0));
	IntPtr_t unmarshaled_m_Ptr_temp_0_temp;
	unmarshaled_m_Ptr_temp_0_temp.set_m_value_0(reinterpret_cast<void*>((intptr_t)(marshaled.___m_Ptr_0)));
	unmarshaled_m_Ptr_temp_0 = unmarshaled_m_Ptr_temp_0_temp;
	unmarshaled.set_m_Ptr_0(unmarshaled_m_Ptr_temp_0);
	if (marshaled.___m_SourceStyle_1 != NULL)
	{
		unmarshaled.set_m_SourceStyle_1(il2cpp_codegen_com_get_or_create_rcw_from_iunknown<RuntimeObject>(marshaled.___m_SourceStyle_1, Il2CppComObject_il2cpp_TypeInfo_var));
	}
	else
	{
		unmarshaled.set_m_SourceStyle_1(NULL);
	}
}
// Conversion method for clean up from marshalling of: UnityEngine.RectOffset
extern "C" void RectOffset_t1315541452_marshal_com_cleanup(RectOffset_t1315541452_marshaled_com& marshaled)
{
	if (marshaled.___m_SourceStyle_1 != NULL)
	{
		(marshaled.___m_SourceStyle_1)->Release();
		marshaled.___m_SourceStyle_1 = NULL;
	}
}
// System.Void UnityEngine.RectOffset::.ctor()
extern "C"  void RectOffset__ctor_m1320241938 (RectOffset_t1315541452 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m2906060009(__this, /*hidden argument*/NULL);
		RectOffset_Init_m3849069081(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.RectOffset::.ctor(System.Object,System.IntPtr)
extern "C"  void RectOffset__ctor_m18644668 (RectOffset_t1315541452 * __this, RuntimeObject * ___sourceStyle0, IntPtr_t ___source1, const RuntimeMethod* method)
{
	{
		Object__ctor_m2906060009(__this, /*hidden argument*/NULL);
		RuntimeObject * L_0 = ___sourceStyle0;
		__this->set_m_SourceStyle_1(L_0);
		IntPtr_t L_1 = ___source1;
		__this->set_m_Ptr_0(L_1);
		return;
	}
}
// System.Void UnityEngine.RectOffset::Init()
extern "C"  void RectOffset_Init_m3849069081 (RectOffset_t1315541452 * __this, const RuntimeMethod* method)
{
	typedef void (*RectOffset_Init_m3849069081_ftn) (RectOffset_t1315541452 *);
	static RectOffset_Init_m3849069081_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectOffset_Init_m3849069081_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectOffset::Init()");
	_il2cpp_icall_func(__this);
}
// System.Void UnityEngine.RectOffset::Cleanup()
extern "C"  void RectOffset_Cleanup_m3041375459 (RectOffset_t1315541452 * __this, const RuntimeMethod* method)
{
	typedef void (*RectOffset_Cleanup_m3041375459_ftn) (RectOffset_t1315541452 *);
	static RectOffset_Cleanup_m3041375459_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectOffset_Cleanup_m3041375459_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectOffset::Cleanup()");
	_il2cpp_icall_func(__this);
}
// System.Int32 UnityEngine.RectOffset::get_left()
extern "C"  int32_t RectOffset_get_left_m643107766 (RectOffset_t1315541452 * __this, const RuntimeMethod* method)
{
	typedef int32_t (*RectOffset_get_left_m643107766_ftn) (RectOffset_t1315541452 *);
	static RectOffset_get_left_m643107766_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectOffset_get_left_m643107766_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectOffset::get_left()");
	int32_t retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// System.Void UnityEngine.RectOffset::set_left(System.Int32)
extern "C"  void RectOffset_set_left_m3991439115 (RectOffset_t1315541452 * __this, int32_t ___value0, const RuntimeMethod* method)
{
	typedef void (*RectOffset_set_left_m3991439115_ftn) (RectOffset_t1315541452 *, int32_t);
	static RectOffset_set_left_m3991439115_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectOffset_set_left_m3991439115_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectOffset::set_left(System.Int32)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Int32 UnityEngine.RectOffset::get_right()
extern "C"  int32_t RectOffset_get_right_m3289664570 (RectOffset_t1315541452 * __this, const RuntimeMethod* method)
{
	typedef int32_t (*RectOffset_get_right_m3289664570_ftn) (RectOffset_t1315541452 *);
	static RectOffset_get_right_m3289664570_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectOffset_get_right_m3289664570_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectOffset::get_right()");
	int32_t retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// System.Void UnityEngine.RectOffset::set_right(System.Int32)
extern "C"  void RectOffset_set_right_m1291085692 (RectOffset_t1315541452 * __this, int32_t ___value0, const RuntimeMethod* method)
{
	typedef void (*RectOffset_set_right_m1291085692_ftn) (RectOffset_t1315541452 *, int32_t);
	static RectOffset_set_right_m1291085692_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectOffset_set_right_m1291085692_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectOffset::set_right(System.Int32)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Int32 UnityEngine.RectOffset::get_top()
extern "C"  int32_t RectOffset_get_top_m1081769127 (RectOffset_t1315541452 * __this, const RuntimeMethod* method)
{
	typedef int32_t (*RectOffset_get_top_m1081769127_ftn) (RectOffset_t1315541452 *);
	static RectOffset_get_top_m1081769127_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectOffset_get_top_m1081769127_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectOffset::get_top()");
	int32_t retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// System.Void UnityEngine.RectOffset::set_top(System.Int32)
extern "C"  void RectOffset_set_top_m4046010662 (RectOffset_t1315541452 * __this, int32_t ___value0, const RuntimeMethod* method)
{
	typedef void (*RectOffset_set_top_m4046010662_ftn) (RectOffset_t1315541452 *, int32_t);
	static RectOffset_set_top_m4046010662_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectOffset_set_top_m4046010662_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectOffset::set_top(System.Int32)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Int32 UnityEngine.RectOffset::get_bottom()
extern "C"  int32_t RectOffset_get_bottom_m2823267338 (RectOffset_t1315541452 * __this, const RuntimeMethod* method)
{
	typedef int32_t (*RectOffset_get_bottom_m2823267338_ftn) (RectOffset_t1315541452 *);
	static RectOffset_get_bottom_m2823267338_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectOffset_get_bottom_m2823267338_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectOffset::get_bottom()");
	int32_t retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// System.Void UnityEngine.RectOffset::set_bottom(System.Int32)
extern "C"  void RectOffset_set_bottom_m1251914031 (RectOffset_t1315541452 * __this, int32_t ___value0, const RuntimeMethod* method)
{
	typedef void (*RectOffset_set_bottom_m1251914031_ftn) (RectOffset_t1315541452 *, int32_t);
	static RectOffset_set_bottom_m1251914031_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectOffset_set_bottom_m1251914031_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectOffset::set_bottom(System.Int32)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Int32 UnityEngine.RectOffset::get_horizontal()
extern "C"  int32_t RectOffset_get_horizontal_m1980673508 (RectOffset_t1315541452 * __this, const RuntimeMethod* method)
{
	typedef int32_t (*RectOffset_get_horizontal_m1980673508_ftn) (RectOffset_t1315541452 *);
	static RectOffset_get_horizontal_m1980673508_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectOffset_get_horizontal_m1980673508_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectOffset::get_horizontal()");
	int32_t retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// System.Int32 UnityEngine.RectOffset::get_vertical()
extern "C"  int32_t RectOffset_get_vertical_m231743444 (RectOffset_t1315541452 * __this, const RuntimeMethod* method)
{
	typedef int32_t (*RectOffset_get_vertical_m231743444_ftn) (RectOffset_t1315541452 *);
	static RectOffset_get_vertical_m231743444_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectOffset_get_vertical_m231743444_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectOffset::get_vertical()");
	int32_t retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// System.Void UnityEngine.RectOffset::Finalize()
extern "C"  void RectOffset_Finalize_m577286498 (RectOffset_t1315541452 * __this, const RuntimeMethod* method)
{
	Exception_t1975590229 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1975590229 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
	}

IL_0001:
	try
	{ // begin try (depth: 1)
		{
			RuntimeObject * L_0 = __this->get_m_SourceStyle_1();
			if (L_0)
			{
				goto IL_0012;
			}
		}

IL_000c:
		{
			RectOffset_Cleanup_m3041375459(__this, /*hidden argument*/NULL);
		}

IL_0012:
		{
			IL2CPP_LEAVE(0x1E, FINALLY_0017);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1975590229 *)e.ex;
		goto FINALLY_0017;
	}

FINALLY_0017:
	{ // begin finally (depth: 1)
		Object_Finalize_m2705829212(__this, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(23)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(23)
	{
		IL2CPP_JUMP_TBL(0x1E, IL_001e)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1975590229 *)
	}

IL_001e:
	{
		return;
	}
}
// System.String UnityEngine.RectOffset::ToString()
extern "C"  String_t* RectOffset_ToString_m108828025 (RectOffset_t1315541452 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RectOffset_ToString_m108828025_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	{
		ObjectU5BU5D_t3270211303* L_0 = ((ObjectU5BU5D_t3270211303*)SZArrayNew(ObjectU5BU5D_t3270211303_il2cpp_TypeInfo_var, (uint32_t)4));
		int32_t L_1 = RectOffset_get_left_m643107766(__this, /*hidden argument*/NULL);
		int32_t L_2 = L_1;
		RuntimeObject * L_3 = Box(Int32_t4237944589_il2cpp_TypeInfo_var, &L_2);
		NullCheck(L_0);
		ArrayElementTypeCheck (L_0, L_3);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (RuntimeObject *)L_3);
		ObjectU5BU5D_t3270211303* L_4 = L_0;
		int32_t L_5 = RectOffset_get_right_m3289664570(__this, /*hidden argument*/NULL);
		int32_t L_6 = L_5;
		RuntimeObject * L_7 = Box(Int32_t4237944589_il2cpp_TypeInfo_var, &L_6);
		NullCheck(L_4);
		ArrayElementTypeCheck (L_4, L_7);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(1), (RuntimeObject *)L_7);
		ObjectU5BU5D_t3270211303* L_8 = L_4;
		int32_t L_9 = RectOffset_get_top_m1081769127(__this, /*hidden argument*/NULL);
		int32_t L_10 = L_9;
		RuntimeObject * L_11 = Box(Int32_t4237944589_il2cpp_TypeInfo_var, &L_10);
		NullCheck(L_8);
		ArrayElementTypeCheck (L_8, L_11);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(2), (RuntimeObject *)L_11);
		ObjectU5BU5D_t3270211303* L_12 = L_8;
		int32_t L_13 = RectOffset_get_bottom_m2823267338(__this, /*hidden argument*/NULL);
		int32_t L_14 = L_13;
		RuntimeObject * L_15 = Box(Int32_t4237944589_il2cpp_TypeInfo_var, &L_14);
		NullCheck(L_12);
		ArrayElementTypeCheck (L_12, L_15);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(3), (RuntimeObject *)L_15);
		String_t* L_16 = UnityString_Format_m2401112968(NULL /*static, unused*/, _stringLiteral2841341723, L_12, /*hidden argument*/NULL);
		V_0 = L_16;
		goto IL_004f;
	}

IL_004f:
	{
		String_t* L_17 = V_0;
		return L_17;
	}
}
// UnityEngine.Rect UnityEngine.RectTransform::get_rect()
extern "C"  Rect_t3436776195  RectTransform_get_rect_m3374483472 (RectTransform_t3962994911 * __this, const RuntimeMethod* method)
{
	Rect_t3436776195  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Rect_t3436776195  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		RectTransform_INTERNAL_get_rect_m752273476(__this, (&V_0), /*hidden argument*/NULL);
		Rect_t3436776195  L_0 = V_0;
		V_1 = L_0;
		goto IL_0010;
	}

IL_0010:
	{
		Rect_t3436776195  L_1 = V_1;
		return L_1;
	}
}
// System.Void UnityEngine.RectTransform::INTERNAL_get_rect(UnityEngine.Rect&)
extern "C"  void RectTransform_INTERNAL_get_rect_m752273476 (RectTransform_t3962994911 * __this, Rect_t3436776195 * ___value0, const RuntimeMethod* method)
{
	typedef void (*RectTransform_INTERNAL_get_rect_m752273476_ftn) (RectTransform_t3962994911 *, Rect_t3436776195 *);
	static RectTransform_INTERNAL_get_rect_m752273476_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectTransform_INTERNAL_get_rect_m752273476_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectTransform::INTERNAL_get_rect(UnityEngine.Rect&)");
	_il2cpp_icall_func(__this, ___value0);
}
// UnityEngine.Vector2 UnityEngine.RectTransform::get_anchorMin()
extern "C"  Vector2_t3577333262  RectTransform_get_anchorMin_m2315738783 (RectTransform_t3962994911 * __this, const RuntimeMethod* method)
{
	Vector2_t3577333262  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector2_t3577333262  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		RectTransform_INTERNAL_get_anchorMin_m740746116(__this, (&V_0), /*hidden argument*/NULL);
		Vector2_t3577333262  L_0 = V_0;
		V_1 = L_0;
		goto IL_0010;
	}

IL_0010:
	{
		Vector2_t3577333262  L_1 = V_1;
		return L_1;
	}
}
// System.Void UnityEngine.RectTransform::set_anchorMin(UnityEngine.Vector2)
extern "C"  void RectTransform_set_anchorMin_m1073640259 (RectTransform_t3962994911 * __this, Vector2_t3577333262  ___value0, const RuntimeMethod* method)
{
	{
		RectTransform_INTERNAL_set_anchorMin_m4106319920(__this, (&___value0), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.RectTransform::INTERNAL_get_anchorMin(UnityEngine.Vector2&)
extern "C"  void RectTransform_INTERNAL_get_anchorMin_m740746116 (RectTransform_t3962994911 * __this, Vector2_t3577333262 * ___value0, const RuntimeMethod* method)
{
	typedef void (*RectTransform_INTERNAL_get_anchorMin_m740746116_ftn) (RectTransform_t3962994911 *, Vector2_t3577333262 *);
	static RectTransform_INTERNAL_get_anchorMin_m740746116_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectTransform_INTERNAL_get_anchorMin_m740746116_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectTransform::INTERNAL_get_anchorMin(UnityEngine.Vector2&)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.RectTransform::INTERNAL_set_anchorMin(UnityEngine.Vector2&)
extern "C"  void RectTransform_INTERNAL_set_anchorMin_m4106319920 (RectTransform_t3962994911 * __this, Vector2_t3577333262 * ___value0, const RuntimeMethod* method)
{
	typedef void (*RectTransform_INTERNAL_set_anchorMin_m4106319920_ftn) (RectTransform_t3962994911 *, Vector2_t3577333262 *);
	static RectTransform_INTERNAL_set_anchorMin_m4106319920_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectTransform_INTERNAL_set_anchorMin_m4106319920_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectTransform::INTERNAL_set_anchorMin(UnityEngine.Vector2&)");
	_il2cpp_icall_func(__this, ___value0);
}
// UnityEngine.Vector2 UnityEngine.RectTransform::get_anchorMax()
extern "C"  Vector2_t3577333262  RectTransform_get_anchorMax_m2215014239 (RectTransform_t3962994911 * __this, const RuntimeMethod* method)
{
	Vector2_t3577333262  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector2_t3577333262  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		RectTransform_INTERNAL_get_anchorMax_m1541525480(__this, (&V_0), /*hidden argument*/NULL);
		Vector2_t3577333262  L_0 = V_0;
		V_1 = L_0;
		goto IL_0010;
	}

IL_0010:
	{
		Vector2_t3577333262  L_1 = V_1;
		return L_1;
	}
}
// System.Void UnityEngine.RectTransform::set_anchorMax(UnityEngine.Vector2)
extern "C"  void RectTransform_set_anchorMax_m1901263563 (RectTransform_t3962994911 * __this, Vector2_t3577333262  ___value0, const RuntimeMethod* method)
{
	{
		RectTransform_INTERNAL_set_anchorMax_m2865678990(__this, (&___value0), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.RectTransform::INTERNAL_get_anchorMax(UnityEngine.Vector2&)
extern "C"  void RectTransform_INTERNAL_get_anchorMax_m1541525480 (RectTransform_t3962994911 * __this, Vector2_t3577333262 * ___value0, const RuntimeMethod* method)
{
	typedef void (*RectTransform_INTERNAL_get_anchorMax_m1541525480_ftn) (RectTransform_t3962994911 *, Vector2_t3577333262 *);
	static RectTransform_INTERNAL_get_anchorMax_m1541525480_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectTransform_INTERNAL_get_anchorMax_m1541525480_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectTransform::INTERNAL_get_anchorMax(UnityEngine.Vector2&)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.RectTransform::INTERNAL_set_anchorMax(UnityEngine.Vector2&)
extern "C"  void RectTransform_INTERNAL_set_anchorMax_m2865678990 (RectTransform_t3962994911 * __this, Vector2_t3577333262 * ___value0, const RuntimeMethod* method)
{
	typedef void (*RectTransform_INTERNAL_set_anchorMax_m2865678990_ftn) (RectTransform_t3962994911 *, Vector2_t3577333262 *);
	static RectTransform_INTERNAL_set_anchorMax_m2865678990_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectTransform_INTERNAL_set_anchorMax_m2865678990_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectTransform::INTERNAL_set_anchorMax(UnityEngine.Vector2&)");
	_il2cpp_icall_func(__this, ___value0);
}
// UnityEngine.Vector2 UnityEngine.RectTransform::get_anchoredPosition()
extern "C"  Vector2_t3577333262  RectTransform_get_anchoredPosition_m1269783566 (RectTransform_t3962994911 * __this, const RuntimeMethod* method)
{
	Vector2_t3577333262  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector2_t3577333262  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		RectTransform_INTERNAL_get_anchoredPosition_m1013122670(__this, (&V_0), /*hidden argument*/NULL);
		Vector2_t3577333262  L_0 = V_0;
		V_1 = L_0;
		goto IL_0010;
	}

IL_0010:
	{
		Vector2_t3577333262  L_1 = V_1;
		return L_1;
	}
}
// System.Void UnityEngine.RectTransform::set_anchoredPosition(UnityEngine.Vector2)
extern "C"  void RectTransform_set_anchoredPosition_m3723613293 (RectTransform_t3962994911 * __this, Vector2_t3577333262  ___value0, const RuntimeMethod* method)
{
	{
		RectTransform_INTERNAL_set_anchoredPosition_m3369370392(__this, (&___value0), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.RectTransform::INTERNAL_get_anchoredPosition(UnityEngine.Vector2&)
extern "C"  void RectTransform_INTERNAL_get_anchoredPosition_m1013122670 (RectTransform_t3962994911 * __this, Vector2_t3577333262 * ___value0, const RuntimeMethod* method)
{
	typedef void (*RectTransform_INTERNAL_get_anchoredPosition_m1013122670_ftn) (RectTransform_t3962994911 *, Vector2_t3577333262 *);
	static RectTransform_INTERNAL_get_anchoredPosition_m1013122670_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectTransform_INTERNAL_get_anchoredPosition_m1013122670_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectTransform::INTERNAL_get_anchoredPosition(UnityEngine.Vector2&)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.RectTransform::INTERNAL_set_anchoredPosition(UnityEngine.Vector2&)
extern "C"  void RectTransform_INTERNAL_set_anchoredPosition_m3369370392 (RectTransform_t3962994911 * __this, Vector2_t3577333262 * ___value0, const RuntimeMethod* method)
{
	typedef void (*RectTransform_INTERNAL_set_anchoredPosition_m3369370392_ftn) (RectTransform_t3962994911 *, Vector2_t3577333262 *);
	static RectTransform_INTERNAL_set_anchoredPosition_m3369370392_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectTransform_INTERNAL_set_anchoredPosition_m3369370392_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectTransform::INTERNAL_set_anchoredPosition(UnityEngine.Vector2&)");
	_il2cpp_icall_func(__this, ___value0);
}
// UnityEngine.Vector2 UnityEngine.RectTransform::get_sizeDelta()
extern "C"  Vector2_t3577333262  RectTransform_get_sizeDelta_m2500894686 (RectTransform_t3962994911 * __this, const RuntimeMethod* method)
{
	Vector2_t3577333262  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector2_t3577333262  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		RectTransform_INTERNAL_get_sizeDelta_m263664391(__this, (&V_0), /*hidden argument*/NULL);
		Vector2_t3577333262  L_0 = V_0;
		V_1 = L_0;
		goto IL_0010;
	}

IL_0010:
	{
		Vector2_t3577333262  L_1 = V_1;
		return L_1;
	}
}
// System.Void UnityEngine.RectTransform::set_sizeDelta(UnityEngine.Vector2)
extern "C"  void RectTransform_set_sizeDelta_m1072091878 (RectTransform_t3962994911 * __this, Vector2_t3577333262  ___value0, const RuntimeMethod* method)
{
	{
		RectTransform_INTERNAL_set_sizeDelta_m3207534479(__this, (&___value0), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.RectTransform::INTERNAL_get_sizeDelta(UnityEngine.Vector2&)
extern "C"  void RectTransform_INTERNAL_get_sizeDelta_m263664391 (RectTransform_t3962994911 * __this, Vector2_t3577333262 * ___value0, const RuntimeMethod* method)
{
	typedef void (*RectTransform_INTERNAL_get_sizeDelta_m263664391_ftn) (RectTransform_t3962994911 *, Vector2_t3577333262 *);
	static RectTransform_INTERNAL_get_sizeDelta_m263664391_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectTransform_INTERNAL_get_sizeDelta_m263664391_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectTransform::INTERNAL_get_sizeDelta(UnityEngine.Vector2&)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.RectTransform::INTERNAL_set_sizeDelta(UnityEngine.Vector2&)
extern "C"  void RectTransform_INTERNAL_set_sizeDelta_m3207534479 (RectTransform_t3962994911 * __this, Vector2_t3577333262 * ___value0, const RuntimeMethod* method)
{
	typedef void (*RectTransform_INTERNAL_set_sizeDelta_m3207534479_ftn) (RectTransform_t3962994911 *, Vector2_t3577333262 *);
	static RectTransform_INTERNAL_set_sizeDelta_m3207534479_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectTransform_INTERNAL_set_sizeDelta_m3207534479_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectTransform::INTERNAL_set_sizeDelta(UnityEngine.Vector2&)");
	_il2cpp_icall_func(__this, ___value0);
}
// UnityEngine.Vector2 UnityEngine.RectTransform::get_pivot()
extern "C"  Vector2_t3577333262  RectTransform_get_pivot_m115984824 (RectTransform_t3962994911 * __this, const RuntimeMethod* method)
{
	Vector2_t3577333262  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector2_t3577333262  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		RectTransform_INTERNAL_get_pivot_m3673734192(__this, (&V_0), /*hidden argument*/NULL);
		Vector2_t3577333262  L_0 = V_0;
		V_1 = L_0;
		goto IL_0010;
	}

IL_0010:
	{
		Vector2_t3577333262  L_1 = V_1;
		return L_1;
	}
}
// System.Void UnityEngine.RectTransform::set_pivot(UnityEngine.Vector2)
extern "C"  void RectTransform_set_pivot_m815166536 (RectTransform_t3962994911 * __this, Vector2_t3577333262  ___value0, const RuntimeMethod* method)
{
	{
		RectTransform_INTERNAL_set_pivot_m3445627362(__this, (&___value0), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.RectTransform::INTERNAL_get_pivot(UnityEngine.Vector2&)
extern "C"  void RectTransform_INTERNAL_get_pivot_m3673734192 (RectTransform_t3962994911 * __this, Vector2_t3577333262 * ___value0, const RuntimeMethod* method)
{
	typedef void (*RectTransform_INTERNAL_get_pivot_m3673734192_ftn) (RectTransform_t3962994911 *, Vector2_t3577333262 *);
	static RectTransform_INTERNAL_get_pivot_m3673734192_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectTransform_INTERNAL_get_pivot_m3673734192_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectTransform::INTERNAL_get_pivot(UnityEngine.Vector2&)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.RectTransform::INTERNAL_set_pivot(UnityEngine.Vector2&)
extern "C"  void RectTransform_INTERNAL_set_pivot_m3445627362 (RectTransform_t3962994911 * __this, Vector2_t3577333262 * ___value0, const RuntimeMethod* method)
{
	typedef void (*RectTransform_INTERNAL_set_pivot_m3445627362_ftn) (RectTransform_t3962994911 *, Vector2_t3577333262 *);
	static RectTransform_INTERNAL_set_pivot_m3445627362_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectTransform_INTERNAL_set_pivot_m3445627362_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectTransform::INTERNAL_set_pivot(UnityEngine.Vector2&)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.RectTransform::add_reapplyDrivenProperties(UnityEngine.RectTransform/ReapplyDrivenProperties)
extern "C"  void RectTransform_add_reapplyDrivenProperties_m3435041181 (RuntimeObject * __this /* static, unused */, ReapplyDrivenProperties_t565817470 * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RectTransform_add_reapplyDrivenProperties_m3435041181_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ReapplyDrivenProperties_t565817470 * V_0 = NULL;
	ReapplyDrivenProperties_t565817470 * V_1 = NULL;
	{
		ReapplyDrivenProperties_t565817470 * L_0 = ((RectTransform_t3962994911_StaticFields*)il2cpp_codegen_static_fields_for(RectTransform_t3962994911_il2cpp_TypeInfo_var))->get_reapplyDrivenProperties_2();
		V_0 = L_0;
	}

IL_0006:
	{
		ReapplyDrivenProperties_t565817470 * L_1 = V_0;
		V_1 = L_1;
		ReapplyDrivenProperties_t565817470 * L_2 = V_1;
		ReapplyDrivenProperties_t565817470 * L_3 = ___value0;
		Delegate_t537306192 * L_4 = Delegate_Combine_m408928526(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		ReapplyDrivenProperties_t565817470 * L_5 = V_0;
		ReapplyDrivenProperties_t565817470 * L_6 = InterlockedCompareExchangeImpl<ReapplyDrivenProperties_t565817470 *>((((RectTransform_t3962994911_StaticFields*)il2cpp_codegen_static_fields_for(RectTransform_t3962994911_il2cpp_TypeInfo_var))->get_address_of_reapplyDrivenProperties_2()), ((ReapplyDrivenProperties_t565817470 *)CastclassSealed((RuntimeObject*)L_4, ReapplyDrivenProperties_t565817470_il2cpp_TypeInfo_var)), L_5);
		V_0 = L_6;
		ReapplyDrivenProperties_t565817470 * L_7 = V_0;
		ReapplyDrivenProperties_t565817470 * L_8 = V_1;
		if ((!(((RuntimeObject*)(ReapplyDrivenProperties_t565817470 *)L_7) == ((RuntimeObject*)(ReapplyDrivenProperties_t565817470 *)L_8))))
		{
			goto IL_0006;
		}
	}
	{
		return;
	}
}
// System.Void UnityEngine.RectTransform::remove_reapplyDrivenProperties(UnityEngine.RectTransform/ReapplyDrivenProperties)
extern "C"  void RectTransform_remove_reapplyDrivenProperties_m3549895845 (RuntimeObject * __this /* static, unused */, ReapplyDrivenProperties_t565817470 * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RectTransform_remove_reapplyDrivenProperties_m3549895845_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ReapplyDrivenProperties_t565817470 * V_0 = NULL;
	ReapplyDrivenProperties_t565817470 * V_1 = NULL;
	{
		ReapplyDrivenProperties_t565817470 * L_0 = ((RectTransform_t3962994911_StaticFields*)il2cpp_codegen_static_fields_for(RectTransform_t3962994911_il2cpp_TypeInfo_var))->get_reapplyDrivenProperties_2();
		V_0 = L_0;
	}

IL_0006:
	{
		ReapplyDrivenProperties_t565817470 * L_1 = V_0;
		V_1 = L_1;
		ReapplyDrivenProperties_t565817470 * L_2 = V_1;
		ReapplyDrivenProperties_t565817470 * L_3 = ___value0;
		Delegate_t537306192 * L_4 = Delegate_Remove_m2220107885(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		ReapplyDrivenProperties_t565817470 * L_5 = V_0;
		ReapplyDrivenProperties_t565817470 * L_6 = InterlockedCompareExchangeImpl<ReapplyDrivenProperties_t565817470 *>((((RectTransform_t3962994911_StaticFields*)il2cpp_codegen_static_fields_for(RectTransform_t3962994911_il2cpp_TypeInfo_var))->get_address_of_reapplyDrivenProperties_2()), ((ReapplyDrivenProperties_t565817470 *)CastclassSealed((RuntimeObject*)L_4, ReapplyDrivenProperties_t565817470_il2cpp_TypeInfo_var)), L_5);
		V_0 = L_6;
		ReapplyDrivenProperties_t565817470 * L_7 = V_0;
		ReapplyDrivenProperties_t565817470 * L_8 = V_1;
		if ((!(((RuntimeObject*)(ReapplyDrivenProperties_t565817470 *)L_7) == ((RuntimeObject*)(ReapplyDrivenProperties_t565817470 *)L_8))))
		{
			goto IL_0006;
		}
	}
	{
		return;
	}
}
// System.Void UnityEngine.RectTransform::SendReapplyDrivenProperties(UnityEngine.RectTransform)
extern "C"  void RectTransform_SendReapplyDrivenProperties_m1201286970 (RuntimeObject * __this /* static, unused */, RectTransform_t3962994911 * ___driven0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RectTransform_SendReapplyDrivenProperties_m1201286970_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		ReapplyDrivenProperties_t565817470 * L_0 = ((RectTransform_t3962994911_StaticFields*)il2cpp_codegen_static_fields_for(RectTransform_t3962994911_il2cpp_TypeInfo_var))->get_reapplyDrivenProperties_2();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		ReapplyDrivenProperties_t565817470 * L_1 = ((RectTransform_t3962994911_StaticFields*)il2cpp_codegen_static_fields_for(RectTransform_t3962994911_il2cpp_TypeInfo_var))->get_reapplyDrivenProperties_2();
		RectTransform_t3962994911 * L_2 = ___driven0;
		NullCheck(L_1);
		ReapplyDrivenProperties_Invoke_m4155422273(L_1, L_2, /*hidden argument*/NULL);
	}

IL_0016:
	{
		return;
	}
}
// System.Void UnityEngine.RectTransform::GetLocalCorners(UnityEngine.Vector3[])
extern "C"  void RectTransform_GetLocalCorners_m1896055443 (RectTransform_t3962994911 * __this, Vector3U5BU5D_t981680887* ___fourCornersArray0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RectTransform_GetLocalCorners_m1896055443_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Rect_t3436776195  V_0;
	memset(&V_0, 0, sizeof(V_0));
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	float V_3 = 0.0f;
	float V_4 = 0.0f;
	{
		Vector3U5BU5D_t981680887* L_0 = ___fourCornersArray0;
		if (!L_0)
		{
			goto IL_0010;
		}
	}
	{
		Vector3U5BU5D_t981680887* L_1 = ___fourCornersArray0;
		NullCheck(L_1);
		if ((((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_1)->max_length))))) >= ((int32_t)4)))
		{
			goto IL_0020;
		}
	}

IL_0010:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3525713211_il2cpp_TypeInfo_var);
		Debug_LogError_m1848790000(NULL /*static, unused*/, _stringLiteral4219001008, /*hidden argument*/NULL);
		goto IL_00aa;
	}

IL_0020:
	{
		Rect_t3436776195  L_2 = RectTransform_get_rect_m3374483472(__this, /*hidden argument*/NULL);
		V_0 = L_2;
		float L_3 = Rect_get_x_m2895292749((&V_0), /*hidden argument*/NULL);
		V_1 = L_3;
		float L_4 = Rect_get_y_m782433804((&V_0), /*hidden argument*/NULL);
		V_2 = L_4;
		float L_5 = Rect_get_xMax_m59812867((&V_0), /*hidden argument*/NULL);
		V_3 = L_5;
		float L_6 = Rect_get_yMax_m439704772((&V_0), /*hidden argument*/NULL);
		V_4 = L_6;
		Vector3U5BU5D_t981680887* L_7 = ___fourCornersArray0;
		NullCheck(L_7);
		float L_8 = V_1;
		float L_9 = V_2;
		Vector3_t2903530434  L_10;
		memset(&L_10, 0, sizeof(L_10));
		Vector3__ctor_m4053220054((&L_10), L_8, L_9, (0.0f), /*hidden argument*/NULL);
		*(Vector3_t2903530434 *)((L_7)->GetAddressAt(static_cast<il2cpp_array_size_t>(0))) = L_10;
		Vector3U5BU5D_t981680887* L_11 = ___fourCornersArray0;
		NullCheck(L_11);
		float L_12 = V_1;
		float L_13 = V_4;
		Vector3_t2903530434  L_14;
		memset(&L_14, 0, sizeof(L_14));
		Vector3__ctor_m4053220054((&L_14), L_12, L_13, (0.0f), /*hidden argument*/NULL);
		*(Vector3_t2903530434 *)((L_11)->GetAddressAt(static_cast<il2cpp_array_size_t>(1))) = L_14;
		Vector3U5BU5D_t981680887* L_15 = ___fourCornersArray0;
		NullCheck(L_15);
		float L_16 = V_3;
		float L_17 = V_4;
		Vector3_t2903530434  L_18;
		memset(&L_18, 0, sizeof(L_18));
		Vector3__ctor_m4053220054((&L_18), L_16, L_17, (0.0f), /*hidden argument*/NULL);
		*(Vector3_t2903530434 *)((L_15)->GetAddressAt(static_cast<il2cpp_array_size_t>(2))) = L_18;
		Vector3U5BU5D_t981680887* L_19 = ___fourCornersArray0;
		NullCheck(L_19);
		float L_20 = V_3;
		float L_21 = V_2;
		Vector3_t2903530434  L_22;
		memset(&L_22, 0, sizeof(L_22));
		Vector3__ctor_m4053220054((&L_22), L_20, L_21, (0.0f), /*hidden argument*/NULL);
		*(Vector3_t2903530434 *)((L_19)->GetAddressAt(static_cast<il2cpp_array_size_t>(3))) = L_22;
	}

IL_00aa:
	{
		return;
	}
}
// System.Void UnityEngine.RectTransform::GetWorldCorners(UnityEngine.Vector3[])
extern "C"  void RectTransform_GetWorldCorners_m2181990217 (RectTransform_t3962994911 * __this, Vector3U5BU5D_t981680887* ___fourCornersArray0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RectTransform_GetWorldCorners_m2181990217_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Transform_t3316442598 * V_0 = NULL;
	int32_t V_1 = 0;
	{
		Vector3U5BU5D_t981680887* L_0 = ___fourCornersArray0;
		if (!L_0)
		{
			goto IL_0010;
		}
	}
	{
		Vector3U5BU5D_t981680887* L_1 = ___fourCornersArray0;
		NullCheck(L_1);
		if ((((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_1)->max_length))))) >= ((int32_t)4)))
		{
			goto IL_0020;
		}
	}

IL_0010:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3525713211_il2cpp_TypeInfo_var);
		Debug_LogError_m1848790000(NULL /*static, unused*/, _stringLiteral3917415373, /*hidden argument*/NULL);
		goto IL_005e;
	}

IL_0020:
	{
		Vector3U5BU5D_t981680887* L_2 = ___fourCornersArray0;
		RectTransform_GetLocalCorners_m1896055443(__this, L_2, /*hidden argument*/NULL);
		Transform_t3316442598 * L_3 = Component_get_transform_m2038396632(__this, /*hidden argument*/NULL);
		V_0 = L_3;
		V_1 = 0;
		goto IL_0057;
	}

IL_0035:
	{
		Vector3U5BU5D_t981680887* L_4 = ___fourCornersArray0;
		int32_t L_5 = V_1;
		NullCheck(L_4);
		Transform_t3316442598 * L_6 = V_0;
		Vector3U5BU5D_t981680887* L_7 = ___fourCornersArray0;
		int32_t L_8 = V_1;
		NullCheck(L_7);
		NullCheck(L_6);
		Vector3_t2903530434  L_9 = Transform_TransformPoint_m2770276166(L_6, (*(Vector3_t2903530434 *)((L_7)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_8)))), /*hidden argument*/NULL);
		*(Vector3_t2903530434 *)((L_4)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_5))) = L_9;
		int32_t L_10 = V_1;
		V_1 = ((int32_t)((int32_t)L_10+(int32_t)1));
	}

IL_0057:
	{
		int32_t L_11 = V_1;
		if ((((int32_t)L_11) < ((int32_t)4)))
		{
			goto IL_0035;
		}
	}

IL_005e:
	{
		return;
	}
}
// System.Void UnityEngine.RectTransform::set_offsetMin(UnityEngine.Vector2)
extern "C"  void RectTransform_set_offsetMin_m4052016319 (RectTransform_t3962994911 * __this, Vector2_t3577333262  ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RectTransform_set_offsetMin_m4052016319_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector2_t3577333262  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Vector2_t3577333262  L_0 = ___value0;
		Vector2_t3577333262  L_1 = RectTransform_get_anchoredPosition_m1269783566(__this, /*hidden argument*/NULL);
		Vector2_t3577333262  L_2 = RectTransform_get_sizeDelta_m2500894686(__this, /*hidden argument*/NULL);
		Vector2_t3577333262  L_3 = RectTransform_get_pivot_m115984824(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_t3577333262_il2cpp_TypeInfo_var);
		Vector2_t3577333262  L_4 = Vector2_Scale_m1829241572(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		Vector2_t3577333262  L_5 = Vector2_op_Subtraction_m1470240788(NULL /*static, unused*/, L_1, L_4, /*hidden argument*/NULL);
		Vector2_t3577333262  L_6 = Vector2_op_Subtraction_m1470240788(NULL /*static, unused*/, L_0, L_5, /*hidden argument*/NULL);
		V_0 = L_6;
		Vector2_t3577333262  L_7 = RectTransform_get_sizeDelta_m2500894686(__this, /*hidden argument*/NULL);
		Vector2_t3577333262  L_8 = V_0;
		Vector2_t3577333262  L_9 = Vector2_op_Subtraction_m1470240788(NULL /*static, unused*/, L_7, L_8, /*hidden argument*/NULL);
		RectTransform_set_sizeDelta_m1072091878(__this, L_9, /*hidden argument*/NULL);
		Vector2_t3577333262  L_10 = RectTransform_get_anchoredPosition_m1269783566(__this, /*hidden argument*/NULL);
		Vector2_t3577333262  L_11 = V_0;
		Vector2_t3577333262  L_12 = Vector2_get_one_m4238015175(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector2_t3577333262  L_13 = RectTransform_get_pivot_m115984824(__this, /*hidden argument*/NULL);
		Vector2_t3577333262  L_14 = Vector2_op_Subtraction_m1470240788(NULL /*static, unused*/, L_12, L_13, /*hidden argument*/NULL);
		Vector2_t3577333262  L_15 = Vector2_Scale_m1829241572(NULL /*static, unused*/, L_11, L_14, /*hidden argument*/NULL);
		Vector2_t3577333262  L_16 = Vector2_op_Addition_m2696862189(NULL /*static, unused*/, L_10, L_15, /*hidden argument*/NULL);
		RectTransform_set_anchoredPosition_m3723613293(__this, L_16, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.RectTransform::set_offsetMax(UnityEngine.Vector2)
extern "C"  void RectTransform_set_offsetMax_m1291583879 (RectTransform_t3962994911 * __this, Vector2_t3577333262  ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RectTransform_set_offsetMax_m1291583879_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector2_t3577333262  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Vector2_t3577333262  L_0 = ___value0;
		Vector2_t3577333262  L_1 = RectTransform_get_anchoredPosition_m1269783566(__this, /*hidden argument*/NULL);
		Vector2_t3577333262  L_2 = RectTransform_get_sizeDelta_m2500894686(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_t3577333262_il2cpp_TypeInfo_var);
		Vector2_t3577333262  L_3 = Vector2_get_one_m4238015175(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector2_t3577333262  L_4 = RectTransform_get_pivot_m115984824(__this, /*hidden argument*/NULL);
		Vector2_t3577333262  L_5 = Vector2_op_Subtraction_m1470240788(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		Vector2_t3577333262  L_6 = Vector2_Scale_m1829241572(NULL /*static, unused*/, L_2, L_5, /*hidden argument*/NULL);
		Vector2_t3577333262  L_7 = Vector2_op_Addition_m2696862189(NULL /*static, unused*/, L_1, L_6, /*hidden argument*/NULL);
		Vector2_t3577333262  L_8 = Vector2_op_Subtraction_m1470240788(NULL /*static, unused*/, L_0, L_7, /*hidden argument*/NULL);
		V_0 = L_8;
		Vector2_t3577333262  L_9 = RectTransform_get_sizeDelta_m2500894686(__this, /*hidden argument*/NULL);
		Vector2_t3577333262  L_10 = V_0;
		Vector2_t3577333262  L_11 = Vector2_op_Addition_m2696862189(NULL /*static, unused*/, L_9, L_10, /*hidden argument*/NULL);
		RectTransform_set_sizeDelta_m1072091878(__this, L_11, /*hidden argument*/NULL);
		Vector2_t3577333262  L_12 = RectTransform_get_anchoredPosition_m1269783566(__this, /*hidden argument*/NULL);
		Vector2_t3577333262  L_13 = V_0;
		Vector2_t3577333262  L_14 = RectTransform_get_pivot_m115984824(__this, /*hidden argument*/NULL);
		Vector2_t3577333262  L_15 = Vector2_Scale_m1829241572(NULL /*static, unused*/, L_13, L_14, /*hidden argument*/NULL);
		Vector2_t3577333262  L_16 = Vector2_op_Addition_m2696862189(NULL /*static, unused*/, L_12, L_15, /*hidden argument*/NULL);
		RectTransform_set_anchoredPosition_m3723613293(__this, L_16, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.RectTransform::SetInsetAndSizeFromParentEdge(UnityEngine.RectTransform/Edge,System.Single,System.Single)
extern "C"  void RectTransform_SetInsetAndSizeFromParentEdge_m922333336 (RectTransform_t3962994911 * __this, int32_t ___edge0, float ___inset1, float ___size2, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	bool V_1 = false;
	float V_2 = 0.0f;
	Vector2_t3577333262  V_3;
	memset(&V_3, 0, sizeof(V_3));
	Vector2_t3577333262  V_4;
	memset(&V_4, 0, sizeof(V_4));
	Vector2_t3577333262  V_5;
	memset(&V_5, 0, sizeof(V_5));
	Vector2_t3577333262  V_6;
	memset(&V_6, 0, sizeof(V_6));
	Vector2_t3577333262  V_7;
	memset(&V_7, 0, sizeof(V_7));
	int32_t G_B4_0 = 0;
	int32_t G_B7_0 = 0;
	int32_t G_B10_0 = 0;
	int32_t G_B12_0 = 0;
	Vector2_t3577333262 * G_B12_1 = NULL;
	int32_t G_B11_0 = 0;
	Vector2_t3577333262 * G_B11_1 = NULL;
	float G_B13_0 = 0.0f;
	int32_t G_B13_1 = 0;
	Vector2_t3577333262 * G_B13_2 = NULL;
	{
		int32_t L_0 = ___edge0;
		if ((((int32_t)L_0) == ((int32_t)2)))
		{
			goto IL_000f;
		}
	}
	{
		int32_t L_1 = ___edge0;
		if ((!(((uint32_t)L_1) == ((uint32_t)3))))
		{
			goto IL_0015;
		}
	}

IL_000f:
	{
		G_B4_0 = 1;
		goto IL_0016;
	}

IL_0015:
	{
		G_B4_0 = 0;
	}

IL_0016:
	{
		V_0 = G_B4_0;
		int32_t L_2 = ___edge0;
		if ((((int32_t)L_2) == ((int32_t)2)))
		{
			goto IL_0024;
		}
	}
	{
		int32_t L_3 = ___edge0;
		G_B7_0 = ((((int32_t)L_3) == ((int32_t)1))? 1 : 0);
		goto IL_0025;
	}

IL_0024:
	{
		G_B7_0 = 1;
	}

IL_0025:
	{
		V_1 = (bool)G_B7_0;
		bool L_4 = V_1;
		if (!L_4)
		{
			goto IL_0032;
		}
	}
	{
		G_B10_0 = 1;
		goto IL_0033;
	}

IL_0032:
	{
		G_B10_0 = 0;
	}

IL_0033:
	{
		V_2 = (((float)((float)G_B10_0)));
		Vector2_t3577333262  L_5 = RectTransform_get_anchorMin_m2315738783(__this, /*hidden argument*/NULL);
		V_3 = L_5;
		int32_t L_6 = V_0;
		float L_7 = V_2;
		Vector2_set_Item_m140300536((&V_3), L_6, L_7, /*hidden argument*/NULL);
		Vector2_t3577333262  L_8 = V_3;
		RectTransform_set_anchorMin_m1073640259(__this, L_8, /*hidden argument*/NULL);
		Vector2_t3577333262  L_9 = RectTransform_get_anchorMax_m2215014239(__this, /*hidden argument*/NULL);
		V_3 = L_9;
		int32_t L_10 = V_0;
		float L_11 = V_2;
		Vector2_set_Item_m140300536((&V_3), L_10, L_11, /*hidden argument*/NULL);
		Vector2_t3577333262  L_12 = V_3;
		RectTransform_set_anchorMax_m1901263563(__this, L_12, /*hidden argument*/NULL);
		Vector2_t3577333262  L_13 = RectTransform_get_sizeDelta_m2500894686(__this, /*hidden argument*/NULL);
		V_4 = L_13;
		int32_t L_14 = V_0;
		float L_15 = ___size2;
		Vector2_set_Item_m140300536((&V_4), L_14, L_15, /*hidden argument*/NULL);
		Vector2_t3577333262  L_16 = V_4;
		RectTransform_set_sizeDelta_m1072091878(__this, L_16, /*hidden argument*/NULL);
		Vector2_t3577333262  L_17 = RectTransform_get_anchoredPosition_m1269783566(__this, /*hidden argument*/NULL);
		V_5 = L_17;
		int32_t L_18 = V_0;
		bool L_19 = V_1;
		G_B11_0 = L_18;
		G_B11_1 = (&V_5);
		if (!L_19)
		{
			G_B12_0 = L_18;
			G_B12_1 = (&V_5);
			goto IL_00ad;
		}
	}
	{
		float L_20 = ___inset1;
		float L_21 = ___size2;
		Vector2_t3577333262  L_22 = RectTransform_get_pivot_m115984824(__this, /*hidden argument*/NULL);
		V_6 = L_22;
		int32_t L_23 = V_0;
		float L_24 = Vector2_get_Item_m373581785((&V_6), L_23, /*hidden argument*/NULL);
		G_B13_0 = ((float)((float)((-L_20))-(float)((float)((float)L_21*(float)((float)((float)(1.0f)-(float)L_24))))));
		G_B13_1 = G_B11_0;
		G_B13_2 = G_B11_1;
		goto IL_00c1;
	}

IL_00ad:
	{
		float L_25 = ___inset1;
		float L_26 = ___size2;
		Vector2_t3577333262  L_27 = RectTransform_get_pivot_m115984824(__this, /*hidden argument*/NULL);
		V_7 = L_27;
		int32_t L_28 = V_0;
		float L_29 = Vector2_get_Item_m373581785((&V_7), L_28, /*hidden argument*/NULL);
		G_B13_0 = ((float)((float)L_25+(float)((float)((float)L_26*(float)L_29))));
		G_B13_1 = G_B12_0;
		G_B13_2 = G_B12_1;
	}

IL_00c1:
	{
		Vector2_set_Item_m140300536(G_B13_2, G_B13_1, G_B13_0, /*hidden argument*/NULL);
		Vector2_t3577333262  L_30 = V_5;
		RectTransform_set_anchoredPosition_m3723613293(__this, L_30, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.RectTransform::SetSizeWithCurrentAnchors(UnityEngine.RectTransform/Axis,System.Single)
extern "C"  void RectTransform_SetSizeWithCurrentAnchors_m1701981155 (RectTransform_t3962994911 * __this, int32_t ___axis0, float ___size1, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	Vector2_t3577333262  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Vector2_t3577333262  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Vector2_t3577333262  V_3;
	memset(&V_3, 0, sizeof(V_3));
	Vector2_t3577333262  V_4;
	memset(&V_4, 0, sizeof(V_4));
	{
		int32_t L_0 = ___axis0;
		V_0 = L_0;
		Vector2_t3577333262  L_1 = RectTransform_get_sizeDelta_m2500894686(__this, /*hidden argument*/NULL);
		V_1 = L_1;
		int32_t L_2 = V_0;
		float L_3 = ___size1;
		Vector2_t3577333262  L_4 = RectTransform_GetParentSize_m3415414519(__this, /*hidden argument*/NULL);
		V_2 = L_4;
		int32_t L_5 = V_0;
		float L_6 = Vector2_get_Item_m373581785((&V_2), L_5, /*hidden argument*/NULL);
		Vector2_t3577333262  L_7 = RectTransform_get_anchorMax_m2215014239(__this, /*hidden argument*/NULL);
		V_3 = L_7;
		int32_t L_8 = V_0;
		float L_9 = Vector2_get_Item_m373581785((&V_3), L_8, /*hidden argument*/NULL);
		Vector2_t3577333262  L_10 = RectTransform_get_anchorMin_m2315738783(__this, /*hidden argument*/NULL);
		V_4 = L_10;
		int32_t L_11 = V_0;
		float L_12 = Vector2_get_Item_m373581785((&V_4), L_11, /*hidden argument*/NULL);
		Vector2_set_Item_m140300536((&V_1), L_2, ((float)((float)L_3-(float)((float)((float)L_6*(float)((float)((float)L_9-(float)L_12)))))), /*hidden argument*/NULL);
		Vector2_t3577333262  L_13 = V_1;
		RectTransform_set_sizeDelta_m1072091878(__this, L_13, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Vector2 UnityEngine.RectTransform::GetParentSize()
extern "C"  Vector2_t3577333262  RectTransform_GetParentSize_m3415414519 (RectTransform_t3962994911 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RectTransform_GetParentSize_m3415414519_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RectTransform_t3962994911 * V_0 = NULL;
	Vector2_t3577333262  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Rect_t3436776195  V_2;
	memset(&V_2, 0, sizeof(V_2));
	{
		Transform_t3316442598 * L_0 = Transform_get_parent_m273289079(__this, /*hidden argument*/NULL);
		V_0 = ((RectTransform_t3962994911 *)IsInstSealed((RuntimeObject*)L_0, RectTransform_t3962994911_il2cpp_TypeInfo_var));
		RectTransform_t3962994911 * L_1 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1502412432_il2cpp_TypeInfo_var);
		bool L_2 = Object_op_Implicit_m2314733868(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		if (L_2)
		{
			goto IL_0023;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_t3577333262_il2cpp_TypeInfo_var);
		Vector2_t3577333262  L_3 = Vector2_get_zero_m4146031368(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_1 = L_3;
		goto IL_0037;
	}

IL_0023:
	{
		RectTransform_t3962994911 * L_4 = V_0;
		NullCheck(L_4);
		Rect_t3436776195  L_5 = RectTransform_get_rect_m3374483472(L_4, /*hidden argument*/NULL);
		V_2 = L_5;
		Vector2_t3577333262  L_6 = Rect_get_size_m2976914550((&V_2), /*hidden argument*/NULL);
		V_1 = L_6;
		goto IL_0037;
	}

IL_0037:
	{
		Vector2_t3577333262  L_7 = V_1;
		return L_7;
	}
}
// System.Void UnityEngine.RectTransform/ReapplyDrivenProperties::.ctor(System.Object,System.IntPtr)
extern "C"  void ReapplyDrivenProperties__ctor_m1218376403 (ReapplyDrivenProperties_t565817470 * __this, RuntimeObject * ___object0, IntPtr_t ___method1, const RuntimeMethod* method)
{
	__this->set_method_ptr_0(il2cpp_codegen_get_method_pointer((RuntimeMethod*)___method1.get_m_value_0()));
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UnityEngine.RectTransform/ReapplyDrivenProperties::Invoke(UnityEngine.RectTransform)
extern "C"  void ReapplyDrivenProperties_Invoke_m4155422273 (ReapplyDrivenProperties_t565817470 * __this, RectTransform_t3962994911 * ___driven0, const RuntimeMethod* method)
{
	if(__this->get_prev_9() != NULL)
	{
		ReapplyDrivenProperties_Invoke_m4155422273((ReapplyDrivenProperties_t565817470 *)__this->get_prev_9(),___driven0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (RuntimeObject *, void* __this, RectTransform_t3962994911 * ___driven0, const RuntimeMethod* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___driven0,(RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (void* __this, RectTransform_t3962994911 * ___driven0, const RuntimeMethod* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___driven0,(RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, const RuntimeMethod* method);
		((FunctionPointerType)__this->get_method_ptr_0())(___driven0,(RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult UnityEngine.RectTransform/ReapplyDrivenProperties::BeginInvoke(UnityEngine.RectTransform,System.AsyncCallback,System.Object)
extern "C"  RuntimeObject* ReapplyDrivenProperties_BeginInvoke_m338801554 (ReapplyDrivenProperties_t565817470 * __this, RectTransform_t3962994911 * ___driven0, AsyncCallback_t1057242265 * ___callback1, RuntimeObject * ___object2, const RuntimeMethod* method)
{
	void *__d_args[2] = {0};
	__d_args[0] = ___driven0;
	return (RuntimeObject*)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (RuntimeObject*)___object2);
}
// System.Void UnityEngine.RectTransform/ReapplyDrivenProperties::EndInvoke(System.IAsyncResult)
extern "C"  void ReapplyDrivenProperties_EndInvoke_m2136662013 (ReapplyDrivenProperties_t565817470 * __this, RuntimeObject* ___result0, const RuntimeMethod* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Boolean UnityEngine.RectTransformUtility::ScreenPointToWorldPointInRectangle(UnityEngine.RectTransform,UnityEngine.Vector2,UnityEngine.Camera,UnityEngine.Vector3&)
extern "C"  bool RectTransformUtility_ScreenPointToWorldPointInRectangle_m4112832593 (RuntimeObject * __this /* static, unused */, RectTransform_t3962994911 * ___rect0, Vector2_t3577333262  ___screenPoint1, Camera_t3175186167 * ___cam2, Vector3_t2903530434 * ___worldPoint3, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RectTransformUtility_ScreenPointToWorldPointInRectangle_m4112832593_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Ray_t3821377119  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Plane_t2300588969  V_1;
	memset(&V_1, 0, sizeof(V_1));
	float V_2 = 0.0f;
	bool V_3 = false;
	{
		Vector3_t2903530434 * L_0 = ___worldPoint3;
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_t3577333262_il2cpp_TypeInfo_var);
		Vector2_t3577333262  L_1 = Vector2_get_zero_m4146031368(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t2903530434  L_2 = Vector2_op_Implicit_m3445183035(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		*(Vector3_t2903530434 *)L_0 = L_2;
		Camera_t3175186167 * L_3 = ___cam2;
		Vector2_t3577333262  L_4 = ___screenPoint1;
		IL2CPP_RUNTIME_CLASS_INIT(RectTransformUtility_t535428424_il2cpp_TypeInfo_var);
		Ray_t3821377119  L_5 = RectTransformUtility_ScreenPointToRay_m339743857(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		V_0 = L_5;
		RectTransform_t3962994911 * L_6 = ___rect0;
		NullCheck(L_6);
		Quaternion_t754065749  L_7 = Transform_get_rotation_m2679542584(L_6, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t2903530434_il2cpp_TypeInfo_var);
		Vector3_t2903530434  L_8 = Vector3_get_back_m440222366(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Quaternion_t754065749_il2cpp_TypeInfo_var);
		Vector3_t2903530434  L_9 = Quaternion_op_Multiply_m1825091092(NULL /*static, unused*/, L_7, L_8, /*hidden argument*/NULL);
		RectTransform_t3962994911 * L_10 = ___rect0;
		NullCheck(L_10);
		Vector3_t2903530434  L_11 = Transform_get_position_m1291471697(L_10, /*hidden argument*/NULL);
		Plane__ctor_m2922395188((&V_1), L_9, L_11, /*hidden argument*/NULL);
		Ray_t3821377119  L_12 = V_0;
		bool L_13 = Plane_Raycast_m235007613((&V_1), L_12, (&V_2), /*hidden argument*/NULL);
		if (L_13)
		{
			goto IL_004c;
		}
	}
	{
		V_3 = (bool)0;
		goto IL_0061;
	}

IL_004c:
	{
		Vector3_t2903530434 * L_14 = ___worldPoint3;
		float L_15 = V_2;
		Vector3_t2903530434  L_16 = Ray_GetPoint_m3472514115((&V_0), L_15, /*hidden argument*/NULL);
		*(Vector3_t2903530434 *)L_14 = L_16;
		V_3 = (bool)1;
		goto IL_0061;
	}

IL_0061:
	{
		bool L_17 = V_3;
		return L_17;
	}
}
// System.Boolean UnityEngine.RectTransformUtility::ScreenPointToLocalPointInRectangle(UnityEngine.RectTransform,UnityEngine.Vector2,UnityEngine.Camera,UnityEngine.Vector2&)
extern "C"  bool RectTransformUtility_ScreenPointToLocalPointInRectangle_m4033027037 (RuntimeObject * __this /* static, unused */, RectTransform_t3962994911 * ___rect0, Vector2_t3577333262  ___screenPoint1, Camera_t3175186167 * ___cam2, Vector2_t3577333262 * ___localPoint3, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RectTransformUtility_ScreenPointToLocalPointInRectangle_m4033027037_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t2903530434  V_0;
	memset(&V_0, 0, sizeof(V_0));
	bool V_1 = false;
	{
		Vector2_t3577333262 * L_0 = ___localPoint3;
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_t3577333262_il2cpp_TypeInfo_var);
		Vector2_t3577333262  L_1 = Vector2_get_zero_m4146031368(NULL /*static, unused*/, /*hidden argument*/NULL);
		*(Vector2_t3577333262 *)L_0 = L_1;
		RectTransform_t3962994911 * L_2 = ___rect0;
		Vector2_t3577333262  L_3 = ___screenPoint1;
		Camera_t3175186167 * L_4 = ___cam2;
		IL2CPP_RUNTIME_CLASS_INIT(RectTransformUtility_t535428424_il2cpp_TypeInfo_var);
		bool L_5 = RectTransformUtility_ScreenPointToWorldPointInRectangle_m4112832593(NULL /*static, unused*/, L_2, L_3, L_4, (&V_0), /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0035;
		}
	}
	{
		Vector2_t3577333262 * L_6 = ___localPoint3;
		RectTransform_t3962994911 * L_7 = ___rect0;
		Vector3_t2903530434  L_8 = V_0;
		NullCheck(L_7);
		Vector3_t2903530434  L_9 = Transform_InverseTransformPoint_m573231499(L_7, L_8, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_t3577333262_il2cpp_TypeInfo_var);
		Vector2_t3577333262  L_10 = Vector2_op_Implicit_m214149158(NULL /*static, unused*/, L_9, /*hidden argument*/NULL);
		*(Vector2_t3577333262 *)L_6 = L_10;
		V_1 = (bool)1;
		goto IL_003c;
	}

IL_0035:
	{
		V_1 = (bool)0;
		goto IL_003c;
	}

IL_003c:
	{
		bool L_11 = V_1;
		return L_11;
	}
}
// UnityEngine.Ray UnityEngine.RectTransformUtility::ScreenPointToRay(UnityEngine.Camera,UnityEngine.Vector2)
extern "C"  Ray_t3821377119  RectTransformUtility_ScreenPointToRay_m339743857 (RuntimeObject * __this /* static, unused */, Camera_t3175186167 * ___cam0, Vector2_t3577333262  ___screenPos1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RectTransformUtility_ScreenPointToRay_m339743857_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Ray_t3821377119  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector3_t2903530434  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		Camera_t3175186167 * L_0 = ___cam0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1502412432_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m1918954159(NULL /*static, unused*/, L_0, (Object_t1502412432 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001f;
		}
	}
	{
		Camera_t3175186167 * L_2 = ___cam0;
		Vector2_t3577333262  L_3 = ___screenPos1;
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_t3577333262_il2cpp_TypeInfo_var);
		Vector3_t2903530434  L_4 = Vector2_op_Implicit_m3445183035(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		NullCheck(L_2);
		Ray_t3821377119  L_5 = Camera_ScreenPointToRay_m3401639554(L_2, L_4, /*hidden argument*/NULL);
		V_0 = L_5;
		goto IL_004a;
	}

IL_001f:
	{
		Vector2_t3577333262  L_6 = ___screenPos1;
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_t3577333262_il2cpp_TypeInfo_var);
		Vector3_t2903530434  L_7 = Vector2_op_Implicit_m3445183035(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		V_1 = L_7;
		Vector3_t2903530434 * L_8 = (&V_1);
		float L_9 = L_8->get_z_3();
		L_8->set_z_3(((float)((float)L_9-(float)(100.0f))));
		Vector3_t2903530434  L_10 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t2903530434_il2cpp_TypeInfo_var);
		Vector3_t2903530434  L_11 = Vector3_get_forward_m2156180237(NULL /*static, unused*/, /*hidden argument*/NULL);
		Ray_t3821377119  L_12;
		memset(&L_12, 0, sizeof(L_12));
		Ray__ctor_m1620751038((&L_12), L_10, L_11, /*hidden argument*/NULL);
		V_0 = L_12;
		goto IL_004a;
	}

IL_004a:
	{
		Ray_t3821377119  L_13 = V_0;
		return L_13;
	}
}
// System.Void UnityEngine.RectTransformUtility::FlipLayoutOnAxis(UnityEngine.RectTransform,System.Int32,System.Boolean,System.Boolean)
extern "C"  void RectTransformUtility_FlipLayoutOnAxis_m1442291361 (RuntimeObject * __this /* static, unused */, RectTransform_t3962994911 * ___rect0, int32_t ___axis1, bool ___keepPositioning2, bool ___recursive3, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RectTransformUtility_FlipLayoutOnAxis_m1442291361_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	RectTransform_t3962994911 * V_1 = NULL;
	Vector2_t3577333262  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Vector2_t3577333262  V_3;
	memset(&V_3, 0, sizeof(V_3));
	Vector2_t3577333262  V_4;
	memset(&V_4, 0, sizeof(V_4));
	Vector2_t3577333262  V_5;
	memset(&V_5, 0, sizeof(V_5));
	float V_6 = 0.0f;
	{
		RectTransform_t3962994911 * L_0 = ___rect0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1502412432_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_m2342070775(NULL /*static, unused*/, L_0, (Object_t1502412432 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0012;
		}
	}
	{
		goto IL_00f3;
	}

IL_0012:
	{
		bool L_2 = ___recursive3;
		if (!L_2)
		{
			goto IL_0055;
		}
	}
	{
		V_0 = 0;
		goto IL_0048;
	}

IL_0020:
	{
		RectTransform_t3962994911 * L_3 = ___rect0;
		int32_t L_4 = V_0;
		NullCheck(L_3);
		Transform_t3316442598 * L_5 = Transform_GetChild_m357825681(L_3, L_4, /*hidden argument*/NULL);
		V_1 = ((RectTransform_t3962994911 *)IsInstSealed((RuntimeObject*)L_5, RectTransform_t3962994911_il2cpp_TypeInfo_var));
		RectTransform_t3962994911 * L_6 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1502412432_il2cpp_TypeInfo_var);
		bool L_7 = Object_op_Inequality_m1918954159(NULL /*static, unused*/, L_6, (Object_t1502412432 *)NULL, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0043;
		}
	}
	{
		RectTransform_t3962994911 * L_8 = V_1;
		int32_t L_9 = ___axis1;
		IL2CPP_RUNTIME_CLASS_INIT(RectTransformUtility_t535428424_il2cpp_TypeInfo_var);
		RectTransformUtility_FlipLayoutOnAxis_m1442291361(NULL /*static, unused*/, L_8, L_9, (bool)0, (bool)1, /*hidden argument*/NULL);
	}

IL_0043:
	{
		int32_t L_10 = V_0;
		V_0 = ((int32_t)((int32_t)L_10+(int32_t)1));
	}

IL_0048:
	{
		int32_t L_11 = V_0;
		RectTransform_t3962994911 * L_12 = ___rect0;
		NullCheck(L_12);
		int32_t L_13 = Transform_get_childCount_m1083485410(L_12, /*hidden argument*/NULL);
		if ((((int32_t)L_11) < ((int32_t)L_13)))
		{
			goto IL_0020;
		}
	}
	{
	}

IL_0055:
	{
		RectTransform_t3962994911 * L_14 = ___rect0;
		NullCheck(L_14);
		Vector2_t3577333262  L_15 = RectTransform_get_pivot_m115984824(L_14, /*hidden argument*/NULL);
		V_2 = L_15;
		int32_t L_16 = ___axis1;
		int32_t L_17 = ___axis1;
		float L_18 = Vector2_get_Item_m373581785((&V_2), L_17, /*hidden argument*/NULL);
		Vector2_set_Item_m140300536((&V_2), L_16, ((float)((float)(1.0f)-(float)L_18)), /*hidden argument*/NULL);
		RectTransform_t3962994911 * L_19 = ___rect0;
		Vector2_t3577333262  L_20 = V_2;
		NullCheck(L_19);
		RectTransform_set_pivot_m815166536(L_19, L_20, /*hidden argument*/NULL);
		bool L_21 = ___keepPositioning2;
		if (!L_21)
		{
			goto IL_0084;
		}
	}
	{
		goto IL_00f3;
	}

IL_0084:
	{
		RectTransform_t3962994911 * L_22 = ___rect0;
		NullCheck(L_22);
		Vector2_t3577333262  L_23 = RectTransform_get_anchoredPosition_m1269783566(L_22, /*hidden argument*/NULL);
		V_3 = L_23;
		int32_t L_24 = ___axis1;
		int32_t L_25 = ___axis1;
		float L_26 = Vector2_get_Item_m373581785((&V_3), L_25, /*hidden argument*/NULL);
		Vector2_set_Item_m140300536((&V_3), L_24, ((-L_26)), /*hidden argument*/NULL);
		RectTransform_t3962994911 * L_27 = ___rect0;
		Vector2_t3577333262  L_28 = V_3;
		NullCheck(L_27);
		RectTransform_set_anchoredPosition_m3723613293(L_27, L_28, /*hidden argument*/NULL);
		RectTransform_t3962994911 * L_29 = ___rect0;
		NullCheck(L_29);
		Vector2_t3577333262  L_30 = RectTransform_get_anchorMin_m2315738783(L_29, /*hidden argument*/NULL);
		V_4 = L_30;
		RectTransform_t3962994911 * L_31 = ___rect0;
		NullCheck(L_31);
		Vector2_t3577333262  L_32 = RectTransform_get_anchorMax_m2215014239(L_31, /*hidden argument*/NULL);
		V_5 = L_32;
		int32_t L_33 = ___axis1;
		float L_34 = Vector2_get_Item_m373581785((&V_4), L_33, /*hidden argument*/NULL);
		V_6 = L_34;
		int32_t L_35 = ___axis1;
		int32_t L_36 = ___axis1;
		float L_37 = Vector2_get_Item_m373581785((&V_5), L_36, /*hidden argument*/NULL);
		Vector2_set_Item_m140300536((&V_4), L_35, ((float)((float)(1.0f)-(float)L_37)), /*hidden argument*/NULL);
		int32_t L_38 = ___axis1;
		float L_39 = V_6;
		Vector2_set_Item_m140300536((&V_5), L_38, ((float)((float)(1.0f)-(float)L_39)), /*hidden argument*/NULL);
		RectTransform_t3962994911 * L_40 = ___rect0;
		Vector2_t3577333262  L_41 = V_4;
		NullCheck(L_40);
		RectTransform_set_anchorMin_m1073640259(L_40, L_41, /*hidden argument*/NULL);
		RectTransform_t3962994911 * L_42 = ___rect0;
		Vector2_t3577333262  L_43 = V_5;
		NullCheck(L_42);
		RectTransform_set_anchorMax_m1901263563(L_42, L_43, /*hidden argument*/NULL);
	}

IL_00f3:
	{
		return;
	}
}
// System.Void UnityEngine.RectTransformUtility::FlipLayoutAxes(UnityEngine.RectTransform,System.Boolean,System.Boolean)
extern "C"  void RectTransformUtility_FlipLayoutAxes_m225875364 (RuntimeObject * __this /* static, unused */, RectTransform_t3962994911 * ___rect0, bool ___keepPositioning1, bool ___recursive2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RectTransformUtility_FlipLayoutAxes_m225875364_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	RectTransform_t3962994911 * V_1 = NULL;
	{
		RectTransform_t3962994911 * L_0 = ___rect0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1502412432_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_m2342070775(NULL /*static, unused*/, L_0, (Object_t1502412432 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0012;
		}
	}
	{
		goto IL_00b4;
	}

IL_0012:
	{
		bool L_2 = ___recursive2;
		if (!L_2)
		{
			goto IL_0054;
		}
	}
	{
		V_0 = 0;
		goto IL_0047;
	}

IL_0020:
	{
		RectTransform_t3962994911 * L_3 = ___rect0;
		int32_t L_4 = V_0;
		NullCheck(L_3);
		Transform_t3316442598 * L_5 = Transform_GetChild_m357825681(L_3, L_4, /*hidden argument*/NULL);
		V_1 = ((RectTransform_t3962994911 *)IsInstSealed((RuntimeObject*)L_5, RectTransform_t3962994911_il2cpp_TypeInfo_var));
		RectTransform_t3962994911 * L_6 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1502412432_il2cpp_TypeInfo_var);
		bool L_7 = Object_op_Inequality_m1918954159(NULL /*static, unused*/, L_6, (Object_t1502412432 *)NULL, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0042;
		}
	}
	{
		RectTransform_t3962994911 * L_8 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(RectTransformUtility_t535428424_il2cpp_TypeInfo_var);
		RectTransformUtility_FlipLayoutAxes_m225875364(NULL /*static, unused*/, L_8, (bool)0, (bool)1, /*hidden argument*/NULL);
	}

IL_0042:
	{
		int32_t L_9 = V_0;
		V_0 = ((int32_t)((int32_t)L_9+(int32_t)1));
	}

IL_0047:
	{
		int32_t L_10 = V_0;
		RectTransform_t3962994911 * L_11 = ___rect0;
		NullCheck(L_11);
		int32_t L_12 = Transform_get_childCount_m1083485410(L_11, /*hidden argument*/NULL);
		if ((((int32_t)L_10) < ((int32_t)L_12)))
		{
			goto IL_0020;
		}
	}
	{
	}

IL_0054:
	{
		RectTransform_t3962994911 * L_13 = ___rect0;
		RectTransform_t3962994911 * L_14 = ___rect0;
		NullCheck(L_14);
		Vector2_t3577333262  L_15 = RectTransform_get_pivot_m115984824(L_14, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(RectTransformUtility_t535428424_il2cpp_TypeInfo_var);
		Vector2_t3577333262  L_16 = RectTransformUtility_GetTransposed_m3431509790(NULL /*static, unused*/, L_15, /*hidden argument*/NULL);
		NullCheck(L_13);
		RectTransform_set_pivot_m815166536(L_13, L_16, /*hidden argument*/NULL);
		RectTransform_t3962994911 * L_17 = ___rect0;
		RectTransform_t3962994911 * L_18 = ___rect0;
		NullCheck(L_18);
		Vector2_t3577333262  L_19 = RectTransform_get_sizeDelta_m2500894686(L_18, /*hidden argument*/NULL);
		Vector2_t3577333262  L_20 = RectTransformUtility_GetTransposed_m3431509790(NULL /*static, unused*/, L_19, /*hidden argument*/NULL);
		NullCheck(L_17);
		RectTransform_set_sizeDelta_m1072091878(L_17, L_20, /*hidden argument*/NULL);
		bool L_21 = ___keepPositioning1;
		if (!L_21)
		{
			goto IL_0081;
		}
	}
	{
		goto IL_00b4;
	}

IL_0081:
	{
		RectTransform_t3962994911 * L_22 = ___rect0;
		RectTransform_t3962994911 * L_23 = ___rect0;
		NullCheck(L_23);
		Vector2_t3577333262  L_24 = RectTransform_get_anchoredPosition_m1269783566(L_23, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(RectTransformUtility_t535428424_il2cpp_TypeInfo_var);
		Vector2_t3577333262  L_25 = RectTransformUtility_GetTransposed_m3431509790(NULL /*static, unused*/, L_24, /*hidden argument*/NULL);
		NullCheck(L_22);
		RectTransform_set_anchoredPosition_m3723613293(L_22, L_25, /*hidden argument*/NULL);
		RectTransform_t3962994911 * L_26 = ___rect0;
		RectTransform_t3962994911 * L_27 = ___rect0;
		NullCheck(L_27);
		Vector2_t3577333262  L_28 = RectTransform_get_anchorMin_m2315738783(L_27, /*hidden argument*/NULL);
		Vector2_t3577333262  L_29 = RectTransformUtility_GetTransposed_m3431509790(NULL /*static, unused*/, L_28, /*hidden argument*/NULL);
		NullCheck(L_26);
		RectTransform_set_anchorMin_m1073640259(L_26, L_29, /*hidden argument*/NULL);
		RectTransform_t3962994911 * L_30 = ___rect0;
		RectTransform_t3962994911 * L_31 = ___rect0;
		NullCheck(L_31);
		Vector2_t3577333262  L_32 = RectTransform_get_anchorMax_m2215014239(L_31, /*hidden argument*/NULL);
		Vector2_t3577333262  L_33 = RectTransformUtility_GetTransposed_m3431509790(NULL /*static, unused*/, L_32, /*hidden argument*/NULL);
		NullCheck(L_30);
		RectTransform_set_anchorMax_m1901263563(L_30, L_33, /*hidden argument*/NULL);
	}

IL_00b4:
	{
		return;
	}
}
// UnityEngine.Vector2 UnityEngine.RectTransformUtility::GetTransposed(UnityEngine.Vector2)
extern "C"  Vector2_t3577333262  RectTransformUtility_GetTransposed_m3431509790 (RuntimeObject * __this /* static, unused */, Vector2_t3577333262  ___input0, const RuntimeMethod* method)
{
	Vector2_t3577333262  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		float L_0 = (&___input0)->get_y_1();
		float L_1 = (&___input0)->get_x_0();
		Vector2_t3577333262  L_2;
		memset(&L_2, 0, sizeof(L_2));
		Vector2__ctor_m2914153465((&L_2), L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_001a;
	}

IL_001a:
	{
		Vector2_t3577333262  L_3 = V_0;
		return L_3;
	}
}
// System.Boolean UnityEngine.RectTransformUtility::RectangleContainsScreenPoint(UnityEngine.RectTransform,UnityEngine.Vector2,UnityEngine.Camera)
extern "C"  bool RectTransformUtility_RectangleContainsScreenPoint_m1829762538 (RuntimeObject * __this /* static, unused */, RectTransform_t3962994911 * ___rect0, Vector2_t3577333262  ___screenPoint1, Camera_t3175186167 * ___cam2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RectTransformUtility_RectangleContainsScreenPoint_m1829762538_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	{
		RectTransform_t3962994911 * L_0 = ___rect0;
		Camera_t3175186167 * L_1 = ___cam2;
		IL2CPP_RUNTIME_CLASS_INIT(RectTransformUtility_t535428424_il2cpp_TypeInfo_var);
		bool L_2 = RectTransformUtility_INTERNAL_CALL_RectangleContainsScreenPoint_m333423143(NULL /*static, unused*/, L_0, (&___screenPoint1), L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_0010;
	}

IL_0010:
	{
		bool L_3 = V_0;
		return L_3;
	}
}
// System.Boolean UnityEngine.RectTransformUtility::INTERNAL_CALL_RectangleContainsScreenPoint(UnityEngine.RectTransform,UnityEngine.Vector2&,UnityEngine.Camera)
extern "C"  bool RectTransformUtility_INTERNAL_CALL_RectangleContainsScreenPoint_m333423143 (RuntimeObject * __this /* static, unused */, RectTransform_t3962994911 * ___rect0, Vector2_t3577333262 * ___screenPoint1, Camera_t3175186167 * ___cam2, const RuntimeMethod* method)
{
	typedef bool (*RectTransformUtility_INTERNAL_CALL_RectangleContainsScreenPoint_m333423143_ftn) (RectTransform_t3962994911 *, Vector2_t3577333262 *, Camera_t3175186167 *);
	static RectTransformUtility_INTERNAL_CALL_RectangleContainsScreenPoint_m333423143_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectTransformUtility_INTERNAL_CALL_RectangleContainsScreenPoint_m333423143_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectTransformUtility::INTERNAL_CALL_RectangleContainsScreenPoint(UnityEngine.RectTransform,UnityEngine.Vector2&,UnityEngine.Camera)");
	bool retVal = _il2cpp_icall_func(___rect0, ___screenPoint1, ___cam2);
	return retVal;
}
// UnityEngine.Vector2 UnityEngine.RectTransformUtility::PixelAdjustPoint(UnityEngine.Vector2,UnityEngine.Transform,UnityEngine.Canvas)
extern "C"  Vector2_t3577333262  RectTransformUtility_PixelAdjustPoint_m368852017 (RuntimeObject * __this /* static, unused */, Vector2_t3577333262  ___point0, Transform_t3316442598 * ___elementTransform1, Canvas_t3744170841 * ___canvas2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RectTransformUtility_PixelAdjustPoint_m368852017_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector2_t3577333262  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector2_t3577333262  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		Transform_t3316442598 * L_0 = ___elementTransform1;
		Canvas_t3744170841 * L_1 = ___canvas2;
		IL2CPP_RUNTIME_CLASS_INIT(RectTransformUtility_t535428424_il2cpp_TypeInfo_var);
		RectTransformUtility_INTERNAL_CALL_PixelAdjustPoint_m3569618632(NULL /*static, unused*/, (&___point0), L_0, L_1, (&V_0), /*hidden argument*/NULL);
		Vector2_t3577333262  L_2 = V_0;
		V_1 = L_2;
		goto IL_0013;
	}

IL_0013:
	{
		Vector2_t3577333262  L_3 = V_1;
		return L_3;
	}
}
// System.Void UnityEngine.RectTransformUtility::INTERNAL_CALL_PixelAdjustPoint(UnityEngine.Vector2&,UnityEngine.Transform,UnityEngine.Canvas,UnityEngine.Vector2&)
extern "C"  void RectTransformUtility_INTERNAL_CALL_PixelAdjustPoint_m3569618632 (RuntimeObject * __this /* static, unused */, Vector2_t3577333262 * ___point0, Transform_t3316442598 * ___elementTransform1, Canvas_t3744170841 * ___canvas2, Vector2_t3577333262 * ___value3, const RuntimeMethod* method)
{
	typedef void (*RectTransformUtility_INTERNAL_CALL_PixelAdjustPoint_m3569618632_ftn) (Vector2_t3577333262 *, Transform_t3316442598 *, Canvas_t3744170841 *, Vector2_t3577333262 *);
	static RectTransformUtility_INTERNAL_CALL_PixelAdjustPoint_m3569618632_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectTransformUtility_INTERNAL_CALL_PixelAdjustPoint_m3569618632_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectTransformUtility::INTERNAL_CALL_PixelAdjustPoint(UnityEngine.Vector2&,UnityEngine.Transform,UnityEngine.Canvas,UnityEngine.Vector2&)");
	_il2cpp_icall_func(___point0, ___elementTransform1, ___canvas2, ___value3);
}
// UnityEngine.Rect UnityEngine.RectTransformUtility::PixelAdjustRect(UnityEngine.RectTransform,UnityEngine.Canvas)
extern "C"  Rect_t3436776195  RectTransformUtility_PixelAdjustRect_m861557880 (RuntimeObject * __this /* static, unused */, RectTransform_t3962994911 * ___rectTransform0, Canvas_t3744170841 * ___canvas1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RectTransformUtility_PixelAdjustRect_m861557880_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Rect_t3436776195  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Rect_t3436776195  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		RectTransform_t3962994911 * L_0 = ___rectTransform0;
		Canvas_t3744170841 * L_1 = ___canvas1;
		IL2CPP_RUNTIME_CLASS_INIT(RectTransformUtility_t535428424_il2cpp_TypeInfo_var);
		RectTransformUtility_INTERNAL_CALL_PixelAdjustRect_m3517741635(NULL /*static, unused*/, L_0, L_1, (&V_0), /*hidden argument*/NULL);
		Rect_t3436776195  L_2 = V_0;
		V_1 = L_2;
		goto IL_0011;
	}

IL_0011:
	{
		Rect_t3436776195  L_3 = V_1;
		return L_3;
	}
}
// System.Void UnityEngine.RectTransformUtility::INTERNAL_CALL_PixelAdjustRect(UnityEngine.RectTransform,UnityEngine.Canvas,UnityEngine.Rect&)
extern "C"  void RectTransformUtility_INTERNAL_CALL_PixelAdjustRect_m3517741635 (RuntimeObject * __this /* static, unused */, RectTransform_t3962994911 * ___rectTransform0, Canvas_t3744170841 * ___canvas1, Rect_t3436776195 * ___value2, const RuntimeMethod* method)
{
	typedef void (*RectTransformUtility_INTERNAL_CALL_PixelAdjustRect_m3517741635_ftn) (RectTransform_t3962994911 *, Canvas_t3744170841 *, Rect_t3436776195 *);
	static RectTransformUtility_INTERNAL_CALL_PixelAdjustRect_m3517741635_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectTransformUtility_INTERNAL_CALL_PixelAdjustRect_m3517741635_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectTransformUtility::INTERNAL_CALL_PixelAdjustRect(UnityEngine.RectTransform,UnityEngine.Canvas,UnityEngine.Rect&)");
	_il2cpp_icall_func(___rectTransform0, ___canvas1, ___value2);
}
// System.Void UnityEngine.RectTransformUtility::.cctor()
extern "C"  void RectTransformUtility__cctor_m977985261 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RectTransformUtility__cctor_m977985261_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		((RectTransformUtility_t535428424_StaticFields*)il2cpp_codegen_static_fields_for(RectTransformUtility_t535428424_il2cpp_TypeInfo_var))->set_s_Corners_0(((Vector3U5BU5D_t981680887*)SZArrayNew(Vector3U5BU5D_t981680887_il2cpp_TypeInfo_var, (uint32_t)4)));
		return;
	}
}
// System.Void UnityEngine.RemoteSettings::CallOnUpdate()
extern "C"  void RemoteSettings_CallOnUpdate_m1780483590 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RemoteSettings_CallOnUpdate_m1780483590_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	UpdatedEventHandler_t729869569 * V_0 = NULL;
	{
		UpdatedEventHandler_t729869569 * L_0 = ((RemoteSettings_t2746965213_StaticFields*)il2cpp_codegen_static_fields_for(RemoteSettings_t2746965213_il2cpp_TypeInfo_var))->get_Updated_0();
		V_0 = L_0;
		UpdatedEventHandler_t729869569 * L_1 = V_0;
		if (!L_1)
		{
			goto IL_0013;
		}
	}
	{
		UpdatedEventHandler_t729869569 * L_2 = V_0;
		NullCheck(L_2);
		UpdatedEventHandler_Invoke_m4098777110(L_2, /*hidden argument*/NULL);
	}

IL_0013:
	{
		return;
	}
}
extern "C"  void DelegatePInvokeWrapper_UpdatedEventHandler_t729869569 (UpdatedEventHandler_t729869569 * __this, const RuntimeMethod* method)
{
	typedef void (STDCALL *PInvokeFunc)();
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(il2cpp_codegen_get_method_pointer(((Il2CppDelegate*)__this)->method));

	// Native function invocation
	il2cppPInvokeFunc();

}
// System.Void UnityEngine.RemoteSettings/UpdatedEventHandler::.ctor(System.Object,System.IntPtr)
extern "C"  void UpdatedEventHandler__ctor_m1707919985 (UpdatedEventHandler_t729869569 * __this, RuntimeObject * ___object0, IntPtr_t ___method1, const RuntimeMethod* method)
{
	__this->set_method_ptr_0(il2cpp_codegen_get_method_pointer((RuntimeMethod*)___method1.get_m_value_0()));
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UnityEngine.RemoteSettings/UpdatedEventHandler::Invoke()
extern "C"  void UpdatedEventHandler_Invoke_m4098777110 (UpdatedEventHandler_t729869569 * __this, const RuntimeMethod* method)
{
	if(__this->get_prev_9() != NULL)
	{
		UpdatedEventHandler_Invoke_m4098777110((UpdatedEventHandler_t729869569 *)__this->get_prev_9(), method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	if ((__this->get_m_target_2() != NULL || MethodHasParameters((RuntimeMethod*)(__this->get_method_3().get_m_value_0()))) && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (RuntimeObject *, void* __this, const RuntimeMethod* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),(RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, const RuntimeMethod* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),(RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult UnityEngine.RemoteSettings/UpdatedEventHandler::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  RuntimeObject* UpdatedEventHandler_BeginInvoke_m3494829382 (UpdatedEventHandler_t729869569 * __this, AsyncCallback_t1057242265 * ___callback0, RuntimeObject * ___object1, const RuntimeMethod* method)
{
	void *__d_args[1] = {0};
	return (RuntimeObject*)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback0, (RuntimeObject*)___object1);
}
// System.Void UnityEngine.RemoteSettings/UpdatedEventHandler::EndInvoke(System.IAsyncResult)
extern "C"  void UpdatedEventHandler_EndInvoke_m2652311434 (UpdatedEventHandler_t729869569 * __this, RuntimeObject* ___result0, const RuntimeMethod* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.IntPtr UnityEngine.RenderBuffer::GetNativeRenderBufferPtr()
extern "C"  IntPtr_t RenderBuffer_GetNativeRenderBufferPtr_m2881121766 (RenderBuffer_t3998806177 * __this, const RuntimeMethod* method)
{
	IntPtr_t V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		IntPtr_t L_0 = __this->get_m_BufferPtr_1();
		IntPtr_t L_1 = RenderBufferHelper_GetNativeRenderBufferPtr_m2083301505(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_0012;
	}

IL_0012:
	{
		IntPtr_t L_2 = V_0;
		return L_2;
	}
}
extern "C"  IntPtr_t RenderBuffer_GetNativeRenderBufferPtr_m2881121766_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	RenderBuffer_t3998806177 * _thisAdjusted = reinterpret_cast<RenderBuffer_t3998806177 *>(__this + 1);
	return RenderBuffer_GetNativeRenderBufferPtr_m2881121766(_thisAdjusted, method);
}
// System.IntPtr UnityEngine.RenderBufferHelper::GetNativeRenderBufferPtr(System.IntPtr)
extern "C"  IntPtr_t RenderBufferHelper_GetNativeRenderBufferPtr_m2083301505 (RuntimeObject * __this /* static, unused */, IntPtr_t ___rb0, const RuntimeMethod* method)
{
	IntPtr_t V_0;
	memset(&V_0, 0, sizeof(V_0));
	IntPtr_t V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		IntPtr_t L_0 = ___rb0;
		RenderBufferHelper_INTERNAL_CALL_GetNativeRenderBufferPtr_m1589378208(NULL /*static, unused*/, L_0, (&V_0), /*hidden argument*/NULL);
		IntPtr_t L_1 = V_0;
		V_1 = L_1;
		goto IL_0010;
	}

IL_0010:
	{
		IntPtr_t L_2 = V_1;
		return L_2;
	}
}
// System.Void UnityEngine.RenderBufferHelper::INTERNAL_CALL_GetNativeRenderBufferPtr(System.IntPtr,System.IntPtr&)
extern "C"  void RenderBufferHelper_INTERNAL_CALL_GetNativeRenderBufferPtr_m1589378208 (RuntimeObject * __this /* static, unused */, IntPtr_t ___rb0, IntPtr_t* ___value1, const RuntimeMethod* method)
{
	typedef void (*RenderBufferHelper_INTERNAL_CALL_GetNativeRenderBufferPtr_m1589378208_ftn) (IntPtr_t, IntPtr_t*);
	static RenderBufferHelper_INTERNAL_CALL_GetNativeRenderBufferPtr_m1589378208_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RenderBufferHelper_INTERNAL_CALL_GetNativeRenderBufferPtr_m1589378208_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RenderBufferHelper::INTERNAL_CALL_GetNativeRenderBufferPtr(System.IntPtr,System.IntPtr&)");
	_il2cpp_icall_func(___rb0, ___value1);
}
// System.Boolean UnityEngine.Renderer::get_enabled()
extern "C"  bool Renderer_get_enabled_m2700419641 (Renderer_t1303575294 * __this, const RuntimeMethod* method)
{
	typedef bool (*Renderer_get_enabled_m2700419641_ftn) (Renderer_t1303575294 *);
	static Renderer_get_enabled_m2700419641_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Renderer_get_enabled_m2700419641_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Renderer::get_enabled()");
	bool retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// System.Void UnityEngine.Renderer::set_enabled(System.Boolean)
extern "C"  void Renderer_set_enabled_m3164806497 (Renderer_t1303575294 * __this, bool ___value0, const RuntimeMethod* method)
{
	typedef void (*Renderer_set_enabled_m3164806497_ftn) (Renderer_t1303575294 *, bool);
	static Renderer_set_enabled_m3164806497_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Renderer_set_enabled_m3164806497_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Renderer::set_enabled(System.Boolean)");
	_il2cpp_icall_func(__this, ___value0);
}
// UnityEngine.Material UnityEngine.Renderer::get_material()
extern "C"  Material_t1079520667 * Renderer_get_material_m115897516 (Renderer_t1303575294 * __this, const RuntimeMethod* method)
{
	typedef Material_t1079520667 * (*Renderer_get_material_m115897516_ftn) (Renderer_t1303575294 *);
	static Renderer_get_material_m115897516_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Renderer_get_material_m115897516_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Renderer::get_material()");
	Material_t1079520667 * retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// System.Void UnityEngine.Renderer::set_material(UnityEngine.Material)
extern "C"  void Renderer_set_material_m908302953 (Renderer_t1303575294 * __this, Material_t1079520667 * ___value0, const RuntimeMethod* method)
{
	typedef void (*Renderer_set_material_m908302953_ftn) (Renderer_t1303575294 *, Material_t1079520667 *);
	static Renderer_set_material_m908302953_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Renderer_set_material_m908302953_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Renderer::set_material(UnityEngine.Material)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.Renderer::set_sharedMaterial(UnityEngine.Material)
extern "C"  void Renderer_set_sharedMaterial_m1952739559 (Renderer_t1303575294 * __this, Material_t1079520667 * ___value0, const RuntimeMethod* method)
{
	typedef void (*Renderer_set_sharedMaterial_m1952739559_ftn) (Renderer_t1303575294 *, Material_t1079520667 *);
	static Renderer_set_sharedMaterial_m1952739559_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Renderer_set_sharedMaterial_m1952739559_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Renderer::set_sharedMaterial(UnityEngine.Material)");
	_il2cpp_icall_func(__this, ___value0);
}
// UnityEngine.Material[] UnityEngine.Renderer::get_materials()
extern "C"  MaterialU5BU5D_t1437145498* Renderer_get_materials_m4017502688 (Renderer_t1303575294 * __this, const RuntimeMethod* method)
{
	typedef MaterialU5BU5D_t1437145498* (*Renderer_get_materials_m4017502688_ftn) (Renderer_t1303575294 *);
	static Renderer_get_materials_m4017502688_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Renderer_get_materials_m4017502688_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Renderer::get_materials()");
	MaterialU5BU5D_t1437145498* retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// System.Void UnityEngine.Renderer::set_materials(UnityEngine.Material[])
extern "C"  void Renderer_set_materials_m787855357 (Renderer_t1303575294 * __this, MaterialU5BU5D_t1437145498* ___value0, const RuntimeMethod* method)
{
	typedef void (*Renderer_set_materials_m787855357_ftn) (Renderer_t1303575294 *, MaterialU5BU5D_t1437145498*);
	static Renderer_set_materials_m787855357_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Renderer_set_materials_m787855357_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Renderer::set_materials(UnityEngine.Material[])");
	_il2cpp_icall_func(__this, ___value0);
}
// UnityEngine.Material[] UnityEngine.Renderer::get_sharedMaterials()
extern "C"  MaterialU5BU5D_t1437145498* Renderer_get_sharedMaterials_m267703003 (Renderer_t1303575294 * __this, const RuntimeMethod* method)
{
	typedef MaterialU5BU5D_t1437145498* (*Renderer_get_sharedMaterials_m267703003_ftn) (Renderer_t1303575294 *);
	static Renderer_get_sharedMaterials_m267703003_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Renderer_get_sharedMaterials_m267703003_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Renderer::get_sharedMaterials()");
	MaterialU5BU5D_t1437145498* retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// System.Void UnityEngine.Renderer::set_sharedMaterials(UnityEngine.Material[])
extern "C"  void Renderer_set_sharedMaterials_m632974399 (Renderer_t1303575294 * __this, MaterialU5BU5D_t1437145498* ___value0, const RuntimeMethod* method)
{
	typedef void (*Renderer_set_sharedMaterials_m632974399_ftn) (Renderer_t1303575294 *, MaterialU5BU5D_t1437145498*);
	static Renderer_set_sharedMaterials_m632974399_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Renderer_set_sharedMaterials_m632974399_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Renderer::set_sharedMaterials(UnityEngine.Material[])");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Int32 UnityEngine.Renderer::get_sortingLayerID()
extern "C"  int32_t Renderer_get_sortingLayerID_m2333050401 (Renderer_t1303575294 * __this, const RuntimeMethod* method)
{
	typedef int32_t (*Renderer_get_sortingLayerID_m2333050401_ftn) (Renderer_t1303575294 *);
	static Renderer_get_sortingLayerID_m2333050401_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Renderer_get_sortingLayerID_m2333050401_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Renderer::get_sortingLayerID()");
	int32_t retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// System.Int32 UnityEngine.Renderer::get_sortingOrder()
extern "C"  int32_t Renderer_get_sortingOrder_m3804343206 (Renderer_t1303575294 * __this, const RuntimeMethod* method)
{
	typedef int32_t (*Renderer_get_sortingOrder_m3804343206_ftn) (Renderer_t1303575294 *);
	static Renderer_get_sortingOrder_m3804343206_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Renderer_get_sortingOrder_m3804343206_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Renderer::get_sortingOrder()");
	int32_t retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// System.Void UnityEngine.RenderTexture::.ctor(System.Int32,System.Int32,System.Int32,UnityEngine.RenderTextureFormat)
extern "C"  void RenderTexture__ctor_m220412340 (RenderTexture_t2315324624 * __this, int32_t ___width0, int32_t ___height1, int32_t ___depth2, int32_t ___format3, const RuntimeMethod* method)
{
	{
		Texture__ctor_m3045778021(__this, /*hidden argument*/NULL);
		RenderTexture_Internal_CreateRenderTexture_m295205310(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		int32_t L_0 = ___width0;
		VirtActionInvoker1< int32_t >::Invoke(5 /* System.Void UnityEngine.Texture::set_width(System.Int32) */, __this, L_0);
		int32_t L_1 = ___height1;
		VirtActionInvoker1< int32_t >::Invoke(7 /* System.Void UnityEngine.Texture::set_height(System.Int32) */, __this, L_1);
		int32_t L_2 = ___depth2;
		RenderTexture_set_depth_m307040776(__this, L_2, /*hidden argument*/NULL);
		int32_t L_3 = ___format3;
		RenderTexture_set_format_m402584206(__this, L_3, /*hidden argument*/NULL);
		int32_t L_4 = QualitySettings_get_activeColorSpace_m3960038853(NULL /*static, unused*/, /*hidden argument*/NULL);
		RenderTexture_Internal_SetSRGBReadWrite_m2250546398(NULL /*static, unused*/, __this, (bool)((((int32_t)L_4) == ((int32_t)1))? 1 : 0), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.RenderTexture::Internal_CreateRenderTexture(UnityEngine.RenderTexture)
extern "C"  void RenderTexture_Internal_CreateRenderTexture_m295205310 (RuntimeObject * __this /* static, unused */, RenderTexture_t2315324624 * ___rt0, const RuntimeMethod* method)
{
	typedef void (*RenderTexture_Internal_CreateRenderTexture_m295205310_ftn) (RenderTexture_t2315324624 *);
	static RenderTexture_Internal_CreateRenderTexture_m295205310_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RenderTexture_Internal_CreateRenderTexture_m295205310_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RenderTexture::Internal_CreateRenderTexture(UnityEngine.RenderTexture)");
	_il2cpp_icall_func(___rt0);
}
// UnityEngine.RenderTexture UnityEngine.RenderTexture::GetTemporary(System.Int32,System.Int32,System.Int32)
extern "C"  RenderTexture_t2315324624 * RenderTexture_GetTemporary_m472878072 (RuntimeObject * __this /* static, unused */, int32_t ___width0, int32_t ___height1, int32_t ___depthBuffer2, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	RenderTexture_t2315324624 * V_4 = NULL;
	{
		V_0 = 0;
		V_1 = 1;
		V_2 = 0;
		V_3 = 7;
		int32_t L_0 = ___width0;
		int32_t L_1 = ___height1;
		int32_t L_2 = ___depthBuffer2;
		int32_t L_3 = V_3;
		int32_t L_4 = V_2;
		int32_t L_5 = V_1;
		int32_t L_6 = V_0;
		RenderTexture_t2315324624 * L_7 = RenderTexture_GetTemporary_m2189383508(NULL /*static, unused*/, L_0, L_1, L_2, L_3, L_4, L_5, L_6, /*hidden argument*/NULL);
		V_4 = L_7;
		goto IL_001c;
	}

IL_001c:
	{
		RenderTexture_t2315324624 * L_8 = V_4;
		return L_8;
	}
}
// UnityEngine.RenderTexture UnityEngine.RenderTexture::GetTemporary(System.Int32,System.Int32,System.Int32,UnityEngine.RenderTextureFormat,UnityEngine.RenderTextureReadWrite,System.Int32,UnityEngine.RenderTextureMemoryless)
extern "C"  RenderTexture_t2315324624 * RenderTexture_GetTemporary_m2189383508 (RuntimeObject * __this /* static, unused */, int32_t ___width0, int32_t ___height1, int32_t ___depthBuffer2, int32_t ___format3, int32_t ___readWrite4, int32_t ___antiAliasing5, int32_t ___memorylessMode6, const RuntimeMethod* method)
{
	RenderTextureDescriptor_t3096142983  V_0;
	memset(&V_0, 0, sizeof(V_0));
	RenderTexture_t2315324624 * V_1 = NULL;
	{
		int32_t L_0 = ___width0;
		int32_t L_1 = ___height1;
		RenderTextureDescriptor__ctor_m3297691984((&V_0), L_0, L_1, /*hidden argument*/NULL);
		int32_t L_2 = ___depthBuffer2;
		RenderTextureDescriptor_set_depthBufferBits_m3455337282((&V_0), L_2, /*hidden argument*/NULL);
		RenderTextureDescriptor_set_vrUsage_m187535372((&V_0), 0, /*hidden argument*/NULL);
		int32_t L_3 = ___format3;
		RenderTextureDescriptor_set_colorFormat_m2687483655((&V_0), L_3, /*hidden argument*/NULL);
		int32_t L_4 = ___readWrite4;
		RenderTextureDescriptor_set_sRGB_m3197683802((&V_0), (bool)((((int32_t)((((int32_t)L_4) == ((int32_t)1))? 1 : 0)) == ((int32_t)0))? 1 : 0), /*hidden argument*/NULL);
		int32_t L_5 = ___antiAliasing5;
		RenderTextureDescriptor_set_msaaSamples_m585653155((&V_0), L_5, /*hidden argument*/NULL);
		int32_t L_6 = ___memorylessMode6;
		RenderTextureDescriptor_set_memoryless_m2217229932((&V_0), L_6, /*hidden argument*/NULL);
		RenderTextureDescriptor_t3096142983  L_7 = V_0;
		RenderTexture_t2315324624 * L_8 = RenderTexture_GetTemporary_m2400612209(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
		V_1 = L_8;
		goto IL_004f;
	}

IL_004f:
	{
		RenderTexture_t2315324624 * L_9 = V_1;
		return L_9;
	}
}
// UnityEngine.RenderTexture UnityEngine.RenderTexture::GetTemporary_Internal(UnityEngine.RenderTextureDescriptor)
extern "C"  RenderTexture_t2315324624 * RenderTexture_GetTemporary_Internal_m689233990 (RuntimeObject * __this /* static, unused */, RenderTextureDescriptor_t3096142983  ___desc0, const RuntimeMethod* method)
{
	RenderTexture_t2315324624 * V_0 = NULL;
	{
		RenderTexture_t2315324624 * L_0 = RenderTexture_INTERNAL_CALL_GetTemporary_Internal_m1897569625(NULL /*static, unused*/, (&___desc0), /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_000e;
	}

IL_000e:
	{
		RenderTexture_t2315324624 * L_1 = V_0;
		return L_1;
	}
}
// UnityEngine.RenderTexture UnityEngine.RenderTexture::INTERNAL_CALL_GetTemporary_Internal(UnityEngine.RenderTextureDescriptor&)
extern "C"  RenderTexture_t2315324624 * RenderTexture_INTERNAL_CALL_GetTemporary_Internal_m1897569625 (RuntimeObject * __this /* static, unused */, RenderTextureDescriptor_t3096142983 * ___desc0, const RuntimeMethod* method)
{
	typedef RenderTexture_t2315324624 * (*RenderTexture_INTERNAL_CALL_GetTemporary_Internal_m1897569625_ftn) (RenderTextureDescriptor_t3096142983 *);
	static RenderTexture_INTERNAL_CALL_GetTemporary_Internal_m1897569625_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RenderTexture_INTERNAL_CALL_GetTemporary_Internal_m1897569625_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RenderTexture::INTERNAL_CALL_GetTemporary_Internal(UnityEngine.RenderTextureDescriptor&)");
	RenderTexture_t2315324624 * retVal = _il2cpp_icall_func(___desc0);
	return retVal;
}
// System.Void UnityEngine.RenderTexture::ReleaseTemporary(UnityEngine.RenderTexture)
extern "C"  void RenderTexture_ReleaseTemporary_m458256871 (RuntimeObject * __this /* static, unused */, RenderTexture_t2315324624 * ___temp0, const RuntimeMethod* method)
{
	typedef void (*RenderTexture_ReleaseTemporary_m458256871_ftn) (RenderTexture_t2315324624 *);
	static RenderTexture_ReleaseTemporary_m458256871_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RenderTexture_ReleaseTemporary_m458256871_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RenderTexture::ReleaseTemporary(UnityEngine.RenderTexture)");
	_il2cpp_icall_func(___temp0);
}
// System.Int32 UnityEngine.RenderTexture::Internal_GetWidth(UnityEngine.RenderTexture)
extern "C"  int32_t RenderTexture_Internal_GetWidth_m1045162166 (RuntimeObject * __this /* static, unused */, RenderTexture_t2315324624 * ___mono0, const RuntimeMethod* method)
{
	typedef int32_t (*RenderTexture_Internal_GetWidth_m1045162166_ftn) (RenderTexture_t2315324624 *);
	static RenderTexture_Internal_GetWidth_m1045162166_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RenderTexture_Internal_GetWidth_m1045162166_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RenderTexture::Internal_GetWidth(UnityEngine.RenderTexture)");
	int32_t retVal = _il2cpp_icall_func(___mono0);
	return retVal;
}
// System.Void UnityEngine.RenderTexture::Internal_SetWidth(UnityEngine.RenderTexture,System.Int32)
extern "C"  void RenderTexture_Internal_SetWidth_m3641274632 (RuntimeObject * __this /* static, unused */, RenderTexture_t2315324624 * ___mono0, int32_t ___width1, const RuntimeMethod* method)
{
	typedef void (*RenderTexture_Internal_SetWidth_m3641274632_ftn) (RenderTexture_t2315324624 *, int32_t);
	static RenderTexture_Internal_SetWidth_m3641274632_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RenderTexture_Internal_SetWidth_m3641274632_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RenderTexture::Internal_SetWidth(UnityEngine.RenderTexture,System.Int32)");
	_il2cpp_icall_func(___mono0, ___width1);
}
// System.Int32 UnityEngine.RenderTexture::Internal_GetHeight(UnityEngine.RenderTexture)
extern "C"  int32_t RenderTexture_Internal_GetHeight_m2844568649 (RuntimeObject * __this /* static, unused */, RenderTexture_t2315324624 * ___mono0, const RuntimeMethod* method)
{
	typedef int32_t (*RenderTexture_Internal_GetHeight_m2844568649_ftn) (RenderTexture_t2315324624 *);
	static RenderTexture_Internal_GetHeight_m2844568649_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RenderTexture_Internal_GetHeight_m2844568649_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RenderTexture::Internal_GetHeight(UnityEngine.RenderTexture)");
	int32_t retVal = _il2cpp_icall_func(___mono0);
	return retVal;
}
// System.Void UnityEngine.RenderTexture::Internal_SetHeight(UnityEngine.RenderTexture,System.Int32)
extern "C"  void RenderTexture_Internal_SetHeight_m3853247974 (RuntimeObject * __this /* static, unused */, RenderTexture_t2315324624 * ___mono0, int32_t ___width1, const RuntimeMethod* method)
{
	typedef void (*RenderTexture_Internal_SetHeight_m3853247974_ftn) (RenderTexture_t2315324624 *, int32_t);
	static RenderTexture_Internal_SetHeight_m3853247974_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RenderTexture_Internal_SetHeight_m3853247974_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RenderTexture::Internal_SetHeight(UnityEngine.RenderTexture,System.Int32)");
	_il2cpp_icall_func(___mono0, ___width1);
}
// System.Void UnityEngine.RenderTexture::Internal_SetSRGBReadWrite(UnityEngine.RenderTexture,System.Boolean)
extern "C"  void RenderTexture_Internal_SetSRGBReadWrite_m2250546398 (RuntimeObject * __this /* static, unused */, RenderTexture_t2315324624 * ___mono0, bool ___sRGB1, const RuntimeMethod* method)
{
	typedef void (*RenderTexture_Internal_SetSRGBReadWrite_m2250546398_ftn) (RenderTexture_t2315324624 *, bool);
	static RenderTexture_Internal_SetSRGBReadWrite_m2250546398_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RenderTexture_Internal_SetSRGBReadWrite_m2250546398_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RenderTexture::Internal_SetSRGBReadWrite(UnityEngine.RenderTexture,System.Boolean)");
	_il2cpp_icall_func(___mono0, ___sRGB1);
}
// System.Int32 UnityEngine.RenderTexture::get_width()
extern "C"  int32_t RenderTexture_get_width_m4096064561 (RenderTexture_t2315324624 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = RenderTexture_Internal_GetWidth_m1045162166(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		int32_t L_1 = V_0;
		return L_1;
	}
}
// System.Void UnityEngine.RenderTexture::set_width(System.Int32)
extern "C"  void RenderTexture_set_width_m3859079382 (RenderTexture_t2315324624 * __this, int32_t ___value0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___value0;
		RenderTexture_Internal_SetWidth_m3641274632(NULL /*static, unused*/, __this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Int32 UnityEngine.RenderTexture::get_height()
extern "C"  int32_t RenderTexture_get_height_m3316765527 (RenderTexture_t2315324624 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = RenderTexture_Internal_GetHeight_m2844568649(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		int32_t L_1 = V_0;
		return L_1;
	}
}
// System.Void UnityEngine.RenderTexture::set_height(System.Int32)
extern "C"  void RenderTexture_set_height_m4229901113 (RenderTexture_t2315324624 * __this, int32_t ___value0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___value0;
		RenderTexture_Internal_SetHeight_m3853247974(NULL /*static, unused*/, __this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.RenderTexture::set_depth(System.Int32)
extern "C"  void RenderTexture_set_depth_m307040776 (RenderTexture_t2315324624 * __this, int32_t ___value0, const RuntimeMethod* method)
{
	typedef void (*RenderTexture_set_depth_m307040776_ftn) (RenderTexture_t2315324624 *, int32_t);
	static RenderTexture_set_depth_m307040776_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RenderTexture_set_depth_m307040776_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RenderTexture::set_depth(System.Int32)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.RenderTexture::set_format(UnityEngine.RenderTextureFormat)
extern "C"  void RenderTexture_set_format_m402584206 (RenderTexture_t2315324624 * __this, int32_t ___value0, const RuntimeMethod* method)
{
	typedef void (*RenderTexture_set_format_m402584206_ftn) (RenderTexture_t2315324624 *, int32_t);
	static RenderTexture_set_format_m402584206_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RenderTexture_set_format_m402584206_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RenderTexture::set_format(UnityEngine.RenderTextureFormat)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.RenderTexture::set_autoGenerateMips(System.Boolean)
extern "C"  void RenderTexture_set_autoGenerateMips_m1431606673 (RenderTexture_t2315324624 * __this, bool ___value0, const RuntimeMethod* method)
{
	typedef void (*RenderTexture_set_autoGenerateMips_m1431606673_ftn) (RenderTexture_t2315324624 *, bool);
	static RenderTexture_set_autoGenerateMips_m1431606673_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RenderTexture_set_autoGenerateMips_m1431606673_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RenderTexture::set_autoGenerateMips(System.Boolean)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.RenderTexture::set_antiAliasing(System.Int32)
extern "C"  void RenderTexture_set_antiAliasing_m2602010778 (RenderTexture_t2315324624 * __this, int32_t ___value0, const RuntimeMethod* method)
{
	typedef void (*RenderTexture_set_antiAliasing_m2602010778_ftn) (RenderTexture_t2315324624 *, int32_t);
	static RenderTexture_set_antiAliasing_m2602010778_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RenderTexture_set_antiAliasing_m2602010778_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RenderTexture::set_antiAliasing(System.Int32)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Boolean UnityEngine.RenderTexture::Create()
extern "C"  bool RenderTexture_Create_m1044963140 (RenderTexture_t2315324624 * __this, const RuntimeMethod* method)
{
	bool V_0 = false;
	{
		bool L_0 = RenderTexture_INTERNAL_CALL_Create_m2261583106(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		bool L_1 = V_0;
		return L_1;
	}
}
// System.Boolean UnityEngine.RenderTexture::INTERNAL_CALL_Create(UnityEngine.RenderTexture)
extern "C"  bool RenderTexture_INTERNAL_CALL_Create_m2261583106 (RuntimeObject * __this /* static, unused */, RenderTexture_t2315324624 * ___self0, const RuntimeMethod* method)
{
	typedef bool (*RenderTexture_INTERNAL_CALL_Create_m2261583106_ftn) (RenderTexture_t2315324624 *);
	static RenderTexture_INTERNAL_CALL_Create_m2261583106_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RenderTexture_INTERNAL_CALL_Create_m2261583106_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RenderTexture::INTERNAL_CALL_Create(UnityEngine.RenderTexture)");
	bool retVal = _il2cpp_icall_func(___self0);
	return retVal;
}
// System.Void UnityEngine.RenderTexture::Release()
extern "C"  void RenderTexture_Release_m311571628 (RenderTexture_t2315324624 * __this, const RuntimeMethod* method)
{
	{
		RenderTexture_INTERNAL_CALL_Release_m2125671618(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.RenderTexture::INTERNAL_CALL_Release(UnityEngine.RenderTexture)
extern "C"  void RenderTexture_INTERNAL_CALL_Release_m2125671618 (RuntimeObject * __this /* static, unused */, RenderTexture_t2315324624 * ___self0, const RuntimeMethod* method)
{
	typedef void (*RenderTexture_INTERNAL_CALL_Release_m2125671618_ftn) (RenderTexture_t2315324624 *);
	static RenderTexture_INTERNAL_CALL_Release_m2125671618_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RenderTexture_INTERNAL_CALL_Release_m2125671618_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RenderTexture::INTERNAL_CALL_Release(UnityEngine.RenderTexture)");
	_il2cpp_icall_func(___self0);
}
// System.Boolean UnityEngine.RenderTexture::IsCreated()
extern "C"  bool RenderTexture_IsCreated_m1241068595 (RenderTexture_t2315324624 * __this, const RuntimeMethod* method)
{
	bool V_0 = false;
	{
		bool L_0 = RenderTexture_INTERNAL_CALL_IsCreated_m2284061715(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		bool L_1 = V_0;
		return L_1;
	}
}
// System.Boolean UnityEngine.RenderTexture::INTERNAL_CALL_IsCreated(UnityEngine.RenderTexture)
extern "C"  bool RenderTexture_INTERNAL_CALL_IsCreated_m2284061715 (RuntimeObject * __this /* static, unused */, RenderTexture_t2315324624 * ___self0, const RuntimeMethod* method)
{
	typedef bool (*RenderTexture_INTERNAL_CALL_IsCreated_m2284061715_ftn) (RenderTexture_t2315324624 *);
	static RenderTexture_INTERNAL_CALL_IsCreated_m2284061715_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RenderTexture_INTERNAL_CALL_IsCreated_m2284061715_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RenderTexture::INTERNAL_CALL_IsCreated(UnityEngine.RenderTexture)");
	bool retVal = _il2cpp_icall_func(___self0);
	return retVal;
}
// UnityEngine.RenderBuffer UnityEngine.RenderTexture::get_colorBuffer()
extern "C"  RenderBuffer_t3998806177  RenderTexture_get_colorBuffer_m595252884 (RenderTexture_t2315324624 * __this, const RuntimeMethod* method)
{
	RenderBuffer_t3998806177  V_0;
	memset(&V_0, 0, sizeof(V_0));
	RenderBuffer_t3998806177  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		RenderTexture_GetColorBuffer_m1930834545(__this, (&V_0), /*hidden argument*/NULL);
		RenderBuffer_t3998806177  L_0 = V_0;
		V_1 = L_0;
		goto IL_0010;
	}

IL_0010:
	{
		RenderBuffer_t3998806177  L_1 = V_1;
		return L_1;
	}
}
// UnityEngine.RenderBuffer UnityEngine.RenderTexture::get_depthBuffer()
extern "C"  RenderBuffer_t3998806177  RenderTexture_get_depthBuffer_m3344100731 (RenderTexture_t2315324624 * __this, const RuntimeMethod* method)
{
	RenderBuffer_t3998806177  V_0;
	memset(&V_0, 0, sizeof(V_0));
	RenderBuffer_t3998806177  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		RenderTexture_GetDepthBuffer_m2229287778(__this, (&V_0), /*hidden argument*/NULL);
		RenderBuffer_t3998806177  L_0 = V_0;
		V_1 = L_0;
		goto IL_0010;
	}

IL_0010:
	{
		RenderBuffer_t3998806177  L_1 = V_1;
		return L_1;
	}
}
// System.Void UnityEngine.RenderTexture::GetColorBuffer(UnityEngine.RenderBuffer&)
extern "C"  void RenderTexture_GetColorBuffer_m1930834545 (RenderTexture_t2315324624 * __this, RenderBuffer_t3998806177 * ___res0, const RuntimeMethod* method)
{
	typedef void (*RenderTexture_GetColorBuffer_m1930834545_ftn) (RenderTexture_t2315324624 *, RenderBuffer_t3998806177 *);
	static RenderTexture_GetColorBuffer_m1930834545_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RenderTexture_GetColorBuffer_m1930834545_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RenderTexture::GetColorBuffer(UnityEngine.RenderBuffer&)");
	_il2cpp_icall_func(__this, ___res0);
}
// System.Void UnityEngine.RenderTexture::GetDepthBuffer(UnityEngine.RenderBuffer&)
extern "C"  void RenderTexture_GetDepthBuffer_m2229287778 (RenderTexture_t2315324624 * __this, RenderBuffer_t3998806177 * ___res0, const RuntimeMethod* method)
{
	typedef void (*RenderTexture_GetDepthBuffer_m2229287778_ftn) (RenderTexture_t2315324624 *, RenderBuffer_t3998806177 *);
	static RenderTexture_GetDepthBuffer_m2229287778_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RenderTexture_GetDepthBuffer_m2229287778_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RenderTexture::GetDepthBuffer(UnityEngine.RenderBuffer&)");
	_il2cpp_icall_func(__this, ___res0);
}
// System.Void UnityEngine.RenderTexture::set_active(UnityEngine.RenderTexture)
extern "C"  void RenderTexture_set_active_m156919682 (RuntimeObject * __this /* static, unused */, RenderTexture_t2315324624 * ___value0, const RuntimeMethod* method)
{
	typedef void (*RenderTexture_set_active_m156919682_ftn) (RenderTexture_t2315324624 *);
	static RenderTexture_set_active_m156919682_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RenderTexture_set_active_m156919682_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RenderTexture::set_active(UnityEngine.RenderTexture)");
	_il2cpp_icall_func(___value0);
}
// UnityEngine.RenderTexture UnityEngine.RenderTexture::GetTemporary(UnityEngine.RenderTextureDescriptor)
extern "C"  RenderTexture_t2315324624 * RenderTexture_GetTemporary_m2400612209 (RuntimeObject * __this /* static, unused */, RenderTextureDescriptor_t3096142983  ___desc0, const RuntimeMethod* method)
{
	RenderTexture_t2315324624 * V_0 = NULL;
	{
		RenderTextureDescriptor_t3096142983  L_0 = ___desc0;
		RenderTexture_ValidateRenderTextureDesc_m3206735494(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		RenderTextureDescriptor_set_createdFromScript_m3731502648((&___desc0), (bool)1, /*hidden argument*/NULL);
		RenderTextureDescriptor_t3096142983  L_1 = ___desc0;
		RenderTexture_t2315324624 * L_2 = RenderTexture_GetTemporary_Internal_m689233990(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_001b;
	}

IL_001b:
	{
		RenderTexture_t2315324624 * L_3 = V_0;
		return L_3;
	}
}
// System.Void UnityEngine.RenderTexture::ValidateRenderTextureDesc(UnityEngine.RenderTextureDescriptor)
extern "C"  void RenderTexture_ValidateRenderTextureDesc_m3206735494 (RuntimeObject * __this /* static, unused */, RenderTextureDescriptor_t3096142983  ___desc0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RenderTexture_ValidateRenderTextureDesc_m3206735494_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = RenderTextureDescriptor_get_width_m253831787((&___desc0), /*hidden argument*/NULL);
		if ((((int32_t)L_0) > ((int32_t)0)))
		{
			goto IL_001f;
		}
	}
	{
		ArgumentException_t4028401650 * L_1 = (ArgumentException_t4028401650 *)il2cpp_codegen_object_new(ArgumentException_t4028401650_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m596893627(L_1, _stringLiteral2874801222, _stringLiteral2103610803, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_001f:
	{
		int32_t L_2 = RenderTextureDescriptor_get_height_m3307205944((&___desc0), /*hidden argument*/NULL);
		if ((((int32_t)L_2) > ((int32_t)0)))
		{
			goto IL_003d;
		}
	}
	{
		ArgumentException_t4028401650 * L_3 = (ArgumentException_t4028401650 *)il2cpp_codegen_object_new(ArgumentException_t4028401650_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m596893627(L_3, _stringLiteral2311410166, _stringLiteral308001844, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_003d:
	{
		int32_t L_4 = RenderTextureDescriptor_get_volumeDepth_m3010468377((&___desc0), /*hidden argument*/NULL);
		if ((((int32_t)L_4) > ((int32_t)0)))
		{
			goto IL_005b;
		}
	}
	{
		ArgumentException_t4028401650 * L_5 = (ArgumentException_t4028401650 *)il2cpp_codegen_object_new(ArgumentException_t4028401650_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m596893627(L_5, _stringLiteral3426330824, _stringLiteral1909265924, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_005b:
	{
		int32_t L_6 = RenderTextureDescriptor_get_msaaSamples_m284599142((&___desc0), /*hidden argument*/NULL);
		if ((((int32_t)L_6) == ((int32_t)1)))
		{
			goto IL_00a0;
		}
	}
	{
		int32_t L_7 = RenderTextureDescriptor_get_msaaSamples_m284599142((&___desc0), /*hidden argument*/NULL);
		if ((((int32_t)L_7) == ((int32_t)2)))
		{
			goto IL_00a0;
		}
	}
	{
		int32_t L_8 = RenderTextureDescriptor_get_msaaSamples_m284599142((&___desc0), /*hidden argument*/NULL);
		if ((((int32_t)L_8) == ((int32_t)4)))
		{
			goto IL_00a0;
		}
	}
	{
		int32_t L_9 = RenderTextureDescriptor_get_msaaSamples_m284599142((&___desc0), /*hidden argument*/NULL);
		if ((((int32_t)L_9) == ((int32_t)8)))
		{
			goto IL_00a0;
		}
	}
	{
		ArgumentException_t4028401650 * L_10 = (ArgumentException_t4028401650 *)il2cpp_codegen_object_new(ArgumentException_t4028401650_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m596893627(L_10, _stringLiteral2615600362, _stringLiteral810757830, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_10);
	}

IL_00a0:
	{
		int32_t L_11 = RenderTextureDescriptor_get_depthBufferBits_m1512939583((&___desc0), /*hidden argument*/NULL);
		if (!L_11)
		{
			goto IL_00d9;
		}
	}
	{
		int32_t L_12 = RenderTextureDescriptor_get_depthBufferBits_m1512939583((&___desc0), /*hidden argument*/NULL);
		if ((((int32_t)L_12) == ((int32_t)((int32_t)16))))
		{
			goto IL_00d9;
		}
	}
	{
		int32_t L_13 = RenderTextureDescriptor_get_depthBufferBits_m1512939583((&___desc0), /*hidden argument*/NULL);
		if ((((int32_t)L_13) == ((int32_t)((int32_t)24))))
		{
			goto IL_00d9;
		}
	}
	{
		ArgumentException_t4028401650 * L_14 = (ArgumentException_t4028401650 *)il2cpp_codegen_object_new(ArgumentException_t4028401650_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m596893627(L_14, _stringLiteral2448784868, _stringLiteral2308451020, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_14);
	}

IL_00d9:
	{
		return;
	}
}
// System.Void UnityEngine.RenderTextureDescriptor::.ctor(System.Int32,System.Int32)
extern "C"  void RenderTextureDescriptor__ctor_m3297691984 (RenderTextureDescriptor_t3096142983 * __this, int32_t ___width0, int32_t ___height1, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___width0;
		int32_t L_1 = ___height1;
		RenderTextureDescriptor__ctor_m430880500(__this, L_0, L_1, 7, 0, /*hidden argument*/NULL);
		return;
	}
}
extern "C"  void RenderTextureDescriptor__ctor_m3297691984_AdjustorThunk (RuntimeObject * __this, int32_t ___width0, int32_t ___height1, const RuntimeMethod* method)
{
	RenderTextureDescriptor_t3096142983 * _thisAdjusted = reinterpret_cast<RenderTextureDescriptor_t3096142983 *>(__this + 1);
	RenderTextureDescriptor__ctor_m3297691984(_thisAdjusted, ___width0, ___height1, method);
}
// System.Void UnityEngine.RenderTextureDescriptor::.ctor(System.Int32,System.Int32,UnityEngine.RenderTextureFormat,System.Int32)
extern "C"  void RenderTextureDescriptor__ctor_m430880500 (RenderTextureDescriptor_t3096142983 * __this, int32_t ___width0, int32_t ___height1, int32_t ___colorFormat2, int32_t ___depthBufferBits3, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RenderTextureDescriptor__ctor_m430880500_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Initobj (RenderTextureDescriptor_t3096142983_il2cpp_TypeInfo_var, __this);
		int32_t L_0 = ___width0;
		RenderTextureDescriptor_set_width_m1163384803(__this, L_0, /*hidden argument*/NULL);
		int32_t L_1 = ___height1;
		RenderTextureDescriptor_set_height_m773633459(__this, L_1, /*hidden argument*/NULL);
		RenderTextureDescriptor_set_volumeDepth_m3887969168(__this, 1, /*hidden argument*/NULL);
		RenderTextureDescriptor_set_msaaSamples_m585653155(__this, 1, /*hidden argument*/NULL);
		int32_t L_2 = ___colorFormat2;
		RenderTextureDescriptor_set_colorFormat_m2687483655(__this, L_2, /*hidden argument*/NULL);
		int32_t L_3 = ___depthBufferBits3;
		RenderTextureDescriptor_set_depthBufferBits_m3455337282(__this, L_3, /*hidden argument*/NULL);
		RenderTextureDescriptor_set_dimension_m2285702884(__this, 2, /*hidden argument*/NULL);
		RenderTextureDescriptor_set_shadowSamplingMode_m3567853727(__this, 2, /*hidden argument*/NULL);
		RenderTextureDescriptor_set_vrUsage_m187535372(__this, 0, /*hidden argument*/NULL);
		__this->set__flags_10(((int32_t)130));
		RenderTextureDescriptor_set_memoryless_m2217229932(__this, 0, /*hidden argument*/NULL);
		return;
	}
}
extern "C"  void RenderTextureDescriptor__ctor_m430880500_AdjustorThunk (RuntimeObject * __this, int32_t ___width0, int32_t ___height1, int32_t ___colorFormat2, int32_t ___depthBufferBits3, const RuntimeMethod* method)
{
	RenderTextureDescriptor_t3096142983 * _thisAdjusted = reinterpret_cast<RenderTextureDescriptor_t3096142983 *>(__this + 1);
	RenderTextureDescriptor__ctor_m430880500(_thisAdjusted, ___width0, ___height1, ___colorFormat2, ___depthBufferBits3, method);
}
// System.Int32 UnityEngine.RenderTextureDescriptor::get_width()
extern "C"  int32_t RenderTextureDescriptor_get_width_m253831787 (RenderTextureDescriptor_t3096142983 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_U3CwidthU3Ek__BackingField_0();
		V_0 = L_0;
		goto IL_000c;
	}

IL_000c:
	{
		int32_t L_1 = V_0;
		return L_1;
	}
}
extern "C"  int32_t RenderTextureDescriptor_get_width_m253831787_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	RenderTextureDescriptor_t3096142983 * _thisAdjusted = reinterpret_cast<RenderTextureDescriptor_t3096142983 *>(__this + 1);
	return RenderTextureDescriptor_get_width_m253831787(_thisAdjusted, method);
}
// System.Void UnityEngine.RenderTextureDescriptor::set_width(System.Int32)
extern "C"  void RenderTextureDescriptor_set_width_m1163384803 (RenderTextureDescriptor_t3096142983 * __this, int32_t ___value0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_U3CwidthU3Ek__BackingField_0(L_0);
		return;
	}
}
extern "C"  void RenderTextureDescriptor_set_width_m1163384803_AdjustorThunk (RuntimeObject * __this, int32_t ___value0, const RuntimeMethod* method)
{
	RenderTextureDescriptor_t3096142983 * _thisAdjusted = reinterpret_cast<RenderTextureDescriptor_t3096142983 *>(__this + 1);
	RenderTextureDescriptor_set_width_m1163384803(_thisAdjusted, ___value0, method);
}
// System.Int32 UnityEngine.RenderTextureDescriptor::get_height()
extern "C"  int32_t RenderTextureDescriptor_get_height_m3307205944 (RenderTextureDescriptor_t3096142983 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_U3CheightU3Ek__BackingField_1();
		V_0 = L_0;
		goto IL_000c;
	}

IL_000c:
	{
		int32_t L_1 = V_0;
		return L_1;
	}
}
extern "C"  int32_t RenderTextureDescriptor_get_height_m3307205944_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	RenderTextureDescriptor_t3096142983 * _thisAdjusted = reinterpret_cast<RenderTextureDescriptor_t3096142983 *>(__this + 1);
	return RenderTextureDescriptor_get_height_m3307205944(_thisAdjusted, method);
}
// System.Void UnityEngine.RenderTextureDescriptor::set_height(System.Int32)
extern "C"  void RenderTextureDescriptor_set_height_m773633459 (RenderTextureDescriptor_t3096142983 * __this, int32_t ___value0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_U3CheightU3Ek__BackingField_1(L_0);
		return;
	}
}
extern "C"  void RenderTextureDescriptor_set_height_m773633459_AdjustorThunk (RuntimeObject * __this, int32_t ___value0, const RuntimeMethod* method)
{
	RenderTextureDescriptor_t3096142983 * _thisAdjusted = reinterpret_cast<RenderTextureDescriptor_t3096142983 *>(__this + 1);
	RenderTextureDescriptor_set_height_m773633459(_thisAdjusted, ___value0, method);
}
// System.Int32 UnityEngine.RenderTextureDescriptor::get_msaaSamples()
extern "C"  int32_t RenderTextureDescriptor_get_msaaSamples_m284599142 (RenderTextureDescriptor_t3096142983 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_U3CmsaaSamplesU3Ek__BackingField_2();
		V_0 = L_0;
		goto IL_000c;
	}

IL_000c:
	{
		int32_t L_1 = V_0;
		return L_1;
	}
}
extern "C"  int32_t RenderTextureDescriptor_get_msaaSamples_m284599142_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	RenderTextureDescriptor_t3096142983 * _thisAdjusted = reinterpret_cast<RenderTextureDescriptor_t3096142983 *>(__this + 1);
	return RenderTextureDescriptor_get_msaaSamples_m284599142(_thisAdjusted, method);
}
// System.Void UnityEngine.RenderTextureDescriptor::set_msaaSamples(System.Int32)
extern "C"  void RenderTextureDescriptor_set_msaaSamples_m585653155 (RenderTextureDescriptor_t3096142983 * __this, int32_t ___value0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_U3CmsaaSamplesU3Ek__BackingField_2(L_0);
		return;
	}
}
extern "C"  void RenderTextureDescriptor_set_msaaSamples_m585653155_AdjustorThunk (RuntimeObject * __this, int32_t ___value0, const RuntimeMethod* method)
{
	RenderTextureDescriptor_t3096142983 * _thisAdjusted = reinterpret_cast<RenderTextureDescriptor_t3096142983 *>(__this + 1);
	RenderTextureDescriptor_set_msaaSamples_m585653155(_thisAdjusted, ___value0, method);
}
// System.Int32 UnityEngine.RenderTextureDescriptor::get_volumeDepth()
extern "C"  int32_t RenderTextureDescriptor_get_volumeDepth_m3010468377 (RenderTextureDescriptor_t3096142983 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_U3CvolumeDepthU3Ek__BackingField_3();
		V_0 = L_0;
		goto IL_000c;
	}

IL_000c:
	{
		int32_t L_1 = V_0;
		return L_1;
	}
}
extern "C"  int32_t RenderTextureDescriptor_get_volumeDepth_m3010468377_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	RenderTextureDescriptor_t3096142983 * _thisAdjusted = reinterpret_cast<RenderTextureDescriptor_t3096142983 *>(__this + 1);
	return RenderTextureDescriptor_get_volumeDepth_m3010468377(_thisAdjusted, method);
}
// System.Void UnityEngine.RenderTextureDescriptor::set_volumeDepth(System.Int32)
extern "C"  void RenderTextureDescriptor_set_volumeDepth_m3887969168 (RenderTextureDescriptor_t3096142983 * __this, int32_t ___value0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_U3CvolumeDepthU3Ek__BackingField_3(L_0);
		return;
	}
}
extern "C"  void RenderTextureDescriptor_set_volumeDepth_m3887969168_AdjustorThunk (RuntimeObject * __this, int32_t ___value0, const RuntimeMethod* method)
{
	RenderTextureDescriptor_t3096142983 * _thisAdjusted = reinterpret_cast<RenderTextureDescriptor_t3096142983 *>(__this + 1);
	RenderTextureDescriptor_set_volumeDepth_m3887969168(_thisAdjusted, ___value0, method);
}
// System.Void UnityEngine.RenderTextureDescriptor::set_colorFormat(UnityEngine.RenderTextureFormat)
extern "C"  void RenderTextureDescriptor_set_colorFormat_m2687483655 (RenderTextureDescriptor_t3096142983 * __this, int32_t ___value0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_U3CcolorFormatU3Ek__BackingField_4(L_0);
		return;
	}
}
extern "C"  void RenderTextureDescriptor_set_colorFormat_m2687483655_AdjustorThunk (RuntimeObject * __this, int32_t ___value0, const RuntimeMethod* method)
{
	RenderTextureDescriptor_t3096142983 * _thisAdjusted = reinterpret_cast<RenderTextureDescriptor_t3096142983 *>(__this + 1);
	RenderTextureDescriptor_set_colorFormat_m2687483655(_thisAdjusted, ___value0, method);
}
// System.Int32 UnityEngine.RenderTextureDescriptor::get_depthBufferBits()
extern "C"  int32_t RenderTextureDescriptor_get_depthBufferBits_m1512939583 (RenderTextureDescriptor_t3096142983 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RenderTextureDescriptor_get_depthBufferBits_m1512939583_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(RenderTextureDescriptor_t3096142983_il2cpp_TypeInfo_var);
		Int32U5BU5D_t2324750880* L_0 = ((RenderTextureDescriptor_t3096142983_StaticFields*)il2cpp_codegen_static_fields_for(RenderTextureDescriptor_t3096142983_il2cpp_TypeInfo_var))->get_depthFormatBits_6();
		int32_t L_1 = __this->get__depthBufferBits_5();
		NullCheck(L_0);
		int32_t L_2 = L_1;
		int32_t L_3 = (L_0)->GetAt(static_cast<il2cpp_array_size_t>(L_2));
		V_0 = L_3;
		goto IL_0013;
	}

IL_0013:
	{
		int32_t L_4 = V_0;
		return L_4;
	}
}
extern "C"  int32_t RenderTextureDescriptor_get_depthBufferBits_m1512939583_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	RenderTextureDescriptor_t3096142983 * _thisAdjusted = reinterpret_cast<RenderTextureDescriptor_t3096142983 *>(__this + 1);
	return RenderTextureDescriptor_get_depthBufferBits_m1512939583(_thisAdjusted, method);
}
// System.Void UnityEngine.RenderTextureDescriptor::set_depthBufferBits(System.Int32)
extern "C"  void RenderTextureDescriptor_set_depthBufferBits_m3455337282 (RenderTextureDescriptor_t3096142983 * __this, int32_t ___value0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___value0;
		if ((((int32_t)L_0) > ((int32_t)0)))
		{
			goto IL_0014;
		}
	}
	{
		__this->set__depthBufferBits_5(0);
		goto IL_002f;
	}

IL_0014:
	{
		int32_t L_1 = ___value0;
		if ((((int32_t)L_1) > ((int32_t)((int32_t)16))))
		{
			goto IL_0028;
		}
	}
	{
		__this->set__depthBufferBits_5(1);
		goto IL_002f;
	}

IL_0028:
	{
		__this->set__depthBufferBits_5(2);
	}

IL_002f:
	{
		return;
	}
}
extern "C"  void RenderTextureDescriptor_set_depthBufferBits_m3455337282_AdjustorThunk (RuntimeObject * __this, int32_t ___value0, const RuntimeMethod* method)
{
	RenderTextureDescriptor_t3096142983 * _thisAdjusted = reinterpret_cast<RenderTextureDescriptor_t3096142983 *>(__this + 1);
	RenderTextureDescriptor_set_depthBufferBits_m3455337282(_thisAdjusted, ___value0, method);
}
// System.Void UnityEngine.RenderTextureDescriptor::set_dimension(UnityEngine.Rendering.TextureDimension)
extern "C"  void RenderTextureDescriptor_set_dimension_m2285702884 (RenderTextureDescriptor_t3096142983 * __this, int32_t ___value0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_U3CdimensionU3Ek__BackingField_7(L_0);
		return;
	}
}
extern "C"  void RenderTextureDescriptor_set_dimension_m2285702884_AdjustorThunk (RuntimeObject * __this, int32_t ___value0, const RuntimeMethod* method)
{
	RenderTextureDescriptor_t3096142983 * _thisAdjusted = reinterpret_cast<RenderTextureDescriptor_t3096142983 *>(__this + 1);
	RenderTextureDescriptor_set_dimension_m2285702884(_thisAdjusted, ___value0, method);
}
// System.Void UnityEngine.RenderTextureDescriptor::set_shadowSamplingMode(UnityEngine.Rendering.ShadowSamplingMode)
extern "C"  void RenderTextureDescriptor_set_shadowSamplingMode_m3567853727 (RenderTextureDescriptor_t3096142983 * __this, int32_t ___value0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_U3CshadowSamplingModeU3Ek__BackingField_8(L_0);
		return;
	}
}
extern "C"  void RenderTextureDescriptor_set_shadowSamplingMode_m3567853727_AdjustorThunk (RuntimeObject * __this, int32_t ___value0, const RuntimeMethod* method)
{
	RenderTextureDescriptor_t3096142983 * _thisAdjusted = reinterpret_cast<RenderTextureDescriptor_t3096142983 *>(__this + 1);
	RenderTextureDescriptor_set_shadowSamplingMode_m3567853727(_thisAdjusted, ___value0, method);
}
// System.Void UnityEngine.RenderTextureDescriptor::set_vrUsage(UnityEngine.VRTextureUsage)
extern "C"  void RenderTextureDescriptor_set_vrUsage_m187535372 (RenderTextureDescriptor_t3096142983 * __this, int32_t ___value0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_U3CvrUsageU3Ek__BackingField_9(L_0);
		return;
	}
}
extern "C"  void RenderTextureDescriptor_set_vrUsage_m187535372_AdjustorThunk (RuntimeObject * __this, int32_t ___value0, const RuntimeMethod* method)
{
	RenderTextureDescriptor_t3096142983 * _thisAdjusted = reinterpret_cast<RenderTextureDescriptor_t3096142983 *>(__this + 1);
	RenderTextureDescriptor_set_vrUsage_m187535372(_thisAdjusted, ___value0, method);
}
// System.Void UnityEngine.RenderTextureDescriptor::set_memoryless(UnityEngine.RenderTextureMemoryless)
extern "C"  void RenderTextureDescriptor_set_memoryless_m2217229932 (RenderTextureDescriptor_t3096142983 * __this, int32_t ___value0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_U3CmemorylessU3Ek__BackingField_11(L_0);
		return;
	}
}
extern "C"  void RenderTextureDescriptor_set_memoryless_m2217229932_AdjustorThunk (RuntimeObject * __this, int32_t ___value0, const RuntimeMethod* method)
{
	RenderTextureDescriptor_t3096142983 * _thisAdjusted = reinterpret_cast<RenderTextureDescriptor_t3096142983 *>(__this + 1);
	RenderTextureDescriptor_set_memoryless_m2217229932(_thisAdjusted, ___value0, method);
}
// System.Void UnityEngine.RenderTextureDescriptor::SetOrClearRenderTextureCreationFlag(System.Boolean,UnityEngine.RenderTextureCreationFlags)
extern "C"  void RenderTextureDescriptor_SetOrClearRenderTextureCreationFlag_m1589951879 (RenderTextureDescriptor_t3096142983 * __this, bool ___value0, int32_t ___flag1, const RuntimeMethod* method)
{
	{
		bool L_0 = ___value0;
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		int32_t L_1 = __this->get__flags_10();
		int32_t L_2 = ___flag1;
		__this->set__flags_10(((int32_t)((int32_t)L_1|(int32_t)L_2)));
		goto IL_002d;
	}

IL_001c:
	{
		int32_t L_3 = __this->get__flags_10();
		int32_t L_4 = ___flag1;
		__this->set__flags_10(((int32_t)((int32_t)L_3&(int32_t)((~L_4)))));
	}

IL_002d:
	{
		return;
	}
}
extern "C"  void RenderTextureDescriptor_SetOrClearRenderTextureCreationFlag_m1589951879_AdjustorThunk (RuntimeObject * __this, bool ___value0, int32_t ___flag1, const RuntimeMethod* method)
{
	RenderTextureDescriptor_t3096142983 * _thisAdjusted = reinterpret_cast<RenderTextureDescriptor_t3096142983 *>(__this + 1);
	RenderTextureDescriptor_SetOrClearRenderTextureCreationFlag_m1589951879(_thisAdjusted, ___value0, ___flag1, method);
}
// System.Void UnityEngine.RenderTextureDescriptor::set_sRGB(System.Boolean)
extern "C"  void RenderTextureDescriptor_set_sRGB_m3197683802 (RenderTextureDescriptor_t3096142983 * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		bool L_0 = ___value0;
		RenderTextureDescriptor_SetOrClearRenderTextureCreationFlag_m1589951879(__this, L_0, 4, /*hidden argument*/NULL);
		return;
	}
}
extern "C"  void RenderTextureDescriptor_set_sRGB_m3197683802_AdjustorThunk (RuntimeObject * __this, bool ___value0, const RuntimeMethod* method)
{
	RenderTextureDescriptor_t3096142983 * _thisAdjusted = reinterpret_cast<RenderTextureDescriptor_t3096142983 *>(__this + 1);
	RenderTextureDescriptor_set_sRGB_m3197683802(_thisAdjusted, ___value0, method);
}
// System.Void UnityEngine.RenderTextureDescriptor::set_createdFromScript(System.Boolean)
extern "C"  void RenderTextureDescriptor_set_createdFromScript_m3731502648 (RenderTextureDescriptor_t3096142983 * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		bool L_0 = ___value0;
		RenderTextureDescriptor_SetOrClearRenderTextureCreationFlag_m1589951879(__this, L_0, ((int32_t)32), /*hidden argument*/NULL);
		return;
	}
}
extern "C"  void RenderTextureDescriptor_set_createdFromScript_m3731502648_AdjustorThunk (RuntimeObject * __this, bool ___value0, const RuntimeMethod* method)
{
	RenderTextureDescriptor_t3096142983 * _thisAdjusted = reinterpret_cast<RenderTextureDescriptor_t3096142983 *>(__this + 1);
	RenderTextureDescriptor_set_createdFromScript_m3731502648(_thisAdjusted, ___value0, method);
}
// System.Void UnityEngine.RenderTextureDescriptor::.cctor()
extern "C"  void RenderTextureDescriptor__cctor_m1159501317 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RenderTextureDescriptor__cctor_m1159501317_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Int32U5BU5D_t2324750880* L_0 = ((Int32U5BU5D_t2324750880*)SZArrayNew(Int32U5BU5D_t2324750880_il2cpp_TypeInfo_var, (uint32_t)3));
		NullCheck(L_0);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(1), (int32_t)((int32_t)16));
		Int32U5BU5D_t2324750880* L_1 = L_0;
		NullCheck(L_1);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(2), (int32_t)((int32_t)24));
		((RenderTextureDescriptor_t3096142983_StaticFields*)il2cpp_codegen_static_fields_for(RenderTextureDescriptor_t3096142983_il2cpp_TypeInfo_var))->set_depthFormatBits_6(L_1);
		return;
	}
}
// System.Void UnityEngine.RequireComponent::.ctor(System.Type)
extern "C"  void RequireComponent__ctor_m4226013883 (RequireComponent_t2076945239 * __this, Type_t * ___requiredComponent0, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m1283917294(__this, /*hidden argument*/NULL);
		Type_t * L_0 = ___requiredComponent0;
		__this->set_m_Type0_0(L_0);
		return;
	}
}
// Conversion methods for marshalling of: UnityEngine.ResourceRequest
extern "C" void ResourceRequest_t963543698_marshal_pinvoke(const ResourceRequest_t963543698& unmarshaled, ResourceRequest_t963543698_marshaled_pinvoke& marshaled)
{
	Il2CppCodeGenException* ___m_Type_2Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_Type' of type 'ResourceRequest': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_Type_2Exception);
}
extern "C" void ResourceRequest_t963543698_marshal_pinvoke_back(const ResourceRequest_t963543698_marshaled_pinvoke& marshaled, ResourceRequest_t963543698& unmarshaled)
{
	Il2CppCodeGenException* ___m_Type_2Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_Type' of type 'ResourceRequest': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_Type_2Exception);
}
// Conversion method for clean up from marshalling of: UnityEngine.ResourceRequest
extern "C" void ResourceRequest_t963543698_marshal_pinvoke_cleanup(ResourceRequest_t963543698_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.ResourceRequest
extern "C" void ResourceRequest_t963543698_marshal_com(const ResourceRequest_t963543698& unmarshaled, ResourceRequest_t963543698_marshaled_com& marshaled)
{
	Il2CppCodeGenException* ___m_Type_2Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_Type' of type 'ResourceRequest': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_Type_2Exception);
}
extern "C" void ResourceRequest_t963543698_marshal_com_back(const ResourceRequest_t963543698_marshaled_com& marshaled, ResourceRequest_t963543698& unmarshaled)
{
	Il2CppCodeGenException* ___m_Type_2Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_Type' of type 'ResourceRequest': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_Type_2Exception);
}
// Conversion method for clean up from marshalling of: UnityEngine.ResourceRequest
extern "C" void ResourceRequest_t963543698_marshal_com_cleanup(ResourceRequest_t963543698_marshaled_com& marshaled)
{
}
// System.Void UnityEngine.ResourceRequest::.ctor()
extern "C"  void ResourceRequest__ctor_m3089335929 (ResourceRequest_t963543698 * __this, const RuntimeMethod* method)
{
	{
		AsyncOperation__ctor_m3138147554(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Object UnityEngine.ResourceRequest::get_asset()
extern "C"  Object_t1502412432 * ResourceRequest_get_asset_m3110762693 (ResourceRequest_t963543698 * __this, const RuntimeMethod* method)
{
	Object_t1502412432 * V_0 = NULL;
	{
		String_t* L_0 = __this->get_m_Path_1();
		Type_t * L_1 = __this->get_m_Type_2();
		Object_t1502412432 * L_2 = Resources_Load_m1847693841(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_0018;
	}

IL_0018:
	{
		Object_t1502412432 * L_3 = V_0;
		return L_3;
	}
}
// UnityEngine.Object UnityEngine.Resources::Load(System.String)
extern "C"  Object_t1502412432 * Resources_Load_m2818004787 (RuntimeObject * __this /* static, unused */, String_t* ___path0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Resources_Load_m2818004787_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Object_t1502412432 * V_0 = NULL;
	{
		String_t* L_0 = ___path0;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_1 = Type_GetTypeFromHandle_m269683661(NULL /*static, unused*/, LoadTypeToken(Object_t1502412432_0_0_0_var), /*hidden argument*/NULL);
		Object_t1502412432 * L_2 = Resources_Load_m1847693841(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_0017;
	}

IL_0017:
	{
		Object_t1502412432 * L_3 = V_0;
		return L_3;
	}
}
// UnityEngine.Object UnityEngine.Resources::Load(System.String,System.Type)
extern "C"  Object_t1502412432 * Resources_Load_m1847693841 (RuntimeObject * __this /* static, unused */, String_t* ___path0, Type_t * ___systemTypeInstance1, const RuntimeMethod* method)
{
	typedef Object_t1502412432 * (*Resources_Load_m1847693841_ftn) (String_t*, Type_t *);
	static Resources_Load_m1847693841_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Resources_Load_m1847693841_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Resources::Load(System.String,System.Type)");
	Object_t1502412432 * retVal = _il2cpp_icall_func(___path0, ___systemTypeInstance1);
	return retVal;
}
// UnityEngine.Object UnityEngine.Resources::GetBuiltinResource(System.Type,System.String)
extern "C"  Object_t1502412432 * Resources_GetBuiltinResource_m3950617287 (RuntimeObject * __this /* static, unused */, Type_t * ___type0, String_t* ___path1, const RuntimeMethod* method)
{
	typedef Object_t1502412432 * (*Resources_GetBuiltinResource_m3950617287_ftn) (Type_t *, String_t*);
	static Resources_GetBuiltinResource_m3950617287_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Resources_GetBuiltinResource_m3950617287_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Resources::GetBuiltinResource(System.Type,System.String)");
	Object_t1502412432 * retVal = _il2cpp_icall_func(___type0, ___path1);
	return retVal;
}
// UnityEngine.AsyncOperation UnityEngine.Resources::UnloadUnusedAssets()
extern "C"  AsyncOperation_t2744032732 * Resources_UnloadUnusedAssets_m2370323369 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	typedef AsyncOperation_t2744032732 * (*Resources_UnloadUnusedAssets_m2370323369_ftn) ();
	static Resources_UnloadUnusedAssets_m2370323369_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Resources_UnloadUnusedAssets_m2370323369_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Resources::UnloadUnusedAssets()");
	AsyncOperation_t2744032732 * retVal = _il2cpp_icall_func();
	return retVal;
}
// UnityEngine.Vector3 UnityEngine.Rigidbody::get_velocity()
extern "C"  Vector3_t2903530434  Rigidbody_get_velocity_m2616337628 (Rigidbody_t2492269564 * __this, const RuntimeMethod* method)
{
	Vector3_t2903530434  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector3_t2903530434  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		Rigidbody_INTERNAL_get_velocity_m3020139032(__this, (&V_0), /*hidden argument*/NULL);
		Vector3_t2903530434  L_0 = V_0;
		V_1 = L_0;
		goto IL_0010;
	}

IL_0010:
	{
		Vector3_t2903530434  L_1 = V_1;
		return L_1;
	}
}
// System.Void UnityEngine.Rigidbody::set_velocity(UnityEngine.Vector3)
extern "C"  void Rigidbody_set_velocity_m3845336739 (Rigidbody_t2492269564 * __this, Vector3_t2903530434  ___value0, const RuntimeMethod* method)
{
	{
		Rigidbody_INTERNAL_set_velocity_m485389597(__this, (&___value0), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Rigidbody::INTERNAL_get_velocity(UnityEngine.Vector3&)
extern "C"  void Rigidbody_INTERNAL_get_velocity_m3020139032 (Rigidbody_t2492269564 * __this, Vector3_t2903530434 * ___value0, const RuntimeMethod* method)
{
	typedef void (*Rigidbody_INTERNAL_get_velocity_m3020139032_ftn) (Rigidbody_t2492269564 *, Vector3_t2903530434 *);
	static Rigidbody_INTERNAL_get_velocity_m3020139032_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody_INTERNAL_get_velocity_m3020139032_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody::INTERNAL_get_velocity(UnityEngine.Vector3&)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.Rigidbody::INTERNAL_set_velocity(UnityEngine.Vector3&)
extern "C"  void Rigidbody_INTERNAL_set_velocity_m485389597 (Rigidbody_t2492269564 * __this, Vector3_t2903530434 * ___value0, const RuntimeMethod* method)
{
	typedef void (*Rigidbody_INTERNAL_set_velocity_m485389597_ftn) (Rigidbody_t2492269564 *, Vector3_t2903530434 *);
	static Rigidbody_INTERNAL_set_velocity_m485389597_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody_INTERNAL_set_velocity_m485389597_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody::INTERNAL_set_velocity(UnityEngine.Vector3&)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.Rigidbody::set_angularVelocity(UnityEngine.Vector3)
extern "C"  void Rigidbody_set_angularVelocity_m3250933960 (Rigidbody_t2492269564 * __this, Vector3_t2903530434  ___value0, const RuntimeMethod* method)
{
	{
		Rigidbody_INTERNAL_set_angularVelocity_m2459330916(__this, (&___value0), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Rigidbody::INTERNAL_set_angularVelocity(UnityEngine.Vector3&)
extern "C"  void Rigidbody_INTERNAL_set_angularVelocity_m2459330916 (Rigidbody_t2492269564 * __this, Vector3_t2903530434 * ___value0, const RuntimeMethod* method)
{
	typedef void (*Rigidbody_INTERNAL_set_angularVelocity_m2459330916_ftn) (Rigidbody_t2492269564 *, Vector3_t2903530434 *);
	static Rigidbody_INTERNAL_set_angularVelocity_m2459330916_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody_INTERNAL_set_angularVelocity_m2459330916_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody::INTERNAL_set_angularVelocity(UnityEngine.Vector3&)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Single UnityEngine.Rigidbody::get_drag()
extern "C"  float Rigidbody_get_drag_m2984746846 (Rigidbody_t2492269564 * __this, const RuntimeMethod* method)
{
	typedef float (*Rigidbody_get_drag_m2984746846_ftn) (Rigidbody_t2492269564 *);
	static Rigidbody_get_drag_m2984746846_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody_get_drag_m2984746846_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody::get_drag()");
	float retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// System.Void UnityEngine.Rigidbody::set_drag(System.Single)
extern "C"  void Rigidbody_set_drag_m3687907778 (Rigidbody_t2492269564 * __this, float ___value0, const RuntimeMethod* method)
{
	typedef void (*Rigidbody_set_drag_m3687907778_ftn) (Rigidbody_t2492269564 *, float);
	static Rigidbody_set_drag_m3687907778_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody_set_drag_m3687907778_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody::set_drag(System.Single)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Single UnityEngine.Rigidbody::get_angularDrag()
extern "C"  float Rigidbody_get_angularDrag_m2407319573 (Rigidbody_t2492269564 * __this, const RuntimeMethod* method)
{
	typedef float (*Rigidbody_get_angularDrag_m2407319573_ftn) (Rigidbody_t2492269564 *);
	static Rigidbody_get_angularDrag_m2407319573_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody_get_angularDrag_m2407319573_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody::get_angularDrag()");
	float retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// System.Void UnityEngine.Rigidbody::set_angularDrag(System.Single)
extern "C"  void Rigidbody_set_angularDrag_m2827532541 (Rigidbody_t2492269564 * __this, float ___value0, const RuntimeMethod* method)
{
	typedef void (*Rigidbody_set_angularDrag_m2827532541_ftn) (Rigidbody_t2492269564 *, float);
	static Rigidbody_set_angularDrag_m2827532541_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody_set_angularDrag_m2827532541_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody::set_angularDrag(System.Single)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Boolean UnityEngine.Rigidbody::get_isKinematic()
extern "C"  bool Rigidbody_get_isKinematic_m1932857723 (Rigidbody_t2492269564 * __this, const RuntimeMethod* method)
{
	typedef bool (*Rigidbody_get_isKinematic_m1932857723_ftn) (Rigidbody_t2492269564 *);
	static Rigidbody_get_isKinematic_m1932857723_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody_get_isKinematic_m1932857723_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody::get_isKinematic()");
	bool retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// System.Void UnityEngine.Rigidbody::set_isKinematic(System.Boolean)
extern "C"  void Rigidbody_set_isKinematic_m3364155693 (Rigidbody_t2492269564 * __this, bool ___value0, const RuntimeMethod* method)
{
	typedef void (*Rigidbody_set_isKinematic_m3364155693_ftn) (Rigidbody_t2492269564 *, bool);
	static Rigidbody_set_isKinematic_m3364155693_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody_set_isKinematic_m3364155693_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody::set_isKinematic(System.Boolean)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.Rigidbody::set_constraints(UnityEngine.RigidbodyConstraints)
extern "C"  void Rigidbody_set_constraints_m1427108748 (Rigidbody_t2492269564 * __this, int32_t ___value0, const RuntimeMethod* method)
{
	typedef void (*Rigidbody_set_constraints_m1427108748_ftn) (Rigidbody_t2492269564 *, int32_t);
	static Rigidbody_set_constraints_m1427108748_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody_set_constraints_m1427108748_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody::set_constraints(UnityEngine.RigidbodyConstraints)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.Rigidbody::AddForce(UnityEngine.Vector3,UnityEngine.ForceMode)
extern "C"  void Rigidbody_AddForce_m3379352602 (Rigidbody_t2492269564 * __this, Vector3_t2903530434  ___force0, int32_t ___mode1, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___mode1;
		Rigidbody_INTERNAL_CALL_AddForce_m3048330967(NULL /*static, unused*/, __this, (&___force0), L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Rigidbody::AddForce(UnityEngine.Vector3)
extern "C"  void Rigidbody_AddForce_m1853670777 (Rigidbody_t2492269564 * __this, Vector3_t2903530434  ___force0, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		V_0 = 0;
		int32_t L_0 = V_0;
		Rigidbody_INTERNAL_CALL_AddForce_m3048330967(NULL /*static, unused*/, __this, (&___force0), L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Rigidbody::INTERNAL_CALL_AddForce(UnityEngine.Rigidbody,UnityEngine.Vector3&,UnityEngine.ForceMode)
extern "C"  void Rigidbody_INTERNAL_CALL_AddForce_m3048330967 (RuntimeObject * __this /* static, unused */, Rigidbody_t2492269564 * ___self0, Vector3_t2903530434 * ___force1, int32_t ___mode2, const RuntimeMethod* method)
{
	typedef void (*Rigidbody_INTERNAL_CALL_AddForce_m3048330967_ftn) (Rigidbody_t2492269564 *, Vector3_t2903530434 *, int32_t);
	static Rigidbody_INTERNAL_CALL_AddForce_m3048330967_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody_INTERNAL_CALL_AddForce_m3048330967_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody::INTERNAL_CALL_AddForce(UnityEngine.Rigidbody,UnityEngine.Vector3&,UnityEngine.ForceMode)");
	_il2cpp_icall_func(___self0, ___force1, ___mode2);
}
// System.Void UnityEngine.Rigidbody::AddTorque(UnityEngine.Vector3)
extern "C"  void Rigidbody_AddTorque_m1584258046 (Rigidbody_t2492269564 * __this, Vector3_t2903530434  ___torque0, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		V_0 = 0;
		int32_t L_0 = V_0;
		Rigidbody_INTERNAL_CALL_AddTorque_m2788891887(NULL /*static, unused*/, __this, (&___torque0), L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Rigidbody::INTERNAL_CALL_AddTorque(UnityEngine.Rigidbody,UnityEngine.Vector3&,UnityEngine.ForceMode)
extern "C"  void Rigidbody_INTERNAL_CALL_AddTorque_m2788891887 (RuntimeObject * __this /* static, unused */, Rigidbody_t2492269564 * ___self0, Vector3_t2903530434 * ___torque1, int32_t ___mode2, const RuntimeMethod* method)
{
	typedef void (*Rigidbody_INTERNAL_CALL_AddTorque_m2788891887_ftn) (Rigidbody_t2492269564 *, Vector3_t2903530434 *, int32_t);
	static Rigidbody_INTERNAL_CALL_AddTorque_m2788891887_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody_INTERNAL_CALL_AddTorque_m2788891887_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody::INTERNAL_CALL_AddTorque(UnityEngine.Rigidbody,UnityEngine.Vector3&,UnityEngine.ForceMode)");
	_il2cpp_icall_func(___self0, ___torque1, ___mode2);
}
// System.Void UnityEngine.Rigidbody::AddForceAtPosition(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.ForceMode)
extern "C"  void Rigidbody_AddForceAtPosition_m2366195222 (Rigidbody_t2492269564 * __this, Vector3_t2903530434  ___force0, Vector3_t2903530434  ___position1, int32_t ___mode2, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___mode2;
		Rigidbody_INTERNAL_CALL_AddForceAtPosition_m517980457(NULL /*static, unused*/, __this, (&___force0), (&___position1), L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Rigidbody::INTERNAL_CALL_AddForceAtPosition(UnityEngine.Rigidbody,UnityEngine.Vector3&,UnityEngine.Vector3&,UnityEngine.ForceMode)
extern "C"  void Rigidbody_INTERNAL_CALL_AddForceAtPosition_m517980457 (RuntimeObject * __this /* static, unused */, Rigidbody_t2492269564 * ___self0, Vector3_t2903530434 * ___force1, Vector3_t2903530434 * ___position2, int32_t ___mode3, const RuntimeMethod* method)
{
	typedef void (*Rigidbody_INTERNAL_CALL_AddForceAtPosition_m517980457_ftn) (Rigidbody_t2492269564 *, Vector3_t2903530434 *, Vector3_t2903530434 *, int32_t);
	static Rigidbody_INTERNAL_CALL_AddForceAtPosition_m517980457_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody_INTERNAL_CALL_AddForceAtPosition_m517980457_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody::INTERNAL_CALL_AddForceAtPosition(UnityEngine.Rigidbody,UnityEngine.Vector3&,UnityEngine.Vector3&,UnityEngine.ForceMode)");
	_il2cpp_icall_func(___self0, ___force1, ___position2, ___mode3);
}
// UnityEngine.Vector3 UnityEngine.Rigidbody::get_position()
extern "C"  Vector3_t2903530434  Rigidbody_get_position_m3391570614 (Rigidbody_t2492269564 * __this, const RuntimeMethod* method)
{
	Vector3_t2903530434  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector3_t2903530434  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		Rigidbody_INTERNAL_get_position_m3982538921(__this, (&V_0), /*hidden argument*/NULL);
		Vector3_t2903530434  L_0 = V_0;
		V_1 = L_0;
		goto IL_0010;
	}

IL_0010:
	{
		Vector3_t2903530434  L_1 = V_1;
		return L_1;
	}
}
// System.Void UnityEngine.Rigidbody::INTERNAL_get_position(UnityEngine.Vector3&)
extern "C"  void Rigidbody_INTERNAL_get_position_m3982538921 (Rigidbody_t2492269564 * __this, Vector3_t2903530434 * ___value0, const RuntimeMethod* method)
{
	typedef void (*Rigidbody_INTERNAL_get_position_m3982538921_ftn) (Rigidbody_t2492269564 *, Vector3_t2903530434 *);
	static Rigidbody_INTERNAL_get_position_m3982538921_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody_INTERNAL_get_position_m3982538921_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody::INTERNAL_get_position(UnityEngine.Vector3&)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.Rigidbody::Sleep()
extern "C"  void Rigidbody_Sleep_m966578025 (Rigidbody_t2492269564 * __this, const RuntimeMethod* method)
{
	{
		Rigidbody_INTERNAL_CALL_Sleep_m3183242862(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Rigidbody::INTERNAL_CALL_Sleep(UnityEngine.Rigidbody)
extern "C"  void Rigidbody_INTERNAL_CALL_Sleep_m3183242862 (RuntimeObject * __this /* static, unused */, Rigidbody_t2492269564 * ___self0, const RuntimeMethod* method)
{
	typedef void (*Rigidbody_INTERNAL_CALL_Sleep_m3183242862_ftn) (Rigidbody_t2492269564 *);
	static Rigidbody_INTERNAL_CALL_Sleep_m3183242862_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody_INTERNAL_CALL_Sleep_m3183242862_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody::INTERNAL_CALL_Sleep(UnityEngine.Rigidbody)");
	_il2cpp_icall_func(___self0);
}
// System.Void UnityEngine.Rigidbody::set_maxAngularVelocity(System.Single)
extern "C"  void Rigidbody_set_maxAngularVelocity_m3943773378 (Rigidbody_t2492269564 * __this, float ___value0, const RuntimeMethod* method)
{
	typedef void (*Rigidbody_set_maxAngularVelocity_m3943773378_ftn) (Rigidbody_t2492269564 *, float);
	static Rigidbody_set_maxAngularVelocity_m3943773378_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody_set_maxAngularVelocity_m3943773378_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody::set_maxAngularVelocity(System.Single)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.RPC::.ctor()
extern "C"  void RPC__ctor_m659581914 (RPC_t1601042701 * __this, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m1283917294(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.RuntimeInitializeOnLoadMethodAttribute::.ctor(UnityEngine.RuntimeInitializeLoadType)
extern "C"  void RuntimeInitializeOnLoadMethodAttribute__ctor_m3217827693 (RuntimeInitializeOnLoadMethodAttribute_t1540303992 * __this, int32_t ___loadType0, const RuntimeMethod* method)
{
	{
		PreserveAttribute__ctor_m237382176(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___loadType0;
		RuntimeInitializeOnLoadMethodAttribute_set_loadType_m128702405(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.RuntimeInitializeOnLoadMethodAttribute::set_loadType(UnityEngine.RuntimeInitializeLoadType)
extern "C"  void RuntimeInitializeOnLoadMethodAttribute_set_loadType_m128702405 (RuntimeInitializeOnLoadMethodAttribute_t1540303992 * __this, int32_t ___value0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_U3CloadTypeU3Ek__BackingField_0(L_0);
		return;
	}
}
// System.Int32 UnityEngine.SceneManagement.Scene::get_handle()
extern "C"  int32_t Scene_get_handle_m215941749 (Scene_t348657329 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_m_Handle_0();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		int32_t L_1 = V_0;
		return L_1;
	}
}
extern "C"  int32_t Scene_get_handle_m215941749_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Scene_t348657329 * _thisAdjusted = reinterpret_cast<Scene_t348657329 *>(__this + 1);
	return Scene_get_handle_m215941749(_thisAdjusted, method);
}
// System.String UnityEngine.SceneManagement.Scene::get_name()
extern "C"  String_t* Scene_get_name_m928930484 (Scene_t348657329 * __this, const RuntimeMethod* method)
{
	String_t* V_0 = NULL;
	{
		int32_t L_0 = Scene_get_handle_m215941749(__this, /*hidden argument*/NULL);
		String_t* L_1 = Scene_GetNameInternal_m2651924806(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_0012;
	}

IL_0012:
	{
		String_t* L_2 = V_0;
		return L_2;
	}
}
extern "C"  String_t* Scene_get_name_m928930484_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Scene_t348657329 * _thisAdjusted = reinterpret_cast<Scene_t348657329 *>(__this + 1);
	return Scene_get_name_m928930484(_thisAdjusted, method);
}
// System.Int32 UnityEngine.SceneManagement.Scene::GetHashCode()
extern "C"  int32_t Scene_GetHashCode_m1444517911 (Scene_t348657329 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_m_Handle_0();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		int32_t L_1 = V_0;
		return L_1;
	}
}
extern "C"  int32_t Scene_GetHashCode_m1444517911_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Scene_t348657329 * _thisAdjusted = reinterpret_cast<Scene_t348657329 *>(__this + 1);
	return Scene_GetHashCode_m1444517911(_thisAdjusted, method);
}
// System.Boolean UnityEngine.SceneManagement.Scene::Equals(System.Object)
extern "C"  bool Scene_Equals_m714854343 (Scene_t348657329 * __this, RuntimeObject * ___other0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Scene_Equals_m714854343_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	Scene_t348657329  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		RuntimeObject * L_0 = ___other0;
		if (((RuntimeObject *)IsInstSealed((RuntimeObject*)L_0, Scene_t348657329_il2cpp_TypeInfo_var)))
		{
			goto IL_0013;
		}
	}
	{
		V_0 = (bool)0;
		goto IL_002f;
	}

IL_0013:
	{
		RuntimeObject * L_1 = ___other0;
		V_1 = ((*(Scene_t348657329 *)((Scene_t348657329 *)UnBox(L_1, Scene_t348657329_il2cpp_TypeInfo_var))));
		int32_t L_2 = Scene_get_handle_m215941749(__this, /*hidden argument*/NULL);
		int32_t L_3 = Scene_get_handle_m215941749((&V_1), /*hidden argument*/NULL);
		V_0 = (bool)((((int32_t)L_2) == ((int32_t)L_3))? 1 : 0);
		goto IL_002f;
	}

IL_002f:
	{
		bool L_4 = V_0;
		return L_4;
	}
}
extern "C"  bool Scene_Equals_m714854343_AdjustorThunk (RuntimeObject * __this, RuntimeObject * ___other0, const RuntimeMethod* method)
{
	Scene_t348657329 * _thisAdjusted = reinterpret_cast<Scene_t348657329 *>(__this + 1);
	return Scene_Equals_m714854343(_thisAdjusted, ___other0, method);
}
// System.String UnityEngine.SceneManagement.Scene::GetNameInternal(System.Int32)
extern "C"  String_t* Scene_GetNameInternal_m2651924806 (RuntimeObject * __this /* static, unused */, int32_t ___sceneHandle0, const RuntimeMethod* method)
{
	typedef String_t* (*Scene_GetNameInternal_m2651924806_ftn) (int32_t);
	static Scene_GetNameInternal_m2651924806_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Scene_GetNameInternal_m2651924806_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SceneManagement.Scene::GetNameInternal(System.Int32)");
	String_t* retVal = _il2cpp_icall_func(___sceneHandle0);
	return retVal;
}
// UnityEngine.SceneManagement.Scene UnityEngine.SceneManagement.SceneManager::GetSceneAt(System.Int32)
extern "C"  Scene_t348657329  SceneManager_GetSceneAt_m170461719 (RuntimeObject * __this /* static, unused */, int32_t ___index0, const RuntimeMethod* method)
{
	Scene_t348657329  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Scene_t348657329  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		int32_t L_0 = ___index0;
		SceneManager_INTERNAL_CALL_GetSceneAt_m312730705(NULL /*static, unused*/, L_0, (&V_0), /*hidden argument*/NULL);
		Scene_t348657329  L_1 = V_0;
		V_1 = L_1;
		goto IL_0010;
	}

IL_0010:
	{
		Scene_t348657329  L_2 = V_1;
		return L_2;
	}
}
// System.Void UnityEngine.SceneManagement.SceneManager::INTERNAL_CALL_GetSceneAt(System.Int32,UnityEngine.SceneManagement.Scene&)
extern "C"  void SceneManager_INTERNAL_CALL_GetSceneAt_m312730705 (RuntimeObject * __this /* static, unused */, int32_t ___index0, Scene_t348657329 * ___value1, const RuntimeMethod* method)
{
	typedef void (*SceneManager_INTERNAL_CALL_GetSceneAt_m312730705_ftn) (int32_t, Scene_t348657329 *);
	static SceneManager_INTERNAL_CALL_GetSceneAt_m312730705_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (SceneManager_INTERNAL_CALL_GetSceneAt_m312730705_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SceneManagement.SceneManager::INTERNAL_CALL_GetSceneAt(System.Int32,UnityEngine.SceneManagement.Scene&)");
	_il2cpp_icall_func(___index0, ___value1);
}
// System.Void UnityEngine.SceneManagement.SceneManager::LoadScene(System.String)
extern "C"  void SceneManager_LoadScene_m2180780985 (RuntimeObject * __this /* static, unused */, String_t* ___sceneName0, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		V_0 = 0;
		String_t* L_0 = ___sceneName0;
		int32_t L_1 = V_0;
		SceneManager_LoadScene_m2739829582(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.SceneManagement.SceneManager::LoadScene(System.String,UnityEngine.SceneManagement.LoadSceneMode)
extern "C"  void SceneManager_LoadScene_m2739829582 (RuntimeObject * __this /* static, unused */, String_t* ___sceneName0, int32_t ___mode1, const RuntimeMethod* method)
{
	int32_t G_B2_0 = 0;
	String_t* G_B2_1 = NULL;
	int32_t G_B1_0 = 0;
	String_t* G_B1_1 = NULL;
	int32_t G_B3_0 = 0;
	int32_t G_B3_1 = 0;
	String_t* G_B3_2 = NULL;
	{
		String_t* L_0 = ___sceneName0;
		int32_t L_1 = ___mode1;
		G_B1_0 = (-1);
		G_B1_1 = L_0;
		if ((!(((uint32_t)L_1) == ((uint32_t)1))))
		{
			G_B2_0 = (-1);
			G_B2_1 = L_0;
			goto IL_0010;
		}
	}
	{
		G_B3_0 = 1;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		goto IL_0011;
	}

IL_0010:
	{
		G_B3_0 = 0;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
	}

IL_0011:
	{
		SceneManager_LoadSceneAsyncNameIndexInternal_m2649939818(NULL /*static, unused*/, G_B3_2, G_B3_1, (bool)G_B3_0, (bool)1, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.AsyncOperation UnityEngine.SceneManagement.SceneManager::LoadSceneAsyncNameIndexInternal(System.String,System.Int32,System.Boolean,System.Boolean)
extern "C"  AsyncOperation_t2744032732 * SceneManager_LoadSceneAsyncNameIndexInternal_m2649939818 (RuntimeObject * __this /* static, unused */, String_t* ___sceneName0, int32_t ___sceneBuildIndex1, bool ___isAdditive2, bool ___mustCompleteNextFrame3, const RuntimeMethod* method)
{
	typedef AsyncOperation_t2744032732 * (*SceneManager_LoadSceneAsyncNameIndexInternal_m2649939818_ftn) (String_t*, int32_t, bool, bool);
	static SceneManager_LoadSceneAsyncNameIndexInternal_m2649939818_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (SceneManager_LoadSceneAsyncNameIndexInternal_m2649939818_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SceneManagement.SceneManager::LoadSceneAsyncNameIndexInternal(System.String,System.Int32,System.Boolean,System.Boolean)");
	AsyncOperation_t2744032732 * retVal = _il2cpp_icall_func(___sceneName0, ___sceneBuildIndex1, ___isAdditive2, ___mustCompleteNextFrame3);
	return retVal;
}
// System.Void UnityEngine.SceneManagement.SceneManager::Internal_SceneLoaded(UnityEngine.SceneManagement.Scene,UnityEngine.SceneManagement.LoadSceneMode)
extern "C"  void SceneManager_Internal_SceneLoaded_m3575317911 (RuntimeObject * __this /* static, unused */, Scene_t348657329  ___scene0, int32_t ___mode1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SceneManager_Internal_SceneLoaded_m3575317911_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		UnityAction_2_t2064023076 * L_0 = ((SceneManager_t1729095165_StaticFields*)il2cpp_codegen_static_fields_for(SceneManager_t1729095165_il2cpp_TypeInfo_var))->get_sceneLoaded_0();
		if (!L_0)
		{
			goto IL_0019;
		}
	}
	{
		UnityAction_2_t2064023076 * L_1 = ((SceneManager_t1729095165_StaticFields*)il2cpp_codegen_static_fields_for(SceneManager_t1729095165_il2cpp_TypeInfo_var))->get_sceneLoaded_0();
		Scene_t348657329  L_2 = ___scene0;
		int32_t L_3 = ___mode1;
		NullCheck(L_1);
		UnityAction_2_Invoke_m2774749000(L_1, L_2, L_3, /*hidden argument*/UnityAction_2_Invoke_m2774749000_RuntimeMethod_var);
	}

IL_0019:
	{
		return;
	}
}
// System.Void UnityEngine.SceneManagement.SceneManager::Internal_SceneUnloaded(UnityEngine.SceneManagement.Scene)
extern "C"  void SceneManager_Internal_SceneUnloaded_m2896774791 (RuntimeObject * __this /* static, unused */, Scene_t348657329  ___scene0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SceneManager_Internal_SceneUnloaded_m2896774791_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		UnityAction_1_t3896475417 * L_0 = ((SceneManager_t1729095165_StaticFields*)il2cpp_codegen_static_fields_for(SceneManager_t1729095165_il2cpp_TypeInfo_var))->get_sceneUnloaded_1();
		if (!L_0)
		{
			goto IL_0018;
		}
	}
	{
		UnityAction_1_t3896475417 * L_1 = ((SceneManager_t1729095165_StaticFields*)il2cpp_codegen_static_fields_for(SceneManager_t1729095165_il2cpp_TypeInfo_var))->get_sceneUnloaded_1();
		Scene_t348657329  L_2 = ___scene0;
		NullCheck(L_1);
		UnityAction_1_Invoke_m2393581145(L_1, L_2, /*hidden argument*/UnityAction_1_Invoke_m2393581145_RuntimeMethod_var);
	}

IL_0018:
	{
		return;
	}
}
// System.Void UnityEngine.SceneManagement.SceneManager::Internal_ActiveSceneChanged(UnityEngine.SceneManagement.Scene,UnityEngine.SceneManagement.Scene)
extern "C"  void SceneManager_Internal_ActiveSceneChanged_m1556810644 (RuntimeObject * __this /* static, unused */, Scene_t348657329  ___previousActiveScene0, Scene_t348657329  ___newActiveScene1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SceneManager_Internal_ActiveSceneChanged_m1556810644_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		UnityAction_2_t121587842 * L_0 = ((SceneManager_t1729095165_StaticFields*)il2cpp_codegen_static_fields_for(SceneManager_t1729095165_il2cpp_TypeInfo_var))->get_activeSceneChanged_2();
		if (!L_0)
		{
			goto IL_0019;
		}
	}
	{
		UnityAction_2_t121587842 * L_1 = ((SceneManager_t1729095165_StaticFields*)il2cpp_codegen_static_fields_for(SceneManager_t1729095165_il2cpp_TypeInfo_var))->get_activeSceneChanged_2();
		Scene_t348657329  L_2 = ___previousActiveScene0;
		Scene_t348657329  L_3 = ___newActiveScene1;
		NullCheck(L_1);
		UnityAction_2_Invoke_m3185242217(L_1, L_2, L_3, /*hidden argument*/UnityAction_2_Invoke_m3185242217_RuntimeMethod_var);
	}

IL_0019:
	{
		return;
	}
}
// System.Int32 UnityEngine.Screen::get_width()
extern "C"  int32_t Screen_get_width_m2947011129 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	typedef int32_t (*Screen_get_width_m2947011129_ftn) ();
	static Screen_get_width_m2947011129_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Screen_get_width_m2947011129_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Screen::get_width()");
	int32_t retVal = _il2cpp_icall_func();
	return retVal;
}
// System.Int32 UnityEngine.Screen::get_height()
extern "C"  int32_t Screen_get_height_m471732428 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	typedef int32_t (*Screen_get_height_m471732428_ftn) ();
	static Screen_get_height_m471732428_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Screen_get_height_m471732428_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Screen::get_height()");
	int32_t retVal = _il2cpp_icall_func();
	return retVal;
}
// System.Single UnityEngine.Screen::get_dpi()
extern "C"  float Screen_get_dpi_m745235119 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	typedef float (*Screen_get_dpi_m745235119_ftn) ();
	static Screen_get_dpi_m745235119_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Screen_get_dpi_m745235119_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Screen::get_dpi()");
	float retVal = _il2cpp_icall_func();
	return retVal;
}
// System.Boolean UnityEngine.Screen::get_autorotateToPortrait()
extern "C"  bool Screen_get_autorotateToPortrait_m2627354279 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	typedef bool (*Screen_get_autorotateToPortrait_m2627354279_ftn) ();
	static Screen_get_autorotateToPortrait_m2627354279_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Screen_get_autorotateToPortrait_m2627354279_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Screen::get_autorotateToPortrait()");
	bool retVal = _il2cpp_icall_func();
	return retVal;
}
// System.Void UnityEngine.Screen::set_autorotateToPortrait(System.Boolean)
extern "C"  void Screen_set_autorotateToPortrait_m1613327744 (RuntimeObject * __this /* static, unused */, bool ___value0, const RuntimeMethod* method)
{
	typedef void (*Screen_set_autorotateToPortrait_m1613327744_ftn) (bool);
	static Screen_set_autorotateToPortrait_m1613327744_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Screen_set_autorotateToPortrait_m1613327744_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Screen::set_autorotateToPortrait(System.Boolean)");
	_il2cpp_icall_func(___value0);
}
// System.Boolean UnityEngine.Screen::get_autorotateToPortraitUpsideDown()
extern "C"  bool Screen_get_autorotateToPortraitUpsideDown_m2979843367 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	typedef bool (*Screen_get_autorotateToPortraitUpsideDown_m2979843367_ftn) ();
	static Screen_get_autorotateToPortraitUpsideDown_m2979843367_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Screen_get_autorotateToPortraitUpsideDown_m2979843367_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Screen::get_autorotateToPortraitUpsideDown()");
	bool retVal = _il2cpp_icall_func();
	return retVal;
}
// System.Void UnityEngine.Screen::set_autorotateToPortraitUpsideDown(System.Boolean)
extern "C"  void Screen_set_autorotateToPortraitUpsideDown_m775657441 (RuntimeObject * __this /* static, unused */, bool ___value0, const RuntimeMethod* method)
{
	typedef void (*Screen_set_autorotateToPortraitUpsideDown_m775657441_ftn) (bool);
	static Screen_set_autorotateToPortraitUpsideDown_m775657441_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Screen_set_autorotateToPortraitUpsideDown_m775657441_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Screen::set_autorotateToPortraitUpsideDown(System.Boolean)");
	_il2cpp_icall_func(___value0);
}
// System.Boolean UnityEngine.Screen::get_autorotateToLandscapeLeft()
extern "C"  bool Screen_get_autorotateToLandscapeLeft_m651746765 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	typedef bool (*Screen_get_autorotateToLandscapeLeft_m651746765_ftn) ();
	static Screen_get_autorotateToLandscapeLeft_m651746765_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Screen_get_autorotateToLandscapeLeft_m651746765_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Screen::get_autorotateToLandscapeLeft()");
	bool retVal = _il2cpp_icall_func();
	return retVal;
}
// System.Void UnityEngine.Screen::set_autorotateToLandscapeLeft(System.Boolean)
extern "C"  void Screen_set_autorotateToLandscapeLeft_m3210878214 (RuntimeObject * __this /* static, unused */, bool ___value0, const RuntimeMethod* method)
{
	typedef void (*Screen_set_autorotateToLandscapeLeft_m3210878214_ftn) (bool);
	static Screen_set_autorotateToLandscapeLeft_m3210878214_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Screen_set_autorotateToLandscapeLeft_m3210878214_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Screen::set_autorotateToLandscapeLeft(System.Boolean)");
	_il2cpp_icall_func(___value0);
}
// System.Boolean UnityEngine.Screen::get_autorotateToLandscapeRight()
extern "C"  bool Screen_get_autorotateToLandscapeRight_m1978526379 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	typedef bool (*Screen_get_autorotateToLandscapeRight_m1978526379_ftn) ();
	static Screen_get_autorotateToLandscapeRight_m1978526379_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Screen_get_autorotateToLandscapeRight_m1978526379_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Screen::get_autorotateToLandscapeRight()");
	bool retVal = _il2cpp_icall_func();
	return retVal;
}
// System.Void UnityEngine.Screen::set_autorotateToLandscapeRight(System.Boolean)
extern "C"  void Screen_set_autorotateToLandscapeRight_m813638257 (RuntimeObject * __this /* static, unused */, bool ___value0, const RuntimeMethod* method)
{
	typedef void (*Screen_set_autorotateToLandscapeRight_m813638257_ftn) (bool);
	static Screen_set_autorotateToLandscapeRight_m813638257_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Screen_set_autorotateToLandscapeRight_m813638257_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Screen::set_autorotateToLandscapeRight(System.Boolean)");
	_il2cpp_icall_func(___value0);
}
// UnityEngine.ScreenOrientation UnityEngine.Screen::get_orientation()
extern "C"  int32_t Screen_get_orientation_m251094311 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	typedef int32_t (*Screen_get_orientation_m251094311_ftn) ();
	static Screen_get_orientation_m251094311_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Screen_get_orientation_m251094311_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Screen::get_orientation()");
	int32_t retVal = _il2cpp_icall_func();
	return retVal;
}
// System.Void UnityEngine.Screen::set_orientation(UnityEngine.ScreenOrientation)
extern "C"  void Screen_set_orientation_m3962058374 (RuntimeObject * __this /* static, unused */, int32_t ___value0, const RuntimeMethod* method)
{
	typedef void (*Screen_set_orientation_m3962058374_ftn) (int32_t);
	static Screen_set_orientation_m3962058374_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Screen_set_orientation_m3962058374_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Screen::set_orientation(UnityEngine.ScreenOrientation)");
	_il2cpp_icall_func(___value0);
}
// System.Void UnityEngine.Screen::set_sleepTimeout(System.Int32)
extern "C"  void Screen_set_sleepTimeout_m740916316 (RuntimeObject * __this /* static, unused */, int32_t ___value0, const RuntimeMethod* method)
{
	typedef void (*Screen_set_sleepTimeout_m740916316_ftn) (int32_t);
	static Screen_set_sleepTimeout_m740916316_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Screen_set_sleepTimeout_m740916316_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Screen::set_sleepTimeout(System.Int32)");
	_il2cpp_icall_func(___value0);
}
// Conversion methods for marshalling of: UnityEngine.ScriptableObject
extern "C" void ScriptableObject_t3635579074_marshal_pinvoke(const ScriptableObject_t3635579074& unmarshaled, ScriptableObject_t3635579074_marshaled_pinvoke& marshaled)
{
	marshaled.___m_CachedPtr_0 = reinterpret_cast<intptr_t>((unmarshaled.get_m_CachedPtr_0()).get_m_value_0());
}
extern "C" void ScriptableObject_t3635579074_marshal_pinvoke_back(const ScriptableObject_t3635579074_marshaled_pinvoke& marshaled, ScriptableObject_t3635579074& unmarshaled)
{
	IntPtr_t unmarshaled_m_CachedPtr_temp_0;
	memset(&unmarshaled_m_CachedPtr_temp_0, 0, sizeof(unmarshaled_m_CachedPtr_temp_0));
	IntPtr_t unmarshaled_m_CachedPtr_temp_0_temp;
	unmarshaled_m_CachedPtr_temp_0_temp.set_m_value_0(reinterpret_cast<void*>((intptr_t)(marshaled.___m_CachedPtr_0)));
	unmarshaled_m_CachedPtr_temp_0 = unmarshaled_m_CachedPtr_temp_0_temp;
	unmarshaled.set_m_CachedPtr_0(unmarshaled_m_CachedPtr_temp_0);
}
// Conversion method for clean up from marshalling of: UnityEngine.ScriptableObject
extern "C" void ScriptableObject_t3635579074_marshal_pinvoke_cleanup(ScriptableObject_t3635579074_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.ScriptableObject
extern "C" void ScriptableObject_t3635579074_marshal_com(const ScriptableObject_t3635579074& unmarshaled, ScriptableObject_t3635579074_marshaled_com& marshaled)
{
	marshaled.___m_CachedPtr_0 = reinterpret_cast<intptr_t>((unmarshaled.get_m_CachedPtr_0()).get_m_value_0());
}
extern "C" void ScriptableObject_t3635579074_marshal_com_back(const ScriptableObject_t3635579074_marshaled_com& marshaled, ScriptableObject_t3635579074& unmarshaled)
{
	IntPtr_t unmarshaled_m_CachedPtr_temp_0;
	memset(&unmarshaled_m_CachedPtr_temp_0, 0, sizeof(unmarshaled_m_CachedPtr_temp_0));
	IntPtr_t unmarshaled_m_CachedPtr_temp_0_temp;
	unmarshaled_m_CachedPtr_temp_0_temp.set_m_value_0(reinterpret_cast<void*>((intptr_t)(marshaled.___m_CachedPtr_0)));
	unmarshaled_m_CachedPtr_temp_0 = unmarshaled_m_CachedPtr_temp_0_temp;
	unmarshaled.set_m_CachedPtr_0(unmarshaled_m_CachedPtr_temp_0);
}
// Conversion method for clean up from marshalling of: UnityEngine.ScriptableObject
extern "C" void ScriptableObject_t3635579074_marshal_com_cleanup(ScriptableObject_t3635579074_marshaled_com& marshaled)
{
}
// System.Void UnityEngine.ScriptableObject::.ctor()
extern "C"  void ScriptableObject__ctor_m4110805243 (ScriptableObject_t3635579074 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ScriptableObject__ctor_m4110805243_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1502412432_il2cpp_TypeInfo_var);
		Object__ctor_m3993713475(__this, /*hidden argument*/NULL);
		ScriptableObject_Internal_CreateScriptableObject_m2602267800(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.ScriptableObject::Internal_CreateScriptableObject(UnityEngine.ScriptableObject)
extern "C"  void ScriptableObject_Internal_CreateScriptableObject_m2602267800 (RuntimeObject * __this /* static, unused */, ScriptableObject_t3635579074 * ___self0, const RuntimeMethod* method)
{
	typedef void (*ScriptableObject_Internal_CreateScriptableObject_m2602267800_ftn) (ScriptableObject_t3635579074 *);
	static ScriptableObject_Internal_CreateScriptableObject_m2602267800_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ScriptableObject_Internal_CreateScriptableObject_m2602267800_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ScriptableObject::Internal_CreateScriptableObject(UnityEngine.ScriptableObject)");
	_il2cpp_icall_func(___self0);
}
// UnityEngine.ScriptableObject UnityEngine.ScriptableObject::CreateInstance(System.String)
extern "C"  ScriptableObject_t3635579074 * ScriptableObject_CreateInstance_m2806801399 (RuntimeObject * __this /* static, unused */, String_t* ___className0, const RuntimeMethod* method)
{
	typedef ScriptableObject_t3635579074 * (*ScriptableObject_CreateInstance_m2806801399_ftn) (String_t*);
	static ScriptableObject_CreateInstance_m2806801399_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ScriptableObject_CreateInstance_m2806801399_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ScriptableObject::CreateInstance(System.String)");
	ScriptableObject_t3635579074 * retVal = _il2cpp_icall_func(___className0);
	return retVal;
}
// UnityEngine.ScriptableObject UnityEngine.ScriptableObject::CreateInstance(System.Type)
extern "C"  ScriptableObject_t3635579074 * ScriptableObject_CreateInstance_m1717550480 (RuntimeObject * __this /* static, unused */, Type_t * ___type0, const RuntimeMethod* method)
{
	ScriptableObject_t3635579074 * V_0 = NULL;
	{
		Type_t * L_0 = ___type0;
		ScriptableObject_t3635579074 * L_1 = ScriptableObject_CreateInstanceFromType_m390136550(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_000d;
	}

IL_000d:
	{
		ScriptableObject_t3635579074 * L_2 = V_0;
		return L_2;
	}
}
// UnityEngine.ScriptableObject UnityEngine.ScriptableObject::CreateInstanceFromType(System.Type)
extern "C"  ScriptableObject_t3635579074 * ScriptableObject_CreateInstanceFromType_m390136550 (RuntimeObject * __this /* static, unused */, Type_t * ___type0, const RuntimeMethod* method)
{
	typedef ScriptableObject_t3635579074 * (*ScriptableObject_CreateInstanceFromType_m390136550_ftn) (Type_t *);
	static ScriptableObject_CreateInstanceFromType_m390136550_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ScriptableObject_CreateInstanceFromType_m390136550_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ScriptableObject::CreateInstanceFromType(System.Type)");
	ScriptableObject_t3635579074 * retVal = _il2cpp_icall_func(___type0);
	return retVal;
}
// System.Void UnityEngine.Scripting.APIUpdating.MovedFromAttribute::.ctor(System.String)
extern "C"  void MovedFromAttribute__ctor_m220695169 (MovedFromAttribute_t2669718911 * __this, String_t* ___sourceNamespace0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___sourceNamespace0;
		MovedFromAttribute__ctor_m1751114112(__this, L_0, (bool)0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Scripting.APIUpdating.MovedFromAttribute::.ctor(System.String,System.Boolean)
extern "C"  void MovedFromAttribute__ctor_m1751114112 (MovedFromAttribute_t2669718911 * __this, String_t* ___sourceNamespace0, bool ___isInDifferentAssembly1, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m1283917294(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___sourceNamespace0;
		MovedFromAttribute_set_Namespace_m2723306931(__this, L_0, /*hidden argument*/NULL);
		bool L_1 = ___isInDifferentAssembly1;
		MovedFromAttribute_set_IsInDifferentAssembly_m655900810(__this, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Scripting.APIUpdating.MovedFromAttribute::set_Namespace(System.String)
extern "C"  void MovedFromAttribute_set_Namespace_m2723306931 (MovedFromAttribute_t2669718911 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CNamespaceU3Ek__BackingField_0(L_0);
		return;
	}
}
// System.Void UnityEngine.Scripting.APIUpdating.MovedFromAttribute::set_IsInDifferentAssembly(System.Boolean)
extern "C"  void MovedFromAttribute_set_IsInDifferentAssembly_m655900810 (MovedFromAttribute_t2669718911 * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		bool L_0 = ___value0;
		__this->set_U3CIsInDifferentAssemblyU3Ek__BackingField_1(L_0);
		return;
	}
}
// System.Void UnityEngine.Scripting.GeneratedByOldBindingsGeneratorAttribute::.ctor()
extern "C"  void GeneratedByOldBindingsGeneratorAttribute__ctor_m3746047965 (GeneratedByOldBindingsGeneratorAttribute_t2914791882 * __this, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m1283917294(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Scripting.PreserveAttribute::.ctor()
extern "C"  void PreserveAttribute__ctor_m237382176 (PreserveAttribute_t3468091719 * __this, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m1283917294(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Scripting.RequiredByNativeCodeAttribute::.ctor()
extern "C"  void RequiredByNativeCodeAttribute__ctor_m4021535315 (RequiredByNativeCodeAttribute_t814971270 * __this, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m1283917294(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Scripting.UsedByNativeCodeAttribute::.ctor()
extern "C"  void UsedByNativeCodeAttribute__ctor_m3316007557 (UsedByNativeCodeAttribute_t1334661680 * __this, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m1283917294(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.ScrollViewState::.ctor()
extern "C"  void ScrollViewState__ctor_m1723295242 (ScrollViewState_t1314793348 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m2906060009(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.SelectionBaseAttribute::.ctor()
extern "C"  void SelectionBaseAttribute__ctor_m866174202 (SelectionBaseAttribute_t2246906333 * __this, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m1283917294(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.SendMouseEvents::SetMouseMoved()
extern "C"  void SendMouseEvents_SetMouseMoved_m4214977923 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SendMouseEvents_SetMouseMoved_m4214977923_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t1825902553_il2cpp_TypeInfo_var);
		((SendMouseEvents_t1825902553_StaticFields*)il2cpp_codegen_static_fields_for(SendMouseEvents_t1825902553_il2cpp_TypeInfo_var))->set_s_MouseUsed_0((bool)1);
		return;
	}
}
// System.Void UnityEngine.SendMouseEvents::DoSendMouseEvents(System.Int32)
extern "C"  void SendMouseEvents_DoSendMouseEvents_m2460376797 (RuntimeObject * __this /* static, unused */, int32_t ___skipRTCameras0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SendMouseEvents_DoSendMouseEvents_m2460376797_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t2903530434  V_0;
	memset(&V_0, 0, sizeof(V_0));
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	HitInfo_t2000020299  V_3;
	memset(&V_3, 0, sizeof(V_3));
	Camera_t3175186167 * V_4 = NULL;
	CameraU5BU5D_t1519774542* V_5 = NULL;
	int32_t V_6 = 0;
	Rect_t3436776195  V_7;
	memset(&V_7, 0, sizeof(V_7));
	GUILayer_t4095959459 * V_8 = NULL;
	GUIElement_t4082881042 * V_9 = NULL;
	Ray_t3821377119  V_10;
	memset(&V_10, 0, sizeof(V_10));
	float V_11 = 0.0f;
	Vector3_t2903530434  V_12;
	memset(&V_12, 0, sizeof(V_12));
	float V_13 = 0.0f;
	GameObject_t1811656094 * V_14 = NULL;
	GameObject_t1811656094 * V_15 = NULL;
	int32_t V_16 = 0;
	float G_B24_0 = 0.0f;
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t3439709668_il2cpp_TypeInfo_var);
		Vector3_t2903530434  L_0 = Input_get_mousePosition_m1483823421(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		int32_t L_1 = Camera_get_allCamerasCount_m1126717630(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_1 = L_1;
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t1825902553_il2cpp_TypeInfo_var);
		CameraU5BU5D_t1519774542* L_2 = ((SendMouseEvents_t1825902553_StaticFields*)il2cpp_codegen_static_fields_for(SendMouseEvents_t1825902553_il2cpp_TypeInfo_var))->get_m_Cameras_4();
		if (!L_2)
		{
			goto IL_0024;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t1825902553_il2cpp_TypeInfo_var);
		CameraU5BU5D_t1519774542* L_3 = ((SendMouseEvents_t1825902553_StaticFields*)il2cpp_codegen_static_fields_for(SendMouseEvents_t1825902553_il2cpp_TypeInfo_var))->get_m_Cameras_4();
		NullCheck(L_3);
		int32_t L_4 = V_1;
		if ((((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_3)->max_length))))) == ((int32_t)L_4)))
		{
			goto IL_002f;
		}
	}

IL_0024:
	{
		int32_t L_5 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t1825902553_il2cpp_TypeInfo_var);
		((SendMouseEvents_t1825902553_StaticFields*)il2cpp_codegen_static_fields_for(SendMouseEvents_t1825902553_il2cpp_TypeInfo_var))->set_m_Cameras_4(((CameraU5BU5D_t1519774542*)SZArrayNew(CameraU5BU5D_t1519774542_il2cpp_TypeInfo_var, (uint32_t)L_5)));
	}

IL_002f:
	{
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t1825902553_il2cpp_TypeInfo_var);
		CameraU5BU5D_t1519774542* L_6 = ((SendMouseEvents_t1825902553_StaticFields*)il2cpp_codegen_static_fields_for(SendMouseEvents_t1825902553_il2cpp_TypeInfo_var))->get_m_Cameras_4();
		Camera_GetAllCameras_m1072741975(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		V_2 = 0;
		goto IL_005e;
	}

IL_0041:
	{
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t1825902553_il2cpp_TypeInfo_var);
		HitInfoU5BU5D_t1076684586* L_7 = ((SendMouseEvents_t1825902553_StaticFields*)il2cpp_codegen_static_fields_for(SendMouseEvents_t1825902553_il2cpp_TypeInfo_var))->get_m_CurrentHit_3();
		int32_t L_8 = V_2;
		NullCheck(L_7);
		Initobj (HitInfo_t2000020299_il2cpp_TypeInfo_var, (&V_3));
		HitInfo_t2000020299  L_9 = V_3;
		*(HitInfo_t2000020299 *)((L_7)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_8))) = L_9;
		int32_t L_10 = V_2;
		V_2 = ((int32_t)((int32_t)L_10+(int32_t)1));
	}

IL_005e:
	{
		int32_t L_11 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t1825902553_il2cpp_TypeInfo_var);
		HitInfoU5BU5D_t1076684586* L_12 = ((SendMouseEvents_t1825902553_StaticFields*)il2cpp_codegen_static_fields_for(SendMouseEvents_t1825902553_il2cpp_TypeInfo_var))->get_m_CurrentHit_3();
		NullCheck(L_12);
		if ((((int32_t)L_11) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_12)->max_length)))))))
		{
			goto IL_0041;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t1825902553_il2cpp_TypeInfo_var);
		bool L_13 = ((SendMouseEvents_t1825902553_StaticFields*)il2cpp_codegen_static_fields_for(SendMouseEvents_t1825902553_il2cpp_TypeInfo_var))->get_s_MouseUsed_0();
		if (L_13)
		{
			goto IL_02ec;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t1825902553_il2cpp_TypeInfo_var);
		CameraU5BU5D_t1519774542* L_14 = ((SendMouseEvents_t1825902553_StaticFields*)il2cpp_codegen_static_fields_for(SendMouseEvents_t1825902553_il2cpp_TypeInfo_var))->get_m_Cameras_4();
		V_5 = L_14;
		V_6 = 0;
		goto IL_02e0;
	}

IL_0086:
	{
		CameraU5BU5D_t1519774542* L_15 = V_5;
		int32_t L_16 = V_6;
		NullCheck(L_15);
		int32_t L_17 = L_16;
		Camera_t3175186167 * L_18 = (L_15)->GetAt(static_cast<il2cpp_array_size_t>(L_17));
		V_4 = L_18;
		Camera_t3175186167 * L_19 = V_4;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1502412432_il2cpp_TypeInfo_var);
		bool L_20 = Object_op_Equality_m2342070775(NULL /*static, unused*/, L_19, (Object_t1502412432 *)NULL, /*hidden argument*/NULL);
		if (L_20)
		{
			goto IL_00b3;
		}
	}
	{
		int32_t L_21 = ___skipRTCameras0;
		if (!L_21)
		{
			goto IL_00b8;
		}
	}
	{
		Camera_t3175186167 * L_22 = V_4;
		NullCheck(L_22);
		RenderTexture_t2315324624 * L_23 = Camera_get_targetTexture_m1786790398(L_22, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1502412432_il2cpp_TypeInfo_var);
		bool L_24 = Object_op_Inequality_m1918954159(NULL /*static, unused*/, L_23, (Object_t1502412432 *)NULL, /*hidden argument*/NULL);
		if (!L_24)
		{
			goto IL_00b8;
		}
	}

IL_00b3:
	{
		goto IL_02da;
	}

IL_00b8:
	{
		Camera_t3175186167 * L_25 = V_4;
		NullCheck(L_25);
		Rect_t3436776195  L_26 = Camera_get_pixelRect_m1533752293(L_25, /*hidden argument*/NULL);
		V_7 = L_26;
		Vector3_t2903530434  L_27 = V_0;
		bool L_28 = Rect_Contains_m3887880031((&V_7), L_27, /*hidden argument*/NULL);
		if (L_28)
		{
			goto IL_00d3;
		}
	}
	{
		goto IL_02da;
	}

IL_00d3:
	{
		Camera_t3175186167 * L_29 = V_4;
		NullCheck(L_29);
		GUILayer_t4095959459 * L_30 = Component_GetComponent_TisGUILayer_t4095959459_m2359659865(L_29, /*hidden argument*/Component_GetComponent_TisGUILayer_t4095959459_m2359659865_RuntimeMethod_var);
		V_8 = L_30;
		GUILayer_t4095959459 * L_31 = V_8;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1502412432_il2cpp_TypeInfo_var);
		bool L_32 = Object_op_Implicit_m2314733868(NULL /*static, unused*/, L_31, /*hidden argument*/NULL);
		if (!L_32)
		{
			goto IL_0154;
		}
	}
	{
		GUILayer_t4095959459 * L_33 = V_8;
		Vector3_t2903530434  L_34 = V_0;
		NullCheck(L_33);
		GUIElement_t4082881042 * L_35 = GUILayer_HitTest_m1319441216(L_33, L_34, /*hidden argument*/NULL);
		V_9 = L_35;
		GUIElement_t4082881042 * L_36 = V_9;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1502412432_il2cpp_TypeInfo_var);
		bool L_37 = Object_op_Implicit_m2314733868(NULL /*static, unused*/, L_36, /*hidden argument*/NULL);
		if (!L_37)
		{
			goto IL_012f;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t1825902553_il2cpp_TypeInfo_var);
		HitInfoU5BU5D_t1076684586* L_38 = ((SendMouseEvents_t1825902553_StaticFields*)il2cpp_codegen_static_fields_for(SendMouseEvents_t1825902553_il2cpp_TypeInfo_var))->get_m_CurrentHit_3();
		NullCheck(L_38);
		GUIElement_t4082881042 * L_39 = V_9;
		NullCheck(L_39);
		GameObject_t1811656094 * L_40 = Component_get_gameObject_m2283027183(L_39, /*hidden argument*/NULL);
		((L_38)->GetAddressAt(static_cast<il2cpp_array_size_t>(0)))->set_target_0(L_40);
		HitInfoU5BU5D_t1076684586* L_41 = ((SendMouseEvents_t1825902553_StaticFields*)il2cpp_codegen_static_fields_for(SendMouseEvents_t1825902553_il2cpp_TypeInfo_var))->get_m_CurrentHit_3();
		NullCheck(L_41);
		Camera_t3175186167 * L_42 = V_4;
		((L_41)->GetAddressAt(static_cast<il2cpp_array_size_t>(0)))->set_camera_1(L_42);
		goto IL_0153;
	}

IL_012f:
	{
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t1825902553_il2cpp_TypeInfo_var);
		HitInfoU5BU5D_t1076684586* L_43 = ((SendMouseEvents_t1825902553_StaticFields*)il2cpp_codegen_static_fields_for(SendMouseEvents_t1825902553_il2cpp_TypeInfo_var))->get_m_CurrentHit_3();
		NullCheck(L_43);
		((L_43)->GetAddressAt(static_cast<il2cpp_array_size_t>(0)))->set_target_0((GameObject_t1811656094 *)NULL);
		HitInfoU5BU5D_t1076684586* L_44 = ((SendMouseEvents_t1825902553_StaticFields*)il2cpp_codegen_static_fields_for(SendMouseEvents_t1825902553_il2cpp_TypeInfo_var))->get_m_CurrentHit_3();
		NullCheck(L_44);
		((L_44)->GetAddressAt(static_cast<il2cpp_array_size_t>(0)))->set_camera_1((Camera_t3175186167 *)NULL);
	}

IL_0153:
	{
	}

IL_0154:
	{
		Camera_t3175186167 * L_45 = V_4;
		NullCheck(L_45);
		int32_t L_46 = Camera_get_eventMask_m3829863520(L_45, /*hidden argument*/NULL);
		if (L_46)
		{
			goto IL_0165;
		}
	}
	{
		goto IL_02da;
	}

IL_0165:
	{
		Camera_t3175186167 * L_47 = V_4;
		Vector3_t2903530434  L_48 = V_0;
		NullCheck(L_47);
		Ray_t3821377119  L_49 = Camera_ScreenPointToRay_m3401639554(L_47, L_48, /*hidden argument*/NULL);
		V_10 = L_49;
		Vector3_t2903530434  L_50 = Ray_get_direction_m3512869925((&V_10), /*hidden argument*/NULL);
		V_12 = L_50;
		float L_51 = (&V_12)->get_z_3();
		V_11 = L_51;
		float L_52 = V_11;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t4094287654_il2cpp_TypeInfo_var);
		bool L_53 = Mathf_Approximately_m3781428536(NULL /*static, unused*/, (0.0f), L_52, /*hidden argument*/NULL);
		if (!L_53)
		{
			goto IL_019c;
		}
	}
	{
		G_B24_0 = (std::numeric_limits<float>::infinity());
		goto IL_01b3;
	}

IL_019c:
	{
		Camera_t3175186167 * L_54 = V_4;
		NullCheck(L_54);
		float L_55 = Camera_get_farClipPlane_m1290388625(L_54, /*hidden argument*/NULL);
		Camera_t3175186167 * L_56 = V_4;
		NullCheck(L_56);
		float L_57 = Camera_get_nearClipPlane_m57381064(L_56, /*hidden argument*/NULL);
		float L_58 = V_11;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t4094287654_il2cpp_TypeInfo_var);
		float L_59 = fabsf(((float)((float)((float)((float)L_55-(float)L_57))/(float)L_58)));
		G_B24_0 = L_59;
	}

IL_01b3:
	{
		V_13 = G_B24_0;
		Camera_t3175186167 * L_60 = V_4;
		Ray_t3821377119  L_61 = V_10;
		float L_62 = V_13;
		Camera_t3175186167 * L_63 = V_4;
		NullCheck(L_63);
		int32_t L_64 = Camera_get_cullingMask_m2080661144(L_63, /*hidden argument*/NULL);
		Camera_t3175186167 * L_65 = V_4;
		NullCheck(L_65);
		int32_t L_66 = Camera_get_eventMask_m3829863520(L_65, /*hidden argument*/NULL);
		NullCheck(L_60);
		GameObject_t1811656094 * L_67 = Camera_RaycastTry_m1516696014(L_60, L_61, L_62, ((int32_t)((int32_t)L_64&(int32_t)L_66)), /*hidden argument*/NULL);
		V_14 = L_67;
		GameObject_t1811656094 * L_68 = V_14;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1502412432_il2cpp_TypeInfo_var);
		bool L_69 = Object_op_Inequality_m1918954159(NULL /*static, unused*/, L_68, (Object_t1502412432 *)NULL, /*hidden argument*/NULL);
		if (!L_69)
		{
			goto IL_0209;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t1825902553_il2cpp_TypeInfo_var);
		HitInfoU5BU5D_t1076684586* L_70 = ((SendMouseEvents_t1825902553_StaticFields*)il2cpp_codegen_static_fields_for(SendMouseEvents_t1825902553_il2cpp_TypeInfo_var))->get_m_CurrentHit_3();
		NullCheck(L_70);
		GameObject_t1811656094 * L_71 = V_14;
		((L_70)->GetAddressAt(static_cast<il2cpp_array_size_t>(1)))->set_target_0(L_71);
		HitInfoU5BU5D_t1076684586* L_72 = ((SendMouseEvents_t1825902553_StaticFields*)il2cpp_codegen_static_fields_for(SendMouseEvents_t1825902553_il2cpp_TypeInfo_var))->get_m_CurrentHit_3();
		NullCheck(L_72);
		Camera_t3175186167 * L_73 = V_4;
		((L_72)->GetAddressAt(static_cast<il2cpp_array_size_t>(1)))->set_camera_1(L_73);
		goto IL_0247;
	}

IL_0209:
	{
		Camera_t3175186167 * L_74 = V_4;
		NullCheck(L_74);
		int32_t L_75 = Camera_get_clearFlags_m2110532879(L_74, /*hidden argument*/NULL);
		if ((((int32_t)L_75) == ((int32_t)1)))
		{
			goto IL_0223;
		}
	}
	{
		Camera_t3175186167 * L_76 = V_4;
		NullCheck(L_76);
		int32_t L_77 = Camera_get_clearFlags_m2110532879(L_76, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_77) == ((uint32_t)2))))
		{
			goto IL_0247;
		}
	}

IL_0223:
	{
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t1825902553_il2cpp_TypeInfo_var);
		HitInfoU5BU5D_t1076684586* L_78 = ((SendMouseEvents_t1825902553_StaticFields*)il2cpp_codegen_static_fields_for(SendMouseEvents_t1825902553_il2cpp_TypeInfo_var))->get_m_CurrentHit_3();
		NullCheck(L_78);
		((L_78)->GetAddressAt(static_cast<il2cpp_array_size_t>(1)))->set_target_0((GameObject_t1811656094 *)NULL);
		HitInfoU5BU5D_t1076684586* L_79 = ((SendMouseEvents_t1825902553_StaticFields*)il2cpp_codegen_static_fields_for(SendMouseEvents_t1825902553_il2cpp_TypeInfo_var))->get_m_CurrentHit_3();
		NullCheck(L_79);
		((L_79)->GetAddressAt(static_cast<il2cpp_array_size_t>(1)))->set_camera_1((Camera_t3175186167 *)NULL);
	}

IL_0247:
	{
		Camera_t3175186167 * L_80 = V_4;
		Ray_t3821377119  L_81 = V_10;
		float L_82 = V_13;
		Camera_t3175186167 * L_83 = V_4;
		NullCheck(L_83);
		int32_t L_84 = Camera_get_cullingMask_m2080661144(L_83, /*hidden argument*/NULL);
		Camera_t3175186167 * L_85 = V_4;
		NullCheck(L_85);
		int32_t L_86 = Camera_get_eventMask_m3829863520(L_85, /*hidden argument*/NULL);
		NullCheck(L_80);
		GameObject_t1811656094 * L_87 = Camera_RaycastTry2D_m3135506373(L_80, L_81, L_82, ((int32_t)((int32_t)L_84&(int32_t)L_86)), /*hidden argument*/NULL);
		V_15 = L_87;
		GameObject_t1811656094 * L_88 = V_15;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1502412432_il2cpp_TypeInfo_var);
		bool L_89 = Object_op_Inequality_m1918954159(NULL /*static, unused*/, L_88, (Object_t1502412432 *)NULL, /*hidden argument*/NULL);
		if (!L_89)
		{
			goto IL_029b;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t1825902553_il2cpp_TypeInfo_var);
		HitInfoU5BU5D_t1076684586* L_90 = ((SendMouseEvents_t1825902553_StaticFields*)il2cpp_codegen_static_fields_for(SendMouseEvents_t1825902553_il2cpp_TypeInfo_var))->get_m_CurrentHit_3();
		NullCheck(L_90);
		GameObject_t1811656094 * L_91 = V_15;
		((L_90)->GetAddressAt(static_cast<il2cpp_array_size_t>(2)))->set_target_0(L_91);
		HitInfoU5BU5D_t1076684586* L_92 = ((SendMouseEvents_t1825902553_StaticFields*)il2cpp_codegen_static_fields_for(SendMouseEvents_t1825902553_il2cpp_TypeInfo_var))->get_m_CurrentHit_3();
		NullCheck(L_92);
		Camera_t3175186167 * L_93 = V_4;
		((L_92)->GetAddressAt(static_cast<il2cpp_array_size_t>(2)))->set_camera_1(L_93);
		goto IL_02d9;
	}

IL_029b:
	{
		Camera_t3175186167 * L_94 = V_4;
		NullCheck(L_94);
		int32_t L_95 = Camera_get_clearFlags_m2110532879(L_94, /*hidden argument*/NULL);
		if ((((int32_t)L_95) == ((int32_t)1)))
		{
			goto IL_02b5;
		}
	}
	{
		Camera_t3175186167 * L_96 = V_4;
		NullCheck(L_96);
		int32_t L_97 = Camera_get_clearFlags_m2110532879(L_96, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_97) == ((uint32_t)2))))
		{
			goto IL_02d9;
		}
	}

IL_02b5:
	{
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t1825902553_il2cpp_TypeInfo_var);
		HitInfoU5BU5D_t1076684586* L_98 = ((SendMouseEvents_t1825902553_StaticFields*)il2cpp_codegen_static_fields_for(SendMouseEvents_t1825902553_il2cpp_TypeInfo_var))->get_m_CurrentHit_3();
		NullCheck(L_98);
		((L_98)->GetAddressAt(static_cast<il2cpp_array_size_t>(2)))->set_target_0((GameObject_t1811656094 *)NULL);
		HitInfoU5BU5D_t1076684586* L_99 = ((SendMouseEvents_t1825902553_StaticFields*)il2cpp_codegen_static_fields_for(SendMouseEvents_t1825902553_il2cpp_TypeInfo_var))->get_m_CurrentHit_3();
		NullCheck(L_99);
		((L_99)->GetAddressAt(static_cast<il2cpp_array_size_t>(2)))->set_camera_1((Camera_t3175186167 *)NULL);
	}

IL_02d9:
	{
	}

IL_02da:
	{
		int32_t L_100 = V_6;
		V_6 = ((int32_t)((int32_t)L_100+(int32_t)1));
	}

IL_02e0:
	{
		int32_t L_101 = V_6;
		CameraU5BU5D_t1519774542* L_102 = V_5;
		NullCheck(L_102);
		if ((((int32_t)L_101) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_102)->max_length)))))))
		{
			goto IL_0086;
		}
	}
	{
	}

IL_02ec:
	{
		V_16 = 0;
		goto IL_0312;
	}

IL_02f4:
	{
		int32_t L_103 = V_16;
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t1825902553_il2cpp_TypeInfo_var);
		HitInfoU5BU5D_t1076684586* L_104 = ((SendMouseEvents_t1825902553_StaticFields*)il2cpp_codegen_static_fields_for(SendMouseEvents_t1825902553_il2cpp_TypeInfo_var))->get_m_CurrentHit_3();
		int32_t L_105 = V_16;
		NullCheck(L_104);
		SendMouseEvents_SendEvents_m3207122301(NULL /*static, unused*/, L_103, (*(HitInfo_t2000020299 *)((L_104)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_105)))), /*hidden argument*/NULL);
		int32_t L_106 = V_16;
		V_16 = ((int32_t)((int32_t)L_106+(int32_t)1));
	}

IL_0312:
	{
		int32_t L_107 = V_16;
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t1825902553_il2cpp_TypeInfo_var);
		HitInfoU5BU5D_t1076684586* L_108 = ((SendMouseEvents_t1825902553_StaticFields*)il2cpp_codegen_static_fields_for(SendMouseEvents_t1825902553_il2cpp_TypeInfo_var))->get_m_CurrentHit_3();
		NullCheck(L_108);
		if ((((int32_t)L_107) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_108)->max_length)))))))
		{
			goto IL_02f4;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t1825902553_il2cpp_TypeInfo_var);
		((SendMouseEvents_t1825902553_StaticFields*)il2cpp_codegen_static_fields_for(SendMouseEvents_t1825902553_il2cpp_TypeInfo_var))->set_s_MouseUsed_0((bool)0);
		return;
	}
}
// System.Void UnityEngine.SendMouseEvents::SendEvents(System.Int32,UnityEngine.SendMouseEvents/HitInfo)
extern "C"  void SendMouseEvents_SendEvents_m3207122301 (RuntimeObject * __this /* static, unused */, int32_t ___i0, HitInfo_t2000020299  ___hit1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SendMouseEvents_SendEvents_m3207122301_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	bool V_1 = false;
	HitInfo_t2000020299  V_2;
	memset(&V_2, 0, sizeof(V_2));
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t3439709668_il2cpp_TypeInfo_var);
		bool L_0 = Input_GetMouseButtonDown_m800484706(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		V_0 = L_0;
		bool L_1 = Input_GetMouseButton_m2523735708(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		V_1 = L_1;
		bool L_2 = V_0;
		if (!L_2)
		{
			goto IL_004f;
		}
	}
	{
		HitInfo_t2000020299  L_3 = ___hit1;
		bool L_4 = HitInfo_op_Implicit_m2296455248(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0049;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t1825902553_il2cpp_TypeInfo_var);
		HitInfoU5BU5D_t1076684586* L_5 = ((SendMouseEvents_t1825902553_StaticFields*)il2cpp_codegen_static_fields_for(SendMouseEvents_t1825902553_il2cpp_TypeInfo_var))->get_m_MouseDownHit_2();
		int32_t L_6 = ___i0;
		NullCheck(L_5);
		HitInfo_t2000020299  L_7 = ___hit1;
		*(HitInfo_t2000020299 *)((L_5)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_6))) = L_7;
		HitInfoU5BU5D_t1076684586* L_8 = ((SendMouseEvents_t1825902553_StaticFields*)il2cpp_codegen_static_fields_for(SendMouseEvents_t1825902553_il2cpp_TypeInfo_var))->get_m_MouseDownHit_2();
		int32_t L_9 = ___i0;
		NullCheck(L_8);
		HitInfo_SendMessage_m2268517799(((L_8)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_9))), _stringLiteral1678730234, /*hidden argument*/NULL);
	}

IL_0049:
	{
		goto IL_0107;
	}

IL_004f:
	{
		bool L_10 = V_1;
		if (L_10)
		{
			goto IL_00d6;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t1825902553_il2cpp_TypeInfo_var);
		HitInfoU5BU5D_t1076684586* L_11 = ((SendMouseEvents_t1825902553_StaticFields*)il2cpp_codegen_static_fields_for(SendMouseEvents_t1825902553_il2cpp_TypeInfo_var))->get_m_MouseDownHit_2();
		int32_t L_12 = ___i0;
		NullCheck(L_11);
		bool L_13 = HitInfo_op_Implicit_m2296455248(NULL /*static, unused*/, (*(HitInfo_t2000020299 *)((L_11)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_12)))), /*hidden argument*/NULL);
		if (!L_13)
		{
			goto IL_00d0;
		}
	}
	{
		HitInfo_t2000020299  L_14 = ___hit1;
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t1825902553_il2cpp_TypeInfo_var);
		HitInfoU5BU5D_t1076684586* L_15 = ((SendMouseEvents_t1825902553_StaticFields*)il2cpp_codegen_static_fields_for(SendMouseEvents_t1825902553_il2cpp_TypeInfo_var))->get_m_MouseDownHit_2();
		int32_t L_16 = ___i0;
		NullCheck(L_15);
		bool L_17 = HitInfo_Compare_m3834232273(NULL /*static, unused*/, L_14, (*(HitInfo_t2000020299 *)((L_15)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_16)))), /*hidden argument*/NULL);
		if (!L_17)
		{
			goto IL_00a1;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t1825902553_il2cpp_TypeInfo_var);
		HitInfoU5BU5D_t1076684586* L_18 = ((SendMouseEvents_t1825902553_StaticFields*)il2cpp_codegen_static_fields_for(SendMouseEvents_t1825902553_il2cpp_TypeInfo_var))->get_m_MouseDownHit_2();
		int32_t L_19 = ___i0;
		NullCheck(L_18);
		HitInfo_SendMessage_m2268517799(((L_18)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_19))), _stringLiteral2097995814, /*hidden argument*/NULL);
	}

IL_00a1:
	{
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t1825902553_il2cpp_TypeInfo_var);
		HitInfoU5BU5D_t1076684586* L_20 = ((SendMouseEvents_t1825902553_StaticFields*)il2cpp_codegen_static_fields_for(SendMouseEvents_t1825902553_il2cpp_TypeInfo_var))->get_m_MouseDownHit_2();
		int32_t L_21 = ___i0;
		NullCheck(L_20);
		HitInfo_SendMessage_m2268517799(((L_20)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_21))), _stringLiteral2124819158, /*hidden argument*/NULL);
		HitInfoU5BU5D_t1076684586* L_22 = ((SendMouseEvents_t1825902553_StaticFields*)il2cpp_codegen_static_fields_for(SendMouseEvents_t1825902553_il2cpp_TypeInfo_var))->get_m_MouseDownHit_2();
		int32_t L_23 = ___i0;
		NullCheck(L_22);
		Initobj (HitInfo_t2000020299_il2cpp_TypeInfo_var, (&V_2));
		HitInfo_t2000020299  L_24 = V_2;
		*(HitInfo_t2000020299 *)((L_22)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_23))) = L_24;
	}

IL_00d0:
	{
		goto IL_0107;
	}

IL_00d6:
	{
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t1825902553_il2cpp_TypeInfo_var);
		HitInfoU5BU5D_t1076684586* L_25 = ((SendMouseEvents_t1825902553_StaticFields*)il2cpp_codegen_static_fields_for(SendMouseEvents_t1825902553_il2cpp_TypeInfo_var))->get_m_MouseDownHit_2();
		int32_t L_26 = ___i0;
		NullCheck(L_25);
		bool L_27 = HitInfo_op_Implicit_m2296455248(NULL /*static, unused*/, (*(HitInfo_t2000020299 *)((L_25)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_26)))), /*hidden argument*/NULL);
		if (!L_27)
		{
			goto IL_0107;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t1825902553_il2cpp_TypeInfo_var);
		HitInfoU5BU5D_t1076684586* L_28 = ((SendMouseEvents_t1825902553_StaticFields*)il2cpp_codegen_static_fields_for(SendMouseEvents_t1825902553_il2cpp_TypeInfo_var))->get_m_MouseDownHit_2();
		int32_t L_29 = ___i0;
		NullCheck(L_28);
		HitInfo_SendMessage_m2268517799(((L_28)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_29))), _stringLiteral322303585, /*hidden argument*/NULL);
	}

IL_0107:
	{
		HitInfo_t2000020299  L_30 = ___hit1;
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t1825902553_il2cpp_TypeInfo_var);
		HitInfoU5BU5D_t1076684586* L_31 = ((SendMouseEvents_t1825902553_StaticFields*)il2cpp_codegen_static_fields_for(SendMouseEvents_t1825902553_il2cpp_TypeInfo_var))->get_m_LastHit_1();
		int32_t L_32 = ___i0;
		NullCheck(L_31);
		bool L_33 = HitInfo_Compare_m3834232273(NULL /*static, unused*/, L_30, (*(HitInfo_t2000020299 *)((L_31)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_32)))), /*hidden argument*/NULL);
		if (!L_33)
		{
			goto IL_0140;
		}
	}
	{
		HitInfo_t2000020299  L_34 = ___hit1;
		bool L_35 = HitInfo_op_Implicit_m2296455248(NULL /*static, unused*/, L_34, /*hidden argument*/NULL);
		if (!L_35)
		{
			goto IL_013a;
		}
	}
	{
		HitInfo_SendMessage_m2268517799((&___hit1), _stringLiteral1932353065, /*hidden argument*/NULL);
	}

IL_013a:
	{
		goto IL_0198;
	}

IL_0140:
	{
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t1825902553_il2cpp_TypeInfo_var);
		HitInfoU5BU5D_t1076684586* L_36 = ((SendMouseEvents_t1825902553_StaticFields*)il2cpp_codegen_static_fields_for(SendMouseEvents_t1825902553_il2cpp_TypeInfo_var))->get_m_LastHit_1();
		int32_t L_37 = ___i0;
		NullCheck(L_36);
		bool L_38 = HitInfo_op_Implicit_m2296455248(NULL /*static, unused*/, (*(HitInfo_t2000020299 *)((L_36)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_37)))), /*hidden argument*/NULL);
		if (!L_38)
		{
			goto IL_0172;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t1825902553_il2cpp_TypeInfo_var);
		HitInfoU5BU5D_t1076684586* L_39 = ((SendMouseEvents_t1825902553_StaticFields*)il2cpp_codegen_static_fields_for(SendMouseEvents_t1825902553_il2cpp_TypeInfo_var))->get_m_LastHit_1();
		int32_t L_40 = ___i0;
		NullCheck(L_39);
		HitInfo_SendMessage_m2268517799(((L_39)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_40))), _stringLiteral2750800491, /*hidden argument*/NULL);
	}

IL_0172:
	{
		HitInfo_t2000020299  L_41 = ___hit1;
		bool L_42 = HitInfo_op_Implicit_m2296455248(NULL /*static, unused*/, L_41, /*hidden argument*/NULL);
		if (!L_42)
		{
			goto IL_0197;
		}
	}
	{
		HitInfo_SendMessage_m2268517799((&___hit1), _stringLiteral2317408620, /*hidden argument*/NULL);
		HitInfo_SendMessage_m2268517799((&___hit1), _stringLiteral1932353065, /*hidden argument*/NULL);
	}

IL_0197:
	{
	}

IL_0198:
	{
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t1825902553_il2cpp_TypeInfo_var);
		HitInfoU5BU5D_t1076684586* L_43 = ((SendMouseEvents_t1825902553_StaticFields*)il2cpp_codegen_static_fields_for(SendMouseEvents_t1825902553_il2cpp_TypeInfo_var))->get_m_LastHit_1();
		int32_t L_44 = ___i0;
		NullCheck(L_43);
		HitInfo_t2000020299  L_45 = ___hit1;
		*(HitInfo_t2000020299 *)((L_43)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_44))) = L_45;
		return;
	}
}
// System.Void UnityEngine.SendMouseEvents::.cctor()
extern "C"  void SendMouseEvents__cctor_m1904736527 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SendMouseEvents__cctor_m1904736527_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	HitInfo_t2000020299  V_0;
	memset(&V_0, 0, sizeof(V_0));
	HitInfo_t2000020299  V_1;
	memset(&V_1, 0, sizeof(V_1));
	HitInfo_t2000020299  V_2;
	memset(&V_2, 0, sizeof(V_2));
	HitInfo_t2000020299  V_3;
	memset(&V_3, 0, sizeof(V_3));
	HitInfo_t2000020299  V_4;
	memset(&V_4, 0, sizeof(V_4));
	HitInfo_t2000020299  V_5;
	memset(&V_5, 0, sizeof(V_5));
	HitInfo_t2000020299  V_6;
	memset(&V_6, 0, sizeof(V_6));
	HitInfo_t2000020299  V_7;
	memset(&V_7, 0, sizeof(V_7));
	HitInfo_t2000020299  V_8;
	memset(&V_8, 0, sizeof(V_8));
	{
		((SendMouseEvents_t1825902553_StaticFields*)il2cpp_codegen_static_fields_for(SendMouseEvents_t1825902553_il2cpp_TypeInfo_var))->set_s_MouseUsed_0((bool)0);
		HitInfoU5BU5D_t1076684586* L_0 = ((HitInfoU5BU5D_t1076684586*)SZArrayNew(HitInfoU5BU5D_t1076684586_il2cpp_TypeInfo_var, (uint32_t)3));
		NullCheck(L_0);
		Initobj (HitInfo_t2000020299_il2cpp_TypeInfo_var, (&V_0));
		HitInfo_t2000020299  L_1 = V_0;
		*(HitInfo_t2000020299 *)((L_0)->GetAddressAt(static_cast<il2cpp_array_size_t>(0))) = L_1;
		HitInfoU5BU5D_t1076684586* L_2 = L_0;
		NullCheck(L_2);
		Initobj (HitInfo_t2000020299_il2cpp_TypeInfo_var, (&V_1));
		HitInfo_t2000020299  L_3 = V_1;
		*(HitInfo_t2000020299 *)((L_2)->GetAddressAt(static_cast<il2cpp_array_size_t>(1))) = L_3;
		HitInfoU5BU5D_t1076684586* L_4 = L_2;
		NullCheck(L_4);
		Initobj (HitInfo_t2000020299_il2cpp_TypeInfo_var, (&V_2));
		HitInfo_t2000020299  L_5 = V_2;
		*(HitInfo_t2000020299 *)((L_4)->GetAddressAt(static_cast<il2cpp_array_size_t>(2))) = L_5;
		((SendMouseEvents_t1825902553_StaticFields*)il2cpp_codegen_static_fields_for(SendMouseEvents_t1825902553_il2cpp_TypeInfo_var))->set_m_LastHit_1(L_4);
		HitInfoU5BU5D_t1076684586* L_6 = ((HitInfoU5BU5D_t1076684586*)SZArrayNew(HitInfoU5BU5D_t1076684586_il2cpp_TypeInfo_var, (uint32_t)3));
		NullCheck(L_6);
		Initobj (HitInfo_t2000020299_il2cpp_TypeInfo_var, (&V_3));
		HitInfo_t2000020299  L_7 = V_3;
		*(HitInfo_t2000020299 *)((L_6)->GetAddressAt(static_cast<il2cpp_array_size_t>(0))) = L_7;
		HitInfoU5BU5D_t1076684586* L_8 = L_6;
		NullCheck(L_8);
		Initobj (HitInfo_t2000020299_il2cpp_TypeInfo_var, (&V_4));
		HitInfo_t2000020299  L_9 = V_4;
		*(HitInfo_t2000020299 *)((L_8)->GetAddressAt(static_cast<il2cpp_array_size_t>(1))) = L_9;
		HitInfoU5BU5D_t1076684586* L_10 = L_8;
		NullCheck(L_10);
		Initobj (HitInfo_t2000020299_il2cpp_TypeInfo_var, (&V_5));
		HitInfo_t2000020299  L_11 = V_5;
		*(HitInfo_t2000020299 *)((L_10)->GetAddressAt(static_cast<il2cpp_array_size_t>(2))) = L_11;
		((SendMouseEvents_t1825902553_StaticFields*)il2cpp_codegen_static_fields_for(SendMouseEvents_t1825902553_il2cpp_TypeInfo_var))->set_m_MouseDownHit_2(L_10);
		HitInfoU5BU5D_t1076684586* L_12 = ((HitInfoU5BU5D_t1076684586*)SZArrayNew(HitInfoU5BU5D_t1076684586_il2cpp_TypeInfo_var, (uint32_t)3));
		NullCheck(L_12);
		Initobj (HitInfo_t2000020299_il2cpp_TypeInfo_var, (&V_6));
		HitInfo_t2000020299  L_13 = V_6;
		*(HitInfo_t2000020299 *)((L_12)->GetAddressAt(static_cast<il2cpp_array_size_t>(0))) = L_13;
		HitInfoU5BU5D_t1076684586* L_14 = L_12;
		NullCheck(L_14);
		Initobj (HitInfo_t2000020299_il2cpp_TypeInfo_var, (&V_7));
		HitInfo_t2000020299  L_15 = V_7;
		*(HitInfo_t2000020299 *)((L_14)->GetAddressAt(static_cast<il2cpp_array_size_t>(1))) = L_15;
		HitInfoU5BU5D_t1076684586* L_16 = L_14;
		NullCheck(L_16);
		Initobj (HitInfo_t2000020299_il2cpp_TypeInfo_var, (&V_8));
		HitInfo_t2000020299  L_17 = V_8;
		*(HitInfo_t2000020299 *)((L_16)->GetAddressAt(static_cast<il2cpp_array_size_t>(2))) = L_17;
		((SendMouseEvents_t1825902553_StaticFields*)il2cpp_codegen_static_fields_for(SendMouseEvents_t1825902553_il2cpp_TypeInfo_var))->set_m_CurrentHit_3(L_16);
		return;
	}
}
// Conversion methods for marshalling of: UnityEngine.SendMouseEvents/HitInfo
extern "C" void HitInfo_t2000020299_marshal_pinvoke(const HitInfo_t2000020299& unmarshaled, HitInfo_t2000020299_marshaled_pinvoke& marshaled)
{
	Il2CppCodeGenException* ___target_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'target' of type 'HitInfo': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___target_0Exception);
}
extern "C" void HitInfo_t2000020299_marshal_pinvoke_back(const HitInfo_t2000020299_marshaled_pinvoke& marshaled, HitInfo_t2000020299& unmarshaled)
{
	Il2CppCodeGenException* ___target_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'target' of type 'HitInfo': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___target_0Exception);
}
// Conversion method for clean up from marshalling of: UnityEngine.SendMouseEvents/HitInfo
extern "C" void HitInfo_t2000020299_marshal_pinvoke_cleanup(HitInfo_t2000020299_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.SendMouseEvents/HitInfo
extern "C" void HitInfo_t2000020299_marshal_com(const HitInfo_t2000020299& unmarshaled, HitInfo_t2000020299_marshaled_com& marshaled)
{
	Il2CppCodeGenException* ___target_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'target' of type 'HitInfo': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___target_0Exception);
}
extern "C" void HitInfo_t2000020299_marshal_com_back(const HitInfo_t2000020299_marshaled_com& marshaled, HitInfo_t2000020299& unmarshaled)
{
	Il2CppCodeGenException* ___target_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'target' of type 'HitInfo': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___target_0Exception);
}
// Conversion method for clean up from marshalling of: UnityEngine.SendMouseEvents/HitInfo
extern "C" void HitInfo_t2000020299_marshal_com_cleanup(HitInfo_t2000020299_marshaled_com& marshaled)
{
}
// System.Void UnityEngine.SendMouseEvents/HitInfo::SendMessage(System.String)
extern "C"  void HitInfo_SendMessage_m2268517799 (HitInfo_t2000020299 * __this, String_t* ___name0, const RuntimeMethod* method)
{
	{
		GameObject_t1811656094 * L_0 = __this->get_target_0();
		String_t* L_1 = ___name0;
		NullCheck(L_0);
		GameObject_SendMessage_m2018546670(L_0, L_1, NULL, 1, /*hidden argument*/NULL);
		return;
	}
}
extern "C"  void HitInfo_SendMessage_m2268517799_AdjustorThunk (RuntimeObject * __this, String_t* ___name0, const RuntimeMethod* method)
{
	HitInfo_t2000020299 * _thisAdjusted = reinterpret_cast<HitInfo_t2000020299 *>(__this + 1);
	HitInfo_SendMessage_m2268517799(_thisAdjusted, ___name0, method);
}
// System.Boolean UnityEngine.SendMouseEvents/HitInfo::op_Implicit(UnityEngine.SendMouseEvents/HitInfo)
extern "C"  bool HitInfo_op_Implicit_m2296455248 (RuntimeObject * __this /* static, unused */, HitInfo_t2000020299  ___exists0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HitInfo_op_Implicit_m2296455248_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	int32_t G_B3_0 = 0;
	{
		GameObject_t1811656094 * L_0 = (&___exists0)->get_target_0();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1502412432_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m1918954159(NULL /*static, unused*/, L_0, (Object_t1502412432 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0022;
		}
	}
	{
		Camera_t3175186167 * L_2 = (&___exists0)->get_camera_1();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1502412432_il2cpp_TypeInfo_var);
		bool L_3 = Object_op_Inequality_m1918954159(NULL /*static, unused*/, L_2, (Object_t1502412432 *)NULL, /*hidden argument*/NULL);
		G_B3_0 = ((int32_t)(L_3));
		goto IL_0023;
	}

IL_0022:
	{
		G_B3_0 = 0;
	}

IL_0023:
	{
		V_0 = (bool)G_B3_0;
		goto IL_0029;
	}

IL_0029:
	{
		bool L_4 = V_0;
		return L_4;
	}
}
// System.Boolean UnityEngine.SendMouseEvents/HitInfo::Compare(UnityEngine.SendMouseEvents/HitInfo,UnityEngine.SendMouseEvents/HitInfo)
extern "C"  bool HitInfo_Compare_m3834232273 (RuntimeObject * __this /* static, unused */, HitInfo_t2000020299  ___lhs0, HitInfo_t2000020299  ___rhs1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HitInfo_Compare_m3834232273_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	int32_t G_B3_0 = 0;
	{
		GameObject_t1811656094 * L_0 = (&___lhs0)->get_target_0();
		GameObject_t1811656094 * L_1 = (&___rhs1)->get_target_0();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1502412432_il2cpp_TypeInfo_var);
		bool L_2 = Object_op_Equality_m2342070775(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_002e;
		}
	}
	{
		Camera_t3175186167 * L_3 = (&___lhs0)->get_camera_1();
		Camera_t3175186167 * L_4 = (&___rhs1)->get_camera_1();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1502412432_il2cpp_TypeInfo_var);
		bool L_5 = Object_op_Equality_m2342070775(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		G_B3_0 = ((int32_t)(L_5));
		goto IL_002f;
	}

IL_002e:
	{
		G_B3_0 = 0;
	}

IL_002f:
	{
		V_0 = (bool)G_B3_0;
		goto IL_0035;
	}

IL_0035:
	{
		bool L_6 = V_0;
		return L_6;
	}
}
// System.Void UnityEngine.Serialization.FormerlySerializedAsAttribute::.ctor(System.String)
extern "C"  void FormerlySerializedAsAttribute__ctor_m263838377 (FormerlySerializedAsAttribute_t1937332047 * __this, String_t* ___oldName0, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m1283917294(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___oldName0;
		__this->set_m_oldName_0(L_0);
		return;
	}
}
// System.String UnityEngine.Serialization.FormerlySerializedAsAttribute::get_oldName()
extern "C"  String_t* FormerlySerializedAsAttribute_get_oldName_m1873283977 (FormerlySerializedAsAttribute_t1937332047 * __this, const RuntimeMethod* method)
{
	String_t* V_0 = NULL;
	{
		String_t* L_0 = __this->get_m_oldName_0();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		String_t* L_1 = V_0;
		return L_1;
	}
}
// System.Void UnityEngine.SerializeField::.ctor()
extern "C"  void SerializeField__ctor_m751354703 (SerializeField_t1027808064 * __this, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m1283917294(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.SerializePrivateVariables::.ctor()
extern "C"  void SerializePrivateVariables__ctor_m659120864 (SerializePrivateVariables_t409785951 * __this, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m1283917294(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.SetupCoroutine::InvokeMoveNext(System.Collections.IEnumerator,System.IntPtr)
extern "C"  void SetupCoroutine_InvokeMoveNext_m3258232562 (RuntimeObject * __this /* static, unused */, RuntimeObject* ___enumerator0, IntPtr_t ___returnValueAddress1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SetupCoroutine_InvokeMoveNext_m3258232562_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IntPtr_t L_0 = ___returnValueAddress1;
		IntPtr_t L_1 = ((IntPtr_t_StaticFields*)il2cpp_codegen_static_fields_for(IntPtr_t_il2cpp_TypeInfo_var))->get_Zero_1();
		bool L_2 = IntPtr_op_Equality_m1136306456(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0021;
		}
	}
	{
		ArgumentException_t4028401650 * L_3 = (ArgumentException_t4028401650 *)il2cpp_codegen_object_new(ArgumentException_t4028401650_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m596893627(L_3, _stringLiteral933600062, _stringLiteral2173215484, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_0021:
	{
		IntPtr_t L_4 = ___returnValueAddress1;
		void* L_5 = IntPtr_op_Explicit_m532509050(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		RuntimeObject* L_6 = ___enumerator0;
		NullCheck(L_6);
		bool L_7 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t2306197993_il2cpp_TypeInfo_var, L_6);
		*((int8_t*)(L_5)) = (int8_t)L_7;
		return;
	}
}
// System.Object UnityEngine.SetupCoroutine::InvokeMember(System.Object,System.String,System.Object)
extern "C"  RuntimeObject * SetupCoroutine_InvokeMember_m1034957027 (RuntimeObject * __this /* static, unused */, RuntimeObject * ___behaviour0, String_t* ___name1, RuntimeObject * ___variable2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SetupCoroutine_InvokeMember_m1034957027_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ObjectU5BU5D_t3270211303* V_0 = NULL;
	RuntimeObject * V_1 = NULL;
	{
		V_0 = (ObjectU5BU5D_t3270211303*)NULL;
		RuntimeObject * L_0 = ___variable2;
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		V_0 = ((ObjectU5BU5D_t3270211303*)SZArrayNew(ObjectU5BU5D_t3270211303_il2cpp_TypeInfo_var, (uint32_t)1));
		ObjectU5BU5D_t3270211303* L_1 = V_0;
		RuntimeObject * L_2 = ___variable2;
		NullCheck(L_1);
		ArrayElementTypeCheck (L_1, L_2);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(0), (RuntimeObject *)L_2);
	}

IL_0016:
	{
		RuntimeObject * L_3 = ___behaviour0;
		NullCheck(L_3);
		Type_t * L_4 = Object_GetType_m3774351292(L_3, /*hidden argument*/NULL);
		String_t* L_5 = ___name1;
		RuntimeObject * L_6 = ___behaviour0;
		ObjectU5BU5D_t3270211303* L_7 = V_0;
		NullCheck(L_4);
		RuntimeObject * L_8 = VirtFuncInvoker8< RuntimeObject *, String_t*, int32_t, Binder_t401576136 *, RuntimeObject *, ObjectU5BU5D_t3270211303*, ParameterModifierU5BU5D_t3421420242*, CultureInfo_t4043279873 *, StringU5BU5D_t2298632947* >::Invoke(74 /* System.Object System.Type::InvokeMember(System.String,System.Reflection.BindingFlags,System.Reflection.Binder,System.Object,System.Object[],System.Reflection.ParameterModifier[],System.Globalization.CultureInfo,System.String[]) */, L_4, L_5, ((int32_t)308), (Binder_t401576136 *)NULL, L_6, L_7, (ParameterModifierU5BU5D_t3421420242*)(ParameterModifierU5BU5D_t3421420242*)NULL, (CultureInfo_t4043279873 *)NULL, (StringU5BU5D_t2298632947*)(StringU5BU5D_t2298632947*)NULL);
		V_1 = L_8;
		goto IL_0033;
	}

IL_0033:
	{
		RuntimeObject * L_9 = V_1;
		return L_9;
	}
}
// UnityEngine.Shader UnityEngine.Shader::Find(System.String)
extern "C"  Shader_t3778723916 * Shader_Find_m4201634592 (RuntimeObject * __this /* static, unused */, String_t* ___name0, const RuntimeMethod* method)
{
	typedef Shader_t3778723916 * (*Shader_Find_m4201634592_ftn) (String_t*);
	static Shader_Find_m4201634592_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Shader_Find_m4201634592_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Shader::Find(System.String)");
	Shader_t3778723916 * retVal = _il2cpp_icall_func(___name0);
	return retVal;
}
// System.Int32 UnityEngine.Shader::PropertyToID(System.String)
extern "C"  int32_t Shader_PropertyToID_m3362753157 (RuntimeObject * __this /* static, unused */, String_t* ___name0, const RuntimeMethod* method)
{
	typedef int32_t (*Shader_PropertyToID_m3362753157_ftn) (String_t*);
	static Shader_PropertyToID_m3362753157_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Shader_PropertyToID_m3362753157_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Shader::PropertyToID(System.String)");
	int32_t retVal = _il2cpp_icall_func(___name0);
	return retVal;
}
// System.Void UnityEngine.SharedBetweenAnimatorsAttribute::.ctor()
extern "C"  void SharedBetweenAnimatorsAttribute__ctor_m3941009005 (SharedBetweenAnimatorsAttribute_t1887607685 * __this, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m1283917294(__this, /*hidden argument*/NULL);
		return;
	}
}
// Conversion methods for marshalling of: UnityEngine.SkeletonBone
extern "C" void SkeletonBone_t3530196780_marshal_pinvoke(const SkeletonBone_t3530196780& unmarshaled, SkeletonBone_t3530196780_marshaled_pinvoke& marshaled)
{
	marshaled.___name_0 = il2cpp_codegen_marshal_string(unmarshaled.get_name_0());
	marshaled.___parentName_1 = il2cpp_codegen_marshal_string(unmarshaled.get_parentName_1());
	marshaled.___position_2 = unmarshaled.get_position_2();
	marshaled.___rotation_3 = unmarshaled.get_rotation_3();
	marshaled.___scale_4 = unmarshaled.get_scale_4();
}
extern "C" void SkeletonBone_t3530196780_marshal_pinvoke_back(const SkeletonBone_t3530196780_marshaled_pinvoke& marshaled, SkeletonBone_t3530196780& unmarshaled)
{
	unmarshaled.set_name_0(il2cpp_codegen_marshal_string_result(marshaled.___name_0));
	unmarshaled.set_parentName_1(il2cpp_codegen_marshal_string_result(marshaled.___parentName_1));
	Vector3_t2903530434  unmarshaled_position_temp_2;
	memset(&unmarshaled_position_temp_2, 0, sizeof(unmarshaled_position_temp_2));
	unmarshaled_position_temp_2 = marshaled.___position_2;
	unmarshaled.set_position_2(unmarshaled_position_temp_2);
	Quaternion_t754065749  unmarshaled_rotation_temp_3;
	memset(&unmarshaled_rotation_temp_3, 0, sizeof(unmarshaled_rotation_temp_3));
	unmarshaled_rotation_temp_3 = marshaled.___rotation_3;
	unmarshaled.set_rotation_3(unmarshaled_rotation_temp_3);
	Vector3_t2903530434  unmarshaled_scale_temp_4;
	memset(&unmarshaled_scale_temp_4, 0, sizeof(unmarshaled_scale_temp_4));
	unmarshaled_scale_temp_4 = marshaled.___scale_4;
	unmarshaled.set_scale_4(unmarshaled_scale_temp_4);
}
// Conversion method for clean up from marshalling of: UnityEngine.SkeletonBone
extern "C" void SkeletonBone_t3530196780_marshal_pinvoke_cleanup(SkeletonBone_t3530196780_marshaled_pinvoke& marshaled)
{
	il2cpp_codegen_marshal_free(marshaled.___name_0);
	marshaled.___name_0 = NULL;
	il2cpp_codegen_marshal_free(marshaled.___parentName_1);
	marshaled.___parentName_1 = NULL;
}
// Conversion methods for marshalling of: UnityEngine.SkeletonBone
extern "C" void SkeletonBone_t3530196780_marshal_com(const SkeletonBone_t3530196780& unmarshaled, SkeletonBone_t3530196780_marshaled_com& marshaled)
{
	marshaled.___name_0 = il2cpp_codegen_marshal_bstring(unmarshaled.get_name_0());
	marshaled.___parentName_1 = il2cpp_codegen_marshal_bstring(unmarshaled.get_parentName_1());
	marshaled.___position_2 = unmarshaled.get_position_2();
	marshaled.___rotation_3 = unmarshaled.get_rotation_3();
	marshaled.___scale_4 = unmarshaled.get_scale_4();
}
extern "C" void SkeletonBone_t3530196780_marshal_com_back(const SkeletonBone_t3530196780_marshaled_com& marshaled, SkeletonBone_t3530196780& unmarshaled)
{
	unmarshaled.set_name_0(il2cpp_codegen_marshal_bstring_result(marshaled.___name_0));
	unmarshaled.set_parentName_1(il2cpp_codegen_marshal_bstring_result(marshaled.___parentName_1));
	Vector3_t2903530434  unmarshaled_position_temp_2;
	memset(&unmarshaled_position_temp_2, 0, sizeof(unmarshaled_position_temp_2));
	unmarshaled_position_temp_2 = marshaled.___position_2;
	unmarshaled.set_position_2(unmarshaled_position_temp_2);
	Quaternion_t754065749  unmarshaled_rotation_temp_3;
	memset(&unmarshaled_rotation_temp_3, 0, sizeof(unmarshaled_rotation_temp_3));
	unmarshaled_rotation_temp_3 = marshaled.___rotation_3;
	unmarshaled.set_rotation_3(unmarshaled_rotation_temp_3);
	Vector3_t2903530434  unmarshaled_scale_temp_4;
	memset(&unmarshaled_scale_temp_4, 0, sizeof(unmarshaled_scale_temp_4));
	unmarshaled_scale_temp_4 = marshaled.___scale_4;
	unmarshaled.set_scale_4(unmarshaled_scale_temp_4);
}
// Conversion method for clean up from marshalling of: UnityEngine.SkeletonBone
extern "C" void SkeletonBone_t3530196780_marshal_com_cleanup(SkeletonBone_t3530196780_marshaled_com& marshaled)
{
	il2cpp_codegen_marshal_free_bstring(marshaled.___name_0);
	marshaled.___name_0 = NULL;
	il2cpp_codegen_marshal_free_bstring(marshaled.___parentName_1);
	marshaled.___parentName_1 = NULL;
}
// System.Int32 UnityEngine.SkeletonBone::get_transformModified()
extern "C"  int32_t SkeletonBone_get_transformModified_m866685587 (SkeletonBone_t3530196780 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		V_0 = 0;
		goto IL_0008;
	}

IL_0008:
	{
		int32_t L_0 = V_0;
		return L_0;
	}
}
extern "C"  int32_t SkeletonBone_get_transformModified_m866685587_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	SkeletonBone_t3530196780 * _thisAdjusted = reinterpret_cast<SkeletonBone_t3530196780 *>(__this + 1);
	return SkeletonBone_get_transformModified_m866685587(_thisAdjusted, method);
}
// System.Void UnityEngine.SkeletonBone::set_transformModified(System.Int32)
extern "C"  void SkeletonBone_set_transformModified_m1750125318 (SkeletonBone_t3530196780 * __this, int32_t ___value0, const RuntimeMethod* method)
{
	{
		return;
	}
}
extern "C"  void SkeletonBone_set_transformModified_m1750125318_AdjustorThunk (RuntimeObject * __this, int32_t ___value0, const RuntimeMethod* method)
{
	SkeletonBone_t3530196780 * _thisAdjusted = reinterpret_cast<SkeletonBone_t3530196780 *>(__this + 1);
	SkeletonBone_set_transformModified_m1750125318(_thisAdjusted, ___value0, method);
}
// System.Void UnityEngine.SliderState::.ctor()
extern "C"  void SliderState__ctor_m3458368140 (SliderState_t3254875344 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m2906060009(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif

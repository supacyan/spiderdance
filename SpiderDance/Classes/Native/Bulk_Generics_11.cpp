﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "object-internals.h"

// System.Predicate`1<System.Reflection.CustomAttributeNamedArgument>
struct Predicate_1_t3357142812;
// System.IAsyncResult
struct IAsyncResult_t1913953129;
// System.AsyncCallback
struct AsyncCallback_t1057242265;
// System.Predicate`1<System.Reflection.CustomAttributeTypedArgument>
struct Predicate_1_t244174443;
// System.Predicate`1<UnityEngine.AnimatorClipInfo>
struct Predicate_1_t1909102483;
// System.Predicate`1<UnityEngine.Color32>
struct Predicate_1_t3173636740;
// System.Predicate`1<UnityEngine.EventSystems.RaycastResult>
struct Predicate_1_t2104426847;
// System.Predicate`1<UnityEngine.UICharInfo>
struct Predicate_1_t1074389306;
// System.Predicate`1<UnityEngine.UILineInfo>
struct Predicate_1_t3886715780;
// System.Predicate`1<UnityEngine.UIVertex>
struct Predicate_1_t1379704529;
// System.Predicate`1<UnityEngine.Vector2>
struct Predicate_1_t2170570351;
// System.Predicate`1<UnityEngine.Vector3>
struct Predicate_1_t1496767523;
// System.Predicate`1<UnityEngine.Vector4>
struct Predicate_1_t1407909434;
// System.Predicate`1<Vuforia.CameraDevice/CameraField>
struct Predicate_1_t3270418782;
// System.Predicate`1<Vuforia.Image/PIXEL_FORMAT>
struct Predicate_1_t178864225;
// System.Predicate`1<Vuforia.TargetFinder/TargetSearchResult>
struct Predicate_1_t3101898792;
// System.Predicate`1<Vuforia.VuforiaManager/TrackableIdPair>
struct Predicate_1_t4219631771;
// System.Predicate`1<Vuforia.VuforiaManagerImpl/TrackableResultData>
struct Predicate_1_t2688636542;
// System.Predicate`1<Vuforia.VuforiaManagerImpl/VuMarkTargetData>
struct Predicate_1_t1566583459;
// System.Predicate`1<Vuforia.VuforiaManagerImpl/VuMarkTargetResultData>
struct Predicate_1_t680208269;
// System.Reflection.MonoProperty/Getter`2<System.Object,System.Object>
struct Getter_2_t1437382685;
// System.Reflection.MonoProperty/StaticGetter`1<System.Object>
struct StaticGetter_1_t2059077758;
// UnityEngine.Events.CachedInvokableCall`1<System.Boolean>
struct CachedInvokableCall_1_t1235820086;
// UnityEngine.Object
struct Object_t1502412432;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.Object[]
struct ObjectU5BU5D_t3270211303;
// UnityEngine.Events.CachedInvokableCall`1<System.Int32>
struct CachedInvokableCall_1_t2522919606;
// UnityEngine.Events.CachedInvokableCall`1<System.Object>
struct CachedInvokableCall_1_t306933307;
// UnityEngine.Events.CachedInvokableCall`1<System.Single>
struct CachedInvokableCall_1_t1330324776;
// UnityEngine.Events.InvokableCall`1<System.Boolean>
struct InvokableCall_1_t1183494759;
// UnityEngine.Events.BaseInvokableCall
struct BaseInvokableCall_t1352930348;
// System.Type
struct Type_t;
// System.Delegate
struct Delegate_t537306192;
// UnityEngine.Events.UnityAction`1<System.Boolean>
struct UnityAction_1_t2203695861;
// System.ArgumentException
struct ArgumentException_t4028401650;
// System.String
struct String_t;
// UnityEngine.Events.InvokableCall`1<System.Int32>
struct InvokableCall_1_t2470594279;
// UnityEngine.Events.UnityAction`1<System.Int32>
struct UnityAction_1_t3490795381;
// UnityEngine.Events.InvokableCall`1<System.Object>
struct InvokableCall_1_t254607980;
// UnityEngine.Events.UnityAction`1<System.Object>
struct UnityAction_1_t1274809082;
// UnityEngine.Events.InvokableCall`1<System.Single>
struct InvokableCall_1_t1277999449;
// UnityEngine.Events.UnityAction`1<System.Single>
struct UnityAction_1_t2298200551;
// UnityEngine.Events.InvokableCall`1<UnityEngine.Color>
struct InvokableCall_1_t739676100;
// UnityEngine.Events.UnityAction`1<UnityEngine.Color>
struct UnityAction_1_t1759877202;
// UnityEngine.Events.InvokableCall`1<UnityEngine.Vector2>
struct InvokableCall_1_t1809982952;
// UnityEngine.Events.UnityAction`1<UnityEngine.Vector2>
struct UnityAction_1_t2830184054;
// UnityEngine.Events.InvokableCall`2<System.Object,System.Object>
struct InvokableCall_2_t1353357206;
// UnityEngine.Events.InvokableCall`3<System.Object,System.Object,System.Object>
struct InvokableCall_3_t3331941758;
// UnityEngine.Events.InvokableCall`4<System.Object,System.Object,System.Object,System.Object>
struct InvokableCall_4_t304489837;
// UnityEngine.Events.UnityAction`1<UnityEngine.SceneManagement.Scene>
struct UnityAction_1_t3896475417;
// UnityEngine.Events.UnityAction`2<System.Object,System.Object>
struct UnityAction_2_t1132847070;
// UnityEngine.Events.UnityAction`2<UnityEngine.SceneManagement.Scene,UnityEngine.SceneManagement.LoadSceneMode>
struct UnityAction_2_t2064023076;
// UnityEngine.Events.UnityAction`2<UnityEngine.SceneManagement.Scene,UnityEngine.SceneManagement.Scene>
struct UnityAction_2_t121587842;
// UnityEngine.Events.UnityAction`3<System.Object,System.Object,System.Object>
struct UnityAction_3_t1794137105;
// UnityEngine.Events.UnityAction`4<System.Object,System.Object,System.Object,System.Object>
struct UnityAction_4_t226130048;
// UnityEngine.Events.UnityEvent`1<System.Boolean>
struct UnityEvent_1_t4001855637;
// UnityEngine.Events.UnityEventBase
struct UnityEventBase_t2008500469;
// System.Type[]
struct TypeU5BU5D_t3854702846;
// UnityEngine.Events.UnityEvent`1<System.Int32>
struct UnityEvent_1_t993987861;
// UnityEngine.Events.UnityEvent`1<System.Object>
struct UnityEvent_1_t3072968858;
// UnityEngine.Events.UnityEvent`1<System.Single>
struct UnityEvent_1_t4096360327;
// UnityEngine.Events.UnityEvent`1<UnityEngine.Color>
struct UnityEvent_1_t3558036978;
// UnityEngine.Events.UnityEvent`1<UnityEngine.Vector2>
struct UnityEvent_1_t333376534;
// UnityEngine.Events.UnityEvent`2<System.Object,System.Object>
struct UnityEvent_2_t2615162100;
// UnityEngine.Events.UnityEvent`3<System.Object,System.Object,System.Object>
struct UnityEvent_3_t2603401452;
// UnityEngine.Events.UnityEvent`4<System.Object,System.Object,System.Object,System.Object>
struct UnityEvent_4_t4221637861;
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<System.Object>
struct EventFunction_1_t3830141588;
// UnityEngine.EventSystems.BaseEventData
struct BaseEventData_t1865211501;
// UnityEngine.UI.Collections.IndexedSet`1<System.Object>
struct IndexedSet_1_t2088528067;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t1486926975;
// System.NotImplementedException
struct NotImplementedException_t2054929637;
// System.Collections.IEnumerator
struct IEnumerator_t2306197993;
// System.NotSupportedException
struct NotSupportedException_t1045231613;
// System.Predicate`1<System.Object>
struct Predicate_1_t615195379;
// System.Comparison`1<System.Object>
struct Comparison_1_t1058283020;
// UnityEngine.UI.CoroutineTween.TweenRunner`1/<Start>c__Iterator0<UnityEngine.UI.CoroutineTween.ColorTween>
struct U3CStartU3Ec__Iterator0_t3365012439;
// UnityEngine.UI.CoroutineTween.TweenRunner`1/<Start>c__Iterator0<UnityEngine.UI.CoroutineTween.FloatTween>
struct U3CStartU3Ec__Iterator0_t2935223578;
// UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween>
struct TweenRunner_1_t645809502;
// UnityEngine.MonoBehaviour
struct MonoBehaviour_t345688271;
// UnityEngine.Component
struct Component_t4087199522;
// UnityEngine.GameObject
struct GameObject_t1811656094;
// UnityEngine.Coroutine
struct Coroutine_t2992159144;
// UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.FloatTween>
struct TweenRunner_1_t216020641;
// System.Collections.Generic.List`1<System.Int32>
struct List_1_t3093093796;
// System.Collections.Generic.List`1<System.Object>
struct List_1_t877107497;
// System.Collections.Generic.List`1<UnityEngine.Color32>
struct List_1_t3435548858;
// System.Collections.Generic.List`1<UnityEngine.UIVertex>
struct List_1_t1641616647;
// System.Collections.Generic.List`1<UnityEngine.Vector2>
struct List_1_t2432482469;
// System.Collections.Generic.List`1<UnityEngine.Vector3>
struct List_1_t1758679641;
// System.Collections.Generic.List`1<UnityEngine.Vector4>
struct List_1_t1669821552;
// UnityEngine.UI.ObjectPool`1<System.Object>
struct ObjectPool_1_t2864675919;
// System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.Color32>>
struct Stack_1_t2022325484;
// UnityEngine.Events.UnityAction`1<System.Collections.Generic.List`1<UnityEngine.Color32>>
struct UnityAction_1_t2688399650;
// UnityEngine.Color32[]
struct Color32U5BU5D_t2266382834;
// UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.Color32>>
struct ObjectPool_1_t4278266487;
// System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<System.Object>>
struct Stack_1_t3758851419;
// UnityEngine.Events.UnityAction`1<System.Collections.Generic.List`1<System.Object>>
struct UnityAction_1_t129958289;
// UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<System.Object>>
struct ObjectPool_1_t1719825126;
// System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<System.Int32>>
struct Stack_1_t1679870422;
// UnityEngine.Events.UnityAction`1<System.Collections.Generic.List`1<System.Int32>>
struct UnityAction_1_t2345944588;
// System.Int32[]
struct Int32U5BU5D_t2324750880;
// UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<System.Int32>>
struct ObjectPool_1_t3935811425;
// System.Collections.Generic.Link[]
struct LinkU5BU5D_t3095297741;
// System.Collections.Generic.IEqualityComparer`1<System.Object>
struct IEqualityComparer_1_t576959646;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t1606602250;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Int32,System.Collections.DictionaryEntry>
struct Transform_1_t2272181723;
// System.Collections.Generic.Dictionary`2<System.Object,System.Int32>
struct Dictionary_2_t2084444880;
// UnityEngine.Events.InvokableCallList
struct InvokableCallList_t462442189;
// UnityEngine.Events.PersistentCallGroup
struct PersistentCallGroup_t1612611655;
// UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.UIVertex>>
struct ObjectPool_1_t2484334276;
// UnityEngine.UIVertex[]
struct UIVertexU5BU5D_t227469777;
// System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.UIVertex>>
struct Stack_1_t228393273;
// UnityEngine.Events.UnityAction`1<System.Collections.Generic.List`1<UnityEngine.UIVertex>>
struct UnityAction_1_t894467439;
// UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.Vector2>>
struct ObjectPool_1_t3275200098;
// System.Collections.Generic.Stack`1<System.Object>
struct Stack_1_t608734916;
// System.Char[]
struct CharU5BU5D_t45629911;
// System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.Vector4>>
struct Stack_1_t256598178;
// UnityEngine.Events.UnityAction`1<System.Collections.Generic.List`1<UnityEngine.Vector4>>
struct UnityAction_1_t922672344;
// UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.Vector4>>
struct ObjectPool_1_t2512539181;
// System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.Vector3>>
struct Stack_1_t345456267;
// UnityEngine.Events.UnityAction`1<System.Collections.Generic.List`1<UnityEngine.Vector3>>
struct UnityAction_1_t1011530433;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t981680887;
// UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.Vector3>>
struct ObjectPool_1_t2601397270;
// System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.Vector2>>
struct Stack_1_t1019259095;
// UnityEngine.Events.UnityAction`1<System.Collections.Generic.List`1<UnityEngine.Vector2>>
struct UnityAction_1_t1685333261;
// UnityEngine.Vector2[]
struct Vector2U5BU5D_t3916253691;
// UnityEngine.Vector4[]
struct Vector4U5BU5D_t2124741284;
// System.IntPtr[]
struct IntPtrU5BU5D_t439965300;
// System.Collections.IDictionary
struct IDictionary_t3485892299;
// UnityEngine.EventSystems.EventSystem
struct EventSystem_t2204029313;
// UnityEngine.UI.CoroutineTween.FloatTween/FloatTweenCallback
struct FloatTweenCallback_t1836342663;
// System.Void
struct Void_t2217553113;
// System.DelegateData
struct DelegateData_t3370401926;
// System.Reflection.MemberInfo
struct MemberInfo_t;
// UnityEngine.EventSystems.BaseRaycaster
struct BaseRaycaster_t318802990;
// UnityEngine.UI.CoroutineTween.ColorTween/ColorTweenCallback
struct ColorTweenCallback_t3819146961;
// System.Reflection.MemberFilter
struct MemberFilter_t4208935459;

extern RuntimeClass* CustomAttributeNamedArgument_t468938427_il2cpp_TypeInfo_var;
extern const uint32_t Predicate_1_BeginInvoke_m618491045_MetadataUsageId;
extern RuntimeClass* CustomAttributeTypedArgument_t1650937354_il2cpp_TypeInfo_var;
extern const uint32_t Predicate_1_BeginInvoke_m220190048_MetadataUsageId;
extern RuntimeClass* AnimatorClipInfo_t3315865394_il2cpp_TypeInfo_var;
extern const uint32_t Predicate_1_BeginInvoke_m1045513357_MetadataUsageId;
extern RuntimeClass* Color32_t285432355_il2cpp_TypeInfo_var;
extern const uint32_t Predicate_1_BeginInvoke_m1689932010_MetadataUsageId;
extern RuntimeClass* RaycastResult_t3511189758_il2cpp_TypeInfo_var;
extern const uint32_t Predicate_1_BeginInvoke_m169345499_MetadataUsageId;
extern RuntimeClass* UICharInfo_t2481152217_il2cpp_TypeInfo_var;
extern const uint32_t Predicate_1_BeginInvoke_m1676152281_MetadataUsageId;
extern RuntimeClass* UILineInfo_t998511395_il2cpp_TypeInfo_var;
extern const uint32_t Predicate_1_BeginInvoke_m3075330401_MetadataUsageId;
extern RuntimeClass* UIVertex_t2786467440_il2cpp_TypeInfo_var;
extern const uint32_t Predicate_1_BeginInvoke_m2946408774_MetadataUsageId;
extern RuntimeClass* Vector2_t3577333262_il2cpp_TypeInfo_var;
extern const uint32_t Predicate_1_BeginInvoke_m1660015351_MetadataUsageId;
extern RuntimeClass* Vector3_t2903530434_il2cpp_TypeInfo_var;
extern const uint32_t Predicate_1_BeginInvoke_m4039012456_MetadataUsageId;
extern RuntimeClass* Vector4_t2814672345_il2cpp_TypeInfo_var;
extern const uint32_t Predicate_1_BeginInvoke_m2369142868_MetadataUsageId;
extern RuntimeClass* CameraField_t382214397_il2cpp_TypeInfo_var;
extern const uint32_t Predicate_1_BeginInvoke_m1857455278_MetadataUsageId;
extern RuntimeClass* PIXEL_FORMAT_t1585627136_il2cpp_TypeInfo_var;
extern const uint32_t Predicate_1_BeginInvoke_m639343012_MetadataUsageId;
extern RuntimeClass* TargetSearchResult_t213694407_il2cpp_TypeInfo_var;
extern const uint32_t Predicate_1_BeginInvoke_m2594383812_MetadataUsageId;
extern RuntimeClass* TrackableIdPair_t1331427386_il2cpp_TypeInfo_var;
extern const uint32_t Predicate_1_BeginInvoke_m2506242557_MetadataUsageId;
extern RuntimeClass* TrackableResultData_t4095399453_il2cpp_TypeInfo_var;
extern const uint32_t Predicate_1_BeginInvoke_m3239071508_MetadataUsageId;
extern RuntimeClass* VuMarkTargetData_t2973346370_il2cpp_TypeInfo_var;
extern const uint32_t Predicate_1_BeginInvoke_m1500370976_MetadataUsageId;
extern RuntimeClass* VuMarkTargetResultData_t2086971180_il2cpp_TypeInfo_var;
extern const uint32_t Predicate_1_BeginInvoke_m370506624_MetadataUsageId;
extern RuntimeClass* ObjectU5BU5D_t3270211303_il2cpp_TypeInfo_var;
extern const uint32_t CachedInvokableCall_1__ctor_m1968817514_MetadataUsageId;
extern const uint32_t CachedInvokableCall_1__ctor_m647347569_MetadataUsageId;
extern const uint32_t CachedInvokableCall_1__ctor_m1679835505_MetadataUsageId;
extern const uint32_t CachedInvokableCall_1__ctor_m1829863914_MetadataUsageId;
extern RuntimeClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t InvokableCall_1__ctor_m2576442280_MetadataUsageId;
extern RuntimeClass* ArgumentException_t4028401650_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral836720390;
extern const uint32_t InvokableCall_1_Invoke_m1308880592_MetadataUsageId;
extern const uint32_t InvokableCall_1__ctor_m1704911211_MetadataUsageId;
extern const uint32_t InvokableCall_1_Invoke_m2675525977_MetadataUsageId;
extern const uint32_t InvokableCall_1__ctor_m3536943171_MetadataUsageId;
extern const uint32_t InvokableCall_1_Invoke_m1948961823_MetadataUsageId;
extern const uint32_t InvokableCall_1__ctor_m3762840761_MetadataUsageId;
extern const uint32_t InvokableCall_1_Invoke_m293062541_MetadataUsageId;
extern const uint32_t InvokableCall_1__ctor_m3162522570_MetadataUsageId;
extern const uint32_t InvokableCall_1_Invoke_m577541394_MetadataUsageId;
extern const uint32_t InvokableCall_1__ctor_m2449436807_MetadataUsageId;
extern const uint32_t InvokableCall_1_Invoke_m2624901433_MetadataUsageId;
extern const uint32_t InvokableCall_2__ctor_m704948989_MetadataUsageId;
extern const uint32_t InvokableCall_2_Invoke_m3704122215_MetadataUsageId;
extern const uint32_t InvokableCall_3__ctor_m1051700724_MetadataUsageId;
extern const uint32_t InvokableCall_3_Invoke_m2568093866_MetadataUsageId;
extern const uint32_t InvokableCall_4__ctor_m4061686552_MetadataUsageId;
extern const uint32_t InvokableCall_4_Invoke_m3667773034_MetadataUsageId;
extern RuntimeClass* Boolean_t2950845069_il2cpp_TypeInfo_var;
extern const uint32_t UnityAction_1_BeginInvoke_m2961385523_MetadataUsageId;
extern RuntimeClass* Int32_t4237944589_il2cpp_TypeInfo_var;
extern const uint32_t UnityAction_1_BeginInvoke_m442003415_MetadataUsageId;
extern RuntimeClass* Single_t3045349759_il2cpp_TypeInfo_var;
extern const uint32_t UnityAction_1_BeginInvoke_m2756264811_MetadataUsageId;
extern RuntimeClass* Color_t2507026410_il2cpp_TypeInfo_var;
extern const uint32_t UnityAction_1_BeginInvoke_m430810788_MetadataUsageId;
extern RuntimeClass* Scene_t348657329_il2cpp_TypeInfo_var;
extern const uint32_t UnityAction_1_BeginInvoke_m3340556640_MetadataUsageId;
extern const uint32_t UnityAction_1_BeginInvoke_m3679434010_MetadataUsageId;
extern RuntimeClass* LoadSceneMode_t2291092563_il2cpp_TypeInfo_var;
extern const uint32_t UnityAction_2_BeginInvoke_m1047322930_MetadataUsageId;
extern const uint32_t UnityAction_2_BeginInvoke_m3900960504_MetadataUsageId;
extern const uint32_t UnityEvent_1__ctor_m3814196951_MetadataUsageId;
extern RuntimeClass* TypeU5BU5D_t3854702846_il2cpp_TypeInfo_var;
extern const uint32_t UnityEvent_1_FindMethod_Impl_m194499244_MetadataUsageId;
extern const uint32_t UnityEvent_1__ctor_m3605924324_MetadataUsageId;
extern const uint32_t UnityEvent_1_FindMethod_Impl_m1672417327_MetadataUsageId;
extern const uint32_t UnityEvent_1__ctor_m503898863_MetadataUsageId;
extern const uint32_t UnityEvent_1_FindMethod_Impl_m1914224043_MetadataUsageId;
extern const uint32_t UnityEvent_1__ctor_m2458152034_MetadataUsageId;
extern const uint32_t UnityEvent_1_FindMethod_Impl_m2231877772_MetadataUsageId;
extern const uint32_t UnityEvent_1__ctor_m3969821180_MetadataUsageId;
extern const uint32_t UnityEvent_1_FindMethod_Impl_m1310705587_MetadataUsageId;
extern const uint32_t UnityEvent_1__ctor_m154205551_MetadataUsageId;
extern const uint32_t UnityEvent_1_FindMethod_Impl_m2152450538_MetadataUsageId;
extern const uint32_t UnityEvent_2__ctor_m2986156325_MetadataUsageId;
extern const uint32_t UnityEvent_2_FindMethod_Impl_m3281817964_MetadataUsageId;
extern const uint32_t UnityEvent_3__ctor_m962964178_MetadataUsageId;
extern const uint32_t UnityEvent_3_FindMethod_Impl_m923441632_MetadataUsageId;
extern const uint32_t UnityEvent_4__ctor_m3870083973_MetadataUsageId;
extern const uint32_t UnityEvent_4_FindMethod_Impl_m3193778268_MetadataUsageId;
extern RuntimeClass* NotImplementedException_t2054929637_il2cpp_TypeInfo_var;
extern const uint32_t IndexedSet_1_GetEnumerator_m1233643819_MetadataUsageId;
extern RuntimeClass* NotSupportedException_t1045231613_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2696702335;
extern const uint32_t IndexedSet_1_Insert_m2770351314_MetadataUsageId;
extern RuntimeClass* Mathf_t4094287654_il2cpp_TypeInfo_var;
extern const uint32_t U3CStartU3Ec__Iterator0_MoveNext_m1847862442_MetadataUsageId;
extern const uint32_t U3CStartU3Ec__Iterator0_Reset_m2520601135_MetadataUsageId;
extern const uint32_t U3CStartU3Ec__Iterator0_MoveNext_m1708609373_MetadataUsageId;
extern const uint32_t U3CStartU3Ec__Iterator0_Reset_m810840618_MetadataUsageId;
extern RuntimeClass* Object_t1502412432_il2cpp_TypeInfo_var;
extern RuntimeClass* Debug_t3525713211_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1668423132;
extern const uint32_t TweenRunner_1_StartTween_m550936528_MetadataUsageId;
extern const uint32_t TweenRunner_1_StartTween_m2649818531_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral972099005;
extern const uint32_t ObjectPool_1_Release_m3069516663_MetadataUsageId;

struct ObjectU5BU5D_t3270211303;
struct TypeU5BU5D_t3854702846;


#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
struct Il2CppArrayBounds;
#ifndef RUNTIMEARRAY_H
#define RUNTIMEARRAY_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Array

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEARRAY_H
#ifndef OBJECTPOOL_1_T4278266487_H
#define OBJECTPOOL_1_T4278266487_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.Color32>>
struct  ObjectPool_1_t4278266487  : public RuntimeObject
{
public:
	// System.Collections.Generic.Stack`1<T> UnityEngine.UI.ObjectPool`1::m_Stack
	Stack_1_t2022325484 * ___m_Stack_0;
	// UnityEngine.Events.UnityAction`1<T> UnityEngine.UI.ObjectPool`1::m_ActionOnGet
	UnityAction_1_t2688399650 * ___m_ActionOnGet_1;
	// UnityEngine.Events.UnityAction`1<T> UnityEngine.UI.ObjectPool`1::m_ActionOnRelease
	UnityAction_1_t2688399650 * ___m_ActionOnRelease_2;
	// System.Int32 UnityEngine.UI.ObjectPool`1::<countAll>k__BackingField
	int32_t ___U3CcountAllU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_m_Stack_0() { return static_cast<int32_t>(offsetof(ObjectPool_1_t4278266487, ___m_Stack_0)); }
	inline Stack_1_t2022325484 * get_m_Stack_0() const { return ___m_Stack_0; }
	inline Stack_1_t2022325484 ** get_address_of_m_Stack_0() { return &___m_Stack_0; }
	inline void set_m_Stack_0(Stack_1_t2022325484 * value)
	{
		___m_Stack_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Stack_0), value);
	}

	inline static int32_t get_offset_of_m_ActionOnGet_1() { return static_cast<int32_t>(offsetof(ObjectPool_1_t4278266487, ___m_ActionOnGet_1)); }
	inline UnityAction_1_t2688399650 * get_m_ActionOnGet_1() const { return ___m_ActionOnGet_1; }
	inline UnityAction_1_t2688399650 ** get_address_of_m_ActionOnGet_1() { return &___m_ActionOnGet_1; }
	inline void set_m_ActionOnGet_1(UnityAction_1_t2688399650 * value)
	{
		___m_ActionOnGet_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_ActionOnGet_1), value);
	}

	inline static int32_t get_offset_of_m_ActionOnRelease_2() { return static_cast<int32_t>(offsetof(ObjectPool_1_t4278266487, ___m_ActionOnRelease_2)); }
	inline UnityAction_1_t2688399650 * get_m_ActionOnRelease_2() const { return ___m_ActionOnRelease_2; }
	inline UnityAction_1_t2688399650 ** get_address_of_m_ActionOnRelease_2() { return &___m_ActionOnRelease_2; }
	inline void set_m_ActionOnRelease_2(UnityAction_1_t2688399650 * value)
	{
		___m_ActionOnRelease_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_ActionOnRelease_2), value);
	}

	inline static int32_t get_offset_of_U3CcountAllU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(ObjectPool_1_t4278266487, ___U3CcountAllU3Ek__BackingField_3)); }
	inline int32_t get_U3CcountAllU3Ek__BackingField_3() const { return ___U3CcountAllU3Ek__BackingField_3; }
	inline int32_t* get_address_of_U3CcountAllU3Ek__BackingField_3() { return &___U3CcountAllU3Ek__BackingField_3; }
	inline void set_U3CcountAllU3Ek__BackingField_3(int32_t value)
	{
		___U3CcountAllU3Ek__BackingField_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OBJECTPOOL_1_T4278266487_H
#ifndef LIST_1_T3435548858_H
#define LIST_1_T3435548858_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1<UnityEngine.Color32>
struct  List_1_t3435548858  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	Color32U5BU5D_t2266382834* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t3435548858, ____items_1)); }
	inline Color32U5BU5D_t2266382834* get__items_1() const { return ____items_1; }
	inline Color32U5BU5D_t2266382834** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(Color32U5BU5D_t2266382834* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((&____items_1), value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t3435548858, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t3435548858, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}
};

struct List_1_t3435548858_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::EmptyArray
	Color32U5BU5D_t2266382834* ___EmptyArray_4;

public:
	inline static int32_t get_offset_of_EmptyArray_4() { return static_cast<int32_t>(offsetof(List_1_t3435548858_StaticFields, ___EmptyArray_4)); }
	inline Color32U5BU5D_t2266382834* get_EmptyArray_4() const { return ___EmptyArray_4; }
	inline Color32U5BU5D_t2266382834** get_address_of_EmptyArray_4() { return &___EmptyArray_4; }
	inline void set_EmptyArray_4(Color32U5BU5D_t2266382834* value)
	{
		___EmptyArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIST_1_T3435548858_H
#ifndef LISTPOOL_1_T3330323051_H
#define LISTPOOL_1_T3330323051_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.ListPool`1<UnityEngine.Color32>
struct  ListPool_1_t3330323051  : public RuntimeObject
{
public:

public:
};

struct ListPool_1_t3330323051_StaticFields
{
public:
	// UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<T>> UnityEngine.UI.ListPool`1::s_ListPool
	ObjectPool_1_t4278266487 * ___s_ListPool_0;

public:
	inline static int32_t get_offset_of_s_ListPool_0() { return static_cast<int32_t>(offsetof(ListPool_1_t3330323051_StaticFields, ___s_ListPool_0)); }
	inline ObjectPool_1_t4278266487 * get_s_ListPool_0() const { return ___s_ListPool_0; }
	inline ObjectPool_1_t4278266487 ** get_address_of_s_ListPool_0() { return &___s_ListPool_0; }
	inline void set_s_ListPool_0(ObjectPool_1_t4278266487 * value)
	{
		___s_ListPool_0 = value;
		Il2CppCodeGenWriteBarrier((&___s_ListPool_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LISTPOOL_1_T3330323051_H
#ifndef OBJECTPOOL_1_T1719825126_H
#define OBJECTPOOL_1_T1719825126_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<System.Object>>
struct  ObjectPool_1_t1719825126  : public RuntimeObject
{
public:
	// System.Collections.Generic.Stack`1<T> UnityEngine.UI.ObjectPool`1::m_Stack
	Stack_1_t3758851419 * ___m_Stack_0;
	// UnityEngine.Events.UnityAction`1<T> UnityEngine.UI.ObjectPool`1::m_ActionOnGet
	UnityAction_1_t129958289 * ___m_ActionOnGet_1;
	// UnityEngine.Events.UnityAction`1<T> UnityEngine.UI.ObjectPool`1::m_ActionOnRelease
	UnityAction_1_t129958289 * ___m_ActionOnRelease_2;
	// System.Int32 UnityEngine.UI.ObjectPool`1::<countAll>k__BackingField
	int32_t ___U3CcountAllU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_m_Stack_0() { return static_cast<int32_t>(offsetof(ObjectPool_1_t1719825126, ___m_Stack_0)); }
	inline Stack_1_t3758851419 * get_m_Stack_0() const { return ___m_Stack_0; }
	inline Stack_1_t3758851419 ** get_address_of_m_Stack_0() { return &___m_Stack_0; }
	inline void set_m_Stack_0(Stack_1_t3758851419 * value)
	{
		___m_Stack_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Stack_0), value);
	}

	inline static int32_t get_offset_of_m_ActionOnGet_1() { return static_cast<int32_t>(offsetof(ObjectPool_1_t1719825126, ___m_ActionOnGet_1)); }
	inline UnityAction_1_t129958289 * get_m_ActionOnGet_1() const { return ___m_ActionOnGet_1; }
	inline UnityAction_1_t129958289 ** get_address_of_m_ActionOnGet_1() { return &___m_ActionOnGet_1; }
	inline void set_m_ActionOnGet_1(UnityAction_1_t129958289 * value)
	{
		___m_ActionOnGet_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_ActionOnGet_1), value);
	}

	inline static int32_t get_offset_of_m_ActionOnRelease_2() { return static_cast<int32_t>(offsetof(ObjectPool_1_t1719825126, ___m_ActionOnRelease_2)); }
	inline UnityAction_1_t129958289 * get_m_ActionOnRelease_2() const { return ___m_ActionOnRelease_2; }
	inline UnityAction_1_t129958289 ** get_address_of_m_ActionOnRelease_2() { return &___m_ActionOnRelease_2; }
	inline void set_m_ActionOnRelease_2(UnityAction_1_t129958289 * value)
	{
		___m_ActionOnRelease_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_ActionOnRelease_2), value);
	}

	inline static int32_t get_offset_of_U3CcountAllU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(ObjectPool_1_t1719825126, ___U3CcountAllU3Ek__BackingField_3)); }
	inline int32_t get_U3CcountAllU3Ek__BackingField_3() const { return ___U3CcountAllU3Ek__BackingField_3; }
	inline int32_t* get_address_of_U3CcountAllU3Ek__BackingField_3() { return &___U3CcountAllU3Ek__BackingField_3; }
	inline void set_U3CcountAllU3Ek__BackingField_3(int32_t value)
	{
		___U3CcountAllU3Ek__BackingField_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OBJECTPOOL_1_T1719825126_H
#ifndef LISTPOOL_1_T771881690_H
#define LISTPOOL_1_T771881690_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.ListPool`1<System.Object>
struct  ListPool_1_t771881690  : public RuntimeObject
{
public:

public:
};

struct ListPool_1_t771881690_StaticFields
{
public:
	// UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<T>> UnityEngine.UI.ListPool`1::s_ListPool
	ObjectPool_1_t1719825126 * ___s_ListPool_0;

public:
	inline static int32_t get_offset_of_s_ListPool_0() { return static_cast<int32_t>(offsetof(ListPool_1_t771881690_StaticFields, ___s_ListPool_0)); }
	inline ObjectPool_1_t1719825126 * get_s_ListPool_0() const { return ___s_ListPool_0; }
	inline ObjectPool_1_t1719825126 ** get_address_of_s_ListPool_0() { return &___s_ListPool_0; }
	inline void set_s_ListPool_0(ObjectPool_1_t1719825126 * value)
	{
		___s_ListPool_0 = value;
		Il2CppCodeGenWriteBarrier((&___s_ListPool_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LISTPOOL_1_T771881690_H
#ifndef OBJECTPOOL_1_T3935811425_H
#define OBJECTPOOL_1_T3935811425_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<System.Int32>>
struct  ObjectPool_1_t3935811425  : public RuntimeObject
{
public:
	// System.Collections.Generic.Stack`1<T> UnityEngine.UI.ObjectPool`1::m_Stack
	Stack_1_t1679870422 * ___m_Stack_0;
	// UnityEngine.Events.UnityAction`1<T> UnityEngine.UI.ObjectPool`1::m_ActionOnGet
	UnityAction_1_t2345944588 * ___m_ActionOnGet_1;
	// UnityEngine.Events.UnityAction`1<T> UnityEngine.UI.ObjectPool`1::m_ActionOnRelease
	UnityAction_1_t2345944588 * ___m_ActionOnRelease_2;
	// System.Int32 UnityEngine.UI.ObjectPool`1::<countAll>k__BackingField
	int32_t ___U3CcountAllU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_m_Stack_0() { return static_cast<int32_t>(offsetof(ObjectPool_1_t3935811425, ___m_Stack_0)); }
	inline Stack_1_t1679870422 * get_m_Stack_0() const { return ___m_Stack_0; }
	inline Stack_1_t1679870422 ** get_address_of_m_Stack_0() { return &___m_Stack_0; }
	inline void set_m_Stack_0(Stack_1_t1679870422 * value)
	{
		___m_Stack_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Stack_0), value);
	}

	inline static int32_t get_offset_of_m_ActionOnGet_1() { return static_cast<int32_t>(offsetof(ObjectPool_1_t3935811425, ___m_ActionOnGet_1)); }
	inline UnityAction_1_t2345944588 * get_m_ActionOnGet_1() const { return ___m_ActionOnGet_1; }
	inline UnityAction_1_t2345944588 ** get_address_of_m_ActionOnGet_1() { return &___m_ActionOnGet_1; }
	inline void set_m_ActionOnGet_1(UnityAction_1_t2345944588 * value)
	{
		___m_ActionOnGet_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_ActionOnGet_1), value);
	}

	inline static int32_t get_offset_of_m_ActionOnRelease_2() { return static_cast<int32_t>(offsetof(ObjectPool_1_t3935811425, ___m_ActionOnRelease_2)); }
	inline UnityAction_1_t2345944588 * get_m_ActionOnRelease_2() const { return ___m_ActionOnRelease_2; }
	inline UnityAction_1_t2345944588 ** get_address_of_m_ActionOnRelease_2() { return &___m_ActionOnRelease_2; }
	inline void set_m_ActionOnRelease_2(UnityAction_1_t2345944588 * value)
	{
		___m_ActionOnRelease_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_ActionOnRelease_2), value);
	}

	inline static int32_t get_offset_of_U3CcountAllU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(ObjectPool_1_t3935811425, ___U3CcountAllU3Ek__BackingField_3)); }
	inline int32_t get_U3CcountAllU3Ek__BackingField_3() const { return ___U3CcountAllU3Ek__BackingField_3; }
	inline int32_t* get_address_of_U3CcountAllU3Ek__BackingField_3() { return &___U3CcountAllU3Ek__BackingField_3; }
	inline void set_U3CcountAllU3Ek__BackingField_3(int32_t value)
	{
		___U3CcountAllU3Ek__BackingField_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OBJECTPOOL_1_T3935811425_H
#ifndef LIST_1_T3093093796_H
#define LIST_1_T3093093796_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1<System.Int32>
struct  List_1_t3093093796  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	Int32U5BU5D_t2324750880* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t3093093796, ____items_1)); }
	inline Int32U5BU5D_t2324750880* get__items_1() const { return ____items_1; }
	inline Int32U5BU5D_t2324750880** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(Int32U5BU5D_t2324750880* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((&____items_1), value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t3093093796, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t3093093796, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}
};

struct List_1_t3093093796_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::EmptyArray
	Int32U5BU5D_t2324750880* ___EmptyArray_4;

public:
	inline static int32_t get_offset_of_EmptyArray_4() { return static_cast<int32_t>(offsetof(List_1_t3093093796_StaticFields, ___EmptyArray_4)); }
	inline Int32U5BU5D_t2324750880* get_EmptyArray_4() const { return ___EmptyArray_4; }
	inline Int32U5BU5D_t2324750880** get_address_of_EmptyArray_4() { return &___EmptyArray_4; }
	inline void set_EmptyArray_4(Int32U5BU5D_t2324750880* value)
	{
		___EmptyArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIST_1_T3093093796_H
#ifndef LISTPOOL_1_T2987867989_H
#define LISTPOOL_1_T2987867989_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.ListPool`1<System.Int32>
struct  ListPool_1_t2987867989  : public RuntimeObject
{
public:

public:
};

struct ListPool_1_t2987867989_StaticFields
{
public:
	// UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<T>> UnityEngine.UI.ListPool`1::s_ListPool
	ObjectPool_1_t3935811425 * ___s_ListPool_0;

public:
	inline static int32_t get_offset_of_s_ListPool_0() { return static_cast<int32_t>(offsetof(ListPool_1_t2987867989_StaticFields, ___s_ListPool_0)); }
	inline ObjectPool_1_t3935811425 * get_s_ListPool_0() const { return ___s_ListPool_0; }
	inline ObjectPool_1_t3935811425 ** get_address_of_s_ListPool_0() { return &___s_ListPool_0; }
	inline void set_s_ListPool_0(ObjectPool_1_t3935811425 * value)
	{
		___s_ListPool_0 = value;
		Il2CppCodeGenWriteBarrier((&___s_ListPool_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LISTPOOL_1_T2987867989_H
#ifndef TWEENRUNNER_1_T216020641_H
#define TWEENRUNNER_1_T216020641_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.FloatTween>
struct  TweenRunner_1_t216020641  : public RuntimeObject
{
public:
	// UnityEngine.MonoBehaviour UnityEngine.UI.CoroutineTween.TweenRunner`1::m_CoroutineContainer
	MonoBehaviour_t345688271 * ___m_CoroutineContainer_0;
	// System.Collections.IEnumerator UnityEngine.UI.CoroutineTween.TweenRunner`1::m_Tween
	RuntimeObject* ___m_Tween_1;

public:
	inline static int32_t get_offset_of_m_CoroutineContainer_0() { return static_cast<int32_t>(offsetof(TweenRunner_1_t216020641, ___m_CoroutineContainer_0)); }
	inline MonoBehaviour_t345688271 * get_m_CoroutineContainer_0() const { return ___m_CoroutineContainer_0; }
	inline MonoBehaviour_t345688271 ** get_address_of_m_CoroutineContainer_0() { return &___m_CoroutineContainer_0; }
	inline void set_m_CoroutineContainer_0(MonoBehaviour_t345688271 * value)
	{
		___m_CoroutineContainer_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_CoroutineContainer_0), value);
	}

	inline static int32_t get_offset_of_m_Tween_1() { return static_cast<int32_t>(offsetof(TweenRunner_1_t216020641, ___m_Tween_1)); }
	inline RuntimeObject* get_m_Tween_1() const { return ___m_Tween_1; }
	inline RuntimeObject** get_address_of_m_Tween_1() { return &___m_Tween_1; }
	inline void set_m_Tween_1(RuntimeObject* value)
	{
		___m_Tween_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_Tween_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TWEENRUNNER_1_T216020641_H
#ifndef TWEENRUNNER_1_T645809502_H
#define TWEENRUNNER_1_T645809502_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween>
struct  TweenRunner_1_t645809502  : public RuntimeObject
{
public:
	// UnityEngine.MonoBehaviour UnityEngine.UI.CoroutineTween.TweenRunner`1::m_CoroutineContainer
	MonoBehaviour_t345688271 * ___m_CoroutineContainer_0;
	// System.Collections.IEnumerator UnityEngine.UI.CoroutineTween.TweenRunner`1::m_Tween
	RuntimeObject* ___m_Tween_1;

public:
	inline static int32_t get_offset_of_m_CoroutineContainer_0() { return static_cast<int32_t>(offsetof(TweenRunner_1_t645809502, ___m_CoroutineContainer_0)); }
	inline MonoBehaviour_t345688271 * get_m_CoroutineContainer_0() const { return ___m_CoroutineContainer_0; }
	inline MonoBehaviour_t345688271 ** get_address_of_m_CoroutineContainer_0() { return &___m_CoroutineContainer_0; }
	inline void set_m_CoroutineContainer_0(MonoBehaviour_t345688271 * value)
	{
		___m_CoroutineContainer_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_CoroutineContainer_0), value);
	}

	inline static int32_t get_offset_of_m_Tween_1() { return static_cast<int32_t>(offsetof(TweenRunner_1_t645809502, ___m_Tween_1)); }
	inline RuntimeObject* get_m_Tween_1() const { return ___m_Tween_1; }
	inline RuntimeObject** get_address_of_m_Tween_1() { return &___m_Tween_1; }
	inline void set_m_Tween_1(RuntimeObject* value)
	{
		___m_Tween_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_Tween_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TWEENRUNNER_1_T645809502_H
#ifndef DICTIONARY_2_T2084444880_H
#define DICTIONARY_2_T2084444880_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.Dictionary`2<System.Object,System.Int32>
struct  Dictionary_2_t2084444880  : public RuntimeObject
{
public:
	// System.Int32[] System.Collections.Generic.Dictionary`2::table
	Int32U5BU5D_t2324750880* ___table_4;
	// System.Collections.Generic.Link[] System.Collections.Generic.Dictionary`2::linkSlots
	LinkU5BU5D_t3095297741* ___linkSlots_5;
	// TKey[] System.Collections.Generic.Dictionary`2::keySlots
	ObjectU5BU5D_t3270211303* ___keySlots_6;
	// TValue[] System.Collections.Generic.Dictionary`2::valueSlots
	Int32U5BU5D_t2324750880* ___valueSlots_7;
	// System.Int32 System.Collections.Generic.Dictionary`2::touchedSlots
	int32_t ___touchedSlots_8;
	// System.Int32 System.Collections.Generic.Dictionary`2::emptySlot
	int32_t ___emptySlot_9;
	// System.Int32 System.Collections.Generic.Dictionary`2::count
	int32_t ___count_10;
	// System.Int32 System.Collections.Generic.Dictionary`2::threshold
	int32_t ___threshold_11;
	// System.Collections.Generic.IEqualityComparer`1<TKey> System.Collections.Generic.Dictionary`2::hcp
	RuntimeObject* ___hcp_12;
	// System.Runtime.Serialization.SerializationInfo System.Collections.Generic.Dictionary`2::serialization_info
	SerializationInfo_t1606602250 * ___serialization_info_13;
	// System.Int32 System.Collections.Generic.Dictionary`2::generation
	int32_t ___generation_14;

public:
	inline static int32_t get_offset_of_table_4() { return static_cast<int32_t>(offsetof(Dictionary_2_t2084444880, ___table_4)); }
	inline Int32U5BU5D_t2324750880* get_table_4() const { return ___table_4; }
	inline Int32U5BU5D_t2324750880** get_address_of_table_4() { return &___table_4; }
	inline void set_table_4(Int32U5BU5D_t2324750880* value)
	{
		___table_4 = value;
		Il2CppCodeGenWriteBarrier((&___table_4), value);
	}

	inline static int32_t get_offset_of_linkSlots_5() { return static_cast<int32_t>(offsetof(Dictionary_2_t2084444880, ___linkSlots_5)); }
	inline LinkU5BU5D_t3095297741* get_linkSlots_5() const { return ___linkSlots_5; }
	inline LinkU5BU5D_t3095297741** get_address_of_linkSlots_5() { return &___linkSlots_5; }
	inline void set_linkSlots_5(LinkU5BU5D_t3095297741* value)
	{
		___linkSlots_5 = value;
		Il2CppCodeGenWriteBarrier((&___linkSlots_5), value);
	}

	inline static int32_t get_offset_of_keySlots_6() { return static_cast<int32_t>(offsetof(Dictionary_2_t2084444880, ___keySlots_6)); }
	inline ObjectU5BU5D_t3270211303* get_keySlots_6() const { return ___keySlots_6; }
	inline ObjectU5BU5D_t3270211303** get_address_of_keySlots_6() { return &___keySlots_6; }
	inline void set_keySlots_6(ObjectU5BU5D_t3270211303* value)
	{
		___keySlots_6 = value;
		Il2CppCodeGenWriteBarrier((&___keySlots_6), value);
	}

	inline static int32_t get_offset_of_valueSlots_7() { return static_cast<int32_t>(offsetof(Dictionary_2_t2084444880, ___valueSlots_7)); }
	inline Int32U5BU5D_t2324750880* get_valueSlots_7() const { return ___valueSlots_7; }
	inline Int32U5BU5D_t2324750880** get_address_of_valueSlots_7() { return &___valueSlots_7; }
	inline void set_valueSlots_7(Int32U5BU5D_t2324750880* value)
	{
		___valueSlots_7 = value;
		Il2CppCodeGenWriteBarrier((&___valueSlots_7), value);
	}

	inline static int32_t get_offset_of_touchedSlots_8() { return static_cast<int32_t>(offsetof(Dictionary_2_t2084444880, ___touchedSlots_8)); }
	inline int32_t get_touchedSlots_8() const { return ___touchedSlots_8; }
	inline int32_t* get_address_of_touchedSlots_8() { return &___touchedSlots_8; }
	inline void set_touchedSlots_8(int32_t value)
	{
		___touchedSlots_8 = value;
	}

	inline static int32_t get_offset_of_emptySlot_9() { return static_cast<int32_t>(offsetof(Dictionary_2_t2084444880, ___emptySlot_9)); }
	inline int32_t get_emptySlot_9() const { return ___emptySlot_9; }
	inline int32_t* get_address_of_emptySlot_9() { return &___emptySlot_9; }
	inline void set_emptySlot_9(int32_t value)
	{
		___emptySlot_9 = value;
	}

	inline static int32_t get_offset_of_count_10() { return static_cast<int32_t>(offsetof(Dictionary_2_t2084444880, ___count_10)); }
	inline int32_t get_count_10() const { return ___count_10; }
	inline int32_t* get_address_of_count_10() { return &___count_10; }
	inline void set_count_10(int32_t value)
	{
		___count_10 = value;
	}

	inline static int32_t get_offset_of_threshold_11() { return static_cast<int32_t>(offsetof(Dictionary_2_t2084444880, ___threshold_11)); }
	inline int32_t get_threshold_11() const { return ___threshold_11; }
	inline int32_t* get_address_of_threshold_11() { return &___threshold_11; }
	inline void set_threshold_11(int32_t value)
	{
		___threshold_11 = value;
	}

	inline static int32_t get_offset_of_hcp_12() { return static_cast<int32_t>(offsetof(Dictionary_2_t2084444880, ___hcp_12)); }
	inline RuntimeObject* get_hcp_12() const { return ___hcp_12; }
	inline RuntimeObject** get_address_of_hcp_12() { return &___hcp_12; }
	inline void set_hcp_12(RuntimeObject* value)
	{
		___hcp_12 = value;
		Il2CppCodeGenWriteBarrier((&___hcp_12), value);
	}

	inline static int32_t get_offset_of_serialization_info_13() { return static_cast<int32_t>(offsetof(Dictionary_2_t2084444880, ___serialization_info_13)); }
	inline SerializationInfo_t1606602250 * get_serialization_info_13() const { return ___serialization_info_13; }
	inline SerializationInfo_t1606602250 ** get_address_of_serialization_info_13() { return &___serialization_info_13; }
	inline void set_serialization_info_13(SerializationInfo_t1606602250 * value)
	{
		___serialization_info_13 = value;
		Il2CppCodeGenWriteBarrier((&___serialization_info_13), value);
	}

	inline static int32_t get_offset_of_generation_14() { return static_cast<int32_t>(offsetof(Dictionary_2_t2084444880, ___generation_14)); }
	inline int32_t get_generation_14() const { return ___generation_14; }
	inline int32_t* get_address_of_generation_14() { return &___generation_14; }
	inline void set_generation_14(int32_t value)
	{
		___generation_14 = value;
	}
};

struct Dictionary_2_t2084444880_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,System.Collections.DictionaryEntry> System.Collections.Generic.Dictionary`2::<>f__am$cacheB
	Transform_1_t2272181723 * ___U3CU3Ef__amU24cacheB_15;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cacheB_15() { return static_cast<int32_t>(offsetof(Dictionary_2_t2084444880_StaticFields, ___U3CU3Ef__amU24cacheB_15)); }
	inline Transform_1_t2272181723 * get_U3CU3Ef__amU24cacheB_15() const { return ___U3CU3Ef__amU24cacheB_15; }
	inline Transform_1_t2272181723 ** get_address_of_U3CU3Ef__amU24cacheB_15() { return &___U3CU3Ef__amU24cacheB_15; }
	inline void set_U3CU3Ef__amU24cacheB_15(Transform_1_t2272181723 * value)
	{
		___U3CU3Ef__amU24cacheB_15 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cacheB_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DICTIONARY_2_T2084444880_H
#ifndef LIST_1_T877107497_H
#define LIST_1_T877107497_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1<System.Object>
struct  List_1_t877107497  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	ObjectU5BU5D_t3270211303* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t877107497, ____items_1)); }
	inline ObjectU5BU5D_t3270211303* get__items_1() const { return ____items_1; }
	inline ObjectU5BU5D_t3270211303** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(ObjectU5BU5D_t3270211303* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((&____items_1), value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t877107497, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t877107497, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}
};

struct List_1_t877107497_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::EmptyArray
	ObjectU5BU5D_t3270211303* ___EmptyArray_4;

public:
	inline static int32_t get_offset_of_EmptyArray_4() { return static_cast<int32_t>(offsetof(List_1_t877107497_StaticFields, ___EmptyArray_4)); }
	inline ObjectU5BU5D_t3270211303* get_EmptyArray_4() const { return ___EmptyArray_4; }
	inline ObjectU5BU5D_t3270211303** get_address_of_EmptyArray_4() { return &___EmptyArray_4; }
	inline void set_EmptyArray_4(ObjectU5BU5D_t3270211303* value)
	{
		___EmptyArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIST_1_T877107497_H
#ifndef INDEXEDSET_1_T2088528067_H
#define INDEXEDSET_1_T2088528067_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Collections.IndexedSet`1<System.Object>
struct  IndexedSet_1_t2088528067  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<T> UnityEngine.UI.Collections.IndexedSet`1::m_List
	List_1_t877107497 * ___m_List_0;
	// System.Collections.Generic.Dictionary`2<T,System.Int32> UnityEngine.UI.Collections.IndexedSet`1::m_Dictionary
	Dictionary_2_t2084444880 * ___m_Dictionary_1;

public:
	inline static int32_t get_offset_of_m_List_0() { return static_cast<int32_t>(offsetof(IndexedSet_1_t2088528067, ___m_List_0)); }
	inline List_1_t877107497 * get_m_List_0() const { return ___m_List_0; }
	inline List_1_t877107497 ** get_address_of_m_List_0() { return &___m_List_0; }
	inline void set_m_List_0(List_1_t877107497 * value)
	{
		___m_List_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_List_0), value);
	}

	inline static int32_t get_offset_of_m_Dictionary_1() { return static_cast<int32_t>(offsetof(IndexedSet_1_t2088528067, ___m_Dictionary_1)); }
	inline Dictionary_2_t2084444880 * get_m_Dictionary_1() const { return ___m_Dictionary_1; }
	inline Dictionary_2_t2084444880 ** get_address_of_m_Dictionary_1() { return &___m_Dictionary_1; }
	inline void set_m_Dictionary_1(Dictionary_2_t2084444880 * value)
	{
		___m_Dictionary_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_Dictionary_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INDEXEDSET_1_T2088528067_H
#ifndef UNITYEVENTBASE_T2008500469_H
#define UNITYEVENTBASE_T2008500469_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEventBase
struct  UnityEventBase_t2008500469  : public RuntimeObject
{
public:
	// UnityEngine.Events.InvokableCallList UnityEngine.Events.UnityEventBase::m_Calls
	InvokableCallList_t462442189 * ___m_Calls_0;
	// UnityEngine.Events.PersistentCallGroup UnityEngine.Events.UnityEventBase::m_PersistentCalls
	PersistentCallGroup_t1612611655 * ___m_PersistentCalls_1;
	// System.String UnityEngine.Events.UnityEventBase::m_TypeName
	String_t* ___m_TypeName_2;
	// System.Boolean UnityEngine.Events.UnityEventBase::m_CallsDirty
	bool ___m_CallsDirty_3;

public:
	inline static int32_t get_offset_of_m_Calls_0() { return static_cast<int32_t>(offsetof(UnityEventBase_t2008500469, ___m_Calls_0)); }
	inline InvokableCallList_t462442189 * get_m_Calls_0() const { return ___m_Calls_0; }
	inline InvokableCallList_t462442189 ** get_address_of_m_Calls_0() { return &___m_Calls_0; }
	inline void set_m_Calls_0(InvokableCallList_t462442189 * value)
	{
		___m_Calls_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Calls_0), value);
	}

	inline static int32_t get_offset_of_m_PersistentCalls_1() { return static_cast<int32_t>(offsetof(UnityEventBase_t2008500469, ___m_PersistentCalls_1)); }
	inline PersistentCallGroup_t1612611655 * get_m_PersistentCalls_1() const { return ___m_PersistentCalls_1; }
	inline PersistentCallGroup_t1612611655 ** get_address_of_m_PersistentCalls_1() { return &___m_PersistentCalls_1; }
	inline void set_m_PersistentCalls_1(PersistentCallGroup_t1612611655 * value)
	{
		___m_PersistentCalls_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_PersistentCalls_1), value);
	}

	inline static int32_t get_offset_of_m_TypeName_2() { return static_cast<int32_t>(offsetof(UnityEventBase_t2008500469, ___m_TypeName_2)); }
	inline String_t* get_m_TypeName_2() const { return ___m_TypeName_2; }
	inline String_t** get_address_of_m_TypeName_2() { return &___m_TypeName_2; }
	inline void set_m_TypeName_2(String_t* value)
	{
		___m_TypeName_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_TypeName_2), value);
	}

	inline static int32_t get_offset_of_m_CallsDirty_3() { return static_cast<int32_t>(offsetof(UnityEventBase_t2008500469, ___m_CallsDirty_3)); }
	inline bool get_m_CallsDirty_3() const { return ___m_CallsDirty_3; }
	inline bool* get_address_of_m_CallsDirty_3() { return &___m_CallsDirty_3; }
	inline void set_m_CallsDirty_3(bool value)
	{
		___m_CallsDirty_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENTBASE_T2008500469_H
#ifndef LISTPOOL_1_T1536390840_H
#define LISTPOOL_1_T1536390840_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.ListPool`1<UnityEngine.UIVertex>
struct  ListPool_1_t1536390840  : public RuntimeObject
{
public:

public:
};

struct ListPool_1_t1536390840_StaticFields
{
public:
	// UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<T>> UnityEngine.UI.ListPool`1::s_ListPool
	ObjectPool_1_t2484334276 * ___s_ListPool_0;

public:
	inline static int32_t get_offset_of_s_ListPool_0() { return static_cast<int32_t>(offsetof(ListPool_1_t1536390840_StaticFields, ___s_ListPool_0)); }
	inline ObjectPool_1_t2484334276 * get_s_ListPool_0() const { return ___s_ListPool_0; }
	inline ObjectPool_1_t2484334276 ** get_address_of_s_ListPool_0() { return &___s_ListPool_0; }
	inline void set_s_ListPool_0(ObjectPool_1_t2484334276 * value)
	{
		___s_ListPool_0 = value;
		Il2CppCodeGenWriteBarrier((&___s_ListPool_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LISTPOOL_1_T1536390840_H
#ifndef LIST_1_T1641616647_H
#define LIST_1_T1641616647_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1<UnityEngine.UIVertex>
struct  List_1_t1641616647  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	UIVertexU5BU5D_t227469777* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t1641616647, ____items_1)); }
	inline UIVertexU5BU5D_t227469777* get__items_1() const { return ____items_1; }
	inline UIVertexU5BU5D_t227469777** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(UIVertexU5BU5D_t227469777* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((&____items_1), value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t1641616647, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t1641616647, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}
};

struct List_1_t1641616647_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::EmptyArray
	UIVertexU5BU5D_t227469777* ___EmptyArray_4;

public:
	inline static int32_t get_offset_of_EmptyArray_4() { return static_cast<int32_t>(offsetof(List_1_t1641616647_StaticFields, ___EmptyArray_4)); }
	inline UIVertexU5BU5D_t227469777* get_EmptyArray_4() const { return ___EmptyArray_4; }
	inline UIVertexU5BU5D_t227469777** get_address_of_EmptyArray_4() { return &___EmptyArray_4; }
	inline void set_EmptyArray_4(UIVertexU5BU5D_t227469777* value)
	{
		___EmptyArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIST_1_T1641616647_H
#ifndef OBJECTPOOL_1_T2484334276_H
#define OBJECTPOOL_1_T2484334276_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.UIVertex>>
struct  ObjectPool_1_t2484334276  : public RuntimeObject
{
public:
	// System.Collections.Generic.Stack`1<T> UnityEngine.UI.ObjectPool`1::m_Stack
	Stack_1_t228393273 * ___m_Stack_0;
	// UnityEngine.Events.UnityAction`1<T> UnityEngine.UI.ObjectPool`1::m_ActionOnGet
	UnityAction_1_t894467439 * ___m_ActionOnGet_1;
	// UnityEngine.Events.UnityAction`1<T> UnityEngine.UI.ObjectPool`1::m_ActionOnRelease
	UnityAction_1_t894467439 * ___m_ActionOnRelease_2;
	// System.Int32 UnityEngine.UI.ObjectPool`1::<countAll>k__BackingField
	int32_t ___U3CcountAllU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_m_Stack_0() { return static_cast<int32_t>(offsetof(ObjectPool_1_t2484334276, ___m_Stack_0)); }
	inline Stack_1_t228393273 * get_m_Stack_0() const { return ___m_Stack_0; }
	inline Stack_1_t228393273 ** get_address_of_m_Stack_0() { return &___m_Stack_0; }
	inline void set_m_Stack_0(Stack_1_t228393273 * value)
	{
		___m_Stack_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Stack_0), value);
	}

	inline static int32_t get_offset_of_m_ActionOnGet_1() { return static_cast<int32_t>(offsetof(ObjectPool_1_t2484334276, ___m_ActionOnGet_1)); }
	inline UnityAction_1_t894467439 * get_m_ActionOnGet_1() const { return ___m_ActionOnGet_1; }
	inline UnityAction_1_t894467439 ** get_address_of_m_ActionOnGet_1() { return &___m_ActionOnGet_1; }
	inline void set_m_ActionOnGet_1(UnityAction_1_t894467439 * value)
	{
		___m_ActionOnGet_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_ActionOnGet_1), value);
	}

	inline static int32_t get_offset_of_m_ActionOnRelease_2() { return static_cast<int32_t>(offsetof(ObjectPool_1_t2484334276, ___m_ActionOnRelease_2)); }
	inline UnityAction_1_t894467439 * get_m_ActionOnRelease_2() const { return ___m_ActionOnRelease_2; }
	inline UnityAction_1_t894467439 ** get_address_of_m_ActionOnRelease_2() { return &___m_ActionOnRelease_2; }
	inline void set_m_ActionOnRelease_2(UnityAction_1_t894467439 * value)
	{
		___m_ActionOnRelease_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_ActionOnRelease_2), value);
	}

	inline static int32_t get_offset_of_U3CcountAllU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(ObjectPool_1_t2484334276, ___U3CcountAllU3Ek__BackingField_3)); }
	inline int32_t get_U3CcountAllU3Ek__BackingField_3() const { return ___U3CcountAllU3Ek__BackingField_3; }
	inline int32_t* get_address_of_U3CcountAllU3Ek__BackingField_3() { return &___U3CcountAllU3Ek__BackingField_3; }
	inline void set_U3CcountAllU3Ek__BackingField_3(int32_t value)
	{
		___U3CcountAllU3Ek__BackingField_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OBJECTPOOL_1_T2484334276_H
#ifndef LISTPOOL_1_T2327256662_H
#define LISTPOOL_1_T2327256662_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.ListPool`1<UnityEngine.Vector2>
struct  ListPool_1_t2327256662  : public RuntimeObject
{
public:

public:
};

struct ListPool_1_t2327256662_StaticFields
{
public:
	// UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<T>> UnityEngine.UI.ListPool`1::s_ListPool
	ObjectPool_1_t3275200098 * ___s_ListPool_0;

public:
	inline static int32_t get_offset_of_s_ListPool_0() { return static_cast<int32_t>(offsetof(ListPool_1_t2327256662_StaticFields, ___s_ListPool_0)); }
	inline ObjectPool_1_t3275200098 * get_s_ListPool_0() const { return ___s_ListPool_0; }
	inline ObjectPool_1_t3275200098 ** get_address_of_s_ListPool_0() { return &___s_ListPool_0; }
	inline void set_s_ListPool_0(ObjectPool_1_t3275200098 * value)
	{
		___s_ListPool_0 = value;
		Il2CppCodeGenWriteBarrier((&___s_ListPool_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LISTPOOL_1_T2327256662_H
#ifndef YIELDINSTRUCTION_T1893592390_H
#define YIELDINSTRUCTION_T1893592390_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.YieldInstruction
struct  YieldInstruction_t1893592390  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.YieldInstruction
struct YieldInstruction_t1893592390_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.YieldInstruction
struct YieldInstruction_t1893592390_marshaled_com
{
};
#endif // YIELDINSTRUCTION_T1893592390_H
#ifndef ABSTRACTEVENTDATA_T3507316000_H
#define ABSTRACTEVENTDATA_T3507316000_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.AbstractEventData
struct  AbstractEventData_t3507316000  : public RuntimeObject
{
public:
	// System.Boolean UnityEngine.EventSystems.AbstractEventData::m_Used
	bool ___m_Used_0;

public:
	inline static int32_t get_offset_of_m_Used_0() { return static_cast<int32_t>(offsetof(AbstractEventData_t3507316000, ___m_Used_0)); }
	inline bool get_m_Used_0() const { return ___m_Used_0; }
	inline bool* get_address_of_m_Used_0() { return &___m_Used_0; }
	inline void set_m_Used_0(bool value)
	{
		___m_Used_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ABSTRACTEVENTDATA_T3507316000_H
#ifndef MEMBERINFO_T_H
#define MEMBERINFO_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.MemberInfo
struct  MemberInfo_t  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MEMBERINFO_T_H
#ifndef VALUETYPE_T1306072465_H
#define VALUETYPE_T1306072465_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t1306072465  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t1306072465_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t1306072465_marshaled_com
{
};
#endif // VALUETYPE_T1306072465_H
#ifndef STACK_1_T608734916_H
#define STACK_1_T608734916_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.Stack`1<System.Object>
struct  Stack_1_t608734916  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.Stack`1::_array
	ObjectU5BU5D_t3270211303* ____array_0;
	// System.Int32 System.Collections.Generic.Stack`1::_size
	int32_t ____size_1;
	// System.Int32 System.Collections.Generic.Stack`1::_version
	int32_t ____version_2;

public:
	inline static int32_t get_offset_of__array_0() { return static_cast<int32_t>(offsetof(Stack_1_t608734916, ____array_0)); }
	inline ObjectU5BU5D_t3270211303* get__array_0() const { return ____array_0; }
	inline ObjectU5BU5D_t3270211303** get_address_of__array_0() { return &____array_0; }
	inline void set__array_0(ObjectU5BU5D_t3270211303* value)
	{
		____array_0 = value;
		Il2CppCodeGenWriteBarrier((&____array_0), value);
	}

	inline static int32_t get_offset_of__size_1() { return static_cast<int32_t>(offsetof(Stack_1_t608734916, ____size_1)); }
	inline int32_t get__size_1() const { return ____size_1; }
	inline int32_t* get_address_of__size_1() { return &____size_1; }
	inline void set__size_1(int32_t value)
	{
		____size_1 = value;
	}

	inline static int32_t get_offset_of__version_2() { return static_cast<int32_t>(offsetof(Stack_1_t608734916, ____version_2)); }
	inline int32_t get__version_2() const { return ____version_2; }
	inline int32_t* get_address_of__version_2() { return &____version_2; }
	inline void set__version_2(int32_t value)
	{
		____version_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STACK_1_T608734916_H
#ifndef OBJECTPOOL_1_T2864675919_H
#define OBJECTPOOL_1_T2864675919_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.ObjectPool`1<System.Object>
struct  ObjectPool_1_t2864675919  : public RuntimeObject
{
public:
	// System.Collections.Generic.Stack`1<T> UnityEngine.UI.ObjectPool`1::m_Stack
	Stack_1_t608734916 * ___m_Stack_0;
	// UnityEngine.Events.UnityAction`1<T> UnityEngine.UI.ObjectPool`1::m_ActionOnGet
	UnityAction_1_t1274809082 * ___m_ActionOnGet_1;
	// UnityEngine.Events.UnityAction`1<T> UnityEngine.UI.ObjectPool`1::m_ActionOnRelease
	UnityAction_1_t1274809082 * ___m_ActionOnRelease_2;
	// System.Int32 UnityEngine.UI.ObjectPool`1::<countAll>k__BackingField
	int32_t ___U3CcountAllU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_m_Stack_0() { return static_cast<int32_t>(offsetof(ObjectPool_1_t2864675919, ___m_Stack_0)); }
	inline Stack_1_t608734916 * get_m_Stack_0() const { return ___m_Stack_0; }
	inline Stack_1_t608734916 ** get_address_of_m_Stack_0() { return &___m_Stack_0; }
	inline void set_m_Stack_0(Stack_1_t608734916 * value)
	{
		___m_Stack_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Stack_0), value);
	}

	inline static int32_t get_offset_of_m_ActionOnGet_1() { return static_cast<int32_t>(offsetof(ObjectPool_1_t2864675919, ___m_ActionOnGet_1)); }
	inline UnityAction_1_t1274809082 * get_m_ActionOnGet_1() const { return ___m_ActionOnGet_1; }
	inline UnityAction_1_t1274809082 ** get_address_of_m_ActionOnGet_1() { return &___m_ActionOnGet_1; }
	inline void set_m_ActionOnGet_1(UnityAction_1_t1274809082 * value)
	{
		___m_ActionOnGet_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_ActionOnGet_1), value);
	}

	inline static int32_t get_offset_of_m_ActionOnRelease_2() { return static_cast<int32_t>(offsetof(ObjectPool_1_t2864675919, ___m_ActionOnRelease_2)); }
	inline UnityAction_1_t1274809082 * get_m_ActionOnRelease_2() const { return ___m_ActionOnRelease_2; }
	inline UnityAction_1_t1274809082 ** get_address_of_m_ActionOnRelease_2() { return &___m_ActionOnRelease_2; }
	inline void set_m_ActionOnRelease_2(UnityAction_1_t1274809082 * value)
	{
		___m_ActionOnRelease_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_ActionOnRelease_2), value);
	}

	inline static int32_t get_offset_of_U3CcountAllU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(ObjectPool_1_t2864675919, ___U3CcountAllU3Ek__BackingField_3)); }
	inline int32_t get_U3CcountAllU3Ek__BackingField_3() const { return ___U3CcountAllU3Ek__BackingField_3; }
	inline int32_t* get_address_of_U3CcountAllU3Ek__BackingField_3() { return &___U3CcountAllU3Ek__BackingField_3; }
	inline void set_U3CcountAllU3Ek__BackingField_3(int32_t value)
	{
		___U3CcountAllU3Ek__BackingField_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OBJECTPOOL_1_T2864675919_H
#ifndef STRING_T_H
#define STRING_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.String
struct  String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::length
	int32_t ___length_0;
	// System.Char System.String::start_char
	Il2CppChar ___start_char_1;

public:
	inline static int32_t get_offset_of_length_0() { return static_cast<int32_t>(offsetof(String_t, ___length_0)); }
	inline int32_t get_length_0() const { return ___length_0; }
	inline int32_t* get_address_of_length_0() { return &___length_0; }
	inline void set_length_0(int32_t value)
	{
		___length_0 = value;
	}

	inline static int32_t get_offset_of_start_char_1() { return static_cast<int32_t>(offsetof(String_t, ___start_char_1)); }
	inline Il2CppChar get_start_char_1() const { return ___start_char_1; }
	inline Il2CppChar* get_address_of_start_char_1() { return &___start_char_1; }
	inline void set_start_char_1(Il2CppChar value)
	{
		___start_char_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_2;
	// System.Char[] System.String::WhiteChars
	CharU5BU5D_t45629911* ___WhiteChars_3;

public:
	inline static int32_t get_offset_of_Empty_2() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_2)); }
	inline String_t* get_Empty_2() const { return ___Empty_2; }
	inline String_t** get_address_of_Empty_2() { return &___Empty_2; }
	inline void set_Empty_2(String_t* value)
	{
		___Empty_2 = value;
		Il2CppCodeGenWriteBarrier((&___Empty_2), value);
	}

	inline static int32_t get_offset_of_WhiteChars_3() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___WhiteChars_3)); }
	inline CharU5BU5D_t45629911* get_WhiteChars_3() const { return ___WhiteChars_3; }
	inline CharU5BU5D_t45629911** get_address_of_WhiteChars_3() { return &___WhiteChars_3; }
	inline void set_WhiteChars_3(CharU5BU5D_t45629911* value)
	{
		___WhiteChars_3 = value;
		Il2CppCodeGenWriteBarrier((&___WhiteChars_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STRING_T_H
#ifndef OBJECTPOOL_1_T2512539181_H
#define OBJECTPOOL_1_T2512539181_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.Vector4>>
struct  ObjectPool_1_t2512539181  : public RuntimeObject
{
public:
	// System.Collections.Generic.Stack`1<T> UnityEngine.UI.ObjectPool`1::m_Stack
	Stack_1_t256598178 * ___m_Stack_0;
	// UnityEngine.Events.UnityAction`1<T> UnityEngine.UI.ObjectPool`1::m_ActionOnGet
	UnityAction_1_t922672344 * ___m_ActionOnGet_1;
	// UnityEngine.Events.UnityAction`1<T> UnityEngine.UI.ObjectPool`1::m_ActionOnRelease
	UnityAction_1_t922672344 * ___m_ActionOnRelease_2;
	// System.Int32 UnityEngine.UI.ObjectPool`1::<countAll>k__BackingField
	int32_t ___U3CcountAllU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_m_Stack_0() { return static_cast<int32_t>(offsetof(ObjectPool_1_t2512539181, ___m_Stack_0)); }
	inline Stack_1_t256598178 * get_m_Stack_0() const { return ___m_Stack_0; }
	inline Stack_1_t256598178 ** get_address_of_m_Stack_0() { return &___m_Stack_0; }
	inline void set_m_Stack_0(Stack_1_t256598178 * value)
	{
		___m_Stack_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Stack_0), value);
	}

	inline static int32_t get_offset_of_m_ActionOnGet_1() { return static_cast<int32_t>(offsetof(ObjectPool_1_t2512539181, ___m_ActionOnGet_1)); }
	inline UnityAction_1_t922672344 * get_m_ActionOnGet_1() const { return ___m_ActionOnGet_1; }
	inline UnityAction_1_t922672344 ** get_address_of_m_ActionOnGet_1() { return &___m_ActionOnGet_1; }
	inline void set_m_ActionOnGet_1(UnityAction_1_t922672344 * value)
	{
		___m_ActionOnGet_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_ActionOnGet_1), value);
	}

	inline static int32_t get_offset_of_m_ActionOnRelease_2() { return static_cast<int32_t>(offsetof(ObjectPool_1_t2512539181, ___m_ActionOnRelease_2)); }
	inline UnityAction_1_t922672344 * get_m_ActionOnRelease_2() const { return ___m_ActionOnRelease_2; }
	inline UnityAction_1_t922672344 ** get_address_of_m_ActionOnRelease_2() { return &___m_ActionOnRelease_2; }
	inline void set_m_ActionOnRelease_2(UnityAction_1_t922672344 * value)
	{
		___m_ActionOnRelease_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_ActionOnRelease_2), value);
	}

	inline static int32_t get_offset_of_U3CcountAllU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(ObjectPool_1_t2512539181, ___U3CcountAllU3Ek__BackingField_3)); }
	inline int32_t get_U3CcountAllU3Ek__BackingField_3() const { return ___U3CcountAllU3Ek__BackingField_3; }
	inline int32_t* get_address_of_U3CcountAllU3Ek__BackingField_3() { return &___U3CcountAllU3Ek__BackingField_3; }
	inline void set_U3CcountAllU3Ek__BackingField_3(int32_t value)
	{
		___U3CcountAllU3Ek__BackingField_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OBJECTPOOL_1_T2512539181_H
#ifndef LISTPOOL_1_T1564595745_H
#define LISTPOOL_1_T1564595745_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.ListPool`1<UnityEngine.Vector4>
struct  ListPool_1_t1564595745  : public RuntimeObject
{
public:

public:
};

struct ListPool_1_t1564595745_StaticFields
{
public:
	// UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<T>> UnityEngine.UI.ListPool`1::s_ListPool
	ObjectPool_1_t2512539181 * ___s_ListPool_0;

public:
	inline static int32_t get_offset_of_s_ListPool_0() { return static_cast<int32_t>(offsetof(ListPool_1_t1564595745_StaticFields, ___s_ListPool_0)); }
	inline ObjectPool_1_t2512539181 * get_s_ListPool_0() const { return ___s_ListPool_0; }
	inline ObjectPool_1_t2512539181 ** get_address_of_s_ListPool_0() { return &___s_ListPool_0; }
	inline void set_s_ListPool_0(ObjectPool_1_t2512539181 * value)
	{
		___s_ListPool_0 = value;
		Il2CppCodeGenWriteBarrier((&___s_ListPool_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LISTPOOL_1_T1564595745_H
#ifndef OBJECTPOOL_1_T2601397270_H
#define OBJECTPOOL_1_T2601397270_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.Vector3>>
struct  ObjectPool_1_t2601397270  : public RuntimeObject
{
public:
	// System.Collections.Generic.Stack`1<T> UnityEngine.UI.ObjectPool`1::m_Stack
	Stack_1_t345456267 * ___m_Stack_0;
	// UnityEngine.Events.UnityAction`1<T> UnityEngine.UI.ObjectPool`1::m_ActionOnGet
	UnityAction_1_t1011530433 * ___m_ActionOnGet_1;
	// UnityEngine.Events.UnityAction`1<T> UnityEngine.UI.ObjectPool`1::m_ActionOnRelease
	UnityAction_1_t1011530433 * ___m_ActionOnRelease_2;
	// System.Int32 UnityEngine.UI.ObjectPool`1::<countAll>k__BackingField
	int32_t ___U3CcountAllU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_m_Stack_0() { return static_cast<int32_t>(offsetof(ObjectPool_1_t2601397270, ___m_Stack_0)); }
	inline Stack_1_t345456267 * get_m_Stack_0() const { return ___m_Stack_0; }
	inline Stack_1_t345456267 ** get_address_of_m_Stack_0() { return &___m_Stack_0; }
	inline void set_m_Stack_0(Stack_1_t345456267 * value)
	{
		___m_Stack_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Stack_0), value);
	}

	inline static int32_t get_offset_of_m_ActionOnGet_1() { return static_cast<int32_t>(offsetof(ObjectPool_1_t2601397270, ___m_ActionOnGet_1)); }
	inline UnityAction_1_t1011530433 * get_m_ActionOnGet_1() const { return ___m_ActionOnGet_1; }
	inline UnityAction_1_t1011530433 ** get_address_of_m_ActionOnGet_1() { return &___m_ActionOnGet_1; }
	inline void set_m_ActionOnGet_1(UnityAction_1_t1011530433 * value)
	{
		___m_ActionOnGet_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_ActionOnGet_1), value);
	}

	inline static int32_t get_offset_of_m_ActionOnRelease_2() { return static_cast<int32_t>(offsetof(ObjectPool_1_t2601397270, ___m_ActionOnRelease_2)); }
	inline UnityAction_1_t1011530433 * get_m_ActionOnRelease_2() const { return ___m_ActionOnRelease_2; }
	inline UnityAction_1_t1011530433 ** get_address_of_m_ActionOnRelease_2() { return &___m_ActionOnRelease_2; }
	inline void set_m_ActionOnRelease_2(UnityAction_1_t1011530433 * value)
	{
		___m_ActionOnRelease_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_ActionOnRelease_2), value);
	}

	inline static int32_t get_offset_of_U3CcountAllU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(ObjectPool_1_t2601397270, ___U3CcountAllU3Ek__BackingField_3)); }
	inline int32_t get_U3CcountAllU3Ek__BackingField_3() const { return ___U3CcountAllU3Ek__BackingField_3; }
	inline int32_t* get_address_of_U3CcountAllU3Ek__BackingField_3() { return &___U3CcountAllU3Ek__BackingField_3; }
	inline void set_U3CcountAllU3Ek__BackingField_3(int32_t value)
	{
		___U3CcountAllU3Ek__BackingField_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OBJECTPOOL_1_T2601397270_H
#ifndef LIST_1_T1758679641_H
#define LIST_1_T1758679641_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1<UnityEngine.Vector3>
struct  List_1_t1758679641  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	Vector3U5BU5D_t981680887* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t1758679641, ____items_1)); }
	inline Vector3U5BU5D_t981680887* get__items_1() const { return ____items_1; }
	inline Vector3U5BU5D_t981680887** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(Vector3U5BU5D_t981680887* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((&____items_1), value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t1758679641, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t1758679641, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}
};

struct List_1_t1758679641_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::EmptyArray
	Vector3U5BU5D_t981680887* ___EmptyArray_4;

public:
	inline static int32_t get_offset_of_EmptyArray_4() { return static_cast<int32_t>(offsetof(List_1_t1758679641_StaticFields, ___EmptyArray_4)); }
	inline Vector3U5BU5D_t981680887* get_EmptyArray_4() const { return ___EmptyArray_4; }
	inline Vector3U5BU5D_t981680887** get_address_of_EmptyArray_4() { return &___EmptyArray_4; }
	inline void set_EmptyArray_4(Vector3U5BU5D_t981680887* value)
	{
		___EmptyArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIST_1_T1758679641_H
#ifndef LISTPOOL_1_T1653453834_H
#define LISTPOOL_1_T1653453834_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.ListPool`1<UnityEngine.Vector3>
struct  ListPool_1_t1653453834  : public RuntimeObject
{
public:

public:
};

struct ListPool_1_t1653453834_StaticFields
{
public:
	// UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<T>> UnityEngine.UI.ListPool`1::s_ListPool
	ObjectPool_1_t2601397270 * ___s_ListPool_0;

public:
	inline static int32_t get_offset_of_s_ListPool_0() { return static_cast<int32_t>(offsetof(ListPool_1_t1653453834_StaticFields, ___s_ListPool_0)); }
	inline ObjectPool_1_t2601397270 * get_s_ListPool_0() const { return ___s_ListPool_0; }
	inline ObjectPool_1_t2601397270 ** get_address_of_s_ListPool_0() { return &___s_ListPool_0; }
	inline void set_s_ListPool_0(ObjectPool_1_t2601397270 * value)
	{
		___s_ListPool_0 = value;
		Il2CppCodeGenWriteBarrier((&___s_ListPool_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LISTPOOL_1_T1653453834_H
#ifndef OBJECTPOOL_1_T3275200098_H
#define OBJECTPOOL_1_T3275200098_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.Vector2>>
struct  ObjectPool_1_t3275200098  : public RuntimeObject
{
public:
	// System.Collections.Generic.Stack`1<T> UnityEngine.UI.ObjectPool`1::m_Stack
	Stack_1_t1019259095 * ___m_Stack_0;
	// UnityEngine.Events.UnityAction`1<T> UnityEngine.UI.ObjectPool`1::m_ActionOnGet
	UnityAction_1_t1685333261 * ___m_ActionOnGet_1;
	// UnityEngine.Events.UnityAction`1<T> UnityEngine.UI.ObjectPool`1::m_ActionOnRelease
	UnityAction_1_t1685333261 * ___m_ActionOnRelease_2;
	// System.Int32 UnityEngine.UI.ObjectPool`1::<countAll>k__BackingField
	int32_t ___U3CcountAllU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_m_Stack_0() { return static_cast<int32_t>(offsetof(ObjectPool_1_t3275200098, ___m_Stack_0)); }
	inline Stack_1_t1019259095 * get_m_Stack_0() const { return ___m_Stack_0; }
	inline Stack_1_t1019259095 ** get_address_of_m_Stack_0() { return &___m_Stack_0; }
	inline void set_m_Stack_0(Stack_1_t1019259095 * value)
	{
		___m_Stack_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Stack_0), value);
	}

	inline static int32_t get_offset_of_m_ActionOnGet_1() { return static_cast<int32_t>(offsetof(ObjectPool_1_t3275200098, ___m_ActionOnGet_1)); }
	inline UnityAction_1_t1685333261 * get_m_ActionOnGet_1() const { return ___m_ActionOnGet_1; }
	inline UnityAction_1_t1685333261 ** get_address_of_m_ActionOnGet_1() { return &___m_ActionOnGet_1; }
	inline void set_m_ActionOnGet_1(UnityAction_1_t1685333261 * value)
	{
		___m_ActionOnGet_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_ActionOnGet_1), value);
	}

	inline static int32_t get_offset_of_m_ActionOnRelease_2() { return static_cast<int32_t>(offsetof(ObjectPool_1_t3275200098, ___m_ActionOnRelease_2)); }
	inline UnityAction_1_t1685333261 * get_m_ActionOnRelease_2() const { return ___m_ActionOnRelease_2; }
	inline UnityAction_1_t1685333261 ** get_address_of_m_ActionOnRelease_2() { return &___m_ActionOnRelease_2; }
	inline void set_m_ActionOnRelease_2(UnityAction_1_t1685333261 * value)
	{
		___m_ActionOnRelease_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_ActionOnRelease_2), value);
	}

	inline static int32_t get_offset_of_U3CcountAllU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(ObjectPool_1_t3275200098, ___U3CcountAllU3Ek__BackingField_3)); }
	inline int32_t get_U3CcountAllU3Ek__BackingField_3() const { return ___U3CcountAllU3Ek__BackingField_3; }
	inline int32_t* get_address_of_U3CcountAllU3Ek__BackingField_3() { return &___U3CcountAllU3Ek__BackingField_3; }
	inline void set_U3CcountAllU3Ek__BackingField_3(int32_t value)
	{
		___U3CcountAllU3Ek__BackingField_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OBJECTPOOL_1_T3275200098_H
#ifndef LIST_1_T2432482469_H
#define LIST_1_T2432482469_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1<UnityEngine.Vector2>
struct  List_1_t2432482469  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	Vector2U5BU5D_t3916253691* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t2432482469, ____items_1)); }
	inline Vector2U5BU5D_t3916253691* get__items_1() const { return ____items_1; }
	inline Vector2U5BU5D_t3916253691** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(Vector2U5BU5D_t3916253691* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((&____items_1), value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t2432482469, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t2432482469, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}
};

struct List_1_t2432482469_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::EmptyArray
	Vector2U5BU5D_t3916253691* ___EmptyArray_4;

public:
	inline static int32_t get_offset_of_EmptyArray_4() { return static_cast<int32_t>(offsetof(List_1_t2432482469_StaticFields, ___EmptyArray_4)); }
	inline Vector2U5BU5D_t3916253691* get_EmptyArray_4() const { return ___EmptyArray_4; }
	inline Vector2U5BU5D_t3916253691** get_address_of_EmptyArray_4() { return &___EmptyArray_4; }
	inline void set_EmptyArray_4(Vector2U5BU5D_t3916253691* value)
	{
		___EmptyArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIST_1_T2432482469_H
#ifndef LIST_1_T1669821552_H
#define LIST_1_T1669821552_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1<UnityEngine.Vector4>
struct  List_1_t1669821552  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	Vector4U5BU5D_t2124741284* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t1669821552, ____items_1)); }
	inline Vector4U5BU5D_t2124741284* get__items_1() const { return ____items_1; }
	inline Vector4U5BU5D_t2124741284** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(Vector4U5BU5D_t2124741284* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((&____items_1), value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t1669821552, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t1669821552, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}
};

struct List_1_t1669821552_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::EmptyArray
	Vector4U5BU5D_t2124741284* ___EmptyArray_4;

public:
	inline static int32_t get_offset_of_EmptyArray_4() { return static_cast<int32_t>(offsetof(List_1_t1669821552_StaticFields, ___EmptyArray_4)); }
	inline Vector4U5BU5D_t2124741284* get_EmptyArray_4() const { return ___EmptyArray_4; }
	inline Vector4U5BU5D_t2124741284** get_address_of_EmptyArray_4() { return &___EmptyArray_4; }
	inline void set_EmptyArray_4(Vector4U5BU5D_t2124741284* value)
	{
		___EmptyArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIST_1_T1669821552_H
#ifndef BASEINVOKABLECALL_T1352930348_H
#define BASEINVOKABLECALL_T1352930348_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.BaseInvokableCall
struct  BaseInvokableCall_t1352930348  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASEINVOKABLECALL_T1352930348_H
#ifndef EXCEPTION_T1975590229_H
#define EXCEPTION_T1975590229_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Exception
struct  Exception_t1975590229  : public RuntimeObject
{
public:
	// System.IntPtr[] System.Exception::trace_ips
	IntPtrU5BU5D_t439965300* ___trace_ips_0;
	// System.Exception System.Exception::inner_exception
	Exception_t1975590229 * ___inner_exception_1;
	// System.String System.Exception::message
	String_t* ___message_2;
	// System.String System.Exception::help_link
	String_t* ___help_link_3;
	// System.String System.Exception::class_name
	String_t* ___class_name_4;
	// System.String System.Exception::stack_trace
	String_t* ___stack_trace_5;
	// System.String System.Exception::_remoteStackTraceString
	String_t* ____remoteStackTraceString_6;
	// System.Int32 System.Exception::remote_stack_index
	int32_t ___remote_stack_index_7;
	// System.Int32 System.Exception::hresult
	int32_t ___hresult_8;
	// System.String System.Exception::source
	String_t* ___source_9;
	// System.Collections.IDictionary System.Exception::_data
	RuntimeObject* ____data_10;

public:
	inline static int32_t get_offset_of_trace_ips_0() { return static_cast<int32_t>(offsetof(Exception_t1975590229, ___trace_ips_0)); }
	inline IntPtrU5BU5D_t439965300* get_trace_ips_0() const { return ___trace_ips_0; }
	inline IntPtrU5BU5D_t439965300** get_address_of_trace_ips_0() { return &___trace_ips_0; }
	inline void set_trace_ips_0(IntPtrU5BU5D_t439965300* value)
	{
		___trace_ips_0 = value;
		Il2CppCodeGenWriteBarrier((&___trace_ips_0), value);
	}

	inline static int32_t get_offset_of_inner_exception_1() { return static_cast<int32_t>(offsetof(Exception_t1975590229, ___inner_exception_1)); }
	inline Exception_t1975590229 * get_inner_exception_1() const { return ___inner_exception_1; }
	inline Exception_t1975590229 ** get_address_of_inner_exception_1() { return &___inner_exception_1; }
	inline void set_inner_exception_1(Exception_t1975590229 * value)
	{
		___inner_exception_1 = value;
		Il2CppCodeGenWriteBarrier((&___inner_exception_1), value);
	}

	inline static int32_t get_offset_of_message_2() { return static_cast<int32_t>(offsetof(Exception_t1975590229, ___message_2)); }
	inline String_t* get_message_2() const { return ___message_2; }
	inline String_t** get_address_of_message_2() { return &___message_2; }
	inline void set_message_2(String_t* value)
	{
		___message_2 = value;
		Il2CppCodeGenWriteBarrier((&___message_2), value);
	}

	inline static int32_t get_offset_of_help_link_3() { return static_cast<int32_t>(offsetof(Exception_t1975590229, ___help_link_3)); }
	inline String_t* get_help_link_3() const { return ___help_link_3; }
	inline String_t** get_address_of_help_link_3() { return &___help_link_3; }
	inline void set_help_link_3(String_t* value)
	{
		___help_link_3 = value;
		Il2CppCodeGenWriteBarrier((&___help_link_3), value);
	}

	inline static int32_t get_offset_of_class_name_4() { return static_cast<int32_t>(offsetof(Exception_t1975590229, ___class_name_4)); }
	inline String_t* get_class_name_4() const { return ___class_name_4; }
	inline String_t** get_address_of_class_name_4() { return &___class_name_4; }
	inline void set_class_name_4(String_t* value)
	{
		___class_name_4 = value;
		Il2CppCodeGenWriteBarrier((&___class_name_4), value);
	}

	inline static int32_t get_offset_of_stack_trace_5() { return static_cast<int32_t>(offsetof(Exception_t1975590229, ___stack_trace_5)); }
	inline String_t* get_stack_trace_5() const { return ___stack_trace_5; }
	inline String_t** get_address_of_stack_trace_5() { return &___stack_trace_5; }
	inline void set_stack_trace_5(String_t* value)
	{
		___stack_trace_5 = value;
		Il2CppCodeGenWriteBarrier((&___stack_trace_5), value);
	}

	inline static int32_t get_offset_of__remoteStackTraceString_6() { return static_cast<int32_t>(offsetof(Exception_t1975590229, ____remoteStackTraceString_6)); }
	inline String_t* get__remoteStackTraceString_6() const { return ____remoteStackTraceString_6; }
	inline String_t** get_address_of__remoteStackTraceString_6() { return &____remoteStackTraceString_6; }
	inline void set__remoteStackTraceString_6(String_t* value)
	{
		____remoteStackTraceString_6 = value;
		Il2CppCodeGenWriteBarrier((&____remoteStackTraceString_6), value);
	}

	inline static int32_t get_offset_of_remote_stack_index_7() { return static_cast<int32_t>(offsetof(Exception_t1975590229, ___remote_stack_index_7)); }
	inline int32_t get_remote_stack_index_7() const { return ___remote_stack_index_7; }
	inline int32_t* get_address_of_remote_stack_index_7() { return &___remote_stack_index_7; }
	inline void set_remote_stack_index_7(int32_t value)
	{
		___remote_stack_index_7 = value;
	}

	inline static int32_t get_offset_of_hresult_8() { return static_cast<int32_t>(offsetof(Exception_t1975590229, ___hresult_8)); }
	inline int32_t get_hresult_8() const { return ___hresult_8; }
	inline int32_t* get_address_of_hresult_8() { return &___hresult_8; }
	inline void set_hresult_8(int32_t value)
	{
		___hresult_8 = value;
	}

	inline static int32_t get_offset_of_source_9() { return static_cast<int32_t>(offsetof(Exception_t1975590229, ___source_9)); }
	inline String_t* get_source_9() const { return ___source_9; }
	inline String_t** get_address_of_source_9() { return &___source_9; }
	inline void set_source_9(String_t* value)
	{
		___source_9 = value;
		Il2CppCodeGenWriteBarrier((&___source_9), value);
	}

	inline static int32_t get_offset_of__data_10() { return static_cast<int32_t>(offsetof(Exception_t1975590229, ____data_10)); }
	inline RuntimeObject* get__data_10() const { return ____data_10; }
	inline RuntimeObject** get_address_of__data_10() { return &____data_10; }
	inline void set__data_10(RuntimeObject* value)
	{
		____data_10 = value;
		Il2CppCodeGenWriteBarrier((&____data_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXCEPTION_T1975590229_H
#ifndef BOOLEAN_T2950845069_H
#define BOOLEAN_T2950845069_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Boolean
struct  Boolean_t2950845069 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Boolean_t2950845069, ___m_value_2)); }
	inline bool get_m_value_2() const { return ___m_value_2; }
	inline bool* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(bool value)
	{
		___m_value_2 = value;
	}
};

struct Boolean_t2950845069_StaticFields
{
public:
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_0;
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_1;

public:
	inline static int32_t get_offset_of_FalseString_0() { return static_cast<int32_t>(offsetof(Boolean_t2950845069_StaticFields, ___FalseString_0)); }
	inline String_t* get_FalseString_0() const { return ___FalseString_0; }
	inline String_t** get_address_of_FalseString_0() { return &___FalseString_0; }
	inline void set_FalseString_0(String_t* value)
	{
		___FalseString_0 = value;
		Il2CppCodeGenWriteBarrier((&___FalseString_0), value);
	}

	inline static int32_t get_offset_of_TrueString_1() { return static_cast<int32_t>(offsetof(Boolean_t2950845069_StaticFields, ___TrueString_1)); }
	inline String_t* get_TrueString_1() const { return ___TrueString_1; }
	inline String_t** get_address_of_TrueString_1() { return &___TrueString_1; }
	inline void set_TrueString_1(String_t* value)
	{
		___TrueString_1 = value;
		Il2CppCodeGenWriteBarrier((&___TrueString_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOOLEAN_T2950845069_H
#ifndef INVOKABLECALL_1_T1809982952_H
#define INVOKABLECALL_1_T1809982952_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.InvokableCall`1<UnityEngine.Vector2>
struct  InvokableCall_1_t1809982952  : public BaseInvokableCall_t1352930348
{
public:
	// UnityEngine.Events.UnityAction`1<T1> UnityEngine.Events.InvokableCall`1::Delegate
	UnityAction_1_t2830184054 * ___Delegate_0;

public:
	inline static int32_t get_offset_of_Delegate_0() { return static_cast<int32_t>(offsetof(InvokableCall_1_t1809982952, ___Delegate_0)); }
	inline UnityAction_1_t2830184054 * get_Delegate_0() const { return ___Delegate_0; }
	inline UnityAction_1_t2830184054 ** get_address_of_Delegate_0() { return &___Delegate_0; }
	inline void set_Delegate_0(UnityAction_1_t2830184054 * value)
	{
		___Delegate_0 = value;
		Il2CppCodeGenWriteBarrier((&___Delegate_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INVOKABLECALL_1_T1809982952_H
#ifndef ENUM_T978401890_H
#define ENUM_T978401890_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t978401890  : public ValueType_t1306072465
{
public:

public:
};

struct Enum_t978401890_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t45629911* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t978401890_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t45629911* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t45629911** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t45629911* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t978401890_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t978401890_marshaled_com
{
};
#endif // ENUM_T978401890_H
#ifndef INVOKABLECALL_2_T1353357206_H
#define INVOKABLECALL_2_T1353357206_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.InvokableCall`2<System.Object,System.Object>
struct  InvokableCall_2_t1353357206  : public BaseInvokableCall_t1352930348
{
public:
	// UnityEngine.Events.UnityAction`2<T1,T2> UnityEngine.Events.InvokableCall`2::Delegate
	UnityAction_2_t1132847070 * ___Delegate_0;

public:
	inline static int32_t get_offset_of_Delegate_0() { return static_cast<int32_t>(offsetof(InvokableCall_2_t1353357206, ___Delegate_0)); }
	inline UnityAction_2_t1132847070 * get_Delegate_0() const { return ___Delegate_0; }
	inline UnityAction_2_t1132847070 ** get_address_of_Delegate_0() { return &___Delegate_0; }
	inline void set_Delegate_0(UnityAction_2_t1132847070 * value)
	{
		___Delegate_0 = value;
		Il2CppCodeGenWriteBarrier((&___Delegate_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INVOKABLECALL_2_T1353357206_H
#ifndef INVOKABLECALL_3_T3331941758_H
#define INVOKABLECALL_3_T3331941758_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.InvokableCall`3<System.Object,System.Object,System.Object>
struct  InvokableCall_3_t3331941758  : public BaseInvokableCall_t1352930348
{
public:
	// UnityEngine.Events.UnityAction`3<T1,T2,T3> UnityEngine.Events.InvokableCall`3::Delegate
	UnityAction_3_t1794137105 * ___Delegate_0;

public:
	inline static int32_t get_offset_of_Delegate_0() { return static_cast<int32_t>(offsetof(InvokableCall_3_t3331941758, ___Delegate_0)); }
	inline UnityAction_3_t1794137105 * get_Delegate_0() const { return ___Delegate_0; }
	inline UnityAction_3_t1794137105 ** get_address_of_Delegate_0() { return &___Delegate_0; }
	inline void set_Delegate_0(UnityAction_3_t1794137105 * value)
	{
		___Delegate_0 = value;
		Il2CppCodeGenWriteBarrier((&___Delegate_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INVOKABLECALL_3_T3331941758_H
#ifndef INVOKABLECALL_4_T304489837_H
#define INVOKABLECALL_4_T304489837_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.InvokableCall`4<System.Object,System.Object,System.Object,System.Object>
struct  InvokableCall_4_t304489837  : public BaseInvokableCall_t1352930348
{
public:
	// UnityEngine.Events.UnityAction`4<T1,T2,T3,T4> UnityEngine.Events.InvokableCall`4::Delegate
	UnityAction_4_t226130048 * ___Delegate_0;

public:
	inline static int32_t get_offset_of_Delegate_0() { return static_cast<int32_t>(offsetof(InvokableCall_4_t304489837, ___Delegate_0)); }
	inline UnityAction_4_t226130048 * get_Delegate_0() const { return ___Delegate_0; }
	inline UnityAction_4_t226130048 ** get_address_of_Delegate_0() { return &___Delegate_0; }
	inline void set_Delegate_0(UnityAction_4_t226130048 * value)
	{
		___Delegate_0 = value;
		Il2CppCodeGenWriteBarrier((&___Delegate_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INVOKABLECALL_4_T304489837_H
#ifndef QUATERNION_T754065749_H
#define QUATERNION_T754065749_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Quaternion
struct  Quaternion_t754065749 
{
public:
	// System.Single UnityEngine.Quaternion::x
	float ___x_0;
	// System.Single UnityEngine.Quaternion::y
	float ___y_1;
	// System.Single UnityEngine.Quaternion::z
	float ___z_2;
	// System.Single UnityEngine.Quaternion::w
	float ___w_3;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Quaternion_t754065749, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Quaternion_t754065749, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(Quaternion_t754065749, ___z_2)); }
	inline float get_z_2() const { return ___z_2; }
	inline float* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(float value)
	{
		___z_2 = value;
	}

	inline static int32_t get_offset_of_w_3() { return static_cast<int32_t>(offsetof(Quaternion_t754065749, ___w_3)); }
	inline float get_w_3() const { return ___w_3; }
	inline float* get_address_of_w_3() { return &___w_3; }
	inline void set_w_3(float value)
	{
		___w_3 = value;
	}
};

struct Quaternion_t754065749_StaticFields
{
public:
	// UnityEngine.Quaternion UnityEngine.Quaternion::identityQuaternion
	Quaternion_t754065749  ___identityQuaternion_4;

public:
	inline static int32_t get_offset_of_identityQuaternion_4() { return static_cast<int32_t>(offsetof(Quaternion_t754065749_StaticFields, ___identityQuaternion_4)); }
	inline Quaternion_t754065749  get_identityQuaternion_4() const { return ___identityQuaternion_4; }
	inline Quaternion_t754065749 * get_address_of_identityQuaternion_4() { return &___identityQuaternion_4; }
	inline void set_identityQuaternion_4(Quaternion_t754065749  value)
	{
		___identityQuaternion_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUATERNION_T754065749_H
#ifndef SCENE_T348657329_H
#define SCENE_T348657329_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.SceneManagement.Scene
struct  Scene_t348657329 
{
public:
	// System.Int32 UnityEngine.SceneManagement.Scene::m_Handle
	int32_t ___m_Handle_0;

public:
	inline static int32_t get_offset_of_m_Handle_0() { return static_cast<int32_t>(offsetof(Scene_t348657329, ___m_Handle_0)); }
	inline int32_t get_m_Handle_0() const { return ___m_Handle_0; }
	inline int32_t* get_address_of_m_Handle_0() { return &___m_Handle_0; }
	inline void set_m_Handle_0(int32_t value)
	{
		___m_Handle_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCENE_T348657329_H
#ifndef UNITYEVENT_1_T4001855637_H
#define UNITYEVENT_1_T4001855637_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEvent`1<System.Boolean>
struct  UnityEvent_1_t4001855637  : public UnityEventBase_t2008500469
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`1::m_InvokeArray
	ObjectU5BU5D_t3270211303* ___m_InvokeArray_4;

public:
	inline static int32_t get_offset_of_m_InvokeArray_4() { return static_cast<int32_t>(offsetof(UnityEvent_1_t4001855637, ___m_InvokeArray_4)); }
	inline ObjectU5BU5D_t3270211303* get_m_InvokeArray_4() const { return ___m_InvokeArray_4; }
	inline ObjectU5BU5D_t3270211303** get_address_of_m_InvokeArray_4() { return &___m_InvokeArray_4; }
	inline void set_m_InvokeArray_4(ObjectU5BU5D_t3270211303* value)
	{
		___m_InvokeArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_InvokeArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENT_1_T4001855637_H
#ifndef UNITYEVENT_1_T993987861_H
#define UNITYEVENT_1_T993987861_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEvent`1<System.Int32>
struct  UnityEvent_1_t993987861  : public UnityEventBase_t2008500469
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`1::m_InvokeArray
	ObjectU5BU5D_t3270211303* ___m_InvokeArray_4;

public:
	inline static int32_t get_offset_of_m_InvokeArray_4() { return static_cast<int32_t>(offsetof(UnityEvent_1_t993987861, ___m_InvokeArray_4)); }
	inline ObjectU5BU5D_t3270211303* get_m_InvokeArray_4() const { return ___m_InvokeArray_4; }
	inline ObjectU5BU5D_t3270211303** get_address_of_m_InvokeArray_4() { return &___m_InvokeArray_4; }
	inline void set_m_InvokeArray_4(ObjectU5BU5D_t3270211303* value)
	{
		___m_InvokeArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_InvokeArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENT_1_T993987861_H
#ifndef UNITYEVENT_1_T3072968858_H
#define UNITYEVENT_1_T3072968858_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEvent`1<System.Object>
struct  UnityEvent_1_t3072968858  : public UnityEventBase_t2008500469
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`1::m_InvokeArray
	ObjectU5BU5D_t3270211303* ___m_InvokeArray_4;

public:
	inline static int32_t get_offset_of_m_InvokeArray_4() { return static_cast<int32_t>(offsetof(UnityEvent_1_t3072968858, ___m_InvokeArray_4)); }
	inline ObjectU5BU5D_t3270211303* get_m_InvokeArray_4() const { return ___m_InvokeArray_4; }
	inline ObjectU5BU5D_t3270211303** get_address_of_m_InvokeArray_4() { return &___m_InvokeArray_4; }
	inline void set_m_InvokeArray_4(ObjectU5BU5D_t3270211303* value)
	{
		___m_InvokeArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_InvokeArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENT_1_T3072968858_H
#ifndef UNITYEVENT_1_T4096360327_H
#define UNITYEVENT_1_T4096360327_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEvent`1<System.Single>
struct  UnityEvent_1_t4096360327  : public UnityEventBase_t2008500469
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`1::m_InvokeArray
	ObjectU5BU5D_t3270211303* ___m_InvokeArray_4;

public:
	inline static int32_t get_offset_of_m_InvokeArray_4() { return static_cast<int32_t>(offsetof(UnityEvent_1_t4096360327, ___m_InvokeArray_4)); }
	inline ObjectU5BU5D_t3270211303* get_m_InvokeArray_4() const { return ___m_InvokeArray_4; }
	inline ObjectU5BU5D_t3270211303** get_address_of_m_InvokeArray_4() { return &___m_InvokeArray_4; }
	inline void set_m_InvokeArray_4(ObjectU5BU5D_t3270211303* value)
	{
		___m_InvokeArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_InvokeArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENT_1_T4096360327_H
#ifndef UNITYEVENT_1_T3558036978_H
#define UNITYEVENT_1_T3558036978_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEvent`1<UnityEngine.Color>
struct  UnityEvent_1_t3558036978  : public UnityEventBase_t2008500469
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`1::m_InvokeArray
	ObjectU5BU5D_t3270211303* ___m_InvokeArray_4;

public:
	inline static int32_t get_offset_of_m_InvokeArray_4() { return static_cast<int32_t>(offsetof(UnityEvent_1_t3558036978, ___m_InvokeArray_4)); }
	inline ObjectU5BU5D_t3270211303* get_m_InvokeArray_4() const { return ___m_InvokeArray_4; }
	inline ObjectU5BU5D_t3270211303** get_address_of_m_InvokeArray_4() { return &___m_InvokeArray_4; }
	inline void set_m_InvokeArray_4(ObjectU5BU5D_t3270211303* value)
	{
		___m_InvokeArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_InvokeArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENT_1_T3558036978_H
#ifndef UNITYEVENT_1_T333376534_H
#define UNITYEVENT_1_T333376534_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEvent`1<UnityEngine.Vector2>
struct  UnityEvent_1_t333376534  : public UnityEventBase_t2008500469
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`1::m_InvokeArray
	ObjectU5BU5D_t3270211303* ___m_InvokeArray_4;

public:
	inline static int32_t get_offset_of_m_InvokeArray_4() { return static_cast<int32_t>(offsetof(UnityEvent_1_t333376534, ___m_InvokeArray_4)); }
	inline ObjectU5BU5D_t3270211303* get_m_InvokeArray_4() const { return ___m_InvokeArray_4; }
	inline ObjectU5BU5D_t3270211303** get_address_of_m_InvokeArray_4() { return &___m_InvokeArray_4; }
	inline void set_m_InvokeArray_4(ObjectU5BU5D_t3270211303* value)
	{
		___m_InvokeArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_InvokeArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENT_1_T333376534_H
#ifndef UNITYEVENT_2_T2615162100_H
#define UNITYEVENT_2_T2615162100_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEvent`2<System.Object,System.Object>
struct  UnityEvent_2_t2615162100  : public UnityEventBase_t2008500469
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`2::m_InvokeArray
	ObjectU5BU5D_t3270211303* ___m_InvokeArray_4;

public:
	inline static int32_t get_offset_of_m_InvokeArray_4() { return static_cast<int32_t>(offsetof(UnityEvent_2_t2615162100, ___m_InvokeArray_4)); }
	inline ObjectU5BU5D_t3270211303* get_m_InvokeArray_4() const { return ___m_InvokeArray_4; }
	inline ObjectU5BU5D_t3270211303** get_address_of_m_InvokeArray_4() { return &___m_InvokeArray_4; }
	inline void set_m_InvokeArray_4(ObjectU5BU5D_t3270211303* value)
	{
		___m_InvokeArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_InvokeArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENT_2_T2615162100_H
#ifndef UNITYEVENT_3_T2603401452_H
#define UNITYEVENT_3_T2603401452_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEvent`3<System.Object,System.Object,System.Object>
struct  UnityEvent_3_t2603401452  : public UnityEventBase_t2008500469
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`3::m_InvokeArray
	ObjectU5BU5D_t3270211303* ___m_InvokeArray_4;

public:
	inline static int32_t get_offset_of_m_InvokeArray_4() { return static_cast<int32_t>(offsetof(UnityEvent_3_t2603401452, ___m_InvokeArray_4)); }
	inline ObjectU5BU5D_t3270211303* get_m_InvokeArray_4() const { return ___m_InvokeArray_4; }
	inline ObjectU5BU5D_t3270211303** get_address_of_m_InvokeArray_4() { return &___m_InvokeArray_4; }
	inline void set_m_InvokeArray_4(ObjectU5BU5D_t3270211303* value)
	{
		___m_InvokeArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_InvokeArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENT_3_T2603401452_H
#ifndef UNITYEVENT_4_T4221637861_H
#define UNITYEVENT_4_T4221637861_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEvent`4<System.Object,System.Object,System.Object,System.Object>
struct  UnityEvent_4_t4221637861  : public UnityEventBase_t2008500469
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`4::m_InvokeArray
	ObjectU5BU5D_t3270211303* ___m_InvokeArray_4;

public:
	inline static int32_t get_offset_of_m_InvokeArray_4() { return static_cast<int32_t>(offsetof(UnityEvent_4_t4221637861, ___m_InvokeArray_4)); }
	inline ObjectU5BU5D_t3270211303* get_m_InvokeArray_4() const { return ___m_InvokeArray_4; }
	inline ObjectU5BU5D_t3270211303** get_address_of_m_InvokeArray_4() { return &___m_InvokeArray_4; }
	inline void set_m_InvokeArray_4(ObjectU5BU5D_t3270211303* value)
	{
		___m_InvokeArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_InvokeArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENT_4_T4221637861_H
#ifndef BASEEVENTDATA_T1865211501_H
#define BASEEVENTDATA_T1865211501_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.BaseEventData
struct  BaseEventData_t1865211501  : public AbstractEventData_t3507316000
{
public:
	// UnityEngine.EventSystems.EventSystem UnityEngine.EventSystems.BaseEventData::m_EventSystem
	EventSystem_t2204029313 * ___m_EventSystem_1;

public:
	inline static int32_t get_offset_of_m_EventSystem_1() { return static_cast<int32_t>(offsetof(BaseEventData_t1865211501, ___m_EventSystem_1)); }
	inline EventSystem_t2204029313 * get_m_EventSystem_1() const { return ___m_EventSystem_1; }
	inline EventSystem_t2204029313 ** get_address_of_m_EventSystem_1() { return &___m_EventSystem_1; }
	inline void set_m_EventSystem_1(EventSystem_t2204029313 * value)
	{
		___m_EventSystem_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_EventSystem_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASEEVENTDATA_T1865211501_H
#ifndef CUSTOMATTRIBUTETYPEDARGUMENT_T1650937354_H
#define CUSTOMATTRIBUTETYPEDARGUMENT_T1650937354_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.CustomAttributeTypedArgument
struct  CustomAttributeTypedArgument_t1650937354 
{
public:
	// System.Type System.Reflection.CustomAttributeTypedArgument::argumentType
	Type_t * ___argumentType_0;
	// System.Object System.Reflection.CustomAttributeTypedArgument::value
	RuntimeObject * ___value_1;

public:
	inline static int32_t get_offset_of_argumentType_0() { return static_cast<int32_t>(offsetof(CustomAttributeTypedArgument_t1650937354, ___argumentType_0)); }
	inline Type_t * get_argumentType_0() const { return ___argumentType_0; }
	inline Type_t ** get_address_of_argumentType_0() { return &___argumentType_0; }
	inline void set_argumentType_0(Type_t * value)
	{
		___argumentType_0 = value;
		Il2CppCodeGenWriteBarrier((&___argumentType_0), value);
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(CustomAttributeTypedArgument_t1650937354, ___value_1)); }
	inline RuntimeObject * get_value_1() const { return ___value_1; }
	inline RuntimeObject ** get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(RuntimeObject * value)
	{
		___value_1 = value;
		Il2CppCodeGenWriteBarrier((&___value_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Reflection.CustomAttributeTypedArgument
struct CustomAttributeTypedArgument_t1650937354_marshaled_pinvoke
{
	Type_t * ___argumentType_0;
	Il2CppIUnknown* ___value_1;
};
// Native definition for COM marshalling of System.Reflection.CustomAttributeTypedArgument
struct CustomAttributeTypedArgument_t1650937354_marshaled_com
{
	Type_t * ___argumentType_0;
	Il2CppIUnknown* ___value_1;
};
#endif // CUSTOMATTRIBUTETYPEDARGUMENT_T1650937354_H
#ifndef ANIMATORCLIPINFO_T3315865394_H
#define ANIMATORCLIPINFO_T3315865394_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.AnimatorClipInfo
struct  AnimatorClipInfo_t3315865394 
{
public:
	// System.Int32 UnityEngine.AnimatorClipInfo::m_ClipInstanceID
	int32_t ___m_ClipInstanceID_0;
	// System.Single UnityEngine.AnimatorClipInfo::m_Weight
	float ___m_Weight_1;

public:
	inline static int32_t get_offset_of_m_ClipInstanceID_0() { return static_cast<int32_t>(offsetof(AnimatorClipInfo_t3315865394, ___m_ClipInstanceID_0)); }
	inline int32_t get_m_ClipInstanceID_0() const { return ___m_ClipInstanceID_0; }
	inline int32_t* get_address_of_m_ClipInstanceID_0() { return &___m_ClipInstanceID_0; }
	inline void set_m_ClipInstanceID_0(int32_t value)
	{
		___m_ClipInstanceID_0 = value;
	}

	inline static int32_t get_offset_of_m_Weight_1() { return static_cast<int32_t>(offsetof(AnimatorClipInfo_t3315865394, ___m_Weight_1)); }
	inline float get_m_Weight_1() const { return ___m_Weight_1; }
	inline float* get_address_of_m_Weight_1() { return &___m_Weight_1; }
	inline void set_m_Weight_1(float value)
	{
		___m_Weight_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANIMATORCLIPINFO_T3315865394_H
#ifndef UINT32_T1209397712_H
#define UINT32_T1209397712_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.UInt32
struct  UInt32_t1209397712 
{
public:
	// System.UInt32 System.UInt32::m_value
	uint32_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(UInt32_t1209397712, ___m_value_0)); }
	inline uint32_t get_m_value_0() const { return ___m_value_0; }
	inline uint32_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(uint32_t value)
	{
		___m_value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UINT32_T1209397712_H
#ifndef FLOATTWEEN_T3485850636_H
#define FLOATTWEEN_T3485850636_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.CoroutineTween.FloatTween
struct  FloatTween_t3485850636 
{
public:
	// UnityEngine.UI.CoroutineTween.FloatTween/FloatTweenCallback UnityEngine.UI.CoroutineTween.FloatTween::m_Target
	FloatTweenCallback_t1836342663 * ___m_Target_0;
	// System.Single UnityEngine.UI.CoroutineTween.FloatTween::m_StartValue
	float ___m_StartValue_1;
	// System.Single UnityEngine.UI.CoroutineTween.FloatTween::m_TargetValue
	float ___m_TargetValue_2;
	// System.Single UnityEngine.UI.CoroutineTween.FloatTween::m_Duration
	float ___m_Duration_3;
	// System.Boolean UnityEngine.UI.CoroutineTween.FloatTween::m_IgnoreTimeScale
	bool ___m_IgnoreTimeScale_4;

public:
	inline static int32_t get_offset_of_m_Target_0() { return static_cast<int32_t>(offsetof(FloatTween_t3485850636, ___m_Target_0)); }
	inline FloatTweenCallback_t1836342663 * get_m_Target_0() const { return ___m_Target_0; }
	inline FloatTweenCallback_t1836342663 ** get_address_of_m_Target_0() { return &___m_Target_0; }
	inline void set_m_Target_0(FloatTweenCallback_t1836342663 * value)
	{
		___m_Target_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Target_0), value);
	}

	inline static int32_t get_offset_of_m_StartValue_1() { return static_cast<int32_t>(offsetof(FloatTween_t3485850636, ___m_StartValue_1)); }
	inline float get_m_StartValue_1() const { return ___m_StartValue_1; }
	inline float* get_address_of_m_StartValue_1() { return &___m_StartValue_1; }
	inline void set_m_StartValue_1(float value)
	{
		___m_StartValue_1 = value;
	}

	inline static int32_t get_offset_of_m_TargetValue_2() { return static_cast<int32_t>(offsetof(FloatTween_t3485850636, ___m_TargetValue_2)); }
	inline float get_m_TargetValue_2() const { return ___m_TargetValue_2; }
	inline float* get_address_of_m_TargetValue_2() { return &___m_TargetValue_2; }
	inline void set_m_TargetValue_2(float value)
	{
		___m_TargetValue_2 = value;
	}

	inline static int32_t get_offset_of_m_Duration_3() { return static_cast<int32_t>(offsetof(FloatTween_t3485850636, ___m_Duration_3)); }
	inline float get_m_Duration_3() const { return ___m_Duration_3; }
	inline float* get_address_of_m_Duration_3() { return &___m_Duration_3; }
	inline void set_m_Duration_3(float value)
	{
		___m_Duration_3 = value;
	}

	inline static int32_t get_offset_of_m_IgnoreTimeScale_4() { return static_cast<int32_t>(offsetof(FloatTween_t3485850636, ___m_IgnoreTimeScale_4)); }
	inline bool get_m_IgnoreTimeScale_4() const { return ___m_IgnoreTimeScale_4; }
	inline bool* get_address_of_m_IgnoreTimeScale_4() { return &___m_IgnoreTimeScale_4; }
	inline void set_m_IgnoreTimeScale_4(bool value)
	{
		___m_IgnoreTimeScale_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.UI.CoroutineTween.FloatTween
struct FloatTween_t3485850636_marshaled_pinvoke
{
	FloatTweenCallback_t1836342663 * ___m_Target_0;
	float ___m_StartValue_1;
	float ___m_TargetValue_2;
	float ___m_Duration_3;
	int32_t ___m_IgnoreTimeScale_4;
};
// Native definition for COM marshalling of UnityEngine.UI.CoroutineTween.FloatTween
struct FloatTween_t3485850636_marshaled_com
{
	FloatTweenCallback_t1836342663 * ___m_Target_0;
	float ___m_StartValue_1;
	float ___m_TargetValue_2;
	float ___m_Duration_3;
	int32_t ___m_IgnoreTimeScale_4;
};
#endif // FLOATTWEEN_T3485850636_H
#ifndef COLOR32_T285432355_H
#define COLOR32_T285432355_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color32
struct ALIGN_TYPE(4) Color32_t285432355 
{
public:
	// System.Byte UnityEngine.Color32::r
	uint8_t ___r_0;
	// System.Byte UnityEngine.Color32::g
	uint8_t ___g_1;
	// System.Byte UnityEngine.Color32::b
	uint8_t ___b_2;
	// System.Byte UnityEngine.Color32::a
	uint8_t ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color32_t285432355, ___r_0)); }
	inline uint8_t get_r_0() const { return ___r_0; }
	inline uint8_t* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(uint8_t value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color32_t285432355, ___g_1)); }
	inline uint8_t get_g_1() const { return ___g_1; }
	inline uint8_t* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(uint8_t value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color32_t285432355, ___b_2)); }
	inline uint8_t get_b_2() const { return ___b_2; }
	inline uint8_t* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(uint8_t value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color32_t285432355, ___a_3)); }
	inline uint8_t get_a_3() const { return ___a_3; }
	inline uint8_t* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(uint8_t value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR32_T285432355_H
#ifndef VECTOR4_T2814672345_H
#define VECTOR4_T2814672345_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector4
struct  Vector4_t2814672345 
{
public:
	// System.Single UnityEngine.Vector4::x
	float ___x_1;
	// System.Single UnityEngine.Vector4::y
	float ___y_2;
	// System.Single UnityEngine.Vector4::z
	float ___z_3;
	// System.Single UnityEngine.Vector4::w
	float ___w_4;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector4_t2814672345, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector4_t2814672345, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector4_t2814672345, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}

	inline static int32_t get_offset_of_w_4() { return static_cast<int32_t>(offsetof(Vector4_t2814672345, ___w_4)); }
	inline float get_w_4() const { return ___w_4; }
	inline float* get_address_of_w_4() { return &___w_4; }
	inline void set_w_4(float value)
	{
		___w_4 = value;
	}
};

struct Vector4_t2814672345_StaticFields
{
public:
	// UnityEngine.Vector4 UnityEngine.Vector4::zeroVector
	Vector4_t2814672345  ___zeroVector_5;
	// UnityEngine.Vector4 UnityEngine.Vector4::oneVector
	Vector4_t2814672345  ___oneVector_6;
	// UnityEngine.Vector4 UnityEngine.Vector4::positiveInfinityVector
	Vector4_t2814672345  ___positiveInfinityVector_7;
	// UnityEngine.Vector4 UnityEngine.Vector4::negativeInfinityVector
	Vector4_t2814672345  ___negativeInfinityVector_8;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector4_t2814672345_StaticFields, ___zeroVector_5)); }
	inline Vector4_t2814672345  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector4_t2814672345 * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector4_t2814672345  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector4_t2814672345_StaticFields, ___oneVector_6)); }
	inline Vector4_t2814672345  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector4_t2814672345 * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector4_t2814672345  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_7() { return static_cast<int32_t>(offsetof(Vector4_t2814672345_StaticFields, ___positiveInfinityVector_7)); }
	inline Vector4_t2814672345  get_positiveInfinityVector_7() const { return ___positiveInfinityVector_7; }
	inline Vector4_t2814672345 * get_address_of_positiveInfinityVector_7() { return &___positiveInfinityVector_7; }
	inline void set_positiveInfinityVector_7(Vector4_t2814672345  value)
	{
		___positiveInfinityVector_7 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector4_t2814672345_StaticFields, ___negativeInfinityVector_8)); }
	inline Vector4_t2814672345  get_negativeInfinityVector_8() const { return ___negativeInfinityVector_8; }
	inline Vector4_t2814672345 * get_address_of_negativeInfinityVector_8() { return &___negativeInfinityVector_8; }
	inline void set_negativeInfinityVector_8(Vector4_t2814672345  value)
	{
		___negativeInfinityVector_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR4_T2814672345_H
#ifndef VECTOR3_T2903530434_H
#define VECTOR3_T2903530434_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_t2903530434 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_1;
	// System.Single UnityEngine.Vector3::y
	float ___y_2;
	// System.Single UnityEngine.Vector3::z
	float ___z_3;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector3_t2903530434, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector3_t2903530434, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector3_t2903530434, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}
};

struct Vector3_t2903530434_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t2903530434  ___zeroVector_4;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t2903530434  ___oneVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t2903530434  ___upVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t2903530434  ___downVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t2903530434  ___leftVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t2903530434  ___rightVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t2903530434  ___forwardVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t2903530434  ___backVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t2903530434  ___positiveInfinityVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t2903530434  ___negativeInfinityVector_13;

public:
	inline static int32_t get_offset_of_zeroVector_4() { return static_cast<int32_t>(offsetof(Vector3_t2903530434_StaticFields, ___zeroVector_4)); }
	inline Vector3_t2903530434  get_zeroVector_4() const { return ___zeroVector_4; }
	inline Vector3_t2903530434 * get_address_of_zeroVector_4() { return &___zeroVector_4; }
	inline void set_zeroVector_4(Vector3_t2903530434  value)
	{
		___zeroVector_4 = value;
	}

	inline static int32_t get_offset_of_oneVector_5() { return static_cast<int32_t>(offsetof(Vector3_t2903530434_StaticFields, ___oneVector_5)); }
	inline Vector3_t2903530434  get_oneVector_5() const { return ___oneVector_5; }
	inline Vector3_t2903530434 * get_address_of_oneVector_5() { return &___oneVector_5; }
	inline void set_oneVector_5(Vector3_t2903530434  value)
	{
		___oneVector_5 = value;
	}

	inline static int32_t get_offset_of_upVector_6() { return static_cast<int32_t>(offsetof(Vector3_t2903530434_StaticFields, ___upVector_6)); }
	inline Vector3_t2903530434  get_upVector_6() const { return ___upVector_6; }
	inline Vector3_t2903530434 * get_address_of_upVector_6() { return &___upVector_6; }
	inline void set_upVector_6(Vector3_t2903530434  value)
	{
		___upVector_6 = value;
	}

	inline static int32_t get_offset_of_downVector_7() { return static_cast<int32_t>(offsetof(Vector3_t2903530434_StaticFields, ___downVector_7)); }
	inline Vector3_t2903530434  get_downVector_7() const { return ___downVector_7; }
	inline Vector3_t2903530434 * get_address_of_downVector_7() { return &___downVector_7; }
	inline void set_downVector_7(Vector3_t2903530434  value)
	{
		___downVector_7 = value;
	}

	inline static int32_t get_offset_of_leftVector_8() { return static_cast<int32_t>(offsetof(Vector3_t2903530434_StaticFields, ___leftVector_8)); }
	inline Vector3_t2903530434  get_leftVector_8() const { return ___leftVector_8; }
	inline Vector3_t2903530434 * get_address_of_leftVector_8() { return &___leftVector_8; }
	inline void set_leftVector_8(Vector3_t2903530434  value)
	{
		___leftVector_8 = value;
	}

	inline static int32_t get_offset_of_rightVector_9() { return static_cast<int32_t>(offsetof(Vector3_t2903530434_StaticFields, ___rightVector_9)); }
	inline Vector3_t2903530434  get_rightVector_9() const { return ___rightVector_9; }
	inline Vector3_t2903530434 * get_address_of_rightVector_9() { return &___rightVector_9; }
	inline void set_rightVector_9(Vector3_t2903530434  value)
	{
		___rightVector_9 = value;
	}

	inline static int32_t get_offset_of_forwardVector_10() { return static_cast<int32_t>(offsetof(Vector3_t2903530434_StaticFields, ___forwardVector_10)); }
	inline Vector3_t2903530434  get_forwardVector_10() const { return ___forwardVector_10; }
	inline Vector3_t2903530434 * get_address_of_forwardVector_10() { return &___forwardVector_10; }
	inline void set_forwardVector_10(Vector3_t2903530434  value)
	{
		___forwardVector_10 = value;
	}

	inline static int32_t get_offset_of_backVector_11() { return static_cast<int32_t>(offsetof(Vector3_t2903530434_StaticFields, ___backVector_11)); }
	inline Vector3_t2903530434  get_backVector_11() const { return ___backVector_11; }
	inline Vector3_t2903530434 * get_address_of_backVector_11() { return &___backVector_11; }
	inline void set_backVector_11(Vector3_t2903530434  value)
	{
		___backVector_11 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_12() { return static_cast<int32_t>(offsetof(Vector3_t2903530434_StaticFields, ___positiveInfinityVector_12)); }
	inline Vector3_t2903530434  get_positiveInfinityVector_12() const { return ___positiveInfinityVector_12; }
	inline Vector3_t2903530434 * get_address_of_positiveInfinityVector_12() { return &___positiveInfinityVector_12; }
	inline void set_positiveInfinityVector_12(Vector3_t2903530434  value)
	{
		___positiveInfinityVector_12 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t2903530434_StaticFields, ___negativeInfinityVector_13)); }
	inline Vector3_t2903530434  get_negativeInfinityVector_13() const { return ___negativeInfinityVector_13; }
	inline Vector3_t2903530434 * get_address_of_negativeInfinityVector_13() { return &___negativeInfinityVector_13; }
	inline void set_negativeInfinityVector_13(Vector3_t2903530434  value)
	{
		___negativeInfinityVector_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_T2903530434_H
#ifndef VECTOR2_T3577333262_H
#define VECTOR2_T3577333262_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2
struct  Vector2_t3577333262 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_t3577333262, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_t3577333262, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_t3577333262_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_t3577333262  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_t3577333262  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_t3577333262  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_t3577333262  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_t3577333262  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_t3577333262  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_t3577333262  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_t3577333262  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_t3577333262_StaticFields, ___zeroVector_2)); }
	inline Vector2_t3577333262  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_t3577333262 * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_t3577333262  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_t3577333262_StaticFields, ___oneVector_3)); }
	inline Vector2_t3577333262  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_t3577333262 * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_t3577333262  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_t3577333262_StaticFields, ___upVector_4)); }
	inline Vector2_t3577333262  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_t3577333262 * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_t3577333262  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_t3577333262_StaticFields, ___downVector_5)); }
	inline Vector2_t3577333262  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_t3577333262 * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_t3577333262  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_t3577333262_StaticFields, ___leftVector_6)); }
	inline Vector2_t3577333262  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_t3577333262 * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_t3577333262  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_t3577333262_StaticFields, ___rightVector_7)); }
	inline Vector2_t3577333262  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_t3577333262 * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_t3577333262  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_t3577333262_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_t3577333262  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_t3577333262 * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_t3577333262  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_t3577333262_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_t3577333262  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_t3577333262 * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_t3577333262  value)
	{
		___negativeInfinityVector_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_T3577333262_H
#ifndef COLOR_T2507026410_H
#define COLOR_T2507026410_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color
struct  Color_t2507026410 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t2507026410, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t2507026410, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t2507026410, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t2507026410, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR_T2507026410_H
#ifndef INVOKABLECALL_1_T739676100_H
#define INVOKABLECALL_1_T739676100_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.InvokableCall`1<UnityEngine.Color>
struct  InvokableCall_1_t739676100  : public BaseInvokableCall_t1352930348
{
public:
	// UnityEngine.Events.UnityAction`1<T1> UnityEngine.Events.InvokableCall`1::Delegate
	UnityAction_1_t1759877202 * ___Delegate_0;

public:
	inline static int32_t get_offset_of_Delegate_0() { return static_cast<int32_t>(offsetof(InvokableCall_1_t739676100, ___Delegate_0)); }
	inline UnityAction_1_t1759877202 * get_Delegate_0() const { return ___Delegate_0; }
	inline UnityAction_1_t1759877202 ** get_address_of_Delegate_0() { return &___Delegate_0; }
	inline void set_Delegate_0(UnityAction_1_t1759877202 * value)
	{
		___Delegate_0 = value;
		Il2CppCodeGenWriteBarrier((&___Delegate_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INVOKABLECALL_1_T739676100_H
#ifndef UILINEINFO_T998511395_H
#define UILINEINFO_T998511395_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UILineInfo
struct  UILineInfo_t998511395 
{
public:
	// System.Int32 UnityEngine.UILineInfo::startCharIdx
	int32_t ___startCharIdx_0;
	// System.Int32 UnityEngine.UILineInfo::height
	int32_t ___height_1;
	// System.Single UnityEngine.UILineInfo::topY
	float ___topY_2;
	// System.Single UnityEngine.UILineInfo::leading
	float ___leading_3;

public:
	inline static int32_t get_offset_of_startCharIdx_0() { return static_cast<int32_t>(offsetof(UILineInfo_t998511395, ___startCharIdx_0)); }
	inline int32_t get_startCharIdx_0() const { return ___startCharIdx_0; }
	inline int32_t* get_address_of_startCharIdx_0() { return &___startCharIdx_0; }
	inline void set_startCharIdx_0(int32_t value)
	{
		___startCharIdx_0 = value;
	}

	inline static int32_t get_offset_of_height_1() { return static_cast<int32_t>(offsetof(UILineInfo_t998511395, ___height_1)); }
	inline int32_t get_height_1() const { return ___height_1; }
	inline int32_t* get_address_of_height_1() { return &___height_1; }
	inline void set_height_1(int32_t value)
	{
		___height_1 = value;
	}

	inline static int32_t get_offset_of_topY_2() { return static_cast<int32_t>(offsetof(UILineInfo_t998511395, ___topY_2)); }
	inline float get_topY_2() const { return ___topY_2; }
	inline float* get_address_of_topY_2() { return &___topY_2; }
	inline void set_topY_2(float value)
	{
		___topY_2 = value;
	}

	inline static int32_t get_offset_of_leading_3() { return static_cast<int32_t>(offsetof(UILineInfo_t998511395, ___leading_3)); }
	inline float get_leading_3() const { return ___leading_3; }
	inline float* get_address_of_leading_3() { return &___leading_3; }
	inline void set_leading_3(float value)
	{
		___leading_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UILINEINFO_T998511395_H
#ifndef INVOKABLECALL_1_T254607980_H
#define INVOKABLECALL_1_T254607980_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.InvokableCall`1<System.Object>
struct  InvokableCall_1_t254607980  : public BaseInvokableCall_t1352930348
{
public:
	// UnityEngine.Events.UnityAction`1<T1> UnityEngine.Events.InvokableCall`1::Delegate
	UnityAction_1_t1274809082 * ___Delegate_0;

public:
	inline static int32_t get_offset_of_Delegate_0() { return static_cast<int32_t>(offsetof(InvokableCall_1_t254607980, ___Delegate_0)); }
	inline UnityAction_1_t1274809082 * get_Delegate_0() const { return ___Delegate_0; }
	inline UnityAction_1_t1274809082 ** get_address_of_Delegate_0() { return &___Delegate_0; }
	inline void set_Delegate_0(UnityAction_1_t1274809082 * value)
	{
		___Delegate_0 = value;
		Il2CppCodeGenWriteBarrier((&___Delegate_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INVOKABLECALL_1_T254607980_H
#ifndef VOID_T2217553113_H
#define VOID_T2217553113_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t2217553113 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T2217553113_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	IntPtr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline IntPtr_t get_Zero_1() const { return ___Zero_1; }
	inline IntPtr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(IntPtr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef INT32_T4237944589_H
#define INT32_T4237944589_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Int32
struct  Int32_t4237944589 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Int32_t4237944589, ___m_value_2)); }
	inline int32_t get_m_value_2() const { return ___m_value_2; }
	inline int32_t* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(int32_t value)
	{
		___m_value_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT32_T4237944589_H
#ifndef SINGLE_T3045349759_H
#define SINGLE_T3045349759_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Single
struct  Single_t3045349759 
{
public:
	// System.Single System.Single::m_value
	float ___m_value_7;

public:
	inline static int32_t get_offset_of_m_value_7() { return static_cast<int32_t>(offsetof(Single_t3045349759, ___m_value_7)); }
	inline float get_m_value_7() const { return ___m_value_7; }
	inline float* get_address_of_m_value_7() { return &___m_value_7; }
	inline void set_m_value_7(float value)
	{
		___m_value_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLE_T3045349759_H
#ifndef SYSTEMEXCEPTION_T870616472_H
#define SYSTEMEXCEPTION_T870616472_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.SystemException
struct  SystemException_t870616472  : public Exception_t1975590229
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SYSTEMEXCEPTION_T870616472_H
#ifndef INVOKABLECALL_1_T1183494759_H
#define INVOKABLECALL_1_T1183494759_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.InvokableCall`1<System.Boolean>
struct  InvokableCall_1_t1183494759  : public BaseInvokableCall_t1352930348
{
public:
	// UnityEngine.Events.UnityAction`1<T1> UnityEngine.Events.InvokableCall`1::Delegate
	UnityAction_1_t2203695861 * ___Delegate_0;

public:
	inline static int32_t get_offset_of_Delegate_0() { return static_cast<int32_t>(offsetof(InvokableCall_1_t1183494759, ___Delegate_0)); }
	inline UnityAction_1_t2203695861 * get_Delegate_0() const { return ___Delegate_0; }
	inline UnityAction_1_t2203695861 ** get_address_of_Delegate_0() { return &___Delegate_0; }
	inline void set_Delegate_0(UnityAction_1_t2203695861 * value)
	{
		___Delegate_0 = value;
		Il2CppCodeGenWriteBarrier((&___Delegate_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INVOKABLECALL_1_T1183494759_H
#ifndef TRACKABLEIDPAIR_T1331427386_H
#define TRACKABLEIDPAIR_T1331427386_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.VuforiaManager/TrackableIdPair
struct  TrackableIdPair_t1331427386 
{
public:
	// System.Int32 Vuforia.VuforiaManager/TrackableIdPair::TrackableId
	int32_t ___TrackableId_0;
	// System.Int32 Vuforia.VuforiaManager/TrackableIdPair::ResultId
	int32_t ___ResultId_1;

public:
	inline static int32_t get_offset_of_TrackableId_0() { return static_cast<int32_t>(offsetof(TrackableIdPair_t1331427386, ___TrackableId_0)); }
	inline int32_t get_TrackableId_0() const { return ___TrackableId_0; }
	inline int32_t* get_address_of_TrackableId_0() { return &___TrackableId_0; }
	inline void set_TrackableId_0(int32_t value)
	{
		___TrackableId_0 = value;
	}

	inline static int32_t get_offset_of_ResultId_1() { return static_cast<int32_t>(offsetof(TrackableIdPair_t1331427386, ___ResultId_1)); }
	inline int32_t get_ResultId_1() const { return ___ResultId_1; }
	inline int32_t* get_address_of_ResultId_1() { return &___ResultId_1; }
	inline void set_ResultId_1(int32_t value)
	{
		___ResultId_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRACKABLEIDPAIR_T1331427386_H
#ifndef INVOKABLECALL_1_T2470594279_H
#define INVOKABLECALL_1_T2470594279_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.InvokableCall`1<System.Int32>
struct  InvokableCall_1_t2470594279  : public BaseInvokableCall_t1352930348
{
public:
	// UnityEngine.Events.UnityAction`1<T1> UnityEngine.Events.InvokableCall`1::Delegate
	UnityAction_1_t3490795381 * ___Delegate_0;

public:
	inline static int32_t get_offset_of_Delegate_0() { return static_cast<int32_t>(offsetof(InvokableCall_1_t2470594279, ___Delegate_0)); }
	inline UnityAction_1_t3490795381 * get_Delegate_0() const { return ___Delegate_0; }
	inline UnityAction_1_t3490795381 ** get_address_of_Delegate_0() { return &___Delegate_0; }
	inline void set_Delegate_0(UnityAction_1_t3490795381 * value)
	{
		___Delegate_0 = value;
		Il2CppCodeGenWriteBarrier((&___Delegate_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INVOKABLECALL_1_T2470594279_H
#ifndef METHODBASE_T3686601602_H
#define METHODBASE_T3686601602_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.MethodBase
struct  MethodBase_t3686601602  : public MemberInfo_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // METHODBASE_T3686601602_H
#ifndef INVOKABLECALL_1_T1277999449_H
#define INVOKABLECALL_1_T1277999449_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.InvokableCall`1<System.Single>
struct  InvokableCall_1_t1277999449  : public BaseInvokableCall_t1352930348
{
public:
	// UnityEngine.Events.UnityAction`1<T1> UnityEngine.Events.InvokableCall`1::Delegate
	UnityAction_1_t2298200551 * ___Delegate_0;

public:
	inline static int32_t get_offset_of_Delegate_0() { return static_cast<int32_t>(offsetof(InvokableCall_1_t1277999449, ___Delegate_0)); }
	inline UnityAction_1_t2298200551 * get_Delegate_0() const { return ___Delegate_0; }
	inline UnityAction_1_t2298200551 ** get_address_of_Delegate_0() { return &___Delegate_0; }
	inline void set_Delegate_0(UnityAction_1_t2298200551 * value)
	{
		___Delegate_0 = value;
		Il2CppCodeGenWriteBarrier((&___Delegate_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INVOKABLECALL_1_T1277999449_H
#ifndef CACHEDINVOKABLECALL_1_T2522919606_H
#define CACHEDINVOKABLECALL_1_T2522919606_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.CachedInvokableCall`1<System.Int32>
struct  CachedInvokableCall_1_t2522919606  : public InvokableCall_1_t2470594279
{
public:
	// System.Object[] UnityEngine.Events.CachedInvokableCall`1::m_Arg1
	ObjectU5BU5D_t3270211303* ___m_Arg1_1;

public:
	inline static int32_t get_offset_of_m_Arg1_1() { return static_cast<int32_t>(offsetof(CachedInvokableCall_1_t2522919606, ___m_Arg1_1)); }
	inline ObjectU5BU5D_t3270211303* get_m_Arg1_1() const { return ___m_Arg1_1; }
	inline ObjectU5BU5D_t3270211303** get_address_of_m_Arg1_1() { return &___m_Arg1_1; }
	inline void set_m_Arg1_1(ObjectU5BU5D_t3270211303* value)
	{
		___m_Arg1_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_Arg1_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CACHEDINVOKABLECALL_1_T2522919606_H
#ifndef METHODINFO_T_H
#define METHODINFO_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.MethodInfo
struct  MethodInfo_t  : public MethodBase_t3686601602
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // METHODINFO_T_H
#ifndef PIXEL_FORMAT_T1585627136_H
#define PIXEL_FORMAT_T1585627136_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.Image/PIXEL_FORMAT
struct  PIXEL_FORMAT_t1585627136 
{
public:
	// System.Int32 Vuforia.Image/PIXEL_FORMAT::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(PIXEL_FORMAT_t1585627136, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PIXEL_FORMAT_T1585627136_H
#ifndef CACHEDINVOKABLECALL_1_T1235820086_H
#define CACHEDINVOKABLECALL_1_T1235820086_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.CachedInvokableCall`1<System.Boolean>
struct  CachedInvokableCall_1_t1235820086  : public InvokableCall_1_t1183494759
{
public:
	// System.Object[] UnityEngine.Events.CachedInvokableCall`1::m_Arg1
	ObjectU5BU5D_t3270211303* ___m_Arg1_1;

public:
	inline static int32_t get_offset_of_m_Arg1_1() { return static_cast<int32_t>(offsetof(CachedInvokableCall_1_t1235820086, ___m_Arg1_1)); }
	inline ObjectU5BU5D_t3270211303* get_m_Arg1_1() const { return ___m_Arg1_1; }
	inline ObjectU5BU5D_t3270211303** get_address_of_m_Arg1_1() { return &___m_Arg1_1; }
	inline void set_m_Arg1_1(ObjectU5BU5D_t3270211303* value)
	{
		___m_Arg1_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_Arg1_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CACHEDINVOKABLECALL_1_T1235820086_H
#ifndef NOTIMPLEMENTEDEXCEPTION_T2054929637_H
#define NOTIMPLEMENTEDEXCEPTION_T2054929637_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.NotImplementedException
struct  NotImplementedException_t2054929637  : public SystemException_t870616472
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NOTIMPLEMENTEDEXCEPTION_T2054929637_H
#ifndef NOTSUPPORTEDEXCEPTION_T1045231613_H
#define NOTSUPPORTEDEXCEPTION_T1045231613_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.NotSupportedException
struct  NotSupportedException_t1045231613  : public SystemException_t870616472
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NOTSUPPORTEDEXCEPTION_T1045231613_H
#ifndef COROUTINE_T2992159144_H
#define COROUTINE_T2992159144_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Coroutine
struct  Coroutine_t2992159144  : public YieldInstruction_t1893592390
{
public:
	// System.IntPtr UnityEngine.Coroutine::m_Ptr
	IntPtr_t ___m_Ptr_0;

public:
	inline static int32_t get_offset_of_m_Ptr_0() { return static_cast<int32_t>(offsetof(Coroutine_t2992159144, ___m_Ptr_0)); }
	inline IntPtr_t get_m_Ptr_0() const { return ___m_Ptr_0; }
	inline IntPtr_t* get_address_of_m_Ptr_0() { return &___m_Ptr_0; }
	inline void set_m_Ptr_0(IntPtr_t value)
	{
		___m_Ptr_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Coroutine
struct Coroutine_t2992159144_marshaled_pinvoke : public YieldInstruction_t1893592390_marshaled_pinvoke
{
	intptr_t ___m_Ptr_0;
};
// Native definition for COM marshalling of UnityEngine.Coroutine
struct Coroutine_t2992159144_marshaled_com : public YieldInstruction_t1893592390_marshaled_com
{
	intptr_t ___m_Ptr_0;
};
#endif // COROUTINE_T2992159144_H
#ifndef CASTHELPER_1_T2384536030_H
#define CASTHELPER_1_T2384536030_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.CastHelper`1<System.Object>
struct  CastHelper_1_t2384536030 
{
public:
	// T UnityEngine.CastHelper`1::t
	RuntimeObject * ___t_0;
	// System.IntPtr UnityEngine.CastHelper`1::onePointerFurtherThanT
	IntPtr_t ___onePointerFurtherThanT_1;

public:
	inline static int32_t get_offset_of_t_0() { return static_cast<int32_t>(offsetof(CastHelper_1_t2384536030, ___t_0)); }
	inline RuntimeObject * get_t_0() const { return ___t_0; }
	inline RuntimeObject ** get_address_of_t_0() { return &___t_0; }
	inline void set_t_0(RuntimeObject * value)
	{
		___t_0 = value;
		Il2CppCodeGenWriteBarrier((&___t_0), value);
	}

	inline static int32_t get_offset_of_onePointerFurtherThanT_1() { return static_cast<int32_t>(offsetof(CastHelper_1_t2384536030, ___onePointerFurtherThanT_1)); }
	inline IntPtr_t get_onePointerFurtherThanT_1() const { return ___onePointerFurtherThanT_1; }
	inline IntPtr_t* get_address_of_onePointerFurtherThanT_1() { return &___onePointerFurtherThanT_1; }
	inline void set_onePointerFurtherThanT_1(IntPtr_t value)
	{
		___onePointerFurtherThanT_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CASTHELPER_1_T2384536030_H
#ifndef OBJECT_T1502412432_H
#define OBJECT_T1502412432_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t1502412432  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	IntPtr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t1502412432, ___m_CachedPtr_0)); }
	inline IntPtr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline IntPtr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(IntPtr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t1502412432_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t1502412432_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t1502412432_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t1502412432_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T1502412432_H
#ifndef CACHEDINVOKABLECALL_1_T306933307_H
#define CACHEDINVOKABLECALL_1_T306933307_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.CachedInvokableCall`1<System.Object>
struct  CachedInvokableCall_1_t306933307  : public InvokableCall_1_t254607980
{
public:
	// System.Object[] UnityEngine.Events.CachedInvokableCall`1::m_Arg1
	ObjectU5BU5D_t3270211303* ___m_Arg1_1;

public:
	inline static int32_t get_offset_of_m_Arg1_1() { return static_cast<int32_t>(offsetof(CachedInvokableCall_1_t306933307, ___m_Arg1_1)); }
	inline ObjectU5BU5D_t3270211303* get_m_Arg1_1() const { return ___m_Arg1_1; }
	inline ObjectU5BU5D_t3270211303** get_address_of_m_Arg1_1() { return &___m_Arg1_1; }
	inline void set_m_Arg1_1(ObjectU5BU5D_t3270211303* value)
	{
		___m_Arg1_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_Arg1_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CACHEDINVOKABLECALL_1_T306933307_H
#ifndef INSTANCEIDDATA_T2286842551_H
#define INSTANCEIDDATA_T2286842551_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.VuforiaManagerImpl/InstanceIdData
#pragma pack(push, tp, 1)
struct  InstanceIdData_t2286842551 
{
public:
	// System.UInt64 Vuforia.VuforiaManagerImpl/InstanceIdData::numericValue
	uint64_t ___numericValue_0;
	// System.IntPtr Vuforia.VuforiaManagerImpl/InstanceIdData::buffer
	IntPtr_t ___buffer_1;
	// System.IntPtr Vuforia.VuforiaManagerImpl/InstanceIdData::reserved
	IntPtr_t ___reserved_2;
	// System.UInt32 Vuforia.VuforiaManagerImpl/InstanceIdData::dataLength
	uint32_t ___dataLength_3;
	// System.Int32 Vuforia.VuforiaManagerImpl/InstanceIdData::dataType
	int32_t ___dataType_4;

public:
	inline static int32_t get_offset_of_numericValue_0() { return static_cast<int32_t>(offsetof(InstanceIdData_t2286842551, ___numericValue_0)); }
	inline uint64_t get_numericValue_0() const { return ___numericValue_0; }
	inline uint64_t* get_address_of_numericValue_0() { return &___numericValue_0; }
	inline void set_numericValue_0(uint64_t value)
	{
		___numericValue_0 = value;
	}

	inline static int32_t get_offset_of_buffer_1() { return static_cast<int32_t>(offsetof(InstanceIdData_t2286842551, ___buffer_1)); }
	inline IntPtr_t get_buffer_1() const { return ___buffer_1; }
	inline IntPtr_t* get_address_of_buffer_1() { return &___buffer_1; }
	inline void set_buffer_1(IntPtr_t value)
	{
		___buffer_1 = value;
	}

	inline static int32_t get_offset_of_reserved_2() { return static_cast<int32_t>(offsetof(InstanceIdData_t2286842551, ___reserved_2)); }
	inline IntPtr_t get_reserved_2() const { return ___reserved_2; }
	inline IntPtr_t* get_address_of_reserved_2() { return &___reserved_2; }
	inline void set_reserved_2(IntPtr_t value)
	{
		___reserved_2 = value;
	}

	inline static int32_t get_offset_of_dataLength_3() { return static_cast<int32_t>(offsetof(InstanceIdData_t2286842551, ___dataLength_3)); }
	inline uint32_t get_dataLength_3() const { return ___dataLength_3; }
	inline uint32_t* get_address_of_dataLength_3() { return &___dataLength_3; }
	inline void set_dataLength_3(uint32_t value)
	{
		___dataLength_3 = value;
	}

	inline static int32_t get_offset_of_dataType_4() { return static_cast<int32_t>(offsetof(InstanceIdData_t2286842551, ___dataType_4)); }
	inline int32_t get_dataType_4() const { return ___dataType_4; }
	inline int32_t* get_address_of_dataType_4() { return &___dataType_4; }
	inline void set_dataType_4(int32_t value)
	{
		___dataType_4 = value;
	}
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INSTANCEIDDATA_T2286842551_H
#ifndef CACHEDINVOKABLECALL_1_T1330324776_H
#define CACHEDINVOKABLECALL_1_T1330324776_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.CachedInvokableCall`1<System.Single>
struct  CachedInvokableCall_1_t1330324776  : public InvokableCall_1_t1277999449
{
public:
	// System.Object[] UnityEngine.Events.CachedInvokableCall`1::m_Arg1
	ObjectU5BU5D_t3270211303* ___m_Arg1_1;

public:
	inline static int32_t get_offset_of_m_Arg1_1() { return static_cast<int32_t>(offsetof(CachedInvokableCall_1_t1330324776, ___m_Arg1_1)); }
	inline ObjectU5BU5D_t3270211303* get_m_Arg1_1() const { return ___m_Arg1_1; }
	inline ObjectU5BU5D_t3270211303** get_address_of_m_Arg1_1() { return &___m_Arg1_1; }
	inline void set_m_Arg1_1(ObjectU5BU5D_t3270211303* value)
	{
		___m_Arg1_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_Arg1_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CACHEDINVOKABLECALL_1_T1330324776_H
#ifndef POSEDATA_T1816720573_H
#define POSEDATA_T1816720573_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.VuforiaManagerImpl/PoseData
#pragma pack(push, tp, 1)
struct  PoseData_t1816720573 
{
public:
	// UnityEngine.Vector3 Vuforia.VuforiaManagerImpl/PoseData::position
	Vector3_t2903530434  ___position_0;
	// UnityEngine.Quaternion Vuforia.VuforiaManagerImpl/PoseData::orientation
	Quaternion_t754065749  ___orientation_1;
	// System.Int32 Vuforia.VuforiaManagerImpl/PoseData::csInteger
	int32_t ___csInteger_2;

public:
	inline static int32_t get_offset_of_position_0() { return static_cast<int32_t>(offsetof(PoseData_t1816720573, ___position_0)); }
	inline Vector3_t2903530434  get_position_0() const { return ___position_0; }
	inline Vector3_t2903530434 * get_address_of_position_0() { return &___position_0; }
	inline void set_position_0(Vector3_t2903530434  value)
	{
		___position_0 = value;
	}

	inline static int32_t get_offset_of_orientation_1() { return static_cast<int32_t>(offsetof(PoseData_t1816720573, ___orientation_1)); }
	inline Quaternion_t754065749  get_orientation_1() const { return ___orientation_1; }
	inline Quaternion_t754065749 * get_address_of_orientation_1() { return &___orientation_1; }
	inline void set_orientation_1(Quaternion_t754065749  value)
	{
		___orientation_1 = value;
	}

	inline static int32_t get_offset_of_csInteger_2() { return static_cast<int32_t>(offsetof(PoseData_t1816720573, ___csInteger_2)); }
	inline int32_t get_csInteger_2() const { return ___csInteger_2; }
	inline int32_t* get_address_of_csInteger_2() { return &___csInteger_2; }
	inline void set_csInteger_2(int32_t value)
	{
		___csInteger_2 = value;
	}
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSEDATA_T1816720573_H
#ifndef ARGUMENTEXCEPTION_T4028401650_H
#define ARGUMENTEXCEPTION_T4028401650_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ArgumentException
struct  ArgumentException_t4028401650  : public SystemException_t870616472
{
public:
	// System.String System.ArgumentException::param_name
	String_t* ___param_name_12;

public:
	inline static int32_t get_offset_of_param_name_12() { return static_cast<int32_t>(offsetof(ArgumentException_t4028401650, ___param_name_12)); }
	inline String_t* get_param_name_12() const { return ___param_name_12; }
	inline String_t** get_address_of_param_name_12() { return &___param_name_12; }
	inline void set_param_name_12(String_t* value)
	{
		___param_name_12 = value;
		Il2CppCodeGenWriteBarrier((&___param_name_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARGUMENTEXCEPTION_T4028401650_H
#ifndef BINDINGFLAGS_T1805189573_H
#define BINDINGFLAGS_T1805189573_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.BindingFlags
struct  BindingFlags_t1805189573 
{
public:
	// System.Int32 System.Reflection.BindingFlags::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(BindingFlags_t1805189573, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BINDINGFLAGS_T1805189573_H
#ifndef DELEGATE_T537306192_H
#define DELEGATE_T537306192_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t537306192  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	IntPtr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	IntPtr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	IntPtr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::method_code
	IntPtr_t ___method_code_5;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_6;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_7;
	// System.DelegateData System.Delegate::data
	DelegateData_t3370401926 * ___data_8;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t537306192, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t537306192, ___invoke_impl_1)); }
	inline IntPtr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline IntPtr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(IntPtr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t537306192, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t537306192, ___method_3)); }
	inline IntPtr_t get_method_3() const { return ___method_3; }
	inline IntPtr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(IntPtr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t537306192, ___delegate_trampoline_4)); }
	inline IntPtr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline IntPtr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(IntPtr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_method_code_5() { return static_cast<int32_t>(offsetof(Delegate_t537306192, ___method_code_5)); }
	inline IntPtr_t get_method_code_5() const { return ___method_code_5; }
	inline IntPtr_t* get_address_of_method_code_5() { return &___method_code_5; }
	inline void set_method_code_5(IntPtr_t value)
	{
		___method_code_5 = value;
	}

	inline static int32_t get_offset_of_method_info_6() { return static_cast<int32_t>(offsetof(Delegate_t537306192, ___method_info_6)); }
	inline MethodInfo_t * get_method_info_6() const { return ___method_info_6; }
	inline MethodInfo_t ** get_address_of_method_info_6() { return &___method_info_6; }
	inline void set_method_info_6(MethodInfo_t * value)
	{
		___method_info_6 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_6), value);
	}

	inline static int32_t get_offset_of_original_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t537306192, ___original_method_info_7)); }
	inline MethodInfo_t * get_original_method_info_7() const { return ___original_method_info_7; }
	inline MethodInfo_t ** get_address_of_original_method_info_7() { return &___original_method_info_7; }
	inline void set_original_method_info_7(MethodInfo_t * value)
	{
		___original_method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_7), value);
	}

	inline static int32_t get_offset_of_data_8() { return static_cast<int32_t>(offsetof(Delegate_t537306192, ___data_8)); }
	inline DelegateData_t3370401926 * get_data_8() const { return ___data_8; }
	inline DelegateData_t3370401926 ** get_address_of_data_8() { return &___data_8; }
	inline void set_data_8(DelegateData_t3370401926 * value)
	{
		___data_8 = value;
		Il2CppCodeGenWriteBarrier((&___data_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DELEGATE_T537306192_H
#ifndef DATATYPE_T3838319351_H
#define DATATYPE_T3838319351_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.CameraDevice/CameraField/DataType
struct  DataType_t3838319351 
{
public:
	// System.Int32 Vuforia.CameraDevice/CameraField/DataType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(DataType_t3838319351, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATATYPE_T3838319351_H
#ifndef U3CSTARTU3EC__ITERATOR0_T2935223578_H
#define U3CSTARTU3EC__ITERATOR0_T2935223578_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.CoroutineTween.TweenRunner`1/<Start>c__Iterator0<UnityEngine.UI.CoroutineTween.FloatTween>
struct  U3CStartU3Ec__Iterator0_t2935223578  : public RuntimeObject
{
public:
	// T UnityEngine.UI.CoroutineTween.TweenRunner`1/<Start>c__Iterator0::tweenInfo
	FloatTween_t3485850636  ___tweenInfo_0;
	// System.Single UnityEngine.UI.CoroutineTween.TweenRunner`1/<Start>c__Iterator0::<elapsedTime>__0
	float ___U3CelapsedTimeU3E__0_1;
	// System.Single UnityEngine.UI.CoroutineTween.TweenRunner`1/<Start>c__Iterator0::<percentage>__1
	float ___U3CpercentageU3E__1_2;
	// System.Object UnityEngine.UI.CoroutineTween.TweenRunner`1/<Start>c__Iterator0::$current
	RuntimeObject * ___U24current_3;
	// System.Boolean UnityEngine.UI.CoroutineTween.TweenRunner`1/<Start>c__Iterator0::$disposing
	bool ___U24disposing_4;
	// System.Int32 UnityEngine.UI.CoroutineTween.TweenRunner`1/<Start>c__Iterator0::$PC
	int32_t ___U24PC_5;

public:
	inline static int32_t get_offset_of_tweenInfo_0() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t2935223578, ___tweenInfo_0)); }
	inline FloatTween_t3485850636  get_tweenInfo_0() const { return ___tweenInfo_0; }
	inline FloatTween_t3485850636 * get_address_of_tweenInfo_0() { return &___tweenInfo_0; }
	inline void set_tweenInfo_0(FloatTween_t3485850636  value)
	{
		___tweenInfo_0 = value;
	}

	inline static int32_t get_offset_of_U3CelapsedTimeU3E__0_1() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t2935223578, ___U3CelapsedTimeU3E__0_1)); }
	inline float get_U3CelapsedTimeU3E__0_1() const { return ___U3CelapsedTimeU3E__0_1; }
	inline float* get_address_of_U3CelapsedTimeU3E__0_1() { return &___U3CelapsedTimeU3E__0_1; }
	inline void set_U3CelapsedTimeU3E__0_1(float value)
	{
		___U3CelapsedTimeU3E__0_1 = value;
	}

	inline static int32_t get_offset_of_U3CpercentageU3E__1_2() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t2935223578, ___U3CpercentageU3E__1_2)); }
	inline float get_U3CpercentageU3E__1_2() const { return ___U3CpercentageU3E__1_2; }
	inline float* get_address_of_U3CpercentageU3E__1_2() { return &___U3CpercentageU3E__1_2; }
	inline void set_U3CpercentageU3E__1_2(float value)
	{
		___U3CpercentageU3E__1_2 = value;
	}

	inline static int32_t get_offset_of_U24current_3() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t2935223578, ___U24current_3)); }
	inline RuntimeObject * get_U24current_3() const { return ___U24current_3; }
	inline RuntimeObject ** get_address_of_U24current_3() { return &___U24current_3; }
	inline void set_U24current_3(RuntimeObject * value)
	{
		___U24current_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_3), value);
	}

	inline static int32_t get_offset_of_U24disposing_4() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t2935223578, ___U24disposing_4)); }
	inline bool get_U24disposing_4() const { return ___U24disposing_4; }
	inline bool* get_address_of_U24disposing_4() { return &___U24disposing_4; }
	inline void set_U24disposing_4(bool value)
	{
		___U24disposing_4 = value;
	}

	inline static int32_t get_offset_of_U24PC_5() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t2935223578, ___U24PC_5)); }
	inline int32_t get_U24PC_5() const { return ___U24PC_5; }
	inline int32_t* get_address_of_U24PC_5() { return &___U24PC_5; }
	inline void set_U24PC_5(int32_t value)
	{
		___U24PC_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSTARTU3EC__ITERATOR0_T2935223578_H
#ifndef UICHARINFO_T2481152217_H
#define UICHARINFO_T2481152217_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UICharInfo
struct  UICharInfo_t2481152217 
{
public:
	// UnityEngine.Vector2 UnityEngine.UICharInfo::cursorPos
	Vector2_t3577333262  ___cursorPos_0;
	// System.Single UnityEngine.UICharInfo::charWidth
	float ___charWidth_1;

public:
	inline static int32_t get_offset_of_cursorPos_0() { return static_cast<int32_t>(offsetof(UICharInfo_t2481152217, ___cursorPos_0)); }
	inline Vector2_t3577333262  get_cursorPos_0() const { return ___cursorPos_0; }
	inline Vector2_t3577333262 * get_address_of_cursorPos_0() { return &___cursorPos_0; }
	inline void set_cursorPos_0(Vector2_t3577333262  value)
	{
		___cursorPos_0 = value;
	}

	inline static int32_t get_offset_of_charWidth_1() { return static_cast<int32_t>(offsetof(UICharInfo_t2481152217, ___charWidth_1)); }
	inline float get_charWidth_1() const { return ___charWidth_1; }
	inline float* get_address_of_charWidth_1() { return &___charWidth_1; }
	inline void set_charWidth_1(float value)
	{
		___charWidth_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UICHARINFO_T2481152217_H
#ifndef CUSTOMATTRIBUTENAMEDARGUMENT_T468938427_H
#define CUSTOMATTRIBUTENAMEDARGUMENT_T468938427_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.CustomAttributeNamedArgument
struct  CustomAttributeNamedArgument_t468938427 
{
public:
	// System.Reflection.CustomAttributeTypedArgument System.Reflection.CustomAttributeNamedArgument::typedArgument
	CustomAttributeTypedArgument_t1650937354  ___typedArgument_0;
	// System.Reflection.MemberInfo System.Reflection.CustomAttributeNamedArgument::memberInfo
	MemberInfo_t * ___memberInfo_1;

public:
	inline static int32_t get_offset_of_typedArgument_0() { return static_cast<int32_t>(offsetof(CustomAttributeNamedArgument_t468938427, ___typedArgument_0)); }
	inline CustomAttributeTypedArgument_t1650937354  get_typedArgument_0() const { return ___typedArgument_0; }
	inline CustomAttributeTypedArgument_t1650937354 * get_address_of_typedArgument_0() { return &___typedArgument_0; }
	inline void set_typedArgument_0(CustomAttributeTypedArgument_t1650937354  value)
	{
		___typedArgument_0 = value;
	}

	inline static int32_t get_offset_of_memberInfo_1() { return static_cast<int32_t>(offsetof(CustomAttributeNamedArgument_t468938427, ___memberInfo_1)); }
	inline MemberInfo_t * get_memberInfo_1() const { return ___memberInfo_1; }
	inline MemberInfo_t ** get_address_of_memberInfo_1() { return &___memberInfo_1; }
	inline void set_memberInfo_1(MemberInfo_t * value)
	{
		___memberInfo_1 = value;
		Il2CppCodeGenWriteBarrier((&___memberInfo_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Reflection.CustomAttributeNamedArgument
struct CustomAttributeNamedArgument_t468938427_marshaled_pinvoke
{
	CustomAttributeTypedArgument_t1650937354_marshaled_pinvoke ___typedArgument_0;
	MemberInfo_t * ___memberInfo_1;
};
// Native definition for COM marshalling of System.Reflection.CustomAttributeNamedArgument
struct CustomAttributeNamedArgument_t468938427_marshaled_com
{
	CustomAttributeTypedArgument_t1650937354_marshaled_com ___typedArgument_0;
	MemberInfo_t * ___memberInfo_1;
};
#endif // CUSTOMATTRIBUTENAMEDARGUMENT_T468938427_H
#ifndef RUNTIMETYPEHANDLE_T845633806_H
#define RUNTIMETYPEHANDLE_T845633806_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.RuntimeTypeHandle
struct  RuntimeTypeHandle_t845633806 
{
public:
	// System.IntPtr System.RuntimeTypeHandle::value
	IntPtr_t ___value_0;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(RuntimeTypeHandle_t845633806, ___value_0)); }
	inline IntPtr_t get_value_0() const { return ___value_0; }
	inline IntPtr_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(IntPtr_t value)
	{
		___value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMETYPEHANDLE_T845633806_H
#ifndef RAYCASTRESULT_T3511189758_H
#define RAYCASTRESULT_T3511189758_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.RaycastResult
struct  RaycastResult_t3511189758 
{
public:
	// UnityEngine.GameObject UnityEngine.EventSystems.RaycastResult::m_GameObject
	GameObject_t1811656094 * ___m_GameObject_0;
	// UnityEngine.EventSystems.BaseRaycaster UnityEngine.EventSystems.RaycastResult::module
	BaseRaycaster_t318802990 * ___module_1;
	// System.Single UnityEngine.EventSystems.RaycastResult::distance
	float ___distance_2;
	// System.Single UnityEngine.EventSystems.RaycastResult::index
	float ___index_3;
	// System.Int32 UnityEngine.EventSystems.RaycastResult::depth
	int32_t ___depth_4;
	// System.Int32 UnityEngine.EventSystems.RaycastResult::sortingLayer
	int32_t ___sortingLayer_5;
	// System.Int32 UnityEngine.EventSystems.RaycastResult::sortingOrder
	int32_t ___sortingOrder_6;
	// UnityEngine.Vector3 UnityEngine.EventSystems.RaycastResult::worldPosition
	Vector3_t2903530434  ___worldPosition_7;
	// UnityEngine.Vector3 UnityEngine.EventSystems.RaycastResult::worldNormal
	Vector3_t2903530434  ___worldNormal_8;
	// UnityEngine.Vector2 UnityEngine.EventSystems.RaycastResult::screenPosition
	Vector2_t3577333262  ___screenPosition_9;

public:
	inline static int32_t get_offset_of_m_GameObject_0() { return static_cast<int32_t>(offsetof(RaycastResult_t3511189758, ___m_GameObject_0)); }
	inline GameObject_t1811656094 * get_m_GameObject_0() const { return ___m_GameObject_0; }
	inline GameObject_t1811656094 ** get_address_of_m_GameObject_0() { return &___m_GameObject_0; }
	inline void set_m_GameObject_0(GameObject_t1811656094 * value)
	{
		___m_GameObject_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_GameObject_0), value);
	}

	inline static int32_t get_offset_of_module_1() { return static_cast<int32_t>(offsetof(RaycastResult_t3511189758, ___module_1)); }
	inline BaseRaycaster_t318802990 * get_module_1() const { return ___module_1; }
	inline BaseRaycaster_t318802990 ** get_address_of_module_1() { return &___module_1; }
	inline void set_module_1(BaseRaycaster_t318802990 * value)
	{
		___module_1 = value;
		Il2CppCodeGenWriteBarrier((&___module_1), value);
	}

	inline static int32_t get_offset_of_distance_2() { return static_cast<int32_t>(offsetof(RaycastResult_t3511189758, ___distance_2)); }
	inline float get_distance_2() const { return ___distance_2; }
	inline float* get_address_of_distance_2() { return &___distance_2; }
	inline void set_distance_2(float value)
	{
		___distance_2 = value;
	}

	inline static int32_t get_offset_of_index_3() { return static_cast<int32_t>(offsetof(RaycastResult_t3511189758, ___index_3)); }
	inline float get_index_3() const { return ___index_3; }
	inline float* get_address_of_index_3() { return &___index_3; }
	inline void set_index_3(float value)
	{
		___index_3 = value;
	}

	inline static int32_t get_offset_of_depth_4() { return static_cast<int32_t>(offsetof(RaycastResult_t3511189758, ___depth_4)); }
	inline int32_t get_depth_4() const { return ___depth_4; }
	inline int32_t* get_address_of_depth_4() { return &___depth_4; }
	inline void set_depth_4(int32_t value)
	{
		___depth_4 = value;
	}

	inline static int32_t get_offset_of_sortingLayer_5() { return static_cast<int32_t>(offsetof(RaycastResult_t3511189758, ___sortingLayer_5)); }
	inline int32_t get_sortingLayer_5() const { return ___sortingLayer_5; }
	inline int32_t* get_address_of_sortingLayer_5() { return &___sortingLayer_5; }
	inline void set_sortingLayer_5(int32_t value)
	{
		___sortingLayer_5 = value;
	}

	inline static int32_t get_offset_of_sortingOrder_6() { return static_cast<int32_t>(offsetof(RaycastResult_t3511189758, ___sortingOrder_6)); }
	inline int32_t get_sortingOrder_6() const { return ___sortingOrder_6; }
	inline int32_t* get_address_of_sortingOrder_6() { return &___sortingOrder_6; }
	inline void set_sortingOrder_6(int32_t value)
	{
		___sortingOrder_6 = value;
	}

	inline static int32_t get_offset_of_worldPosition_7() { return static_cast<int32_t>(offsetof(RaycastResult_t3511189758, ___worldPosition_7)); }
	inline Vector3_t2903530434  get_worldPosition_7() const { return ___worldPosition_7; }
	inline Vector3_t2903530434 * get_address_of_worldPosition_7() { return &___worldPosition_7; }
	inline void set_worldPosition_7(Vector3_t2903530434  value)
	{
		___worldPosition_7 = value;
	}

	inline static int32_t get_offset_of_worldNormal_8() { return static_cast<int32_t>(offsetof(RaycastResult_t3511189758, ___worldNormal_8)); }
	inline Vector3_t2903530434  get_worldNormal_8() const { return ___worldNormal_8; }
	inline Vector3_t2903530434 * get_address_of_worldNormal_8() { return &___worldNormal_8; }
	inline void set_worldNormal_8(Vector3_t2903530434  value)
	{
		___worldNormal_8 = value;
	}

	inline static int32_t get_offset_of_screenPosition_9() { return static_cast<int32_t>(offsetof(RaycastResult_t3511189758, ___screenPosition_9)); }
	inline Vector2_t3577333262  get_screenPosition_9() const { return ___screenPosition_9; }
	inline Vector2_t3577333262 * get_address_of_screenPosition_9() { return &___screenPosition_9; }
	inline void set_screenPosition_9(Vector2_t3577333262  value)
	{
		___screenPosition_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.EventSystems.RaycastResult
struct RaycastResult_t3511189758_marshaled_pinvoke
{
	GameObject_t1811656094 * ___m_GameObject_0;
	BaseRaycaster_t318802990 * ___module_1;
	float ___distance_2;
	float ___index_3;
	int32_t ___depth_4;
	int32_t ___sortingLayer_5;
	int32_t ___sortingOrder_6;
	Vector3_t2903530434  ___worldPosition_7;
	Vector3_t2903530434  ___worldNormal_8;
	Vector2_t3577333262  ___screenPosition_9;
};
// Native definition for COM marshalling of UnityEngine.EventSystems.RaycastResult
struct RaycastResult_t3511189758_marshaled_com
{
	GameObject_t1811656094 * ___m_GameObject_0;
	BaseRaycaster_t318802990 * ___module_1;
	float ___distance_2;
	float ___index_3;
	int32_t ___depth_4;
	int32_t ___sortingLayer_5;
	int32_t ___sortingOrder_6;
	Vector3_t2903530434  ___worldPosition_7;
	Vector3_t2903530434  ___worldNormal_8;
	Vector2_t3577333262  ___screenPosition_9;
};
#endif // RAYCASTRESULT_T3511189758_H
#ifndef LOADSCENEMODE_T2291092563_H
#define LOADSCENEMODE_T2291092563_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.SceneManagement.LoadSceneMode
struct  LoadSceneMode_t2291092563 
{
public:
	// System.Int32 UnityEngine.SceneManagement.LoadSceneMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(LoadSceneMode_t2291092563, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOADSCENEMODE_T2291092563_H
#ifndef TARGETSEARCHRESULT_T213694407_H
#define TARGETSEARCHRESULT_T213694407_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.TargetFinder/TargetSearchResult
struct  TargetSearchResult_t213694407 
{
public:
	// System.String Vuforia.TargetFinder/TargetSearchResult::TargetName
	String_t* ___TargetName_0;
	// System.String Vuforia.TargetFinder/TargetSearchResult::UniqueTargetId
	String_t* ___UniqueTargetId_1;
	// System.Single Vuforia.TargetFinder/TargetSearchResult::TargetSize
	float ___TargetSize_2;
	// System.String Vuforia.TargetFinder/TargetSearchResult::MetaData
	String_t* ___MetaData_3;
	// System.Byte Vuforia.TargetFinder/TargetSearchResult::TrackingRating
	uint8_t ___TrackingRating_4;
	// System.IntPtr Vuforia.TargetFinder/TargetSearchResult::TargetSearchResultPtr
	IntPtr_t ___TargetSearchResultPtr_5;

public:
	inline static int32_t get_offset_of_TargetName_0() { return static_cast<int32_t>(offsetof(TargetSearchResult_t213694407, ___TargetName_0)); }
	inline String_t* get_TargetName_0() const { return ___TargetName_0; }
	inline String_t** get_address_of_TargetName_0() { return &___TargetName_0; }
	inline void set_TargetName_0(String_t* value)
	{
		___TargetName_0 = value;
		Il2CppCodeGenWriteBarrier((&___TargetName_0), value);
	}

	inline static int32_t get_offset_of_UniqueTargetId_1() { return static_cast<int32_t>(offsetof(TargetSearchResult_t213694407, ___UniqueTargetId_1)); }
	inline String_t* get_UniqueTargetId_1() const { return ___UniqueTargetId_1; }
	inline String_t** get_address_of_UniqueTargetId_1() { return &___UniqueTargetId_1; }
	inline void set_UniqueTargetId_1(String_t* value)
	{
		___UniqueTargetId_1 = value;
		Il2CppCodeGenWriteBarrier((&___UniqueTargetId_1), value);
	}

	inline static int32_t get_offset_of_TargetSize_2() { return static_cast<int32_t>(offsetof(TargetSearchResult_t213694407, ___TargetSize_2)); }
	inline float get_TargetSize_2() const { return ___TargetSize_2; }
	inline float* get_address_of_TargetSize_2() { return &___TargetSize_2; }
	inline void set_TargetSize_2(float value)
	{
		___TargetSize_2 = value;
	}

	inline static int32_t get_offset_of_MetaData_3() { return static_cast<int32_t>(offsetof(TargetSearchResult_t213694407, ___MetaData_3)); }
	inline String_t* get_MetaData_3() const { return ___MetaData_3; }
	inline String_t** get_address_of_MetaData_3() { return &___MetaData_3; }
	inline void set_MetaData_3(String_t* value)
	{
		___MetaData_3 = value;
		Il2CppCodeGenWriteBarrier((&___MetaData_3), value);
	}

	inline static int32_t get_offset_of_TrackingRating_4() { return static_cast<int32_t>(offsetof(TargetSearchResult_t213694407, ___TrackingRating_4)); }
	inline uint8_t get_TrackingRating_4() const { return ___TrackingRating_4; }
	inline uint8_t* get_address_of_TrackingRating_4() { return &___TrackingRating_4; }
	inline void set_TrackingRating_4(uint8_t value)
	{
		___TrackingRating_4 = value;
	}

	inline static int32_t get_offset_of_TargetSearchResultPtr_5() { return static_cast<int32_t>(offsetof(TargetSearchResult_t213694407, ___TargetSearchResultPtr_5)); }
	inline IntPtr_t get_TargetSearchResultPtr_5() const { return ___TargetSearchResultPtr_5; }
	inline IntPtr_t* get_address_of_TargetSearchResultPtr_5() { return &___TargetSearchResultPtr_5; }
	inline void set_TargetSearchResultPtr_5(IntPtr_t value)
	{
		___TargetSearchResultPtr_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Vuforia.TargetFinder/TargetSearchResult
struct TargetSearchResult_t213694407_marshaled_pinvoke
{
	char* ___TargetName_0;
	char* ___UniqueTargetId_1;
	float ___TargetSize_2;
	char* ___MetaData_3;
	uint8_t ___TrackingRating_4;
	intptr_t ___TargetSearchResultPtr_5;
};
// Native definition for COM marshalling of Vuforia.TargetFinder/TargetSearchResult
struct TargetSearchResult_t213694407_marshaled_com
{
	Il2CppChar* ___TargetName_0;
	Il2CppChar* ___UniqueTargetId_1;
	float ___TargetSize_2;
	Il2CppChar* ___MetaData_3;
	uint8_t ___TrackingRating_4;
	intptr_t ___TargetSearchResultPtr_5;
};
#endif // TARGETSEARCHRESULT_T213694407_H
#ifndef UIVERTEX_T2786467440_H
#define UIVERTEX_T2786467440_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UIVertex
struct  UIVertex_t2786467440 
{
public:
	// UnityEngine.Vector3 UnityEngine.UIVertex::position
	Vector3_t2903530434  ___position_0;
	// UnityEngine.Vector3 UnityEngine.UIVertex::normal
	Vector3_t2903530434  ___normal_1;
	// UnityEngine.Color32 UnityEngine.UIVertex::color
	Color32_t285432355  ___color_2;
	// UnityEngine.Vector2 UnityEngine.UIVertex::uv0
	Vector2_t3577333262  ___uv0_3;
	// UnityEngine.Vector2 UnityEngine.UIVertex::uv1
	Vector2_t3577333262  ___uv1_4;
	// UnityEngine.Vector2 UnityEngine.UIVertex::uv2
	Vector2_t3577333262  ___uv2_5;
	// UnityEngine.Vector2 UnityEngine.UIVertex::uv3
	Vector2_t3577333262  ___uv3_6;
	// UnityEngine.Vector4 UnityEngine.UIVertex::tangent
	Vector4_t2814672345  ___tangent_7;

public:
	inline static int32_t get_offset_of_position_0() { return static_cast<int32_t>(offsetof(UIVertex_t2786467440, ___position_0)); }
	inline Vector3_t2903530434  get_position_0() const { return ___position_0; }
	inline Vector3_t2903530434 * get_address_of_position_0() { return &___position_0; }
	inline void set_position_0(Vector3_t2903530434  value)
	{
		___position_0 = value;
	}

	inline static int32_t get_offset_of_normal_1() { return static_cast<int32_t>(offsetof(UIVertex_t2786467440, ___normal_1)); }
	inline Vector3_t2903530434  get_normal_1() const { return ___normal_1; }
	inline Vector3_t2903530434 * get_address_of_normal_1() { return &___normal_1; }
	inline void set_normal_1(Vector3_t2903530434  value)
	{
		___normal_1 = value;
	}

	inline static int32_t get_offset_of_color_2() { return static_cast<int32_t>(offsetof(UIVertex_t2786467440, ___color_2)); }
	inline Color32_t285432355  get_color_2() const { return ___color_2; }
	inline Color32_t285432355 * get_address_of_color_2() { return &___color_2; }
	inline void set_color_2(Color32_t285432355  value)
	{
		___color_2 = value;
	}

	inline static int32_t get_offset_of_uv0_3() { return static_cast<int32_t>(offsetof(UIVertex_t2786467440, ___uv0_3)); }
	inline Vector2_t3577333262  get_uv0_3() const { return ___uv0_3; }
	inline Vector2_t3577333262 * get_address_of_uv0_3() { return &___uv0_3; }
	inline void set_uv0_3(Vector2_t3577333262  value)
	{
		___uv0_3 = value;
	}

	inline static int32_t get_offset_of_uv1_4() { return static_cast<int32_t>(offsetof(UIVertex_t2786467440, ___uv1_4)); }
	inline Vector2_t3577333262  get_uv1_4() const { return ___uv1_4; }
	inline Vector2_t3577333262 * get_address_of_uv1_4() { return &___uv1_4; }
	inline void set_uv1_4(Vector2_t3577333262  value)
	{
		___uv1_4 = value;
	}

	inline static int32_t get_offset_of_uv2_5() { return static_cast<int32_t>(offsetof(UIVertex_t2786467440, ___uv2_5)); }
	inline Vector2_t3577333262  get_uv2_5() const { return ___uv2_5; }
	inline Vector2_t3577333262 * get_address_of_uv2_5() { return &___uv2_5; }
	inline void set_uv2_5(Vector2_t3577333262  value)
	{
		___uv2_5 = value;
	}

	inline static int32_t get_offset_of_uv3_6() { return static_cast<int32_t>(offsetof(UIVertex_t2786467440, ___uv3_6)); }
	inline Vector2_t3577333262  get_uv3_6() const { return ___uv3_6; }
	inline Vector2_t3577333262 * get_address_of_uv3_6() { return &___uv3_6; }
	inline void set_uv3_6(Vector2_t3577333262  value)
	{
		___uv3_6 = value;
	}

	inline static int32_t get_offset_of_tangent_7() { return static_cast<int32_t>(offsetof(UIVertex_t2786467440, ___tangent_7)); }
	inline Vector4_t2814672345  get_tangent_7() const { return ___tangent_7; }
	inline Vector4_t2814672345 * get_address_of_tangent_7() { return &___tangent_7; }
	inline void set_tangent_7(Vector4_t2814672345  value)
	{
		___tangent_7 = value;
	}
};

struct UIVertex_t2786467440_StaticFields
{
public:
	// UnityEngine.Color32 UnityEngine.UIVertex::s_DefaultColor
	Color32_t285432355  ___s_DefaultColor_8;
	// UnityEngine.Vector4 UnityEngine.UIVertex::s_DefaultTangent
	Vector4_t2814672345  ___s_DefaultTangent_9;
	// UnityEngine.UIVertex UnityEngine.UIVertex::simpleVert
	UIVertex_t2786467440  ___simpleVert_10;

public:
	inline static int32_t get_offset_of_s_DefaultColor_8() { return static_cast<int32_t>(offsetof(UIVertex_t2786467440_StaticFields, ___s_DefaultColor_8)); }
	inline Color32_t285432355  get_s_DefaultColor_8() const { return ___s_DefaultColor_8; }
	inline Color32_t285432355 * get_address_of_s_DefaultColor_8() { return &___s_DefaultColor_8; }
	inline void set_s_DefaultColor_8(Color32_t285432355  value)
	{
		___s_DefaultColor_8 = value;
	}

	inline static int32_t get_offset_of_s_DefaultTangent_9() { return static_cast<int32_t>(offsetof(UIVertex_t2786467440_StaticFields, ___s_DefaultTangent_9)); }
	inline Vector4_t2814672345  get_s_DefaultTangent_9() const { return ___s_DefaultTangent_9; }
	inline Vector4_t2814672345 * get_address_of_s_DefaultTangent_9() { return &___s_DefaultTangent_9; }
	inline void set_s_DefaultTangent_9(Vector4_t2814672345  value)
	{
		___s_DefaultTangent_9 = value;
	}

	inline static int32_t get_offset_of_simpleVert_10() { return static_cast<int32_t>(offsetof(UIVertex_t2786467440_StaticFields, ___simpleVert_10)); }
	inline UIVertex_t2786467440  get_simpleVert_10() const { return ___simpleVert_10; }
	inline UIVertex_t2786467440 * get_address_of_simpleVert_10() { return &___simpleVert_10; }
	inline void set_simpleVert_10(UIVertex_t2786467440  value)
	{
		___simpleVert_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIVERTEX_T2786467440_H
#ifndef COLORTWEENMODE_T2379080415_H
#define COLORTWEENMODE_T2379080415_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.CoroutineTween.ColorTween/ColorTweenMode
struct  ColorTweenMode_t2379080415 
{
public:
	// System.Int32 UnityEngine.UI.CoroutineTween.ColorTween/ColorTweenMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ColorTweenMode_t2379080415, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLORTWEENMODE_T2379080415_H
#ifndef VUMARKTARGETRESULTDATA_T2086971180_H
#define VUMARKTARGETRESULTDATA_T2086971180_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.VuforiaManagerImpl/VuMarkTargetResultData
#pragma pack(push, tp, 1)
struct  VuMarkTargetResultData_t2086971180 
{
public:
	// Vuforia.VuforiaManagerImpl/PoseData Vuforia.VuforiaManagerImpl/VuMarkTargetResultData::pose
	PoseData_t1816720573  ___pose_0;
	// System.Double Vuforia.VuforiaManagerImpl/VuMarkTargetResultData::timeStamp
	double ___timeStamp_1;
	// System.Int32 Vuforia.VuforiaManagerImpl/VuMarkTargetResultData::statusInteger
	int32_t ___statusInteger_2;
	// System.Int32 Vuforia.VuforiaManagerImpl/VuMarkTargetResultData::targetID
	int32_t ___targetID_3;
	// System.Int32 Vuforia.VuforiaManagerImpl/VuMarkTargetResultData::resultID
	int32_t ___resultID_4;
	// System.Int32 Vuforia.VuforiaManagerImpl/VuMarkTargetResultData::unused
	int32_t ___unused_5;

public:
	inline static int32_t get_offset_of_pose_0() { return static_cast<int32_t>(offsetof(VuMarkTargetResultData_t2086971180, ___pose_0)); }
	inline PoseData_t1816720573  get_pose_0() const { return ___pose_0; }
	inline PoseData_t1816720573 * get_address_of_pose_0() { return &___pose_0; }
	inline void set_pose_0(PoseData_t1816720573  value)
	{
		___pose_0 = value;
	}

	inline static int32_t get_offset_of_timeStamp_1() { return static_cast<int32_t>(offsetof(VuMarkTargetResultData_t2086971180, ___timeStamp_1)); }
	inline double get_timeStamp_1() const { return ___timeStamp_1; }
	inline double* get_address_of_timeStamp_1() { return &___timeStamp_1; }
	inline void set_timeStamp_1(double value)
	{
		___timeStamp_1 = value;
	}

	inline static int32_t get_offset_of_statusInteger_2() { return static_cast<int32_t>(offsetof(VuMarkTargetResultData_t2086971180, ___statusInteger_2)); }
	inline int32_t get_statusInteger_2() const { return ___statusInteger_2; }
	inline int32_t* get_address_of_statusInteger_2() { return &___statusInteger_2; }
	inline void set_statusInteger_2(int32_t value)
	{
		___statusInteger_2 = value;
	}

	inline static int32_t get_offset_of_targetID_3() { return static_cast<int32_t>(offsetof(VuMarkTargetResultData_t2086971180, ___targetID_3)); }
	inline int32_t get_targetID_3() const { return ___targetID_3; }
	inline int32_t* get_address_of_targetID_3() { return &___targetID_3; }
	inline void set_targetID_3(int32_t value)
	{
		___targetID_3 = value;
	}

	inline static int32_t get_offset_of_resultID_4() { return static_cast<int32_t>(offsetof(VuMarkTargetResultData_t2086971180, ___resultID_4)); }
	inline int32_t get_resultID_4() const { return ___resultID_4; }
	inline int32_t* get_address_of_resultID_4() { return &___resultID_4; }
	inline void set_resultID_4(int32_t value)
	{
		___resultID_4 = value;
	}

	inline static int32_t get_offset_of_unused_5() { return static_cast<int32_t>(offsetof(VuMarkTargetResultData_t2086971180, ___unused_5)); }
	inline int32_t get_unused_5() const { return ___unused_5; }
	inline int32_t* get_address_of_unused_5() { return &___unused_5; }
	inline void set_unused_5(int32_t value)
	{
		___unused_5 = value;
	}
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VUMARKTARGETRESULTDATA_T2086971180_H
#ifndef COMPONENT_T4087199522_H
#define COMPONENT_T4087199522_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t4087199522  : public Object_t1502412432
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T4087199522_H
#ifndef VUMARKTARGETDATA_T2973346370_H
#define VUMARKTARGETDATA_T2973346370_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.VuforiaManagerImpl/VuMarkTargetData
#pragma pack(push, tp, 1)
struct  VuMarkTargetData_t2973346370 
{
public:
	// Vuforia.VuforiaManagerImpl/InstanceIdData Vuforia.VuforiaManagerImpl/VuMarkTargetData::instanceId
	InstanceIdData_t2286842551  ___instanceId_0;
	// System.Int32 Vuforia.VuforiaManagerImpl/VuMarkTargetData::id
	int32_t ___id_1;
	// System.Int32 Vuforia.VuforiaManagerImpl/VuMarkTargetData::templateId
	int32_t ___templateId_2;
	// UnityEngine.Vector3 Vuforia.VuforiaManagerImpl/VuMarkTargetData::size
	Vector3_t2903530434  ___size_3;
	// System.Int32 Vuforia.VuforiaManagerImpl/VuMarkTargetData::unused
	int32_t ___unused_4;

public:
	inline static int32_t get_offset_of_instanceId_0() { return static_cast<int32_t>(offsetof(VuMarkTargetData_t2973346370, ___instanceId_0)); }
	inline InstanceIdData_t2286842551  get_instanceId_0() const { return ___instanceId_0; }
	inline InstanceIdData_t2286842551 * get_address_of_instanceId_0() { return &___instanceId_0; }
	inline void set_instanceId_0(InstanceIdData_t2286842551  value)
	{
		___instanceId_0 = value;
	}

	inline static int32_t get_offset_of_id_1() { return static_cast<int32_t>(offsetof(VuMarkTargetData_t2973346370, ___id_1)); }
	inline int32_t get_id_1() const { return ___id_1; }
	inline int32_t* get_address_of_id_1() { return &___id_1; }
	inline void set_id_1(int32_t value)
	{
		___id_1 = value;
	}

	inline static int32_t get_offset_of_templateId_2() { return static_cast<int32_t>(offsetof(VuMarkTargetData_t2973346370, ___templateId_2)); }
	inline int32_t get_templateId_2() const { return ___templateId_2; }
	inline int32_t* get_address_of_templateId_2() { return &___templateId_2; }
	inline void set_templateId_2(int32_t value)
	{
		___templateId_2 = value;
	}

	inline static int32_t get_offset_of_size_3() { return static_cast<int32_t>(offsetof(VuMarkTargetData_t2973346370, ___size_3)); }
	inline Vector3_t2903530434  get_size_3() const { return ___size_3; }
	inline Vector3_t2903530434 * get_address_of_size_3() { return &___size_3; }
	inline void set_size_3(Vector3_t2903530434  value)
	{
		___size_3 = value;
	}

	inline static int32_t get_offset_of_unused_4() { return static_cast<int32_t>(offsetof(VuMarkTargetData_t2973346370, ___unused_4)); }
	inline int32_t get_unused_4() const { return ___unused_4; }
	inline int32_t* get_address_of_unused_4() { return &___unused_4; }
	inline void set_unused_4(int32_t value)
	{
		___unused_4 = value;
	}
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VUMARKTARGETDATA_T2973346370_H
#ifndef GAMEOBJECT_T1811656094_H
#define GAMEOBJECT_T1811656094_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.GameObject
struct  GameObject_t1811656094  : public Object_t1502412432
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAMEOBJECT_T1811656094_H
#ifndef TRACKABLERESULTDATA_T4095399453_H
#define TRACKABLERESULTDATA_T4095399453_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.VuforiaManagerImpl/TrackableResultData
#pragma pack(push, tp, 1)
struct  TrackableResultData_t4095399453 
{
public:
	// Vuforia.VuforiaManagerImpl/PoseData Vuforia.VuforiaManagerImpl/TrackableResultData::pose
	PoseData_t1816720573  ___pose_0;
	// System.Double Vuforia.VuforiaManagerImpl/TrackableResultData::timeStamp
	double ___timeStamp_1;
	// System.Int32 Vuforia.VuforiaManagerImpl/TrackableResultData::statusInteger
	int32_t ___statusInteger_2;
	// System.Int32 Vuforia.VuforiaManagerImpl/TrackableResultData::id
	int32_t ___id_3;

public:
	inline static int32_t get_offset_of_pose_0() { return static_cast<int32_t>(offsetof(TrackableResultData_t4095399453, ___pose_0)); }
	inline PoseData_t1816720573  get_pose_0() const { return ___pose_0; }
	inline PoseData_t1816720573 * get_address_of_pose_0() { return &___pose_0; }
	inline void set_pose_0(PoseData_t1816720573  value)
	{
		___pose_0 = value;
	}

	inline static int32_t get_offset_of_timeStamp_1() { return static_cast<int32_t>(offsetof(TrackableResultData_t4095399453, ___timeStamp_1)); }
	inline double get_timeStamp_1() const { return ___timeStamp_1; }
	inline double* get_address_of_timeStamp_1() { return &___timeStamp_1; }
	inline void set_timeStamp_1(double value)
	{
		___timeStamp_1 = value;
	}

	inline static int32_t get_offset_of_statusInteger_2() { return static_cast<int32_t>(offsetof(TrackableResultData_t4095399453, ___statusInteger_2)); }
	inline int32_t get_statusInteger_2() const { return ___statusInteger_2; }
	inline int32_t* get_address_of_statusInteger_2() { return &___statusInteger_2; }
	inline void set_statusInteger_2(int32_t value)
	{
		___statusInteger_2 = value;
	}

	inline static int32_t get_offset_of_id_3() { return static_cast<int32_t>(offsetof(TrackableResultData_t4095399453, ___id_3)); }
	inline int32_t get_id_3() const { return ___id_3; }
	inline int32_t* get_address_of_id_3() { return &___id_3; }
	inline void set_id_3(int32_t value)
	{
		___id_3 = value;
	}
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRACKABLERESULTDATA_T4095399453_H
#ifndef COLORTWEEN_T3915639497_H
#define COLORTWEEN_T3915639497_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.CoroutineTween.ColorTween
struct  ColorTween_t3915639497 
{
public:
	// UnityEngine.UI.CoroutineTween.ColorTween/ColorTweenCallback UnityEngine.UI.CoroutineTween.ColorTween::m_Target
	ColorTweenCallback_t3819146961 * ___m_Target_0;
	// UnityEngine.Color UnityEngine.UI.CoroutineTween.ColorTween::m_StartColor
	Color_t2507026410  ___m_StartColor_1;
	// UnityEngine.Color UnityEngine.UI.CoroutineTween.ColorTween::m_TargetColor
	Color_t2507026410  ___m_TargetColor_2;
	// UnityEngine.UI.CoroutineTween.ColorTween/ColorTweenMode UnityEngine.UI.CoroutineTween.ColorTween::m_TweenMode
	int32_t ___m_TweenMode_3;
	// System.Single UnityEngine.UI.CoroutineTween.ColorTween::m_Duration
	float ___m_Duration_4;
	// System.Boolean UnityEngine.UI.CoroutineTween.ColorTween::m_IgnoreTimeScale
	bool ___m_IgnoreTimeScale_5;

public:
	inline static int32_t get_offset_of_m_Target_0() { return static_cast<int32_t>(offsetof(ColorTween_t3915639497, ___m_Target_0)); }
	inline ColorTweenCallback_t3819146961 * get_m_Target_0() const { return ___m_Target_0; }
	inline ColorTweenCallback_t3819146961 ** get_address_of_m_Target_0() { return &___m_Target_0; }
	inline void set_m_Target_0(ColorTweenCallback_t3819146961 * value)
	{
		___m_Target_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Target_0), value);
	}

	inline static int32_t get_offset_of_m_StartColor_1() { return static_cast<int32_t>(offsetof(ColorTween_t3915639497, ___m_StartColor_1)); }
	inline Color_t2507026410  get_m_StartColor_1() const { return ___m_StartColor_1; }
	inline Color_t2507026410 * get_address_of_m_StartColor_1() { return &___m_StartColor_1; }
	inline void set_m_StartColor_1(Color_t2507026410  value)
	{
		___m_StartColor_1 = value;
	}

	inline static int32_t get_offset_of_m_TargetColor_2() { return static_cast<int32_t>(offsetof(ColorTween_t3915639497, ___m_TargetColor_2)); }
	inline Color_t2507026410  get_m_TargetColor_2() const { return ___m_TargetColor_2; }
	inline Color_t2507026410 * get_address_of_m_TargetColor_2() { return &___m_TargetColor_2; }
	inline void set_m_TargetColor_2(Color_t2507026410  value)
	{
		___m_TargetColor_2 = value;
	}

	inline static int32_t get_offset_of_m_TweenMode_3() { return static_cast<int32_t>(offsetof(ColorTween_t3915639497, ___m_TweenMode_3)); }
	inline int32_t get_m_TweenMode_3() const { return ___m_TweenMode_3; }
	inline int32_t* get_address_of_m_TweenMode_3() { return &___m_TweenMode_3; }
	inline void set_m_TweenMode_3(int32_t value)
	{
		___m_TweenMode_3 = value;
	}

	inline static int32_t get_offset_of_m_Duration_4() { return static_cast<int32_t>(offsetof(ColorTween_t3915639497, ___m_Duration_4)); }
	inline float get_m_Duration_4() const { return ___m_Duration_4; }
	inline float* get_address_of_m_Duration_4() { return &___m_Duration_4; }
	inline void set_m_Duration_4(float value)
	{
		___m_Duration_4 = value;
	}

	inline static int32_t get_offset_of_m_IgnoreTimeScale_5() { return static_cast<int32_t>(offsetof(ColorTween_t3915639497, ___m_IgnoreTimeScale_5)); }
	inline bool get_m_IgnoreTimeScale_5() const { return ___m_IgnoreTimeScale_5; }
	inline bool* get_address_of_m_IgnoreTimeScale_5() { return &___m_IgnoreTimeScale_5; }
	inline void set_m_IgnoreTimeScale_5(bool value)
	{
		___m_IgnoreTimeScale_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.UI.CoroutineTween.ColorTween
struct ColorTween_t3915639497_marshaled_pinvoke
{
	ColorTweenCallback_t3819146961 * ___m_Target_0;
	Color_t2507026410  ___m_StartColor_1;
	Color_t2507026410  ___m_TargetColor_2;
	int32_t ___m_TweenMode_3;
	float ___m_Duration_4;
	int32_t ___m_IgnoreTimeScale_5;
};
// Native definition for COM marshalling of UnityEngine.UI.CoroutineTween.ColorTween
struct ColorTween_t3915639497_marshaled_com
{
	ColorTweenCallback_t3819146961 * ___m_Target_0;
	Color_t2507026410  ___m_StartColor_1;
	Color_t2507026410  ___m_TargetColor_2;
	int32_t ___m_TweenMode_3;
	float ___m_Duration_4;
	int32_t ___m_IgnoreTimeScale_5;
};
#endif // COLORTWEEN_T3915639497_H
#ifndef TYPE_T_H
#define TYPE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Type
struct  Type_t  : public MemberInfo_t
{
public:
	// System.RuntimeTypeHandle System.Type::_impl
	RuntimeTypeHandle_t845633806  ____impl_1;

public:
	inline static int32_t get_offset_of__impl_1() { return static_cast<int32_t>(offsetof(Type_t, ____impl_1)); }
	inline RuntimeTypeHandle_t845633806  get__impl_1() const { return ____impl_1; }
	inline RuntimeTypeHandle_t845633806 * get_address_of__impl_1() { return &____impl_1; }
	inline void set__impl_1(RuntimeTypeHandle_t845633806  value)
	{
		____impl_1 = value;
	}
};

struct Type_t_StaticFields
{
public:
	// System.Char System.Type::Delimiter
	Il2CppChar ___Delimiter_2;
	// System.Type[] System.Type::EmptyTypes
	TypeU5BU5D_t3854702846* ___EmptyTypes_3;
	// System.Reflection.MemberFilter System.Type::FilterAttribute
	MemberFilter_t4208935459 * ___FilterAttribute_4;
	// System.Reflection.MemberFilter System.Type::FilterName
	MemberFilter_t4208935459 * ___FilterName_5;
	// System.Reflection.MemberFilter System.Type::FilterNameIgnoreCase
	MemberFilter_t4208935459 * ___FilterNameIgnoreCase_6;
	// System.Object System.Type::Missing
	RuntimeObject * ___Missing_7;

public:
	inline static int32_t get_offset_of_Delimiter_2() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Delimiter_2)); }
	inline Il2CppChar get_Delimiter_2() const { return ___Delimiter_2; }
	inline Il2CppChar* get_address_of_Delimiter_2() { return &___Delimiter_2; }
	inline void set_Delimiter_2(Il2CppChar value)
	{
		___Delimiter_2 = value;
	}

	inline static int32_t get_offset_of_EmptyTypes_3() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___EmptyTypes_3)); }
	inline TypeU5BU5D_t3854702846* get_EmptyTypes_3() const { return ___EmptyTypes_3; }
	inline TypeU5BU5D_t3854702846** get_address_of_EmptyTypes_3() { return &___EmptyTypes_3; }
	inline void set_EmptyTypes_3(TypeU5BU5D_t3854702846* value)
	{
		___EmptyTypes_3 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyTypes_3), value);
	}

	inline static int32_t get_offset_of_FilterAttribute_4() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterAttribute_4)); }
	inline MemberFilter_t4208935459 * get_FilterAttribute_4() const { return ___FilterAttribute_4; }
	inline MemberFilter_t4208935459 ** get_address_of_FilterAttribute_4() { return &___FilterAttribute_4; }
	inline void set_FilterAttribute_4(MemberFilter_t4208935459 * value)
	{
		___FilterAttribute_4 = value;
		Il2CppCodeGenWriteBarrier((&___FilterAttribute_4), value);
	}

	inline static int32_t get_offset_of_FilterName_5() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterName_5)); }
	inline MemberFilter_t4208935459 * get_FilterName_5() const { return ___FilterName_5; }
	inline MemberFilter_t4208935459 ** get_address_of_FilterName_5() { return &___FilterName_5; }
	inline void set_FilterName_5(MemberFilter_t4208935459 * value)
	{
		___FilterName_5 = value;
		Il2CppCodeGenWriteBarrier((&___FilterName_5), value);
	}

	inline static int32_t get_offset_of_FilterNameIgnoreCase_6() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterNameIgnoreCase_6)); }
	inline MemberFilter_t4208935459 * get_FilterNameIgnoreCase_6() const { return ___FilterNameIgnoreCase_6; }
	inline MemberFilter_t4208935459 ** get_address_of_FilterNameIgnoreCase_6() { return &___FilterNameIgnoreCase_6; }
	inline void set_FilterNameIgnoreCase_6(MemberFilter_t4208935459 * value)
	{
		___FilterNameIgnoreCase_6 = value;
		Il2CppCodeGenWriteBarrier((&___FilterNameIgnoreCase_6), value);
	}

	inline static int32_t get_offset_of_Missing_7() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Missing_7)); }
	inline RuntimeObject * get_Missing_7() const { return ___Missing_7; }
	inline RuntimeObject ** get_address_of_Missing_7() { return &___Missing_7; }
	inline void set_Missing_7(RuntimeObject * value)
	{
		___Missing_7 = value;
		Il2CppCodeGenWriteBarrier((&___Missing_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TYPE_T_H
#ifndef MULTICASTDELEGATE_T4139169907_H
#define MULTICASTDELEGATE_T4139169907_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t4139169907  : public Delegate_t537306192
{
public:
	// System.MulticastDelegate System.MulticastDelegate::prev
	MulticastDelegate_t4139169907 * ___prev_9;
	// System.MulticastDelegate System.MulticastDelegate::kpm_next
	MulticastDelegate_t4139169907 * ___kpm_next_10;

public:
	inline static int32_t get_offset_of_prev_9() { return static_cast<int32_t>(offsetof(MulticastDelegate_t4139169907, ___prev_9)); }
	inline MulticastDelegate_t4139169907 * get_prev_9() const { return ___prev_9; }
	inline MulticastDelegate_t4139169907 ** get_address_of_prev_9() { return &___prev_9; }
	inline void set_prev_9(MulticastDelegate_t4139169907 * value)
	{
		___prev_9 = value;
		Il2CppCodeGenWriteBarrier((&___prev_9), value);
	}

	inline static int32_t get_offset_of_kpm_next_10() { return static_cast<int32_t>(offsetof(MulticastDelegate_t4139169907, ___kpm_next_10)); }
	inline MulticastDelegate_t4139169907 * get_kpm_next_10() const { return ___kpm_next_10; }
	inline MulticastDelegate_t4139169907 ** get_address_of_kpm_next_10() { return &___kpm_next_10; }
	inline void set_kpm_next_10(MulticastDelegate_t4139169907 * value)
	{
		___kpm_next_10 = value;
		Il2CppCodeGenWriteBarrier((&___kpm_next_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MULTICASTDELEGATE_T4139169907_H
#ifndef CAMERAFIELD_T382214397_H
#define CAMERAFIELD_T382214397_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.CameraDevice/CameraField
struct  CameraField_t382214397 
{
public:
	// Vuforia.CameraDevice/CameraField/DataType Vuforia.CameraDevice/CameraField::Type
	int32_t ___Type_0;
	// System.String Vuforia.CameraDevice/CameraField::Key
	String_t* ___Key_1;

public:
	inline static int32_t get_offset_of_Type_0() { return static_cast<int32_t>(offsetof(CameraField_t382214397, ___Type_0)); }
	inline int32_t get_Type_0() const { return ___Type_0; }
	inline int32_t* get_address_of_Type_0() { return &___Type_0; }
	inline void set_Type_0(int32_t value)
	{
		___Type_0 = value;
	}

	inline static int32_t get_offset_of_Key_1() { return static_cast<int32_t>(offsetof(CameraField_t382214397, ___Key_1)); }
	inline String_t* get_Key_1() const { return ___Key_1; }
	inline String_t** get_address_of_Key_1() { return &___Key_1; }
	inline void set_Key_1(String_t* value)
	{
		___Key_1 = value;
		Il2CppCodeGenWriteBarrier((&___Key_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Vuforia.CameraDevice/CameraField
struct CameraField_t382214397_marshaled_pinvoke
{
	int32_t ___Type_0;
	char* ___Key_1;
};
// Native definition for COM marshalling of Vuforia.CameraDevice/CameraField
struct CameraField_t382214397_marshaled_com
{
	int32_t ___Type_0;
	Il2CppChar* ___Key_1;
};
#endif // CAMERAFIELD_T382214397_H
#ifndef PREDICATE_1_T3173636740_H
#define PREDICATE_1_T3173636740_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Predicate`1<UnityEngine.Color32>
struct  Predicate_1_t3173636740  : public MulticastDelegate_t4139169907
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PREDICATE_1_T3173636740_H
#ifndef UNITYACTION_1_T894467439_H
#define UNITYACTION_1_T894467439_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityAction`1<System.Collections.Generic.List`1<UnityEngine.UIVertex>>
struct  UnityAction_1_t894467439  : public MulticastDelegate_t4139169907
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYACTION_1_T894467439_H
#ifndef PREDICATE_1_T1074389306_H
#define PREDICATE_1_T1074389306_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Predicate`1<UnityEngine.UICharInfo>
struct  Predicate_1_t1074389306  : public MulticastDelegate_t4139169907
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PREDICATE_1_T1074389306_H
#ifndef PREDICATE_1_T3357142812_H
#define PREDICATE_1_T3357142812_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Predicate`1<System.Reflection.CustomAttributeNamedArgument>
struct  Predicate_1_t3357142812  : public MulticastDelegate_t4139169907
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PREDICATE_1_T3357142812_H
#ifndef PREDICATE_1_T3886715780_H
#define PREDICATE_1_T3886715780_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Predicate`1<UnityEngine.UILineInfo>
struct  Predicate_1_t3886715780  : public MulticastDelegate_t4139169907
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PREDICATE_1_T3886715780_H
#ifndef PREDICATE_1_T2104426847_H
#define PREDICATE_1_T2104426847_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Predicate`1<UnityEngine.EventSystems.RaycastResult>
struct  Predicate_1_t2104426847  : public MulticastDelegate_t4139169907
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PREDICATE_1_T2104426847_H
#ifndef UNITYACTION_1_T1685333261_H
#define UNITYACTION_1_T1685333261_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityAction`1<System.Collections.Generic.List`1<UnityEngine.Vector2>>
struct  UnityAction_1_t1685333261  : public MulticastDelegate_t4139169907
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYACTION_1_T1685333261_H
#ifndef BEHAVIOUR_T363748010_H
#define BEHAVIOUR_T363748010_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_t363748010  : public Component_t4087199522
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_T363748010_H
#ifndef UNITYACTION_1_T922672344_H
#define UNITYACTION_1_T922672344_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityAction`1<System.Collections.Generic.List`1<UnityEngine.Vector4>>
struct  UnityAction_1_t922672344  : public MulticastDelegate_t4139169907
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYACTION_1_T922672344_H
#ifndef PREDICATE_1_T1909102483_H
#define PREDICATE_1_T1909102483_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Predicate`1<UnityEngine.AnimatorClipInfo>
struct  Predicate_1_t1909102483  : public MulticastDelegate_t4139169907
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PREDICATE_1_T1909102483_H
#ifndef UNITYACTION_1_T1011530433_H
#define UNITYACTION_1_T1011530433_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityAction`1<System.Collections.Generic.List`1<UnityEngine.Vector3>>
struct  UnityAction_1_t1011530433  : public MulticastDelegate_t4139169907
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYACTION_1_T1011530433_H
#ifndef ASYNCCALLBACK_T1057242265_H
#define ASYNCCALLBACK_T1057242265_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.AsyncCallback
struct  AsyncCallback_t1057242265  : public MulticastDelegate_t4139169907
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASYNCCALLBACK_T1057242265_H
#ifndef PREDICATE_1_T244174443_H
#define PREDICATE_1_T244174443_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Predicate`1<System.Reflection.CustomAttributeTypedArgument>
struct  Predicate_1_t244174443  : public MulticastDelegate_t4139169907
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PREDICATE_1_T244174443_H
#ifndef PREDICATE_1_T178864225_H
#define PREDICATE_1_T178864225_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Predicate`1<Vuforia.Image/PIXEL_FORMAT>
struct  Predicate_1_t178864225  : public MulticastDelegate_t4139169907
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PREDICATE_1_T178864225_H
#ifndef PREDICATE_1_T1379704529_H
#define PREDICATE_1_T1379704529_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Predicate`1<UnityEngine.UIVertex>
struct  Predicate_1_t1379704529  : public MulticastDelegate_t4139169907
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PREDICATE_1_T1379704529_H
#ifndef PREDICATE_1_T1566583459_H
#define PREDICATE_1_T1566583459_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Predicate`1<Vuforia.VuforiaManagerImpl/VuMarkTargetData>
struct  Predicate_1_t1566583459  : public MulticastDelegate_t4139169907
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PREDICATE_1_T1566583459_H
#ifndef PREDICATE_1_T680208269_H
#define PREDICATE_1_T680208269_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Predicate`1<Vuforia.VuforiaManagerImpl/VuMarkTargetResultData>
struct  Predicate_1_t680208269  : public MulticastDelegate_t4139169907
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PREDICATE_1_T680208269_H
#ifndef GETTER_2_T1437382685_H
#define GETTER_2_T1437382685_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.MonoProperty/Getter`2<System.Object,System.Object>
struct  Getter_2_t1437382685  : public MulticastDelegate_t4139169907
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETTER_2_T1437382685_H
#ifndef STATICGETTER_1_T2059077758_H
#define STATICGETTER_1_T2059077758_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.MonoProperty/StaticGetter`1<System.Object>
struct  StaticGetter_1_t2059077758  : public MulticastDelegate_t4139169907
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STATICGETTER_1_T2059077758_H
#ifndef UNITYACTION_1_T2203695861_H
#define UNITYACTION_1_T2203695861_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityAction`1<System.Boolean>
struct  UnityAction_1_t2203695861  : public MulticastDelegate_t4139169907
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYACTION_1_T2203695861_H
#ifndef PREDICATE_1_T4219631771_H
#define PREDICATE_1_T4219631771_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Predicate`1<Vuforia.VuforiaManager/TrackableIdPair>
struct  Predicate_1_t4219631771  : public MulticastDelegate_t4139169907
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PREDICATE_1_T4219631771_H
#ifndef UNITYACTION_1_T3490795381_H
#define UNITYACTION_1_T3490795381_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityAction`1<System.Int32>
struct  UnityAction_1_t3490795381  : public MulticastDelegate_t4139169907
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYACTION_1_T3490795381_H
#ifndef UNITYACTION_1_T1274809082_H
#define UNITYACTION_1_T1274809082_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityAction`1<System.Object>
struct  UnityAction_1_t1274809082  : public MulticastDelegate_t4139169907
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYACTION_1_T1274809082_H
#ifndef UNITYACTION_1_T2298200551_H
#define UNITYACTION_1_T2298200551_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityAction`1<System.Single>
struct  UnityAction_1_t2298200551  : public MulticastDelegate_t4139169907
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYACTION_1_T2298200551_H
#ifndef UNITYACTION_1_T1759877202_H
#define UNITYACTION_1_T1759877202_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityAction`1<UnityEngine.Color>
struct  UnityAction_1_t1759877202  : public MulticastDelegate_t4139169907
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYACTION_1_T1759877202_H
#ifndef UNITYACTION_1_T2830184054_H
#define UNITYACTION_1_T2830184054_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityAction`1<UnityEngine.Vector2>
struct  UnityAction_1_t2830184054  : public MulticastDelegate_t4139169907
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYACTION_1_T2830184054_H
#ifndef UNITYACTION_2_T1132847070_H
#define UNITYACTION_2_T1132847070_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityAction`2<System.Object,System.Object>
struct  UnityAction_2_t1132847070  : public MulticastDelegate_t4139169907
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYACTION_2_T1132847070_H
#ifndef UNITYACTION_3_T1794137105_H
#define UNITYACTION_3_T1794137105_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityAction`3<System.Object,System.Object,System.Object>
struct  UnityAction_3_t1794137105  : public MulticastDelegate_t4139169907
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYACTION_3_T1794137105_H
#ifndef UNITYACTION_4_T226130048_H
#define UNITYACTION_4_T226130048_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityAction`4<System.Object,System.Object,System.Object,System.Object>
struct  UnityAction_4_t226130048  : public MulticastDelegate_t4139169907
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYACTION_4_T226130048_H
#ifndef UNITYACTION_2_T2064023076_H
#define UNITYACTION_2_T2064023076_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityAction`2<UnityEngine.SceneManagement.Scene,UnityEngine.SceneManagement.LoadSceneMode>
struct  UnityAction_2_t2064023076  : public MulticastDelegate_t4139169907
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYACTION_2_T2064023076_H
#ifndef UNITYACTION_2_T121587842_H
#define UNITYACTION_2_T121587842_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityAction`2<UnityEngine.SceneManagement.Scene,UnityEngine.SceneManagement.Scene>
struct  UnityAction_2_t121587842  : public MulticastDelegate_t4139169907
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYACTION_2_T121587842_H
#ifndef EVENTFUNCTION_1_T3830141588_H
#define EVENTFUNCTION_1_T3830141588_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<System.Object>
struct  EventFunction_1_t3830141588  : public MulticastDelegate_t4139169907
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EVENTFUNCTION_1_T3830141588_H
#ifndef PREDICATE_1_T3101898792_H
#define PREDICATE_1_T3101898792_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Predicate`1<Vuforia.TargetFinder/TargetSearchResult>
struct  Predicate_1_t3101898792  : public MulticastDelegate_t4139169907
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PREDICATE_1_T3101898792_H
#ifndef PREDICATE_1_T2688636542_H
#define PREDICATE_1_T2688636542_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Predicate`1<Vuforia.VuforiaManagerImpl/TrackableResultData>
struct  Predicate_1_t2688636542  : public MulticastDelegate_t4139169907
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PREDICATE_1_T2688636542_H
#ifndef PREDICATE_1_T615195379_H
#define PREDICATE_1_T615195379_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Predicate`1<System.Object>
struct  Predicate_1_t615195379  : public MulticastDelegate_t4139169907
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PREDICATE_1_T615195379_H
#ifndef COMPARISON_1_T1058283020_H
#define COMPARISON_1_T1058283020_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Comparison`1<System.Object>
struct  Comparison_1_t1058283020  : public MulticastDelegate_t4139169907
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPARISON_1_T1058283020_H
#ifndef U3CSTARTU3EC__ITERATOR0_T3365012439_H
#define U3CSTARTU3EC__ITERATOR0_T3365012439_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.CoroutineTween.TweenRunner`1/<Start>c__Iterator0<UnityEngine.UI.CoroutineTween.ColorTween>
struct  U3CStartU3Ec__Iterator0_t3365012439  : public RuntimeObject
{
public:
	// T UnityEngine.UI.CoroutineTween.TweenRunner`1/<Start>c__Iterator0::tweenInfo
	ColorTween_t3915639497  ___tweenInfo_0;
	// System.Single UnityEngine.UI.CoroutineTween.TweenRunner`1/<Start>c__Iterator0::<elapsedTime>__0
	float ___U3CelapsedTimeU3E__0_1;
	// System.Single UnityEngine.UI.CoroutineTween.TweenRunner`1/<Start>c__Iterator0::<percentage>__1
	float ___U3CpercentageU3E__1_2;
	// System.Object UnityEngine.UI.CoroutineTween.TweenRunner`1/<Start>c__Iterator0::$current
	RuntimeObject * ___U24current_3;
	// System.Boolean UnityEngine.UI.CoroutineTween.TweenRunner`1/<Start>c__Iterator0::$disposing
	bool ___U24disposing_4;
	// System.Int32 UnityEngine.UI.CoroutineTween.TweenRunner`1/<Start>c__Iterator0::$PC
	int32_t ___U24PC_5;

public:
	inline static int32_t get_offset_of_tweenInfo_0() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t3365012439, ___tweenInfo_0)); }
	inline ColorTween_t3915639497  get_tweenInfo_0() const { return ___tweenInfo_0; }
	inline ColorTween_t3915639497 * get_address_of_tweenInfo_0() { return &___tweenInfo_0; }
	inline void set_tweenInfo_0(ColorTween_t3915639497  value)
	{
		___tweenInfo_0 = value;
	}

	inline static int32_t get_offset_of_U3CelapsedTimeU3E__0_1() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t3365012439, ___U3CelapsedTimeU3E__0_1)); }
	inline float get_U3CelapsedTimeU3E__0_1() const { return ___U3CelapsedTimeU3E__0_1; }
	inline float* get_address_of_U3CelapsedTimeU3E__0_1() { return &___U3CelapsedTimeU3E__0_1; }
	inline void set_U3CelapsedTimeU3E__0_1(float value)
	{
		___U3CelapsedTimeU3E__0_1 = value;
	}

	inline static int32_t get_offset_of_U3CpercentageU3E__1_2() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t3365012439, ___U3CpercentageU3E__1_2)); }
	inline float get_U3CpercentageU3E__1_2() const { return ___U3CpercentageU3E__1_2; }
	inline float* get_address_of_U3CpercentageU3E__1_2() { return &___U3CpercentageU3E__1_2; }
	inline void set_U3CpercentageU3E__1_2(float value)
	{
		___U3CpercentageU3E__1_2 = value;
	}

	inline static int32_t get_offset_of_U24current_3() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t3365012439, ___U24current_3)); }
	inline RuntimeObject * get_U24current_3() const { return ___U24current_3; }
	inline RuntimeObject ** get_address_of_U24current_3() { return &___U24current_3; }
	inline void set_U24current_3(RuntimeObject * value)
	{
		___U24current_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_3), value);
	}

	inline static int32_t get_offset_of_U24disposing_4() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t3365012439, ___U24disposing_4)); }
	inline bool get_U24disposing_4() const { return ___U24disposing_4; }
	inline bool* get_address_of_U24disposing_4() { return &___U24disposing_4; }
	inline void set_U24disposing_4(bool value)
	{
		___U24disposing_4 = value;
	}

	inline static int32_t get_offset_of_U24PC_5() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t3365012439, ___U24PC_5)); }
	inline int32_t get_U24PC_5() const { return ___U24PC_5; }
	inline int32_t* get_address_of_U24PC_5() { return &___U24PC_5; }
	inline void set_U24PC_5(int32_t value)
	{
		___U24PC_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSTARTU3EC__ITERATOR0_T3365012439_H
#ifndef PREDICATE_1_T3270418782_H
#define PREDICATE_1_T3270418782_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Predicate`1<Vuforia.CameraDevice/CameraField>
struct  Predicate_1_t3270418782  : public MulticastDelegate_t4139169907
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PREDICATE_1_T3270418782_H
#ifndef PREDICATE_1_T1407909434_H
#define PREDICATE_1_T1407909434_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Predicate`1<UnityEngine.Vector4>
struct  Predicate_1_t1407909434  : public MulticastDelegate_t4139169907
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PREDICATE_1_T1407909434_H
#ifndef UNITYACTION_1_T2345944588_H
#define UNITYACTION_1_T2345944588_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityAction`1<System.Collections.Generic.List`1<System.Int32>>
struct  UnityAction_1_t2345944588  : public MulticastDelegate_t4139169907
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYACTION_1_T2345944588_H
#ifndef PREDICATE_1_T1496767523_H
#define PREDICATE_1_T1496767523_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Predicate`1<UnityEngine.Vector3>
struct  Predicate_1_t1496767523  : public MulticastDelegate_t4139169907
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PREDICATE_1_T1496767523_H
#ifndef UNITYACTION_1_T129958289_H
#define UNITYACTION_1_T129958289_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityAction`1<System.Collections.Generic.List`1<System.Object>>
struct  UnityAction_1_t129958289  : public MulticastDelegate_t4139169907
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYACTION_1_T129958289_H
#ifndef PREDICATE_1_T2170570351_H
#define PREDICATE_1_T2170570351_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Predicate`1<UnityEngine.Vector2>
struct  Predicate_1_t2170570351  : public MulticastDelegate_t4139169907
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PREDICATE_1_T2170570351_H
#ifndef UNITYACTION_1_T2688399650_H
#define UNITYACTION_1_T2688399650_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityAction`1<System.Collections.Generic.List`1<UnityEngine.Color32>>
struct  UnityAction_1_t2688399650  : public MulticastDelegate_t4139169907
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYACTION_1_T2688399650_H
#ifndef UNITYACTION_1_T3896475417_H
#define UNITYACTION_1_T3896475417_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityAction`1<UnityEngine.SceneManagement.Scene>
struct  UnityAction_1_t3896475417  : public MulticastDelegate_t4139169907
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYACTION_1_T3896475417_H
#ifndef MONOBEHAVIOUR_T345688271_H
#define MONOBEHAVIOUR_T345688271_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t345688271  : public Behaviour_t363748010
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T345688271_H
// System.Object[]
struct ObjectU5BU5D_t3270211303  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) RuntimeObject * m_Items[1];

public:
	inline RuntimeObject * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline RuntimeObject ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, RuntimeObject * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline RuntimeObject * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline RuntimeObject ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, RuntimeObject * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Type[]
struct TypeU5BU5D_t3854702846  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Type_t * m_Items[1];

public:
	inline Type_t * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Type_t ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Type_t * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline Type_t * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Type_t ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Type_t * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};


// System.Boolean System.Predicate`1<System.Reflection.CustomAttributeNamedArgument>::Invoke(T)
extern "C"  bool Predicate_1_Invoke_m2106050168_gshared (Predicate_1_t3357142812 * __this, CustomAttributeNamedArgument_t468938427  ___obj0, const RuntimeMethod* method);
// System.Boolean System.Predicate`1<System.Reflection.CustomAttributeTypedArgument>::Invoke(T)
extern "C"  bool Predicate_1_Invoke_m109527032_gshared (Predicate_1_t244174443 * __this, CustomAttributeTypedArgument_t1650937354  ___obj0, const RuntimeMethod* method);
// System.Boolean System.Predicate`1<UnityEngine.AnimatorClipInfo>::Invoke(T)
extern "C"  bool Predicate_1_Invoke_m3622366513_gshared (Predicate_1_t1909102483 * __this, AnimatorClipInfo_t3315865394  ___obj0, const RuntimeMethod* method);
// System.Boolean System.Predicate`1<UnityEngine.Color32>::Invoke(T)
extern "C"  bool Predicate_1_Invoke_m3120200200_gshared (Predicate_1_t3173636740 * __this, Color32_t285432355  ___obj0, const RuntimeMethod* method);
// System.Boolean System.Predicate`1<UnityEngine.EventSystems.RaycastResult>::Invoke(T)
extern "C"  bool Predicate_1_Invoke_m1614392815_gshared (Predicate_1_t2104426847 * __this, RaycastResult_t3511189758  ___obj0, const RuntimeMethod* method);
// System.Boolean System.Predicate`1<UnityEngine.UICharInfo>::Invoke(T)
extern "C"  bool Predicate_1_Invoke_m3923023531_gshared (Predicate_1_t1074389306 * __this, UICharInfo_t2481152217  ___obj0, const RuntimeMethod* method);
// System.Boolean System.Predicate`1<UnityEngine.UILineInfo>::Invoke(T)
extern "C"  bool Predicate_1_Invoke_m3726374234_gshared (Predicate_1_t3886715780 * __this, UILineInfo_t998511395  ___obj0, const RuntimeMethod* method);
// System.Boolean System.Predicate`1<UnityEngine.UIVertex>::Invoke(T)
extern "C"  bool Predicate_1_Invoke_m3878657971_gshared (Predicate_1_t1379704529 * __this, UIVertex_t2786467440  ___obj0, const RuntimeMethod* method);
// System.Boolean System.Predicate`1<UnityEngine.Vector2>::Invoke(T)
extern "C"  bool Predicate_1_Invoke_m1253036457_gshared (Predicate_1_t2170570351 * __this, Vector2_t3577333262  ___obj0, const RuntimeMethod* method);
// System.Boolean System.Predicate`1<UnityEngine.Vector3>::Invoke(T)
extern "C"  bool Predicate_1_Invoke_m269421512_gshared (Predicate_1_t1496767523 * __this, Vector3_t2903530434  ___obj0, const RuntimeMethod* method);
// System.Boolean System.Predicate`1<UnityEngine.Vector4>::Invoke(T)
extern "C"  bool Predicate_1_Invoke_m3968676339_gshared (Predicate_1_t1407909434 * __this, Vector4_t2814672345  ___obj0, const RuntimeMethod* method);
// System.Boolean System.Predicate`1<Vuforia.CameraDevice/CameraField>::Invoke(T)
extern "C"  bool Predicate_1_Invoke_m1773537808_gshared (Predicate_1_t3270418782 * __this, CameraField_t382214397  ___obj0, const RuntimeMethod* method);
// System.Boolean System.Predicate`1<Vuforia.Image/PIXEL_FORMAT>::Invoke(T)
extern "C"  bool Predicate_1_Invoke_m2525697755_gshared (Predicate_1_t178864225 * __this, int32_t ___obj0, const RuntimeMethod* method);
// System.Boolean System.Predicate`1<Vuforia.TargetFinder/TargetSearchResult>::Invoke(T)
extern "C"  bool Predicate_1_Invoke_m1814129163_gshared (Predicate_1_t3101898792 * __this, TargetSearchResult_t213694407  ___obj0, const RuntimeMethod* method);
// System.Boolean System.Predicate`1<Vuforia.VuforiaManager/TrackableIdPair>::Invoke(T)
extern "C"  bool Predicate_1_Invoke_m1599171516_gshared (Predicate_1_t4219631771 * __this, TrackableIdPair_t1331427386  ___obj0, const RuntimeMethod* method);
// System.Boolean System.Predicate`1<Vuforia.VuforiaManagerImpl/TrackableResultData>::Invoke(T)
extern "C"  bool Predicate_1_Invoke_m3067916862_gshared (Predicate_1_t2688636542 * __this, TrackableResultData_t4095399453  ___obj0, const RuntimeMethod* method);
// System.Boolean System.Predicate`1<Vuforia.VuforiaManagerImpl/VuMarkTargetData>::Invoke(T)
extern "C"  bool Predicate_1_Invoke_m174242021_gshared (Predicate_1_t1566583459 * __this, VuMarkTargetData_t2973346370  ___obj0, const RuntimeMethod* method);
// System.Boolean System.Predicate`1<Vuforia.VuforiaManagerImpl/VuMarkTargetResultData>::Invoke(T)
extern "C"  bool Predicate_1_Invoke_m2759091423_gshared (Predicate_1_t680208269 * __this, VuMarkTargetResultData_t2086971180  ___obj0, const RuntimeMethod* method);
// R System.Reflection.MonoProperty/Getter`2<System.Object,System.Object>::Invoke(T)
extern "C"  RuntimeObject * Getter_2_Invoke_m861197390_gshared (Getter_2_t1437382685 * __this, RuntimeObject * ____this0, const RuntimeMethod* method);
// R System.Reflection.MonoProperty/StaticGetter`1<System.Object>::Invoke()
extern "C"  RuntimeObject * StaticGetter_1_Invoke_m497351595_gshared (StaticGetter_1_t2059077758 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Events.UnityAction`1<System.Boolean>::Invoke(T0)
extern "C"  void UnityAction_1_Invoke_m2131770343_gshared (UnityAction_1_t2203695861 * __this, bool ___arg00, const RuntimeMethod* method);
// System.Void UnityEngine.Events.UnityAction`1<System.Int32>::Invoke(T0)
extern "C"  void UnityAction_1_Invoke_m1109029515_gshared (UnityAction_1_t3490795381 * __this, int32_t ___arg00, const RuntimeMethod* method);
// System.Void UnityEngine.Events.UnityAction`1<System.Object>::Invoke(T0)
extern "C"  void UnityAction_1_Invoke_m1618713631_gshared (UnityAction_1_t1274809082 * __this, RuntimeObject * ___arg00, const RuntimeMethod* method);
// System.Void UnityEngine.Events.UnityAction`1<System.Single>::Invoke(T0)
extern "C"  void UnityAction_1_Invoke_m3238493645_gshared (UnityAction_1_t2298200551 * __this, float ___arg00, const RuntimeMethod* method);
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.Color>::Invoke(T0)
extern "C"  void UnityAction_1_Invoke_m3221612891_gshared (UnityAction_1_t1759877202 * __this, Color_t2507026410  ___arg00, const RuntimeMethod* method);
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.SceneManagement.Scene>::Invoke(T0)
extern "C"  void UnityAction_1_Invoke_m2393581145_gshared (UnityAction_1_t3896475417 * __this, Scene_t348657329  ___arg00, const RuntimeMethod* method);
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.Vector2>::Invoke(T0)
extern "C"  void UnityAction_1_Invoke_m1552629663_gshared (UnityAction_1_t2830184054 * __this, Vector2_t3577333262  ___arg00, const RuntimeMethod* method);
// System.Void UnityEngine.Events.UnityAction`2<System.Object,System.Object>::Invoke(T0,T1)
extern "C"  void UnityAction_2_Invoke_m321356426_gshared (UnityAction_2_t1132847070 * __this, RuntimeObject * ___arg00, RuntimeObject * ___arg11, const RuntimeMethod* method);
// System.Void UnityEngine.Events.UnityAction`2<UnityEngine.SceneManagement.Scene,UnityEngine.SceneManagement.LoadSceneMode>::Invoke(T0,T1)
extern "C"  void UnityAction_2_Invoke_m2774749000_gshared (UnityAction_2_t2064023076 * __this, Scene_t348657329  ___arg00, int32_t ___arg11, const RuntimeMethod* method);
// System.Void UnityEngine.Events.UnityAction`2<UnityEngine.SceneManagement.Scene,UnityEngine.SceneManagement.Scene>::Invoke(T0,T1)
extern "C"  void UnityAction_2_Invoke_m3185242217_gshared (UnityAction_2_t121587842 * __this, Scene_t348657329  ___arg00, Scene_t348657329  ___arg11, const RuntimeMethod* method);
// System.Void UnityEngine.Events.UnityAction`3<System.Object,System.Object,System.Object>::Invoke(T0,T1,T2)
extern "C"  void UnityAction_3_Invoke_m2022456289_gshared (UnityAction_3_t1794137105 * __this, RuntimeObject * ___arg00, RuntimeObject * ___arg11, RuntimeObject * ___arg22, const RuntimeMethod* method);
// System.Void UnityEngine.Events.UnityAction`4<System.Object,System.Object,System.Object,System.Object>::Invoke(T0,T1,T2,T3)
extern "C"  void UnityAction_4_Invoke_m2730965101_gshared (UnityAction_4_t226130048 * __this, RuntimeObject * ___arg00, RuntimeObject * ___arg11, RuntimeObject * ___arg22, RuntimeObject * ___arg33, const RuntimeMethod* method);
// System.Void UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<System.Object>::Invoke(T1,UnityEngine.EventSystems.BaseEventData)
extern "C"  void EventFunction_1_Invoke_m3395982758_gshared (EventFunction_1_t3830141588 * __this, RuntimeObject * ___handler0, BaseEventData_t1865211501 * ___eventData1, const RuntimeMethod* method);

// System.Boolean System.Predicate`1<System.Reflection.CustomAttributeNamedArgument>::Invoke(T)
#define Predicate_1_Invoke_m2106050168(__this, ___obj0, method) ((  bool (*) (Predicate_1_t3357142812 *, CustomAttributeNamedArgument_t468938427 , const RuntimeMethod*))Predicate_1_Invoke_m2106050168_gshared)(__this, ___obj0, method)
// System.Boolean System.Predicate`1<System.Reflection.CustomAttributeTypedArgument>::Invoke(T)
#define Predicate_1_Invoke_m109527032(__this, ___obj0, method) ((  bool (*) (Predicate_1_t244174443 *, CustomAttributeTypedArgument_t1650937354 , const RuntimeMethod*))Predicate_1_Invoke_m109527032_gshared)(__this, ___obj0, method)
// System.Boolean System.Predicate`1<UnityEngine.AnimatorClipInfo>::Invoke(T)
#define Predicate_1_Invoke_m3622366513(__this, ___obj0, method) ((  bool (*) (Predicate_1_t1909102483 *, AnimatorClipInfo_t3315865394 , const RuntimeMethod*))Predicate_1_Invoke_m3622366513_gshared)(__this, ___obj0, method)
// System.Boolean System.Predicate`1<UnityEngine.Color32>::Invoke(T)
#define Predicate_1_Invoke_m3120200200(__this, ___obj0, method) ((  bool (*) (Predicate_1_t3173636740 *, Color32_t285432355 , const RuntimeMethod*))Predicate_1_Invoke_m3120200200_gshared)(__this, ___obj0, method)
// System.Boolean System.Predicate`1<UnityEngine.EventSystems.RaycastResult>::Invoke(T)
#define Predicate_1_Invoke_m1614392815(__this, ___obj0, method) ((  bool (*) (Predicate_1_t2104426847 *, RaycastResult_t3511189758 , const RuntimeMethod*))Predicate_1_Invoke_m1614392815_gshared)(__this, ___obj0, method)
// System.Boolean System.Predicate`1<UnityEngine.UICharInfo>::Invoke(T)
#define Predicate_1_Invoke_m3923023531(__this, ___obj0, method) ((  bool (*) (Predicate_1_t1074389306 *, UICharInfo_t2481152217 , const RuntimeMethod*))Predicate_1_Invoke_m3923023531_gshared)(__this, ___obj0, method)
// System.Boolean System.Predicate`1<UnityEngine.UILineInfo>::Invoke(T)
#define Predicate_1_Invoke_m3726374234(__this, ___obj0, method) ((  bool (*) (Predicate_1_t3886715780 *, UILineInfo_t998511395 , const RuntimeMethod*))Predicate_1_Invoke_m3726374234_gshared)(__this, ___obj0, method)
// System.Boolean System.Predicate`1<UnityEngine.UIVertex>::Invoke(T)
#define Predicate_1_Invoke_m3878657971(__this, ___obj0, method) ((  bool (*) (Predicate_1_t1379704529 *, UIVertex_t2786467440 , const RuntimeMethod*))Predicate_1_Invoke_m3878657971_gshared)(__this, ___obj0, method)
// System.Boolean System.Predicate`1<UnityEngine.Vector2>::Invoke(T)
#define Predicate_1_Invoke_m1253036457(__this, ___obj0, method) ((  bool (*) (Predicate_1_t2170570351 *, Vector2_t3577333262 , const RuntimeMethod*))Predicate_1_Invoke_m1253036457_gshared)(__this, ___obj0, method)
// System.Boolean System.Predicate`1<UnityEngine.Vector3>::Invoke(T)
#define Predicate_1_Invoke_m269421512(__this, ___obj0, method) ((  bool (*) (Predicate_1_t1496767523 *, Vector3_t2903530434 , const RuntimeMethod*))Predicate_1_Invoke_m269421512_gshared)(__this, ___obj0, method)
// System.Boolean System.Predicate`1<UnityEngine.Vector4>::Invoke(T)
#define Predicate_1_Invoke_m3968676339(__this, ___obj0, method) ((  bool (*) (Predicate_1_t1407909434 *, Vector4_t2814672345 , const RuntimeMethod*))Predicate_1_Invoke_m3968676339_gshared)(__this, ___obj0, method)
// System.Boolean System.Predicate`1<Vuforia.CameraDevice/CameraField>::Invoke(T)
#define Predicate_1_Invoke_m1773537808(__this, ___obj0, method) ((  bool (*) (Predicate_1_t3270418782 *, CameraField_t382214397 , const RuntimeMethod*))Predicate_1_Invoke_m1773537808_gshared)(__this, ___obj0, method)
// System.Boolean System.Predicate`1<Vuforia.Image/PIXEL_FORMAT>::Invoke(T)
#define Predicate_1_Invoke_m2525697755(__this, ___obj0, method) ((  bool (*) (Predicate_1_t178864225 *, int32_t, const RuntimeMethod*))Predicate_1_Invoke_m2525697755_gshared)(__this, ___obj0, method)
// System.Boolean System.Predicate`1<Vuforia.TargetFinder/TargetSearchResult>::Invoke(T)
#define Predicate_1_Invoke_m1814129163(__this, ___obj0, method) ((  bool (*) (Predicate_1_t3101898792 *, TargetSearchResult_t213694407 , const RuntimeMethod*))Predicate_1_Invoke_m1814129163_gshared)(__this, ___obj0, method)
// System.Boolean System.Predicate`1<Vuforia.VuforiaManager/TrackableIdPair>::Invoke(T)
#define Predicate_1_Invoke_m1599171516(__this, ___obj0, method) ((  bool (*) (Predicate_1_t4219631771 *, TrackableIdPair_t1331427386 , const RuntimeMethod*))Predicate_1_Invoke_m1599171516_gshared)(__this, ___obj0, method)
// System.Boolean System.Predicate`1<Vuforia.VuforiaManagerImpl/TrackableResultData>::Invoke(T)
#define Predicate_1_Invoke_m3067916862(__this, ___obj0, method) ((  bool (*) (Predicate_1_t2688636542 *, TrackableResultData_t4095399453 , const RuntimeMethod*))Predicate_1_Invoke_m3067916862_gshared)(__this, ___obj0, method)
// System.Boolean System.Predicate`1<Vuforia.VuforiaManagerImpl/VuMarkTargetData>::Invoke(T)
#define Predicate_1_Invoke_m174242021(__this, ___obj0, method) ((  bool (*) (Predicate_1_t1566583459 *, VuMarkTargetData_t2973346370 , const RuntimeMethod*))Predicate_1_Invoke_m174242021_gshared)(__this, ___obj0, method)
// System.Boolean System.Predicate`1<Vuforia.VuforiaManagerImpl/VuMarkTargetResultData>::Invoke(T)
#define Predicate_1_Invoke_m2759091423(__this, ___obj0, method) ((  bool (*) (Predicate_1_t680208269 *, VuMarkTargetResultData_t2086971180 , const RuntimeMethod*))Predicate_1_Invoke_m2759091423_gshared)(__this, ___obj0, method)
// R System.Reflection.MonoProperty/Getter`2<System.Object,System.Object>::Invoke(T)
#define Getter_2_Invoke_m861197390(__this, ____this0, method) ((  RuntimeObject * (*) (Getter_2_t1437382685 *, RuntimeObject *, const RuntimeMethod*))Getter_2_Invoke_m861197390_gshared)(__this, ____this0, method)
// R System.Reflection.MonoProperty/StaticGetter`1<System.Object>::Invoke()
#define StaticGetter_1_Invoke_m497351595(__this, method) ((  RuntimeObject * (*) (StaticGetter_1_t2059077758 *, const RuntimeMethod*))StaticGetter_1_Invoke_m497351595_gshared)(__this, method)
// System.Void UnityEngine.Events.BaseInvokableCall::.ctor(System.Object,System.Reflection.MethodInfo)
extern "C"  void BaseInvokableCall__ctor_m2708238577 (BaseInvokableCall_t1352930348 * __this, RuntimeObject * ___target0, MethodInfo_t * ___function1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Type System.Type::GetTypeFromHandle(System.RuntimeTypeHandle)
extern "C"  Type_t * Type_GetTypeFromHandle_m269683661 (RuntimeObject * __this /* static, unused */, RuntimeTypeHandle_t845633806  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Delegate UnityEngineInternal.NetFxCoreExtensions::CreateDelegate(System.Reflection.MethodInfo,System.Type,System.Object)
extern "C"  Delegate_t537306192 * NetFxCoreExtensions_CreateDelegate_m4287214728 (RuntimeObject * __this /* static, unused */, MethodInfo_t * ___self0, Type_t * ___delegateType1, RuntimeObject * ___target2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Events.BaseInvokableCall::.ctor()
extern "C"  void BaseInvokableCall__ctor_m1099533435 (BaseInvokableCall_t1352930348 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Delegate System.Delegate::Combine(System.Delegate,System.Delegate)
extern "C"  Delegate_t537306192 * Delegate_Combine_m408928526 (RuntimeObject * __this /* static, unused */, Delegate_t537306192 * p0, Delegate_t537306192 * p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Delegate System.Delegate::Remove(System.Delegate,System.Delegate)
extern "C"  Delegate_t537306192 * Delegate_Remove_m2220107885 (RuntimeObject * __this /* static, unused */, Delegate_t537306192 * p0, Delegate_t537306192 * p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.ArgumentException::.ctor(System.String)
extern "C"  void ArgumentException__ctor_m1186658332 (ArgumentException_t4028401650 * __this, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Events.BaseInvokableCall::AllowInvoke(System.Delegate)
extern "C"  bool BaseInvokableCall_AllowInvoke_m3827668627 (RuntimeObject * __this /* static, unused */, Delegate_t537306192 * ___delegate0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Object System.Delegate::get_Target()
extern "C"  RuntimeObject * Delegate_get_Target_m2013817205 (Delegate_t537306192 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Reflection.MethodInfo UnityEngineInternal.NetFxCoreExtensions::GetMethodInfo(System.Delegate)
extern "C"  MethodInfo_t * NetFxCoreExtensions_GetMethodInfo_m1250622137 (RuntimeObject * __this /* static, unused */, Delegate_t537306192 * ___self0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Events.UnityAction`1<System.Boolean>::Invoke(T0)
#define UnityAction_1_Invoke_m2131770343(__this, ___arg00, method) ((  void (*) (UnityAction_1_t2203695861 *, bool, const RuntimeMethod*))UnityAction_1_Invoke_m2131770343_gshared)(__this, ___arg00, method)
// System.Void UnityEngine.Events.UnityAction`1<System.Int32>::Invoke(T0)
#define UnityAction_1_Invoke_m1109029515(__this, ___arg00, method) ((  void (*) (UnityAction_1_t3490795381 *, int32_t, const RuntimeMethod*))UnityAction_1_Invoke_m1109029515_gshared)(__this, ___arg00, method)
// System.Void UnityEngine.Events.UnityAction`1<System.Object>::Invoke(T0)
#define UnityAction_1_Invoke_m1618713631(__this, ___arg00, method) ((  void (*) (UnityAction_1_t1274809082 *, RuntimeObject *, const RuntimeMethod*))UnityAction_1_Invoke_m1618713631_gshared)(__this, ___arg00, method)
// System.Void UnityEngine.Events.UnityAction`1<System.Single>::Invoke(T0)
#define UnityAction_1_Invoke_m3238493645(__this, ___arg00, method) ((  void (*) (UnityAction_1_t2298200551 *, float, const RuntimeMethod*))UnityAction_1_Invoke_m3238493645_gshared)(__this, ___arg00, method)
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.Color>::Invoke(T0)
#define UnityAction_1_Invoke_m3221612891(__this, ___arg00, method) ((  void (*) (UnityAction_1_t1759877202 *, Color_t2507026410 , const RuntimeMethod*))UnityAction_1_Invoke_m3221612891_gshared)(__this, ___arg00, method)
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.SceneManagement.Scene>::Invoke(T0)
#define UnityAction_1_Invoke_m2393581145(__this, ___arg00, method) ((  void (*) (UnityAction_1_t3896475417 *, Scene_t348657329 , const RuntimeMethod*))UnityAction_1_Invoke_m2393581145_gshared)(__this, ___arg00, method)
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.Vector2>::Invoke(T0)
#define UnityAction_1_Invoke_m1552629663(__this, ___arg00, method) ((  void (*) (UnityAction_1_t2830184054 *, Vector2_t3577333262 , const RuntimeMethod*))UnityAction_1_Invoke_m1552629663_gshared)(__this, ___arg00, method)
// System.Void UnityEngine.Events.UnityAction`2<System.Object,System.Object>::Invoke(T0,T1)
#define UnityAction_2_Invoke_m321356426(__this, ___arg00, ___arg11, method) ((  void (*) (UnityAction_2_t1132847070 *, RuntimeObject *, RuntimeObject *, const RuntimeMethod*))UnityAction_2_Invoke_m321356426_gshared)(__this, ___arg00, ___arg11, method)
// System.Void UnityEngine.Events.UnityAction`2<UnityEngine.SceneManagement.Scene,UnityEngine.SceneManagement.LoadSceneMode>::Invoke(T0,T1)
#define UnityAction_2_Invoke_m2774749000(__this, ___arg00, ___arg11, method) ((  void (*) (UnityAction_2_t2064023076 *, Scene_t348657329 , int32_t, const RuntimeMethod*))UnityAction_2_Invoke_m2774749000_gshared)(__this, ___arg00, ___arg11, method)
// System.Void UnityEngine.Events.UnityAction`2<UnityEngine.SceneManagement.Scene,UnityEngine.SceneManagement.Scene>::Invoke(T0,T1)
#define UnityAction_2_Invoke_m3185242217(__this, ___arg00, ___arg11, method) ((  void (*) (UnityAction_2_t121587842 *, Scene_t348657329 , Scene_t348657329 , const RuntimeMethod*))UnityAction_2_Invoke_m3185242217_gshared)(__this, ___arg00, ___arg11, method)
// System.Void UnityEngine.Events.UnityAction`3<System.Object,System.Object,System.Object>::Invoke(T0,T1,T2)
#define UnityAction_3_Invoke_m2022456289(__this, ___arg00, ___arg11, ___arg22, method) ((  void (*) (UnityAction_3_t1794137105 *, RuntimeObject *, RuntimeObject *, RuntimeObject *, const RuntimeMethod*))UnityAction_3_Invoke_m2022456289_gshared)(__this, ___arg00, ___arg11, ___arg22, method)
// System.Void UnityEngine.Events.UnityAction`4<System.Object,System.Object,System.Object,System.Object>::Invoke(T0,T1,T2,T3)
#define UnityAction_4_Invoke_m2730965101(__this, ___arg00, ___arg11, ___arg22, ___arg33, method) ((  void (*) (UnityAction_4_t226130048 *, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, const RuntimeMethod*))UnityAction_4_Invoke_m2730965101_gshared)(__this, ___arg00, ___arg11, ___arg22, ___arg33, method)
// System.Void UnityEngine.Events.UnityEventBase::.ctor()
extern "C"  void UnityEventBase__ctor_m821375546 (UnityEventBase_t2008500469 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Events.UnityEventBase::AddCall(UnityEngine.Events.BaseInvokableCall)
extern "C"  void UnityEventBase_AddCall_m3837898691 (UnityEventBase_t2008500469 * __this, BaseInvokableCall_t1352930348 * ___call0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Events.UnityEventBase::RemoveListener(System.Object,System.Reflection.MethodInfo)
extern "C"  void UnityEventBase_RemoveListener_m2620267480 (UnityEventBase_t2008500469 * __this, RuntimeObject * ___targetObj0, MethodInfo_t * ___method1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Reflection.MethodInfo UnityEngine.Events.UnityEventBase::GetValidMethodInfo(System.Object,System.String,System.Type[])
extern "C"  MethodInfo_t * UnityEventBase_GetValidMethodInfo_m2594745603 (RuntimeObject * __this /* static, unused */, RuntimeObject * ___obj0, String_t* ___functionName1, TypeU5BU5D_t3854702846* ___argumentTypes2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Events.UnityEventBase::Invoke(System.Object[])
extern "C"  void UnityEventBase_Invoke_m3195354837 (UnityEventBase_t2008500469 * __this, ObjectU5BU5D_t3270211303* ___parameters0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<System.Object>::Invoke(T1,UnityEngine.EventSystems.BaseEventData)
#define EventFunction_1_Invoke_m3395982758(__this, ___handler0, ___eventData1, method) ((  void (*) (EventFunction_1_t3830141588 *, RuntimeObject *, BaseEventData_t1865211501 *, const RuntimeMethod*))EventFunction_1_Invoke_m3395982758_gshared)(__this, ___handler0, ___eventData1, method)
// System.Void System.Object::.ctor()
extern "C"  void Object__ctor_m2906060009 (RuntimeObject * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.NotImplementedException::.ctor()
extern "C"  void NotImplementedException__ctor_m4036280126 (NotImplementedException_t2054929637 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.NotSupportedException::.ctor(System.String)
extern "C"  void NotSupportedException__ctor_m3333199409 (NotSupportedException_t1045231613 * __this, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.UI.CoroutineTween.ColorTween::ValidTarget()
extern "C"  bool ColorTween_ValidTarget_m980511496 (ColorTween_t3915639497 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.UI.CoroutineTween.ColorTween::get_ignoreTimeScale()
extern "C"  bool ColorTween_get_ignoreTimeScale_m3179503341 (ColorTween_t3915639497 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Time::get_unscaledDeltaTime()
extern "C"  float Time_get_unscaledDeltaTime_m3382011778 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Time::get_deltaTime()
extern "C"  float Time_get_deltaTime_m185246434 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.UI.CoroutineTween.ColorTween::get_duration()
extern "C"  float ColorTween_get_duration_m1500655497 (ColorTween_t3915639497 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Mathf::Clamp01(System.Single)
extern "C"  float Mathf_Clamp01_m664342029 (RuntimeObject * __this /* static, unused */, float p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.CoroutineTween.ColorTween::TweenValue(System.Single)
extern "C"  void ColorTween_TweenValue_m3743140801 (ColorTween_t3915639497 * __this, float ___floatPercentage0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.NotSupportedException::.ctor()
extern "C"  void NotSupportedException__ctor_m1411024145 (NotSupportedException_t1045231613 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.UI.CoroutineTween.FloatTween::ValidTarget()
extern "C"  bool FloatTween_ValidTarget_m1876138479 (FloatTween_t3485850636 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.UI.CoroutineTween.FloatTween::get_ignoreTimeScale()
extern "C"  bool FloatTween_get_ignoreTimeScale_m1804933600 (FloatTween_t3485850636 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.UI.CoroutineTween.FloatTween::get_duration()
extern "C"  float FloatTween_get_duration_m3446392591 (FloatTween_t3485850636 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.CoroutineTween.FloatTween::TweenValue(System.Single)
extern "C"  void FloatTween_TweenValue_m4176001828 (FloatTween_t3485850636 * __this, float ___floatPercentage0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Object::op_Equality(UnityEngine.Object,UnityEngine.Object)
extern "C"  bool Object_op_Equality_m2342070775 (RuntimeObject * __this /* static, unused */, Object_t1502412432 * p0, Object_t1502412432 * p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Debug::LogWarning(System.Object)
extern "C"  void Debug_LogWarning_m1906964694 (RuntimeObject * __this /* static, unused */, RuntimeObject * p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject UnityEngine.Component::get_gameObject()
extern "C"  GameObject_t1811656094 * Component_get_gameObject_m2283027183 (Component_t4087199522 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.GameObject::get_activeInHierarchy()
extern "C"  bool GameObject_get_activeInHierarchy_m1361876394 (GameObject_t1811656094 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Coroutine UnityEngine.MonoBehaviour::StartCoroutine(System.Collections.IEnumerator)
extern "C"  Coroutine_t2992159144 * MonoBehaviour_StartCoroutine_m2747597228 (MonoBehaviour_t345688271 * __this, RuntimeObject* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.MonoBehaviour::StopCoroutine(System.Collections.IEnumerator)
extern "C"  void MonoBehaviour_StopCoroutine_m366373519 (MonoBehaviour_t345688271 * __this, RuntimeObject* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Object::ReferenceEquals(System.Object,System.Object)
extern "C"  bool Object_ReferenceEquals_m2014193643 (RuntimeObject * __this /* static, unused */, RuntimeObject * p0, RuntimeObject * p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Debug::LogError(System.Object)
extern "C"  void Debug_LogError_m1848790000 (RuntimeObject * __this /* static, unused */, RuntimeObject * p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Predicate`1<System.Reflection.CustomAttributeNamedArgument>::.ctor(System.Object,System.IntPtr)
extern "C"  void Predicate_1__ctor_m3242669913_gshared (Predicate_1_t3357142812 * __this, RuntimeObject * ___object0, IntPtr_t ___method1, const RuntimeMethod* method)
{
	__this->set_method_ptr_0(il2cpp_codegen_get_method_pointer((RuntimeMethod*)___method1.get_m_value_0()));
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Boolean System.Predicate`1<System.Reflection.CustomAttributeNamedArgument>::Invoke(T)
extern "C"  bool Predicate_1_Invoke_m2106050168_gshared (Predicate_1_t3357142812 * __this, CustomAttributeNamedArgument_t468938427  ___obj0, const RuntimeMethod* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Predicate_1_Invoke_m2106050168((Predicate_1_t3357142812 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (RuntimeObject *, void* __this, CustomAttributeNamedArgument_t468938427  ___obj0, const RuntimeMethod* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___obj0,(RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef bool (*FunctionPointerType) (void* __this, CustomAttributeNamedArgument_t468938427  ___obj0, const RuntimeMethod* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___obj0,(RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Predicate`1<System.Reflection.CustomAttributeNamedArgument>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern "C"  RuntimeObject* Predicate_1_BeginInvoke_m618491045_gshared (Predicate_1_t3357142812 * __this, CustomAttributeNamedArgument_t468938427  ___obj0, AsyncCallback_t1057242265 * ___callback1, RuntimeObject * ___object2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Predicate_1_BeginInvoke_m618491045_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(CustomAttributeNamedArgument_t468938427_il2cpp_TypeInfo_var, &___obj0);
	return (RuntimeObject*)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (RuntimeObject*)___object2);
}
// System.Boolean System.Predicate`1<System.Reflection.CustomAttributeNamedArgument>::EndInvoke(System.IAsyncResult)
extern "C"  bool Predicate_1_EndInvoke_m2104320267_gshared (Predicate_1_t3357142812 * __this, RuntimeObject* ___result0, const RuntimeMethod* method)
{
	RuntimeObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(bool*)UnBox ((RuntimeObject*)__result);
}
// System.Void System.Predicate`1<System.Reflection.CustomAttributeTypedArgument>::.ctor(System.Object,System.IntPtr)
extern "C"  void Predicate_1__ctor_m899303296_gshared (Predicate_1_t244174443 * __this, RuntimeObject * ___object0, IntPtr_t ___method1, const RuntimeMethod* method)
{
	__this->set_method_ptr_0(il2cpp_codegen_get_method_pointer((RuntimeMethod*)___method1.get_m_value_0()));
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Boolean System.Predicate`1<System.Reflection.CustomAttributeTypedArgument>::Invoke(T)
extern "C"  bool Predicate_1_Invoke_m109527032_gshared (Predicate_1_t244174443 * __this, CustomAttributeTypedArgument_t1650937354  ___obj0, const RuntimeMethod* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Predicate_1_Invoke_m109527032((Predicate_1_t244174443 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (RuntimeObject *, void* __this, CustomAttributeTypedArgument_t1650937354  ___obj0, const RuntimeMethod* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___obj0,(RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef bool (*FunctionPointerType) (void* __this, CustomAttributeTypedArgument_t1650937354  ___obj0, const RuntimeMethod* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___obj0,(RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Predicate`1<System.Reflection.CustomAttributeTypedArgument>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern "C"  RuntimeObject* Predicate_1_BeginInvoke_m220190048_gshared (Predicate_1_t244174443 * __this, CustomAttributeTypedArgument_t1650937354  ___obj0, AsyncCallback_t1057242265 * ___callback1, RuntimeObject * ___object2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Predicate_1_BeginInvoke_m220190048_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(CustomAttributeTypedArgument_t1650937354_il2cpp_TypeInfo_var, &___obj0);
	return (RuntimeObject*)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (RuntimeObject*)___object2);
}
// System.Boolean System.Predicate`1<System.Reflection.CustomAttributeTypedArgument>::EndInvoke(System.IAsyncResult)
extern "C"  bool Predicate_1_EndInvoke_m1436033049_gshared (Predicate_1_t244174443 * __this, RuntimeObject* ___result0, const RuntimeMethod* method)
{
	RuntimeObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(bool*)UnBox ((RuntimeObject*)__result);
}
// System.Void System.Predicate`1<UnityEngine.AnimatorClipInfo>::.ctor(System.Object,System.IntPtr)
extern "C"  void Predicate_1__ctor_m2457847283_gshared (Predicate_1_t1909102483 * __this, RuntimeObject * ___object0, IntPtr_t ___method1, const RuntimeMethod* method)
{
	__this->set_method_ptr_0(il2cpp_codegen_get_method_pointer((RuntimeMethod*)___method1.get_m_value_0()));
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Boolean System.Predicate`1<UnityEngine.AnimatorClipInfo>::Invoke(T)
extern "C"  bool Predicate_1_Invoke_m3622366513_gshared (Predicate_1_t1909102483 * __this, AnimatorClipInfo_t3315865394  ___obj0, const RuntimeMethod* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Predicate_1_Invoke_m3622366513((Predicate_1_t1909102483 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (RuntimeObject *, void* __this, AnimatorClipInfo_t3315865394  ___obj0, const RuntimeMethod* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___obj0,(RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef bool (*FunctionPointerType) (void* __this, AnimatorClipInfo_t3315865394  ___obj0, const RuntimeMethod* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___obj0,(RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Predicate`1<UnityEngine.AnimatorClipInfo>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern "C"  RuntimeObject* Predicate_1_BeginInvoke_m1045513357_gshared (Predicate_1_t1909102483 * __this, AnimatorClipInfo_t3315865394  ___obj0, AsyncCallback_t1057242265 * ___callback1, RuntimeObject * ___object2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Predicate_1_BeginInvoke_m1045513357_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(AnimatorClipInfo_t3315865394_il2cpp_TypeInfo_var, &___obj0);
	return (RuntimeObject*)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (RuntimeObject*)___object2);
}
// System.Boolean System.Predicate`1<UnityEngine.AnimatorClipInfo>::EndInvoke(System.IAsyncResult)
extern "C"  bool Predicate_1_EndInvoke_m97379067_gshared (Predicate_1_t1909102483 * __this, RuntimeObject* ___result0, const RuntimeMethod* method)
{
	RuntimeObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(bool*)UnBox ((RuntimeObject*)__result);
}
// System.Void System.Predicate`1<UnityEngine.Color32>::.ctor(System.Object,System.IntPtr)
extern "C"  void Predicate_1__ctor_m2121338646_gshared (Predicate_1_t3173636740 * __this, RuntimeObject * ___object0, IntPtr_t ___method1, const RuntimeMethod* method)
{
	__this->set_method_ptr_0(il2cpp_codegen_get_method_pointer((RuntimeMethod*)___method1.get_m_value_0()));
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Boolean System.Predicate`1<UnityEngine.Color32>::Invoke(T)
extern "C"  bool Predicate_1_Invoke_m3120200200_gshared (Predicate_1_t3173636740 * __this, Color32_t285432355  ___obj0, const RuntimeMethod* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Predicate_1_Invoke_m3120200200((Predicate_1_t3173636740 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (RuntimeObject *, void* __this, Color32_t285432355  ___obj0, const RuntimeMethod* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___obj0,(RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef bool (*FunctionPointerType) (void* __this, Color32_t285432355  ___obj0, const RuntimeMethod* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___obj0,(RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Predicate`1<UnityEngine.Color32>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern "C"  RuntimeObject* Predicate_1_BeginInvoke_m1689932010_gshared (Predicate_1_t3173636740 * __this, Color32_t285432355  ___obj0, AsyncCallback_t1057242265 * ___callback1, RuntimeObject * ___object2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Predicate_1_BeginInvoke_m1689932010_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Color32_t285432355_il2cpp_TypeInfo_var, &___obj0);
	return (RuntimeObject*)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (RuntimeObject*)___object2);
}
// System.Boolean System.Predicate`1<UnityEngine.Color32>::EndInvoke(System.IAsyncResult)
extern "C"  bool Predicate_1_EndInvoke_m1888316527_gshared (Predicate_1_t3173636740 * __this, RuntimeObject* ___result0, const RuntimeMethod* method)
{
	RuntimeObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(bool*)UnBox ((RuntimeObject*)__result);
}
// System.Void System.Predicate`1<UnityEngine.EventSystems.RaycastResult>::.ctor(System.Object,System.IntPtr)
extern "C"  void Predicate_1__ctor_m460955614_gshared (Predicate_1_t2104426847 * __this, RuntimeObject * ___object0, IntPtr_t ___method1, const RuntimeMethod* method)
{
	__this->set_method_ptr_0(il2cpp_codegen_get_method_pointer((RuntimeMethod*)___method1.get_m_value_0()));
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Boolean System.Predicate`1<UnityEngine.EventSystems.RaycastResult>::Invoke(T)
extern "C"  bool Predicate_1_Invoke_m1614392815_gshared (Predicate_1_t2104426847 * __this, RaycastResult_t3511189758  ___obj0, const RuntimeMethod* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Predicate_1_Invoke_m1614392815((Predicate_1_t2104426847 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (RuntimeObject *, void* __this, RaycastResult_t3511189758  ___obj0, const RuntimeMethod* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___obj0,(RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef bool (*FunctionPointerType) (void* __this, RaycastResult_t3511189758  ___obj0, const RuntimeMethod* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___obj0,(RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Predicate`1<UnityEngine.EventSystems.RaycastResult>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern "C"  RuntimeObject* Predicate_1_BeginInvoke_m169345499_gshared (Predicate_1_t2104426847 * __this, RaycastResult_t3511189758  ___obj0, AsyncCallback_t1057242265 * ___callback1, RuntimeObject * ___object2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Predicate_1_BeginInvoke_m169345499_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(RaycastResult_t3511189758_il2cpp_TypeInfo_var, &___obj0);
	return (RuntimeObject*)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (RuntimeObject*)___object2);
}
// System.Boolean System.Predicate`1<UnityEngine.EventSystems.RaycastResult>::EndInvoke(System.IAsyncResult)
extern "C"  bool Predicate_1_EndInvoke_m1930201460_gshared (Predicate_1_t2104426847 * __this, RuntimeObject* ___result0, const RuntimeMethod* method)
{
	RuntimeObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(bool*)UnBox ((RuntimeObject*)__result);
}
// System.Void System.Predicate`1<UnityEngine.UICharInfo>::.ctor(System.Object,System.IntPtr)
extern "C"  void Predicate_1__ctor_m3352336357_gshared (Predicate_1_t1074389306 * __this, RuntimeObject * ___object0, IntPtr_t ___method1, const RuntimeMethod* method)
{
	__this->set_method_ptr_0(il2cpp_codegen_get_method_pointer((RuntimeMethod*)___method1.get_m_value_0()));
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Boolean System.Predicate`1<UnityEngine.UICharInfo>::Invoke(T)
extern "C"  bool Predicate_1_Invoke_m3923023531_gshared (Predicate_1_t1074389306 * __this, UICharInfo_t2481152217  ___obj0, const RuntimeMethod* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Predicate_1_Invoke_m3923023531((Predicate_1_t1074389306 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (RuntimeObject *, void* __this, UICharInfo_t2481152217  ___obj0, const RuntimeMethod* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___obj0,(RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef bool (*FunctionPointerType) (void* __this, UICharInfo_t2481152217  ___obj0, const RuntimeMethod* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___obj0,(RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Predicate`1<UnityEngine.UICharInfo>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern "C"  RuntimeObject* Predicate_1_BeginInvoke_m1676152281_gshared (Predicate_1_t1074389306 * __this, UICharInfo_t2481152217  ___obj0, AsyncCallback_t1057242265 * ___callback1, RuntimeObject * ___object2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Predicate_1_BeginInvoke_m1676152281_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(UICharInfo_t2481152217_il2cpp_TypeInfo_var, &___obj0);
	return (RuntimeObject*)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (RuntimeObject*)___object2);
}
// System.Boolean System.Predicate`1<UnityEngine.UICharInfo>::EndInvoke(System.IAsyncResult)
extern "C"  bool Predicate_1_EndInvoke_m730307406_gshared (Predicate_1_t1074389306 * __this, RuntimeObject* ___result0, const RuntimeMethod* method)
{
	RuntimeObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(bool*)UnBox ((RuntimeObject*)__result);
}
// System.Void System.Predicate`1<UnityEngine.UILineInfo>::.ctor(System.Object,System.IntPtr)
extern "C"  void Predicate_1__ctor_m3494382946_gshared (Predicate_1_t3886715780 * __this, RuntimeObject * ___object0, IntPtr_t ___method1, const RuntimeMethod* method)
{
	__this->set_method_ptr_0(il2cpp_codegen_get_method_pointer((RuntimeMethod*)___method1.get_m_value_0()));
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Boolean System.Predicate`1<UnityEngine.UILineInfo>::Invoke(T)
extern "C"  bool Predicate_1_Invoke_m3726374234_gshared (Predicate_1_t3886715780 * __this, UILineInfo_t998511395  ___obj0, const RuntimeMethod* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Predicate_1_Invoke_m3726374234((Predicate_1_t3886715780 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (RuntimeObject *, void* __this, UILineInfo_t998511395  ___obj0, const RuntimeMethod* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___obj0,(RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef bool (*FunctionPointerType) (void* __this, UILineInfo_t998511395  ___obj0, const RuntimeMethod* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___obj0,(RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Predicate`1<UnityEngine.UILineInfo>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern "C"  RuntimeObject* Predicate_1_BeginInvoke_m3075330401_gshared (Predicate_1_t3886715780 * __this, UILineInfo_t998511395  ___obj0, AsyncCallback_t1057242265 * ___callback1, RuntimeObject * ___object2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Predicate_1_BeginInvoke_m3075330401_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(UILineInfo_t998511395_il2cpp_TypeInfo_var, &___obj0);
	return (RuntimeObject*)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (RuntimeObject*)___object2);
}
// System.Boolean System.Predicate`1<UnityEngine.UILineInfo>::EndInvoke(System.IAsyncResult)
extern "C"  bool Predicate_1_EndInvoke_m2442834223_gshared (Predicate_1_t3886715780 * __this, RuntimeObject* ___result0, const RuntimeMethod* method)
{
	RuntimeObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(bool*)UnBox ((RuntimeObject*)__result);
}
// System.Void System.Predicate`1<UnityEngine.UIVertex>::.ctor(System.Object,System.IntPtr)
extern "C"  void Predicate_1__ctor_m3146396903_gshared (Predicate_1_t1379704529 * __this, RuntimeObject * ___object0, IntPtr_t ___method1, const RuntimeMethod* method)
{
	__this->set_method_ptr_0(il2cpp_codegen_get_method_pointer((RuntimeMethod*)___method1.get_m_value_0()));
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Boolean System.Predicate`1<UnityEngine.UIVertex>::Invoke(T)
extern "C"  bool Predicate_1_Invoke_m3878657971_gshared (Predicate_1_t1379704529 * __this, UIVertex_t2786467440  ___obj0, const RuntimeMethod* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Predicate_1_Invoke_m3878657971((Predicate_1_t1379704529 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (RuntimeObject *, void* __this, UIVertex_t2786467440  ___obj0, const RuntimeMethod* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___obj0,(RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef bool (*FunctionPointerType) (void* __this, UIVertex_t2786467440  ___obj0, const RuntimeMethod* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___obj0,(RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Predicate`1<UnityEngine.UIVertex>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern "C"  RuntimeObject* Predicate_1_BeginInvoke_m2946408774_gshared (Predicate_1_t1379704529 * __this, UIVertex_t2786467440  ___obj0, AsyncCallback_t1057242265 * ___callback1, RuntimeObject * ___object2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Predicate_1_BeginInvoke_m2946408774_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(UIVertex_t2786467440_il2cpp_TypeInfo_var, &___obj0);
	return (RuntimeObject*)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (RuntimeObject*)___object2);
}
// System.Boolean System.Predicate`1<UnityEngine.UIVertex>::EndInvoke(System.IAsyncResult)
extern "C"  bool Predicate_1_EndInvoke_m2844612177_gshared (Predicate_1_t1379704529 * __this, RuntimeObject* ___result0, const RuntimeMethod* method)
{
	RuntimeObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(bool*)UnBox ((RuntimeObject*)__result);
}
// System.Void System.Predicate`1<UnityEngine.Vector2>::.ctor(System.Object,System.IntPtr)
extern "C"  void Predicate_1__ctor_m3286293337_gshared (Predicate_1_t2170570351 * __this, RuntimeObject * ___object0, IntPtr_t ___method1, const RuntimeMethod* method)
{
	__this->set_method_ptr_0(il2cpp_codegen_get_method_pointer((RuntimeMethod*)___method1.get_m_value_0()));
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Boolean System.Predicate`1<UnityEngine.Vector2>::Invoke(T)
extern "C"  bool Predicate_1_Invoke_m1253036457_gshared (Predicate_1_t2170570351 * __this, Vector2_t3577333262  ___obj0, const RuntimeMethod* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Predicate_1_Invoke_m1253036457((Predicate_1_t2170570351 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (RuntimeObject *, void* __this, Vector2_t3577333262  ___obj0, const RuntimeMethod* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___obj0,(RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef bool (*FunctionPointerType) (void* __this, Vector2_t3577333262  ___obj0, const RuntimeMethod* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___obj0,(RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Predicate`1<UnityEngine.Vector2>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern "C"  RuntimeObject* Predicate_1_BeginInvoke_m1660015351_gshared (Predicate_1_t2170570351 * __this, Vector2_t3577333262  ___obj0, AsyncCallback_t1057242265 * ___callback1, RuntimeObject * ___object2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Predicate_1_BeginInvoke_m1660015351_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Vector2_t3577333262_il2cpp_TypeInfo_var, &___obj0);
	return (RuntimeObject*)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (RuntimeObject*)___object2);
}
// System.Boolean System.Predicate`1<UnityEngine.Vector2>::EndInvoke(System.IAsyncResult)
extern "C"  bool Predicate_1_EndInvoke_m2044220707_gshared (Predicate_1_t2170570351 * __this, RuntimeObject* ___result0, const RuntimeMethod* method)
{
	RuntimeObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(bool*)UnBox ((RuntimeObject*)__result);
}
// System.Void System.Predicate`1<UnityEngine.Vector3>::.ctor(System.Object,System.IntPtr)
extern "C"  void Predicate_1__ctor_m248615637_gshared (Predicate_1_t1496767523 * __this, RuntimeObject * ___object0, IntPtr_t ___method1, const RuntimeMethod* method)
{
	__this->set_method_ptr_0(il2cpp_codegen_get_method_pointer((RuntimeMethod*)___method1.get_m_value_0()));
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Boolean System.Predicate`1<UnityEngine.Vector3>::Invoke(T)
extern "C"  bool Predicate_1_Invoke_m269421512_gshared (Predicate_1_t1496767523 * __this, Vector3_t2903530434  ___obj0, const RuntimeMethod* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Predicate_1_Invoke_m269421512((Predicate_1_t1496767523 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (RuntimeObject *, void* __this, Vector3_t2903530434  ___obj0, const RuntimeMethod* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___obj0,(RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef bool (*FunctionPointerType) (void* __this, Vector3_t2903530434  ___obj0, const RuntimeMethod* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___obj0,(RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Predicate`1<UnityEngine.Vector3>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern "C"  RuntimeObject* Predicate_1_BeginInvoke_m4039012456_gshared (Predicate_1_t1496767523 * __this, Vector3_t2903530434  ___obj0, AsyncCallback_t1057242265 * ___callback1, RuntimeObject * ___object2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Predicate_1_BeginInvoke_m4039012456_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Vector3_t2903530434_il2cpp_TypeInfo_var, &___obj0);
	return (RuntimeObject*)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (RuntimeObject*)___object2);
}
// System.Boolean System.Predicate`1<UnityEngine.Vector3>::EndInvoke(System.IAsyncResult)
extern "C"  bool Predicate_1_EndInvoke_m540763832_gshared (Predicate_1_t1496767523 * __this, RuntimeObject* ___result0, const RuntimeMethod* method)
{
	RuntimeObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(bool*)UnBox ((RuntimeObject*)__result);
}
// System.Void System.Predicate`1<UnityEngine.Vector4>::.ctor(System.Object,System.IntPtr)
extern "C"  void Predicate_1__ctor_m4106038023_gshared (Predicate_1_t1407909434 * __this, RuntimeObject * ___object0, IntPtr_t ___method1, const RuntimeMethod* method)
{
	__this->set_method_ptr_0(il2cpp_codegen_get_method_pointer((RuntimeMethod*)___method1.get_m_value_0()));
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Boolean System.Predicate`1<UnityEngine.Vector4>::Invoke(T)
extern "C"  bool Predicate_1_Invoke_m3968676339_gshared (Predicate_1_t1407909434 * __this, Vector4_t2814672345  ___obj0, const RuntimeMethod* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Predicate_1_Invoke_m3968676339((Predicate_1_t1407909434 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (RuntimeObject *, void* __this, Vector4_t2814672345  ___obj0, const RuntimeMethod* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___obj0,(RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef bool (*FunctionPointerType) (void* __this, Vector4_t2814672345  ___obj0, const RuntimeMethod* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___obj0,(RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Predicate`1<UnityEngine.Vector4>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern "C"  RuntimeObject* Predicate_1_BeginInvoke_m2369142868_gshared (Predicate_1_t1407909434 * __this, Vector4_t2814672345  ___obj0, AsyncCallback_t1057242265 * ___callback1, RuntimeObject * ___object2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Predicate_1_BeginInvoke_m2369142868_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Vector4_t2814672345_il2cpp_TypeInfo_var, &___obj0);
	return (RuntimeObject*)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (RuntimeObject*)___object2);
}
// System.Boolean System.Predicate`1<UnityEngine.Vector4>::EndInvoke(System.IAsyncResult)
extern "C"  bool Predicate_1_EndInvoke_m80171137_gshared (Predicate_1_t1407909434 * __this, RuntimeObject* ___result0, const RuntimeMethod* method)
{
	RuntimeObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(bool*)UnBox ((RuntimeObject*)__result);
}
// System.Void System.Predicate`1<Vuforia.CameraDevice/CameraField>::.ctor(System.Object,System.IntPtr)
extern "C"  void Predicate_1__ctor_m159038208_gshared (Predicate_1_t3270418782 * __this, RuntimeObject * ___object0, IntPtr_t ___method1, const RuntimeMethod* method)
{
	__this->set_method_ptr_0(il2cpp_codegen_get_method_pointer((RuntimeMethod*)___method1.get_m_value_0()));
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Boolean System.Predicate`1<Vuforia.CameraDevice/CameraField>::Invoke(T)
extern "C"  bool Predicate_1_Invoke_m1773537808_gshared (Predicate_1_t3270418782 * __this, CameraField_t382214397  ___obj0, const RuntimeMethod* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Predicate_1_Invoke_m1773537808((Predicate_1_t3270418782 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (RuntimeObject *, void* __this, CameraField_t382214397  ___obj0, const RuntimeMethod* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___obj0,(RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef bool (*FunctionPointerType) (void* __this, CameraField_t382214397  ___obj0, const RuntimeMethod* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___obj0,(RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Predicate`1<Vuforia.CameraDevice/CameraField>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern "C"  RuntimeObject* Predicate_1_BeginInvoke_m1857455278_gshared (Predicate_1_t3270418782 * __this, CameraField_t382214397  ___obj0, AsyncCallback_t1057242265 * ___callback1, RuntimeObject * ___object2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Predicate_1_BeginInvoke_m1857455278_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(CameraField_t382214397_il2cpp_TypeInfo_var, &___obj0);
	return (RuntimeObject*)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (RuntimeObject*)___object2);
}
// System.Boolean System.Predicate`1<Vuforia.CameraDevice/CameraField>::EndInvoke(System.IAsyncResult)
extern "C"  bool Predicate_1_EndInvoke_m2093692673_gshared (Predicate_1_t3270418782 * __this, RuntimeObject* ___result0, const RuntimeMethod* method)
{
	RuntimeObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(bool*)UnBox ((RuntimeObject*)__result);
}
// System.Void System.Predicate`1<Vuforia.Image/PIXEL_FORMAT>::.ctor(System.Object,System.IntPtr)
extern "C"  void Predicate_1__ctor_m1380067748_gshared (Predicate_1_t178864225 * __this, RuntimeObject * ___object0, IntPtr_t ___method1, const RuntimeMethod* method)
{
	__this->set_method_ptr_0(il2cpp_codegen_get_method_pointer((RuntimeMethod*)___method1.get_m_value_0()));
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Boolean System.Predicate`1<Vuforia.Image/PIXEL_FORMAT>::Invoke(T)
extern "C"  bool Predicate_1_Invoke_m2525697755_gshared (Predicate_1_t178864225 * __this, int32_t ___obj0, const RuntimeMethod* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Predicate_1_Invoke_m2525697755((Predicate_1_t178864225 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (RuntimeObject *, void* __this, int32_t ___obj0, const RuntimeMethod* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___obj0,(RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef bool (*FunctionPointerType) (void* __this, int32_t ___obj0, const RuntimeMethod* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___obj0,(RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Predicate`1<Vuforia.Image/PIXEL_FORMAT>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern "C"  RuntimeObject* Predicate_1_BeginInvoke_m639343012_gshared (Predicate_1_t178864225 * __this, int32_t ___obj0, AsyncCallback_t1057242265 * ___callback1, RuntimeObject * ___object2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Predicate_1_BeginInvoke_m639343012_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(PIXEL_FORMAT_t1585627136_il2cpp_TypeInfo_var, &___obj0);
	return (RuntimeObject*)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (RuntimeObject*)___object2);
}
// System.Boolean System.Predicate`1<Vuforia.Image/PIXEL_FORMAT>::EndInvoke(System.IAsyncResult)
extern "C"  bool Predicate_1_EndInvoke_m3049006694_gshared (Predicate_1_t178864225 * __this, RuntimeObject* ___result0, const RuntimeMethod* method)
{
	RuntimeObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(bool*)UnBox ((RuntimeObject*)__result);
}
// System.Void System.Predicate`1<Vuforia.TargetFinder/TargetSearchResult>::.ctor(System.Object,System.IntPtr)
extern "C"  void Predicate_1__ctor_m3711544337_gshared (Predicate_1_t3101898792 * __this, RuntimeObject * ___object0, IntPtr_t ___method1, const RuntimeMethod* method)
{
	__this->set_method_ptr_0(il2cpp_codegen_get_method_pointer((RuntimeMethod*)___method1.get_m_value_0()));
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Boolean System.Predicate`1<Vuforia.TargetFinder/TargetSearchResult>::Invoke(T)
extern "C"  bool Predicate_1_Invoke_m1814129163_gshared (Predicate_1_t3101898792 * __this, TargetSearchResult_t213694407  ___obj0, const RuntimeMethod* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Predicate_1_Invoke_m1814129163((Predicate_1_t3101898792 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (RuntimeObject *, void* __this, TargetSearchResult_t213694407  ___obj0, const RuntimeMethod* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___obj0,(RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef bool (*FunctionPointerType) (void* __this, TargetSearchResult_t213694407  ___obj0, const RuntimeMethod* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___obj0,(RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Predicate`1<Vuforia.TargetFinder/TargetSearchResult>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern "C"  RuntimeObject* Predicate_1_BeginInvoke_m2594383812_gshared (Predicate_1_t3101898792 * __this, TargetSearchResult_t213694407  ___obj0, AsyncCallback_t1057242265 * ___callback1, RuntimeObject * ___object2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Predicate_1_BeginInvoke_m2594383812_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(TargetSearchResult_t213694407_il2cpp_TypeInfo_var, &___obj0);
	return (RuntimeObject*)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (RuntimeObject*)___object2);
}
// System.Boolean System.Predicate`1<Vuforia.TargetFinder/TargetSearchResult>::EndInvoke(System.IAsyncResult)
extern "C"  bool Predicate_1_EndInvoke_m9357172_gshared (Predicate_1_t3101898792 * __this, RuntimeObject* ___result0, const RuntimeMethod* method)
{
	RuntimeObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(bool*)UnBox ((RuntimeObject*)__result);
}
// System.Void System.Predicate`1<Vuforia.VuforiaManager/TrackableIdPair>::.ctor(System.Object,System.IntPtr)
extern "C"  void Predicate_1__ctor_m1960368074_gshared (Predicate_1_t4219631771 * __this, RuntimeObject * ___object0, IntPtr_t ___method1, const RuntimeMethod* method)
{
	__this->set_method_ptr_0(il2cpp_codegen_get_method_pointer((RuntimeMethod*)___method1.get_m_value_0()));
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Boolean System.Predicate`1<Vuforia.VuforiaManager/TrackableIdPair>::Invoke(T)
extern "C"  bool Predicate_1_Invoke_m1599171516_gshared (Predicate_1_t4219631771 * __this, TrackableIdPair_t1331427386  ___obj0, const RuntimeMethod* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Predicate_1_Invoke_m1599171516((Predicate_1_t4219631771 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (RuntimeObject *, void* __this, TrackableIdPair_t1331427386  ___obj0, const RuntimeMethod* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___obj0,(RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef bool (*FunctionPointerType) (void* __this, TrackableIdPair_t1331427386  ___obj0, const RuntimeMethod* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___obj0,(RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Predicate`1<Vuforia.VuforiaManager/TrackableIdPair>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern "C"  RuntimeObject* Predicate_1_BeginInvoke_m2506242557_gshared (Predicate_1_t4219631771 * __this, TrackableIdPair_t1331427386  ___obj0, AsyncCallback_t1057242265 * ___callback1, RuntimeObject * ___object2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Predicate_1_BeginInvoke_m2506242557_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(TrackableIdPair_t1331427386_il2cpp_TypeInfo_var, &___obj0);
	return (RuntimeObject*)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (RuntimeObject*)___object2);
}
// System.Boolean System.Predicate`1<Vuforia.VuforiaManager/TrackableIdPair>::EndInvoke(System.IAsyncResult)
extern "C"  bool Predicate_1_EndInvoke_m2184951363_gshared (Predicate_1_t4219631771 * __this, RuntimeObject* ___result0, const RuntimeMethod* method)
{
	RuntimeObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(bool*)UnBox ((RuntimeObject*)__result);
}
// System.Void System.Predicate`1<Vuforia.VuforiaManagerImpl/TrackableResultData>::.ctor(System.Object,System.IntPtr)
extern "C"  void Predicate_1__ctor_m2714586489_gshared (Predicate_1_t2688636542 * __this, RuntimeObject * ___object0, IntPtr_t ___method1, const RuntimeMethod* method)
{
	__this->set_method_ptr_0(il2cpp_codegen_get_method_pointer((RuntimeMethod*)___method1.get_m_value_0()));
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Boolean System.Predicate`1<Vuforia.VuforiaManagerImpl/TrackableResultData>::Invoke(T)
extern "C"  bool Predicate_1_Invoke_m3067916862_gshared (Predicate_1_t2688636542 * __this, TrackableResultData_t4095399453  ___obj0, const RuntimeMethod* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Predicate_1_Invoke_m3067916862((Predicate_1_t2688636542 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (RuntimeObject *, void* __this, TrackableResultData_t4095399453  ___obj0, const RuntimeMethod* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___obj0,(RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef bool (*FunctionPointerType) (void* __this, TrackableResultData_t4095399453  ___obj0, const RuntimeMethod* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___obj0,(RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Predicate`1<Vuforia.VuforiaManagerImpl/TrackableResultData>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern "C"  RuntimeObject* Predicate_1_BeginInvoke_m3239071508_gshared (Predicate_1_t2688636542 * __this, TrackableResultData_t4095399453  ___obj0, AsyncCallback_t1057242265 * ___callback1, RuntimeObject * ___object2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Predicate_1_BeginInvoke_m3239071508_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(TrackableResultData_t4095399453_il2cpp_TypeInfo_var, &___obj0);
	return (RuntimeObject*)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (RuntimeObject*)___object2);
}
// System.Boolean System.Predicate`1<Vuforia.VuforiaManagerImpl/TrackableResultData>::EndInvoke(System.IAsyncResult)
extern "C"  bool Predicate_1_EndInvoke_m2995947567_gshared (Predicate_1_t2688636542 * __this, RuntimeObject* ___result0, const RuntimeMethod* method)
{
	RuntimeObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(bool*)UnBox ((RuntimeObject*)__result);
}
// System.Void System.Predicate`1<Vuforia.VuforiaManagerImpl/VuMarkTargetData>::.ctor(System.Object,System.IntPtr)
extern "C"  void Predicate_1__ctor_m4085032143_gshared (Predicate_1_t1566583459 * __this, RuntimeObject * ___object0, IntPtr_t ___method1, const RuntimeMethod* method)
{
	__this->set_method_ptr_0(il2cpp_codegen_get_method_pointer((RuntimeMethod*)___method1.get_m_value_0()));
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Boolean System.Predicate`1<Vuforia.VuforiaManagerImpl/VuMarkTargetData>::Invoke(T)
extern "C"  bool Predicate_1_Invoke_m174242021_gshared (Predicate_1_t1566583459 * __this, VuMarkTargetData_t2973346370  ___obj0, const RuntimeMethod* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Predicate_1_Invoke_m174242021((Predicate_1_t1566583459 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (RuntimeObject *, void* __this, VuMarkTargetData_t2973346370  ___obj0, const RuntimeMethod* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___obj0,(RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef bool (*FunctionPointerType) (void* __this, VuMarkTargetData_t2973346370  ___obj0, const RuntimeMethod* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___obj0,(RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Predicate`1<Vuforia.VuforiaManagerImpl/VuMarkTargetData>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern "C"  RuntimeObject* Predicate_1_BeginInvoke_m1500370976_gshared (Predicate_1_t1566583459 * __this, VuMarkTargetData_t2973346370  ___obj0, AsyncCallback_t1057242265 * ___callback1, RuntimeObject * ___object2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Predicate_1_BeginInvoke_m1500370976_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(VuMarkTargetData_t2973346370_il2cpp_TypeInfo_var, &___obj0);
	return (RuntimeObject*)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (RuntimeObject*)___object2);
}
// System.Boolean System.Predicate`1<Vuforia.VuforiaManagerImpl/VuMarkTargetData>::EndInvoke(System.IAsyncResult)
extern "C"  bool Predicate_1_EndInvoke_m1587536889_gshared (Predicate_1_t1566583459 * __this, RuntimeObject* ___result0, const RuntimeMethod* method)
{
	RuntimeObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(bool*)UnBox ((RuntimeObject*)__result);
}
// System.Void System.Predicate`1<Vuforia.VuforiaManagerImpl/VuMarkTargetResultData>::.ctor(System.Object,System.IntPtr)
extern "C"  void Predicate_1__ctor_m1097198853_gshared (Predicate_1_t680208269 * __this, RuntimeObject * ___object0, IntPtr_t ___method1, const RuntimeMethod* method)
{
	__this->set_method_ptr_0(il2cpp_codegen_get_method_pointer((RuntimeMethod*)___method1.get_m_value_0()));
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Boolean System.Predicate`1<Vuforia.VuforiaManagerImpl/VuMarkTargetResultData>::Invoke(T)
extern "C"  bool Predicate_1_Invoke_m2759091423_gshared (Predicate_1_t680208269 * __this, VuMarkTargetResultData_t2086971180  ___obj0, const RuntimeMethod* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Predicate_1_Invoke_m2759091423((Predicate_1_t680208269 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (RuntimeObject *, void* __this, VuMarkTargetResultData_t2086971180  ___obj0, const RuntimeMethod* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___obj0,(RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef bool (*FunctionPointerType) (void* __this, VuMarkTargetResultData_t2086971180  ___obj0, const RuntimeMethod* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___obj0,(RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Predicate`1<Vuforia.VuforiaManagerImpl/VuMarkTargetResultData>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern "C"  RuntimeObject* Predicate_1_BeginInvoke_m370506624_gshared (Predicate_1_t680208269 * __this, VuMarkTargetResultData_t2086971180  ___obj0, AsyncCallback_t1057242265 * ___callback1, RuntimeObject * ___object2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Predicate_1_BeginInvoke_m370506624_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(VuMarkTargetResultData_t2086971180_il2cpp_TypeInfo_var, &___obj0);
	return (RuntimeObject*)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (RuntimeObject*)___object2);
}
// System.Boolean System.Predicate`1<Vuforia.VuforiaManagerImpl/VuMarkTargetResultData>::EndInvoke(System.IAsyncResult)
extern "C"  bool Predicate_1_EndInvoke_m1947272148_gshared (Predicate_1_t680208269 * __this, RuntimeObject* ___result0, const RuntimeMethod* method)
{
	RuntimeObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(bool*)UnBox ((RuntimeObject*)__result);
}
// System.Void System.Reflection.MonoProperty/Getter`2<System.Object,System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void Getter_2__ctor_m392206493_gshared (Getter_2_t1437382685 * __this, RuntimeObject * ___object0, IntPtr_t ___method1, const RuntimeMethod* method)
{
	__this->set_method_ptr_0(il2cpp_codegen_get_method_pointer((RuntimeMethod*)___method1.get_m_value_0()));
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// R System.Reflection.MonoProperty/Getter`2<System.Object,System.Object>::Invoke(T)
extern "C"  RuntimeObject * Getter_2_Invoke_m861197390_gshared (Getter_2_t1437382685 * __this, RuntimeObject * ____this0, const RuntimeMethod* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Getter_2_Invoke_m861197390((Getter_2_t1437382685 *)__this->get_prev_9(),____this0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef RuntimeObject * (*FunctionPointerType) (RuntimeObject *, void* __this, RuntimeObject * ____this0, const RuntimeMethod* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),____this0,(RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef RuntimeObject * (*FunctionPointerType) (void* __this, RuntimeObject * ____this0, const RuntimeMethod* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),____this0,(RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef RuntimeObject * (*FunctionPointerType) (void* __this, const RuntimeMethod* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(____this0,(RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Reflection.MonoProperty/Getter`2<System.Object,System.Object>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern "C"  RuntimeObject* Getter_2_BeginInvoke_m1904262177_gshared (Getter_2_t1437382685 * __this, RuntimeObject * ____this0, AsyncCallback_t1057242265 * ___callback1, RuntimeObject * ___object2, const RuntimeMethod* method)
{
	void *__d_args[2] = {0};
	__d_args[0] = ____this0;
	return (RuntimeObject*)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (RuntimeObject*)___object2);
}
// R System.Reflection.MonoProperty/Getter`2<System.Object,System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  RuntimeObject * Getter_2_EndInvoke_m1009674532_gshared (Getter_2_t1437382685 * __this, RuntimeObject* ___result0, const RuntimeMethod* method)
{
	RuntimeObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return (RuntimeObject *)__result;
}
// System.Void System.Reflection.MonoProperty/StaticGetter`1<System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void StaticGetter_1__ctor_m13203901_gshared (StaticGetter_1_t2059077758 * __this, RuntimeObject * ___object0, IntPtr_t ___method1, const RuntimeMethod* method)
{
	__this->set_method_ptr_0(il2cpp_codegen_get_method_pointer((RuntimeMethod*)___method1.get_m_value_0()));
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// R System.Reflection.MonoProperty/StaticGetter`1<System.Object>::Invoke()
extern "C"  RuntimeObject * StaticGetter_1_Invoke_m497351595_gshared (StaticGetter_1_t2059077758 * __this, const RuntimeMethod* method)
{
	if(__this->get_prev_9() != NULL)
	{
		StaticGetter_1_Invoke_m497351595((StaticGetter_1_t2059077758 *)__this->get_prev_9(), method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	if ((__this->get_m_target_2() != NULL || MethodHasParameters((RuntimeMethod*)(__this->get_method_3().get_m_value_0()))) && ___methodIsStatic)
	{
		typedef RuntimeObject * (*FunctionPointerType) (RuntimeObject *, void* __this, const RuntimeMethod* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),(RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef RuntimeObject * (*FunctionPointerType) (void* __this, const RuntimeMethod* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),(RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Reflection.MonoProperty/StaticGetter`1<System.Object>::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  RuntimeObject* StaticGetter_1_BeginInvoke_m3966435007_gshared (StaticGetter_1_t2059077758 * __this, AsyncCallback_t1057242265 * ___callback0, RuntimeObject * ___object1, const RuntimeMethod* method)
{
	void *__d_args[1] = {0};
	return (RuntimeObject*)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback0, (RuntimeObject*)___object1);
}
// R System.Reflection.MonoProperty/StaticGetter`1<System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  RuntimeObject * StaticGetter_1_EndInvoke_m938003237_gshared (StaticGetter_1_t2059077758 * __this, RuntimeObject* ___result0, const RuntimeMethod* method)
{
	RuntimeObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return (RuntimeObject *)__result;
}
// System.Void UnityEngine.Events.CachedInvokableCall`1<System.Boolean>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
extern "C"  void CachedInvokableCall_1__ctor_m1968817514_gshared (CachedInvokableCall_1_t1235820086 * __this, Object_t1502412432 * ___target0, MethodInfo_t * ___theFunction1, bool ___argument2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CachedInvokableCall_1__ctor_m1968817514_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set_m_Arg1_1(((ObjectU5BU5D_t3270211303*)SZArrayNew(ObjectU5BU5D_t3270211303_il2cpp_TypeInfo_var, (uint32_t)1)));
		Object_t1502412432 * L_0 = ___target0;
		MethodInfo_t * L_1 = ___theFunction1;
		NullCheck((InvokableCall_1_t1183494759 *)__this);
		((  void (*) (InvokableCall_1_t1183494759 *, RuntimeObject *, MethodInfo_t *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((InvokableCall_1_t1183494759 *)__this, (RuntimeObject *)L_0, (MethodInfo_t *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		ObjectU5BU5D_t3270211303* L_2 = (ObjectU5BU5D_t3270211303*)__this->get_m_Arg1_1();
		bool L_3 = ___argument2;
		bool L_4 = L_3;
		RuntimeObject * L_5 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_4);
		NullCheck(L_2);
		ArrayElementTypeCheck (L_2, L_5);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(0), (RuntimeObject *)L_5);
		return;
	}
}
// System.Void UnityEngine.Events.CachedInvokableCall`1<System.Boolean>::Invoke(System.Object[])
extern "C"  void CachedInvokableCall_1_Invoke_m1651178765_gshared (CachedInvokableCall_1_t1235820086 * __this, ObjectU5BU5D_t3270211303* ___args0, const RuntimeMethod* method)
{
	{
		ObjectU5BU5D_t3270211303* L_0 = (ObjectU5BU5D_t3270211303*)__this->get_m_Arg1_1();
		NullCheck((InvokableCall_1_t1183494759 *)__this);
		((  void (*) (InvokableCall_1_t1183494759 *, ObjectU5BU5D_t3270211303*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)((InvokableCall_1_t1183494759 *)__this, (ObjectU5BU5D_t3270211303*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		return;
	}
}
// System.Void UnityEngine.Events.CachedInvokableCall`1<System.Int32>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
extern "C"  void CachedInvokableCall_1__ctor_m647347569_gshared (CachedInvokableCall_1_t2522919606 * __this, Object_t1502412432 * ___target0, MethodInfo_t * ___theFunction1, int32_t ___argument2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CachedInvokableCall_1__ctor_m647347569_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set_m_Arg1_1(((ObjectU5BU5D_t3270211303*)SZArrayNew(ObjectU5BU5D_t3270211303_il2cpp_TypeInfo_var, (uint32_t)1)));
		Object_t1502412432 * L_0 = ___target0;
		MethodInfo_t * L_1 = ___theFunction1;
		NullCheck((InvokableCall_1_t2470594279 *)__this);
		((  void (*) (InvokableCall_1_t2470594279 *, RuntimeObject *, MethodInfo_t *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((InvokableCall_1_t2470594279 *)__this, (RuntimeObject *)L_0, (MethodInfo_t *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		ObjectU5BU5D_t3270211303* L_2 = (ObjectU5BU5D_t3270211303*)__this->get_m_Arg1_1();
		int32_t L_3 = ___argument2;
		int32_t L_4 = L_3;
		RuntimeObject * L_5 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_4);
		NullCheck(L_2);
		ArrayElementTypeCheck (L_2, L_5);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(0), (RuntimeObject *)L_5);
		return;
	}
}
// System.Void UnityEngine.Events.CachedInvokableCall`1<System.Int32>::Invoke(System.Object[])
extern "C"  void CachedInvokableCall_1_Invoke_m1967892773_gshared (CachedInvokableCall_1_t2522919606 * __this, ObjectU5BU5D_t3270211303* ___args0, const RuntimeMethod* method)
{
	{
		ObjectU5BU5D_t3270211303* L_0 = (ObjectU5BU5D_t3270211303*)__this->get_m_Arg1_1();
		NullCheck((InvokableCall_1_t2470594279 *)__this);
		((  void (*) (InvokableCall_1_t2470594279 *, ObjectU5BU5D_t3270211303*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)((InvokableCall_1_t2470594279 *)__this, (ObjectU5BU5D_t3270211303*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		return;
	}
}
// System.Void UnityEngine.Events.CachedInvokableCall`1<System.Object>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
extern "C"  void CachedInvokableCall_1__ctor_m1679835505_gshared (CachedInvokableCall_1_t306933307 * __this, Object_t1502412432 * ___target0, MethodInfo_t * ___theFunction1, RuntimeObject * ___argument2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CachedInvokableCall_1__ctor_m1679835505_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set_m_Arg1_1(((ObjectU5BU5D_t3270211303*)SZArrayNew(ObjectU5BU5D_t3270211303_il2cpp_TypeInfo_var, (uint32_t)1)));
		Object_t1502412432 * L_0 = ___target0;
		MethodInfo_t * L_1 = ___theFunction1;
		NullCheck((InvokableCall_1_t254607980 *)__this);
		((  void (*) (InvokableCall_1_t254607980 *, RuntimeObject *, MethodInfo_t *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((InvokableCall_1_t254607980 *)__this, (RuntimeObject *)L_0, (MethodInfo_t *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		ObjectU5BU5D_t3270211303* L_2 = (ObjectU5BU5D_t3270211303*)__this->get_m_Arg1_1();
		RuntimeObject * L_3 = ___argument2;
		NullCheck(L_2);
		ArrayElementTypeCheck (L_2, L_3);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(0), (RuntimeObject *)L_3);
		return;
	}
}
// System.Void UnityEngine.Events.CachedInvokableCall`1<System.Object>::Invoke(System.Object[])
extern "C"  void CachedInvokableCall_1_Invoke_m2383493770_gshared (CachedInvokableCall_1_t306933307 * __this, ObjectU5BU5D_t3270211303* ___args0, const RuntimeMethod* method)
{
	{
		ObjectU5BU5D_t3270211303* L_0 = (ObjectU5BU5D_t3270211303*)__this->get_m_Arg1_1();
		NullCheck((InvokableCall_1_t254607980 *)__this);
		((  void (*) (InvokableCall_1_t254607980 *, ObjectU5BU5D_t3270211303*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)((InvokableCall_1_t254607980 *)__this, (ObjectU5BU5D_t3270211303*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		return;
	}
}
// System.Void UnityEngine.Events.CachedInvokableCall`1<System.Single>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
extern "C"  void CachedInvokableCall_1__ctor_m1829863914_gshared (CachedInvokableCall_1_t1330324776 * __this, Object_t1502412432 * ___target0, MethodInfo_t * ___theFunction1, float ___argument2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CachedInvokableCall_1__ctor_m1829863914_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set_m_Arg1_1(((ObjectU5BU5D_t3270211303*)SZArrayNew(ObjectU5BU5D_t3270211303_il2cpp_TypeInfo_var, (uint32_t)1)));
		Object_t1502412432 * L_0 = ___target0;
		MethodInfo_t * L_1 = ___theFunction1;
		NullCheck((InvokableCall_1_t1277999449 *)__this);
		((  void (*) (InvokableCall_1_t1277999449 *, RuntimeObject *, MethodInfo_t *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((InvokableCall_1_t1277999449 *)__this, (RuntimeObject *)L_0, (MethodInfo_t *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		ObjectU5BU5D_t3270211303* L_2 = (ObjectU5BU5D_t3270211303*)__this->get_m_Arg1_1();
		float L_3 = ___argument2;
		float L_4 = L_3;
		RuntimeObject * L_5 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_4);
		NullCheck(L_2);
		ArrayElementTypeCheck (L_2, L_5);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(0), (RuntimeObject *)L_5);
		return;
	}
}
// System.Void UnityEngine.Events.CachedInvokableCall`1<System.Single>::Invoke(System.Object[])
extern "C"  void CachedInvokableCall_1_Invoke_m1311209760_gshared (CachedInvokableCall_1_t1330324776 * __this, ObjectU5BU5D_t3270211303* ___args0, const RuntimeMethod* method)
{
	{
		ObjectU5BU5D_t3270211303* L_0 = (ObjectU5BU5D_t3270211303*)__this->get_m_Arg1_1();
		NullCheck((InvokableCall_1_t1277999449 *)__this);
		((  void (*) (InvokableCall_1_t1277999449 *, ObjectU5BU5D_t3270211303*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)((InvokableCall_1_t1277999449 *)__this, (ObjectU5BU5D_t3270211303*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCall`1<System.Boolean>::.ctor(System.Object,System.Reflection.MethodInfo)
extern "C"  void InvokableCall_1__ctor_m2576442280_gshared (InvokableCall_1_t1183494759 * __this, RuntimeObject * ___target0, MethodInfo_t * ___theFunction1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InvokableCall_1__ctor_m2576442280_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		RuntimeObject * L_0 = ___target0;
		MethodInfo_t * L_1 = ___theFunction1;
		NullCheck((BaseInvokableCall_t1352930348 *)__this);
		BaseInvokableCall__ctor_m2708238577((BaseInvokableCall_t1352930348 *)__this, (RuntimeObject *)L_0, (MethodInfo_t *)L_1, /*hidden argument*/NULL);
		MethodInfo_t * L_2 = ___theFunction1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m269683661(NULL /*static, unused*/, (RuntimeTypeHandle_t845633806 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 0)), /*hidden argument*/NULL);
		RuntimeObject * L_4 = ___target0;
		Delegate_t537306192 * L_5 = NetFxCoreExtensions_CreateDelegate_m4287214728(NULL /*static, unused*/, (MethodInfo_t *)L_2, (Type_t *)L_3, (RuntimeObject *)L_4, /*hidden argument*/NULL);
		NullCheck((InvokableCall_1_t1183494759 *)__this);
		((  void (*) (InvokableCall_1_t1183494759 *, UnityAction_1_t2203695861 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)((InvokableCall_1_t1183494759 *)__this, (UnityAction_1_t2203695861 *)((UnityAction_1_t2203695861 *)Castclass((RuntimeObject*)L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCall`1<System.Boolean>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
extern "C"  void InvokableCall_1__ctor_m3460933065_gshared (InvokableCall_1_t1183494759 * __this, UnityAction_1_t2203695861 * ___action0, const RuntimeMethod* method)
{
	{
		NullCheck((BaseInvokableCall_t1352930348 *)__this);
		BaseInvokableCall__ctor_m1099533435((BaseInvokableCall_t1352930348 *)__this, /*hidden argument*/NULL);
		UnityAction_1_t2203695861 * L_0 = ___action0;
		NullCheck((InvokableCall_1_t1183494759 *)__this);
		((  void (*) (InvokableCall_1_t1183494759 *, UnityAction_1_t2203695861 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)((InvokableCall_1_t1183494759 *)__this, (UnityAction_1_t2203695861 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCall`1<System.Boolean>::add_Delegate(UnityEngine.Events.UnityAction`1<T1>)
extern "C"  void InvokableCall_1_add_Delegate_m3870792113_gshared (InvokableCall_1_t1183494759 * __this, UnityAction_1_t2203695861 * ___value0, const RuntimeMethod* method)
{
	UnityAction_1_t2203695861 * V_0 = NULL;
	UnityAction_1_t2203695861 * V_1 = NULL;
	{
		UnityAction_1_t2203695861 * L_0 = (UnityAction_1_t2203695861 *)__this->get_Delegate_0();
		V_0 = (UnityAction_1_t2203695861 *)L_0;
	}

IL_0007:
	{
		UnityAction_1_t2203695861 * L_1 = V_0;
		V_1 = (UnityAction_1_t2203695861 *)L_1;
		UnityAction_1_t2203695861 ** L_2 = (UnityAction_1_t2203695861 **)__this->get_address_of_Delegate_0();
		UnityAction_1_t2203695861 * L_3 = V_1;
		UnityAction_1_t2203695861 * L_4 = ___value0;
		Delegate_t537306192 * L_5 = Delegate_Combine_m408928526(NULL /*static, unused*/, (Delegate_t537306192 *)L_3, (Delegate_t537306192 *)L_4, /*hidden argument*/NULL);
		UnityAction_1_t2203695861 * L_6 = V_0;
		UnityAction_1_t2203695861 * L_7 = InterlockedCompareExchangeImpl<UnityAction_1_t2203695861 *>((UnityAction_1_t2203695861 **)L_2, (UnityAction_1_t2203695861 *)((UnityAction_1_t2203695861 *)Castclass((RuntimeObject*)L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1))), (UnityAction_1_t2203695861 *)L_6);
		V_0 = (UnityAction_1_t2203695861 *)L_7;
		UnityAction_1_t2203695861 * L_8 = V_0;
		UnityAction_1_t2203695861 * L_9 = V_1;
		if ((!(((RuntimeObject*)(UnityAction_1_t2203695861 *)L_8) == ((RuntimeObject*)(UnityAction_1_t2203695861 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCall`1<System.Boolean>::remove_Delegate(UnityEngine.Events.UnityAction`1<T1>)
extern "C"  void InvokableCall_1_remove_Delegate_m4045560170_gshared (InvokableCall_1_t1183494759 * __this, UnityAction_1_t2203695861 * ___value0, const RuntimeMethod* method)
{
	UnityAction_1_t2203695861 * V_0 = NULL;
	UnityAction_1_t2203695861 * V_1 = NULL;
	{
		UnityAction_1_t2203695861 * L_0 = (UnityAction_1_t2203695861 *)__this->get_Delegate_0();
		V_0 = (UnityAction_1_t2203695861 *)L_0;
	}

IL_0007:
	{
		UnityAction_1_t2203695861 * L_1 = V_0;
		V_1 = (UnityAction_1_t2203695861 *)L_1;
		UnityAction_1_t2203695861 ** L_2 = (UnityAction_1_t2203695861 **)__this->get_address_of_Delegate_0();
		UnityAction_1_t2203695861 * L_3 = V_1;
		UnityAction_1_t2203695861 * L_4 = ___value0;
		Delegate_t537306192 * L_5 = Delegate_Remove_m2220107885(NULL /*static, unused*/, (Delegate_t537306192 *)L_3, (Delegate_t537306192 *)L_4, /*hidden argument*/NULL);
		UnityAction_1_t2203695861 * L_6 = V_0;
		UnityAction_1_t2203695861 * L_7 = InterlockedCompareExchangeImpl<UnityAction_1_t2203695861 *>((UnityAction_1_t2203695861 **)L_2, (UnityAction_1_t2203695861 *)((UnityAction_1_t2203695861 *)Castclass((RuntimeObject*)L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1))), (UnityAction_1_t2203695861 *)L_6);
		V_0 = (UnityAction_1_t2203695861 *)L_7;
		UnityAction_1_t2203695861 * L_8 = V_0;
		UnityAction_1_t2203695861 * L_9 = V_1;
		if ((!(((RuntimeObject*)(UnityAction_1_t2203695861 *)L_8) == ((RuntimeObject*)(UnityAction_1_t2203695861 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCall`1<System.Boolean>::Invoke(System.Object[])
extern "C"  void InvokableCall_1_Invoke_m1308880592_gshared (InvokableCall_1_t1183494759 * __this, ObjectU5BU5D_t3270211303* ___args0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InvokableCall_1_Invoke_m1308880592_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		ObjectU5BU5D_t3270211303* L_0 = ___args0;
		NullCheck(L_0);
		if ((((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_0)->max_length))))) == ((int32_t)1)))
		{
			goto IL_0015;
		}
	}
	{
		ArgumentException_t4028401650 * L_1 = (ArgumentException_t4028401650 *)il2cpp_codegen_object_new(ArgumentException_t4028401650_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m1186658332(L_1, (String_t*)_stringLiteral836720390, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0015:
	{
		ObjectU5BU5D_t3270211303* L_2 = ___args0;
		NullCheck(L_2);
		int32_t L_3 = 0;
		RuntimeObject * L_4 = (L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_3));
		((  void (*) (RuntimeObject * /* static, unused */, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)(NULL /*static, unused*/, (RuntimeObject *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		UnityAction_1_t2203695861 * L_5 = (UnityAction_1_t2203695861 *)__this->get_Delegate_0();
		bool L_6 = BaseInvokableCall_AllowInvoke_m3827668627(NULL /*static, unused*/, (Delegate_t537306192 *)L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0040;
		}
	}
	{
		UnityAction_1_t2203695861 * L_7 = (UnityAction_1_t2203695861 *)__this->get_Delegate_0();
		ObjectU5BU5D_t3270211303* L_8 = ___args0;
		NullCheck(L_8);
		int32_t L_9 = 0;
		RuntimeObject * L_10 = (L_8)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		NullCheck((UnityAction_1_t2203695861 *)L_7);
		((  void (*) (UnityAction_1_t2203695861 *, bool, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((UnityAction_1_t2203695861 *)L_7, (bool)((*(bool*)((bool*)UnBox(L_10, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5))))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
	}

IL_0040:
	{
		return;
	}
}
// System.Boolean UnityEngine.Events.InvokableCall`1<System.Boolean>::Find(System.Object,System.Reflection.MethodInfo)
extern "C"  bool InvokableCall_1_Find_m3175839680_gshared (InvokableCall_1_t1183494759 * __this, RuntimeObject * ___targetObj0, MethodInfo_t * ___method1, const RuntimeMethod* method)
{
	bool V_0 = false;
	int32_t G_B3_0 = 0;
	{
		UnityAction_1_t2203695861 * L_0 = (UnityAction_1_t2203695861 *)__this->get_Delegate_0();
		NullCheck((Delegate_t537306192 *)L_0);
		RuntimeObject * L_1 = Delegate_get_Target_m2013817205((Delegate_t537306192 *)L_0, /*hidden argument*/NULL);
		RuntimeObject * L_2 = ___targetObj0;
		if ((!(((RuntimeObject*)(RuntimeObject *)L_1) == ((RuntimeObject*)(RuntimeObject *)L_2))))
		{
			goto IL_0025;
		}
	}
	{
		UnityAction_1_t2203695861 * L_3 = (UnityAction_1_t2203695861 *)__this->get_Delegate_0();
		MethodInfo_t * L_4 = NetFxCoreExtensions_GetMethodInfo_m1250622137(NULL /*static, unused*/, (Delegate_t537306192 *)L_3, /*hidden argument*/NULL);
		MethodInfo_t * L_5 = ___method1;
		NullCheck((RuntimeObject *)L_4);
		bool L_6 = VirtFuncInvoker1< bool, RuntimeObject * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, (RuntimeObject *)L_4, (RuntimeObject *)L_5);
		G_B3_0 = ((int32_t)(L_6));
		goto IL_0026;
	}

IL_0025:
	{
		G_B3_0 = 0;
	}

IL_0026:
	{
		V_0 = (bool)G_B3_0;
		goto IL_002c;
	}

IL_002c:
	{
		bool L_7 = V_0;
		return L_7;
	}
}
// System.Void UnityEngine.Events.InvokableCall`1<System.Int32>::.ctor(System.Object,System.Reflection.MethodInfo)
extern "C"  void InvokableCall_1__ctor_m1704911211_gshared (InvokableCall_1_t2470594279 * __this, RuntimeObject * ___target0, MethodInfo_t * ___theFunction1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InvokableCall_1__ctor_m1704911211_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		RuntimeObject * L_0 = ___target0;
		MethodInfo_t * L_1 = ___theFunction1;
		NullCheck((BaseInvokableCall_t1352930348 *)__this);
		BaseInvokableCall__ctor_m2708238577((BaseInvokableCall_t1352930348 *)__this, (RuntimeObject *)L_0, (MethodInfo_t *)L_1, /*hidden argument*/NULL);
		MethodInfo_t * L_2 = ___theFunction1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m269683661(NULL /*static, unused*/, (RuntimeTypeHandle_t845633806 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 0)), /*hidden argument*/NULL);
		RuntimeObject * L_4 = ___target0;
		Delegate_t537306192 * L_5 = NetFxCoreExtensions_CreateDelegate_m4287214728(NULL /*static, unused*/, (MethodInfo_t *)L_2, (Type_t *)L_3, (RuntimeObject *)L_4, /*hidden argument*/NULL);
		NullCheck((InvokableCall_1_t2470594279 *)__this);
		((  void (*) (InvokableCall_1_t2470594279 *, UnityAction_1_t3490795381 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)((InvokableCall_1_t2470594279 *)__this, (UnityAction_1_t3490795381 *)((UnityAction_1_t3490795381 *)Castclass((RuntimeObject*)L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCall`1<System.Int32>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
extern "C"  void InvokableCall_1__ctor_m1091267002_gshared (InvokableCall_1_t2470594279 * __this, UnityAction_1_t3490795381 * ___action0, const RuntimeMethod* method)
{
	{
		NullCheck((BaseInvokableCall_t1352930348 *)__this);
		BaseInvokableCall__ctor_m1099533435((BaseInvokableCall_t1352930348 *)__this, /*hidden argument*/NULL);
		UnityAction_1_t3490795381 * L_0 = ___action0;
		NullCheck((InvokableCall_1_t2470594279 *)__this);
		((  void (*) (InvokableCall_1_t2470594279 *, UnityAction_1_t3490795381 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)((InvokableCall_1_t2470594279 *)__this, (UnityAction_1_t3490795381 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCall`1<System.Int32>::add_Delegate(UnityEngine.Events.UnityAction`1<T1>)
extern "C"  void InvokableCall_1_add_Delegate_m1640822335_gshared (InvokableCall_1_t2470594279 * __this, UnityAction_1_t3490795381 * ___value0, const RuntimeMethod* method)
{
	UnityAction_1_t3490795381 * V_0 = NULL;
	UnityAction_1_t3490795381 * V_1 = NULL;
	{
		UnityAction_1_t3490795381 * L_0 = (UnityAction_1_t3490795381 *)__this->get_Delegate_0();
		V_0 = (UnityAction_1_t3490795381 *)L_0;
	}

IL_0007:
	{
		UnityAction_1_t3490795381 * L_1 = V_0;
		V_1 = (UnityAction_1_t3490795381 *)L_1;
		UnityAction_1_t3490795381 ** L_2 = (UnityAction_1_t3490795381 **)__this->get_address_of_Delegate_0();
		UnityAction_1_t3490795381 * L_3 = V_1;
		UnityAction_1_t3490795381 * L_4 = ___value0;
		Delegate_t537306192 * L_5 = Delegate_Combine_m408928526(NULL /*static, unused*/, (Delegate_t537306192 *)L_3, (Delegate_t537306192 *)L_4, /*hidden argument*/NULL);
		UnityAction_1_t3490795381 * L_6 = V_0;
		UnityAction_1_t3490795381 * L_7 = InterlockedCompareExchangeImpl<UnityAction_1_t3490795381 *>((UnityAction_1_t3490795381 **)L_2, (UnityAction_1_t3490795381 *)((UnityAction_1_t3490795381 *)Castclass((RuntimeObject*)L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1))), (UnityAction_1_t3490795381 *)L_6);
		V_0 = (UnityAction_1_t3490795381 *)L_7;
		UnityAction_1_t3490795381 * L_8 = V_0;
		UnityAction_1_t3490795381 * L_9 = V_1;
		if ((!(((RuntimeObject*)(UnityAction_1_t3490795381 *)L_8) == ((RuntimeObject*)(UnityAction_1_t3490795381 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCall`1<System.Int32>::remove_Delegate(UnityEngine.Events.UnityAction`1<T1>)
extern "C"  void InvokableCall_1_remove_Delegate_m3447808876_gshared (InvokableCall_1_t2470594279 * __this, UnityAction_1_t3490795381 * ___value0, const RuntimeMethod* method)
{
	UnityAction_1_t3490795381 * V_0 = NULL;
	UnityAction_1_t3490795381 * V_1 = NULL;
	{
		UnityAction_1_t3490795381 * L_0 = (UnityAction_1_t3490795381 *)__this->get_Delegate_0();
		V_0 = (UnityAction_1_t3490795381 *)L_0;
	}

IL_0007:
	{
		UnityAction_1_t3490795381 * L_1 = V_0;
		V_1 = (UnityAction_1_t3490795381 *)L_1;
		UnityAction_1_t3490795381 ** L_2 = (UnityAction_1_t3490795381 **)__this->get_address_of_Delegate_0();
		UnityAction_1_t3490795381 * L_3 = V_1;
		UnityAction_1_t3490795381 * L_4 = ___value0;
		Delegate_t537306192 * L_5 = Delegate_Remove_m2220107885(NULL /*static, unused*/, (Delegate_t537306192 *)L_3, (Delegate_t537306192 *)L_4, /*hidden argument*/NULL);
		UnityAction_1_t3490795381 * L_6 = V_0;
		UnityAction_1_t3490795381 * L_7 = InterlockedCompareExchangeImpl<UnityAction_1_t3490795381 *>((UnityAction_1_t3490795381 **)L_2, (UnityAction_1_t3490795381 *)((UnityAction_1_t3490795381 *)Castclass((RuntimeObject*)L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1))), (UnityAction_1_t3490795381 *)L_6);
		V_0 = (UnityAction_1_t3490795381 *)L_7;
		UnityAction_1_t3490795381 * L_8 = V_0;
		UnityAction_1_t3490795381 * L_9 = V_1;
		if ((!(((RuntimeObject*)(UnityAction_1_t3490795381 *)L_8) == ((RuntimeObject*)(UnityAction_1_t3490795381 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCall`1<System.Int32>::Invoke(System.Object[])
extern "C"  void InvokableCall_1_Invoke_m2675525977_gshared (InvokableCall_1_t2470594279 * __this, ObjectU5BU5D_t3270211303* ___args0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InvokableCall_1_Invoke_m2675525977_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		ObjectU5BU5D_t3270211303* L_0 = ___args0;
		NullCheck(L_0);
		if ((((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_0)->max_length))))) == ((int32_t)1)))
		{
			goto IL_0015;
		}
	}
	{
		ArgumentException_t4028401650 * L_1 = (ArgumentException_t4028401650 *)il2cpp_codegen_object_new(ArgumentException_t4028401650_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m1186658332(L_1, (String_t*)_stringLiteral836720390, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0015:
	{
		ObjectU5BU5D_t3270211303* L_2 = ___args0;
		NullCheck(L_2);
		int32_t L_3 = 0;
		RuntimeObject * L_4 = (L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_3));
		((  void (*) (RuntimeObject * /* static, unused */, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)(NULL /*static, unused*/, (RuntimeObject *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		UnityAction_1_t3490795381 * L_5 = (UnityAction_1_t3490795381 *)__this->get_Delegate_0();
		bool L_6 = BaseInvokableCall_AllowInvoke_m3827668627(NULL /*static, unused*/, (Delegate_t537306192 *)L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0040;
		}
	}
	{
		UnityAction_1_t3490795381 * L_7 = (UnityAction_1_t3490795381 *)__this->get_Delegate_0();
		ObjectU5BU5D_t3270211303* L_8 = ___args0;
		NullCheck(L_8);
		int32_t L_9 = 0;
		RuntimeObject * L_10 = (L_8)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		NullCheck((UnityAction_1_t3490795381 *)L_7);
		((  void (*) (UnityAction_1_t3490795381 *, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((UnityAction_1_t3490795381 *)L_7, (int32_t)((*(int32_t*)((int32_t*)UnBox(L_10, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5))))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
	}

IL_0040:
	{
		return;
	}
}
// System.Boolean UnityEngine.Events.InvokableCall`1<System.Int32>::Find(System.Object,System.Reflection.MethodInfo)
extern "C"  bool InvokableCall_1_Find_m217624119_gshared (InvokableCall_1_t2470594279 * __this, RuntimeObject * ___targetObj0, MethodInfo_t * ___method1, const RuntimeMethod* method)
{
	bool V_0 = false;
	int32_t G_B3_0 = 0;
	{
		UnityAction_1_t3490795381 * L_0 = (UnityAction_1_t3490795381 *)__this->get_Delegate_0();
		NullCheck((Delegate_t537306192 *)L_0);
		RuntimeObject * L_1 = Delegate_get_Target_m2013817205((Delegate_t537306192 *)L_0, /*hidden argument*/NULL);
		RuntimeObject * L_2 = ___targetObj0;
		if ((!(((RuntimeObject*)(RuntimeObject *)L_1) == ((RuntimeObject*)(RuntimeObject *)L_2))))
		{
			goto IL_0025;
		}
	}
	{
		UnityAction_1_t3490795381 * L_3 = (UnityAction_1_t3490795381 *)__this->get_Delegate_0();
		MethodInfo_t * L_4 = NetFxCoreExtensions_GetMethodInfo_m1250622137(NULL /*static, unused*/, (Delegate_t537306192 *)L_3, /*hidden argument*/NULL);
		MethodInfo_t * L_5 = ___method1;
		NullCheck((RuntimeObject *)L_4);
		bool L_6 = VirtFuncInvoker1< bool, RuntimeObject * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, (RuntimeObject *)L_4, (RuntimeObject *)L_5);
		G_B3_0 = ((int32_t)(L_6));
		goto IL_0026;
	}

IL_0025:
	{
		G_B3_0 = 0;
	}

IL_0026:
	{
		V_0 = (bool)G_B3_0;
		goto IL_002c;
	}

IL_002c:
	{
		bool L_7 = V_0;
		return L_7;
	}
}
// System.Void UnityEngine.Events.InvokableCall`1<System.Object>::.ctor(System.Object,System.Reflection.MethodInfo)
extern "C"  void InvokableCall_1__ctor_m3536943171_gshared (InvokableCall_1_t254607980 * __this, RuntimeObject * ___target0, MethodInfo_t * ___theFunction1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InvokableCall_1__ctor_m3536943171_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		RuntimeObject * L_0 = ___target0;
		MethodInfo_t * L_1 = ___theFunction1;
		NullCheck((BaseInvokableCall_t1352930348 *)__this);
		BaseInvokableCall__ctor_m2708238577((BaseInvokableCall_t1352930348 *)__this, (RuntimeObject *)L_0, (MethodInfo_t *)L_1, /*hidden argument*/NULL);
		MethodInfo_t * L_2 = ___theFunction1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m269683661(NULL /*static, unused*/, (RuntimeTypeHandle_t845633806 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 0)), /*hidden argument*/NULL);
		RuntimeObject * L_4 = ___target0;
		Delegate_t537306192 * L_5 = NetFxCoreExtensions_CreateDelegate_m4287214728(NULL /*static, unused*/, (MethodInfo_t *)L_2, (Type_t *)L_3, (RuntimeObject *)L_4, /*hidden argument*/NULL);
		NullCheck((InvokableCall_1_t254607980 *)__this);
		((  void (*) (InvokableCall_1_t254607980 *, UnityAction_1_t1274809082 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)((InvokableCall_1_t254607980 *)__this, (UnityAction_1_t1274809082 *)((UnityAction_1_t1274809082 *)Castclass((RuntimeObject*)L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCall`1<System.Object>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
extern "C"  void InvokableCall_1__ctor_m2858492764_gshared (InvokableCall_1_t254607980 * __this, UnityAction_1_t1274809082 * ___action0, const RuntimeMethod* method)
{
	{
		NullCheck((BaseInvokableCall_t1352930348 *)__this);
		BaseInvokableCall__ctor_m1099533435((BaseInvokableCall_t1352930348 *)__this, /*hidden argument*/NULL);
		UnityAction_1_t1274809082 * L_0 = ___action0;
		NullCheck((InvokableCall_1_t254607980 *)__this);
		((  void (*) (InvokableCall_1_t254607980 *, UnityAction_1_t1274809082 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)((InvokableCall_1_t254607980 *)__this, (UnityAction_1_t1274809082 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCall`1<System.Object>::add_Delegate(UnityEngine.Events.UnityAction`1<T1>)
extern "C"  void InvokableCall_1_add_Delegate_m2588353253_gshared (InvokableCall_1_t254607980 * __this, UnityAction_1_t1274809082 * ___value0, const RuntimeMethod* method)
{
	UnityAction_1_t1274809082 * V_0 = NULL;
	UnityAction_1_t1274809082 * V_1 = NULL;
	{
		UnityAction_1_t1274809082 * L_0 = (UnityAction_1_t1274809082 *)__this->get_Delegate_0();
		V_0 = (UnityAction_1_t1274809082 *)L_0;
	}

IL_0007:
	{
		UnityAction_1_t1274809082 * L_1 = V_0;
		V_1 = (UnityAction_1_t1274809082 *)L_1;
		UnityAction_1_t1274809082 ** L_2 = (UnityAction_1_t1274809082 **)__this->get_address_of_Delegate_0();
		UnityAction_1_t1274809082 * L_3 = V_1;
		UnityAction_1_t1274809082 * L_4 = ___value0;
		Delegate_t537306192 * L_5 = Delegate_Combine_m408928526(NULL /*static, unused*/, (Delegate_t537306192 *)L_3, (Delegate_t537306192 *)L_4, /*hidden argument*/NULL);
		UnityAction_1_t1274809082 * L_6 = V_0;
		UnityAction_1_t1274809082 * L_7 = InterlockedCompareExchangeImpl<UnityAction_1_t1274809082 *>((UnityAction_1_t1274809082 **)L_2, (UnityAction_1_t1274809082 *)((UnityAction_1_t1274809082 *)Castclass((RuntimeObject*)L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1))), (UnityAction_1_t1274809082 *)L_6);
		V_0 = (UnityAction_1_t1274809082 *)L_7;
		UnityAction_1_t1274809082 * L_8 = V_0;
		UnityAction_1_t1274809082 * L_9 = V_1;
		if ((!(((RuntimeObject*)(UnityAction_1_t1274809082 *)L_8) == ((RuntimeObject*)(UnityAction_1_t1274809082 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCall`1<System.Object>::remove_Delegate(UnityEngine.Events.UnityAction`1<T1>)
extern "C"  void InvokableCall_1_remove_Delegate_m1381041653_gshared (InvokableCall_1_t254607980 * __this, UnityAction_1_t1274809082 * ___value0, const RuntimeMethod* method)
{
	UnityAction_1_t1274809082 * V_0 = NULL;
	UnityAction_1_t1274809082 * V_1 = NULL;
	{
		UnityAction_1_t1274809082 * L_0 = (UnityAction_1_t1274809082 *)__this->get_Delegate_0();
		V_0 = (UnityAction_1_t1274809082 *)L_0;
	}

IL_0007:
	{
		UnityAction_1_t1274809082 * L_1 = V_0;
		V_1 = (UnityAction_1_t1274809082 *)L_1;
		UnityAction_1_t1274809082 ** L_2 = (UnityAction_1_t1274809082 **)__this->get_address_of_Delegate_0();
		UnityAction_1_t1274809082 * L_3 = V_1;
		UnityAction_1_t1274809082 * L_4 = ___value0;
		Delegate_t537306192 * L_5 = Delegate_Remove_m2220107885(NULL /*static, unused*/, (Delegate_t537306192 *)L_3, (Delegate_t537306192 *)L_4, /*hidden argument*/NULL);
		UnityAction_1_t1274809082 * L_6 = V_0;
		UnityAction_1_t1274809082 * L_7 = InterlockedCompareExchangeImpl<UnityAction_1_t1274809082 *>((UnityAction_1_t1274809082 **)L_2, (UnityAction_1_t1274809082 *)((UnityAction_1_t1274809082 *)Castclass((RuntimeObject*)L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1))), (UnityAction_1_t1274809082 *)L_6);
		V_0 = (UnityAction_1_t1274809082 *)L_7;
		UnityAction_1_t1274809082 * L_8 = V_0;
		UnityAction_1_t1274809082 * L_9 = V_1;
		if ((!(((RuntimeObject*)(UnityAction_1_t1274809082 *)L_8) == ((RuntimeObject*)(UnityAction_1_t1274809082 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCall`1<System.Object>::Invoke(System.Object[])
extern "C"  void InvokableCall_1_Invoke_m1948961823_gshared (InvokableCall_1_t254607980 * __this, ObjectU5BU5D_t3270211303* ___args0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InvokableCall_1_Invoke_m1948961823_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		ObjectU5BU5D_t3270211303* L_0 = ___args0;
		NullCheck(L_0);
		if ((((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_0)->max_length))))) == ((int32_t)1)))
		{
			goto IL_0015;
		}
	}
	{
		ArgumentException_t4028401650 * L_1 = (ArgumentException_t4028401650 *)il2cpp_codegen_object_new(ArgumentException_t4028401650_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m1186658332(L_1, (String_t*)_stringLiteral836720390, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0015:
	{
		ObjectU5BU5D_t3270211303* L_2 = ___args0;
		NullCheck(L_2);
		int32_t L_3 = 0;
		RuntimeObject * L_4 = (L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_3));
		((  void (*) (RuntimeObject * /* static, unused */, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)(NULL /*static, unused*/, (RuntimeObject *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		UnityAction_1_t1274809082 * L_5 = (UnityAction_1_t1274809082 *)__this->get_Delegate_0();
		bool L_6 = BaseInvokableCall_AllowInvoke_m3827668627(NULL /*static, unused*/, (Delegate_t537306192 *)L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0040;
		}
	}
	{
		UnityAction_1_t1274809082 * L_7 = (UnityAction_1_t1274809082 *)__this->get_Delegate_0();
		ObjectU5BU5D_t3270211303* L_8 = ___args0;
		NullCheck(L_8);
		int32_t L_9 = 0;
		RuntimeObject * L_10 = (L_8)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		NullCheck((UnityAction_1_t1274809082 *)L_7);
		((  void (*) (UnityAction_1_t1274809082 *, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((UnityAction_1_t1274809082 *)L_7, (RuntimeObject *)((RuntimeObject *)Castclass((RuntimeObject*)L_10, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
	}

IL_0040:
	{
		return;
	}
}
// System.Boolean UnityEngine.Events.InvokableCall`1<System.Object>::Find(System.Object,System.Reflection.MethodInfo)
extern "C"  bool InvokableCall_1_Find_m601517152_gshared (InvokableCall_1_t254607980 * __this, RuntimeObject * ___targetObj0, MethodInfo_t * ___method1, const RuntimeMethod* method)
{
	bool V_0 = false;
	int32_t G_B3_0 = 0;
	{
		UnityAction_1_t1274809082 * L_0 = (UnityAction_1_t1274809082 *)__this->get_Delegate_0();
		NullCheck((Delegate_t537306192 *)L_0);
		RuntimeObject * L_1 = Delegate_get_Target_m2013817205((Delegate_t537306192 *)L_0, /*hidden argument*/NULL);
		RuntimeObject * L_2 = ___targetObj0;
		if ((!(((RuntimeObject*)(RuntimeObject *)L_1) == ((RuntimeObject*)(RuntimeObject *)L_2))))
		{
			goto IL_0025;
		}
	}
	{
		UnityAction_1_t1274809082 * L_3 = (UnityAction_1_t1274809082 *)__this->get_Delegate_0();
		MethodInfo_t * L_4 = NetFxCoreExtensions_GetMethodInfo_m1250622137(NULL /*static, unused*/, (Delegate_t537306192 *)L_3, /*hidden argument*/NULL);
		MethodInfo_t * L_5 = ___method1;
		NullCheck((RuntimeObject *)L_4);
		bool L_6 = VirtFuncInvoker1< bool, RuntimeObject * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, (RuntimeObject *)L_4, (RuntimeObject *)L_5);
		G_B3_0 = ((int32_t)(L_6));
		goto IL_0026;
	}

IL_0025:
	{
		G_B3_0 = 0;
	}

IL_0026:
	{
		V_0 = (bool)G_B3_0;
		goto IL_002c;
	}

IL_002c:
	{
		bool L_7 = V_0;
		return L_7;
	}
}
// System.Void UnityEngine.Events.InvokableCall`1<System.Single>::.ctor(System.Object,System.Reflection.MethodInfo)
extern "C"  void InvokableCall_1__ctor_m3762840761_gshared (InvokableCall_1_t1277999449 * __this, RuntimeObject * ___target0, MethodInfo_t * ___theFunction1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InvokableCall_1__ctor_m3762840761_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		RuntimeObject * L_0 = ___target0;
		MethodInfo_t * L_1 = ___theFunction1;
		NullCheck((BaseInvokableCall_t1352930348 *)__this);
		BaseInvokableCall__ctor_m2708238577((BaseInvokableCall_t1352930348 *)__this, (RuntimeObject *)L_0, (MethodInfo_t *)L_1, /*hidden argument*/NULL);
		MethodInfo_t * L_2 = ___theFunction1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m269683661(NULL /*static, unused*/, (RuntimeTypeHandle_t845633806 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 0)), /*hidden argument*/NULL);
		RuntimeObject * L_4 = ___target0;
		Delegate_t537306192 * L_5 = NetFxCoreExtensions_CreateDelegate_m4287214728(NULL /*static, unused*/, (MethodInfo_t *)L_2, (Type_t *)L_3, (RuntimeObject *)L_4, /*hidden argument*/NULL);
		NullCheck((InvokableCall_1_t1277999449 *)__this);
		((  void (*) (InvokableCall_1_t1277999449 *, UnityAction_1_t2298200551 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)((InvokableCall_1_t1277999449 *)__this, (UnityAction_1_t2298200551 *)((UnityAction_1_t2298200551 *)Castclass((RuntimeObject*)L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCall`1<System.Single>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
extern "C"  void InvokableCall_1__ctor_m4168626135_gshared (InvokableCall_1_t1277999449 * __this, UnityAction_1_t2298200551 * ___action0, const RuntimeMethod* method)
{
	{
		NullCheck((BaseInvokableCall_t1352930348 *)__this);
		BaseInvokableCall__ctor_m1099533435((BaseInvokableCall_t1352930348 *)__this, /*hidden argument*/NULL);
		UnityAction_1_t2298200551 * L_0 = ___action0;
		NullCheck((InvokableCall_1_t1277999449 *)__this);
		((  void (*) (InvokableCall_1_t1277999449 *, UnityAction_1_t2298200551 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)((InvokableCall_1_t1277999449 *)__this, (UnityAction_1_t2298200551 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCall`1<System.Single>::add_Delegate(UnityEngine.Events.UnityAction`1<T1>)
extern "C"  void InvokableCall_1_add_Delegate_m600285857_gshared (InvokableCall_1_t1277999449 * __this, UnityAction_1_t2298200551 * ___value0, const RuntimeMethod* method)
{
	UnityAction_1_t2298200551 * V_0 = NULL;
	UnityAction_1_t2298200551 * V_1 = NULL;
	{
		UnityAction_1_t2298200551 * L_0 = (UnityAction_1_t2298200551 *)__this->get_Delegate_0();
		V_0 = (UnityAction_1_t2298200551 *)L_0;
	}

IL_0007:
	{
		UnityAction_1_t2298200551 * L_1 = V_0;
		V_1 = (UnityAction_1_t2298200551 *)L_1;
		UnityAction_1_t2298200551 ** L_2 = (UnityAction_1_t2298200551 **)__this->get_address_of_Delegate_0();
		UnityAction_1_t2298200551 * L_3 = V_1;
		UnityAction_1_t2298200551 * L_4 = ___value0;
		Delegate_t537306192 * L_5 = Delegate_Combine_m408928526(NULL /*static, unused*/, (Delegate_t537306192 *)L_3, (Delegate_t537306192 *)L_4, /*hidden argument*/NULL);
		UnityAction_1_t2298200551 * L_6 = V_0;
		UnityAction_1_t2298200551 * L_7 = InterlockedCompareExchangeImpl<UnityAction_1_t2298200551 *>((UnityAction_1_t2298200551 **)L_2, (UnityAction_1_t2298200551 *)((UnityAction_1_t2298200551 *)Castclass((RuntimeObject*)L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1))), (UnityAction_1_t2298200551 *)L_6);
		V_0 = (UnityAction_1_t2298200551 *)L_7;
		UnityAction_1_t2298200551 * L_8 = V_0;
		UnityAction_1_t2298200551 * L_9 = V_1;
		if ((!(((RuntimeObject*)(UnityAction_1_t2298200551 *)L_8) == ((RuntimeObject*)(UnityAction_1_t2298200551 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCall`1<System.Single>::remove_Delegate(UnityEngine.Events.UnityAction`1<T1>)
extern "C"  void InvokableCall_1_remove_Delegate_m99444513_gshared (InvokableCall_1_t1277999449 * __this, UnityAction_1_t2298200551 * ___value0, const RuntimeMethod* method)
{
	UnityAction_1_t2298200551 * V_0 = NULL;
	UnityAction_1_t2298200551 * V_1 = NULL;
	{
		UnityAction_1_t2298200551 * L_0 = (UnityAction_1_t2298200551 *)__this->get_Delegate_0();
		V_0 = (UnityAction_1_t2298200551 *)L_0;
	}

IL_0007:
	{
		UnityAction_1_t2298200551 * L_1 = V_0;
		V_1 = (UnityAction_1_t2298200551 *)L_1;
		UnityAction_1_t2298200551 ** L_2 = (UnityAction_1_t2298200551 **)__this->get_address_of_Delegate_0();
		UnityAction_1_t2298200551 * L_3 = V_1;
		UnityAction_1_t2298200551 * L_4 = ___value0;
		Delegate_t537306192 * L_5 = Delegate_Remove_m2220107885(NULL /*static, unused*/, (Delegate_t537306192 *)L_3, (Delegate_t537306192 *)L_4, /*hidden argument*/NULL);
		UnityAction_1_t2298200551 * L_6 = V_0;
		UnityAction_1_t2298200551 * L_7 = InterlockedCompareExchangeImpl<UnityAction_1_t2298200551 *>((UnityAction_1_t2298200551 **)L_2, (UnityAction_1_t2298200551 *)((UnityAction_1_t2298200551 *)Castclass((RuntimeObject*)L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1))), (UnityAction_1_t2298200551 *)L_6);
		V_0 = (UnityAction_1_t2298200551 *)L_7;
		UnityAction_1_t2298200551 * L_8 = V_0;
		UnityAction_1_t2298200551 * L_9 = V_1;
		if ((!(((RuntimeObject*)(UnityAction_1_t2298200551 *)L_8) == ((RuntimeObject*)(UnityAction_1_t2298200551 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCall`1<System.Single>::Invoke(System.Object[])
extern "C"  void InvokableCall_1_Invoke_m293062541_gshared (InvokableCall_1_t1277999449 * __this, ObjectU5BU5D_t3270211303* ___args0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InvokableCall_1_Invoke_m293062541_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		ObjectU5BU5D_t3270211303* L_0 = ___args0;
		NullCheck(L_0);
		if ((((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_0)->max_length))))) == ((int32_t)1)))
		{
			goto IL_0015;
		}
	}
	{
		ArgumentException_t4028401650 * L_1 = (ArgumentException_t4028401650 *)il2cpp_codegen_object_new(ArgumentException_t4028401650_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m1186658332(L_1, (String_t*)_stringLiteral836720390, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0015:
	{
		ObjectU5BU5D_t3270211303* L_2 = ___args0;
		NullCheck(L_2);
		int32_t L_3 = 0;
		RuntimeObject * L_4 = (L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_3));
		((  void (*) (RuntimeObject * /* static, unused */, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)(NULL /*static, unused*/, (RuntimeObject *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		UnityAction_1_t2298200551 * L_5 = (UnityAction_1_t2298200551 *)__this->get_Delegate_0();
		bool L_6 = BaseInvokableCall_AllowInvoke_m3827668627(NULL /*static, unused*/, (Delegate_t537306192 *)L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0040;
		}
	}
	{
		UnityAction_1_t2298200551 * L_7 = (UnityAction_1_t2298200551 *)__this->get_Delegate_0();
		ObjectU5BU5D_t3270211303* L_8 = ___args0;
		NullCheck(L_8);
		int32_t L_9 = 0;
		RuntimeObject * L_10 = (L_8)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		NullCheck((UnityAction_1_t2298200551 *)L_7);
		((  void (*) (UnityAction_1_t2298200551 *, float, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((UnityAction_1_t2298200551 *)L_7, (float)((*(float*)((float*)UnBox(L_10, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5))))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
	}

IL_0040:
	{
		return;
	}
}
// System.Boolean UnityEngine.Events.InvokableCall`1<System.Single>::Find(System.Object,System.Reflection.MethodInfo)
extern "C"  bool InvokableCall_1_Find_m2341263959_gshared (InvokableCall_1_t1277999449 * __this, RuntimeObject * ___targetObj0, MethodInfo_t * ___method1, const RuntimeMethod* method)
{
	bool V_0 = false;
	int32_t G_B3_0 = 0;
	{
		UnityAction_1_t2298200551 * L_0 = (UnityAction_1_t2298200551 *)__this->get_Delegate_0();
		NullCheck((Delegate_t537306192 *)L_0);
		RuntimeObject * L_1 = Delegate_get_Target_m2013817205((Delegate_t537306192 *)L_0, /*hidden argument*/NULL);
		RuntimeObject * L_2 = ___targetObj0;
		if ((!(((RuntimeObject*)(RuntimeObject *)L_1) == ((RuntimeObject*)(RuntimeObject *)L_2))))
		{
			goto IL_0025;
		}
	}
	{
		UnityAction_1_t2298200551 * L_3 = (UnityAction_1_t2298200551 *)__this->get_Delegate_0();
		MethodInfo_t * L_4 = NetFxCoreExtensions_GetMethodInfo_m1250622137(NULL /*static, unused*/, (Delegate_t537306192 *)L_3, /*hidden argument*/NULL);
		MethodInfo_t * L_5 = ___method1;
		NullCheck((RuntimeObject *)L_4);
		bool L_6 = VirtFuncInvoker1< bool, RuntimeObject * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, (RuntimeObject *)L_4, (RuntimeObject *)L_5);
		G_B3_0 = ((int32_t)(L_6));
		goto IL_0026;
	}

IL_0025:
	{
		G_B3_0 = 0;
	}

IL_0026:
	{
		V_0 = (bool)G_B3_0;
		goto IL_002c;
	}

IL_002c:
	{
		bool L_7 = V_0;
		return L_7;
	}
}
// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.Color>::.ctor(System.Object,System.Reflection.MethodInfo)
extern "C"  void InvokableCall_1__ctor_m3162522570_gshared (InvokableCall_1_t739676100 * __this, RuntimeObject * ___target0, MethodInfo_t * ___theFunction1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InvokableCall_1__ctor_m3162522570_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		RuntimeObject * L_0 = ___target0;
		MethodInfo_t * L_1 = ___theFunction1;
		NullCheck((BaseInvokableCall_t1352930348 *)__this);
		BaseInvokableCall__ctor_m2708238577((BaseInvokableCall_t1352930348 *)__this, (RuntimeObject *)L_0, (MethodInfo_t *)L_1, /*hidden argument*/NULL);
		MethodInfo_t * L_2 = ___theFunction1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m269683661(NULL /*static, unused*/, (RuntimeTypeHandle_t845633806 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 0)), /*hidden argument*/NULL);
		RuntimeObject * L_4 = ___target0;
		Delegate_t537306192 * L_5 = NetFxCoreExtensions_CreateDelegate_m4287214728(NULL /*static, unused*/, (MethodInfo_t *)L_2, (Type_t *)L_3, (RuntimeObject *)L_4, /*hidden argument*/NULL);
		NullCheck((InvokableCall_1_t739676100 *)__this);
		((  void (*) (InvokableCall_1_t739676100 *, UnityAction_1_t1759877202 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)((InvokableCall_1_t739676100 *)__this, (UnityAction_1_t1759877202 *)((UnityAction_1_t1759877202 *)Castclass((RuntimeObject*)L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.Color>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
extern "C"  void InvokableCall_1__ctor_m2144327403_gshared (InvokableCall_1_t739676100 * __this, UnityAction_1_t1759877202 * ___action0, const RuntimeMethod* method)
{
	{
		NullCheck((BaseInvokableCall_t1352930348 *)__this);
		BaseInvokableCall__ctor_m1099533435((BaseInvokableCall_t1352930348 *)__this, /*hidden argument*/NULL);
		UnityAction_1_t1759877202 * L_0 = ___action0;
		NullCheck((InvokableCall_1_t739676100 *)__this);
		((  void (*) (InvokableCall_1_t739676100 *, UnityAction_1_t1759877202 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)((InvokableCall_1_t739676100 *)__this, (UnityAction_1_t1759877202 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.Color>::add_Delegate(UnityEngine.Events.UnityAction`1<T1>)
extern "C"  void InvokableCall_1_add_Delegate_m4283863247_gshared (InvokableCall_1_t739676100 * __this, UnityAction_1_t1759877202 * ___value0, const RuntimeMethod* method)
{
	UnityAction_1_t1759877202 * V_0 = NULL;
	UnityAction_1_t1759877202 * V_1 = NULL;
	{
		UnityAction_1_t1759877202 * L_0 = (UnityAction_1_t1759877202 *)__this->get_Delegate_0();
		V_0 = (UnityAction_1_t1759877202 *)L_0;
	}

IL_0007:
	{
		UnityAction_1_t1759877202 * L_1 = V_0;
		V_1 = (UnityAction_1_t1759877202 *)L_1;
		UnityAction_1_t1759877202 ** L_2 = (UnityAction_1_t1759877202 **)__this->get_address_of_Delegate_0();
		UnityAction_1_t1759877202 * L_3 = V_1;
		UnityAction_1_t1759877202 * L_4 = ___value0;
		Delegate_t537306192 * L_5 = Delegate_Combine_m408928526(NULL /*static, unused*/, (Delegate_t537306192 *)L_3, (Delegate_t537306192 *)L_4, /*hidden argument*/NULL);
		UnityAction_1_t1759877202 * L_6 = V_0;
		UnityAction_1_t1759877202 * L_7 = InterlockedCompareExchangeImpl<UnityAction_1_t1759877202 *>((UnityAction_1_t1759877202 **)L_2, (UnityAction_1_t1759877202 *)((UnityAction_1_t1759877202 *)Castclass((RuntimeObject*)L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1))), (UnityAction_1_t1759877202 *)L_6);
		V_0 = (UnityAction_1_t1759877202 *)L_7;
		UnityAction_1_t1759877202 * L_8 = V_0;
		UnityAction_1_t1759877202 * L_9 = V_1;
		if ((!(((RuntimeObject*)(UnityAction_1_t1759877202 *)L_8) == ((RuntimeObject*)(UnityAction_1_t1759877202 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.Color>::remove_Delegate(UnityEngine.Events.UnityAction`1<T1>)
extern "C"  void InvokableCall_1_remove_Delegate_m1346792618_gshared (InvokableCall_1_t739676100 * __this, UnityAction_1_t1759877202 * ___value0, const RuntimeMethod* method)
{
	UnityAction_1_t1759877202 * V_0 = NULL;
	UnityAction_1_t1759877202 * V_1 = NULL;
	{
		UnityAction_1_t1759877202 * L_0 = (UnityAction_1_t1759877202 *)__this->get_Delegate_0();
		V_0 = (UnityAction_1_t1759877202 *)L_0;
	}

IL_0007:
	{
		UnityAction_1_t1759877202 * L_1 = V_0;
		V_1 = (UnityAction_1_t1759877202 *)L_1;
		UnityAction_1_t1759877202 ** L_2 = (UnityAction_1_t1759877202 **)__this->get_address_of_Delegate_0();
		UnityAction_1_t1759877202 * L_3 = V_1;
		UnityAction_1_t1759877202 * L_4 = ___value0;
		Delegate_t537306192 * L_5 = Delegate_Remove_m2220107885(NULL /*static, unused*/, (Delegate_t537306192 *)L_3, (Delegate_t537306192 *)L_4, /*hidden argument*/NULL);
		UnityAction_1_t1759877202 * L_6 = V_0;
		UnityAction_1_t1759877202 * L_7 = InterlockedCompareExchangeImpl<UnityAction_1_t1759877202 *>((UnityAction_1_t1759877202 **)L_2, (UnityAction_1_t1759877202 *)((UnityAction_1_t1759877202 *)Castclass((RuntimeObject*)L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1))), (UnityAction_1_t1759877202 *)L_6);
		V_0 = (UnityAction_1_t1759877202 *)L_7;
		UnityAction_1_t1759877202 * L_8 = V_0;
		UnityAction_1_t1759877202 * L_9 = V_1;
		if ((!(((RuntimeObject*)(UnityAction_1_t1759877202 *)L_8) == ((RuntimeObject*)(UnityAction_1_t1759877202 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.Color>::Invoke(System.Object[])
extern "C"  void InvokableCall_1_Invoke_m577541394_gshared (InvokableCall_1_t739676100 * __this, ObjectU5BU5D_t3270211303* ___args0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InvokableCall_1_Invoke_m577541394_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		ObjectU5BU5D_t3270211303* L_0 = ___args0;
		NullCheck(L_0);
		if ((((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_0)->max_length))))) == ((int32_t)1)))
		{
			goto IL_0015;
		}
	}
	{
		ArgumentException_t4028401650 * L_1 = (ArgumentException_t4028401650 *)il2cpp_codegen_object_new(ArgumentException_t4028401650_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m1186658332(L_1, (String_t*)_stringLiteral836720390, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0015:
	{
		ObjectU5BU5D_t3270211303* L_2 = ___args0;
		NullCheck(L_2);
		int32_t L_3 = 0;
		RuntimeObject * L_4 = (L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_3));
		((  void (*) (RuntimeObject * /* static, unused */, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)(NULL /*static, unused*/, (RuntimeObject *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		UnityAction_1_t1759877202 * L_5 = (UnityAction_1_t1759877202 *)__this->get_Delegate_0();
		bool L_6 = BaseInvokableCall_AllowInvoke_m3827668627(NULL /*static, unused*/, (Delegate_t537306192 *)L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0040;
		}
	}
	{
		UnityAction_1_t1759877202 * L_7 = (UnityAction_1_t1759877202 *)__this->get_Delegate_0();
		ObjectU5BU5D_t3270211303* L_8 = ___args0;
		NullCheck(L_8);
		int32_t L_9 = 0;
		RuntimeObject * L_10 = (L_8)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		NullCheck((UnityAction_1_t1759877202 *)L_7);
		((  void (*) (UnityAction_1_t1759877202 *, Color_t2507026410 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((UnityAction_1_t1759877202 *)L_7, (Color_t2507026410 )((*(Color_t2507026410 *)((Color_t2507026410 *)UnBox(L_10, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5))))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
	}

IL_0040:
	{
		return;
	}
}
// System.Boolean UnityEngine.Events.InvokableCall`1<UnityEngine.Color>::Find(System.Object,System.Reflection.MethodInfo)
extern "C"  bool InvokableCall_1_Find_m2695445740_gshared (InvokableCall_1_t739676100 * __this, RuntimeObject * ___targetObj0, MethodInfo_t * ___method1, const RuntimeMethod* method)
{
	bool V_0 = false;
	int32_t G_B3_0 = 0;
	{
		UnityAction_1_t1759877202 * L_0 = (UnityAction_1_t1759877202 *)__this->get_Delegate_0();
		NullCheck((Delegate_t537306192 *)L_0);
		RuntimeObject * L_1 = Delegate_get_Target_m2013817205((Delegate_t537306192 *)L_0, /*hidden argument*/NULL);
		RuntimeObject * L_2 = ___targetObj0;
		if ((!(((RuntimeObject*)(RuntimeObject *)L_1) == ((RuntimeObject*)(RuntimeObject *)L_2))))
		{
			goto IL_0025;
		}
	}
	{
		UnityAction_1_t1759877202 * L_3 = (UnityAction_1_t1759877202 *)__this->get_Delegate_0();
		MethodInfo_t * L_4 = NetFxCoreExtensions_GetMethodInfo_m1250622137(NULL /*static, unused*/, (Delegate_t537306192 *)L_3, /*hidden argument*/NULL);
		MethodInfo_t * L_5 = ___method1;
		NullCheck((RuntimeObject *)L_4);
		bool L_6 = VirtFuncInvoker1< bool, RuntimeObject * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, (RuntimeObject *)L_4, (RuntimeObject *)L_5);
		G_B3_0 = ((int32_t)(L_6));
		goto IL_0026;
	}

IL_0025:
	{
		G_B3_0 = 0;
	}

IL_0026:
	{
		V_0 = (bool)G_B3_0;
		goto IL_002c;
	}

IL_002c:
	{
		bool L_7 = V_0;
		return L_7;
	}
}
// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.Vector2>::.ctor(System.Object,System.Reflection.MethodInfo)
extern "C"  void InvokableCall_1__ctor_m2449436807_gshared (InvokableCall_1_t1809982952 * __this, RuntimeObject * ___target0, MethodInfo_t * ___theFunction1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InvokableCall_1__ctor_m2449436807_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		RuntimeObject * L_0 = ___target0;
		MethodInfo_t * L_1 = ___theFunction1;
		NullCheck((BaseInvokableCall_t1352930348 *)__this);
		BaseInvokableCall__ctor_m2708238577((BaseInvokableCall_t1352930348 *)__this, (RuntimeObject *)L_0, (MethodInfo_t *)L_1, /*hidden argument*/NULL);
		MethodInfo_t * L_2 = ___theFunction1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m269683661(NULL /*static, unused*/, (RuntimeTypeHandle_t845633806 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 0)), /*hidden argument*/NULL);
		RuntimeObject * L_4 = ___target0;
		Delegate_t537306192 * L_5 = NetFxCoreExtensions_CreateDelegate_m4287214728(NULL /*static, unused*/, (MethodInfo_t *)L_2, (Type_t *)L_3, (RuntimeObject *)L_4, /*hidden argument*/NULL);
		NullCheck((InvokableCall_1_t1809982952 *)__this);
		((  void (*) (InvokableCall_1_t1809982952 *, UnityAction_1_t2830184054 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)((InvokableCall_1_t1809982952 *)__this, (UnityAction_1_t2830184054 *)((UnityAction_1_t2830184054 *)Castclass((RuntimeObject*)L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.Vector2>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
extern "C"  void InvokableCall_1__ctor_m3111002877_gshared (InvokableCall_1_t1809982952 * __this, UnityAction_1_t2830184054 * ___action0, const RuntimeMethod* method)
{
	{
		NullCheck((BaseInvokableCall_t1352930348 *)__this);
		BaseInvokableCall__ctor_m1099533435((BaseInvokableCall_t1352930348 *)__this, /*hidden argument*/NULL);
		UnityAction_1_t2830184054 * L_0 = ___action0;
		NullCheck((InvokableCall_1_t1809982952 *)__this);
		((  void (*) (InvokableCall_1_t1809982952 *, UnityAction_1_t2830184054 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)((InvokableCall_1_t1809982952 *)__this, (UnityAction_1_t2830184054 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.Vector2>::add_Delegate(UnityEngine.Events.UnityAction`1<T1>)
extern "C"  void InvokableCall_1_add_Delegate_m3555743373_gshared (InvokableCall_1_t1809982952 * __this, UnityAction_1_t2830184054 * ___value0, const RuntimeMethod* method)
{
	UnityAction_1_t2830184054 * V_0 = NULL;
	UnityAction_1_t2830184054 * V_1 = NULL;
	{
		UnityAction_1_t2830184054 * L_0 = (UnityAction_1_t2830184054 *)__this->get_Delegate_0();
		V_0 = (UnityAction_1_t2830184054 *)L_0;
	}

IL_0007:
	{
		UnityAction_1_t2830184054 * L_1 = V_0;
		V_1 = (UnityAction_1_t2830184054 *)L_1;
		UnityAction_1_t2830184054 ** L_2 = (UnityAction_1_t2830184054 **)__this->get_address_of_Delegate_0();
		UnityAction_1_t2830184054 * L_3 = V_1;
		UnityAction_1_t2830184054 * L_4 = ___value0;
		Delegate_t537306192 * L_5 = Delegate_Combine_m408928526(NULL /*static, unused*/, (Delegate_t537306192 *)L_3, (Delegate_t537306192 *)L_4, /*hidden argument*/NULL);
		UnityAction_1_t2830184054 * L_6 = V_0;
		UnityAction_1_t2830184054 * L_7 = InterlockedCompareExchangeImpl<UnityAction_1_t2830184054 *>((UnityAction_1_t2830184054 **)L_2, (UnityAction_1_t2830184054 *)((UnityAction_1_t2830184054 *)Castclass((RuntimeObject*)L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1))), (UnityAction_1_t2830184054 *)L_6);
		V_0 = (UnityAction_1_t2830184054 *)L_7;
		UnityAction_1_t2830184054 * L_8 = V_0;
		UnityAction_1_t2830184054 * L_9 = V_1;
		if ((!(((RuntimeObject*)(UnityAction_1_t2830184054 *)L_8) == ((RuntimeObject*)(UnityAction_1_t2830184054 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.Vector2>::remove_Delegate(UnityEngine.Events.UnityAction`1<T1>)
extern "C"  void InvokableCall_1_remove_Delegate_m2152532731_gshared (InvokableCall_1_t1809982952 * __this, UnityAction_1_t2830184054 * ___value0, const RuntimeMethod* method)
{
	UnityAction_1_t2830184054 * V_0 = NULL;
	UnityAction_1_t2830184054 * V_1 = NULL;
	{
		UnityAction_1_t2830184054 * L_0 = (UnityAction_1_t2830184054 *)__this->get_Delegate_0();
		V_0 = (UnityAction_1_t2830184054 *)L_0;
	}

IL_0007:
	{
		UnityAction_1_t2830184054 * L_1 = V_0;
		V_1 = (UnityAction_1_t2830184054 *)L_1;
		UnityAction_1_t2830184054 ** L_2 = (UnityAction_1_t2830184054 **)__this->get_address_of_Delegate_0();
		UnityAction_1_t2830184054 * L_3 = V_1;
		UnityAction_1_t2830184054 * L_4 = ___value0;
		Delegate_t537306192 * L_5 = Delegate_Remove_m2220107885(NULL /*static, unused*/, (Delegate_t537306192 *)L_3, (Delegate_t537306192 *)L_4, /*hidden argument*/NULL);
		UnityAction_1_t2830184054 * L_6 = V_0;
		UnityAction_1_t2830184054 * L_7 = InterlockedCompareExchangeImpl<UnityAction_1_t2830184054 *>((UnityAction_1_t2830184054 **)L_2, (UnityAction_1_t2830184054 *)((UnityAction_1_t2830184054 *)Castclass((RuntimeObject*)L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1))), (UnityAction_1_t2830184054 *)L_6);
		V_0 = (UnityAction_1_t2830184054 *)L_7;
		UnityAction_1_t2830184054 * L_8 = V_0;
		UnityAction_1_t2830184054 * L_9 = V_1;
		if ((!(((RuntimeObject*)(UnityAction_1_t2830184054 *)L_8) == ((RuntimeObject*)(UnityAction_1_t2830184054 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.Vector2>::Invoke(System.Object[])
extern "C"  void InvokableCall_1_Invoke_m2624901433_gshared (InvokableCall_1_t1809982952 * __this, ObjectU5BU5D_t3270211303* ___args0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InvokableCall_1_Invoke_m2624901433_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		ObjectU5BU5D_t3270211303* L_0 = ___args0;
		NullCheck(L_0);
		if ((((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_0)->max_length))))) == ((int32_t)1)))
		{
			goto IL_0015;
		}
	}
	{
		ArgumentException_t4028401650 * L_1 = (ArgumentException_t4028401650 *)il2cpp_codegen_object_new(ArgumentException_t4028401650_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m1186658332(L_1, (String_t*)_stringLiteral836720390, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0015:
	{
		ObjectU5BU5D_t3270211303* L_2 = ___args0;
		NullCheck(L_2);
		int32_t L_3 = 0;
		RuntimeObject * L_4 = (L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_3));
		((  void (*) (RuntimeObject * /* static, unused */, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)(NULL /*static, unused*/, (RuntimeObject *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		UnityAction_1_t2830184054 * L_5 = (UnityAction_1_t2830184054 *)__this->get_Delegate_0();
		bool L_6 = BaseInvokableCall_AllowInvoke_m3827668627(NULL /*static, unused*/, (Delegate_t537306192 *)L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0040;
		}
	}
	{
		UnityAction_1_t2830184054 * L_7 = (UnityAction_1_t2830184054 *)__this->get_Delegate_0();
		ObjectU5BU5D_t3270211303* L_8 = ___args0;
		NullCheck(L_8);
		int32_t L_9 = 0;
		RuntimeObject * L_10 = (L_8)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		NullCheck((UnityAction_1_t2830184054 *)L_7);
		((  void (*) (UnityAction_1_t2830184054 *, Vector2_t3577333262 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((UnityAction_1_t2830184054 *)L_7, (Vector2_t3577333262 )((*(Vector2_t3577333262 *)((Vector2_t3577333262 *)UnBox(L_10, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5))))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
	}

IL_0040:
	{
		return;
	}
}
// System.Boolean UnityEngine.Events.InvokableCall`1<UnityEngine.Vector2>::Find(System.Object,System.Reflection.MethodInfo)
extern "C"  bool InvokableCall_1_Find_m4173888579_gshared (InvokableCall_1_t1809982952 * __this, RuntimeObject * ___targetObj0, MethodInfo_t * ___method1, const RuntimeMethod* method)
{
	bool V_0 = false;
	int32_t G_B3_0 = 0;
	{
		UnityAction_1_t2830184054 * L_0 = (UnityAction_1_t2830184054 *)__this->get_Delegate_0();
		NullCheck((Delegate_t537306192 *)L_0);
		RuntimeObject * L_1 = Delegate_get_Target_m2013817205((Delegate_t537306192 *)L_0, /*hidden argument*/NULL);
		RuntimeObject * L_2 = ___targetObj0;
		if ((!(((RuntimeObject*)(RuntimeObject *)L_1) == ((RuntimeObject*)(RuntimeObject *)L_2))))
		{
			goto IL_0025;
		}
	}
	{
		UnityAction_1_t2830184054 * L_3 = (UnityAction_1_t2830184054 *)__this->get_Delegate_0();
		MethodInfo_t * L_4 = NetFxCoreExtensions_GetMethodInfo_m1250622137(NULL /*static, unused*/, (Delegate_t537306192 *)L_3, /*hidden argument*/NULL);
		MethodInfo_t * L_5 = ___method1;
		NullCheck((RuntimeObject *)L_4);
		bool L_6 = VirtFuncInvoker1< bool, RuntimeObject * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, (RuntimeObject *)L_4, (RuntimeObject *)L_5);
		G_B3_0 = ((int32_t)(L_6));
		goto IL_0026;
	}

IL_0025:
	{
		G_B3_0 = 0;
	}

IL_0026:
	{
		V_0 = (bool)G_B3_0;
		goto IL_002c;
	}

IL_002c:
	{
		bool L_7 = V_0;
		return L_7;
	}
}
// System.Void UnityEngine.Events.InvokableCall`2<System.Object,System.Object>::.ctor(System.Object,System.Reflection.MethodInfo)
extern "C"  void InvokableCall_2__ctor_m704948989_gshared (InvokableCall_2_t1353357206 * __this, RuntimeObject * ___target0, MethodInfo_t * ___theFunction1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InvokableCall_2__ctor_m704948989_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		RuntimeObject * L_0 = ___target0;
		MethodInfo_t * L_1 = ___theFunction1;
		NullCheck((BaseInvokableCall_t1352930348 *)__this);
		BaseInvokableCall__ctor_m2708238577((BaseInvokableCall_t1352930348 *)__this, (RuntimeObject *)L_0, (MethodInfo_t *)L_1, /*hidden argument*/NULL);
		MethodInfo_t * L_2 = ___theFunction1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m269683661(NULL /*static, unused*/, (RuntimeTypeHandle_t845633806 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 0)), /*hidden argument*/NULL);
		RuntimeObject * L_4 = ___target0;
		Delegate_t537306192 * L_5 = NetFxCoreExtensions_CreateDelegate_m4287214728(NULL /*static, unused*/, (MethodInfo_t *)L_2, (Type_t *)L_3, (RuntimeObject *)L_4, /*hidden argument*/NULL);
		__this->set_Delegate_0(((UnityAction_2_t1132847070 *)Castclass((RuntimeObject*)L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1))));
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCall`2<System.Object,System.Object>::Invoke(System.Object[])
extern "C"  void InvokableCall_2_Invoke_m3704122215_gshared (InvokableCall_2_t1353357206 * __this, ObjectU5BU5D_t3270211303* ___args0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InvokableCall_2_Invoke_m3704122215_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		ObjectU5BU5D_t3270211303* L_0 = ___args0;
		NullCheck(L_0);
		if ((((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_0)->max_length))))) == ((int32_t)2)))
		{
			goto IL_0015;
		}
	}
	{
		ArgumentException_t4028401650 * L_1 = (ArgumentException_t4028401650 *)il2cpp_codegen_object_new(ArgumentException_t4028401650_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m1186658332(L_1, (String_t*)_stringLiteral836720390, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0015:
	{
		ObjectU5BU5D_t3270211303* L_2 = ___args0;
		NullCheck(L_2);
		int32_t L_3 = 0;
		RuntimeObject * L_4 = (L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_3));
		((  void (*) (RuntimeObject * /* static, unused */, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)(NULL /*static, unused*/, (RuntimeObject *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		ObjectU5BU5D_t3270211303* L_5 = ___args0;
		NullCheck(L_5);
		int32_t L_6 = 1;
		RuntimeObject * L_7 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_6));
		((  void (*) (RuntimeObject * /* static, unused */, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)(NULL /*static, unused*/, (RuntimeObject *)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		UnityAction_2_t1132847070 * L_8 = (UnityAction_2_t1132847070 *)__this->get_Delegate_0();
		bool L_9 = BaseInvokableCall_AllowInvoke_m3827668627(NULL /*static, unused*/, (Delegate_t537306192 *)L_8, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_0050;
		}
	}
	{
		UnityAction_2_t1132847070 * L_10 = (UnityAction_2_t1132847070 *)__this->get_Delegate_0();
		ObjectU5BU5D_t3270211303* L_11 = ___args0;
		NullCheck(L_11);
		int32_t L_12 = 0;
		RuntimeObject * L_13 = (L_11)->GetAt(static_cast<il2cpp_array_size_t>(L_12));
		ObjectU5BU5D_t3270211303* L_14 = ___args0;
		NullCheck(L_14);
		int32_t L_15 = 1;
		RuntimeObject * L_16 = (L_14)->GetAt(static_cast<il2cpp_array_size_t>(L_15));
		NullCheck((UnityAction_2_t1132847070 *)L_10);
		((  void (*) (UnityAction_2_t1132847070 *, RuntimeObject *, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((UnityAction_2_t1132847070 *)L_10, (RuntimeObject *)((RuntimeObject *)Castclass((RuntimeObject*)L_13, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 4))), (RuntimeObject *)((RuntimeObject *)Castclass((RuntimeObject*)L_16, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
	}

IL_0050:
	{
		return;
	}
}
// System.Boolean UnityEngine.Events.InvokableCall`2<System.Object,System.Object>::Find(System.Object,System.Reflection.MethodInfo)
extern "C"  bool InvokableCall_2_Find_m1574859239_gshared (InvokableCall_2_t1353357206 * __this, RuntimeObject * ___targetObj0, MethodInfo_t * ___method1, const RuntimeMethod* method)
{
	bool V_0 = false;
	int32_t G_B3_0 = 0;
	{
		UnityAction_2_t1132847070 * L_0 = (UnityAction_2_t1132847070 *)__this->get_Delegate_0();
		NullCheck((Delegate_t537306192 *)L_0);
		RuntimeObject * L_1 = Delegate_get_Target_m2013817205((Delegate_t537306192 *)L_0, /*hidden argument*/NULL);
		RuntimeObject * L_2 = ___targetObj0;
		if ((!(((RuntimeObject*)(RuntimeObject *)L_1) == ((RuntimeObject*)(RuntimeObject *)L_2))))
		{
			goto IL_0025;
		}
	}
	{
		UnityAction_2_t1132847070 * L_3 = (UnityAction_2_t1132847070 *)__this->get_Delegate_0();
		MethodInfo_t * L_4 = NetFxCoreExtensions_GetMethodInfo_m1250622137(NULL /*static, unused*/, (Delegate_t537306192 *)L_3, /*hidden argument*/NULL);
		MethodInfo_t * L_5 = ___method1;
		NullCheck((RuntimeObject *)L_4);
		bool L_6 = VirtFuncInvoker1< bool, RuntimeObject * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, (RuntimeObject *)L_4, (RuntimeObject *)L_5);
		G_B3_0 = ((int32_t)(L_6));
		goto IL_0026;
	}

IL_0025:
	{
		G_B3_0 = 0;
	}

IL_0026:
	{
		V_0 = (bool)G_B3_0;
		goto IL_002c;
	}

IL_002c:
	{
		bool L_7 = V_0;
		return L_7;
	}
}
// System.Void UnityEngine.Events.InvokableCall`3<System.Object,System.Object,System.Object>::.ctor(System.Object,System.Reflection.MethodInfo)
extern "C"  void InvokableCall_3__ctor_m1051700724_gshared (InvokableCall_3_t3331941758 * __this, RuntimeObject * ___target0, MethodInfo_t * ___theFunction1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InvokableCall_3__ctor_m1051700724_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		RuntimeObject * L_0 = ___target0;
		MethodInfo_t * L_1 = ___theFunction1;
		NullCheck((BaseInvokableCall_t1352930348 *)__this);
		BaseInvokableCall__ctor_m2708238577((BaseInvokableCall_t1352930348 *)__this, (RuntimeObject *)L_0, (MethodInfo_t *)L_1, /*hidden argument*/NULL);
		MethodInfo_t * L_2 = ___theFunction1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m269683661(NULL /*static, unused*/, (RuntimeTypeHandle_t845633806 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 0)), /*hidden argument*/NULL);
		RuntimeObject * L_4 = ___target0;
		Delegate_t537306192 * L_5 = NetFxCoreExtensions_CreateDelegate_m4287214728(NULL /*static, unused*/, (MethodInfo_t *)L_2, (Type_t *)L_3, (RuntimeObject *)L_4, /*hidden argument*/NULL);
		__this->set_Delegate_0(((UnityAction_3_t1794137105 *)Castclass((RuntimeObject*)L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1))));
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCall`3<System.Object,System.Object,System.Object>::Invoke(System.Object[])
extern "C"  void InvokableCall_3_Invoke_m2568093866_gshared (InvokableCall_3_t3331941758 * __this, ObjectU5BU5D_t3270211303* ___args0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InvokableCall_3_Invoke_m2568093866_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		ObjectU5BU5D_t3270211303* L_0 = ___args0;
		NullCheck(L_0);
		if ((((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_0)->max_length))))) == ((int32_t)3)))
		{
			goto IL_0015;
		}
	}
	{
		ArgumentException_t4028401650 * L_1 = (ArgumentException_t4028401650 *)il2cpp_codegen_object_new(ArgumentException_t4028401650_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m1186658332(L_1, (String_t*)_stringLiteral836720390, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0015:
	{
		ObjectU5BU5D_t3270211303* L_2 = ___args0;
		NullCheck(L_2);
		int32_t L_3 = 0;
		RuntimeObject * L_4 = (L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_3));
		((  void (*) (RuntimeObject * /* static, unused */, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)(NULL /*static, unused*/, (RuntimeObject *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		ObjectU5BU5D_t3270211303* L_5 = ___args0;
		NullCheck(L_5);
		int32_t L_6 = 1;
		RuntimeObject * L_7 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_6));
		((  void (*) (RuntimeObject * /* static, unused */, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)(NULL /*static, unused*/, (RuntimeObject *)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		ObjectU5BU5D_t3270211303* L_8 = ___args0;
		NullCheck(L_8);
		int32_t L_9 = 2;
		RuntimeObject * L_10 = (L_8)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		((  void (*) (RuntimeObject * /* static, unused */, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)(NULL /*static, unused*/, (RuntimeObject *)L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		UnityAction_3_t1794137105 * L_11 = (UnityAction_3_t1794137105 *)__this->get_Delegate_0();
		bool L_12 = BaseInvokableCall_AllowInvoke_m3827668627(NULL /*static, unused*/, (Delegate_t537306192 *)L_11, /*hidden argument*/NULL);
		if (!L_12)
		{
			goto IL_0060;
		}
	}
	{
		UnityAction_3_t1794137105 * L_13 = (UnityAction_3_t1794137105 *)__this->get_Delegate_0();
		ObjectU5BU5D_t3270211303* L_14 = ___args0;
		NullCheck(L_14);
		int32_t L_15 = 0;
		RuntimeObject * L_16 = (L_14)->GetAt(static_cast<il2cpp_array_size_t>(L_15));
		ObjectU5BU5D_t3270211303* L_17 = ___args0;
		NullCheck(L_17);
		int32_t L_18 = 1;
		RuntimeObject * L_19 = (L_17)->GetAt(static_cast<il2cpp_array_size_t>(L_18));
		ObjectU5BU5D_t3270211303* L_20 = ___args0;
		NullCheck(L_20);
		int32_t L_21 = 2;
		RuntimeObject * L_22 = (L_20)->GetAt(static_cast<il2cpp_array_size_t>(L_21));
		NullCheck((UnityAction_3_t1794137105 *)L_13);
		((  void (*) (UnityAction_3_t1794137105 *, RuntimeObject *, RuntimeObject *, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)((UnityAction_3_t1794137105 *)L_13, (RuntimeObject *)((RuntimeObject *)Castclass((RuntimeObject*)L_16, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5))), (RuntimeObject *)((RuntimeObject *)Castclass((RuntimeObject*)L_19, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))), (RuntimeObject *)((RuntimeObject *)Castclass((RuntimeObject*)L_22, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 7))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
	}

IL_0060:
	{
		return;
	}
}
// System.Boolean UnityEngine.Events.InvokableCall`3<System.Object,System.Object,System.Object>::Find(System.Object,System.Reflection.MethodInfo)
extern "C"  bool InvokableCall_3_Find_m2122926775_gshared (InvokableCall_3_t3331941758 * __this, RuntimeObject * ___targetObj0, MethodInfo_t * ___method1, const RuntimeMethod* method)
{
	bool V_0 = false;
	int32_t G_B3_0 = 0;
	{
		UnityAction_3_t1794137105 * L_0 = (UnityAction_3_t1794137105 *)__this->get_Delegate_0();
		NullCheck((Delegate_t537306192 *)L_0);
		RuntimeObject * L_1 = Delegate_get_Target_m2013817205((Delegate_t537306192 *)L_0, /*hidden argument*/NULL);
		RuntimeObject * L_2 = ___targetObj0;
		if ((!(((RuntimeObject*)(RuntimeObject *)L_1) == ((RuntimeObject*)(RuntimeObject *)L_2))))
		{
			goto IL_0025;
		}
	}
	{
		UnityAction_3_t1794137105 * L_3 = (UnityAction_3_t1794137105 *)__this->get_Delegate_0();
		MethodInfo_t * L_4 = NetFxCoreExtensions_GetMethodInfo_m1250622137(NULL /*static, unused*/, (Delegate_t537306192 *)L_3, /*hidden argument*/NULL);
		MethodInfo_t * L_5 = ___method1;
		NullCheck((RuntimeObject *)L_4);
		bool L_6 = VirtFuncInvoker1< bool, RuntimeObject * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, (RuntimeObject *)L_4, (RuntimeObject *)L_5);
		G_B3_0 = ((int32_t)(L_6));
		goto IL_0026;
	}

IL_0025:
	{
		G_B3_0 = 0;
	}

IL_0026:
	{
		V_0 = (bool)G_B3_0;
		goto IL_002c;
	}

IL_002c:
	{
		bool L_7 = V_0;
		return L_7;
	}
}
// System.Void UnityEngine.Events.InvokableCall`4<System.Object,System.Object,System.Object,System.Object>::.ctor(System.Object,System.Reflection.MethodInfo)
extern "C"  void InvokableCall_4__ctor_m4061686552_gshared (InvokableCall_4_t304489837 * __this, RuntimeObject * ___target0, MethodInfo_t * ___theFunction1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InvokableCall_4__ctor_m4061686552_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		RuntimeObject * L_0 = ___target0;
		MethodInfo_t * L_1 = ___theFunction1;
		NullCheck((BaseInvokableCall_t1352930348 *)__this);
		BaseInvokableCall__ctor_m2708238577((BaseInvokableCall_t1352930348 *)__this, (RuntimeObject *)L_0, (MethodInfo_t *)L_1, /*hidden argument*/NULL);
		MethodInfo_t * L_2 = ___theFunction1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m269683661(NULL /*static, unused*/, (RuntimeTypeHandle_t845633806 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 0)), /*hidden argument*/NULL);
		RuntimeObject * L_4 = ___target0;
		Delegate_t537306192 * L_5 = NetFxCoreExtensions_CreateDelegate_m4287214728(NULL /*static, unused*/, (MethodInfo_t *)L_2, (Type_t *)L_3, (RuntimeObject *)L_4, /*hidden argument*/NULL);
		__this->set_Delegate_0(((UnityAction_4_t226130048 *)Castclass((RuntimeObject*)L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1))));
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCall`4<System.Object,System.Object,System.Object,System.Object>::Invoke(System.Object[])
extern "C"  void InvokableCall_4_Invoke_m3667773034_gshared (InvokableCall_4_t304489837 * __this, ObjectU5BU5D_t3270211303* ___args0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InvokableCall_4_Invoke_m3667773034_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		ObjectU5BU5D_t3270211303* L_0 = ___args0;
		NullCheck(L_0);
		if ((((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_0)->max_length))))) == ((int32_t)4)))
		{
			goto IL_0015;
		}
	}
	{
		ArgumentException_t4028401650 * L_1 = (ArgumentException_t4028401650 *)il2cpp_codegen_object_new(ArgumentException_t4028401650_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m1186658332(L_1, (String_t*)_stringLiteral836720390, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0015:
	{
		ObjectU5BU5D_t3270211303* L_2 = ___args0;
		NullCheck(L_2);
		int32_t L_3 = 0;
		RuntimeObject * L_4 = (L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_3));
		((  void (*) (RuntimeObject * /* static, unused */, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)(NULL /*static, unused*/, (RuntimeObject *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		ObjectU5BU5D_t3270211303* L_5 = ___args0;
		NullCheck(L_5);
		int32_t L_6 = 1;
		RuntimeObject * L_7 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_6));
		((  void (*) (RuntimeObject * /* static, unused */, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)(NULL /*static, unused*/, (RuntimeObject *)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		ObjectU5BU5D_t3270211303* L_8 = ___args0;
		NullCheck(L_8);
		int32_t L_9 = 2;
		RuntimeObject * L_10 = (L_8)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		((  void (*) (RuntimeObject * /* static, unused */, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)(NULL /*static, unused*/, (RuntimeObject *)L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		ObjectU5BU5D_t3270211303* L_11 = ___args0;
		NullCheck(L_11);
		int32_t L_12 = 3;
		RuntimeObject * L_13 = (L_11)->GetAt(static_cast<il2cpp_array_size_t>(L_12));
		((  void (*) (RuntimeObject * /* static, unused */, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)(NULL /*static, unused*/, (RuntimeObject *)L_13, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		UnityAction_4_t226130048 * L_14 = (UnityAction_4_t226130048 *)__this->get_Delegate_0();
		bool L_15 = BaseInvokableCall_AllowInvoke_m3827668627(NULL /*static, unused*/, (Delegate_t537306192 *)L_14, /*hidden argument*/NULL);
		if (!L_15)
		{
			goto IL_0070;
		}
	}
	{
		UnityAction_4_t226130048 * L_16 = (UnityAction_4_t226130048 *)__this->get_Delegate_0();
		ObjectU5BU5D_t3270211303* L_17 = ___args0;
		NullCheck(L_17);
		int32_t L_18 = 0;
		RuntimeObject * L_19 = (L_17)->GetAt(static_cast<il2cpp_array_size_t>(L_18));
		ObjectU5BU5D_t3270211303* L_20 = ___args0;
		NullCheck(L_20);
		int32_t L_21 = 1;
		RuntimeObject * L_22 = (L_20)->GetAt(static_cast<il2cpp_array_size_t>(L_21));
		ObjectU5BU5D_t3270211303* L_23 = ___args0;
		NullCheck(L_23);
		int32_t L_24 = 2;
		RuntimeObject * L_25 = (L_23)->GetAt(static_cast<il2cpp_array_size_t>(L_24));
		ObjectU5BU5D_t3270211303* L_26 = ___args0;
		NullCheck(L_26);
		int32_t L_27 = 3;
		RuntimeObject * L_28 = (L_26)->GetAt(static_cast<il2cpp_array_size_t>(L_27));
		NullCheck((UnityAction_4_t226130048 *)L_16);
		((  void (*) (UnityAction_4_t226130048 *, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10)->methodPointer)((UnityAction_4_t226130048 *)L_16, (RuntimeObject *)((RuntimeObject *)Castclass((RuntimeObject*)L_19, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))), (RuntimeObject *)((RuntimeObject *)Castclass((RuntimeObject*)L_22, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 7))), (RuntimeObject *)((RuntimeObject *)Castclass((RuntimeObject*)L_25, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8))), (RuntimeObject *)((RuntimeObject *)Castclass((RuntimeObject*)L_28, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 9))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10));
	}

IL_0070:
	{
		return;
	}
}
// System.Boolean UnityEngine.Events.InvokableCall`4<System.Object,System.Object,System.Object,System.Object>::Find(System.Object,System.Reflection.MethodInfo)
extern "C"  bool InvokableCall_4_Find_m634826185_gshared (InvokableCall_4_t304489837 * __this, RuntimeObject * ___targetObj0, MethodInfo_t * ___method1, const RuntimeMethod* method)
{
	bool V_0 = false;
	int32_t G_B3_0 = 0;
	{
		UnityAction_4_t226130048 * L_0 = (UnityAction_4_t226130048 *)__this->get_Delegate_0();
		NullCheck((Delegate_t537306192 *)L_0);
		RuntimeObject * L_1 = Delegate_get_Target_m2013817205((Delegate_t537306192 *)L_0, /*hidden argument*/NULL);
		RuntimeObject * L_2 = ___targetObj0;
		if ((!(((RuntimeObject*)(RuntimeObject *)L_1) == ((RuntimeObject*)(RuntimeObject *)L_2))))
		{
			goto IL_0025;
		}
	}
	{
		UnityAction_4_t226130048 * L_3 = (UnityAction_4_t226130048 *)__this->get_Delegate_0();
		MethodInfo_t * L_4 = NetFxCoreExtensions_GetMethodInfo_m1250622137(NULL /*static, unused*/, (Delegate_t537306192 *)L_3, /*hidden argument*/NULL);
		MethodInfo_t * L_5 = ___method1;
		NullCheck((RuntimeObject *)L_4);
		bool L_6 = VirtFuncInvoker1< bool, RuntimeObject * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, (RuntimeObject *)L_4, (RuntimeObject *)L_5);
		G_B3_0 = ((int32_t)(L_6));
		goto IL_0026;
	}

IL_0025:
	{
		G_B3_0 = 0;
	}

IL_0026:
	{
		V_0 = (bool)G_B3_0;
		goto IL_002c;
	}

IL_002c:
	{
		bool L_7 = V_0;
		return L_7;
	}
}
// System.Void UnityEngine.Events.UnityAction`1<System.Boolean>::.ctor(System.Object,System.IntPtr)
extern "C"  void UnityAction_1__ctor_m3632717045_gshared (UnityAction_1_t2203695861 * __this, RuntimeObject * ___object0, IntPtr_t ___method1, const RuntimeMethod* method)
{
	__this->set_method_ptr_0(il2cpp_codegen_get_method_pointer((RuntimeMethod*)___method1.get_m_value_0()));
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UnityEngine.Events.UnityAction`1<System.Boolean>::Invoke(T0)
extern "C"  void UnityAction_1_Invoke_m2131770343_gshared (UnityAction_1_t2203695861 * __this, bool ___arg00, const RuntimeMethod* method)
{
	if(__this->get_prev_9() != NULL)
	{
		UnityAction_1_Invoke_m2131770343((UnityAction_1_t2203695861 *)__this->get_prev_9(),___arg00, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (RuntimeObject *, void* __this, bool ___arg00, const RuntimeMethod* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___arg00,(RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, bool ___arg00, const RuntimeMethod* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___arg00,(RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult UnityEngine.Events.UnityAction`1<System.Boolean>::BeginInvoke(T0,System.AsyncCallback,System.Object)
extern "C"  RuntimeObject* UnityAction_1_BeginInvoke_m2961385523_gshared (UnityAction_1_t2203695861 * __this, bool ___arg00, AsyncCallback_t1057242265 * ___callback1, RuntimeObject * ___object2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityAction_1_BeginInvoke_m2961385523_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Boolean_t2950845069_il2cpp_TypeInfo_var, &___arg00);
	return (RuntimeObject*)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (RuntimeObject*)___object2);
}
// System.Void UnityEngine.Events.UnityAction`1<System.Boolean>::EndInvoke(System.IAsyncResult)
extern "C"  void UnityAction_1_EndInvoke_m1028016382_gshared (UnityAction_1_t2203695861 * __this, RuntimeObject* ___result0, const RuntimeMethod* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void UnityEngine.Events.UnityAction`1<System.Int32>::.ctor(System.Object,System.IntPtr)
extern "C"  void UnityAction_1__ctor_m4176351641_gshared (UnityAction_1_t3490795381 * __this, RuntimeObject * ___object0, IntPtr_t ___method1, const RuntimeMethod* method)
{
	__this->set_method_ptr_0(il2cpp_codegen_get_method_pointer((RuntimeMethod*)___method1.get_m_value_0()));
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UnityEngine.Events.UnityAction`1<System.Int32>::Invoke(T0)
extern "C"  void UnityAction_1_Invoke_m1109029515_gshared (UnityAction_1_t3490795381 * __this, int32_t ___arg00, const RuntimeMethod* method)
{
	if(__this->get_prev_9() != NULL)
	{
		UnityAction_1_Invoke_m1109029515((UnityAction_1_t3490795381 *)__this->get_prev_9(),___arg00, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (RuntimeObject *, void* __this, int32_t ___arg00, const RuntimeMethod* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___arg00,(RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, int32_t ___arg00, const RuntimeMethod* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___arg00,(RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult UnityEngine.Events.UnityAction`1<System.Int32>::BeginInvoke(T0,System.AsyncCallback,System.Object)
extern "C"  RuntimeObject* UnityAction_1_BeginInvoke_m442003415_gshared (UnityAction_1_t3490795381 * __this, int32_t ___arg00, AsyncCallback_t1057242265 * ___callback1, RuntimeObject * ___object2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityAction_1_BeginInvoke_m442003415_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Int32_t4237944589_il2cpp_TypeInfo_var, &___arg00);
	return (RuntimeObject*)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (RuntimeObject*)___object2);
}
// System.Void UnityEngine.Events.UnityAction`1<System.Int32>::EndInvoke(System.IAsyncResult)
extern "C"  void UnityAction_1_EndInvoke_m442968386_gshared (UnityAction_1_t3490795381 * __this, RuntimeObject* ___result0, const RuntimeMethod* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void UnityEngine.Events.UnityAction`1<System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void UnityAction_1__ctor_m603530557_gshared (UnityAction_1_t1274809082 * __this, RuntimeObject * ___object0, IntPtr_t ___method1, const RuntimeMethod* method)
{
	__this->set_method_ptr_0(il2cpp_codegen_get_method_pointer((RuntimeMethod*)___method1.get_m_value_0()));
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UnityEngine.Events.UnityAction`1<System.Object>::Invoke(T0)
extern "C"  void UnityAction_1_Invoke_m1618713631_gshared (UnityAction_1_t1274809082 * __this, RuntimeObject * ___arg00, const RuntimeMethod* method)
{
	if(__this->get_prev_9() != NULL)
	{
		UnityAction_1_Invoke_m1618713631((UnityAction_1_t1274809082 *)__this->get_prev_9(),___arg00, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (RuntimeObject *, void* __this, RuntimeObject * ___arg00, const RuntimeMethod* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___arg00,(RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (void* __this, RuntimeObject * ___arg00, const RuntimeMethod* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___arg00,(RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, const RuntimeMethod* method);
		((FunctionPointerType)__this->get_method_ptr_0())(___arg00,(RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult UnityEngine.Events.UnityAction`1<System.Object>::BeginInvoke(T0,System.AsyncCallback,System.Object)
extern "C"  RuntimeObject* UnityAction_1_BeginInvoke_m53378174_gshared (UnityAction_1_t1274809082 * __this, RuntimeObject * ___arg00, AsyncCallback_t1057242265 * ___callback1, RuntimeObject * ___object2, const RuntimeMethod* method)
{
	void *__d_args[2] = {0};
	__d_args[0] = ___arg00;
	return (RuntimeObject*)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (RuntimeObject*)___object2);
}
// System.Void UnityEngine.Events.UnityAction`1<System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  void UnityAction_1_EndInvoke_m4258177151_gshared (UnityAction_1_t1274809082 * __this, RuntimeObject* ___result0, const RuntimeMethod* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void UnityEngine.Events.UnityAction`1<System.Single>::.ctor(System.Object,System.IntPtr)
extern "C"  void UnityAction_1__ctor_m4169503577_gshared (UnityAction_1_t2298200551 * __this, RuntimeObject * ___object0, IntPtr_t ___method1, const RuntimeMethod* method)
{
	__this->set_method_ptr_0(il2cpp_codegen_get_method_pointer((RuntimeMethod*)___method1.get_m_value_0()));
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UnityEngine.Events.UnityAction`1<System.Single>::Invoke(T0)
extern "C"  void UnityAction_1_Invoke_m3238493645_gshared (UnityAction_1_t2298200551 * __this, float ___arg00, const RuntimeMethod* method)
{
	if(__this->get_prev_9() != NULL)
	{
		UnityAction_1_Invoke_m3238493645((UnityAction_1_t2298200551 *)__this->get_prev_9(),___arg00, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (RuntimeObject *, void* __this, float ___arg00, const RuntimeMethod* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___arg00,(RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, float ___arg00, const RuntimeMethod* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___arg00,(RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult UnityEngine.Events.UnityAction`1<System.Single>::BeginInvoke(T0,System.AsyncCallback,System.Object)
extern "C"  RuntimeObject* UnityAction_1_BeginInvoke_m2756264811_gshared (UnityAction_1_t2298200551 * __this, float ___arg00, AsyncCallback_t1057242265 * ___callback1, RuntimeObject * ___object2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityAction_1_BeginInvoke_m2756264811_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Single_t3045349759_il2cpp_TypeInfo_var, &___arg00);
	return (RuntimeObject*)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (RuntimeObject*)___object2);
}
// System.Void UnityEngine.Events.UnityAction`1<System.Single>::EndInvoke(System.IAsyncResult)
extern "C"  void UnityAction_1_EndInvoke_m1049008212_gshared (UnityAction_1_t2298200551 * __this, RuntimeObject* ___result0, const RuntimeMethod* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.Color>::.ctor(System.Object,System.IntPtr)
extern "C"  void UnityAction_1__ctor_m3677349274_gshared (UnityAction_1_t1759877202 * __this, RuntimeObject * ___object0, IntPtr_t ___method1, const RuntimeMethod* method)
{
	__this->set_method_ptr_0(il2cpp_codegen_get_method_pointer((RuntimeMethod*)___method1.get_m_value_0()));
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.Color>::Invoke(T0)
extern "C"  void UnityAction_1_Invoke_m3221612891_gshared (UnityAction_1_t1759877202 * __this, Color_t2507026410  ___arg00, const RuntimeMethod* method)
{
	if(__this->get_prev_9() != NULL)
	{
		UnityAction_1_Invoke_m3221612891((UnityAction_1_t1759877202 *)__this->get_prev_9(),___arg00, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (RuntimeObject *, void* __this, Color_t2507026410  ___arg00, const RuntimeMethod* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___arg00,(RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, Color_t2507026410  ___arg00, const RuntimeMethod* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___arg00,(RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult UnityEngine.Events.UnityAction`1<UnityEngine.Color>::BeginInvoke(T0,System.AsyncCallback,System.Object)
extern "C"  RuntimeObject* UnityAction_1_BeginInvoke_m430810788_gshared (UnityAction_1_t1759877202 * __this, Color_t2507026410  ___arg00, AsyncCallback_t1057242265 * ___callback1, RuntimeObject * ___object2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityAction_1_BeginInvoke_m430810788_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Color_t2507026410_il2cpp_TypeInfo_var, &___arg00);
	return (RuntimeObject*)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (RuntimeObject*)___object2);
}
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.Color>::EndInvoke(System.IAsyncResult)
extern "C"  void UnityAction_1_EndInvoke_m513956045_gshared (UnityAction_1_t1759877202 * __this, RuntimeObject* ___result0, const RuntimeMethod* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.SceneManagement.Scene>::.ctor(System.Object,System.IntPtr)
extern "C"  void UnityAction_1__ctor_m2429228119_gshared (UnityAction_1_t3896475417 * __this, RuntimeObject * ___object0, IntPtr_t ___method1, const RuntimeMethod* method)
{
	__this->set_method_ptr_0(il2cpp_codegen_get_method_pointer((RuntimeMethod*)___method1.get_m_value_0()));
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.SceneManagement.Scene>::Invoke(T0)
extern "C"  void UnityAction_1_Invoke_m2393581145_gshared (UnityAction_1_t3896475417 * __this, Scene_t348657329  ___arg00, const RuntimeMethod* method)
{
	if(__this->get_prev_9() != NULL)
	{
		UnityAction_1_Invoke_m2393581145((UnityAction_1_t3896475417 *)__this->get_prev_9(),___arg00, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (RuntimeObject *, void* __this, Scene_t348657329  ___arg00, const RuntimeMethod* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___arg00,(RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, Scene_t348657329  ___arg00, const RuntimeMethod* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___arg00,(RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult UnityEngine.Events.UnityAction`1<UnityEngine.SceneManagement.Scene>::BeginInvoke(T0,System.AsyncCallback,System.Object)
extern "C"  RuntimeObject* UnityAction_1_BeginInvoke_m3340556640_gshared (UnityAction_1_t3896475417 * __this, Scene_t348657329  ___arg00, AsyncCallback_t1057242265 * ___callback1, RuntimeObject * ___object2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityAction_1_BeginInvoke_m3340556640_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Scene_t348657329_il2cpp_TypeInfo_var, &___arg00);
	return (RuntimeObject*)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (RuntimeObject*)___object2);
}
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.SceneManagement.Scene>::EndInvoke(System.IAsyncResult)
extern "C"  void UnityAction_1_EndInvoke_m3014669578_gshared (UnityAction_1_t3896475417 * __this, RuntimeObject* ___result0, const RuntimeMethod* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.Vector2>::.ctor(System.Object,System.IntPtr)
extern "C"  void UnityAction_1__ctor_m2572018435_gshared (UnityAction_1_t2830184054 * __this, RuntimeObject * ___object0, IntPtr_t ___method1, const RuntimeMethod* method)
{
	__this->set_method_ptr_0(il2cpp_codegen_get_method_pointer((RuntimeMethod*)___method1.get_m_value_0()));
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.Vector2>::Invoke(T0)
extern "C"  void UnityAction_1_Invoke_m1552629663_gshared (UnityAction_1_t2830184054 * __this, Vector2_t3577333262  ___arg00, const RuntimeMethod* method)
{
	if(__this->get_prev_9() != NULL)
	{
		UnityAction_1_Invoke_m1552629663((UnityAction_1_t2830184054 *)__this->get_prev_9(),___arg00, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (RuntimeObject *, void* __this, Vector2_t3577333262  ___arg00, const RuntimeMethod* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___arg00,(RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, Vector2_t3577333262  ___arg00, const RuntimeMethod* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___arg00,(RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult UnityEngine.Events.UnityAction`1<UnityEngine.Vector2>::BeginInvoke(T0,System.AsyncCallback,System.Object)
extern "C"  RuntimeObject* UnityAction_1_BeginInvoke_m3679434010_gshared (UnityAction_1_t2830184054 * __this, Vector2_t3577333262  ___arg00, AsyncCallback_t1057242265 * ___callback1, RuntimeObject * ___object2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityAction_1_BeginInvoke_m3679434010_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Vector2_t3577333262_il2cpp_TypeInfo_var, &___arg00);
	return (RuntimeObject*)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (RuntimeObject*)___object2);
}
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.Vector2>::EndInvoke(System.IAsyncResult)
extern "C"  void UnityAction_1_EndInvoke_m2165102285_gshared (UnityAction_1_t2830184054 * __this, RuntimeObject* ___result0, const RuntimeMethod* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void UnityEngine.Events.UnityAction`2<System.Object,System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void UnityAction_2__ctor_m3048295632_gshared (UnityAction_2_t1132847070 * __this, RuntimeObject * ___object0, IntPtr_t ___method1, const RuntimeMethod* method)
{
	__this->set_method_ptr_0(il2cpp_codegen_get_method_pointer((RuntimeMethod*)___method1.get_m_value_0()));
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UnityEngine.Events.UnityAction`2<System.Object,System.Object>::Invoke(T0,T1)
extern "C"  void UnityAction_2_Invoke_m321356426_gshared (UnityAction_2_t1132847070 * __this, RuntimeObject * ___arg00, RuntimeObject * ___arg11, const RuntimeMethod* method)
{
	if(__this->get_prev_9() != NULL)
	{
		UnityAction_2_Invoke_m321356426((UnityAction_2_t1132847070 *)__this->get_prev_9(),___arg00, ___arg11, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (RuntimeObject *, void* __this, RuntimeObject * ___arg00, RuntimeObject * ___arg11, const RuntimeMethod* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___arg00, ___arg11,(RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (void* __this, RuntimeObject * ___arg00, RuntimeObject * ___arg11, const RuntimeMethod* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___arg00, ___arg11,(RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, RuntimeObject * ___arg11, const RuntimeMethod* method);
		((FunctionPointerType)__this->get_method_ptr_0())(___arg00, ___arg11,(RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult UnityEngine.Events.UnityAction`2<System.Object,System.Object>::BeginInvoke(T0,T1,System.AsyncCallback,System.Object)
extern "C"  RuntimeObject* UnityAction_2_BeginInvoke_m116305060_gshared (UnityAction_2_t1132847070 * __this, RuntimeObject * ___arg00, RuntimeObject * ___arg11, AsyncCallback_t1057242265 * ___callback2, RuntimeObject * ___object3, const RuntimeMethod* method)
{
	void *__d_args[3] = {0};
	__d_args[0] = ___arg00;
	__d_args[1] = ___arg11;
	return (RuntimeObject*)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (RuntimeObject*)___object3);
}
// System.Void UnityEngine.Events.UnityAction`2<System.Object,System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  void UnityAction_2_EndInvoke_m3208053362_gshared (UnityAction_2_t1132847070 * __this, RuntimeObject* ___result0, const RuntimeMethod* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void UnityEngine.Events.UnityAction`2<UnityEngine.SceneManagement.Scene,UnityEngine.SceneManagement.LoadSceneMode>::.ctor(System.Object,System.IntPtr)
extern "C"  void UnityAction_2__ctor_m3178219255_gshared (UnityAction_2_t2064023076 * __this, RuntimeObject * ___object0, IntPtr_t ___method1, const RuntimeMethod* method)
{
	__this->set_method_ptr_0(il2cpp_codegen_get_method_pointer((RuntimeMethod*)___method1.get_m_value_0()));
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UnityEngine.Events.UnityAction`2<UnityEngine.SceneManagement.Scene,UnityEngine.SceneManagement.LoadSceneMode>::Invoke(T0,T1)
extern "C"  void UnityAction_2_Invoke_m2774749000_gshared (UnityAction_2_t2064023076 * __this, Scene_t348657329  ___arg00, int32_t ___arg11, const RuntimeMethod* method)
{
	if(__this->get_prev_9() != NULL)
	{
		UnityAction_2_Invoke_m2774749000((UnityAction_2_t2064023076 *)__this->get_prev_9(),___arg00, ___arg11, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (RuntimeObject *, void* __this, Scene_t348657329  ___arg00, int32_t ___arg11, const RuntimeMethod* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___arg00, ___arg11,(RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, Scene_t348657329  ___arg00, int32_t ___arg11, const RuntimeMethod* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___arg00, ___arg11,(RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult UnityEngine.Events.UnityAction`2<UnityEngine.SceneManagement.Scene,UnityEngine.SceneManagement.LoadSceneMode>::BeginInvoke(T0,T1,System.AsyncCallback,System.Object)
extern "C"  RuntimeObject* UnityAction_2_BeginInvoke_m1047322930_gshared (UnityAction_2_t2064023076 * __this, Scene_t348657329  ___arg00, int32_t ___arg11, AsyncCallback_t1057242265 * ___callback2, RuntimeObject * ___object3, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityAction_2_BeginInvoke_m1047322930_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(Scene_t348657329_il2cpp_TypeInfo_var, &___arg00);
	__d_args[1] = Box(LoadSceneMode_t2291092563_il2cpp_TypeInfo_var, &___arg11);
	return (RuntimeObject*)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (RuntimeObject*)___object3);
}
// System.Void UnityEngine.Events.UnityAction`2<UnityEngine.SceneManagement.Scene,UnityEngine.SceneManagement.LoadSceneMode>::EndInvoke(System.IAsyncResult)
extern "C"  void UnityAction_2_EndInvoke_m2184149813_gshared (UnityAction_2_t2064023076 * __this, RuntimeObject* ___result0, const RuntimeMethod* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void UnityEngine.Events.UnityAction`2<UnityEngine.SceneManagement.Scene,UnityEngine.SceneManagement.Scene>::.ctor(System.Object,System.IntPtr)
extern "C"  void UnityAction_2__ctor_m3238258875_gshared (UnityAction_2_t121587842 * __this, RuntimeObject * ___object0, IntPtr_t ___method1, const RuntimeMethod* method)
{
	__this->set_method_ptr_0(il2cpp_codegen_get_method_pointer((RuntimeMethod*)___method1.get_m_value_0()));
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UnityEngine.Events.UnityAction`2<UnityEngine.SceneManagement.Scene,UnityEngine.SceneManagement.Scene>::Invoke(T0,T1)
extern "C"  void UnityAction_2_Invoke_m3185242217_gshared (UnityAction_2_t121587842 * __this, Scene_t348657329  ___arg00, Scene_t348657329  ___arg11, const RuntimeMethod* method)
{
	if(__this->get_prev_9() != NULL)
	{
		UnityAction_2_Invoke_m3185242217((UnityAction_2_t121587842 *)__this->get_prev_9(),___arg00, ___arg11, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (RuntimeObject *, void* __this, Scene_t348657329  ___arg00, Scene_t348657329  ___arg11, const RuntimeMethod* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___arg00, ___arg11,(RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, Scene_t348657329  ___arg00, Scene_t348657329  ___arg11, const RuntimeMethod* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___arg00, ___arg11,(RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult UnityEngine.Events.UnityAction`2<UnityEngine.SceneManagement.Scene,UnityEngine.SceneManagement.Scene>::BeginInvoke(T0,T1,System.AsyncCallback,System.Object)
extern "C"  RuntimeObject* UnityAction_2_BeginInvoke_m3900960504_gshared (UnityAction_2_t121587842 * __this, Scene_t348657329  ___arg00, Scene_t348657329  ___arg11, AsyncCallback_t1057242265 * ___callback2, RuntimeObject * ___object3, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityAction_2_BeginInvoke_m3900960504_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(Scene_t348657329_il2cpp_TypeInfo_var, &___arg00);
	__d_args[1] = Box(Scene_t348657329_il2cpp_TypeInfo_var, &___arg11);
	return (RuntimeObject*)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (RuntimeObject*)___object3);
}
// System.Void UnityEngine.Events.UnityAction`2<UnityEngine.SceneManagement.Scene,UnityEngine.SceneManagement.Scene>::EndInvoke(System.IAsyncResult)
extern "C"  void UnityAction_2_EndInvoke_m3152085177_gshared (UnityAction_2_t121587842 * __this, RuntimeObject* ___result0, const RuntimeMethod* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void UnityEngine.Events.UnityAction`3<System.Object,System.Object,System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void UnityAction_3__ctor_m1573759626_gshared (UnityAction_3_t1794137105 * __this, RuntimeObject * ___object0, IntPtr_t ___method1, const RuntimeMethod* method)
{
	__this->set_method_ptr_0(il2cpp_codegen_get_method_pointer((RuntimeMethod*)___method1.get_m_value_0()));
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UnityEngine.Events.UnityAction`3<System.Object,System.Object,System.Object>::Invoke(T0,T1,T2)
extern "C"  void UnityAction_3_Invoke_m2022456289_gshared (UnityAction_3_t1794137105 * __this, RuntimeObject * ___arg00, RuntimeObject * ___arg11, RuntimeObject * ___arg22, const RuntimeMethod* method)
{
	if(__this->get_prev_9() != NULL)
	{
		UnityAction_3_Invoke_m2022456289((UnityAction_3_t1794137105 *)__this->get_prev_9(),___arg00, ___arg11, ___arg22, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (RuntimeObject *, void* __this, RuntimeObject * ___arg00, RuntimeObject * ___arg11, RuntimeObject * ___arg22, const RuntimeMethod* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___arg00, ___arg11, ___arg22,(RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (void* __this, RuntimeObject * ___arg00, RuntimeObject * ___arg11, RuntimeObject * ___arg22, const RuntimeMethod* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___arg00, ___arg11, ___arg22,(RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, RuntimeObject * ___arg11, RuntimeObject * ___arg22, const RuntimeMethod* method);
		((FunctionPointerType)__this->get_method_ptr_0())(___arg00, ___arg11, ___arg22,(RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult UnityEngine.Events.UnityAction`3<System.Object,System.Object,System.Object>::BeginInvoke(T0,T1,T2,System.AsyncCallback,System.Object)
extern "C"  RuntimeObject* UnityAction_3_BeginInvoke_m2204076281_gshared (UnityAction_3_t1794137105 * __this, RuntimeObject * ___arg00, RuntimeObject * ___arg11, RuntimeObject * ___arg22, AsyncCallback_t1057242265 * ___callback3, RuntimeObject * ___object4, const RuntimeMethod* method)
{
	void *__d_args[4] = {0};
	__d_args[0] = ___arg00;
	__d_args[1] = ___arg11;
	__d_args[2] = ___arg22;
	return (RuntimeObject*)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback3, (RuntimeObject*)___object4);
}
// System.Void UnityEngine.Events.UnityAction`3<System.Object,System.Object,System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  void UnityAction_3_EndInvoke_m3161047250_gshared (UnityAction_3_t1794137105 * __this, RuntimeObject* ___result0, const RuntimeMethod* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void UnityEngine.Events.UnityAction`4<System.Object,System.Object,System.Object,System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void UnityAction_4__ctor_m2844711482_gshared (UnityAction_4_t226130048 * __this, RuntimeObject * ___object0, IntPtr_t ___method1, const RuntimeMethod* method)
{
	__this->set_method_ptr_0(il2cpp_codegen_get_method_pointer((RuntimeMethod*)___method1.get_m_value_0()));
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UnityEngine.Events.UnityAction`4<System.Object,System.Object,System.Object,System.Object>::Invoke(T0,T1,T2,T3)
extern "C"  void UnityAction_4_Invoke_m2730965101_gshared (UnityAction_4_t226130048 * __this, RuntimeObject * ___arg00, RuntimeObject * ___arg11, RuntimeObject * ___arg22, RuntimeObject * ___arg33, const RuntimeMethod* method)
{
	if(__this->get_prev_9() != NULL)
	{
		UnityAction_4_Invoke_m2730965101((UnityAction_4_t226130048 *)__this->get_prev_9(),___arg00, ___arg11, ___arg22, ___arg33, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (RuntimeObject *, void* __this, RuntimeObject * ___arg00, RuntimeObject * ___arg11, RuntimeObject * ___arg22, RuntimeObject * ___arg33, const RuntimeMethod* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___arg00, ___arg11, ___arg22, ___arg33,(RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (void* __this, RuntimeObject * ___arg00, RuntimeObject * ___arg11, RuntimeObject * ___arg22, RuntimeObject * ___arg33, const RuntimeMethod* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___arg00, ___arg11, ___arg22, ___arg33,(RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, RuntimeObject * ___arg11, RuntimeObject * ___arg22, RuntimeObject * ___arg33, const RuntimeMethod* method);
		((FunctionPointerType)__this->get_method_ptr_0())(___arg00, ___arg11, ___arg22, ___arg33,(RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult UnityEngine.Events.UnityAction`4<System.Object,System.Object,System.Object,System.Object>::BeginInvoke(T0,T1,T2,T3,System.AsyncCallback,System.Object)
extern "C"  RuntimeObject* UnityAction_4_BeginInvoke_m63696548_gshared (UnityAction_4_t226130048 * __this, RuntimeObject * ___arg00, RuntimeObject * ___arg11, RuntimeObject * ___arg22, RuntimeObject * ___arg33, AsyncCallback_t1057242265 * ___callback4, RuntimeObject * ___object5, const RuntimeMethod* method)
{
	void *__d_args[5] = {0};
	__d_args[0] = ___arg00;
	__d_args[1] = ___arg11;
	__d_args[2] = ___arg22;
	__d_args[3] = ___arg33;
	return (RuntimeObject*)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback4, (RuntimeObject*)___object5);
}
// System.Void UnityEngine.Events.UnityAction`4<System.Object,System.Object,System.Object,System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  void UnityAction_4_EndInvoke_m3245382934_gshared (UnityAction_4_t226130048 * __this, RuntimeObject* ___result0, const RuntimeMethod* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void UnityEngine.Events.UnityEvent`1<System.Boolean>::.ctor()
extern "C"  void UnityEvent_1__ctor_m3814196951_gshared (UnityEvent_1_t4001855637 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityEvent_1__ctor_m3814196951_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set_m_InvokeArray_4(((ObjectU5BU5D_t3270211303*)SZArrayNew(ObjectU5BU5D_t3270211303_il2cpp_TypeInfo_var, (uint32_t)1)));
		NullCheck((UnityEventBase_t2008500469 *)__this);
		UnityEventBase__ctor_m821375546((UnityEventBase_t2008500469 *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Events.UnityEvent`1<System.Boolean>::AddListener(UnityEngine.Events.UnityAction`1<T0>)
extern "C"  void UnityEvent_1_AddListener_m3717414295_gshared (UnityEvent_1_t4001855637 * __this, UnityAction_1_t2203695861 * ___call0, const RuntimeMethod* method)
{
	{
		UnityAction_1_t2203695861 * L_0 = ___call0;
		BaseInvokableCall_t1352930348 * L_1 = ((  BaseInvokableCall_t1352930348 * (*) (RuntimeObject * /* static, unused */, UnityAction_1_t2203695861 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)(NULL /*static, unused*/, (UnityAction_1_t2203695861 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		NullCheck((UnityEventBase_t2008500469 *)__this);
		UnityEventBase_AddCall_m3837898691((UnityEventBase_t2008500469 *)__this, (BaseInvokableCall_t1352930348 *)L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Events.UnityEvent`1<System.Boolean>::RemoveListener(UnityEngine.Events.UnityAction`1<T0>)
extern "C"  void UnityEvent_1_RemoveListener_m1633148677_gshared (UnityEvent_1_t4001855637 * __this, UnityAction_1_t2203695861 * ___call0, const RuntimeMethod* method)
{
	{
		UnityAction_1_t2203695861 * L_0 = ___call0;
		NullCheck((Delegate_t537306192 *)L_0);
		RuntimeObject * L_1 = Delegate_get_Target_m2013817205((Delegate_t537306192 *)L_0, /*hidden argument*/NULL);
		UnityAction_1_t2203695861 * L_2 = ___call0;
		MethodInfo_t * L_3 = NetFxCoreExtensions_GetMethodInfo_m1250622137(NULL /*static, unused*/, (Delegate_t537306192 *)L_2, /*hidden argument*/NULL);
		NullCheck((UnityEventBase_t2008500469 *)__this);
		UnityEventBase_RemoveListener_m2620267480((UnityEventBase_t2008500469 *)__this, (RuntimeObject *)L_1, (MethodInfo_t *)L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Reflection.MethodInfo UnityEngine.Events.UnityEvent`1<System.Boolean>::FindMethod_Impl(System.String,System.Object)
extern "C"  MethodInfo_t * UnityEvent_1_FindMethod_Impl_m194499244_gshared (UnityEvent_1_t4001855637 * __this, String_t* ___name0, RuntimeObject * ___targetObj1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityEvent_1_FindMethod_Impl_m194499244_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	MethodInfo_t * V_0 = NULL;
	{
		RuntimeObject * L_0 = ___targetObj1;
		String_t* L_1 = ___name0;
		TypeU5BU5D_t3854702846* L_2 = (TypeU5BU5D_t3854702846*)((TypeU5BU5D_t3854702846*)SZArrayNew(TypeU5BU5D_t3854702846_il2cpp_TypeInfo_var, (uint32_t)1));
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m269683661(NULL /*static, unused*/, (RuntimeTypeHandle_t845633806 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 2)), /*hidden argument*/NULL);
		NullCheck(L_2);
		ArrayElementTypeCheck (L_2, L_3);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_3);
		MethodInfo_t * L_4 = UnityEventBase_GetValidMethodInfo_m2594745603(NULL /*static, unused*/, (RuntimeObject *)L_0, (String_t*)L_1, (TypeU5BU5D_t3854702846*)L_2, /*hidden argument*/NULL);
		V_0 = (MethodInfo_t *)L_4;
		goto IL_0021;
	}

IL_0021:
	{
		MethodInfo_t * L_5 = V_0;
		return L_5;
	}
}
// UnityEngine.Events.BaseInvokableCall UnityEngine.Events.UnityEvent`1<System.Boolean>::GetDelegate(System.Object,System.Reflection.MethodInfo)
extern "C"  BaseInvokableCall_t1352930348 * UnityEvent_1_GetDelegate_m3331121802_gshared (UnityEvent_1_t4001855637 * __this, RuntimeObject * ___target0, MethodInfo_t * ___theFunction1, const RuntimeMethod* method)
{
	BaseInvokableCall_t1352930348 * V_0 = NULL;
	{
		RuntimeObject * L_0 = ___target0;
		MethodInfo_t * L_1 = ___theFunction1;
		InvokableCall_1_t1183494759 * L_2 = (InvokableCall_1_t1183494759 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3));
		((  void (*) (InvokableCall_1_t1183494759 *, RuntimeObject *, MethodInfo_t *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)(L_2, (RuntimeObject *)L_0, (MethodInfo_t *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		V_0 = (BaseInvokableCall_t1352930348 *)L_2;
		goto IL_000e;
	}

IL_000e:
	{
		BaseInvokableCall_t1352930348 * L_3 = V_0;
		return L_3;
	}
}
// UnityEngine.Events.BaseInvokableCall UnityEngine.Events.UnityEvent`1<System.Boolean>::GetDelegate(UnityEngine.Events.UnityAction`1<T0>)
extern "C"  BaseInvokableCall_t1352930348 * UnityEvent_1_GetDelegate_m3286030885_gshared (RuntimeObject * __this /* static, unused */, UnityAction_1_t2203695861 * ___action0, const RuntimeMethod* method)
{
	BaseInvokableCall_t1352930348 * V_0 = NULL;
	{
		UnityAction_1_t2203695861 * L_0 = ___action0;
		InvokableCall_1_t1183494759 * L_1 = (InvokableCall_1_t1183494759 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		((  void (*) (InvokableCall_1_t1183494759 *, UnityAction_1_t2203695861 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->methodPointer)(L_1, (UnityAction_1_t2203695861 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		V_0 = (BaseInvokableCall_t1352930348 *)L_1;
		goto IL_000d;
	}

IL_000d:
	{
		BaseInvokableCall_t1352930348 * L_2 = V_0;
		return L_2;
	}
}
// System.Void UnityEngine.Events.UnityEvent`1<System.Boolean>::Invoke(T0)
extern "C"  void UnityEvent_1_Invoke_m2110184892_gshared (UnityEvent_1_t4001855637 * __this, bool ___arg00, const RuntimeMethod* method)
{
	{
		ObjectU5BU5D_t3270211303* L_0 = (ObjectU5BU5D_t3270211303*)__this->get_m_InvokeArray_4();
		bool L_1 = ___arg00;
		bool L_2 = L_1;
		RuntimeObject * L_3 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6), &L_2);
		NullCheck(L_0);
		ArrayElementTypeCheck (L_0, L_3);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (RuntimeObject *)L_3);
		ObjectU5BU5D_t3270211303* L_4 = (ObjectU5BU5D_t3270211303*)__this->get_m_InvokeArray_4();
		NullCheck((UnityEventBase_t2008500469 *)__this);
		UnityEventBase_Invoke_m3195354837((UnityEventBase_t2008500469 *)__this, (ObjectU5BU5D_t3270211303*)L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Events.UnityEvent`1<System.Int32>::.ctor()
extern "C"  void UnityEvent_1__ctor_m3605924324_gshared (UnityEvent_1_t993987861 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityEvent_1__ctor_m3605924324_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set_m_InvokeArray_4(((ObjectU5BU5D_t3270211303*)SZArrayNew(ObjectU5BU5D_t3270211303_il2cpp_TypeInfo_var, (uint32_t)1)));
		NullCheck((UnityEventBase_t2008500469 *)__this);
		UnityEventBase__ctor_m821375546((UnityEventBase_t2008500469 *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Events.UnityEvent`1<System.Int32>::AddListener(UnityEngine.Events.UnityAction`1<T0>)
extern "C"  void UnityEvent_1_AddListener_m2921287836_gshared (UnityEvent_1_t993987861 * __this, UnityAction_1_t3490795381 * ___call0, const RuntimeMethod* method)
{
	{
		UnityAction_1_t3490795381 * L_0 = ___call0;
		BaseInvokableCall_t1352930348 * L_1 = ((  BaseInvokableCall_t1352930348 * (*) (RuntimeObject * /* static, unused */, UnityAction_1_t3490795381 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)(NULL /*static, unused*/, (UnityAction_1_t3490795381 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		NullCheck((UnityEventBase_t2008500469 *)__this);
		UnityEventBase_AddCall_m3837898691((UnityEventBase_t2008500469 *)__this, (BaseInvokableCall_t1352930348 *)L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Events.UnityEvent`1<System.Int32>::RemoveListener(UnityEngine.Events.UnityAction`1<T0>)
extern "C"  void UnityEvent_1_RemoveListener_m547578480_gshared (UnityEvent_1_t993987861 * __this, UnityAction_1_t3490795381 * ___call0, const RuntimeMethod* method)
{
	{
		UnityAction_1_t3490795381 * L_0 = ___call0;
		NullCheck((Delegate_t537306192 *)L_0);
		RuntimeObject * L_1 = Delegate_get_Target_m2013817205((Delegate_t537306192 *)L_0, /*hidden argument*/NULL);
		UnityAction_1_t3490795381 * L_2 = ___call0;
		MethodInfo_t * L_3 = NetFxCoreExtensions_GetMethodInfo_m1250622137(NULL /*static, unused*/, (Delegate_t537306192 *)L_2, /*hidden argument*/NULL);
		NullCheck((UnityEventBase_t2008500469 *)__this);
		UnityEventBase_RemoveListener_m2620267480((UnityEventBase_t2008500469 *)__this, (RuntimeObject *)L_1, (MethodInfo_t *)L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Reflection.MethodInfo UnityEngine.Events.UnityEvent`1<System.Int32>::FindMethod_Impl(System.String,System.Object)
extern "C"  MethodInfo_t * UnityEvent_1_FindMethod_Impl_m1672417327_gshared (UnityEvent_1_t993987861 * __this, String_t* ___name0, RuntimeObject * ___targetObj1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityEvent_1_FindMethod_Impl_m1672417327_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	MethodInfo_t * V_0 = NULL;
	{
		RuntimeObject * L_0 = ___targetObj1;
		String_t* L_1 = ___name0;
		TypeU5BU5D_t3854702846* L_2 = (TypeU5BU5D_t3854702846*)((TypeU5BU5D_t3854702846*)SZArrayNew(TypeU5BU5D_t3854702846_il2cpp_TypeInfo_var, (uint32_t)1));
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m269683661(NULL /*static, unused*/, (RuntimeTypeHandle_t845633806 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 2)), /*hidden argument*/NULL);
		NullCheck(L_2);
		ArrayElementTypeCheck (L_2, L_3);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_3);
		MethodInfo_t * L_4 = UnityEventBase_GetValidMethodInfo_m2594745603(NULL /*static, unused*/, (RuntimeObject *)L_0, (String_t*)L_1, (TypeU5BU5D_t3854702846*)L_2, /*hidden argument*/NULL);
		V_0 = (MethodInfo_t *)L_4;
		goto IL_0021;
	}

IL_0021:
	{
		MethodInfo_t * L_5 = V_0;
		return L_5;
	}
}
// UnityEngine.Events.BaseInvokableCall UnityEngine.Events.UnityEvent`1<System.Int32>::GetDelegate(System.Object,System.Reflection.MethodInfo)
extern "C"  BaseInvokableCall_t1352930348 * UnityEvent_1_GetDelegate_m1768097598_gshared (UnityEvent_1_t993987861 * __this, RuntimeObject * ___target0, MethodInfo_t * ___theFunction1, const RuntimeMethod* method)
{
	BaseInvokableCall_t1352930348 * V_0 = NULL;
	{
		RuntimeObject * L_0 = ___target0;
		MethodInfo_t * L_1 = ___theFunction1;
		InvokableCall_1_t2470594279 * L_2 = (InvokableCall_1_t2470594279 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3));
		((  void (*) (InvokableCall_1_t2470594279 *, RuntimeObject *, MethodInfo_t *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)(L_2, (RuntimeObject *)L_0, (MethodInfo_t *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		V_0 = (BaseInvokableCall_t1352930348 *)L_2;
		goto IL_000e;
	}

IL_000e:
	{
		BaseInvokableCall_t1352930348 * L_3 = V_0;
		return L_3;
	}
}
// UnityEngine.Events.BaseInvokableCall UnityEngine.Events.UnityEvent`1<System.Int32>::GetDelegate(UnityEngine.Events.UnityAction`1<T0>)
extern "C"  BaseInvokableCall_t1352930348 * UnityEvent_1_GetDelegate_m4277636302_gshared (RuntimeObject * __this /* static, unused */, UnityAction_1_t3490795381 * ___action0, const RuntimeMethod* method)
{
	BaseInvokableCall_t1352930348 * V_0 = NULL;
	{
		UnityAction_1_t3490795381 * L_0 = ___action0;
		InvokableCall_1_t2470594279 * L_1 = (InvokableCall_1_t2470594279 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		((  void (*) (InvokableCall_1_t2470594279 *, UnityAction_1_t3490795381 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->methodPointer)(L_1, (UnityAction_1_t3490795381 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		V_0 = (BaseInvokableCall_t1352930348 *)L_1;
		goto IL_000d;
	}

IL_000d:
	{
		BaseInvokableCall_t1352930348 * L_2 = V_0;
		return L_2;
	}
}
// System.Void UnityEngine.Events.UnityEvent`1<System.Int32>::Invoke(T0)
extern "C"  void UnityEvent_1_Invoke_m947458304_gshared (UnityEvent_1_t993987861 * __this, int32_t ___arg00, const RuntimeMethod* method)
{
	{
		ObjectU5BU5D_t3270211303* L_0 = (ObjectU5BU5D_t3270211303*)__this->get_m_InvokeArray_4();
		int32_t L_1 = ___arg00;
		int32_t L_2 = L_1;
		RuntimeObject * L_3 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6), &L_2);
		NullCheck(L_0);
		ArrayElementTypeCheck (L_0, L_3);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (RuntimeObject *)L_3);
		ObjectU5BU5D_t3270211303* L_4 = (ObjectU5BU5D_t3270211303*)__this->get_m_InvokeArray_4();
		NullCheck((UnityEventBase_t2008500469 *)__this);
		UnityEventBase_Invoke_m3195354837((UnityEventBase_t2008500469 *)__this, (ObjectU5BU5D_t3270211303*)L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Events.UnityEvent`1<System.Object>::.ctor()
extern "C"  void UnityEvent_1__ctor_m503898863_gshared (UnityEvent_1_t3072968858 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityEvent_1__ctor_m503898863_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set_m_InvokeArray_4(((ObjectU5BU5D_t3270211303*)SZArrayNew(ObjectU5BU5D_t3270211303_il2cpp_TypeInfo_var, (uint32_t)1)));
		NullCheck((UnityEventBase_t2008500469 *)__this);
		UnityEventBase__ctor_m821375546((UnityEventBase_t2008500469 *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Events.UnityEvent`1<System.Object>::AddListener(UnityEngine.Events.UnityAction`1<T0>)
extern "C"  void UnityEvent_1_AddListener_m1517690206_gshared (UnityEvent_1_t3072968858 * __this, UnityAction_1_t1274809082 * ___call0, const RuntimeMethod* method)
{
	{
		UnityAction_1_t1274809082 * L_0 = ___call0;
		BaseInvokableCall_t1352930348 * L_1 = ((  BaseInvokableCall_t1352930348 * (*) (RuntimeObject * /* static, unused */, UnityAction_1_t1274809082 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)(NULL /*static, unused*/, (UnityAction_1_t1274809082 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		NullCheck((UnityEventBase_t2008500469 *)__this);
		UnityEventBase_AddCall_m3837898691((UnityEventBase_t2008500469 *)__this, (BaseInvokableCall_t1352930348 *)L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Events.UnityEvent`1<System.Object>::RemoveListener(UnityEngine.Events.UnityAction`1<T0>)
extern "C"  void UnityEvent_1_RemoveListener_m266185357_gshared (UnityEvent_1_t3072968858 * __this, UnityAction_1_t1274809082 * ___call0, const RuntimeMethod* method)
{
	{
		UnityAction_1_t1274809082 * L_0 = ___call0;
		NullCheck((Delegate_t537306192 *)L_0);
		RuntimeObject * L_1 = Delegate_get_Target_m2013817205((Delegate_t537306192 *)L_0, /*hidden argument*/NULL);
		UnityAction_1_t1274809082 * L_2 = ___call0;
		MethodInfo_t * L_3 = NetFxCoreExtensions_GetMethodInfo_m1250622137(NULL /*static, unused*/, (Delegate_t537306192 *)L_2, /*hidden argument*/NULL);
		NullCheck((UnityEventBase_t2008500469 *)__this);
		UnityEventBase_RemoveListener_m2620267480((UnityEventBase_t2008500469 *)__this, (RuntimeObject *)L_1, (MethodInfo_t *)L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Reflection.MethodInfo UnityEngine.Events.UnityEvent`1<System.Object>::FindMethod_Impl(System.String,System.Object)
extern "C"  MethodInfo_t * UnityEvent_1_FindMethod_Impl_m1914224043_gshared (UnityEvent_1_t3072968858 * __this, String_t* ___name0, RuntimeObject * ___targetObj1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityEvent_1_FindMethod_Impl_m1914224043_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	MethodInfo_t * V_0 = NULL;
	{
		RuntimeObject * L_0 = ___targetObj1;
		String_t* L_1 = ___name0;
		TypeU5BU5D_t3854702846* L_2 = (TypeU5BU5D_t3854702846*)((TypeU5BU5D_t3854702846*)SZArrayNew(TypeU5BU5D_t3854702846_il2cpp_TypeInfo_var, (uint32_t)1));
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m269683661(NULL /*static, unused*/, (RuntimeTypeHandle_t845633806 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 2)), /*hidden argument*/NULL);
		NullCheck(L_2);
		ArrayElementTypeCheck (L_2, L_3);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_3);
		MethodInfo_t * L_4 = UnityEventBase_GetValidMethodInfo_m2594745603(NULL /*static, unused*/, (RuntimeObject *)L_0, (String_t*)L_1, (TypeU5BU5D_t3854702846*)L_2, /*hidden argument*/NULL);
		V_0 = (MethodInfo_t *)L_4;
		goto IL_0021;
	}

IL_0021:
	{
		MethodInfo_t * L_5 = V_0;
		return L_5;
	}
}
// UnityEngine.Events.BaseInvokableCall UnityEngine.Events.UnityEvent`1<System.Object>::GetDelegate(System.Object,System.Reflection.MethodInfo)
extern "C"  BaseInvokableCall_t1352930348 * UnityEvent_1_GetDelegate_m401053184_gshared (UnityEvent_1_t3072968858 * __this, RuntimeObject * ___target0, MethodInfo_t * ___theFunction1, const RuntimeMethod* method)
{
	BaseInvokableCall_t1352930348 * V_0 = NULL;
	{
		RuntimeObject * L_0 = ___target0;
		MethodInfo_t * L_1 = ___theFunction1;
		InvokableCall_1_t254607980 * L_2 = (InvokableCall_1_t254607980 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3));
		((  void (*) (InvokableCall_1_t254607980 *, RuntimeObject *, MethodInfo_t *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)(L_2, (RuntimeObject *)L_0, (MethodInfo_t *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		V_0 = (BaseInvokableCall_t1352930348 *)L_2;
		goto IL_000e;
	}

IL_000e:
	{
		BaseInvokableCall_t1352930348 * L_3 = V_0;
		return L_3;
	}
}
// UnityEngine.Events.BaseInvokableCall UnityEngine.Events.UnityEvent`1<System.Object>::GetDelegate(UnityEngine.Events.UnityAction`1<T0>)
extern "C"  BaseInvokableCall_t1352930348 * UnityEvent_1_GetDelegate_m2958689604_gshared (RuntimeObject * __this /* static, unused */, UnityAction_1_t1274809082 * ___action0, const RuntimeMethod* method)
{
	BaseInvokableCall_t1352930348 * V_0 = NULL;
	{
		UnityAction_1_t1274809082 * L_0 = ___action0;
		InvokableCall_1_t254607980 * L_1 = (InvokableCall_1_t254607980 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		((  void (*) (InvokableCall_1_t254607980 *, UnityAction_1_t1274809082 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->methodPointer)(L_1, (UnityAction_1_t1274809082 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		V_0 = (BaseInvokableCall_t1352930348 *)L_1;
		goto IL_000d;
	}

IL_000d:
	{
		BaseInvokableCall_t1352930348 * L_2 = V_0;
		return L_2;
	}
}
// System.Void UnityEngine.Events.UnityEvent`1<System.Object>::Invoke(T0)
extern "C"  void UnityEvent_1_Invoke_m20326348_gshared (UnityEvent_1_t3072968858 * __this, RuntimeObject * ___arg00, const RuntimeMethod* method)
{
	{
		ObjectU5BU5D_t3270211303* L_0 = (ObjectU5BU5D_t3270211303*)__this->get_m_InvokeArray_4();
		RuntimeObject * L_1 = ___arg00;
		NullCheck(L_0);
		ArrayElementTypeCheck (L_0, L_1);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (RuntimeObject *)L_1);
		ObjectU5BU5D_t3270211303* L_2 = (ObjectU5BU5D_t3270211303*)__this->get_m_InvokeArray_4();
		NullCheck((UnityEventBase_t2008500469 *)__this);
		UnityEventBase_Invoke_m3195354837((UnityEventBase_t2008500469 *)__this, (ObjectU5BU5D_t3270211303*)L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Events.UnityEvent`1<System.Single>::.ctor()
extern "C"  void UnityEvent_1__ctor_m2458152034_gshared (UnityEvent_1_t4096360327 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityEvent_1__ctor_m2458152034_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set_m_InvokeArray_4(((ObjectU5BU5D_t3270211303*)SZArrayNew(ObjectU5BU5D_t3270211303_il2cpp_TypeInfo_var, (uint32_t)1)));
		NullCheck((UnityEventBase_t2008500469 *)__this);
		UnityEventBase__ctor_m821375546((UnityEventBase_t2008500469 *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Events.UnityEvent`1<System.Single>::AddListener(UnityEngine.Events.UnityAction`1<T0>)
extern "C"  void UnityEvent_1_AddListener_m3783567057_gshared (UnityEvent_1_t4096360327 * __this, UnityAction_1_t2298200551 * ___call0, const RuntimeMethod* method)
{
	{
		UnityAction_1_t2298200551 * L_0 = ___call0;
		BaseInvokableCall_t1352930348 * L_1 = ((  BaseInvokableCall_t1352930348 * (*) (RuntimeObject * /* static, unused */, UnityAction_1_t2298200551 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)(NULL /*static, unused*/, (UnityAction_1_t2298200551 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		NullCheck((UnityEventBase_t2008500469 *)__this);
		UnityEventBase_AddCall_m3837898691((UnityEventBase_t2008500469 *)__this, (BaseInvokableCall_t1352930348 *)L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Events.UnityEvent`1<System.Single>::RemoveListener(UnityEngine.Events.UnityAction`1<T0>)
extern "C"  void UnityEvent_1_RemoveListener_m1434749096_gshared (UnityEvent_1_t4096360327 * __this, UnityAction_1_t2298200551 * ___call0, const RuntimeMethod* method)
{
	{
		UnityAction_1_t2298200551 * L_0 = ___call0;
		NullCheck((Delegate_t537306192 *)L_0);
		RuntimeObject * L_1 = Delegate_get_Target_m2013817205((Delegate_t537306192 *)L_0, /*hidden argument*/NULL);
		UnityAction_1_t2298200551 * L_2 = ___call0;
		MethodInfo_t * L_3 = NetFxCoreExtensions_GetMethodInfo_m1250622137(NULL /*static, unused*/, (Delegate_t537306192 *)L_2, /*hidden argument*/NULL);
		NullCheck((UnityEventBase_t2008500469 *)__this);
		UnityEventBase_RemoveListener_m2620267480((UnityEventBase_t2008500469 *)__this, (RuntimeObject *)L_1, (MethodInfo_t *)L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Reflection.MethodInfo UnityEngine.Events.UnityEvent`1<System.Single>::FindMethod_Impl(System.String,System.Object)
extern "C"  MethodInfo_t * UnityEvent_1_FindMethod_Impl_m2231877772_gshared (UnityEvent_1_t4096360327 * __this, String_t* ___name0, RuntimeObject * ___targetObj1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityEvent_1_FindMethod_Impl_m2231877772_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	MethodInfo_t * V_0 = NULL;
	{
		RuntimeObject * L_0 = ___targetObj1;
		String_t* L_1 = ___name0;
		TypeU5BU5D_t3854702846* L_2 = (TypeU5BU5D_t3854702846*)((TypeU5BU5D_t3854702846*)SZArrayNew(TypeU5BU5D_t3854702846_il2cpp_TypeInfo_var, (uint32_t)1));
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m269683661(NULL /*static, unused*/, (RuntimeTypeHandle_t845633806 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 2)), /*hidden argument*/NULL);
		NullCheck(L_2);
		ArrayElementTypeCheck (L_2, L_3);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_3);
		MethodInfo_t * L_4 = UnityEventBase_GetValidMethodInfo_m2594745603(NULL /*static, unused*/, (RuntimeObject *)L_0, (String_t*)L_1, (TypeU5BU5D_t3854702846*)L_2, /*hidden argument*/NULL);
		V_0 = (MethodInfo_t *)L_4;
		goto IL_0021;
	}

IL_0021:
	{
		MethodInfo_t * L_5 = V_0;
		return L_5;
	}
}
// UnityEngine.Events.BaseInvokableCall UnityEngine.Events.UnityEvent`1<System.Single>::GetDelegate(System.Object,System.Reflection.MethodInfo)
extern "C"  BaseInvokableCall_t1352930348 * UnityEvent_1_GetDelegate_m3629836112_gshared (UnityEvent_1_t4096360327 * __this, RuntimeObject * ___target0, MethodInfo_t * ___theFunction1, const RuntimeMethod* method)
{
	BaseInvokableCall_t1352930348 * V_0 = NULL;
	{
		RuntimeObject * L_0 = ___target0;
		MethodInfo_t * L_1 = ___theFunction1;
		InvokableCall_1_t1277999449 * L_2 = (InvokableCall_1_t1277999449 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3));
		((  void (*) (InvokableCall_1_t1277999449 *, RuntimeObject *, MethodInfo_t *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)(L_2, (RuntimeObject *)L_0, (MethodInfo_t *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		V_0 = (BaseInvokableCall_t1352930348 *)L_2;
		goto IL_000e;
	}

IL_000e:
	{
		BaseInvokableCall_t1352930348 * L_3 = V_0;
		return L_3;
	}
}
// UnityEngine.Events.BaseInvokableCall UnityEngine.Events.UnityEvent`1<System.Single>::GetDelegate(UnityEngine.Events.UnityAction`1<T0>)
extern "C"  BaseInvokableCall_t1352930348 * UnityEvent_1_GetDelegate_m2160666064_gshared (RuntimeObject * __this /* static, unused */, UnityAction_1_t2298200551 * ___action0, const RuntimeMethod* method)
{
	BaseInvokableCall_t1352930348 * V_0 = NULL;
	{
		UnityAction_1_t2298200551 * L_0 = ___action0;
		InvokableCall_1_t1277999449 * L_1 = (InvokableCall_1_t1277999449 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		((  void (*) (InvokableCall_1_t1277999449 *, UnityAction_1_t2298200551 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->methodPointer)(L_1, (UnityAction_1_t2298200551 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		V_0 = (BaseInvokableCall_t1352930348 *)L_1;
		goto IL_000d;
	}

IL_000d:
	{
		BaseInvokableCall_t1352930348 * L_2 = V_0;
		return L_2;
	}
}
// System.Void UnityEngine.Events.UnityEvent`1<System.Single>::Invoke(T0)
extern "C"  void UnityEvent_1_Invoke_m1659563837_gshared (UnityEvent_1_t4096360327 * __this, float ___arg00, const RuntimeMethod* method)
{
	{
		ObjectU5BU5D_t3270211303* L_0 = (ObjectU5BU5D_t3270211303*)__this->get_m_InvokeArray_4();
		float L_1 = ___arg00;
		float L_2 = L_1;
		RuntimeObject * L_3 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6), &L_2);
		NullCheck(L_0);
		ArrayElementTypeCheck (L_0, L_3);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (RuntimeObject *)L_3);
		ObjectU5BU5D_t3270211303* L_4 = (ObjectU5BU5D_t3270211303*)__this->get_m_InvokeArray_4();
		NullCheck((UnityEventBase_t2008500469 *)__this);
		UnityEventBase_Invoke_m3195354837((UnityEventBase_t2008500469 *)__this, (ObjectU5BU5D_t3270211303*)L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Events.UnityEvent`1<UnityEngine.Color>::.ctor()
extern "C"  void UnityEvent_1__ctor_m3969821180_gshared (UnityEvent_1_t3558036978 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityEvent_1__ctor_m3969821180_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set_m_InvokeArray_4(((ObjectU5BU5D_t3270211303*)SZArrayNew(ObjectU5BU5D_t3270211303_il2cpp_TypeInfo_var, (uint32_t)1)));
		NullCheck((UnityEventBase_t2008500469 *)__this);
		UnityEventBase__ctor_m821375546((UnityEventBase_t2008500469 *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Events.UnityEvent`1<UnityEngine.Color>::AddListener(UnityEngine.Events.UnityAction`1<T0>)
extern "C"  void UnityEvent_1_AddListener_m1073272906_gshared (UnityEvent_1_t3558036978 * __this, UnityAction_1_t1759877202 * ___call0, const RuntimeMethod* method)
{
	{
		UnityAction_1_t1759877202 * L_0 = ___call0;
		BaseInvokableCall_t1352930348 * L_1 = ((  BaseInvokableCall_t1352930348 * (*) (RuntimeObject * /* static, unused */, UnityAction_1_t1759877202 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)(NULL /*static, unused*/, (UnityAction_1_t1759877202 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		NullCheck((UnityEventBase_t2008500469 *)__this);
		UnityEventBase_AddCall_m3837898691((UnityEventBase_t2008500469 *)__this, (BaseInvokableCall_t1352930348 *)L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Events.UnityEvent`1<UnityEngine.Color>::RemoveListener(UnityEngine.Events.UnityAction`1<T0>)
extern "C"  void UnityEvent_1_RemoveListener_m1134809070_gshared (UnityEvent_1_t3558036978 * __this, UnityAction_1_t1759877202 * ___call0, const RuntimeMethod* method)
{
	{
		UnityAction_1_t1759877202 * L_0 = ___call0;
		NullCheck((Delegate_t537306192 *)L_0);
		RuntimeObject * L_1 = Delegate_get_Target_m2013817205((Delegate_t537306192 *)L_0, /*hidden argument*/NULL);
		UnityAction_1_t1759877202 * L_2 = ___call0;
		MethodInfo_t * L_3 = NetFxCoreExtensions_GetMethodInfo_m1250622137(NULL /*static, unused*/, (Delegate_t537306192 *)L_2, /*hidden argument*/NULL);
		NullCheck((UnityEventBase_t2008500469 *)__this);
		UnityEventBase_RemoveListener_m2620267480((UnityEventBase_t2008500469 *)__this, (RuntimeObject *)L_1, (MethodInfo_t *)L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Reflection.MethodInfo UnityEngine.Events.UnityEvent`1<UnityEngine.Color>::FindMethod_Impl(System.String,System.Object)
extern "C"  MethodInfo_t * UnityEvent_1_FindMethod_Impl_m1310705587_gshared (UnityEvent_1_t3558036978 * __this, String_t* ___name0, RuntimeObject * ___targetObj1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityEvent_1_FindMethod_Impl_m1310705587_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	MethodInfo_t * V_0 = NULL;
	{
		RuntimeObject * L_0 = ___targetObj1;
		String_t* L_1 = ___name0;
		TypeU5BU5D_t3854702846* L_2 = (TypeU5BU5D_t3854702846*)((TypeU5BU5D_t3854702846*)SZArrayNew(TypeU5BU5D_t3854702846_il2cpp_TypeInfo_var, (uint32_t)1));
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m269683661(NULL /*static, unused*/, (RuntimeTypeHandle_t845633806 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 2)), /*hidden argument*/NULL);
		NullCheck(L_2);
		ArrayElementTypeCheck (L_2, L_3);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_3);
		MethodInfo_t * L_4 = UnityEventBase_GetValidMethodInfo_m2594745603(NULL /*static, unused*/, (RuntimeObject *)L_0, (String_t*)L_1, (TypeU5BU5D_t3854702846*)L_2, /*hidden argument*/NULL);
		V_0 = (MethodInfo_t *)L_4;
		goto IL_0021;
	}

IL_0021:
	{
		MethodInfo_t * L_5 = V_0;
		return L_5;
	}
}
// UnityEngine.Events.BaseInvokableCall UnityEngine.Events.UnityEvent`1<UnityEngine.Color>::GetDelegate(System.Object,System.Reflection.MethodInfo)
extern "C"  BaseInvokableCall_t1352930348 * UnityEvent_1_GetDelegate_m1408303316_gshared (UnityEvent_1_t3558036978 * __this, RuntimeObject * ___target0, MethodInfo_t * ___theFunction1, const RuntimeMethod* method)
{
	BaseInvokableCall_t1352930348 * V_0 = NULL;
	{
		RuntimeObject * L_0 = ___target0;
		MethodInfo_t * L_1 = ___theFunction1;
		InvokableCall_1_t739676100 * L_2 = (InvokableCall_1_t739676100 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3));
		((  void (*) (InvokableCall_1_t739676100 *, RuntimeObject *, MethodInfo_t *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)(L_2, (RuntimeObject *)L_0, (MethodInfo_t *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		V_0 = (BaseInvokableCall_t1352930348 *)L_2;
		goto IL_000e;
	}

IL_000e:
	{
		BaseInvokableCall_t1352930348 * L_3 = V_0;
		return L_3;
	}
}
// UnityEngine.Events.BaseInvokableCall UnityEngine.Events.UnityEvent`1<UnityEngine.Color>::GetDelegate(UnityEngine.Events.UnityAction`1<T0>)
extern "C"  BaseInvokableCall_t1352930348 * UnityEvent_1_GetDelegate_m663779574_gshared (RuntimeObject * __this /* static, unused */, UnityAction_1_t1759877202 * ___action0, const RuntimeMethod* method)
{
	BaseInvokableCall_t1352930348 * V_0 = NULL;
	{
		UnityAction_1_t1759877202 * L_0 = ___action0;
		InvokableCall_1_t739676100 * L_1 = (InvokableCall_1_t739676100 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		((  void (*) (InvokableCall_1_t739676100 *, UnityAction_1_t1759877202 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->methodPointer)(L_1, (UnityAction_1_t1759877202 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		V_0 = (BaseInvokableCall_t1352930348 *)L_1;
		goto IL_000d;
	}

IL_000d:
	{
		BaseInvokableCall_t1352930348 * L_2 = V_0;
		return L_2;
	}
}
// System.Void UnityEngine.Events.UnityEvent`1<UnityEngine.Color>::Invoke(T0)
extern "C"  void UnityEvent_1_Invoke_m1293140227_gshared (UnityEvent_1_t3558036978 * __this, Color_t2507026410  ___arg00, const RuntimeMethod* method)
{
	{
		ObjectU5BU5D_t3270211303* L_0 = (ObjectU5BU5D_t3270211303*)__this->get_m_InvokeArray_4();
		Color_t2507026410  L_1 = ___arg00;
		Color_t2507026410  L_2 = L_1;
		RuntimeObject * L_3 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6), &L_2);
		NullCheck(L_0);
		ArrayElementTypeCheck (L_0, L_3);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (RuntimeObject *)L_3);
		ObjectU5BU5D_t3270211303* L_4 = (ObjectU5BU5D_t3270211303*)__this->get_m_InvokeArray_4();
		NullCheck((UnityEventBase_t2008500469 *)__this);
		UnityEventBase_Invoke_m3195354837((UnityEventBase_t2008500469 *)__this, (ObjectU5BU5D_t3270211303*)L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Events.UnityEvent`1<UnityEngine.Vector2>::.ctor()
extern "C"  void UnityEvent_1__ctor_m154205551_gshared (UnityEvent_1_t333376534 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityEvent_1__ctor_m154205551_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set_m_InvokeArray_4(((ObjectU5BU5D_t3270211303*)SZArrayNew(ObjectU5BU5D_t3270211303_il2cpp_TypeInfo_var, (uint32_t)1)));
		NullCheck((UnityEventBase_t2008500469 *)__this);
		UnityEventBase__ctor_m821375546((UnityEventBase_t2008500469 *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Events.UnityEvent`1<UnityEngine.Vector2>::AddListener(UnityEngine.Events.UnityAction`1<T0>)
extern "C"  void UnityEvent_1_AddListener_m3795694340_gshared (UnityEvent_1_t333376534 * __this, UnityAction_1_t2830184054 * ___call0, const RuntimeMethod* method)
{
	{
		UnityAction_1_t2830184054 * L_0 = ___call0;
		BaseInvokableCall_t1352930348 * L_1 = ((  BaseInvokableCall_t1352930348 * (*) (RuntimeObject * /* static, unused */, UnityAction_1_t2830184054 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)(NULL /*static, unused*/, (UnityAction_1_t2830184054 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		NullCheck((UnityEventBase_t2008500469 *)__this);
		UnityEventBase_AddCall_m3837898691((UnityEventBase_t2008500469 *)__this, (BaseInvokableCall_t1352930348 *)L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Events.UnityEvent`1<UnityEngine.Vector2>::RemoveListener(UnityEngine.Events.UnityAction`1<T0>)
extern "C"  void UnityEvent_1_RemoveListener_m3962512554_gshared (UnityEvent_1_t333376534 * __this, UnityAction_1_t2830184054 * ___call0, const RuntimeMethod* method)
{
	{
		UnityAction_1_t2830184054 * L_0 = ___call0;
		NullCheck((Delegate_t537306192 *)L_0);
		RuntimeObject * L_1 = Delegate_get_Target_m2013817205((Delegate_t537306192 *)L_0, /*hidden argument*/NULL);
		UnityAction_1_t2830184054 * L_2 = ___call0;
		MethodInfo_t * L_3 = NetFxCoreExtensions_GetMethodInfo_m1250622137(NULL /*static, unused*/, (Delegate_t537306192 *)L_2, /*hidden argument*/NULL);
		NullCheck((UnityEventBase_t2008500469 *)__this);
		UnityEventBase_RemoveListener_m2620267480((UnityEventBase_t2008500469 *)__this, (RuntimeObject *)L_1, (MethodInfo_t *)L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Reflection.MethodInfo UnityEngine.Events.UnityEvent`1<UnityEngine.Vector2>::FindMethod_Impl(System.String,System.Object)
extern "C"  MethodInfo_t * UnityEvent_1_FindMethod_Impl_m2152450538_gshared (UnityEvent_1_t333376534 * __this, String_t* ___name0, RuntimeObject * ___targetObj1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityEvent_1_FindMethod_Impl_m2152450538_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	MethodInfo_t * V_0 = NULL;
	{
		RuntimeObject * L_0 = ___targetObj1;
		String_t* L_1 = ___name0;
		TypeU5BU5D_t3854702846* L_2 = (TypeU5BU5D_t3854702846*)((TypeU5BU5D_t3854702846*)SZArrayNew(TypeU5BU5D_t3854702846_il2cpp_TypeInfo_var, (uint32_t)1));
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m269683661(NULL /*static, unused*/, (RuntimeTypeHandle_t845633806 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 2)), /*hidden argument*/NULL);
		NullCheck(L_2);
		ArrayElementTypeCheck (L_2, L_3);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_3);
		MethodInfo_t * L_4 = UnityEventBase_GetValidMethodInfo_m2594745603(NULL /*static, unused*/, (RuntimeObject *)L_0, (String_t*)L_1, (TypeU5BU5D_t3854702846*)L_2, /*hidden argument*/NULL);
		V_0 = (MethodInfo_t *)L_4;
		goto IL_0021;
	}

IL_0021:
	{
		MethodInfo_t * L_5 = V_0;
		return L_5;
	}
}
// UnityEngine.Events.BaseInvokableCall UnityEngine.Events.UnityEvent`1<UnityEngine.Vector2>::GetDelegate(System.Object,System.Reflection.MethodInfo)
extern "C"  BaseInvokableCall_t1352930348 * UnityEvent_1_GetDelegate_m2280061379_gshared (UnityEvent_1_t333376534 * __this, RuntimeObject * ___target0, MethodInfo_t * ___theFunction1, const RuntimeMethod* method)
{
	BaseInvokableCall_t1352930348 * V_0 = NULL;
	{
		RuntimeObject * L_0 = ___target0;
		MethodInfo_t * L_1 = ___theFunction1;
		InvokableCall_1_t1809982952 * L_2 = (InvokableCall_1_t1809982952 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3));
		((  void (*) (InvokableCall_1_t1809982952 *, RuntimeObject *, MethodInfo_t *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)(L_2, (RuntimeObject *)L_0, (MethodInfo_t *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		V_0 = (BaseInvokableCall_t1352930348 *)L_2;
		goto IL_000e;
	}

IL_000e:
	{
		BaseInvokableCall_t1352930348 * L_3 = V_0;
		return L_3;
	}
}
// UnityEngine.Events.BaseInvokableCall UnityEngine.Events.UnityEvent`1<UnityEngine.Vector2>::GetDelegate(UnityEngine.Events.UnityAction`1<T0>)
extern "C"  BaseInvokableCall_t1352930348 * UnityEvent_1_GetDelegate_m1650458825_gshared (RuntimeObject * __this /* static, unused */, UnityAction_1_t2830184054 * ___action0, const RuntimeMethod* method)
{
	BaseInvokableCall_t1352930348 * V_0 = NULL;
	{
		UnityAction_1_t2830184054 * L_0 = ___action0;
		InvokableCall_1_t1809982952 * L_1 = (InvokableCall_1_t1809982952 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		((  void (*) (InvokableCall_1_t1809982952 *, UnityAction_1_t2830184054 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->methodPointer)(L_1, (UnityAction_1_t2830184054 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		V_0 = (BaseInvokableCall_t1352930348 *)L_1;
		goto IL_000d;
	}

IL_000d:
	{
		BaseInvokableCall_t1352930348 * L_2 = V_0;
		return L_2;
	}
}
// System.Void UnityEngine.Events.UnityEvent`1<UnityEngine.Vector2>::Invoke(T0)
extern "C"  void UnityEvent_1_Invoke_m789391282_gshared (UnityEvent_1_t333376534 * __this, Vector2_t3577333262  ___arg00, const RuntimeMethod* method)
{
	{
		ObjectU5BU5D_t3270211303* L_0 = (ObjectU5BU5D_t3270211303*)__this->get_m_InvokeArray_4();
		Vector2_t3577333262  L_1 = ___arg00;
		Vector2_t3577333262  L_2 = L_1;
		RuntimeObject * L_3 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6), &L_2);
		NullCheck(L_0);
		ArrayElementTypeCheck (L_0, L_3);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (RuntimeObject *)L_3);
		ObjectU5BU5D_t3270211303* L_4 = (ObjectU5BU5D_t3270211303*)__this->get_m_InvokeArray_4();
		NullCheck((UnityEventBase_t2008500469 *)__this);
		UnityEventBase_Invoke_m3195354837((UnityEventBase_t2008500469 *)__this, (ObjectU5BU5D_t3270211303*)L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Events.UnityEvent`2<System.Object,System.Object>::.ctor()
extern "C"  void UnityEvent_2__ctor_m2986156325_gshared (UnityEvent_2_t2615162100 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityEvent_2__ctor_m2986156325_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set_m_InvokeArray_4(((ObjectU5BU5D_t3270211303*)SZArrayNew(ObjectU5BU5D_t3270211303_il2cpp_TypeInfo_var, (uint32_t)2)));
		NullCheck((UnityEventBase_t2008500469 *)__this);
		UnityEventBase__ctor_m821375546((UnityEventBase_t2008500469 *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Reflection.MethodInfo UnityEngine.Events.UnityEvent`2<System.Object,System.Object>::FindMethod_Impl(System.String,System.Object)
extern "C"  MethodInfo_t * UnityEvent_2_FindMethod_Impl_m3281817964_gshared (UnityEvent_2_t2615162100 * __this, String_t* ___name0, RuntimeObject * ___targetObj1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityEvent_2_FindMethod_Impl_m3281817964_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	MethodInfo_t * V_0 = NULL;
	{
		RuntimeObject * L_0 = ___targetObj1;
		String_t* L_1 = ___name0;
		TypeU5BU5D_t3854702846* L_2 = (TypeU5BU5D_t3854702846*)((TypeU5BU5D_t3854702846*)SZArrayNew(TypeU5BU5D_t3854702846_il2cpp_TypeInfo_var, (uint32_t)2));
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m269683661(NULL /*static, unused*/, (RuntimeTypeHandle_t845633806 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 0)), /*hidden argument*/NULL);
		NullCheck(L_2);
		ArrayElementTypeCheck (L_2, L_3);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_3);
		TypeU5BU5D_t3854702846* L_4 = (TypeU5BU5D_t3854702846*)L_2;
		Type_t * L_5 = Type_GetTypeFromHandle_m269683661(NULL /*static, unused*/, (RuntimeTypeHandle_t845633806 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck(L_4);
		ArrayElementTypeCheck (L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(1), (Type_t *)L_5);
		MethodInfo_t * L_6 = UnityEventBase_GetValidMethodInfo_m2594745603(NULL /*static, unused*/, (RuntimeObject *)L_0, (String_t*)L_1, (TypeU5BU5D_t3854702846*)L_4, /*hidden argument*/NULL);
		V_0 = (MethodInfo_t *)L_6;
		goto IL_002e;
	}

IL_002e:
	{
		MethodInfo_t * L_7 = V_0;
		return L_7;
	}
}
// UnityEngine.Events.BaseInvokableCall UnityEngine.Events.UnityEvent`2<System.Object,System.Object>::GetDelegate(System.Object,System.Reflection.MethodInfo)
extern "C"  BaseInvokableCall_t1352930348 * UnityEvent_2_GetDelegate_m1468658818_gshared (UnityEvent_2_t2615162100 * __this, RuntimeObject * ___target0, MethodInfo_t * ___theFunction1, const RuntimeMethod* method)
{
	BaseInvokableCall_t1352930348 * V_0 = NULL;
	{
		RuntimeObject * L_0 = ___target0;
		MethodInfo_t * L_1 = ___theFunction1;
		InvokableCall_2_t1353357206 * L_2 = (InvokableCall_2_t1353357206 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2));
		((  void (*) (InvokableCall_2_t1353357206 *, RuntimeObject *, MethodInfo_t *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)(L_2, (RuntimeObject *)L_0, (MethodInfo_t *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		V_0 = (BaseInvokableCall_t1352930348 *)L_2;
		goto IL_000e;
	}

IL_000e:
	{
		BaseInvokableCall_t1352930348 * L_3 = V_0;
		return L_3;
	}
}
// System.Void UnityEngine.Events.UnityEvent`3<System.Object,System.Object,System.Object>::.ctor()
extern "C"  void UnityEvent_3__ctor_m962964178_gshared (UnityEvent_3_t2603401452 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityEvent_3__ctor_m962964178_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set_m_InvokeArray_4(((ObjectU5BU5D_t3270211303*)SZArrayNew(ObjectU5BU5D_t3270211303_il2cpp_TypeInfo_var, (uint32_t)3)));
		NullCheck((UnityEventBase_t2008500469 *)__this);
		UnityEventBase__ctor_m821375546((UnityEventBase_t2008500469 *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Reflection.MethodInfo UnityEngine.Events.UnityEvent`3<System.Object,System.Object,System.Object>::FindMethod_Impl(System.String,System.Object)
extern "C"  MethodInfo_t * UnityEvent_3_FindMethod_Impl_m923441632_gshared (UnityEvent_3_t2603401452 * __this, String_t* ___name0, RuntimeObject * ___targetObj1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityEvent_3_FindMethod_Impl_m923441632_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	MethodInfo_t * V_0 = NULL;
	{
		RuntimeObject * L_0 = ___targetObj1;
		String_t* L_1 = ___name0;
		TypeU5BU5D_t3854702846* L_2 = (TypeU5BU5D_t3854702846*)((TypeU5BU5D_t3854702846*)SZArrayNew(TypeU5BU5D_t3854702846_il2cpp_TypeInfo_var, (uint32_t)3));
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m269683661(NULL /*static, unused*/, (RuntimeTypeHandle_t845633806 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 0)), /*hidden argument*/NULL);
		NullCheck(L_2);
		ArrayElementTypeCheck (L_2, L_3);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_3);
		TypeU5BU5D_t3854702846* L_4 = (TypeU5BU5D_t3854702846*)L_2;
		Type_t * L_5 = Type_GetTypeFromHandle_m269683661(NULL /*static, unused*/, (RuntimeTypeHandle_t845633806 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck(L_4);
		ArrayElementTypeCheck (L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(1), (Type_t *)L_5);
		TypeU5BU5D_t3854702846* L_6 = (TypeU5BU5D_t3854702846*)L_4;
		Type_t * L_7 = Type_GetTypeFromHandle_m269683661(NULL /*static, unused*/, (RuntimeTypeHandle_t845633806 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 2)), /*hidden argument*/NULL);
		NullCheck(L_6);
		ArrayElementTypeCheck (L_6, L_7);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(2), (Type_t *)L_7);
		MethodInfo_t * L_8 = UnityEventBase_GetValidMethodInfo_m2594745603(NULL /*static, unused*/, (RuntimeObject *)L_0, (String_t*)L_1, (TypeU5BU5D_t3854702846*)L_6, /*hidden argument*/NULL);
		V_0 = (MethodInfo_t *)L_8;
		goto IL_003b;
	}

IL_003b:
	{
		MethodInfo_t * L_9 = V_0;
		return L_9;
	}
}
// UnityEngine.Events.BaseInvokableCall UnityEngine.Events.UnityEvent`3<System.Object,System.Object,System.Object>::GetDelegate(System.Object,System.Reflection.MethodInfo)
extern "C"  BaseInvokableCall_t1352930348 * UnityEvent_3_GetDelegate_m212977428_gshared (UnityEvent_3_t2603401452 * __this, RuntimeObject * ___target0, MethodInfo_t * ___theFunction1, const RuntimeMethod* method)
{
	BaseInvokableCall_t1352930348 * V_0 = NULL;
	{
		RuntimeObject * L_0 = ___target0;
		MethodInfo_t * L_1 = ___theFunction1;
		InvokableCall_3_t3331941758 * L_2 = (InvokableCall_3_t3331941758 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3));
		((  void (*) (InvokableCall_3_t3331941758 *, RuntimeObject *, MethodInfo_t *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)(L_2, (RuntimeObject *)L_0, (MethodInfo_t *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		V_0 = (BaseInvokableCall_t1352930348 *)L_2;
		goto IL_000e;
	}

IL_000e:
	{
		BaseInvokableCall_t1352930348 * L_3 = V_0;
		return L_3;
	}
}
// System.Void UnityEngine.Events.UnityEvent`4<System.Object,System.Object,System.Object,System.Object>::.ctor()
extern "C"  void UnityEvent_4__ctor_m3870083973_gshared (UnityEvent_4_t4221637861 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityEvent_4__ctor_m3870083973_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set_m_InvokeArray_4(((ObjectU5BU5D_t3270211303*)SZArrayNew(ObjectU5BU5D_t3270211303_il2cpp_TypeInfo_var, (uint32_t)4)));
		NullCheck((UnityEventBase_t2008500469 *)__this);
		UnityEventBase__ctor_m821375546((UnityEventBase_t2008500469 *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Reflection.MethodInfo UnityEngine.Events.UnityEvent`4<System.Object,System.Object,System.Object,System.Object>::FindMethod_Impl(System.String,System.Object)
extern "C"  MethodInfo_t * UnityEvent_4_FindMethod_Impl_m3193778268_gshared (UnityEvent_4_t4221637861 * __this, String_t* ___name0, RuntimeObject * ___targetObj1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityEvent_4_FindMethod_Impl_m3193778268_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	MethodInfo_t * V_0 = NULL;
	{
		RuntimeObject * L_0 = ___targetObj1;
		String_t* L_1 = ___name0;
		TypeU5BU5D_t3854702846* L_2 = (TypeU5BU5D_t3854702846*)((TypeU5BU5D_t3854702846*)SZArrayNew(TypeU5BU5D_t3854702846_il2cpp_TypeInfo_var, (uint32_t)4));
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m269683661(NULL /*static, unused*/, (RuntimeTypeHandle_t845633806 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 0)), /*hidden argument*/NULL);
		NullCheck(L_2);
		ArrayElementTypeCheck (L_2, L_3);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_3);
		TypeU5BU5D_t3854702846* L_4 = (TypeU5BU5D_t3854702846*)L_2;
		Type_t * L_5 = Type_GetTypeFromHandle_m269683661(NULL /*static, unused*/, (RuntimeTypeHandle_t845633806 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck(L_4);
		ArrayElementTypeCheck (L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(1), (Type_t *)L_5);
		TypeU5BU5D_t3854702846* L_6 = (TypeU5BU5D_t3854702846*)L_4;
		Type_t * L_7 = Type_GetTypeFromHandle_m269683661(NULL /*static, unused*/, (RuntimeTypeHandle_t845633806 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 2)), /*hidden argument*/NULL);
		NullCheck(L_6);
		ArrayElementTypeCheck (L_6, L_7);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(2), (Type_t *)L_7);
		TypeU5BU5D_t3854702846* L_8 = (TypeU5BU5D_t3854702846*)L_6;
		Type_t * L_9 = Type_GetTypeFromHandle_m269683661(NULL /*static, unused*/, (RuntimeTypeHandle_t845633806 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 3)), /*hidden argument*/NULL);
		NullCheck(L_8);
		ArrayElementTypeCheck (L_8, L_9);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(3), (Type_t *)L_9);
		MethodInfo_t * L_10 = UnityEventBase_GetValidMethodInfo_m2594745603(NULL /*static, unused*/, (RuntimeObject *)L_0, (String_t*)L_1, (TypeU5BU5D_t3854702846*)L_8, /*hidden argument*/NULL);
		V_0 = (MethodInfo_t *)L_10;
		goto IL_0048;
	}

IL_0048:
	{
		MethodInfo_t * L_11 = V_0;
		return L_11;
	}
}
// UnityEngine.Events.BaseInvokableCall UnityEngine.Events.UnityEvent`4<System.Object,System.Object,System.Object,System.Object>::GetDelegate(System.Object,System.Reflection.MethodInfo)
extern "C"  BaseInvokableCall_t1352930348 * UnityEvent_4_GetDelegate_m1795825197_gshared (UnityEvent_4_t4221637861 * __this, RuntimeObject * ___target0, MethodInfo_t * ___theFunction1, const RuntimeMethod* method)
{
	BaseInvokableCall_t1352930348 * V_0 = NULL;
	{
		RuntimeObject * L_0 = ___target0;
		MethodInfo_t * L_1 = ___theFunction1;
		InvokableCall_4_t304489837 * L_2 = (InvokableCall_4_t304489837 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 4));
		((  void (*) (InvokableCall_4_t304489837 *, RuntimeObject *, MethodInfo_t *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)(L_2, (RuntimeObject *)L_0, (MethodInfo_t *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		V_0 = (BaseInvokableCall_t1352930348 *)L_2;
		goto IL_000e;
	}

IL_000e:
	{
		BaseInvokableCall_t1352930348 * L_3 = V_0;
		return L_3;
	}
}
// System.Void UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void EventFunction_1__ctor_m1993303380_gshared (EventFunction_1_t3830141588 * __this, RuntimeObject * ___object0, IntPtr_t ___method1, const RuntimeMethod* method)
{
	__this->set_method_ptr_0(il2cpp_codegen_get_method_pointer((RuntimeMethod*)___method1.get_m_value_0()));
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<System.Object>::Invoke(T1,UnityEngine.EventSystems.BaseEventData)
extern "C"  void EventFunction_1_Invoke_m3395982758_gshared (EventFunction_1_t3830141588 * __this, RuntimeObject * ___handler0, BaseEventData_t1865211501 * ___eventData1, const RuntimeMethod* method)
{
	if(__this->get_prev_9() != NULL)
	{
		EventFunction_1_Invoke_m3395982758((EventFunction_1_t3830141588 *)__this->get_prev_9(),___handler0, ___eventData1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (RuntimeObject *, void* __this, RuntimeObject * ___handler0, BaseEventData_t1865211501 * ___eventData1, const RuntimeMethod* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___handler0, ___eventData1,(RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (void* __this, RuntimeObject * ___handler0, BaseEventData_t1865211501 * ___eventData1, const RuntimeMethod* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___handler0, ___eventData1,(RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, BaseEventData_t1865211501 * ___eventData1, const RuntimeMethod* method);
		((FunctionPointerType)__this->get_method_ptr_0())(___handler0, ___eventData1,(RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<System.Object>::BeginInvoke(T1,UnityEngine.EventSystems.BaseEventData,System.AsyncCallback,System.Object)
extern "C"  RuntimeObject* EventFunction_1_BeginInvoke_m1321537966_gshared (EventFunction_1_t3830141588 * __this, RuntimeObject * ___handler0, BaseEventData_t1865211501 * ___eventData1, AsyncCallback_t1057242265 * ___callback2, RuntimeObject * ___object3, const RuntimeMethod* method)
{
	void *__d_args[3] = {0};
	__d_args[0] = ___handler0;
	__d_args[1] = ___eventData1;
	return (RuntimeObject*)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (RuntimeObject*)___object3);
}
// System.Void UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  void EventFunction_1_EndInvoke_m1514553445_gshared (EventFunction_1_t3830141588 * __this, RuntimeObject* ___result0, const RuntimeMethod* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void UnityEngine.UI.Collections.IndexedSet`1<System.Object>::.ctor()
extern "C"  void IndexedSet_1__ctor_m1450156132_gshared (IndexedSet_1_t2088528067 * __this, const RuntimeMethod* method)
{
	{
		List_1_t877107497 * L_0 = (List_1_t877107497 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0));
		((  void (*) (List_1_t877107497 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		__this->set_m_List_0(L_0);
		Dictionary_2_t2084444880 * L_1 = (Dictionary_2_t2084444880 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2));
		((  void (*) (Dictionary_2_t2084444880 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)(L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		__this->set_m_Dictionary_1(L_1);
		NullCheck((RuntimeObject *)__this);
		Object__ctor_m2906060009((RuntimeObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.UI.Collections.IndexedSet`1<System.Object>::Add(T)
extern "C"  void IndexedSet_1_Add_m1007233996_gshared (IndexedSet_1_t2088528067 * __this, RuntimeObject * ___item0, const RuntimeMethod* method)
{
	{
		List_1_t877107497 * L_0 = (List_1_t877107497 *)__this->get_m_List_0();
		RuntimeObject * L_1 = ___item0;
		NullCheck((List_1_t877107497 *)L_0);
		((  void (*) (List_1_t877107497 *, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)((List_1_t877107497 *)L_0, (RuntimeObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		Dictionary_2_t2084444880 * L_2 = (Dictionary_2_t2084444880 *)__this->get_m_Dictionary_1();
		RuntimeObject * L_3 = ___item0;
		List_1_t877107497 * L_4 = (List_1_t877107497 *)__this->get_m_List_0();
		NullCheck((List_1_t877107497 *)L_4);
		int32_t L_5 = ((  int32_t (*) (List_1_t877107497 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)((List_1_t877107497 *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		NullCheck((Dictionary_2_t2084444880 *)L_2);
		((  void (*) (Dictionary_2_t2084444880 *, RuntimeObject *, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((Dictionary_2_t2084444880 *)L_2, (RuntimeObject *)L_3, (int32_t)((int32_t)((int32_t)L_5-(int32_t)1)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		return;
	}
}
// System.Boolean UnityEngine.UI.Collections.IndexedSet`1<System.Object>::AddUnique(T)
extern "C"  bool IndexedSet_1_AddUnique_m3811171913_gshared (IndexedSet_1_t2088528067 * __this, RuntimeObject * ___item0, const RuntimeMethod* method)
{
	bool V_0 = false;
	{
		Dictionary_2_t2084444880 * L_0 = (Dictionary_2_t2084444880 *)__this->get_m_Dictionary_1();
		RuntimeObject * L_1 = ___item0;
		NullCheck((Dictionary_2_t2084444880 *)L_0);
		bool L_2 = ((  bool (*) (Dictionary_2_t2084444880 *, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7)->methodPointer)((Dictionary_2_t2084444880 *)L_0, (RuntimeObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7));
		if (!L_2)
		{
			goto IL_0019;
		}
	}
	{
		V_0 = (bool)0;
		goto IL_0045;
	}

IL_0019:
	{
		List_1_t877107497 * L_3 = (List_1_t877107497 *)__this->get_m_List_0();
		RuntimeObject * L_4 = ___item0;
		NullCheck((List_1_t877107497 *)L_3);
		((  void (*) (List_1_t877107497 *, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)((List_1_t877107497 *)L_3, (RuntimeObject *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		Dictionary_2_t2084444880 * L_5 = (Dictionary_2_t2084444880 *)__this->get_m_Dictionary_1();
		RuntimeObject * L_6 = ___item0;
		List_1_t877107497 * L_7 = (List_1_t877107497 *)__this->get_m_List_0();
		NullCheck((List_1_t877107497 *)L_7);
		int32_t L_8 = ((  int32_t (*) (List_1_t877107497 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)((List_1_t877107497 *)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		NullCheck((Dictionary_2_t2084444880 *)L_5);
		((  void (*) (Dictionary_2_t2084444880 *, RuntimeObject *, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((Dictionary_2_t2084444880 *)L_5, (RuntimeObject *)L_6, (int32_t)((int32_t)((int32_t)L_8-(int32_t)1)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		V_0 = (bool)1;
		goto IL_0045;
	}

IL_0045:
	{
		bool L_9 = V_0;
		return L_9;
	}
}
// System.Boolean UnityEngine.UI.Collections.IndexedSet`1<System.Object>::Remove(T)
extern "C"  bool IndexedSet_1_Remove_m326755531_gshared (IndexedSet_1_t2088528067 * __this, RuntimeObject * ___item0, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	bool V_1 = false;
	{
		V_0 = (int32_t)(-1);
		Dictionary_2_t2084444880 * L_0 = (Dictionary_2_t2084444880 *)__this->get_m_Dictionary_1();
		RuntimeObject * L_1 = ___item0;
		NullCheck((Dictionary_2_t2084444880 *)L_0);
		bool L_2 = ((  bool (*) (Dictionary_2_t2084444880 *, RuntimeObject *, int32_t*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)((Dictionary_2_t2084444880 *)L_0, (RuntimeObject *)L_1, (int32_t*)(&V_0), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		if (L_2)
		{
			goto IL_001d;
		}
	}
	{
		V_1 = (bool)0;
		goto IL_002b;
	}

IL_001d:
	{
		int32_t L_3 = V_0;
		NullCheck((IndexedSet_1_t2088528067 *)__this);
		((  void (*) (IndexedSet_1_t2088528067 *, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9)->methodPointer)((IndexedSet_1_t2088528067 *)__this, (int32_t)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
		V_1 = (bool)1;
		goto IL_002b;
	}

IL_002b:
	{
		bool L_4 = V_1;
		return L_4;
	}
}
// System.Collections.Generic.IEnumerator`1<T> UnityEngine.UI.Collections.IndexedSet`1<System.Object>::GetEnumerator()
extern "C"  RuntimeObject* IndexedSet_1_GetEnumerator_m1233643819_gshared (IndexedSet_1_t2088528067 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IndexedSet_1_GetEnumerator_m1233643819_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotImplementedException_t2054929637 * L_0 = (NotImplementedException_t2054929637 *)il2cpp_codegen_object_new(NotImplementedException_t2054929637_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m4036280126(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Collections.IEnumerator UnityEngine.UI.Collections.IndexedSet`1<System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  RuntimeObject* IndexedSet_1_System_Collections_IEnumerable_GetEnumerator_m417661705_gshared (IndexedSet_1_t2088528067 * __this, const RuntimeMethod* method)
{
	RuntimeObject* V_0 = NULL;
	{
		NullCheck((IndexedSet_1_t2088528067 *)__this);
		RuntimeObject* L_0 = ((  RuntimeObject* (*) (IndexedSet_1_t2088528067 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10)->methodPointer)((IndexedSet_1_t2088528067 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10));
		V_0 = (RuntimeObject*)L_0;
		goto IL_000d;
	}

IL_000d:
	{
		RuntimeObject* L_1 = V_0;
		return L_1;
	}
}
// System.Void UnityEngine.UI.Collections.IndexedSet`1<System.Object>::Clear()
extern "C"  void IndexedSet_1_Clear_m4256500658_gshared (IndexedSet_1_t2088528067 * __this, const RuntimeMethod* method)
{
	{
		List_1_t877107497 * L_0 = (List_1_t877107497 *)__this->get_m_List_0();
		NullCheck((List_1_t877107497 *)L_0);
		((  void (*) (List_1_t877107497 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11)->methodPointer)((List_1_t877107497 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11));
		Dictionary_2_t2084444880 * L_1 = (Dictionary_2_t2084444880 *)__this->get_m_Dictionary_1();
		NullCheck((Dictionary_2_t2084444880 *)L_1);
		((  void (*) (Dictionary_2_t2084444880 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12)->methodPointer)((Dictionary_2_t2084444880 *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12));
		return;
	}
}
// System.Boolean UnityEngine.UI.Collections.IndexedSet`1<System.Object>::Contains(T)
extern "C"  bool IndexedSet_1_Contains_m338232552_gshared (IndexedSet_1_t2088528067 * __this, RuntimeObject * ___item0, const RuntimeMethod* method)
{
	bool V_0 = false;
	{
		Dictionary_2_t2084444880 * L_0 = (Dictionary_2_t2084444880 *)__this->get_m_Dictionary_1();
		RuntimeObject * L_1 = ___item0;
		NullCheck((Dictionary_2_t2084444880 *)L_0);
		bool L_2 = ((  bool (*) (Dictionary_2_t2084444880 *, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7)->methodPointer)((Dictionary_2_t2084444880 *)L_0, (RuntimeObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7));
		V_0 = (bool)L_2;
		goto IL_0013;
	}

IL_0013:
	{
		bool L_3 = V_0;
		return L_3;
	}
}
// System.Void UnityEngine.UI.Collections.IndexedSet`1<System.Object>::CopyTo(T[],System.Int32)
extern "C"  void IndexedSet_1_CopyTo_m3331283592_gshared (IndexedSet_1_t2088528067 * __this, ObjectU5BU5D_t3270211303* ___array0, int32_t ___arrayIndex1, const RuntimeMethod* method)
{
	{
		List_1_t877107497 * L_0 = (List_1_t877107497 *)__this->get_m_List_0();
		ObjectU5BU5D_t3270211303* L_1 = ___array0;
		int32_t L_2 = ___arrayIndex1;
		NullCheck((List_1_t877107497 *)L_0);
		((  void (*) (List_1_t877107497 *, ObjectU5BU5D_t3270211303*, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 13)->methodPointer)((List_1_t877107497 *)L_0, (ObjectU5BU5D_t3270211303*)L_1, (int32_t)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 13));
		return;
	}
}
// System.Int32 UnityEngine.UI.Collections.IndexedSet`1<System.Object>::get_Count()
extern "C"  int32_t IndexedSet_1_get_Count_m3943755507_gshared (IndexedSet_1_t2088528067 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		List_1_t877107497 * L_0 = (List_1_t877107497 *)__this->get_m_List_0();
		NullCheck((List_1_t877107497 *)L_0);
		int32_t L_1 = ((  int32_t (*) (List_1_t877107497 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)((List_1_t877107497 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		V_0 = (int32_t)L_1;
		goto IL_0012;
	}

IL_0012:
	{
		int32_t L_2 = V_0;
		return L_2;
	}
}
// System.Boolean UnityEngine.UI.Collections.IndexedSet`1<System.Object>::get_IsReadOnly()
extern "C"  bool IndexedSet_1_get_IsReadOnly_m4087077942_gshared (IndexedSet_1_t2088528067 * __this, const RuntimeMethod* method)
{
	bool V_0 = false;
	{
		V_0 = (bool)0;
		goto IL_0008;
	}

IL_0008:
	{
		bool L_0 = V_0;
		return L_0;
	}
}
// System.Int32 UnityEngine.UI.Collections.IndexedSet`1<System.Object>::IndexOf(T)
extern "C"  int32_t IndexedSet_1_IndexOf_m2915305515_gshared (IndexedSet_1_t2088528067 * __this, RuntimeObject * ___item0, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		V_0 = (int32_t)(-1);
		Dictionary_2_t2084444880 * L_0 = (Dictionary_2_t2084444880 *)__this->get_m_Dictionary_1();
		RuntimeObject * L_1 = ___item0;
		NullCheck((Dictionary_2_t2084444880 *)L_0);
		((  bool (*) (Dictionary_2_t2084444880 *, RuntimeObject *, int32_t*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)((Dictionary_2_t2084444880 *)L_0, (RuntimeObject *)L_1, (int32_t*)(&V_0), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		int32_t L_2 = V_0;
		V_1 = (int32_t)L_2;
		goto IL_0019;
	}

IL_0019:
	{
		int32_t L_3 = V_1;
		return L_3;
	}
}
// System.Void UnityEngine.UI.Collections.IndexedSet`1<System.Object>::Insert(System.Int32,T)
extern "C"  void IndexedSet_1_Insert_m2770351314_gshared (IndexedSet_1_t2088528067 * __this, int32_t ___index0, RuntimeObject * ___item1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IndexedSet_1_Insert_m2770351314_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1045231613 * L_0 = (NotSupportedException_t1045231613 *)il2cpp_codegen_object_new(NotSupportedException_t1045231613_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m3333199409(L_0, (String_t*)_stringLiteral2696702335, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void UnityEngine.UI.Collections.IndexedSet`1<System.Object>::RemoveAt(System.Int32)
extern "C"  void IndexedSet_1_RemoveAt_m2137930214_gshared (IndexedSet_1_t2088528067 * __this, int32_t ___index0, const RuntimeMethod* method)
{
	RuntimeObject * V_0 = NULL;
	int32_t V_1 = 0;
	RuntimeObject * V_2 = NULL;
	{
		List_1_t877107497 * L_0 = (List_1_t877107497 *)__this->get_m_List_0();
		int32_t L_1 = ___index0;
		NullCheck((List_1_t877107497 *)L_0);
		RuntimeObject * L_2 = ((  RuntimeObject * (*) (List_1_t877107497 *, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 14)->methodPointer)((List_1_t877107497 *)L_0, (int32_t)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 14));
		V_0 = (RuntimeObject *)L_2;
		Dictionary_2_t2084444880 * L_3 = (Dictionary_2_t2084444880 *)__this->get_m_Dictionary_1();
		RuntimeObject * L_4 = V_0;
		NullCheck((Dictionary_2_t2084444880 *)L_3);
		((  bool (*) (Dictionary_2_t2084444880 *, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 15)->methodPointer)((Dictionary_2_t2084444880 *)L_3, (RuntimeObject *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 15));
		int32_t L_5 = ___index0;
		List_1_t877107497 * L_6 = (List_1_t877107497 *)__this->get_m_List_0();
		NullCheck((List_1_t877107497 *)L_6);
		int32_t L_7 = ((  int32_t (*) (List_1_t877107497 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)((List_1_t877107497 *)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		if ((!(((uint32_t)L_5) == ((uint32_t)((int32_t)((int32_t)L_7-(int32_t)1))))))
		{
			goto IL_003f;
		}
	}
	{
		List_1_t877107497 * L_8 = (List_1_t877107497 *)__this->get_m_List_0();
		int32_t L_9 = ___index0;
		NullCheck((List_1_t877107497 *)L_8);
		((  void (*) (List_1_t877107497 *, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 16)->methodPointer)((List_1_t877107497 *)L_8, (int32_t)L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 16));
		goto IL_0082;
	}

IL_003f:
	{
		List_1_t877107497 * L_10 = (List_1_t877107497 *)__this->get_m_List_0();
		NullCheck((List_1_t877107497 *)L_10);
		int32_t L_11 = ((  int32_t (*) (List_1_t877107497 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)((List_1_t877107497 *)L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		V_1 = (int32_t)((int32_t)((int32_t)L_11-(int32_t)1));
		List_1_t877107497 * L_12 = (List_1_t877107497 *)__this->get_m_List_0();
		int32_t L_13 = V_1;
		NullCheck((List_1_t877107497 *)L_12);
		RuntimeObject * L_14 = ((  RuntimeObject * (*) (List_1_t877107497 *, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 14)->methodPointer)((List_1_t877107497 *)L_12, (int32_t)L_13, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 14));
		V_2 = (RuntimeObject *)L_14;
		List_1_t877107497 * L_15 = (List_1_t877107497 *)__this->get_m_List_0();
		int32_t L_16 = ___index0;
		RuntimeObject * L_17 = V_2;
		NullCheck((List_1_t877107497 *)L_15);
		((  void (*) (List_1_t877107497 *, int32_t, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 17)->methodPointer)((List_1_t877107497 *)L_15, (int32_t)L_16, (RuntimeObject *)L_17, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 17));
		Dictionary_2_t2084444880 * L_18 = (Dictionary_2_t2084444880 *)__this->get_m_Dictionary_1();
		RuntimeObject * L_19 = V_2;
		int32_t L_20 = ___index0;
		NullCheck((Dictionary_2_t2084444880 *)L_18);
		((  void (*) (Dictionary_2_t2084444880 *, RuntimeObject *, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 18)->methodPointer)((Dictionary_2_t2084444880 *)L_18, (RuntimeObject *)L_19, (int32_t)L_20, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 18));
		List_1_t877107497 * L_21 = (List_1_t877107497 *)__this->get_m_List_0();
		int32_t L_22 = V_1;
		NullCheck((List_1_t877107497 *)L_21);
		((  void (*) (List_1_t877107497 *, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 16)->methodPointer)((List_1_t877107497 *)L_21, (int32_t)L_22, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 16));
	}

IL_0082:
	{
		return;
	}
}
// T UnityEngine.UI.Collections.IndexedSet`1<System.Object>::get_Item(System.Int32)
extern "C"  RuntimeObject * IndexedSet_1_get_Item_m1022543993_gshared (IndexedSet_1_t2088528067 * __this, int32_t ___index0, const RuntimeMethod* method)
{
	RuntimeObject * V_0 = NULL;
	{
		List_1_t877107497 * L_0 = (List_1_t877107497 *)__this->get_m_List_0();
		int32_t L_1 = ___index0;
		NullCheck((List_1_t877107497 *)L_0);
		RuntimeObject * L_2 = ((  RuntimeObject * (*) (List_1_t877107497 *, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 14)->methodPointer)((List_1_t877107497 *)L_0, (int32_t)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 14));
		V_0 = (RuntimeObject *)L_2;
		goto IL_0013;
	}

IL_0013:
	{
		RuntimeObject * L_3 = V_0;
		return L_3;
	}
}
// System.Void UnityEngine.UI.Collections.IndexedSet`1<System.Object>::set_Item(System.Int32,T)
extern "C"  void IndexedSet_1_set_Item_m669229128_gshared (IndexedSet_1_t2088528067 * __this, int32_t ___index0, RuntimeObject * ___value1, const RuntimeMethod* method)
{
	RuntimeObject * V_0 = NULL;
	{
		List_1_t877107497 * L_0 = (List_1_t877107497 *)__this->get_m_List_0();
		int32_t L_1 = ___index0;
		NullCheck((List_1_t877107497 *)L_0);
		RuntimeObject * L_2 = ((  RuntimeObject * (*) (List_1_t877107497 *, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 14)->methodPointer)((List_1_t877107497 *)L_0, (int32_t)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 14));
		V_0 = (RuntimeObject *)L_2;
		Dictionary_2_t2084444880 * L_3 = (Dictionary_2_t2084444880 *)__this->get_m_Dictionary_1();
		RuntimeObject * L_4 = V_0;
		NullCheck((Dictionary_2_t2084444880 *)L_3);
		((  bool (*) (Dictionary_2_t2084444880 *, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 15)->methodPointer)((Dictionary_2_t2084444880 *)L_3, (RuntimeObject *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 15));
		List_1_t877107497 * L_5 = (List_1_t877107497 *)__this->get_m_List_0();
		int32_t L_6 = ___index0;
		RuntimeObject * L_7 = ___value1;
		NullCheck((List_1_t877107497 *)L_5);
		((  void (*) (List_1_t877107497 *, int32_t, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 17)->methodPointer)((List_1_t877107497 *)L_5, (int32_t)L_6, (RuntimeObject *)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 17));
		Dictionary_2_t2084444880 * L_8 = (Dictionary_2_t2084444880 *)__this->get_m_Dictionary_1();
		RuntimeObject * L_9 = V_0;
		int32_t L_10 = ___index0;
		NullCheck((Dictionary_2_t2084444880 *)L_8);
		((  void (*) (Dictionary_2_t2084444880 *, RuntimeObject *, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((Dictionary_2_t2084444880 *)L_8, (RuntimeObject *)L_9, (int32_t)L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		return;
	}
}
// System.Void UnityEngine.UI.Collections.IndexedSet`1<System.Object>::RemoveAll(System.Predicate`1<T>)
extern "C"  void IndexedSet_1_RemoveAll_m1002868182_gshared (IndexedSet_1_t2088528067 * __this, Predicate_1_t615195379 * ___match0, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	RuntimeObject * V_1 = NULL;
	{
		V_0 = (int32_t)0;
		goto IL_0034;
	}

IL_0008:
	{
		List_1_t877107497 * L_0 = (List_1_t877107497 *)__this->get_m_List_0();
		int32_t L_1 = V_0;
		NullCheck((List_1_t877107497 *)L_0);
		RuntimeObject * L_2 = ((  RuntimeObject * (*) (List_1_t877107497 *, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 14)->methodPointer)((List_1_t877107497 *)L_0, (int32_t)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 14));
		V_1 = (RuntimeObject *)L_2;
		Predicate_1_t615195379 * L_3 = ___match0;
		RuntimeObject * L_4 = V_1;
		NullCheck((Predicate_1_t615195379 *)L_3);
		bool L_5 = ((  bool (*) (Predicate_1_t615195379 *, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 19)->methodPointer)((Predicate_1_t615195379 *)L_3, (RuntimeObject *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 19));
		if (!L_5)
		{
			goto IL_002f;
		}
	}
	{
		RuntimeObject * L_6 = V_1;
		NullCheck((IndexedSet_1_t2088528067 *)__this);
		((  bool (*) (IndexedSet_1_t2088528067 *, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 20)->methodPointer)((IndexedSet_1_t2088528067 *)__this, (RuntimeObject *)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 20));
		goto IL_0033;
	}

IL_002f:
	{
		int32_t L_7 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_7+(int32_t)1));
	}

IL_0033:
	{
	}

IL_0034:
	{
		int32_t L_8 = V_0;
		List_1_t877107497 * L_9 = (List_1_t877107497 *)__this->get_m_List_0();
		NullCheck((List_1_t877107497 *)L_9);
		int32_t L_10 = ((  int32_t (*) (List_1_t877107497 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)((List_1_t877107497 *)L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		if ((((int32_t)L_8) < ((int32_t)L_10)))
		{
			goto IL_0008;
		}
	}
	{
		return;
	}
}
// System.Void UnityEngine.UI.Collections.IndexedSet`1<System.Object>::Sort(System.Comparison`1<T>)
extern "C"  void IndexedSet_1_Sort_m4178332250_gshared (IndexedSet_1_t2088528067 * __this, Comparison_1_t1058283020 * ___sortLayoutFunction0, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	RuntimeObject * V_1 = NULL;
	{
		List_1_t877107497 * L_0 = (List_1_t877107497 *)__this->get_m_List_0();
		Comparison_1_t1058283020 * L_1 = ___sortLayoutFunction0;
		NullCheck((List_1_t877107497 *)L_0);
		((  void (*) (List_1_t877107497 *, Comparison_1_t1058283020 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 21)->methodPointer)((List_1_t877107497 *)L_0, (Comparison_1_t1058283020 *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 21));
		V_0 = (int32_t)0;
		goto IL_0034;
	}

IL_0014:
	{
		List_1_t877107497 * L_2 = (List_1_t877107497 *)__this->get_m_List_0();
		int32_t L_3 = V_0;
		NullCheck((List_1_t877107497 *)L_2);
		RuntimeObject * L_4 = ((  RuntimeObject * (*) (List_1_t877107497 *, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 14)->methodPointer)((List_1_t877107497 *)L_2, (int32_t)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 14));
		V_1 = (RuntimeObject *)L_4;
		Dictionary_2_t2084444880 * L_5 = (Dictionary_2_t2084444880 *)__this->get_m_Dictionary_1();
		RuntimeObject * L_6 = V_1;
		int32_t L_7 = V_0;
		NullCheck((Dictionary_2_t2084444880 *)L_5);
		((  void (*) (Dictionary_2_t2084444880 *, RuntimeObject *, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 18)->methodPointer)((Dictionary_2_t2084444880 *)L_5, (RuntimeObject *)L_6, (int32_t)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 18));
		int32_t L_8 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_8+(int32_t)1));
	}

IL_0034:
	{
		int32_t L_9 = V_0;
		List_1_t877107497 * L_10 = (List_1_t877107497 *)__this->get_m_List_0();
		NullCheck((List_1_t877107497 *)L_10);
		int32_t L_11 = ((  int32_t (*) (List_1_t877107497 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)((List_1_t877107497 *)L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		if ((((int32_t)L_9) < ((int32_t)L_11)))
		{
			goto IL_0014;
		}
	}
	{
		return;
	}
}
// System.Void UnityEngine.UI.CoroutineTween.TweenRunner`1/<Start>c__Iterator0<UnityEngine.UI.CoroutineTween.ColorTween>::.ctor()
extern "C"  void U3CStartU3Ec__Iterator0__ctor_m3709008813_gshared (U3CStartU3Ec__Iterator0_t3365012439 * __this, const RuntimeMethod* method)
{
	{
		NullCheck((RuntimeObject *)__this);
		Object__ctor_m2906060009((RuntimeObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean UnityEngine.UI.CoroutineTween.TweenRunner`1/<Start>c__Iterator0<UnityEngine.UI.CoroutineTween.ColorTween>::MoveNext()
extern "C"  bool U3CStartU3Ec__Iterator0_MoveNext_m1847862442_gshared (U3CStartU3Ec__Iterator0_t3365012439 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CStartU3Ec__Iterator0_MoveNext_m1847862442_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	float G_B7_0 = 0.0f;
	U3CStartU3Ec__Iterator0_t3365012439 * G_B7_1 = NULL;
	float G_B6_0 = 0.0f;
	U3CStartU3Ec__Iterator0_t3365012439 * G_B6_1 = NULL;
	float G_B8_0 = 0.0f;
	float G_B8_1 = 0.0f;
	U3CStartU3Ec__Iterator0_t3365012439 * G_B8_2 = NULL;
	{
		int32_t L_0 = (int32_t)__this->get_U24PC_5();
		V_0 = (uint32_t)L_0;
		__this->set_U24PC_5((-1));
		uint32_t L_1 = V_0;
		switch (L_1)
		{
			case 0:
			{
				goto IL_0021;
			}
			case 1:
			{
				goto IL_00d5;
			}
		}
	}
	{
		goto IL_010f;
	}

IL_0021:
	{
		ColorTween_t3915639497 * L_2 = (ColorTween_t3915639497 *)__this->get_address_of_tweenInfo_0();
		bool L_3 = ColorTween_ValidTarget_m980511496((ColorTween_t3915639497 *)L_2, /*hidden argument*/NULL);
		if (L_3)
		{
			goto IL_003d;
		}
	}
	{
		goto IL_010f;
	}

IL_003d:
	{
		__this->set_U3CelapsedTimeU3E__0_1((0.0f));
		goto IL_00d6;
	}

IL_004d:
	{
		float L_4 = (float)__this->get_U3CelapsedTimeU3E__0_1();
		ColorTween_t3915639497 * L_5 = (ColorTween_t3915639497 *)__this->get_address_of_tweenInfo_0();
		bool L_6 = ColorTween_get_ignoreTimeScale_m3179503341((ColorTween_t3915639497 *)L_5, /*hidden argument*/NULL);
		G_B6_0 = L_4;
		G_B6_1 = ((U3CStartU3Ec__Iterator0_t3365012439 *)(__this));
		if (!L_6)
		{
			G_B7_0 = L_4;
			G_B7_1 = ((U3CStartU3Ec__Iterator0_t3365012439 *)(__this));
			goto IL_0075;
		}
	}
	{
		float L_7 = Time_get_unscaledDeltaTime_m3382011778(NULL /*static, unused*/, /*hidden argument*/NULL);
		G_B8_0 = L_7;
		G_B8_1 = G_B6_0;
		G_B8_2 = ((U3CStartU3Ec__Iterator0_t3365012439 *)(G_B6_1));
		goto IL_007a;
	}

IL_0075:
	{
		float L_8 = Time_get_deltaTime_m185246434(NULL /*static, unused*/, /*hidden argument*/NULL);
		G_B8_0 = L_8;
		G_B8_1 = G_B7_0;
		G_B8_2 = ((U3CStartU3Ec__Iterator0_t3365012439 *)(G_B7_1));
	}

IL_007a:
	{
		NullCheck(G_B8_2);
		G_B8_2->set_U3CelapsedTimeU3E__0_1(((float)((float)G_B8_1+(float)G_B8_0)));
		float L_9 = (float)__this->get_U3CelapsedTimeU3E__0_1();
		ColorTween_t3915639497 * L_10 = (ColorTween_t3915639497 *)__this->get_address_of_tweenInfo_0();
		float L_11 = ColorTween_get_duration_m1500655497((ColorTween_t3915639497 *)L_10, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t4094287654_il2cpp_TypeInfo_var);
		float L_12 = Mathf_Clamp01_m664342029(NULL /*static, unused*/, (float)((float)((float)L_9/(float)L_11)), /*hidden argument*/NULL);
		__this->set_U3CpercentageU3E__1_2(L_12);
		ColorTween_t3915639497 * L_13 = (ColorTween_t3915639497 *)__this->get_address_of_tweenInfo_0();
		float L_14 = (float)__this->get_U3CpercentageU3E__1_2();
		ColorTween_TweenValue_m3743140801((ColorTween_t3915639497 *)L_13, (float)L_14, /*hidden argument*/NULL);
		__this->set_U24current_3(NULL);
		bool L_15 = (bool)__this->get_U24disposing_4();
		if (L_15)
		{
			goto IL_00d0;
		}
	}
	{
		__this->set_U24PC_5(1);
	}

IL_00d0:
	{
		goto IL_0111;
	}

IL_00d5:
	{
	}

IL_00d6:
	{
		float L_16 = (float)__this->get_U3CelapsedTimeU3E__0_1();
		ColorTween_t3915639497 * L_17 = (ColorTween_t3915639497 *)__this->get_address_of_tweenInfo_0();
		float L_18 = ColorTween_get_duration_m1500655497((ColorTween_t3915639497 *)L_17, /*hidden argument*/NULL);
		if ((((float)L_16) < ((float)L_18)))
		{
			goto IL_004d;
		}
	}
	{
		ColorTween_t3915639497 * L_19 = (ColorTween_t3915639497 *)__this->get_address_of_tweenInfo_0();
		ColorTween_TweenValue_m3743140801((ColorTween_t3915639497 *)L_19, (float)(1.0f), /*hidden argument*/NULL);
		__this->set_U24PC_5((-1));
	}

IL_010f:
	{
		return (bool)0;
	}

IL_0111:
	{
		return (bool)1;
	}
}
// System.Object UnityEngine.UI.CoroutineTween.TweenRunner`1/<Start>c__Iterator0<UnityEngine.UI.CoroutineTween.ColorTween>::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  RuntimeObject * U3CStartU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2566029981_gshared (U3CStartU3Ec__Iterator0_t3365012439 * __this, const RuntimeMethod* method)
{
	RuntimeObject * V_0 = NULL;
	{
		RuntimeObject * L_0 = (RuntimeObject *)__this->get_U24current_3();
		V_0 = (RuntimeObject *)L_0;
		goto IL_000c;
	}

IL_000c:
	{
		RuntimeObject * L_1 = V_0;
		return L_1;
	}
}
// System.Object UnityEngine.UI.CoroutineTween.TweenRunner`1/<Start>c__Iterator0<UnityEngine.UI.CoroutineTween.ColorTween>::System.Collections.IEnumerator.get_Current()
extern "C"  RuntimeObject * U3CStartU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m2630272077_gshared (U3CStartU3Ec__Iterator0_t3365012439 * __this, const RuntimeMethod* method)
{
	RuntimeObject * V_0 = NULL;
	{
		RuntimeObject * L_0 = (RuntimeObject *)__this->get_U24current_3();
		V_0 = (RuntimeObject *)L_0;
		goto IL_000c;
	}

IL_000c:
	{
		RuntimeObject * L_1 = V_0;
		return L_1;
	}
}
// System.Void UnityEngine.UI.CoroutineTween.TweenRunner`1/<Start>c__Iterator0<UnityEngine.UI.CoroutineTween.ColorTween>::Dispose()
extern "C"  void U3CStartU3Ec__Iterator0_Dispose_m2550299288_gshared (U3CStartU3Ec__Iterator0_t3365012439 * __this, const RuntimeMethod* method)
{
	{
		__this->set_U24disposing_4((bool)1);
		__this->set_U24PC_5((-1));
		return;
	}
}
// System.Void UnityEngine.UI.CoroutineTween.TweenRunner`1/<Start>c__Iterator0<UnityEngine.UI.CoroutineTween.ColorTween>::Reset()
extern "C"  void U3CStartU3Ec__Iterator0_Reset_m2520601135_gshared (U3CStartU3Ec__Iterator0_t3365012439 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CStartU3Ec__Iterator0_Reset_m2520601135_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1045231613 * L_0 = (NotSupportedException_t1045231613 *)il2cpp_codegen_object_new(NotSupportedException_t1045231613_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m1411024145(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void UnityEngine.UI.CoroutineTween.TweenRunner`1/<Start>c__Iterator0<UnityEngine.UI.CoroutineTween.FloatTween>::.ctor()
extern "C"  void U3CStartU3Ec__Iterator0__ctor_m24058626_gshared (U3CStartU3Ec__Iterator0_t2935223578 * __this, const RuntimeMethod* method)
{
	{
		NullCheck((RuntimeObject *)__this);
		Object__ctor_m2906060009((RuntimeObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean UnityEngine.UI.CoroutineTween.TweenRunner`1/<Start>c__Iterator0<UnityEngine.UI.CoroutineTween.FloatTween>::MoveNext()
extern "C"  bool U3CStartU3Ec__Iterator0_MoveNext_m1708609373_gshared (U3CStartU3Ec__Iterator0_t2935223578 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CStartU3Ec__Iterator0_MoveNext_m1708609373_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	float G_B7_0 = 0.0f;
	U3CStartU3Ec__Iterator0_t2935223578 * G_B7_1 = NULL;
	float G_B6_0 = 0.0f;
	U3CStartU3Ec__Iterator0_t2935223578 * G_B6_1 = NULL;
	float G_B8_0 = 0.0f;
	float G_B8_1 = 0.0f;
	U3CStartU3Ec__Iterator0_t2935223578 * G_B8_2 = NULL;
	{
		int32_t L_0 = (int32_t)__this->get_U24PC_5();
		V_0 = (uint32_t)L_0;
		__this->set_U24PC_5((-1));
		uint32_t L_1 = V_0;
		switch (L_1)
		{
			case 0:
			{
				goto IL_0021;
			}
			case 1:
			{
				goto IL_00d5;
			}
		}
	}
	{
		goto IL_010f;
	}

IL_0021:
	{
		FloatTween_t3485850636 * L_2 = (FloatTween_t3485850636 *)__this->get_address_of_tweenInfo_0();
		bool L_3 = FloatTween_ValidTarget_m1876138479((FloatTween_t3485850636 *)L_2, /*hidden argument*/NULL);
		if (L_3)
		{
			goto IL_003d;
		}
	}
	{
		goto IL_010f;
	}

IL_003d:
	{
		__this->set_U3CelapsedTimeU3E__0_1((0.0f));
		goto IL_00d6;
	}

IL_004d:
	{
		float L_4 = (float)__this->get_U3CelapsedTimeU3E__0_1();
		FloatTween_t3485850636 * L_5 = (FloatTween_t3485850636 *)__this->get_address_of_tweenInfo_0();
		bool L_6 = FloatTween_get_ignoreTimeScale_m1804933600((FloatTween_t3485850636 *)L_5, /*hidden argument*/NULL);
		G_B6_0 = L_4;
		G_B6_1 = ((U3CStartU3Ec__Iterator0_t2935223578 *)(__this));
		if (!L_6)
		{
			G_B7_0 = L_4;
			G_B7_1 = ((U3CStartU3Ec__Iterator0_t2935223578 *)(__this));
			goto IL_0075;
		}
	}
	{
		float L_7 = Time_get_unscaledDeltaTime_m3382011778(NULL /*static, unused*/, /*hidden argument*/NULL);
		G_B8_0 = L_7;
		G_B8_1 = G_B6_0;
		G_B8_2 = ((U3CStartU3Ec__Iterator0_t2935223578 *)(G_B6_1));
		goto IL_007a;
	}

IL_0075:
	{
		float L_8 = Time_get_deltaTime_m185246434(NULL /*static, unused*/, /*hidden argument*/NULL);
		G_B8_0 = L_8;
		G_B8_1 = G_B7_0;
		G_B8_2 = ((U3CStartU3Ec__Iterator0_t2935223578 *)(G_B7_1));
	}

IL_007a:
	{
		NullCheck(G_B8_2);
		G_B8_2->set_U3CelapsedTimeU3E__0_1(((float)((float)G_B8_1+(float)G_B8_0)));
		float L_9 = (float)__this->get_U3CelapsedTimeU3E__0_1();
		FloatTween_t3485850636 * L_10 = (FloatTween_t3485850636 *)__this->get_address_of_tweenInfo_0();
		float L_11 = FloatTween_get_duration_m3446392591((FloatTween_t3485850636 *)L_10, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t4094287654_il2cpp_TypeInfo_var);
		float L_12 = Mathf_Clamp01_m664342029(NULL /*static, unused*/, (float)((float)((float)L_9/(float)L_11)), /*hidden argument*/NULL);
		__this->set_U3CpercentageU3E__1_2(L_12);
		FloatTween_t3485850636 * L_13 = (FloatTween_t3485850636 *)__this->get_address_of_tweenInfo_0();
		float L_14 = (float)__this->get_U3CpercentageU3E__1_2();
		FloatTween_TweenValue_m4176001828((FloatTween_t3485850636 *)L_13, (float)L_14, /*hidden argument*/NULL);
		__this->set_U24current_3(NULL);
		bool L_15 = (bool)__this->get_U24disposing_4();
		if (L_15)
		{
			goto IL_00d0;
		}
	}
	{
		__this->set_U24PC_5(1);
	}

IL_00d0:
	{
		goto IL_0111;
	}

IL_00d5:
	{
	}

IL_00d6:
	{
		float L_16 = (float)__this->get_U3CelapsedTimeU3E__0_1();
		FloatTween_t3485850636 * L_17 = (FloatTween_t3485850636 *)__this->get_address_of_tweenInfo_0();
		float L_18 = FloatTween_get_duration_m3446392591((FloatTween_t3485850636 *)L_17, /*hidden argument*/NULL);
		if ((((float)L_16) < ((float)L_18)))
		{
			goto IL_004d;
		}
	}
	{
		FloatTween_t3485850636 * L_19 = (FloatTween_t3485850636 *)__this->get_address_of_tweenInfo_0();
		FloatTween_TweenValue_m4176001828((FloatTween_t3485850636 *)L_19, (float)(1.0f), /*hidden argument*/NULL);
		__this->set_U24PC_5((-1));
	}

IL_010f:
	{
		return (bool)0;
	}

IL_0111:
	{
		return (bool)1;
	}
}
// System.Object UnityEngine.UI.CoroutineTween.TweenRunner`1/<Start>c__Iterator0<UnityEngine.UI.CoroutineTween.FloatTween>::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  RuntimeObject * U3CStartU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3390933514_gshared (U3CStartU3Ec__Iterator0_t2935223578 * __this, const RuntimeMethod* method)
{
	RuntimeObject * V_0 = NULL;
	{
		RuntimeObject * L_0 = (RuntimeObject *)__this->get_U24current_3();
		V_0 = (RuntimeObject *)L_0;
		goto IL_000c;
	}

IL_000c:
	{
		RuntimeObject * L_1 = V_0;
		return L_1;
	}
}
// System.Object UnityEngine.UI.CoroutineTween.TweenRunner`1/<Start>c__Iterator0<UnityEngine.UI.CoroutineTween.FloatTween>::System.Collections.IEnumerator.get_Current()
extern "C"  RuntimeObject * U3CStartU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m2473046241_gshared (U3CStartU3Ec__Iterator0_t2935223578 * __this, const RuntimeMethod* method)
{
	RuntimeObject * V_0 = NULL;
	{
		RuntimeObject * L_0 = (RuntimeObject *)__this->get_U24current_3();
		V_0 = (RuntimeObject *)L_0;
		goto IL_000c;
	}

IL_000c:
	{
		RuntimeObject * L_1 = V_0;
		return L_1;
	}
}
// System.Void UnityEngine.UI.CoroutineTween.TweenRunner`1/<Start>c__Iterator0<UnityEngine.UI.CoroutineTween.FloatTween>::Dispose()
extern "C"  void U3CStartU3Ec__Iterator0_Dispose_m1276107022_gshared (U3CStartU3Ec__Iterator0_t2935223578 * __this, const RuntimeMethod* method)
{
	{
		__this->set_U24disposing_4((bool)1);
		__this->set_U24PC_5((-1));
		return;
	}
}
// System.Void UnityEngine.UI.CoroutineTween.TweenRunner`1/<Start>c__Iterator0<UnityEngine.UI.CoroutineTween.FloatTween>::Reset()
extern "C"  void U3CStartU3Ec__Iterator0_Reset_m810840618_gshared (U3CStartU3Ec__Iterator0_t2935223578 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CStartU3Ec__Iterator0_Reset_m810840618_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1045231613 * L_0 = (NotSupportedException_t1045231613 *)il2cpp_codegen_object_new(NotSupportedException_t1045231613_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m1411024145(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween>::.ctor()
extern "C"  void TweenRunner_1__ctor_m1665316750_gshared (TweenRunner_1_t645809502 * __this, const RuntimeMethod* method)
{
	{
		NullCheck((RuntimeObject *)__this);
		Object__ctor_m2906060009((RuntimeObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.IEnumerator UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween>::Start(T)
extern "C"  RuntimeObject* TweenRunner_1_Start_m3464271052_gshared (RuntimeObject * __this /* static, unused */, ColorTween_t3915639497  ___tweenInfo0, const RuntimeMethod* method)
{
	U3CStartU3Ec__Iterator0_t3365012439 * V_0 = NULL;
	RuntimeObject* V_1 = NULL;
	{
		U3CStartU3Ec__Iterator0_t3365012439 * L_0 = (U3CStartU3Ec__Iterator0_t3365012439 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		((  void (*) (U3CStartU3Ec__Iterator0_t3365012439 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->methodPointer)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		V_0 = (U3CStartU3Ec__Iterator0_t3365012439 *)L_0;
		U3CStartU3Ec__Iterator0_t3365012439 * L_1 = V_0;
		ColorTween_t3915639497  L_2 = ___tweenInfo0;
		NullCheck(L_1);
		L_1->set_tweenInfo_0(L_2);
		U3CStartU3Ec__Iterator0_t3365012439 * L_3 = V_0;
		V_1 = (RuntimeObject*)L_3;
		goto IL_0014;
	}

IL_0014:
	{
		RuntimeObject* L_4 = V_1;
		return L_4;
	}
}
// System.Void UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween>::Init(UnityEngine.MonoBehaviour)
extern "C"  void TweenRunner_1_Init_m431308678_gshared (TweenRunner_1_t645809502 * __this, MonoBehaviour_t345688271 * ___coroutineContainer0, const RuntimeMethod* method)
{
	{
		MonoBehaviour_t345688271 * L_0 = ___coroutineContainer0;
		__this->set_m_CoroutineContainer_0(L_0);
		return;
	}
}
// System.Void UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween>::StartTween(T)
extern "C"  void TweenRunner_1_StartTween_m550936528_gshared (TweenRunner_1_t645809502 * __this, ColorTween_t3915639497  ___info0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenRunner_1_StartTween_m550936528_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		MonoBehaviour_t345688271 * L_0 = (MonoBehaviour_t345688271 *)__this->get_m_CoroutineContainer_0();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1502412432_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_m2342070775(NULL /*static, unused*/, (Object_t1502412432 *)L_0, (Object_t1502412432 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0022;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3525713211_il2cpp_TypeInfo_var);
		Debug_LogWarning_m1906964694(NULL /*static, unused*/, (RuntimeObject *)_stringLiteral1668423132, /*hidden argument*/NULL);
		goto IL_0073;
	}

IL_0022:
	{
		NullCheck((TweenRunner_1_t645809502 *)__this);
		((  void (*) (TweenRunner_1_t645809502 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)((TweenRunner_1_t645809502 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		MonoBehaviour_t345688271 * L_2 = (MonoBehaviour_t345688271 *)__this->get_m_CoroutineContainer_0();
		NullCheck((Component_t4087199522 *)L_2);
		GameObject_t1811656094 * L_3 = Component_get_gameObject_m2283027183((Component_t4087199522 *)L_2, /*hidden argument*/NULL);
		NullCheck((GameObject_t1811656094 *)L_3);
		bool L_4 = GameObject_get_activeInHierarchy_m1361876394((GameObject_t1811656094 *)L_3, /*hidden argument*/NULL);
		if (L_4)
		{
			goto IL_0055;
		}
	}
	{
		ColorTween_TweenValue_m3743140801((ColorTween_t3915639497 *)(&___info0), (float)(1.0f), /*hidden argument*/NULL);
		goto IL_0073;
	}

IL_0055:
	{
		ColorTween_t3915639497  L_5 = ___info0;
		RuntimeObject* L_6 = ((  RuntimeObject* (*) (RuntimeObject * /* static, unused */, ColorTween_t3915639497 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)(NULL /*static, unused*/, (ColorTween_t3915639497 )L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		__this->set_m_Tween_1(L_6);
		MonoBehaviour_t345688271 * L_7 = (MonoBehaviour_t345688271 *)__this->get_m_CoroutineContainer_0();
		RuntimeObject* L_8 = (RuntimeObject*)__this->get_m_Tween_1();
		NullCheck((MonoBehaviour_t345688271 *)L_7);
		MonoBehaviour_StartCoroutine_m2747597228((MonoBehaviour_t345688271 *)L_7, (RuntimeObject*)L_8, /*hidden argument*/NULL);
	}

IL_0073:
	{
		return;
	}
}
// System.Void UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween>::StopTween()
extern "C"  void TweenRunner_1_StopTween_m404705734_gshared (TweenRunner_1_t645809502 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject* L_0 = (RuntimeObject*)__this->get_m_Tween_1();
		if (!L_0)
		{
			goto IL_0026;
		}
	}
	{
		MonoBehaviour_t345688271 * L_1 = (MonoBehaviour_t345688271 *)__this->get_m_CoroutineContainer_0();
		RuntimeObject* L_2 = (RuntimeObject*)__this->get_m_Tween_1();
		NullCheck((MonoBehaviour_t345688271 *)L_1);
		MonoBehaviour_StopCoroutine_m366373519((MonoBehaviour_t345688271 *)L_1, (RuntimeObject*)L_2, /*hidden argument*/NULL);
		__this->set_m_Tween_1((RuntimeObject*)NULL);
	}

IL_0026:
	{
		return;
	}
}
// System.Void UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.FloatTween>::.ctor()
extern "C"  void TweenRunner_1__ctor_m689267585_gshared (TweenRunner_1_t216020641 * __this, const RuntimeMethod* method)
{
	{
		NullCheck((RuntimeObject *)__this);
		Object__ctor_m2906060009((RuntimeObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.IEnumerator UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.FloatTween>::Start(T)
extern "C"  RuntimeObject* TweenRunner_1_Start_m1663826642_gshared (RuntimeObject * __this /* static, unused */, FloatTween_t3485850636  ___tweenInfo0, const RuntimeMethod* method)
{
	U3CStartU3Ec__Iterator0_t2935223578 * V_0 = NULL;
	RuntimeObject* V_1 = NULL;
	{
		U3CStartU3Ec__Iterator0_t2935223578 * L_0 = (U3CStartU3Ec__Iterator0_t2935223578 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		((  void (*) (U3CStartU3Ec__Iterator0_t2935223578 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->methodPointer)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		V_0 = (U3CStartU3Ec__Iterator0_t2935223578 *)L_0;
		U3CStartU3Ec__Iterator0_t2935223578 * L_1 = V_0;
		FloatTween_t3485850636  L_2 = ___tweenInfo0;
		NullCheck(L_1);
		L_1->set_tweenInfo_0(L_2);
		U3CStartU3Ec__Iterator0_t2935223578 * L_3 = V_0;
		V_1 = (RuntimeObject*)L_3;
		goto IL_0014;
	}

IL_0014:
	{
		RuntimeObject* L_4 = V_1;
		return L_4;
	}
}
// System.Void UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.FloatTween>::Init(UnityEngine.MonoBehaviour)
extern "C"  void TweenRunner_1_Init_m3188868685_gshared (TweenRunner_1_t216020641 * __this, MonoBehaviour_t345688271 * ___coroutineContainer0, const RuntimeMethod* method)
{
	{
		MonoBehaviour_t345688271 * L_0 = ___coroutineContainer0;
		__this->set_m_CoroutineContainer_0(L_0);
		return;
	}
}
// System.Void UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.FloatTween>::StartTween(T)
extern "C"  void TweenRunner_1_StartTween_m2649818531_gshared (TweenRunner_1_t216020641 * __this, FloatTween_t3485850636  ___info0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenRunner_1_StartTween_m2649818531_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		MonoBehaviour_t345688271 * L_0 = (MonoBehaviour_t345688271 *)__this->get_m_CoroutineContainer_0();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1502412432_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_m2342070775(NULL /*static, unused*/, (Object_t1502412432 *)L_0, (Object_t1502412432 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0022;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3525713211_il2cpp_TypeInfo_var);
		Debug_LogWarning_m1906964694(NULL /*static, unused*/, (RuntimeObject *)_stringLiteral1668423132, /*hidden argument*/NULL);
		goto IL_0073;
	}

IL_0022:
	{
		NullCheck((TweenRunner_1_t216020641 *)__this);
		((  void (*) (TweenRunner_1_t216020641 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)((TweenRunner_1_t216020641 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		MonoBehaviour_t345688271 * L_2 = (MonoBehaviour_t345688271 *)__this->get_m_CoroutineContainer_0();
		NullCheck((Component_t4087199522 *)L_2);
		GameObject_t1811656094 * L_3 = Component_get_gameObject_m2283027183((Component_t4087199522 *)L_2, /*hidden argument*/NULL);
		NullCheck((GameObject_t1811656094 *)L_3);
		bool L_4 = GameObject_get_activeInHierarchy_m1361876394((GameObject_t1811656094 *)L_3, /*hidden argument*/NULL);
		if (L_4)
		{
			goto IL_0055;
		}
	}
	{
		FloatTween_TweenValue_m4176001828((FloatTween_t3485850636 *)(&___info0), (float)(1.0f), /*hidden argument*/NULL);
		goto IL_0073;
	}

IL_0055:
	{
		FloatTween_t3485850636  L_5 = ___info0;
		RuntimeObject* L_6 = ((  RuntimeObject* (*) (RuntimeObject * /* static, unused */, FloatTween_t3485850636 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)(NULL /*static, unused*/, (FloatTween_t3485850636 )L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		__this->set_m_Tween_1(L_6);
		MonoBehaviour_t345688271 * L_7 = (MonoBehaviour_t345688271 *)__this->get_m_CoroutineContainer_0();
		RuntimeObject* L_8 = (RuntimeObject*)__this->get_m_Tween_1();
		NullCheck((MonoBehaviour_t345688271 *)L_7);
		MonoBehaviour_StartCoroutine_m2747597228((MonoBehaviour_t345688271 *)L_7, (RuntimeObject*)L_8, /*hidden argument*/NULL);
	}

IL_0073:
	{
		return;
	}
}
// System.Void UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.FloatTween>::StopTween()
extern "C"  void TweenRunner_1_StopTween_m1366099789_gshared (TweenRunner_1_t216020641 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject* L_0 = (RuntimeObject*)__this->get_m_Tween_1();
		if (!L_0)
		{
			goto IL_0026;
		}
	}
	{
		MonoBehaviour_t345688271 * L_1 = (MonoBehaviour_t345688271 *)__this->get_m_CoroutineContainer_0();
		RuntimeObject* L_2 = (RuntimeObject*)__this->get_m_Tween_1();
		NullCheck((MonoBehaviour_t345688271 *)L_1);
		MonoBehaviour_StopCoroutine_m366373519((MonoBehaviour_t345688271 *)L_1, (RuntimeObject*)L_2, /*hidden argument*/NULL);
		__this->set_m_Tween_1((RuntimeObject*)NULL);
	}

IL_0026:
	{
		return;
	}
}
// System.Collections.Generic.List`1<T> UnityEngine.UI.ListPool`1<System.Int32>::Get()
extern "C"  List_1_t3093093796 * ListPool_1_Get_m1188930183_gshared (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	List_1_t3093093796 * V_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		ObjectPool_1_t3935811425 * L_0 = ((ListPool_1_t2987867989_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)))->get_s_ListPool_0();
		NullCheck((ObjectPool_1_t3935811425 *)L_0);
		List_1_t3093093796 * L_1 = ((  List_1_t3093093796 * (*) (ObjectPool_1_t3935811425 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->methodPointer)((ObjectPool_1_t3935811425 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		V_0 = (List_1_t3093093796 *)L_1;
		goto IL_0011;
	}

IL_0011:
	{
		List_1_t3093093796 * L_2 = V_0;
		return L_2;
	}
}
// System.Void UnityEngine.UI.ListPool`1<System.Int32>::Release(System.Collections.Generic.List`1<T>)
extern "C"  void ListPool_1_Release_m2404475836_gshared (RuntimeObject * __this /* static, unused */, List_1_t3093093796 * ___toRelease0, const RuntimeMethod* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		ObjectPool_1_t3935811425 * L_0 = ((ListPool_1_t2987867989_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)))->get_s_ListPool_0();
		List_1_t3093093796 * L_1 = ___toRelease0;
		NullCheck((ObjectPool_1_t3935811425 *)L_0);
		((  void (*) (ObjectPool_1_t3935811425 *, List_1_t3093093796 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((ObjectPool_1_t3935811425 *)L_0, (List_1_t3093093796 *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return;
	}
}
// System.Void UnityEngine.UI.ListPool`1<System.Int32>::.cctor()
extern "C"  void ListPool_1__cctor_m3489468707_gshared (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	{
		IntPtr_t L_0;
		L_0.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		UnityAction_1_t2345944588 * L_1 = (UnityAction_1_t2345944588 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (UnityAction_1_t2345944588 *, RuntimeObject *, IntPtr_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->methodPointer)(L_1, (RuntimeObject *)NULL, (IntPtr_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		ObjectPool_1_t3935811425 * L_2 = (ObjectPool_1_t3935811425 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		((  void (*) (ObjectPool_1_t3935811425 *, UnityAction_1_t2345944588 *, UnityAction_1_t2345944588 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7)->methodPointer)(L_2, (UnityAction_1_t2345944588 *)NULL, (UnityAction_1_t2345944588 *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		((ListPool_1_t2987867989_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)))->set_s_ListPool_0(L_2);
		return;
	}
}
// System.Void UnityEngine.UI.ListPool`1<System.Int32>::<s_ListPool>m__0(System.Collections.Generic.List`1<T>)
extern "C"  void ListPool_1_U3Cs_ListPoolU3Em__0_m4041423650_gshared (RuntimeObject * __this /* static, unused */, List_1_t3093093796 * ___l0, const RuntimeMethod* method)
{
	{
		List_1_t3093093796 * L_0 = ___l0;
		NullCheck((List_1_t3093093796 *)L_0);
		((  void (*) (List_1_t3093093796 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8)->methodPointer)((List_1_t3093093796 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
		return;
	}
}
// System.Collections.Generic.List`1<T> UnityEngine.UI.ListPool`1<System.Object>::Get()
extern "C"  List_1_t877107497 * ListPool_1_Get_m1083610225_gshared (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	List_1_t877107497 * V_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		ObjectPool_1_t1719825126 * L_0 = ((ListPool_1_t771881690_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)))->get_s_ListPool_0();
		NullCheck((ObjectPool_1_t1719825126 *)L_0);
		List_1_t877107497 * L_1 = ((  List_1_t877107497 * (*) (ObjectPool_1_t1719825126 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->methodPointer)((ObjectPool_1_t1719825126 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		V_0 = (List_1_t877107497 *)L_1;
		goto IL_0011;
	}

IL_0011:
	{
		List_1_t877107497 * L_2 = V_0;
		return L_2;
	}
}
// System.Void UnityEngine.UI.ListPool`1<System.Object>::Release(System.Collections.Generic.List`1<T>)
extern "C"  void ListPool_1_Release_m2732476835_gshared (RuntimeObject * __this /* static, unused */, List_1_t877107497 * ___toRelease0, const RuntimeMethod* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		ObjectPool_1_t1719825126 * L_0 = ((ListPool_1_t771881690_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)))->get_s_ListPool_0();
		List_1_t877107497 * L_1 = ___toRelease0;
		NullCheck((ObjectPool_1_t1719825126 *)L_0);
		((  void (*) (ObjectPool_1_t1719825126 *, List_1_t877107497 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((ObjectPool_1_t1719825126 *)L_0, (List_1_t877107497 *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return;
	}
}
// System.Void UnityEngine.UI.ListPool`1<System.Object>::.cctor()
extern "C"  void ListPool_1__cctor_m4130707747_gshared (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	{
		IntPtr_t L_0;
		L_0.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		UnityAction_1_t129958289 * L_1 = (UnityAction_1_t129958289 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (UnityAction_1_t129958289 *, RuntimeObject *, IntPtr_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->methodPointer)(L_1, (RuntimeObject *)NULL, (IntPtr_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		ObjectPool_1_t1719825126 * L_2 = (ObjectPool_1_t1719825126 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		((  void (*) (ObjectPool_1_t1719825126 *, UnityAction_1_t129958289 *, UnityAction_1_t129958289 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7)->methodPointer)(L_2, (UnityAction_1_t129958289 *)NULL, (UnityAction_1_t129958289 *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		((ListPool_1_t771881690_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)))->set_s_ListPool_0(L_2);
		return;
	}
}
// System.Void UnityEngine.UI.ListPool`1<System.Object>::<s_ListPool>m__0(System.Collections.Generic.List`1<T>)
extern "C"  void ListPool_1_U3Cs_ListPoolU3Em__0_m1399301744_gshared (RuntimeObject * __this /* static, unused */, List_1_t877107497 * ___l0, const RuntimeMethod* method)
{
	{
		List_1_t877107497 * L_0 = ___l0;
		NullCheck((List_1_t877107497 *)L_0);
		((  void (*) (List_1_t877107497 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8)->methodPointer)((List_1_t877107497 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
		return;
	}
}
// System.Collections.Generic.List`1<T> UnityEngine.UI.ListPool`1<UnityEngine.Color32>::Get()
extern "C"  List_1_t3435548858 * ListPool_1_Get_m634527380_gshared (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	List_1_t3435548858 * V_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		ObjectPool_1_t4278266487 * L_0 = ((ListPool_1_t3330323051_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)))->get_s_ListPool_0();
		NullCheck((ObjectPool_1_t4278266487 *)L_0);
		List_1_t3435548858 * L_1 = ((  List_1_t3435548858 * (*) (ObjectPool_1_t4278266487 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->methodPointer)((ObjectPool_1_t4278266487 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		V_0 = (List_1_t3435548858 *)L_1;
		goto IL_0011;
	}

IL_0011:
	{
		List_1_t3435548858 * L_2 = V_0;
		return L_2;
	}
}
// System.Void UnityEngine.UI.ListPool`1<UnityEngine.Color32>::Release(System.Collections.Generic.List`1<T>)
extern "C"  void ListPool_1_Release_m3831616937_gshared (RuntimeObject * __this /* static, unused */, List_1_t3435548858 * ___toRelease0, const RuntimeMethod* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		ObjectPool_1_t4278266487 * L_0 = ((ListPool_1_t3330323051_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)))->get_s_ListPool_0();
		List_1_t3435548858 * L_1 = ___toRelease0;
		NullCheck((ObjectPool_1_t4278266487 *)L_0);
		((  void (*) (ObjectPool_1_t4278266487 *, List_1_t3435548858 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((ObjectPool_1_t4278266487 *)L_0, (List_1_t3435548858 *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return;
	}
}
// System.Void UnityEngine.UI.ListPool`1<UnityEngine.Color32>::.cctor()
extern "C"  void ListPool_1__cctor_m3237983855_gshared (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	{
		IntPtr_t L_0;
		L_0.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		UnityAction_1_t2688399650 * L_1 = (UnityAction_1_t2688399650 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (UnityAction_1_t2688399650 *, RuntimeObject *, IntPtr_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->methodPointer)(L_1, (RuntimeObject *)NULL, (IntPtr_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		ObjectPool_1_t4278266487 * L_2 = (ObjectPool_1_t4278266487 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		((  void (*) (ObjectPool_1_t4278266487 *, UnityAction_1_t2688399650 *, UnityAction_1_t2688399650 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7)->methodPointer)(L_2, (UnityAction_1_t2688399650 *)NULL, (UnityAction_1_t2688399650 *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		((ListPool_1_t3330323051_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)))->set_s_ListPool_0(L_2);
		return;
	}
}
// System.Void UnityEngine.UI.ListPool`1<UnityEngine.Color32>::<s_ListPool>m__0(System.Collections.Generic.List`1<T>)
extern "C"  void ListPool_1_U3Cs_ListPoolU3Em__0_m3811197984_gshared (RuntimeObject * __this /* static, unused */, List_1_t3435548858 * ___l0, const RuntimeMethod* method)
{
	{
		List_1_t3435548858 * L_0 = ___l0;
		NullCheck((List_1_t3435548858 *)L_0);
		((  void (*) (List_1_t3435548858 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8)->methodPointer)((List_1_t3435548858 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
		return;
	}
}
// System.Collections.Generic.List`1<T> UnityEngine.UI.ListPool`1<UnityEngine.UIVertex>::Get()
extern "C"  List_1_t1641616647 * ListPool_1_Get_m4172079206_gshared (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	List_1_t1641616647 * V_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		ObjectPool_1_t2484334276 * L_0 = ((ListPool_1_t1536390840_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)))->get_s_ListPool_0();
		NullCheck((ObjectPool_1_t2484334276 *)L_0);
		List_1_t1641616647 * L_1 = ((  List_1_t1641616647 * (*) (ObjectPool_1_t2484334276 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->methodPointer)((ObjectPool_1_t2484334276 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		V_0 = (List_1_t1641616647 *)L_1;
		goto IL_0011;
	}

IL_0011:
	{
		List_1_t1641616647 * L_2 = V_0;
		return L_2;
	}
}
// System.Void UnityEngine.UI.ListPool`1<UnityEngine.UIVertex>::Release(System.Collections.Generic.List`1<T>)
extern "C"  void ListPool_1_Release_m685237745_gshared (RuntimeObject * __this /* static, unused */, List_1_t1641616647 * ___toRelease0, const RuntimeMethod* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		ObjectPool_1_t2484334276 * L_0 = ((ListPool_1_t1536390840_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)))->get_s_ListPool_0();
		List_1_t1641616647 * L_1 = ___toRelease0;
		NullCheck((ObjectPool_1_t2484334276 *)L_0);
		((  void (*) (ObjectPool_1_t2484334276 *, List_1_t1641616647 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((ObjectPool_1_t2484334276 *)L_0, (List_1_t1641616647 *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return;
	}
}
// System.Void UnityEngine.UI.ListPool`1<UnityEngine.UIVertex>::.cctor()
extern "C"  void ListPool_1__cctor_m2401039243_gshared (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	{
		IntPtr_t L_0;
		L_0.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		UnityAction_1_t894467439 * L_1 = (UnityAction_1_t894467439 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (UnityAction_1_t894467439 *, RuntimeObject *, IntPtr_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->methodPointer)(L_1, (RuntimeObject *)NULL, (IntPtr_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		ObjectPool_1_t2484334276 * L_2 = (ObjectPool_1_t2484334276 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		((  void (*) (ObjectPool_1_t2484334276 *, UnityAction_1_t894467439 *, UnityAction_1_t894467439 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7)->methodPointer)(L_2, (UnityAction_1_t894467439 *)NULL, (UnityAction_1_t894467439 *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		((ListPool_1_t1536390840_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)))->set_s_ListPool_0(L_2);
		return;
	}
}
// System.Void UnityEngine.UI.ListPool`1<UnityEngine.UIVertex>::<s_ListPool>m__0(System.Collections.Generic.List`1<T>)
extern "C"  void ListPool_1_U3Cs_ListPoolU3Em__0_m3174249320_gshared (RuntimeObject * __this /* static, unused */, List_1_t1641616647 * ___l0, const RuntimeMethod* method)
{
	{
		List_1_t1641616647 * L_0 = ___l0;
		NullCheck((List_1_t1641616647 *)L_0);
		((  void (*) (List_1_t1641616647 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8)->methodPointer)((List_1_t1641616647 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
		return;
	}
}
// System.Collections.Generic.List`1<T> UnityEngine.UI.ListPool`1<UnityEngine.Vector2>::Get()
extern "C"  List_1_t2432482469 * ListPool_1_Get_m3590927271_gshared (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	List_1_t2432482469 * V_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		ObjectPool_1_t3275200098 * L_0 = ((ListPool_1_t2327256662_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)))->get_s_ListPool_0();
		NullCheck((ObjectPool_1_t3275200098 *)L_0);
		List_1_t2432482469 * L_1 = ((  List_1_t2432482469 * (*) (ObjectPool_1_t3275200098 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->methodPointer)((ObjectPool_1_t3275200098 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		V_0 = (List_1_t2432482469 *)L_1;
		goto IL_0011;
	}

IL_0011:
	{
		List_1_t2432482469 * L_2 = V_0;
		return L_2;
	}
}
// System.Void UnityEngine.UI.ListPool`1<UnityEngine.Vector2>::Release(System.Collections.Generic.List`1<T>)
extern "C"  void ListPool_1_Release_m1140671520_gshared (RuntimeObject * __this /* static, unused */, List_1_t2432482469 * ___toRelease0, const RuntimeMethod* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		ObjectPool_1_t3275200098 * L_0 = ((ListPool_1_t2327256662_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)))->get_s_ListPool_0();
		List_1_t2432482469 * L_1 = ___toRelease0;
		NullCheck((ObjectPool_1_t3275200098 *)L_0);
		((  void (*) (ObjectPool_1_t3275200098 *, List_1_t2432482469 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((ObjectPool_1_t3275200098 *)L_0, (List_1_t2432482469 *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return;
	}
}
// System.Void UnityEngine.UI.ListPool`1<UnityEngine.Vector2>::.cctor()
extern "C"  void ListPool_1__cctor_m3129416040_gshared (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	{
		IntPtr_t L_0;
		L_0.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		UnityAction_1_t1685333261 * L_1 = (UnityAction_1_t1685333261 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (UnityAction_1_t1685333261 *, RuntimeObject *, IntPtr_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->methodPointer)(L_1, (RuntimeObject *)NULL, (IntPtr_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		ObjectPool_1_t3275200098 * L_2 = (ObjectPool_1_t3275200098 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		((  void (*) (ObjectPool_1_t3275200098 *, UnityAction_1_t1685333261 *, UnityAction_1_t1685333261 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7)->methodPointer)(L_2, (UnityAction_1_t1685333261 *)NULL, (UnityAction_1_t1685333261 *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		((ListPool_1_t2327256662_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)))->set_s_ListPool_0(L_2);
		return;
	}
}
// System.Void UnityEngine.UI.ListPool`1<UnityEngine.Vector2>::<s_ListPool>m__0(System.Collections.Generic.List`1<T>)
extern "C"  void ListPool_1_U3Cs_ListPoolU3Em__0_m336767989_gshared (RuntimeObject * __this /* static, unused */, List_1_t2432482469 * ___l0, const RuntimeMethod* method)
{
	{
		List_1_t2432482469 * L_0 = ___l0;
		NullCheck((List_1_t2432482469 *)L_0);
		((  void (*) (List_1_t2432482469 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8)->methodPointer)((List_1_t2432482469 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
		return;
	}
}
// System.Collections.Generic.List`1<T> UnityEngine.UI.ListPool`1<UnityEngine.Vector3>::Get()
extern "C"  List_1_t1758679641 * ListPool_1_Get_m3472976392_gshared (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	List_1_t1758679641 * V_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		ObjectPool_1_t2601397270 * L_0 = ((ListPool_1_t1653453834_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)))->get_s_ListPool_0();
		NullCheck((ObjectPool_1_t2601397270 *)L_0);
		List_1_t1758679641 * L_1 = ((  List_1_t1758679641 * (*) (ObjectPool_1_t2601397270 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->methodPointer)((ObjectPool_1_t2601397270 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		V_0 = (List_1_t1758679641 *)L_1;
		goto IL_0011;
	}

IL_0011:
	{
		List_1_t1758679641 * L_2 = V_0;
		return L_2;
	}
}
// System.Void UnityEngine.UI.ListPool`1<UnityEngine.Vector3>::Release(System.Collections.Generic.List`1<T>)
extern "C"  void ListPool_1_Release_m1974518202_gshared (RuntimeObject * __this /* static, unused */, List_1_t1758679641 * ___toRelease0, const RuntimeMethod* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		ObjectPool_1_t2601397270 * L_0 = ((ListPool_1_t1653453834_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)))->get_s_ListPool_0();
		List_1_t1758679641 * L_1 = ___toRelease0;
		NullCheck((ObjectPool_1_t2601397270 *)L_0);
		((  void (*) (ObjectPool_1_t2601397270 *, List_1_t1758679641 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((ObjectPool_1_t2601397270 *)L_0, (List_1_t1758679641 *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return;
	}
}
// System.Void UnityEngine.UI.ListPool`1<UnityEngine.Vector3>::.cctor()
extern "C"  void ListPool_1__cctor_m1368781998_gshared (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	{
		IntPtr_t L_0;
		L_0.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		UnityAction_1_t1011530433 * L_1 = (UnityAction_1_t1011530433 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (UnityAction_1_t1011530433 *, RuntimeObject *, IntPtr_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->methodPointer)(L_1, (RuntimeObject *)NULL, (IntPtr_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		ObjectPool_1_t2601397270 * L_2 = (ObjectPool_1_t2601397270 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		((  void (*) (ObjectPool_1_t2601397270 *, UnityAction_1_t1011530433 *, UnityAction_1_t1011530433 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7)->methodPointer)(L_2, (UnityAction_1_t1011530433 *)NULL, (UnityAction_1_t1011530433 *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		((ListPool_1_t1653453834_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)))->set_s_ListPool_0(L_2);
		return;
	}
}
// System.Void UnityEngine.UI.ListPool`1<UnityEngine.Vector3>::<s_ListPool>m__0(System.Collections.Generic.List`1<T>)
extern "C"  void ListPool_1_U3Cs_ListPoolU3Em__0_m4118574160_gshared (RuntimeObject * __this /* static, unused */, List_1_t1758679641 * ___l0, const RuntimeMethod* method)
{
	{
		List_1_t1758679641 * L_0 = ___l0;
		NullCheck((List_1_t1758679641 *)L_0);
		((  void (*) (List_1_t1758679641 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8)->methodPointer)((List_1_t1758679641 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
		return;
	}
}
// System.Collections.Generic.List`1<T> UnityEngine.UI.ListPool`1<UnityEngine.Vector4>::Get()
extern "C"  List_1_t1669821552 * ListPool_1_Get_m2412364934_gshared (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	List_1_t1669821552 * V_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		ObjectPool_1_t2512539181 * L_0 = ((ListPool_1_t1564595745_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)))->get_s_ListPool_0();
		NullCheck((ObjectPool_1_t2512539181 *)L_0);
		List_1_t1669821552 * L_1 = ((  List_1_t1669821552 * (*) (ObjectPool_1_t2512539181 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->methodPointer)((ObjectPool_1_t2512539181 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		V_0 = (List_1_t1669821552 *)L_1;
		goto IL_0011;
	}

IL_0011:
	{
		List_1_t1669821552 * L_2 = V_0;
		return L_2;
	}
}
// System.Void UnityEngine.UI.ListPool`1<UnityEngine.Vector4>::Release(System.Collections.Generic.List`1<T>)
extern "C"  void ListPool_1_Release_m3269335554_gshared (RuntimeObject * __this /* static, unused */, List_1_t1669821552 * ___toRelease0, const RuntimeMethod* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		ObjectPool_1_t2512539181 * L_0 = ((ListPool_1_t1564595745_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)))->get_s_ListPool_0();
		List_1_t1669821552 * L_1 = ___toRelease0;
		NullCheck((ObjectPool_1_t2512539181 *)L_0);
		((  void (*) (ObjectPool_1_t2512539181 *, List_1_t1669821552 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((ObjectPool_1_t2512539181 *)L_0, (List_1_t1669821552 *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return;
	}
}
// System.Void UnityEngine.UI.ListPool`1<UnityEngine.Vector4>::.cctor()
extern "C"  void ListPool_1__cctor_m3955057976_gshared (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	{
		IntPtr_t L_0;
		L_0.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		UnityAction_1_t922672344 * L_1 = (UnityAction_1_t922672344 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (UnityAction_1_t922672344 *, RuntimeObject *, IntPtr_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->methodPointer)(L_1, (RuntimeObject *)NULL, (IntPtr_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		ObjectPool_1_t2512539181 * L_2 = (ObjectPool_1_t2512539181 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		((  void (*) (ObjectPool_1_t2512539181 *, UnityAction_1_t922672344 *, UnityAction_1_t922672344 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7)->methodPointer)(L_2, (UnityAction_1_t922672344 *)NULL, (UnityAction_1_t922672344 *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		((ListPool_1_t1564595745_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)))->set_s_ListPool_0(L_2);
		return;
	}
}
// System.Void UnityEngine.UI.ListPool`1<UnityEngine.Vector4>::<s_ListPool>m__0(System.Collections.Generic.List`1<T>)
extern "C"  void ListPool_1_U3Cs_ListPoolU3Em__0_m3908173328_gshared (RuntimeObject * __this /* static, unused */, List_1_t1669821552 * ___l0, const RuntimeMethod* method)
{
	{
		List_1_t1669821552 * L_0 = ___l0;
		NullCheck((List_1_t1669821552 *)L_0);
		((  void (*) (List_1_t1669821552 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8)->methodPointer)((List_1_t1669821552 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
		return;
	}
}
// System.Void UnityEngine.UI.ObjectPool`1<System.Object>::.ctor(UnityEngine.Events.UnityAction`1<T>,UnityEngine.Events.UnityAction`1<T>)
extern "C"  void ObjectPool_1__ctor_m3368623349_gshared (ObjectPool_1_t2864675919 * __this, UnityAction_1_t1274809082 * ___actionOnGet0, UnityAction_1_t1274809082 * ___actionOnRelease1, const RuntimeMethod* method)
{
	{
		Stack_1_t608734916 * L_0 = (Stack_1_t608734916 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0));
		((  void (*) (Stack_1_t608734916 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		__this->set_m_Stack_0(L_0);
		NullCheck((RuntimeObject *)__this);
		Object__ctor_m2906060009((RuntimeObject *)__this, /*hidden argument*/NULL);
		UnityAction_1_t1274809082 * L_1 = ___actionOnGet0;
		__this->set_m_ActionOnGet_1(L_1);
		UnityAction_1_t1274809082 * L_2 = ___actionOnRelease1;
		__this->set_m_ActionOnRelease_2(L_2);
		return;
	}
}
// System.Int32 UnityEngine.UI.ObjectPool`1<System.Object>::get_countAll()
extern "C"  int32_t ObjectPool_1_get_countAll_m3309026701_gshared (ObjectPool_1_t2864675919 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_U3CcountAllU3Ek__BackingField_3();
		V_0 = (int32_t)L_0;
		goto IL_000c;
	}

IL_000c:
	{
		int32_t L_1 = V_0;
		return L_1;
	}
}
// System.Void UnityEngine.UI.ObjectPool`1<System.Object>::set_countAll(System.Int32)
extern "C"  void ObjectPool_1_set_countAll_m1206179758_gshared (ObjectPool_1_t2864675919 * __this, int32_t ___value0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_U3CcountAllU3Ek__BackingField_3(L_0);
		return;
	}
}
// System.Int32 UnityEngine.UI.ObjectPool`1<System.Object>::get_countActive()
extern "C"  int32_t ObjectPool_1_get_countActive_m3247228502_gshared (ObjectPool_1_t2864675919 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		NullCheck((ObjectPool_1_t2864675919 *)__this);
		int32_t L_0 = ((  int32_t (*) (ObjectPool_1_t2864675919 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)((ObjectPool_1_t2864675919 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		NullCheck((ObjectPool_1_t2864675919 *)__this);
		int32_t L_1 = ((  int32_t (*) (ObjectPool_1_t2864675919 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)((ObjectPool_1_t2864675919 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		V_0 = (int32_t)((int32_t)((int32_t)L_0-(int32_t)L_1));
		goto IL_0014;
	}

IL_0014:
	{
		int32_t L_2 = V_0;
		return L_2;
	}
}
// System.Int32 UnityEngine.UI.ObjectPool`1<System.Object>::get_countInactive()
extern "C"  int32_t ObjectPool_1_get_countInactive_m314868870_gshared (ObjectPool_1_t2864675919 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		Stack_1_t608734916 * L_0 = (Stack_1_t608734916 *)__this->get_m_Stack_0();
		NullCheck((Stack_1_t608734916 *)L_0);
		int32_t L_1 = ((  int32_t (*) (Stack_1_t608734916 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)((Stack_1_t608734916 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		V_0 = (int32_t)L_1;
		goto IL_0012;
	}

IL_0012:
	{
		int32_t L_2 = V_0;
		return L_2;
	}
}
// T UnityEngine.UI.ObjectPool`1<System.Object>::Get()
extern "C"  RuntimeObject * ObjectPool_1_Get_m2769607430_gshared (ObjectPool_1_t2864675919 * __this, const RuntimeMethod* method)
{
	RuntimeObject * V_0 = NULL;
	RuntimeObject * V_1 = NULL;
	{
		Stack_1_t608734916 * L_0 = (Stack_1_t608734916 *)__this->get_m_Stack_0();
		NullCheck((Stack_1_t608734916 *)L_0);
		int32_t L_1 = ((  int32_t (*) (Stack_1_t608734916 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)((Stack_1_t608734916 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		if (L_1)
		{
			goto IL_002c;
		}
	}
	{
		RuntimeObject * L_2 = ((  RuntimeObject * (*) (RuntimeObject * /* static, unused */, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		V_0 = (RuntimeObject *)L_2;
		NullCheck((ObjectPool_1_t2864675919 *)__this);
		int32_t L_3 = ((  int32_t (*) (ObjectPool_1_t2864675919 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)((ObjectPool_1_t2864675919 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		NullCheck((ObjectPool_1_t2864675919 *)__this);
		((  void (*) (ObjectPool_1_t2864675919 *, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((ObjectPool_1_t2864675919 *)__this, (int32_t)((int32_t)((int32_t)L_3+(int32_t)1)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		goto IL_003a;
	}

IL_002c:
	{
		Stack_1_t608734916 * L_4 = (Stack_1_t608734916 *)__this->get_m_Stack_0();
		NullCheck((Stack_1_t608734916 *)L_4);
		RuntimeObject * L_5 = ((  RuntimeObject * (*) (Stack_1_t608734916 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7)->methodPointer)((Stack_1_t608734916 *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7));
		V_0 = (RuntimeObject *)L_5;
	}

IL_003a:
	{
		UnityAction_1_t1274809082 * L_6 = (UnityAction_1_t1274809082 *)__this->get_m_ActionOnGet_1();
		if (!L_6)
		{
			goto IL_0051;
		}
	}
	{
		UnityAction_1_t1274809082 * L_7 = (UnityAction_1_t1274809082 *)__this->get_m_ActionOnGet_1();
		RuntimeObject * L_8 = V_0;
		NullCheck((UnityAction_1_t1274809082 *)L_7);
		((  void (*) (UnityAction_1_t1274809082 *, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)((UnityAction_1_t1274809082 *)L_7, (RuntimeObject *)L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
	}

IL_0051:
	{
		RuntimeObject * L_9 = V_0;
		V_1 = (RuntimeObject *)L_9;
		goto IL_0058;
	}

IL_0058:
	{
		RuntimeObject * L_10 = V_1;
		return L_10;
	}
}
// System.Void UnityEngine.UI.ObjectPool`1<System.Object>::Release(T)
extern "C"  void ObjectPool_1_Release_m3069516663_gshared (ObjectPool_1_t2864675919 * __this, RuntimeObject * ___element0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ObjectPool_1_Release_m3069516663_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Stack_1_t608734916 * L_0 = (Stack_1_t608734916 *)__this->get_m_Stack_0();
		NullCheck((Stack_1_t608734916 *)L_0);
		int32_t L_1 = ((  int32_t (*) (Stack_1_t608734916 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)((Stack_1_t608734916 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		if ((((int32_t)L_1) <= ((int32_t)0)))
		{
			goto IL_003c;
		}
	}
	{
		Stack_1_t608734916 * L_2 = (Stack_1_t608734916 *)__this->get_m_Stack_0();
		NullCheck((Stack_1_t608734916 *)L_2);
		RuntimeObject * L_3 = ((  RuntimeObject * (*) (Stack_1_t608734916 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9)->methodPointer)((Stack_1_t608734916 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
		RuntimeObject * L_4 = ___element0;
		bool L_5 = Object_ReferenceEquals_m2014193643(NULL /*static, unused*/, (RuntimeObject *)L_3, (RuntimeObject *)L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_003c;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3525713211_il2cpp_TypeInfo_var);
		Debug_LogError_m1848790000(NULL /*static, unused*/, (RuntimeObject *)_stringLiteral972099005, /*hidden argument*/NULL);
	}

IL_003c:
	{
		UnityAction_1_t1274809082 * L_6 = (UnityAction_1_t1274809082 *)__this->get_m_ActionOnRelease_2();
		if (!L_6)
		{
			goto IL_0053;
		}
	}
	{
		UnityAction_1_t1274809082 * L_7 = (UnityAction_1_t1274809082 *)__this->get_m_ActionOnRelease_2();
		RuntimeObject * L_8 = ___element0;
		NullCheck((UnityAction_1_t1274809082 *)L_7);
		((  void (*) (UnityAction_1_t1274809082 *, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)((UnityAction_1_t1274809082 *)L_7, (RuntimeObject *)L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
	}

IL_0053:
	{
		Stack_1_t608734916 * L_9 = (Stack_1_t608734916 *)__this->get_m_Stack_0();
		RuntimeObject * L_10 = ___element0;
		NullCheck((Stack_1_t608734916 *)L_9);
		((  void (*) (Stack_1_t608734916 *, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11)->methodPointer)((Stack_1_t608734916 *)L_9, (RuntimeObject *)L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11));
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif

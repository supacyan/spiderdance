﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "object-internals.h"

// UnityEngine.Camera
struct Camera_t3175186167;
// UnityEngine.AnimationCurve
struct AnimationCurve_t2184836714;
// UnityStandardAssets.Utility.FOVKick
struct FOVKick_t1285422362;
// UnityStandardAssets.Utility.LerpControlledBob
struct LerpControlledBob_t4065608754;
// UnityEngine.Transform[]
struct TransformU5BU5D_t788543427;
// UnityStandardAssets.Utility.ObjectResetter
struct ObjectResetter_t1579296102;
// UnityStandardAssets.Utility.TimedObjectActivator/Entry[]
struct EntryU5BU5D_t267038830;
// UnityStandardAssets.Utility.AutoMobileShaderSwitch/ReplacementDefinition[]
struct ReplacementDefinitionU5BU5D_t2292654658;
// UnityStandardAssets.Utility.TimedObjectActivator/Entry
struct Entry_t3279652951;
// UnityStandardAssets.Utility.WaypointCircuit
struct WaypointCircuit_t802585562;
// UnityEngine.ParticleSystem[]
struct ParticleSystemU5BU5D_t3849375490;
// UnityStandardAssets.Utility.ParticleSystemDestroyer
struct ParticleSystemDestroyer_t3293524282;
// UnityEngine.Shader
struct Shader_t3778723916;
// UnityStandardAssets.CrossPlatformInput.VirtualInput
struct VirtualInput_t2381112337;
// System.String
struct String_t;
// System.Void
struct Void_t2217553113;
// System.Char[]
struct CharU5BU5D_t45629911;
// System.Collections.Generic.Dictionary`2<System.String,UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualAxis>
struct Dictionary_2_t3553060542;
// System.Collections.Generic.Dictionary`2<System.String,UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualButton>
struct Dictionary_2_t2715111429;
// System.Collections.Generic.List`1<System.String>
struct List_1_t1230370061;
// UnityEngine.Transform
struct Transform_t3316442598;
// UnityEngine.GameObject
struct GameObject_t1811656094;
// UnityStandardAssets.Utility.DragRigidbody
struct DragRigidbody_t781943330;
// Vuforia.ReconstructionFromTarget
struct ReconstructionFromTarget_t1541138710;
// Vuforia.ReconstructionAbstractBehaviour
struct ReconstructionAbstractBehaviour_t954468633;
// UnityStandardAssets.Characters.FirstPerson.MouseLook
struct MouseLook_t3694953670;
// UnityStandardAssets.Utility.CurveControlledBob
struct CurveControlledBob_t3985701645;
// UnityEngine.AudioClip[]
struct AudioClipU5BU5D_t3571400391;
// UnityEngine.AudioClip
struct AudioClip_t474304562;
// UnityEngine.CharacterController
struct CharacterController_t2498358276;
// UnityEngine.AudioSource
struct AudioSource_t1862618521;
// UnityEngine.Mesh
struct Mesh_t996500909;
// Vuforia.VuforiaARController
struct VuforiaARController_t1328503142;
// System.Action
struct Action_t3619184611;
// Vuforia.ObjectTracker
struct ObjectTracker_t4023638979;
// System.Collections.Generic.List`1<Vuforia.ICloudRecoEventHandler>
struct List_1_t1465274546;
// Vuforia.IExcessAreaClipping
struct IExcessAreaClipping_t2974524491;
// Vuforia.VideoBackgroundManager
struct VideoBackgroundManager_t2068552549;
// UnityEngine.Material
struct Material_t1079520667;
// System.Collections.Generic.List`1<Vuforia.ISmartTerrainEventHandler>
struct List_1_t3446838688;
// System.Action`1<Vuforia.SmartTerrainInitializationInfo>
struct Action_1_t1488611326;
// System.Action`1<Vuforia.Prop>
struct Action_1_t1560204979;
// System.Action`1<Vuforia.Surface>
struct Action_1_t1020068010;
// Vuforia.Reconstruction
struct Reconstruction_t3574071110;
// System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.Surface>
struct Dictionary_2_t2230247832;
// System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.SurfaceAbstractBehaviour>
struct Dictionary_2_t1468548340;
// System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.Prop>
struct Dictionary_2_t2770384801;
// System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.PropAbstractBehaviour>
struct Dictionary_2_t3844889330;
// Vuforia.SurfaceAbstractBehaviour
struct SurfaceAbstractBehaviour_t272541176;
// System.Collections.Generic.List`1<Vuforia.ITextRecoEventHandler>
struct List_1_t4098171074;
// Vuforia.Trackable
struct Trackable_t1140696582;
// System.Collections.Generic.List`1<Vuforia.ITrackableEventHandler>
struct List_1_t3589236026;
// UnityStandardAssets.Characters.FirstPerson.RigidbodyFirstPersonController
struct RigidbodyFirstPersonController_t1713505141;
// Vuforia.TrackableBehaviour
struct TrackableBehaviour_t2419079356;
// UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualAxis
struct VirtualAxis_t2383171311;
// UnityEngine.AI.NavMeshAgent
struct NavMeshAgent_t1476393864;
// UnityStandardAssets.Characters.ThirdPerson.ThirdPersonCharacter
struct ThirdPersonCharacter_t1426137179;
// Vuforia.ReconstructionBehaviour
struct ReconstructionBehaviour_t3936121853;
// Vuforia.PropBehaviour
struct PropBehaviour_t3897855688;
// Vuforia.SurfaceBehaviour
struct SurfaceBehaviour_t1975960194;
// UnityEngine.Rigidbody
struct Rigidbody_t2492269564;
// UnityEngine.Animator
struct Animator_t4075492284;
// UnityEngine.CapsuleCollider
struct CapsuleCollider_t77672896;
// System.Collections.Generic.List`1<UnityEngine.Transform>
struct List_1_t2171591805;
// UnityEngine.UI.Text
struct Text_t3057673244;
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t2021098027;
// UnityEngine.MonoBehaviour[]
struct MonoBehaviourU5BU5D_t1369622294;
// UnityEngine.Light
struct Light_t3435818087;
// UnityEngine.SpringJoint
struct SpringJoint_t4170598825;
// UnityStandardAssets.Utility.AutoMoveAndRotate/Vector3andSpace
struct Vector3andSpace_t2340595124;
// UnityStandardAssets.Utility.AutoMobileShaderSwitch/ReplacementList
struct ReplacementList_t22610105;
// UnityEngine.UI.Image
struct Image_t266201966;
// UnityEngine.Object
struct Object_t1502412432;
// UnityStandardAssets.CrossPlatformInput.TiltInput/AxisMapping
struct AxisMapping_t2403352015;
// UnityEngine.GUIText
struct GUIText_t1809039568;
// UnityStandardAssets.Utility.TimedObjectActivator/Entries
struct Entries_t1342860269;
// UnityStandardAssets.Vehicles.Ball.Ball
struct Ball_t3802515361;
// UnityStandardAssets.Utility.WaypointCircuit/WaypointList
struct WaypointList_t2428355280;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t981680887;
// System.Single[]
struct SingleU5BU5D_t1390472614;
// UnityEngine.Animation
struct Animation_t2605054404;
// UnityStandardAssets.Characters.FirstPerson.RigidbodyFirstPersonController/MovementSettings
struct MovementSettings_t2494390400;
// UnityStandardAssets.Characters.FirstPerson.RigidbodyFirstPersonController/AdvancedSettings
struct AdvancedSettings_t2632082398;
// Vuforia.SmartTerrainTrackable
struct SmartTerrainTrackable_t3286114719;
// UnityEngine.MeshFilter
struct MeshFilter_t137687116;
// UnityEngine.MeshCollider
struct MeshCollider_t4172739389;
// Vuforia.ReconstructionFromTargetAbstractBehaviour
struct ReconstructionFromTargetAbstractBehaviour_t2676804737;
// Vuforia.Prop
struct Prop_t1574377637;
// UnityEngine.BoxCollider
struct BoxCollider_t1861275623;
// Vuforia.ObjectTarget
struct ObjectTarget_t1833683407;
// UnityEngine.Texture2D
struct Texture2D_t415585320;
// Vuforia.MultiTarget
struct MultiTarget_t114888232;
// Vuforia.ImageTarget
struct ImageTarget_t193025956;
// System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VirtualButtonAbstractBehaviour>
struct Dictionary_2_t3161827512;
// Vuforia.CylinderTarget
struct CylinderTarget_t1924254572;
// Vuforia.Surface
struct Surface_t1034240668;




#ifndef U3CMODULEU3E_T160650891_H
#define U3CMODULEU3E_T160650891_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t160650891 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T160650891_H
#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef U3CMODULEU3E_T160650892_H
#define U3CMODULEU3E_T160650892_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t160650892 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T160650892_H
#ifndef FOVKICK_T1285422362_H
#define FOVKICK_T1285422362_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.FOVKick
struct  FOVKick_t1285422362  : public RuntimeObject
{
public:
	// UnityEngine.Camera UnityStandardAssets.Utility.FOVKick::Camera
	Camera_t3175186167 * ___Camera_0;
	// System.Single UnityStandardAssets.Utility.FOVKick::originalFov
	float ___originalFov_1;
	// System.Single UnityStandardAssets.Utility.FOVKick::FOVIncrease
	float ___FOVIncrease_2;
	// System.Single UnityStandardAssets.Utility.FOVKick::TimeToIncrease
	float ___TimeToIncrease_3;
	// System.Single UnityStandardAssets.Utility.FOVKick::TimeToDecrease
	float ___TimeToDecrease_4;
	// UnityEngine.AnimationCurve UnityStandardAssets.Utility.FOVKick::IncreaseCurve
	AnimationCurve_t2184836714 * ___IncreaseCurve_5;

public:
	inline static int32_t get_offset_of_Camera_0() { return static_cast<int32_t>(offsetof(FOVKick_t1285422362, ___Camera_0)); }
	inline Camera_t3175186167 * get_Camera_0() const { return ___Camera_0; }
	inline Camera_t3175186167 ** get_address_of_Camera_0() { return &___Camera_0; }
	inline void set_Camera_0(Camera_t3175186167 * value)
	{
		___Camera_0 = value;
		Il2CppCodeGenWriteBarrier((&___Camera_0), value);
	}

	inline static int32_t get_offset_of_originalFov_1() { return static_cast<int32_t>(offsetof(FOVKick_t1285422362, ___originalFov_1)); }
	inline float get_originalFov_1() const { return ___originalFov_1; }
	inline float* get_address_of_originalFov_1() { return &___originalFov_1; }
	inline void set_originalFov_1(float value)
	{
		___originalFov_1 = value;
	}

	inline static int32_t get_offset_of_FOVIncrease_2() { return static_cast<int32_t>(offsetof(FOVKick_t1285422362, ___FOVIncrease_2)); }
	inline float get_FOVIncrease_2() const { return ___FOVIncrease_2; }
	inline float* get_address_of_FOVIncrease_2() { return &___FOVIncrease_2; }
	inline void set_FOVIncrease_2(float value)
	{
		___FOVIncrease_2 = value;
	}

	inline static int32_t get_offset_of_TimeToIncrease_3() { return static_cast<int32_t>(offsetof(FOVKick_t1285422362, ___TimeToIncrease_3)); }
	inline float get_TimeToIncrease_3() const { return ___TimeToIncrease_3; }
	inline float* get_address_of_TimeToIncrease_3() { return &___TimeToIncrease_3; }
	inline void set_TimeToIncrease_3(float value)
	{
		___TimeToIncrease_3 = value;
	}

	inline static int32_t get_offset_of_TimeToDecrease_4() { return static_cast<int32_t>(offsetof(FOVKick_t1285422362, ___TimeToDecrease_4)); }
	inline float get_TimeToDecrease_4() const { return ___TimeToDecrease_4; }
	inline float* get_address_of_TimeToDecrease_4() { return &___TimeToDecrease_4; }
	inline void set_TimeToDecrease_4(float value)
	{
		___TimeToDecrease_4 = value;
	}

	inline static int32_t get_offset_of_IncreaseCurve_5() { return static_cast<int32_t>(offsetof(FOVKick_t1285422362, ___IncreaseCurve_5)); }
	inline AnimationCurve_t2184836714 * get_IncreaseCurve_5() const { return ___IncreaseCurve_5; }
	inline AnimationCurve_t2184836714 ** get_address_of_IncreaseCurve_5() { return &___IncreaseCurve_5; }
	inline void set_IncreaseCurve_5(AnimationCurve_t2184836714 * value)
	{
		___IncreaseCurve_5 = value;
		Il2CppCodeGenWriteBarrier((&___IncreaseCurve_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FOVKICK_T1285422362_H
#ifndef U3CFOVKICKUPU3EC__ITERATOR0_T3168521387_H
#define U3CFOVKICKUPU3EC__ITERATOR0_T3168521387_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.FOVKick/<FOVKickUp>c__Iterator0
struct  U3CFOVKickUpU3Ec__Iterator0_t3168521387  : public RuntimeObject
{
public:
	// System.Single UnityStandardAssets.Utility.FOVKick/<FOVKickUp>c__Iterator0::<t>__0
	float ___U3CtU3E__0_0;
	// UnityStandardAssets.Utility.FOVKick UnityStandardAssets.Utility.FOVKick/<FOVKickUp>c__Iterator0::$this
	FOVKick_t1285422362 * ___U24this_1;
	// System.Object UnityStandardAssets.Utility.FOVKick/<FOVKickUp>c__Iterator0::$current
	RuntimeObject * ___U24current_2;
	// System.Boolean UnityStandardAssets.Utility.FOVKick/<FOVKickUp>c__Iterator0::$disposing
	bool ___U24disposing_3;
	// System.Int32 UnityStandardAssets.Utility.FOVKick/<FOVKickUp>c__Iterator0::$PC
	int32_t ___U24PC_4;

public:
	inline static int32_t get_offset_of_U3CtU3E__0_0() { return static_cast<int32_t>(offsetof(U3CFOVKickUpU3Ec__Iterator0_t3168521387, ___U3CtU3E__0_0)); }
	inline float get_U3CtU3E__0_0() const { return ___U3CtU3E__0_0; }
	inline float* get_address_of_U3CtU3E__0_0() { return &___U3CtU3E__0_0; }
	inline void set_U3CtU3E__0_0(float value)
	{
		___U3CtU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CFOVKickUpU3Ec__Iterator0_t3168521387, ___U24this_1)); }
	inline FOVKick_t1285422362 * get_U24this_1() const { return ___U24this_1; }
	inline FOVKick_t1285422362 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(FOVKick_t1285422362 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CFOVKickUpU3Ec__Iterator0_t3168521387, ___U24current_2)); }
	inline RuntimeObject * get_U24current_2() const { return ___U24current_2; }
	inline RuntimeObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(RuntimeObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_2), value);
	}

	inline static int32_t get_offset_of_U24disposing_3() { return static_cast<int32_t>(offsetof(U3CFOVKickUpU3Ec__Iterator0_t3168521387, ___U24disposing_3)); }
	inline bool get_U24disposing_3() const { return ___U24disposing_3; }
	inline bool* get_address_of_U24disposing_3() { return &___U24disposing_3; }
	inline void set_U24disposing_3(bool value)
	{
		___U24disposing_3 = value;
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CFOVKickUpU3Ec__Iterator0_t3168521387, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CFOVKICKUPU3EC__ITERATOR0_T3168521387_H
#ifndef U3CFOVKICKDOWNU3EC__ITERATOR1_T1100257635_H
#define U3CFOVKICKDOWNU3EC__ITERATOR1_T1100257635_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.FOVKick/<FOVKickDown>c__Iterator1
struct  U3CFOVKickDownU3Ec__Iterator1_t1100257635  : public RuntimeObject
{
public:
	// System.Single UnityStandardAssets.Utility.FOVKick/<FOVKickDown>c__Iterator1::<t>__0
	float ___U3CtU3E__0_0;
	// UnityStandardAssets.Utility.FOVKick UnityStandardAssets.Utility.FOVKick/<FOVKickDown>c__Iterator1::$this
	FOVKick_t1285422362 * ___U24this_1;
	// System.Object UnityStandardAssets.Utility.FOVKick/<FOVKickDown>c__Iterator1::$current
	RuntimeObject * ___U24current_2;
	// System.Boolean UnityStandardAssets.Utility.FOVKick/<FOVKickDown>c__Iterator1::$disposing
	bool ___U24disposing_3;
	// System.Int32 UnityStandardAssets.Utility.FOVKick/<FOVKickDown>c__Iterator1::$PC
	int32_t ___U24PC_4;

public:
	inline static int32_t get_offset_of_U3CtU3E__0_0() { return static_cast<int32_t>(offsetof(U3CFOVKickDownU3Ec__Iterator1_t1100257635, ___U3CtU3E__0_0)); }
	inline float get_U3CtU3E__0_0() const { return ___U3CtU3E__0_0; }
	inline float* get_address_of_U3CtU3E__0_0() { return &___U3CtU3E__0_0; }
	inline void set_U3CtU3E__0_0(float value)
	{
		___U3CtU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CFOVKickDownU3Ec__Iterator1_t1100257635, ___U24this_1)); }
	inline FOVKick_t1285422362 * get_U24this_1() const { return ___U24this_1; }
	inline FOVKick_t1285422362 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(FOVKick_t1285422362 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CFOVKickDownU3Ec__Iterator1_t1100257635, ___U24current_2)); }
	inline RuntimeObject * get_U24current_2() const { return ___U24current_2; }
	inline RuntimeObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(RuntimeObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_2), value);
	}

	inline static int32_t get_offset_of_U24disposing_3() { return static_cast<int32_t>(offsetof(U3CFOVKickDownU3Ec__Iterator1_t1100257635, ___U24disposing_3)); }
	inline bool get_U24disposing_3() const { return ___U24disposing_3; }
	inline bool* get_address_of_U24disposing_3() { return &___U24disposing_3; }
	inline void set_U24disposing_3(bool value)
	{
		___U24disposing_3 = value;
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CFOVKickDownU3Ec__Iterator1_t1100257635, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CFOVKICKDOWNU3EC__ITERATOR1_T1100257635_H
#ifndef LERPCONTROLLEDBOB_T4065608754_H
#define LERPCONTROLLEDBOB_T4065608754_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.LerpControlledBob
struct  LerpControlledBob_t4065608754  : public RuntimeObject
{
public:
	// System.Single UnityStandardAssets.Utility.LerpControlledBob::BobDuration
	float ___BobDuration_0;
	// System.Single UnityStandardAssets.Utility.LerpControlledBob::BobAmount
	float ___BobAmount_1;
	// System.Single UnityStandardAssets.Utility.LerpControlledBob::m_Offset
	float ___m_Offset_2;

public:
	inline static int32_t get_offset_of_BobDuration_0() { return static_cast<int32_t>(offsetof(LerpControlledBob_t4065608754, ___BobDuration_0)); }
	inline float get_BobDuration_0() const { return ___BobDuration_0; }
	inline float* get_address_of_BobDuration_0() { return &___BobDuration_0; }
	inline void set_BobDuration_0(float value)
	{
		___BobDuration_0 = value;
	}

	inline static int32_t get_offset_of_BobAmount_1() { return static_cast<int32_t>(offsetof(LerpControlledBob_t4065608754, ___BobAmount_1)); }
	inline float get_BobAmount_1() const { return ___BobAmount_1; }
	inline float* get_address_of_BobAmount_1() { return &___BobAmount_1; }
	inline void set_BobAmount_1(float value)
	{
		___BobAmount_1 = value;
	}

	inline static int32_t get_offset_of_m_Offset_2() { return static_cast<int32_t>(offsetof(LerpControlledBob_t4065608754, ___m_Offset_2)); }
	inline float get_m_Offset_2() const { return ___m_Offset_2; }
	inline float* get_address_of_m_Offset_2() { return &___m_Offset_2; }
	inline void set_m_Offset_2(float value)
	{
		___m_Offset_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LERPCONTROLLEDBOB_T4065608754_H
#ifndef U3CDOBOBCYCLEU3EC__ITERATOR0_T1487389126_H
#define U3CDOBOBCYCLEU3EC__ITERATOR0_T1487389126_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.LerpControlledBob/<DoBobCycle>c__Iterator0
struct  U3CDoBobCycleU3Ec__Iterator0_t1487389126  : public RuntimeObject
{
public:
	// System.Single UnityStandardAssets.Utility.LerpControlledBob/<DoBobCycle>c__Iterator0::<t>__0
	float ___U3CtU3E__0_0;
	// UnityStandardAssets.Utility.LerpControlledBob UnityStandardAssets.Utility.LerpControlledBob/<DoBobCycle>c__Iterator0::$this
	LerpControlledBob_t4065608754 * ___U24this_1;
	// System.Object UnityStandardAssets.Utility.LerpControlledBob/<DoBobCycle>c__Iterator0::$current
	RuntimeObject * ___U24current_2;
	// System.Boolean UnityStandardAssets.Utility.LerpControlledBob/<DoBobCycle>c__Iterator0::$disposing
	bool ___U24disposing_3;
	// System.Int32 UnityStandardAssets.Utility.LerpControlledBob/<DoBobCycle>c__Iterator0::$PC
	int32_t ___U24PC_4;

public:
	inline static int32_t get_offset_of_U3CtU3E__0_0() { return static_cast<int32_t>(offsetof(U3CDoBobCycleU3Ec__Iterator0_t1487389126, ___U3CtU3E__0_0)); }
	inline float get_U3CtU3E__0_0() const { return ___U3CtU3E__0_0; }
	inline float* get_address_of_U3CtU3E__0_0() { return &___U3CtU3E__0_0; }
	inline void set_U3CtU3E__0_0(float value)
	{
		___U3CtU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CDoBobCycleU3Ec__Iterator0_t1487389126, ___U24this_1)); }
	inline LerpControlledBob_t4065608754 * get_U24this_1() const { return ___U24this_1; }
	inline LerpControlledBob_t4065608754 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(LerpControlledBob_t4065608754 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CDoBobCycleU3Ec__Iterator0_t1487389126, ___U24current_2)); }
	inline RuntimeObject * get_U24current_2() const { return ___U24current_2; }
	inline RuntimeObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(RuntimeObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_2), value);
	}

	inline static int32_t get_offset_of_U24disposing_3() { return static_cast<int32_t>(offsetof(U3CDoBobCycleU3Ec__Iterator0_t1487389126, ___U24disposing_3)); }
	inline bool get_U24disposing_3() const { return ___U24disposing_3; }
	inline bool* get_address_of_U24disposing_3() { return &___U24disposing_3; }
	inline void set_U24disposing_3(bool value)
	{
		___U24disposing_3 = value;
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CDoBobCycleU3Ec__Iterator0_t1487389126, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CDOBOBCYCLEU3EC__ITERATOR0_T1487389126_H
#ifndef U3CRESETCOROUTINEU3EC__ITERATOR0_T4213400666_H
#define U3CRESETCOROUTINEU3EC__ITERATOR0_T4213400666_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.ObjectResetter/<ResetCoroutine>c__Iterator0
struct  U3CResetCoroutineU3Ec__Iterator0_t4213400666  : public RuntimeObject
{
public:
	// System.Single UnityStandardAssets.Utility.ObjectResetter/<ResetCoroutine>c__Iterator0::delay
	float ___delay_0;
	// UnityEngine.Transform[] UnityStandardAssets.Utility.ObjectResetter/<ResetCoroutine>c__Iterator0::$locvar0
	TransformU5BU5D_t788543427* ___U24locvar0_1;
	// System.Int32 UnityStandardAssets.Utility.ObjectResetter/<ResetCoroutine>c__Iterator0::$locvar1
	int32_t ___U24locvar1_2;
	// UnityStandardAssets.Utility.ObjectResetter UnityStandardAssets.Utility.ObjectResetter/<ResetCoroutine>c__Iterator0::$this
	ObjectResetter_t1579296102 * ___U24this_3;
	// System.Object UnityStandardAssets.Utility.ObjectResetter/<ResetCoroutine>c__Iterator0::$current
	RuntimeObject * ___U24current_4;
	// System.Boolean UnityStandardAssets.Utility.ObjectResetter/<ResetCoroutine>c__Iterator0::$disposing
	bool ___U24disposing_5;
	// System.Int32 UnityStandardAssets.Utility.ObjectResetter/<ResetCoroutine>c__Iterator0::$PC
	int32_t ___U24PC_6;

public:
	inline static int32_t get_offset_of_delay_0() { return static_cast<int32_t>(offsetof(U3CResetCoroutineU3Ec__Iterator0_t4213400666, ___delay_0)); }
	inline float get_delay_0() const { return ___delay_0; }
	inline float* get_address_of_delay_0() { return &___delay_0; }
	inline void set_delay_0(float value)
	{
		___delay_0 = value;
	}

	inline static int32_t get_offset_of_U24locvar0_1() { return static_cast<int32_t>(offsetof(U3CResetCoroutineU3Ec__Iterator0_t4213400666, ___U24locvar0_1)); }
	inline TransformU5BU5D_t788543427* get_U24locvar0_1() const { return ___U24locvar0_1; }
	inline TransformU5BU5D_t788543427** get_address_of_U24locvar0_1() { return &___U24locvar0_1; }
	inline void set_U24locvar0_1(TransformU5BU5D_t788543427* value)
	{
		___U24locvar0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24locvar0_1), value);
	}

	inline static int32_t get_offset_of_U24locvar1_2() { return static_cast<int32_t>(offsetof(U3CResetCoroutineU3Ec__Iterator0_t4213400666, ___U24locvar1_2)); }
	inline int32_t get_U24locvar1_2() const { return ___U24locvar1_2; }
	inline int32_t* get_address_of_U24locvar1_2() { return &___U24locvar1_2; }
	inline void set_U24locvar1_2(int32_t value)
	{
		___U24locvar1_2 = value;
	}

	inline static int32_t get_offset_of_U24this_3() { return static_cast<int32_t>(offsetof(U3CResetCoroutineU3Ec__Iterator0_t4213400666, ___U24this_3)); }
	inline ObjectResetter_t1579296102 * get_U24this_3() const { return ___U24this_3; }
	inline ObjectResetter_t1579296102 ** get_address_of_U24this_3() { return &___U24this_3; }
	inline void set_U24this_3(ObjectResetter_t1579296102 * value)
	{
		___U24this_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_3), value);
	}

	inline static int32_t get_offset_of_U24current_4() { return static_cast<int32_t>(offsetof(U3CResetCoroutineU3Ec__Iterator0_t4213400666, ___U24current_4)); }
	inline RuntimeObject * get_U24current_4() const { return ___U24current_4; }
	inline RuntimeObject ** get_address_of_U24current_4() { return &___U24current_4; }
	inline void set_U24current_4(RuntimeObject * value)
	{
		___U24current_4 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_4), value);
	}

	inline static int32_t get_offset_of_U24disposing_5() { return static_cast<int32_t>(offsetof(U3CResetCoroutineU3Ec__Iterator0_t4213400666, ___U24disposing_5)); }
	inline bool get_U24disposing_5() const { return ___U24disposing_5; }
	inline bool* get_address_of_U24disposing_5() { return &___U24disposing_5; }
	inline void set_U24disposing_5(bool value)
	{
		___U24disposing_5 = value;
	}

	inline static int32_t get_offset_of_U24PC_6() { return static_cast<int32_t>(offsetof(U3CResetCoroutineU3Ec__Iterator0_t4213400666, ___U24PC_6)); }
	inline int32_t get_U24PC_6() const { return ___U24PC_6; }
	inline int32_t* get_address_of_U24PC_6() { return &___U24PC_6; }
	inline void set_U24PC_6(int32_t value)
	{
		___U24PC_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CRESETCOROUTINEU3EC__ITERATOR0_T4213400666_H
#ifndef ENTRIES_T1342860269_H
#define ENTRIES_T1342860269_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.TimedObjectActivator/Entries
struct  Entries_t1342860269  : public RuntimeObject
{
public:
	// UnityStandardAssets.Utility.TimedObjectActivator/Entry[] UnityStandardAssets.Utility.TimedObjectActivator/Entries::entries
	EntryU5BU5D_t267038830* ___entries_0;

public:
	inline static int32_t get_offset_of_entries_0() { return static_cast<int32_t>(offsetof(Entries_t1342860269, ___entries_0)); }
	inline EntryU5BU5D_t267038830* get_entries_0() const { return ___entries_0; }
	inline EntryU5BU5D_t267038830** get_address_of_entries_0() { return &___entries_0; }
	inline void set_entries_0(EntryU5BU5D_t267038830* value)
	{
		___entries_0 = value;
		Il2CppCodeGenWriteBarrier((&___entries_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENTRIES_T1342860269_H
#ifndef REPLACEMENTLIST_T22610105_H
#define REPLACEMENTLIST_T22610105_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.AutoMobileShaderSwitch/ReplacementList
struct  ReplacementList_t22610105  : public RuntimeObject
{
public:
	// UnityStandardAssets.Utility.AutoMobileShaderSwitch/ReplacementDefinition[] UnityStandardAssets.Utility.AutoMobileShaderSwitch/ReplacementList::items
	ReplacementDefinitionU5BU5D_t2292654658* ___items_0;

public:
	inline static int32_t get_offset_of_items_0() { return static_cast<int32_t>(offsetof(ReplacementList_t22610105, ___items_0)); }
	inline ReplacementDefinitionU5BU5D_t2292654658* get_items_0() const { return ___items_0; }
	inline ReplacementDefinitionU5BU5D_t2292654658** get_address_of_items_0() { return &___items_0; }
	inline void set_items_0(ReplacementDefinitionU5BU5D_t2292654658* value)
	{
		___items_0 = value;
		Il2CppCodeGenWriteBarrier((&___items_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REPLACEMENTLIST_T22610105_H
#ifndef U3CACTIVATEU3EC__ITERATOR0_T368634801_H
#define U3CACTIVATEU3EC__ITERATOR0_T368634801_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.TimedObjectActivator/<Activate>c__Iterator0
struct  U3CActivateU3Ec__Iterator0_t368634801  : public RuntimeObject
{
public:
	// UnityStandardAssets.Utility.TimedObjectActivator/Entry UnityStandardAssets.Utility.TimedObjectActivator/<Activate>c__Iterator0::entry
	Entry_t3279652951 * ___entry_0;
	// System.Object UnityStandardAssets.Utility.TimedObjectActivator/<Activate>c__Iterator0::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean UnityStandardAssets.Utility.TimedObjectActivator/<Activate>c__Iterator0::$disposing
	bool ___U24disposing_2;
	// System.Int32 UnityStandardAssets.Utility.TimedObjectActivator/<Activate>c__Iterator0::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_entry_0() { return static_cast<int32_t>(offsetof(U3CActivateU3Ec__Iterator0_t368634801, ___entry_0)); }
	inline Entry_t3279652951 * get_entry_0() const { return ___entry_0; }
	inline Entry_t3279652951 ** get_address_of_entry_0() { return &___entry_0; }
	inline void set_entry_0(Entry_t3279652951 * value)
	{
		___entry_0 = value;
		Il2CppCodeGenWriteBarrier((&___entry_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CActivateU3Ec__Iterator0_t368634801, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CActivateU3Ec__Iterator0_t368634801, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CActivateU3Ec__Iterator0_t368634801, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CACTIVATEU3EC__ITERATOR0_T368634801_H
#ifndef U3CDEACTIVATEU3EC__ITERATOR1_T3199820129_H
#define U3CDEACTIVATEU3EC__ITERATOR1_T3199820129_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.TimedObjectActivator/<Deactivate>c__Iterator1
struct  U3CDeactivateU3Ec__Iterator1_t3199820129  : public RuntimeObject
{
public:
	// UnityStandardAssets.Utility.TimedObjectActivator/Entry UnityStandardAssets.Utility.TimedObjectActivator/<Deactivate>c__Iterator1::entry
	Entry_t3279652951 * ___entry_0;
	// System.Object UnityStandardAssets.Utility.TimedObjectActivator/<Deactivate>c__Iterator1::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean UnityStandardAssets.Utility.TimedObjectActivator/<Deactivate>c__Iterator1::$disposing
	bool ___U24disposing_2;
	// System.Int32 UnityStandardAssets.Utility.TimedObjectActivator/<Deactivate>c__Iterator1::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_entry_0() { return static_cast<int32_t>(offsetof(U3CDeactivateU3Ec__Iterator1_t3199820129, ___entry_0)); }
	inline Entry_t3279652951 * get_entry_0() const { return ___entry_0; }
	inline Entry_t3279652951 ** get_address_of_entry_0() { return &___entry_0; }
	inline void set_entry_0(Entry_t3279652951 * value)
	{
		___entry_0 = value;
		Il2CppCodeGenWriteBarrier((&___entry_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CDeactivateU3Ec__Iterator1_t3199820129, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CDeactivateU3Ec__Iterator1_t3199820129, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CDeactivateU3Ec__Iterator1_t3199820129, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CDEACTIVATEU3EC__ITERATOR1_T3199820129_H
#ifndef WAYPOINTLIST_T2428355280_H
#define WAYPOINTLIST_T2428355280_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.WaypointCircuit/WaypointList
struct  WaypointList_t2428355280  : public RuntimeObject
{
public:
	// UnityStandardAssets.Utility.WaypointCircuit UnityStandardAssets.Utility.WaypointCircuit/WaypointList::circuit
	WaypointCircuit_t802585562 * ___circuit_0;
	// UnityEngine.Transform[] UnityStandardAssets.Utility.WaypointCircuit/WaypointList::items
	TransformU5BU5D_t788543427* ___items_1;

public:
	inline static int32_t get_offset_of_circuit_0() { return static_cast<int32_t>(offsetof(WaypointList_t2428355280, ___circuit_0)); }
	inline WaypointCircuit_t802585562 * get_circuit_0() const { return ___circuit_0; }
	inline WaypointCircuit_t802585562 ** get_address_of_circuit_0() { return &___circuit_0; }
	inline void set_circuit_0(WaypointCircuit_t802585562 * value)
	{
		___circuit_0 = value;
		Il2CppCodeGenWriteBarrier((&___circuit_0), value);
	}

	inline static int32_t get_offset_of_items_1() { return static_cast<int32_t>(offsetof(WaypointList_t2428355280, ___items_1)); }
	inline TransformU5BU5D_t788543427* get_items_1() const { return ___items_1; }
	inline TransformU5BU5D_t788543427** get_address_of_items_1() { return &___items_1; }
	inline void set_items_1(TransformU5BU5D_t788543427* value)
	{
		___items_1 = value;
		Il2CppCodeGenWriteBarrier((&___items_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WAYPOINTLIST_T2428355280_H
#ifndef VUFORIABEHAVIOURCOMPONENTFACTORY_T2590131513_H
#define VUFORIABEHAVIOURCOMPONENTFACTORY_T2590131513_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.VuforiaBehaviourComponentFactory
struct  VuforiaBehaviourComponentFactory_t2590131513  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VUFORIABEHAVIOURCOMPONENTFACTORY_T2590131513_H
#ifndef VALUETYPE_T1306072465_H
#define VALUETYPE_T1306072465_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t1306072465  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t1306072465_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t1306072465_marshaled_com
{
};
#endif // VALUETYPE_T1306072465_H
#ifndef U3CSTARTU3EC__ITERATOR0_T3352859634_H
#define U3CSTARTU3EC__ITERATOR0_T3352859634_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.ParticleSystemDestroyer/<Start>c__Iterator0
struct  U3CStartU3Ec__Iterator0_t3352859634  : public RuntimeObject
{
public:
	// UnityEngine.ParticleSystem[] UnityStandardAssets.Utility.ParticleSystemDestroyer/<Start>c__Iterator0::<systems>__0
	ParticleSystemU5BU5D_t3849375490* ___U3CsystemsU3E__0_0;
	// UnityEngine.ParticleSystem[] UnityStandardAssets.Utility.ParticleSystemDestroyer/<Start>c__Iterator0::$locvar0
	ParticleSystemU5BU5D_t3849375490* ___U24locvar0_1;
	// System.Int32 UnityStandardAssets.Utility.ParticleSystemDestroyer/<Start>c__Iterator0::$locvar1
	int32_t ___U24locvar1_2;
	// System.Single UnityStandardAssets.Utility.ParticleSystemDestroyer/<Start>c__Iterator0::<stopTime>__0
	float ___U3CstopTimeU3E__0_3;
	// UnityEngine.ParticleSystem[] UnityStandardAssets.Utility.ParticleSystemDestroyer/<Start>c__Iterator0::$locvar2
	ParticleSystemU5BU5D_t3849375490* ___U24locvar2_4;
	// System.Int32 UnityStandardAssets.Utility.ParticleSystemDestroyer/<Start>c__Iterator0::$locvar3
	int32_t ___U24locvar3_5;
	// UnityStandardAssets.Utility.ParticleSystemDestroyer UnityStandardAssets.Utility.ParticleSystemDestroyer/<Start>c__Iterator0::$this
	ParticleSystemDestroyer_t3293524282 * ___U24this_6;
	// System.Object UnityStandardAssets.Utility.ParticleSystemDestroyer/<Start>c__Iterator0::$current
	RuntimeObject * ___U24current_7;
	// System.Boolean UnityStandardAssets.Utility.ParticleSystemDestroyer/<Start>c__Iterator0::$disposing
	bool ___U24disposing_8;
	// System.Int32 UnityStandardAssets.Utility.ParticleSystemDestroyer/<Start>c__Iterator0::$PC
	int32_t ___U24PC_9;

public:
	inline static int32_t get_offset_of_U3CsystemsU3E__0_0() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t3352859634, ___U3CsystemsU3E__0_0)); }
	inline ParticleSystemU5BU5D_t3849375490* get_U3CsystemsU3E__0_0() const { return ___U3CsystemsU3E__0_0; }
	inline ParticleSystemU5BU5D_t3849375490** get_address_of_U3CsystemsU3E__0_0() { return &___U3CsystemsU3E__0_0; }
	inline void set_U3CsystemsU3E__0_0(ParticleSystemU5BU5D_t3849375490* value)
	{
		___U3CsystemsU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CsystemsU3E__0_0), value);
	}

	inline static int32_t get_offset_of_U24locvar0_1() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t3352859634, ___U24locvar0_1)); }
	inline ParticleSystemU5BU5D_t3849375490* get_U24locvar0_1() const { return ___U24locvar0_1; }
	inline ParticleSystemU5BU5D_t3849375490** get_address_of_U24locvar0_1() { return &___U24locvar0_1; }
	inline void set_U24locvar0_1(ParticleSystemU5BU5D_t3849375490* value)
	{
		___U24locvar0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24locvar0_1), value);
	}

	inline static int32_t get_offset_of_U24locvar1_2() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t3352859634, ___U24locvar1_2)); }
	inline int32_t get_U24locvar1_2() const { return ___U24locvar1_2; }
	inline int32_t* get_address_of_U24locvar1_2() { return &___U24locvar1_2; }
	inline void set_U24locvar1_2(int32_t value)
	{
		___U24locvar1_2 = value;
	}

	inline static int32_t get_offset_of_U3CstopTimeU3E__0_3() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t3352859634, ___U3CstopTimeU3E__0_3)); }
	inline float get_U3CstopTimeU3E__0_3() const { return ___U3CstopTimeU3E__0_3; }
	inline float* get_address_of_U3CstopTimeU3E__0_3() { return &___U3CstopTimeU3E__0_3; }
	inline void set_U3CstopTimeU3E__0_3(float value)
	{
		___U3CstopTimeU3E__0_3 = value;
	}

	inline static int32_t get_offset_of_U24locvar2_4() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t3352859634, ___U24locvar2_4)); }
	inline ParticleSystemU5BU5D_t3849375490* get_U24locvar2_4() const { return ___U24locvar2_4; }
	inline ParticleSystemU5BU5D_t3849375490** get_address_of_U24locvar2_4() { return &___U24locvar2_4; }
	inline void set_U24locvar2_4(ParticleSystemU5BU5D_t3849375490* value)
	{
		___U24locvar2_4 = value;
		Il2CppCodeGenWriteBarrier((&___U24locvar2_4), value);
	}

	inline static int32_t get_offset_of_U24locvar3_5() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t3352859634, ___U24locvar3_5)); }
	inline int32_t get_U24locvar3_5() const { return ___U24locvar3_5; }
	inline int32_t* get_address_of_U24locvar3_5() { return &___U24locvar3_5; }
	inline void set_U24locvar3_5(int32_t value)
	{
		___U24locvar3_5 = value;
	}

	inline static int32_t get_offset_of_U24this_6() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t3352859634, ___U24this_6)); }
	inline ParticleSystemDestroyer_t3293524282 * get_U24this_6() const { return ___U24this_6; }
	inline ParticleSystemDestroyer_t3293524282 ** get_address_of_U24this_6() { return &___U24this_6; }
	inline void set_U24this_6(ParticleSystemDestroyer_t3293524282 * value)
	{
		___U24this_6 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_6), value);
	}

	inline static int32_t get_offset_of_U24current_7() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t3352859634, ___U24current_7)); }
	inline RuntimeObject * get_U24current_7() const { return ___U24current_7; }
	inline RuntimeObject ** get_address_of_U24current_7() { return &___U24current_7; }
	inline void set_U24current_7(RuntimeObject * value)
	{
		___U24current_7 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_7), value);
	}

	inline static int32_t get_offset_of_U24disposing_8() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t3352859634, ___U24disposing_8)); }
	inline bool get_U24disposing_8() const { return ___U24disposing_8; }
	inline bool* get_address_of_U24disposing_8() { return &___U24disposing_8; }
	inline void set_U24disposing_8(bool value)
	{
		___U24disposing_8 = value;
	}

	inline static int32_t get_offset_of_U24PC_9() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t3352859634, ___U24PC_9)); }
	inline int32_t get_U24PC_9() const { return ___U24PC_9; }
	inline int32_t* get_address_of_U24PC_9() { return &___U24PC_9; }
	inline void set_U24PC_9(int32_t value)
	{
		___U24PC_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSTARTU3EC__ITERATOR0_T3352859634_H
#ifndef REPLACEMENTDEFINITION_T1571218451_H
#define REPLACEMENTDEFINITION_T1571218451_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.AutoMobileShaderSwitch/ReplacementDefinition
struct  ReplacementDefinition_t1571218451  : public RuntimeObject
{
public:
	// UnityEngine.Shader UnityStandardAssets.Utility.AutoMobileShaderSwitch/ReplacementDefinition::original
	Shader_t3778723916 * ___original_0;
	// UnityEngine.Shader UnityStandardAssets.Utility.AutoMobileShaderSwitch/ReplacementDefinition::replacement
	Shader_t3778723916 * ___replacement_1;

public:
	inline static int32_t get_offset_of_original_0() { return static_cast<int32_t>(offsetof(ReplacementDefinition_t1571218451, ___original_0)); }
	inline Shader_t3778723916 * get_original_0() const { return ___original_0; }
	inline Shader_t3778723916 ** get_address_of_original_0() { return &___original_0; }
	inline void set_original_0(Shader_t3778723916 * value)
	{
		___original_0 = value;
		Il2CppCodeGenWriteBarrier((&___original_0), value);
	}

	inline static int32_t get_offset_of_replacement_1() { return static_cast<int32_t>(offsetof(ReplacementDefinition_t1571218451, ___replacement_1)); }
	inline Shader_t3778723916 * get_replacement_1() const { return ___replacement_1; }
	inline Shader_t3778723916 ** get_address_of_replacement_1() { return &___replacement_1; }
	inline void set_replacement_1(Shader_t3778723916 * value)
	{
		___replacement_1 = value;
		Il2CppCodeGenWriteBarrier((&___replacement_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REPLACEMENTDEFINITION_T1571218451_H
#ifndef U3CRELOADLEVELU3EC__ITERATOR2_T1713074067_H
#define U3CRELOADLEVELU3EC__ITERATOR2_T1713074067_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.TimedObjectActivator/<ReloadLevel>c__Iterator2
struct  U3CReloadLevelU3Ec__Iterator2_t1713074067  : public RuntimeObject
{
public:
	// UnityStandardAssets.Utility.TimedObjectActivator/Entry UnityStandardAssets.Utility.TimedObjectActivator/<ReloadLevel>c__Iterator2::entry
	Entry_t3279652951 * ___entry_0;
	// System.Object UnityStandardAssets.Utility.TimedObjectActivator/<ReloadLevel>c__Iterator2::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean UnityStandardAssets.Utility.TimedObjectActivator/<ReloadLevel>c__Iterator2::$disposing
	bool ___U24disposing_2;
	// System.Int32 UnityStandardAssets.Utility.TimedObjectActivator/<ReloadLevel>c__Iterator2::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_entry_0() { return static_cast<int32_t>(offsetof(U3CReloadLevelU3Ec__Iterator2_t1713074067, ___entry_0)); }
	inline Entry_t3279652951 * get_entry_0() const { return ___entry_0; }
	inline Entry_t3279652951 ** get_address_of_entry_0() { return &___entry_0; }
	inline void set_entry_0(Entry_t3279652951 * value)
	{
		___entry_0 = value;
		Il2CppCodeGenWriteBarrier((&___entry_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CReloadLevelU3Ec__Iterator2_t1713074067, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CReloadLevelU3Ec__Iterator2_t1713074067, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CReloadLevelU3Ec__Iterator2_t1713074067, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CRELOADLEVELU3EC__ITERATOR2_T1713074067_H
#ifndef ADVANCEDSETTINGS_T2632082398_H
#define ADVANCEDSETTINGS_T2632082398_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Characters.FirstPerson.RigidbodyFirstPersonController/AdvancedSettings
struct  AdvancedSettings_t2632082398  : public RuntimeObject
{
public:
	// System.Single UnityStandardAssets.Characters.FirstPerson.RigidbodyFirstPersonController/AdvancedSettings::groundCheckDistance
	float ___groundCheckDistance_0;
	// System.Single UnityStandardAssets.Characters.FirstPerson.RigidbodyFirstPersonController/AdvancedSettings::stickToGroundHelperDistance
	float ___stickToGroundHelperDistance_1;
	// System.Single UnityStandardAssets.Characters.FirstPerson.RigidbodyFirstPersonController/AdvancedSettings::slowDownRate
	float ___slowDownRate_2;
	// System.Boolean UnityStandardAssets.Characters.FirstPerson.RigidbodyFirstPersonController/AdvancedSettings::airControl
	bool ___airControl_3;
	// System.Single UnityStandardAssets.Characters.FirstPerson.RigidbodyFirstPersonController/AdvancedSettings::shellOffset
	float ___shellOffset_4;

public:
	inline static int32_t get_offset_of_groundCheckDistance_0() { return static_cast<int32_t>(offsetof(AdvancedSettings_t2632082398, ___groundCheckDistance_0)); }
	inline float get_groundCheckDistance_0() const { return ___groundCheckDistance_0; }
	inline float* get_address_of_groundCheckDistance_0() { return &___groundCheckDistance_0; }
	inline void set_groundCheckDistance_0(float value)
	{
		___groundCheckDistance_0 = value;
	}

	inline static int32_t get_offset_of_stickToGroundHelperDistance_1() { return static_cast<int32_t>(offsetof(AdvancedSettings_t2632082398, ___stickToGroundHelperDistance_1)); }
	inline float get_stickToGroundHelperDistance_1() const { return ___stickToGroundHelperDistance_1; }
	inline float* get_address_of_stickToGroundHelperDistance_1() { return &___stickToGroundHelperDistance_1; }
	inline void set_stickToGroundHelperDistance_1(float value)
	{
		___stickToGroundHelperDistance_1 = value;
	}

	inline static int32_t get_offset_of_slowDownRate_2() { return static_cast<int32_t>(offsetof(AdvancedSettings_t2632082398, ___slowDownRate_2)); }
	inline float get_slowDownRate_2() const { return ___slowDownRate_2; }
	inline float* get_address_of_slowDownRate_2() { return &___slowDownRate_2; }
	inline void set_slowDownRate_2(float value)
	{
		___slowDownRate_2 = value;
	}

	inline static int32_t get_offset_of_airControl_3() { return static_cast<int32_t>(offsetof(AdvancedSettings_t2632082398, ___airControl_3)); }
	inline bool get_airControl_3() const { return ___airControl_3; }
	inline bool* get_address_of_airControl_3() { return &___airControl_3; }
	inline void set_airControl_3(bool value)
	{
		___airControl_3 = value;
	}

	inline static int32_t get_offset_of_shellOffset_4() { return static_cast<int32_t>(offsetof(AdvancedSettings_t2632082398, ___shellOffset_4)); }
	inline float get_shellOffset_4() const { return ___shellOffset_4; }
	inline float* get_address_of_shellOffset_4() { return &___shellOffset_4; }
	inline void set_shellOffset_4(float value)
	{
		___shellOffset_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADVANCEDSETTINGS_T2632082398_H
#ifndef CROSSPLATFORMINPUTMANAGER_T2677747245_H
#define CROSSPLATFORMINPUTMANAGER_T2677747245_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager
struct  CrossPlatformInputManager_t2677747245  : public RuntimeObject
{
public:

public:
};

struct CrossPlatformInputManager_t2677747245_StaticFields
{
public:
	// UnityStandardAssets.CrossPlatformInput.VirtualInput UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager::activeInput
	VirtualInput_t2381112337 * ___activeInput_0;
	// UnityStandardAssets.CrossPlatformInput.VirtualInput UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager::s_TouchInput
	VirtualInput_t2381112337 * ___s_TouchInput_1;
	// UnityStandardAssets.CrossPlatformInput.VirtualInput UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager::s_HardwareInput
	VirtualInput_t2381112337 * ___s_HardwareInput_2;

public:
	inline static int32_t get_offset_of_activeInput_0() { return static_cast<int32_t>(offsetof(CrossPlatformInputManager_t2677747245_StaticFields, ___activeInput_0)); }
	inline VirtualInput_t2381112337 * get_activeInput_0() const { return ___activeInput_0; }
	inline VirtualInput_t2381112337 ** get_address_of_activeInput_0() { return &___activeInput_0; }
	inline void set_activeInput_0(VirtualInput_t2381112337 * value)
	{
		___activeInput_0 = value;
		Il2CppCodeGenWriteBarrier((&___activeInput_0), value);
	}

	inline static int32_t get_offset_of_s_TouchInput_1() { return static_cast<int32_t>(offsetof(CrossPlatformInputManager_t2677747245_StaticFields, ___s_TouchInput_1)); }
	inline VirtualInput_t2381112337 * get_s_TouchInput_1() const { return ___s_TouchInput_1; }
	inline VirtualInput_t2381112337 ** get_address_of_s_TouchInput_1() { return &___s_TouchInput_1; }
	inline void set_s_TouchInput_1(VirtualInput_t2381112337 * value)
	{
		___s_TouchInput_1 = value;
		Il2CppCodeGenWriteBarrier((&___s_TouchInput_1), value);
	}

	inline static int32_t get_offset_of_s_HardwareInput_2() { return static_cast<int32_t>(offsetof(CrossPlatformInputManager_t2677747245_StaticFields, ___s_HardwareInput_2)); }
	inline VirtualInput_t2381112337 * get_s_HardwareInput_2() const { return ___s_HardwareInput_2; }
	inline VirtualInput_t2381112337 ** get_address_of_s_HardwareInput_2() { return &___s_HardwareInput_2; }
	inline void set_s_HardwareInput_2(VirtualInput_t2381112337 * value)
	{
		___s_HardwareInput_2 = value;
		Il2CppCodeGenWriteBarrier((&___s_HardwareInput_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CROSSPLATFORMINPUTMANAGER_T2677747245_H
#ifndef VIRTUALAXIS_T2383171311_H
#define VIRTUALAXIS_T2383171311_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualAxis
struct  VirtualAxis_t2383171311  : public RuntimeObject
{
public:
	// System.String UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualAxis::<name>k__BackingField
	String_t* ___U3CnameU3Ek__BackingField_0;
	// System.Single UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualAxis::m_Value
	float ___m_Value_1;
	// System.Boolean UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualAxis::<matchWithInputManager>k__BackingField
	bool ___U3CmatchWithInputManagerU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CnameU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(VirtualAxis_t2383171311, ___U3CnameU3Ek__BackingField_0)); }
	inline String_t* get_U3CnameU3Ek__BackingField_0() const { return ___U3CnameU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CnameU3Ek__BackingField_0() { return &___U3CnameU3Ek__BackingField_0; }
	inline void set_U3CnameU3Ek__BackingField_0(String_t* value)
	{
		___U3CnameU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CnameU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_m_Value_1() { return static_cast<int32_t>(offsetof(VirtualAxis_t2383171311, ___m_Value_1)); }
	inline float get_m_Value_1() const { return ___m_Value_1; }
	inline float* get_address_of_m_Value_1() { return &___m_Value_1; }
	inline void set_m_Value_1(float value)
	{
		___m_Value_1 = value;
	}

	inline static int32_t get_offset_of_U3CmatchWithInputManagerU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(VirtualAxis_t2383171311, ___U3CmatchWithInputManagerU3Ek__BackingField_2)); }
	inline bool get_U3CmatchWithInputManagerU3Ek__BackingField_2() const { return ___U3CmatchWithInputManagerU3Ek__BackingField_2; }
	inline bool* get_address_of_U3CmatchWithInputManagerU3Ek__BackingField_2() { return &___U3CmatchWithInputManagerU3Ek__BackingField_2; }
	inline void set_U3CmatchWithInputManagerU3Ek__BackingField_2(bool value)
	{
		___U3CmatchWithInputManagerU3Ek__BackingField_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VIRTUALAXIS_T2383171311_H
#ifndef VIRTUALBUTTON_T1545222198_H
#define VIRTUALBUTTON_T1545222198_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualButton
struct  VirtualButton_t1545222198  : public RuntimeObject
{
public:
	// System.String UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualButton::<name>k__BackingField
	String_t* ___U3CnameU3Ek__BackingField_0;
	// System.Boolean UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualButton::<matchWithInputManager>k__BackingField
	bool ___U3CmatchWithInputManagerU3Ek__BackingField_1;
	// System.Int32 UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualButton::m_LastPressedFrame
	int32_t ___m_LastPressedFrame_2;
	// System.Int32 UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualButton::m_ReleasedFrame
	int32_t ___m_ReleasedFrame_3;
	// System.Boolean UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualButton::m_Pressed
	bool ___m_Pressed_4;

public:
	inline static int32_t get_offset_of_U3CnameU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(VirtualButton_t1545222198, ___U3CnameU3Ek__BackingField_0)); }
	inline String_t* get_U3CnameU3Ek__BackingField_0() const { return ___U3CnameU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CnameU3Ek__BackingField_0() { return &___U3CnameU3Ek__BackingField_0; }
	inline void set_U3CnameU3Ek__BackingField_0(String_t* value)
	{
		___U3CnameU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CnameU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CmatchWithInputManagerU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(VirtualButton_t1545222198, ___U3CmatchWithInputManagerU3Ek__BackingField_1)); }
	inline bool get_U3CmatchWithInputManagerU3Ek__BackingField_1() const { return ___U3CmatchWithInputManagerU3Ek__BackingField_1; }
	inline bool* get_address_of_U3CmatchWithInputManagerU3Ek__BackingField_1() { return &___U3CmatchWithInputManagerU3Ek__BackingField_1; }
	inline void set_U3CmatchWithInputManagerU3Ek__BackingField_1(bool value)
	{
		___U3CmatchWithInputManagerU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_m_LastPressedFrame_2() { return static_cast<int32_t>(offsetof(VirtualButton_t1545222198, ___m_LastPressedFrame_2)); }
	inline int32_t get_m_LastPressedFrame_2() const { return ___m_LastPressedFrame_2; }
	inline int32_t* get_address_of_m_LastPressedFrame_2() { return &___m_LastPressedFrame_2; }
	inline void set_m_LastPressedFrame_2(int32_t value)
	{
		___m_LastPressedFrame_2 = value;
	}

	inline static int32_t get_offset_of_m_ReleasedFrame_3() { return static_cast<int32_t>(offsetof(VirtualButton_t1545222198, ___m_ReleasedFrame_3)); }
	inline int32_t get_m_ReleasedFrame_3() const { return ___m_ReleasedFrame_3; }
	inline int32_t* get_address_of_m_ReleasedFrame_3() { return &___m_ReleasedFrame_3; }
	inline void set_m_ReleasedFrame_3(int32_t value)
	{
		___m_ReleasedFrame_3 = value;
	}

	inline static int32_t get_offset_of_m_Pressed_4() { return static_cast<int32_t>(offsetof(VirtualButton_t1545222198, ___m_Pressed_4)); }
	inline bool get_m_Pressed_4() const { return ___m_Pressed_4; }
	inline bool* get_address_of_m_Pressed_4() { return &___m_Pressed_4; }
	inline void set_m_Pressed_4(bool value)
	{
		___m_Pressed_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VIRTUALBUTTON_T1545222198_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	IntPtr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline IntPtr_t get_Zero_1() const { return ___Zero_1; }
	inline IntPtr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(IntPtr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef VECTOR2_T3577333262_H
#define VECTOR2_T3577333262_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2
struct  Vector2_t3577333262 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_t3577333262, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_t3577333262, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_t3577333262_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_t3577333262  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_t3577333262  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_t3577333262  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_t3577333262  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_t3577333262  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_t3577333262  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_t3577333262  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_t3577333262  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_t3577333262_StaticFields, ___zeroVector_2)); }
	inline Vector2_t3577333262  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_t3577333262 * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_t3577333262  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_t3577333262_StaticFields, ___oneVector_3)); }
	inline Vector2_t3577333262  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_t3577333262 * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_t3577333262  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_t3577333262_StaticFields, ___upVector_4)); }
	inline Vector2_t3577333262  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_t3577333262 * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_t3577333262  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_t3577333262_StaticFields, ___downVector_5)); }
	inline Vector2_t3577333262  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_t3577333262 * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_t3577333262  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_t3577333262_StaticFields, ___leftVector_6)); }
	inline Vector2_t3577333262  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_t3577333262 * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_t3577333262  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_t3577333262_StaticFields, ___rightVector_7)); }
	inline Vector2_t3577333262  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_t3577333262 * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_t3577333262  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_t3577333262_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_t3577333262  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_t3577333262 * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_t3577333262  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_t3577333262_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_t3577333262  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_t3577333262 * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_t3577333262  value)
	{
		___negativeInfinityVector_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_T3577333262_H
#ifndef QUATERNION_T754065749_H
#define QUATERNION_T754065749_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Quaternion
struct  Quaternion_t754065749 
{
public:
	// System.Single UnityEngine.Quaternion::x
	float ___x_0;
	// System.Single UnityEngine.Quaternion::y
	float ___y_1;
	// System.Single UnityEngine.Quaternion::z
	float ___z_2;
	// System.Single UnityEngine.Quaternion::w
	float ___w_3;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Quaternion_t754065749, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Quaternion_t754065749, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(Quaternion_t754065749, ___z_2)); }
	inline float get_z_2() const { return ___z_2; }
	inline float* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(float value)
	{
		___z_2 = value;
	}

	inline static int32_t get_offset_of_w_3() { return static_cast<int32_t>(offsetof(Quaternion_t754065749, ___w_3)); }
	inline float get_w_3() const { return ___w_3; }
	inline float* get_address_of_w_3() { return &___w_3; }
	inline void set_w_3(float value)
	{
		___w_3 = value;
	}
};

struct Quaternion_t754065749_StaticFields
{
public:
	// UnityEngine.Quaternion UnityEngine.Quaternion::identityQuaternion
	Quaternion_t754065749  ___identityQuaternion_4;

public:
	inline static int32_t get_offset_of_identityQuaternion_4() { return static_cast<int32_t>(offsetof(Quaternion_t754065749_StaticFields, ___identityQuaternion_4)); }
	inline Quaternion_t754065749  get_identityQuaternion_4() const { return ___identityQuaternion_4; }
	inline Quaternion_t754065749 * get_address_of_identityQuaternion_4() { return &___identityQuaternion_4; }
	inline void set_identityQuaternion_4(Quaternion_t754065749  value)
	{
		___identityQuaternion_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUATERNION_T754065749_H
#ifndef ENUM_T978401890_H
#define ENUM_T978401890_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t978401890  : public ValueType_t1306072465
{
public:

public:
};

struct Enum_t978401890_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t45629911* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t978401890_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t45629911* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t45629911** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t45629911* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t978401890_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t978401890_marshaled_com
{
};
#endif // ENUM_T978401890_H
#ifndef RECT_T3436776195_H
#define RECT_T3436776195_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rect
struct  Rect_t3436776195 
{
public:
	// System.Single UnityEngine.Rect::m_XMin
	float ___m_XMin_0;
	// System.Single UnityEngine.Rect::m_YMin
	float ___m_YMin_1;
	// System.Single UnityEngine.Rect::m_Width
	float ___m_Width_2;
	// System.Single UnityEngine.Rect::m_Height
	float ___m_Height_3;

public:
	inline static int32_t get_offset_of_m_XMin_0() { return static_cast<int32_t>(offsetof(Rect_t3436776195, ___m_XMin_0)); }
	inline float get_m_XMin_0() const { return ___m_XMin_0; }
	inline float* get_address_of_m_XMin_0() { return &___m_XMin_0; }
	inline void set_m_XMin_0(float value)
	{
		___m_XMin_0 = value;
	}

	inline static int32_t get_offset_of_m_YMin_1() { return static_cast<int32_t>(offsetof(Rect_t3436776195, ___m_YMin_1)); }
	inline float get_m_YMin_1() const { return ___m_YMin_1; }
	inline float* get_address_of_m_YMin_1() { return &___m_YMin_1; }
	inline void set_m_YMin_1(float value)
	{
		___m_YMin_1 = value;
	}

	inline static int32_t get_offset_of_m_Width_2() { return static_cast<int32_t>(offsetof(Rect_t3436776195, ___m_Width_2)); }
	inline float get_m_Width_2() const { return ___m_Width_2; }
	inline float* get_address_of_m_Width_2() { return &___m_Width_2; }
	inline void set_m_Width_2(float value)
	{
		___m_Width_2 = value;
	}

	inline static int32_t get_offset_of_m_Height_3() { return static_cast<int32_t>(offsetof(Rect_t3436776195, ___m_Height_3)); }
	inline float get_m_Height_3() const { return ___m_Height_3; }
	inline float* get_address_of_m_Height_3() { return &___m_Height_3; }
	inline void set_m_Height_3(float value)
	{
		___m_Height_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RECT_T3436776195_H
#ifndef VECTOR3_T2903530434_H
#define VECTOR3_T2903530434_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_t2903530434 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_1;
	// System.Single UnityEngine.Vector3::y
	float ___y_2;
	// System.Single UnityEngine.Vector3::z
	float ___z_3;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector3_t2903530434, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector3_t2903530434, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector3_t2903530434, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}
};

struct Vector3_t2903530434_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t2903530434  ___zeroVector_4;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t2903530434  ___oneVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t2903530434  ___upVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t2903530434  ___downVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t2903530434  ___leftVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t2903530434  ___rightVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t2903530434  ___forwardVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t2903530434  ___backVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t2903530434  ___positiveInfinityVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t2903530434  ___negativeInfinityVector_13;

public:
	inline static int32_t get_offset_of_zeroVector_4() { return static_cast<int32_t>(offsetof(Vector3_t2903530434_StaticFields, ___zeroVector_4)); }
	inline Vector3_t2903530434  get_zeroVector_4() const { return ___zeroVector_4; }
	inline Vector3_t2903530434 * get_address_of_zeroVector_4() { return &___zeroVector_4; }
	inline void set_zeroVector_4(Vector3_t2903530434  value)
	{
		___zeroVector_4 = value;
	}

	inline static int32_t get_offset_of_oneVector_5() { return static_cast<int32_t>(offsetof(Vector3_t2903530434_StaticFields, ___oneVector_5)); }
	inline Vector3_t2903530434  get_oneVector_5() const { return ___oneVector_5; }
	inline Vector3_t2903530434 * get_address_of_oneVector_5() { return &___oneVector_5; }
	inline void set_oneVector_5(Vector3_t2903530434  value)
	{
		___oneVector_5 = value;
	}

	inline static int32_t get_offset_of_upVector_6() { return static_cast<int32_t>(offsetof(Vector3_t2903530434_StaticFields, ___upVector_6)); }
	inline Vector3_t2903530434  get_upVector_6() const { return ___upVector_6; }
	inline Vector3_t2903530434 * get_address_of_upVector_6() { return &___upVector_6; }
	inline void set_upVector_6(Vector3_t2903530434  value)
	{
		___upVector_6 = value;
	}

	inline static int32_t get_offset_of_downVector_7() { return static_cast<int32_t>(offsetof(Vector3_t2903530434_StaticFields, ___downVector_7)); }
	inline Vector3_t2903530434  get_downVector_7() const { return ___downVector_7; }
	inline Vector3_t2903530434 * get_address_of_downVector_7() { return &___downVector_7; }
	inline void set_downVector_7(Vector3_t2903530434  value)
	{
		___downVector_7 = value;
	}

	inline static int32_t get_offset_of_leftVector_8() { return static_cast<int32_t>(offsetof(Vector3_t2903530434_StaticFields, ___leftVector_8)); }
	inline Vector3_t2903530434  get_leftVector_8() const { return ___leftVector_8; }
	inline Vector3_t2903530434 * get_address_of_leftVector_8() { return &___leftVector_8; }
	inline void set_leftVector_8(Vector3_t2903530434  value)
	{
		___leftVector_8 = value;
	}

	inline static int32_t get_offset_of_rightVector_9() { return static_cast<int32_t>(offsetof(Vector3_t2903530434_StaticFields, ___rightVector_9)); }
	inline Vector3_t2903530434  get_rightVector_9() const { return ___rightVector_9; }
	inline Vector3_t2903530434 * get_address_of_rightVector_9() { return &___rightVector_9; }
	inline void set_rightVector_9(Vector3_t2903530434  value)
	{
		___rightVector_9 = value;
	}

	inline static int32_t get_offset_of_forwardVector_10() { return static_cast<int32_t>(offsetof(Vector3_t2903530434_StaticFields, ___forwardVector_10)); }
	inline Vector3_t2903530434  get_forwardVector_10() const { return ___forwardVector_10; }
	inline Vector3_t2903530434 * get_address_of_forwardVector_10() { return &___forwardVector_10; }
	inline void set_forwardVector_10(Vector3_t2903530434  value)
	{
		___forwardVector_10 = value;
	}

	inline static int32_t get_offset_of_backVector_11() { return static_cast<int32_t>(offsetof(Vector3_t2903530434_StaticFields, ___backVector_11)); }
	inline Vector3_t2903530434  get_backVector_11() const { return ___backVector_11; }
	inline Vector3_t2903530434 * get_address_of_backVector_11() { return &___backVector_11; }
	inline void set_backVector_11(Vector3_t2903530434  value)
	{
		___backVector_11 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_12() { return static_cast<int32_t>(offsetof(Vector3_t2903530434_StaticFields, ___positiveInfinityVector_12)); }
	inline Vector3_t2903530434  get_positiveInfinityVector_12() const { return ___positiveInfinityVector_12; }
	inline Vector3_t2903530434 * get_address_of_positiveInfinityVector_12() { return &___positiveInfinityVector_12; }
	inline void set_positiveInfinityVector_12(Vector3_t2903530434  value)
	{
		___positiveInfinityVector_12 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t2903530434_StaticFields, ___negativeInfinityVector_13)); }
	inline Vector3_t2903530434  get_negativeInfinityVector_13() const { return ___negativeInfinityVector_13; }
	inline Vector3_t2903530434 * get_address_of_negativeInfinityVector_13() { return &___negativeInfinityVector_13; }
	inline void set_negativeInfinityVector_13(Vector3_t2903530434  value)
	{
		___negativeInfinityVector_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_T2903530434_H
#ifndef VEC2I_T895597728_H
#define VEC2I_T895597728_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.VuforiaRenderer/Vec2I
#pragma pack(push, tp, 1)
struct  Vec2I_t895597728 
{
public:
	// System.Int32 Vuforia.VuforiaRenderer/Vec2I::x
	int32_t ___x_0;
	// System.Int32 Vuforia.VuforiaRenderer/Vec2I::y
	int32_t ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vec2I_t895597728, ___x_0)); }
	inline int32_t get_x_0() const { return ___x_0; }
	inline int32_t* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(int32_t value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vec2I_t895597728, ___y_1)); }
	inline int32_t get_y_1() const { return ___y_1; }
	inline int32_t* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(int32_t value)
	{
		___y_1 = value;
	}
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VEC2I_T895597728_H
#ifndef AXISOPTION_T438894465_H
#define AXISOPTION_T438894465_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.CrossPlatformInput.TouchPad/AxisOption
struct  AxisOption_t438894465 
{
public:
	// System.Int32 UnityStandardAssets.CrossPlatformInput.TouchPad/AxisOption::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(AxisOption_t438894465, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AXISOPTION_T438894465_H
#ifndef AXISOPTION_T1620100083_H
#define AXISOPTION_T1620100083_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.CrossPlatformInput.Joystick/AxisOption
struct  AxisOption_t1620100083 
{
public:
	// System.Int32 UnityStandardAssets.CrossPlatformInput.Joystick/AxisOption::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(AxisOption_t1620100083, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AXISOPTION_T1620100083_H
#ifndef COLLISIONFLAGS_T2538223826_H
#define COLLISIONFLAGS_T2538223826_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.CollisionFlags
struct  CollisionFlags_t2538223826 
{
public:
	// System.Int32 UnityEngine.CollisionFlags::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(CollisionFlags_t2538223826, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLLISIONFLAGS_T2538223826_H
#ifndef OBJECT_T1502412432_H
#define OBJECT_T1502412432_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t1502412432  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	IntPtr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t1502412432, ___m_CachedPtr_0)); }
	inline IntPtr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline IntPtr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(IntPtr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t1502412432_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t1502412432_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t1502412432_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t1502412432_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T1502412432_H
#ifndef ACTION_T2958281111_H
#define ACTION_T2958281111_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.TimedObjectActivator/Action
struct  Action_t2958281111 
{
public:
	// System.Int32 UnityStandardAssets.Utility.TimedObjectActivator/Action::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Action_t2958281111, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ACTION_T2958281111_H
#ifndef KEYCODE_T3546492115_H
#define KEYCODE_T3546492115_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.KeyCode
struct  KeyCode_t3546492115 
{
public:
	// System.Int32 UnityEngine.KeyCode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(KeyCode_t3546492115, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYCODE_T3546492115_H
#ifndef SPACE_T3412239527_H
#define SPACE_T3412239527_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Space
struct  Space_t3412239527 
{
public:
	// System.Int32 UnityEngine.Space::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Space_t3412239527, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPACE_T3412239527_H
#ifndef BUILDTARGETGROUP_T3433781248_H
#define BUILDTARGETGROUP_T3433781248_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.PlatformSpecificContent/BuildTargetGroup
struct  BuildTargetGroup_t3433781248 
{
public:
	// System.Int32 UnityStandardAssets.Utility.PlatformSpecificContent/BuildTargetGroup::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(BuildTargetGroup_t3433781248, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUILDTARGETGROUP_T3433781248_H
#ifndef RAY_T3821377119_H
#define RAY_T3821377119_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Ray
struct  Ray_t3821377119 
{
public:
	// UnityEngine.Vector3 UnityEngine.Ray::m_Origin
	Vector3_t2903530434  ___m_Origin_0;
	// UnityEngine.Vector3 UnityEngine.Ray::m_Direction
	Vector3_t2903530434  ___m_Direction_1;

public:
	inline static int32_t get_offset_of_m_Origin_0() { return static_cast<int32_t>(offsetof(Ray_t3821377119, ___m_Origin_0)); }
	inline Vector3_t2903530434  get_m_Origin_0() const { return ___m_Origin_0; }
	inline Vector3_t2903530434 * get_address_of_m_Origin_0() { return &___m_Origin_0; }
	inline void set_m_Origin_0(Vector3_t2903530434  value)
	{
		___m_Origin_0 = value;
	}

	inline static int32_t get_offset_of_m_Direction_1() { return static_cast<int32_t>(offsetof(Ray_t3821377119, ___m_Direction_1)); }
	inline Vector3_t2903530434  get_m_Direction_1() const { return ___m_Direction_1; }
	inline Vector3_t2903530434 * get_address_of_m_Direction_1() { return &___m_Direction_1; }
	inline void set_m_Direction_1(Vector3_t2903530434  value)
	{
		___m_Direction_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RAY_T3821377119_H
#ifndef ROUTEPOINT_T4047115680_H
#define ROUTEPOINT_T4047115680_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.WaypointCircuit/RoutePoint
struct  RoutePoint_t4047115680 
{
public:
	// UnityEngine.Vector3 UnityStandardAssets.Utility.WaypointCircuit/RoutePoint::position
	Vector3_t2903530434  ___position_0;
	// UnityEngine.Vector3 UnityStandardAssets.Utility.WaypointCircuit/RoutePoint::direction
	Vector3_t2903530434  ___direction_1;

public:
	inline static int32_t get_offset_of_position_0() { return static_cast<int32_t>(offsetof(RoutePoint_t4047115680, ___position_0)); }
	inline Vector3_t2903530434  get_position_0() const { return ___position_0; }
	inline Vector3_t2903530434 * get_address_of_position_0() { return &___position_0; }
	inline void set_position_0(Vector3_t2903530434  value)
	{
		___position_0 = value;
	}

	inline static int32_t get_offset_of_direction_1() { return static_cast<int32_t>(offsetof(RoutePoint_t4047115680, ___direction_1)); }
	inline Vector3_t2903530434  get_direction_1() const { return ___direction_1; }
	inline Vector3_t2903530434 * get_address_of_direction_1() { return &___direction_1; }
	inline void set_direction_1(Vector3_t2903530434  value)
	{
		___direction_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ROUTEPOINT_T4047115680_H
#ifndef PROGRESSSTYLE_T2590666018_H
#define PROGRESSSTYLE_T2590666018_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.WaypointProgressTracker/ProgressStyle
struct  ProgressStyle_t2590666018 
{
public:
	// System.Int32 UnityStandardAssets.Utility.WaypointProgressTracker/ProgressStyle::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ProgressStyle_t2590666018, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROGRESSSTYLE_T2590666018_H
#ifndef SCREENORIENTATION_T2818551509_H
#define SCREENORIENTATION_T2818551509_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ScreenOrientation
struct  ScreenOrientation_t2818551509 
{
public:
	// System.Int32 UnityEngine.ScreenOrientation::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ScreenOrientation_t2818551509, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCREENORIENTATION_T2818551509_H
#ifndef MOUSELOOK_T3694953670_H
#define MOUSELOOK_T3694953670_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Characters.FirstPerson.MouseLook
struct  MouseLook_t3694953670  : public RuntimeObject
{
public:
	// System.Single UnityStandardAssets.Characters.FirstPerson.MouseLook::XSensitivity
	float ___XSensitivity_0;
	// System.Single UnityStandardAssets.Characters.FirstPerson.MouseLook::YSensitivity
	float ___YSensitivity_1;
	// System.Boolean UnityStandardAssets.Characters.FirstPerson.MouseLook::clampVerticalRotation
	bool ___clampVerticalRotation_2;
	// System.Single UnityStandardAssets.Characters.FirstPerson.MouseLook::MinimumX
	float ___MinimumX_3;
	// System.Single UnityStandardAssets.Characters.FirstPerson.MouseLook::MaximumX
	float ___MaximumX_4;
	// System.Boolean UnityStandardAssets.Characters.FirstPerson.MouseLook::smooth
	bool ___smooth_5;
	// System.Single UnityStandardAssets.Characters.FirstPerson.MouseLook::smoothTime
	float ___smoothTime_6;
	// System.Boolean UnityStandardAssets.Characters.FirstPerson.MouseLook::lockCursor
	bool ___lockCursor_7;
	// UnityEngine.Quaternion UnityStandardAssets.Characters.FirstPerson.MouseLook::m_CharacterTargetRot
	Quaternion_t754065749  ___m_CharacterTargetRot_8;
	// UnityEngine.Quaternion UnityStandardAssets.Characters.FirstPerson.MouseLook::m_CameraTargetRot
	Quaternion_t754065749  ___m_CameraTargetRot_9;
	// System.Boolean UnityStandardAssets.Characters.FirstPerson.MouseLook::m_cursorIsLocked
	bool ___m_cursorIsLocked_10;

public:
	inline static int32_t get_offset_of_XSensitivity_0() { return static_cast<int32_t>(offsetof(MouseLook_t3694953670, ___XSensitivity_0)); }
	inline float get_XSensitivity_0() const { return ___XSensitivity_0; }
	inline float* get_address_of_XSensitivity_0() { return &___XSensitivity_0; }
	inline void set_XSensitivity_0(float value)
	{
		___XSensitivity_0 = value;
	}

	inline static int32_t get_offset_of_YSensitivity_1() { return static_cast<int32_t>(offsetof(MouseLook_t3694953670, ___YSensitivity_1)); }
	inline float get_YSensitivity_1() const { return ___YSensitivity_1; }
	inline float* get_address_of_YSensitivity_1() { return &___YSensitivity_1; }
	inline void set_YSensitivity_1(float value)
	{
		___YSensitivity_1 = value;
	}

	inline static int32_t get_offset_of_clampVerticalRotation_2() { return static_cast<int32_t>(offsetof(MouseLook_t3694953670, ___clampVerticalRotation_2)); }
	inline bool get_clampVerticalRotation_2() const { return ___clampVerticalRotation_2; }
	inline bool* get_address_of_clampVerticalRotation_2() { return &___clampVerticalRotation_2; }
	inline void set_clampVerticalRotation_2(bool value)
	{
		___clampVerticalRotation_2 = value;
	}

	inline static int32_t get_offset_of_MinimumX_3() { return static_cast<int32_t>(offsetof(MouseLook_t3694953670, ___MinimumX_3)); }
	inline float get_MinimumX_3() const { return ___MinimumX_3; }
	inline float* get_address_of_MinimumX_3() { return &___MinimumX_3; }
	inline void set_MinimumX_3(float value)
	{
		___MinimumX_3 = value;
	}

	inline static int32_t get_offset_of_MaximumX_4() { return static_cast<int32_t>(offsetof(MouseLook_t3694953670, ___MaximumX_4)); }
	inline float get_MaximumX_4() const { return ___MaximumX_4; }
	inline float* get_address_of_MaximumX_4() { return &___MaximumX_4; }
	inline void set_MaximumX_4(float value)
	{
		___MaximumX_4 = value;
	}

	inline static int32_t get_offset_of_smooth_5() { return static_cast<int32_t>(offsetof(MouseLook_t3694953670, ___smooth_5)); }
	inline bool get_smooth_5() const { return ___smooth_5; }
	inline bool* get_address_of_smooth_5() { return &___smooth_5; }
	inline void set_smooth_5(bool value)
	{
		___smooth_5 = value;
	}

	inline static int32_t get_offset_of_smoothTime_6() { return static_cast<int32_t>(offsetof(MouseLook_t3694953670, ___smoothTime_6)); }
	inline float get_smoothTime_6() const { return ___smoothTime_6; }
	inline float* get_address_of_smoothTime_6() { return &___smoothTime_6; }
	inline void set_smoothTime_6(float value)
	{
		___smoothTime_6 = value;
	}

	inline static int32_t get_offset_of_lockCursor_7() { return static_cast<int32_t>(offsetof(MouseLook_t3694953670, ___lockCursor_7)); }
	inline bool get_lockCursor_7() const { return ___lockCursor_7; }
	inline bool* get_address_of_lockCursor_7() { return &___lockCursor_7; }
	inline void set_lockCursor_7(bool value)
	{
		___lockCursor_7 = value;
	}

	inline static int32_t get_offset_of_m_CharacterTargetRot_8() { return static_cast<int32_t>(offsetof(MouseLook_t3694953670, ___m_CharacterTargetRot_8)); }
	inline Quaternion_t754065749  get_m_CharacterTargetRot_8() const { return ___m_CharacterTargetRot_8; }
	inline Quaternion_t754065749 * get_address_of_m_CharacterTargetRot_8() { return &___m_CharacterTargetRot_8; }
	inline void set_m_CharacterTargetRot_8(Quaternion_t754065749  value)
	{
		___m_CharacterTargetRot_8 = value;
	}

	inline static int32_t get_offset_of_m_CameraTargetRot_9() { return static_cast<int32_t>(offsetof(MouseLook_t3694953670, ___m_CameraTargetRot_9)); }
	inline Quaternion_t754065749  get_m_CameraTargetRot_9() const { return ___m_CameraTargetRot_9; }
	inline Quaternion_t754065749 * get_address_of_m_CameraTargetRot_9() { return &___m_CameraTargetRot_9; }
	inline void set_m_CameraTargetRot_9(Quaternion_t754065749  value)
	{
		___m_CameraTargetRot_9 = value;
	}

	inline static int32_t get_offset_of_m_cursorIsLocked_10() { return static_cast<int32_t>(offsetof(MouseLook_t3694953670, ___m_cursorIsLocked_10)); }
	inline bool get_m_cursorIsLocked_10() const { return ___m_cursorIsLocked_10; }
	inline bool* get_address_of_m_cursorIsLocked_10() { return &___m_cursorIsLocked_10; }
	inline void set_m_cursorIsLocked_10(bool value)
	{
		___m_cursorIsLocked_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MOUSELOOK_T3694953670_H
#ifndef ACTIVEINPUTMETHOD_T3635810715_H
#define ACTIVEINPUTMETHOD_T3635810715_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/ActiveInputMethod
struct  ActiveInputMethod_t3635810715 
{
public:
	// System.Int32 UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/ActiveInputMethod::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ActiveInputMethod_t3635810715, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ACTIVEINPUTMETHOD_T3635810715_H
#ifndef VIDEOTEXTUREINFO_T773013061_H
#define VIDEOTEXTUREINFO_T773013061_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.VuforiaRenderer/VideoTextureInfo
#pragma pack(push, tp, 1)
struct  VideoTextureInfo_t773013061 
{
public:
	// Vuforia.VuforiaRenderer/Vec2I Vuforia.VuforiaRenderer/VideoTextureInfo::textureSize
	Vec2I_t895597728  ___textureSize_0;
	// Vuforia.VuforiaRenderer/Vec2I Vuforia.VuforiaRenderer/VideoTextureInfo::imageSize
	Vec2I_t895597728  ___imageSize_1;

public:
	inline static int32_t get_offset_of_textureSize_0() { return static_cast<int32_t>(offsetof(VideoTextureInfo_t773013061, ___textureSize_0)); }
	inline Vec2I_t895597728  get_textureSize_0() const { return ___textureSize_0; }
	inline Vec2I_t895597728 * get_address_of_textureSize_0() { return &___textureSize_0; }
	inline void set_textureSize_0(Vec2I_t895597728  value)
	{
		___textureSize_0 = value;
	}

	inline static int32_t get_offset_of_imageSize_1() { return static_cast<int32_t>(offsetof(VideoTextureInfo_t773013061, ___imageSize_1)); }
	inline Vec2I_t895597728  get_imageSize_1() const { return ___imageSize_1; }
	inline Vec2I_t895597728 * get_address_of_imageSize_1() { return &___imageSize_1; }
	inline void set_imageSize_1(Vec2I_t895597728  value)
	{
		___imageSize_1 = value;
	}
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VIDEOTEXTUREINFO_T773013061_H
#ifndef CLIPPING_MODE_T306275072_H
#define CLIPPING_MODE_T306275072_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.HideExcessAreaAbstractBehaviour/CLIPPING_MODE
struct  CLIPPING_MODE_t306275072 
{
public:
	// System.Int32 Vuforia.HideExcessAreaAbstractBehaviour/CLIPPING_MODE::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(CLIPPING_MODE_t306275072, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CLIPPING_MODE_T306275072_H
#ifndef CONTROLSTYLE_T26348249_H
#define CONTROLSTYLE_T26348249_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.CrossPlatformInput.TouchPad/ControlStyle
struct  ControlStyle_t26348249 
{
public:
	// System.Int32 UnityStandardAssets.CrossPlatformInput.TouchPad/ControlStyle::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ControlStyle_t26348249, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONTROLSTYLE_T26348249_H
#ifndef VIRTUALINPUT_T2381112337_H
#define VIRTUALINPUT_T2381112337_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.CrossPlatformInput.VirtualInput
struct  VirtualInput_t2381112337  : public RuntimeObject
{
public:
	// UnityEngine.Vector3 UnityStandardAssets.CrossPlatformInput.VirtualInput::<virtualMousePosition>k__BackingField
	Vector3_t2903530434  ___U3CvirtualMousePositionU3Ek__BackingField_0;
	// System.Collections.Generic.Dictionary`2<System.String,UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualAxis> UnityStandardAssets.CrossPlatformInput.VirtualInput::m_VirtualAxes
	Dictionary_2_t3553060542 * ___m_VirtualAxes_1;
	// System.Collections.Generic.Dictionary`2<System.String,UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualButton> UnityStandardAssets.CrossPlatformInput.VirtualInput::m_VirtualButtons
	Dictionary_2_t2715111429 * ___m_VirtualButtons_2;
	// System.Collections.Generic.List`1<System.String> UnityStandardAssets.CrossPlatformInput.VirtualInput::m_AlwaysUseVirtual
	List_1_t1230370061 * ___m_AlwaysUseVirtual_3;

public:
	inline static int32_t get_offset_of_U3CvirtualMousePositionU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(VirtualInput_t2381112337, ___U3CvirtualMousePositionU3Ek__BackingField_0)); }
	inline Vector3_t2903530434  get_U3CvirtualMousePositionU3Ek__BackingField_0() const { return ___U3CvirtualMousePositionU3Ek__BackingField_0; }
	inline Vector3_t2903530434 * get_address_of_U3CvirtualMousePositionU3Ek__BackingField_0() { return &___U3CvirtualMousePositionU3Ek__BackingField_0; }
	inline void set_U3CvirtualMousePositionU3Ek__BackingField_0(Vector3_t2903530434  value)
	{
		___U3CvirtualMousePositionU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_m_VirtualAxes_1() { return static_cast<int32_t>(offsetof(VirtualInput_t2381112337, ___m_VirtualAxes_1)); }
	inline Dictionary_2_t3553060542 * get_m_VirtualAxes_1() const { return ___m_VirtualAxes_1; }
	inline Dictionary_2_t3553060542 ** get_address_of_m_VirtualAxes_1() { return &___m_VirtualAxes_1; }
	inline void set_m_VirtualAxes_1(Dictionary_2_t3553060542 * value)
	{
		___m_VirtualAxes_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_VirtualAxes_1), value);
	}

	inline static int32_t get_offset_of_m_VirtualButtons_2() { return static_cast<int32_t>(offsetof(VirtualInput_t2381112337, ___m_VirtualButtons_2)); }
	inline Dictionary_2_t2715111429 * get_m_VirtualButtons_2() const { return ___m_VirtualButtons_2; }
	inline Dictionary_2_t2715111429 ** get_address_of_m_VirtualButtons_2() { return &___m_VirtualButtons_2; }
	inline void set_m_VirtualButtons_2(Dictionary_2_t2715111429 * value)
	{
		___m_VirtualButtons_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_VirtualButtons_2), value);
	}

	inline static int32_t get_offset_of_m_AlwaysUseVirtual_3() { return static_cast<int32_t>(offsetof(VirtualInput_t2381112337, ___m_AlwaysUseVirtual_3)); }
	inline List_1_t1230370061 * get_m_AlwaysUseVirtual_3() const { return ___m_AlwaysUseVirtual_3; }
	inline List_1_t1230370061 ** get_address_of_m_AlwaysUseVirtual_3() { return &___m_AlwaysUseVirtual_3; }
	inline void set_m_AlwaysUseVirtual_3(List_1_t1230370061 * value)
	{
		___m_AlwaysUseVirtual_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_AlwaysUseVirtual_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VIRTUALINPUT_T2381112337_H
#ifndef IMAGETARGETTYPE_T2769160834_H
#define IMAGETARGETTYPE_T2769160834_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.ImageTargetType
struct  ImageTargetType_t2769160834 
{
public:
	// System.Int32 Vuforia.ImageTargetType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ImageTargetType_t2769160834, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IMAGETARGETTYPE_T2769160834_H
#ifndef CURVECONTROLLEDBOB_T3985701645_H
#define CURVECONTROLLEDBOB_T3985701645_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.CurveControlledBob
struct  CurveControlledBob_t3985701645  : public RuntimeObject
{
public:
	// System.Single UnityStandardAssets.Utility.CurveControlledBob::HorizontalBobRange
	float ___HorizontalBobRange_0;
	// System.Single UnityStandardAssets.Utility.CurveControlledBob::VerticalBobRange
	float ___VerticalBobRange_1;
	// UnityEngine.AnimationCurve UnityStandardAssets.Utility.CurveControlledBob::Bobcurve
	AnimationCurve_t2184836714 * ___Bobcurve_2;
	// System.Single UnityStandardAssets.Utility.CurveControlledBob::VerticaltoHorizontalRatio
	float ___VerticaltoHorizontalRatio_3;
	// System.Single UnityStandardAssets.Utility.CurveControlledBob::m_CyclePositionX
	float ___m_CyclePositionX_4;
	// System.Single UnityStandardAssets.Utility.CurveControlledBob::m_CyclePositionY
	float ___m_CyclePositionY_5;
	// System.Single UnityStandardAssets.Utility.CurveControlledBob::m_BobBaseInterval
	float ___m_BobBaseInterval_6;
	// UnityEngine.Vector3 UnityStandardAssets.Utility.CurveControlledBob::m_OriginalCameraPosition
	Vector3_t2903530434  ___m_OriginalCameraPosition_7;
	// System.Single UnityStandardAssets.Utility.CurveControlledBob::m_Time
	float ___m_Time_8;

public:
	inline static int32_t get_offset_of_HorizontalBobRange_0() { return static_cast<int32_t>(offsetof(CurveControlledBob_t3985701645, ___HorizontalBobRange_0)); }
	inline float get_HorizontalBobRange_0() const { return ___HorizontalBobRange_0; }
	inline float* get_address_of_HorizontalBobRange_0() { return &___HorizontalBobRange_0; }
	inline void set_HorizontalBobRange_0(float value)
	{
		___HorizontalBobRange_0 = value;
	}

	inline static int32_t get_offset_of_VerticalBobRange_1() { return static_cast<int32_t>(offsetof(CurveControlledBob_t3985701645, ___VerticalBobRange_1)); }
	inline float get_VerticalBobRange_1() const { return ___VerticalBobRange_1; }
	inline float* get_address_of_VerticalBobRange_1() { return &___VerticalBobRange_1; }
	inline void set_VerticalBobRange_1(float value)
	{
		___VerticalBobRange_1 = value;
	}

	inline static int32_t get_offset_of_Bobcurve_2() { return static_cast<int32_t>(offsetof(CurveControlledBob_t3985701645, ___Bobcurve_2)); }
	inline AnimationCurve_t2184836714 * get_Bobcurve_2() const { return ___Bobcurve_2; }
	inline AnimationCurve_t2184836714 ** get_address_of_Bobcurve_2() { return &___Bobcurve_2; }
	inline void set_Bobcurve_2(AnimationCurve_t2184836714 * value)
	{
		___Bobcurve_2 = value;
		Il2CppCodeGenWriteBarrier((&___Bobcurve_2), value);
	}

	inline static int32_t get_offset_of_VerticaltoHorizontalRatio_3() { return static_cast<int32_t>(offsetof(CurveControlledBob_t3985701645, ___VerticaltoHorizontalRatio_3)); }
	inline float get_VerticaltoHorizontalRatio_3() const { return ___VerticaltoHorizontalRatio_3; }
	inline float* get_address_of_VerticaltoHorizontalRatio_3() { return &___VerticaltoHorizontalRatio_3; }
	inline void set_VerticaltoHorizontalRatio_3(float value)
	{
		___VerticaltoHorizontalRatio_3 = value;
	}

	inline static int32_t get_offset_of_m_CyclePositionX_4() { return static_cast<int32_t>(offsetof(CurveControlledBob_t3985701645, ___m_CyclePositionX_4)); }
	inline float get_m_CyclePositionX_4() const { return ___m_CyclePositionX_4; }
	inline float* get_address_of_m_CyclePositionX_4() { return &___m_CyclePositionX_4; }
	inline void set_m_CyclePositionX_4(float value)
	{
		___m_CyclePositionX_4 = value;
	}

	inline static int32_t get_offset_of_m_CyclePositionY_5() { return static_cast<int32_t>(offsetof(CurveControlledBob_t3985701645, ___m_CyclePositionY_5)); }
	inline float get_m_CyclePositionY_5() const { return ___m_CyclePositionY_5; }
	inline float* get_address_of_m_CyclePositionY_5() { return &___m_CyclePositionY_5; }
	inline void set_m_CyclePositionY_5(float value)
	{
		___m_CyclePositionY_5 = value;
	}

	inline static int32_t get_offset_of_m_BobBaseInterval_6() { return static_cast<int32_t>(offsetof(CurveControlledBob_t3985701645, ___m_BobBaseInterval_6)); }
	inline float get_m_BobBaseInterval_6() const { return ___m_BobBaseInterval_6; }
	inline float* get_address_of_m_BobBaseInterval_6() { return &___m_BobBaseInterval_6; }
	inline void set_m_BobBaseInterval_6(float value)
	{
		___m_BobBaseInterval_6 = value;
	}

	inline static int32_t get_offset_of_m_OriginalCameraPosition_7() { return static_cast<int32_t>(offsetof(CurveControlledBob_t3985701645, ___m_OriginalCameraPosition_7)); }
	inline Vector3_t2903530434  get_m_OriginalCameraPosition_7() const { return ___m_OriginalCameraPosition_7; }
	inline Vector3_t2903530434 * get_address_of_m_OriginalCameraPosition_7() { return &___m_OriginalCameraPosition_7; }
	inline void set_m_OriginalCameraPosition_7(Vector3_t2903530434  value)
	{
		___m_OriginalCameraPosition_7 = value;
	}

	inline static int32_t get_offset_of_m_Time_8() { return static_cast<int32_t>(offsetof(CurveControlledBob_t3985701645, ___m_Time_8)); }
	inline float get_m_Time_8() const { return ___m_Time_8; }
	inline float* get_address_of_m_Time_8() { return &___m_Time_8; }
	inline void set_m_Time_8(float value)
	{
		___m_Time_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CURVECONTROLLEDBOB_T3985701645_H
#ifndef CAMERAREFOCUS_T1375739319_H
#define CAMERAREFOCUS_T1375739319_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.CameraRefocus
struct  CameraRefocus_t1375739319  : public RuntimeObject
{
public:
	// UnityEngine.Camera UnityStandardAssets.Utility.CameraRefocus::Camera
	Camera_t3175186167 * ___Camera_0;
	// UnityEngine.Vector3 UnityStandardAssets.Utility.CameraRefocus::Lookatpoint
	Vector3_t2903530434  ___Lookatpoint_1;
	// UnityEngine.Transform UnityStandardAssets.Utility.CameraRefocus::Parent
	Transform_t3316442598 * ___Parent_2;
	// UnityEngine.Vector3 UnityStandardAssets.Utility.CameraRefocus::m_OrigCameraPos
	Vector3_t2903530434  ___m_OrigCameraPos_3;
	// System.Boolean UnityStandardAssets.Utility.CameraRefocus::m_Refocus
	bool ___m_Refocus_4;

public:
	inline static int32_t get_offset_of_Camera_0() { return static_cast<int32_t>(offsetof(CameraRefocus_t1375739319, ___Camera_0)); }
	inline Camera_t3175186167 * get_Camera_0() const { return ___Camera_0; }
	inline Camera_t3175186167 ** get_address_of_Camera_0() { return &___Camera_0; }
	inline void set_Camera_0(Camera_t3175186167 * value)
	{
		___Camera_0 = value;
		Il2CppCodeGenWriteBarrier((&___Camera_0), value);
	}

	inline static int32_t get_offset_of_Lookatpoint_1() { return static_cast<int32_t>(offsetof(CameraRefocus_t1375739319, ___Lookatpoint_1)); }
	inline Vector3_t2903530434  get_Lookatpoint_1() const { return ___Lookatpoint_1; }
	inline Vector3_t2903530434 * get_address_of_Lookatpoint_1() { return &___Lookatpoint_1; }
	inline void set_Lookatpoint_1(Vector3_t2903530434  value)
	{
		___Lookatpoint_1 = value;
	}

	inline static int32_t get_offset_of_Parent_2() { return static_cast<int32_t>(offsetof(CameraRefocus_t1375739319, ___Parent_2)); }
	inline Transform_t3316442598 * get_Parent_2() const { return ___Parent_2; }
	inline Transform_t3316442598 ** get_address_of_Parent_2() { return &___Parent_2; }
	inline void set_Parent_2(Transform_t3316442598 * value)
	{
		___Parent_2 = value;
		Il2CppCodeGenWriteBarrier((&___Parent_2), value);
	}

	inline static int32_t get_offset_of_m_OrigCameraPos_3() { return static_cast<int32_t>(offsetof(CameraRefocus_t1375739319, ___m_OrigCameraPos_3)); }
	inline Vector3_t2903530434  get_m_OrigCameraPos_3() const { return ___m_OrigCameraPos_3; }
	inline Vector3_t2903530434 * get_address_of_m_OrigCameraPos_3() { return &___m_OrigCameraPos_3; }
	inline void set_m_OrigCameraPos_3(Vector3_t2903530434  value)
	{
		___m_OrigCameraPos_3 = value;
	}

	inline static int32_t get_offset_of_m_Refocus_4() { return static_cast<int32_t>(offsetof(CameraRefocus_t1375739319, ___m_Refocus_4)); }
	inline bool get_m_Refocus_4() const { return ___m_Refocus_4; }
	inline bool* get_address_of_m_Refocus_4() { return &___m_Refocus_4; }
	inline void set_m_Refocus_4(bool value)
	{
		___m_Refocus_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAMERAREFOCUS_T1375739319_H
#ifndef MODE_T1276594512_H
#define MODE_T1276594512_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.ActivateTrigger/Mode
struct  Mode_t1276594512 
{
public:
	// System.Int32 UnityStandardAssets.Utility.ActivateTrigger/Mode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Mode_t1276594512, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MODE_T1276594512_H
#ifndef AXISOPTIONS_T836301849_H
#define AXISOPTIONS_T836301849_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.CrossPlatformInput.TiltInput/AxisOptions
struct  AxisOptions_t836301849 
{
public:
	// System.Int32 UnityStandardAssets.CrossPlatformInput.TiltInput/AxisOptions::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(AxisOptions_t836301849, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AXISOPTIONS_T836301849_H
#ifndef WORDPREFABCREATIONMODE_T2868429441_H
#define WORDPREFABCREATIONMODE_T2868429441_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.WordPrefabCreationMode
struct  WordPrefabCreationMode_t2868429441 
{
public:
	// System.Int32 Vuforia.WordPrefabCreationMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(WordPrefabCreationMode_t2868429441, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WORDPREFABCREATIONMODE_T2868429441_H
#ifndef STATUS_T1358118869_H
#define STATUS_T1358118869_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.TrackableBehaviour/Status
struct  Status_t1358118869 
{
public:
	// System.Int32 Vuforia.TrackableBehaviour/Status::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Status_t1358118869, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STATUS_T1358118869_H
#ifndef MAPPINGTYPE_T502031566_H
#define MAPPINGTYPE_T502031566_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.CrossPlatformInput.TiltInput/AxisMapping/MappingType
struct  MappingType_t502031566 
{
public:
	// System.Int32 UnityStandardAssets.CrossPlatformInput.TiltInput/AxisMapping/MappingType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(MappingType_t502031566, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MAPPINGTYPE_T502031566_H
#ifndef WORDFILTERMODE_T2466111678_H
#define WORDFILTERMODE_T2466111678_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.WordFilterMode
struct  WordFilterMode_t2466111678 
{
public:
	// System.Int32 Vuforia.WordFilterMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(WordFilterMode_t2466111678, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WORDFILTERMODE_T2466111678_H
#ifndef AXISMAPPING_T2403352015_H
#define AXISMAPPING_T2403352015_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.CrossPlatformInput.TiltInput/AxisMapping
struct  AxisMapping_t2403352015  : public RuntimeObject
{
public:
	// UnityStandardAssets.CrossPlatformInput.TiltInput/AxisMapping/MappingType UnityStandardAssets.CrossPlatformInput.TiltInput/AxisMapping::type
	int32_t ___type_0;
	// System.String UnityStandardAssets.CrossPlatformInput.TiltInput/AxisMapping::axisName
	String_t* ___axisName_1;

public:
	inline static int32_t get_offset_of_type_0() { return static_cast<int32_t>(offsetof(AxisMapping_t2403352015, ___type_0)); }
	inline int32_t get_type_0() const { return ___type_0; }
	inline int32_t* get_address_of_type_0() { return &___type_0; }
	inline void set_type_0(int32_t value)
	{
		___type_0 = value;
	}

	inline static int32_t get_offset_of_axisName_1() { return static_cast<int32_t>(offsetof(AxisMapping_t2403352015, ___axisName_1)); }
	inline String_t* get_axisName_1() const { return ___axisName_1; }
	inline String_t** get_address_of_axisName_1() { return &___axisName_1; }
	inline void set_axisName_1(String_t* value)
	{
		___axisName_1 = value;
		Il2CppCodeGenWriteBarrier((&___axisName_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AXISMAPPING_T2403352015_H
#ifndef COMPONENT_T4087199522_H
#define COMPONENT_T4087199522_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t4087199522  : public Object_t1502412432
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T4087199522_H
#ifndef ENTRY_T3279652951_H
#define ENTRY_T3279652951_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.TimedObjectActivator/Entry
struct  Entry_t3279652951  : public RuntimeObject
{
public:
	// UnityEngine.GameObject UnityStandardAssets.Utility.TimedObjectActivator/Entry::target
	GameObject_t1811656094 * ___target_0;
	// UnityStandardAssets.Utility.TimedObjectActivator/Action UnityStandardAssets.Utility.TimedObjectActivator/Entry::action
	int32_t ___action_1;
	// System.Single UnityStandardAssets.Utility.TimedObjectActivator/Entry::delay
	float ___delay_2;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(Entry_t3279652951, ___target_0)); }
	inline GameObject_t1811656094 * get_target_0() const { return ___target_0; }
	inline GameObject_t1811656094 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(GameObject_t1811656094 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}

	inline static int32_t get_offset_of_action_1() { return static_cast<int32_t>(offsetof(Entry_t3279652951, ___action_1)); }
	inline int32_t get_action_1() const { return ___action_1; }
	inline int32_t* get_address_of_action_1() { return &___action_1; }
	inline void set_action_1(int32_t value)
	{
		___action_1 = value;
	}

	inline static int32_t get_offset_of_delay_2() { return static_cast<int32_t>(offsetof(Entry_t3279652951, ___delay_2)); }
	inline float get_delay_2() const { return ___delay_2; }
	inline float* get_address_of_delay_2() { return &___delay_2; }
	inline void set_delay_2(float value)
	{
		___delay_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENTRY_T3279652951_H
#ifndef MOBILEINPUT_T3557021844_H
#define MOBILEINPUT_T3557021844_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.CrossPlatformInput.PlatformSpecific.MobileInput
struct  MobileInput_t3557021844  : public VirtualInput_t2381112337
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MOBILEINPUT_T3557021844_H
#ifndef ANDROIDUNITYPLAYER_T3571566439_H
#define ANDROIDUNITYPLAYER_T3571566439_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.AndroidUnityPlayer
struct  AndroidUnityPlayer_t3571566439  : public RuntimeObject
{
public:
	// UnityEngine.ScreenOrientation Vuforia.AndroidUnityPlayer::mScreenOrientation
	int32_t ___mScreenOrientation_2;
	// UnityEngine.ScreenOrientation Vuforia.AndroidUnityPlayer::mJavaScreenOrientation
	int32_t ___mJavaScreenOrientation_3;
	// System.Int32 Vuforia.AndroidUnityPlayer::mFramesSinceLastOrientationReset
	int32_t ___mFramesSinceLastOrientationReset_4;
	// System.Int32 Vuforia.AndroidUnityPlayer::mFramesSinceLastJavaOrientationCheck
	int32_t ___mFramesSinceLastJavaOrientationCheck_5;

public:
	inline static int32_t get_offset_of_mScreenOrientation_2() { return static_cast<int32_t>(offsetof(AndroidUnityPlayer_t3571566439, ___mScreenOrientation_2)); }
	inline int32_t get_mScreenOrientation_2() const { return ___mScreenOrientation_2; }
	inline int32_t* get_address_of_mScreenOrientation_2() { return &___mScreenOrientation_2; }
	inline void set_mScreenOrientation_2(int32_t value)
	{
		___mScreenOrientation_2 = value;
	}

	inline static int32_t get_offset_of_mJavaScreenOrientation_3() { return static_cast<int32_t>(offsetof(AndroidUnityPlayer_t3571566439, ___mJavaScreenOrientation_3)); }
	inline int32_t get_mJavaScreenOrientation_3() const { return ___mJavaScreenOrientation_3; }
	inline int32_t* get_address_of_mJavaScreenOrientation_3() { return &___mJavaScreenOrientation_3; }
	inline void set_mJavaScreenOrientation_3(int32_t value)
	{
		___mJavaScreenOrientation_3 = value;
	}

	inline static int32_t get_offset_of_mFramesSinceLastOrientationReset_4() { return static_cast<int32_t>(offsetof(AndroidUnityPlayer_t3571566439, ___mFramesSinceLastOrientationReset_4)); }
	inline int32_t get_mFramesSinceLastOrientationReset_4() const { return ___mFramesSinceLastOrientationReset_4; }
	inline int32_t* get_address_of_mFramesSinceLastOrientationReset_4() { return &___mFramesSinceLastOrientationReset_4; }
	inline void set_mFramesSinceLastOrientationReset_4(int32_t value)
	{
		___mFramesSinceLastOrientationReset_4 = value;
	}

	inline static int32_t get_offset_of_mFramesSinceLastJavaOrientationCheck_5() { return static_cast<int32_t>(offsetof(AndroidUnityPlayer_t3571566439, ___mFramesSinceLastJavaOrientationCheck_5)); }
	inline int32_t get_mFramesSinceLastJavaOrientationCheck_5() const { return ___mFramesSinceLastJavaOrientationCheck_5; }
	inline int32_t* get_address_of_mFramesSinceLastJavaOrientationCheck_5() { return &___mFramesSinceLastJavaOrientationCheck_5; }
	inline void set_mFramesSinceLastJavaOrientationCheck_5(int32_t value)
	{
		___mFramesSinceLastJavaOrientationCheck_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANDROIDUNITYPLAYER_T3571566439_H
#ifndef MOVEMENTSETTINGS_T2494390400_H
#define MOVEMENTSETTINGS_T2494390400_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Characters.FirstPerson.RigidbodyFirstPersonController/MovementSettings
struct  MovementSettings_t2494390400  : public RuntimeObject
{
public:
	// System.Single UnityStandardAssets.Characters.FirstPerson.RigidbodyFirstPersonController/MovementSettings::ForwardSpeed
	float ___ForwardSpeed_0;
	// System.Single UnityStandardAssets.Characters.FirstPerson.RigidbodyFirstPersonController/MovementSettings::BackwardSpeed
	float ___BackwardSpeed_1;
	// System.Single UnityStandardAssets.Characters.FirstPerson.RigidbodyFirstPersonController/MovementSettings::StrafeSpeed
	float ___StrafeSpeed_2;
	// System.Single UnityStandardAssets.Characters.FirstPerson.RigidbodyFirstPersonController/MovementSettings::RunMultiplier
	float ___RunMultiplier_3;
	// UnityEngine.KeyCode UnityStandardAssets.Characters.FirstPerson.RigidbodyFirstPersonController/MovementSettings::RunKey
	int32_t ___RunKey_4;
	// System.Single UnityStandardAssets.Characters.FirstPerson.RigidbodyFirstPersonController/MovementSettings::JumpForce
	float ___JumpForce_5;
	// UnityEngine.AnimationCurve UnityStandardAssets.Characters.FirstPerson.RigidbodyFirstPersonController/MovementSettings::SlopeCurveModifier
	AnimationCurve_t2184836714 * ___SlopeCurveModifier_6;
	// System.Single UnityStandardAssets.Characters.FirstPerson.RigidbodyFirstPersonController/MovementSettings::CurrentTargetSpeed
	float ___CurrentTargetSpeed_7;

public:
	inline static int32_t get_offset_of_ForwardSpeed_0() { return static_cast<int32_t>(offsetof(MovementSettings_t2494390400, ___ForwardSpeed_0)); }
	inline float get_ForwardSpeed_0() const { return ___ForwardSpeed_0; }
	inline float* get_address_of_ForwardSpeed_0() { return &___ForwardSpeed_0; }
	inline void set_ForwardSpeed_0(float value)
	{
		___ForwardSpeed_0 = value;
	}

	inline static int32_t get_offset_of_BackwardSpeed_1() { return static_cast<int32_t>(offsetof(MovementSettings_t2494390400, ___BackwardSpeed_1)); }
	inline float get_BackwardSpeed_1() const { return ___BackwardSpeed_1; }
	inline float* get_address_of_BackwardSpeed_1() { return &___BackwardSpeed_1; }
	inline void set_BackwardSpeed_1(float value)
	{
		___BackwardSpeed_1 = value;
	}

	inline static int32_t get_offset_of_StrafeSpeed_2() { return static_cast<int32_t>(offsetof(MovementSettings_t2494390400, ___StrafeSpeed_2)); }
	inline float get_StrafeSpeed_2() const { return ___StrafeSpeed_2; }
	inline float* get_address_of_StrafeSpeed_2() { return &___StrafeSpeed_2; }
	inline void set_StrafeSpeed_2(float value)
	{
		___StrafeSpeed_2 = value;
	}

	inline static int32_t get_offset_of_RunMultiplier_3() { return static_cast<int32_t>(offsetof(MovementSettings_t2494390400, ___RunMultiplier_3)); }
	inline float get_RunMultiplier_3() const { return ___RunMultiplier_3; }
	inline float* get_address_of_RunMultiplier_3() { return &___RunMultiplier_3; }
	inline void set_RunMultiplier_3(float value)
	{
		___RunMultiplier_3 = value;
	}

	inline static int32_t get_offset_of_RunKey_4() { return static_cast<int32_t>(offsetof(MovementSettings_t2494390400, ___RunKey_4)); }
	inline int32_t get_RunKey_4() const { return ___RunKey_4; }
	inline int32_t* get_address_of_RunKey_4() { return &___RunKey_4; }
	inline void set_RunKey_4(int32_t value)
	{
		___RunKey_4 = value;
	}

	inline static int32_t get_offset_of_JumpForce_5() { return static_cast<int32_t>(offsetof(MovementSettings_t2494390400, ___JumpForce_5)); }
	inline float get_JumpForce_5() const { return ___JumpForce_5; }
	inline float* get_address_of_JumpForce_5() { return &___JumpForce_5; }
	inline void set_JumpForce_5(float value)
	{
		___JumpForce_5 = value;
	}

	inline static int32_t get_offset_of_SlopeCurveModifier_6() { return static_cast<int32_t>(offsetof(MovementSettings_t2494390400, ___SlopeCurveModifier_6)); }
	inline AnimationCurve_t2184836714 * get_SlopeCurveModifier_6() const { return ___SlopeCurveModifier_6; }
	inline AnimationCurve_t2184836714 ** get_address_of_SlopeCurveModifier_6() { return &___SlopeCurveModifier_6; }
	inline void set_SlopeCurveModifier_6(AnimationCurve_t2184836714 * value)
	{
		___SlopeCurveModifier_6 = value;
		Il2CppCodeGenWriteBarrier((&___SlopeCurveModifier_6), value);
	}

	inline static int32_t get_offset_of_CurrentTargetSpeed_7() { return static_cast<int32_t>(offsetof(MovementSettings_t2494390400, ___CurrentTargetSpeed_7)); }
	inline float get_CurrentTargetSpeed_7() const { return ___CurrentTargetSpeed_7; }
	inline float* get_address_of_CurrentTargetSpeed_7() { return &___CurrentTargetSpeed_7; }
	inline void set_CurrentTargetSpeed_7(float value)
	{
		___CurrentTargetSpeed_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MOVEMENTSETTINGS_T2494390400_H
#ifndef VECTOR3ANDSPACE_T2340595124_H
#define VECTOR3ANDSPACE_T2340595124_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.AutoMoveAndRotate/Vector3andSpace
struct  Vector3andSpace_t2340595124  : public RuntimeObject
{
public:
	// UnityEngine.Vector3 UnityStandardAssets.Utility.AutoMoveAndRotate/Vector3andSpace::value
	Vector3_t2903530434  ___value_0;
	// UnityEngine.Space UnityStandardAssets.Utility.AutoMoveAndRotate/Vector3andSpace::space
	int32_t ___space_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Vector3andSpace_t2340595124, ___value_0)); }
	inline Vector3_t2903530434  get_value_0() const { return ___value_0; }
	inline Vector3_t2903530434 * get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(Vector3_t2903530434  value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_space_1() { return static_cast<int32_t>(offsetof(Vector3andSpace_t2340595124, ___space_1)); }
	inline int32_t get_space_1() const { return ___space_1; }
	inline int32_t* get_address_of_space_1() { return &___space_1; }
	inline void set_space_1(int32_t value)
	{
		___space_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3ANDSPACE_T2340595124_H
#ifndef WSAUNITYPLAYER_T1576425251_H
#define WSAUNITYPLAYER_T1576425251_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.WSAUnityPlayer
struct  WSAUnityPlayer_t1576425251  : public RuntimeObject
{
public:
	// UnityEngine.ScreenOrientation Vuforia.WSAUnityPlayer::mScreenOrientation
	int32_t ___mScreenOrientation_0;

public:
	inline static int32_t get_offset_of_mScreenOrientation_0() { return static_cast<int32_t>(offsetof(WSAUnityPlayer_t1576425251, ___mScreenOrientation_0)); }
	inline int32_t get_mScreenOrientation_0() const { return ___mScreenOrientation_0; }
	inline int32_t* get_address_of_mScreenOrientation_0() { return &___mScreenOrientation_0; }
	inline void set_mScreenOrientation_0(int32_t value)
	{
		___mScreenOrientation_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WSAUNITYPLAYER_T1576425251_H
#ifndef IOSUNITYPLAYER_T1005570149_H
#define IOSUNITYPLAYER_T1005570149_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.IOSUnityPlayer
struct  IOSUnityPlayer_t1005570149  : public RuntimeObject
{
public:
	// UnityEngine.ScreenOrientation Vuforia.IOSUnityPlayer::mScreenOrientation
	int32_t ___mScreenOrientation_0;

public:
	inline static int32_t get_offset_of_mScreenOrientation_0() { return static_cast<int32_t>(offsetof(IOSUnityPlayer_t1005570149, ___mScreenOrientation_0)); }
	inline int32_t get_mScreenOrientation_0() const { return ___mScreenOrientation_0; }
	inline int32_t* get_address_of_mScreenOrientation_0() { return &___mScreenOrientation_0; }
	inline void set_mScreenOrientation_0(int32_t value)
	{
		___mScreenOrientation_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IOSUNITYPLAYER_T1005570149_H
#ifndef U3CDRAGOBJECTU3EC__ITERATOR0_T2945102041_H
#define U3CDRAGOBJECTU3EC__ITERATOR0_T2945102041_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.DragRigidbody/<DragObject>c__Iterator0
struct  U3CDragObjectU3Ec__Iterator0_t2945102041  : public RuntimeObject
{
public:
	// System.Single UnityStandardAssets.Utility.DragRigidbody/<DragObject>c__Iterator0::<oldDrag>__0
	float ___U3ColdDragU3E__0_0;
	// System.Single UnityStandardAssets.Utility.DragRigidbody/<DragObject>c__Iterator0::<oldAngularDrag>__0
	float ___U3ColdAngularDragU3E__0_1;
	// UnityEngine.Camera UnityStandardAssets.Utility.DragRigidbody/<DragObject>c__Iterator0::<mainCamera>__0
	Camera_t3175186167 * ___U3CmainCameraU3E__0_2;
	// UnityEngine.Ray UnityStandardAssets.Utility.DragRigidbody/<DragObject>c__Iterator0::<ray>__1
	Ray_t3821377119  ___U3CrayU3E__1_3;
	// System.Single UnityStandardAssets.Utility.DragRigidbody/<DragObject>c__Iterator0::distance
	float ___distance_4;
	// UnityStandardAssets.Utility.DragRigidbody UnityStandardAssets.Utility.DragRigidbody/<DragObject>c__Iterator0::$this
	DragRigidbody_t781943330 * ___U24this_5;
	// System.Object UnityStandardAssets.Utility.DragRigidbody/<DragObject>c__Iterator0::$current
	RuntimeObject * ___U24current_6;
	// System.Boolean UnityStandardAssets.Utility.DragRigidbody/<DragObject>c__Iterator0::$disposing
	bool ___U24disposing_7;
	// System.Int32 UnityStandardAssets.Utility.DragRigidbody/<DragObject>c__Iterator0::$PC
	int32_t ___U24PC_8;

public:
	inline static int32_t get_offset_of_U3ColdDragU3E__0_0() { return static_cast<int32_t>(offsetof(U3CDragObjectU3Ec__Iterator0_t2945102041, ___U3ColdDragU3E__0_0)); }
	inline float get_U3ColdDragU3E__0_0() const { return ___U3ColdDragU3E__0_0; }
	inline float* get_address_of_U3ColdDragU3E__0_0() { return &___U3ColdDragU3E__0_0; }
	inline void set_U3ColdDragU3E__0_0(float value)
	{
		___U3ColdDragU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_U3ColdAngularDragU3E__0_1() { return static_cast<int32_t>(offsetof(U3CDragObjectU3Ec__Iterator0_t2945102041, ___U3ColdAngularDragU3E__0_1)); }
	inline float get_U3ColdAngularDragU3E__0_1() const { return ___U3ColdAngularDragU3E__0_1; }
	inline float* get_address_of_U3ColdAngularDragU3E__0_1() { return &___U3ColdAngularDragU3E__0_1; }
	inline void set_U3ColdAngularDragU3E__0_1(float value)
	{
		___U3ColdAngularDragU3E__0_1 = value;
	}

	inline static int32_t get_offset_of_U3CmainCameraU3E__0_2() { return static_cast<int32_t>(offsetof(U3CDragObjectU3Ec__Iterator0_t2945102041, ___U3CmainCameraU3E__0_2)); }
	inline Camera_t3175186167 * get_U3CmainCameraU3E__0_2() const { return ___U3CmainCameraU3E__0_2; }
	inline Camera_t3175186167 ** get_address_of_U3CmainCameraU3E__0_2() { return &___U3CmainCameraU3E__0_2; }
	inline void set_U3CmainCameraU3E__0_2(Camera_t3175186167 * value)
	{
		___U3CmainCameraU3E__0_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CmainCameraU3E__0_2), value);
	}

	inline static int32_t get_offset_of_U3CrayU3E__1_3() { return static_cast<int32_t>(offsetof(U3CDragObjectU3Ec__Iterator0_t2945102041, ___U3CrayU3E__1_3)); }
	inline Ray_t3821377119  get_U3CrayU3E__1_3() const { return ___U3CrayU3E__1_3; }
	inline Ray_t3821377119 * get_address_of_U3CrayU3E__1_3() { return &___U3CrayU3E__1_3; }
	inline void set_U3CrayU3E__1_3(Ray_t3821377119  value)
	{
		___U3CrayU3E__1_3 = value;
	}

	inline static int32_t get_offset_of_distance_4() { return static_cast<int32_t>(offsetof(U3CDragObjectU3Ec__Iterator0_t2945102041, ___distance_4)); }
	inline float get_distance_4() const { return ___distance_4; }
	inline float* get_address_of_distance_4() { return &___distance_4; }
	inline void set_distance_4(float value)
	{
		___distance_4 = value;
	}

	inline static int32_t get_offset_of_U24this_5() { return static_cast<int32_t>(offsetof(U3CDragObjectU3Ec__Iterator0_t2945102041, ___U24this_5)); }
	inline DragRigidbody_t781943330 * get_U24this_5() const { return ___U24this_5; }
	inline DragRigidbody_t781943330 ** get_address_of_U24this_5() { return &___U24this_5; }
	inline void set_U24this_5(DragRigidbody_t781943330 * value)
	{
		___U24this_5 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_5), value);
	}

	inline static int32_t get_offset_of_U24current_6() { return static_cast<int32_t>(offsetof(U3CDragObjectU3Ec__Iterator0_t2945102041, ___U24current_6)); }
	inline RuntimeObject * get_U24current_6() const { return ___U24current_6; }
	inline RuntimeObject ** get_address_of_U24current_6() { return &___U24current_6; }
	inline void set_U24current_6(RuntimeObject * value)
	{
		___U24current_6 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_6), value);
	}

	inline static int32_t get_offset_of_U24disposing_7() { return static_cast<int32_t>(offsetof(U3CDragObjectU3Ec__Iterator0_t2945102041, ___U24disposing_7)); }
	inline bool get_U24disposing_7() const { return ___U24disposing_7; }
	inline bool* get_address_of_U24disposing_7() { return &___U24disposing_7; }
	inline void set_U24disposing_7(bool value)
	{
		___U24disposing_7 = value;
	}

	inline static int32_t get_offset_of_U24PC_8() { return static_cast<int32_t>(offsetof(U3CDragObjectU3Ec__Iterator0_t2945102041, ___U24PC_8)); }
	inline int32_t get_U24PC_8() const { return ___U24PC_8; }
	inline int32_t* get_address_of_U24PC_8() { return &___U24PC_8; }
	inline void set_U24PC_8(int32_t value)
	{
		___U24PC_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CDRAGOBJECTU3EC__ITERATOR0_T2945102041_H
#ifndef STANDALONEINPUT_T2753768079_H
#define STANDALONEINPUT_T2753768079_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.CrossPlatformInput.PlatformSpecific.StandaloneInput
struct  StandaloneInput_t2753768079  : public VirtualInput_t2381112337
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STANDALONEINPUT_T2753768079_H
#ifndef BEHAVIOUR_T363748010_H
#define BEHAVIOUR_T363748010_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_t363748010  : public Component_t4087199522
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_T363748010_H
#ifndef MONOBEHAVIOUR_T345688271_H
#define MONOBEHAVIOUR_T345688271_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t345688271  : public Behaviour_t363748010
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T345688271_H
#ifndef COMPONENTFACTORYSTARTERBEHAVIOUR_T2906678938_H
#define COMPONENTFACTORYSTARTERBEHAVIOUR_T2906678938_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.ComponentFactoryStarterBehaviour
struct  ComponentFactoryStarterBehaviour_t2906678938  : public MonoBehaviour_t345688271
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENTFACTORYSTARTERBEHAVIOUR_T2906678938_H
#ifndef RECONSTRUCTIONFROMTARGETABSTRACTBEHAVIOUR_T2676804737_H
#define RECONSTRUCTIONFROMTARGETABSTRACTBEHAVIOUR_T2676804737_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.ReconstructionFromTargetAbstractBehaviour
struct  ReconstructionFromTargetAbstractBehaviour_t2676804737  : public MonoBehaviour_t345688271
{
public:
	// Vuforia.ReconstructionFromTarget Vuforia.ReconstructionFromTargetAbstractBehaviour::mReconstructionFromTarget
	RuntimeObject* ___mReconstructionFromTarget_2;
	// Vuforia.ReconstructionAbstractBehaviour Vuforia.ReconstructionFromTargetAbstractBehaviour::mReconstructionBehaviour
	ReconstructionAbstractBehaviour_t954468633 * ___mReconstructionBehaviour_3;

public:
	inline static int32_t get_offset_of_mReconstructionFromTarget_2() { return static_cast<int32_t>(offsetof(ReconstructionFromTargetAbstractBehaviour_t2676804737, ___mReconstructionFromTarget_2)); }
	inline RuntimeObject* get_mReconstructionFromTarget_2() const { return ___mReconstructionFromTarget_2; }
	inline RuntimeObject** get_address_of_mReconstructionFromTarget_2() { return &___mReconstructionFromTarget_2; }
	inline void set_mReconstructionFromTarget_2(RuntimeObject* value)
	{
		___mReconstructionFromTarget_2 = value;
		Il2CppCodeGenWriteBarrier((&___mReconstructionFromTarget_2), value);
	}

	inline static int32_t get_offset_of_mReconstructionBehaviour_3() { return static_cast<int32_t>(offsetof(ReconstructionFromTargetAbstractBehaviour_t2676804737, ___mReconstructionBehaviour_3)); }
	inline ReconstructionAbstractBehaviour_t954468633 * get_mReconstructionBehaviour_3() const { return ___mReconstructionBehaviour_3; }
	inline ReconstructionAbstractBehaviour_t954468633 ** get_address_of_mReconstructionBehaviour_3() { return &___mReconstructionBehaviour_3; }
	inline void set_mReconstructionBehaviour_3(ReconstructionAbstractBehaviour_t954468633 * value)
	{
		___mReconstructionBehaviour_3 = value;
		Il2CppCodeGenWriteBarrier((&___mReconstructionBehaviour_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RECONSTRUCTIONFROMTARGETABSTRACTBEHAVIOUR_T2676804737_H
#ifndef FIRSTPERSONCONTROLLER_T2645666525_H
#define FIRSTPERSONCONTROLLER_T2645666525_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Characters.FirstPerson.FirstPersonController
struct  FirstPersonController_t2645666525  : public MonoBehaviour_t345688271
{
public:
	// System.Boolean UnityStandardAssets.Characters.FirstPerson.FirstPersonController::m_IsWalking
	bool ___m_IsWalking_2;
	// System.Single UnityStandardAssets.Characters.FirstPerson.FirstPersonController::m_WalkSpeed
	float ___m_WalkSpeed_3;
	// System.Single UnityStandardAssets.Characters.FirstPerson.FirstPersonController::m_RunSpeed
	float ___m_RunSpeed_4;
	// System.Single UnityStandardAssets.Characters.FirstPerson.FirstPersonController::m_RunstepLenghten
	float ___m_RunstepLenghten_5;
	// System.Single UnityStandardAssets.Characters.FirstPerson.FirstPersonController::m_JumpSpeed
	float ___m_JumpSpeed_6;
	// System.Single UnityStandardAssets.Characters.FirstPerson.FirstPersonController::m_StickToGroundForce
	float ___m_StickToGroundForce_7;
	// System.Single UnityStandardAssets.Characters.FirstPerson.FirstPersonController::m_GravityMultiplier
	float ___m_GravityMultiplier_8;
	// UnityStandardAssets.Characters.FirstPerson.MouseLook UnityStandardAssets.Characters.FirstPerson.FirstPersonController::m_MouseLook
	MouseLook_t3694953670 * ___m_MouseLook_9;
	// System.Boolean UnityStandardAssets.Characters.FirstPerson.FirstPersonController::m_UseFovKick
	bool ___m_UseFovKick_10;
	// UnityStandardAssets.Utility.FOVKick UnityStandardAssets.Characters.FirstPerson.FirstPersonController::m_FovKick
	FOVKick_t1285422362 * ___m_FovKick_11;
	// System.Boolean UnityStandardAssets.Characters.FirstPerson.FirstPersonController::m_UseHeadBob
	bool ___m_UseHeadBob_12;
	// UnityStandardAssets.Utility.CurveControlledBob UnityStandardAssets.Characters.FirstPerson.FirstPersonController::m_HeadBob
	CurveControlledBob_t3985701645 * ___m_HeadBob_13;
	// UnityStandardAssets.Utility.LerpControlledBob UnityStandardAssets.Characters.FirstPerson.FirstPersonController::m_JumpBob
	LerpControlledBob_t4065608754 * ___m_JumpBob_14;
	// System.Single UnityStandardAssets.Characters.FirstPerson.FirstPersonController::m_StepInterval
	float ___m_StepInterval_15;
	// UnityEngine.AudioClip[] UnityStandardAssets.Characters.FirstPerson.FirstPersonController::m_FootstepSounds
	AudioClipU5BU5D_t3571400391* ___m_FootstepSounds_16;
	// UnityEngine.AudioClip UnityStandardAssets.Characters.FirstPerson.FirstPersonController::m_JumpSound
	AudioClip_t474304562 * ___m_JumpSound_17;
	// UnityEngine.AudioClip UnityStandardAssets.Characters.FirstPerson.FirstPersonController::m_LandSound
	AudioClip_t474304562 * ___m_LandSound_18;
	// UnityEngine.Camera UnityStandardAssets.Characters.FirstPerson.FirstPersonController::m_Camera
	Camera_t3175186167 * ___m_Camera_19;
	// System.Boolean UnityStandardAssets.Characters.FirstPerson.FirstPersonController::m_Jump
	bool ___m_Jump_20;
	// System.Single UnityStandardAssets.Characters.FirstPerson.FirstPersonController::m_YRotation
	float ___m_YRotation_21;
	// UnityEngine.Vector2 UnityStandardAssets.Characters.FirstPerson.FirstPersonController::m_Input
	Vector2_t3577333262  ___m_Input_22;
	// UnityEngine.Vector3 UnityStandardAssets.Characters.FirstPerson.FirstPersonController::m_MoveDir
	Vector3_t2903530434  ___m_MoveDir_23;
	// UnityEngine.CharacterController UnityStandardAssets.Characters.FirstPerson.FirstPersonController::m_CharacterController
	CharacterController_t2498358276 * ___m_CharacterController_24;
	// UnityEngine.CollisionFlags UnityStandardAssets.Characters.FirstPerson.FirstPersonController::m_CollisionFlags
	int32_t ___m_CollisionFlags_25;
	// System.Boolean UnityStandardAssets.Characters.FirstPerson.FirstPersonController::m_PreviouslyGrounded
	bool ___m_PreviouslyGrounded_26;
	// UnityEngine.Vector3 UnityStandardAssets.Characters.FirstPerson.FirstPersonController::m_OriginalCameraPosition
	Vector3_t2903530434  ___m_OriginalCameraPosition_27;
	// System.Single UnityStandardAssets.Characters.FirstPerson.FirstPersonController::m_StepCycle
	float ___m_StepCycle_28;
	// System.Single UnityStandardAssets.Characters.FirstPerson.FirstPersonController::m_NextStep
	float ___m_NextStep_29;
	// System.Boolean UnityStandardAssets.Characters.FirstPerson.FirstPersonController::m_Jumping
	bool ___m_Jumping_30;
	// UnityEngine.AudioSource UnityStandardAssets.Characters.FirstPerson.FirstPersonController::m_AudioSource
	AudioSource_t1862618521 * ___m_AudioSource_31;

public:
	inline static int32_t get_offset_of_m_IsWalking_2() { return static_cast<int32_t>(offsetof(FirstPersonController_t2645666525, ___m_IsWalking_2)); }
	inline bool get_m_IsWalking_2() const { return ___m_IsWalking_2; }
	inline bool* get_address_of_m_IsWalking_2() { return &___m_IsWalking_2; }
	inline void set_m_IsWalking_2(bool value)
	{
		___m_IsWalking_2 = value;
	}

	inline static int32_t get_offset_of_m_WalkSpeed_3() { return static_cast<int32_t>(offsetof(FirstPersonController_t2645666525, ___m_WalkSpeed_3)); }
	inline float get_m_WalkSpeed_3() const { return ___m_WalkSpeed_3; }
	inline float* get_address_of_m_WalkSpeed_3() { return &___m_WalkSpeed_3; }
	inline void set_m_WalkSpeed_3(float value)
	{
		___m_WalkSpeed_3 = value;
	}

	inline static int32_t get_offset_of_m_RunSpeed_4() { return static_cast<int32_t>(offsetof(FirstPersonController_t2645666525, ___m_RunSpeed_4)); }
	inline float get_m_RunSpeed_4() const { return ___m_RunSpeed_4; }
	inline float* get_address_of_m_RunSpeed_4() { return &___m_RunSpeed_4; }
	inline void set_m_RunSpeed_4(float value)
	{
		___m_RunSpeed_4 = value;
	}

	inline static int32_t get_offset_of_m_RunstepLenghten_5() { return static_cast<int32_t>(offsetof(FirstPersonController_t2645666525, ___m_RunstepLenghten_5)); }
	inline float get_m_RunstepLenghten_5() const { return ___m_RunstepLenghten_5; }
	inline float* get_address_of_m_RunstepLenghten_5() { return &___m_RunstepLenghten_5; }
	inline void set_m_RunstepLenghten_5(float value)
	{
		___m_RunstepLenghten_5 = value;
	}

	inline static int32_t get_offset_of_m_JumpSpeed_6() { return static_cast<int32_t>(offsetof(FirstPersonController_t2645666525, ___m_JumpSpeed_6)); }
	inline float get_m_JumpSpeed_6() const { return ___m_JumpSpeed_6; }
	inline float* get_address_of_m_JumpSpeed_6() { return &___m_JumpSpeed_6; }
	inline void set_m_JumpSpeed_6(float value)
	{
		___m_JumpSpeed_6 = value;
	}

	inline static int32_t get_offset_of_m_StickToGroundForce_7() { return static_cast<int32_t>(offsetof(FirstPersonController_t2645666525, ___m_StickToGroundForce_7)); }
	inline float get_m_StickToGroundForce_7() const { return ___m_StickToGroundForce_7; }
	inline float* get_address_of_m_StickToGroundForce_7() { return &___m_StickToGroundForce_7; }
	inline void set_m_StickToGroundForce_7(float value)
	{
		___m_StickToGroundForce_7 = value;
	}

	inline static int32_t get_offset_of_m_GravityMultiplier_8() { return static_cast<int32_t>(offsetof(FirstPersonController_t2645666525, ___m_GravityMultiplier_8)); }
	inline float get_m_GravityMultiplier_8() const { return ___m_GravityMultiplier_8; }
	inline float* get_address_of_m_GravityMultiplier_8() { return &___m_GravityMultiplier_8; }
	inline void set_m_GravityMultiplier_8(float value)
	{
		___m_GravityMultiplier_8 = value;
	}

	inline static int32_t get_offset_of_m_MouseLook_9() { return static_cast<int32_t>(offsetof(FirstPersonController_t2645666525, ___m_MouseLook_9)); }
	inline MouseLook_t3694953670 * get_m_MouseLook_9() const { return ___m_MouseLook_9; }
	inline MouseLook_t3694953670 ** get_address_of_m_MouseLook_9() { return &___m_MouseLook_9; }
	inline void set_m_MouseLook_9(MouseLook_t3694953670 * value)
	{
		___m_MouseLook_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_MouseLook_9), value);
	}

	inline static int32_t get_offset_of_m_UseFovKick_10() { return static_cast<int32_t>(offsetof(FirstPersonController_t2645666525, ___m_UseFovKick_10)); }
	inline bool get_m_UseFovKick_10() const { return ___m_UseFovKick_10; }
	inline bool* get_address_of_m_UseFovKick_10() { return &___m_UseFovKick_10; }
	inline void set_m_UseFovKick_10(bool value)
	{
		___m_UseFovKick_10 = value;
	}

	inline static int32_t get_offset_of_m_FovKick_11() { return static_cast<int32_t>(offsetof(FirstPersonController_t2645666525, ___m_FovKick_11)); }
	inline FOVKick_t1285422362 * get_m_FovKick_11() const { return ___m_FovKick_11; }
	inline FOVKick_t1285422362 ** get_address_of_m_FovKick_11() { return &___m_FovKick_11; }
	inline void set_m_FovKick_11(FOVKick_t1285422362 * value)
	{
		___m_FovKick_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_FovKick_11), value);
	}

	inline static int32_t get_offset_of_m_UseHeadBob_12() { return static_cast<int32_t>(offsetof(FirstPersonController_t2645666525, ___m_UseHeadBob_12)); }
	inline bool get_m_UseHeadBob_12() const { return ___m_UseHeadBob_12; }
	inline bool* get_address_of_m_UseHeadBob_12() { return &___m_UseHeadBob_12; }
	inline void set_m_UseHeadBob_12(bool value)
	{
		___m_UseHeadBob_12 = value;
	}

	inline static int32_t get_offset_of_m_HeadBob_13() { return static_cast<int32_t>(offsetof(FirstPersonController_t2645666525, ___m_HeadBob_13)); }
	inline CurveControlledBob_t3985701645 * get_m_HeadBob_13() const { return ___m_HeadBob_13; }
	inline CurveControlledBob_t3985701645 ** get_address_of_m_HeadBob_13() { return &___m_HeadBob_13; }
	inline void set_m_HeadBob_13(CurveControlledBob_t3985701645 * value)
	{
		___m_HeadBob_13 = value;
		Il2CppCodeGenWriteBarrier((&___m_HeadBob_13), value);
	}

	inline static int32_t get_offset_of_m_JumpBob_14() { return static_cast<int32_t>(offsetof(FirstPersonController_t2645666525, ___m_JumpBob_14)); }
	inline LerpControlledBob_t4065608754 * get_m_JumpBob_14() const { return ___m_JumpBob_14; }
	inline LerpControlledBob_t4065608754 ** get_address_of_m_JumpBob_14() { return &___m_JumpBob_14; }
	inline void set_m_JumpBob_14(LerpControlledBob_t4065608754 * value)
	{
		___m_JumpBob_14 = value;
		Il2CppCodeGenWriteBarrier((&___m_JumpBob_14), value);
	}

	inline static int32_t get_offset_of_m_StepInterval_15() { return static_cast<int32_t>(offsetof(FirstPersonController_t2645666525, ___m_StepInterval_15)); }
	inline float get_m_StepInterval_15() const { return ___m_StepInterval_15; }
	inline float* get_address_of_m_StepInterval_15() { return &___m_StepInterval_15; }
	inline void set_m_StepInterval_15(float value)
	{
		___m_StepInterval_15 = value;
	}

	inline static int32_t get_offset_of_m_FootstepSounds_16() { return static_cast<int32_t>(offsetof(FirstPersonController_t2645666525, ___m_FootstepSounds_16)); }
	inline AudioClipU5BU5D_t3571400391* get_m_FootstepSounds_16() const { return ___m_FootstepSounds_16; }
	inline AudioClipU5BU5D_t3571400391** get_address_of_m_FootstepSounds_16() { return &___m_FootstepSounds_16; }
	inline void set_m_FootstepSounds_16(AudioClipU5BU5D_t3571400391* value)
	{
		___m_FootstepSounds_16 = value;
		Il2CppCodeGenWriteBarrier((&___m_FootstepSounds_16), value);
	}

	inline static int32_t get_offset_of_m_JumpSound_17() { return static_cast<int32_t>(offsetof(FirstPersonController_t2645666525, ___m_JumpSound_17)); }
	inline AudioClip_t474304562 * get_m_JumpSound_17() const { return ___m_JumpSound_17; }
	inline AudioClip_t474304562 ** get_address_of_m_JumpSound_17() { return &___m_JumpSound_17; }
	inline void set_m_JumpSound_17(AudioClip_t474304562 * value)
	{
		___m_JumpSound_17 = value;
		Il2CppCodeGenWriteBarrier((&___m_JumpSound_17), value);
	}

	inline static int32_t get_offset_of_m_LandSound_18() { return static_cast<int32_t>(offsetof(FirstPersonController_t2645666525, ___m_LandSound_18)); }
	inline AudioClip_t474304562 * get_m_LandSound_18() const { return ___m_LandSound_18; }
	inline AudioClip_t474304562 ** get_address_of_m_LandSound_18() { return &___m_LandSound_18; }
	inline void set_m_LandSound_18(AudioClip_t474304562 * value)
	{
		___m_LandSound_18 = value;
		Il2CppCodeGenWriteBarrier((&___m_LandSound_18), value);
	}

	inline static int32_t get_offset_of_m_Camera_19() { return static_cast<int32_t>(offsetof(FirstPersonController_t2645666525, ___m_Camera_19)); }
	inline Camera_t3175186167 * get_m_Camera_19() const { return ___m_Camera_19; }
	inline Camera_t3175186167 ** get_address_of_m_Camera_19() { return &___m_Camera_19; }
	inline void set_m_Camera_19(Camera_t3175186167 * value)
	{
		___m_Camera_19 = value;
		Il2CppCodeGenWriteBarrier((&___m_Camera_19), value);
	}

	inline static int32_t get_offset_of_m_Jump_20() { return static_cast<int32_t>(offsetof(FirstPersonController_t2645666525, ___m_Jump_20)); }
	inline bool get_m_Jump_20() const { return ___m_Jump_20; }
	inline bool* get_address_of_m_Jump_20() { return &___m_Jump_20; }
	inline void set_m_Jump_20(bool value)
	{
		___m_Jump_20 = value;
	}

	inline static int32_t get_offset_of_m_YRotation_21() { return static_cast<int32_t>(offsetof(FirstPersonController_t2645666525, ___m_YRotation_21)); }
	inline float get_m_YRotation_21() const { return ___m_YRotation_21; }
	inline float* get_address_of_m_YRotation_21() { return &___m_YRotation_21; }
	inline void set_m_YRotation_21(float value)
	{
		___m_YRotation_21 = value;
	}

	inline static int32_t get_offset_of_m_Input_22() { return static_cast<int32_t>(offsetof(FirstPersonController_t2645666525, ___m_Input_22)); }
	inline Vector2_t3577333262  get_m_Input_22() const { return ___m_Input_22; }
	inline Vector2_t3577333262 * get_address_of_m_Input_22() { return &___m_Input_22; }
	inline void set_m_Input_22(Vector2_t3577333262  value)
	{
		___m_Input_22 = value;
	}

	inline static int32_t get_offset_of_m_MoveDir_23() { return static_cast<int32_t>(offsetof(FirstPersonController_t2645666525, ___m_MoveDir_23)); }
	inline Vector3_t2903530434  get_m_MoveDir_23() const { return ___m_MoveDir_23; }
	inline Vector3_t2903530434 * get_address_of_m_MoveDir_23() { return &___m_MoveDir_23; }
	inline void set_m_MoveDir_23(Vector3_t2903530434  value)
	{
		___m_MoveDir_23 = value;
	}

	inline static int32_t get_offset_of_m_CharacterController_24() { return static_cast<int32_t>(offsetof(FirstPersonController_t2645666525, ___m_CharacterController_24)); }
	inline CharacterController_t2498358276 * get_m_CharacterController_24() const { return ___m_CharacterController_24; }
	inline CharacterController_t2498358276 ** get_address_of_m_CharacterController_24() { return &___m_CharacterController_24; }
	inline void set_m_CharacterController_24(CharacterController_t2498358276 * value)
	{
		___m_CharacterController_24 = value;
		Il2CppCodeGenWriteBarrier((&___m_CharacterController_24), value);
	}

	inline static int32_t get_offset_of_m_CollisionFlags_25() { return static_cast<int32_t>(offsetof(FirstPersonController_t2645666525, ___m_CollisionFlags_25)); }
	inline int32_t get_m_CollisionFlags_25() const { return ___m_CollisionFlags_25; }
	inline int32_t* get_address_of_m_CollisionFlags_25() { return &___m_CollisionFlags_25; }
	inline void set_m_CollisionFlags_25(int32_t value)
	{
		___m_CollisionFlags_25 = value;
	}

	inline static int32_t get_offset_of_m_PreviouslyGrounded_26() { return static_cast<int32_t>(offsetof(FirstPersonController_t2645666525, ___m_PreviouslyGrounded_26)); }
	inline bool get_m_PreviouslyGrounded_26() const { return ___m_PreviouslyGrounded_26; }
	inline bool* get_address_of_m_PreviouslyGrounded_26() { return &___m_PreviouslyGrounded_26; }
	inline void set_m_PreviouslyGrounded_26(bool value)
	{
		___m_PreviouslyGrounded_26 = value;
	}

	inline static int32_t get_offset_of_m_OriginalCameraPosition_27() { return static_cast<int32_t>(offsetof(FirstPersonController_t2645666525, ___m_OriginalCameraPosition_27)); }
	inline Vector3_t2903530434  get_m_OriginalCameraPosition_27() const { return ___m_OriginalCameraPosition_27; }
	inline Vector3_t2903530434 * get_address_of_m_OriginalCameraPosition_27() { return &___m_OriginalCameraPosition_27; }
	inline void set_m_OriginalCameraPosition_27(Vector3_t2903530434  value)
	{
		___m_OriginalCameraPosition_27 = value;
	}

	inline static int32_t get_offset_of_m_StepCycle_28() { return static_cast<int32_t>(offsetof(FirstPersonController_t2645666525, ___m_StepCycle_28)); }
	inline float get_m_StepCycle_28() const { return ___m_StepCycle_28; }
	inline float* get_address_of_m_StepCycle_28() { return &___m_StepCycle_28; }
	inline void set_m_StepCycle_28(float value)
	{
		___m_StepCycle_28 = value;
	}

	inline static int32_t get_offset_of_m_NextStep_29() { return static_cast<int32_t>(offsetof(FirstPersonController_t2645666525, ___m_NextStep_29)); }
	inline float get_m_NextStep_29() const { return ___m_NextStep_29; }
	inline float* get_address_of_m_NextStep_29() { return &___m_NextStep_29; }
	inline void set_m_NextStep_29(float value)
	{
		___m_NextStep_29 = value;
	}

	inline static int32_t get_offset_of_m_Jumping_30() { return static_cast<int32_t>(offsetof(FirstPersonController_t2645666525, ___m_Jumping_30)); }
	inline bool get_m_Jumping_30() const { return ___m_Jumping_30; }
	inline bool* get_address_of_m_Jumping_30() { return &___m_Jumping_30; }
	inline void set_m_Jumping_30(bool value)
	{
		___m_Jumping_30 = value;
	}

	inline static int32_t get_offset_of_m_AudioSource_31() { return static_cast<int32_t>(offsetof(FirstPersonController_t2645666525, ___m_AudioSource_31)); }
	inline AudioSource_t1862618521 * get_m_AudioSource_31() const { return ___m_AudioSource_31; }
	inline AudioSource_t1862618521 ** get_address_of_m_AudioSource_31() { return &___m_AudioSource_31; }
	inline void set_m_AudioSource_31(AudioSource_t1862618521 * value)
	{
		___m_AudioSource_31 = value;
		Il2CppCodeGenWriteBarrier((&___m_AudioSource_31), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FIRSTPERSONCONTROLLER_T2645666525_H
#ifndef BACKGROUNDPLANEABSTRACTBEHAVIOUR_T4259835756_H
#define BACKGROUNDPLANEABSTRACTBEHAVIOUR_T4259835756_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.BackgroundPlaneAbstractBehaviour
struct  BackgroundPlaneAbstractBehaviour_t4259835756  : public MonoBehaviour_t345688271
{
public:
	// Vuforia.VuforiaRenderer/VideoTextureInfo Vuforia.BackgroundPlaneAbstractBehaviour::mTextureInfo
	VideoTextureInfo_t773013061  ___mTextureInfo_2;
	// System.Int32 Vuforia.BackgroundPlaneAbstractBehaviour::mViewWidth
	int32_t ___mViewWidth_3;
	// System.Int32 Vuforia.BackgroundPlaneAbstractBehaviour::mViewHeight
	int32_t ___mViewHeight_4;
	// System.Int32 Vuforia.BackgroundPlaneAbstractBehaviour::mNumFramesToUpdateVideoBg
	int32_t ___mNumFramesToUpdateVideoBg_5;
	// UnityEngine.Camera Vuforia.BackgroundPlaneAbstractBehaviour::mCamera
	Camera_t3175186167 * ___mCamera_6;
	// System.Int32 Vuforia.BackgroundPlaneAbstractBehaviour::defaultNumDivisions
	int32_t ___defaultNumDivisions_8;
	// UnityEngine.Mesh Vuforia.BackgroundPlaneAbstractBehaviour::mMesh
	Mesh_t996500909 * ___mMesh_9;
	// System.Single Vuforia.BackgroundPlaneAbstractBehaviour::mStereoDepth
	float ___mStereoDepth_10;
	// UnityEngine.Vector3 Vuforia.BackgroundPlaneAbstractBehaviour::mBackgroundOffset
	Vector3_t2903530434  ___mBackgroundOffset_11;
	// Vuforia.VuforiaARController Vuforia.BackgroundPlaneAbstractBehaviour::mVuforiaBehaviour
	VuforiaARController_t1328503142 * ___mVuforiaBehaviour_12;
	// System.Action Vuforia.BackgroundPlaneAbstractBehaviour::mBackgroundPlacedCallback
	Action_t3619184611 * ___mBackgroundPlacedCallback_13;
	// System.Int32 Vuforia.BackgroundPlaneAbstractBehaviour::mNumDivisions
	int32_t ___mNumDivisions_14;

public:
	inline static int32_t get_offset_of_mTextureInfo_2() { return static_cast<int32_t>(offsetof(BackgroundPlaneAbstractBehaviour_t4259835756, ___mTextureInfo_2)); }
	inline VideoTextureInfo_t773013061  get_mTextureInfo_2() const { return ___mTextureInfo_2; }
	inline VideoTextureInfo_t773013061 * get_address_of_mTextureInfo_2() { return &___mTextureInfo_2; }
	inline void set_mTextureInfo_2(VideoTextureInfo_t773013061  value)
	{
		___mTextureInfo_2 = value;
	}

	inline static int32_t get_offset_of_mViewWidth_3() { return static_cast<int32_t>(offsetof(BackgroundPlaneAbstractBehaviour_t4259835756, ___mViewWidth_3)); }
	inline int32_t get_mViewWidth_3() const { return ___mViewWidth_3; }
	inline int32_t* get_address_of_mViewWidth_3() { return &___mViewWidth_3; }
	inline void set_mViewWidth_3(int32_t value)
	{
		___mViewWidth_3 = value;
	}

	inline static int32_t get_offset_of_mViewHeight_4() { return static_cast<int32_t>(offsetof(BackgroundPlaneAbstractBehaviour_t4259835756, ___mViewHeight_4)); }
	inline int32_t get_mViewHeight_4() const { return ___mViewHeight_4; }
	inline int32_t* get_address_of_mViewHeight_4() { return &___mViewHeight_4; }
	inline void set_mViewHeight_4(int32_t value)
	{
		___mViewHeight_4 = value;
	}

	inline static int32_t get_offset_of_mNumFramesToUpdateVideoBg_5() { return static_cast<int32_t>(offsetof(BackgroundPlaneAbstractBehaviour_t4259835756, ___mNumFramesToUpdateVideoBg_5)); }
	inline int32_t get_mNumFramesToUpdateVideoBg_5() const { return ___mNumFramesToUpdateVideoBg_5; }
	inline int32_t* get_address_of_mNumFramesToUpdateVideoBg_5() { return &___mNumFramesToUpdateVideoBg_5; }
	inline void set_mNumFramesToUpdateVideoBg_5(int32_t value)
	{
		___mNumFramesToUpdateVideoBg_5 = value;
	}

	inline static int32_t get_offset_of_mCamera_6() { return static_cast<int32_t>(offsetof(BackgroundPlaneAbstractBehaviour_t4259835756, ___mCamera_6)); }
	inline Camera_t3175186167 * get_mCamera_6() const { return ___mCamera_6; }
	inline Camera_t3175186167 ** get_address_of_mCamera_6() { return &___mCamera_6; }
	inline void set_mCamera_6(Camera_t3175186167 * value)
	{
		___mCamera_6 = value;
		Il2CppCodeGenWriteBarrier((&___mCamera_6), value);
	}

	inline static int32_t get_offset_of_defaultNumDivisions_8() { return static_cast<int32_t>(offsetof(BackgroundPlaneAbstractBehaviour_t4259835756, ___defaultNumDivisions_8)); }
	inline int32_t get_defaultNumDivisions_8() const { return ___defaultNumDivisions_8; }
	inline int32_t* get_address_of_defaultNumDivisions_8() { return &___defaultNumDivisions_8; }
	inline void set_defaultNumDivisions_8(int32_t value)
	{
		___defaultNumDivisions_8 = value;
	}

	inline static int32_t get_offset_of_mMesh_9() { return static_cast<int32_t>(offsetof(BackgroundPlaneAbstractBehaviour_t4259835756, ___mMesh_9)); }
	inline Mesh_t996500909 * get_mMesh_9() const { return ___mMesh_9; }
	inline Mesh_t996500909 ** get_address_of_mMesh_9() { return &___mMesh_9; }
	inline void set_mMesh_9(Mesh_t996500909 * value)
	{
		___mMesh_9 = value;
		Il2CppCodeGenWriteBarrier((&___mMesh_9), value);
	}

	inline static int32_t get_offset_of_mStereoDepth_10() { return static_cast<int32_t>(offsetof(BackgroundPlaneAbstractBehaviour_t4259835756, ___mStereoDepth_10)); }
	inline float get_mStereoDepth_10() const { return ___mStereoDepth_10; }
	inline float* get_address_of_mStereoDepth_10() { return &___mStereoDepth_10; }
	inline void set_mStereoDepth_10(float value)
	{
		___mStereoDepth_10 = value;
	}

	inline static int32_t get_offset_of_mBackgroundOffset_11() { return static_cast<int32_t>(offsetof(BackgroundPlaneAbstractBehaviour_t4259835756, ___mBackgroundOffset_11)); }
	inline Vector3_t2903530434  get_mBackgroundOffset_11() const { return ___mBackgroundOffset_11; }
	inline Vector3_t2903530434 * get_address_of_mBackgroundOffset_11() { return &___mBackgroundOffset_11; }
	inline void set_mBackgroundOffset_11(Vector3_t2903530434  value)
	{
		___mBackgroundOffset_11 = value;
	}

	inline static int32_t get_offset_of_mVuforiaBehaviour_12() { return static_cast<int32_t>(offsetof(BackgroundPlaneAbstractBehaviour_t4259835756, ___mVuforiaBehaviour_12)); }
	inline VuforiaARController_t1328503142 * get_mVuforiaBehaviour_12() const { return ___mVuforiaBehaviour_12; }
	inline VuforiaARController_t1328503142 ** get_address_of_mVuforiaBehaviour_12() { return &___mVuforiaBehaviour_12; }
	inline void set_mVuforiaBehaviour_12(VuforiaARController_t1328503142 * value)
	{
		___mVuforiaBehaviour_12 = value;
		Il2CppCodeGenWriteBarrier((&___mVuforiaBehaviour_12), value);
	}

	inline static int32_t get_offset_of_mBackgroundPlacedCallback_13() { return static_cast<int32_t>(offsetof(BackgroundPlaneAbstractBehaviour_t4259835756, ___mBackgroundPlacedCallback_13)); }
	inline Action_t3619184611 * get_mBackgroundPlacedCallback_13() const { return ___mBackgroundPlacedCallback_13; }
	inline Action_t3619184611 ** get_address_of_mBackgroundPlacedCallback_13() { return &___mBackgroundPlacedCallback_13; }
	inline void set_mBackgroundPlacedCallback_13(Action_t3619184611 * value)
	{
		___mBackgroundPlacedCallback_13 = value;
		Il2CppCodeGenWriteBarrier((&___mBackgroundPlacedCallback_13), value);
	}

	inline static int32_t get_offset_of_mNumDivisions_14() { return static_cast<int32_t>(offsetof(BackgroundPlaneAbstractBehaviour_t4259835756, ___mNumDivisions_14)); }
	inline int32_t get_mNumDivisions_14() const { return ___mNumDivisions_14; }
	inline int32_t* get_address_of_mNumDivisions_14() { return &___mNumDivisions_14; }
	inline void set_mNumDivisions_14(int32_t value)
	{
		___mNumDivisions_14 = value;
	}
};

struct BackgroundPlaneAbstractBehaviour_t4259835756_StaticFields
{
public:
	// System.Single Vuforia.BackgroundPlaneAbstractBehaviour::maxDisplacement
	float ___maxDisplacement_7;

public:
	inline static int32_t get_offset_of_maxDisplacement_7() { return static_cast<int32_t>(offsetof(BackgroundPlaneAbstractBehaviour_t4259835756_StaticFields, ___maxDisplacement_7)); }
	inline float get_maxDisplacement_7() const { return ___maxDisplacement_7; }
	inline float* get_address_of_maxDisplacement_7() { return &___maxDisplacement_7; }
	inline void set_maxDisplacement_7(float value)
	{
		___maxDisplacement_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BACKGROUNDPLANEABSTRACTBEHAVIOUR_T4259835756_H
#ifndef CLOUDRECOABSTRACTBEHAVIOUR_T2501456771_H
#define CLOUDRECOABSTRACTBEHAVIOUR_T2501456771_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.CloudRecoAbstractBehaviour
struct  CloudRecoAbstractBehaviour_t2501456771  : public MonoBehaviour_t345688271
{
public:
	// Vuforia.ObjectTracker Vuforia.CloudRecoAbstractBehaviour::mObjectTracker
	ObjectTracker_t4023638979 * ___mObjectTracker_2;
	// System.Boolean Vuforia.CloudRecoAbstractBehaviour::mCurrentlyInitializing
	bool ___mCurrentlyInitializing_3;
	// System.Boolean Vuforia.CloudRecoAbstractBehaviour::mInitSuccess
	bool ___mInitSuccess_4;
	// System.Boolean Vuforia.CloudRecoAbstractBehaviour::mCloudRecoStarted
	bool ___mCloudRecoStarted_5;
	// System.Boolean Vuforia.CloudRecoAbstractBehaviour::mOnInitializedCalled
	bool ___mOnInitializedCalled_6;
	// System.Collections.Generic.List`1<Vuforia.ICloudRecoEventHandler> Vuforia.CloudRecoAbstractBehaviour::mHandlers
	List_1_t1465274546 * ___mHandlers_7;
	// System.Boolean Vuforia.CloudRecoAbstractBehaviour::mTargetFinderStartedBeforeDisable
	bool ___mTargetFinderStartedBeforeDisable_8;
	// System.String Vuforia.CloudRecoAbstractBehaviour::AccessKey
	String_t* ___AccessKey_9;
	// System.String Vuforia.CloudRecoAbstractBehaviour::SecretKey
	String_t* ___SecretKey_10;

public:
	inline static int32_t get_offset_of_mObjectTracker_2() { return static_cast<int32_t>(offsetof(CloudRecoAbstractBehaviour_t2501456771, ___mObjectTracker_2)); }
	inline ObjectTracker_t4023638979 * get_mObjectTracker_2() const { return ___mObjectTracker_2; }
	inline ObjectTracker_t4023638979 ** get_address_of_mObjectTracker_2() { return &___mObjectTracker_2; }
	inline void set_mObjectTracker_2(ObjectTracker_t4023638979 * value)
	{
		___mObjectTracker_2 = value;
		Il2CppCodeGenWriteBarrier((&___mObjectTracker_2), value);
	}

	inline static int32_t get_offset_of_mCurrentlyInitializing_3() { return static_cast<int32_t>(offsetof(CloudRecoAbstractBehaviour_t2501456771, ___mCurrentlyInitializing_3)); }
	inline bool get_mCurrentlyInitializing_3() const { return ___mCurrentlyInitializing_3; }
	inline bool* get_address_of_mCurrentlyInitializing_3() { return &___mCurrentlyInitializing_3; }
	inline void set_mCurrentlyInitializing_3(bool value)
	{
		___mCurrentlyInitializing_3 = value;
	}

	inline static int32_t get_offset_of_mInitSuccess_4() { return static_cast<int32_t>(offsetof(CloudRecoAbstractBehaviour_t2501456771, ___mInitSuccess_4)); }
	inline bool get_mInitSuccess_4() const { return ___mInitSuccess_4; }
	inline bool* get_address_of_mInitSuccess_4() { return &___mInitSuccess_4; }
	inline void set_mInitSuccess_4(bool value)
	{
		___mInitSuccess_4 = value;
	}

	inline static int32_t get_offset_of_mCloudRecoStarted_5() { return static_cast<int32_t>(offsetof(CloudRecoAbstractBehaviour_t2501456771, ___mCloudRecoStarted_5)); }
	inline bool get_mCloudRecoStarted_5() const { return ___mCloudRecoStarted_5; }
	inline bool* get_address_of_mCloudRecoStarted_5() { return &___mCloudRecoStarted_5; }
	inline void set_mCloudRecoStarted_5(bool value)
	{
		___mCloudRecoStarted_5 = value;
	}

	inline static int32_t get_offset_of_mOnInitializedCalled_6() { return static_cast<int32_t>(offsetof(CloudRecoAbstractBehaviour_t2501456771, ___mOnInitializedCalled_6)); }
	inline bool get_mOnInitializedCalled_6() const { return ___mOnInitializedCalled_6; }
	inline bool* get_address_of_mOnInitializedCalled_6() { return &___mOnInitializedCalled_6; }
	inline void set_mOnInitializedCalled_6(bool value)
	{
		___mOnInitializedCalled_6 = value;
	}

	inline static int32_t get_offset_of_mHandlers_7() { return static_cast<int32_t>(offsetof(CloudRecoAbstractBehaviour_t2501456771, ___mHandlers_7)); }
	inline List_1_t1465274546 * get_mHandlers_7() const { return ___mHandlers_7; }
	inline List_1_t1465274546 ** get_address_of_mHandlers_7() { return &___mHandlers_7; }
	inline void set_mHandlers_7(List_1_t1465274546 * value)
	{
		___mHandlers_7 = value;
		Il2CppCodeGenWriteBarrier((&___mHandlers_7), value);
	}

	inline static int32_t get_offset_of_mTargetFinderStartedBeforeDisable_8() { return static_cast<int32_t>(offsetof(CloudRecoAbstractBehaviour_t2501456771, ___mTargetFinderStartedBeforeDisable_8)); }
	inline bool get_mTargetFinderStartedBeforeDisable_8() const { return ___mTargetFinderStartedBeforeDisable_8; }
	inline bool* get_address_of_mTargetFinderStartedBeforeDisable_8() { return &___mTargetFinderStartedBeforeDisable_8; }
	inline void set_mTargetFinderStartedBeforeDisable_8(bool value)
	{
		___mTargetFinderStartedBeforeDisable_8 = value;
	}

	inline static int32_t get_offset_of_AccessKey_9() { return static_cast<int32_t>(offsetof(CloudRecoAbstractBehaviour_t2501456771, ___AccessKey_9)); }
	inline String_t* get_AccessKey_9() const { return ___AccessKey_9; }
	inline String_t** get_address_of_AccessKey_9() { return &___AccessKey_9; }
	inline void set_AccessKey_9(String_t* value)
	{
		___AccessKey_9 = value;
		Il2CppCodeGenWriteBarrier((&___AccessKey_9), value);
	}

	inline static int32_t get_offset_of_SecretKey_10() { return static_cast<int32_t>(offsetof(CloudRecoAbstractBehaviour_t2501456771, ___SecretKey_10)); }
	inline String_t* get_SecretKey_10() const { return ___SecretKey_10; }
	inline String_t** get_address_of_SecretKey_10() { return &___SecretKey_10; }
	inline void set_SecretKey_10(String_t* value)
	{
		___SecretKey_10 = value;
		Il2CppCodeGenWriteBarrier((&___SecretKey_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CLOUDRECOABSTRACTBEHAVIOUR_T2501456771_H
#ifndef HIDEEXCESSAREAABSTRACTBEHAVIOUR_T1324413388_H
#define HIDEEXCESSAREAABSTRACTBEHAVIOUR_T1324413388_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.HideExcessAreaAbstractBehaviour
struct  HideExcessAreaAbstractBehaviour_t1324413388  : public MonoBehaviour_t345688271
{
public:
	// Vuforia.IExcessAreaClipping Vuforia.HideExcessAreaAbstractBehaviour::mClippingImpl
	RuntimeObject* ___mClippingImpl_2;
	// Vuforia.HideExcessAreaAbstractBehaviour/CLIPPING_MODE Vuforia.HideExcessAreaAbstractBehaviour::mClippingMode
	int32_t ___mClippingMode_3;
	// Vuforia.VuforiaARController Vuforia.HideExcessAreaAbstractBehaviour::mVuforiaBehaviour
	VuforiaARController_t1328503142 * ___mVuforiaBehaviour_4;
	// Vuforia.VideoBackgroundManager Vuforia.HideExcessAreaAbstractBehaviour::mVideoBgMgr
	VideoBackgroundManager_t2068552549 * ___mVideoBgMgr_5;
	// UnityEngine.Vector3 Vuforia.HideExcessAreaAbstractBehaviour::mPlaneOffset
	Vector3_t2903530434  ___mPlaneOffset_6;
	// System.Boolean Vuforia.HideExcessAreaAbstractBehaviour::mSceneScaledDown
	bool ___mSceneScaledDown_7;
	// System.Boolean Vuforia.HideExcessAreaAbstractBehaviour::mStarted
	bool ___mStarted_8;

public:
	inline static int32_t get_offset_of_mClippingImpl_2() { return static_cast<int32_t>(offsetof(HideExcessAreaAbstractBehaviour_t1324413388, ___mClippingImpl_2)); }
	inline RuntimeObject* get_mClippingImpl_2() const { return ___mClippingImpl_2; }
	inline RuntimeObject** get_address_of_mClippingImpl_2() { return &___mClippingImpl_2; }
	inline void set_mClippingImpl_2(RuntimeObject* value)
	{
		___mClippingImpl_2 = value;
		Il2CppCodeGenWriteBarrier((&___mClippingImpl_2), value);
	}

	inline static int32_t get_offset_of_mClippingMode_3() { return static_cast<int32_t>(offsetof(HideExcessAreaAbstractBehaviour_t1324413388, ___mClippingMode_3)); }
	inline int32_t get_mClippingMode_3() const { return ___mClippingMode_3; }
	inline int32_t* get_address_of_mClippingMode_3() { return &___mClippingMode_3; }
	inline void set_mClippingMode_3(int32_t value)
	{
		___mClippingMode_3 = value;
	}

	inline static int32_t get_offset_of_mVuforiaBehaviour_4() { return static_cast<int32_t>(offsetof(HideExcessAreaAbstractBehaviour_t1324413388, ___mVuforiaBehaviour_4)); }
	inline VuforiaARController_t1328503142 * get_mVuforiaBehaviour_4() const { return ___mVuforiaBehaviour_4; }
	inline VuforiaARController_t1328503142 ** get_address_of_mVuforiaBehaviour_4() { return &___mVuforiaBehaviour_4; }
	inline void set_mVuforiaBehaviour_4(VuforiaARController_t1328503142 * value)
	{
		___mVuforiaBehaviour_4 = value;
		Il2CppCodeGenWriteBarrier((&___mVuforiaBehaviour_4), value);
	}

	inline static int32_t get_offset_of_mVideoBgMgr_5() { return static_cast<int32_t>(offsetof(HideExcessAreaAbstractBehaviour_t1324413388, ___mVideoBgMgr_5)); }
	inline VideoBackgroundManager_t2068552549 * get_mVideoBgMgr_5() const { return ___mVideoBgMgr_5; }
	inline VideoBackgroundManager_t2068552549 ** get_address_of_mVideoBgMgr_5() { return &___mVideoBgMgr_5; }
	inline void set_mVideoBgMgr_5(VideoBackgroundManager_t2068552549 * value)
	{
		___mVideoBgMgr_5 = value;
		Il2CppCodeGenWriteBarrier((&___mVideoBgMgr_5), value);
	}

	inline static int32_t get_offset_of_mPlaneOffset_6() { return static_cast<int32_t>(offsetof(HideExcessAreaAbstractBehaviour_t1324413388, ___mPlaneOffset_6)); }
	inline Vector3_t2903530434  get_mPlaneOffset_6() const { return ___mPlaneOffset_6; }
	inline Vector3_t2903530434 * get_address_of_mPlaneOffset_6() { return &___mPlaneOffset_6; }
	inline void set_mPlaneOffset_6(Vector3_t2903530434  value)
	{
		___mPlaneOffset_6 = value;
	}

	inline static int32_t get_offset_of_mSceneScaledDown_7() { return static_cast<int32_t>(offsetof(HideExcessAreaAbstractBehaviour_t1324413388, ___mSceneScaledDown_7)); }
	inline bool get_mSceneScaledDown_7() const { return ___mSceneScaledDown_7; }
	inline bool* get_address_of_mSceneScaledDown_7() { return &___mSceneScaledDown_7; }
	inline void set_mSceneScaledDown_7(bool value)
	{
		___mSceneScaledDown_7 = value;
	}

	inline static int32_t get_offset_of_mStarted_8() { return static_cast<int32_t>(offsetof(HideExcessAreaAbstractBehaviour_t1324413388, ___mStarted_8)); }
	inline bool get_mStarted_8() const { return ___mStarted_8; }
	inline bool* get_address_of_mStarted_8() { return &___mStarted_8; }
	inline void set_mStarted_8(bool value)
	{
		___mStarted_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HIDEEXCESSAREAABSTRACTBEHAVIOUR_T1324413388_H
#ifndef MASKOUTABSTRACTBEHAVIOUR_T2174599097_H
#define MASKOUTABSTRACTBEHAVIOUR_T2174599097_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.MaskOutAbstractBehaviour
struct  MaskOutAbstractBehaviour_t2174599097  : public MonoBehaviour_t345688271
{
public:
	// UnityEngine.Material Vuforia.MaskOutAbstractBehaviour::maskMaterial
	Material_t1079520667 * ___maskMaterial_2;

public:
	inline static int32_t get_offset_of_maskMaterial_2() { return static_cast<int32_t>(offsetof(MaskOutAbstractBehaviour_t2174599097, ___maskMaterial_2)); }
	inline Material_t1079520667 * get_maskMaterial_2() const { return ___maskMaterial_2; }
	inline Material_t1079520667 ** get_address_of_maskMaterial_2() { return &___maskMaterial_2; }
	inline void set_maskMaterial_2(Material_t1079520667 * value)
	{
		___maskMaterial_2 = value;
		Il2CppCodeGenWriteBarrier((&___maskMaterial_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MASKOUTABSTRACTBEHAVIOUR_T2174599097_H
#ifndef RECONSTRUCTIONABSTRACTBEHAVIOUR_T954468633_H
#define RECONSTRUCTIONABSTRACTBEHAVIOUR_T954468633_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.ReconstructionAbstractBehaviour
struct  ReconstructionAbstractBehaviour_t954468633  : public MonoBehaviour_t345688271
{
public:
	// System.Boolean Vuforia.ReconstructionAbstractBehaviour::mHasInitialized
	bool ___mHasInitialized_2;
	// System.Collections.Generic.List`1<Vuforia.ISmartTerrainEventHandler> Vuforia.ReconstructionAbstractBehaviour::mSmartTerrainEventHandlers
	List_1_t3446838688 * ___mSmartTerrainEventHandlers_3;
	// System.Action`1<Vuforia.SmartTerrainInitializationInfo> Vuforia.ReconstructionAbstractBehaviour::mOnInitialized
	Action_1_t1488611326 * ___mOnInitialized_4;
	// System.Action`1<Vuforia.Prop> Vuforia.ReconstructionAbstractBehaviour::mOnPropCreated
	Action_1_t1560204979 * ___mOnPropCreated_5;
	// System.Action`1<Vuforia.Prop> Vuforia.ReconstructionAbstractBehaviour::mOnPropUpdated
	Action_1_t1560204979 * ___mOnPropUpdated_6;
	// System.Action`1<Vuforia.Prop> Vuforia.ReconstructionAbstractBehaviour::mOnPropDeleted
	Action_1_t1560204979 * ___mOnPropDeleted_7;
	// System.Action`1<Vuforia.Surface> Vuforia.ReconstructionAbstractBehaviour::mOnSurfaceCreated
	Action_1_t1020068010 * ___mOnSurfaceCreated_8;
	// System.Action`1<Vuforia.Surface> Vuforia.ReconstructionAbstractBehaviour::mOnSurfaceUpdated
	Action_1_t1020068010 * ___mOnSurfaceUpdated_9;
	// System.Action`1<Vuforia.Surface> Vuforia.ReconstructionAbstractBehaviour::mOnSurfaceDeleted
	Action_1_t1020068010 * ___mOnSurfaceDeleted_10;
	// System.Boolean Vuforia.ReconstructionAbstractBehaviour::mInitializedInEditor
	bool ___mInitializedInEditor_11;
	// System.Boolean Vuforia.ReconstructionAbstractBehaviour::mMaximumExtentEnabled
	bool ___mMaximumExtentEnabled_12;
	// UnityEngine.Rect Vuforia.ReconstructionAbstractBehaviour::mMaximumExtent
	Rect_t3436776195  ___mMaximumExtent_13;
	// System.Boolean Vuforia.ReconstructionAbstractBehaviour::mAutomaticStart
	bool ___mAutomaticStart_14;
	// System.Boolean Vuforia.ReconstructionAbstractBehaviour::mNavMeshUpdates
	bool ___mNavMeshUpdates_15;
	// System.Single Vuforia.ReconstructionAbstractBehaviour::mNavMeshPadding
	float ___mNavMeshPadding_16;
	// Vuforia.Reconstruction Vuforia.ReconstructionAbstractBehaviour::mReconstruction
	RuntimeObject* ___mReconstruction_17;
	// System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.Surface> Vuforia.ReconstructionAbstractBehaviour::mSurfaces
	Dictionary_2_t2230247832 * ___mSurfaces_18;
	// System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.SurfaceAbstractBehaviour> Vuforia.ReconstructionAbstractBehaviour::mActiveSurfaceBehaviours
	Dictionary_2_t1468548340 * ___mActiveSurfaceBehaviours_19;
	// System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.Prop> Vuforia.ReconstructionAbstractBehaviour::mProps
	Dictionary_2_t2770384801 * ___mProps_20;
	// System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.PropAbstractBehaviour> Vuforia.ReconstructionAbstractBehaviour::mActivePropBehaviours
	Dictionary_2_t3844889330 * ___mActivePropBehaviours_21;
	// Vuforia.SurfaceAbstractBehaviour Vuforia.ReconstructionAbstractBehaviour::mPreviouslySetWorldCenterSurfaceTemplate
	SurfaceAbstractBehaviour_t272541176 * ___mPreviouslySetWorldCenterSurfaceTemplate_22;
	// System.Boolean Vuforia.ReconstructionAbstractBehaviour::mIgnoreNextUpdate
	bool ___mIgnoreNextUpdate_23;

public:
	inline static int32_t get_offset_of_mHasInitialized_2() { return static_cast<int32_t>(offsetof(ReconstructionAbstractBehaviour_t954468633, ___mHasInitialized_2)); }
	inline bool get_mHasInitialized_2() const { return ___mHasInitialized_2; }
	inline bool* get_address_of_mHasInitialized_2() { return &___mHasInitialized_2; }
	inline void set_mHasInitialized_2(bool value)
	{
		___mHasInitialized_2 = value;
	}

	inline static int32_t get_offset_of_mSmartTerrainEventHandlers_3() { return static_cast<int32_t>(offsetof(ReconstructionAbstractBehaviour_t954468633, ___mSmartTerrainEventHandlers_3)); }
	inline List_1_t3446838688 * get_mSmartTerrainEventHandlers_3() const { return ___mSmartTerrainEventHandlers_3; }
	inline List_1_t3446838688 ** get_address_of_mSmartTerrainEventHandlers_3() { return &___mSmartTerrainEventHandlers_3; }
	inline void set_mSmartTerrainEventHandlers_3(List_1_t3446838688 * value)
	{
		___mSmartTerrainEventHandlers_3 = value;
		Il2CppCodeGenWriteBarrier((&___mSmartTerrainEventHandlers_3), value);
	}

	inline static int32_t get_offset_of_mOnInitialized_4() { return static_cast<int32_t>(offsetof(ReconstructionAbstractBehaviour_t954468633, ___mOnInitialized_4)); }
	inline Action_1_t1488611326 * get_mOnInitialized_4() const { return ___mOnInitialized_4; }
	inline Action_1_t1488611326 ** get_address_of_mOnInitialized_4() { return &___mOnInitialized_4; }
	inline void set_mOnInitialized_4(Action_1_t1488611326 * value)
	{
		___mOnInitialized_4 = value;
		Il2CppCodeGenWriteBarrier((&___mOnInitialized_4), value);
	}

	inline static int32_t get_offset_of_mOnPropCreated_5() { return static_cast<int32_t>(offsetof(ReconstructionAbstractBehaviour_t954468633, ___mOnPropCreated_5)); }
	inline Action_1_t1560204979 * get_mOnPropCreated_5() const { return ___mOnPropCreated_5; }
	inline Action_1_t1560204979 ** get_address_of_mOnPropCreated_5() { return &___mOnPropCreated_5; }
	inline void set_mOnPropCreated_5(Action_1_t1560204979 * value)
	{
		___mOnPropCreated_5 = value;
		Il2CppCodeGenWriteBarrier((&___mOnPropCreated_5), value);
	}

	inline static int32_t get_offset_of_mOnPropUpdated_6() { return static_cast<int32_t>(offsetof(ReconstructionAbstractBehaviour_t954468633, ___mOnPropUpdated_6)); }
	inline Action_1_t1560204979 * get_mOnPropUpdated_6() const { return ___mOnPropUpdated_6; }
	inline Action_1_t1560204979 ** get_address_of_mOnPropUpdated_6() { return &___mOnPropUpdated_6; }
	inline void set_mOnPropUpdated_6(Action_1_t1560204979 * value)
	{
		___mOnPropUpdated_6 = value;
		Il2CppCodeGenWriteBarrier((&___mOnPropUpdated_6), value);
	}

	inline static int32_t get_offset_of_mOnPropDeleted_7() { return static_cast<int32_t>(offsetof(ReconstructionAbstractBehaviour_t954468633, ___mOnPropDeleted_7)); }
	inline Action_1_t1560204979 * get_mOnPropDeleted_7() const { return ___mOnPropDeleted_7; }
	inline Action_1_t1560204979 ** get_address_of_mOnPropDeleted_7() { return &___mOnPropDeleted_7; }
	inline void set_mOnPropDeleted_7(Action_1_t1560204979 * value)
	{
		___mOnPropDeleted_7 = value;
		Il2CppCodeGenWriteBarrier((&___mOnPropDeleted_7), value);
	}

	inline static int32_t get_offset_of_mOnSurfaceCreated_8() { return static_cast<int32_t>(offsetof(ReconstructionAbstractBehaviour_t954468633, ___mOnSurfaceCreated_8)); }
	inline Action_1_t1020068010 * get_mOnSurfaceCreated_8() const { return ___mOnSurfaceCreated_8; }
	inline Action_1_t1020068010 ** get_address_of_mOnSurfaceCreated_8() { return &___mOnSurfaceCreated_8; }
	inline void set_mOnSurfaceCreated_8(Action_1_t1020068010 * value)
	{
		___mOnSurfaceCreated_8 = value;
		Il2CppCodeGenWriteBarrier((&___mOnSurfaceCreated_8), value);
	}

	inline static int32_t get_offset_of_mOnSurfaceUpdated_9() { return static_cast<int32_t>(offsetof(ReconstructionAbstractBehaviour_t954468633, ___mOnSurfaceUpdated_9)); }
	inline Action_1_t1020068010 * get_mOnSurfaceUpdated_9() const { return ___mOnSurfaceUpdated_9; }
	inline Action_1_t1020068010 ** get_address_of_mOnSurfaceUpdated_9() { return &___mOnSurfaceUpdated_9; }
	inline void set_mOnSurfaceUpdated_9(Action_1_t1020068010 * value)
	{
		___mOnSurfaceUpdated_9 = value;
		Il2CppCodeGenWriteBarrier((&___mOnSurfaceUpdated_9), value);
	}

	inline static int32_t get_offset_of_mOnSurfaceDeleted_10() { return static_cast<int32_t>(offsetof(ReconstructionAbstractBehaviour_t954468633, ___mOnSurfaceDeleted_10)); }
	inline Action_1_t1020068010 * get_mOnSurfaceDeleted_10() const { return ___mOnSurfaceDeleted_10; }
	inline Action_1_t1020068010 ** get_address_of_mOnSurfaceDeleted_10() { return &___mOnSurfaceDeleted_10; }
	inline void set_mOnSurfaceDeleted_10(Action_1_t1020068010 * value)
	{
		___mOnSurfaceDeleted_10 = value;
		Il2CppCodeGenWriteBarrier((&___mOnSurfaceDeleted_10), value);
	}

	inline static int32_t get_offset_of_mInitializedInEditor_11() { return static_cast<int32_t>(offsetof(ReconstructionAbstractBehaviour_t954468633, ___mInitializedInEditor_11)); }
	inline bool get_mInitializedInEditor_11() const { return ___mInitializedInEditor_11; }
	inline bool* get_address_of_mInitializedInEditor_11() { return &___mInitializedInEditor_11; }
	inline void set_mInitializedInEditor_11(bool value)
	{
		___mInitializedInEditor_11 = value;
	}

	inline static int32_t get_offset_of_mMaximumExtentEnabled_12() { return static_cast<int32_t>(offsetof(ReconstructionAbstractBehaviour_t954468633, ___mMaximumExtentEnabled_12)); }
	inline bool get_mMaximumExtentEnabled_12() const { return ___mMaximumExtentEnabled_12; }
	inline bool* get_address_of_mMaximumExtentEnabled_12() { return &___mMaximumExtentEnabled_12; }
	inline void set_mMaximumExtentEnabled_12(bool value)
	{
		___mMaximumExtentEnabled_12 = value;
	}

	inline static int32_t get_offset_of_mMaximumExtent_13() { return static_cast<int32_t>(offsetof(ReconstructionAbstractBehaviour_t954468633, ___mMaximumExtent_13)); }
	inline Rect_t3436776195  get_mMaximumExtent_13() const { return ___mMaximumExtent_13; }
	inline Rect_t3436776195 * get_address_of_mMaximumExtent_13() { return &___mMaximumExtent_13; }
	inline void set_mMaximumExtent_13(Rect_t3436776195  value)
	{
		___mMaximumExtent_13 = value;
	}

	inline static int32_t get_offset_of_mAutomaticStart_14() { return static_cast<int32_t>(offsetof(ReconstructionAbstractBehaviour_t954468633, ___mAutomaticStart_14)); }
	inline bool get_mAutomaticStart_14() const { return ___mAutomaticStart_14; }
	inline bool* get_address_of_mAutomaticStart_14() { return &___mAutomaticStart_14; }
	inline void set_mAutomaticStart_14(bool value)
	{
		___mAutomaticStart_14 = value;
	}

	inline static int32_t get_offset_of_mNavMeshUpdates_15() { return static_cast<int32_t>(offsetof(ReconstructionAbstractBehaviour_t954468633, ___mNavMeshUpdates_15)); }
	inline bool get_mNavMeshUpdates_15() const { return ___mNavMeshUpdates_15; }
	inline bool* get_address_of_mNavMeshUpdates_15() { return &___mNavMeshUpdates_15; }
	inline void set_mNavMeshUpdates_15(bool value)
	{
		___mNavMeshUpdates_15 = value;
	}

	inline static int32_t get_offset_of_mNavMeshPadding_16() { return static_cast<int32_t>(offsetof(ReconstructionAbstractBehaviour_t954468633, ___mNavMeshPadding_16)); }
	inline float get_mNavMeshPadding_16() const { return ___mNavMeshPadding_16; }
	inline float* get_address_of_mNavMeshPadding_16() { return &___mNavMeshPadding_16; }
	inline void set_mNavMeshPadding_16(float value)
	{
		___mNavMeshPadding_16 = value;
	}

	inline static int32_t get_offset_of_mReconstruction_17() { return static_cast<int32_t>(offsetof(ReconstructionAbstractBehaviour_t954468633, ___mReconstruction_17)); }
	inline RuntimeObject* get_mReconstruction_17() const { return ___mReconstruction_17; }
	inline RuntimeObject** get_address_of_mReconstruction_17() { return &___mReconstruction_17; }
	inline void set_mReconstruction_17(RuntimeObject* value)
	{
		___mReconstruction_17 = value;
		Il2CppCodeGenWriteBarrier((&___mReconstruction_17), value);
	}

	inline static int32_t get_offset_of_mSurfaces_18() { return static_cast<int32_t>(offsetof(ReconstructionAbstractBehaviour_t954468633, ___mSurfaces_18)); }
	inline Dictionary_2_t2230247832 * get_mSurfaces_18() const { return ___mSurfaces_18; }
	inline Dictionary_2_t2230247832 ** get_address_of_mSurfaces_18() { return &___mSurfaces_18; }
	inline void set_mSurfaces_18(Dictionary_2_t2230247832 * value)
	{
		___mSurfaces_18 = value;
		Il2CppCodeGenWriteBarrier((&___mSurfaces_18), value);
	}

	inline static int32_t get_offset_of_mActiveSurfaceBehaviours_19() { return static_cast<int32_t>(offsetof(ReconstructionAbstractBehaviour_t954468633, ___mActiveSurfaceBehaviours_19)); }
	inline Dictionary_2_t1468548340 * get_mActiveSurfaceBehaviours_19() const { return ___mActiveSurfaceBehaviours_19; }
	inline Dictionary_2_t1468548340 ** get_address_of_mActiveSurfaceBehaviours_19() { return &___mActiveSurfaceBehaviours_19; }
	inline void set_mActiveSurfaceBehaviours_19(Dictionary_2_t1468548340 * value)
	{
		___mActiveSurfaceBehaviours_19 = value;
		Il2CppCodeGenWriteBarrier((&___mActiveSurfaceBehaviours_19), value);
	}

	inline static int32_t get_offset_of_mProps_20() { return static_cast<int32_t>(offsetof(ReconstructionAbstractBehaviour_t954468633, ___mProps_20)); }
	inline Dictionary_2_t2770384801 * get_mProps_20() const { return ___mProps_20; }
	inline Dictionary_2_t2770384801 ** get_address_of_mProps_20() { return &___mProps_20; }
	inline void set_mProps_20(Dictionary_2_t2770384801 * value)
	{
		___mProps_20 = value;
		Il2CppCodeGenWriteBarrier((&___mProps_20), value);
	}

	inline static int32_t get_offset_of_mActivePropBehaviours_21() { return static_cast<int32_t>(offsetof(ReconstructionAbstractBehaviour_t954468633, ___mActivePropBehaviours_21)); }
	inline Dictionary_2_t3844889330 * get_mActivePropBehaviours_21() const { return ___mActivePropBehaviours_21; }
	inline Dictionary_2_t3844889330 ** get_address_of_mActivePropBehaviours_21() { return &___mActivePropBehaviours_21; }
	inline void set_mActivePropBehaviours_21(Dictionary_2_t3844889330 * value)
	{
		___mActivePropBehaviours_21 = value;
		Il2CppCodeGenWriteBarrier((&___mActivePropBehaviours_21), value);
	}

	inline static int32_t get_offset_of_mPreviouslySetWorldCenterSurfaceTemplate_22() { return static_cast<int32_t>(offsetof(ReconstructionAbstractBehaviour_t954468633, ___mPreviouslySetWorldCenterSurfaceTemplate_22)); }
	inline SurfaceAbstractBehaviour_t272541176 * get_mPreviouslySetWorldCenterSurfaceTemplate_22() const { return ___mPreviouslySetWorldCenterSurfaceTemplate_22; }
	inline SurfaceAbstractBehaviour_t272541176 ** get_address_of_mPreviouslySetWorldCenterSurfaceTemplate_22() { return &___mPreviouslySetWorldCenterSurfaceTemplate_22; }
	inline void set_mPreviouslySetWorldCenterSurfaceTemplate_22(SurfaceAbstractBehaviour_t272541176 * value)
	{
		___mPreviouslySetWorldCenterSurfaceTemplate_22 = value;
		Il2CppCodeGenWriteBarrier((&___mPreviouslySetWorldCenterSurfaceTemplate_22), value);
	}

	inline static int32_t get_offset_of_mIgnoreNextUpdate_23() { return static_cast<int32_t>(offsetof(ReconstructionAbstractBehaviour_t954468633, ___mIgnoreNextUpdate_23)); }
	inline bool get_mIgnoreNextUpdate_23() const { return ___mIgnoreNextUpdate_23; }
	inline bool* get_address_of_mIgnoreNextUpdate_23() { return &___mIgnoreNextUpdate_23; }
	inline void set_mIgnoreNextUpdate_23(bool value)
	{
		___mIgnoreNextUpdate_23 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RECONSTRUCTIONABSTRACTBEHAVIOUR_T954468633_H
#ifndef GLERRORHANDLER_T1216999495_H
#define GLERRORHANDLER_T1216999495_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.GLErrorHandler
struct  GLErrorHandler_t1216999495  : public MonoBehaviour_t345688271
{
public:

public:
};

struct GLErrorHandler_t1216999495_StaticFields
{
public:
	// System.String Vuforia.GLErrorHandler::mErrorText
	String_t* ___mErrorText_2;
	// System.Boolean Vuforia.GLErrorHandler::mErrorOccurred
	bool ___mErrorOccurred_3;

public:
	inline static int32_t get_offset_of_mErrorText_2() { return static_cast<int32_t>(offsetof(GLErrorHandler_t1216999495_StaticFields, ___mErrorText_2)); }
	inline String_t* get_mErrorText_2() const { return ___mErrorText_2; }
	inline String_t** get_address_of_mErrorText_2() { return &___mErrorText_2; }
	inline void set_mErrorText_2(String_t* value)
	{
		___mErrorText_2 = value;
		Il2CppCodeGenWriteBarrier((&___mErrorText_2), value);
	}

	inline static int32_t get_offset_of_mErrorOccurred_3() { return static_cast<int32_t>(offsetof(GLErrorHandler_t1216999495_StaticFields, ___mErrorOccurred_3)); }
	inline bool get_mErrorOccurred_3() const { return ___mErrorOccurred_3; }
	inline bool* get_address_of_mErrorOccurred_3() { return &___mErrorOccurred_3; }
	inline void set_mErrorOccurred_3(bool value)
	{
		___mErrorOccurred_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GLERRORHANDLER_T1216999495_H
#ifndef TEXTRECOABSTRACTBEHAVIOUR_T1751158196_H
#define TEXTRECOABSTRACTBEHAVIOUR_T1751158196_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.TextRecoAbstractBehaviour
struct  TextRecoAbstractBehaviour_t1751158196  : public MonoBehaviour_t345688271
{
public:
	// System.Boolean Vuforia.TextRecoAbstractBehaviour::mHasInitialized
	bool ___mHasInitialized_2;
	// System.Boolean Vuforia.TextRecoAbstractBehaviour::mTrackerWasActiveBeforePause
	bool ___mTrackerWasActiveBeforePause_3;
	// System.Boolean Vuforia.TextRecoAbstractBehaviour::mTrackerWasActiveBeforeDisabling
	bool ___mTrackerWasActiveBeforeDisabling_4;
	// System.String Vuforia.TextRecoAbstractBehaviour::mWordListFile
	String_t* ___mWordListFile_5;
	// System.String Vuforia.TextRecoAbstractBehaviour::mCustomWordListFile
	String_t* ___mCustomWordListFile_6;
	// System.String Vuforia.TextRecoAbstractBehaviour::mAdditionalCustomWords
	String_t* ___mAdditionalCustomWords_7;
	// Vuforia.WordFilterMode Vuforia.TextRecoAbstractBehaviour::mFilterMode
	int32_t ___mFilterMode_8;
	// System.String Vuforia.TextRecoAbstractBehaviour::mFilterListFile
	String_t* ___mFilterListFile_9;
	// System.String Vuforia.TextRecoAbstractBehaviour::mAdditionalFilterWords
	String_t* ___mAdditionalFilterWords_10;
	// Vuforia.WordPrefabCreationMode Vuforia.TextRecoAbstractBehaviour::mWordPrefabCreationMode
	int32_t ___mWordPrefabCreationMode_11;
	// System.Int32 Vuforia.TextRecoAbstractBehaviour::mMaximumWordInstances
	int32_t ___mMaximumWordInstances_12;
	// System.Collections.Generic.List`1<Vuforia.ITextRecoEventHandler> Vuforia.TextRecoAbstractBehaviour::mTextRecoEventHandlers
	List_1_t4098171074 * ___mTextRecoEventHandlers_13;

public:
	inline static int32_t get_offset_of_mHasInitialized_2() { return static_cast<int32_t>(offsetof(TextRecoAbstractBehaviour_t1751158196, ___mHasInitialized_2)); }
	inline bool get_mHasInitialized_2() const { return ___mHasInitialized_2; }
	inline bool* get_address_of_mHasInitialized_2() { return &___mHasInitialized_2; }
	inline void set_mHasInitialized_2(bool value)
	{
		___mHasInitialized_2 = value;
	}

	inline static int32_t get_offset_of_mTrackerWasActiveBeforePause_3() { return static_cast<int32_t>(offsetof(TextRecoAbstractBehaviour_t1751158196, ___mTrackerWasActiveBeforePause_3)); }
	inline bool get_mTrackerWasActiveBeforePause_3() const { return ___mTrackerWasActiveBeforePause_3; }
	inline bool* get_address_of_mTrackerWasActiveBeforePause_3() { return &___mTrackerWasActiveBeforePause_3; }
	inline void set_mTrackerWasActiveBeforePause_3(bool value)
	{
		___mTrackerWasActiveBeforePause_3 = value;
	}

	inline static int32_t get_offset_of_mTrackerWasActiveBeforeDisabling_4() { return static_cast<int32_t>(offsetof(TextRecoAbstractBehaviour_t1751158196, ___mTrackerWasActiveBeforeDisabling_4)); }
	inline bool get_mTrackerWasActiveBeforeDisabling_4() const { return ___mTrackerWasActiveBeforeDisabling_4; }
	inline bool* get_address_of_mTrackerWasActiveBeforeDisabling_4() { return &___mTrackerWasActiveBeforeDisabling_4; }
	inline void set_mTrackerWasActiveBeforeDisabling_4(bool value)
	{
		___mTrackerWasActiveBeforeDisabling_4 = value;
	}

	inline static int32_t get_offset_of_mWordListFile_5() { return static_cast<int32_t>(offsetof(TextRecoAbstractBehaviour_t1751158196, ___mWordListFile_5)); }
	inline String_t* get_mWordListFile_5() const { return ___mWordListFile_5; }
	inline String_t** get_address_of_mWordListFile_5() { return &___mWordListFile_5; }
	inline void set_mWordListFile_5(String_t* value)
	{
		___mWordListFile_5 = value;
		Il2CppCodeGenWriteBarrier((&___mWordListFile_5), value);
	}

	inline static int32_t get_offset_of_mCustomWordListFile_6() { return static_cast<int32_t>(offsetof(TextRecoAbstractBehaviour_t1751158196, ___mCustomWordListFile_6)); }
	inline String_t* get_mCustomWordListFile_6() const { return ___mCustomWordListFile_6; }
	inline String_t** get_address_of_mCustomWordListFile_6() { return &___mCustomWordListFile_6; }
	inline void set_mCustomWordListFile_6(String_t* value)
	{
		___mCustomWordListFile_6 = value;
		Il2CppCodeGenWriteBarrier((&___mCustomWordListFile_6), value);
	}

	inline static int32_t get_offset_of_mAdditionalCustomWords_7() { return static_cast<int32_t>(offsetof(TextRecoAbstractBehaviour_t1751158196, ___mAdditionalCustomWords_7)); }
	inline String_t* get_mAdditionalCustomWords_7() const { return ___mAdditionalCustomWords_7; }
	inline String_t** get_address_of_mAdditionalCustomWords_7() { return &___mAdditionalCustomWords_7; }
	inline void set_mAdditionalCustomWords_7(String_t* value)
	{
		___mAdditionalCustomWords_7 = value;
		Il2CppCodeGenWriteBarrier((&___mAdditionalCustomWords_7), value);
	}

	inline static int32_t get_offset_of_mFilterMode_8() { return static_cast<int32_t>(offsetof(TextRecoAbstractBehaviour_t1751158196, ___mFilterMode_8)); }
	inline int32_t get_mFilterMode_8() const { return ___mFilterMode_8; }
	inline int32_t* get_address_of_mFilterMode_8() { return &___mFilterMode_8; }
	inline void set_mFilterMode_8(int32_t value)
	{
		___mFilterMode_8 = value;
	}

	inline static int32_t get_offset_of_mFilterListFile_9() { return static_cast<int32_t>(offsetof(TextRecoAbstractBehaviour_t1751158196, ___mFilterListFile_9)); }
	inline String_t* get_mFilterListFile_9() const { return ___mFilterListFile_9; }
	inline String_t** get_address_of_mFilterListFile_9() { return &___mFilterListFile_9; }
	inline void set_mFilterListFile_9(String_t* value)
	{
		___mFilterListFile_9 = value;
		Il2CppCodeGenWriteBarrier((&___mFilterListFile_9), value);
	}

	inline static int32_t get_offset_of_mAdditionalFilterWords_10() { return static_cast<int32_t>(offsetof(TextRecoAbstractBehaviour_t1751158196, ___mAdditionalFilterWords_10)); }
	inline String_t* get_mAdditionalFilterWords_10() const { return ___mAdditionalFilterWords_10; }
	inline String_t** get_address_of_mAdditionalFilterWords_10() { return &___mAdditionalFilterWords_10; }
	inline void set_mAdditionalFilterWords_10(String_t* value)
	{
		___mAdditionalFilterWords_10 = value;
		Il2CppCodeGenWriteBarrier((&___mAdditionalFilterWords_10), value);
	}

	inline static int32_t get_offset_of_mWordPrefabCreationMode_11() { return static_cast<int32_t>(offsetof(TextRecoAbstractBehaviour_t1751158196, ___mWordPrefabCreationMode_11)); }
	inline int32_t get_mWordPrefabCreationMode_11() const { return ___mWordPrefabCreationMode_11; }
	inline int32_t* get_address_of_mWordPrefabCreationMode_11() { return &___mWordPrefabCreationMode_11; }
	inline void set_mWordPrefabCreationMode_11(int32_t value)
	{
		___mWordPrefabCreationMode_11 = value;
	}

	inline static int32_t get_offset_of_mMaximumWordInstances_12() { return static_cast<int32_t>(offsetof(TextRecoAbstractBehaviour_t1751158196, ___mMaximumWordInstances_12)); }
	inline int32_t get_mMaximumWordInstances_12() const { return ___mMaximumWordInstances_12; }
	inline int32_t* get_address_of_mMaximumWordInstances_12() { return &___mMaximumWordInstances_12; }
	inline void set_mMaximumWordInstances_12(int32_t value)
	{
		___mMaximumWordInstances_12 = value;
	}

	inline static int32_t get_offset_of_mTextRecoEventHandlers_13() { return static_cast<int32_t>(offsetof(TextRecoAbstractBehaviour_t1751158196, ___mTextRecoEventHandlers_13)); }
	inline List_1_t4098171074 * get_mTextRecoEventHandlers_13() const { return ___mTextRecoEventHandlers_13; }
	inline List_1_t4098171074 ** get_address_of_mTextRecoEventHandlers_13() { return &___mTextRecoEventHandlers_13; }
	inline void set_mTextRecoEventHandlers_13(List_1_t4098171074 * value)
	{
		___mTextRecoEventHandlers_13 = value;
		Il2CppCodeGenWriteBarrier((&___mTextRecoEventHandlers_13), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTRECOABSTRACTBEHAVIOUR_T1751158196_H
#ifndef TRACKABLEBEHAVIOUR_T2419079356_H
#define TRACKABLEBEHAVIOUR_T2419079356_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.TrackableBehaviour
struct  TrackableBehaviour_t2419079356  : public MonoBehaviour_t345688271
{
public:
	// System.Double Vuforia.TrackableBehaviour::<TimeStamp>k__BackingField
	double ___U3CTimeStampU3Ek__BackingField_2;
	// System.String Vuforia.TrackableBehaviour::mTrackableName
	String_t* ___mTrackableName_3;
	// System.Boolean Vuforia.TrackableBehaviour::mPreserveChildSize
	bool ___mPreserveChildSize_4;
	// System.Boolean Vuforia.TrackableBehaviour::mInitializedInEditor
	bool ___mInitializedInEditor_5;
	// UnityEngine.Vector3 Vuforia.TrackableBehaviour::mPreviousScale
	Vector3_t2903530434  ___mPreviousScale_6;
	// Vuforia.TrackableBehaviour/Status Vuforia.TrackableBehaviour::mStatus
	int32_t ___mStatus_7;
	// Vuforia.Trackable Vuforia.TrackableBehaviour::mTrackable
	RuntimeObject* ___mTrackable_8;
	// System.Collections.Generic.List`1<Vuforia.ITrackableEventHandler> Vuforia.TrackableBehaviour::mTrackableEventHandlers
	List_1_t3589236026 * ___mTrackableEventHandlers_9;

public:
	inline static int32_t get_offset_of_U3CTimeStampU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(TrackableBehaviour_t2419079356, ___U3CTimeStampU3Ek__BackingField_2)); }
	inline double get_U3CTimeStampU3Ek__BackingField_2() const { return ___U3CTimeStampU3Ek__BackingField_2; }
	inline double* get_address_of_U3CTimeStampU3Ek__BackingField_2() { return &___U3CTimeStampU3Ek__BackingField_2; }
	inline void set_U3CTimeStampU3Ek__BackingField_2(double value)
	{
		___U3CTimeStampU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_mTrackableName_3() { return static_cast<int32_t>(offsetof(TrackableBehaviour_t2419079356, ___mTrackableName_3)); }
	inline String_t* get_mTrackableName_3() const { return ___mTrackableName_3; }
	inline String_t** get_address_of_mTrackableName_3() { return &___mTrackableName_3; }
	inline void set_mTrackableName_3(String_t* value)
	{
		___mTrackableName_3 = value;
		Il2CppCodeGenWriteBarrier((&___mTrackableName_3), value);
	}

	inline static int32_t get_offset_of_mPreserveChildSize_4() { return static_cast<int32_t>(offsetof(TrackableBehaviour_t2419079356, ___mPreserveChildSize_4)); }
	inline bool get_mPreserveChildSize_4() const { return ___mPreserveChildSize_4; }
	inline bool* get_address_of_mPreserveChildSize_4() { return &___mPreserveChildSize_4; }
	inline void set_mPreserveChildSize_4(bool value)
	{
		___mPreserveChildSize_4 = value;
	}

	inline static int32_t get_offset_of_mInitializedInEditor_5() { return static_cast<int32_t>(offsetof(TrackableBehaviour_t2419079356, ___mInitializedInEditor_5)); }
	inline bool get_mInitializedInEditor_5() const { return ___mInitializedInEditor_5; }
	inline bool* get_address_of_mInitializedInEditor_5() { return &___mInitializedInEditor_5; }
	inline void set_mInitializedInEditor_5(bool value)
	{
		___mInitializedInEditor_5 = value;
	}

	inline static int32_t get_offset_of_mPreviousScale_6() { return static_cast<int32_t>(offsetof(TrackableBehaviour_t2419079356, ___mPreviousScale_6)); }
	inline Vector3_t2903530434  get_mPreviousScale_6() const { return ___mPreviousScale_6; }
	inline Vector3_t2903530434 * get_address_of_mPreviousScale_6() { return &___mPreviousScale_6; }
	inline void set_mPreviousScale_6(Vector3_t2903530434  value)
	{
		___mPreviousScale_6 = value;
	}

	inline static int32_t get_offset_of_mStatus_7() { return static_cast<int32_t>(offsetof(TrackableBehaviour_t2419079356, ___mStatus_7)); }
	inline int32_t get_mStatus_7() const { return ___mStatus_7; }
	inline int32_t* get_address_of_mStatus_7() { return &___mStatus_7; }
	inline void set_mStatus_7(int32_t value)
	{
		___mStatus_7 = value;
	}

	inline static int32_t get_offset_of_mTrackable_8() { return static_cast<int32_t>(offsetof(TrackableBehaviour_t2419079356, ___mTrackable_8)); }
	inline RuntimeObject* get_mTrackable_8() const { return ___mTrackable_8; }
	inline RuntimeObject** get_address_of_mTrackable_8() { return &___mTrackable_8; }
	inline void set_mTrackable_8(RuntimeObject* value)
	{
		___mTrackable_8 = value;
		Il2CppCodeGenWriteBarrier((&___mTrackable_8), value);
	}

	inline static int32_t get_offset_of_mTrackableEventHandlers_9() { return static_cast<int32_t>(offsetof(TrackableBehaviour_t2419079356, ___mTrackableEventHandlers_9)); }
	inline List_1_t3589236026 * get_mTrackableEventHandlers_9() const { return ___mTrackableEventHandlers_9; }
	inline List_1_t3589236026 ** get_address_of_mTrackableEventHandlers_9() { return &___mTrackableEventHandlers_9; }
	inline void set_mTrackableEventHandlers_9(List_1_t3589236026 * value)
	{
		___mTrackableEventHandlers_9 = value;
		Il2CppCodeGenWriteBarrier((&___mTrackableEventHandlers_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRACKABLEBEHAVIOUR_T2419079356_H
#ifndef HEADBOB_T1764489347_H
#define HEADBOB_T1764489347_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Characters.FirstPerson.HeadBob
struct  HeadBob_t1764489347  : public MonoBehaviour_t345688271
{
public:
	// UnityEngine.Camera UnityStandardAssets.Characters.FirstPerson.HeadBob::Camera
	Camera_t3175186167 * ___Camera_2;
	// UnityStandardAssets.Utility.CurveControlledBob UnityStandardAssets.Characters.FirstPerson.HeadBob::motionBob
	CurveControlledBob_t3985701645 * ___motionBob_3;
	// UnityStandardAssets.Utility.LerpControlledBob UnityStandardAssets.Characters.FirstPerson.HeadBob::jumpAndLandingBob
	LerpControlledBob_t4065608754 * ___jumpAndLandingBob_4;
	// UnityStandardAssets.Characters.FirstPerson.RigidbodyFirstPersonController UnityStandardAssets.Characters.FirstPerson.HeadBob::rigidbodyFirstPersonController
	RigidbodyFirstPersonController_t1713505141 * ___rigidbodyFirstPersonController_5;
	// System.Single UnityStandardAssets.Characters.FirstPerson.HeadBob::StrideInterval
	float ___StrideInterval_6;
	// System.Single UnityStandardAssets.Characters.FirstPerson.HeadBob::RunningStrideLengthen
	float ___RunningStrideLengthen_7;
	// System.Boolean UnityStandardAssets.Characters.FirstPerson.HeadBob::m_PreviouslyGrounded
	bool ___m_PreviouslyGrounded_8;
	// UnityEngine.Vector3 UnityStandardAssets.Characters.FirstPerson.HeadBob::m_OriginalCameraPosition
	Vector3_t2903530434  ___m_OriginalCameraPosition_9;

public:
	inline static int32_t get_offset_of_Camera_2() { return static_cast<int32_t>(offsetof(HeadBob_t1764489347, ___Camera_2)); }
	inline Camera_t3175186167 * get_Camera_2() const { return ___Camera_2; }
	inline Camera_t3175186167 ** get_address_of_Camera_2() { return &___Camera_2; }
	inline void set_Camera_2(Camera_t3175186167 * value)
	{
		___Camera_2 = value;
		Il2CppCodeGenWriteBarrier((&___Camera_2), value);
	}

	inline static int32_t get_offset_of_motionBob_3() { return static_cast<int32_t>(offsetof(HeadBob_t1764489347, ___motionBob_3)); }
	inline CurveControlledBob_t3985701645 * get_motionBob_3() const { return ___motionBob_3; }
	inline CurveControlledBob_t3985701645 ** get_address_of_motionBob_3() { return &___motionBob_3; }
	inline void set_motionBob_3(CurveControlledBob_t3985701645 * value)
	{
		___motionBob_3 = value;
		Il2CppCodeGenWriteBarrier((&___motionBob_3), value);
	}

	inline static int32_t get_offset_of_jumpAndLandingBob_4() { return static_cast<int32_t>(offsetof(HeadBob_t1764489347, ___jumpAndLandingBob_4)); }
	inline LerpControlledBob_t4065608754 * get_jumpAndLandingBob_4() const { return ___jumpAndLandingBob_4; }
	inline LerpControlledBob_t4065608754 ** get_address_of_jumpAndLandingBob_4() { return &___jumpAndLandingBob_4; }
	inline void set_jumpAndLandingBob_4(LerpControlledBob_t4065608754 * value)
	{
		___jumpAndLandingBob_4 = value;
		Il2CppCodeGenWriteBarrier((&___jumpAndLandingBob_4), value);
	}

	inline static int32_t get_offset_of_rigidbodyFirstPersonController_5() { return static_cast<int32_t>(offsetof(HeadBob_t1764489347, ___rigidbodyFirstPersonController_5)); }
	inline RigidbodyFirstPersonController_t1713505141 * get_rigidbodyFirstPersonController_5() const { return ___rigidbodyFirstPersonController_5; }
	inline RigidbodyFirstPersonController_t1713505141 ** get_address_of_rigidbodyFirstPersonController_5() { return &___rigidbodyFirstPersonController_5; }
	inline void set_rigidbodyFirstPersonController_5(RigidbodyFirstPersonController_t1713505141 * value)
	{
		___rigidbodyFirstPersonController_5 = value;
		Il2CppCodeGenWriteBarrier((&___rigidbodyFirstPersonController_5), value);
	}

	inline static int32_t get_offset_of_StrideInterval_6() { return static_cast<int32_t>(offsetof(HeadBob_t1764489347, ___StrideInterval_6)); }
	inline float get_StrideInterval_6() const { return ___StrideInterval_6; }
	inline float* get_address_of_StrideInterval_6() { return &___StrideInterval_6; }
	inline void set_StrideInterval_6(float value)
	{
		___StrideInterval_6 = value;
	}

	inline static int32_t get_offset_of_RunningStrideLengthen_7() { return static_cast<int32_t>(offsetof(HeadBob_t1764489347, ___RunningStrideLengthen_7)); }
	inline float get_RunningStrideLengthen_7() const { return ___RunningStrideLengthen_7; }
	inline float* get_address_of_RunningStrideLengthen_7() { return &___RunningStrideLengthen_7; }
	inline void set_RunningStrideLengthen_7(float value)
	{
		___RunningStrideLengthen_7 = value;
	}

	inline static int32_t get_offset_of_m_PreviouslyGrounded_8() { return static_cast<int32_t>(offsetof(HeadBob_t1764489347, ___m_PreviouslyGrounded_8)); }
	inline bool get_m_PreviouslyGrounded_8() const { return ___m_PreviouslyGrounded_8; }
	inline bool* get_address_of_m_PreviouslyGrounded_8() { return &___m_PreviouslyGrounded_8; }
	inline void set_m_PreviouslyGrounded_8(bool value)
	{
		___m_PreviouslyGrounded_8 = value;
	}

	inline static int32_t get_offset_of_m_OriginalCameraPosition_9() { return static_cast<int32_t>(offsetof(HeadBob_t1764489347, ___m_OriginalCameraPosition_9)); }
	inline Vector3_t2903530434  get_m_OriginalCameraPosition_9() const { return ___m_OriginalCameraPosition_9; }
	inline Vector3_t2903530434 * get_address_of_m_OriginalCameraPosition_9() { return &___m_OriginalCameraPosition_9; }
	inline void set_m_OriginalCameraPosition_9(Vector3_t2903530434  value)
	{
		___m_OriginalCameraPosition_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HEADBOB_T1764489347_H
#ifndef DEFAULTTRACKABLEEVENTHANDLER_T2331528573_H
#define DEFAULTTRACKABLEEVENTHANDLER_T2331528573_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.DefaultTrackableEventHandler
struct  DefaultTrackableEventHandler_t2331528573  : public MonoBehaviour_t345688271
{
public:
	// Vuforia.TrackableBehaviour Vuforia.DefaultTrackableEventHandler::mTrackableBehaviour
	TrackableBehaviour_t2419079356 * ___mTrackableBehaviour_2;

public:
	inline static int32_t get_offset_of_mTrackableBehaviour_2() { return static_cast<int32_t>(offsetof(DefaultTrackableEventHandler_t2331528573, ___mTrackableBehaviour_2)); }
	inline TrackableBehaviour_t2419079356 * get_mTrackableBehaviour_2() const { return ___mTrackableBehaviour_2; }
	inline TrackableBehaviour_t2419079356 ** get_address_of_mTrackableBehaviour_2() { return &___mTrackableBehaviour_2; }
	inline void set_mTrackableBehaviour_2(TrackableBehaviour_t2419079356 * value)
	{
		___mTrackableBehaviour_2 = value;
		Il2CppCodeGenWriteBarrier((&___mTrackableBehaviour_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFAULTTRACKABLEEVENTHANDLER_T2331528573_H
#ifndef JOYSTICK_T3754997822_H
#define JOYSTICK_T3754997822_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.CrossPlatformInput.Joystick
struct  Joystick_t3754997822  : public MonoBehaviour_t345688271
{
public:
	// System.Int32 UnityStandardAssets.CrossPlatformInput.Joystick::MovementRange
	int32_t ___MovementRange_2;
	// UnityStandardAssets.CrossPlatformInput.Joystick/AxisOption UnityStandardAssets.CrossPlatformInput.Joystick::axesToUse
	int32_t ___axesToUse_3;
	// System.String UnityStandardAssets.CrossPlatformInput.Joystick::horizontalAxisName
	String_t* ___horizontalAxisName_4;
	// System.String UnityStandardAssets.CrossPlatformInput.Joystick::verticalAxisName
	String_t* ___verticalAxisName_5;
	// UnityEngine.Vector3 UnityStandardAssets.CrossPlatformInput.Joystick::m_StartPos
	Vector3_t2903530434  ___m_StartPos_6;
	// System.Boolean UnityStandardAssets.CrossPlatformInput.Joystick::m_UseX
	bool ___m_UseX_7;
	// System.Boolean UnityStandardAssets.CrossPlatformInput.Joystick::m_UseY
	bool ___m_UseY_8;
	// UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualAxis UnityStandardAssets.CrossPlatformInput.Joystick::m_HorizontalVirtualAxis
	VirtualAxis_t2383171311 * ___m_HorizontalVirtualAxis_9;
	// UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualAxis UnityStandardAssets.CrossPlatformInput.Joystick::m_VerticalVirtualAxis
	VirtualAxis_t2383171311 * ___m_VerticalVirtualAxis_10;

public:
	inline static int32_t get_offset_of_MovementRange_2() { return static_cast<int32_t>(offsetof(Joystick_t3754997822, ___MovementRange_2)); }
	inline int32_t get_MovementRange_2() const { return ___MovementRange_2; }
	inline int32_t* get_address_of_MovementRange_2() { return &___MovementRange_2; }
	inline void set_MovementRange_2(int32_t value)
	{
		___MovementRange_2 = value;
	}

	inline static int32_t get_offset_of_axesToUse_3() { return static_cast<int32_t>(offsetof(Joystick_t3754997822, ___axesToUse_3)); }
	inline int32_t get_axesToUse_3() const { return ___axesToUse_3; }
	inline int32_t* get_address_of_axesToUse_3() { return &___axesToUse_3; }
	inline void set_axesToUse_3(int32_t value)
	{
		___axesToUse_3 = value;
	}

	inline static int32_t get_offset_of_horizontalAxisName_4() { return static_cast<int32_t>(offsetof(Joystick_t3754997822, ___horizontalAxisName_4)); }
	inline String_t* get_horizontalAxisName_4() const { return ___horizontalAxisName_4; }
	inline String_t** get_address_of_horizontalAxisName_4() { return &___horizontalAxisName_4; }
	inline void set_horizontalAxisName_4(String_t* value)
	{
		___horizontalAxisName_4 = value;
		Il2CppCodeGenWriteBarrier((&___horizontalAxisName_4), value);
	}

	inline static int32_t get_offset_of_verticalAxisName_5() { return static_cast<int32_t>(offsetof(Joystick_t3754997822, ___verticalAxisName_5)); }
	inline String_t* get_verticalAxisName_5() const { return ___verticalAxisName_5; }
	inline String_t** get_address_of_verticalAxisName_5() { return &___verticalAxisName_5; }
	inline void set_verticalAxisName_5(String_t* value)
	{
		___verticalAxisName_5 = value;
		Il2CppCodeGenWriteBarrier((&___verticalAxisName_5), value);
	}

	inline static int32_t get_offset_of_m_StartPos_6() { return static_cast<int32_t>(offsetof(Joystick_t3754997822, ___m_StartPos_6)); }
	inline Vector3_t2903530434  get_m_StartPos_6() const { return ___m_StartPos_6; }
	inline Vector3_t2903530434 * get_address_of_m_StartPos_6() { return &___m_StartPos_6; }
	inline void set_m_StartPos_6(Vector3_t2903530434  value)
	{
		___m_StartPos_6 = value;
	}

	inline static int32_t get_offset_of_m_UseX_7() { return static_cast<int32_t>(offsetof(Joystick_t3754997822, ___m_UseX_7)); }
	inline bool get_m_UseX_7() const { return ___m_UseX_7; }
	inline bool* get_address_of_m_UseX_7() { return &___m_UseX_7; }
	inline void set_m_UseX_7(bool value)
	{
		___m_UseX_7 = value;
	}

	inline static int32_t get_offset_of_m_UseY_8() { return static_cast<int32_t>(offsetof(Joystick_t3754997822, ___m_UseY_8)); }
	inline bool get_m_UseY_8() const { return ___m_UseY_8; }
	inline bool* get_address_of_m_UseY_8() { return &___m_UseY_8; }
	inline void set_m_UseY_8(bool value)
	{
		___m_UseY_8 = value;
	}

	inline static int32_t get_offset_of_m_HorizontalVirtualAxis_9() { return static_cast<int32_t>(offsetof(Joystick_t3754997822, ___m_HorizontalVirtualAxis_9)); }
	inline VirtualAxis_t2383171311 * get_m_HorizontalVirtualAxis_9() const { return ___m_HorizontalVirtualAxis_9; }
	inline VirtualAxis_t2383171311 ** get_address_of_m_HorizontalVirtualAxis_9() { return &___m_HorizontalVirtualAxis_9; }
	inline void set_m_HorizontalVirtualAxis_9(VirtualAxis_t2383171311 * value)
	{
		___m_HorizontalVirtualAxis_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_HorizontalVirtualAxis_9), value);
	}

	inline static int32_t get_offset_of_m_VerticalVirtualAxis_10() { return static_cast<int32_t>(offsetof(Joystick_t3754997822, ___m_VerticalVirtualAxis_10)); }
	inline VirtualAxis_t2383171311 * get_m_VerticalVirtualAxis_10() const { return ___m_VerticalVirtualAxis_10; }
	inline VirtualAxis_t2383171311 ** get_address_of_m_VerticalVirtualAxis_10() { return &___m_VerticalVirtualAxis_10; }
	inline void set_m_VerticalVirtualAxis_10(VirtualAxis_t2383171311 * value)
	{
		___m_VerticalVirtualAxis_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_VerticalVirtualAxis_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JOYSTICK_T3754997822_H
#ifndef DEFAULTINITIALIZATIONERRORHANDLER_T414875900_H
#define DEFAULTINITIALIZATIONERRORHANDLER_T414875900_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.DefaultInitializationErrorHandler
struct  DefaultInitializationErrorHandler_t414875900  : public MonoBehaviour_t345688271
{
public:
	// System.String Vuforia.DefaultInitializationErrorHandler::mErrorText
	String_t* ___mErrorText_2;
	// System.Boolean Vuforia.DefaultInitializationErrorHandler::mErrorOccurred
	bool ___mErrorOccurred_3;

public:
	inline static int32_t get_offset_of_mErrorText_2() { return static_cast<int32_t>(offsetof(DefaultInitializationErrorHandler_t414875900, ___mErrorText_2)); }
	inline String_t* get_mErrorText_2() const { return ___mErrorText_2; }
	inline String_t** get_address_of_mErrorText_2() { return &___mErrorText_2; }
	inline void set_mErrorText_2(String_t* value)
	{
		___mErrorText_2 = value;
		Il2CppCodeGenWriteBarrier((&___mErrorText_2), value);
	}

	inline static int32_t get_offset_of_mErrorOccurred_3() { return static_cast<int32_t>(offsetof(DefaultInitializationErrorHandler_t414875900, ___mErrorOccurred_3)); }
	inline bool get_mErrorOccurred_3() const { return ___mErrorOccurred_3; }
	inline bool* get_address_of_mErrorOccurred_3() { return &___mErrorOccurred_3; }
	inline void set_mErrorOccurred_3(bool value)
	{
		___mErrorOccurred_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFAULTINITIALIZATIONERRORHANDLER_T414875900_H
#ifndef AICHARACTERCONTROL_T2038132910_H
#define AICHARACTERCONTROL_T2038132910_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Characters.ThirdPerson.AICharacterControl
struct  AICharacterControl_t2038132910  : public MonoBehaviour_t345688271
{
public:
	// UnityEngine.AI.NavMeshAgent UnityStandardAssets.Characters.ThirdPerson.AICharacterControl::<agent>k__BackingField
	NavMeshAgent_t1476393864 * ___U3CagentU3Ek__BackingField_2;
	// UnityStandardAssets.Characters.ThirdPerson.ThirdPersonCharacter UnityStandardAssets.Characters.ThirdPerson.AICharacterControl::<character>k__BackingField
	ThirdPersonCharacter_t1426137179 * ___U3CcharacterU3Ek__BackingField_3;
	// UnityEngine.Transform UnityStandardAssets.Characters.ThirdPerson.AICharacterControl::target
	Transform_t3316442598 * ___target_4;

public:
	inline static int32_t get_offset_of_U3CagentU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(AICharacterControl_t2038132910, ___U3CagentU3Ek__BackingField_2)); }
	inline NavMeshAgent_t1476393864 * get_U3CagentU3Ek__BackingField_2() const { return ___U3CagentU3Ek__BackingField_2; }
	inline NavMeshAgent_t1476393864 ** get_address_of_U3CagentU3Ek__BackingField_2() { return &___U3CagentU3Ek__BackingField_2; }
	inline void set_U3CagentU3Ek__BackingField_2(NavMeshAgent_t1476393864 * value)
	{
		___U3CagentU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CagentU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3CcharacterU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(AICharacterControl_t2038132910, ___U3CcharacterU3Ek__BackingField_3)); }
	inline ThirdPersonCharacter_t1426137179 * get_U3CcharacterU3Ek__BackingField_3() const { return ___U3CcharacterU3Ek__BackingField_3; }
	inline ThirdPersonCharacter_t1426137179 ** get_address_of_U3CcharacterU3Ek__BackingField_3() { return &___U3CcharacterU3Ek__BackingField_3; }
	inline void set_U3CcharacterU3Ek__BackingField_3(ThirdPersonCharacter_t1426137179 * value)
	{
		___U3CcharacterU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CcharacterU3Ek__BackingField_3), value);
	}

	inline static int32_t get_offset_of_target_4() { return static_cast<int32_t>(offsetof(AICharacterControl_t2038132910, ___target_4)); }
	inline Transform_t3316442598 * get_target_4() const { return ___target_4; }
	inline Transform_t3316442598 ** get_address_of_target_4() { return &___target_4; }
	inline void set_target_4(Transform_t3316442598 * value)
	{
		___target_4 = value;
		Il2CppCodeGenWriteBarrier((&___target_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AICHARACTERCONTROL_T2038132910_H
#ifndef DEFAULTSMARTTERRAINEVENTHANDLER_T3202897208_H
#define DEFAULTSMARTTERRAINEVENTHANDLER_T3202897208_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.DefaultSmartTerrainEventHandler
struct  DefaultSmartTerrainEventHandler_t3202897208  : public MonoBehaviour_t345688271
{
public:
	// Vuforia.ReconstructionBehaviour Vuforia.DefaultSmartTerrainEventHandler::mReconstructionBehaviour
	ReconstructionBehaviour_t3936121853 * ___mReconstructionBehaviour_2;
	// Vuforia.PropBehaviour Vuforia.DefaultSmartTerrainEventHandler::PropTemplate
	PropBehaviour_t3897855688 * ___PropTemplate_3;
	// Vuforia.SurfaceBehaviour Vuforia.DefaultSmartTerrainEventHandler::SurfaceTemplate
	SurfaceBehaviour_t1975960194 * ___SurfaceTemplate_4;

public:
	inline static int32_t get_offset_of_mReconstructionBehaviour_2() { return static_cast<int32_t>(offsetof(DefaultSmartTerrainEventHandler_t3202897208, ___mReconstructionBehaviour_2)); }
	inline ReconstructionBehaviour_t3936121853 * get_mReconstructionBehaviour_2() const { return ___mReconstructionBehaviour_2; }
	inline ReconstructionBehaviour_t3936121853 ** get_address_of_mReconstructionBehaviour_2() { return &___mReconstructionBehaviour_2; }
	inline void set_mReconstructionBehaviour_2(ReconstructionBehaviour_t3936121853 * value)
	{
		___mReconstructionBehaviour_2 = value;
		Il2CppCodeGenWriteBarrier((&___mReconstructionBehaviour_2), value);
	}

	inline static int32_t get_offset_of_PropTemplate_3() { return static_cast<int32_t>(offsetof(DefaultSmartTerrainEventHandler_t3202897208, ___PropTemplate_3)); }
	inline PropBehaviour_t3897855688 * get_PropTemplate_3() const { return ___PropTemplate_3; }
	inline PropBehaviour_t3897855688 ** get_address_of_PropTemplate_3() { return &___PropTemplate_3; }
	inline void set_PropTemplate_3(PropBehaviour_t3897855688 * value)
	{
		___PropTemplate_3 = value;
		Il2CppCodeGenWriteBarrier((&___PropTemplate_3), value);
	}

	inline static int32_t get_offset_of_SurfaceTemplate_4() { return static_cast<int32_t>(offsetof(DefaultSmartTerrainEventHandler_t3202897208, ___SurfaceTemplate_4)); }
	inline SurfaceBehaviour_t1975960194 * get_SurfaceTemplate_4() const { return ___SurfaceTemplate_4; }
	inline SurfaceBehaviour_t1975960194 ** get_address_of_SurfaceTemplate_4() { return &___SurfaceTemplate_4; }
	inline void set_SurfaceTemplate_4(SurfaceBehaviour_t1975960194 * value)
	{
		___SurfaceTemplate_4 = value;
		Il2CppCodeGenWriteBarrier((&___SurfaceTemplate_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFAULTSMARTTERRAINEVENTHANDLER_T3202897208_H
#ifndef THIRDPERSONCHARACTER_T1426137179_H
#define THIRDPERSONCHARACTER_T1426137179_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Characters.ThirdPerson.ThirdPersonCharacter
struct  ThirdPersonCharacter_t1426137179  : public MonoBehaviour_t345688271
{
public:
	// System.Single UnityStandardAssets.Characters.ThirdPerson.ThirdPersonCharacter::m_MovingTurnSpeed
	float ___m_MovingTurnSpeed_2;
	// System.Single UnityStandardAssets.Characters.ThirdPerson.ThirdPersonCharacter::m_StationaryTurnSpeed
	float ___m_StationaryTurnSpeed_3;
	// System.Single UnityStandardAssets.Characters.ThirdPerson.ThirdPersonCharacter::m_JumpPower
	float ___m_JumpPower_4;
	// System.Single UnityStandardAssets.Characters.ThirdPerson.ThirdPersonCharacter::m_GravityMultiplier
	float ___m_GravityMultiplier_5;
	// System.Single UnityStandardAssets.Characters.ThirdPerson.ThirdPersonCharacter::m_RunCycleLegOffset
	float ___m_RunCycleLegOffset_6;
	// System.Single UnityStandardAssets.Characters.ThirdPerson.ThirdPersonCharacter::m_MoveSpeedMultiplier
	float ___m_MoveSpeedMultiplier_7;
	// System.Single UnityStandardAssets.Characters.ThirdPerson.ThirdPersonCharacter::m_AnimSpeedMultiplier
	float ___m_AnimSpeedMultiplier_8;
	// System.Single UnityStandardAssets.Characters.ThirdPerson.ThirdPersonCharacter::m_GroundCheckDistance
	float ___m_GroundCheckDistance_9;
	// UnityEngine.Rigidbody UnityStandardAssets.Characters.ThirdPerson.ThirdPersonCharacter::m_Rigidbody
	Rigidbody_t2492269564 * ___m_Rigidbody_10;
	// UnityEngine.Animator UnityStandardAssets.Characters.ThirdPerson.ThirdPersonCharacter::m_Animator
	Animator_t4075492284 * ___m_Animator_11;
	// System.Boolean UnityStandardAssets.Characters.ThirdPerson.ThirdPersonCharacter::m_IsGrounded
	bool ___m_IsGrounded_12;
	// System.Single UnityStandardAssets.Characters.ThirdPerson.ThirdPersonCharacter::m_OrigGroundCheckDistance
	float ___m_OrigGroundCheckDistance_13;
	// System.Single UnityStandardAssets.Characters.ThirdPerson.ThirdPersonCharacter::m_TurnAmount
	float ___m_TurnAmount_15;
	// System.Single UnityStandardAssets.Characters.ThirdPerson.ThirdPersonCharacter::m_ForwardAmount
	float ___m_ForwardAmount_16;
	// UnityEngine.Vector3 UnityStandardAssets.Characters.ThirdPerson.ThirdPersonCharacter::m_GroundNormal
	Vector3_t2903530434  ___m_GroundNormal_17;
	// System.Single UnityStandardAssets.Characters.ThirdPerson.ThirdPersonCharacter::m_CapsuleHeight
	float ___m_CapsuleHeight_18;
	// UnityEngine.Vector3 UnityStandardAssets.Characters.ThirdPerson.ThirdPersonCharacter::m_CapsuleCenter
	Vector3_t2903530434  ___m_CapsuleCenter_19;
	// UnityEngine.CapsuleCollider UnityStandardAssets.Characters.ThirdPerson.ThirdPersonCharacter::m_Capsule
	CapsuleCollider_t77672896 * ___m_Capsule_20;
	// System.Boolean UnityStandardAssets.Characters.ThirdPerson.ThirdPersonCharacter::m_Crouching
	bool ___m_Crouching_21;

public:
	inline static int32_t get_offset_of_m_MovingTurnSpeed_2() { return static_cast<int32_t>(offsetof(ThirdPersonCharacter_t1426137179, ___m_MovingTurnSpeed_2)); }
	inline float get_m_MovingTurnSpeed_2() const { return ___m_MovingTurnSpeed_2; }
	inline float* get_address_of_m_MovingTurnSpeed_2() { return &___m_MovingTurnSpeed_2; }
	inline void set_m_MovingTurnSpeed_2(float value)
	{
		___m_MovingTurnSpeed_2 = value;
	}

	inline static int32_t get_offset_of_m_StationaryTurnSpeed_3() { return static_cast<int32_t>(offsetof(ThirdPersonCharacter_t1426137179, ___m_StationaryTurnSpeed_3)); }
	inline float get_m_StationaryTurnSpeed_3() const { return ___m_StationaryTurnSpeed_3; }
	inline float* get_address_of_m_StationaryTurnSpeed_3() { return &___m_StationaryTurnSpeed_3; }
	inline void set_m_StationaryTurnSpeed_3(float value)
	{
		___m_StationaryTurnSpeed_3 = value;
	}

	inline static int32_t get_offset_of_m_JumpPower_4() { return static_cast<int32_t>(offsetof(ThirdPersonCharacter_t1426137179, ___m_JumpPower_4)); }
	inline float get_m_JumpPower_4() const { return ___m_JumpPower_4; }
	inline float* get_address_of_m_JumpPower_4() { return &___m_JumpPower_4; }
	inline void set_m_JumpPower_4(float value)
	{
		___m_JumpPower_4 = value;
	}

	inline static int32_t get_offset_of_m_GravityMultiplier_5() { return static_cast<int32_t>(offsetof(ThirdPersonCharacter_t1426137179, ___m_GravityMultiplier_5)); }
	inline float get_m_GravityMultiplier_5() const { return ___m_GravityMultiplier_5; }
	inline float* get_address_of_m_GravityMultiplier_5() { return &___m_GravityMultiplier_5; }
	inline void set_m_GravityMultiplier_5(float value)
	{
		___m_GravityMultiplier_5 = value;
	}

	inline static int32_t get_offset_of_m_RunCycleLegOffset_6() { return static_cast<int32_t>(offsetof(ThirdPersonCharacter_t1426137179, ___m_RunCycleLegOffset_6)); }
	inline float get_m_RunCycleLegOffset_6() const { return ___m_RunCycleLegOffset_6; }
	inline float* get_address_of_m_RunCycleLegOffset_6() { return &___m_RunCycleLegOffset_6; }
	inline void set_m_RunCycleLegOffset_6(float value)
	{
		___m_RunCycleLegOffset_6 = value;
	}

	inline static int32_t get_offset_of_m_MoveSpeedMultiplier_7() { return static_cast<int32_t>(offsetof(ThirdPersonCharacter_t1426137179, ___m_MoveSpeedMultiplier_7)); }
	inline float get_m_MoveSpeedMultiplier_7() const { return ___m_MoveSpeedMultiplier_7; }
	inline float* get_address_of_m_MoveSpeedMultiplier_7() { return &___m_MoveSpeedMultiplier_7; }
	inline void set_m_MoveSpeedMultiplier_7(float value)
	{
		___m_MoveSpeedMultiplier_7 = value;
	}

	inline static int32_t get_offset_of_m_AnimSpeedMultiplier_8() { return static_cast<int32_t>(offsetof(ThirdPersonCharacter_t1426137179, ___m_AnimSpeedMultiplier_8)); }
	inline float get_m_AnimSpeedMultiplier_8() const { return ___m_AnimSpeedMultiplier_8; }
	inline float* get_address_of_m_AnimSpeedMultiplier_8() { return &___m_AnimSpeedMultiplier_8; }
	inline void set_m_AnimSpeedMultiplier_8(float value)
	{
		___m_AnimSpeedMultiplier_8 = value;
	}

	inline static int32_t get_offset_of_m_GroundCheckDistance_9() { return static_cast<int32_t>(offsetof(ThirdPersonCharacter_t1426137179, ___m_GroundCheckDistance_9)); }
	inline float get_m_GroundCheckDistance_9() const { return ___m_GroundCheckDistance_9; }
	inline float* get_address_of_m_GroundCheckDistance_9() { return &___m_GroundCheckDistance_9; }
	inline void set_m_GroundCheckDistance_9(float value)
	{
		___m_GroundCheckDistance_9 = value;
	}

	inline static int32_t get_offset_of_m_Rigidbody_10() { return static_cast<int32_t>(offsetof(ThirdPersonCharacter_t1426137179, ___m_Rigidbody_10)); }
	inline Rigidbody_t2492269564 * get_m_Rigidbody_10() const { return ___m_Rigidbody_10; }
	inline Rigidbody_t2492269564 ** get_address_of_m_Rigidbody_10() { return &___m_Rigidbody_10; }
	inline void set_m_Rigidbody_10(Rigidbody_t2492269564 * value)
	{
		___m_Rigidbody_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_Rigidbody_10), value);
	}

	inline static int32_t get_offset_of_m_Animator_11() { return static_cast<int32_t>(offsetof(ThirdPersonCharacter_t1426137179, ___m_Animator_11)); }
	inline Animator_t4075492284 * get_m_Animator_11() const { return ___m_Animator_11; }
	inline Animator_t4075492284 ** get_address_of_m_Animator_11() { return &___m_Animator_11; }
	inline void set_m_Animator_11(Animator_t4075492284 * value)
	{
		___m_Animator_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_Animator_11), value);
	}

	inline static int32_t get_offset_of_m_IsGrounded_12() { return static_cast<int32_t>(offsetof(ThirdPersonCharacter_t1426137179, ___m_IsGrounded_12)); }
	inline bool get_m_IsGrounded_12() const { return ___m_IsGrounded_12; }
	inline bool* get_address_of_m_IsGrounded_12() { return &___m_IsGrounded_12; }
	inline void set_m_IsGrounded_12(bool value)
	{
		___m_IsGrounded_12 = value;
	}

	inline static int32_t get_offset_of_m_OrigGroundCheckDistance_13() { return static_cast<int32_t>(offsetof(ThirdPersonCharacter_t1426137179, ___m_OrigGroundCheckDistance_13)); }
	inline float get_m_OrigGroundCheckDistance_13() const { return ___m_OrigGroundCheckDistance_13; }
	inline float* get_address_of_m_OrigGroundCheckDistance_13() { return &___m_OrigGroundCheckDistance_13; }
	inline void set_m_OrigGroundCheckDistance_13(float value)
	{
		___m_OrigGroundCheckDistance_13 = value;
	}

	inline static int32_t get_offset_of_m_TurnAmount_15() { return static_cast<int32_t>(offsetof(ThirdPersonCharacter_t1426137179, ___m_TurnAmount_15)); }
	inline float get_m_TurnAmount_15() const { return ___m_TurnAmount_15; }
	inline float* get_address_of_m_TurnAmount_15() { return &___m_TurnAmount_15; }
	inline void set_m_TurnAmount_15(float value)
	{
		___m_TurnAmount_15 = value;
	}

	inline static int32_t get_offset_of_m_ForwardAmount_16() { return static_cast<int32_t>(offsetof(ThirdPersonCharacter_t1426137179, ___m_ForwardAmount_16)); }
	inline float get_m_ForwardAmount_16() const { return ___m_ForwardAmount_16; }
	inline float* get_address_of_m_ForwardAmount_16() { return &___m_ForwardAmount_16; }
	inline void set_m_ForwardAmount_16(float value)
	{
		___m_ForwardAmount_16 = value;
	}

	inline static int32_t get_offset_of_m_GroundNormal_17() { return static_cast<int32_t>(offsetof(ThirdPersonCharacter_t1426137179, ___m_GroundNormal_17)); }
	inline Vector3_t2903530434  get_m_GroundNormal_17() const { return ___m_GroundNormal_17; }
	inline Vector3_t2903530434 * get_address_of_m_GroundNormal_17() { return &___m_GroundNormal_17; }
	inline void set_m_GroundNormal_17(Vector3_t2903530434  value)
	{
		___m_GroundNormal_17 = value;
	}

	inline static int32_t get_offset_of_m_CapsuleHeight_18() { return static_cast<int32_t>(offsetof(ThirdPersonCharacter_t1426137179, ___m_CapsuleHeight_18)); }
	inline float get_m_CapsuleHeight_18() const { return ___m_CapsuleHeight_18; }
	inline float* get_address_of_m_CapsuleHeight_18() { return &___m_CapsuleHeight_18; }
	inline void set_m_CapsuleHeight_18(float value)
	{
		___m_CapsuleHeight_18 = value;
	}

	inline static int32_t get_offset_of_m_CapsuleCenter_19() { return static_cast<int32_t>(offsetof(ThirdPersonCharacter_t1426137179, ___m_CapsuleCenter_19)); }
	inline Vector3_t2903530434  get_m_CapsuleCenter_19() const { return ___m_CapsuleCenter_19; }
	inline Vector3_t2903530434 * get_address_of_m_CapsuleCenter_19() { return &___m_CapsuleCenter_19; }
	inline void set_m_CapsuleCenter_19(Vector3_t2903530434  value)
	{
		___m_CapsuleCenter_19 = value;
	}

	inline static int32_t get_offset_of_m_Capsule_20() { return static_cast<int32_t>(offsetof(ThirdPersonCharacter_t1426137179, ___m_Capsule_20)); }
	inline CapsuleCollider_t77672896 * get_m_Capsule_20() const { return ___m_Capsule_20; }
	inline CapsuleCollider_t77672896 ** get_address_of_m_Capsule_20() { return &___m_Capsule_20; }
	inline void set_m_Capsule_20(CapsuleCollider_t77672896 * value)
	{
		___m_Capsule_20 = value;
		Il2CppCodeGenWriteBarrier((&___m_Capsule_20), value);
	}

	inline static int32_t get_offset_of_m_Crouching_21() { return static_cast<int32_t>(offsetof(ThirdPersonCharacter_t1426137179, ___m_Crouching_21)); }
	inline bool get_m_Crouching_21() const { return ___m_Crouching_21; }
	inline bool* get_address_of_m_Crouching_21() { return &___m_Crouching_21; }
	inline void set_m_Crouching_21(bool value)
	{
		___m_Crouching_21 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // THIRDPERSONCHARACTER_T1426137179_H
#ifndef OBJECTRESETTER_T1579296102_H
#define OBJECTRESETTER_T1579296102_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.ObjectResetter
struct  ObjectResetter_t1579296102  : public MonoBehaviour_t345688271
{
public:
	// UnityEngine.Vector3 UnityStandardAssets.Utility.ObjectResetter::originalPosition
	Vector3_t2903530434  ___originalPosition_2;
	// UnityEngine.Quaternion UnityStandardAssets.Utility.ObjectResetter::originalRotation
	Quaternion_t754065749  ___originalRotation_3;
	// System.Collections.Generic.List`1<UnityEngine.Transform> UnityStandardAssets.Utility.ObjectResetter::originalStructure
	List_1_t2171591805 * ___originalStructure_4;
	// UnityEngine.Rigidbody UnityStandardAssets.Utility.ObjectResetter::Rigidbody
	Rigidbody_t2492269564 * ___Rigidbody_5;

public:
	inline static int32_t get_offset_of_originalPosition_2() { return static_cast<int32_t>(offsetof(ObjectResetter_t1579296102, ___originalPosition_2)); }
	inline Vector3_t2903530434  get_originalPosition_2() const { return ___originalPosition_2; }
	inline Vector3_t2903530434 * get_address_of_originalPosition_2() { return &___originalPosition_2; }
	inline void set_originalPosition_2(Vector3_t2903530434  value)
	{
		___originalPosition_2 = value;
	}

	inline static int32_t get_offset_of_originalRotation_3() { return static_cast<int32_t>(offsetof(ObjectResetter_t1579296102, ___originalRotation_3)); }
	inline Quaternion_t754065749  get_originalRotation_3() const { return ___originalRotation_3; }
	inline Quaternion_t754065749 * get_address_of_originalRotation_3() { return &___originalRotation_3; }
	inline void set_originalRotation_3(Quaternion_t754065749  value)
	{
		___originalRotation_3 = value;
	}

	inline static int32_t get_offset_of_originalStructure_4() { return static_cast<int32_t>(offsetof(ObjectResetter_t1579296102, ___originalStructure_4)); }
	inline List_1_t2171591805 * get_originalStructure_4() const { return ___originalStructure_4; }
	inline List_1_t2171591805 ** get_address_of_originalStructure_4() { return &___originalStructure_4; }
	inline void set_originalStructure_4(List_1_t2171591805 * value)
	{
		___originalStructure_4 = value;
		Il2CppCodeGenWriteBarrier((&___originalStructure_4), value);
	}

	inline static int32_t get_offset_of_Rigidbody_5() { return static_cast<int32_t>(offsetof(ObjectResetter_t1579296102, ___Rigidbody_5)); }
	inline Rigidbody_t2492269564 * get_Rigidbody_5() const { return ___Rigidbody_5; }
	inline Rigidbody_t2492269564 ** get_address_of_Rigidbody_5() { return &___Rigidbody_5; }
	inline void set_Rigidbody_5(Rigidbody_t2492269564 * value)
	{
		___Rigidbody_5 = value;
		Il2CppCodeGenWriteBarrier((&___Rigidbody_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OBJECTRESETTER_T1579296102_H
#ifndef THIRDPERSONUSERCONTROL_T3559085786_H
#define THIRDPERSONUSERCONTROL_T3559085786_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Characters.ThirdPerson.ThirdPersonUserControl
struct  ThirdPersonUserControl_t3559085786  : public MonoBehaviour_t345688271
{
public:
	// UnityStandardAssets.Characters.ThirdPerson.ThirdPersonCharacter UnityStandardAssets.Characters.ThirdPerson.ThirdPersonUserControl::m_Character
	ThirdPersonCharacter_t1426137179 * ___m_Character_2;
	// UnityEngine.Transform UnityStandardAssets.Characters.ThirdPerson.ThirdPersonUserControl::m_Cam
	Transform_t3316442598 * ___m_Cam_3;
	// UnityEngine.Vector3 UnityStandardAssets.Characters.ThirdPerson.ThirdPersonUserControl::m_CamForward
	Vector3_t2903530434  ___m_CamForward_4;
	// UnityEngine.Vector3 UnityStandardAssets.Characters.ThirdPerson.ThirdPersonUserControl::m_Move
	Vector3_t2903530434  ___m_Move_5;
	// System.Boolean UnityStandardAssets.Characters.ThirdPerson.ThirdPersonUserControl::m_Jump
	bool ___m_Jump_6;

public:
	inline static int32_t get_offset_of_m_Character_2() { return static_cast<int32_t>(offsetof(ThirdPersonUserControl_t3559085786, ___m_Character_2)); }
	inline ThirdPersonCharacter_t1426137179 * get_m_Character_2() const { return ___m_Character_2; }
	inline ThirdPersonCharacter_t1426137179 ** get_address_of_m_Character_2() { return &___m_Character_2; }
	inline void set_m_Character_2(ThirdPersonCharacter_t1426137179 * value)
	{
		___m_Character_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_Character_2), value);
	}

	inline static int32_t get_offset_of_m_Cam_3() { return static_cast<int32_t>(offsetof(ThirdPersonUserControl_t3559085786, ___m_Cam_3)); }
	inline Transform_t3316442598 * get_m_Cam_3() const { return ___m_Cam_3; }
	inline Transform_t3316442598 ** get_address_of_m_Cam_3() { return &___m_Cam_3; }
	inline void set_m_Cam_3(Transform_t3316442598 * value)
	{
		___m_Cam_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_Cam_3), value);
	}

	inline static int32_t get_offset_of_m_CamForward_4() { return static_cast<int32_t>(offsetof(ThirdPersonUserControl_t3559085786, ___m_CamForward_4)); }
	inline Vector3_t2903530434  get_m_CamForward_4() const { return ___m_CamForward_4; }
	inline Vector3_t2903530434 * get_address_of_m_CamForward_4() { return &___m_CamForward_4; }
	inline void set_m_CamForward_4(Vector3_t2903530434  value)
	{
		___m_CamForward_4 = value;
	}

	inline static int32_t get_offset_of_m_Move_5() { return static_cast<int32_t>(offsetof(ThirdPersonUserControl_t3559085786, ___m_Move_5)); }
	inline Vector3_t2903530434  get_m_Move_5() const { return ___m_Move_5; }
	inline Vector3_t2903530434 * get_address_of_m_Move_5() { return &___m_Move_5; }
	inline void set_m_Move_5(Vector3_t2903530434  value)
	{
		___m_Move_5 = value;
	}

	inline static int32_t get_offset_of_m_Jump_6() { return static_cast<int32_t>(offsetof(ThirdPersonUserControl_t3559085786, ___m_Jump_6)); }
	inline bool get_m_Jump_6() const { return ___m_Jump_6; }
	inline bool* get_address_of_m_Jump_6() { return &___m_Jump_6; }
	inline void set_m_Jump_6(bool value)
	{
		___m_Jump_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // THIRDPERSONUSERCONTROL_T3559085786_H
#ifndef AXISTOUCHBUTTON_T693686604_H
#define AXISTOUCHBUTTON_T693686604_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.CrossPlatformInput.AxisTouchButton
struct  AxisTouchButton_t693686604  : public MonoBehaviour_t345688271
{
public:
	// System.String UnityStandardAssets.CrossPlatformInput.AxisTouchButton::axisName
	String_t* ___axisName_2;
	// System.Single UnityStandardAssets.CrossPlatformInput.AxisTouchButton::axisValue
	float ___axisValue_3;
	// System.Single UnityStandardAssets.CrossPlatformInput.AxisTouchButton::responseSpeed
	float ___responseSpeed_4;
	// System.Single UnityStandardAssets.CrossPlatformInput.AxisTouchButton::returnToCentreSpeed
	float ___returnToCentreSpeed_5;
	// UnityStandardAssets.CrossPlatformInput.AxisTouchButton UnityStandardAssets.CrossPlatformInput.AxisTouchButton::m_PairedWith
	AxisTouchButton_t693686604 * ___m_PairedWith_6;
	// UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualAxis UnityStandardAssets.CrossPlatformInput.AxisTouchButton::m_Axis
	VirtualAxis_t2383171311 * ___m_Axis_7;

public:
	inline static int32_t get_offset_of_axisName_2() { return static_cast<int32_t>(offsetof(AxisTouchButton_t693686604, ___axisName_2)); }
	inline String_t* get_axisName_2() const { return ___axisName_2; }
	inline String_t** get_address_of_axisName_2() { return &___axisName_2; }
	inline void set_axisName_2(String_t* value)
	{
		___axisName_2 = value;
		Il2CppCodeGenWriteBarrier((&___axisName_2), value);
	}

	inline static int32_t get_offset_of_axisValue_3() { return static_cast<int32_t>(offsetof(AxisTouchButton_t693686604, ___axisValue_3)); }
	inline float get_axisValue_3() const { return ___axisValue_3; }
	inline float* get_address_of_axisValue_3() { return &___axisValue_3; }
	inline void set_axisValue_3(float value)
	{
		___axisValue_3 = value;
	}

	inline static int32_t get_offset_of_responseSpeed_4() { return static_cast<int32_t>(offsetof(AxisTouchButton_t693686604, ___responseSpeed_4)); }
	inline float get_responseSpeed_4() const { return ___responseSpeed_4; }
	inline float* get_address_of_responseSpeed_4() { return &___responseSpeed_4; }
	inline void set_responseSpeed_4(float value)
	{
		___responseSpeed_4 = value;
	}

	inline static int32_t get_offset_of_returnToCentreSpeed_5() { return static_cast<int32_t>(offsetof(AxisTouchButton_t693686604, ___returnToCentreSpeed_5)); }
	inline float get_returnToCentreSpeed_5() const { return ___returnToCentreSpeed_5; }
	inline float* get_address_of_returnToCentreSpeed_5() { return &___returnToCentreSpeed_5; }
	inline void set_returnToCentreSpeed_5(float value)
	{
		___returnToCentreSpeed_5 = value;
	}

	inline static int32_t get_offset_of_m_PairedWith_6() { return static_cast<int32_t>(offsetof(AxisTouchButton_t693686604, ___m_PairedWith_6)); }
	inline AxisTouchButton_t693686604 * get_m_PairedWith_6() const { return ___m_PairedWith_6; }
	inline AxisTouchButton_t693686604 ** get_address_of_m_PairedWith_6() { return &___m_PairedWith_6; }
	inline void set_m_PairedWith_6(AxisTouchButton_t693686604 * value)
	{
		___m_PairedWith_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_PairedWith_6), value);
	}

	inline static int32_t get_offset_of_m_Axis_7() { return static_cast<int32_t>(offsetof(AxisTouchButton_t693686604, ___m_Axis_7)); }
	inline VirtualAxis_t2383171311 * get_m_Axis_7() const { return ___m_Axis_7; }
	inline VirtualAxis_t2383171311 ** get_address_of_m_Axis_7() { return &___m_Axis_7; }
	inline void set_m_Axis_7(VirtualAxis_t2383171311 * value)
	{
		___m_Axis_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_Axis_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AXISTOUCHBUTTON_T693686604_H
#ifndef FPSCOUNTER_T2273183518_H
#define FPSCOUNTER_T2273183518_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.FPSCounter
struct  FPSCounter_t2273183518  : public MonoBehaviour_t345688271
{
public:
	// System.Int32 UnityStandardAssets.Utility.FPSCounter::m_FpsAccumulator
	int32_t ___m_FpsAccumulator_3;
	// System.Single UnityStandardAssets.Utility.FPSCounter::m_FpsNextPeriod
	float ___m_FpsNextPeriod_4;
	// System.Int32 UnityStandardAssets.Utility.FPSCounter::m_CurrentFps
	int32_t ___m_CurrentFps_5;
	// UnityEngine.UI.Text UnityStandardAssets.Utility.FPSCounter::m_Text
	Text_t3057673244 * ___m_Text_7;

public:
	inline static int32_t get_offset_of_m_FpsAccumulator_3() { return static_cast<int32_t>(offsetof(FPSCounter_t2273183518, ___m_FpsAccumulator_3)); }
	inline int32_t get_m_FpsAccumulator_3() const { return ___m_FpsAccumulator_3; }
	inline int32_t* get_address_of_m_FpsAccumulator_3() { return &___m_FpsAccumulator_3; }
	inline void set_m_FpsAccumulator_3(int32_t value)
	{
		___m_FpsAccumulator_3 = value;
	}

	inline static int32_t get_offset_of_m_FpsNextPeriod_4() { return static_cast<int32_t>(offsetof(FPSCounter_t2273183518, ___m_FpsNextPeriod_4)); }
	inline float get_m_FpsNextPeriod_4() const { return ___m_FpsNextPeriod_4; }
	inline float* get_address_of_m_FpsNextPeriod_4() { return &___m_FpsNextPeriod_4; }
	inline void set_m_FpsNextPeriod_4(float value)
	{
		___m_FpsNextPeriod_4 = value;
	}

	inline static int32_t get_offset_of_m_CurrentFps_5() { return static_cast<int32_t>(offsetof(FPSCounter_t2273183518, ___m_CurrentFps_5)); }
	inline int32_t get_m_CurrentFps_5() const { return ___m_CurrentFps_5; }
	inline int32_t* get_address_of_m_CurrentFps_5() { return &___m_CurrentFps_5; }
	inline void set_m_CurrentFps_5(int32_t value)
	{
		___m_CurrentFps_5 = value;
	}

	inline static int32_t get_offset_of_m_Text_7() { return static_cast<int32_t>(offsetof(FPSCounter_t2273183518, ___m_Text_7)); }
	inline Text_t3057673244 * get_m_Text_7() const { return ___m_Text_7; }
	inline Text_t3057673244 ** get_address_of_m_Text_7() { return &___m_Text_7; }
	inline void set_m_Text_7(Text_t3057673244 * value)
	{
		___m_Text_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_Text_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FPSCOUNTER_T2273183518_H
#ifndef BUTTONHANDLER_T1472824933_H
#define BUTTONHANDLER_T1472824933_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.CrossPlatformInput.ButtonHandler
struct  ButtonHandler_t1472824933  : public MonoBehaviour_t345688271
{
public:
	// System.String UnityStandardAssets.CrossPlatformInput.ButtonHandler::Name
	String_t* ___Name_2;

public:
	inline static int32_t get_offset_of_Name_2() { return static_cast<int32_t>(offsetof(ButtonHandler_t1472824933, ___Name_2)); }
	inline String_t* get_Name_2() const { return ___Name_2; }
	inline String_t** get_address_of_Name_2() { return &___Name_2; }
	inline void set_Name_2(String_t* value)
	{
		___Name_2 = value;
		Il2CppCodeGenWriteBarrier((&___Name_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUTTONHANDLER_T1472824933_H
#ifndef FORCEDRESET_T3887116877_H
#define FORCEDRESET_T3887116877_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ForcedReset
struct  ForcedReset_t3887116877  : public MonoBehaviour_t345688271
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FORCEDRESET_T3887116877_H
#ifndef PLATFORMSPECIFICCONTENT_T328033666_H
#define PLATFORMSPECIFICCONTENT_T328033666_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.PlatformSpecificContent
struct  PlatformSpecificContent_t328033666  : public MonoBehaviour_t345688271
{
public:
	// UnityStandardAssets.Utility.PlatformSpecificContent/BuildTargetGroup UnityStandardAssets.Utility.PlatformSpecificContent::m_BuildTargetGroup
	int32_t ___m_BuildTargetGroup_2;
	// UnityEngine.GameObject[] UnityStandardAssets.Utility.PlatformSpecificContent::m_Content
	GameObjectU5BU5D_t2021098027* ___m_Content_3;
	// UnityEngine.MonoBehaviour[] UnityStandardAssets.Utility.PlatformSpecificContent::m_MonoBehaviours
	MonoBehaviourU5BU5D_t1369622294* ___m_MonoBehaviours_4;
	// System.Boolean UnityStandardAssets.Utility.PlatformSpecificContent::m_ChildrenOfThisObject
	bool ___m_ChildrenOfThisObject_5;

public:
	inline static int32_t get_offset_of_m_BuildTargetGroup_2() { return static_cast<int32_t>(offsetof(PlatformSpecificContent_t328033666, ___m_BuildTargetGroup_2)); }
	inline int32_t get_m_BuildTargetGroup_2() const { return ___m_BuildTargetGroup_2; }
	inline int32_t* get_address_of_m_BuildTargetGroup_2() { return &___m_BuildTargetGroup_2; }
	inline void set_m_BuildTargetGroup_2(int32_t value)
	{
		___m_BuildTargetGroup_2 = value;
	}

	inline static int32_t get_offset_of_m_Content_3() { return static_cast<int32_t>(offsetof(PlatformSpecificContent_t328033666, ___m_Content_3)); }
	inline GameObjectU5BU5D_t2021098027* get_m_Content_3() const { return ___m_Content_3; }
	inline GameObjectU5BU5D_t2021098027** get_address_of_m_Content_3() { return &___m_Content_3; }
	inline void set_m_Content_3(GameObjectU5BU5D_t2021098027* value)
	{
		___m_Content_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_Content_3), value);
	}

	inline static int32_t get_offset_of_m_MonoBehaviours_4() { return static_cast<int32_t>(offsetof(PlatformSpecificContent_t328033666, ___m_MonoBehaviours_4)); }
	inline MonoBehaviourU5BU5D_t1369622294* get_m_MonoBehaviours_4() const { return ___m_MonoBehaviours_4; }
	inline MonoBehaviourU5BU5D_t1369622294** get_address_of_m_MonoBehaviours_4() { return &___m_MonoBehaviours_4; }
	inline void set_m_MonoBehaviours_4(MonoBehaviourU5BU5D_t1369622294* value)
	{
		___m_MonoBehaviours_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_MonoBehaviours_4), value);
	}

	inline static int32_t get_offset_of_m_ChildrenOfThisObject_5() { return static_cast<int32_t>(offsetof(PlatformSpecificContent_t328033666, ___m_ChildrenOfThisObject_5)); }
	inline bool get_m_ChildrenOfThisObject_5() const { return ___m_ChildrenOfThisObject_5; }
	inline bool* get_address_of_m_ChildrenOfThisObject_5() { return &___m_ChildrenOfThisObject_5; }
	inline void set_m_ChildrenOfThisObject_5(bool value)
	{
		___m_ChildrenOfThisObject_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLATFORMSPECIFICCONTENT_T328033666_H
#ifndef FOLLOWTARGET_T702953223_H
#define FOLLOWTARGET_T702953223_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.FollowTarget
struct  FollowTarget_t702953223  : public MonoBehaviour_t345688271
{
public:
	// UnityEngine.Transform UnityStandardAssets.Utility.FollowTarget::target
	Transform_t3316442598 * ___target_2;
	// UnityEngine.Vector3 UnityStandardAssets.Utility.FollowTarget::offset
	Vector3_t2903530434  ___offset_3;

public:
	inline static int32_t get_offset_of_target_2() { return static_cast<int32_t>(offsetof(FollowTarget_t702953223, ___target_2)); }
	inline Transform_t3316442598 * get_target_2() const { return ___target_2; }
	inline Transform_t3316442598 ** get_address_of_target_2() { return &___target_2; }
	inline void set_target_2(Transform_t3316442598 * value)
	{
		___target_2 = value;
		Il2CppCodeGenWriteBarrier((&___target_2), value);
	}

	inline static int32_t get_offset_of_offset_3() { return static_cast<int32_t>(offsetof(FollowTarget_t702953223, ___offset_3)); }
	inline Vector3_t2903530434  get_offset_3() const { return ___offset_3; }
	inline Vector3_t2903530434 * get_address_of_offset_3() { return &___offset_3; }
	inline void set_offset_3(Vector3_t2903530434  value)
	{
		___offset_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FOLLOWTARGET_T702953223_H
#ifndef DYNAMICSHADOWSETTINGS_T3505630543_H
#define DYNAMICSHADOWSETTINGS_T3505630543_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.DynamicShadowSettings
struct  DynamicShadowSettings_t3505630543  : public MonoBehaviour_t345688271
{
public:
	// UnityEngine.Light UnityStandardAssets.Utility.DynamicShadowSettings::sunLight
	Light_t3435818087 * ___sunLight_2;
	// System.Single UnityStandardAssets.Utility.DynamicShadowSettings::minHeight
	float ___minHeight_3;
	// System.Single UnityStandardAssets.Utility.DynamicShadowSettings::minShadowDistance
	float ___minShadowDistance_4;
	// System.Single UnityStandardAssets.Utility.DynamicShadowSettings::minShadowBias
	float ___minShadowBias_5;
	// System.Single UnityStandardAssets.Utility.DynamicShadowSettings::maxHeight
	float ___maxHeight_6;
	// System.Single UnityStandardAssets.Utility.DynamicShadowSettings::maxShadowDistance
	float ___maxShadowDistance_7;
	// System.Single UnityStandardAssets.Utility.DynamicShadowSettings::maxShadowBias
	float ___maxShadowBias_8;
	// System.Single UnityStandardAssets.Utility.DynamicShadowSettings::adaptTime
	float ___adaptTime_9;
	// System.Single UnityStandardAssets.Utility.DynamicShadowSettings::m_SmoothHeight
	float ___m_SmoothHeight_10;
	// System.Single UnityStandardAssets.Utility.DynamicShadowSettings::m_ChangeSpeed
	float ___m_ChangeSpeed_11;
	// System.Single UnityStandardAssets.Utility.DynamicShadowSettings::m_OriginalStrength
	float ___m_OriginalStrength_12;

public:
	inline static int32_t get_offset_of_sunLight_2() { return static_cast<int32_t>(offsetof(DynamicShadowSettings_t3505630543, ___sunLight_2)); }
	inline Light_t3435818087 * get_sunLight_2() const { return ___sunLight_2; }
	inline Light_t3435818087 ** get_address_of_sunLight_2() { return &___sunLight_2; }
	inline void set_sunLight_2(Light_t3435818087 * value)
	{
		___sunLight_2 = value;
		Il2CppCodeGenWriteBarrier((&___sunLight_2), value);
	}

	inline static int32_t get_offset_of_minHeight_3() { return static_cast<int32_t>(offsetof(DynamicShadowSettings_t3505630543, ___minHeight_3)); }
	inline float get_minHeight_3() const { return ___minHeight_3; }
	inline float* get_address_of_minHeight_3() { return &___minHeight_3; }
	inline void set_minHeight_3(float value)
	{
		___minHeight_3 = value;
	}

	inline static int32_t get_offset_of_minShadowDistance_4() { return static_cast<int32_t>(offsetof(DynamicShadowSettings_t3505630543, ___minShadowDistance_4)); }
	inline float get_minShadowDistance_4() const { return ___minShadowDistance_4; }
	inline float* get_address_of_minShadowDistance_4() { return &___minShadowDistance_4; }
	inline void set_minShadowDistance_4(float value)
	{
		___minShadowDistance_4 = value;
	}

	inline static int32_t get_offset_of_minShadowBias_5() { return static_cast<int32_t>(offsetof(DynamicShadowSettings_t3505630543, ___minShadowBias_5)); }
	inline float get_minShadowBias_5() const { return ___minShadowBias_5; }
	inline float* get_address_of_minShadowBias_5() { return &___minShadowBias_5; }
	inline void set_minShadowBias_5(float value)
	{
		___minShadowBias_5 = value;
	}

	inline static int32_t get_offset_of_maxHeight_6() { return static_cast<int32_t>(offsetof(DynamicShadowSettings_t3505630543, ___maxHeight_6)); }
	inline float get_maxHeight_6() const { return ___maxHeight_6; }
	inline float* get_address_of_maxHeight_6() { return &___maxHeight_6; }
	inline void set_maxHeight_6(float value)
	{
		___maxHeight_6 = value;
	}

	inline static int32_t get_offset_of_maxShadowDistance_7() { return static_cast<int32_t>(offsetof(DynamicShadowSettings_t3505630543, ___maxShadowDistance_7)); }
	inline float get_maxShadowDistance_7() const { return ___maxShadowDistance_7; }
	inline float* get_address_of_maxShadowDistance_7() { return &___maxShadowDistance_7; }
	inline void set_maxShadowDistance_7(float value)
	{
		___maxShadowDistance_7 = value;
	}

	inline static int32_t get_offset_of_maxShadowBias_8() { return static_cast<int32_t>(offsetof(DynamicShadowSettings_t3505630543, ___maxShadowBias_8)); }
	inline float get_maxShadowBias_8() const { return ___maxShadowBias_8; }
	inline float* get_address_of_maxShadowBias_8() { return &___maxShadowBias_8; }
	inline void set_maxShadowBias_8(float value)
	{
		___maxShadowBias_8 = value;
	}

	inline static int32_t get_offset_of_adaptTime_9() { return static_cast<int32_t>(offsetof(DynamicShadowSettings_t3505630543, ___adaptTime_9)); }
	inline float get_adaptTime_9() const { return ___adaptTime_9; }
	inline float* get_address_of_adaptTime_9() { return &___adaptTime_9; }
	inline void set_adaptTime_9(float value)
	{
		___adaptTime_9 = value;
	}

	inline static int32_t get_offset_of_m_SmoothHeight_10() { return static_cast<int32_t>(offsetof(DynamicShadowSettings_t3505630543, ___m_SmoothHeight_10)); }
	inline float get_m_SmoothHeight_10() const { return ___m_SmoothHeight_10; }
	inline float* get_address_of_m_SmoothHeight_10() { return &___m_SmoothHeight_10; }
	inline void set_m_SmoothHeight_10(float value)
	{
		___m_SmoothHeight_10 = value;
	}

	inline static int32_t get_offset_of_m_ChangeSpeed_11() { return static_cast<int32_t>(offsetof(DynamicShadowSettings_t3505630543, ___m_ChangeSpeed_11)); }
	inline float get_m_ChangeSpeed_11() const { return ___m_ChangeSpeed_11; }
	inline float* get_address_of_m_ChangeSpeed_11() { return &___m_ChangeSpeed_11; }
	inline void set_m_ChangeSpeed_11(float value)
	{
		___m_ChangeSpeed_11 = value;
	}

	inline static int32_t get_offset_of_m_OriginalStrength_12() { return static_cast<int32_t>(offsetof(DynamicShadowSettings_t3505630543, ___m_OriginalStrength_12)); }
	inline float get_m_OriginalStrength_12() const { return ___m_OriginalStrength_12; }
	inline float* get_address_of_m_OriginalStrength_12() { return &___m_OriginalStrength_12; }
	inline void set_m_OriginalStrength_12(float value)
	{
		___m_OriginalStrength_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DYNAMICSHADOWSETTINGS_T3505630543_H
#ifndef DRAGRIGIDBODY_T781943330_H
#define DRAGRIGIDBODY_T781943330_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.DragRigidbody
struct  DragRigidbody_t781943330  : public MonoBehaviour_t345688271
{
public:
	// UnityEngine.SpringJoint UnityStandardAssets.Utility.DragRigidbody::m_SpringJoint
	SpringJoint_t4170598825 * ___m_SpringJoint_8;

public:
	inline static int32_t get_offset_of_m_SpringJoint_8() { return static_cast<int32_t>(offsetof(DragRigidbody_t781943330, ___m_SpringJoint_8)); }
	inline SpringJoint_t4170598825 * get_m_SpringJoint_8() const { return ___m_SpringJoint_8; }
	inline SpringJoint_t4170598825 ** get_address_of_m_SpringJoint_8() { return &___m_SpringJoint_8; }
	inline void set_m_SpringJoint_8(SpringJoint_t4170598825 * value)
	{
		___m_SpringJoint_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_SpringJoint_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DRAGRIGIDBODY_T781943330_H
#ifndef AUTOMOVEANDROTATE_T607824664_H
#define AUTOMOVEANDROTATE_T607824664_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.AutoMoveAndRotate
struct  AutoMoveAndRotate_t607824664  : public MonoBehaviour_t345688271
{
public:
	// UnityStandardAssets.Utility.AutoMoveAndRotate/Vector3andSpace UnityStandardAssets.Utility.AutoMoveAndRotate::moveUnitsPerSecond
	Vector3andSpace_t2340595124 * ___moveUnitsPerSecond_2;
	// UnityStandardAssets.Utility.AutoMoveAndRotate/Vector3andSpace UnityStandardAssets.Utility.AutoMoveAndRotate::rotateDegreesPerSecond
	Vector3andSpace_t2340595124 * ___rotateDegreesPerSecond_3;
	// System.Boolean UnityStandardAssets.Utility.AutoMoveAndRotate::ignoreTimescale
	bool ___ignoreTimescale_4;
	// System.Single UnityStandardAssets.Utility.AutoMoveAndRotate::m_LastRealTime
	float ___m_LastRealTime_5;

public:
	inline static int32_t get_offset_of_moveUnitsPerSecond_2() { return static_cast<int32_t>(offsetof(AutoMoveAndRotate_t607824664, ___moveUnitsPerSecond_2)); }
	inline Vector3andSpace_t2340595124 * get_moveUnitsPerSecond_2() const { return ___moveUnitsPerSecond_2; }
	inline Vector3andSpace_t2340595124 ** get_address_of_moveUnitsPerSecond_2() { return &___moveUnitsPerSecond_2; }
	inline void set_moveUnitsPerSecond_2(Vector3andSpace_t2340595124 * value)
	{
		___moveUnitsPerSecond_2 = value;
		Il2CppCodeGenWriteBarrier((&___moveUnitsPerSecond_2), value);
	}

	inline static int32_t get_offset_of_rotateDegreesPerSecond_3() { return static_cast<int32_t>(offsetof(AutoMoveAndRotate_t607824664, ___rotateDegreesPerSecond_3)); }
	inline Vector3andSpace_t2340595124 * get_rotateDegreesPerSecond_3() const { return ___rotateDegreesPerSecond_3; }
	inline Vector3andSpace_t2340595124 ** get_address_of_rotateDegreesPerSecond_3() { return &___rotateDegreesPerSecond_3; }
	inline void set_rotateDegreesPerSecond_3(Vector3andSpace_t2340595124 * value)
	{
		___rotateDegreesPerSecond_3 = value;
		Il2CppCodeGenWriteBarrier((&___rotateDegreesPerSecond_3), value);
	}

	inline static int32_t get_offset_of_ignoreTimescale_4() { return static_cast<int32_t>(offsetof(AutoMoveAndRotate_t607824664, ___ignoreTimescale_4)); }
	inline bool get_ignoreTimescale_4() const { return ___ignoreTimescale_4; }
	inline bool* get_address_of_ignoreTimescale_4() { return &___ignoreTimescale_4; }
	inline void set_ignoreTimescale_4(bool value)
	{
		___ignoreTimescale_4 = value;
	}

	inline static int32_t get_offset_of_m_LastRealTime_5() { return static_cast<int32_t>(offsetof(AutoMoveAndRotate_t607824664, ___m_LastRealTime_5)); }
	inline float get_m_LastRealTime_5() const { return ___m_LastRealTime_5; }
	inline float* get_address_of_m_LastRealTime_5() { return &___m_LastRealTime_5; }
	inline void set_m_LastRealTime_5(float value)
	{
		___m_LastRealTime_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AUTOMOVEANDROTATE_T607824664_H
#ifndef AUTOMOBILESHADERSWITCH_T608167732_H
#define AUTOMOBILESHADERSWITCH_T608167732_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.AutoMobileShaderSwitch
struct  AutoMobileShaderSwitch_t608167732  : public MonoBehaviour_t345688271
{
public:
	// UnityStandardAssets.Utility.AutoMobileShaderSwitch/ReplacementList UnityStandardAssets.Utility.AutoMobileShaderSwitch::m_ReplacementList
	ReplacementList_t22610105 * ___m_ReplacementList_2;

public:
	inline static int32_t get_offset_of_m_ReplacementList_2() { return static_cast<int32_t>(offsetof(AutoMobileShaderSwitch_t608167732, ___m_ReplacementList_2)); }
	inline ReplacementList_t22610105 * get_m_ReplacementList_2() const { return ___m_ReplacementList_2; }
	inline ReplacementList_t22610105 ** get_address_of_m_ReplacementList_2() { return &___m_ReplacementList_2; }
	inline void set_m_ReplacementList_2(ReplacementList_t22610105 * value)
	{
		___m_ReplacementList_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_ReplacementList_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AUTOMOBILESHADERSWITCH_T608167732_H
#ifndef ALPHABUTTONCLICKMASK_T2290582035_H
#define ALPHABUTTONCLICKMASK_T2290582035_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AlphaButtonClickMask
struct  AlphaButtonClickMask_t2290582035  : public MonoBehaviour_t345688271
{
public:
	// UnityEngine.UI.Image AlphaButtonClickMask::_image
	Image_t266201966 * ____image_2;

public:
	inline static int32_t get_offset_of__image_2() { return static_cast<int32_t>(offsetof(AlphaButtonClickMask_t2290582035, ____image_2)); }
	inline Image_t266201966 * get__image_2() const { return ____image_2; }
	inline Image_t266201966 ** get_address_of__image_2() { return &____image_2; }
	inline void set__image_2(Image_t266201966 * value)
	{
		____image_2 = value;
		Il2CppCodeGenWriteBarrier((&____image_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ALPHABUTTONCLICKMASK_T2290582035_H
#ifndef ACTIVATETRIGGER_T2507406930_H
#define ACTIVATETRIGGER_T2507406930_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.ActivateTrigger
struct  ActivateTrigger_t2507406930  : public MonoBehaviour_t345688271
{
public:
	// UnityStandardAssets.Utility.ActivateTrigger/Mode UnityStandardAssets.Utility.ActivateTrigger::action
	int32_t ___action_2;
	// UnityEngine.Object UnityStandardAssets.Utility.ActivateTrigger::target
	Object_t1502412432 * ___target_3;
	// UnityEngine.GameObject UnityStandardAssets.Utility.ActivateTrigger::source
	GameObject_t1811656094 * ___source_4;
	// System.Int32 UnityStandardAssets.Utility.ActivateTrigger::triggerCount
	int32_t ___triggerCount_5;
	// System.Boolean UnityStandardAssets.Utility.ActivateTrigger::repeatTrigger
	bool ___repeatTrigger_6;

public:
	inline static int32_t get_offset_of_action_2() { return static_cast<int32_t>(offsetof(ActivateTrigger_t2507406930, ___action_2)); }
	inline int32_t get_action_2() const { return ___action_2; }
	inline int32_t* get_address_of_action_2() { return &___action_2; }
	inline void set_action_2(int32_t value)
	{
		___action_2 = value;
	}

	inline static int32_t get_offset_of_target_3() { return static_cast<int32_t>(offsetof(ActivateTrigger_t2507406930, ___target_3)); }
	inline Object_t1502412432 * get_target_3() const { return ___target_3; }
	inline Object_t1502412432 ** get_address_of_target_3() { return &___target_3; }
	inline void set_target_3(Object_t1502412432 * value)
	{
		___target_3 = value;
		Il2CppCodeGenWriteBarrier((&___target_3), value);
	}

	inline static int32_t get_offset_of_source_4() { return static_cast<int32_t>(offsetof(ActivateTrigger_t2507406930, ___source_4)); }
	inline GameObject_t1811656094 * get_source_4() const { return ___source_4; }
	inline GameObject_t1811656094 ** get_address_of_source_4() { return &___source_4; }
	inline void set_source_4(GameObject_t1811656094 * value)
	{
		___source_4 = value;
		Il2CppCodeGenWriteBarrier((&___source_4), value);
	}

	inline static int32_t get_offset_of_triggerCount_5() { return static_cast<int32_t>(offsetof(ActivateTrigger_t2507406930, ___triggerCount_5)); }
	inline int32_t get_triggerCount_5() const { return ___triggerCount_5; }
	inline int32_t* get_address_of_triggerCount_5() { return &___triggerCount_5; }
	inline void set_triggerCount_5(int32_t value)
	{
		___triggerCount_5 = value;
	}

	inline static int32_t get_offset_of_repeatTrigger_6() { return static_cast<int32_t>(offsetof(ActivateTrigger_t2507406930, ___repeatTrigger_6)); }
	inline bool get_repeatTrigger_6() const { return ___repeatTrigger_6; }
	inline bool* get_address_of_repeatTrigger_6() { return &___repeatTrigger_6; }
	inline void set_repeatTrigger_6(bool value)
	{
		___repeatTrigger_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ACTIVATETRIGGER_T2507406930_H
#ifndef TOUCHPAD_T3890668177_H
#define TOUCHPAD_T3890668177_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.CrossPlatformInput.TouchPad
struct  TouchPad_t3890668177  : public MonoBehaviour_t345688271
{
public:
	// UnityStandardAssets.CrossPlatformInput.TouchPad/AxisOption UnityStandardAssets.CrossPlatformInput.TouchPad::axesToUse
	int32_t ___axesToUse_2;
	// UnityStandardAssets.CrossPlatformInput.TouchPad/ControlStyle UnityStandardAssets.CrossPlatformInput.TouchPad::controlStyle
	int32_t ___controlStyle_3;
	// System.String UnityStandardAssets.CrossPlatformInput.TouchPad::horizontalAxisName
	String_t* ___horizontalAxisName_4;
	// System.String UnityStandardAssets.CrossPlatformInput.TouchPad::verticalAxisName
	String_t* ___verticalAxisName_5;
	// System.Single UnityStandardAssets.CrossPlatformInput.TouchPad::Xsensitivity
	float ___Xsensitivity_6;
	// System.Single UnityStandardAssets.CrossPlatformInput.TouchPad::Ysensitivity
	float ___Ysensitivity_7;
	// UnityEngine.Vector3 UnityStandardAssets.CrossPlatformInput.TouchPad::m_StartPos
	Vector3_t2903530434  ___m_StartPos_8;
	// UnityEngine.Vector2 UnityStandardAssets.CrossPlatformInput.TouchPad::m_PreviousDelta
	Vector2_t3577333262  ___m_PreviousDelta_9;
	// UnityEngine.Vector3 UnityStandardAssets.CrossPlatformInput.TouchPad::m_JoytickOutput
	Vector3_t2903530434  ___m_JoytickOutput_10;
	// System.Boolean UnityStandardAssets.CrossPlatformInput.TouchPad::m_UseX
	bool ___m_UseX_11;
	// System.Boolean UnityStandardAssets.CrossPlatformInput.TouchPad::m_UseY
	bool ___m_UseY_12;
	// UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualAxis UnityStandardAssets.CrossPlatformInput.TouchPad::m_HorizontalVirtualAxis
	VirtualAxis_t2383171311 * ___m_HorizontalVirtualAxis_13;
	// UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualAxis UnityStandardAssets.CrossPlatformInput.TouchPad::m_VerticalVirtualAxis
	VirtualAxis_t2383171311 * ___m_VerticalVirtualAxis_14;
	// System.Boolean UnityStandardAssets.CrossPlatformInput.TouchPad::m_Dragging
	bool ___m_Dragging_15;
	// System.Int32 UnityStandardAssets.CrossPlatformInput.TouchPad::m_Id
	int32_t ___m_Id_16;
	// UnityEngine.Vector2 UnityStandardAssets.CrossPlatformInput.TouchPad::m_PreviousTouchPos
	Vector2_t3577333262  ___m_PreviousTouchPos_17;
	// UnityEngine.Vector3 UnityStandardAssets.CrossPlatformInput.TouchPad::m_Center
	Vector3_t2903530434  ___m_Center_18;
	// UnityEngine.UI.Image UnityStandardAssets.CrossPlatformInput.TouchPad::m_Image
	Image_t266201966 * ___m_Image_19;

public:
	inline static int32_t get_offset_of_axesToUse_2() { return static_cast<int32_t>(offsetof(TouchPad_t3890668177, ___axesToUse_2)); }
	inline int32_t get_axesToUse_2() const { return ___axesToUse_2; }
	inline int32_t* get_address_of_axesToUse_2() { return &___axesToUse_2; }
	inline void set_axesToUse_2(int32_t value)
	{
		___axesToUse_2 = value;
	}

	inline static int32_t get_offset_of_controlStyle_3() { return static_cast<int32_t>(offsetof(TouchPad_t3890668177, ___controlStyle_3)); }
	inline int32_t get_controlStyle_3() const { return ___controlStyle_3; }
	inline int32_t* get_address_of_controlStyle_3() { return &___controlStyle_3; }
	inline void set_controlStyle_3(int32_t value)
	{
		___controlStyle_3 = value;
	}

	inline static int32_t get_offset_of_horizontalAxisName_4() { return static_cast<int32_t>(offsetof(TouchPad_t3890668177, ___horizontalAxisName_4)); }
	inline String_t* get_horizontalAxisName_4() const { return ___horizontalAxisName_4; }
	inline String_t** get_address_of_horizontalAxisName_4() { return &___horizontalAxisName_4; }
	inline void set_horizontalAxisName_4(String_t* value)
	{
		___horizontalAxisName_4 = value;
		Il2CppCodeGenWriteBarrier((&___horizontalAxisName_4), value);
	}

	inline static int32_t get_offset_of_verticalAxisName_5() { return static_cast<int32_t>(offsetof(TouchPad_t3890668177, ___verticalAxisName_5)); }
	inline String_t* get_verticalAxisName_5() const { return ___verticalAxisName_5; }
	inline String_t** get_address_of_verticalAxisName_5() { return &___verticalAxisName_5; }
	inline void set_verticalAxisName_5(String_t* value)
	{
		___verticalAxisName_5 = value;
		Il2CppCodeGenWriteBarrier((&___verticalAxisName_5), value);
	}

	inline static int32_t get_offset_of_Xsensitivity_6() { return static_cast<int32_t>(offsetof(TouchPad_t3890668177, ___Xsensitivity_6)); }
	inline float get_Xsensitivity_6() const { return ___Xsensitivity_6; }
	inline float* get_address_of_Xsensitivity_6() { return &___Xsensitivity_6; }
	inline void set_Xsensitivity_6(float value)
	{
		___Xsensitivity_6 = value;
	}

	inline static int32_t get_offset_of_Ysensitivity_7() { return static_cast<int32_t>(offsetof(TouchPad_t3890668177, ___Ysensitivity_7)); }
	inline float get_Ysensitivity_7() const { return ___Ysensitivity_7; }
	inline float* get_address_of_Ysensitivity_7() { return &___Ysensitivity_7; }
	inline void set_Ysensitivity_7(float value)
	{
		___Ysensitivity_7 = value;
	}

	inline static int32_t get_offset_of_m_StartPos_8() { return static_cast<int32_t>(offsetof(TouchPad_t3890668177, ___m_StartPos_8)); }
	inline Vector3_t2903530434  get_m_StartPos_8() const { return ___m_StartPos_8; }
	inline Vector3_t2903530434 * get_address_of_m_StartPos_8() { return &___m_StartPos_8; }
	inline void set_m_StartPos_8(Vector3_t2903530434  value)
	{
		___m_StartPos_8 = value;
	}

	inline static int32_t get_offset_of_m_PreviousDelta_9() { return static_cast<int32_t>(offsetof(TouchPad_t3890668177, ___m_PreviousDelta_9)); }
	inline Vector2_t3577333262  get_m_PreviousDelta_9() const { return ___m_PreviousDelta_9; }
	inline Vector2_t3577333262 * get_address_of_m_PreviousDelta_9() { return &___m_PreviousDelta_9; }
	inline void set_m_PreviousDelta_9(Vector2_t3577333262  value)
	{
		___m_PreviousDelta_9 = value;
	}

	inline static int32_t get_offset_of_m_JoytickOutput_10() { return static_cast<int32_t>(offsetof(TouchPad_t3890668177, ___m_JoytickOutput_10)); }
	inline Vector3_t2903530434  get_m_JoytickOutput_10() const { return ___m_JoytickOutput_10; }
	inline Vector3_t2903530434 * get_address_of_m_JoytickOutput_10() { return &___m_JoytickOutput_10; }
	inline void set_m_JoytickOutput_10(Vector3_t2903530434  value)
	{
		___m_JoytickOutput_10 = value;
	}

	inline static int32_t get_offset_of_m_UseX_11() { return static_cast<int32_t>(offsetof(TouchPad_t3890668177, ___m_UseX_11)); }
	inline bool get_m_UseX_11() const { return ___m_UseX_11; }
	inline bool* get_address_of_m_UseX_11() { return &___m_UseX_11; }
	inline void set_m_UseX_11(bool value)
	{
		___m_UseX_11 = value;
	}

	inline static int32_t get_offset_of_m_UseY_12() { return static_cast<int32_t>(offsetof(TouchPad_t3890668177, ___m_UseY_12)); }
	inline bool get_m_UseY_12() const { return ___m_UseY_12; }
	inline bool* get_address_of_m_UseY_12() { return &___m_UseY_12; }
	inline void set_m_UseY_12(bool value)
	{
		___m_UseY_12 = value;
	}

	inline static int32_t get_offset_of_m_HorizontalVirtualAxis_13() { return static_cast<int32_t>(offsetof(TouchPad_t3890668177, ___m_HorizontalVirtualAxis_13)); }
	inline VirtualAxis_t2383171311 * get_m_HorizontalVirtualAxis_13() const { return ___m_HorizontalVirtualAxis_13; }
	inline VirtualAxis_t2383171311 ** get_address_of_m_HorizontalVirtualAxis_13() { return &___m_HorizontalVirtualAxis_13; }
	inline void set_m_HorizontalVirtualAxis_13(VirtualAxis_t2383171311 * value)
	{
		___m_HorizontalVirtualAxis_13 = value;
		Il2CppCodeGenWriteBarrier((&___m_HorizontalVirtualAxis_13), value);
	}

	inline static int32_t get_offset_of_m_VerticalVirtualAxis_14() { return static_cast<int32_t>(offsetof(TouchPad_t3890668177, ___m_VerticalVirtualAxis_14)); }
	inline VirtualAxis_t2383171311 * get_m_VerticalVirtualAxis_14() const { return ___m_VerticalVirtualAxis_14; }
	inline VirtualAxis_t2383171311 ** get_address_of_m_VerticalVirtualAxis_14() { return &___m_VerticalVirtualAxis_14; }
	inline void set_m_VerticalVirtualAxis_14(VirtualAxis_t2383171311 * value)
	{
		___m_VerticalVirtualAxis_14 = value;
		Il2CppCodeGenWriteBarrier((&___m_VerticalVirtualAxis_14), value);
	}

	inline static int32_t get_offset_of_m_Dragging_15() { return static_cast<int32_t>(offsetof(TouchPad_t3890668177, ___m_Dragging_15)); }
	inline bool get_m_Dragging_15() const { return ___m_Dragging_15; }
	inline bool* get_address_of_m_Dragging_15() { return &___m_Dragging_15; }
	inline void set_m_Dragging_15(bool value)
	{
		___m_Dragging_15 = value;
	}

	inline static int32_t get_offset_of_m_Id_16() { return static_cast<int32_t>(offsetof(TouchPad_t3890668177, ___m_Id_16)); }
	inline int32_t get_m_Id_16() const { return ___m_Id_16; }
	inline int32_t* get_address_of_m_Id_16() { return &___m_Id_16; }
	inline void set_m_Id_16(int32_t value)
	{
		___m_Id_16 = value;
	}

	inline static int32_t get_offset_of_m_PreviousTouchPos_17() { return static_cast<int32_t>(offsetof(TouchPad_t3890668177, ___m_PreviousTouchPos_17)); }
	inline Vector2_t3577333262  get_m_PreviousTouchPos_17() const { return ___m_PreviousTouchPos_17; }
	inline Vector2_t3577333262 * get_address_of_m_PreviousTouchPos_17() { return &___m_PreviousTouchPos_17; }
	inline void set_m_PreviousTouchPos_17(Vector2_t3577333262  value)
	{
		___m_PreviousTouchPos_17 = value;
	}

	inline static int32_t get_offset_of_m_Center_18() { return static_cast<int32_t>(offsetof(TouchPad_t3890668177, ___m_Center_18)); }
	inline Vector3_t2903530434  get_m_Center_18() const { return ___m_Center_18; }
	inline Vector3_t2903530434 * get_address_of_m_Center_18() { return &___m_Center_18; }
	inline void set_m_Center_18(Vector3_t2903530434  value)
	{
		___m_Center_18 = value;
	}

	inline static int32_t get_offset_of_m_Image_19() { return static_cast<int32_t>(offsetof(TouchPad_t3890668177, ___m_Image_19)); }
	inline Image_t266201966 * get_m_Image_19() const { return ___m_Image_19; }
	inline Image_t266201966 ** get_address_of_m_Image_19() { return &___m_Image_19; }
	inline void set_m_Image_19(Image_t266201966 * value)
	{
		___m_Image_19 = value;
		Il2CppCodeGenWriteBarrier((&___m_Image_19), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOUCHPAD_T3890668177_H
#ifndef TILTINPUT_T484673937_H
#define TILTINPUT_T484673937_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.CrossPlatformInput.TiltInput
struct  TiltInput_t484673937  : public MonoBehaviour_t345688271
{
public:
	// UnityStandardAssets.CrossPlatformInput.TiltInput/AxisMapping UnityStandardAssets.CrossPlatformInput.TiltInput::mapping
	AxisMapping_t2403352015 * ___mapping_2;
	// UnityStandardAssets.CrossPlatformInput.TiltInput/AxisOptions UnityStandardAssets.CrossPlatformInput.TiltInput::tiltAroundAxis
	int32_t ___tiltAroundAxis_3;
	// System.Single UnityStandardAssets.CrossPlatformInput.TiltInput::fullTiltAngle
	float ___fullTiltAngle_4;
	// System.Single UnityStandardAssets.CrossPlatformInput.TiltInput::centreAngleOffset
	float ___centreAngleOffset_5;
	// UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualAxis UnityStandardAssets.CrossPlatformInput.TiltInput::m_SteerAxis
	VirtualAxis_t2383171311 * ___m_SteerAxis_6;

public:
	inline static int32_t get_offset_of_mapping_2() { return static_cast<int32_t>(offsetof(TiltInput_t484673937, ___mapping_2)); }
	inline AxisMapping_t2403352015 * get_mapping_2() const { return ___mapping_2; }
	inline AxisMapping_t2403352015 ** get_address_of_mapping_2() { return &___mapping_2; }
	inline void set_mapping_2(AxisMapping_t2403352015 * value)
	{
		___mapping_2 = value;
		Il2CppCodeGenWriteBarrier((&___mapping_2), value);
	}

	inline static int32_t get_offset_of_tiltAroundAxis_3() { return static_cast<int32_t>(offsetof(TiltInput_t484673937, ___tiltAroundAxis_3)); }
	inline int32_t get_tiltAroundAxis_3() const { return ___tiltAroundAxis_3; }
	inline int32_t* get_address_of_tiltAroundAxis_3() { return &___tiltAroundAxis_3; }
	inline void set_tiltAroundAxis_3(int32_t value)
	{
		___tiltAroundAxis_3 = value;
	}

	inline static int32_t get_offset_of_fullTiltAngle_4() { return static_cast<int32_t>(offsetof(TiltInput_t484673937, ___fullTiltAngle_4)); }
	inline float get_fullTiltAngle_4() const { return ___fullTiltAngle_4; }
	inline float* get_address_of_fullTiltAngle_4() { return &___fullTiltAngle_4; }
	inline void set_fullTiltAngle_4(float value)
	{
		___fullTiltAngle_4 = value;
	}

	inline static int32_t get_offset_of_centreAngleOffset_5() { return static_cast<int32_t>(offsetof(TiltInput_t484673937, ___centreAngleOffset_5)); }
	inline float get_centreAngleOffset_5() const { return ___centreAngleOffset_5; }
	inline float* get_address_of_centreAngleOffset_5() { return &___centreAngleOffset_5; }
	inline void set_centreAngleOffset_5(float value)
	{
		___centreAngleOffset_5 = value;
	}

	inline static int32_t get_offset_of_m_SteerAxis_6() { return static_cast<int32_t>(offsetof(TiltInput_t484673937, ___m_SteerAxis_6)); }
	inline VirtualAxis_t2383171311 * get_m_SteerAxis_6() const { return ___m_SteerAxis_6; }
	inline VirtualAxis_t2383171311 ** get_address_of_m_SteerAxis_6() { return &___m_SteerAxis_6; }
	inline void set_m_SteerAxis_6(VirtualAxis_t2383171311 * value)
	{
		___m_SteerAxis_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_SteerAxis_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TILTINPUT_T484673937_H
#ifndef MOBILECONTROLRIG_T3318571898_H
#define MOBILECONTROLRIG_T3318571898_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.CrossPlatformInput.MobileControlRig
struct  MobileControlRig_t3318571898  : public MonoBehaviour_t345688271
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MOBILECONTROLRIG_T3318571898_H
#ifndef EVENTSYSTEMCHECKER_T2535489545_H
#define EVENTSYSTEMCHECKER_T2535489545_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EventSystemChecker
struct  EventSystemChecker_t2535489545  : public MonoBehaviour_t345688271
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EVENTSYSTEMCHECKER_T2535489545_H
#ifndef SIMPLEACTIVATORMENU_T3675303537_H
#define SIMPLEACTIVATORMENU_T3675303537_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.SimpleActivatorMenu
struct  SimpleActivatorMenu_t3675303537  : public MonoBehaviour_t345688271
{
public:
	// UnityEngine.GUIText UnityStandardAssets.Utility.SimpleActivatorMenu::camSwitchButton
	GUIText_t1809039568 * ___camSwitchButton_2;
	// UnityEngine.GameObject[] UnityStandardAssets.Utility.SimpleActivatorMenu::objects
	GameObjectU5BU5D_t2021098027* ___objects_3;
	// System.Int32 UnityStandardAssets.Utility.SimpleActivatorMenu::m_CurrentActiveObject
	int32_t ___m_CurrentActiveObject_4;

public:
	inline static int32_t get_offset_of_camSwitchButton_2() { return static_cast<int32_t>(offsetof(SimpleActivatorMenu_t3675303537, ___camSwitchButton_2)); }
	inline GUIText_t1809039568 * get_camSwitchButton_2() const { return ___camSwitchButton_2; }
	inline GUIText_t1809039568 ** get_address_of_camSwitchButton_2() { return &___camSwitchButton_2; }
	inline void set_camSwitchButton_2(GUIText_t1809039568 * value)
	{
		___camSwitchButton_2 = value;
		Il2CppCodeGenWriteBarrier((&___camSwitchButton_2), value);
	}

	inline static int32_t get_offset_of_objects_3() { return static_cast<int32_t>(offsetof(SimpleActivatorMenu_t3675303537, ___objects_3)); }
	inline GameObjectU5BU5D_t2021098027* get_objects_3() const { return ___objects_3; }
	inline GameObjectU5BU5D_t2021098027** get_address_of_objects_3() { return &___objects_3; }
	inline void set_objects_3(GameObjectU5BU5D_t2021098027* value)
	{
		___objects_3 = value;
		Il2CppCodeGenWriteBarrier((&___objects_3), value);
	}

	inline static int32_t get_offset_of_m_CurrentActiveObject_4() { return static_cast<int32_t>(offsetof(SimpleActivatorMenu_t3675303537, ___m_CurrentActiveObject_4)); }
	inline int32_t get_m_CurrentActiveObject_4() const { return ___m_CurrentActiveObject_4; }
	inline int32_t* get_address_of_m_CurrentActiveObject_4() { return &___m_CurrentActiveObject_4; }
	inline void set_m_CurrentActiveObject_4(int32_t value)
	{
		___m_CurrentActiveObject_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SIMPLEACTIVATORMENU_T3675303537_H
#ifndef PARTICLESYSTEMDESTROYER_T3293524282_H
#define PARTICLESYSTEMDESTROYER_T3293524282_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.ParticleSystemDestroyer
struct  ParticleSystemDestroyer_t3293524282  : public MonoBehaviour_t345688271
{
public:
	// System.Single UnityStandardAssets.Utility.ParticleSystemDestroyer::minDuration
	float ___minDuration_2;
	// System.Single UnityStandardAssets.Utility.ParticleSystemDestroyer::maxDuration
	float ___maxDuration_3;
	// System.Single UnityStandardAssets.Utility.ParticleSystemDestroyer::m_MaxLifetime
	float ___m_MaxLifetime_4;
	// System.Boolean UnityStandardAssets.Utility.ParticleSystemDestroyer::m_EarlyStop
	bool ___m_EarlyStop_5;

public:
	inline static int32_t get_offset_of_minDuration_2() { return static_cast<int32_t>(offsetof(ParticleSystemDestroyer_t3293524282, ___minDuration_2)); }
	inline float get_minDuration_2() const { return ___minDuration_2; }
	inline float* get_address_of_minDuration_2() { return &___minDuration_2; }
	inline void set_minDuration_2(float value)
	{
		___minDuration_2 = value;
	}

	inline static int32_t get_offset_of_maxDuration_3() { return static_cast<int32_t>(offsetof(ParticleSystemDestroyer_t3293524282, ___maxDuration_3)); }
	inline float get_maxDuration_3() const { return ___maxDuration_3; }
	inline float* get_address_of_maxDuration_3() { return &___maxDuration_3; }
	inline void set_maxDuration_3(float value)
	{
		___maxDuration_3 = value;
	}

	inline static int32_t get_offset_of_m_MaxLifetime_4() { return static_cast<int32_t>(offsetof(ParticleSystemDestroyer_t3293524282, ___m_MaxLifetime_4)); }
	inline float get_m_MaxLifetime_4() const { return ___m_MaxLifetime_4; }
	inline float* get_address_of_m_MaxLifetime_4() { return &___m_MaxLifetime_4; }
	inline void set_m_MaxLifetime_4(float value)
	{
		___m_MaxLifetime_4 = value;
	}

	inline static int32_t get_offset_of_m_EarlyStop_5() { return static_cast<int32_t>(offsetof(ParticleSystemDestroyer_t3293524282, ___m_EarlyStop_5)); }
	inline bool get_m_EarlyStop_5() const { return ___m_EarlyStop_5; }
	inline bool* get_address_of_m_EarlyStop_5() { return &___m_EarlyStop_5; }
	inline void set_m_EarlyStop_5(bool value)
	{
		___m_EarlyStop_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PARTICLESYSTEMDESTROYER_T3293524282_H
#ifndef SMOOTHFOLLOW_T1265817179_H
#define SMOOTHFOLLOW_T1265817179_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.SmoothFollow
struct  SmoothFollow_t1265817179  : public MonoBehaviour_t345688271
{
public:
	// UnityEngine.Transform UnityStandardAssets.Utility.SmoothFollow::target
	Transform_t3316442598 * ___target_2;
	// System.Single UnityStandardAssets.Utility.SmoothFollow::distance
	float ___distance_3;
	// System.Single UnityStandardAssets.Utility.SmoothFollow::height
	float ___height_4;
	// System.Single UnityStandardAssets.Utility.SmoothFollow::rotationDamping
	float ___rotationDamping_5;
	// System.Single UnityStandardAssets.Utility.SmoothFollow::heightDamping
	float ___heightDamping_6;

public:
	inline static int32_t get_offset_of_target_2() { return static_cast<int32_t>(offsetof(SmoothFollow_t1265817179, ___target_2)); }
	inline Transform_t3316442598 * get_target_2() const { return ___target_2; }
	inline Transform_t3316442598 ** get_address_of_target_2() { return &___target_2; }
	inline void set_target_2(Transform_t3316442598 * value)
	{
		___target_2 = value;
		Il2CppCodeGenWriteBarrier((&___target_2), value);
	}

	inline static int32_t get_offset_of_distance_3() { return static_cast<int32_t>(offsetof(SmoothFollow_t1265817179, ___distance_3)); }
	inline float get_distance_3() const { return ___distance_3; }
	inline float* get_address_of_distance_3() { return &___distance_3; }
	inline void set_distance_3(float value)
	{
		___distance_3 = value;
	}

	inline static int32_t get_offset_of_height_4() { return static_cast<int32_t>(offsetof(SmoothFollow_t1265817179, ___height_4)); }
	inline float get_height_4() const { return ___height_4; }
	inline float* get_address_of_height_4() { return &___height_4; }
	inline void set_height_4(float value)
	{
		___height_4 = value;
	}

	inline static int32_t get_offset_of_rotationDamping_5() { return static_cast<int32_t>(offsetof(SmoothFollow_t1265817179, ___rotationDamping_5)); }
	inline float get_rotationDamping_5() const { return ___rotationDamping_5; }
	inline float* get_address_of_rotationDamping_5() { return &___rotationDamping_5; }
	inline void set_rotationDamping_5(float value)
	{
		___rotationDamping_5 = value;
	}

	inline static int32_t get_offset_of_heightDamping_6() { return static_cast<int32_t>(offsetof(SmoothFollow_t1265817179, ___heightDamping_6)); }
	inline float get_heightDamping_6() const { return ___heightDamping_6; }
	inline float* get_address_of_heightDamping_6() { return &___heightDamping_6; }
	inline void set_heightDamping_6(float value)
	{
		___heightDamping_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SMOOTHFOLLOW_T1265817179_H
#ifndef TIMEDOBJECTACTIVATOR_T1049299440_H
#define TIMEDOBJECTACTIVATOR_T1049299440_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.TimedObjectActivator
struct  TimedObjectActivator_t1049299440  : public MonoBehaviour_t345688271
{
public:
	// UnityStandardAssets.Utility.TimedObjectActivator/Entries UnityStandardAssets.Utility.TimedObjectActivator::entries
	Entries_t1342860269 * ___entries_2;

public:
	inline static int32_t get_offset_of_entries_2() { return static_cast<int32_t>(offsetof(TimedObjectActivator_t1049299440, ___entries_2)); }
	inline Entries_t1342860269 * get_entries_2() const { return ___entries_2; }
	inline Entries_t1342860269 ** get_address_of_entries_2() { return &___entries_2; }
	inline void set_entries_2(Entries_t1342860269 * value)
	{
		___entries_2 = value;
		Il2CppCodeGenWriteBarrier((&___entries_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TIMEDOBJECTACTIVATOR_T1049299440_H
#ifndef BALLUSERCONTROL_T4201493832_H
#define BALLUSERCONTROL_T4201493832_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Vehicles.Ball.BallUserControl
struct  BallUserControl_t4201493832  : public MonoBehaviour_t345688271
{
public:
	// UnityStandardAssets.Vehicles.Ball.Ball UnityStandardAssets.Vehicles.Ball.BallUserControl::ball
	Ball_t3802515361 * ___ball_2;
	// UnityEngine.Vector3 UnityStandardAssets.Vehicles.Ball.BallUserControl::move
	Vector3_t2903530434  ___move_3;
	// UnityEngine.Transform UnityStandardAssets.Vehicles.Ball.BallUserControl::cam
	Transform_t3316442598 * ___cam_4;
	// UnityEngine.Vector3 UnityStandardAssets.Vehicles.Ball.BallUserControl::camForward
	Vector3_t2903530434  ___camForward_5;
	// System.Boolean UnityStandardAssets.Vehicles.Ball.BallUserControl::jump
	bool ___jump_6;

public:
	inline static int32_t get_offset_of_ball_2() { return static_cast<int32_t>(offsetof(BallUserControl_t4201493832, ___ball_2)); }
	inline Ball_t3802515361 * get_ball_2() const { return ___ball_2; }
	inline Ball_t3802515361 ** get_address_of_ball_2() { return &___ball_2; }
	inline void set_ball_2(Ball_t3802515361 * value)
	{
		___ball_2 = value;
		Il2CppCodeGenWriteBarrier((&___ball_2), value);
	}

	inline static int32_t get_offset_of_move_3() { return static_cast<int32_t>(offsetof(BallUserControl_t4201493832, ___move_3)); }
	inline Vector3_t2903530434  get_move_3() const { return ___move_3; }
	inline Vector3_t2903530434 * get_address_of_move_3() { return &___move_3; }
	inline void set_move_3(Vector3_t2903530434  value)
	{
		___move_3 = value;
	}

	inline static int32_t get_offset_of_cam_4() { return static_cast<int32_t>(offsetof(BallUserControl_t4201493832, ___cam_4)); }
	inline Transform_t3316442598 * get_cam_4() const { return ___cam_4; }
	inline Transform_t3316442598 ** get_address_of_cam_4() { return &___cam_4; }
	inline void set_cam_4(Transform_t3316442598 * value)
	{
		___cam_4 = value;
		Il2CppCodeGenWriteBarrier((&___cam_4), value);
	}

	inline static int32_t get_offset_of_camForward_5() { return static_cast<int32_t>(offsetof(BallUserControl_t4201493832, ___camForward_5)); }
	inline Vector3_t2903530434  get_camForward_5() const { return ___camForward_5; }
	inline Vector3_t2903530434 * get_address_of_camForward_5() { return &___camForward_5; }
	inline void set_camForward_5(Vector3_t2903530434  value)
	{
		___camForward_5 = value;
	}

	inline static int32_t get_offset_of_jump_6() { return static_cast<int32_t>(offsetof(BallUserControl_t4201493832, ___jump_6)); }
	inline bool get_jump_6() const { return ___jump_6; }
	inline bool* get_address_of_jump_6() { return &___jump_6; }
	inline void set_jump_6(bool value)
	{
		___jump_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BALLUSERCONTROL_T4201493832_H
#ifndef BALL_T3802515361_H
#define BALL_T3802515361_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Vehicles.Ball.Ball
struct  Ball_t3802515361  : public MonoBehaviour_t345688271
{
public:
	// System.Single UnityStandardAssets.Vehicles.Ball.Ball::m_MovePower
	float ___m_MovePower_2;
	// System.Boolean UnityStandardAssets.Vehicles.Ball.Ball::m_UseTorque
	bool ___m_UseTorque_3;
	// System.Single UnityStandardAssets.Vehicles.Ball.Ball::m_MaxAngularVelocity
	float ___m_MaxAngularVelocity_4;
	// System.Single UnityStandardAssets.Vehicles.Ball.Ball::m_JumpPower
	float ___m_JumpPower_5;
	// UnityEngine.Rigidbody UnityStandardAssets.Vehicles.Ball.Ball::m_Rigidbody
	Rigidbody_t2492269564 * ___m_Rigidbody_7;

public:
	inline static int32_t get_offset_of_m_MovePower_2() { return static_cast<int32_t>(offsetof(Ball_t3802515361, ___m_MovePower_2)); }
	inline float get_m_MovePower_2() const { return ___m_MovePower_2; }
	inline float* get_address_of_m_MovePower_2() { return &___m_MovePower_2; }
	inline void set_m_MovePower_2(float value)
	{
		___m_MovePower_2 = value;
	}

	inline static int32_t get_offset_of_m_UseTorque_3() { return static_cast<int32_t>(offsetof(Ball_t3802515361, ___m_UseTorque_3)); }
	inline bool get_m_UseTorque_3() const { return ___m_UseTorque_3; }
	inline bool* get_address_of_m_UseTorque_3() { return &___m_UseTorque_3; }
	inline void set_m_UseTorque_3(bool value)
	{
		___m_UseTorque_3 = value;
	}

	inline static int32_t get_offset_of_m_MaxAngularVelocity_4() { return static_cast<int32_t>(offsetof(Ball_t3802515361, ___m_MaxAngularVelocity_4)); }
	inline float get_m_MaxAngularVelocity_4() const { return ___m_MaxAngularVelocity_4; }
	inline float* get_address_of_m_MaxAngularVelocity_4() { return &___m_MaxAngularVelocity_4; }
	inline void set_m_MaxAngularVelocity_4(float value)
	{
		___m_MaxAngularVelocity_4 = value;
	}

	inline static int32_t get_offset_of_m_JumpPower_5() { return static_cast<int32_t>(offsetof(Ball_t3802515361, ___m_JumpPower_5)); }
	inline float get_m_JumpPower_5() const { return ___m_JumpPower_5; }
	inline float* get_address_of_m_JumpPower_5() { return &___m_JumpPower_5; }
	inline void set_m_JumpPower_5(float value)
	{
		___m_JumpPower_5 = value;
	}

	inline static int32_t get_offset_of_m_Rigidbody_7() { return static_cast<int32_t>(offsetof(Ball_t3802515361, ___m_Rigidbody_7)); }
	inline Rigidbody_t2492269564 * get_m_Rigidbody_7() const { return ___m_Rigidbody_7; }
	inline Rigidbody_t2492269564 ** get_address_of_m_Rigidbody_7() { return &___m_Rigidbody_7; }
	inline void set_m_Rigidbody_7(Rigidbody_t2492269564 * value)
	{
		___m_Rigidbody_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_Rigidbody_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BALL_T3802515361_H
#ifndef TIMEDOBJECTDESTRUCTOR_T2755931980_H
#define TIMEDOBJECTDESTRUCTOR_T2755931980_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.TimedObjectDestructor
struct  TimedObjectDestructor_t2755931980  : public MonoBehaviour_t345688271
{
public:
	// System.Single UnityStandardAssets.Utility.TimedObjectDestructor::m_TimeOut
	float ___m_TimeOut_2;
	// System.Boolean UnityStandardAssets.Utility.TimedObjectDestructor::m_DetachChildren
	bool ___m_DetachChildren_3;

public:
	inline static int32_t get_offset_of_m_TimeOut_2() { return static_cast<int32_t>(offsetof(TimedObjectDestructor_t2755931980, ___m_TimeOut_2)); }
	inline float get_m_TimeOut_2() const { return ___m_TimeOut_2; }
	inline float* get_address_of_m_TimeOut_2() { return &___m_TimeOut_2; }
	inline void set_m_TimeOut_2(float value)
	{
		___m_TimeOut_2 = value;
	}

	inline static int32_t get_offset_of_m_DetachChildren_3() { return static_cast<int32_t>(offsetof(TimedObjectDestructor_t2755931980, ___m_DetachChildren_3)); }
	inline bool get_m_DetachChildren_3() const { return ___m_DetachChildren_3; }
	inline bool* get_address_of_m_DetachChildren_3() { return &___m_DetachChildren_3; }
	inline void set_m_DetachChildren_3(bool value)
	{
		___m_DetachChildren_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TIMEDOBJECTDESTRUCTOR_T2755931980_H
#ifndef WAYPOINTCIRCUIT_T802585562_H
#define WAYPOINTCIRCUIT_T802585562_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.WaypointCircuit
struct  WaypointCircuit_t802585562  : public MonoBehaviour_t345688271
{
public:
	// UnityStandardAssets.Utility.WaypointCircuit/WaypointList UnityStandardAssets.Utility.WaypointCircuit::waypointList
	WaypointList_t2428355280 * ___waypointList_2;
	// System.Boolean UnityStandardAssets.Utility.WaypointCircuit::smoothRoute
	bool ___smoothRoute_3;
	// System.Int32 UnityStandardAssets.Utility.WaypointCircuit::numPoints
	int32_t ___numPoints_4;
	// UnityEngine.Vector3[] UnityStandardAssets.Utility.WaypointCircuit::points
	Vector3U5BU5D_t981680887* ___points_5;
	// System.Single[] UnityStandardAssets.Utility.WaypointCircuit::distances
	SingleU5BU5D_t1390472614* ___distances_6;
	// System.Single UnityStandardAssets.Utility.WaypointCircuit::editorVisualisationSubsteps
	float ___editorVisualisationSubsteps_7;
	// System.Single UnityStandardAssets.Utility.WaypointCircuit::<Length>k__BackingField
	float ___U3CLengthU3Ek__BackingField_8;
	// System.Int32 UnityStandardAssets.Utility.WaypointCircuit::p0n
	int32_t ___p0n_9;
	// System.Int32 UnityStandardAssets.Utility.WaypointCircuit::p1n
	int32_t ___p1n_10;
	// System.Int32 UnityStandardAssets.Utility.WaypointCircuit::p2n
	int32_t ___p2n_11;
	// System.Int32 UnityStandardAssets.Utility.WaypointCircuit::p3n
	int32_t ___p3n_12;
	// System.Single UnityStandardAssets.Utility.WaypointCircuit::i
	float ___i_13;
	// UnityEngine.Vector3 UnityStandardAssets.Utility.WaypointCircuit::P0
	Vector3_t2903530434  ___P0_14;
	// UnityEngine.Vector3 UnityStandardAssets.Utility.WaypointCircuit::P1
	Vector3_t2903530434  ___P1_15;
	// UnityEngine.Vector3 UnityStandardAssets.Utility.WaypointCircuit::P2
	Vector3_t2903530434  ___P2_16;
	// UnityEngine.Vector3 UnityStandardAssets.Utility.WaypointCircuit::P3
	Vector3_t2903530434  ___P3_17;

public:
	inline static int32_t get_offset_of_waypointList_2() { return static_cast<int32_t>(offsetof(WaypointCircuit_t802585562, ___waypointList_2)); }
	inline WaypointList_t2428355280 * get_waypointList_2() const { return ___waypointList_2; }
	inline WaypointList_t2428355280 ** get_address_of_waypointList_2() { return &___waypointList_2; }
	inline void set_waypointList_2(WaypointList_t2428355280 * value)
	{
		___waypointList_2 = value;
		Il2CppCodeGenWriteBarrier((&___waypointList_2), value);
	}

	inline static int32_t get_offset_of_smoothRoute_3() { return static_cast<int32_t>(offsetof(WaypointCircuit_t802585562, ___smoothRoute_3)); }
	inline bool get_smoothRoute_3() const { return ___smoothRoute_3; }
	inline bool* get_address_of_smoothRoute_3() { return &___smoothRoute_3; }
	inline void set_smoothRoute_3(bool value)
	{
		___smoothRoute_3 = value;
	}

	inline static int32_t get_offset_of_numPoints_4() { return static_cast<int32_t>(offsetof(WaypointCircuit_t802585562, ___numPoints_4)); }
	inline int32_t get_numPoints_4() const { return ___numPoints_4; }
	inline int32_t* get_address_of_numPoints_4() { return &___numPoints_4; }
	inline void set_numPoints_4(int32_t value)
	{
		___numPoints_4 = value;
	}

	inline static int32_t get_offset_of_points_5() { return static_cast<int32_t>(offsetof(WaypointCircuit_t802585562, ___points_5)); }
	inline Vector3U5BU5D_t981680887* get_points_5() const { return ___points_5; }
	inline Vector3U5BU5D_t981680887** get_address_of_points_5() { return &___points_5; }
	inline void set_points_5(Vector3U5BU5D_t981680887* value)
	{
		___points_5 = value;
		Il2CppCodeGenWriteBarrier((&___points_5), value);
	}

	inline static int32_t get_offset_of_distances_6() { return static_cast<int32_t>(offsetof(WaypointCircuit_t802585562, ___distances_6)); }
	inline SingleU5BU5D_t1390472614* get_distances_6() const { return ___distances_6; }
	inline SingleU5BU5D_t1390472614** get_address_of_distances_6() { return &___distances_6; }
	inline void set_distances_6(SingleU5BU5D_t1390472614* value)
	{
		___distances_6 = value;
		Il2CppCodeGenWriteBarrier((&___distances_6), value);
	}

	inline static int32_t get_offset_of_editorVisualisationSubsteps_7() { return static_cast<int32_t>(offsetof(WaypointCircuit_t802585562, ___editorVisualisationSubsteps_7)); }
	inline float get_editorVisualisationSubsteps_7() const { return ___editorVisualisationSubsteps_7; }
	inline float* get_address_of_editorVisualisationSubsteps_7() { return &___editorVisualisationSubsteps_7; }
	inline void set_editorVisualisationSubsteps_7(float value)
	{
		___editorVisualisationSubsteps_7 = value;
	}

	inline static int32_t get_offset_of_U3CLengthU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(WaypointCircuit_t802585562, ___U3CLengthU3Ek__BackingField_8)); }
	inline float get_U3CLengthU3Ek__BackingField_8() const { return ___U3CLengthU3Ek__BackingField_8; }
	inline float* get_address_of_U3CLengthU3Ek__BackingField_8() { return &___U3CLengthU3Ek__BackingField_8; }
	inline void set_U3CLengthU3Ek__BackingField_8(float value)
	{
		___U3CLengthU3Ek__BackingField_8 = value;
	}

	inline static int32_t get_offset_of_p0n_9() { return static_cast<int32_t>(offsetof(WaypointCircuit_t802585562, ___p0n_9)); }
	inline int32_t get_p0n_9() const { return ___p0n_9; }
	inline int32_t* get_address_of_p0n_9() { return &___p0n_9; }
	inline void set_p0n_9(int32_t value)
	{
		___p0n_9 = value;
	}

	inline static int32_t get_offset_of_p1n_10() { return static_cast<int32_t>(offsetof(WaypointCircuit_t802585562, ___p1n_10)); }
	inline int32_t get_p1n_10() const { return ___p1n_10; }
	inline int32_t* get_address_of_p1n_10() { return &___p1n_10; }
	inline void set_p1n_10(int32_t value)
	{
		___p1n_10 = value;
	}

	inline static int32_t get_offset_of_p2n_11() { return static_cast<int32_t>(offsetof(WaypointCircuit_t802585562, ___p2n_11)); }
	inline int32_t get_p2n_11() const { return ___p2n_11; }
	inline int32_t* get_address_of_p2n_11() { return &___p2n_11; }
	inline void set_p2n_11(int32_t value)
	{
		___p2n_11 = value;
	}

	inline static int32_t get_offset_of_p3n_12() { return static_cast<int32_t>(offsetof(WaypointCircuit_t802585562, ___p3n_12)); }
	inline int32_t get_p3n_12() const { return ___p3n_12; }
	inline int32_t* get_address_of_p3n_12() { return &___p3n_12; }
	inline void set_p3n_12(int32_t value)
	{
		___p3n_12 = value;
	}

	inline static int32_t get_offset_of_i_13() { return static_cast<int32_t>(offsetof(WaypointCircuit_t802585562, ___i_13)); }
	inline float get_i_13() const { return ___i_13; }
	inline float* get_address_of_i_13() { return &___i_13; }
	inline void set_i_13(float value)
	{
		___i_13 = value;
	}

	inline static int32_t get_offset_of_P0_14() { return static_cast<int32_t>(offsetof(WaypointCircuit_t802585562, ___P0_14)); }
	inline Vector3_t2903530434  get_P0_14() const { return ___P0_14; }
	inline Vector3_t2903530434 * get_address_of_P0_14() { return &___P0_14; }
	inline void set_P0_14(Vector3_t2903530434  value)
	{
		___P0_14 = value;
	}

	inline static int32_t get_offset_of_P1_15() { return static_cast<int32_t>(offsetof(WaypointCircuit_t802585562, ___P1_15)); }
	inline Vector3_t2903530434  get_P1_15() const { return ___P1_15; }
	inline Vector3_t2903530434 * get_address_of_P1_15() { return &___P1_15; }
	inline void set_P1_15(Vector3_t2903530434  value)
	{
		___P1_15 = value;
	}

	inline static int32_t get_offset_of_P2_16() { return static_cast<int32_t>(offsetof(WaypointCircuit_t802585562, ___P2_16)); }
	inline Vector3_t2903530434  get_P2_16() const { return ___P2_16; }
	inline Vector3_t2903530434 * get_address_of_P2_16() { return &___P2_16; }
	inline void set_P2_16(Vector3_t2903530434  value)
	{
		___P2_16 = value;
	}

	inline static int32_t get_offset_of_P3_17() { return static_cast<int32_t>(offsetof(WaypointCircuit_t802585562, ___P3_17)); }
	inline Vector3_t2903530434  get_P3_17() const { return ___P3_17; }
	inline Vector3_t2903530434 * get_address_of_P3_17() { return &___P3_17; }
	inline void set_P3_17(Vector3_t2903530434  value)
	{
		___P3_17 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WAYPOINTCIRCUIT_T802585562_H
#ifndef INPUTAXISSCROLLBAR_T2200868714_H
#define INPUTAXISSCROLLBAR_T2200868714_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.CrossPlatformInput.InputAxisScrollbar
struct  InputAxisScrollbar_t2200868714  : public MonoBehaviour_t345688271
{
public:
	// System.String UnityStandardAssets.CrossPlatformInput.InputAxisScrollbar::axis
	String_t* ___axis_2;

public:
	inline static int32_t get_offset_of_axis_2() { return static_cast<int32_t>(offsetof(InputAxisScrollbar_t2200868714, ___axis_2)); }
	inline String_t* get_axis_2() const { return ___axis_2; }
	inline String_t** get_address_of_axis_2() { return &___axis_2; }
	inline void set_axis_2(String_t* value)
	{
		___axis_2 = value;
		Il2CppCodeGenWriteBarrier((&___axis_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INPUTAXISSCROLLBAR_T2200868714_H
#ifndef WAYPOINTPROGRESSTRACKER_T3979672196_H
#define WAYPOINTPROGRESSTRACKER_T3979672196_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.WaypointProgressTracker
struct  WaypointProgressTracker_t3979672196  : public MonoBehaviour_t345688271
{
public:
	// UnityStandardAssets.Utility.WaypointCircuit UnityStandardAssets.Utility.WaypointProgressTracker::circuit
	WaypointCircuit_t802585562 * ___circuit_2;
	// System.Single UnityStandardAssets.Utility.WaypointProgressTracker::lookAheadForTargetOffset
	float ___lookAheadForTargetOffset_3;
	// System.Single UnityStandardAssets.Utility.WaypointProgressTracker::lookAheadForTargetFactor
	float ___lookAheadForTargetFactor_4;
	// System.Single UnityStandardAssets.Utility.WaypointProgressTracker::lookAheadForSpeedOffset
	float ___lookAheadForSpeedOffset_5;
	// System.Single UnityStandardAssets.Utility.WaypointProgressTracker::lookAheadForSpeedFactor
	float ___lookAheadForSpeedFactor_6;
	// UnityStandardAssets.Utility.WaypointProgressTracker/ProgressStyle UnityStandardAssets.Utility.WaypointProgressTracker::progressStyle
	int32_t ___progressStyle_7;
	// System.Single UnityStandardAssets.Utility.WaypointProgressTracker::pointToPointThreshold
	float ___pointToPointThreshold_8;
	// UnityStandardAssets.Utility.WaypointCircuit/RoutePoint UnityStandardAssets.Utility.WaypointProgressTracker::<targetPoint>k__BackingField
	RoutePoint_t4047115680  ___U3CtargetPointU3Ek__BackingField_9;
	// UnityStandardAssets.Utility.WaypointCircuit/RoutePoint UnityStandardAssets.Utility.WaypointProgressTracker::<speedPoint>k__BackingField
	RoutePoint_t4047115680  ___U3CspeedPointU3Ek__BackingField_10;
	// UnityStandardAssets.Utility.WaypointCircuit/RoutePoint UnityStandardAssets.Utility.WaypointProgressTracker::<progressPoint>k__BackingField
	RoutePoint_t4047115680  ___U3CprogressPointU3Ek__BackingField_11;
	// UnityEngine.Transform UnityStandardAssets.Utility.WaypointProgressTracker::target
	Transform_t3316442598 * ___target_12;
	// System.Single UnityStandardAssets.Utility.WaypointProgressTracker::progressDistance
	float ___progressDistance_13;
	// System.Int32 UnityStandardAssets.Utility.WaypointProgressTracker::progressNum
	int32_t ___progressNum_14;
	// UnityEngine.Vector3 UnityStandardAssets.Utility.WaypointProgressTracker::lastPosition
	Vector3_t2903530434  ___lastPosition_15;
	// System.Single UnityStandardAssets.Utility.WaypointProgressTracker::speed
	float ___speed_16;

public:
	inline static int32_t get_offset_of_circuit_2() { return static_cast<int32_t>(offsetof(WaypointProgressTracker_t3979672196, ___circuit_2)); }
	inline WaypointCircuit_t802585562 * get_circuit_2() const { return ___circuit_2; }
	inline WaypointCircuit_t802585562 ** get_address_of_circuit_2() { return &___circuit_2; }
	inline void set_circuit_2(WaypointCircuit_t802585562 * value)
	{
		___circuit_2 = value;
		Il2CppCodeGenWriteBarrier((&___circuit_2), value);
	}

	inline static int32_t get_offset_of_lookAheadForTargetOffset_3() { return static_cast<int32_t>(offsetof(WaypointProgressTracker_t3979672196, ___lookAheadForTargetOffset_3)); }
	inline float get_lookAheadForTargetOffset_3() const { return ___lookAheadForTargetOffset_3; }
	inline float* get_address_of_lookAheadForTargetOffset_3() { return &___lookAheadForTargetOffset_3; }
	inline void set_lookAheadForTargetOffset_3(float value)
	{
		___lookAheadForTargetOffset_3 = value;
	}

	inline static int32_t get_offset_of_lookAheadForTargetFactor_4() { return static_cast<int32_t>(offsetof(WaypointProgressTracker_t3979672196, ___lookAheadForTargetFactor_4)); }
	inline float get_lookAheadForTargetFactor_4() const { return ___lookAheadForTargetFactor_4; }
	inline float* get_address_of_lookAheadForTargetFactor_4() { return &___lookAheadForTargetFactor_4; }
	inline void set_lookAheadForTargetFactor_4(float value)
	{
		___lookAheadForTargetFactor_4 = value;
	}

	inline static int32_t get_offset_of_lookAheadForSpeedOffset_5() { return static_cast<int32_t>(offsetof(WaypointProgressTracker_t3979672196, ___lookAheadForSpeedOffset_5)); }
	inline float get_lookAheadForSpeedOffset_5() const { return ___lookAheadForSpeedOffset_5; }
	inline float* get_address_of_lookAheadForSpeedOffset_5() { return &___lookAheadForSpeedOffset_5; }
	inline void set_lookAheadForSpeedOffset_5(float value)
	{
		___lookAheadForSpeedOffset_5 = value;
	}

	inline static int32_t get_offset_of_lookAheadForSpeedFactor_6() { return static_cast<int32_t>(offsetof(WaypointProgressTracker_t3979672196, ___lookAheadForSpeedFactor_6)); }
	inline float get_lookAheadForSpeedFactor_6() const { return ___lookAheadForSpeedFactor_6; }
	inline float* get_address_of_lookAheadForSpeedFactor_6() { return &___lookAheadForSpeedFactor_6; }
	inline void set_lookAheadForSpeedFactor_6(float value)
	{
		___lookAheadForSpeedFactor_6 = value;
	}

	inline static int32_t get_offset_of_progressStyle_7() { return static_cast<int32_t>(offsetof(WaypointProgressTracker_t3979672196, ___progressStyle_7)); }
	inline int32_t get_progressStyle_7() const { return ___progressStyle_7; }
	inline int32_t* get_address_of_progressStyle_7() { return &___progressStyle_7; }
	inline void set_progressStyle_7(int32_t value)
	{
		___progressStyle_7 = value;
	}

	inline static int32_t get_offset_of_pointToPointThreshold_8() { return static_cast<int32_t>(offsetof(WaypointProgressTracker_t3979672196, ___pointToPointThreshold_8)); }
	inline float get_pointToPointThreshold_8() const { return ___pointToPointThreshold_8; }
	inline float* get_address_of_pointToPointThreshold_8() { return &___pointToPointThreshold_8; }
	inline void set_pointToPointThreshold_8(float value)
	{
		___pointToPointThreshold_8 = value;
	}

	inline static int32_t get_offset_of_U3CtargetPointU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(WaypointProgressTracker_t3979672196, ___U3CtargetPointU3Ek__BackingField_9)); }
	inline RoutePoint_t4047115680  get_U3CtargetPointU3Ek__BackingField_9() const { return ___U3CtargetPointU3Ek__BackingField_9; }
	inline RoutePoint_t4047115680 * get_address_of_U3CtargetPointU3Ek__BackingField_9() { return &___U3CtargetPointU3Ek__BackingField_9; }
	inline void set_U3CtargetPointU3Ek__BackingField_9(RoutePoint_t4047115680  value)
	{
		___U3CtargetPointU3Ek__BackingField_9 = value;
	}

	inline static int32_t get_offset_of_U3CspeedPointU3Ek__BackingField_10() { return static_cast<int32_t>(offsetof(WaypointProgressTracker_t3979672196, ___U3CspeedPointU3Ek__BackingField_10)); }
	inline RoutePoint_t4047115680  get_U3CspeedPointU3Ek__BackingField_10() const { return ___U3CspeedPointU3Ek__BackingField_10; }
	inline RoutePoint_t4047115680 * get_address_of_U3CspeedPointU3Ek__BackingField_10() { return &___U3CspeedPointU3Ek__BackingField_10; }
	inline void set_U3CspeedPointU3Ek__BackingField_10(RoutePoint_t4047115680  value)
	{
		___U3CspeedPointU3Ek__BackingField_10 = value;
	}

	inline static int32_t get_offset_of_U3CprogressPointU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(WaypointProgressTracker_t3979672196, ___U3CprogressPointU3Ek__BackingField_11)); }
	inline RoutePoint_t4047115680  get_U3CprogressPointU3Ek__BackingField_11() const { return ___U3CprogressPointU3Ek__BackingField_11; }
	inline RoutePoint_t4047115680 * get_address_of_U3CprogressPointU3Ek__BackingField_11() { return &___U3CprogressPointU3Ek__BackingField_11; }
	inline void set_U3CprogressPointU3Ek__BackingField_11(RoutePoint_t4047115680  value)
	{
		___U3CprogressPointU3Ek__BackingField_11 = value;
	}

	inline static int32_t get_offset_of_target_12() { return static_cast<int32_t>(offsetof(WaypointProgressTracker_t3979672196, ___target_12)); }
	inline Transform_t3316442598 * get_target_12() const { return ___target_12; }
	inline Transform_t3316442598 ** get_address_of_target_12() { return &___target_12; }
	inline void set_target_12(Transform_t3316442598 * value)
	{
		___target_12 = value;
		Il2CppCodeGenWriteBarrier((&___target_12), value);
	}

	inline static int32_t get_offset_of_progressDistance_13() { return static_cast<int32_t>(offsetof(WaypointProgressTracker_t3979672196, ___progressDistance_13)); }
	inline float get_progressDistance_13() const { return ___progressDistance_13; }
	inline float* get_address_of_progressDistance_13() { return &___progressDistance_13; }
	inline void set_progressDistance_13(float value)
	{
		___progressDistance_13 = value;
	}

	inline static int32_t get_offset_of_progressNum_14() { return static_cast<int32_t>(offsetof(WaypointProgressTracker_t3979672196, ___progressNum_14)); }
	inline int32_t get_progressNum_14() const { return ___progressNum_14; }
	inline int32_t* get_address_of_progressNum_14() { return &___progressNum_14; }
	inline void set_progressNum_14(int32_t value)
	{
		___progressNum_14 = value;
	}

	inline static int32_t get_offset_of_lastPosition_15() { return static_cast<int32_t>(offsetof(WaypointProgressTracker_t3979672196, ___lastPosition_15)); }
	inline Vector3_t2903530434  get_lastPosition_15() const { return ___lastPosition_15; }
	inline Vector3_t2903530434 * get_address_of_lastPosition_15() { return &___lastPosition_15; }
	inline void set_lastPosition_15(Vector3_t2903530434  value)
	{
		___lastPosition_15 = value;
	}

	inline static int32_t get_offset_of_speed_16() { return static_cast<int32_t>(offsetof(WaypointProgressTracker_t3979672196, ___speed_16)); }
	inline float get_speed_16() const { return ___speed_16; }
	inline float* get_address_of_speed_16() { return &___speed_16; }
	inline void set_speed_16(float value)
	{
		___speed_16 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WAYPOINTPROGRESSTRACKER_T3979672196_H
#ifndef SPIDERCONTROLLER_T373354123_H
#define SPIDERCONTROLLER_T373354123_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SpiderController
struct  SpiderController_t373354123  : public MonoBehaviour_t345688271
{
public:
	// UnityEngine.Rigidbody SpiderController::rb
	Rigidbody_t2492269564 * ___rb_2;
	// UnityEngine.Animation SpiderController::anim
	Animation_t2605054404 * ___anim_3;

public:
	inline static int32_t get_offset_of_rb_2() { return static_cast<int32_t>(offsetof(SpiderController_t373354123, ___rb_2)); }
	inline Rigidbody_t2492269564 * get_rb_2() const { return ___rb_2; }
	inline Rigidbody_t2492269564 ** get_address_of_rb_2() { return &___rb_2; }
	inline void set_rb_2(Rigidbody_t2492269564 * value)
	{
		___rb_2 = value;
		Il2CppCodeGenWriteBarrier((&___rb_2), value);
	}

	inline static int32_t get_offset_of_anim_3() { return static_cast<int32_t>(offsetof(SpiderController_t373354123, ___anim_3)); }
	inline Animation_t2605054404 * get_anim_3() const { return ___anim_3; }
	inline Animation_t2605054404 ** get_address_of_anim_3() { return &___anim_3; }
	inline void set_anim_3(Animation_t2605054404 * value)
	{
		___anim_3 = value;
		Il2CppCodeGenWriteBarrier((&___anim_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPIDERCONTROLLER_T373354123_H
#ifndef RIGIDBODYFIRSTPERSONCONTROLLER_T1713505141_H
#define RIGIDBODYFIRSTPERSONCONTROLLER_T1713505141_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Characters.FirstPerson.RigidbodyFirstPersonController
struct  RigidbodyFirstPersonController_t1713505141  : public MonoBehaviour_t345688271
{
public:
	// UnityEngine.Camera UnityStandardAssets.Characters.FirstPerson.RigidbodyFirstPersonController::cam
	Camera_t3175186167 * ___cam_2;
	// UnityStandardAssets.Characters.FirstPerson.RigidbodyFirstPersonController/MovementSettings UnityStandardAssets.Characters.FirstPerson.RigidbodyFirstPersonController::movementSettings
	MovementSettings_t2494390400 * ___movementSettings_3;
	// UnityStandardAssets.Characters.FirstPerson.MouseLook UnityStandardAssets.Characters.FirstPerson.RigidbodyFirstPersonController::mouseLook
	MouseLook_t3694953670 * ___mouseLook_4;
	// UnityStandardAssets.Characters.FirstPerson.RigidbodyFirstPersonController/AdvancedSettings UnityStandardAssets.Characters.FirstPerson.RigidbodyFirstPersonController::advancedSettings
	AdvancedSettings_t2632082398 * ___advancedSettings_5;
	// UnityEngine.Rigidbody UnityStandardAssets.Characters.FirstPerson.RigidbodyFirstPersonController::m_RigidBody
	Rigidbody_t2492269564 * ___m_RigidBody_6;
	// UnityEngine.CapsuleCollider UnityStandardAssets.Characters.FirstPerson.RigidbodyFirstPersonController::m_Capsule
	CapsuleCollider_t77672896 * ___m_Capsule_7;
	// System.Single UnityStandardAssets.Characters.FirstPerson.RigidbodyFirstPersonController::m_YRotation
	float ___m_YRotation_8;
	// UnityEngine.Vector3 UnityStandardAssets.Characters.FirstPerson.RigidbodyFirstPersonController::m_GroundContactNormal
	Vector3_t2903530434  ___m_GroundContactNormal_9;
	// System.Boolean UnityStandardAssets.Characters.FirstPerson.RigidbodyFirstPersonController::m_Jump
	bool ___m_Jump_10;
	// System.Boolean UnityStandardAssets.Characters.FirstPerson.RigidbodyFirstPersonController::m_PreviouslyGrounded
	bool ___m_PreviouslyGrounded_11;
	// System.Boolean UnityStandardAssets.Characters.FirstPerson.RigidbodyFirstPersonController::m_Jumping
	bool ___m_Jumping_12;
	// System.Boolean UnityStandardAssets.Characters.FirstPerson.RigidbodyFirstPersonController::m_IsGrounded
	bool ___m_IsGrounded_13;

public:
	inline static int32_t get_offset_of_cam_2() { return static_cast<int32_t>(offsetof(RigidbodyFirstPersonController_t1713505141, ___cam_2)); }
	inline Camera_t3175186167 * get_cam_2() const { return ___cam_2; }
	inline Camera_t3175186167 ** get_address_of_cam_2() { return &___cam_2; }
	inline void set_cam_2(Camera_t3175186167 * value)
	{
		___cam_2 = value;
		Il2CppCodeGenWriteBarrier((&___cam_2), value);
	}

	inline static int32_t get_offset_of_movementSettings_3() { return static_cast<int32_t>(offsetof(RigidbodyFirstPersonController_t1713505141, ___movementSettings_3)); }
	inline MovementSettings_t2494390400 * get_movementSettings_3() const { return ___movementSettings_3; }
	inline MovementSettings_t2494390400 ** get_address_of_movementSettings_3() { return &___movementSettings_3; }
	inline void set_movementSettings_3(MovementSettings_t2494390400 * value)
	{
		___movementSettings_3 = value;
		Il2CppCodeGenWriteBarrier((&___movementSettings_3), value);
	}

	inline static int32_t get_offset_of_mouseLook_4() { return static_cast<int32_t>(offsetof(RigidbodyFirstPersonController_t1713505141, ___mouseLook_4)); }
	inline MouseLook_t3694953670 * get_mouseLook_4() const { return ___mouseLook_4; }
	inline MouseLook_t3694953670 ** get_address_of_mouseLook_4() { return &___mouseLook_4; }
	inline void set_mouseLook_4(MouseLook_t3694953670 * value)
	{
		___mouseLook_4 = value;
		Il2CppCodeGenWriteBarrier((&___mouseLook_4), value);
	}

	inline static int32_t get_offset_of_advancedSettings_5() { return static_cast<int32_t>(offsetof(RigidbodyFirstPersonController_t1713505141, ___advancedSettings_5)); }
	inline AdvancedSettings_t2632082398 * get_advancedSettings_5() const { return ___advancedSettings_5; }
	inline AdvancedSettings_t2632082398 ** get_address_of_advancedSettings_5() { return &___advancedSettings_5; }
	inline void set_advancedSettings_5(AdvancedSettings_t2632082398 * value)
	{
		___advancedSettings_5 = value;
		Il2CppCodeGenWriteBarrier((&___advancedSettings_5), value);
	}

	inline static int32_t get_offset_of_m_RigidBody_6() { return static_cast<int32_t>(offsetof(RigidbodyFirstPersonController_t1713505141, ___m_RigidBody_6)); }
	inline Rigidbody_t2492269564 * get_m_RigidBody_6() const { return ___m_RigidBody_6; }
	inline Rigidbody_t2492269564 ** get_address_of_m_RigidBody_6() { return &___m_RigidBody_6; }
	inline void set_m_RigidBody_6(Rigidbody_t2492269564 * value)
	{
		___m_RigidBody_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_RigidBody_6), value);
	}

	inline static int32_t get_offset_of_m_Capsule_7() { return static_cast<int32_t>(offsetof(RigidbodyFirstPersonController_t1713505141, ___m_Capsule_7)); }
	inline CapsuleCollider_t77672896 * get_m_Capsule_7() const { return ___m_Capsule_7; }
	inline CapsuleCollider_t77672896 ** get_address_of_m_Capsule_7() { return &___m_Capsule_7; }
	inline void set_m_Capsule_7(CapsuleCollider_t77672896 * value)
	{
		___m_Capsule_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_Capsule_7), value);
	}

	inline static int32_t get_offset_of_m_YRotation_8() { return static_cast<int32_t>(offsetof(RigidbodyFirstPersonController_t1713505141, ___m_YRotation_8)); }
	inline float get_m_YRotation_8() const { return ___m_YRotation_8; }
	inline float* get_address_of_m_YRotation_8() { return &___m_YRotation_8; }
	inline void set_m_YRotation_8(float value)
	{
		___m_YRotation_8 = value;
	}

	inline static int32_t get_offset_of_m_GroundContactNormal_9() { return static_cast<int32_t>(offsetof(RigidbodyFirstPersonController_t1713505141, ___m_GroundContactNormal_9)); }
	inline Vector3_t2903530434  get_m_GroundContactNormal_9() const { return ___m_GroundContactNormal_9; }
	inline Vector3_t2903530434 * get_address_of_m_GroundContactNormal_9() { return &___m_GroundContactNormal_9; }
	inline void set_m_GroundContactNormal_9(Vector3_t2903530434  value)
	{
		___m_GroundContactNormal_9 = value;
	}

	inline static int32_t get_offset_of_m_Jump_10() { return static_cast<int32_t>(offsetof(RigidbodyFirstPersonController_t1713505141, ___m_Jump_10)); }
	inline bool get_m_Jump_10() const { return ___m_Jump_10; }
	inline bool* get_address_of_m_Jump_10() { return &___m_Jump_10; }
	inline void set_m_Jump_10(bool value)
	{
		___m_Jump_10 = value;
	}

	inline static int32_t get_offset_of_m_PreviouslyGrounded_11() { return static_cast<int32_t>(offsetof(RigidbodyFirstPersonController_t1713505141, ___m_PreviouslyGrounded_11)); }
	inline bool get_m_PreviouslyGrounded_11() const { return ___m_PreviouslyGrounded_11; }
	inline bool* get_address_of_m_PreviouslyGrounded_11() { return &___m_PreviouslyGrounded_11; }
	inline void set_m_PreviouslyGrounded_11(bool value)
	{
		___m_PreviouslyGrounded_11 = value;
	}

	inline static int32_t get_offset_of_m_Jumping_12() { return static_cast<int32_t>(offsetof(RigidbodyFirstPersonController_t1713505141, ___m_Jumping_12)); }
	inline bool get_m_Jumping_12() const { return ___m_Jumping_12; }
	inline bool* get_address_of_m_Jumping_12() { return &___m_Jumping_12; }
	inline void set_m_Jumping_12(bool value)
	{
		___m_Jumping_12 = value;
	}

	inline static int32_t get_offset_of_m_IsGrounded_13() { return static_cast<int32_t>(offsetof(RigidbodyFirstPersonController_t1713505141, ___m_IsGrounded_13)); }
	inline bool get_m_IsGrounded_13() const { return ___m_IsGrounded_13; }
	inline bool* get_address_of_m_IsGrounded_13() { return &___m_IsGrounded_13; }
	inline void set_m_IsGrounded_13(bool value)
	{
		___m_IsGrounded_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RIGIDBODYFIRSTPERSONCONTROLLER_T1713505141_H
#ifndef SIMPLEMOUSEROTATOR_T2630432798_H
#define SIMPLEMOUSEROTATOR_T2630432798_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.SimpleMouseRotator
struct  SimpleMouseRotator_t2630432798  : public MonoBehaviour_t345688271
{
public:
	// UnityEngine.Vector2 UnityStandardAssets.Utility.SimpleMouseRotator::rotationRange
	Vector2_t3577333262  ___rotationRange_2;
	// System.Single UnityStandardAssets.Utility.SimpleMouseRotator::rotationSpeed
	float ___rotationSpeed_3;
	// System.Single UnityStandardAssets.Utility.SimpleMouseRotator::dampingTime
	float ___dampingTime_4;
	// System.Boolean UnityStandardAssets.Utility.SimpleMouseRotator::autoZeroVerticalOnMobile
	bool ___autoZeroVerticalOnMobile_5;
	// System.Boolean UnityStandardAssets.Utility.SimpleMouseRotator::autoZeroHorizontalOnMobile
	bool ___autoZeroHorizontalOnMobile_6;
	// System.Boolean UnityStandardAssets.Utility.SimpleMouseRotator::relative
	bool ___relative_7;
	// UnityEngine.Vector3 UnityStandardAssets.Utility.SimpleMouseRotator::m_TargetAngles
	Vector3_t2903530434  ___m_TargetAngles_8;
	// UnityEngine.Vector3 UnityStandardAssets.Utility.SimpleMouseRotator::m_FollowAngles
	Vector3_t2903530434  ___m_FollowAngles_9;
	// UnityEngine.Vector3 UnityStandardAssets.Utility.SimpleMouseRotator::m_FollowVelocity
	Vector3_t2903530434  ___m_FollowVelocity_10;
	// UnityEngine.Quaternion UnityStandardAssets.Utility.SimpleMouseRotator::m_OriginalRotation
	Quaternion_t754065749  ___m_OriginalRotation_11;

public:
	inline static int32_t get_offset_of_rotationRange_2() { return static_cast<int32_t>(offsetof(SimpleMouseRotator_t2630432798, ___rotationRange_2)); }
	inline Vector2_t3577333262  get_rotationRange_2() const { return ___rotationRange_2; }
	inline Vector2_t3577333262 * get_address_of_rotationRange_2() { return &___rotationRange_2; }
	inline void set_rotationRange_2(Vector2_t3577333262  value)
	{
		___rotationRange_2 = value;
	}

	inline static int32_t get_offset_of_rotationSpeed_3() { return static_cast<int32_t>(offsetof(SimpleMouseRotator_t2630432798, ___rotationSpeed_3)); }
	inline float get_rotationSpeed_3() const { return ___rotationSpeed_3; }
	inline float* get_address_of_rotationSpeed_3() { return &___rotationSpeed_3; }
	inline void set_rotationSpeed_3(float value)
	{
		___rotationSpeed_3 = value;
	}

	inline static int32_t get_offset_of_dampingTime_4() { return static_cast<int32_t>(offsetof(SimpleMouseRotator_t2630432798, ___dampingTime_4)); }
	inline float get_dampingTime_4() const { return ___dampingTime_4; }
	inline float* get_address_of_dampingTime_4() { return &___dampingTime_4; }
	inline void set_dampingTime_4(float value)
	{
		___dampingTime_4 = value;
	}

	inline static int32_t get_offset_of_autoZeroVerticalOnMobile_5() { return static_cast<int32_t>(offsetof(SimpleMouseRotator_t2630432798, ___autoZeroVerticalOnMobile_5)); }
	inline bool get_autoZeroVerticalOnMobile_5() const { return ___autoZeroVerticalOnMobile_5; }
	inline bool* get_address_of_autoZeroVerticalOnMobile_5() { return &___autoZeroVerticalOnMobile_5; }
	inline void set_autoZeroVerticalOnMobile_5(bool value)
	{
		___autoZeroVerticalOnMobile_5 = value;
	}

	inline static int32_t get_offset_of_autoZeroHorizontalOnMobile_6() { return static_cast<int32_t>(offsetof(SimpleMouseRotator_t2630432798, ___autoZeroHorizontalOnMobile_6)); }
	inline bool get_autoZeroHorizontalOnMobile_6() const { return ___autoZeroHorizontalOnMobile_6; }
	inline bool* get_address_of_autoZeroHorizontalOnMobile_6() { return &___autoZeroHorizontalOnMobile_6; }
	inline void set_autoZeroHorizontalOnMobile_6(bool value)
	{
		___autoZeroHorizontalOnMobile_6 = value;
	}

	inline static int32_t get_offset_of_relative_7() { return static_cast<int32_t>(offsetof(SimpleMouseRotator_t2630432798, ___relative_7)); }
	inline bool get_relative_7() const { return ___relative_7; }
	inline bool* get_address_of_relative_7() { return &___relative_7; }
	inline void set_relative_7(bool value)
	{
		___relative_7 = value;
	}

	inline static int32_t get_offset_of_m_TargetAngles_8() { return static_cast<int32_t>(offsetof(SimpleMouseRotator_t2630432798, ___m_TargetAngles_8)); }
	inline Vector3_t2903530434  get_m_TargetAngles_8() const { return ___m_TargetAngles_8; }
	inline Vector3_t2903530434 * get_address_of_m_TargetAngles_8() { return &___m_TargetAngles_8; }
	inline void set_m_TargetAngles_8(Vector3_t2903530434  value)
	{
		___m_TargetAngles_8 = value;
	}

	inline static int32_t get_offset_of_m_FollowAngles_9() { return static_cast<int32_t>(offsetof(SimpleMouseRotator_t2630432798, ___m_FollowAngles_9)); }
	inline Vector3_t2903530434  get_m_FollowAngles_9() const { return ___m_FollowAngles_9; }
	inline Vector3_t2903530434 * get_address_of_m_FollowAngles_9() { return &___m_FollowAngles_9; }
	inline void set_m_FollowAngles_9(Vector3_t2903530434  value)
	{
		___m_FollowAngles_9 = value;
	}

	inline static int32_t get_offset_of_m_FollowVelocity_10() { return static_cast<int32_t>(offsetof(SimpleMouseRotator_t2630432798, ___m_FollowVelocity_10)); }
	inline Vector3_t2903530434  get_m_FollowVelocity_10() const { return ___m_FollowVelocity_10; }
	inline Vector3_t2903530434 * get_address_of_m_FollowVelocity_10() { return &___m_FollowVelocity_10; }
	inline void set_m_FollowVelocity_10(Vector3_t2903530434  value)
	{
		___m_FollowVelocity_10 = value;
	}

	inline static int32_t get_offset_of_m_OriginalRotation_11() { return static_cast<int32_t>(offsetof(SimpleMouseRotator_t2630432798, ___m_OriginalRotation_11)); }
	inline Quaternion_t754065749  get_m_OriginalRotation_11() const { return ___m_OriginalRotation_11; }
	inline Quaternion_t754065749 * get_address_of_m_OriginalRotation_11() { return &___m_OriginalRotation_11; }
	inline void set_m_OriginalRotation_11(Quaternion_t754065749  value)
	{
		___m_OriginalRotation_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SIMPLEMOUSEROTATOR_T2630432798_H
#ifndef HIDEEXCESSAREABEHAVIOUR_T554831333_H
#define HIDEEXCESSAREABEHAVIOUR_T554831333_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.HideExcessAreaBehaviour
struct  HideExcessAreaBehaviour_t554831333  : public HideExcessAreaAbstractBehaviour_t1324413388
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HIDEEXCESSAREABEHAVIOUR_T554831333_H
#ifndef BACKGROUNDPLANEBEHAVIOUR_T311858283_H
#define BACKGROUNDPLANEBEHAVIOUR_T311858283_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.BackgroundPlaneBehaviour
struct  BackgroundPlaneBehaviour_t311858283  : public BackgroundPlaneAbstractBehaviour_t4259835756
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BACKGROUNDPLANEBEHAVIOUR_T311858283_H
#ifndef SMARTTERRAINTRACKABLEBEHAVIOUR_T4179131702_H
#define SMARTTERRAINTRACKABLEBEHAVIOUR_T4179131702_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.SmartTerrainTrackableBehaviour
struct  SmartTerrainTrackableBehaviour_t4179131702  : public TrackableBehaviour_t2419079356
{
public:
	// Vuforia.SmartTerrainTrackable Vuforia.SmartTerrainTrackableBehaviour::mSmartTerrainTrackable
	RuntimeObject* ___mSmartTerrainTrackable_10;
	// System.Boolean Vuforia.SmartTerrainTrackableBehaviour::mDisableAutomaticUpdates
	bool ___mDisableAutomaticUpdates_11;
	// UnityEngine.MeshFilter Vuforia.SmartTerrainTrackableBehaviour::mMeshFilterToUpdate
	MeshFilter_t137687116 * ___mMeshFilterToUpdate_12;
	// UnityEngine.MeshCollider Vuforia.SmartTerrainTrackableBehaviour::mMeshColliderToUpdate
	MeshCollider_t4172739389 * ___mMeshColliderToUpdate_13;

public:
	inline static int32_t get_offset_of_mSmartTerrainTrackable_10() { return static_cast<int32_t>(offsetof(SmartTerrainTrackableBehaviour_t4179131702, ___mSmartTerrainTrackable_10)); }
	inline RuntimeObject* get_mSmartTerrainTrackable_10() const { return ___mSmartTerrainTrackable_10; }
	inline RuntimeObject** get_address_of_mSmartTerrainTrackable_10() { return &___mSmartTerrainTrackable_10; }
	inline void set_mSmartTerrainTrackable_10(RuntimeObject* value)
	{
		___mSmartTerrainTrackable_10 = value;
		Il2CppCodeGenWriteBarrier((&___mSmartTerrainTrackable_10), value);
	}

	inline static int32_t get_offset_of_mDisableAutomaticUpdates_11() { return static_cast<int32_t>(offsetof(SmartTerrainTrackableBehaviour_t4179131702, ___mDisableAutomaticUpdates_11)); }
	inline bool get_mDisableAutomaticUpdates_11() const { return ___mDisableAutomaticUpdates_11; }
	inline bool* get_address_of_mDisableAutomaticUpdates_11() { return &___mDisableAutomaticUpdates_11; }
	inline void set_mDisableAutomaticUpdates_11(bool value)
	{
		___mDisableAutomaticUpdates_11 = value;
	}

	inline static int32_t get_offset_of_mMeshFilterToUpdate_12() { return static_cast<int32_t>(offsetof(SmartTerrainTrackableBehaviour_t4179131702, ___mMeshFilterToUpdate_12)); }
	inline MeshFilter_t137687116 * get_mMeshFilterToUpdate_12() const { return ___mMeshFilterToUpdate_12; }
	inline MeshFilter_t137687116 ** get_address_of_mMeshFilterToUpdate_12() { return &___mMeshFilterToUpdate_12; }
	inline void set_mMeshFilterToUpdate_12(MeshFilter_t137687116 * value)
	{
		___mMeshFilterToUpdate_12 = value;
		Il2CppCodeGenWriteBarrier((&___mMeshFilterToUpdate_12), value);
	}

	inline static int32_t get_offset_of_mMeshColliderToUpdate_13() { return static_cast<int32_t>(offsetof(SmartTerrainTrackableBehaviour_t4179131702, ___mMeshColliderToUpdate_13)); }
	inline MeshCollider_t4172739389 * get_mMeshColliderToUpdate_13() const { return ___mMeshColliderToUpdate_13; }
	inline MeshCollider_t4172739389 ** get_address_of_mMeshColliderToUpdate_13() { return &___mMeshColliderToUpdate_13; }
	inline void set_mMeshColliderToUpdate_13(MeshCollider_t4172739389 * value)
	{
		___mMeshColliderToUpdate_13 = value;
		Il2CppCodeGenWriteBarrier((&___mMeshColliderToUpdate_13), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SMARTTERRAINTRACKABLEBEHAVIOUR_T4179131702_H
#ifndef MASKOUTBEHAVIOUR_T62027326_H
#define MASKOUTBEHAVIOUR_T62027326_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.MaskOutBehaviour
struct  MaskOutBehaviour_t62027326  : public MaskOutAbstractBehaviour_t2174599097
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MASKOUTBEHAVIOUR_T62027326_H
#ifndef DATASETTRACKABLEBEHAVIOUR_T3960979584_H
#define DATASETTRACKABLEBEHAVIOUR_T3960979584_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.DataSetTrackableBehaviour
struct  DataSetTrackableBehaviour_t3960979584  : public TrackableBehaviour_t2419079356
{
public:
	// System.String Vuforia.DataSetTrackableBehaviour::mDataSetPath
	String_t* ___mDataSetPath_10;
	// System.Boolean Vuforia.DataSetTrackableBehaviour::mExtendedTracking
	bool ___mExtendedTracking_11;
	// System.Boolean Vuforia.DataSetTrackableBehaviour::mInitializeSmartTerrain
	bool ___mInitializeSmartTerrain_12;
	// Vuforia.ReconstructionFromTargetAbstractBehaviour Vuforia.DataSetTrackableBehaviour::mReconstructionToInitialize
	ReconstructionFromTargetAbstractBehaviour_t2676804737 * ___mReconstructionToInitialize_13;
	// UnityEngine.Vector3 Vuforia.DataSetTrackableBehaviour::mSmartTerrainOccluderBoundsMin
	Vector3_t2903530434  ___mSmartTerrainOccluderBoundsMin_14;
	// UnityEngine.Vector3 Vuforia.DataSetTrackableBehaviour::mSmartTerrainOccluderBoundsMax
	Vector3_t2903530434  ___mSmartTerrainOccluderBoundsMax_15;
	// System.Boolean Vuforia.DataSetTrackableBehaviour::mIsSmartTerrainOccluderOffset
	bool ___mIsSmartTerrainOccluderOffset_16;
	// UnityEngine.Vector3 Vuforia.DataSetTrackableBehaviour::mSmartTerrainOccluderOffset
	Vector3_t2903530434  ___mSmartTerrainOccluderOffset_17;
	// UnityEngine.Quaternion Vuforia.DataSetTrackableBehaviour::mSmartTerrainOccluderRotation
	Quaternion_t754065749  ___mSmartTerrainOccluderRotation_18;
	// System.Boolean Vuforia.DataSetTrackableBehaviour::mAutoSetOccluderFromTargetSize
	bool ___mAutoSetOccluderFromTargetSize_19;

public:
	inline static int32_t get_offset_of_mDataSetPath_10() { return static_cast<int32_t>(offsetof(DataSetTrackableBehaviour_t3960979584, ___mDataSetPath_10)); }
	inline String_t* get_mDataSetPath_10() const { return ___mDataSetPath_10; }
	inline String_t** get_address_of_mDataSetPath_10() { return &___mDataSetPath_10; }
	inline void set_mDataSetPath_10(String_t* value)
	{
		___mDataSetPath_10 = value;
		Il2CppCodeGenWriteBarrier((&___mDataSetPath_10), value);
	}

	inline static int32_t get_offset_of_mExtendedTracking_11() { return static_cast<int32_t>(offsetof(DataSetTrackableBehaviour_t3960979584, ___mExtendedTracking_11)); }
	inline bool get_mExtendedTracking_11() const { return ___mExtendedTracking_11; }
	inline bool* get_address_of_mExtendedTracking_11() { return &___mExtendedTracking_11; }
	inline void set_mExtendedTracking_11(bool value)
	{
		___mExtendedTracking_11 = value;
	}

	inline static int32_t get_offset_of_mInitializeSmartTerrain_12() { return static_cast<int32_t>(offsetof(DataSetTrackableBehaviour_t3960979584, ___mInitializeSmartTerrain_12)); }
	inline bool get_mInitializeSmartTerrain_12() const { return ___mInitializeSmartTerrain_12; }
	inline bool* get_address_of_mInitializeSmartTerrain_12() { return &___mInitializeSmartTerrain_12; }
	inline void set_mInitializeSmartTerrain_12(bool value)
	{
		___mInitializeSmartTerrain_12 = value;
	}

	inline static int32_t get_offset_of_mReconstructionToInitialize_13() { return static_cast<int32_t>(offsetof(DataSetTrackableBehaviour_t3960979584, ___mReconstructionToInitialize_13)); }
	inline ReconstructionFromTargetAbstractBehaviour_t2676804737 * get_mReconstructionToInitialize_13() const { return ___mReconstructionToInitialize_13; }
	inline ReconstructionFromTargetAbstractBehaviour_t2676804737 ** get_address_of_mReconstructionToInitialize_13() { return &___mReconstructionToInitialize_13; }
	inline void set_mReconstructionToInitialize_13(ReconstructionFromTargetAbstractBehaviour_t2676804737 * value)
	{
		___mReconstructionToInitialize_13 = value;
		Il2CppCodeGenWriteBarrier((&___mReconstructionToInitialize_13), value);
	}

	inline static int32_t get_offset_of_mSmartTerrainOccluderBoundsMin_14() { return static_cast<int32_t>(offsetof(DataSetTrackableBehaviour_t3960979584, ___mSmartTerrainOccluderBoundsMin_14)); }
	inline Vector3_t2903530434  get_mSmartTerrainOccluderBoundsMin_14() const { return ___mSmartTerrainOccluderBoundsMin_14; }
	inline Vector3_t2903530434 * get_address_of_mSmartTerrainOccluderBoundsMin_14() { return &___mSmartTerrainOccluderBoundsMin_14; }
	inline void set_mSmartTerrainOccluderBoundsMin_14(Vector3_t2903530434  value)
	{
		___mSmartTerrainOccluderBoundsMin_14 = value;
	}

	inline static int32_t get_offset_of_mSmartTerrainOccluderBoundsMax_15() { return static_cast<int32_t>(offsetof(DataSetTrackableBehaviour_t3960979584, ___mSmartTerrainOccluderBoundsMax_15)); }
	inline Vector3_t2903530434  get_mSmartTerrainOccluderBoundsMax_15() const { return ___mSmartTerrainOccluderBoundsMax_15; }
	inline Vector3_t2903530434 * get_address_of_mSmartTerrainOccluderBoundsMax_15() { return &___mSmartTerrainOccluderBoundsMax_15; }
	inline void set_mSmartTerrainOccluderBoundsMax_15(Vector3_t2903530434  value)
	{
		___mSmartTerrainOccluderBoundsMax_15 = value;
	}

	inline static int32_t get_offset_of_mIsSmartTerrainOccluderOffset_16() { return static_cast<int32_t>(offsetof(DataSetTrackableBehaviour_t3960979584, ___mIsSmartTerrainOccluderOffset_16)); }
	inline bool get_mIsSmartTerrainOccluderOffset_16() const { return ___mIsSmartTerrainOccluderOffset_16; }
	inline bool* get_address_of_mIsSmartTerrainOccluderOffset_16() { return &___mIsSmartTerrainOccluderOffset_16; }
	inline void set_mIsSmartTerrainOccluderOffset_16(bool value)
	{
		___mIsSmartTerrainOccluderOffset_16 = value;
	}

	inline static int32_t get_offset_of_mSmartTerrainOccluderOffset_17() { return static_cast<int32_t>(offsetof(DataSetTrackableBehaviour_t3960979584, ___mSmartTerrainOccluderOffset_17)); }
	inline Vector3_t2903530434  get_mSmartTerrainOccluderOffset_17() const { return ___mSmartTerrainOccluderOffset_17; }
	inline Vector3_t2903530434 * get_address_of_mSmartTerrainOccluderOffset_17() { return &___mSmartTerrainOccluderOffset_17; }
	inline void set_mSmartTerrainOccluderOffset_17(Vector3_t2903530434  value)
	{
		___mSmartTerrainOccluderOffset_17 = value;
	}

	inline static int32_t get_offset_of_mSmartTerrainOccluderRotation_18() { return static_cast<int32_t>(offsetof(DataSetTrackableBehaviour_t3960979584, ___mSmartTerrainOccluderRotation_18)); }
	inline Quaternion_t754065749  get_mSmartTerrainOccluderRotation_18() const { return ___mSmartTerrainOccluderRotation_18; }
	inline Quaternion_t754065749 * get_address_of_mSmartTerrainOccluderRotation_18() { return &___mSmartTerrainOccluderRotation_18; }
	inline void set_mSmartTerrainOccluderRotation_18(Quaternion_t754065749  value)
	{
		___mSmartTerrainOccluderRotation_18 = value;
	}

	inline static int32_t get_offset_of_mAutoSetOccluderFromTargetSize_19() { return static_cast<int32_t>(offsetof(DataSetTrackableBehaviour_t3960979584, ___mAutoSetOccluderFromTargetSize_19)); }
	inline bool get_mAutoSetOccluderFromTargetSize_19() const { return ___mAutoSetOccluderFromTargetSize_19; }
	inline bool* get_address_of_mAutoSetOccluderFromTargetSize_19() { return &___mAutoSetOccluderFromTargetSize_19; }
	inline void set_mAutoSetOccluderFromTargetSize_19(bool value)
	{
		___mAutoSetOccluderFromTargetSize_19 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATASETTRACKABLEBEHAVIOUR_T3960979584_H
#ifndef CLOUDRECOBEHAVIOUR_T2989569978_H
#define CLOUDRECOBEHAVIOUR_T2989569978_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.CloudRecoBehaviour
struct  CloudRecoBehaviour_t2989569978  : public CloudRecoAbstractBehaviour_t2501456771
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CLOUDRECOBEHAVIOUR_T2989569978_H
#ifndef RECONSTRUCTIONFROMTARGETBEHAVIOUR_T2819884381_H
#define RECONSTRUCTIONFROMTARGETBEHAVIOUR_T2819884381_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.ReconstructionFromTargetBehaviour
struct  ReconstructionFromTargetBehaviour_t2819884381  : public ReconstructionFromTargetAbstractBehaviour_t2676804737
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RECONSTRUCTIONFROMTARGETBEHAVIOUR_T2819884381_H
#ifndef TEXTRECOBEHAVIOUR_T3253755448_H
#define TEXTRECOBEHAVIOUR_T3253755448_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.TextRecoBehaviour
struct  TextRecoBehaviour_t3253755448  : public TextRecoAbstractBehaviour_t1751158196
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTRECOBEHAVIOUR_T3253755448_H
#ifndef RECONSTRUCTIONBEHAVIOUR_T3936121853_H
#define RECONSTRUCTIONBEHAVIOUR_T3936121853_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.ReconstructionBehaviour
struct  ReconstructionBehaviour_t3936121853  : public ReconstructionAbstractBehaviour_t954468633
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RECONSTRUCTIONBEHAVIOUR_T3936121853_H
#ifndef PROPABSTRACTBEHAVIOUR_T2648882166_H
#define PROPABSTRACTBEHAVIOUR_T2648882166_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.PropAbstractBehaviour
struct  PropAbstractBehaviour_t2648882166  : public SmartTerrainTrackableBehaviour_t4179131702
{
public:
	// Vuforia.Prop Vuforia.PropAbstractBehaviour::mProp
	RuntimeObject* ___mProp_14;
	// UnityEngine.BoxCollider Vuforia.PropAbstractBehaviour::mBoxColliderToUpdate
	BoxCollider_t1861275623 * ___mBoxColliderToUpdate_15;

public:
	inline static int32_t get_offset_of_mProp_14() { return static_cast<int32_t>(offsetof(PropAbstractBehaviour_t2648882166, ___mProp_14)); }
	inline RuntimeObject* get_mProp_14() const { return ___mProp_14; }
	inline RuntimeObject** get_address_of_mProp_14() { return &___mProp_14; }
	inline void set_mProp_14(RuntimeObject* value)
	{
		___mProp_14 = value;
		Il2CppCodeGenWriteBarrier((&___mProp_14), value);
	}

	inline static int32_t get_offset_of_mBoxColliderToUpdate_15() { return static_cast<int32_t>(offsetof(PropAbstractBehaviour_t2648882166, ___mBoxColliderToUpdate_15)); }
	inline BoxCollider_t1861275623 * get_mBoxColliderToUpdate_15() const { return ___mBoxColliderToUpdate_15; }
	inline BoxCollider_t1861275623 ** get_address_of_mBoxColliderToUpdate_15() { return &___mBoxColliderToUpdate_15; }
	inline void set_mBoxColliderToUpdate_15(BoxCollider_t1861275623 * value)
	{
		___mBoxColliderToUpdate_15 = value;
		Il2CppCodeGenWriteBarrier((&___mBoxColliderToUpdate_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROPABSTRACTBEHAVIOUR_T2648882166_H
#ifndef OBJECTTARGETABSTRACTBEHAVIOUR_T1410714589_H
#define OBJECTTARGETABSTRACTBEHAVIOUR_T1410714589_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.ObjectTargetAbstractBehaviour
struct  ObjectTargetAbstractBehaviour_t1410714589  : public DataSetTrackableBehaviour_t3960979584
{
public:
	// Vuforia.ObjectTarget Vuforia.ObjectTargetAbstractBehaviour::mObjectTarget
	RuntimeObject* ___mObjectTarget_20;
	// System.Single Vuforia.ObjectTargetAbstractBehaviour::mAspectRatioXY
	float ___mAspectRatioXY_21;
	// System.Single Vuforia.ObjectTargetAbstractBehaviour::mAspectRatioXZ
	float ___mAspectRatioXZ_22;
	// System.Boolean Vuforia.ObjectTargetAbstractBehaviour::mShowBoundingBox
	bool ___mShowBoundingBox_23;
	// UnityEngine.Vector3 Vuforia.ObjectTargetAbstractBehaviour::mBBoxMin
	Vector3_t2903530434  ___mBBoxMin_24;
	// UnityEngine.Vector3 Vuforia.ObjectTargetAbstractBehaviour::mBBoxMax
	Vector3_t2903530434  ___mBBoxMax_25;
	// UnityEngine.Texture2D Vuforia.ObjectTargetAbstractBehaviour::mPreviewImage
	Texture2D_t415585320 * ___mPreviewImage_26;
	// System.Single Vuforia.ObjectTargetAbstractBehaviour::mLength
	float ___mLength_27;
	// System.Single Vuforia.ObjectTargetAbstractBehaviour::mWidth
	float ___mWidth_28;
	// System.Single Vuforia.ObjectTargetAbstractBehaviour::mHeight
	float ___mHeight_29;
	// System.Single Vuforia.ObjectTargetAbstractBehaviour::mLastTransformScale
	float ___mLastTransformScale_30;
	// UnityEngine.Vector3 Vuforia.ObjectTargetAbstractBehaviour::mLastSize
	Vector3_t2903530434  ___mLastSize_31;

public:
	inline static int32_t get_offset_of_mObjectTarget_20() { return static_cast<int32_t>(offsetof(ObjectTargetAbstractBehaviour_t1410714589, ___mObjectTarget_20)); }
	inline RuntimeObject* get_mObjectTarget_20() const { return ___mObjectTarget_20; }
	inline RuntimeObject** get_address_of_mObjectTarget_20() { return &___mObjectTarget_20; }
	inline void set_mObjectTarget_20(RuntimeObject* value)
	{
		___mObjectTarget_20 = value;
		Il2CppCodeGenWriteBarrier((&___mObjectTarget_20), value);
	}

	inline static int32_t get_offset_of_mAspectRatioXY_21() { return static_cast<int32_t>(offsetof(ObjectTargetAbstractBehaviour_t1410714589, ___mAspectRatioXY_21)); }
	inline float get_mAspectRatioXY_21() const { return ___mAspectRatioXY_21; }
	inline float* get_address_of_mAspectRatioXY_21() { return &___mAspectRatioXY_21; }
	inline void set_mAspectRatioXY_21(float value)
	{
		___mAspectRatioXY_21 = value;
	}

	inline static int32_t get_offset_of_mAspectRatioXZ_22() { return static_cast<int32_t>(offsetof(ObjectTargetAbstractBehaviour_t1410714589, ___mAspectRatioXZ_22)); }
	inline float get_mAspectRatioXZ_22() const { return ___mAspectRatioXZ_22; }
	inline float* get_address_of_mAspectRatioXZ_22() { return &___mAspectRatioXZ_22; }
	inline void set_mAspectRatioXZ_22(float value)
	{
		___mAspectRatioXZ_22 = value;
	}

	inline static int32_t get_offset_of_mShowBoundingBox_23() { return static_cast<int32_t>(offsetof(ObjectTargetAbstractBehaviour_t1410714589, ___mShowBoundingBox_23)); }
	inline bool get_mShowBoundingBox_23() const { return ___mShowBoundingBox_23; }
	inline bool* get_address_of_mShowBoundingBox_23() { return &___mShowBoundingBox_23; }
	inline void set_mShowBoundingBox_23(bool value)
	{
		___mShowBoundingBox_23 = value;
	}

	inline static int32_t get_offset_of_mBBoxMin_24() { return static_cast<int32_t>(offsetof(ObjectTargetAbstractBehaviour_t1410714589, ___mBBoxMin_24)); }
	inline Vector3_t2903530434  get_mBBoxMin_24() const { return ___mBBoxMin_24; }
	inline Vector3_t2903530434 * get_address_of_mBBoxMin_24() { return &___mBBoxMin_24; }
	inline void set_mBBoxMin_24(Vector3_t2903530434  value)
	{
		___mBBoxMin_24 = value;
	}

	inline static int32_t get_offset_of_mBBoxMax_25() { return static_cast<int32_t>(offsetof(ObjectTargetAbstractBehaviour_t1410714589, ___mBBoxMax_25)); }
	inline Vector3_t2903530434  get_mBBoxMax_25() const { return ___mBBoxMax_25; }
	inline Vector3_t2903530434 * get_address_of_mBBoxMax_25() { return &___mBBoxMax_25; }
	inline void set_mBBoxMax_25(Vector3_t2903530434  value)
	{
		___mBBoxMax_25 = value;
	}

	inline static int32_t get_offset_of_mPreviewImage_26() { return static_cast<int32_t>(offsetof(ObjectTargetAbstractBehaviour_t1410714589, ___mPreviewImage_26)); }
	inline Texture2D_t415585320 * get_mPreviewImage_26() const { return ___mPreviewImage_26; }
	inline Texture2D_t415585320 ** get_address_of_mPreviewImage_26() { return &___mPreviewImage_26; }
	inline void set_mPreviewImage_26(Texture2D_t415585320 * value)
	{
		___mPreviewImage_26 = value;
		Il2CppCodeGenWriteBarrier((&___mPreviewImage_26), value);
	}

	inline static int32_t get_offset_of_mLength_27() { return static_cast<int32_t>(offsetof(ObjectTargetAbstractBehaviour_t1410714589, ___mLength_27)); }
	inline float get_mLength_27() const { return ___mLength_27; }
	inline float* get_address_of_mLength_27() { return &___mLength_27; }
	inline void set_mLength_27(float value)
	{
		___mLength_27 = value;
	}

	inline static int32_t get_offset_of_mWidth_28() { return static_cast<int32_t>(offsetof(ObjectTargetAbstractBehaviour_t1410714589, ___mWidth_28)); }
	inline float get_mWidth_28() const { return ___mWidth_28; }
	inline float* get_address_of_mWidth_28() { return &___mWidth_28; }
	inline void set_mWidth_28(float value)
	{
		___mWidth_28 = value;
	}

	inline static int32_t get_offset_of_mHeight_29() { return static_cast<int32_t>(offsetof(ObjectTargetAbstractBehaviour_t1410714589, ___mHeight_29)); }
	inline float get_mHeight_29() const { return ___mHeight_29; }
	inline float* get_address_of_mHeight_29() { return &___mHeight_29; }
	inline void set_mHeight_29(float value)
	{
		___mHeight_29 = value;
	}

	inline static int32_t get_offset_of_mLastTransformScale_30() { return static_cast<int32_t>(offsetof(ObjectTargetAbstractBehaviour_t1410714589, ___mLastTransformScale_30)); }
	inline float get_mLastTransformScale_30() const { return ___mLastTransformScale_30; }
	inline float* get_address_of_mLastTransformScale_30() { return &___mLastTransformScale_30; }
	inline void set_mLastTransformScale_30(float value)
	{
		___mLastTransformScale_30 = value;
	}

	inline static int32_t get_offset_of_mLastSize_31() { return static_cast<int32_t>(offsetof(ObjectTargetAbstractBehaviour_t1410714589, ___mLastSize_31)); }
	inline Vector3_t2903530434  get_mLastSize_31() const { return ___mLastSize_31; }
	inline Vector3_t2903530434 * get_address_of_mLastSize_31() { return &___mLastSize_31; }
	inline void set_mLastSize_31(Vector3_t2903530434  value)
	{
		___mLastSize_31 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OBJECTTARGETABSTRACTBEHAVIOUR_T1410714589_H
#ifndef MULTITARGETABSTRACTBEHAVIOUR_T1527469731_H
#define MULTITARGETABSTRACTBEHAVIOUR_T1527469731_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.MultiTargetAbstractBehaviour
struct  MultiTargetAbstractBehaviour_t1527469731  : public DataSetTrackableBehaviour_t3960979584
{
public:
	// Vuforia.MultiTarget Vuforia.MultiTargetAbstractBehaviour::mMultiTarget
	RuntimeObject* ___mMultiTarget_20;

public:
	inline static int32_t get_offset_of_mMultiTarget_20() { return static_cast<int32_t>(offsetof(MultiTargetAbstractBehaviour_t1527469731, ___mMultiTarget_20)); }
	inline RuntimeObject* get_mMultiTarget_20() const { return ___mMultiTarget_20; }
	inline RuntimeObject** get_address_of_mMultiTarget_20() { return &___mMultiTarget_20; }
	inline void set_mMultiTarget_20(RuntimeObject* value)
	{
		___mMultiTarget_20 = value;
		Il2CppCodeGenWriteBarrier((&___mMultiTarget_20), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MULTITARGETABSTRACTBEHAVIOUR_T1527469731_H
#ifndef IMAGETARGETABSTRACTBEHAVIOUR_T355543434_H
#define IMAGETARGETABSTRACTBEHAVIOUR_T355543434_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.ImageTargetAbstractBehaviour
struct  ImageTargetAbstractBehaviour_t355543434  : public DataSetTrackableBehaviour_t3960979584
{
public:
	// System.Single Vuforia.ImageTargetAbstractBehaviour::mAspectRatio
	float ___mAspectRatio_20;
	// Vuforia.ImageTargetType Vuforia.ImageTargetAbstractBehaviour::mImageTargetType
	int32_t ___mImageTargetType_21;
	// System.Single Vuforia.ImageTargetAbstractBehaviour::mWidth
	float ___mWidth_22;
	// System.Single Vuforia.ImageTargetAbstractBehaviour::mHeight
	float ___mHeight_23;
	// Vuforia.ImageTarget Vuforia.ImageTargetAbstractBehaviour::mImageTarget
	RuntimeObject* ___mImageTarget_24;
	// System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VirtualButtonAbstractBehaviour> Vuforia.ImageTargetAbstractBehaviour::mVirtualButtonBehaviours
	Dictionary_2_t3161827512 * ___mVirtualButtonBehaviours_25;
	// System.Single Vuforia.ImageTargetAbstractBehaviour::mLastTransformScale
	float ___mLastTransformScale_26;
	// UnityEngine.Vector2 Vuforia.ImageTargetAbstractBehaviour::mLastSize
	Vector2_t3577333262  ___mLastSize_27;

public:
	inline static int32_t get_offset_of_mAspectRatio_20() { return static_cast<int32_t>(offsetof(ImageTargetAbstractBehaviour_t355543434, ___mAspectRatio_20)); }
	inline float get_mAspectRatio_20() const { return ___mAspectRatio_20; }
	inline float* get_address_of_mAspectRatio_20() { return &___mAspectRatio_20; }
	inline void set_mAspectRatio_20(float value)
	{
		___mAspectRatio_20 = value;
	}

	inline static int32_t get_offset_of_mImageTargetType_21() { return static_cast<int32_t>(offsetof(ImageTargetAbstractBehaviour_t355543434, ___mImageTargetType_21)); }
	inline int32_t get_mImageTargetType_21() const { return ___mImageTargetType_21; }
	inline int32_t* get_address_of_mImageTargetType_21() { return &___mImageTargetType_21; }
	inline void set_mImageTargetType_21(int32_t value)
	{
		___mImageTargetType_21 = value;
	}

	inline static int32_t get_offset_of_mWidth_22() { return static_cast<int32_t>(offsetof(ImageTargetAbstractBehaviour_t355543434, ___mWidth_22)); }
	inline float get_mWidth_22() const { return ___mWidth_22; }
	inline float* get_address_of_mWidth_22() { return &___mWidth_22; }
	inline void set_mWidth_22(float value)
	{
		___mWidth_22 = value;
	}

	inline static int32_t get_offset_of_mHeight_23() { return static_cast<int32_t>(offsetof(ImageTargetAbstractBehaviour_t355543434, ___mHeight_23)); }
	inline float get_mHeight_23() const { return ___mHeight_23; }
	inline float* get_address_of_mHeight_23() { return &___mHeight_23; }
	inline void set_mHeight_23(float value)
	{
		___mHeight_23 = value;
	}

	inline static int32_t get_offset_of_mImageTarget_24() { return static_cast<int32_t>(offsetof(ImageTargetAbstractBehaviour_t355543434, ___mImageTarget_24)); }
	inline RuntimeObject* get_mImageTarget_24() const { return ___mImageTarget_24; }
	inline RuntimeObject** get_address_of_mImageTarget_24() { return &___mImageTarget_24; }
	inline void set_mImageTarget_24(RuntimeObject* value)
	{
		___mImageTarget_24 = value;
		Il2CppCodeGenWriteBarrier((&___mImageTarget_24), value);
	}

	inline static int32_t get_offset_of_mVirtualButtonBehaviours_25() { return static_cast<int32_t>(offsetof(ImageTargetAbstractBehaviour_t355543434, ___mVirtualButtonBehaviours_25)); }
	inline Dictionary_2_t3161827512 * get_mVirtualButtonBehaviours_25() const { return ___mVirtualButtonBehaviours_25; }
	inline Dictionary_2_t3161827512 ** get_address_of_mVirtualButtonBehaviours_25() { return &___mVirtualButtonBehaviours_25; }
	inline void set_mVirtualButtonBehaviours_25(Dictionary_2_t3161827512 * value)
	{
		___mVirtualButtonBehaviours_25 = value;
		Il2CppCodeGenWriteBarrier((&___mVirtualButtonBehaviours_25), value);
	}

	inline static int32_t get_offset_of_mLastTransformScale_26() { return static_cast<int32_t>(offsetof(ImageTargetAbstractBehaviour_t355543434, ___mLastTransformScale_26)); }
	inline float get_mLastTransformScale_26() const { return ___mLastTransformScale_26; }
	inline float* get_address_of_mLastTransformScale_26() { return &___mLastTransformScale_26; }
	inline void set_mLastTransformScale_26(float value)
	{
		___mLastTransformScale_26 = value;
	}

	inline static int32_t get_offset_of_mLastSize_27() { return static_cast<int32_t>(offsetof(ImageTargetAbstractBehaviour_t355543434, ___mLastSize_27)); }
	inline Vector2_t3577333262  get_mLastSize_27() const { return ___mLastSize_27; }
	inline Vector2_t3577333262 * get_address_of_mLastSize_27() { return &___mLastSize_27; }
	inline void set_mLastSize_27(Vector2_t3577333262  value)
	{
		___mLastSize_27 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IMAGETARGETABSTRACTBEHAVIOUR_T355543434_H
#ifndef CYLINDERTARGETABSTRACTBEHAVIOUR_T2158579059_H
#define CYLINDERTARGETABSTRACTBEHAVIOUR_T2158579059_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.CylinderTargetAbstractBehaviour
struct  CylinderTargetAbstractBehaviour_t2158579059  : public DataSetTrackableBehaviour_t3960979584
{
public:
	// Vuforia.CylinderTarget Vuforia.CylinderTargetAbstractBehaviour::mCylinderTarget
	RuntimeObject* ___mCylinderTarget_20;
	// System.Single Vuforia.CylinderTargetAbstractBehaviour::mTopDiameterRatio
	float ___mTopDiameterRatio_21;
	// System.Single Vuforia.CylinderTargetAbstractBehaviour::mBottomDiameterRatio
	float ___mBottomDiameterRatio_22;
	// System.Single Vuforia.CylinderTargetAbstractBehaviour::mSideLength
	float ___mSideLength_23;
	// System.Single Vuforia.CylinderTargetAbstractBehaviour::mTopDiameter
	float ___mTopDiameter_24;
	// System.Single Vuforia.CylinderTargetAbstractBehaviour::mBottomDiameter
	float ___mBottomDiameter_25;
	// System.Int32 Vuforia.CylinderTargetAbstractBehaviour::mFrameIndex
	int32_t ___mFrameIndex_26;
	// System.Int32 Vuforia.CylinderTargetAbstractBehaviour::mUpdateFrameIndex
	int32_t ___mUpdateFrameIndex_27;
	// System.Single Vuforia.CylinderTargetAbstractBehaviour::mFutureScale
	float ___mFutureScale_28;
	// System.Single Vuforia.CylinderTargetAbstractBehaviour::mLastTransformScale
	float ___mLastTransformScale_29;

public:
	inline static int32_t get_offset_of_mCylinderTarget_20() { return static_cast<int32_t>(offsetof(CylinderTargetAbstractBehaviour_t2158579059, ___mCylinderTarget_20)); }
	inline RuntimeObject* get_mCylinderTarget_20() const { return ___mCylinderTarget_20; }
	inline RuntimeObject** get_address_of_mCylinderTarget_20() { return &___mCylinderTarget_20; }
	inline void set_mCylinderTarget_20(RuntimeObject* value)
	{
		___mCylinderTarget_20 = value;
		Il2CppCodeGenWriteBarrier((&___mCylinderTarget_20), value);
	}

	inline static int32_t get_offset_of_mTopDiameterRatio_21() { return static_cast<int32_t>(offsetof(CylinderTargetAbstractBehaviour_t2158579059, ___mTopDiameterRatio_21)); }
	inline float get_mTopDiameterRatio_21() const { return ___mTopDiameterRatio_21; }
	inline float* get_address_of_mTopDiameterRatio_21() { return &___mTopDiameterRatio_21; }
	inline void set_mTopDiameterRatio_21(float value)
	{
		___mTopDiameterRatio_21 = value;
	}

	inline static int32_t get_offset_of_mBottomDiameterRatio_22() { return static_cast<int32_t>(offsetof(CylinderTargetAbstractBehaviour_t2158579059, ___mBottomDiameterRatio_22)); }
	inline float get_mBottomDiameterRatio_22() const { return ___mBottomDiameterRatio_22; }
	inline float* get_address_of_mBottomDiameterRatio_22() { return &___mBottomDiameterRatio_22; }
	inline void set_mBottomDiameterRatio_22(float value)
	{
		___mBottomDiameterRatio_22 = value;
	}

	inline static int32_t get_offset_of_mSideLength_23() { return static_cast<int32_t>(offsetof(CylinderTargetAbstractBehaviour_t2158579059, ___mSideLength_23)); }
	inline float get_mSideLength_23() const { return ___mSideLength_23; }
	inline float* get_address_of_mSideLength_23() { return &___mSideLength_23; }
	inline void set_mSideLength_23(float value)
	{
		___mSideLength_23 = value;
	}

	inline static int32_t get_offset_of_mTopDiameter_24() { return static_cast<int32_t>(offsetof(CylinderTargetAbstractBehaviour_t2158579059, ___mTopDiameter_24)); }
	inline float get_mTopDiameter_24() const { return ___mTopDiameter_24; }
	inline float* get_address_of_mTopDiameter_24() { return &___mTopDiameter_24; }
	inline void set_mTopDiameter_24(float value)
	{
		___mTopDiameter_24 = value;
	}

	inline static int32_t get_offset_of_mBottomDiameter_25() { return static_cast<int32_t>(offsetof(CylinderTargetAbstractBehaviour_t2158579059, ___mBottomDiameter_25)); }
	inline float get_mBottomDiameter_25() const { return ___mBottomDiameter_25; }
	inline float* get_address_of_mBottomDiameter_25() { return &___mBottomDiameter_25; }
	inline void set_mBottomDiameter_25(float value)
	{
		___mBottomDiameter_25 = value;
	}

	inline static int32_t get_offset_of_mFrameIndex_26() { return static_cast<int32_t>(offsetof(CylinderTargetAbstractBehaviour_t2158579059, ___mFrameIndex_26)); }
	inline int32_t get_mFrameIndex_26() const { return ___mFrameIndex_26; }
	inline int32_t* get_address_of_mFrameIndex_26() { return &___mFrameIndex_26; }
	inline void set_mFrameIndex_26(int32_t value)
	{
		___mFrameIndex_26 = value;
	}

	inline static int32_t get_offset_of_mUpdateFrameIndex_27() { return static_cast<int32_t>(offsetof(CylinderTargetAbstractBehaviour_t2158579059, ___mUpdateFrameIndex_27)); }
	inline int32_t get_mUpdateFrameIndex_27() const { return ___mUpdateFrameIndex_27; }
	inline int32_t* get_address_of_mUpdateFrameIndex_27() { return &___mUpdateFrameIndex_27; }
	inline void set_mUpdateFrameIndex_27(int32_t value)
	{
		___mUpdateFrameIndex_27 = value;
	}

	inline static int32_t get_offset_of_mFutureScale_28() { return static_cast<int32_t>(offsetof(CylinderTargetAbstractBehaviour_t2158579059, ___mFutureScale_28)); }
	inline float get_mFutureScale_28() const { return ___mFutureScale_28; }
	inline float* get_address_of_mFutureScale_28() { return &___mFutureScale_28; }
	inline void set_mFutureScale_28(float value)
	{
		___mFutureScale_28 = value;
	}

	inline static int32_t get_offset_of_mLastTransformScale_29() { return static_cast<int32_t>(offsetof(CylinderTargetAbstractBehaviour_t2158579059, ___mLastTransformScale_29)); }
	inline float get_mLastTransformScale_29() const { return ___mLastTransformScale_29; }
	inline float* get_address_of_mLastTransformScale_29() { return &___mLastTransformScale_29; }
	inline void set_mLastTransformScale_29(float value)
	{
		___mLastTransformScale_29 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CYLINDERTARGETABSTRACTBEHAVIOUR_T2158579059_H
#ifndef SURFACEABSTRACTBEHAVIOUR_T272541176_H
#define SURFACEABSTRACTBEHAVIOUR_T272541176_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.SurfaceAbstractBehaviour
struct  SurfaceAbstractBehaviour_t272541176  : public SmartTerrainTrackableBehaviour_t4179131702
{
public:
	// Vuforia.Surface Vuforia.SurfaceAbstractBehaviour::mSurface
	RuntimeObject* ___mSurface_14;

public:
	inline static int32_t get_offset_of_mSurface_14() { return static_cast<int32_t>(offsetof(SurfaceAbstractBehaviour_t272541176, ___mSurface_14)); }
	inline RuntimeObject* get_mSurface_14() const { return ___mSurface_14; }
	inline RuntimeObject** get_address_of_mSurface_14() { return &___mSurface_14; }
	inline void set_mSurface_14(RuntimeObject* value)
	{
		___mSurface_14 = value;
		Il2CppCodeGenWriteBarrier((&___mSurface_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SURFACEABSTRACTBEHAVIOUR_T272541176_H
#ifndef PROPBEHAVIOUR_T3897855688_H
#define PROPBEHAVIOUR_T3897855688_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.PropBehaviour
struct  PropBehaviour_t3897855688  : public PropAbstractBehaviour_t2648882166
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROPBEHAVIOUR_T3897855688_H
#ifndef OBJECTTARGETBEHAVIOUR_T1761394684_H
#define OBJECTTARGETBEHAVIOUR_T1761394684_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.ObjectTargetBehaviour
struct  ObjectTargetBehaviour_t1761394684  : public ObjectTargetAbstractBehaviour_t1410714589
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OBJECTTARGETBEHAVIOUR_T1761394684_H
#ifndef MULTITARGETBEHAVIOUR_T687655925_H
#define MULTITARGETBEHAVIOUR_T687655925_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.MultiTargetBehaviour
struct  MultiTargetBehaviour_t687655925  : public MultiTargetAbstractBehaviour_t1527469731
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MULTITARGETBEHAVIOUR_T687655925_H
#ifndef SURFACEBEHAVIOUR_T1975960194_H
#define SURFACEBEHAVIOUR_T1975960194_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.SurfaceBehaviour
struct  SurfaceBehaviour_t1975960194  : public SurfaceAbstractBehaviour_t272541176
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SURFACEBEHAVIOUR_T1975960194_H
#ifndef CYLINDERTARGETBEHAVIOUR_T1224832031_H
#define CYLINDERTARGETBEHAVIOUR_T1224832031_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.CylinderTargetBehaviour
struct  CylinderTargetBehaviour_t1224832031  : public CylinderTargetAbstractBehaviour_t2158579059
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CYLINDERTARGETBEHAVIOUR_T1224832031_H
#ifndef IMAGETARGETBEHAVIOUR_T2918114530_H
#define IMAGETARGETBEHAVIOUR_T2918114530_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.ImageTargetBehaviour
struct  ImageTargetBehaviour_t2918114530  : public ImageTargetAbstractBehaviour_t355543434
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IMAGETARGETBEHAVIOUR_T2918114530_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2100 = { sizeof (U3CModuleU3E_t160650891), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2101 = { sizeof (FirstPersonController_t2645666525), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2101[30] = 
{
	FirstPersonController_t2645666525::get_offset_of_m_IsWalking_2(),
	FirstPersonController_t2645666525::get_offset_of_m_WalkSpeed_3(),
	FirstPersonController_t2645666525::get_offset_of_m_RunSpeed_4(),
	FirstPersonController_t2645666525::get_offset_of_m_RunstepLenghten_5(),
	FirstPersonController_t2645666525::get_offset_of_m_JumpSpeed_6(),
	FirstPersonController_t2645666525::get_offset_of_m_StickToGroundForce_7(),
	FirstPersonController_t2645666525::get_offset_of_m_GravityMultiplier_8(),
	FirstPersonController_t2645666525::get_offset_of_m_MouseLook_9(),
	FirstPersonController_t2645666525::get_offset_of_m_UseFovKick_10(),
	FirstPersonController_t2645666525::get_offset_of_m_FovKick_11(),
	FirstPersonController_t2645666525::get_offset_of_m_UseHeadBob_12(),
	FirstPersonController_t2645666525::get_offset_of_m_HeadBob_13(),
	FirstPersonController_t2645666525::get_offset_of_m_JumpBob_14(),
	FirstPersonController_t2645666525::get_offset_of_m_StepInterval_15(),
	FirstPersonController_t2645666525::get_offset_of_m_FootstepSounds_16(),
	FirstPersonController_t2645666525::get_offset_of_m_JumpSound_17(),
	FirstPersonController_t2645666525::get_offset_of_m_LandSound_18(),
	FirstPersonController_t2645666525::get_offset_of_m_Camera_19(),
	FirstPersonController_t2645666525::get_offset_of_m_Jump_20(),
	FirstPersonController_t2645666525::get_offset_of_m_YRotation_21(),
	FirstPersonController_t2645666525::get_offset_of_m_Input_22(),
	FirstPersonController_t2645666525::get_offset_of_m_MoveDir_23(),
	FirstPersonController_t2645666525::get_offset_of_m_CharacterController_24(),
	FirstPersonController_t2645666525::get_offset_of_m_CollisionFlags_25(),
	FirstPersonController_t2645666525::get_offset_of_m_PreviouslyGrounded_26(),
	FirstPersonController_t2645666525::get_offset_of_m_OriginalCameraPosition_27(),
	FirstPersonController_t2645666525::get_offset_of_m_StepCycle_28(),
	FirstPersonController_t2645666525::get_offset_of_m_NextStep_29(),
	FirstPersonController_t2645666525::get_offset_of_m_Jumping_30(),
	FirstPersonController_t2645666525::get_offset_of_m_AudioSource_31(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2102 = { sizeof (HeadBob_t1764489347), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2102[8] = 
{
	HeadBob_t1764489347::get_offset_of_Camera_2(),
	HeadBob_t1764489347::get_offset_of_motionBob_3(),
	HeadBob_t1764489347::get_offset_of_jumpAndLandingBob_4(),
	HeadBob_t1764489347::get_offset_of_rigidbodyFirstPersonController_5(),
	HeadBob_t1764489347::get_offset_of_StrideInterval_6(),
	HeadBob_t1764489347::get_offset_of_RunningStrideLengthen_7(),
	HeadBob_t1764489347::get_offset_of_m_PreviouslyGrounded_8(),
	HeadBob_t1764489347::get_offset_of_m_OriginalCameraPosition_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2103 = { sizeof (MouseLook_t3694953670), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2103[11] = 
{
	MouseLook_t3694953670::get_offset_of_XSensitivity_0(),
	MouseLook_t3694953670::get_offset_of_YSensitivity_1(),
	MouseLook_t3694953670::get_offset_of_clampVerticalRotation_2(),
	MouseLook_t3694953670::get_offset_of_MinimumX_3(),
	MouseLook_t3694953670::get_offset_of_MaximumX_4(),
	MouseLook_t3694953670::get_offset_of_smooth_5(),
	MouseLook_t3694953670::get_offset_of_smoothTime_6(),
	MouseLook_t3694953670::get_offset_of_lockCursor_7(),
	MouseLook_t3694953670::get_offset_of_m_CharacterTargetRot_8(),
	MouseLook_t3694953670::get_offset_of_m_CameraTargetRot_9(),
	MouseLook_t3694953670::get_offset_of_m_cursorIsLocked_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2104 = { sizeof (RigidbodyFirstPersonController_t1713505141), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2104[12] = 
{
	RigidbodyFirstPersonController_t1713505141::get_offset_of_cam_2(),
	RigidbodyFirstPersonController_t1713505141::get_offset_of_movementSettings_3(),
	RigidbodyFirstPersonController_t1713505141::get_offset_of_mouseLook_4(),
	RigidbodyFirstPersonController_t1713505141::get_offset_of_advancedSettings_5(),
	RigidbodyFirstPersonController_t1713505141::get_offset_of_m_RigidBody_6(),
	RigidbodyFirstPersonController_t1713505141::get_offset_of_m_Capsule_7(),
	RigidbodyFirstPersonController_t1713505141::get_offset_of_m_YRotation_8(),
	RigidbodyFirstPersonController_t1713505141::get_offset_of_m_GroundContactNormal_9(),
	RigidbodyFirstPersonController_t1713505141::get_offset_of_m_Jump_10(),
	RigidbodyFirstPersonController_t1713505141::get_offset_of_m_PreviouslyGrounded_11(),
	RigidbodyFirstPersonController_t1713505141::get_offset_of_m_Jumping_12(),
	RigidbodyFirstPersonController_t1713505141::get_offset_of_m_IsGrounded_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2105 = { sizeof (MovementSettings_t2494390400), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2105[8] = 
{
	MovementSettings_t2494390400::get_offset_of_ForwardSpeed_0(),
	MovementSettings_t2494390400::get_offset_of_BackwardSpeed_1(),
	MovementSettings_t2494390400::get_offset_of_StrafeSpeed_2(),
	MovementSettings_t2494390400::get_offset_of_RunMultiplier_3(),
	MovementSettings_t2494390400::get_offset_of_RunKey_4(),
	MovementSettings_t2494390400::get_offset_of_JumpForce_5(),
	MovementSettings_t2494390400::get_offset_of_SlopeCurveModifier_6(),
	MovementSettings_t2494390400::get_offset_of_CurrentTargetSpeed_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2106 = { sizeof (AdvancedSettings_t2632082398), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2106[5] = 
{
	AdvancedSettings_t2632082398::get_offset_of_groundCheckDistance_0(),
	AdvancedSettings_t2632082398::get_offset_of_stickToGroundHelperDistance_1(),
	AdvancedSettings_t2632082398::get_offset_of_slowDownRate_2(),
	AdvancedSettings_t2632082398::get_offset_of_airControl_3(),
	AdvancedSettings_t2632082398::get_offset_of_shellOffset_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2107 = { sizeof (Ball_t3802515361), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2107[6] = 
{
	Ball_t3802515361::get_offset_of_m_MovePower_2(),
	Ball_t3802515361::get_offset_of_m_UseTorque_3(),
	Ball_t3802515361::get_offset_of_m_MaxAngularVelocity_4(),
	Ball_t3802515361::get_offset_of_m_JumpPower_5(),
	0,
	Ball_t3802515361::get_offset_of_m_Rigidbody_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2108 = { sizeof (BallUserControl_t4201493832), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2108[5] = 
{
	BallUserControl_t4201493832::get_offset_of_ball_2(),
	BallUserControl_t4201493832::get_offset_of_move_3(),
	BallUserControl_t4201493832::get_offset_of_cam_4(),
	BallUserControl_t4201493832::get_offset_of_camForward_5(),
	BallUserControl_t4201493832::get_offset_of_jump_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2109 = { sizeof (AICharacterControl_t2038132910), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2109[3] = 
{
	AICharacterControl_t2038132910::get_offset_of_U3CagentU3Ek__BackingField_2(),
	AICharacterControl_t2038132910::get_offset_of_U3CcharacterU3Ek__BackingField_3(),
	AICharacterControl_t2038132910::get_offset_of_target_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2110 = { sizeof (ThirdPersonCharacter_t1426137179), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2110[20] = 
{
	ThirdPersonCharacter_t1426137179::get_offset_of_m_MovingTurnSpeed_2(),
	ThirdPersonCharacter_t1426137179::get_offset_of_m_StationaryTurnSpeed_3(),
	ThirdPersonCharacter_t1426137179::get_offset_of_m_JumpPower_4(),
	ThirdPersonCharacter_t1426137179::get_offset_of_m_GravityMultiplier_5(),
	ThirdPersonCharacter_t1426137179::get_offset_of_m_RunCycleLegOffset_6(),
	ThirdPersonCharacter_t1426137179::get_offset_of_m_MoveSpeedMultiplier_7(),
	ThirdPersonCharacter_t1426137179::get_offset_of_m_AnimSpeedMultiplier_8(),
	ThirdPersonCharacter_t1426137179::get_offset_of_m_GroundCheckDistance_9(),
	ThirdPersonCharacter_t1426137179::get_offset_of_m_Rigidbody_10(),
	ThirdPersonCharacter_t1426137179::get_offset_of_m_Animator_11(),
	ThirdPersonCharacter_t1426137179::get_offset_of_m_IsGrounded_12(),
	ThirdPersonCharacter_t1426137179::get_offset_of_m_OrigGroundCheckDistance_13(),
	0,
	ThirdPersonCharacter_t1426137179::get_offset_of_m_TurnAmount_15(),
	ThirdPersonCharacter_t1426137179::get_offset_of_m_ForwardAmount_16(),
	ThirdPersonCharacter_t1426137179::get_offset_of_m_GroundNormal_17(),
	ThirdPersonCharacter_t1426137179::get_offset_of_m_CapsuleHeight_18(),
	ThirdPersonCharacter_t1426137179::get_offset_of_m_CapsuleCenter_19(),
	ThirdPersonCharacter_t1426137179::get_offset_of_m_Capsule_20(),
	ThirdPersonCharacter_t1426137179::get_offset_of_m_Crouching_21(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2111 = { sizeof (ThirdPersonUserControl_t3559085786), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2111[5] = 
{
	ThirdPersonUserControl_t3559085786::get_offset_of_m_Character_2(),
	ThirdPersonUserControl_t3559085786::get_offset_of_m_Cam_3(),
	ThirdPersonUserControl_t3559085786::get_offset_of_m_CamForward_4(),
	ThirdPersonUserControl_t3559085786::get_offset_of_m_Move_5(),
	ThirdPersonUserControl_t3559085786::get_offset_of_m_Jump_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2112 = { sizeof (AxisTouchButton_t693686604), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2112[6] = 
{
	AxisTouchButton_t693686604::get_offset_of_axisName_2(),
	AxisTouchButton_t693686604::get_offset_of_axisValue_3(),
	AxisTouchButton_t693686604::get_offset_of_responseSpeed_4(),
	AxisTouchButton_t693686604::get_offset_of_returnToCentreSpeed_5(),
	AxisTouchButton_t693686604::get_offset_of_m_PairedWith_6(),
	AxisTouchButton_t693686604::get_offset_of_m_Axis_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2113 = { sizeof (ButtonHandler_t1472824933), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2113[1] = 
{
	ButtonHandler_t1472824933::get_offset_of_Name_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2114 = { sizeof (CrossPlatformInputManager_t2677747245), -1, sizeof(CrossPlatformInputManager_t2677747245_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2114[3] = 
{
	CrossPlatformInputManager_t2677747245_StaticFields::get_offset_of_activeInput_0(),
	CrossPlatformInputManager_t2677747245_StaticFields::get_offset_of_s_TouchInput_1(),
	CrossPlatformInputManager_t2677747245_StaticFields::get_offset_of_s_HardwareInput_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2115 = { sizeof (ActiveInputMethod_t3635810715)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2115[3] = 
{
	ActiveInputMethod_t3635810715::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2116 = { sizeof (VirtualAxis_t2383171311), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2116[3] = 
{
	VirtualAxis_t2383171311::get_offset_of_U3CnameU3Ek__BackingField_0(),
	VirtualAxis_t2383171311::get_offset_of_m_Value_1(),
	VirtualAxis_t2383171311::get_offset_of_U3CmatchWithInputManagerU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2117 = { sizeof (VirtualButton_t1545222198), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2117[5] = 
{
	VirtualButton_t1545222198::get_offset_of_U3CnameU3Ek__BackingField_0(),
	VirtualButton_t1545222198::get_offset_of_U3CmatchWithInputManagerU3Ek__BackingField_1(),
	VirtualButton_t1545222198::get_offset_of_m_LastPressedFrame_2(),
	VirtualButton_t1545222198::get_offset_of_m_ReleasedFrame_3(),
	VirtualButton_t1545222198::get_offset_of_m_Pressed_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2118 = { sizeof (InputAxisScrollbar_t2200868714), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2118[1] = 
{
	InputAxisScrollbar_t2200868714::get_offset_of_axis_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2119 = { sizeof (Joystick_t3754997822), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2119[9] = 
{
	Joystick_t3754997822::get_offset_of_MovementRange_2(),
	Joystick_t3754997822::get_offset_of_axesToUse_3(),
	Joystick_t3754997822::get_offset_of_horizontalAxisName_4(),
	Joystick_t3754997822::get_offset_of_verticalAxisName_5(),
	Joystick_t3754997822::get_offset_of_m_StartPos_6(),
	Joystick_t3754997822::get_offset_of_m_UseX_7(),
	Joystick_t3754997822::get_offset_of_m_UseY_8(),
	Joystick_t3754997822::get_offset_of_m_HorizontalVirtualAxis_9(),
	Joystick_t3754997822::get_offset_of_m_VerticalVirtualAxis_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2120 = { sizeof (AxisOption_t1620100083)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2120[4] = 
{
	AxisOption_t1620100083::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2121 = { sizeof (MobileControlRig_t3318571898), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2122 = { sizeof (MobileInput_t3557021844), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2123 = { sizeof (StandaloneInput_t2753768079), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2124 = { sizeof (TiltInput_t484673937), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2124[5] = 
{
	TiltInput_t484673937::get_offset_of_mapping_2(),
	TiltInput_t484673937::get_offset_of_tiltAroundAxis_3(),
	TiltInput_t484673937::get_offset_of_fullTiltAngle_4(),
	TiltInput_t484673937::get_offset_of_centreAngleOffset_5(),
	TiltInput_t484673937::get_offset_of_m_SteerAxis_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2125 = { sizeof (AxisOptions_t836301849)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2125[3] = 
{
	AxisOptions_t836301849::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2126 = { sizeof (AxisMapping_t2403352015), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2126[2] = 
{
	AxisMapping_t2403352015::get_offset_of_type_0(),
	AxisMapping_t2403352015::get_offset_of_axisName_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2127 = { sizeof (MappingType_t502031566)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2127[5] = 
{
	MappingType_t502031566::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2128 = { sizeof (TouchPad_t3890668177), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2128[18] = 
{
	TouchPad_t3890668177::get_offset_of_axesToUse_2(),
	TouchPad_t3890668177::get_offset_of_controlStyle_3(),
	TouchPad_t3890668177::get_offset_of_horizontalAxisName_4(),
	TouchPad_t3890668177::get_offset_of_verticalAxisName_5(),
	TouchPad_t3890668177::get_offset_of_Xsensitivity_6(),
	TouchPad_t3890668177::get_offset_of_Ysensitivity_7(),
	TouchPad_t3890668177::get_offset_of_m_StartPos_8(),
	TouchPad_t3890668177::get_offset_of_m_PreviousDelta_9(),
	TouchPad_t3890668177::get_offset_of_m_JoytickOutput_10(),
	TouchPad_t3890668177::get_offset_of_m_UseX_11(),
	TouchPad_t3890668177::get_offset_of_m_UseY_12(),
	TouchPad_t3890668177::get_offset_of_m_HorizontalVirtualAxis_13(),
	TouchPad_t3890668177::get_offset_of_m_VerticalVirtualAxis_14(),
	TouchPad_t3890668177::get_offset_of_m_Dragging_15(),
	TouchPad_t3890668177::get_offset_of_m_Id_16(),
	TouchPad_t3890668177::get_offset_of_m_PreviousTouchPos_17(),
	TouchPad_t3890668177::get_offset_of_m_Center_18(),
	TouchPad_t3890668177::get_offset_of_m_Image_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2129 = { sizeof (AxisOption_t438894465)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2129[4] = 
{
	AxisOption_t438894465::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2130 = { sizeof (ControlStyle_t26348249)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2130[4] = 
{
	ControlStyle_t26348249::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2131 = { sizeof (VirtualInput_t2381112337), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2131[4] = 
{
	VirtualInput_t2381112337::get_offset_of_U3CvirtualMousePositionU3Ek__BackingField_0(),
	VirtualInput_t2381112337::get_offset_of_m_VirtualAxes_1(),
	VirtualInput_t2381112337::get_offset_of_m_VirtualButtons_2(),
	VirtualInput_t2381112337::get_offset_of_m_AlwaysUseVirtual_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2132 = { sizeof (ActivateTrigger_t2507406930), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2132[5] = 
{
	ActivateTrigger_t2507406930::get_offset_of_action_2(),
	ActivateTrigger_t2507406930::get_offset_of_target_3(),
	ActivateTrigger_t2507406930::get_offset_of_source_4(),
	ActivateTrigger_t2507406930::get_offset_of_triggerCount_5(),
	ActivateTrigger_t2507406930::get_offset_of_repeatTrigger_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2133 = { sizeof (Mode_t1276594512)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2133[7] = 
{
	Mode_t1276594512::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2134 = { sizeof (AlphaButtonClickMask_t2290582035), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2134[1] = 
{
	AlphaButtonClickMask_t2290582035::get_offset_of__image_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2135 = { sizeof (AutoMobileShaderSwitch_t608167732), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2135[1] = 
{
	AutoMobileShaderSwitch_t608167732::get_offset_of_m_ReplacementList_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2136 = { sizeof (ReplacementDefinition_t1571218451), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2136[2] = 
{
	ReplacementDefinition_t1571218451::get_offset_of_original_0(),
	ReplacementDefinition_t1571218451::get_offset_of_replacement_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2137 = { sizeof (ReplacementList_t22610105), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2137[1] = 
{
	ReplacementList_t22610105::get_offset_of_items_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2138 = { sizeof (AutoMoveAndRotate_t607824664), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2138[4] = 
{
	AutoMoveAndRotate_t607824664::get_offset_of_moveUnitsPerSecond_2(),
	AutoMoveAndRotate_t607824664::get_offset_of_rotateDegreesPerSecond_3(),
	AutoMoveAndRotate_t607824664::get_offset_of_ignoreTimescale_4(),
	AutoMoveAndRotate_t607824664::get_offset_of_m_LastRealTime_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2139 = { sizeof (Vector3andSpace_t2340595124), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2139[2] = 
{
	Vector3andSpace_t2340595124::get_offset_of_value_0(),
	Vector3andSpace_t2340595124::get_offset_of_space_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2140 = { sizeof (CameraRefocus_t1375739319), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2140[5] = 
{
	CameraRefocus_t1375739319::get_offset_of_Camera_0(),
	CameraRefocus_t1375739319::get_offset_of_Lookatpoint_1(),
	CameraRefocus_t1375739319::get_offset_of_Parent_2(),
	CameraRefocus_t1375739319::get_offset_of_m_OrigCameraPos_3(),
	CameraRefocus_t1375739319::get_offset_of_m_Refocus_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2141 = { sizeof (CurveControlledBob_t3985701645), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2141[9] = 
{
	CurveControlledBob_t3985701645::get_offset_of_HorizontalBobRange_0(),
	CurveControlledBob_t3985701645::get_offset_of_VerticalBobRange_1(),
	CurveControlledBob_t3985701645::get_offset_of_Bobcurve_2(),
	CurveControlledBob_t3985701645::get_offset_of_VerticaltoHorizontalRatio_3(),
	CurveControlledBob_t3985701645::get_offset_of_m_CyclePositionX_4(),
	CurveControlledBob_t3985701645::get_offset_of_m_CyclePositionY_5(),
	CurveControlledBob_t3985701645::get_offset_of_m_BobBaseInterval_6(),
	CurveControlledBob_t3985701645::get_offset_of_m_OriginalCameraPosition_7(),
	CurveControlledBob_t3985701645::get_offset_of_m_Time_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2142 = { sizeof (DragRigidbody_t781943330), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2142[7] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	DragRigidbody_t781943330::get_offset_of_m_SpringJoint_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2143 = { sizeof (U3CDragObjectU3Ec__Iterator0_t2945102041), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2143[9] = 
{
	U3CDragObjectU3Ec__Iterator0_t2945102041::get_offset_of_U3ColdDragU3E__0_0(),
	U3CDragObjectU3Ec__Iterator0_t2945102041::get_offset_of_U3ColdAngularDragU3E__0_1(),
	U3CDragObjectU3Ec__Iterator0_t2945102041::get_offset_of_U3CmainCameraU3E__0_2(),
	U3CDragObjectU3Ec__Iterator0_t2945102041::get_offset_of_U3CrayU3E__1_3(),
	U3CDragObjectU3Ec__Iterator0_t2945102041::get_offset_of_distance_4(),
	U3CDragObjectU3Ec__Iterator0_t2945102041::get_offset_of_U24this_5(),
	U3CDragObjectU3Ec__Iterator0_t2945102041::get_offset_of_U24current_6(),
	U3CDragObjectU3Ec__Iterator0_t2945102041::get_offset_of_U24disposing_7(),
	U3CDragObjectU3Ec__Iterator0_t2945102041::get_offset_of_U24PC_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2144 = { sizeof (DynamicShadowSettings_t3505630543), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2144[11] = 
{
	DynamicShadowSettings_t3505630543::get_offset_of_sunLight_2(),
	DynamicShadowSettings_t3505630543::get_offset_of_minHeight_3(),
	DynamicShadowSettings_t3505630543::get_offset_of_minShadowDistance_4(),
	DynamicShadowSettings_t3505630543::get_offset_of_minShadowBias_5(),
	DynamicShadowSettings_t3505630543::get_offset_of_maxHeight_6(),
	DynamicShadowSettings_t3505630543::get_offset_of_maxShadowDistance_7(),
	DynamicShadowSettings_t3505630543::get_offset_of_maxShadowBias_8(),
	DynamicShadowSettings_t3505630543::get_offset_of_adaptTime_9(),
	DynamicShadowSettings_t3505630543::get_offset_of_m_SmoothHeight_10(),
	DynamicShadowSettings_t3505630543::get_offset_of_m_ChangeSpeed_11(),
	DynamicShadowSettings_t3505630543::get_offset_of_m_OriginalStrength_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2145 = { sizeof (EventSystemChecker_t2535489545), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2146 = { sizeof (FollowTarget_t702953223), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2146[2] = 
{
	FollowTarget_t702953223::get_offset_of_target_2(),
	FollowTarget_t702953223::get_offset_of_offset_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2147 = { sizeof (ForcedReset_t3887116877), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2148 = { sizeof (FOVKick_t1285422362), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2148[6] = 
{
	FOVKick_t1285422362::get_offset_of_Camera_0(),
	FOVKick_t1285422362::get_offset_of_originalFov_1(),
	FOVKick_t1285422362::get_offset_of_FOVIncrease_2(),
	FOVKick_t1285422362::get_offset_of_TimeToIncrease_3(),
	FOVKick_t1285422362::get_offset_of_TimeToDecrease_4(),
	FOVKick_t1285422362::get_offset_of_IncreaseCurve_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2149 = { sizeof (U3CFOVKickUpU3Ec__Iterator0_t3168521387), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2149[5] = 
{
	U3CFOVKickUpU3Ec__Iterator0_t3168521387::get_offset_of_U3CtU3E__0_0(),
	U3CFOVKickUpU3Ec__Iterator0_t3168521387::get_offset_of_U24this_1(),
	U3CFOVKickUpU3Ec__Iterator0_t3168521387::get_offset_of_U24current_2(),
	U3CFOVKickUpU3Ec__Iterator0_t3168521387::get_offset_of_U24disposing_3(),
	U3CFOVKickUpU3Ec__Iterator0_t3168521387::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2150 = { sizeof (U3CFOVKickDownU3Ec__Iterator1_t1100257635), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2150[5] = 
{
	U3CFOVKickDownU3Ec__Iterator1_t1100257635::get_offset_of_U3CtU3E__0_0(),
	U3CFOVKickDownU3Ec__Iterator1_t1100257635::get_offset_of_U24this_1(),
	U3CFOVKickDownU3Ec__Iterator1_t1100257635::get_offset_of_U24current_2(),
	U3CFOVKickDownU3Ec__Iterator1_t1100257635::get_offset_of_U24disposing_3(),
	U3CFOVKickDownU3Ec__Iterator1_t1100257635::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2151 = { sizeof (FPSCounter_t2273183518), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2151[6] = 
{
	0,
	FPSCounter_t2273183518::get_offset_of_m_FpsAccumulator_3(),
	FPSCounter_t2273183518::get_offset_of_m_FpsNextPeriod_4(),
	FPSCounter_t2273183518::get_offset_of_m_CurrentFps_5(),
	0,
	FPSCounter_t2273183518::get_offset_of_m_Text_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2152 = { sizeof (LerpControlledBob_t4065608754), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2152[3] = 
{
	LerpControlledBob_t4065608754::get_offset_of_BobDuration_0(),
	LerpControlledBob_t4065608754::get_offset_of_BobAmount_1(),
	LerpControlledBob_t4065608754::get_offset_of_m_Offset_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2153 = { sizeof (U3CDoBobCycleU3Ec__Iterator0_t1487389126), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2153[5] = 
{
	U3CDoBobCycleU3Ec__Iterator0_t1487389126::get_offset_of_U3CtU3E__0_0(),
	U3CDoBobCycleU3Ec__Iterator0_t1487389126::get_offset_of_U24this_1(),
	U3CDoBobCycleU3Ec__Iterator0_t1487389126::get_offset_of_U24current_2(),
	U3CDoBobCycleU3Ec__Iterator0_t1487389126::get_offset_of_U24disposing_3(),
	U3CDoBobCycleU3Ec__Iterator0_t1487389126::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2154 = { sizeof (ObjectResetter_t1579296102), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2154[4] = 
{
	ObjectResetter_t1579296102::get_offset_of_originalPosition_2(),
	ObjectResetter_t1579296102::get_offset_of_originalRotation_3(),
	ObjectResetter_t1579296102::get_offset_of_originalStructure_4(),
	ObjectResetter_t1579296102::get_offset_of_Rigidbody_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2155 = { sizeof (U3CResetCoroutineU3Ec__Iterator0_t4213400666), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2155[7] = 
{
	U3CResetCoroutineU3Ec__Iterator0_t4213400666::get_offset_of_delay_0(),
	U3CResetCoroutineU3Ec__Iterator0_t4213400666::get_offset_of_U24locvar0_1(),
	U3CResetCoroutineU3Ec__Iterator0_t4213400666::get_offset_of_U24locvar1_2(),
	U3CResetCoroutineU3Ec__Iterator0_t4213400666::get_offset_of_U24this_3(),
	U3CResetCoroutineU3Ec__Iterator0_t4213400666::get_offset_of_U24current_4(),
	U3CResetCoroutineU3Ec__Iterator0_t4213400666::get_offset_of_U24disposing_5(),
	U3CResetCoroutineU3Ec__Iterator0_t4213400666::get_offset_of_U24PC_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2156 = { sizeof (ParticleSystemDestroyer_t3293524282), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2156[4] = 
{
	ParticleSystemDestroyer_t3293524282::get_offset_of_minDuration_2(),
	ParticleSystemDestroyer_t3293524282::get_offset_of_maxDuration_3(),
	ParticleSystemDestroyer_t3293524282::get_offset_of_m_MaxLifetime_4(),
	ParticleSystemDestroyer_t3293524282::get_offset_of_m_EarlyStop_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2157 = { sizeof (U3CStartU3Ec__Iterator0_t3352859634), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2157[10] = 
{
	U3CStartU3Ec__Iterator0_t3352859634::get_offset_of_U3CsystemsU3E__0_0(),
	U3CStartU3Ec__Iterator0_t3352859634::get_offset_of_U24locvar0_1(),
	U3CStartU3Ec__Iterator0_t3352859634::get_offset_of_U24locvar1_2(),
	U3CStartU3Ec__Iterator0_t3352859634::get_offset_of_U3CstopTimeU3E__0_3(),
	U3CStartU3Ec__Iterator0_t3352859634::get_offset_of_U24locvar2_4(),
	U3CStartU3Ec__Iterator0_t3352859634::get_offset_of_U24locvar3_5(),
	U3CStartU3Ec__Iterator0_t3352859634::get_offset_of_U24this_6(),
	U3CStartU3Ec__Iterator0_t3352859634::get_offset_of_U24current_7(),
	U3CStartU3Ec__Iterator0_t3352859634::get_offset_of_U24disposing_8(),
	U3CStartU3Ec__Iterator0_t3352859634::get_offset_of_U24PC_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2158 = { sizeof (PlatformSpecificContent_t328033666), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2158[4] = 
{
	PlatformSpecificContent_t328033666::get_offset_of_m_BuildTargetGroup_2(),
	PlatformSpecificContent_t328033666::get_offset_of_m_Content_3(),
	PlatformSpecificContent_t328033666::get_offset_of_m_MonoBehaviours_4(),
	PlatformSpecificContent_t328033666::get_offset_of_m_ChildrenOfThisObject_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2159 = { sizeof (BuildTargetGroup_t3433781248)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2159[3] = 
{
	BuildTargetGroup_t3433781248::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2160 = { sizeof (SimpleActivatorMenu_t3675303537), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2160[3] = 
{
	SimpleActivatorMenu_t3675303537::get_offset_of_camSwitchButton_2(),
	SimpleActivatorMenu_t3675303537::get_offset_of_objects_3(),
	SimpleActivatorMenu_t3675303537::get_offset_of_m_CurrentActiveObject_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2161 = { sizeof (SimpleMouseRotator_t2630432798), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2161[10] = 
{
	SimpleMouseRotator_t2630432798::get_offset_of_rotationRange_2(),
	SimpleMouseRotator_t2630432798::get_offset_of_rotationSpeed_3(),
	SimpleMouseRotator_t2630432798::get_offset_of_dampingTime_4(),
	SimpleMouseRotator_t2630432798::get_offset_of_autoZeroVerticalOnMobile_5(),
	SimpleMouseRotator_t2630432798::get_offset_of_autoZeroHorizontalOnMobile_6(),
	SimpleMouseRotator_t2630432798::get_offset_of_relative_7(),
	SimpleMouseRotator_t2630432798::get_offset_of_m_TargetAngles_8(),
	SimpleMouseRotator_t2630432798::get_offset_of_m_FollowAngles_9(),
	SimpleMouseRotator_t2630432798::get_offset_of_m_FollowVelocity_10(),
	SimpleMouseRotator_t2630432798::get_offset_of_m_OriginalRotation_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2162 = { sizeof (SmoothFollow_t1265817179), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2162[5] = 
{
	SmoothFollow_t1265817179::get_offset_of_target_2(),
	SmoothFollow_t1265817179::get_offset_of_distance_3(),
	SmoothFollow_t1265817179::get_offset_of_height_4(),
	SmoothFollow_t1265817179::get_offset_of_rotationDamping_5(),
	SmoothFollow_t1265817179::get_offset_of_heightDamping_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2163 = { sizeof (TimedObjectActivator_t1049299440), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2163[1] = 
{
	TimedObjectActivator_t1049299440::get_offset_of_entries_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2164 = { sizeof (Action_t2958281111)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2164[6] = 
{
	Action_t2958281111::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2165 = { sizeof (Entry_t3279652951), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2165[3] = 
{
	Entry_t3279652951::get_offset_of_target_0(),
	Entry_t3279652951::get_offset_of_action_1(),
	Entry_t3279652951::get_offset_of_delay_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2166 = { sizeof (Entries_t1342860269), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2166[1] = 
{
	Entries_t1342860269::get_offset_of_entries_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2167 = { sizeof (U3CActivateU3Ec__Iterator0_t368634801), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2167[4] = 
{
	U3CActivateU3Ec__Iterator0_t368634801::get_offset_of_entry_0(),
	U3CActivateU3Ec__Iterator0_t368634801::get_offset_of_U24current_1(),
	U3CActivateU3Ec__Iterator0_t368634801::get_offset_of_U24disposing_2(),
	U3CActivateU3Ec__Iterator0_t368634801::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2168 = { sizeof (U3CDeactivateU3Ec__Iterator1_t3199820129), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2168[4] = 
{
	U3CDeactivateU3Ec__Iterator1_t3199820129::get_offset_of_entry_0(),
	U3CDeactivateU3Ec__Iterator1_t3199820129::get_offset_of_U24current_1(),
	U3CDeactivateU3Ec__Iterator1_t3199820129::get_offset_of_U24disposing_2(),
	U3CDeactivateU3Ec__Iterator1_t3199820129::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2169 = { sizeof (U3CReloadLevelU3Ec__Iterator2_t1713074067), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2169[4] = 
{
	U3CReloadLevelU3Ec__Iterator2_t1713074067::get_offset_of_entry_0(),
	U3CReloadLevelU3Ec__Iterator2_t1713074067::get_offset_of_U24current_1(),
	U3CReloadLevelU3Ec__Iterator2_t1713074067::get_offset_of_U24disposing_2(),
	U3CReloadLevelU3Ec__Iterator2_t1713074067::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2170 = { sizeof (TimedObjectDestructor_t2755931980), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2170[2] = 
{
	TimedObjectDestructor_t2755931980::get_offset_of_m_TimeOut_2(),
	TimedObjectDestructor_t2755931980::get_offset_of_m_DetachChildren_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2171 = { sizeof (WaypointCircuit_t802585562), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2171[16] = 
{
	WaypointCircuit_t802585562::get_offset_of_waypointList_2(),
	WaypointCircuit_t802585562::get_offset_of_smoothRoute_3(),
	WaypointCircuit_t802585562::get_offset_of_numPoints_4(),
	WaypointCircuit_t802585562::get_offset_of_points_5(),
	WaypointCircuit_t802585562::get_offset_of_distances_6(),
	WaypointCircuit_t802585562::get_offset_of_editorVisualisationSubsteps_7(),
	WaypointCircuit_t802585562::get_offset_of_U3CLengthU3Ek__BackingField_8(),
	WaypointCircuit_t802585562::get_offset_of_p0n_9(),
	WaypointCircuit_t802585562::get_offset_of_p1n_10(),
	WaypointCircuit_t802585562::get_offset_of_p2n_11(),
	WaypointCircuit_t802585562::get_offset_of_p3n_12(),
	WaypointCircuit_t802585562::get_offset_of_i_13(),
	WaypointCircuit_t802585562::get_offset_of_P0_14(),
	WaypointCircuit_t802585562::get_offset_of_P1_15(),
	WaypointCircuit_t802585562::get_offset_of_P2_16(),
	WaypointCircuit_t802585562::get_offset_of_P3_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2172 = { sizeof (WaypointList_t2428355280), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2172[2] = 
{
	WaypointList_t2428355280::get_offset_of_circuit_0(),
	WaypointList_t2428355280::get_offset_of_items_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2173 = { sizeof (RoutePoint_t4047115680)+ sizeof (RuntimeObject), sizeof(RoutePoint_t4047115680 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2173[2] = 
{
	RoutePoint_t4047115680::get_offset_of_position_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	RoutePoint_t4047115680::get_offset_of_direction_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2174 = { sizeof (WaypointProgressTracker_t3979672196), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2174[15] = 
{
	WaypointProgressTracker_t3979672196::get_offset_of_circuit_2(),
	WaypointProgressTracker_t3979672196::get_offset_of_lookAheadForTargetOffset_3(),
	WaypointProgressTracker_t3979672196::get_offset_of_lookAheadForTargetFactor_4(),
	WaypointProgressTracker_t3979672196::get_offset_of_lookAheadForSpeedOffset_5(),
	WaypointProgressTracker_t3979672196::get_offset_of_lookAheadForSpeedFactor_6(),
	WaypointProgressTracker_t3979672196::get_offset_of_progressStyle_7(),
	WaypointProgressTracker_t3979672196::get_offset_of_pointToPointThreshold_8(),
	WaypointProgressTracker_t3979672196::get_offset_of_U3CtargetPointU3Ek__BackingField_9(),
	WaypointProgressTracker_t3979672196::get_offset_of_U3CspeedPointU3Ek__BackingField_10(),
	WaypointProgressTracker_t3979672196::get_offset_of_U3CprogressPointU3Ek__BackingField_11(),
	WaypointProgressTracker_t3979672196::get_offset_of_target_12(),
	WaypointProgressTracker_t3979672196::get_offset_of_progressDistance_13(),
	WaypointProgressTracker_t3979672196::get_offset_of_progressNum_14(),
	WaypointProgressTracker_t3979672196::get_offset_of_lastPosition_15(),
	WaypointProgressTracker_t3979672196::get_offset_of_speed_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2175 = { sizeof (ProgressStyle_t2590666018)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2175[3] = 
{
	ProgressStyle_t2590666018::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2176 = { sizeof (U3CModuleU3E_t160650892), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2177 = { sizeof (SpiderController_t373354123), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2177[2] = 
{
	SpiderController_t373354123::get_offset_of_rb_2(),
	SpiderController_t373354123::get_offset_of_anim_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2178 = { sizeof (BackgroundPlaneBehaviour_t311858283), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2179 = { sizeof (CloudRecoBehaviour_t2989569978), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2180 = { sizeof (CylinderTargetBehaviour_t1224832031), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2181 = { sizeof (DefaultInitializationErrorHandler_t414875900), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2181[3] = 
{
	DefaultInitializationErrorHandler_t414875900::get_offset_of_mErrorText_2(),
	DefaultInitializationErrorHandler_t414875900::get_offset_of_mErrorOccurred_3(),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2182 = { sizeof (DefaultSmartTerrainEventHandler_t3202897208), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2182[3] = 
{
	DefaultSmartTerrainEventHandler_t3202897208::get_offset_of_mReconstructionBehaviour_2(),
	DefaultSmartTerrainEventHandler_t3202897208::get_offset_of_PropTemplate_3(),
	DefaultSmartTerrainEventHandler_t3202897208::get_offset_of_SurfaceTemplate_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2183 = { sizeof (DefaultTrackableEventHandler_t2331528573), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2183[1] = 
{
	DefaultTrackableEventHandler_t2331528573::get_offset_of_mTrackableBehaviour_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2184 = { sizeof (GLErrorHandler_t1216999495), -1, sizeof(GLErrorHandler_t1216999495_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2184[3] = 
{
	GLErrorHandler_t1216999495_StaticFields::get_offset_of_mErrorText_2(),
	GLErrorHandler_t1216999495_StaticFields::get_offset_of_mErrorOccurred_3(),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2185 = { sizeof (HideExcessAreaBehaviour_t554831333), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2186 = { sizeof (ImageTargetBehaviour_t2918114530), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2187 = { sizeof (AndroidUnityPlayer_t3571566439), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2187[6] = 
{
	0,
	0,
	AndroidUnityPlayer_t3571566439::get_offset_of_mScreenOrientation_2(),
	AndroidUnityPlayer_t3571566439::get_offset_of_mJavaScreenOrientation_3(),
	AndroidUnityPlayer_t3571566439::get_offset_of_mFramesSinceLastOrientationReset_4(),
	AndroidUnityPlayer_t3571566439::get_offset_of_mFramesSinceLastJavaOrientationCheck_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2188 = { sizeof (ComponentFactoryStarterBehaviour_t2906678938), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2189 = { sizeof (IOSUnityPlayer_t1005570149), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2189[1] = 
{
	IOSUnityPlayer_t1005570149::get_offset_of_mScreenOrientation_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2190 = { sizeof (VuforiaBehaviourComponentFactory_t2590131513), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2191 = { sizeof (WSAUnityPlayer_t1576425251), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2191[1] = 
{
	WSAUnityPlayer_t1576425251::get_offset_of_mScreenOrientation_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2192 = { sizeof (MaskOutBehaviour_t62027326), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2193 = { sizeof (MultiTargetBehaviour_t687655925), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2194 = { sizeof (ObjectTargetBehaviour_t1761394684), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2195 = { sizeof (PropBehaviour_t3897855688), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2196 = { sizeof (ReconstructionBehaviour_t3936121853), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2197 = { sizeof (ReconstructionFromTargetBehaviour_t2819884381), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2198 = { sizeof (SurfaceBehaviour_t1975960194), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2199 = { sizeof (TextRecoBehaviour_t3253755448), -1, 0, 0 };
#ifdef __clang__
#pragma clang diagnostic pop
#endif

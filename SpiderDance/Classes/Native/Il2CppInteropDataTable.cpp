﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"








extern "C" void Context_t302147483_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Context_t302147483_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Context_t302147483_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType Context_t302147483_0_0_0;
extern "C" void Escape_t1636068463_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Escape_t1636068463_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Escape_t1636068463_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType Escape_t1636068463_0_0_0;
extern "C" void PreviousInfo_t3099150937_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void PreviousInfo_t3099150937_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void PreviousInfo_t3099150937_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType PreviousInfo_t3099150937_0_0_0;
extern "C" void DelegatePInvokeWrapper_AppDomainInitializer_t477143072();
extern const RuntimeType AppDomainInitializer_t477143072_0_0_0;
extern "C" void DelegatePInvokeWrapper_Swapper_t194216602();
extern const RuntimeType Swapper_t194216602_0_0_0;
extern "C" void DictionaryEntry_t2976401038_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void DictionaryEntry_t2976401038_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void DictionaryEntry_t2976401038_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType DictionaryEntry_t2976401038_0_0_0;
extern "C" void Slot_t3782017575_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Slot_t3782017575_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Slot_t3782017575_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType Slot_t3782017575_0_0_0;
extern "C" void Slot_t2367449639_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Slot_t2367449639_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Slot_t2367449639_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType Slot_t2367449639_0_0_0;
extern "C" void Enum_t978401890_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Enum_t978401890_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Enum_t978401890_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType Enum_t978401890_0_0_0;
extern "C" void DelegatePInvokeWrapper_ReadDelegate_t1212028933();
extern const RuntimeType ReadDelegate_t1212028933_0_0_0;
extern "C" void DelegatePInvokeWrapper_WriteDelegate_t1649207064();
extern const RuntimeType WriteDelegate_t1649207064_0_0_0;
extern "C" void MonoIOStat_t3565773003_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void MonoIOStat_t3565773003_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void MonoIOStat_t3565773003_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType MonoIOStat_t3565773003_0_0_0;
extern "C" void MonoEnumInfo_t3653578247_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void MonoEnumInfo_t3653578247_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void MonoEnumInfo_t3653578247_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType MonoEnumInfo_t3653578247_0_0_0;
extern "C" void CustomAttributeNamedArgument_t468938427_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void CustomAttributeNamedArgument_t468938427_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void CustomAttributeNamedArgument_t468938427_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType CustomAttributeNamedArgument_t468938427_0_0_0;
extern "C" void CustomAttributeTypedArgument_t1650937354_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void CustomAttributeTypedArgument_t1650937354_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void CustomAttributeTypedArgument_t1650937354_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType CustomAttributeTypedArgument_t1650937354_0_0_0;
extern "C" void ILTokenInfo_t4112045227_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void ILTokenInfo_t4112045227_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void ILTokenInfo_t4112045227_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType ILTokenInfo_t4112045227_0_0_0;
extern "C" void MonoEventInfo_t3631846553_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void MonoEventInfo_t3631846553_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void MonoEventInfo_t3631846553_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType MonoEventInfo_t3631846553_0_0_0;
extern "C" void MonoMethodInfo_t1835660199_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void MonoMethodInfo_t1835660199_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void MonoMethodInfo_t1835660199_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType MonoMethodInfo_t1835660199_0_0_0;
extern "C" void MonoPropertyInfo_t438901902_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void MonoPropertyInfo_t438901902_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void MonoPropertyInfo_t438901902_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType MonoPropertyInfo_t438901902_0_0_0;
extern "C" void ParameterModifier_t2577558211_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void ParameterModifier_t2577558211_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void ParameterModifier_t2577558211_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType ParameterModifier_t2577558211_0_0_0;
extern "C" void ResourceCacheItem_t3669411578_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void ResourceCacheItem_t3669411578_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void ResourceCacheItem_t3669411578_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType ResourceCacheItem_t3669411578_0_0_0;
extern "C" void ResourceInfo_t3406613714_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void ResourceInfo_t3406613714_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void ResourceInfo_t3406613714_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType ResourceInfo_t3406613714_0_0_0;
extern "C" void DelegatePInvokeWrapper_CrossContextDelegate_t2939620309();
extern const RuntimeType CrossContextDelegate_t2939620309_0_0_0;
extern "C" void DelegatePInvokeWrapper_CallbackHandler_t2468618251();
extern const RuntimeType CallbackHandler_t2468618251_0_0_0;
extern "C" void SerializationEntry_t3747383008_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void SerializationEntry_t3747383008_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void SerializationEntry_t3747383008_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType SerializationEntry_t3747383008_0_0_0;
extern "C" void StreamingContext_t4278693462_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void StreamingContext_t4278693462_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void StreamingContext_t4278693462_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType StreamingContext_t4278693462_0_0_0;
extern "C" void DSAParameters_t517146465_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void DSAParameters_t517146465_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void DSAParameters_t517146465_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType DSAParameters_t517146465_0_0_0;
extern "C" void RSAParameters_t1949400183_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void RSAParameters_t1949400183_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void RSAParameters_t1949400183_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType RSAParameters_t1949400183_0_0_0;
extern "C" void SecurityFrame_t922960315_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void SecurityFrame_t922960315_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void SecurityFrame_t922960315_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType SecurityFrame_t922960315_0_0_0;
extern "C" void DelegatePInvokeWrapper_ThreadStart_t4244546428();
extern const RuntimeType ThreadStart_t4244546428_0_0_0;
extern "C" void ValueType_t1306072465_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void ValueType_t1306072465_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void ValueType_t1306072465_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType ValueType_t1306072465_0_0_0;
extern "C" void X509ChainStatus_t2640914292_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void X509ChainStatus_t2640914292_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void X509ChainStatus_t2640914292_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType X509ChainStatus_t2640914292_0_0_0;
extern "C" void IntStack_t1798017419_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void IntStack_t1798017419_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void IntStack_t1798017419_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType IntStack_t1798017419_0_0_0;
extern "C" void Interval_t2910910658_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Interval_t2910910658_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Interval_t2910910658_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType Interval_t2910910658_0_0_0;
extern "C" void DelegatePInvokeWrapper_CostDelegate_t2684266991();
extern const RuntimeType CostDelegate_t2684266991_0_0_0;
extern "C" void UriScheme_t3280900487_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void UriScheme_t3280900487_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void UriScheme_t3280900487_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType UriScheme_t3280900487_0_0_0;
extern "C" void DelegatePInvokeWrapper_Action_t3619184611();
extern const RuntimeType Action_t3619184611_0_0_0;
extern "C" void DelegatePInvokeWrapper_OnNavMeshPreUpdate_t3968231997();
extern const RuntimeType OnNavMeshPreUpdate_t3968231997_0_0_0;
extern "C" void CustomEventData_t3696846854_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void CustomEventData_t3696846854_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void CustomEventData_t3696846854_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType CustomEventData_t3696846854_0_0_0;
extern "C" void UnityAnalyticsHandler_t922256166_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void UnityAnalyticsHandler_t922256166_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void UnityAnalyticsHandler_t922256166_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType UnityAnalyticsHandler_t922256166_0_0_0;
extern "C" void AnimationCurve_t2184836714_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void AnimationCurve_t2184836714_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void AnimationCurve_t2184836714_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType AnimationCurve_t2184836714_0_0_0;
extern "C" void AnimationEvent_t2906863783_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void AnimationEvent_t2906863783_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void AnimationEvent_t2906863783_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType AnimationEvent_t2906863783_0_0_0;
extern "C" void AnimatorTransitionInfo_t4040367972_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void AnimatorTransitionInfo_t4040367972_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void AnimatorTransitionInfo_t4040367972_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType AnimatorTransitionInfo_t4040367972_0_0_0;
extern "C" void DelegatePInvokeWrapper_LogCallback_t974229335();
extern const RuntimeType LogCallback_t974229335_0_0_0;
extern "C" void DelegatePInvokeWrapper_LowMemoryCallback_t2472087382();
extern const RuntimeType LowMemoryCallback_t2472087382_0_0_0;
extern "C" void AssetBundleRequest_t2081915472_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void AssetBundleRequest_t2081915472_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void AssetBundleRequest_t2081915472_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType AssetBundleRequest_t2081915472_0_0_0;
extern "C" void AsyncOperation_t2744032732_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void AsyncOperation_t2744032732_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void AsyncOperation_t2744032732_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType AsyncOperation_t2744032732_0_0_0;
extern "C" void DelegatePInvokeWrapper_PCMReaderCallback_t4196704670();
extern const RuntimeType PCMReaderCallback_t4196704670_0_0_0;
extern "C" void DelegatePInvokeWrapper_PCMSetPositionCallback_t1529165644();
extern const RuntimeType PCMSetPositionCallback_t1529165644_0_0_0;
extern "C" void DelegatePInvokeWrapper_AudioConfigurationChangeHandler_t4211095080();
extern const RuntimeType AudioConfigurationChangeHandler_t4211095080_0_0_0;
extern "C" void DelegatePInvokeWrapper_WillRenderCanvases_t1524812347();
extern const RuntimeType WillRenderCanvases_t1524812347_0_0_0;
extern "C" void Collision_t250273015_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Collision_t250273015_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Collision_t250273015_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType Collision_t250273015_0_0_0;
extern "C" void ControllerColliderHit_t3984690091_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void ControllerColliderHit_t3984690091_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void ControllerColliderHit_t3984690091_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType ControllerColliderHit_t3984690091_0_0_0;
extern "C" void Coroutine_t2992159144_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Coroutine_t2992159144_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Coroutine_t2992159144_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType Coroutine_t2992159144_0_0_0;
extern "C" void DelegatePInvokeWrapper_CSSMeasureFunc_t3500966164();
extern const RuntimeType CSSMeasureFunc_t3500966164_0_0_0;
extern "C" void CullingGroup_t785600452_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void CullingGroup_t785600452_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void CullingGroup_t785600452_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType CullingGroup_t785600452_0_0_0;
extern "C" void DelegatePInvokeWrapper_StateChanged_t1846550948();
extern const RuntimeType StateChanged_t1846550948_0_0_0;
extern "C" void DelegatePInvokeWrapper_DisplaysUpdatedDelegate_t2316085723();
extern const RuntimeType DisplaysUpdatedDelegate_t2316085723_0_0_0;
extern "C" void Event_t2016371179_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Event_t2016371179_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Event_t2016371179_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType Event_t2016371179_0_0_0;
extern "C" void DelegatePInvokeWrapper_UnityAction_t178163897();
extern const RuntimeType UnityAction_t178163897_0_0_0;
extern "C" void FailedToLoadScriptObject_t2849411290_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void FailedToLoadScriptObject_t2849411290_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void FailedToLoadScriptObject_t2849411290_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType FailedToLoadScriptObject_t2849411290_0_0_0;
extern "C" void DelegatePInvokeWrapper_FontTextureRebuildCallback_t793818912();
extern const RuntimeType FontTextureRebuildCallback_t793818912_0_0_0;
extern "C" void Gradient_t2853445639_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Gradient_t2853445639_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Gradient_t2853445639_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType Gradient_t2853445639_0_0_0;
extern "C" void DelegatePInvokeWrapper_WindowFunction_t3577962812();
extern const RuntimeType WindowFunction_t3577962812_0_0_0;
extern "C" void GUIContent_t277514532_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void GUIContent_t277514532_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void GUIContent_t277514532_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType GUIContent_t277514532_0_0_0;
extern "C" void DelegatePInvokeWrapper_SkinChangedDelegate_t2555826545();
extern const RuntimeType SkinChangedDelegate_t2555826545_0_0_0;
extern "C" void GUIStyle_t3341239576_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void GUIStyle_t3341239576_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void GUIStyle_t3341239576_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType GUIStyle_t3341239576_0_0_0;
extern "C" void GUIStyleState_t945464788_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void GUIStyleState_t945464788_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void GUIStyleState_t945464788_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType GUIStyleState_t945464788_0_0_0;
extern "C" void HostData_t909994897_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void HostData_t909994897_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void HostData_t909994897_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType HostData_t909994897_0_0_0;
extern "C" void HumanBone_t202171525_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void HumanBone_t202171525_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void HumanBone_t202171525_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType HumanBone_t202171525_0_0_0;
extern "C" void Object_t1502412432_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Object_t1502412432_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Object_t1502412432_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType Object_t1502412432_0_0_0;
extern "C" void EmissionModule_t216550349_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void EmissionModule_t216550349_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void EmissionModule_t216550349_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType EmissionModule_t216550349_0_0_0;
extern "C" void MainModule_t2858435356_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void MainModule_t2858435356_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void MainModule_t2858435356_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType MainModule_t2858435356_0_0_0;
extern "C" void MinMaxCurve_t2961159436_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void MinMaxCurve_t2961159436_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void MinMaxCurve_t2961159436_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType MinMaxCurve_t2961159436_0_0_0;
extern "C" void PlayableBinding_t3706191642_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void PlayableBinding_t3706191642_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void PlayableBinding_t3706191642_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType PlayableBinding_t3706191642_0_0_0;
extern "C" void RaycastHit_t2327766465_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void RaycastHit_t2327766465_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void RaycastHit_t2327766465_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType RaycastHit_t2327766465_0_0_0;
extern "C" void RaycastHit2D_t1105181991_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void RaycastHit2D_t1105181991_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void RaycastHit2D_t1105181991_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType RaycastHit2D_t1105181991_0_0_0;
extern "C" void RectOffset_t1315541452_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void RectOffset_t1315541452_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void RectOffset_t1315541452_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType RectOffset_t1315541452_0_0_0;
extern "C" void DelegatePInvokeWrapper_UpdatedEventHandler_t729869569();
extern const RuntimeType UpdatedEventHandler_t729869569_0_0_0;
extern "C" void ResourceRequest_t963543698_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void ResourceRequest_t963543698_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void ResourceRequest_t963543698_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType ResourceRequest_t963543698_0_0_0;
extern "C" void ScriptableObject_t3635579074_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void ScriptableObject_t3635579074_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void ScriptableObject_t3635579074_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType ScriptableObject_t3635579074_0_0_0;
extern "C" void HitInfo_t2000020299_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void HitInfo_t2000020299_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void HitInfo_t2000020299_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType HitInfo_t2000020299_0_0_0;
extern "C" void SkeletonBone_t3530196780_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void SkeletonBone_t3530196780_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void SkeletonBone_t3530196780_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType SkeletonBone_t3530196780_0_0_0;
extern "C" void GcAchievementData_t130881817_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void GcAchievementData_t130881817_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void GcAchievementData_t130881817_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType GcAchievementData_t130881817_0_0_0;
extern "C" void GcAchievementDescriptionData_t3546866232_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void GcAchievementDescriptionData_t3546866232_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void GcAchievementDescriptionData_t3546866232_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType GcAchievementDescriptionData_t3546866232_0_0_0;
extern "C" void GcLeaderboard_t3877110677_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void GcLeaderboard_t3877110677_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void GcLeaderboard_t3877110677_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType GcLeaderboard_t3877110677_0_0_0;
extern "C" void GcScoreData_t2307565855_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void GcScoreData_t2307565855_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void GcScoreData_t2307565855_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType GcScoreData_t2307565855_0_0_0;
extern "C" void GcUserProfileData_t3081075717_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void GcUserProfileData_t3081075717_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void GcUserProfileData_t3081075717_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType GcUserProfileData_t3081075717_0_0_0;
extern "C" void TextGenerationSettings_t1547620708_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void TextGenerationSettings_t1547620708_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void TextGenerationSettings_t1547620708_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType TextGenerationSettings_t1547620708_0_0_0;
extern "C" void TextGenerator_t1816786767_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void TextGenerator_t1816786767_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void TextGenerator_t1816786767_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType TextGenerator_t1816786767_0_0_0;
extern "C" void TrackedReference_t788981845_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void TrackedReference_t788981845_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void TrackedReference_t788981845_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType TrackedReference_t788981845_0_0_0;
extern "C" void DelegatePInvokeWrapper_RequestAtlasCallback_t2815162418();
extern const RuntimeType RequestAtlasCallback_t2815162418_0_0_0;
extern "C" void WorkRequest_t1434254581_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void WorkRequest_t1434254581_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void WorkRequest_t1434254581_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType WorkRequest_t1434254581_0_0_0;
extern "C" void WaitForSeconds_t2153438233_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void WaitForSeconds_t2153438233_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void WaitForSeconds_t2153438233_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType WaitForSeconds_t2153438233_0_0_0;
extern "C" void WebCamDevice_t312959275_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void WebCamDevice_t312959275_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void WebCamDevice_t312959275_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType WebCamDevice_t312959275_0_0_0;
extern "C" void YieldInstruction_t1893592390_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void YieldInstruction_t1893592390_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void YieldInstruction_t1893592390_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType YieldInstruction_t1893592390_0_0_0;
extern "C" void RaycastResult_t3511189758_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void RaycastResult_t3511189758_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void RaycastResult_t3511189758_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType RaycastResult_t3511189758_0_0_0;
extern "C" void ColorTween_t3915639497_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void ColorTween_t3915639497_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void ColorTween_t3915639497_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType ColorTween_t3915639497_0_0_0;
extern "C" void FloatTween_t3485850636_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void FloatTween_t3485850636_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void FloatTween_t3485850636_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType FloatTween_t3485850636_0_0_0;
extern "C" void Resources_t3325762479_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Resources_t3325762479_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Resources_t3325762479_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType Resources_t3325762479_0_0_0;
extern "C" void DelegatePInvokeWrapper_OnValidateInput_t1678856956();
extern const RuntimeType OnValidateInput_t1678856956_0_0_0;
extern "C" void Navigation_t3595639716_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Navigation_t3595639716_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Navigation_t3595639716_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType Navigation_t3595639716_0_0_0;
extern "C" void SpriteState_t3525508784_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void SpriteState_t3525508784_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void SpriteState_t3525508784_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType SpriteState_t3525508784_0_0_0;
extern "C" void CameraField_t382214397_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void CameraField_t382214397_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void CameraField_t382214397_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType CameraField_t382214397_0_0_0;
extern "C" void CameraFieldData_t3856148331_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void CameraFieldData_t3856148331_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void CameraFieldData_t3856148331_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType CameraFieldData_t3856148331_0_0_0;
extern "C" void EyewearCalibrationReading_t972160870_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void EyewearCalibrationReading_t972160870_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void EyewearCalibrationReading_t972160870_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType EyewearCalibrationReading_t972160870_0_0_0;
extern "C" void TargetSearchResult_t213694407_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void TargetSearchResult_t213694407_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void TargetSearchResult_t213694407_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType TargetSearchResult_t213694407_0_0_0;
extern "C" void AutoRotationState_t545146858_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void AutoRotationState_t545146858_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void AutoRotationState_t545146858_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType AutoRotationState_t545146858_0_0_0;
extern "C" void ProfileCollection_t3131405081_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void ProfileCollection_t3131405081_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void ProfileCollection_t3131405081_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType ProfileCollection_t3131405081_0_0_0;
extern Il2CppInteropData g_Il2CppInteropData[110] = 
{
	{ NULL, Context_t302147483_marshal_pinvoke, Context_t302147483_marshal_pinvoke_back, Context_t302147483_marshal_pinvoke_cleanup, NULL, NULL, &Context_t302147483_0_0_0 } /* Mono.Globalization.Unicode.SimpleCollator/Context */,
	{ NULL, Escape_t1636068463_marshal_pinvoke, Escape_t1636068463_marshal_pinvoke_back, Escape_t1636068463_marshal_pinvoke_cleanup, NULL, NULL, &Escape_t1636068463_0_0_0 } /* Mono.Globalization.Unicode.SimpleCollator/Escape */,
	{ NULL, PreviousInfo_t3099150937_marshal_pinvoke, PreviousInfo_t3099150937_marshal_pinvoke_back, PreviousInfo_t3099150937_marshal_pinvoke_cleanup, NULL, NULL, &PreviousInfo_t3099150937_0_0_0 } /* Mono.Globalization.Unicode.SimpleCollator/PreviousInfo */,
	{ DelegatePInvokeWrapper_AppDomainInitializer_t477143072, NULL, NULL, NULL, NULL, NULL, &AppDomainInitializer_t477143072_0_0_0 } /* System.AppDomainInitializer */,
	{ DelegatePInvokeWrapper_Swapper_t194216602, NULL, NULL, NULL, NULL, NULL, &Swapper_t194216602_0_0_0 } /* System.Array/Swapper */,
	{ NULL, DictionaryEntry_t2976401038_marshal_pinvoke, DictionaryEntry_t2976401038_marshal_pinvoke_back, DictionaryEntry_t2976401038_marshal_pinvoke_cleanup, NULL, NULL, &DictionaryEntry_t2976401038_0_0_0 } /* System.Collections.DictionaryEntry */,
	{ NULL, Slot_t3782017575_marshal_pinvoke, Slot_t3782017575_marshal_pinvoke_back, Slot_t3782017575_marshal_pinvoke_cleanup, NULL, NULL, &Slot_t3782017575_0_0_0 } /* System.Collections.Hashtable/Slot */,
	{ NULL, Slot_t2367449639_marshal_pinvoke, Slot_t2367449639_marshal_pinvoke_back, Slot_t2367449639_marshal_pinvoke_cleanup, NULL, NULL, &Slot_t2367449639_0_0_0 } /* System.Collections.SortedList/Slot */,
	{ NULL, Enum_t978401890_marshal_pinvoke, Enum_t978401890_marshal_pinvoke_back, Enum_t978401890_marshal_pinvoke_cleanup, NULL, NULL, &Enum_t978401890_0_0_0 } /* System.Enum */,
	{ DelegatePInvokeWrapper_ReadDelegate_t1212028933, NULL, NULL, NULL, NULL, NULL, &ReadDelegate_t1212028933_0_0_0 } /* System.IO.FileStream/ReadDelegate */,
	{ DelegatePInvokeWrapper_WriteDelegate_t1649207064, NULL, NULL, NULL, NULL, NULL, &WriteDelegate_t1649207064_0_0_0 } /* System.IO.FileStream/WriteDelegate */,
	{ NULL, MonoIOStat_t3565773003_marshal_pinvoke, MonoIOStat_t3565773003_marshal_pinvoke_back, MonoIOStat_t3565773003_marshal_pinvoke_cleanup, NULL, NULL, &MonoIOStat_t3565773003_0_0_0 } /* System.IO.MonoIOStat */,
	{ NULL, MonoEnumInfo_t3653578247_marshal_pinvoke, MonoEnumInfo_t3653578247_marshal_pinvoke_back, MonoEnumInfo_t3653578247_marshal_pinvoke_cleanup, NULL, NULL, &MonoEnumInfo_t3653578247_0_0_0 } /* System.MonoEnumInfo */,
	{ NULL, CustomAttributeNamedArgument_t468938427_marshal_pinvoke, CustomAttributeNamedArgument_t468938427_marshal_pinvoke_back, CustomAttributeNamedArgument_t468938427_marshal_pinvoke_cleanup, NULL, NULL, &CustomAttributeNamedArgument_t468938427_0_0_0 } /* System.Reflection.CustomAttributeNamedArgument */,
	{ NULL, CustomAttributeTypedArgument_t1650937354_marshal_pinvoke, CustomAttributeTypedArgument_t1650937354_marshal_pinvoke_back, CustomAttributeTypedArgument_t1650937354_marshal_pinvoke_cleanup, NULL, NULL, &CustomAttributeTypedArgument_t1650937354_0_0_0 } /* System.Reflection.CustomAttributeTypedArgument */,
	{ NULL, ILTokenInfo_t4112045227_marshal_pinvoke, ILTokenInfo_t4112045227_marshal_pinvoke_back, ILTokenInfo_t4112045227_marshal_pinvoke_cleanup, NULL, NULL, &ILTokenInfo_t4112045227_0_0_0 } /* System.Reflection.Emit.ILTokenInfo */,
	{ NULL, MonoEventInfo_t3631846553_marshal_pinvoke, MonoEventInfo_t3631846553_marshal_pinvoke_back, MonoEventInfo_t3631846553_marshal_pinvoke_cleanup, NULL, NULL, &MonoEventInfo_t3631846553_0_0_0 } /* System.Reflection.MonoEventInfo */,
	{ NULL, MonoMethodInfo_t1835660199_marshal_pinvoke, MonoMethodInfo_t1835660199_marshal_pinvoke_back, MonoMethodInfo_t1835660199_marshal_pinvoke_cleanup, NULL, NULL, &MonoMethodInfo_t1835660199_0_0_0 } /* System.Reflection.MonoMethodInfo */,
	{ NULL, MonoPropertyInfo_t438901902_marshal_pinvoke, MonoPropertyInfo_t438901902_marshal_pinvoke_back, MonoPropertyInfo_t438901902_marshal_pinvoke_cleanup, NULL, NULL, &MonoPropertyInfo_t438901902_0_0_0 } /* System.Reflection.MonoPropertyInfo */,
	{ NULL, ParameterModifier_t2577558211_marshal_pinvoke, ParameterModifier_t2577558211_marshal_pinvoke_back, ParameterModifier_t2577558211_marshal_pinvoke_cleanup, NULL, NULL, &ParameterModifier_t2577558211_0_0_0 } /* System.Reflection.ParameterModifier */,
	{ NULL, ResourceCacheItem_t3669411578_marshal_pinvoke, ResourceCacheItem_t3669411578_marshal_pinvoke_back, ResourceCacheItem_t3669411578_marshal_pinvoke_cleanup, NULL, NULL, &ResourceCacheItem_t3669411578_0_0_0 } /* System.Resources.ResourceReader/ResourceCacheItem */,
	{ NULL, ResourceInfo_t3406613714_marshal_pinvoke, ResourceInfo_t3406613714_marshal_pinvoke_back, ResourceInfo_t3406613714_marshal_pinvoke_cleanup, NULL, NULL, &ResourceInfo_t3406613714_0_0_0 } /* System.Resources.ResourceReader/ResourceInfo */,
	{ DelegatePInvokeWrapper_CrossContextDelegate_t2939620309, NULL, NULL, NULL, NULL, NULL, &CrossContextDelegate_t2939620309_0_0_0 } /* System.Runtime.Remoting.Contexts.CrossContextDelegate */,
	{ DelegatePInvokeWrapper_CallbackHandler_t2468618251, NULL, NULL, NULL, NULL, NULL, &CallbackHandler_t2468618251_0_0_0 } /* System.Runtime.Serialization.SerializationCallbacks/CallbackHandler */,
	{ NULL, SerializationEntry_t3747383008_marshal_pinvoke, SerializationEntry_t3747383008_marshal_pinvoke_back, SerializationEntry_t3747383008_marshal_pinvoke_cleanup, NULL, NULL, &SerializationEntry_t3747383008_0_0_0 } /* System.Runtime.Serialization.SerializationEntry */,
	{ NULL, StreamingContext_t4278693462_marshal_pinvoke, StreamingContext_t4278693462_marshal_pinvoke_back, StreamingContext_t4278693462_marshal_pinvoke_cleanup, NULL, NULL, &StreamingContext_t4278693462_0_0_0 } /* System.Runtime.Serialization.StreamingContext */,
	{ NULL, DSAParameters_t517146465_marshal_pinvoke, DSAParameters_t517146465_marshal_pinvoke_back, DSAParameters_t517146465_marshal_pinvoke_cleanup, NULL, NULL, &DSAParameters_t517146465_0_0_0 } /* System.Security.Cryptography.DSAParameters */,
	{ NULL, RSAParameters_t1949400183_marshal_pinvoke, RSAParameters_t1949400183_marshal_pinvoke_back, RSAParameters_t1949400183_marshal_pinvoke_cleanup, NULL, NULL, &RSAParameters_t1949400183_0_0_0 } /* System.Security.Cryptography.RSAParameters */,
	{ NULL, SecurityFrame_t922960315_marshal_pinvoke, SecurityFrame_t922960315_marshal_pinvoke_back, SecurityFrame_t922960315_marshal_pinvoke_cleanup, NULL, NULL, &SecurityFrame_t922960315_0_0_0 } /* System.Security.SecurityFrame */,
	{ DelegatePInvokeWrapper_ThreadStart_t4244546428, NULL, NULL, NULL, NULL, NULL, &ThreadStart_t4244546428_0_0_0 } /* System.Threading.ThreadStart */,
	{ NULL, ValueType_t1306072465_marshal_pinvoke, ValueType_t1306072465_marshal_pinvoke_back, ValueType_t1306072465_marshal_pinvoke_cleanup, NULL, NULL, &ValueType_t1306072465_0_0_0 } /* System.ValueType */,
	{ NULL, X509ChainStatus_t2640914292_marshal_pinvoke, X509ChainStatus_t2640914292_marshal_pinvoke_back, X509ChainStatus_t2640914292_marshal_pinvoke_cleanup, NULL, NULL, &X509ChainStatus_t2640914292_0_0_0 } /* System.Security.Cryptography.X509Certificates.X509ChainStatus */,
	{ NULL, IntStack_t1798017419_marshal_pinvoke, IntStack_t1798017419_marshal_pinvoke_back, IntStack_t1798017419_marshal_pinvoke_cleanup, NULL, NULL, &IntStack_t1798017419_0_0_0 } /* System.Text.RegularExpressions.Interpreter/IntStack */,
	{ NULL, Interval_t2910910658_marshal_pinvoke, Interval_t2910910658_marshal_pinvoke_back, Interval_t2910910658_marshal_pinvoke_cleanup, NULL, NULL, &Interval_t2910910658_0_0_0 } /* System.Text.RegularExpressions.Interval */,
	{ DelegatePInvokeWrapper_CostDelegate_t2684266991, NULL, NULL, NULL, NULL, NULL, &CostDelegate_t2684266991_0_0_0 } /* System.Text.RegularExpressions.IntervalCollection/CostDelegate */,
	{ NULL, UriScheme_t3280900487_marshal_pinvoke, UriScheme_t3280900487_marshal_pinvoke_back, UriScheme_t3280900487_marshal_pinvoke_cleanup, NULL, NULL, &UriScheme_t3280900487_0_0_0 } /* System.Uri/UriScheme */,
	{ DelegatePInvokeWrapper_Action_t3619184611, NULL, NULL, NULL, NULL, NULL, &Action_t3619184611_0_0_0 } /* System.Action */,
	{ DelegatePInvokeWrapper_OnNavMeshPreUpdate_t3968231997, NULL, NULL, NULL, NULL, NULL, &OnNavMeshPreUpdate_t3968231997_0_0_0 } /* UnityEngine.AI.NavMesh/OnNavMeshPreUpdate */,
	{ NULL, CustomEventData_t3696846854_marshal_pinvoke, CustomEventData_t3696846854_marshal_pinvoke_back, CustomEventData_t3696846854_marshal_pinvoke_cleanup, NULL, NULL, &CustomEventData_t3696846854_0_0_0 } /* UnityEngine.Analytics.CustomEventData */,
	{ NULL, UnityAnalyticsHandler_t922256166_marshal_pinvoke, UnityAnalyticsHandler_t922256166_marshal_pinvoke_back, UnityAnalyticsHandler_t922256166_marshal_pinvoke_cleanup, NULL, NULL, &UnityAnalyticsHandler_t922256166_0_0_0 } /* UnityEngine.Analytics.UnityAnalyticsHandler */,
	{ NULL, AnimationCurve_t2184836714_marshal_pinvoke, AnimationCurve_t2184836714_marshal_pinvoke_back, AnimationCurve_t2184836714_marshal_pinvoke_cleanup, NULL, NULL, &AnimationCurve_t2184836714_0_0_0 } /* UnityEngine.AnimationCurve */,
	{ NULL, AnimationEvent_t2906863783_marshal_pinvoke, AnimationEvent_t2906863783_marshal_pinvoke_back, AnimationEvent_t2906863783_marshal_pinvoke_cleanup, NULL, NULL, &AnimationEvent_t2906863783_0_0_0 } /* UnityEngine.AnimationEvent */,
	{ NULL, AnimatorTransitionInfo_t4040367972_marshal_pinvoke, AnimatorTransitionInfo_t4040367972_marshal_pinvoke_back, AnimatorTransitionInfo_t4040367972_marshal_pinvoke_cleanup, NULL, NULL, &AnimatorTransitionInfo_t4040367972_0_0_0 } /* UnityEngine.AnimatorTransitionInfo */,
	{ DelegatePInvokeWrapper_LogCallback_t974229335, NULL, NULL, NULL, NULL, NULL, &LogCallback_t974229335_0_0_0 } /* UnityEngine.Application/LogCallback */,
	{ DelegatePInvokeWrapper_LowMemoryCallback_t2472087382, NULL, NULL, NULL, NULL, NULL, &LowMemoryCallback_t2472087382_0_0_0 } /* UnityEngine.Application/LowMemoryCallback */,
	{ NULL, AssetBundleRequest_t2081915472_marshal_pinvoke, AssetBundleRequest_t2081915472_marshal_pinvoke_back, AssetBundleRequest_t2081915472_marshal_pinvoke_cleanup, NULL, NULL, &AssetBundleRequest_t2081915472_0_0_0 } /* UnityEngine.AssetBundleRequest */,
	{ NULL, AsyncOperation_t2744032732_marshal_pinvoke, AsyncOperation_t2744032732_marshal_pinvoke_back, AsyncOperation_t2744032732_marshal_pinvoke_cleanup, NULL, NULL, &AsyncOperation_t2744032732_0_0_0 } /* UnityEngine.AsyncOperation */,
	{ DelegatePInvokeWrapper_PCMReaderCallback_t4196704670, NULL, NULL, NULL, NULL, NULL, &PCMReaderCallback_t4196704670_0_0_0 } /* UnityEngine.AudioClip/PCMReaderCallback */,
	{ DelegatePInvokeWrapper_PCMSetPositionCallback_t1529165644, NULL, NULL, NULL, NULL, NULL, &PCMSetPositionCallback_t1529165644_0_0_0 } /* UnityEngine.AudioClip/PCMSetPositionCallback */,
	{ DelegatePInvokeWrapper_AudioConfigurationChangeHandler_t4211095080, NULL, NULL, NULL, NULL, NULL, &AudioConfigurationChangeHandler_t4211095080_0_0_0 } /* UnityEngine.AudioSettings/AudioConfigurationChangeHandler */,
	{ DelegatePInvokeWrapper_WillRenderCanvases_t1524812347, NULL, NULL, NULL, NULL, NULL, &WillRenderCanvases_t1524812347_0_0_0 } /* UnityEngine.Canvas/WillRenderCanvases */,
	{ NULL, Collision_t250273015_marshal_pinvoke, Collision_t250273015_marshal_pinvoke_back, Collision_t250273015_marshal_pinvoke_cleanup, NULL, NULL, &Collision_t250273015_0_0_0 } /* UnityEngine.Collision */,
	{ NULL, ControllerColliderHit_t3984690091_marshal_pinvoke, ControllerColliderHit_t3984690091_marshal_pinvoke_back, ControllerColliderHit_t3984690091_marshal_pinvoke_cleanup, NULL, NULL, &ControllerColliderHit_t3984690091_0_0_0 } /* UnityEngine.ControllerColliderHit */,
	{ NULL, Coroutine_t2992159144_marshal_pinvoke, Coroutine_t2992159144_marshal_pinvoke_back, Coroutine_t2992159144_marshal_pinvoke_cleanup, NULL, NULL, &Coroutine_t2992159144_0_0_0 } /* UnityEngine.Coroutine */,
	{ DelegatePInvokeWrapper_CSSMeasureFunc_t3500966164, NULL, NULL, NULL, NULL, NULL, &CSSMeasureFunc_t3500966164_0_0_0 } /* UnityEngine.CSSLayout.CSSMeasureFunc */,
	{ NULL, CullingGroup_t785600452_marshal_pinvoke, CullingGroup_t785600452_marshal_pinvoke_back, CullingGroup_t785600452_marshal_pinvoke_cleanup, NULL, NULL, &CullingGroup_t785600452_0_0_0 } /* UnityEngine.CullingGroup */,
	{ DelegatePInvokeWrapper_StateChanged_t1846550948, NULL, NULL, NULL, NULL, NULL, &StateChanged_t1846550948_0_0_0 } /* UnityEngine.CullingGroup/StateChanged */,
	{ DelegatePInvokeWrapper_DisplaysUpdatedDelegate_t2316085723, NULL, NULL, NULL, NULL, NULL, &DisplaysUpdatedDelegate_t2316085723_0_0_0 } /* UnityEngine.Display/DisplaysUpdatedDelegate */,
	{ NULL, Event_t2016371179_marshal_pinvoke, Event_t2016371179_marshal_pinvoke_back, Event_t2016371179_marshal_pinvoke_cleanup, NULL, NULL, &Event_t2016371179_0_0_0 } /* UnityEngine.Event */,
	{ DelegatePInvokeWrapper_UnityAction_t178163897, NULL, NULL, NULL, NULL, NULL, &UnityAction_t178163897_0_0_0 } /* UnityEngine.Events.UnityAction */,
	{ NULL, FailedToLoadScriptObject_t2849411290_marshal_pinvoke, FailedToLoadScriptObject_t2849411290_marshal_pinvoke_back, FailedToLoadScriptObject_t2849411290_marshal_pinvoke_cleanup, NULL, NULL, &FailedToLoadScriptObject_t2849411290_0_0_0 } /* UnityEngine.FailedToLoadScriptObject */,
	{ DelegatePInvokeWrapper_FontTextureRebuildCallback_t793818912, NULL, NULL, NULL, NULL, NULL, &FontTextureRebuildCallback_t793818912_0_0_0 } /* UnityEngine.Font/FontTextureRebuildCallback */,
	{ NULL, Gradient_t2853445639_marshal_pinvoke, Gradient_t2853445639_marshal_pinvoke_back, Gradient_t2853445639_marshal_pinvoke_cleanup, NULL, NULL, &Gradient_t2853445639_0_0_0 } /* UnityEngine.Gradient */,
	{ DelegatePInvokeWrapper_WindowFunction_t3577962812, NULL, NULL, NULL, NULL, NULL, &WindowFunction_t3577962812_0_0_0 } /* UnityEngine.GUI/WindowFunction */,
	{ NULL, GUIContent_t277514532_marshal_pinvoke, GUIContent_t277514532_marshal_pinvoke_back, GUIContent_t277514532_marshal_pinvoke_cleanup, NULL, NULL, &GUIContent_t277514532_0_0_0 } /* UnityEngine.GUIContent */,
	{ DelegatePInvokeWrapper_SkinChangedDelegate_t2555826545, NULL, NULL, NULL, NULL, NULL, &SkinChangedDelegate_t2555826545_0_0_0 } /* UnityEngine.GUISkin/SkinChangedDelegate */,
	{ NULL, GUIStyle_t3341239576_marshal_pinvoke, GUIStyle_t3341239576_marshal_pinvoke_back, GUIStyle_t3341239576_marshal_pinvoke_cleanup, NULL, NULL, &GUIStyle_t3341239576_0_0_0 } /* UnityEngine.GUIStyle */,
	{ NULL, GUIStyleState_t945464788_marshal_pinvoke, GUIStyleState_t945464788_marshal_pinvoke_back, GUIStyleState_t945464788_marshal_pinvoke_cleanup, NULL, NULL, &GUIStyleState_t945464788_0_0_0 } /* UnityEngine.GUIStyleState */,
	{ NULL, HostData_t909994897_marshal_pinvoke, HostData_t909994897_marshal_pinvoke_back, HostData_t909994897_marshal_pinvoke_cleanup, NULL, NULL, &HostData_t909994897_0_0_0 } /* UnityEngine.HostData */,
	{ NULL, HumanBone_t202171525_marshal_pinvoke, HumanBone_t202171525_marshal_pinvoke_back, HumanBone_t202171525_marshal_pinvoke_cleanup, NULL, NULL, &HumanBone_t202171525_0_0_0 } /* UnityEngine.HumanBone */,
	{ NULL, Object_t1502412432_marshal_pinvoke, Object_t1502412432_marshal_pinvoke_back, Object_t1502412432_marshal_pinvoke_cleanup, NULL, NULL, &Object_t1502412432_0_0_0 } /* UnityEngine.Object */,
	{ NULL, EmissionModule_t216550349_marshal_pinvoke, EmissionModule_t216550349_marshal_pinvoke_back, EmissionModule_t216550349_marshal_pinvoke_cleanup, NULL, NULL, &EmissionModule_t216550349_0_0_0 } /* UnityEngine.ParticleSystem/EmissionModule */,
	{ NULL, MainModule_t2858435356_marshal_pinvoke, MainModule_t2858435356_marshal_pinvoke_back, MainModule_t2858435356_marshal_pinvoke_cleanup, NULL, NULL, &MainModule_t2858435356_0_0_0 } /* UnityEngine.ParticleSystem/MainModule */,
	{ NULL, MinMaxCurve_t2961159436_marshal_pinvoke, MinMaxCurve_t2961159436_marshal_pinvoke_back, MinMaxCurve_t2961159436_marshal_pinvoke_cleanup, NULL, NULL, &MinMaxCurve_t2961159436_0_0_0 } /* UnityEngine.ParticleSystem/MinMaxCurve */,
	{ NULL, PlayableBinding_t3706191642_marshal_pinvoke, PlayableBinding_t3706191642_marshal_pinvoke_back, PlayableBinding_t3706191642_marshal_pinvoke_cleanup, NULL, NULL, &PlayableBinding_t3706191642_0_0_0 } /* UnityEngine.Playables.PlayableBinding */,
	{ NULL, RaycastHit_t2327766465_marshal_pinvoke, RaycastHit_t2327766465_marshal_pinvoke_back, RaycastHit_t2327766465_marshal_pinvoke_cleanup, NULL, NULL, &RaycastHit_t2327766465_0_0_0 } /* UnityEngine.RaycastHit */,
	{ NULL, RaycastHit2D_t1105181991_marshal_pinvoke, RaycastHit2D_t1105181991_marshal_pinvoke_back, RaycastHit2D_t1105181991_marshal_pinvoke_cleanup, NULL, NULL, &RaycastHit2D_t1105181991_0_0_0 } /* UnityEngine.RaycastHit2D */,
	{ NULL, RectOffset_t1315541452_marshal_pinvoke, RectOffset_t1315541452_marshal_pinvoke_back, RectOffset_t1315541452_marshal_pinvoke_cleanup, NULL, NULL, &RectOffset_t1315541452_0_0_0 } /* UnityEngine.RectOffset */,
	{ DelegatePInvokeWrapper_UpdatedEventHandler_t729869569, NULL, NULL, NULL, NULL, NULL, &UpdatedEventHandler_t729869569_0_0_0 } /* UnityEngine.RemoteSettings/UpdatedEventHandler */,
	{ NULL, ResourceRequest_t963543698_marshal_pinvoke, ResourceRequest_t963543698_marshal_pinvoke_back, ResourceRequest_t963543698_marshal_pinvoke_cleanup, NULL, NULL, &ResourceRequest_t963543698_0_0_0 } /* UnityEngine.ResourceRequest */,
	{ NULL, ScriptableObject_t3635579074_marshal_pinvoke, ScriptableObject_t3635579074_marshal_pinvoke_back, ScriptableObject_t3635579074_marshal_pinvoke_cleanup, NULL, NULL, &ScriptableObject_t3635579074_0_0_0 } /* UnityEngine.ScriptableObject */,
	{ NULL, HitInfo_t2000020299_marshal_pinvoke, HitInfo_t2000020299_marshal_pinvoke_back, HitInfo_t2000020299_marshal_pinvoke_cleanup, NULL, NULL, &HitInfo_t2000020299_0_0_0 } /* UnityEngine.SendMouseEvents/HitInfo */,
	{ NULL, SkeletonBone_t3530196780_marshal_pinvoke, SkeletonBone_t3530196780_marshal_pinvoke_back, SkeletonBone_t3530196780_marshal_pinvoke_cleanup, NULL, NULL, &SkeletonBone_t3530196780_0_0_0 } /* UnityEngine.SkeletonBone */,
	{ NULL, GcAchievementData_t130881817_marshal_pinvoke, GcAchievementData_t130881817_marshal_pinvoke_back, GcAchievementData_t130881817_marshal_pinvoke_cleanup, NULL, NULL, &GcAchievementData_t130881817_0_0_0 } /* UnityEngine.SocialPlatforms.GameCenter.GcAchievementData */,
	{ NULL, GcAchievementDescriptionData_t3546866232_marshal_pinvoke, GcAchievementDescriptionData_t3546866232_marshal_pinvoke_back, GcAchievementDescriptionData_t3546866232_marshal_pinvoke_cleanup, NULL, NULL, &GcAchievementDescriptionData_t3546866232_0_0_0 } /* UnityEngine.SocialPlatforms.GameCenter.GcAchievementDescriptionData */,
	{ NULL, GcLeaderboard_t3877110677_marshal_pinvoke, GcLeaderboard_t3877110677_marshal_pinvoke_back, GcLeaderboard_t3877110677_marshal_pinvoke_cleanup, NULL, NULL, &GcLeaderboard_t3877110677_0_0_0 } /* UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard */,
	{ NULL, GcScoreData_t2307565855_marshal_pinvoke, GcScoreData_t2307565855_marshal_pinvoke_back, GcScoreData_t2307565855_marshal_pinvoke_cleanup, NULL, NULL, &GcScoreData_t2307565855_0_0_0 } /* UnityEngine.SocialPlatforms.GameCenter.GcScoreData */,
	{ NULL, GcUserProfileData_t3081075717_marshal_pinvoke, GcUserProfileData_t3081075717_marshal_pinvoke_back, GcUserProfileData_t3081075717_marshal_pinvoke_cleanup, NULL, NULL, &GcUserProfileData_t3081075717_0_0_0 } /* UnityEngine.SocialPlatforms.GameCenter.GcUserProfileData */,
	{ NULL, TextGenerationSettings_t1547620708_marshal_pinvoke, TextGenerationSettings_t1547620708_marshal_pinvoke_back, TextGenerationSettings_t1547620708_marshal_pinvoke_cleanup, NULL, NULL, &TextGenerationSettings_t1547620708_0_0_0 } /* UnityEngine.TextGenerationSettings */,
	{ NULL, TextGenerator_t1816786767_marshal_pinvoke, TextGenerator_t1816786767_marshal_pinvoke_back, TextGenerator_t1816786767_marshal_pinvoke_cleanup, NULL, NULL, &TextGenerator_t1816786767_0_0_0 } /* UnityEngine.TextGenerator */,
	{ NULL, TrackedReference_t788981845_marshal_pinvoke, TrackedReference_t788981845_marshal_pinvoke_back, TrackedReference_t788981845_marshal_pinvoke_cleanup, NULL, NULL, &TrackedReference_t788981845_0_0_0 } /* UnityEngine.TrackedReference */,
	{ DelegatePInvokeWrapper_RequestAtlasCallback_t2815162418, NULL, NULL, NULL, NULL, NULL, &RequestAtlasCallback_t2815162418_0_0_0 } /* UnityEngine.U2D.SpriteAtlasManager/RequestAtlasCallback */,
	{ NULL, WorkRequest_t1434254581_marshal_pinvoke, WorkRequest_t1434254581_marshal_pinvoke_back, WorkRequest_t1434254581_marshal_pinvoke_cleanup, NULL, NULL, &WorkRequest_t1434254581_0_0_0 } /* UnityEngine.UnitySynchronizationContext/WorkRequest */,
	{ NULL, WaitForSeconds_t2153438233_marshal_pinvoke, WaitForSeconds_t2153438233_marshal_pinvoke_back, WaitForSeconds_t2153438233_marshal_pinvoke_cleanup, NULL, NULL, &WaitForSeconds_t2153438233_0_0_0 } /* UnityEngine.WaitForSeconds */,
	{ NULL, WebCamDevice_t312959275_marshal_pinvoke, WebCamDevice_t312959275_marshal_pinvoke_back, WebCamDevice_t312959275_marshal_pinvoke_cleanup, NULL, NULL, &WebCamDevice_t312959275_0_0_0 } /* UnityEngine.WebCamDevice */,
	{ NULL, YieldInstruction_t1893592390_marshal_pinvoke, YieldInstruction_t1893592390_marshal_pinvoke_back, YieldInstruction_t1893592390_marshal_pinvoke_cleanup, NULL, NULL, &YieldInstruction_t1893592390_0_0_0 } /* UnityEngine.YieldInstruction */,
	{ NULL, RaycastResult_t3511189758_marshal_pinvoke, RaycastResult_t3511189758_marshal_pinvoke_back, RaycastResult_t3511189758_marshal_pinvoke_cleanup, NULL, NULL, &RaycastResult_t3511189758_0_0_0 } /* UnityEngine.EventSystems.RaycastResult */,
	{ NULL, ColorTween_t3915639497_marshal_pinvoke, ColorTween_t3915639497_marshal_pinvoke_back, ColorTween_t3915639497_marshal_pinvoke_cleanup, NULL, NULL, &ColorTween_t3915639497_0_0_0 } /* UnityEngine.UI.CoroutineTween.ColorTween */,
	{ NULL, FloatTween_t3485850636_marshal_pinvoke, FloatTween_t3485850636_marshal_pinvoke_back, FloatTween_t3485850636_marshal_pinvoke_cleanup, NULL, NULL, &FloatTween_t3485850636_0_0_0 } /* UnityEngine.UI.CoroutineTween.FloatTween */,
	{ NULL, Resources_t3325762479_marshal_pinvoke, Resources_t3325762479_marshal_pinvoke_back, Resources_t3325762479_marshal_pinvoke_cleanup, NULL, NULL, &Resources_t3325762479_0_0_0 } /* UnityEngine.UI.DefaultControls/Resources */,
	{ DelegatePInvokeWrapper_OnValidateInput_t1678856956, NULL, NULL, NULL, NULL, NULL, &OnValidateInput_t1678856956_0_0_0 } /* UnityEngine.UI.InputField/OnValidateInput */,
	{ NULL, Navigation_t3595639716_marshal_pinvoke, Navigation_t3595639716_marshal_pinvoke_back, Navigation_t3595639716_marshal_pinvoke_cleanup, NULL, NULL, &Navigation_t3595639716_0_0_0 } /* UnityEngine.UI.Navigation */,
	{ NULL, SpriteState_t3525508784_marshal_pinvoke, SpriteState_t3525508784_marshal_pinvoke_back, SpriteState_t3525508784_marshal_pinvoke_cleanup, NULL, NULL, &SpriteState_t3525508784_0_0_0 } /* UnityEngine.UI.SpriteState */,
	{ NULL, CameraField_t382214397_marshal_pinvoke, CameraField_t382214397_marshal_pinvoke_back, CameraField_t382214397_marshal_pinvoke_cleanup, NULL, NULL, &CameraField_t382214397_0_0_0 } /* Vuforia.CameraDevice/CameraField */,
	{ NULL, CameraFieldData_t3856148331_marshal_pinvoke, CameraFieldData_t3856148331_marshal_pinvoke_back, CameraFieldData_t3856148331_marshal_pinvoke_cleanup, NULL, NULL, &CameraFieldData_t3856148331_0_0_0 } /* Vuforia.CameraDeviceImpl/CameraFieldData */,
	{ NULL, EyewearCalibrationReading_t972160870_marshal_pinvoke, EyewearCalibrationReading_t972160870_marshal_pinvoke_back, EyewearCalibrationReading_t972160870_marshal_pinvoke_cleanup, NULL, NULL, &EyewearCalibrationReading_t972160870_0_0_0 } /* Vuforia.EyewearDevice/EyewearCalibrationReading */,
	{ NULL, TargetSearchResult_t213694407_marshal_pinvoke, TargetSearchResult_t213694407_marshal_pinvoke_back, TargetSearchResult_t213694407_marshal_pinvoke_cleanup, NULL, NULL, &TargetSearchResult_t213694407_0_0_0 } /* Vuforia.TargetFinder/TargetSearchResult */,
	{ NULL, AutoRotationState_t545146858_marshal_pinvoke, AutoRotationState_t545146858_marshal_pinvoke_back, AutoRotationState_t545146858_marshal_pinvoke_cleanup, NULL, NULL, &AutoRotationState_t545146858_0_0_0 } /* Vuforia.VuforiaManagerImpl/AutoRotationState */,
	{ NULL, ProfileCollection_t3131405081_marshal_pinvoke, ProfileCollection_t3131405081_marshal_pinvoke_back, ProfileCollection_t3131405081_marshal_pinvoke_cleanup, NULL, NULL, &ProfileCollection_t3131405081_0_0_0 } /* Vuforia.WebCamProfile/ProfileCollection */,
	NULL,
};
